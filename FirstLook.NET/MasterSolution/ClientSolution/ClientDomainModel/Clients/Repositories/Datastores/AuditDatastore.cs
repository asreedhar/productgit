﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// An audit datastore.
    /// </summary>
    public class AuditDatastore : SessionDataStore, IAuditDatastore
    {        
        /// <summary>
        /// Fetch the audit record that is the parent to the audit row with the given idenditifer.
        /// </summary>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with audit values.</returns>
        public IDataReader Audit_Fetch(int auditRowId)
        {
            const string fetch = Prefix + ".AuditDatastore_Audit_Fetch_AuditRow.txt";

            return Query(new[] { "Audit" }, fetch, new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        /// <summary>
        /// Insert an audit for an action performed by the user that is valid starting now.
        /// </summary>
        /// <param name="user">User identifier.</param>
        /// <returns>Data reader with the audit identifier.</returns>
        public IDataReader Audit_Insert(int user)
        {
            const string insert = Prefix + ".AuditDatastore_Audit_Insert.txt";

            NonQuery(insert,
                     new Parameter("User", user, DbType.Int32));

            const string fetch = Prefix + ".AuditDatastore_Audit_Fetch_Identity.txt";

            return Query(new[] {"Audit"}, fetch);
        }

        /// <summary>
        /// Insert an audit row that is a child of the audit with the given identifier, and that is of a particular
        /// type of action, and is valid starting now.
        /// </summary>
        /// <param name="auditId">Audit identifier.</param>
        /// <param name="wasInsert">Was this audited action an insert?</param>
        /// <param name="wasUpdate">Was this audited action an update?</param>
        /// <param name="wasDelete">Was this audited action a delete?</param>
        /// <returns>Data reader with audit row values.</returns>
        public IDataReader AuditRow_Insert(int auditId, bool wasInsert, bool wasUpdate, bool wasDelete)
        {
            const string insert = Prefix + ".AuditDatastore_AuditRow_Insert.txt";

            NonQuery(insert,
                     new Parameter("AuditID", auditId, DbType.Int32),                     
                     new Parameter("WasInsert", wasInsert, DbType.Boolean),
                     new Parameter("WasUpdate", wasUpdate, DbType.Boolean),
                     new Parameter("WasDelete", wasDelete, DbType.Boolean));

            const string fetch = Prefix + ".AuditDatastore_AuditRow_Fetch_Identity.txt";

            return Query(new[] {"AuditRow"}, fetch);
        }

        /// <summary>
        /// Update the audit row with the given identifier to no longer be valid.
        /// </summary>
        /// <param name="id">Audit row identifier.</param>
        public void AuditRow_Update(int id)
        {
            const string update = Prefix + ".AuditDatastore_AuditRow_Update.txt";

            NonQuery(update, new Parameter("AuditRowID", id, DbType.Int32));
        }

        /// <summary>
        /// Insert an association audit row.
        /// </summary>
        /// <returns>Data reader with the association identifier.</returns>
        public IDataReader Association_Insert()
        {
            const string insert = Prefix + ".AuditDatastore_Association_Insert.txt";

            NonQuery(insert);

            const string fetch = Prefix + ".AuditDatastore_Association_Fetch_Identity.txt";

            return Query(new[] { "Association" }, fetch);
        }

        /// <summary>
        /// Insert an historical record for an audited association.
        /// </summary>
        /// <param name="associationId">Association identifier.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with the identifier of the association historical identifier.</returns>
        public IDataReader Association_History_Insert(int associationId, int auditRowId)
        {
            const string insert = Prefix + ".AuditDatastore_Association_History_Insert.txt";

            NonQuery(insert,
                new Parameter("AssociationId", associationId, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            const string fetch = Prefix + ".AuditDatastore_Association_History_Fetch_Id.txt";

            return Query(new[] { "Association" }, fetch, new Parameter("AssociationID", associationId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the identifier of the current historical association record for the association with the given 
        /// identifier.
        /// </summary>
        /// <param name="associationId">Association identifier.</param>
        /// <returns>Data reader with the identifier of the association historical record.</returns>
        public IDataReader Association_History_Fetch(int associationId)
        {
            const string fetch = Prefix + ".AuditDatastore_Association_History_Fetch_Id.txt";

            return Query(
                new[] { "Association" },
                fetch,
                new Parameter("AssociationID", associationId, DbType.Int32));
        }

        #region Miscellaneous

        /// <summary>
        /// Base location of sql scripts.
        /// </summary>
        private const string Prefix = "FirstLook.Client.DomainModel.Clients.Repositories.Resources";

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion
    }
}
