﻿using System;
using System.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Interface for an auditing datastore.
    /// </summary>
    public interface IAuditDatastore
    {
        /// <summary>
        /// Fetch the audit record that is the parent to the audit row with the given idenditifer.
        /// </summary>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with audit values.</returns>
        IDataReader Audit_Fetch(int auditRowId);

        /// <summary>
        /// Insert an audit for an action performed by the user that is valid starting now.
        /// </summary>
        /// <param name="user">User identifier.</param>
        /// <returns>Data reader with the audit identifier.</returns>
        IDataReader Audit_Insert(int user);

        /// <summary>
        /// Insert an audit row that is a child of the audit with the given identifier, and that is of a particular
        /// type of action, and is valid starting now.
        /// </summary>
        /// <param name="auditId">Audit identifier.</param>
        /// <param name="wasInsert">Was this audited action an insert?</param>
        /// <param name="wasUpdate">Was this audited action an update?</param>
        /// <param name="wasDelete">Was this audited action a delete?</param>
        /// <returns>Data reader with audit row values.</returns>
        IDataReader AuditRow_Insert(int auditId, bool wasInsert, bool wasUpdate, bool wasDelete);

        /// <summary>
        /// Update the audit row with the given identifier to no longer be valid.
        /// </summary>
        /// <param name="id">Audit row identifier.</param>
        void AuditRow_Update(int id);

        /// <summary>
        /// Insert an association audit row.
        /// </summary>
        /// <returns>Data reader with the association identifier.</returns>
        IDataReader Association_Insert();

        /// <summary>
        /// Insert an historical record for an audited association.
        /// </summary>
        /// <param name="associationId">Association identifier.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with the identifier of the association historical identifier.</returns>
        IDataReader Association_History_Insert(int associationId, int auditRowId);

        /// <summary>
        /// Fetch the identifier of the current historical association record for the association with the given 
        /// identifier.
        /// </summary>
        /// <param name="associationId">Association identifier.</param>
        /// <returns>Data reader with the identifier of the association historical record.</returns>
        IDataReader Association_History_Fetch(int associationId);
    }
}
