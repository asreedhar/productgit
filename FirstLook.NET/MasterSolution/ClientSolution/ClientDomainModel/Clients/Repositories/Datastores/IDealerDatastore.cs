﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Interface for a dealer datastore. For operations on retrieving dealers and members.
    /// </summary>
    public interface IDealerDatastore
    {
        #region Dealer

        /// <summary>
        /// Fetch the dealer with the given dealer identifier.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Data reader with dealer values.</returns>
        IDataReader Dealer(int dealerId);

        /// <summary>
        /// Fetch the paginated list of dealers that match the provided example.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="example">Example dealer to match results by.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Data reader with a paginated list of matching dealers.</returns>
        IDataReader Dealers(string userName, IClient example, PageArguments pagination);

        /// <summary>
        /// Fetch the dealer with the given identifier if it is accessible to the user with the given username.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Data reader with dealer values.</returns>
        IDataReader Dealer(string userName, int dealerId);

        #endregion

        #region Member

        /// <summary>
        /// Fetch the member with the given username.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <returns>Data reader with member values.</returns>
        IDataReader Member(string userName);

        /// <summary>
        /// Fetch the member with the given identifier.
        /// </summary>
        /// <param name="memberId">Member identifier.</param>
        /// <returns>Data reader with member values.</returns>
        IDataReader Member(int memberId);

        /// <summary>
        /// Fetch the members tied to the dealer with the given identifier.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Data reader with members values.</returns>
        IDataReader Members(int dealerId);

        #endregion
    }
}
