﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Interface for a client datastore. Performs tasks on: clients; the mapping of clients to specific client types;
    /// users; and the mapping of users to specific user types.
    /// </summary>
    public interface IClientDatastore
    {
        #region Clients

        /// <summary>
        /// Get the identifying details of the client with the given handle.
        /// </summary>
        /// <param name="handle">Client handle.</param>
        /// <returns>Data reader with client identifiers.</returns>
        IDataReader Client(Guid handle);

        /// <summary>
        /// For the given identifier that is specific to a type of client (e.g dealer identifier for dealers, seller
        /// identifier for sellers, etc), get the client identifying details.        
        /// </summary>        
        /// <param name="type">Client type (e.g. Dealer, Seller).</param>
        /// <param name="id">Client type-specific identifier.</param>
        /// <returns>Data reader with client identifiers.</returns>
        IDataReader Client(ClientType type, int id);        

        /// <summary>
        /// Insert a new client of the given type.
        /// </summary>
        /// <param name="type">Client type.</param>
        /// <returns>Data reader with client values.</returns>
        IDataReader Client_Insert(ClientType type);        

        /// <summary>
        /// Fetch the identifier of the dealer that is mapped to the client with the given identifier.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader with a dealer identifier.</returns>
        IDataReader Dealer(int clientId);

        /// <summary>
        /// Map a client to a dealer via the given identifiers.        
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        void Dealer_Insert(int clientId, int dealerId);

        /// <summary>
        /// Fetch the identifier of the seller that is mapped to the client witht he given identifier.        
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader with a seller identifier.</returns>
        IDataReader Seller(int clientId);

        /// <summary>
        /// Map a client to a seller via the given identifiers.        
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="sellerId">Seller identifier.</param>
        void Seller_Insert(int clientId, int sellerId);

        #endregion

        #region Users

        /// <summary>
        /// Get the identifying details of the user with the given handle.
        /// </summary>
        /// <param name="handle">User handle.</param>
        /// <returns>Data reader with user identifiers.</returns>
        IDataReader User(Guid handle);

        /// <summary>
        /// For the given identifier that is specific to a type of user (e.g member identifier for dealers, system 
        /// identifier for system accounts, etc), get the user identifying details.   
        /// </summary>        
        /// <param name="userType">User type (e.g. Member, System)</param>
        /// <param name="id">User type-specific identifier.</param>
        /// <returns>Data reader with user values.</returns>
        IDataReader User(UserType userType, int id);

        /// <summary>
        /// Insert a new user of the given type.
        /// </summary>
        /// <param name="type">User type.</param>
        /// <returns>Data reader with user values.</returns>
        IDataReader User_Insert(UserType type);

        /// <summary>
        /// Fetch the identifier of the dealer member that is mapped to the user with the given identifier.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with a dealer member identifier.</returns>
        IDataReader Member(int userId);

        /// <summary>
        /// Insert a mapping between the user and member with the given identifiers.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="memberId">Member identifier.</param>
        void Member_Insert(int userId, int memberId);        

        /// <summary>
        /// Fetch the identifier of the system account that is mapped to the user with the given identifier.        
        /// </summary>
        /// <param name="userId">System user identifier.</param>
        /// <returns>Data reader with a system user identifier.</returns>
        IDataReader System(int userId);

        /// <summary>
        /// Fetch the identifier of the system account that has the given username.        
        /// </summary>
        /// <param name="userName">System username.</param>
        /// <returns>Data reader with a system user identifier.</returns>
        IDataReader System(string userName);

        #endregion
    }
}
