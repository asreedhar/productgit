﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Interface for a seller datastore.
    /// </summary>
    public interface ISellerDatastore
    {
        /// <summary>
        /// Fetch the details of the seller with the given seller identifier.
        /// </summary>
        /// <param name="id">Seller identifier.</param>
        /// <returns>Data reader with seller details.</returns>
        IDataReader Seller(int id);

        /// <summary>
        /// Fetch a paginated list of sellers that match the given example.
        /// </summary>
        /// <param name="userName">Username. Ignored.</param>
        /// <param name="example">Example seller to match by.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Data reader with a paginated list of sellers.</returns>
        IDataReader Sellers(string userName, IClient example, PageArguments pagination);
    }
}
