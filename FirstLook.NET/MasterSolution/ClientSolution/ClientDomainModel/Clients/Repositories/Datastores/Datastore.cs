using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Abstract base class for client datastores.
    /// </summary>
    public abstract class Datastore : SimpleDataStore
    {        
        /// <summary>
        /// Map paginated user details into database parameters.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="example">Example client details.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Array of database parameters.</returns>
        protected Parameter[] Map(string userName, IClient example, PageArguments pagination)
        {
            Address address = example.Address ?? new Address();

            object id = null;
            if (example.Id != 0)
            {
                id = example.Id;
            }

            return new[]
                       {
                           new Parameter("UserName", userName, DbType.String),
                           new Parameter("Name", example.Name, DbType.String),
                           new Parameter("Id", id, DbType.Int32),
                           new Parameter("OrganizationName", example.OrganizationName, DbType.String),
                           new Parameter("Line1", address.Line1, DbType.String),
                           new Parameter("Line2", address.Line2, DbType.String),
                           new Parameter("City", address.City, DbType.String),
                           new Parameter("State", address.State, DbType.String),
                           new Parameter("ZipCode", address.ZipCode, DbType.String),
                           new Parameter("SortColumns", pagination.SortExpression, DbType.String),
                           new Parameter("MaximumRows", pagination.MaximumRows, DbType.Int32),
                           new Parameter("StartRowIndex", pagination.StartRowIndex, DbType.Int32)
                       };
        }

        #region Miscellaneous

        /// <summary>
        /// Base location of sql scripts.
        /// </summary>
        internal const string Prefix = "FirstLook.Client.DomainModel.Clients.Repositories.Resources";

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion
    }
}