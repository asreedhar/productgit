﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Dealer datastore. For operations on retrieving dealers and members.
    /// </summary>
    public class DealerDatastore : Datastore, IDealerDatastore
    {
        #region Dealer

        /// <summary>
        /// Fetch the dealer with the given dealer identifier.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Data reader with dealer values.</returns>
        public IDataReader Dealer(int dealerId)
        {
            const string queryName = Prefix + ".DealerDatastore_Dealer_Fetch_Id.txt";

            return Query(new[] { "Dealer" }, queryName, new Parameter("DealerId", dealerId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the paginated list of dealers that match the provided example.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="example">Example dealer to match results by.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Data reader with a paginated list of matching dealers.</returns>
        public IDataReader Dealers(string userName, IClient example, PageArguments pagination)
        {
            const string queryName = Prefix + ".DealerDatastore_Dealer_Fetch_Example.txt";

            return Query(new[] { "Dealer", "TotalRowCount" }, queryName, Map(userName, example, pagination));
        }

        /// <summary>
        /// Fetch the dealer with the given identifier if it is accessible to the user with the given username.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Data reader with dealer values.</returns>
        public IDataReader Dealer(string userName, int dealerId)
        {
            const string queryName = Prefix + ".DealerDatastore_Dealer_Fetch_UserName_Id.txt";

            return Query(
                new[] { "Dealer" },
                queryName,
                new Parameter("UserName", userName, DbType.String),
                new Parameter("DealerID", dealerId, DbType.Int32));
        }

        #endregion

        #region Member

        /// <summary>
        /// Fetch the member with the given username.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <returns>Data reader with member values.</returns>
        public IDataReader Member(string userName)
        {
            const string queryName = Prefix + ".DealerDatastore_Member_Fetch_UserName.txt";

            return Query(new[] { "Member" }, queryName, new Parameter("UserName", userName, DbType.String));
        }

        /// <summary>
        /// Fetch the member with the given identifier.
        /// </summary>
        /// <param name="memberId">Member identifier.</param>
        /// <returns>Data reader with member values.</returns>
        public IDataReader Member(int memberId)
        {
            const string queryName = Prefix + ".DealerDatastore_Member_Fetch_Id.txt";

            return Query(new[] { "Member" }, queryName, new Parameter("MemberID", memberId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the members tied to the dealer with the given identifier.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Data reader with members values.</returns>
        public IDataReader Members(int dealerId)
        {
            const string queryName = Prefix + ".DealerDatastore_Member_Fetch_DealerId.txt";

            return Query(new[] { "Member" }, queryName, new Parameter("DealerId", dealerId, DbType.Int32));
        }

        #endregion

        /// <summary>
        /// The dealer database is IMT.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "IMT"; }
        }
    }
}
