﻿using System.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Interface for a reference data datastore. The reference data maintains identifiers to components that are 
    /// defined in other databases.
    /// </summary>
    public interface IReferenceDatastore
    {
        #region Dealer

        /// <summary>
        /// Fetch the dealer row from the reference data that has the given identifier.
        /// </summary>
        /// <param name="id">Dealer identifier.</param>
        /// <returns>Data reader with the dealer identifier.</returns>
        IDataReader Dealer_Fetch(int id);

        /// <summary>
        /// Insert the given dealer identifier into the reference data.
        /// </summary>
        /// <param name="id">Dealer identifier.</param>
        /// <returns>Data reader with the dealer identifier.</returns>
        IDataReader Dealer_Insert(int id);

        #endregion

        #region Seller

        /// <summary>
        /// Fetch the seller row from the reference data that has the given identifier.
        /// </summary>
        /// <param name="id">Seller identifier.</param>
        /// <returns>Data reader with the seller identifier.</returns>
        IDataReader Seller_Fetch(int id);

        /// <summary>
        /// Insert the given seller identifier into the reference data.
        /// </summary>
        /// <param name="id">Seller identifier.</param>
        /// <returns>Data reader with the seller identifier.</returns>
        IDataReader Seller_Insert(int id);

        #endregion

        #region Member

        /// <summary>
        /// Fetch the member row from the reference data that has the given identifier.
        /// </summary>
        /// <param name="id">Member identifier.</param>
        /// <returns>Data reader with the member identifier.</returns>
        IDataReader Member_Fetch(int id);

        /// <summary>
        /// Insert the given member identifier into the reference data.
        /// </summary>
        /// <param name="id">Member identifier.</param>
        /// <returns>Data reader with the member identifier.</returns>
        IDataReader Member_Insert(int id);

        #endregion        
    }
}
