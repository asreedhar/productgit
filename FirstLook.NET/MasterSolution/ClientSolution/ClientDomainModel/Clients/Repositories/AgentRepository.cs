using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories
{
    /// <summary>
    /// Repository for system agents.
    /// </summary>
    public class AgentRepository : RepositoryBase, IRepository
    {
        /// <summary>
        /// Fetch the system agent with 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Agent Agent(string userName)
        {
            return new UserGateway().Fetch_System(userName);
        }


        public IPrincipal Principal(int id)
        {
            return DoInTransaction(
                delegate
                {
                    UserGateway userGateway = new UserGateway();

                    User entity = userGateway.Fetch(UserType.System, id);

                    Agent system = userGateway.Fetch_System(id);

                    return new Principal
                    {
                        AuthorityName = Authority.AuthorityName,
                        Entity = entity,
                        User = system,
                        Handle = entity.Handle
                    };
                });
        }

        /// <summary>
        /// Vehicle is the system agent database.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing -- we do not audited creation of reference/client items
        }
    }
}
