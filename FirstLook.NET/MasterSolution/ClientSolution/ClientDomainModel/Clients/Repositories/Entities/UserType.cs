﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Entities
{
    [Serializable]
    public enum UserType : byte
    {
        Undefined = 0,
        Member = 1,
        System = 2
    }
}
