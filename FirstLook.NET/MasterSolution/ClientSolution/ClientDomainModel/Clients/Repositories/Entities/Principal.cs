using System;
using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Entities
{
    [Serializable]
    public class Principal : IPrincipal
    {
        private string _authorityName;
        private IUser _user;
        private Guid _handle;

        public string AuthorityName
        {
            get { return _authorityName; }
            internal set { _authorityName = value; }
        }

        public IUser User
        {
            get { return _user; }
            internal set { _user = value; }
        }

        public Guid Handle
        {
            get { return _handle; }
            internal set { _handle = value; }
        }

        private User _entity;

        public User Entity
        {
            get { return _entity; }
            internal set { _entity = value; }
        }
    }
}
