﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Entities
{
    /// <summary>
    /// A user entity - identifiers and the type of user.
    /// </summary>
    [Serializable]
    public class User
    {
        /// <summary>
        /// User identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// User handle.
        /// </summary>
        public Guid Handle { get; internal set; }

        /// <summary>
        /// Type of user.
        /// </summary>
        public UserType UserType { get; internal set; }
    }
}
