﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Entities
{
    [Serializable]
    public enum ClientType : byte
    {
        Undefined,
        Dealer,
        Seller
    }
}
