﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Entities
{
    [Serializable]
    public class Client
    {
        private int _id;
        private ClientType _clientType;
        private Guid _handle;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public ClientType ClientType
        {
            get { return _clientType; }
            internal set { _clientType = value; }
        }

        public Guid Handle
        {
            get { return _handle; }
            internal set { _handle = value; }
        }
    }
}
