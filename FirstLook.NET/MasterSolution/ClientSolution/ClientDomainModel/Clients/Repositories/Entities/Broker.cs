using System;
using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Entities
{
    [Serializable]
    public class Broker : IBroker
    {
        private string _authorityName;
        private IClient _client;
        private Integration _integration;

        public string AuthorityName
        {
            get { return _authorityName; }
            internal set { _authorityName = value; }
        }

        public IClient Client
        {
            get { return _client; }
            internal set { _client = value; }
        }

        public Integration Integration
        {
            get { return _integration; }
            internal set { _integration = value; }
        }

        private Client _entity;

        public Client Entity
        {
            get { return _entity; }
            internal set { _entity = value; }
        }

        #region IBroker Members

        string IBroker.AuthorityName
        {
            get { return AuthorityName; }
        }

        IClient IBroker.Client
        {
            get { return Client; }
        }

        Guid IBroker.Handle
        {
            get { return Entity.Handle; }
        }

        int IBroker.Id
        {
            get { return Entity.Id; }
        }

        Integration IBroker.Integration
        {
            get { return Integration; }
        }

        #endregion
    }
}
