﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;

namespace FirstLook.Client.DomainModel.Clients.Repositories
{
    /// <summary>
    /// Audit repository.
    /// </summary>
    public class AuditRepository : IRepository
    {
        /// <summary>
        /// Create an audit for the given principal user.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <returns>New audit.</returns>
        public Audit Create(IPrincipal principal)
        {
            Principal entity = (Principal) principal;

            return new AuditGateway().Insert(
                entity.Entity.Id,
                DateTime.Now);
        }

        /// <summary>
        /// Insert an audit row.
        /// </summary>
        /// <remarks>
        /// Audit information must be present in the session prior to calling this function.
        /// </remarks>
        /// <param name="wasInsert">Was the action performed an insert?</param>
        /// <param name="wasUpdate">Was the action performed an update?</param>
        /// <param name="wasDelete">Was the action performed a delete?</param>
        /// <returns>New audit row, or null if one couldn't be created.</returns>
        public AuditRow Insert(bool wasInsert, bool wasUpdate, bool wasDelete)
        {
            return new AuditRowGateway().Insert(wasInsert, wasUpdate, wasDelete);
        }

        /// <summary>
        /// Insert a new audit row that updates the given row.
        /// </summary>
        /// <remarks>
        /// Audit information must be present in the session prior to calling this function.
        /// </remarks>
        /// <param name="row">Audit row to update as being no longer active.</param>
        /// <param name="wasInsert">Was the action performed an insert?</param>
        /// <param name="wasUpdate">Was the action performed an update?</param>
        /// <param name="wasDelete">Was the action performed a delete?</param>
        /// <returns>New audit row, or null if one couldn't be created.</returns>
        public AuditRow Update(AuditRow row, bool wasInsert, bool wasUpdate, bool wasDelete)
        {
            return new AuditRowGateway().Insert(row, wasInsert, wasUpdate, wasDelete);
        }
    }
}
