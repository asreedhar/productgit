using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    /// <summary>
    /// Dealer implementation of a client data gateway.
    /// </summary>
    public class DealerGateway : GatewayBase
    {
        /// <summary>
        /// Query for dealers for the given user than match the user-provided example.
        /// </summary>
        /// <param name="userName">User to retrieve clients for.</param>
        /// <param name="example">Example client to match results against.</param>
        /// <param name="pagination">Paging arguments.</param>
        /// <returns>Data reader.</returns>
        public Page<Dealer> Fetch(string userName, IClient example, PageArguments pagination)
        {
            string key = CreateCacheKey(userName, ToString(example), pagination);

            Page<Dealer> value = Cache.Get(key) as Page<Dealer>;

            if (value == null)
            {
                ISerializer<Dealer> serializer = Resolve<ISerializer<Dealer>>();

                IDealerDatastore datastore = Resolve<IDealerDatastore>();

                using (IDataReader reader = datastore.Dealers(userName, example, pagination))
                {
                    IList<Dealer> items = serializer.Deserialize(reader);

                    int totalRowCount = 0;

                    if (reader.NextResult())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                        }
                    }

                    value = new Page<Dealer>
                    {
                        Arguments = pagination,
                        Items = items,
                        TotalRowCount = totalRowCount
                    };

                    Remember(key, value);
                }
            }

            return value;
        }

        /// <summary>
        /// Fetch the dealer with the given identifier if it is accessible to the user with the given username.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Dealer.</returns>
        public Dealer Fetch(string userName, int dealerId)
        {
            string key = CreateCacheKey(userName, dealerId);

            Dealer value = Cache.Get(key) as Dealer;

            if (value == null)
            {
                ISerializer<Dealer> serializer = Resolve<ISerializer<Dealer>>();

                IDealerDatastore datastore = Resolve<IDealerDatastore>();

                using (IDataReader reader = datastore.Dealer(userName, dealerId))
                {
                    if (reader.Read())
                    {
                        value = serializer.Deserialize((IDataRecord) reader);

                        Remember(key, value);
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Fetch the dealer with the given identifier.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Dealer.</returns>
        public Dealer Fetch(int dealerId)
        {
            string key = CreateCacheKey(dealerId);

            Dealer value = Cache.Get(key) as Dealer;

            if (value == null)
            {
                ISerializer<Dealer> serializer = Resolve<ISerializer<Dealer>>();

                IDealerDatastore datastore = Resolve<IDealerDatastore>();

                using (IDataReader reader = datastore.Dealer(dealerId))
                {
                    if (reader.Read())
                    {
                        value = serializer.Deserialize((IDataRecord) reader);

                        Remember(key, value);
                    }
                }
            }

            return value;
        }

        public class Row
        {
            public int Id;
        }

        public Row Insert(int id)
        {
            string key = CreateCacheKey(id);

            Row value = Cache.Get(key) as Row;

            if (value == null)
            {
                IReferenceDatastore store = Resolve<IReferenceDatastore>();

                using (IDataReader reader = store.Dealer_Fetch(id))
                {
                    if (reader.Read())
                        {
                            value = Fetch(reader);
                        }
                }

                if (value == null)
                {
                    using (IDataReader reader = store.Dealer_Insert(id))
                    {
                        if (reader.Read())
                        {
                            value = Fetch(reader);
                        }
                    }
                }
                else
                {
                    Remember(key, value);
                }
            }

            return value;
        }

        protected Row Fetch(IDataReader reader)
        {
            return new Row
            {
                Id = reader.GetInt32(reader.GetOrdinal("DealerID"))
            };
        }

        static void Append(StringBuilder sb, string key, string value)
        {
            sb.Append(key).Append(":").Append(value).Append(";");
        }

        /// <summary>
        /// Stringify a client.
        /// </summary>
        /// <param name="client">Client to string up.</param>
        /// <returns>A string.</returns>
        static string ToString(IClient client)
        {
            StringBuilder sb = new StringBuilder("[");
            Append(sb, "Id", client.Id.ToString());
            Append(sb, "Name", client.Name);
            Append(sb, "OrganizationName", client.OrganizationName);
            Append(sb, "Line1", client.Address.Line1);
            Append(sb, "Line2", client.Address.Line2);
            Append(sb, "State", client.Address.State);
            Append(sb, "ZipCode", client.Address.ZipCode);
            return sb.Append("]").ToString();
        }
    }
}
