using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    public class UserGateway : GatewayBase
    {
        public User Fetch(Guid handle)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            ISerializer<User> serializer = Resolve<ISerializer<User>>();

            using (IDataReader reader = datastore.User(handle))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord) reader);
                }

                return null;
            }
        }

        public User Fetch(UserType userType, int id)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            ISerializer<User> serializer = Resolve<ISerializer<User>>();

            using (IDataReader reader = datastore.User(userType, id))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }

        public User Insert(UserType userType, int id)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            ISerializer<User> serializer = Resolve<ISerializer<User>>();

            using (IDataReader reader = datastore.User_Insert(userType))
            {
                if (reader.Read())
                {
                    User user = serializer.Deserialize((IDataRecord)reader);

                    if (user == null)
                    {
                        throw new NotSupportedException();
                    }

                    datastore.Member_Insert(user.Id, id);

                    return user;
                }
            }

            throw new NotSupportedException();
        }

        public int Fetch_Member(int id)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            using (IDataReader reader = datastore.Member(id))
            {
                if (reader.Read())
                {
                    return reader.GetInt32(reader.GetOrdinal("MemberID"));
                }

                throw new ArgumentOutOfRangeException("id", id, "No such member");
            }
        }

        public Agent Fetch_System(int id)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            ISerializer<Agent> serializer = Resolve<ISerializer<Agent>>();

            using (IDataReader reader = datastore.System(id))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                throw new ArgumentOutOfRangeException("id", id, "No such system account");
            }
        }

        public Agent Fetch_System(string userName)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            ISerializer<Agent> serializer = Resolve<ISerializer<Agent>>();

            using (IDataReader reader = datastore.System(userName))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord) reader);
                }

                throw new ArgumentOutOfRangeException("userName", userName, "No such system account");
            }
        }
    }
}
