﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    /// <summary>
    /// Association data gateway.
    /// </summary>
    public class AssociationGateway : AuditingGateway
    {
        /// <summary>
        /// Insert a base record of an association.
        /// </summary>
        /// <returns>Association.</returns>
        public Association InsertAssociation()
        {
            IAuditDatastore datastore = Resolve<IAuditDatastore>();

            ISerializer<Association> serializer = Resolve<ISerializer<Association>>();

            using (IDataReader head = datastore.Association_Insert())
            {
                if (head.Read())
                {
                    int id = head.GetInt32(head.GetOrdinal("AssociationID"));

                    AuditRow row = Insert();
                    using (IDataReader body = datastore.Association_History_Insert(id, row.Id))
                    {
                        if (body.Read())
                        {
                            return serializer.Deserialize((IDataRecord) body);
                        }
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Insert a historical record of an association.
        /// </summary>
        /// <param name="associationId">Association identifier.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Association.</returns>
        public Association InsertAssociation(int associationId, int auditRowId)
        {
            IAuditDatastore datastore = Resolve<IAuditDatastore>();

            ISerializer<Association> serializer = Resolve<ISerializer<Association>>();

            using (IDataReader reader = datastore.Association_History_Insert(associationId, auditRowId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }             
            return null;
        }

        /// <summary>
        /// Fetch the currently valid historical identifier of the association with the given identifier.
        /// </summary>
        /// <param name="associationId">Association identifier.</param>
        /// <returns>Association.</returns>
        public Association FetchAssociation(int associationId)
        {
            IAuditDatastore clientDatastore = Resolve<IAuditDatastore>();

            ISerializer<Association> serializer = Resolve<ISerializer<Association>>();

            using (IDataReader reader = clientDatastore.Association_History_Fetch(associationId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }
}
