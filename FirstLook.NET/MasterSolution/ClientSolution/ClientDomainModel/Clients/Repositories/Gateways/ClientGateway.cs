using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    /// <summary>
    /// Client data gateway.
    /// </summary>
    public class ClientGateway : GatewayBase
    {
        /// <summary>
        /// Fetch the client that is of the given type and has the given identifier. Values will be cached.
        /// </summary>
        /// <param name="clientType">Client type.</param>
        /// <param name="id">Client identifier.</param>
        /// <returns>Client.</returns>
        public Entities.Client Fetch(ClientType clientType, int id)
        {
            string key = CreateCacheKey(clientType, id);

            Entities.Client value = Cache.Get(key) as Entities.Client;

            if (value == null)
            {
                IClientDatastore store = Resolve<IClientDatastore>();

                ISerializer<Entities.Client> serializer = Resolve<ISerializer<Entities.Client>>();

                using (IDataReader reader = store.Client(clientType, id))
                {
                    if (reader.Read())
                    {
                        value = serializer.Deserialize((IDataRecord)reader);
                    }
                }

                Remember(key, value);
            }

            return value;
        }

        /// <summary>
        /// Fetch the client with the given guid.
        /// </summary>
        /// <param name="handle">Client handle.</param>
        /// <returns>Client.</returns>
        public Entities.Client Fetch(Guid handle)
        {
            string key = CreateCacheKey(handle);

            Entities.Client value = Cache.Get(key) as Entities.Client;

            if (value == null)
            {
                IClientDatastore store = Resolve<IClientDatastore>();

                ISerializer<Entities.Client> serializer = Resolve<ISerializer<Entities.Client>>();

                using (IDataReader reader = store.Client(handle))
                {
                    if (reader.Read())
                    {
                        value = serializer.Deserialize((IDataRecord)reader);
                    }
                }

                Remember(key, value);
            }

            return value;
        }

        /// <summary>
        /// Insert a client of the given type, and map it to the given identifier. The context of the given identifier
        /// varies by the given type.
        /// </summary>
        /// <param name="type">Client type.</param>
        /// <param name="id">Dealer identifier if type is Dealer, otherwise Seller identifier.</param>
        /// <returns>Client.</returns>
        public Entities.Client Insert(ClientType type, int id)
        {
            IClientDatastore store = Resolve<IClientDatastore>();

            ISerializer<Entities.Client> serializer = Resolve<ISerializer<Entities.Client>>();

            Entities.Client client = null;

            using (IDataReader reader = store.Client_Insert(type))
            {
                if (reader.Read())
                {
                    client = serializer.Deserialize((IDataRecord) reader);

                    if (client != null)
                    {
                        switch (type)
                        {
                            case ClientType.Dealer:
                                store.Dealer_Insert(client.Id, id);
                                break;

                            case ClientType.Seller:
                                store.Seller_Insert(client.Id, id);
                                break;
                        }
                    }
                }

                return client;
            }
        }

        /// <summary>
        /// Fetch the dealer with the given client identifier.
        /// </summary>
        /// <param name="id">Client identifier.</param>
        /// <returns>Dealer identifier.</returns>
        public int Fetch_Dealer(int id)
        {
            IClientDatastore store = Resolve<IClientDatastore>();

            using (IDataReader reader = store.Dealer(id))
            {
                if (reader.Read())
                {
                    return reader.GetInt32(reader.GetOrdinal("DealerID"));
                }

                throw new ArgumentOutOfRangeException("id", id, "No such dealer");
            }
        }

        /// <summary>
        /// Fetch the seller with the given client identifier.
        /// </summary>
        /// <param name="id">Client identifier.</param>
        /// <returns>Seller identifier.</returns>
        public int Fetch_Seller(int id)
        {
            IClientDatastore store = Resolve<IClientDatastore>();

            using (IDataReader reader = store.Seller(id))
            {
                if (reader.Read())
                {
                    return reader.GetInt32(reader.GetOrdinal("SellerID"));
                }

                throw new ArgumentOutOfRangeException("id", id, "No such seller");
            }
        }
    }
}
