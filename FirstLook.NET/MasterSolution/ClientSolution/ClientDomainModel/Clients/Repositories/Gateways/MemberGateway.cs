using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    public class MemberGateway : GatewayBase
    {
        public Member Fetch(string userName)
        {
            string key = CreateCacheKey(userName);

            Member value = Cache.Get(key) as Member;

            if (value == null)
            {
                IDealerDatastore store = Resolve<IDealerDatastore>();

                ISerializer<Member> serializer = Resolve<ISerializer<Member>>();

                using (IDataReader reader = store.Member(userName))
                {
                    if (reader.Read())
                    {
                        value = serializer.Deserialize((IDataRecord)reader);
                    }
                }
                
                Remember(key, value);
            }

            return value;
        }

        public Member Fetch(int memberId)
        {
            IDealerDatastore store = Resolve<IDealerDatastore>();

            ISerializer<Member> serializer = Resolve<ISerializer<Member>>();

            using (IDataReader reader = store.Member(memberId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord) reader);
                }
            }

            return null;
        }

        public IList<Member> FetchForDealer(int dealerId)
        {
            string key = CreateCacheKey(dealerId);

            IList<Member> values = Cache.Get(key) as IList<Member>;

            if (values == null)
            {
                IDealerDatastore store = Resolve<IDealerDatastore>();

                ISerializer<Member> serializer = Resolve<ISerializer<Member>>();

                using (IDataReader reader = store.Members(dealerId))
                {
                    if (reader.Read())
                    {
                        values = serializer.Deserialize(reader);
                    }
                }

                Remember(key, values);
            }

            return values;
        }

        public class Row
        {
            public int Id;
        }

        public Row Insert(int id)
        {
            string key = CreateCacheKey(id);

            Row value = Cache.Get(key) as Row;

            if (value == null)
            {
                IReferenceDatastore store = Resolve<IReferenceDatastore>();

                using (IDataReader reader = store.Member_Fetch(id))
                {
                    if (reader.Read())
                    {
                        value = Fetch(reader);
                    }
                }

                if (value == null)
                {
                    using (IDataReader reader = store.Member_Insert(id))
                    {
                        if (reader.Read())
                        {
                            value = Fetch(reader);
                        }
                    }
                }
                else
                {
                    Remember(key, value);
                }
            }

            return value;
        }

        private static Row Fetch(IDataRecord reader)
        {
            return new Row
            {
                Id = reader.GetInt32(reader.GetOrdinal("MemberID"))
            };
        }
    }
}
