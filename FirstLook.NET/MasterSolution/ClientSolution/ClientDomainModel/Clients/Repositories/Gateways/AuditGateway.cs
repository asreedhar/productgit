using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    /// <summary>
    /// Audit data gateway.
    /// </summary>
    public class AuditGateway : GatewayBase
    {
        /// <summary>
        /// Fetch an audit that owns the audit row with the given identifier.
        /// </summary>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Audit.</returns>
        public Audit Fetch(int auditRowId)
        {
            IAuditDatastore store = Resolve<IAuditDatastore>();

            ISerializer<Audit> serializer = Resolve<ISerializer<Audit>>();

            using (IDataReader reader = store.Audit_Fetch(auditRowId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }

        /// <summary>
        /// Insert a new audit for the given user at the given date. Returns the newly created audit.
        /// </summary>
        /// <param name="user">User performing an action.</param>
        /// <param name="date">Date of the action.</param>
        /// <returns>Newly created audit.</returns>
        public Audit Insert(int user, DateTime date)
        {
            IAuditDatastore store = Resolve<IAuditDatastore>();

            ISerializer<Audit> serializer = Resolve<ISerializer<Audit>>();

            using (IDataReader reader = store.Audit_Insert(user))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord) reader);
                }

                return null;
            }
        }
    }
}