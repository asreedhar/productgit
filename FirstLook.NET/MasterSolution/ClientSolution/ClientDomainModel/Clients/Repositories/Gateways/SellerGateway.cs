using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    /// <summary>
    /// Seller implementation of a client data gateway.
    /// </summary>
    public class SellerGateway : GatewayBase
    {
        /// <summary>
        /// Query for sellers for the given user than match the user-provided example.
        /// </summary>
        /// <param name="userName">User to retrieve clients for.</param>
        /// <param name="example">Example client to match results against.</param>
        /// <param name="pagination">Paging arguments.</param>
        /// <returns>Data reader.</returns>
        public Page<Seller> Fetch(string userName, IClient example, PageArguments pagination)
        {
            ISerializer<Seller> serializer = Resolve<ISerializer<Seller>>();

            ISellerDatastore datastore = Resolve<ISellerDatastore>();

            IDataReader reader = datastore.Sellers(userName, example, pagination);

            IList<Seller> items = serializer.Deserialize(reader);

            int totalRowCount = 0;

            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                }
            }

            return new Page<Seller>
            {
                Arguments = pagination,
                Items = items,
                TotalRowCount = totalRowCount
            };
        }

        public Seller Fetch(int id)
        {
            ISerializer<Seller> serializer = Resolve<ISerializer<Seller>>();

            ISellerDatastore datastore = Resolve<ISellerDatastore>();

            using (IDataReader reader = datastore.Seller(id))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }
}
