using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Gateways
{
    /// <summary>
    /// Audit row data gateway.
    /// </summary>
    public class AuditRowGateway : GatewayBase
    {
        /// <summary>
        /// Insert a new audit row.
        /// </summary>
        /// <remarks>
        /// Audit information must be present in the session prior to calling this function.
        /// </remarks>
        /// <param name="wasInsert">Was the action performed an insert?</param>
        /// <param name="wasUpdate">Was the action performed an update?</param>
        /// <param name="wasDelete">Was the action performed a delete?</param>
        /// <returns>New audit row, or null if one couldn't be created.</returns>
        public AuditRow Insert(bool wasInsert, bool wasUpdate, bool wasDelete)
        {
            IAuditDatastore datastore = Resolve<IAuditDatastore>();

            ISerializer<AuditRow> serializer = Resolve<ISerializer<AuditRow>>();

            Audit auditEntity = (Audit) Session.Items["Audit"];

            using (IDataReader reader = datastore.AuditRow_Insert(auditEntity.Id, wasInsert, wasUpdate, wasDelete))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }

        /// <summary>
        /// Insert a new audit row as an update to an existing audit row.
        /// </summary>
        /// <remarks>
        /// Audit information must be present in the session prior to calling this function.
        /// </remarks>
        /// <param name="previous">Audit row to update as being no longer active.</param>
        /// <param name="wasInsert">Was the action performed an insert?</param>
        /// <param name="wasUpdate">Was the action performed an update?</param>
        /// <param name="wasDelete">Was the action performed a delete?</param>
        /// <returns>New audit row, or null if one couldn't be created.</returns>
        public AuditRow Insert(AuditRow previous, bool wasInsert, bool wasUpdate, bool wasDelete)
        {
            Resolve<IAuditDatastore>().AuditRow_Update(previous.Id);

            return Insert(wasInsert, wasUpdate, wasDelete);
        }

        /// <summary>
        /// Load an AuditRow by its primary key.
        /// </summary>
        /// <remarks>
        /// This method is quite possibly deprecated and only used by the <code>VehicleInformation</code>
        /// solution (which, stylistically, needs to be brought up to date).
        /// </remarks>
        /// <param name="auditRowId"></param>
        /// <returns></returns>
        public AuditRow Fetch(int auditRowId)
        {
            return new AuditRow{Id = auditRowId};
        }
    }
}