﻿
SELECT  Id = S.SellerID,
        Name = S.Name,
        OrganizationName = null,
        AddressLine1 = S.Address1,
        AddressLine2 = S.Address2,
        S.City,
        S.State,
        S.ZipCode
FROM    Listing.Seller S
WHERE   S.SellerID = @SellerID
