﻿
SELECT  U.UserID ,
        U.UserTypeID ,
        U.Handle
FROM    Client.[User] U
WHERE   Handle = @Handle
