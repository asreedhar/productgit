﻿
; WITH Dealers (DealerID) AS (
	SELECT	A.BusinessUnitID
	FROM	dbo.MemberAccess A
	JOIN    dbo.Member M ON M.MemberID = A.MemberID
	WHERE	M.Login = @UserName
	AND     M.MemberType in(2,4) -- user
UNION ALL
	SELECT	B.BusinessUnitID
	FROM	dbo.BusinessUnit B CROSS JOIN dbo.Member M
	WHERE	M.Login = @UserName
	AND     M.MemberType = 1 -- admin
	AND     B.BusinessUnitTypeID = 4
	AND	B.Active = 1
)
SELECT  Id = B.BusinessUnitID,
        Name = B.BusinessUnit,
        OrganizationName = P.BusinessUnit,
        AddressLine1 = B.Address1,
        AddressLine2 = B.Address2,
        B.City,
        B.State,
        B.ZipCode
FROM    Dealers D
JOIN    dbo.BusinessUnit B ON B.BusinessUnitID = D.DealerID
JOIN    dbo.BusinessUnitRelationship R ON R.BusinessUnitID = B.BusinessUnitID
JOIN    dbo.BusinessUnit P ON P.BusinessUnitID = R.ParentID
WHERE   DealerID = @DealerID