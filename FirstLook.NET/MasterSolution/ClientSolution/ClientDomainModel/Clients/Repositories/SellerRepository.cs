using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories
{
    public class SellerRepository : RepositoryBase, IRepository
    {
        public Seller Seller(int sellerId)
        {
            return new SellerGateway().Fetch(sellerId);
        }

        public Page<Seller> Sellers(string userName, IClient example, PageArguments pagination)
        {
            return new SellerGateway().Fetch(userName, example, pagination);
        }

        public Seller Seller(string userName, int id)
        {
            return new SellerGateway().Fetch(id);
        }

        public IBroker Broker(int sellerId)
        {
            return DoInTransaction(delegate
            {
                ClientGateway gateway = new ClientGateway();

                Entities.Client entity = gateway.Fetch(ClientType.Seller, sellerId);

                if (entity == null)
                {
                    entity = gateway.Insert(ClientType.Seller, sellerId);
                }

                return new Broker
                {
                    AuthorityName = "CAS",
                    Entity = entity,
                    Client = Seller(sellerId),
                    Integration = new Integration
                    {
                        IsDealer = false,
                        IsSeller = true
                    }
                };
            });
        }

        public IBroker Broker(Guid id)
        {
            return DoInSession(
                delegate
                {
                    ClientGateway gateway = new ClientGateway();

                    Entities.Client entity = gateway.Fetch(id);

                    if (entity == null || entity.ClientType != ClientType.Seller)
                    {
                        return null;
                    }

                    return new Broker
                    {
                        AuthorityName = "CAS",
                        Client = Seller(gateway.Fetch_Seller(entity.Id)),
                        Entity = entity,
                        Integration = new Integration
                        {
                            IsDealer = false,
                            IsSeller = true
                        }
                    };
                });
        }

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // not audited
        }
    }
}
