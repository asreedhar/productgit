using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients.Repositories
{
    /// <summary>
    /// Register the components required for client repositories.
    /// </summary>
    [Serializable]
    public class Module : IModule
    {
        /// <summary>
        /// Register the implementation of the model repositories.
        /// </summary>
        /// <param name="registry">Component registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<Model.Auditing.IRepository, AuditRepository>(ImplementationScope.Shared);

            registry.Register<Model.Authorities.Dealers.IRepository, DealerRepository>(ImplementationScope.Shared);

            registry.Register<Model.Authorities.Sellers.IRepository, SellerRepository>(ImplementationScope.Shared);

            registry.Register<Model.Authorities.Agents.IRepository, AgentRepository>(ImplementationScope.Shared);
        }
    }
}
