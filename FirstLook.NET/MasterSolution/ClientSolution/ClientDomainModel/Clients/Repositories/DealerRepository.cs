using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using IRepository=FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers.IRepository;

namespace FirstLook.Client.DomainModel.Clients.Repositories
{
    /// <summary>
    /// Dealer data repository.
    /// </summary>
    public class DealerRepository : RepositoryBase, IRepository
    {
        /// <summary>
        /// Get the member with the given username.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <returns>Member.</returns>
        public Member Member(string userName)
        {
            return new MemberGateway().Fetch(userName);
        }

        /// <summary>
        /// Get the members tied to the dealer with the given identifier.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>List of members.</returns>
        public IList<Member> Members(int dealerId)
        {
            return new MemberGateway().FetchForDealer(dealerId);
        }

        /// <summary>
        /// Get the dealer with the given identifier.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Dealer.</returns>
        public Dealer Dealer(int dealerId)
        {
            return new DealerGateway().Fetch(dealerId);
        }

        /// <summary>
        /// Get the paged list of dealers tied to the given user that match the provided example.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="example">Example dealer to match results by.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Page of dealers.</returns>
        public Page<Dealer> Dealers(string userName, IClient example, PageArguments pagination)
        {
            return new DealerGateway().Fetch(userName, example, pagination);
        }

        /// <summary>
        /// Get the dealer with the given identifier that is tied to the given user.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <param name="id">Dealer identifier.</param>
        /// <returns>Dealer.</returns>
        public Dealer Dealer(string userName, int id)
        {
            return new DealerGateway().Fetch(userName, id);
        }

        /// <summary>
        /// Get the broker for the dealer with the given identifier. If a broker does not exist for this dealer, one
        /// will be created.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Broker.</returns>
        public IBroker Broker(int dealerId)
        {
            return DoInTransaction(
                delegate
                {
                    ClientGateway gateway = new ClientGateway();

                    Entities.Client entity = gateway.Fetch(ClientType.Dealer, dealerId);

                    if (entity == null)
                    {
                        new DealerGateway().Insert(dealerId); // ensure reference value exists

                        entity = gateway.Insert(ClientType.Dealer, dealerId);
                    }

                    Broker broker = new Broker
                    {
                        AuthorityName = Model.Authorities.Dealers.Authority.AuthorityName,
                        Entity = entity,
                        Client = Dealer(dealerId),
                        Integration = new Integration
                        {
                            IsDealer = true,
                            IsSeller = false
                        }
                    };

                    return broker;
                });
        }

        public IBroker Broker(Guid id)
        {
            return DoInSession(
                delegate
                {
                    ClientGateway gateway = new ClientGateway();

                    Entities.Client entity = gateway.Fetch(id);

                    if (entity == null || entity.ClientType != ClientType.Dealer)
                    {
                        return null;
                    }

                    return new Broker
                    {
                        AuthorityName = "CAS",
                        Client = Dealer(gateway.Fetch_Dealer(entity.Id)),
                        Entity = entity,
                        Integration = new Integration
                        {
                            IsDealer = true,
                            IsSeller = false
                        }
                    };
                });
        }

        public IPrincipal Principal(int memberId)
        {
            return DoInTransaction(
                delegate
                {
                    MemberGateway memberGateway = new MemberGateway();

                    UserGateway userGateway = new UserGateway();

                    User entity = userGateway.Fetch(UserType.Member, memberId);

                    if (entity == null)
                    {
                        memberGateway.Insert(memberId); // ensure reference value exists

                        entity = userGateway.Insert(UserType.Member, memberId);
                    }

                    return new Principal
                    {
                        AuthorityName = Model.Authorities.Dealers.Authority.AuthorityName,
                        Entity = entity,
                        User = memberGateway.Fetch(memberId),
                        Handle = entity.Handle
                    };
                });
        }

        public IPrincipal Principal(Guid id)
        {
            return DoInSession(
                delegate
                {
                    UserGateway gateway = new UserGateway();

                    User entity = gateway.Fetch(id);

                    if (entity.UserType != UserType.Member)
                    {
                        throw new ArgumentException("Not a member", "id");
                    }

                    return new Principal
                    {
                        AuthorityName = "CAS",
                        Entity = entity,
                        User = new MemberGateway().Fetch(gateway.Fetch_Member(entity.Id)),
                        Handle = entity.Handle
                    };
                });
        }

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing -- we do not audited creation of reference/client items
        }
    }
}
