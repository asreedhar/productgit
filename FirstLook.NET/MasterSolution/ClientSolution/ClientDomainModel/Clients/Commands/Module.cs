﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.Impl;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients.Commands
{
    /// <summary>
    /// Register the components required for client commands.
    /// </summary>
    [Serializable]
    public class Module : IModule
    {
        /// <summary>
        /// Register the client command factory.
        /// </summary>
        /// <param name="registry">Component registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
