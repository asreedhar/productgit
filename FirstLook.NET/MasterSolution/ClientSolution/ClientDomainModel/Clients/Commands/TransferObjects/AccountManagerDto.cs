﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects
{
    [Serializable]
    public class AccountManagerDto
    {
        public string AuthorityName { get; set; }

        public string Name { get; set; }
    }
}
