﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects
{
    [Serializable]
    public class BrokerDto
    {
        public string AuthorityName { get; set; }

        public ClientDto Client { get; set; }

        public Guid Handle { get; set; }
    }
}
