﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects
{
    /// <summary>
    /// A street address.
    /// </summary>
    [Serializable]
    public class AddressDto
    {
        /// <summary>
        /// First line of street address.
        /// </summary>
        public string Line1 { get; set; }

        /// <summary>
        /// Second line of street address.
        /// </summary>
        public string Line2 { get; set; }

        /// <summary>
        /// City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// State.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Zip code.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Create a blank address.
        /// </summary>
        public AddressDto()
        {
            Line1 = string.Empty;
            Line2 = string.Empty;
            City = string.Empty;
            State = string.Empty;
            ZipCode = string.Empty;
        }
    }
}