﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects
{
    /// <summary>
    /// A client that is either a dealer or a seller.
    /// </summary>
    [Serializable]
    public class ClientDto
    {
        /// <summary>
        /// Client's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Client's organization (optional).
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// Client's identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Client's street address.
        /// </summary>
        public AddressDto Address { get; set; }
    }
}