﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects
{
    [Serializable]
    public class PrincipalDto
    {
        public string AuthorityName { get; set; }

        public UserDto User { get; set; }

        public Guid Handle { get; set; }
    }
}
