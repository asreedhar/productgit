﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects
{
    [Serializable]
    public class UserDto
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
