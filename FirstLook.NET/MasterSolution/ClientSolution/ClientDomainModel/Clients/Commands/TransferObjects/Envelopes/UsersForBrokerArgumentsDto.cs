﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for retrieving users tied to a broker.
    /// </summary>
    [Serializable]
    public class UsersForBrokerArgumentsDto
    {
        /// <summary>
        /// Broker handle.
        /// </summary>
        public Guid Broker { get; set; }
    }
}
