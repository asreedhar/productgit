﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of getting users tied to a broker.
    /// </summary>
    [Serializable]
    public class UsersForBrokerResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public UsersForBrokerArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Broker's users.
        /// </summary>
        public List<UserDto> Users { get; set; }
    }
}
