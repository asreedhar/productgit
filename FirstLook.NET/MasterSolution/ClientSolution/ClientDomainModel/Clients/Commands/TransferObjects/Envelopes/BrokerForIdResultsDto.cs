using System;
using FirstLook.Client.DomainModel.Common.Commands;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of fetching a broker for a handle.
    /// </summary>
    [Serializable]
    public class BrokerForIdResultsDto : ICommandResults<BrokerForIdArgumentsDto>
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        public BrokerForIdArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Broker that was found.
        /// </summary>
        public BrokerDto Broker { get; set; }
    }
}
