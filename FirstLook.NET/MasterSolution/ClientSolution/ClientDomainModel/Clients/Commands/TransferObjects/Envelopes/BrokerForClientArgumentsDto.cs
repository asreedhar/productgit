using System;
using FirstLook.Client.DomainModel.Common.Commands;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used to fetch or create a broker.
    /// </summary>
    [Serializable]
    public class BrokerForClientArgumentsDto : ICommandArguments
    {
        public string AuthorityName { get; set; }

        /// <summary>
        /// The client for which a broker should be found (and created, if necessary).
        /// </summary>
        public ClientDto Client { get; set; }
    }
}
