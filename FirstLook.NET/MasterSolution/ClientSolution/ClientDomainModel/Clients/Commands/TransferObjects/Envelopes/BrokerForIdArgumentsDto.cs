using System;
using FirstLook.Client.DomainModel.Common.Commands;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for fetching a broker by its handle.
    /// </summary>
    [Serializable]
    public class BrokerForIdArgumentsDto : ICommandArguments
    {
        /// <summary>
        /// Handle of the broker to fetch.
        /// </summary>
        public Guid Id { get; set; }
    }
}
