using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Paginated results of querying for a client by example.
    /// </summary>
    [Serializable]
    public class QueryClientByExampleResultsDto : PaginatedResultsDto, ICommandResults<QueryClientByExampleArgumentsDto>
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        public QueryClientByExampleArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Clients found based on the query arguments.
        /// </summary>
        public List<ClientDto> Clients { get; set; }

        /// <summary>
        /// Create an empty set of results.
        /// </summary>
        public QueryClientByExampleResultsDto()
        {
            Clients = new List<ClientDto>();
        }
    }
}