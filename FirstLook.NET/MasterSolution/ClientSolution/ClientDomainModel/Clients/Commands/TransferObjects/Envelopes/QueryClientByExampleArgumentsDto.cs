using System;
using FirstLook.Client.DomainModel.Common.Commands;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for fetching a client by example, including paging options.
    /// </summary>
    [Serializable]
    public class QueryClientByExampleArgumentsDto : PaginatedArgumentsDto, ICommandArguments
    {
        public string AuthorityName { get; set; }

        /// <summary>
        /// Example information about the client that is being queried.
        /// </summary>
        public ClientDto Example { get; set; }
    }
}