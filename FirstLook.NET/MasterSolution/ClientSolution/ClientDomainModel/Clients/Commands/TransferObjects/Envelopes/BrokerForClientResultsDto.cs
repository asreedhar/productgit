using System;
using FirstLook.Client.DomainModel.Common.Commands;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of fetching or creating a broker for a client.
    /// </summary>
    [Serializable]
    public class BrokerForClientResultsDto : ICommandResults<BrokerForClientArgumentsDto>
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        public BrokerForClientArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Broker that was found (and possibly created).
        /// </summary>
        public BrokerDto Broker { get; set; }
    }
}
