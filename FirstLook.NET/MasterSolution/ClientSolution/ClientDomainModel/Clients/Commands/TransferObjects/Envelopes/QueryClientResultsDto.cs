﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class QueryClientResultsDto : PaginatedResultsDto
    {
        private QueryClientArgumentsDto _arguments;
        private PrincipalDto _principal;
        private List<ClientDto> _clients;
        private List<AccountManagerDto> _accountManagers;

        public PrincipalDto Principal
        {
            get { return _principal; }
            set { _principal = value; }
        }

        public QueryClientArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<ClientDto> Clients
        {
            get { return _clients; }
            set { _clients = value; }
        }

        public List<AccountManagerDto> AccountManagers
        {
            get { return _accountManagers; }
            set { _accountManagers = value; }
        }
    }
}
