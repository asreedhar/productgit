﻿using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Clients.Commands
{
    /// <summary>
    /// Factory interface for client-related commands.
    /// </summary>
    public interface ICommandFactory : IPolicyProvider
    {
        /// <summary>
        /// Create a command to query for a clients that match the user-provided example.
        /// </summary>
        /// <returns>Command to query a client.</returns>
        ICommand<QueryClientByExampleResultsDto, IdentityContextDto<QueryClientByExampleArgumentsDto>>
            CreateQueryClientByExampleCommand();

        /// <summary>
        /// Create a command to query for all clients.
        /// </summary>
        /// <returns>Command to query for clients.</returns>
        ICommand<QueryClientResultsDto, IdentityContextDto<QueryClientArgumentsDto>>
            CreateQueryClientCommand();

        /// <summary>
        /// Create a command to fetch a broker by its handle.
        /// </summary>
        /// <returns>Command to fetch a broker.</returns>
        ICommand<BrokerForIdResultsDto, IdentityContextDto<BrokerForIdArgumentsDto>>
            CreateBrokerForHandleCommand();

        /// <summary>
        /// Create a command to fetch a broker by a client and create it if necessary.
        /// </summary>
        /// <returns>Command to fetch or create a broker.</returns>
        ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>>
            CreateBrokerForClientCommand();

        /// <summary>
        /// Create a command to fetch a client's users.
        /// </summary>
        /// <returns>Command to fetch a client's users.</returns>
        ICommand<UsersForBrokerResultsDto, IdentityContextDto<UsersForBrokerArgumentsDto>> 
            CreateUsersForBrokerCommand();
    }
}
