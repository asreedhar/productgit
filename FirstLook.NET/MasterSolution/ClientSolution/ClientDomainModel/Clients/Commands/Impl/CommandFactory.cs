using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Clients.Commands.Impl
{
    /// <summary>
    /// Factory for creating client-related commands.
    /// </summary>
    public class CommandFactory : ICommandFactory
    {
        /// <summary>
        /// The policy that defines access to a command.
        /// </summary>
        public IPolicy Policy
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Create a command to query for a clients that match the user-provided example.
        /// </summary>
        /// <returns>Command to query a client.</returns>
        public ICommand<QueryClientByExampleResultsDto, IdentityContextDto<QueryClientByExampleArgumentsDto>> CreateQueryClientByExampleCommand()
        {
            // If permissible...
            return new QueryClientByExampleCommand();
            // else throw new NotPermissibleException();
        }

        public ICommand<QueryClientResultsDto, IdentityContextDto<QueryClientArgumentsDto>> CreateQueryClientCommand()
        {
            // If permissible...
            return new QueryClientCommand();
            // else throw new NotPermissibleException();
        }

        /// <summary>
        /// Create a command to fetch a broker by its handle.
        /// </summary>
        /// <returns>Command to fetch a broker.</returns>
        public ICommand<BrokerForIdResultsDto, IdentityContextDto<BrokerForIdArgumentsDto>> CreateBrokerForHandleCommand()
        {
            // If permissible...
            return new BrokerForIdCommand();
            // else throw new NotPermissibileException();
        }

        /// <summary>
        /// Create a command to fetch a broker by a client and create it if necessary.
        /// </summary>
        /// <returns>Command to fetch or create a broker.</returns>
        public ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>> CreateBrokerForClientCommand()
        {
            // If permissible...
            return new BrokerForClientCommand();
            // else throw new NotPermissibileException();
        }

        /// <summary>
        /// Create a command to fetch a client's users.
        /// </summary>
        /// <returns>Command to fetch a client's users.</returns>
        public ICommand<UsersForBrokerResultsDto, IdentityContextDto<UsersForBrokerArgumentsDto>> CreateUsersForBrokerCommand()
        {
            return new UsersForBrokerCommand();
        }
    }
}
