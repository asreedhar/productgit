using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Clients.Commands.Impl
{
    /// <summary>
    /// Fetch a broker by its handle.
    /// </summary>
    public class BrokerForIdCommand : ICommand<BrokerForIdResultsDto, IdentityContextDto<BrokerForIdArgumentsDto>>
    {
        /// <summary>
        /// Fetch the broker with the given handle.
        /// </summary>
        /// <param name="parameters">Broker handle.</param>
        /// <returns>Broker.</returns>
        public BrokerForIdResultsDto Execute(IdentityContextDto<BrokerForIdArgumentsDto> parameters)
        {
            BrokerForIdArgumentsDto arguments = parameters.Arguments;

            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, arguments.Id);

            BrokerDto dto = broker == null ? null : new BrokerDto
            {
                AuthorityName = broker.AuthorityName,
                Client = Mapper.Map(broker.Client),
                Handle = broker.Handle
            };

            return new BrokerForIdResultsDto
            {
                Arguments = arguments,
                Broker = dto
            };
        }
    }
}
