using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Clients.Commands.Impl
{
    /// <summary>
    /// Fetch a broker for a client, creating it if it does not already exist.
    /// </summary>
    public class BrokerForClientCommand : ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>>
    {
        /// <summary>
        /// Fetch the broker tied to the given client. If it does not yet exist, create it.        
        /// </summary>
        /// <param name="parameters">Client.</param>
        /// <returns>Broker.</returns>
        public BrokerForClientResultsDto Execute(IdentityContextDto<BrokerForClientArgumentsDto> parameters)
        {
            BrokerForClientArgumentsDto arguments = parameters.Arguments;

            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, arguments.AuthorityName, arguments.Client.Id);

            if (broker == null) return null;
            
            return new BrokerForClientResultsDto
            {
                Arguments = arguments,
                Broker = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client = Mapper.Map(broker.Client),
                    Handle = broker.Handle
                }
            };
        }
    }
}
