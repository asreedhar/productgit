using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Clients.Commands.Impl
{
    /// <summary>
    /// Command to query for clients that match the user-provided example data.
    /// </summary>
    public class QueryClientByExampleCommand : ICommand<QueryClientByExampleResultsDto, IdentityContextDto<QueryClientByExampleArgumentsDto>>
    {
        internal static readonly IList<string> ColumnNames = new List<string>
                                                                {
                                                                    "Id",
                                                                    "Name",
                                                                    "OrganizationName",
                                                                    "AddressLine1",
                                                                    "AddressLine2",
                                                                    "City",
                                                                    "State",
                                                                    "ZipCode"
                                                                }.AsReadOnly();

        /// <summary>
        /// Execute the command to fetch clients that match the user-provided example data.
        /// </summary>
        /// <param name="parameters">Example info on the client being searched for.</param>
        /// <returns>List of clients that match the provided data.</returns>
        public QueryClientByExampleResultsDto Execute(IdentityContextDto<QueryClientByExampleArgumentsDto> parameters)
        {
            QueryClientByExampleArgumentsDto arguments = parameters.Arguments;

            IdentityDto identity = parameters.Identity;

            string authorityName = identity.AuthorityName;

            IAuthority authority = AuthorityFactory.GetAuthority(authorityName);

            IAccountManager accountManager = authority.AccountManager;

            if (!Equals(authority.Name, arguments.AuthorityName))
            {
                accountManager = accountManager.GetAccountManager(arguments.AuthorityName, identity.Name);
            }

            IUser user = accountManager.User(identity.Name);

            Page<IClient> page = accountManager.Clients(
                user,
                Mapper.Map(arguments.Example),
                Common.Commands.Mapper.Map(ColumnNames, arguments));

            List<ClientDto> values = new List<ClientDto>();

            foreach (IClient client in page.Items)
            {
                values.Add(Mapper.Map(client));
            }

            QueryClientByExampleResultsDto results = new QueryClientByExampleResultsDto
            {
                Arguments = arguments,
                Clients = new List<ClientDto>(values),
                TotalRowCount = page.TotalRowCount
            };

            return results;
        }
    }
}