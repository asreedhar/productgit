﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Clients.Commands.Impl
{
    /// <summary>
    /// Command to get users tied to a broker.
    /// </summary>
    public class UsersForBrokerCommand : ICommand<UsersForBrokerResultsDto, IdentityContextDto<UsersForBrokerArgumentsDto>>
    {        
        /// <summary>
        /// Get the broker's users.
        /// </summary>
        /// <param name="parameters">Broker handle.</param>
        /// <returns>List of users.</returns>
        public UsersForBrokerResultsDto Execute(IdentityContextDto<UsersForBrokerArgumentsDto> parameters)
        {
            UsersForBrokerArgumentsDto arguments = parameters.Arguments;
            IdentityDto identity = parameters.Identity;

            IAuthority      authority      = AuthorityFactory.GetAuthority(identity.AuthorityName);
            IAccountManager accountManager = authority.AccountManager;
            IBroker         broker         = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);                                    

            var sortedUsers = from u
                              in accountManager.Users(broker.Client)
                              orderby u.LastName, u.LastName
                              select u;

            List<UserDto> users = new List<UserDto>();
            foreach (IUser user in sortedUsers)
            {
                users.Add(Mapper.Map(user));
            }

            return new UsersForBrokerResultsDto
            {
                Arguments = arguments,
                Users = users
            };
        }        
    }
}
