using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Clients.Commands.Impl
{
    public static class Mapper
    {
        class Client : IClient
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string OrganizationName { get; set; }

            public Address Address { get; set; }

            public Client()
            {
                Name = OrganizationName = String.Empty;

                Address = new Address();
            }
        }

        public static IClient EmptyExample()
        {
            return new Client();
        }

        public static IClient Map(ClientDto client)
        {
            AddressDto address = client.Address ?? new AddressDto();

            return new Client
            {
                Id = client.Id,
                Name = client.Name,
                OrganizationName = client.OrganizationName,
                Address = new Address
                {
                    Line1 = address.Line1,
                    Line2 = address.Line2,
                    City = address.City,
                    State = address.State,
                    ZipCode = address.ZipCode
                }
            };
        }

        public static ClientDto Map(IClient client)
        {
            Address address = client.Address ?? new Address();

            return new ClientDto
            {
                Address = new AddressDto
                {
                    Line1 = address.Line1,
                    Line2 = address.Line2,
                    City = address.City,
                    State = address.State,
                    ZipCode = address.ZipCode
                },
                Id = client.Id,
                Name = client.Name,
                OrganizationName = client.OrganizationName
            };
        }

        public static UserDto Map(IUser user)
        {
            return new UserDto
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName
            };
        }

        public static PrincipalDto Map(IPrincipal principal)
        {
            return new PrincipalDto
            {
                AuthorityName = principal.AuthorityName,
                Handle = principal.Handle,
                User = Map(principal.User)
            };
        }
    }
}
