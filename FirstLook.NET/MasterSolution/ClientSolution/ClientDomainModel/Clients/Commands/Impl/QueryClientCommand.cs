using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Clients.Commands.Impl
{
    [Serializable]
    public class QueryClientCommand : ICommand<QueryClientResultsDto, IdentityContextDto<QueryClientArgumentsDto>>
    {
        public QueryClientResultsDto Execute(IdentityContextDto<QueryClientArgumentsDto> parameters)
        {
            IdentityDto identity = parameters.Identity;

            string authorityName = identity.AuthorityName;

            IAuthority authority = AuthorityFactory.GetAuthority(authorityName);

            IAccountManager accountManager = authority.AccountManager;

            IUser user = accountManager.User(identity.Name);

            IPrincipal principal = accountManager.Principal(user);

            Page<IClient> page = accountManager.Clients(
                user,
                Mapper.EmptyExample(),
                Common.Commands.Mapper.Map(QueryClientByExampleCommand.ColumnNames, parameters.Arguments));

            List<ClientDto> values = new List<ClientDto>();

            foreach (IClient client in page.Items)
            {
                values.Add(Mapper.Map(client));
            }

            List<AccountManagerDto> managers = new List<AccountManagerDto>();

            foreach (string name in accountManager.Authorities(identity.Name))
            {
                managers.Add(
                    new AccountManagerDto
                        {
                            AuthorityName = name,
                            Name = accountManager.GetAccountManager(name, identity.Name).Name
                        });
            }
            
            QueryClientResultsDto results = new QueryClientResultsDto
            {
                Arguments = parameters.Arguments,
                Clients = new List<ClientDto>(values),
                AccountManagers = managers,
                TotalRowCount = page.TotalRowCount,
                Principal = Mapper.Map(principal)
            };

            return results;
        }
    }
}
