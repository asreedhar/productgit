﻿using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Clients.Utilities
{
    public static class Principal
    {
        public static IPrincipal Get(string identityName, string authorityName)
        {
            IAuthority authority = AuthorityFactory.GetAuthority(authorityName);

            IAccountManager accountManager = authority.AccountManager;

            IUser user = accountManager.User(identityName);

            return accountManager.Principal(user);
        }
    }
}
