﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Clients.Utilities
{
    public static class Broker
    {
        public static IBroker Get(string identityName, string authorityName, string targetAuthorityName, int id)
        {
            IAuthority authority = AuthorityFactory.GetAuthority(authorityName);

            IAccountManager accountManager = authority.AccountManager;

            if (!Equals(authority.Name, targetAuthorityName))
            {
                accountManager = accountManager.GetAccountManager(targetAuthorityName, identityName);
            }

            IUser user = accountManager.User(identityName);

            IClient client = accountManager.Client(user, id);
            
            if (client == null) return null;

            return accountManager.Broker(client);
        }

        public static IBroker Get(string identityName, string authorityName, Guid id)
        {
            IAuthority authority = AuthorityFactory.GetAuthority(authorityName);

            IAccountManager accountManager = authority.AccountManager;

            IBroker broker = accountManager.Broker(id);

            if (broker == null)
            {
                foreach (string name in accountManager.Authorities(identityName))
                {
                    broker = accountManager.GetAccountManager(name, identityName).Broker(id);

                    if (broker != null)
                    {
                        break;
                    }
                }
            }

            if (broker != null)
            {
                return broker;
            }
            throw new ApplicationException("No broker.");
        }
    }
}
