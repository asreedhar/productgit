﻿namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers
{
    public class Authority : IAuthority
    {
        internal const string AuthorityName = "DEMO";

        private readonly AccountManager _manager = new AccountManager();

        public string Name
        {
            get { return AuthorityName; }
        }

        public IAccountManager AccountManager
        {
            get
            {
                if (_manager.Authority == null)
                {
                    _manager.Authority = this;
                }

                return _manager;
            }
        }
    }
}
