﻿using System;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers
{
    public interface IRepository
    {
        Page<Seller> Sellers(string userName, IClient example, PageArguments pagination);

        Seller Seller(string userName, int id);

        IBroker Broker(int id);

        IBroker Broker(Guid id);
    }
}
