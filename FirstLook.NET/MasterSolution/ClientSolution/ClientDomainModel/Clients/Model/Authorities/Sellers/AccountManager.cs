﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers
{
    public class AccountManager : IAccountManager
    {
        private IAuthority _authority;

        public IAuthority Authority
        {
            get { return _authority; }
            internal set { _authority = value; }
        }

        public IEnumerable<string> Authorities(string userName)
        {
            return new string[] {};
        }

        public IAccountManager GetAccountManager(string authorityName, string userName)
        {
            throw new ArgumentOutOfRangeException("authorityName", authorityName);
        }

        public string Name
        {
            get { return "Seller"; }
        }

        public IUser User(string userName)
        {
            return new Dealers.AccountManager().User(userName);
        }

        public IList<IUser> Users(IClient client)
        {
            return new List<IUser>();
        }

        public Page<IClient> Clients(IUser user, IClient example, PageArguments pagination)
        {
            Page<Seller> page = Resolve<IRepository>().Sellers(user.UserName, example, pagination);

            Page<IClient> clients = new Page<IClient>
            {
                Items = new List<IClient>(),
                TotalRowCount = page.TotalRowCount
            };

            foreach (Seller seller in page.Items)
            {
                clients.Items.Add(seller);
            }

            return clients;
        }

        public IClient Client(IUser user, int id)
        {
            return Resolve<IRepository>().Seller(user.UserName, id);
        }

        public IBroker Broker(IClient client)
        {
            return Resolve<IRepository>().Broker(client.Id);
        }

        public IBroker Broker(Guid id)
        {
            return Resolve<IRepository>().Broker(id);
        }

        public IPrincipal Principal(IUser user)
        {
            return new Dealers.AccountManager().Principal(user);
        }

        public IPrincipal Principal(Guid id)
        {
            return new Dealers.AccountManager().Principal(id);
        }

        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
