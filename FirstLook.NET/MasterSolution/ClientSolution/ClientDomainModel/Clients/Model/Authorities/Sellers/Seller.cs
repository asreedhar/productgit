﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers
{
    [Serializable]
    public class Seller : IClient
    {
        private int _id;
        private string _name;
        private string _organizationName;
        private Address _address;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        public string OrganizationName
        {
            get { return _organizationName; }
            internal set { _organizationName = value; }
        }

        public Address Address
        {
            get { return _address; }
            internal set { _address = value; }
        }
    }
}
