﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers
{
    [Serializable]
    public enum MemberType
    {
        Dummy = 0,
        Administrator = 1,
        User = 2,
        AccountRepresentative = 3
    }
}
