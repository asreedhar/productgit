﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers
{
    public interface IRepository
    {
        Member Member(string userName);

        IList<Member> Members(int id);

        Page<Dealer> Dealers(string userName, IClient example, PageArguments pagination);

        Dealer Dealer(string userName, int id);

        IBroker Broker(int id);

        IBroker Broker(Guid id);

        IPrincipal Principal(int id);

        IPrincipal Principal(Guid id);
    }
}
