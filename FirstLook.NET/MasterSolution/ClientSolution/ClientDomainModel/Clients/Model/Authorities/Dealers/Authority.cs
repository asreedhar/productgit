﻿namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers
{
    /// <summary>
    /// Dealer authority.
    /// </summary>
    public class Authority : IAuthority
    {
        /// <summary>
        /// CAS is the dealer authority.
        /// </summary>
        internal const string AuthorityName = "CAS";       

        /// <summary>
        /// Get the name of the dealer authority (i.e. CAS).
        /// </summary>
        public string Name
        {
            get { return AuthorityName; }
        }

        /// <summary>
        /// Account manager for this authority.
        /// </summary>
        private readonly AccountManager _manager = new AccountManager();

        /// <summary>
        /// Get this authority's account manager.
        /// </summary>
        public IAccountManager AccountManager
        {
            get
            {
                if (_manager.Authority == null)
                {
                    _manager.Authority = this;
                }

                return _manager;
            }
        }
    }
}
