﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers
{
    [Serializable]
    public class Member : IUser
    {
        private int _id;
        private string _userName;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string UserName
        {
            get { return _userName; }
            internal set { _userName = value; }
        }

        private string _firstName;
        private string _lastName;

        public string FirstName
        {
            get { return _firstName; }
            internal set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            internal set { _lastName = value; }
        }

        private MemberType _memberType;

        public MemberType MemberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }
    }
}
