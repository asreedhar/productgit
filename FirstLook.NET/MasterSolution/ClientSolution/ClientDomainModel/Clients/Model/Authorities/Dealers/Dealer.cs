﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers
{
    [Serializable]
    public class Dealer : IClient
    {
        private int _id;
        private string _name;
        private string _shortName;
        private string _organizationName;
        private string _code;
        private Address _address;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        public string ShortName
        {
            get { return _shortName; }
            internal set { _shortName = value; }
        }

        public string OrganizationName
        {
            get { return _organizationName; }
            internal set { _organizationName = value; }
        }

        public string Code
        {
            get { return _code; }
            internal set { _code = value; }
        }

        public Address Address
        {
            get { return _address; }
            internal set { _address = value; }
        }
    }
}
