﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers
{
    public class AccountManager : IAccountManager
    {
        /// <summary>
        /// Name of the authority this account manager is for.
        /// </summary>
        public IAuthority Authority { get; internal set; }

        /// <summary>
        /// Get the authorities tied to the user with the given name. If the user is an administrator, he has access to
        /// all user authorities. Otherwise, the user just has access to the dealer authority.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public IEnumerable<string> Authorities(string userName)
        {
            Member member = Resolve<IRepository>().Member(userName);

            if (member != null)
            {
                if (member.MemberType == MemberType.Administrator)
                {
                    return new[] {Dealers.Authority.AuthorityName, Sellers.Authority.AuthorityName};
                }
            }

            return new[] { Dealers.Authority.AuthorityName };
        }

        public IAccountManager GetAccountManager(string authorityName, string userName)
        {
            if (!Authorities(userName).Contains(authorityName))
            {
                throw new ArgumentOutOfRangeException("authorityName", authorityName, "User does not access to this authority");
            }

            return AuthorityFactory.GetAuthority(authorityName).AccountManager;
        }

        /// <summary>
        /// This is the dealer account manager.
        /// </summary>
        public string Name
        {
            get { return "Dealer"; }
        }

        /// <summary>
        /// Get the user with the given username.
        /// </summary>
        /// <param name="userName">Username.</param>
        /// <returns>User.</returns>
        public IUser User(string userName)
        {
            return Resolve<IRepository>().Member(userName);
        }

        /// <summary>
        /// Get the list of users for a client.
        /// </summary>
        /// <param name="client">Client whose users to retrieve.</param>
        /// <returns>List of users.</returns>
        public IList<IUser> Users(IClient client)
        {
            List<IUser> users = new List<IUser>();

            foreach (Member member in Resolve<IRepository>().Members(client.Id))
            {
                users.Add(member);
            }
            
            return users;
        }

        /// <summary>
        /// Get the paged list of clients that the given user has access to and that match the given example.
        /// </summary>
        /// <param name="user">User whose available clients should be retrieved.</param>
        /// <param name="example">Example that retrieved clients must match.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Paged list of clients.</returns>
        public Page<IClient> Clients(IUser user, IClient example, PageArguments pagination)
        {
            Page<Dealer> page = Resolve<IRepository>().Dealers(user.UserName, example, pagination);

            Page<IClient> clients = new Page<IClient>
            {
                Items = new List<IClient>(),
                TotalRowCount = page.TotalRowCount
            };

            foreach (Dealer dealer in page.Items)
            {
                clients.Items.Add(dealer);
            }

            return clients;
        }

        public IClient Client(IUser user, int id)
        {
            return Resolve<IRepository>().Dealer(user.UserName, id);
        }

        /// <summary>
        /// Get the broker for the given client.
        /// </summary>
        /// <param name="client">Client.</param>
        /// <returns>Broker.</returns>
        public IBroker Broker(IClient client)
        {
            return Resolve<IRepository>().Broker(client.Id);
        }

        /// <summary>
        /// Get the broker with the given handle.
        /// </summary>
        /// <param name="id">Handle.</param>
        /// <returns>Broker.</returns>
        public IBroker Broker(Guid id)
        {
            return Resolve<IRepository>().Broker(id);
        }

        /// <summary>
        /// Get the principal for the given user.
        /// </summary>
        /// <param name="user">User.</param>
        /// <returns>Principal.</returns>
        public IPrincipal Principal(IUser user)
        {
            return Resolve<IRepository>().Principal(user.Id);
        }

        /// <summary>
        /// Get the principal with the given handle.
        /// </summary>
        /// <param name="id">Handle.</param>
        /// <returns>Principal.</returns>
        public IPrincipal Principal(Guid id)
        {
            return Resolve<IRepository>().Principal(id);
        }

        /// <summary>
        /// Helper for resolving the registered implementation of type T.
        /// </summary>
        /// <typeparam name="T">Type to get an implementation of.</typeparam>
        /// <returns>The registered implementation of type T.</returns>
        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
