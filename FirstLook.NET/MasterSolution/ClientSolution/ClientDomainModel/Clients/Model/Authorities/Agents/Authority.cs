
namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents
{
    /// <summary>
    /// System account authority.
    /// </summary>
    public class Authority : IAuthority
    {
        /// <summary>
        /// Name of this authority.
        /// </summary>
        internal const string AuthorityName = "SYSTEM";

        /// <summary>
        /// Name of this authority.
        /// </summary>
        public string Name
        {
            get { return AuthorityName; }
        }        

        /// <summary>
        /// The account manager for this authority.
        /// </summary>
        public IAccountManager AccountManager
        {
            get
            {
                if (_manager.Authority == null)
                {
                    _manager.Authority = this;
                }

                return _manager;
            }
        }
        private readonly AccountManager _manager = new AccountManager();
    }
}
