﻿using FirstLook.Client.DomainModel.Clients.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents
{
    /// <summary>
    /// System account.
    /// </summary>
    public class Agent : IUser
    {
        /// <summary>
        /// User identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Type of system account.
        /// </summary>
        public UserType Type { get; set; }

        /// <summary>
        /// User name.
        /// </summary>
        public string UserName { get; internal set; }

        /// <summary>
        /// First name.
        /// </summary>
        public string FirstName { get; internal set; }

        /// <summary>
        /// Last name.
        /// </summary>
        public string LastName { get; internal set; }
    }
}
