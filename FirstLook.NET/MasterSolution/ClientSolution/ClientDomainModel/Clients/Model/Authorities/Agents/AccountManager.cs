﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents
{
    /// <summary>
    /// Account manager for system accounts.
    /// </summary>
    public class AccountManager : IAccountManager
    {
        /// <summary>
        /// Get the system account authority.
        /// </summary>
        public IAuthority Authority { get; internal set; }

        public IEnumerable<string> Authorities(string userName)
        {
            Agent member = Resolve<IRepository>().Agent(userName);

            if (member != null)
            {
                return new[] { Agents.Authority.AuthorityName };
            }

            throw new ArgumentException("Not a system account", "userName");
        }

        public IAccountManager GetAccountManager(string authorityName, string userName)
        {
            if (!Authorities(userName).Contains(authorityName))
            {
                throw new ArgumentOutOfRangeException("authorityName", authorityName, "User does not access to this authority");
            }

            return AuthorityFactory.GetAuthority(authorityName).AccountManager;
        }
        
        /// <summary>
        /// Get the name of this account manager.
        /// </summary>
        public string Name
        {
            get { return "System"; }
        }

        public IUser User(string userName)
        {
            return Resolve<IRepository>().Agent(userName);
        }

        public IList<IUser> Users(IClient client)
        {
            throw new NotSupportedException();
        }
        public Page<IClient> Clients(IUser user, IClient example, PageArguments pagination)
        {
            throw new NotSupportedException();
        }

        public IClient Client(IUser user, int id)
        {
            throw new NotSupportedException();
        }

        public IBroker Broker(IClient client)
        {
            throw new NotSupportedException();
        }

        public IBroker Broker(Guid id)
        {
            throw new NotSupportedException();
        }
        public IPrincipal Principal(IUser user)
        {
            return Resolve<IRepository>().Principal(user.Id);
        }

        public IPrincipal Principal(Guid id)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Helper class for resolving an instance of type T from the registry.
        /// </summary>
        /// <typeparam name="T">Type of object to get an implementation of.</typeparam>
        /// <returns>The registered implementation of type T.</returns>
        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
