﻿namespace FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents
{
    /// <summary>
    /// Interface for a system account repository.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Fetch the system agent with the given name.
        /// </summary>
        /// <param name="userName">Agent name.</param>
        /// <returns>System agent.</returns>
        Agent Agent(string userName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IPrincipal Principal(int id);
    }
}
