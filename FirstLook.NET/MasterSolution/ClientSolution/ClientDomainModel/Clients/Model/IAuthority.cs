﻿namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Interface for an access granting authority.
    /// </summary>
    public interface IAuthority
    {
        /// <summary>
        /// Name of the authority.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Authority's account manager.
        /// </summary>
        IAccountManager AccountManager { get; }
    }
}
