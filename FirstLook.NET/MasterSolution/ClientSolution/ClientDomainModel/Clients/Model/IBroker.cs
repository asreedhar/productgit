﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Inteface for a combination of a client and the authority that provides access for the client.
    /// </summary>
    public interface IBroker
    {
        /// <summary>
        /// Name of the access granting authority.
        /// </summary>
        string AuthorityName { get; }

        /// <summary>
        /// Client.
        /// </summary>
        IClient Client { get; }

        /// <summary>
        /// Handle.
        /// </summary>
        Guid Handle { get; }

        /// <summary>
        /// Identifier.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Legacy integration for determining if this broker concerns a dealer or seller.
        /// </summary>
        Integration Integration { get; }
    }
}
