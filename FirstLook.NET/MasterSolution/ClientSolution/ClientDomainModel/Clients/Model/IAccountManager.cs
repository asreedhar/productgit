﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Interface for an account manager.
    /// </summary>
    public interface IAccountManager
    {
        IAuthority Authority { get; }

        IEnumerable<string> Authorities(string userName);

        IAccountManager GetAccountManager(string authorityName, string userName);

        string Name { get; }

        IUser User(string userName);

        IList<IUser> Users(IClient client);

        /// <summary>
        /// Query for all clients that match the user-provided example.
        /// </summary>
        /// <param name="user">User to find clients for.</param>
        /// <param name="example">Example client by which to narrow the results.</param>
        /// <param name="pagination">Paging arguments (e.g. sort columns, etc).</param>
        /// <returns>List of clients.</returns>
        Page<IClient> Clients(IUser user, IClient example, PageArguments pagination);

        IClient Client(IUser user, int id);

        IBroker Broker(IClient client);

        IBroker Broker(Guid id);

        IPrincipal Principal(IUser user);

        IPrincipal Principal(Guid id);
    }
}
