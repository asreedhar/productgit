﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Client street address.
    /// </summary>
    [Serializable]
    public class Address
    {
        /// <summary>
        /// First line of the street address.
        /// </summary>
        public string Line1 { get; internal set; }

        /// <summary>
        /// Second line of the street address.
        /// </summary>
        public string Line2 { get; internal set; }

        /// <summary>
        /// City.
        /// </summary>
        public string City { get; internal set; }

        /// <summary>
        /// State.
        /// </summary>
        public string State { get; internal set; }

        /// <summary>
        /// ZIP code.
        /// </summary>
        public string ZipCode { get; internal set; }
    }
}
