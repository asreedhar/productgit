﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Interface for a combination of a user and the authority that provides access for the user.
    /// </summary>
    public interface IPrincipal
    {
        /// <summary>
        /// Name of the access granting authority.
        /// </summary>
        string AuthorityName { get; }

        /// <summary>
        /// User.
        /// </summary>
        IUser User { get; }

        /// <summary>
        /// Handle.
        /// </summary>
        Guid Handle { get; }
    }
}
