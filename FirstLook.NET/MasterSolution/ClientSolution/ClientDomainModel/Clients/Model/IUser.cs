﻿namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Interface for a user.
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// User identifier.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// User name.
        /// </summary>
        string UserName { get; }

        /// <summary>
        /// First name.
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Last name.
        /// </summary>
        string LastName { get; }
    }
}
