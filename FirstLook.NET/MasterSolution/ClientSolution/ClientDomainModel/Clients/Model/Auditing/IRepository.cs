﻿namespace FirstLook.Client.DomainModel.Clients.Model.Auditing
{
    /// <summary>
    /// Interface for an audit repository.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Create an audit for the given principal user.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <returns>New audit.</returns>
        Audit Create(IPrincipal principal);

        /// <summary>
        /// Insert an audit row.
        /// </summary>
        /// <remarks>
        /// Audit information must be present in the session prior to calling this function.
        /// </remarks>
        /// <param name="wasInsert">Was the action performed an insert?</param>
        /// <param name="wasUpdate">Was the action performed an update?</param>
        /// <param name="wasDelete">Was the action performed a delete?</param>
        /// <returns>New audit row, or null if one couldn't be created.</returns>
        AuditRow Insert(bool wasInsert, bool wasUpdate, bool wasDelete);

        /// <summary>
        /// Insert a new audit row that updates the given row.
        /// </summary>
        /// <remarks>
        /// Audit information must be present in the session prior to calling this function.
        /// </remarks>
        /// <param name="row">Audit row to update as being no longer active.</param>
        /// <param name="wasInsert">Was the action performed an insert?</param>
        /// <param name="wasUpdate">Was the action performed an update?</param>
        /// <param name="wasDelete">Was the action performed a delete?</param>
        /// <returns>New audit row, or null if one couldn't be created.</returns>
        AuditRow Update(AuditRow row, bool wasInsert, bool wasUpdate, bool wasDelete);
    }
}
