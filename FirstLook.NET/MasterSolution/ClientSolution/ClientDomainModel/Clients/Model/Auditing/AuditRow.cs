﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model.Auditing
{
    /// <summary>
    /// The association of an audit to a particular user action. Multiple audit rows may exist for a single audit.
    /// </summary>
    [Serializable]
    public class AuditRow
    {
        /// <summary>
        /// Audit row identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Was the action that is being audited an insert?
        /// </summary>
        public bool WasInsert { get; internal set; }

        /// <summary>
        /// Was the action that is being audited an update?
        /// </summary>
        public bool WasUpdate { get; internal set; }

        /// <summary>
        /// Was the action that is being audited a delete?
        /// </summary>
        public bool WasDelete { get; internal set; }

        /// <summary>
        /// Date from which the audited action is valid.
        /// </summary>
        public DateTime ValidFrom { get; internal set; }

        /// <summary>
        /// Date until which the audited action is valid.
        /// </summary>
        public DateTime ValidUpTo { get; internal set; }
    }
}
