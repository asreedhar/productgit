﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model.Auditing
{
    /// <summary>
    /// An audited association between two or more objects.
    /// </summary>
    [Serializable]
    public class Association
    {
        /// <summary>
        /// Association identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Audit row for this association.
        /// </summary>
        public AuditRow AuditRow { get; internal set; }
    }
}
