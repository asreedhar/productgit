﻿using System;

namespace FirstLook.Client.DomainModel.Clients.Model.Auditing
{
    /// <summary>
    /// An auditing of one or more user actions. This audit object serves as a parent to one or more audit rows, which
    /// serve as the association of this audit to an individual action.
    /// </summary>
    [Serializable]
    public class Audit
    {
        /// <summary>
        /// Audit identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// User performing the action or actions that are being audited.
        /// </summary>
        public int User { get; internal set; }

        /// <summary>
        /// Date of the action being that is being audited.
        /// </summary>
        public DateTime Date { get; internal set; }
    }
}
