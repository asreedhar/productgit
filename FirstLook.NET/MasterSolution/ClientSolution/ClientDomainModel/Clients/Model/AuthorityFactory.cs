using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Authority factory.
    /// </summary>
    public static class AuthorityFactory
    {
        /// <summary>
        /// Available authorities.
        /// </summary>
        private static readonly IDictionary<string, IAuthority> Values;

        /// <summary>
        /// Initialize this factory with the list of available authorities.
        /// </summary>
        static AuthorityFactory()
        {
            IAuthority a = new Authorities.Dealers.Authority(),
                       b = new Authorities.Sellers.Authority(),
                       c = new Authorities.Agents.Authority();

            Values = new Dictionary<string, IAuthority> {{a.Name, a}, {b.Name, b}, {c.Name, c}};
        }

        /// <summary>
        /// Get the authority that has the given name.
        /// </summary>
        /// <param name="name">Authority name.</param>
        /// <returns>Authority that has the given name.</returns>
        public static IAuthority GetAuthority(string name)
        {
            if (Values.ContainsKey(name))
            {
                return Values[name];
            }

            throw new ArgumentOutOfRangeException("name", name, "Unknown authority");
        }
    }
}
