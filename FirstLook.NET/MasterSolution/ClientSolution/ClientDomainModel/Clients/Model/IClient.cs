﻿namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// Interface for a client.
    /// </summary>
    public interface IClient
    {
        /// <summary>
        /// Client identifier.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Client name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Name of the client's organization.
        /// </summary>
        string OrganizationName { get; }

        /// <summary>
        /// Street address.
        /// </summary>
        Address Address { get; }
    }
}
