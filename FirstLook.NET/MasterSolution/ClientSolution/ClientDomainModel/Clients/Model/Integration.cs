﻿namespace FirstLook.Client.DomainModel.Clients.Model
{
    /// <summary>
    /// The purpose of this class is to expose properties that allow components to integrate with legacy systems by 
    /// determining the type of client.  As such, every system created before the broker concept was introduced is 
    /// legacy.  We should not create any more legacy systems.  This class should not get larger.
    /// </summary>
    /// <remarks>
    /// The class is called broken as a big, fat warning sign to people that using the information on this class is 
    /// considered bad and only accceptable when integrating with legacy components.
    /// </remarks>
    public class Integration
    {
        /// <summary>
        /// Is the client a dealer?
        /// </summary>
        public bool IsDealer { get; internal set; }

        /// <summary>
        /// Is the client a seller?
        /// </summary>
        public bool IsSeller { get; internal set; }
    }
}
