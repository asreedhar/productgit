﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients
{
    /// <summary>
    /// Module for configuring the client model.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register components required by clients.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Repositories.Module>();
        }
    }
}
