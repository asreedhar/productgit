﻿using System;

namespace FirstLook.Client.DomainModel.Common.Model
{
    /// <summary>
    /// Arguments for paging results.
    /// </summary>
    [Serializable]
    public class PageArguments
    {
        /// <summary>
        /// How to sort hte results.
        /// </summary>
        public string SortExpression { get; internal set; }

        /// <summary>
        /// The row to start the results from.
        /// </summary>
        public int StartRowIndex { get; internal set; }

        /// <summary>
        /// The number of rows to return.
        /// </summary>
        public int MaximumRows { get; internal set; }

        public override string ToString()
        {
            return string.Format(
                "[SortExpression:{0};StartRowIndex:{1};MaximumRows{2}]",
                SortExpression,
                StartRowIndex,
                MaximumRows);
        }
    }
}
