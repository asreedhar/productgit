﻿using System;

namespace FirstLook.Client.DomainModel.Common.Model
{
    [Serializable]
    public class RowFilter
    {
        /// <summary>
        /// The specific column to filter on.
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// The value of the dimension.
        /// </summary>
        public string Value { get; set; }
    }
}
