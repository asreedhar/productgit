﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Common.Model
{
    /// <summary>
    /// A page of results.
    /// </summary>
    /// <typeparam name="T">The type of result that is being paged.</typeparam>
    [Serializable]
    public class Page<T>
    {
        /// <summary>
        /// Paging arguments used to produce the results.
        /// </summary>
        public PageArguments Arguments { get; set; }

        /// <summary>
        /// Total number of results returned.
        /// </summary>
        public int TotalRowCount { get; set; }

        /// <summary>
        /// The paged results.
        /// </summary>
        public IList<T> Items { get; set; }
    }
}
