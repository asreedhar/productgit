using System.ComponentModel;

namespace FirstLook.Client.DomainModel.Common.Repositories.Entities
{
    public class SavableState
    {
        private bool _new, _deleted, _dirty;

        public bool IsNew
        {
            get { return _new; }
            set { _new = value; }
        }

        public bool IsDeleted
        {
            get { return _deleted; }
            set { _deleted = value; }
        }

        public bool IsDirty
        {
            get { return _dirty; }
            set { _dirty = value; }
        }

        public SavableState()
        {
            IsDirty = true;
            IsNew = true;
        }

        public void MarkNew()
        {
            IsNew = true;
            IsDeleted = false;
            MarkDirty();
        }

        public void MarkOld()
        {
            IsNew = false;
            MarkClean();
        }

        public void MarkDeleted()
        {
            IsDeleted = true;
            MarkDirty();
        }

        public void MarkDirty()
        {
            IsDirty = true;
        }

        public void MarkClean()
        {
            IsDirty = false;
        }

        public void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            MarkDirty();
        }
    }
}