using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;

namespace FirstLook.Client.DomainModel.Common.Repositories.Entities
{
    public class SavedEventArgs : EventArgs
    {
        private readonly AuditRow _auditRow;

        public SavedEventArgs(AuditRow auditRow)
        {
            _auditRow = auditRow;
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }
    }
}