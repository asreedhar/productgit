using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Common.Core.Component;

namespace FirstLook.Client.DomainModel.Common.Repositories.Entities
{
    public interface ISavable : IPersisted
    {
        AuditRow AuditRow { get; }

        void OnSaved(object sender, SavedEventArgs e);
    }
}