﻿using System.Collections.Generic;
using System.Data;

namespace FirstLook.Client.DomainModel.Common.Repositories
{
    public interface ISerializer<T>
    {
        IList<T> Deserialize(IDataReader reader);

        T Deserialize(IDataRecord record);
    }
}