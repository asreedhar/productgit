﻿using System.Collections.Generic;
using System.Data;

namespace FirstLook.Client.DomainModel.Common.Repositories
{
    public abstract class Serializer<T> : ISerializer<T>
    {
        public virtual IList<T> Deserialize(IDataReader reader)
        {
            List<T> list = new List<T>();

            while (reader.Read())
            {
                list.Add(Deserialize((IDataRecord)reader));
            }

            return list;
        }

        public abstract T Deserialize(IDataRecord record);
    }
}