﻿using System;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes
{
    public interface IIdentityContextDto
    {
        IdentityDto Identity { get; set; }
    }

    public interface IIdentityContextDto<T> : IIdentityContextDto
    {
        T Arguments { get; set; }
    }

    [Serializable]
    public class IdentityContextDto : IIdentityContextDto
    {
        public IdentityDto Identity { get; set; }
    }

    [Serializable]
    public class IdentityContextDto<T> : IdentityContextDto, IIdentityContextDto<T>
    {
        public T Arguments { get; set; }
    }
}
