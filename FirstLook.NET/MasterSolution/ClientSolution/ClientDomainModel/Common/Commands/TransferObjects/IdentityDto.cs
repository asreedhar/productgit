﻿using System;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    [Serializable]
    public class IdentityDto
    {
        public string Name { get; set; }

        public string AuthorityName { get; set; }
    }
}