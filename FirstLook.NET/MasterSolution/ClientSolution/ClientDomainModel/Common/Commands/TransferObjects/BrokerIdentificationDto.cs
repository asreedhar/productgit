﻿using System;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    [Serializable]
    public class BrokerIdentificationDto
    {
        private int _id;
        private Guid _handle;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Guid Handle
        {
            get { return _handle; }
            set { _handle = value; }
        }
    }
}