﻿using System;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    /// <summary>
    /// Command results that will be paginated.
    /// </summary>
    [Serializable]
    public abstract class PaginatedResultsDto
    {
        /// <summary>
        /// Total number of rows in the results.
        /// </summary>
        public int TotalRowCount { get; set; }
    }
}
