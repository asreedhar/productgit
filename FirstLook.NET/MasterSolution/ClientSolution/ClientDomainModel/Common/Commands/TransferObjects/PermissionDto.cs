﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    /// <summary>
    /// <para>Abstract class for representing access to a system resource. All permissions have a name whose 
    /// interpretation depends on the subclass.</para>
    /// <para>Most permission objects also include an "actions" list that tells the actions that are permitted for the
    /// object. The actions list is optional for permission objects that don't need such a list; you either have the 
    /// named permission (such as "system.exit") or you don't.</para>
    /// </summary>
    [Serializable]
    public abstract class PermissionDto
    {
        /// <summary>
        /// Name of the permission.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// List of actions for this permission.
        /// </summary>
        public List<string> Actions { get; set; }

        /// <summary>
        /// Create an empty permission object.
        /// </summary>
        public PermissionDto()
        {
            Actions = new List<string>();
            
        }
    }
}
