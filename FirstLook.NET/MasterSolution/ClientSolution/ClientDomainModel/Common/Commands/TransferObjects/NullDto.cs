﻿using System;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    /// <summary>
    /// Empty transfer object.
    /// </summary>
    [Serializable]
    public class NullDto
    {
    }
}
