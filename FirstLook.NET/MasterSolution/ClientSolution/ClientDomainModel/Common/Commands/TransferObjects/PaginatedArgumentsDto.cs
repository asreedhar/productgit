﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    /// <summary>
    /// Command arguments that affect pagination, including options for sorting and the number of results per page.
    /// </summary>
    [Serializable]
    public abstract class PaginatedArgumentsDto
    {
        /// <summary>
        /// Column to sort the results on.
        /// </summary>
        public List<SortColumnDto> SortColumns { get; set; }

        /// <summary>
        /// Maximum number of results per page.
        /// </summary>
        public int MaximumRows { get; set; }

        /// <summary>
        /// The row to begin displaying.
        /// </summary>
        public int StartRowIndex { get; set; }
    }
}
