﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    /// <summary>
    /// A list of permissions.
    /// </summary>
    [Serializable]
    public class PermissionsDto
    {
        /// <summary>
        /// List of permissions.
        /// </summary>
        public List<PermissionDto> Items { get; set; }

        /// <summary>
        /// Create an empty list of permissions.
        /// </summary>
        public PermissionsDto()
        {
            Items = new List<PermissionDto>();
        }
    }
}
