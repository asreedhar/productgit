﻿using System;

namespace FirstLook.Client.DomainModel.Common.Commands.TransferObjects
{
    /// <summary>
    /// Filter to apply to a row-set.
    /// </summary>
    [Serializable]
    public class RowFilterDto
    {
        /// <summary>
        /// The specific column to filter on.
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// The value of the dimension.
        /// </summary>
        public string Value { get; set; }
    }
}
