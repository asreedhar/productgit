﻿namespace FirstLook.Client.DomainModel.Common.Commands
{
    /// <summary>
    /// Policy provider.
    /// </summary>
    public interface IPolicyProvider
    {
        /// <summary>
        /// The policy that defines access to a command.
        /// </summary>
        IPolicy Policy { get; }
    }
}
