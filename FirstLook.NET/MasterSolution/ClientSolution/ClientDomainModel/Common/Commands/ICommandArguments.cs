﻿namespace FirstLook.Client.DomainModel.Common.Commands
{
    /// <summary>
    /// Arguments used in a command to produce results.
    /// </summary>    
    public interface ICommandArguments
    {
    }
}