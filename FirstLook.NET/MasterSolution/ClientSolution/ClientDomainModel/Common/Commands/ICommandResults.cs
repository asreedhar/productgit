﻿namespace FirstLook.Client.DomainModel.Common.Commands
{
    /// <summary>
    /// Results of a command must include the arguments used to produce those results.
    /// </summary>
    /// <typeparam name="T">Command arguments type.</typeparam>
    public interface ICommandResults<T> where T : ICommandArguments
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        T Arguments { get; set;}
    }
}