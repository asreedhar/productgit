﻿using System.Security.Principal;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Common.Commands
{
    /// <summary>
    /// Interface for a permission policy.
    /// </summary>
    public interface IPolicy
    {
        /// <summary>
        /// Permissions as applicable to the current user.
        /// </summary>
        /// <param name="principal">Current user.</param>
        /// <returns>List of permissions.</returns>
        PermissionsDto Permissions(IPrincipal principal);

        /// <summary>
        /// Permissions as applicable to the current user for the given broker.
        /// </summary>
        /// <param name="principal">Current user.</param>
        /// <param name="brokerHandle">Broker.</param>
        /// <returns>List of permissions.</returns>
        PermissionsDto Permissions(IPrincipal principal, string brokerHandle);

        /// <summary>
        /// Permissions as applicable to the current user for the given broker and vehicle.
        /// </summary>
        /// <param name="principal">Current user.</param>
        /// <param name="brokerHandle">Broker.</param>
        /// <param name="vehicleHandle">Vehicle.</param>
        /// <returns>List of permissions.</returns>
        PermissionsDto Permissions(IPrincipal principal, string brokerHandle, string vehicleHandle);
    }
}
