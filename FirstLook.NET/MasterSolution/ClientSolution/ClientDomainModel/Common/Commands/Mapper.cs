﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Common.Commands
{
    public static class Mapper
    {
        /// <summary>
        /// Map pagination arguments.
        /// </summary>
        /// <param name="columnNames"></param>
        /// <param name="arguments">Paging arguments transfer object.</param>
        /// <returns>Paging arguments.</returns>
        public static PageArguments Map(IEnumerable<string> columnNames, PaginatedArgumentsDto arguments)
        {
            StringBuilder sortExpression = new StringBuilder();

            foreach (SortColumnDto sortColumn in arguments.SortColumns)
            {
                string name = sortColumn.ColumnName;

                if (columnNames.Contains(name))
                {
                    if (sortExpression.Length > 0)
                    {
                        sortExpression.Append(", ");
                    }

                    sortExpression.Append(name);

                    if (sortColumn.Ascending)
                    {
                        sortExpression.Append(" ASC");
                    }
                    else
                    {
                        sortExpression.Append(" DESC");
                    }
                }
            }

            int maximumRows = arguments.MaximumRows;

            if (maximumRows < 1)
            {
                maximumRows = 1;
            }

            int startRowIndex = arguments.StartRowIndex;

            if (startRowIndex < 0)
            {
                startRowIndex = 0;
            }

            return new PageArguments
            {
                MaximumRows = maximumRows,
                SortExpression = sortExpression.ToString(),
                StartRowIndex = startRowIndex
            };
        }
    }
}
