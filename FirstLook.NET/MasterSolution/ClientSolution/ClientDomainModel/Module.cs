﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel
{    
    /// <summary>
    /// Module for configuring the domain model.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register components required by the domain model.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<Clients.Module>();

            registry.Register<Licenses.Module>();

            registry.Register<Vehicles.Module>();
        }
    }
}
