﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FirstLook.Client.DomainModel")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("FirstLook.Client.DomainModel")]

[assembly: InternalsVisibleTo("FirstLook.VehicleValuationGuide.DomainModel.Test")]

[assembly: InternalsVisibleTo("FirstLook.Client.DomainModel.Test")]