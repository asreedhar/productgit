﻿using System;
using AmazingCloudSearch.Contract;
using Newtonsoft.Json;

namespace AmazingCloudSearch.MaxInterface.Documents
{
    public class DealerSearchDocument : ICloudSearchDocument
    {
        private string _ownerhandle;
        
        [JsonProperty("id")]
        public string Id { get { return _ownerhandle; } set { _ownerhandle = value; } }

        #region int values

        [JsonProperty("businessunitid")]
        public int BusinessUnitId { get; set; }

        [JsonProperty("groupid")]
        public int GroupId { get; set; }

        #endregion int values

        #region string values

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("businessunitname")]
        public string BusinessUnitName { get; set; }

        [JsonProperty("businessunitcode")]
        public string BusinessUnitCode { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// The OwnerHandle is the document id. "id" is a reserved keyword in CloudSearch to identify a specific document.
        /// We want only one document (search result) per business unit, so by using the ownerhandle we can perform updates
        /// on existing documents thus avoiding the need to tear down and re-create the cloudsearch instance.
        /// </summary>
        [JsonProperty("ownerhandle")]
        public string OwnerHandle { 
            get { return _ownerhandle; }
            set {
                _ownerhandle = value;
            }
        }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
        
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("logourl")]
        public string LogoUrl { get; set; }

        [JsonProperty("zip")]
        public string ZipCode { get; set; }
        
        #endregion string values

        #region upgrades

		[JsonProperty("maxforwebsite1_enabled")]
		[JsonConverter(typeof(BooleanToIntJsonConverter))]
		public bool HasMaxForWebsite1Upgrade { get; set; }
		
		[JsonProperty("maxforwebsite2_enabled")]
        [JsonConverter(typeof(BooleanToIntJsonConverter))]
        public bool HasMaxForWebsite2Upgrade { get; set; }

        [JsonProperty("mobileshowroom_enabled")]
        [JsonConverter(typeof(BooleanToIntJsonConverter))]
        public bool HasMobileShowroomUpgrade { get; set; }

        [JsonProperty("showroom_enabled")]
        [JsonConverter(typeof(BooleanToIntJsonConverter))]
        public bool HasShowroomUpgrade { get; set; }

        [JsonProperty("bookvaluations_enabled")]
        [JsonConverter(typeof(BooleanToIntJsonConverter))]
        public bool HasShowroomBookValuations { get; set; }

        [JsonProperty("new_vehicles_enabled")]
        [JsonConverter(typeof(BooleanToIntJsonConverter))]
        public bool HasNewVehiclesEnabled { get; set; }

        [JsonProperty("used_vehicles_enabled")]
        [JsonConverter(typeof(BooleanToIntJsonConverter))]
        public bool HasUsedVehiclesEnabled { get; set; }

	    #endregion upgrades

    }
}