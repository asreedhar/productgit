﻿using System;
using System.Configuration;
using AmazingCloudSearch.Contract;

namespace AmazingCloudSearch.MaxInterface
{
    public static class CloudSearchFactory<TDocument> where TDocument : ICloudSearchDocument, new()
    {
        /// <summary>
        /// Returns an AmazingCloudSearch instance of TDocument that is pointed at the awsCloudSearchId provided. This should be the
        /// cloudsearch endpoint without the search- or doc- prefix.
        /// E.g., with a search endpoint of search-dev-dealer-search-shh7vyz7qrklbzuhd7v4njwzea.us-east-1.cloudsearch.amazonaws.com
        /// we would here only want the dev-dealer-search-shh7vyz7qrklbzuhd7v4njwzea.us-east-1.cloudsearch.amazonaws.com
        /// </summary>
        /// <param name="awsCloudSearchId"></param>
        /// <param name="apiVersion"></param>
        /// <returns></returns>
        public static CloudSearch<TDocument> CreateCloudSearchConnection(string awsCloudSearchId="", string apiVersion = "2013-01-01")
        {
            if (string.IsNullOrEmpty(awsCloudSearchId))
                awsCloudSearchId = ConfigurationManager.AppSettings["DealerCloudSearch"];
            if(string.IsNullOrEmpty(awsCloudSearchId))
                throw new ArgumentNullException("awsCloudSearchId", "You must provide a cloudsearch id or set it in the app.config");
            
            return new CloudSearch<TDocument>(awsCloudSearchId, apiVersion);
        }
    }
}
