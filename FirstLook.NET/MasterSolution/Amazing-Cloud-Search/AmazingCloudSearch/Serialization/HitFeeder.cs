﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;
using AmazingCloudSearch.Contract;
using AmazingCloudSearch.Contract.Result;
using AmazingCloudSearch.Helper;
using AmazingCloudSearch.Query;
using Newtonsoft.Json;

namespace AmazingCloudSearch.Serialization
{
    internal class HitFeeder<T> where T : ICloudSearchDocument, new()
    {
        readonly JavaScriptSerializer _serializer;
        readonly ConvertArray _convertArray;
        readonly ListProperties<T> _listProperties;

        public HitFeeder()
        {
            _convertArray = new ConvertArray();
            _serializer = new JavaScriptSerializer();
            _listProperties = new ListProperties<T>();
        }

        public void Feed(SearchResult<T> searchResult, dynamic dyHit)
        {
            searchResult.hits.hit = new List<SearchResult<T>.Hit<T>>();

            foreach (var hitDocument in dyHit)
            {
				Dictionary<string, string> jsonHitField = _serializer.Deserialize<Dictionary<string, string>>(hitDocument.fields.ToString());

                T hit = Map(jsonHitField);

                searchResult.hits.hit.Add(new SearchResult<T>.Hit<T> {id = hitDocument.id, data = hit});
            }
        }

        T Map(Dictionary<string, string> data)
        {
            var hit = new T();
	        var properties = GetProperties();
			var props = from p in properties
						let attr = p.GetCustomAttributes(typeof(JsonPropertyAttribute), true)
						where attr.Length == 1
						select new { Property = p, Attribute = attr.First() as JsonPropertyAttribute };

            foreach (var p in props)
            {
	            var name = string.IsNullOrWhiteSpace(p.Attribute.PropertyName) ? p.Property.Name : p.Attribute.PropertyName;
                List<string> newValues = FindField(name, data);

                if (newValues == null)
                {
                    continue;
                }

                if (p.Property.PropertyType == typeof(List<string>))
                {
					p.Property.SetValue(hit, newValues, null);
                }

				else if (p.Property.PropertyType == typeof(List<int?>))
                {
					p.Property.SetValue(hit, _convertArray.StringToIntNull(newValues), null);
                }

				else if (p.Property.PropertyType == typeof(List<int>))
                {
					p.Property.SetValue(hit, _convertArray.StringToInt(newValues), null);
                }

				else if (p.Property.PropertyType == typeof(List<DateTime>))
                {
					p.Property.SetValue(hit, _convertArray.StringToDate(newValues), null);
                }


				else if (p.Property.PropertyType == typeof(string))
                {
					p.Property.SetValue(hit, newValues.FirstOrDefault(), null);
                }

				else if (p.Property.PropertyType == typeof(int?))
                {
					p.Property.SetValue(hit, _convertArray.StringToIntNull(newValues).FirstOrDefault(), null);
                }

				else if (p.Property.PropertyType == typeof(int))
                {
					p.Property.SetValue(hit, _convertArray.StringToInt(newValues).FirstOrDefault(), null);
                }

				else if (p.Property.PropertyType == typeof(DateTime))
                {
					p.Property.SetValue(hit, _convertArray.StringToDate(newValues).FirstOrDefault(), null);
                }
            }

            return hit;
        }

		private IEnumerable<PropertyInfo> GetProperties()
		{
			return _listProperties.List().Where(info => Attribute.IsDefined(info, typeof (IgnoreReturnFieldAttribute)) == false);
		}

        List<string> FindField(string propertyName, Dictionary<string, string> data)
        {
	        return new List<string> {data.FirstOrDefault(d => d.Key == propertyName).Value};
        }
    }
}
