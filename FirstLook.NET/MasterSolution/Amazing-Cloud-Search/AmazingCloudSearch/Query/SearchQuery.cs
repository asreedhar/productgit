using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AmazingCloudSearch.Contract;
using AmazingCloudSearch.Helper;
using AmazingCloudSearch.Query.Boolean;
using AmazingCloudSearch.Query.Facets;
using AmazingCloudSearch.Enum;
using Newtonsoft.Json;

namespace AmazingCloudSearch.Query
{
    public class SearchQuery<T> where T : ICloudSearchDocument, new()
    {        
        public BooleanQuery BooleanQuery { get; set; }

        public string Keyword { get; set; }

        public List<Facet> Facets { get; set; }

        public List<string> Fields { get; set; }

        public int? Start { get; set; }

        public int? Size { get; set; }

        public string PublicSearchQueryString { get; set; }

        public string OrderByField { get; set; }

        public Order Order { get; set; }

        public SearchQuery(bool buildFieldsFromType = true)
        {
            BooleanQuery = new BooleanQuery();
            if (buildFieldsFromType)
            {
	            var propertyList = new ListProperties<T>().GetProperties();
	            var pl = propertyList.Where(info => Attribute.IsDefined(info, typeof(IgnoreReturnFieldAttribute)) == false);
                BuildPropertiesArray(pl.ToList());
            }
        }

        void BuildPropertiesArray(IEnumerable<PropertyInfo> properties)
        {
	        var props = from p in properties
						let attr = p.GetCustomAttributes(typeof(JsonPropertyAttribute), true)
						where attr.Length == 1
						select new { Property = p, Attribute = attr.First() as JsonPropertyAttribute };

	        var li = props.Select(pInfo => string.IsNullOrWhiteSpace(pInfo.Attribute.PropertyName) ? pInfo.Property.Name : pInfo.Attribute.PropertyName).ToList();

	        Fields = li;
        }
    }

	public class IgnoreReturnFieldAttribute : Attribute
	{
	}
}
