using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace FirstLook.CertifiedProgram.WebApplication.Common
{
    public interface IBenefitView
    {
        List<BenefitDto> Benefits { get; set; }
        Repeater BenefitRepeater { get;}
    }
}