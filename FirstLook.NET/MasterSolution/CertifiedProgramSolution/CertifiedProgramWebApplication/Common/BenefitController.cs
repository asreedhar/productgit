using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using FirstLook.CertifiedProgram.DomainModel;

namespace FirstLook.CertifiedProgram.WebApplication.Common
{
    /// <summary>
    /// I use the term "controller" here lightly.  I have factored out all of the common code from the system and dealer
    /// cpo screens to this class.  It is a collection of common behavior.
    /// </summary>
    public class BenefitController
    {
        private readonly IBenefitView _view;

        internal enum SwapTarget
        {
            Before,
            After
        }

        internal BenefitController(IBenefitView benefitView)
        {
            _view = benefitView;
        }

        internal int GetIndexOfBenefitByKey(string key)
        {
            for (int i = 0; i < _view.Benefits.Count; i++)
            {
                BenefitDto dto = _view.Benefits[i];
                if (dto.Key == key)
                    return i;
            }
            return Int32.MinValue;
        }

        internal BenefitDto GetBenefitByKey(string key)
        {
            return _view.Benefits[GetIndexOfBenefitByKey(key)];
        }

        // Get first benefit (according to rank).
        internal BenefitDto GetFirst()
        {
            foreach (BenefitDto benefit in _view.Benefits)
            {
                return benefit;
            }

            return null;
        }

        // Get last benefit (according to rank).
        internal BenefitDto GetLast()
        {
            BenefitDto[] reversed = _view.Benefits.ToArray();
            Array.Reverse(reversed);

            foreach (BenefitDto benefit in reversed)
            {
                return benefit;
            }

            return null;
        }

        // Get first non-deleted benefit (according to rank).
        internal BenefitDto GetFirstNonDeleted()
        {
            foreach (BenefitDto benefit in _view.Benefits)
            {
                if (!benefit.Deleted)
                {
                    return benefit;
                }
            }

            return null;
        }

        internal short GetNextBenefitRank()
        {
            short max = 0;
            foreach (BenefitDto benefit in _view.Benefits)
            {
                if (benefit.Rank > max)
                {
                    max = benefit.Rank;
                }
            }
            return (Int16)(max + 1);
        }

        // Get last non-deleted benefit (according to rank).
        internal BenefitDto GetLastNonDeleted()
        {
            BenefitDto[] reversed = _view.Benefits.ToArray();
            Array.Reverse(reversed);

            foreach (BenefitDto benefit in reversed)
            {
                if (!benefit.Deleted)
                {
                    return benefit;
                }
            }

            return null;
        }

        internal void SwapBenefitRanks(BenefitDto benefitOne, BenefitDto benefitTwo)
        {
            short tempRank = benefitOne.Rank;

            benefitOne.Rank = benefitTwo.Rank;
            benefitTwo.Rank = tempRank;
        }

        // Bind benefits to benefits repeater
        internal void BindBenefits(Repeater repeater, List<BenefitDto> benefits)
        {
            repeater.DataSource = benefits;
            repeater.DataBind();
        }

        internal void AdjustRanks(string key, SwapTarget target)
        {
            // Get the index of the benefit by key.
            int index = GetIndexOfBenefitByKey(key);

            // Swap this benefit and the other.
            int targetIndex;

            if (target == SwapTarget.Before)
            {
                targetIndex = index - 1;
            }
            else if (target == SwapTarget.After)
            {
                targetIndex = index + 1;
            }
            else
            {
                throw new ApplicationException("Invalid target swap.");
            }
            SwapBenefitRanks(_view.Benefits[index], _view.Benefits[targetIndex]);

            // Resort the collection and rebind.
            _view.Benefits.Sort(new BenefitDtoRankSorter());
            BindBenefits(_view.BenefitRepeater, _view.Benefits);
        }

        internal bool BenefitNameExists(string benefitName)
        {
            foreach (BenefitDto benefitDto in _view.Benefits)
            {
                if (benefitName.Trim() == benefitDto.Name.Trim()) 
                    return true;
            }
            return false;
        }

        internal List<BenefitDto> CacheBenefits( IEnumerator<IBenefit> benefitEnumerator)
        {
            List<BenefitDto> benefitDtos = new List<BenefitDto>();

            // Cache benefits
            while( benefitEnumerator.MoveNext() ) 
            {
                IBenefit benefit = benefitEnumerator.Current;

                BenefitDto benefitDto = new BenefitDto();
                benefitDto.Id = benefit.Id;
                benefitDto.IsProgramHighlight = benefit.IsProgramHighlight;
                benefitDto.Name = benefit.Name;
                benefitDto.Rank = benefit.Rank;
                benefitDto.Text = benefit.Text;

                benefitDtos.Add(benefitDto);
            }

            _view.Benefits = benefitDtos;

            return benefitDtos;
        }

        internal IEnumerator<IBenefit> GetEnumerator( OwnerCertifiedProgramBenefitCollection collection )
        {
            foreach (OwnerCertifiedProgramBenefit item in collection)
            {
                yield return item;
            }
        }

        internal IEnumerator<IBenefit> GetEnumerator( CertifiedProgramBenefitCollection collection )
        {
            foreach (CertifiedProgramBenefit item in collection)
            {
                yield return item;
            }            
        }
    }
}