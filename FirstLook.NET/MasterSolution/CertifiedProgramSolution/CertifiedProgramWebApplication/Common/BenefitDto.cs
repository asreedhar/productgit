using System;
using System.Collections.Generic;

namespace FirstLook.CertifiedProgram.WebApplication.Common
{
    [Serializable]
    public class BenefitDto
    {
        private int? _id;
        private string _name;
        private string _text;
        private bool _isProgramHighlight;
        private Int16 _rank;
        private bool _deleted;

        public int? Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public bool IsProgramHighlight
        {
            get { return _isProgramHighlight; }
            set { _isProgramHighlight = value; }
        }

        public short Rank
        {
            get { return _rank; }
            set { _rank = value; }
        }

        public bool Deleted
        {
            get { return _deleted; }
            set { _deleted = value; }
        }

        public string Key
        {
            get
            {
                string id = String.Empty;
                if (_id.HasValue) id = _id.Value.ToString();

                return id + ":" + _name;
            }
        }
    }

    public class BenefitDtoRankSorter : IComparer<BenefitDto>
    {
        public int Compare(BenefitDto x, BenefitDto y)
        {
            return x.Rank.CompareTo(y.Rank);
        }
    }
}