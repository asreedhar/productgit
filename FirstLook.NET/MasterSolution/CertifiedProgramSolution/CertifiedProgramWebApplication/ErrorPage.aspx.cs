using System;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.SoftwareSystem;

public partial class ErrorPage : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // get status code

        int status = 500;

        string statusText = Request.QueryString["Error"];

        if (!string.IsNullOrEmpty(statusText))
        {
            try
            {
                status = Int32Helper.ToInt32(statusText);
            }
            catch
            {
                // ignore the error (we have a sensible default)
            }
        }

        // set status title/description

        ErrorTitle.InnerText = GetLocalResourceObject(string.Format("{0}.title", status)).ToString();

        ErrorDescription.InnerText = GetLocalResourceObject(string.Format("{0}.description", status)).ToString();

        // set text

        //string configuration = WebConfigurationManager.AppSettings[AccessControlHttpModule.Key];

        //string referrer = String.Empty;

        //if (Request.UrlReferrer != null)
        //{
        //    referrer = Request.UrlReferrer.AbsolutePath;
        //    referrer = referrer.Substring(referrer.IndexOf('/', 1) + 1);
        //}

        ErrorAction action = ErrorAction.Close;

        //if (!string.IsNullOrEmpty(referrer))
        //    action = WebResourceRegistryCache.CacheCopy(configuration).OnError(referrer);

        if (status == 410)
        {
            action = ErrorAction.Page;
        }

        string target = string.Empty;

        if (status == 410)
        {
            target = "~/Default.aspx";
        }
        
        //if (!action.Equals(ErrorAction.Close))
        //    target = WebResourceRegistryCache.CacheCopy(configuration).OnErrorTarget(referrer);
        
        switch (action)
        {
            case ErrorAction.Close:
                ErrorClose.Visible = true;
                break;
            case ErrorAction.Home:
                ErrorHome.Visible = true;
                HomeLink.NavigateUrl = ResolveClientUrl(target);
                break;
            case ErrorAction.Page:
                ErrorTarget.Visible = true;
                PageLink.NavigateUrl = ResolveClientUrl(target);
                break;
        }

        // set the status code for the browser

        Response.StatusCode = status;
    }

    protected void Page_Error(object sender, EventArgs e)
    {
        Server.ClearError(); // if the error page is in error stop the recursive call to self
    }
}
