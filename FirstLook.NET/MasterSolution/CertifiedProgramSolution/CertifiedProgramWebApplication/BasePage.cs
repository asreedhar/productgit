namespace FirstLook.CertifiedProgram.WebApplication
{
    public class BasePage : System.Web.UI.Page
    {
        protected T CacheGet<T>(string key)
        {
            object value = ViewState[key];
            if (value == null)
                return default(T);

            return (T) value;
        }

        protected void CacheSet<T>(string key, T value)
        {
            ViewState[key] = value;
        }

        protected void CacheReset(string key)
        {
            ViewState.Remove(key);
        }


    }
}
