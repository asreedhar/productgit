using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.CertifiedProgram.WebApplication.Dealer.Pages
{
    public partial class CPOAdmin : MasterPage
    {
        protected string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        protected string StartingNodeUrl
        {
            get
            {
                return Request.Path;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(
                      HttpContext.Current.User.Identity.Name,
                      SoftwareSystemComponentStateFacade.DealerComponentToken);

            if (state != null)
            {
                HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

                BusinessUnit dealer = state.Dealer.GetValue();

                HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);
            }
        }

        protected void PreferencesSiteMapDataSource_DataBound(object sender, MenuEventArgs e)
        {
            MenuItem item = e.Item;

            if (item.NavigateUrl == string.Format("{0}?oh={1}", StartingNodeUrl, OwnerHandle))
            {
                item.Selected = true;
            }
        }
    }
}
