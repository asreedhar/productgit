using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;
using FirstLook.CertifiedProgram.DomainModel;
using FirstLook.CertifiedProgram.WebApplication.Common;

namespace FirstLook.CertifiedProgram.WebApplication.Dealer.Pages
{
    public partial class DealerCertifiedPrograms : BasePage, IBenefitView
    {
        private const string CachedBenefits = "benefits";
        private readonly BenefitController _benefitController;

        #region Properties

        // DTO package used to store benefit changes before 'Save'
        public List<BenefitDto> Benefits
        {
            get
            {
                // Only pull from domain if we have nothing in the cache.
                List<BenefitDto> benefits = CacheGet<List<BenefitDto>>(CachedBenefits) ?? LoadBenefitsFromDomain();

                // If nothing was returned from the cache or the domain, create an empty list.
                if (benefits == null)
                {
                    benefits = new List<BenefitDto>();

                    // Cache the benefits.
                    Benefits = benefits;
                }

                return benefits;
            }
            set
            {
                CacheSet(CachedBenefits, value);
            }
        }

        public Repeater BenefitRepeater
        {
            get { return CertifiedProgramBenefitsRepeater; }
        }

        private static IList<CertifiedProgramInfo> GetFilteredByParticipation(string ownerHandle)
        {
            var collection = CertifiedProgramInfoCollection.GetCertifiedProgramInfoCollection(CertifiedProgramType.Dealer);
            var filteredCollection = collection.Where(
				x => OwnerCertifiedProgram.GetOwnerCertifiedProgramAssociation(x.Id, ownerHandle).Association != OwnerCertifiedProgram.OwnerProgramAssociationType.None
				&& x.Active)
                .ToList();
            
            return filteredCollection;
        }

        private string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        private string CurrentProgramId
        {
            get
            {
                return SelectCPODropDownList.SelectedValue;
            }
        }

		protected OwnerCertifiedProgram.OwnerProgramAssociationType ProgramAssociation
        {
            get { return OwnerCertifiedProgram.GetOwnerCertifiedProgramAssociation(int.Parse(CurrentProgramId), OwnerHandle).Association; }
        }

        #endregion

        #region Construction and Finalization

        public DealerCertifiedPrograms()
        {
            _benefitController = new BenefitController(this);                        
        }

        #endregion

        #region Event Handlers

        protected void SelectCPODropDownList_Changed(object sender, EventArgs e)
        {
            ParticipateProgramCheckbox.Enabled = false;
            ParticipateProgramCheckbox.Checked = false;
            CertifiedProgramBenefitsRepeater.Visible = false;
            FormButtonsFieldset.Visible = false;
            CacheReset(CachedBenefits);

            if (((DropDownList)sender).SelectedValue != string.Empty)
            {
                ParticipateProgramCheckbox.Enabled = true;
				// for a dealer progam, an explicit association means customizations (aka OwnerCertifiedProgram) exist

	            if (ProgramAssociation == OwnerCertifiedProgram.OwnerProgramAssociationType.Explicit)
	            {
		            ParticipateProgramCheckbox.Checked = true;
					FormButtonsFieldset.Visible = true;
		            CertifiedProgramBenefitsRepeater.Visible = true;
	            }
				else if (ProgramAssociation == OwnerCertifiedProgram.OwnerProgramAssociationType.Implicit)
				{
					ParticipateProgramCheckbox.Checked = false;
					FormButtonsFieldset.Visible = true;
					CertifiedProgramBenefitsRepeater.Visible = true;
				}

            }
        }

        protected void ParticipateProgramCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                CertifiedProgramBenefitsRepeater.Visible = true;
                FormButtonsFieldset.Visible = true;
            }
            else
            {
                CertifiedProgramBenefitsRepeater.Visible = false;
            }
        }

        protected void ProgramBenefitsRepeater_PreRender(object sender, EventArgs e)
        {
            // Only bind if not already bound.
            if( CertifiedProgramBenefitsRepeater.DataSource != null ) 
                return;

            if (!string.IsNullOrEmpty(CurrentProgramId))
            {
                CertifiedProgramBenefitsRepeater.Visible = true;
                _benefitController.BindBenefits(CertifiedProgramBenefitsRepeater, Benefits);
            }
        }

        protected void ProgramBenefitsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            // Get the command name and the name of the benefit.
            string commandName = e.CommandName;
            string key = (string) e.CommandArgument;

            // Move up / down
            if( commandName == "MoveUp")
            {
                // Move the benefit up
                _benefitController.AdjustRanks(key, BenefitController.SwapTarget.Before);
            }
            else if(commandName == "MoveDown")
            {
                _benefitController.AdjustRanks(key, BenefitController.SwapTarget.After);
            }

        }

        // Append button IDs, hide up/down arrows for first/last, assign event handlers
        protected void ProgramBenefitsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem == null) return;

            BenefitDto benefitDto = (BenefitDto)e.Item.DataItem;

            ImageButton upButton = ((ImageButton)e.Item.FindControl("MoveBenefitUpButton"));
            ImageButton downButton = ((ImageButton)e.Item.FindControl("MoveBenefitDownButton"));

            upButton.CommandArgument = benefitDto.Key;
            downButton.CommandArgument = benefitDto.Key;

            LinkButton highlightCkbButton = ((LinkButton)e.Item.FindControl("BenefitHighlightCkb"));
            highlightCkbButton.CommandArgument = benefitDto.Key;
            highlightCkbButton.CssClass = benefitDto.IsProgramHighlight ? "checked" : "unchecked";
            highlightCkbButton.Click += BenefitHighlightCkb_CheckedChanged;

            // Is it the first?
            if (benefitDto == _benefitController.GetFirst())
            {
                e.Item.FindControl(upButton.ID).Visible = false;
            }
            // Is it the last?
            if (benefitDto == _benefitController.GetLast())
            {
                e.Item.FindControl(downButton.ID).Visible = false;
            }
        }

        // Toggle benefit highlight
        protected void BenefitHighlightCkb_CheckedChanged(object sender, EventArgs e)
        {
            LinkButton ckbButton = (LinkButton)sender;

            bool wasChecked = !ckbButton.CssClass.Contains("unchecked");

            // Extract key
            string key = ckbButton.CommandArgument;

            // Get benefit.
            BenefitDto benefit = _benefitController.GetBenefitByKey(key);
            benefit.IsProgramHighlight = wasChecked ? false : true;

            Repeater cpbRepeater = (Repeater)ckbButton.BindingContainer.NamingContainer;
            _benefitController.BindBenefits(cpbRepeater, Benefits);
        }

        // Add child span to <a> tag for CSS purposes
        protected void UpdateProgramButton_PreRender(object sender, EventArgs e)
        {
            if (!((LinkButton)sender).Text.Contains("/span"))
            {
                ((LinkButton)sender).Text = "<span>" + ((LinkButton)sender).Text + "</span>";
            }
        }

        // Save program
        protected void UpdateProgramButton_OnClick(object sender, EventArgs e)
        {
            int programId = int.Parse(SelectCPODropDownList.SelectedValue);

            OwnerCertifiedProgram program;

            if (ProgramAssociation == OwnerCertifiedProgram.OwnerProgramAssociationType.Explicit)
            {
                program = OwnerCertifiedProgram.GetOwnerCertifiedProgram(programId, OwnerHandle);
                if (!ParticipateProgramCheckbox.Checked)
                {
                    // Disassociate (Delete OwnerCertifiedProgram/Benefits)
                    program.Delete();
                    program.Save();
                    CacheReset(CachedBenefits);
                    CertifiedProgramBenefitsRepeater.Visible = false;
                    FormButtonsFieldset.Visible = false;
                    return;
                }
            }
            else
            {
                program = OwnerCertifiedProgram.NewOwnerCertifiedProgram();
                program.LoadAssociation(programId, OwnerHandle);
            }

            #region Save benefits -- those with null ids are the ones which were inserted.

            foreach (BenefitDto benefit in Benefits)
            {
                string name = benefit.Name;
                OwnerCertifiedProgramBenefit programBenefit = program.OwnerCertifiedProgramBenefits.FindAccordingToName(name);
                programBenefit.Rank = benefit.Rank;
                programBenefit.IsProgramHighlight = benefit.IsProgramHighlight;
            }

            #endregion

            program.Save();

            // Clear the cache.
            CacheReset(CachedBenefits);
        }

        protected override void OnPreRender(EventArgs e)
        {
            var items = GetFilteredByParticipation(OwnerHandle);
            ProgramParticipationEmpty.Visible = (items.Count == 0);

            ProgramParticipationDataList.DataSource = items;
            ProgramParticipationDataList.DataBind();
        }

        #endregion

        // Get benefits from domain
        private List<BenefitDto> LoadBenefitsFromDomain()
        {
            // Locate or create the program.
            OwnerCertifiedProgram program = LocateOrCreateProgram();

            // Load and cache benefits
            IEnumerator<IBenefit> enumerator = _benefitController.GetEnumerator(program.OwnerCertifiedProgramBenefits);
            return _benefitController.CacheBenefits(enumerator);
        }

        /// <summary>
        /// If a program already exists, return it.  Otherwise, create one.
        /// </summary>
        /// <returns></returns>
        private OwnerCertifiedProgram LocateOrCreateProgram()
        {
            OwnerCertifiedProgram program;
            if (ProgramAssociation == OwnerCertifiedProgram.OwnerProgramAssociationType.Explicit)
            {
                program = OwnerCertifiedProgram.GetOwnerCertifiedProgram(int.Parse(CurrentProgramId), OwnerHandle);
            }
            else
            {
                program = OwnerCertifiedProgram.NewOwnerCertifiedProgram();
                program.LoadAssociation(int.Parse(CurrentProgramId), OwnerHandle);
            }
            return program;
        }

    }
}