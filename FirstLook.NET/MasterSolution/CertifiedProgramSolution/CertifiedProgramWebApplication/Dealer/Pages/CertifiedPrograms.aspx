<%@ Page Language="C#" MasterPageFile="~/Dealer/Pages/CPODealer.Master" Title="Manufacturer Certified Program Benefit Programs" AutoEventWireup="True" CodeBehind="CertifiedPrograms.aspx.cs" Inherits="FirstLook.CertifiedProgram.WebApplication.Dealer.Pages.CertifiedPrograms" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="ContentPlaceHolder1" runat="server">
    <div id="TextFragmentFormView" class="form_view">
        <asp:ObjectDataSource ID="CertifiedProgramInfoCollectionDataSource" runat="server" TypeName="FirstLook.CertifiedProgram.DomainModel.CertifiedProgramInfoCollection+DataSource"
            SelectMethod="Select">
                <SelectParameters>
                    <%-- The following value corresponds to the CertifiedProgramType enum --%>
                    <asp:Parameter Name="ProgramTypeVal" Type="Int32" DefaultValue="1"/>
                    <asp:Parameter Name="ActiveOnly" Type="Boolean" DefaultValue="true"/>
                </SelectParameters>
        </asp:ObjectDataSource>
        <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList"></asp:BulletedList>
	    <div class="participation">
	        <h2>Manufacturer Certified Program Participation</h2>
	        <asp:Panel ID="ProgramParticipationEmpty" CssClass="participation-item" runat="server">Not Currently Participating</asp:Panel>
	        <div>
	            <asp:DataList ID="ProgramParticipationDataList" RepeatColumns="2" RepeatDirection="Horizontal" runat="server">
	                <ItemTemplate>
	                    <div class="participation-item">
	                        <a id="participate-<%# Eval("id") %>" href="#"><%# Eval("Name") %></a>
	                    </div>
	                </ItemTemplate>
	            </asp:DataList>
	        </div>
	    </div>
	    <fieldset class="snippet_form vform">
		    <legend style="padding-left:0px;margin-left:0px;">Select Manufacturer Certified Program</legend>
		    <p class="desc">
			    To associate your dealership with a Certified Pre-Owned Program, simply select the desired program from the <strong>Select Manufacturer Certified Program</strong> drop-down list below and click the <strong>Participate in Program</strong> checkbox.<br /><br />This will display a list of the associated benefits which you can choose to highlight (with the <strong>Highlight</strong> column checkboxes) or reorder with the <strong>Adjust Rank</strong> arrows. When you have configured the benefits  to your satisfaction click the <strong>Save</strong> button to store your changes.
		    </p>
		    <p>
			    <label for="<%= SelectManufacturerCPODropDownList.ClientID %>">Select Manufacturer Certified Program:</label>
			    <asp:DropDownList ID="SelectManufacturerCPODropDownList" runat="server" OnSelectedIndexChanged="SelectManufacturerCPODropDownList_Changed" AppendDataBoundItems="true" DataSourceID="CertifiedProgramInfoCollectionDataSource" DataTextField="Name" DataValueField="Id"  AutoPostBack="true">
			        <asp:ListItem Text="Please Select a Certified Program..." Value=""></asp:ListItem>
			    </asp:DropDownList>
                <asp:CheckBox id="ParticipateProgramCheckbox" Enabled="false" runat="server" CssClass="checkbox" OnCheckedChanged="ParticipateProgramCheckbox_CheckedChanged" AutoPostBack="true" />
                <label class="checkboxLabel" for="<%= ParticipateProgramCheckbox.ClientID %>">Participate in Program</label>
		    </p>
	    </fieldset>
	    <div class="dataGridDiv">
	        <asp:Repeater ID="CertifiedProgramBenefitsRepeater" runat="server" Visible="<%# ParticipateProgramCheckbox.Checked %>" 
	            OnPreRender="ProgramBenefitsRepeater_PreRender" 
	            OnItemDataBound="ProgramBenefitsRepeater_ItemDataBound"
	            OnItemCommand="ProgramBenefitsRepeater_ItemCommand">
                
                <HeaderTemplate>
                    <table id="ProgramsListTable" style="width:100%;border-collapse:collapse;">
                        <tr>
                            <th style="width:30%;">Name</th>
                            <th style="width:50%;">Text</th>
                            <th style="text-align:center;width:10%;">Highlight</th>
                            <th style="text-align:center;width:10%;white-space:nowrap;" colspan="2">Adjust Rank</th>
                        </tr>
                </HeaderTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
                <AlternatingItemTemplate>
                    <tr class="benefitRow altRow">
                        <td><%# Eval("Name") %></td>
                        <td><%# Eval("Text") %></td>
                        <td style="text-align:center;vertical-align:middle;">
                            <asp:LinkButton CausesValidation="false" ID="BenefitHighlightCkb" 
                                OnClick="BenefitHighlightCkb_CheckedChanged" runat="server"></asp:LinkButton></td>
                        <td style="text-align:center;vertical-align:middle;">
                            <asp:ImageButton CausesValidation="false" ID="MoveBenefitUpButton" ToolTip="Move up" runat="server" AlternateText="^" 
                                ImageUrl="~/App_Themes/Leopard/Images/arrow_Up.gif" CommandName="MoveUp"/></td>
					    <td style="text-align:center;vertical-align:middle;">
					        <asp:ImageButton CausesValidation="false" ID="MoveBenefitDownButton"  ToolTip="Move down" runat="server" AlternateText="v" 
					            ImageUrl="~/App_Themes/Leopard/Images/arrow_Down.gif" CommandName="MoveDown" /></td>
					</tr>
                </AlternatingItemTemplate>
                <ItemTemplate>
                    <tr class="benefitRow">
                        <td><%# Eval("Name") %></td>
                        <td><%# Eval("Text") %></td>
                        <td style="text-align:center;vertical-align:middle;">
                            <asp:LinkButton CausesValidation="false" ID="BenefitHighlightCkb"
                                OnClick="BenefitHighlightCkb_CheckedChanged" runat="server"></asp:LinkButton></td>
                        <td style="text-align:center;vertical-align:middle;">
                            <asp:ImageButton CausesValidation="false" ID="MoveBenefitUpButton" ToolTip="Move up" runat="server" AlternateText="^" 
                                ImageUrl="~/App_Themes/Leopard/Images/arrow_Up.gif" CommandName="MoveUp" /></td>
					    <td style="text-align:center;vertical-align:middle;">
					        <asp:ImageButton CausesValidation="false" ID="MoveBenefitDownButton" ToolTip="Move down" runat="server" AlternateText="v" 
					            ImageUrl="~/App_Themes/Leopard/Images/arrow_Down.gif" CommandName="MoveDown" /></td>
					</tr>
                </ItemTemplate>
            </asp:Repeater>
	        <fieldset class="formButtons" id="FormButtonsFieldset" runat="server" visible="false">
                <asp:LinkButton ID="UpdateProgramButton" runat="server" Text="Save" CssClass="button saveButton" OnPreRender="UpdateProgramButton_PreRender" OnClick="UpdateProgramButton_OnClick" />
                <a href="<%= Request.RawUrl %>" class="button cancelButton"><span>Cancel</span></a>
	        </fieldset>
	    </div>
    </div>
    <div id="ValidationErrorDialog" class="ui-dialog" style="top:300px;left:310px;height:150px;width:290px;display:none;">
	    <fieldset style="display:none;">
		    <input type="hidden" name="ValidationErrorDialog$ClientState" id="ValidationErrorDialog_ClientState" value="{ hidden: true }" />
	    </fieldset>
	    <div class="ui-dialog-container">
		    <div id="ValidationErrorDialog_TitleBar" class="ui-dialog-titlebar">
			    <span class="ui-dialog-title">Valiation Error</span>
		    </div>
		    <div class="ui-dialog-content">
			    <div id="ValidationErrorDialog_ItemPanel" class="ui-dialog-content-wrapper"></div>
		    </div>
	    </div>
	    <div id="ValidationErrorDialog_FooterPanel" class="ui-dialog-buttonpane">
		    <ul>
			    <li>
				    <button type="submit" name="ValidationErrorDialog$FooterPanel$button_0" value="Ok" id="ValidationErrorDialog_FooterPanel_button_0">Ok</button>
			    </li>
		    </ul>
	    </div>
	    <div class="ui-resizable-n ui-resizable-handle"></div>
	    <div class="ui-resizable-s ui-resizable-handle"></div>
	    <div class="ui-resizable-e ui-resizable-handle"></div>
	    <div class="ui-resizable-w ui-resizable-handle"></div>
	    <div class="ui-resizable-ne ui-resizable-handle"></div>
	    <div class="ui-resizable-se ui-resizable-handle"></div>
	    <div class="ui-resizable-sw ui-resizable-handle"></div>
	    <div class="ui-resizable-nw ui-resizable-handle"></div>
    </div>
</asp:Content>
