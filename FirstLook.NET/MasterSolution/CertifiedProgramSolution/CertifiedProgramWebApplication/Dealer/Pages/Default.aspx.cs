using System;
using System.Web;
using System.Web.UI;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.CertifiedProgram.WebApplication.Dealer.Pages
{
    public partial class Default : Page
    {
        private const string RedirectUrl = "/CertifiedProgram/Dealer/Pages/CertifiedPrograms.aspx";

        private string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(OwnerHandle))
            {
                EnterDealerPage(OwnerHandle);
            }

            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(
                      HttpContext.Current.User.Identity.Name,
                      SoftwareSystemComponentStateFacade.DealerComponentToken);

            bool forbidden = true;

            if (state != null)
            {

                HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

                BusinessUnit dealer = state.Dealer.GetValue();

                HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);

                if (dealer != null)
                {
                    Owner owner = Owner.GetOwner(dealer.Id);

                    if (owner != null)
                    {
                        forbidden = false;
                        EnterDealerPage(owner.Handle);
                    }
                }
            }

            if (forbidden)
            {
                Response.StatusCode = 403;
                Response.Status = "403 Forbidden";
            }
        }       

        protected void EnterDealerPage(string ownerHandle)
        {
            Response.Redirect(string.Format("{0}?oh={1}", RedirectUrl, ownerHandle), true);
        }
    }
}
