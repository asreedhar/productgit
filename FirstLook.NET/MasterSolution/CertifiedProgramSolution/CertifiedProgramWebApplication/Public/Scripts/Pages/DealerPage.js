﻿

$(document).ready(function(){
    
    $("div.participation a").click(function(){
        var id = $(this).attr("id");
        var expression = /participate-(\d+)/;
        
        if(expression.test(id)){
            var match = expression.exec(id);
            var programID = parseInt(match[1]);
            $("fieldset.snippet_form select").val(programID).trigger("change");
        }
        
        return false;
    });

});