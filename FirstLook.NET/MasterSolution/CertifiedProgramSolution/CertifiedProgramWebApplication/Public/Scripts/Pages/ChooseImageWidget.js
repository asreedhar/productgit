﻿
var ChooseImageWidget = function(widgetId, showWidgetLinkId, imagesBasePath) {
    this.OnReady(widgetId, showWidgetLinkId, imagesBasePath);
};

ChooseImageWidget.prototype = {
    WidgetId: "", WidgetLinkId: "", ImagesDir: "",
    OnReady: function(widgetId, showWidgetLinkId, imagesBasePath) {
        this.WidgetId = widgetId;
        this.WidgetLinkId = showWidgetLinkId;
        this.ImagesDir = imagesBasePath;
        
        var widget = $("#" + this.WidgetId);
        $("#" + this.WidgetLinkId).bind("click", { instance: this }, this.ShowWidget);
        $(widget).find("a").bind("click", { instance: this }, this.UpdateImage);
        
        // If selected image hidden field has value, use that for display image
        if ($(widget).siblings("input[type=hidden]:first").val() != "") {
            $(widget).siblings("img").attr("src", this.ImagesDir + $(widget).siblings("input[type=hidden]:first").val());
        }
    },
    ShowWidget: function(event) {
        var instance = event.data.instance;
        $("#" + instance.WidgetLinkId).hide();
        $("#" + instance.WidgetId).show();
        return false;
    },
    UpdateImage: function(event) {
        var instance = event.data.instance;
        var selectedFileName = event.target.name;
        var widget = $("#" + instance.WidgetId);
        $(widget).siblings(".imageWidgetSelectedImage:first").attr("src", instance.ImagesDir + selectedFileName);
        $(widget).siblings("input[type=hidden]:first").val(selectedFileName);
        $(widget).find("div").scrollTop(0);
        $(widget).hide();
        $("#" + instance.WidgetLinkId).show();
        return false;
    }
};

var CPOLogoWidget;
$(document).ready(function() {
    // Pass in the block element ID containing the widget,
    // element ID to show the widget and image base path.
	CPOLogoWidget = new ChooseImageWidget("SelectLogoWidget", "CPOLogoWidgetLink", "../../Public/Images/CPOLogos/");
});