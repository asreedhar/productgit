<%@ Page Language="C#" AutoEventWireup="true" Inherits="ErrorPage" Codebehind="ErrorPage.aspx.cs" Theme="Leopard" %>
<%@ Register Assembly="FirstLook.Common.WebControls" Namespace="FirstLook.Common.WebControls.UI" TagPrefix="cwc" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Error Page</title>
    <link href="Public/Css/Standard.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    
        <asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" LoadScriptsBeforeUI="false" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
            </Scripts>
        </asp:ScriptManager>
    
        <div id="doc3">
			<div id="hd">
			<asp:SiteMapDataSource ID="ApplicationSiteMapDataSource" runat="server" ShowStartingNode="false" SiteMapProvider="ApplicationSiteMap" />	
			<asp:Menu id="application_menu_container" runat="server" CssClass="smallCaps blue" Orientation="Horizontal" DataSourceID="ApplicationSiteMapDataSource" />
			<asp:SiteMapDataSource ID="MemberSiteMapDataSource" runat="server" ShowStartingNode="false" SiteMapProvider="MemberSiteMap" />
			<asp:Menu ID="member_menu_container" runat="server" CssClass="ltGrey" Orientation="Horizontal" DataSourceID="MemberSiteMapDataSource" />
			</div>		
            <div id="bd">
                <h2 runat="server" id="ErrorTitle"></h2>
                <p runat="server" id="ErrorDescription"></p>
                <p runat="server" id="ErrorClose" visible="false">If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665. Click <a href="javascript:window.close()">here</a> to close the window.</p>
                <p runat="server" id="ErrorTarget" visible="false">You can <asp:HyperLink runat="server" ID="PageLink">enter the application</asp:HyperLink> if you like. If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.</p>
                <p runat="server" id="ErrorHome" visible="false">You can <asp:HyperLink runat="server" ID="HomeLink">return home</asp:HyperLink> if you like. If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.</p>
            </div>
            <cwc:PageFooter ID="SiteFooter" runat="server" />
        </div>
    </form>
</body>
</html>
