using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace FirstLook.CertifiedProgram.WebApplication.Admin
{
    public partial class CPOAdmin1 : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            string pageName = Request.AppRelativeCurrentExecutionFilePath;

            if (pageName.Contains("Carfax/SystemAccount.aspx") ||
                pageName.Contains("AutoCheck/ServiceAccount.aspx") ||
                pageName.Contains("AutoCheck/SystemAccount.aspx") ||
                pageName.Contains("VehicleHistoryReportAdminHelp.aspx"))
            {
                if (pageName.Contains("SystemAccount.aspx"))
                {
                    DealerNameLabel.Text = "System Account";
                }
                else
                {
                    DealerNameLabel.Text = "Service Account";
                }

                //CarfaxAccountLink.Enabled = false;
                //CarfaxPermissionsLink.Enabled = false;
                //CarfaxReportPreferenceLink_I.Enabled = false;
                //CarfaxReportPreferenceLink_A.Enabled = false;

                //AutoCheckAccountLink.Enabled = false;
                //AutoCheckPermissionsLink.Enabled = false;
            }
        }
    }
}
