using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.AccessControl;
using System.Web.UI;
using System.Web.UI.WebControls;

using FirstLook.CertifiedProgram.DomainModel;
using FirstLook.CertifiedProgram.WebApplication.Common;
using FirstLook.Common.Core.Extensions;

namespace FirstLook.CertifiedProgram.WebApplication.Admin.Pages
{
    public partial class ManageDealerCertifiedProgram : BasePage, IBenefitView
    {
        private const string CachedBenefits = "benefits";
        private readonly BenefitController _benefitController;

        #region Properties

        public List<string> LogoFileNames = new List<string>();

        // DTO package used to store benefit changes before 'Save'
        public List<BenefitDto> Benefits
        {
            get
            {
                // Only pull from domain if we have nothing in the cache.
                List<BenefitDto> benefits = CacheGet<List<BenefitDto>>(CachedBenefits) ?? LoadBenefitsFromDomain();

                // If nothing was returned from the cache or the domain, create an empty list.
                if( benefits == null )
                {
                    benefits = new List<BenefitDto>();

                    // Cache the benefits.
                    Benefits = benefits;
                }

                return benefits;
            }
            set
            {
                CacheSet(CachedBenefits, value);
            }
        }


        public Repeater BenefitRepeater
        {
            get { return (Repeater) ProgramFormView.FindControl("CertifiedProgramBenefitsRepeater"); }
        }

        #endregion

        #region Construction and Finalization

        public ManageDealerCertifiedProgram()
        {
            _benefitController = new BenefitController(this);
        }

        #endregion

        #region Event Handlers

        // Show Insert FormView
        protected void AddProgramButton_Click(object sender, EventArgs e)
        {
            CpoProgramsListPanel.Visible = false;
            ProgramFormView.Visible = true;
            ProgramFormView.ChangeMode(FormViewMode.Insert);
            DefaultHelpText.Visible = false;
            InsertHelpText.Visible = true;
        }

        // Show Update FormView
        protected void EditProgramLinkButton_Click(object sender, EventArgs e)
        {
            string id = ((LinkButton)sender).CommandArgument;
            CpoProgramsListPanel.Visible = false;
            ProgramFormView.Visible = true;
            CurrentProgramId.Value = id;
            _benefitController.BindBenefits((Repeater)ProgramFormView.FindControl("CertifiedProgramBenefitsRepeater"), Benefits);
            DefaultHelpText.Visible = false;
            EditHelpText.Visible = true;
        }

        // Toggle Active state of program (from list view)
        protected void ProgramActiveCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            RepeaterItem ri = (RepeaterItem)((Control)sender).BindingContainer;
            string id = ((HiddenField)(ri.FindControl("ProgramId"))).Value;
            int pid = Int32.Parse(id);
            DomainModel.CertifiedProgram program = DomainModel.CertifiedProgram.GetCertifiedProgram(pid);
			CurrentProgramBusinessUnitId.Value = program.BusinessUnitId.ToString();
            program.Active = ((CheckBox)sender).Checked;
            program.Save();
            Response.Redirect(Request.CurrentExecutionFilePath);
        }

        // Handle Insert of new program
        protected void CertifiedProgramDataSource_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            CpoProgramsListPanel.Visible = false;
            ProgramFormView.Visible = true;
            CurrentProgramId.Value = e.ReturnValue.ToString();
        }

        // Handle Update of new program
        protected void CertifiedProgramDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            Response.Redirect(Request.CurrentExecutionFilePath);
        }

        // Load benefits for current program
        protected void CertifiedProgramBenefitsRepeater_Load(object sender, EventArgs e)
        {
            Repeater cpbRepeater = ((Repeater)sender);
            _benefitController.BindBenefits(cpbRepeater, Benefits);
        }

        // Set up benefit repeater ineractions
        protected void ProgramBenefitsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem == null) return;

            BenefitDto benefitDto = (BenefitDto)e.Item.DataItem;

            // hide row if marked for delete
            if (benefitDto.Deleted)
            {
                e.Item.Visible = false;
            }
            else
            {
                ImageButton upButton = ((ImageButton)e.Item.FindControl("MoveBenefitUpButton"));
                ImageButton downButton = ((ImageButton)e.Item.FindControl("MoveBenefitDownButton"));
                upButton.CommandArgument = benefitDto.Key;
                downButton.CommandArgument = benefitDto.Key;

                upButton.ID += "_" + e.Item.ItemIndex;
                downButton.ID += "_" + e.Item.ItemIndex;
                upButton.Click += MoveBenefitUpButton_Clicked;
                downButton.Click += MoveBenefitDownButton_Clicked;

                LinkButton highlightCkbButton = ((LinkButton)e.Item.FindControl("BenefitHighlightCkb"));
                highlightCkbButton.CommandArgument = benefitDto.Key;
                highlightCkbButton.CssClass = benefitDto.IsProgramHighlight ? "checked" : "unchecked";
                highlightCkbButton.Click += BenefitHighlightCkb_CheckedChanged;

                // May want to add a CssClass to the row if the benefit is already marked for deletion, similar to how highlights are done.
                ImageButton deleteButton = (ImageButton)e.Item.FindControl("DeleteBenefit");
                deleteButton.CommandArgument = benefitDto.Key;
                deleteButton.Click += DeleteBenefit_OnClick;
                deleteButton.ID += "_" + e.Item.ItemIndex;

                // Edit benefit link.
                LinkButton editButton = (LinkButton)e.Item.FindControl("EditBenefitLink");
                editButton.CommandArgument = benefitDto.Key;

                // Is it the first?
                if (benefitDto == _benefitController.GetFirstNonDeleted())
                {
                    e.Item.FindControl(upButton.ID).Visible = false;
                }
                // Is it the last?
                if (benefitDto == _benefitController.GetLastNonDeleted())
                {
                    e.Item.FindControl(downButton.ID).Visible = false;
                }
            }
        }

        // Set up edit of benefit
        protected void EditBenefitLink_Click(object sender, EventArgs e)
        {
            Button insertBtn = (Button)ProgramFormView.FindControl("InsertBenefitButton");
            Button updateBtn = (Button)ProgramFormView.FindControl("UpdateBenefitButton");
            TextBox nameTextBox = (TextBox)ProgramFormView.FindControl("ProgramBenefitNameTextBox");
            TextBox textTextBox = (TextBox)ProgramFormView.FindControl("ProgramBenefitTextTextBox");

            // Extract key
            string key = ((LinkButton)sender).CommandArgument;

            insertBtn.Visible = false;
            updateBtn.Visible = true;

            foreach (BenefitDto benefitDto in Benefits)
            {
                if (benefitDto.Key == key)
                {
                    nameTextBox.Text = benefitDto.Name;
                    nameTextBox.Enabled = false;
                    textTextBox.Text = benefitDto.Text;

                    // Write benefit key to the update button.
                    updateBtn.Attributes.Add("benefitKey", benefitDto.Key);
                    break;
                }
            }
        }

        // Handle save of edited benefit (saves to benefit DTO package)
        protected void UpdateBenefitButton_Click(object sender, EventArgs e)
        {
            TextBox nameTextBox = (TextBox)ProgramFormView.FindControl("ProgramBenefitNameTextBox");
            TextBox textTextBox = (TextBox)ProgramFormView.FindControl("ProgramBenefitTextTextBox");

            // Extract key and find the correct benefit.
            string key = ((Button)sender).Attributes["benefitKey"];
            BenefitDto benefit = _benefitController.GetBenefitByKey(key);

            if (!IsBenefitInputValid(nameTextBox.Text, textTextBox.Text, false)) return;

            benefit.Text = textTextBox.Text;

            Repeater repeater = (Repeater)ProgramFormView.FindControl("CertifiedProgramBenefitsRepeater");
            _benefitController.BindBenefits(repeater, Benefits);
            nameTextBox.Enabled = true;
            nameTextBox.Text = string.Empty;
            textTextBox.Text = string.Empty;

            Button insertBtn = (Button)ProgramFormView.FindControl("InsertBenefitButton");
            Button updateBtn = (Button)ProgramFormView.FindControl("UpdateBenefitButton");
            insertBtn.Visible = true;
            updateBtn.Visible = false;
            updateBtn.Attributes.Remove("benefitKey");
        }

        // Handle Insert of new benefit
        protected void InsertBenefitButton_Click(object sender, EventArgs e)
        {
            TextBox nameTextBox = (TextBox)ProgramFormView.FindControl("ProgramBenefitNameTextBox");
            TextBox textTextBox = (TextBox)ProgramFormView.FindControl("ProgramBenefitTextTextBox");

            if (!IsBenefitInputValid(nameTextBox.Text, textTextBox.Text, true)) return;

            Repeater repeater = ((Repeater)ProgramFormView.FindControl("CertifiedProgramBenefitsRepeater"));

            // Create a BenefitDTO
            BenefitDto dto = new BenefitDto();

            dto.Id = null;
            dto.IsProgramHighlight = false;
            dto.Name = GetBenefitName(sender);
            dto.Rank = _benefitController.GetNextBenefitRank();
            dto.Text = GetBenefitText(sender);
            dto.Deleted = false;

            Benefits.Add(dto);

            // rebind benefits repeater            
            _benefitController.BindBenefits(repeater, Benefits);

            // clear textboxes
            nameTextBox.Text = "";
            textTextBox.Text = "";
        }

        // Toggle benefit highlight
        protected void BenefitHighlightCkb_CheckedChanged(object sender, EventArgs e)
        {
            BenefitDto benefit = GetBenefit( sender as IButtonControl );
            bool wasChecked = !((WebControl) sender).CssClass.Contains("unchecked");

            benefit.IsProgramHighlight = wasChecked ? false : true;

            RebindBenefits( sender as Control );
        }

        // Delete benefit
        protected void DeleteBenefit_OnClick(object sender, EventArgs e)
        {
            BenefitDto benefit = GetBenefit( sender as IButtonControl );
            benefit.Deleted = true;

            RebindBenefits(sender as Control);
        }

        // Add child span to <a> tag for CSS purposes
        protected void UpdateProgramButton_PreRender(object sender, EventArgs e)
        {
            //base.OnPreRender(e);
            if (!((LinkButton)sender).Text.Contains("/span"))
            {
                ((LinkButton)sender).Text = "<span>" + ((LinkButton)sender).Text + "</span>";
            }
        }

        // Save program
        protected void UpdateProgramButton_OnClick(object sender, EventArgs e)
        {
            int programId = GetProgramId();

            DomainModel.CertifiedProgram program = DomainModel.CertifiedProgram.GetCertifiedProgram(programId);
			CurrentProgramBusinessUnitId.Value = program.BusinessUnitId.ToString();

            #region Save benefits -- those with null ids are the ones which were inserted.

            foreach (BenefitDto benefit in Benefits)
            {
                CertifiedProgramBenefit programBenefit;

                if (benefit.Id == null)
                {
                    if (!benefit.Deleted)
                    {
                        programBenefit = CertifiedProgramBenefit.NewCertifiedProgramBenefit(benefit.Name);

                        programBenefit.Text = benefit.Text;
                        programBenefit.Rank = benefit.Rank;
                        programBenefit.IsProgramHighlight = benefit.IsProgramHighlight;

                        program.CertifiedProgramBenefits.AddCertifiedProgramBenefit(programBenefit);
                    }
                }
                else
                {
                    int bid = (int)benefit.Id;
                    programBenefit = program.CertifiedProgramBenefits.FindAccordingToId(bid);

                    // delete or update.
                    if (benefit.Deleted)
                    {
                        program.CertifiedProgramBenefits.RemoveCertifiedProgramBenefit(programBenefit);
                    }
                    else
                    {
                        programBenefit.Name = benefit.Name;
                        programBenefit.Text = benefit.Text;
                        programBenefit.Rank = benefit.Rank;
                        programBenefit.IsProgramHighlight = benefit.IsProgramHighlight;
                    }

                }
            }

            #endregion


            program.Name = ((TextBox)ProgramFormView.FindControl("CpoProgramNameTextBox")).Text;
            program.Text = ((TextBox)ProgramFormView.FindControl("CpoDescriptionTextBox")).Text;
            program.Icon = ((HiddenField) ProgramFormView.FindControl("CurrentProgramIconHidden")).Value.ToString();

            program.Save();

            // Clear the cache.
            CacheReset(CachedBenefits);
        }

        // Increase benefit rank
        protected void MoveBenefitUpButton_Clicked(object sender, EventArgs e)
        {
            string key = ((ImageButton)sender).CommandArgument;
            _benefitController.AdjustRanks(key, BenefitController.SwapTarget.Before);
        }

        // Decrease benefit rank
        protected void MoveBenefitDownButton_Clicked(object sender, EventArgs e)
        {
            string key = ((ImageButton)sender).CommandArgument;
            _benefitController.AdjustRanks(key, BenefitController.SwapTarget.After);
        }

        protected void CpoLogosRepeater_PreRender(object sender, EventArgs e)
        {
            DirectoryInfo logoDir = new DirectoryInfo(Server.MapPath("~/Public/Images/CPOLogos"));
            foreach (FileInfo file in logoDir.GetFiles())
            {
                if (file.Extension == ".jpg" || file.Extension == ".gif" || file.Extension == ".png" || file.Extension == ".bmp")
                {
                    LogoFileNames.Add(file.Name);
                }
            }
            Repeater logosRepeater = ((Repeater)sender);
            logosRepeater.DataSource = LogoFileNames;
            logosRepeater.DataBind();
        }

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrator"))
            {
                throw new PrivilegeNotHeldException("Administrator");
            }
        }

	    private BenefitDto GetBenefit(IButtonControl button)
        {
            // Extract key and find benefit.
            string key = button.CommandArgument;
            return _benefitController.GetBenefitByKey(key);
        }

        private void RebindBenefits(Control sender)
        {
            Repeater repeater = (Repeater)sender.BindingContainer.NamingContainer;
            _benefitController.BindBenefits(repeater, Benefits);
        }

        // Get benefits from domain
        private List<BenefitDto> LoadBenefitsFromDomain()
        {
            // Get exising benefits from the db.
            DomainModel.CertifiedProgram program =
               DomainModel.CertifiedProgram.GetCertifiedProgram(int.Parse(CurrentProgramId.Value));
	        CurrentProgramBusinessUnitId.Value = program.BusinessUnitId.ToString();

            // Load and cache benefits
            IEnumerator<IBenefit> enumerator = _benefitController.GetEnumerator(program.CertifiedProgramBenefits);
            return _benefitController.CacheBenefits(enumerator);

        }

        private bool IsBenefitInputValid(string nameValue, string textValue, bool insert)
        {
            // TODO: Might need to take a benefit key.

            bool isValid = true;
            
            BulletedList errorList = (BulletedList)ProgramFormView.FindControl("BenefitErrorList");
            errorList.Items.Clear();
            errorList.Visible = false;

            if (string.IsNullOrEmpty(nameValue.Trim()))
            {
                errorList.Items.Add("Benefit Name is required.");
                isValid = false;
            }
            if (string.IsNullOrEmpty(textValue.Trim()))
            {
                errorList.Items.Add("Benefit Text is required.");
                isValid = false;
            }
            if (insert && _benefitController.BenefitNameExists(nameValue) )
            {
                errorList.Items.Add("There is already a benefit with that name.");
                isValid = false;
            }
            if (!isValid)
            {
                errorList.Visible = true;
            }
            return isValid;
        }

        // Get benefit name from text box
        private static string GetBenefitName(object sender)
        {
            return ((TextBox) ((Button) sender).BindingContainer.FindControl("ProgramBenefitNameTextBox")).Text;
        }

        // Get benefit text from text box
        private static string GetBenefitText(object sender)
        {
            return ((TextBox)((Button)sender).BindingContainer.FindControl("ProgramBenefitTextTextBox")).Text;
        }

        // Get program IDictionary from hidden field
        private int GetProgramId()
        {
            string id = CurrentProgramId.Value;
            return Int32.Parse(id);
        }

	    private int GetProgramBusinessUnitId()
	    {
			string id = CurrentProgramBusinessUnitId.Value;
		    return id.Length > 0 ? Int32.Parse(id) : -1;
	    }


		protected void ProgramTypeChanged(object sender, EventArgs e)
	    {
		    var list = (RadioButtonList) sender;

		    var groupLabel = ProgramFormView.FindControl("GroupSelectLabel");
			var dealerLabel = ProgramFormView.FindControl("DealerSelectLabel");

		    groupLabel.Visible = list.SelectedValue == "3";
			dealerLabel.Visible = list.SelectedValue == "4";

		    var ddl = list.BindingContainer.FindControl("BUDropDownList");
			
			if (ddl != null)
				ddl.DataBind();

		    //list.BindingContainer.FindControl("DealerProgramPanel").Visible = ! list.SelectedValue.Equals("0");
		    //list.BindingContainer.FindControl("GroupSelectLabel").Visible = list.SelectedValue.Equals("3");
		    //list.BindingContainer.FindControl("DealerSelectLabel").Visible = list.SelectedValue.Equals("4");
	    }

	    protected void CertifiedProgramDataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
	    {
			var selectedBUID = ((DropDownList)ProgramFormView.FindControl("BUDropDownList")).SelectedValue;

			if (!string.IsNullOrEmpty(selectedBUID))
				e.InputParameters["businessUnitId"] = selectedBUID;

			foreach (var p in e.InputParameters)
			{
				Debug.WriteLine(string.Format("after...paramater: {0}", p.ToJson()));
			}
	    }
    }
}
