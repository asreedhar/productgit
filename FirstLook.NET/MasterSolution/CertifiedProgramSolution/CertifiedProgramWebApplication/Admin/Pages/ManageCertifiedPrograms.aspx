<%@ Page MaintainScrollPositionOnPostback="true" Language="C#" Title="Manage Certified Pre-Owned Benefit Programs" MasterPageFile="~/Admin/Pages/CPOAdmin.Master" AutoEventWireup="True" CodeBehind="ManageCertifiedPrograms.aspx.cs" Inherits="FirstLook.CertifiedProgram.WebApplication.Admin.Pages.ManageCertifiedProgram" %>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="BodyContentPlaceHolder1" runat="server">
<div class="form_view">
    <h2>Manage Manufacturer Certified Pre-Owned Benefit Programs</h2>
    <div class="help_text">
        <p id="DefaultHelpText" runat="server">To add a new Certified Pre-Owned Program click the <strong>Add New Program</strong> link below. To edit an exisitng Certified Pre-Owned Program click the name of the Certified Pre-Owned Program you wish to edit. To activate or deactivate an existing Certified Pre-Owned Program simply check or uncheck the associated checkbox in the <strong>Active</strong> column.</p>
        <p id="InsertHelpText" runat="server" visible="false">To add a new program provide the requested <strong>Program Name</strong> and <strong>Description</strong> and click the <strong>Insert</strong> button.</p>
        <p id="EditHelpText" runat="server" visible="false">To edit a Certified Pre-Owned Program make the desired updates to <strong>Program Information</strong>, <strong>Makes</strong> and <strong>Benefits</strong> and click the <strong>Save</strong> button.</p>
    </div>
    
    <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList"></asp:BulletedList>
    <asp:Panel ID="CpoProgramsListPanel" runat="server">
        <fieldset id="CpoProgramsListDiv">
            <legend>Certified Pre-Owned Benefit Programs <span class="normal">(<asp:LinkButton ID="AddProgramButton" runat="server" Text="Add New Program" OnClick="AddProgramButton_Click"></asp:LinkButton>)</span></legend>
            <asp:ObjectDataSource ID="CertifiedProgramInfoCollectionDataSource" runat="server" TypeName="FirstLook.CertifiedProgram.DomainModel.CertifiedProgramInfoCollection+DataSource"
            SelectMethod="Select">
                <SelectParameters>
                    <asp:Parameter Name="ProgramTypeVal" Type="Int32" DefaultValue="1"/>
                    <asp:Parameter Name="ActiveOnly" Type="Boolean" DefaultValue="False"/>
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Repeater ID="CertifiedProgramInfoCollectionRepeater" runat="server" DataSourceID="CertifiedProgramInfoCollectionDataSource">
                <HeaderTemplate>
                    <table id="ProgramsListTable" style="width:100%;border-collapse:collapse;">
                        <tr>
                            <th style="border-bottom-width:1px;">Program Name</th>
                            <th style="border-bottom-width:1px;">Marketing Name</th>
                            <th style="border-bottom-width:1px;text-align:center;">Active</th>
                        </tr>
                </HeaderTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
                <ItemTemplate>
                    <tr class="active<%# Eval("Active") %>">
                        <td>
                            <asp:LinkButton ID="EditProgramLink1" runat="server" CommandArgument='<%# Eval("Id") %>' OnClick="EditProgramLinkButton_Click"><%# Eval("Name") %></asp:LinkButton>
                            <asp:HiddenField ID="ProgramId" runat="server" Value='<%# Eval("Id") %>' />
                        </td>
                        <td><label><%# Eval("MarketingName") %></label></td>
                        <td style="text-align:center;vertical-align:middle;"><asp:CheckBox ID="ProgramActiveCheckBox" runat="server" Checked='<%# Eval("Active") %>' AutoPostBack="true" OnCheckedChanged="ProgramActiveCheckBox_CheckedChanged" /> <%--<asp:ImageButton id="DeleteProgramHyperLink" ImageUrl="~/App_Themes/Leopard/Images/VPA/button_cancel_x.gif" OnClientClick="if (confirm('Are you sure you want to set this program to \'Inactive\'?')) {return true;} else {return false;}" runat="server" OnClick="DeleteProgramHyperLink_Click" />--%></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </fieldset>
    </asp:Panel>
    
    <asp:HiddenField ID="CurrentProgramId" runat="server" />

    <asp:ObjectDataSource ID="CertifiedProgramDataSource" runat="server" TypeName="FirstLook.CertifiedProgram.DomainModel.CertifiedProgram+DataSource"
            OnInserted="CertifiedProgramDataSource_Inserted"
            OnUpdated="CertifiedProgramDataSource_Updated"
            InsertMethod="Insert"
            SelectMethod="Select"
            UpdateMethod="Update">
        <InsertParameters>
            <asp:Parameter Name="Name" Type="string"  />
            <asp:Parameter Name="BusinessUnitId" Type="Int32"/>
            <asp:Parameter Name="Text" Type="string" />
            <asp:Parameter Name="Icon" Type="string" />
            <asp:Parameter Name="MarketingName" Type="string"  />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter Name="CertifiedProgramID" ControlID="CurrentProgramId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:ControlParameter Name="CertifiedProgramID" ControlID="CurrentProgramId" Type="Int32" />
            <asp:ControlParameter Name="Name" ControlID="ProgramFormView$CpoProgramNameTextBox" Type="string"  />
            <asp:ControlParameter Name="Active" ControlID="ProgramFormView$CpoActiveHiddenField" Type="string" />
            <asp:ControlParameter Name="Text" ControlID="ProgramFormView$CpoDescriptionTextBox" Type="string" />
            <asp:ControlParameter Name="Icon" ControlID="ProgramFormView$CurrentProgramIconHidden" Type="string" />
            <asp:ControlParameter Name="MarketingName" ControlID="ProgramFormView$CpoMarketingNameTextBox" Type="string" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:FormView ID="ProgramFormView" runat="server" DefaultMode="Edit" DataSourceID="CertifiedProgramDataSource" Visible="false" CssClass="formView" Enabled="true">
        <InsertItemTemplate>
            <h3>Add Certified Pre-Owned Benefit Programs</h3>
            <%--   
                This fieldset's runat="server" is a hack to get the add'l markup in this template to render. Currently, without it,
                only the form fields, validation message spans and other markup rendered by server controls are output to the page
            --%>
            <fieldset id="ProgramInfoFieldset" runat="server">
                <legend>Program Information</legend>

                <div class="fieldLabelPair">
                    <label for="<%= ProgramFormView.FindControl("CpoProgramNameTextBox").ClientID %>">Program Name:</label>
                    <asp:TextBox ID="CpoProgramNameTextBox" runat="server" Width="579" Text='<%# Bind("Name") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="CpoProgramNameTextBoxValidator" runat="server" ControlToValidate="CpoProgramNameTextBox" ErrorMessage="Program Name is required."></asp:RequiredFieldValidator>
                </div>
                <div class="fieldLabelPair">
                    <label for="<%= ProgramFormView.FindControl("CpoMarketingNameTextBox").ClientID %>">Marketing Name:</label>
                    <asp:TextBox ID="CpoMarketingNameTextBox" runat="server" Width="579" Text='<%# Bind("MarketingName") %>'></asp:TextBox>
                </div>
                <div class="fieldLabelPair">
                    <label for="<%= ProgramFormView.FindControl("CpoDescriptionTextBox").ClientID %>">Description:</label>
                    <asp:TextBox ID="CpoDescriptionTextBox" runat="server" TextMode="MultiLine" Width="570" Height="40" Text='<%# Bind("Text") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="CpoDescriptionTextBoxValidator" runat="server" ControlToValidate="CpoDescriptionTextBox" ErrorMessage="Description is required."></asp:RequiredFieldValidator>
                </div>
            </fieldset>
            <div class="formButtons">
                <asp:LinkButton ID="InsertProgramButton" runat="server" CommandName="Insert" Text="Insert" CssClass="button saveButton" OnPreRender="UpdateProgramButton_PreRender" />
                <a href="ManageCertifiedPrograms.aspx" class="button cancelButton"><span>Cancel</span></a>
            </div>
        </InsertItemTemplate>
        <EditItemTemplate>
            <%--   
                This fieldset's runat="server" is a hack to get the add'l markup in this template to render. Currently, without it,
                only the form fields, validation message spans and other markup rendered by server controls are output to the page
            --%>
            <fieldset id="ProgramInfoFieldset" runat="server">
                <legend>Program Information</legend>
                
                <div class="fieldLabelPair">
                    <label for="<%= ProgramFormView.FindControl("ProgramBUIDLabel").ClientID %>">BusinessUnitId</label>
                    <asp:Label ID="ProgramBUIDLabel" runat="server" Text='<%# Eval("BusinessUnitId") %>'></asp:Label>
                </div>

                <div class="fieldLabelPair">
                    <label for="<%= ProgramFormView.FindControl("CpoProgramNameTextBox").ClientID %>">Program Name:</label>
                    <asp:TextBox ID="CpoProgramNameTextBox" runat="server" Width="579" Text='<%# Bind("Name") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="CpoProgramNameTextBoxValidator" runat="server" ControlToValidate="CpoProgramNameTextBox" ErrorMessage="Program Name is required."></asp:RequiredFieldValidator>
                </div>
                <div class="fieldLabelPair">
                    <label for="<%= ProgramFormView.FindControl("CpoMarketingNameTextBox").ClientID %>">Marketing Name:</label>
                    <asp:TextBox ID="CpoMarketingNameTextBox" runat="server" Width="579" Text='<%# Bind("MarketingName") %>'></asp:TextBox>
                </div>
                <div class="fieldLabelPair">
                    <label for="<%= ProgramFormView.FindControl("CpoDescriptionTextBox").ClientID %>">Description:</label>
                    <asp:TextBox ID="CpoDescriptionTextBox" runat="server" TextMode="MultiLine" Width="570" Height="40" Text='<%# Bind("Text") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="CpoDescriptionTextBoxValidator" runat="server" ControlToValidate="CpoDescriptionTextBox" ErrorMessage="Description is required."></asp:RequiredFieldValidator>
                </div>
                <asp:HiddenField ID="CpoActiveHiddenField" runat="server" Value='<%# Bind("Active") %>' />
            </fieldset>
            <fieldset id="ProgramIconFieldset" runat="server">
                <legend>Program Icon</legend>
                <asp:Image ID="CurrentProgramIcon" runat="server" ImageUrl="../../Public/Images/CPOLogos/acura.jpg" CssClass="imageWidgetSelectedImage" />
                <asp:HiddenField ID="CurrentProgramIconHidden" runat="server" Value='<%# Bind("Icon") %>' />
                <a href="#" id="CPOLogoWidgetLink">Select Icon</a>
                <div id="SelectLogoWidget" style="display:none;">
                    <p>Click a logo below to replace the currently selected Certified Program logo.</p>
                    <div>
                        <asp:Repeater ID="CpoLogosRepeater" runat="server" OnPreRender="CpoLogosRepeater_PreRender">
                            <HeaderTemplate><ul></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li><a href="#"><img src="../../Public/Images/CPOLogos/<%# (string)Container.DataItem %>" name="<%# (string)Container.DataItem %>" title="" alt="" /></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </fieldset>
            <fieldset id="ProgramMakesFieldset" runat="server">
                <legend>Makes</legend>
                <asp:CheckBoxList AutoPostBack="true" OnSelectedIndexChanged="MakesCheckBoxList_SelectedIndexChanged" ID="MakesCheckBoxList" DataValueField="Id" DataTextField="Name" runat="server" CssClass="makesCkbList" RepeatColumns="5" RepeatDirection="Horizontal" OnPreRender="MakesCheckBoxList_PreRender" OnDataBound="MakesCheckBoxList_Bound"></asp:CheckBoxList>
            </fieldset>
            <fieldset>
                <legend>Benefits</legend>
                <asp:BulletedList ID="BenefitErrorList" runat="server" Visible="false" CssClass="ErrorList" style="text-align:left;"></asp:BulletedList>
                <div class="fieldLabelPair">
                    <div>
                        <label>Benefit Name:</label>
                        <asp:TextBox ID="ProgramBenefitNameTextBox" runat="server" Width="579"></asp:TextBox>
                    </div>
                    <div style="margin-top:5px;">
                        <label>Benefit Text:</label>
                        <asp:TextBox ID="ProgramBenefitTextTextBox" runat="server" TextMode="MultiLine" Width="570" Height="40" style="position:relative;top:-7px;"></asp:TextBox>
                    </div>
                    <asp:Button ID="InsertBenefitButton" runat="server" Text="Insert" CommandArgument='<%# Eval("Id") %>' OnClick="InsertBenefitButton_Click" />
                    <asp:Button ID="UpdateBenefitButton" runat="server" Text="Save" OnClick="UpdateBenefitButton_Click" Visible="false" />
                </div>
                <asp:Repeater ID="CertifiedProgramBenefitsRepeater" runat="server" OnLoad="CertifiedProgramBenefitsRepeater_Load" OnItemDataBound="ProgramBenefitsRepeater_ItemDataBound">
                    <HeaderTemplate>
                        <table id="ProgramsListTable" style="width:100%;border-collapse:collapse;">
                            <tr>
                                <th style="width:25%;">Name</th>
                                <th style="width:40%;">Text</th>
                                <th style="text-align:center;width:10%;">Highlight</th>
                                <th style="text-align:center;width:5%;" colspan="2">Adjust Rank</th>
                                <th style="text-align:center;width:10%;">Delete</th>
                            </tr>
                    </HeaderTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                    <ItemTemplate>
                        <tr class="benefitRow">
                            <td><asp:LinkButton CausesValidation="false" ID="EditBenefitLink" runat="server" OnClick="EditBenefitLink_Click" Text='<%# Eval("Name") %>'></asp:LinkButton></td>
                            <td><%# Eval("Text") %></td>
                            <td style="text-align:center;vertical-align:middle;"><asp:LinkButton CausesValidation="false" ID="BenefitHighlightCkb" runat="server"></asp:LinkButton></td>
                            <td style="text-align:center;vertical-align:middle;"><asp:ImageButton CausesValidation="false" ID="MoveBenefitUpButton" ToolTip="Move up" runat="server" AlternateText="^" ImageUrl="~/App_Themes/Leopard/Images/arrow_Up.gif" /></td>
						    <td style="text-align:center;vertical-align:middle;"><asp:ImageButton CausesValidation="false" ID="MoveBenefitDownButton" ToolTip="Move down" runat="server" AlternateText="v" ImageUrl="~/App_Themes/Leopard/Images/arrow_Down.gif" /></td>
						    <td style="text-align:center;vertical-align:middle;"><asp:ImageButton CausesValidation="false" ID="DeleteBenefit" AlternateText="X" ImageUrl="~/App_Themes/Leopard/Images/VPA/button_cancel_x.gif" ToolTip="Delete" runat="server" /></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </fieldset>
            <div class="formButtons">
                <asp:LinkButton ID="UpdateProgramButton" runat="server" CommandName="Update" Text="Save" CssClass="button saveButton" 
                    OnPreRender="UpdateProgramButton_PreRender" OnClick="UpdateProgramButton_OnClick" />
                <a href="ManageCertifiedPrograms.aspx" class="button cancelButton"><span>Cancel</span></a>
            </div>
        </EditItemTemplate>
    </asp:FormView>
</div>
</asp:Content>
