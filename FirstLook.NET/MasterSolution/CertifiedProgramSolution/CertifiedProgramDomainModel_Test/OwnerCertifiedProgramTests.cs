using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using FirstLook.CertifiedProgram.DomainModel;
using NUnit.Framework;
using CertDomainModel = FirstLook.CertifiedProgram.DomainModel;

namespace FirstLook.CertifiedProgram.DomainModel_Test
{
    [TestFixture]
    public class OwnerCertifiedProgramTests
    {


        //The array of ids the tests
        private Int32[] _certifiedProgramFetchIds = null;
        private readonly String _testOwnerHandle = ConfigurationManager.AppSettings["TestOwnerHandle"];



        public OwnerCertifiedProgramTests()
        {
            //Make sure the principal is created
            CertifiedPrincipal principal = CertifiedPrincipal.Instance;
        }


        private void MakeSureWeHavePrograms()
        {
            _certifiedProgramFetchIds = new Int32[3];
            //Make sure we have the needed programs for the test
            for (int i = 1; i < 4; i++)
            {
                CertDomainModel.CertifiedProgram certProgram;

                //Make sure we have at least three programs
                if (!CertDomainModel.CertifiedProgram.Exists("TestCertifiedProgram" + i))
                {
                    certProgram =
                        CertDomainModel.CertifiedProgram.NewCertifiedProgram();
                    certProgram.Name = ("TestCertifiedProgram" + i);
                    certProgram.Text = "TestCertifiedProgram" + i + " Text";
                    certProgram.Save();
                }


                //This gets the ids for the first programs it finds not necessarily the ones just added
                certProgram = CertDomainModel.CertifiedProgram.GetCertifiedProgram(i);
                _certifiedProgramFetchIds[i - 1] = certProgram.Id;
            }

        }


        [SetUp]
        public void Setup()
        {
            MakeSureWeHavePrograms();
        }

        [TearDown]
        public void TearDown()
        {
            _certifiedProgramFetchIds = null;
        }


        [Test]
        public void AffiliateAndSave()
        {
            CertDomainModel.OwnerCertifiedProgram owcp =
                CertDomainModel.OwnerCertifiedProgram.NewOwnerCertifiedProgram();
            owcp.LoadAssociation(_certifiedProgramFetchIds[0], _testOwnerHandle);
            owcp.Save();
            owcp = CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgram(_certifiedProgramFetchIds[0], _testOwnerHandle);
            Assert.AreNotEqual(owcp, null);


        }

        [Test]
        public void Exists()
        {
            //Make sure this is using a Certified Program id
            var here = CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgramAssociation(_certifiedProgramFetchIds[0], _testOwnerHandle);
            Assert.AreNotEqual(here.Association == OwnerCertifiedProgram.OwnerProgramAssociationType.Explicit, false);
        }



        [Test]
        public void Fetch()
        {
            CertDomainModel.OwnerCertifiedProgram owcp =
                CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgram(_certifiedProgramFetchIds[0], _testOwnerHandle);

            Assert.AreNotEqual(owcp, null);
        }


        [Test]
        public void Select()
        {
            CertDomainModel.OwnerCertifiedProgram.DataSource ds =
                new OwnerCertifiedProgram.DataSource();
            CertDomainModel.OwnerCertifiedProgram ownerCertifiedProgram =
                    ds.Select(_certifiedProgramFetchIds[0], _testOwnerHandle);
            Assert.AreNotEqual(ownerCertifiedProgram, null);
        }



        [Test]
        public void ReorderAndSave()
        {
            //Get the program
            CertDomainModel.OwnerCertifiedProgram owcp =
                CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgram(3, _testOwnerHandle);
            Assert.AreNotEqual(owcp, null);

            //Reorder
            OwnerCertifiedProgramBenefit benefit1 =  owcp.OwnerCertifiedProgramBenefits[2];
            OwnerCertifiedProgramBenefit benefit2 =  owcp.OwnerCertifiedProgramBenefits[3];

            short orgBen1Rank = benefit1.Rank;
            benefit1.Rank = benefit2.Rank;
            String benefitName2 = benefit2.Name;
            benefit2.Rank = orgBen1Rank;

            owcp.Save();

            //Get the program again
            owcp = CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgram(_certifiedProgramFetchIds[0], _testOwnerHandle);
            Assert.AreNotEqual(owcp, null);

            //Show the rank has changed
            benefit2 = owcp.OwnerCertifiedProgramBenefits.FindAccordingToName(benefitName2);
            Assert.AreEqual(benefit2.Rank, orgBen1Rank);


        }

        [Test]
        public void HighlightAndSave()
        {
            //Get the program
            CertDomainModel.OwnerCertifiedProgram owcp =
                CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgram(_certifiedProgramFetchIds[0], _testOwnerHandle);
            Assert.AreNotEqual(owcp, null);

            //Reorder
            OwnerCertifiedProgramBenefit benefit1 = owcp.OwnerCertifiedProgramBenefits[2];
            Boolean orgIsHighlighted = benefit1.IsProgramHighlight;

            //reverse
            benefit1.IsProgramHighlight = !benefit1.IsProgramHighlight;
            owcp.Save();

            //Get the program again
            owcp = CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgram(3, _testOwnerHandle);
            Assert.AreNotEqual(owcp, null);

            //Show that the highlight has been reversed
            Assert.AreNotEqual(benefit1.IsProgramHighlight, orgIsHighlighted);

        }

        [Test]
        public void DisaffiliateAndSave()
        {

            Assert.AreNotEqual(CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgramAssociation(_certifiedProgramFetchIds[0], _testOwnerHandle), false);

            CertDomainModel.OwnerCertifiedProgram owcp =
                CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgram(_certifiedProgramFetchIds[0], _testOwnerHandle);

            Assert.AreNotEqual(owcp, null);
            owcp.RemoveAssociation();
            owcp.Delete();
            owcp.Save();
            Boolean here = CertDomainModel.OwnerCertifiedProgram.GetOwnerCertifiedProgramAssociation(_certifiedProgramFetchIds[0], _testOwnerHandle).Association == OwnerCertifiedProgram.OwnerProgramAssociationType.Explicit;
            Assert.AreEqual(here, false);
        }



 

    }
}
