using System;
using NUnit.Framework;
using CertDomainModel = FirstLook.CertifiedProgram.DomainModel;


namespace FirstLook.CertifiedProgram.DomainModel_Test
{
    [TestFixture]
    public class CertifiedProgramInfoCollectionTests
    {
        public CertifiedProgramInfoCollectionTests()
        {
            //Make sure the principal is created
            CertifiedPrincipal principal = CertifiedPrincipal.Instance;
        }

        [SetUp]
        public void Setup()
        {}

        [TearDown]
        public void TearDown()
        {}

        [Test]
        public void Fetch()
        {
            CertDomainModel.CertifiedProgramInfoCollection cpc =
                CertDomainModel.CertifiedProgramInfoCollection.GetCertifiedProgramInfoCollection(CertDomainModel.CertifiedProgramType.None);
            Assert.AreNotEqual(cpc, null);
        }

    }
}
