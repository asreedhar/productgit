using System;
using FirstLook.CertifiedProgram.DomainModel;
using NUnit.Framework;
using CertDomainModel = FirstLook.CertifiedProgram.DomainModel;
using System.ComponentModel;
using Csla;
using System.Diagnostics;


namespace FirstLook.CertifiedProgram.DomainModel_Test
{
    [TestFixture]
    public class CertifiedProgramTests
    {
        
        //The array of ids the tests
        private Int32[] _certifiedProgramFetchIds = null;

        public CertifiedProgramTests()
        {
            //Make sure the principal is created
            CertifiedPrincipal principal = CertifiedPrincipal.Instance;
        }


        [SetUp]
        public void Setup()
        {

            _certifiedProgramFetchIds = new Int32[3];
            //Make sure we have the needed programs for the test
            for (int i = 1; i < 4; i++)
            {
                CertDomainModel.CertifiedProgram certProgram;
                
                //Make sure we have at least three programs
                if (!CertDomainModel.CertifiedProgram.Exists("TestCertifiedProgram" + i))
                {
                    certProgram =
                        CertDomainModel.CertifiedProgram.NewCertifiedProgram();
                    certProgram.Name = ("TestCertifiedProgram" + i);
                    certProgram.Text = "TestCertifiedProgram" + i + " Text";
                    certProgram.Save();
                }

                //Get the ids for the array
                certProgram = CertDomainModel.CertifiedProgram.GetCertifiedProgram(i);
                _certifiedProgramFetchIds[i-1] = certProgram.Id;
            }
        }

        [TearDown]
        public void TearDown()
        {
            //Empty the ids
            _certifiedProgramFetchIds = null;
        }

        [Test]
        public void SelectTest()
        {
            //we can be certain that the _certifiedProgramFetchIds[0]
            //has been initialized with that name thanks to the setup code
            
            CertDomainModel.CertifiedProgram cp =
             (new DomainModel.CertifiedProgram.DataSource().Select(_certifiedProgramFetchIds[0]));
            
            Assert.AreNotEqual(cp, null);
            
        }



        [Test]
        public void UpdateViaDataSource()
        {
            //Let's fetch first
            CertDomainModel.CertifiedProgram cp =
             (new DomainModel.CertifiedProgram.DataSource().Select(_certifiedProgramFetchIds[0]));
            
            DomainModel.CertifiedProgram.DataSource ds =
                new DomainModel.CertifiedProgram.DataSource();

            //we can be certain that the _certifiedProgramFetchIds[0]
            //has been initialized with that name thanks to the setup code
            ds.Update(cp.Id, cp.Name + 1, "Some text", "icon", true, null);


            CertDomainModel.CertifiedProgram certifiedProgram =
                CertDomainModel.CertifiedProgram.GetCertifiedProgram(_certifiedProgramFetchIds[0]);

            Assert.AreEqual("Some text", certifiedProgram.Text);
        }
        




        [Test]
        public void InsertAndFetch()
        {
            //This code assumes the db is without that name
            //If a certified program exists with that name, we will only fetch
            CertDomainModel.CertifiedProgram cp 
                = CertDomainModel.CertifiedProgram.NewCertifiedProgram();
            cp.Name = "ViaInsertAndFetchName";
            cp.Text = "Via insert and fetch Text";
            
            if (!CertDomainModel.CertifiedProgram.Exists(cp.Name))
            {
                cp.Save();

                //See if we can reget it from the db
                cp = CertDomainModel.CertifiedProgram.GetCertifiedProgram(cp.Id);
                Assert.AreNotEqual(cp, null);
            }
            
        }

        [Test]
        public void Update()
        {
            //we can be certain that the _certifiedProgramFetchIds[0]
            //has been initialized with that name thanks to the setup code
            CertDomainModel.CertifiedProgram certifiedProgram =
                CertDomainModel.CertifiedProgram.GetCertifiedProgram(_certifiedProgramFetchIds[0]);
            certifiedProgram.Text = "Some text 2";
            certifiedProgram.Save();

            //See if we can reget it from the db
            certifiedProgram =
                CertDomainModel.CertifiedProgram.GetCertifiedProgram(_certifiedProgramFetchIds[0]);
            Assert.AreEqual("Some text 2", certifiedProgram.Text);
        }

        [Test]
        public void RemoveAndRestore()
        {
            //Get first to get a valid certified program to work with.
            CertDomainModel.CertifiedProgram certifiedProgram =
               CertDomainModel.CertifiedProgram.GetCertifiedProgram(_certifiedProgramFetchIds[0]);
            Assert.AreNotEqual(certifiedProgram, null);
            certifiedProgram.Active = false;
            certifiedProgram.Save();
            
            //Get it again to see if anythig has changed
            certifiedProgram =
                CertDomainModel.CertifiedProgram.GetCertifiedProgram(_certifiedProgramFetchIds[0]);
            Assert.AreEqual(certifiedProgram.Active, false);

            //Now change to active
            certifiedProgram.Active = true;
            certifiedProgram.Save();

            //Get it again to see if anythig has changed
            certifiedProgram =
                CertDomainModel.CertifiedProgram.GetCertifiedProgram(_certifiedProgramFetchIds[0]);
            Assert.AreEqual(certifiedProgram.Active, true);

        }
    }
}
