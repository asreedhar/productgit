using System;
using NUnit.Framework;
using CertDomainModel = FirstLook.CertifiedProgram.DomainModel;

namespace FirstLook.CertifiedProgram.DomainModel_Test
{
    [TestFixture]
    public class MakeInfoCollectionTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        //tests the fetch of all the make infos
        //see the CertifiedProgramInfoCollectionTests for a fetch of the makes affiliated with
        //a specific CertifiedProgram
        [Test]
        public void Fetch()
        {
            CertDomainModel.MakeInfoCollection mic = 
                CertDomainModel.MakeInfoCollection.GetMakeInfoCollection();

            Assert.AreNotEqual(mic, null);
            Assert.AreNotEqual(mic.Count, 0);
        }

        [Test]
        public void AddRemoveAssociation()
        {
            const Int32 makeId = 9;
            const Int32 certifiedProgramId = 2;

            CertDomainModel.MakeAssignment.AssignMake(makeId, certifiedProgramId);

            CertDomainModel.MakeInfoCollection mic =
                CertDomainModel.MakeInfoCollection.GetMakeInfoCollection();

            CertDomainModel.MakeAssignment.UnassignMake(makeId, certifiedProgramId);

            mic = CertDomainModel.MakeInfoCollection.GetMakeInfoCollection();
        }

    }
}
