using System;
using System.Security.Principal;
using System.Threading;

namespace FirstLook.CertifiedProgram.DomainModel_Test
{
    //Using a singleton to make sure the principal is set only once.
    public class CertifiedPrincipal
    {

        static private String[] _roles = {"Administrator"};
        static readonly CertifiedPrincipal _instance = new CertifiedPrincipal();
        

        private IIdentity _identity = null;
        private IPrincipal _principal = null;


        private CertifiedPrincipal()
        {
            // Create a principal to use for new threads.
            //I am using Benson's ID - at sometime this should be mapped
            //to something meaningfull from dbo.Member like "CPOTestUser"
            _identity = new GenericIdentity("bfung"); 
            _principal = new GenericPrincipal(_identity, _roles);

            AppDomain appDomain = Thread.GetDomain();
            appDomain.SetThreadPrincipal(_principal);
        }

        public static  CertifiedPrincipal Instance
        {
            get
            {
                return _instance;
            }
        }

        public override string ToString()
        {
            return "Current Identity Name is: "+ _principal.Identity.Name;
        }
    }
}