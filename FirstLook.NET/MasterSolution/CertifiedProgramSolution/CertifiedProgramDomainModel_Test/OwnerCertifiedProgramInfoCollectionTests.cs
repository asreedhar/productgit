using System;
using NUnit.Framework;
using CertDomainModel = FirstLook.CertifiedProgram.DomainModel;


namespace FirstLook.CertifiedProgram.DomainModel_Test
{
    [TestFixture]
    public class OwnerCertifiedProgramInfoCollectionTests
    {
        public OwnerCertifiedProgramInfoCollectionTests()
        {
            //Make sure the principal is created
            CertifiedPrincipal principal = CertifiedPrincipal.Instance;
        }

        [SetUp]
        public void Setup()
        { }

        [TearDown]
        public void TearDown()
        { }

        [Test]
        public void Fetch()
        {
            CertDomainModel.OwnerCertifiedProgramInfoCollection ocpInfos =
                CertDomainModel.OwnerCertifiedProgramInfoCollection.GetOwnerCertifiedProgramInfoCollection(
                    "58B2C338-B5C3-DC11-9377-0014221831B0");
            
            Assert.AreNotEqual(ocpInfos, null);
            Assert.AreNotEqual(ocpInfos.Count, 0);
        }



    }
}
