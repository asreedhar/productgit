using System;
using System.Diagnostics;
using NUnit.Framework;
using CertDomainModel = FirstLook.CertifiedProgram.DomainModel;

namespace FirstLook.CertifiedProgram.DomainModel_Test
{
    [TestFixture]
    public class OwnerCertifiedProgramBenefitCollectionTests
    {
        public OwnerCertifiedProgramBenefitCollectionTests()
        {
            //Make sure the principal is created
            CertifiedPrincipal principal = CertifiedPrincipal.Instance;
        }

        [SetUp]
        public void Setup()
        { }

        [TearDown]
        public void TearDown()
        { }

        [Test]
        public void Fetch()
        {
            Int32 ownerCertifiedProgramId;
            CertDomainModel.OwnerCertifiedProgramBenefitCollection ownerCertifiedProgramBenefits =
                CertDomainModel.OwnerCertifiedProgramBenefitCollection.GetOwnerCertifiedProgramBenefitCollection
                (
                    2,
                    "58B2C338-B5C3-DC11-9377-0014221831B0",
                    out ownerCertifiedProgramId
                 );

            Assert.AreNotEqual(ownerCertifiedProgramBenefits, null);
            Assert.AreNotEqual(ownerCertifiedProgramBenefits.Count, 0);
        }


        [Test]
        public void SaveAfterReRank()
        {
            //get a list
            Int32 ownerCertifiedProgramId;
            CertDomainModel.OwnerCertifiedProgramBenefitCollection ownerCertifiedProgramBenefits =
                CertDomainModel.OwnerCertifiedProgramBenefitCollection.GetOwnerCertifiedProgramBenefitCollection(2, "58B2C338-B5C3-DC11-9377-0014221831B0", out ownerCertifiedProgramId);

            Assert.AreNotEqual(ownerCertifiedProgramBenefits, null);
            Assert.AreNotEqual(ownerCertifiedProgramBenefits.Count, 0);


            //print the status
            Debug.WriteLine("Before:");
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Text);

            //Increase rank
            ownerCertifiedProgramBenefits.IncreaseRank(ownerCertifiedProgramBenefits[2].Name);
            
            //print the status
            Debug.WriteLine("After:");
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Text);
            ownerCertifiedProgramBenefits.Save();
        }
        
        
        [Test]
        public void ReRank()
        {
            //get a list
            Int32 ownerCertifiedProgramId;
            CertDomainModel.OwnerCertifiedProgramBenefitCollection ownerCertifiedProgramBenefits =
                CertDomainModel.OwnerCertifiedProgramBenefitCollection.GetOwnerCertifiedProgramBenefitCollection(2, "58B2C338-B5C3-DC11-9377-0014221831B0", out ownerCertifiedProgramId);

            Assert.AreNotEqual(ownerCertifiedProgramBenefits, null);
            Assert.AreNotEqual(ownerCertifiedProgramBenefits.Count, 0);


            //print the status
            Debug.WriteLine("Before:");
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Text);

            //Increase rank
            ownerCertifiedProgramBenefits.IncreaseRank(ownerCertifiedProgramBenefits[2].Name);
            
            //print the status
            Debug.WriteLine("After:");
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[0].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[1].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[2].Text);

            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Id);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Rank);
            Debug.WriteLine(ownerCertifiedProgramBenefits[3].Text);
        }
    }
}
