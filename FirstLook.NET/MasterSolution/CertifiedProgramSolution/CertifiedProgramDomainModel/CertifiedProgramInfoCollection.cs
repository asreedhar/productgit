using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using Csla;
using Csla.Data;
using FirstLook.Common.Data;
using System.Security;


using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{
    [Serializable]
    public class CertifiedProgramInfoCollection : ReadOnlyListBase<CertifiedProgramInfoCollection, CertifiedProgramInfo>
    {
       #region Factory Methods

        /// <summary>
        /// Returns the CertifiedProgramInfo Collection
        /// </summary>
        /// <returns>The list of active projects</returns>
        public static CertifiedProgramInfoCollection GetCertifiedProgramInfoCollection(CertifiedProgramType programType, bool activeOnly = false)
        {
            return DataPortal.Fetch<CertifiedProgramInfoCollection>(new Criteria(programType, activeOnly));
        }

		public static CertifiedProgramInfoCollection GetCertifiedProgramInfoCollection(CertifiedProgramType programType, string ownerHandle, bool activeOnly)
		{
			return DataPortal.Fetch<CertifiedProgramInfoCollection>(new Criteria(programType, ownerHandle, activeOnly));
		}

        private CertifiedProgramInfoCollection()
        { /* require use of factory methods */ }

        #endregion

        #region Data Access

	    [Serializable()]
	    protected class Criteria
	    {
		    public CertifiedProgramType ProgramType { get; private set; }
		    public string OwnerHandle { get; private set; }
		    public object ActiveOnly { get; set; }

		    public Criteria()
		    {
				ProgramType = CertifiedProgramType.None;
			    ActiveOnly = false;
		    }

		    public Criteria(CertifiedProgramType programType, bool activeOnly = false)
		    {
			    ProgramType = programType;
			    ActiveOnly = activeOnly;
		    }

			public Criteria(CertifiedProgramType programType, string ownerHandle, bool activeOnly = false)
			{
				ProgramType = programType;
				OwnerHandle = ownerHandle;
				ActiveOnly = activeOnly;
			}

	    }

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {

            RaiseListChangedEvents = false;
            IsReadOnly = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Certified.CertifiedProgramInfoCollection#Fetch";
					command.AddParameterWithValue("ProgramType", DbType.Int32, false, criteria.ProgramType);
					command.AddParameterWithValue("ActiveOnly", DbType.Boolean, false, criteria.ActiveOnly);

					if (!string.IsNullOrEmpty(criteria.OwnerHandle))
						command.AddParameterWithValue("OwnerHandle", DbType.String, true, criteria.OwnerHandle);

                    using (var reader = new SafeDataReader(command.ExecuteReader()))
                    {
                        while (reader.Read())
                        {
                            Add(CertifiedProgramInfo.GetCertifiedProgramInfo(reader));
                        }
                    }
                }
            }

            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        #endregion Data Access

        #region DataSource
        [DataObject]
        public class DataSource
        {
            /// <summary>
            /// Returns the CertifiedProgramInfo Collection
            /// </summary>
            /// <returns>The list of active projects</returns>
            [DataObjectMethod(DataObjectMethodType.Select)]
            public CertifiedProgramInfoCollection Select(int programTypeVal, bool activeOnly)
            {
				var programType = (CertifiedProgramType) programTypeVal;
				return GetCertifiedProgramInfoCollection(programType, activeOnly);
            }

			/// <summary>
			/// Returns the CertifiedProgramInfo Collection
			/// </summary>
			/// <returns>The list of active projects</returns>
			[DataObjectMethod(DataObjectMethodType.Select)]
			public CertifiedProgramInfoCollection Select(int programTypeVal, string ownerHandle, bool activeOnly)
			{
				var programType = (CertifiedProgramType)programTypeVal;
				return GetCertifiedProgramInfoCollection(programType, ownerHandle, activeOnly);
			}
		}
        #endregion DataSource

    }
}
