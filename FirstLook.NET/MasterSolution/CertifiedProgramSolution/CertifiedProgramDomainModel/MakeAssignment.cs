using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;
using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{
        [Serializable()]
        public class MakeAssignment : CommandBase
        {

            #region Authorization Methods

            public static bool CanExecuteCommand()
            {
                return CslaAppContext.User.IsInRole("Administrator");
            }

            #endregion

            #region Client-side Code


            private Boolean _assigned = false;
            private readonly Int32   _makeId = 0;
            private readonly Int32   _certifiedProgramId = 0;

            public bool Assigned
            {
                get { return _assigned; }
            }

            #endregion

            #region Factory Methods

            public static bool AssignMake(int makeId, int certifiedProgramId)
            {
                MakeAssignment cmd = new MakeAssignment(makeId,certifiedProgramId);
                cmd._assigned= true;
                cmd = DataPortal.Execute<MakeAssignment>(cmd);
                return cmd.Assigned;
            }

            public static bool UnassignMake(int makeId, int certifiedProgramId)
            {
                MakeAssignment cmd = new MakeAssignment(makeId, certifiedProgramId);
                cmd._assigned = false;
                cmd = DataPortal.Execute<MakeAssignment>(cmd);
                return cmd.Assigned;
            }


            private MakeAssignment()
            { /* require use of factory methods */ }

            private MakeAssignment(int makeId, int certifiedProgramId)
            {
                _makeId = makeId;
                _certifiedProgramId = certifiedProgramId;
            }

            #endregion

            #region Server-side Code

            [Transactional(TransactionalTypes.Manual)]
            protected override void DataPortal_Execute()
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
                {
                    connection.Open();
                    using (IDbTransaction transaction = connection.BeginTransaction())
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.AddParameterWithValue("CertifiedProgramID", DbType.Int32, false, _certifiedProgramId);
                            command.AddParameterWithValue("MakeID", DbType.Int32, false, _makeId);
                            command.Transaction = transaction;

                            command.CommandText = Assigned ?
                                "Certified.CertifiedProgram#AddMake" :
                                "Certified.CertifiedProgram#RemoveMake";

                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                }
            }
            
            #endregion
        }
}
