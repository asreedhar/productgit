using System;
using Csla;
using System.Data;
using System.Security;
using FirstLook.Common.Data;


using CslaAppContext = Csla.ApplicationContext;


namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public class CertifiedProgramBenefit : BusinessBase<CertifiedProgramBenefit>, IBenefit 
	{
        #region Business Methods

        private int _id = 0;
        private string _name = String.Empty;
        private string _text = String.Empty;
        private Int16 _rank = 0;
        private bool _isProgramHighlight = false;

        [System.ComponentModel.DataObjectField(true, true)]
        public int Id
        {
            get
            {
                CanReadProperty("Id", true);
                return _id;
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return _name;
            }
            set
            {
                CanWriteProperty("Name", true);
                _name = value;
                PropertyHasChanged("Name");
            }
        }

        public string Text
        {
            get
            {
                CanReadProperty("Text", true);
                return _text;
            }
            set
            {
                CanWriteProperty("Text", true);
                _text = value;
                PropertyHasChanged("Text");
            }
        }

        public Int16 Rank
        {
            get
            {
                CanReadProperty("Rank", true);
                return _rank;
            }
            set
            {
                CanWriteProperty("Rank", true);
                _rank = value;
                PropertyHasChanged("Rank");
            }
        }

        public bool IsProgramHighlight
        {
            get
            {
                CanReadProperty("IsProgramHighlight", true);
                return _isProgramHighlight;
            }
            set
            {
                CanWriteProperty("IsProgramHighlight", true);
                _isProgramHighlight = value;
                PropertyHasChanged("IsProgramHighlight");
            }
        }

        protected override object GetIdValue()
        {
            return Id;
        }

        //Protected setter for derived classes
        protected void SetId(Int32 id)
        {
            _id = id;
        }


        #endregion Business Methods

        #region Validation Rules

        //NTODO: add business rules after looking at the BusinessRules in the DB
        protected override void AddBusinessRules()
        {
            // NTODO: add validation rules
        }

        #endregion Validation Rules

        #region Authorization Rules


        protected override void AddAuthorizationRules()
        {
            // NTODO: add validation rules        
        }


        public static bool CanAddObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }

        public static bool CanGetObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }


        public static bool CanDeleteObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }

        public static bool CanEditObject()
        {
            return CslaAppContext.User.IsInRole(
                "Administrator");
        }

        #endregion Authorization Rules


        //Not sure exist is needed for child objects
        //#region Exists

        //public static bool Exists(Int32 id)
        //{
        //    ExistsCommand result =
        //        DataPortal.Execute<ExistsCommand>(new ExistsCommand(id));
        //    return result.Exists;
        //}

        //[Serializable()]
        //private class ExistsCommand : CommandBase
        //{
        //    private Int32 _id;
        //    private Boolean _exists;

        //    public bool Exists
        //    {
        //        get { return _exists; }
        //    }

        //    public ExistsCommand(Int32 id)
        //    {
        //        _id = id;
        //    }

        //    protected override void DataPortal_Execute()
        //    {
        //        //NTODO; make sure we have a connection string here
        //        using (SqlConnection cn = new SqlConnection("xxxxxxxxxxxxxxxxxxx Connection String"))
        //        {
        //            cn.Open();
        //            using (SqlCommand cm = cn.CreateCommand())
        //            {
        //                cm.CommandType = CommandType.StoredProcedure;
        //                cm.CommandText = "existsCertifiedProgramBenefit";
        //                cm.Parameters.AddWithValue("@id", _id);
        //                int count = (int)cm.ExecuteScalar();
        //                _exists = (count > 0);
        //            }
        //        }
        //    }
        //}

        //#endregion Exists


        #region Factory Methods

        public static CertifiedProgramBenefit NewCertifiedProgramBenefit(String benefitName)
        {
            if (!CanAddObject())
            {
                throw new SecurityException("User not authorized to add a Certified Program Benefit");
            }
            CertifiedProgramBenefit certifiedProgramBenefit = new CertifiedProgramBenefit();
            certifiedProgramBenefit.Name = benefitName;
            certifiedProgramBenefit._text = "Text goes here";
            certifiedProgramBenefit.IsProgramHighlight = false;
            
            //We still don't have rank or id

            return certifiedProgramBenefit;
        }


        internal static CertifiedProgramBenefit GetCertifiedProgramBenefit(IDataReader dr)
        {
            return new CertifiedProgramBenefit(dr);
        }


        /// <summary>
        /// Called to when a new object is created.
        /// </summary>
        protected CertifiedProgramBenefit()
        {/* Require use of factory methods */

            MarkAsChild(); // The benefit is a child contained in CertifiedProgramBenefitCollection
        }

        /// <summary>
        /// Called when loading data from the database.
        /// </summary>
        private CertifiedProgramBenefit(IDataRecord dr)
        {
            MarkAsChild(); // The benefit is a child contained in CertifiedProgramBenefitCollection
            Fetch(dr);

        }

        #endregion Factory Methods

        #region Data Access

        private void Fetch(IDataRecord dr)
        {
            _id = dr.GetInt32(dr.GetOrdinal("CertifiedProgramBenefitID"));
            Name = dr.GetString(dr.GetOrdinal("Name"));
            Text = dr.GetString(dr.GetOrdinal("Text"));
            Rank = dr.GetInt16(dr.GetOrdinal("Rank"));
            IsProgramHighlight = dr.GetBoolean(dr.GetOrdinal("IsProgramHighlight"));
            MarkOld();
        }

        internal void Insert(Int32 parentProgramId, IDbConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Certified.CertifiedProgramBenefit#Insert";
                command.Transaction = transaction;

                command.AddParameterWithValue("CertifiedProgramID ",DbType.Int32, false, parentProgramId);
                command.AddParameterWithValue("Name", DbType.String, false, Name);
                command.AddParameterWithValue("Text", DbType.String, false, Text);
                command.AddParameterWithValue("Rank", DbType.Int16, false, Rank);
                command.AddParameterWithValue("IsProgramHighlight", DbType.Boolean, false, IsProgramHighlight);
                command.AddParameterWithValue("InsertUser", DbType.String, false, CslaAppContext.User.Identity.Name);

                //Add the out param
                IDataParameter newId = command.AddOutParameter("CertifiedProgramBenefitID", DbType.Int32);

                command.ExecuteNonQuery();
                _id = (Int32)newId.Value;
                
            }
            MarkOld();
        }

        internal void Update(IDbConnection connection, IDbTransaction transaction)
        {

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Certified.CertifiedProgramBenefit#Update";
                command.Transaction = transaction;

                command.AddParameterWithValue("CertifiedProgramBenefitID ", DbType.Int32, false, _id);
                command.AddParameterWithValue("Name", DbType.String, false, Name);
                command.AddParameterWithValue("Text", DbType.String, false, Text);
                command.AddParameterWithValue("Rank", DbType.Int16, false, Rank);
                command.AddParameterWithValue("IsProgramHighlight", DbType.Boolean, false, IsProgramHighlight);
                command.AddParameterWithValue("UpdateUser", DbType.String, false, CslaAppContext.User.Identity.Name);

                command.ExecuteNonQuery();
            }
            MarkOld();
        }

        internal void DeleteSelf(IDbConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Certified.CertifiedProgramBenefit#Delete";
                command.Transaction = transaction;
                command.AddParameterWithValue("CertifiedProgramBenefitID", DbType.Int32, false, Id);
                command.ExecuteNonQuery();
            }
            MarkNew();
        }

        #endregion Data Access

   }
}
