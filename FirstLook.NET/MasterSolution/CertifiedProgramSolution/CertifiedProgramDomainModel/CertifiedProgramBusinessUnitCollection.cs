using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.CertifiedProgram.DomainModel.Annotations;
using FirstLook.Common.Data;

namespace FirstLook.CertifiedProgram.DomainModel
{
	public class CertifiedProgramBusinessUnitCollection :
		ReadOnlyListBase<CertifiedProgramBusinessUnitCollection, CertifiedProgramBusinessUnit>
	{
		#region Factory Methods

		/// <summary>
		/// Returns the CertifiedProgramBusinessUnit Collection
		/// </summary>
		/// <returns>The list</returns>
		public static CertifiedProgramBusinessUnitCollection GetCertifiedProgramBusinessUnitCollection(int businessUnitTypeId)
		{
			return DataPortal.Fetch<CertifiedProgramBusinessUnitCollection>(new Criteria(businessUnitTypeId));
		}

		private CertifiedProgramBusinessUnitCollection()
		{ /* require use of factory methods */ }

		#endregion

		#region Data Access

		[Serializable]
		protected class Criteria
		{
			public Criteria(int buType)
			{
				BUType = buType;
			}

			public int BUType { get; set; }
		}

		[Transactional(TransactionalTypes.Manual), UsedImplicitly]
		private void DataPortal_Fetch(Criteria criteria)
		{
			RaiseListChangedEvents = false;
			IsReadOnly = false;

			if (criteria.BUType == 0) return;

			using (var connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
			{
				connection.Open();
				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Certified.CertifiedProgramBusinessUnitCollection#Fetch";

					command.AddParameterWithValue("@BusinessUnitTypeId", DbType.Int32, false, (int) criteria.BUType);

					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							Add(CertifiedProgramBusinessUnit.GetCertifiedProgramBusinessUnit(reader));
						}
					}
				}
			}

			IsReadOnly = true;

		}

		#endregion Data Access

		#region DataSource
		[DataObject]
		public class DataSource
		{
			/// <summary>
			/// Returns the CertifiedProgramBusinessUnit Collection
			/// </summary>
			/// <returns>The list</returns>
			[DataObjectMethod(DataObjectMethodType.Select)]
			public CertifiedProgramBusinessUnitCollection Select(int butype)
			{
				return GetCertifiedProgramBusinessUnitCollection(butype);
			}
		}
		#endregion DataSource

	}
}