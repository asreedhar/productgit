using System;
using System.ComponentModel;
using System.Data;
using System.Security;
using System.Collections.Generic;
using System.Text;
using Csla;
using Csla.Data;
using FirstLook.Common.Data;
using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{




	public class OwnerCertifiedProgram : BusinessBase<OwnerCertifiedProgram>
    {
        #region Business Methods

        private OwnerCertifiedProgramBenefitCollection _ownerCertifiedProgramBenefits =
                OwnerCertifiedProgramBenefitCollection.NewOwnerCertifiedProgramBenefitCollection();

        private Int32 _certifiedProgramId = 0;
        private Int32 _ownerCertifiedProgramId = 0;
        
        [DataObjectField(true, true)]
        public int Id
        {
            get
            {
                CanReadProperty("Id", true);
                return _ownerCertifiedProgramId;
            }
        }
        // Return unique id value for object this is to support
        // Equals(), GetHashCode(), and ToString() given to us by CSLA
        protected override object GetIdValue()
        {
            return Id;
        }


        private String _ownerHandle = String.Empty;
        public string OwnerHandle
        {
            get
            {
                CanReadProperty("OwnerHandle", true);
                return _ownerHandle;
            }
            set
            {
                CanWriteProperty("OwnerHandle", true);
                if (value == null) value = string.Empty;
                if (_ownerHandle != value)
                {
                    _ownerHandle = value;
                    PropertyHasChanged("OwnerHandle");
                }
            }
        }


        public OwnerCertifiedProgramBenefitCollection OwnerCertifiedProgramBenefits
        {
            get
            {
                CanReadProperty("OwnerCertifiedProgramBenefits", true);
                return _ownerCertifiedProgramBenefits;
            }
        }

        
        
        public override bool IsValid
        {
            get { return base.IsValid && _ownerCertifiedProgramBenefits.IsValid; }
        }

        public override bool IsDirty
        {
            get { return base.IsDirty || _ownerCertifiedProgramBenefits.IsDirty; }
        }


        //This loads a non existing association between 
        //CertifiedBenefits and OwnerCertified Program
        public void LoadAssociation(int certifiedProgramId, String ownerHandle)
        {
            _ownerCertifiedProgramBenefits= OwnerCertifiedProgramBenefitCollection.AssociatedOwnerCertifiedProgramBenefitCollection(
                    certifiedProgramId,ownerHandle);
            _ownerHandle = ownerHandle;
            _certifiedProgramId = certifiedProgramId;
            MarkNew();
        }

        public void RemoveAssociation()
        {
            //Load an empty list
            _ownerCertifiedProgramBenefits =
                OwnerCertifiedProgramBenefitCollection.NewOwnerCertifiedProgramBenefitCollection();
        }
        
        #endregion Business Methods

        #region Authorization Rules

        public static bool CanAddObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }

        public static bool CanGetObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }

        public static bool CanEditObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }

        public static bool CanDeleteObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }



        #endregion Authorization Rules

        #region Factory Methods

        public static OwnerCertifiedProgram NewOwnerCertifiedProgram()
        {
            if (!CanAddObject())
            {
                throw new SecurityException("User not authorized to add a owner certified program");
            }
            return DataPortal.Create<OwnerCertifiedProgram>();
        }



        public static OwnerCertifiedProgram GetOwnerCertifiedProgram(int certifiedProgramId, String ownerHandle)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Owner Certified Programs");


	        var programAssociation = GetOwnerCertifiedProgramAssociation(certifiedProgramId, ownerHandle);
            if (programAssociation.Association != OwnerProgramAssociationType.Explicit)
                throw new ArgumentException(
                    "No owner certified program exists for the specified certifiedProgramId and ownerHandle");

            return DataPortal.Fetch<OwnerCertifiedProgram>(new Criteria(certifiedProgramId, ownerHandle));
        }


        public static void DeleteOwnerCertifiedProgram(Int32 certifiedProgramId, String ownerHandle)
        {
            if (!CanDeleteObject())
                throw new System.Security.SecurityException(
                  "User not authorized to remove an Owner Certified Program");

            DataPortal.Delete(new Criteria(certifiedProgramId, ownerHandle));
        }


        private OwnerCertifiedProgram()
        { /* Require use of factory methods */ }

        #endregion
        
        #region Data Access



        public override OwnerCertifiedProgram Save()
        {
            if (IsDeleted && !CanDeleteObject())
            {
                throw new System.Security.SecurityException
                    ("User not authorized to remove an Owner Certified Program");
            }
            else if (IsNew && !CanAddObject())
            {
                throw new System.Security.SecurityException
                    ("User not authorized to add a Owner Certified Program");
            }
            else if (!CanEditObject())
            {
                throw new System.Security.SecurityException
                    ("User not authorized to update a Owner Certified Program");
            }
            return base.Save();
        }





        [Serializable()]
        private class Criteria
        {
            private readonly Int32 _certifiedProgramId;
            private readonly String _ownerHandle;
            public int CertifiedProgramId
            {
                get { return _certifiedProgramId; }
            }

            public String OwnerHandle
            {
                get { return _ownerHandle; }
            }
            public Criteria(int certifiedProgramId, String ownerHandle)
            { _certifiedProgramId = certifiedProgramId; _ownerHandle = ownerHandle; }
        }


        [RunLocal()]
        protected override void DataPortal_Create()
        {
            //The id is kept as 0 it will be given to the
            //object from the DB when it is inserted into it.
            ValidationRules.CheckRules();
        }



        private void DataPortal_Fetch(Criteria criteria)
        {
            _ownerCertifiedProgramBenefits =
                OwnerCertifiedProgramBenefitCollection.GetOwnerCertifiedProgramBenefitCollection
                    (criteria.CertifiedProgramId, criteria.OwnerHandle);
            
            this._ownerCertifiedProgramId = _ownerCertifiedProgramBenefits.OwnerCertifiedProgramId;
            _certifiedProgramId = criteria.CertifiedProgramId;
            _ownerHandle = criteria.OwnerHandle;
            
            MarkOld();
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Insert()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();

                using(IDbTransaction transaction = connection.BeginTransaction())
                {
                         using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                         {
                             command.CommandType = CommandType.StoredProcedure;
                             command.CommandText = "Certified.OwnerCertifiedProgramBenefitCollection#Create";
                             command.Transaction = transaction;

                             command.AddParameterWithValue("OwnerHandle", DbType.String, false, this._ownerHandle);
                             command.AddParameterWithValue("CertifiedProgramID", DbType.Int32, false, this._certifiedProgramId);
                             command.AddParameterWithValue("InsertUser", DbType.String, false, CslaAppContext.User.Identity.Name);

                             //Add the out param
                             IDataParameter newId = command.AddOutParameter("OwnerCertifiedProgramID", DbType.Int32);

                             command.ExecuteNonQuery();
                             _ownerCertifiedProgramId = (Int32)newId.Value;
                         }
                         transaction.Commit();
                }

                //Fetch and update
                using(IDbTransaction transaction = connection.BeginTransaction())
                {
                    OwnerCertifiedProgramBenefitCollection ownerCertifiedProgramBenefits =
                        OwnerCertifiedProgramBenefitCollection.GetOwnerCertifiedProgramBenefitCollection(
                    _certifiedProgramId, _ownerHandle);

                    foreach (OwnerCertifiedProgramBenefit benefit in _ownerCertifiedProgramBenefits)
                    {
                        OwnerCertifiedProgramBenefit ownerCertifiedProgramBenefit =
                            ownerCertifiedProgramBenefits.FindAccordingToName(benefit.Name);

                        if (ownerCertifiedProgramBenefit != null)
                        {
                            ownerCertifiedProgramBenefit.Text = benefit.Text;
                            ownerCertifiedProgramBenefit.Rank = benefit.Rank;
                            ownerCertifiedProgramBenefit.IsProgramHighlight = benefit.IsProgramHighlight;
                        }

                    }
                    _ownerCertifiedProgramBenefits = ownerCertifiedProgramBenefits;
                    
                    //Now update the kids
                    _ownerCertifiedProgramBenefits.Update(connection, transaction);
                    transaction.Commit();
                }

            }
            MarkOld();
        }




        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            if (!IsDirty) return;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    //Update the kids...
                    _ownerCertifiedProgramBenefits.Update(connection, transaction);
                    
                    transaction.Commit();
                }
                MarkOld();
            }

        }

        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new Criteria(this._certifiedProgramId, _ownerHandle));
        }

        
        //The delete disassociate the owner from the collection
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Delete(Criteria criteria)
        {
	        var programAssociation = GetOwnerCertifiedProgramAssociation(criteria.CertifiedProgramId, criteria.OwnerHandle);
            if (programAssociation.Association != OwnerProgramAssociationType.Explicit)
                throw new ArgumentException(
                    "No owner certified program exists for the specified certifiedProgramId and ownerHandle");


            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();
                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Certified.OwnerCertifiedProgramBenefitCollection#Delete";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("OwnerCertifiedProgramID", DbType.Int32, false, this._ownerCertifiedProgramId);
                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        #endregion Data Access


        #region DataSource
        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public OwnerCertifiedProgram Select(int certifiedProgramId, String ownerHandle)
            {
	            var programAssociation = GetOwnerCertifiedProgramAssociation(certifiedProgramId, ownerHandle);
                return programAssociation.Association == OwnerProgramAssociationType.Explicit  ?
                    GetOwnerCertifiedProgram(certifiedProgramId, ownerHandle) : null;
            }

        }

        # endregion DataSource

        #region Exists


		public class OwnerCertifiedProgramAssociation
		{
			public int CertifiedProgramId { get; private set; }
			public string OwnerHandle { get; private set; }
			public OwnerProgramAssociationType Association { get; private set; }

			public OwnerCertifiedProgramAssociation(int certifiedProgramId, string ownerHandle, OwnerProgramAssociationType assocationType)
			{
				CertifiedProgramId = certifiedProgramId;
				OwnerHandle = ownerHandle;
				Association = assocationType;
			}
		}

		public static OwnerCertifiedProgramAssociation GetOwnerCertifiedProgramAssociation(Int32 certifiedProgramId, String ownerHandle)
        {
            var result = DataPortal.Execute(new GetOwnerCertifiedProgramAssociationCommand(certifiedProgramId, ownerHandle));
            return result.ProgramAssociation;
        }

        [Serializable()]
        private class GetOwnerCertifiedProgramAssociationCommand : CommandBase
        {
            private readonly Int32 _certifiedProgramId;
            private readonly String _ownerHandle;
			private OwnerCertifiedProgramAssociation _programAssociation;

			public OwnerCertifiedProgramAssociation ProgramAssociation
            {
                get { return _programAssociation; }
            }

            public GetOwnerCertifiedProgramAssociationCommand(Int32 certifiedProgramId, String ownerHandle)
            {
                _ownerHandle = ownerHandle;
                _certifiedProgramId = certifiedProgramId;
            }

            protected override void DataPortal_Execute()
            {

                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
                {
                    connection.Open();
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Certified.OwnerCertifiedProgram#GetAssociation";
						command.AddParameterWithValue("CertifiedProgramID", DbType.Int32, false, this._certifiedProgramId);
                        command.AddParameterWithValue("OwnerHandle", DbType.String, false, this._ownerHandle);
						command.AddOutParameter("Result", DbType.Int32);
						var resultParam = (IDataParameter)command.Parameters["Result"];

                        command.ExecuteNonQuery();

						_programAssociation = new OwnerCertifiedProgramAssociation(_certifiedProgramId, _ownerHandle, (OwnerProgramAssociationType) int.Parse(resultParam.Value.ToString()));
                    }
                }
            }
        }

        #endregion

		/// <summary>
		/// Indicates the nature of the participation of a Dealership with a certified program.
		/// 
		/// None = no relationship exists, dealership cannot use this 
		/// 
		/// Implicit = no OwnerCertifiedProgram exists, but the dealership has the program by virtue of a dealer or group program.
		/// 
		/// Explicit = OwnerCertifiedProgram exists - the user has a set of benefits they can modify because these rows exist.  
		/// "Participating" in a program and saving the benefit records creates this type of assocation.
		/// 
		/// </summary>
		public enum OwnerProgramAssociationType
		{
			None = 0, 
			Implicit = 1,
			Explicit = 2
		}
    }
}
