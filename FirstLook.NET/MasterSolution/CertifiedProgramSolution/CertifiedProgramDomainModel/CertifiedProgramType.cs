using System;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public enum CertifiedProgramType
	{
		None = 0,
		Manufacturer = 1,
		Dealer = 2
	}
}
