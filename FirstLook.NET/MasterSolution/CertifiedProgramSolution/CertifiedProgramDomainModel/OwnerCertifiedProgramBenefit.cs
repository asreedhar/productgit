using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
    public class OwnerCertifiedProgramBenefit : BusinessBase<OwnerCertifiedProgramBenefit>, IBenefit
    {

        #region Business Methods

        // TODO: add your own fields, properties and methods
        private int _id;
        private string _name = String.Empty;
        private string _text = String.Empty;
        private Int16 _rank = 0;
        private bool _isProgramHighlight = false;

        public int Id
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("Id", true);
                return _id;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("Id", true);
                if (_id != value)
                {
                    _id = value;
                    PropertyHasChanged("Id");
                }
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return _name;
            }
            set
            {
                CanWriteProperty("Name", true);
                _name = value;
                PropertyHasChanged("Name");
            }
        }

        public string Text
        {
            get
            {
                CanReadProperty("Text", true);
                return _text;
            }
            set
            {
                CanWriteProperty("Text", true);
                _text = value;
                PropertyHasChanged("Text");
            }
        }

        public Int16 Rank
        {
            get
            {
                CanReadProperty("Rank", true);
                return _rank;
            }
            set
            {
                CanWriteProperty("Rank", true);
                _rank = value;
                PropertyHasChanged("Rank");
            }
        }

        public bool IsProgramHighlight
        {
            get
            {
                CanReadProperty("IsProgramHighlight", true);
                return _isProgramHighlight;
            }
            set
            {
                CanWriteProperty("IsProgramHighlight", true);
                _isProgramHighlight = value;
                PropertyHasChanged("IsProgramHighlight");
            }
        }

        #endregion



        #region Factory Methods


        internal static OwnerCertifiedProgramBenefit GetOwnerCertifiedProgramBenefit(IDataReader dr)
        {
            return new OwnerCertifiedProgramBenefit(dr);
        }

        internal static OwnerCertifiedProgramBenefit GetOwnerCertifiedProgramBenefit(CertifiedProgramBenefit certifiedProgramBenefit)
        {
            return new OwnerCertifiedProgramBenefit(certifiedProgramBenefit);
        }

        /// <summary>
        /// Called to when a new object is created.
        /// </summary>
        protected OwnerCertifiedProgramBenefit()
        {/* Require use of factory methods */

            MarkAsChild(); // The benefit is a child contained in CertifiedProgramBenefitCollection
        }

        /// <summary>
        /// Called when loading data from the database.
        /// </summary>
        protected OwnerCertifiedProgramBenefit(IDataRecord dr)
        {
            MarkAsChild(); // The benefit is a child contained in CertifiedProgramBenefitCollection
            Fetch(dr);
        }

        /// <summary>
        /// Called when loading data from a loaded Certified Program.
        /// </summary>
        private OwnerCertifiedProgramBenefit(CertifiedProgramBenefit certifiedProgramBenefit)
        {
            MarkAsChild(); // The benefit is a child contained in CertifiedProgramBenefitCollection
            _id = 0;
            Name = certifiedProgramBenefit.Name;
            Text = certifiedProgramBenefit.Text;
            Rank = certifiedProgramBenefit.Rank;
            IsProgramHighlight = certifiedProgramBenefit.IsProgramHighlight;
            //See if this has to be MarkOld or MarkNew...
            MarkNew();

        }

        



        #endregion Factory Methods

        #region Data Access

        private void Fetch(IDataRecord dr)
        {
            _id = dr.GetInt32(dr.GetOrdinal("OwnerCertifiedProgramBenefitID"));
            Name = dr.GetString(dr.GetOrdinal("Name"));
            Text = dr.GetString(dr.GetOrdinal("Text"));
            Rank = dr.GetInt16(dr.GetOrdinal("Rank"));
            IsProgramHighlight = dr.GetBoolean(dr.GetOrdinal("IsProgramHighlight"));
            MarkOld();
        }

        internal void Update(IDbConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Certified.OwnerCertifiedProgramBenefit#Update";
                command.Transaction = transaction;

                command.AddParameterWithValue("OwnerCertifiedProgramBenefitID ", DbType.Int32, false,Id);
                command.AddParameterWithValue("Rank", DbType.Int16, false, Rank);
                command.AddParameterWithValue("IsProgramHighlight", DbType.Boolean, false, IsProgramHighlight);
                command.AddParameterWithValue("UpdateUser", DbType.String, false, CslaAppContext.User.Identity.Name);

                command.ExecuteNonQuery();
            }
            MarkOld();
        }

        #endregion Data Access

    }
}
