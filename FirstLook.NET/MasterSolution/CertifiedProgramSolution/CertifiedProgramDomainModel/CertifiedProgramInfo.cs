using System;
using System.Data;
using Csla;
using Csla.Data;
using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public class CertifiedProgramInfo : ReadOnlyBase<CertifiedProgramInfo>
    {
        #region Business Methods

		public int Id { get; private set; }

		public string Name { get; private set; }

		public string MarketingName { get; set; }

		public int? BusinessUnitId { get; private set; }

		public CertifiedProgramType ProgramType { get; private set; }

		public string Text { get; private set; }

		public bool Active { get; private set; }

		protected override object GetIdValue()
		{
			return Id;
        }
        #endregion Business Methods

       #region Factory Methods

        internal static CertifiedProgramInfo GetCertifiedProgramInfo(IDataRecord record)
        {
          return new CertifiedProgramInfo(record);
        }

        private CertifiedProgramInfo()
        {
	        Name = String.Empty;
	        Text = String.Empty;
/* require use of factory methods */
        }

		private CertifiedProgramInfo(IDataRecord record)
		{
			Name = String.Empty;
			Text = String.Empty;
			Fetch(record);
		}

		#endregion Factory Methods


        #region Data Access

        private void Fetch(IDataRecord record)
        {
          // load values
            Id = record.GetInt32(record.GetOrdinal("CertifiedProgramID"));
			var o = record.GetValue(record.GetOrdinal("BusinessUnitId"));
	        BusinessUnitId = o == null ? new int?() : (int) o;
            Name = record.GetString(record.GetOrdinal("Name"));
            Text = record.GetString(record.GetOrdinal("Text"));
            Active = record.GetBoolean(record.GetOrdinal("Active"));
	        MarketingName = record.GetString(record.GetOrdinal("MarketingName"));
	        ProgramType = BusinessUnitId == null ? CertifiedProgramType.Manufacturer : CertifiedProgramType.Dealer;
        }


		#endregion Data Access
    }
}
