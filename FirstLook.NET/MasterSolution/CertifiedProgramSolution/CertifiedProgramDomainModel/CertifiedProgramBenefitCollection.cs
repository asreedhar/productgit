using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using System.ComponentModel;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public class CertifiedProgramBenefitCollection : BusinessListBase<CertifiedProgramBenefitCollection, CertifiedProgramBenefit>
    {

        #region Business Methods
        private int _certifiedProgramId = 0;

		public int CertifiedProgramId
		{
			get { return _certifiedProgramId; }
		}

		
        public void ToggleHighlight(int certifiedProgramBenefitId)
		{
            this[certifiedProgramBenefitId].IsProgramHighlight =
                !this[certifiedProgramBenefitId].IsProgramHighlight;
		}

        
        
        
        public void RemoveCertifiedProgramBenefit(CertifiedProgramBenefit certifiedProgramBenefit)
        {
            //Increase all that is under
            CertifiedProgramBenefit currentBenefit =
                FindAccordingToName(certifiedProgramBenefit.Name);

            if (currentBenefit == null)
            {
                throw new ArgumentException("The certified program benefit name was not found in the collection");
            }

            if (currentBenefit.Rank < this.Count)  // Was not the last one there is a reason to promote the others
            {
                for (Int32 i = currentBenefit.Rank + 1; i <= this.Count; i++)
                {
                    CertifiedProgramBenefit benefit = FindAccordingToRank(i);
                    if (benefit == null)
                    {
                        break; // all has been promoted
                    }
                    IncreaseRank(benefit.Name);
                }
            }
            Remove(currentBenefit);
        }

        public void AddCertifiedProgramBenefit(CertifiedProgramBenefit certifiedProgramBenefit)
        {
            if (FindAccordingToName(certifiedProgramBenefit.Name) != null)
            {
                throw new ArgumentException("The Benefit List contains a benefit the provided name");
            }
            
            // Add the program.  Rank is already set.  ZB
            Add(certifiedProgramBenefit);
        }
        


        //NTODO: Support ranking + make sure to support the addition and the deletion
        public void IncreaseRank(String certifiedProgramBenefitName)
		{//Higher ranks are lower numbers
            
            CertifiedProgramBenefit currentBenefit =
                FindAccordingToName(certifiedProgramBenefitName);

            if (currentBenefit == null)
            {
                throw new ArgumentException("The certified program benefit name was not found in the collection");
            }

            if (currentBenefit.Rank <= 1) return; //nothing to promote
           
            CertifiedProgramBenefit aboveBenefit =
                FindAccordingToRank(currentBenefit.Rank - 1);

            if (aboveBenefit == null)
            {
                throw new InvalidOperationException("The certified program benefit name was found in the collection but no benefit ranked directly above it has been found");
            }

            Int16 currntRank = currentBenefit.Rank;
            aboveBenefit.Rank = currntRank;
           
            //do the promotion
            currentBenefit.Rank--;
		}

		public void DecreaseRank(String certifiedProgramBenefitName)
		{//Lower ranks are higher numbers
            CertifiedProgramBenefit currentBenefit =
                FindAccordingToName(certifiedProgramBenefitName);

            if (currentBenefit == null)
            {
                throw new ArgumentException("The certified program benefit name was not found in the collection");
            }

            if (currentBenefit.Rank >= this.Count) return; //nothing to demote

            CertifiedProgramBenefit belowBenefit =
                FindAccordingToRank(currentBenefit.Rank + 1);

            if (belowBenefit == null)
            {
                throw new InvalidOperationException("The certified program benefit name was found in the collection but no benefit ranked directly below it has been found");
            }

            Int16 currntRank = currentBenefit.Rank;
            belowBenefit.Rank = currntRank;

            //do the demotion
            currentBenefit.Rank++;

		}

        //internal util to return a CertifiedProgramBenefit accoding to the rank
        private CertifiedProgramBenefit FindAccordingToRank(Int32 rank)
        {
            foreach (CertifiedProgramBenefit certifiedProgramBenefit in Items)
            {
                if (certifiedProgramBenefit.Rank == rank) 
                    return certifiedProgramBenefit;
            }
            return null;
        }

        //internal util to return a CertifiedProgramBenefit accoding to the Id
        public CertifiedProgramBenefit FindAccordingToId(Int32 id)
        {
            foreach (CertifiedProgramBenefit certifiedProgramBenefit in Items)
            {
                if (certifiedProgramBenefit.Id == id)
                    return certifiedProgramBenefit;
            }
            return null;
        }


        //internal util to return a CertifiedProgramBenefit accoding to the Name
        private CertifiedProgramBenefit FindAccordingToName(String name)
        {
            foreach (CertifiedProgramBenefit certifiedProgramBenefit in Items)
            {
                if (certifiedProgramBenefit.Name.Equals(name))
                    return certifiedProgramBenefit;
            }
            return null;
        }

        #endregion Business Methods


        #region Factory Methods
        internal static CertifiedProgramBenefitCollection NewCertifiedProgramBenefitCollection()
        {
          return new CertifiedProgramBenefitCollection();
        }

        internal static CertifiedProgramBenefitCollection GetCertifiedProgramBenefitCollection(Int32 certifiedProgramId)
        {
            return new CertifiedProgramBenefitCollection(certifiedProgramId);
        }


        protected CertifiedProgramBenefitCollection()
        {
          MarkAsChild();
        }

        protected CertifiedProgramBenefitCollection(Int32 certifiedProgramId)
        {
            _certifiedProgramId = certifiedProgramId;
            MarkAsChild();
            Fetch(certifiedProgramId);
        }

        
        #endregion Factory Methods


        #region Data Access


        private void Fetch(int certifiedProgramId)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Certified.CertifiedProgramBenefitCollection#Fetch";
                    command.AddParameterWithValue("CertifiedProgramID", DbType.Int32, false, certifiedProgramId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            this.Add(CertifiedProgramBenefit.GetCertifiedProgramBenefit(reader));
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }


        internal void Update(IDbConnection connection, IDbTransaction transaction)
        {
            RaiseListChangedEvents = false;

            foreach (CertifiedProgramBenefit certifiedProgramBenefit in this)
            {
                if (certifiedProgramBenefit.IsNew)
                    certifiedProgramBenefit.Insert(this._certifiedProgramId, connection, transaction);
                else
                    certifiedProgramBenefit.Update(connection, transaction);
            }
            
            foreach (CertifiedProgramBenefit certifiedProgramBenefit in DeletedList)
            {
                certifiedProgramBenefit.DeleteSelf(connection,transaction);
            }
            DeletedList.Clear();


            RaiseListChangedEvents = true;
        }

        #endregion Data Access

        #region DataSource
        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public CertifiedProgramBenefitCollection Select(Int32 certifiedProgramId)
            {
                return GetCertifiedProgramBenefitCollection(certifiedProgramId);
            }

        }

        #endregion DataSource


    }
}
