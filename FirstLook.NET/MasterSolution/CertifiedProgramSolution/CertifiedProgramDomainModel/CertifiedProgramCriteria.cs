using System;
using System.ComponentModel;
using Csla;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public class CertifiedProgramCriteria : BusinessBase<CertifiedProgramCriteria>
	{
	    private int _certifiedProgramId = 0;
	    private int _maxAgeInYears = 0;
	    private int _maxOdometer = 0;

		public int CertifiedProgramId
		{
			get { return _certifiedProgramId; }
		}

		public int MaxAgeInYears
		{
			get { return _maxAgeInYears; }
			set { _maxAgeInYears = value; }
		}

		public int MaxOdometer
		{
			get { return _maxOdometer; }
			set { _maxOdometer = value; }
		}

		protected override object GetIdValue()
		{
			return CertifiedProgramId;
		}

		[DataObject]
		public class DataSource
		{
			[DataObjectMethod(DataObjectMethodType.Select)]
			public CertifiedProgramCriteria Select(int certifiedProgramId)
			{
				return null;
			}

			[DataObjectMethod(DataObjectMethodType.Insert)]
			public void Insert(int maxAgeInYears, int maxOdometer)
			{

			}

			[DataObjectMethod(DataObjectMethodType.Update)]
			public void Update(int certifiedProgramId, int maxAgeInYears, int maxOdometer)
			{

			}
		}
	}
}
