using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace FirstLook.CertifiedProgram.DomainModel
{
    public static class Database
    {
        private static String CPO_DB_NAME = "IMT";
        
        public static string CPOConnection
        {
            get
            {
                return ConfigurationManager.ConnectionStrings
                  [CPO_DB_NAME].ConnectionString;
            }
        }

        public static String CPOConnectionName
        {
            get
            {
                return CPO_DB_NAME;
            }
        }
    }
}
