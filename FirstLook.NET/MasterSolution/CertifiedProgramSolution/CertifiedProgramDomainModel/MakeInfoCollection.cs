using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public class MakeInfoCollection : ReadOnlyListBase<MakeInfoCollection, MakeInfo>
	{

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return CslaAppContext.User.IsInRole("Administrator");
        }

        #endregion

       #region Factory Methods

        public static MakeInfoCollection GetMakeInfoCollection()
        {
          return DataPortal.Fetch<MakeInfoCollection>(new Criteria());
        }

        private MakeInfoCollection()
        { /* require use of factory methods */ }

        #endregion


        #region Data Access
        [Serializable()]
        protected class Criteria
        { /* no criteria - retrieve all CertifiedProgramInfos which are active */ }


    
    private void DataPortal_Fetch(Criteria criteria)
    {

      RaiseListChangedEvents = false;
      IsReadOnly = false;
      
      using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
      {
        connection.Open();
        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
	    {
		    command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "Certified.MakeInfoCollection#Fetch";

            using (IDataReader reader = command.ExecuteReader())
		    {
			    while (reader.Read())
			    {
				    Add(MakeInfo.GetMakeInfo(reader));
			    }
		    }
	    }
      }

      IsReadOnly = true;
      RaiseListChangedEvents = true;
    }

    #endregion Data Access

    #region DataSource
        [DataObject]
		public class DataSource
		{
			[DataObjectMethod(DataObjectMethodType.Select)]
			public MakeInfoCollection Select()
			{
			    return GetMakeInfoCollection();
			}
		}
     #endregion DataSource
	}
}
