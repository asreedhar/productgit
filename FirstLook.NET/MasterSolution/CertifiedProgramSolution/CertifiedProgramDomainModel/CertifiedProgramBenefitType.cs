using System;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public enum CertifiedProgramBenefitType
	{
		Unknown,
		BasicWarranty,
		DriveTrainWarranty,
		AdditionalWarranty,
		ExtendedWarranty,
		CorrosionProtection,
		WarrantyUpgrades,
		CustomerService,
		Deductible,
		Financing,
		Return,
		History,
		Inspection,
		LoanVehicle,
		Other,
		RoadsideAssistance,
		Transferrable,
		Trials,
		TripInterruption,
		TripPlanning
	}
}
