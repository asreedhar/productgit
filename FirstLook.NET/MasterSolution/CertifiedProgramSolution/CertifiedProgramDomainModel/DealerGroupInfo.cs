using System;
using System.ComponentModel;
using Csla;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public class DealerGroupInfo : ReadOnlyBase<DealerGroupInfo>
	{
	    private int _id = 0;
	    private String _name = String.Empty;
	    private CertifiedProgramInfo _certifiedProgram = null;

		public int Id
		{
			get { return _id; }
		}

		public String Name
		{
			get { return _name; }
		}

		public CertifiedProgramInfo CertifiedProgram
		{
			get { return _certifiedProgram; }
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		[DataObject]
		public class DataSource
		{
			[DataObjectMethod(DataObjectMethodType.Select)]
			public DealerGroupInfo Select(int dealerGroupId)
			{
				return null;
			}
		}
	}
}
