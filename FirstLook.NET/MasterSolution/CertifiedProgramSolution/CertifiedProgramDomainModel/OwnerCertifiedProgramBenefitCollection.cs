using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
    public class OwnerCertifiedProgramBenefitCollection : BusinessListBase<OwnerCertifiedProgramBenefitCollection, OwnerCertifiedProgramBenefit>
	{
	    #region Business Methods
        private String _ownerHandle = String.Empty;
        private Int32 _ownerCertifiedProgramId = 0;

		public string OwnerHandle
		{
			get
			{
			    return _ownerHandle;
			}
            set
            {
                _ownerHandle = value;
            }
		}

	    public Int32 OwnerCertifiedProgramId
	    {
	        get
	        {
                return _ownerCertifiedProgramId;
	        }
	    }

        public void IncreaseRank(String ownerCertifiedProgramBenefitName)
        {//Higher ranks are lower numbers

            OwnerCertifiedProgramBenefit currentBenefit =
                FindAccordingToName(ownerCertifiedProgramBenefitName);

            if (currentBenefit == null)
            {
                throw new ArgumentException("The owner certified program benefit name was not found in the collection");
            }

            if (currentBenefit.Rank <= 1) return; //nothing to promote

            OwnerCertifiedProgramBenefit aboveBenefit =
                FindAccordingToRank(currentBenefit.Rank - 1);

            if (aboveBenefit == null)
            {
                throw new InvalidOperationException("The owner certified program benefit name was found in the collection but no benefit ranked directly above it has been found");
            }

            Int16 currntRank = currentBenefit.Rank;
            aboveBenefit.Rank = currntRank;

            //do the promotion
            currentBenefit.Rank--;
        }

        public void DecreaseRank(String ownerCertifiedProgramBenefitName)
        {//Lower ranks are higher numbers
            OwnerCertifiedProgramBenefit currentBenefit =
                FindAccordingToName(ownerCertifiedProgramBenefitName);

            if (currentBenefit == null)
            {
                throw new ArgumentException("The owner certified program benefit name was not found in the collection");
            }

            if (currentBenefit.Rank >= this.Count) return; //nothing to demote

            OwnerCertifiedProgramBenefit belowBenefit =
                FindAccordingToRank(currentBenefit.Rank + 1);

            if (belowBenefit == null)
            {
                throw new InvalidOperationException("The owner certified program benefit name was found in the collection but no benefit ranked directly below it has been found");
            }

            Int16 currntRank = currentBenefit.Rank;
            belowBenefit.Rank = currntRank;

            //do the demotion
            currentBenefit.Rank++;

        }

        //internal util to return a CertifiedProgramBenefit accoding to the rank
        private OwnerCertifiedProgramBenefit FindAccordingToRank(Int32 rank)
        {
            foreach (OwnerCertifiedProgramBenefit certifiedProgramBenefit in Items)
            {
                if (certifiedProgramBenefit.Rank == rank)
                    return certifiedProgramBenefit;
            }
            return null;
        }

        //internal util to return a CertifiedProgramBenefit accoding to the Id
        public OwnerCertifiedProgramBenefit FindAccordingToId(Int32 id)
        {
            foreach (OwnerCertifiedProgramBenefit certifiedProgramBenefit in Items)
            {
                if (certifiedProgramBenefit.Id == id)
                    return certifiedProgramBenefit;
            }
            return null;
        }


        //internal util to return a CertifiedProgramBenefit accoding to the Name
        public OwnerCertifiedProgramBenefit FindAccordingToName(String name)
        {
            foreach (OwnerCertifiedProgramBenefit certifiedProgramBenefit in Items)
            {
                if (certifiedProgramBenefit.Name.Equals(name))
                    return certifiedProgramBenefit;
            }
            return null;
        }



		
        #endregion Business Methods
        
        #region Factory Methods

        public static OwnerCertifiedProgramBenefitCollection NewOwnerCertifiedProgramBenefitCollection()
        {
            return new OwnerCertifiedProgramBenefitCollection();
        }


        public static OwnerCertifiedProgramBenefitCollection AssociatedOwnerCertifiedProgramBenefitCollection(Int32 certifiedProgramId, String ownerHandle)
        {
            CertifiedProgramBenefitCollection certifiedProgramBenefits =
                CertifiedProgramBenefitCollection.GetCertifiedProgramBenefitCollection(certifiedProgramId);

            OwnerCertifiedProgramBenefitCollection ownerCertifiedProgramBenefits =
                new OwnerCertifiedProgramBenefitCollection();
            
            
            foreach (CertifiedProgramBenefit benefit in certifiedProgramBenefits)
            {
                ownerCertifiedProgramBenefits.Add
                    (OwnerCertifiedProgramBenefit.GetOwnerCertifiedProgramBenefit(benefit));
            }

            ownerCertifiedProgramBenefits.OwnerHandle = ownerHandle;
            return ownerCertifiedProgramBenefits;
        }



        public static OwnerCertifiedProgramBenefitCollection GetOwnerCertifiedProgramBenefitCollection(Int32 certifiedProgramId, String ownerHandle)
        {
            return new OwnerCertifiedProgramBenefitCollection(certifiedProgramId, ownerHandle);
        }

        private OwnerCertifiedProgramBenefitCollection()
        {
            MarkAsChild();
        }

        private OwnerCertifiedProgramBenefitCollection(Int32 certifiedProgramId, String ownerHandle)
        {
            MarkAsChild();
            Fetch(certifiedProgramId, ownerHandle);

        }

        #endregion Factory Methods

        

        #region Data Access

        //[Serializable()]
        //private class Criteria
        //{
        //    private readonly Int32 _id;
        //    private readonly String _ownerHandle;
        //    public int Id
        //    {
        //        get { return _id; }
        //    }

        //    public String OwnerHandle
        //    {
        //        get { return _ownerHandle; }
        //    }
        //    public Criteria(int id, String ownerHandle)
        //    { _id = id; _ownerHandle = ownerHandle; }
        //}


        private void Fetch(int certifiedProgramId, String ownerHandle)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Certified.OwnerCertifiedProgramBenefitCollection#Fetch";

                    //Ins
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("CertifiedProgramID", DbType.Int32, false, certifiedProgramId);

                    //Outs
                    IDataParameter id = command.AddOutParameter("OwnerCertifiedProgramID", DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            this.Add(OwnerCertifiedProgramBenefit.GetOwnerCertifiedProgramBenefit(reader));
                        }
                    }
                    //Set the out param after the reader has closed
                    _ownerCertifiedProgramId = (Int32)id.Value;
                }
            }
            
            RaiseListChangedEvents = true;
        }





        internal void Update(IDbConnection connection, IDbTransaction transaction)
        {
            RaiseListChangedEvents = false;

            foreach (OwnerCertifiedProgramBenefit ownerCertifiedProgramBenefit in this)
            {
                ownerCertifiedProgramBenefit.Update(connection, transaction);
            }

            RaiseListChangedEvents = true;
        }


        #endregion Data Access


        # region DataSource
        [DataObject]
		public class DataSource
		{
            [DataObjectMethod(DataObjectMethodType.Select)]
            public OwnerCertifiedProgramBenefitCollection Select(Int32 certifiedProgramId, String ownerHandle)
            {
                return GetOwnerCertifiedProgramBenefitCollection(certifiedProgramId, ownerHandle);
            }
        }
        # endregion DataSource
	    
	}
}
