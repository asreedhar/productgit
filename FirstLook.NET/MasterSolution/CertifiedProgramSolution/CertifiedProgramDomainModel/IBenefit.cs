using System;

namespace FirstLook.CertifiedProgram.DomainModel
{
    public interface IBenefit
    {
        int Id { get; }
        string Name { get;}
        string Text { get;}
        Int16 Rank { get;}
        bool IsProgramHighlight { get;}
        
    }
}
