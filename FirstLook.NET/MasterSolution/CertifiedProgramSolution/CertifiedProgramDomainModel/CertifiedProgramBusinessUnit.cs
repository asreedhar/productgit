using System;
using System.Data;
using Csla;

namespace FirstLook.CertifiedProgram.DomainModel
{
	public class CertifiedProgramBusinessUnit : ReadOnlyBase<CertifiedProgramBusinessUnit>
	{
		public int Id { get; private set; }
		public string Name { get; private set; }

		private CertifiedProgramBusinessUnit(IDataReader reader)
		{
			Fetch(reader);
		}

		protected override object GetIdValue()
		{
			return Id;
		}

		public static CertifiedProgramBusinessUnit GetCertifiedProgramBusinessUnit(IDataReader reader)
		{
			return new CertifiedProgramBusinessUnit(reader);
		}

		#region Data Access

		private void Fetch(IDataRecord record)
        {
          // load values
            Id = record.GetInt32(record.GetOrdinal("BusinessUnitId"));
            Name = record.GetString(record.GetOrdinal("BusinessUnit"));
        }

        #endregion Data Access
	}
}