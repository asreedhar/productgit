using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Csla;
using Csla.Data;
using System.Security;
using System.Data.SqlClient;
using System.Data;
using FirstLook.Common.Data;

using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel
{
	[Serializable]
	public class CertifiedProgram : BusinessBase<CertifiedProgram>
	{

        #region Business Methods

        private Int32 _id = 0;
		private int? _businessUnitId = null;
        private String _name = String.Empty;
		private String _marketingName = String.Empty;
		private String _text = String.Empty;
        private CertifiedProgramType _certifiedProgramType = CertifiedProgramType.None;
        private bool _active = false;
        private String _icon = String.Empty;

	    private CertifiedProgramBenefitCollection _certifiedProgramBenefits =
	        CertifiedProgramBenefitCollection.NewCertifiedProgramBenefitCollection();


        [DataObjectField(true, true)]
        public int Id
        {
            get
            {
                CanReadProperty("Id", true);
                return _id;
            }
        }

        //Protected for derived classed
        protected void SetId(Int32 certifiedProgramId)
        {
            _id = certifiedProgramId;
        }
        
        public string Name
        {
            get 
            {
                CanReadProperty("Name", true);
                return _name; 
            }
            set 
            {
                CanWriteProperty("Name", true);
                if (value == null) value = String.Empty;
                if (_name != value)
                {
                    _name = value;
                    PropertyHasChanged("Name");
                }
            }
        }

		public string MarketingName
		{

			get
			{
				CanReadProperty("MarketingName", true);
				return _marketingName;
			}
			set
			{
				CanWriteProperty("MarketingName", true);
				if (value == null) value = String.Empty;
				if (_marketingName != value)
				{
					_marketingName = value;
					PropertyHasChanged("MarketingName");
				}
			}

		}

        public string Text
        {
            get 
            {
                CanReadProperty("Text", true);
                return _text; 
            }
            set 
            {
                CanWriteProperty("Text", true);
                if (value == null) value = string.Empty;
                if (_text != value)
                {
                    _text = value;
                    PropertyHasChanged("Text");
                }

            }
        }

		public int? BusinessUnitId
		{
			get
			{
				CanReadProperty("BusinessUnitId", true);
				return _businessUnitId; 
			}
			set
			{
				CanWriteProperty("BusinessUnitId", true);
				if (_businessUnitId != value)
				{
					_businessUnitId = value;
					PropertyHasChanged("BusinessUnitId");
				}
			}
		}

		public CertifiedProgramType CertifiedProgramType
        {
            get
            {
                CanReadProperty("CertifiedProgramType", true);
                return _certifiedProgramType;
            }
            set
            {
                CanWriteProperty("CertifiedProgramType", true);
                if (_certifiedProgramType != value)
                {
                    _certifiedProgramType = value;
                    PropertyHasChanged("CertifiedProgramType");
                }
            }
        }

        public bool Active
        {
            get 
            {
                CanReadProperty("Active", true);
                return _active; 
            }
            set 
            {
                CanWriteProperty("Active", true);
                _active = value;
                PropertyHasChanged("Active");
            }
        }

        public CertifiedProgramBenefitCollection CertifiedProgramBenefits
        {
            get
            {
                CanReadProperty("CertifiedProgramBenefits", true);
                return _certifiedProgramBenefits;
            }
        }


        public string Icon
        {
            get
            {
                CanReadProperty("Icon", true);
                return _icon;
            }
            set
            {
                CanWriteProperty("Icon", true);
                if (value == null) value = string.Empty;
                if (_icon != value)
                {
                    _icon = value;
                    PropertyHasChanged("Icon");
                }
            }
        }

        //For derived classes
        protected void SetCertifiedProgramBenefits(CertifiedProgramBenefitCollection certifiedProgramBenefits)
        {
             if (!CanEditObject())
              throw new SecurityException("User not authorized to update a Certified Program");
            
            if (certifiedProgramBenefits == null)
            {
                _certifiedProgramBenefits = 
                    CertifiedProgramBenefitCollection.NewCertifiedProgramBenefitCollection();
            }
            else
            {
                _certifiedProgramBenefits = certifiedProgramBenefits;
            }

            MarkDirty();
        }


        public override bool IsValid
        {
            get
            {
                return base.IsValid && _certifiedProgramBenefits.IsValid;
            }
        }

        public override bool IsDirty
        {
            get { return base.IsDirty || _certifiedProgramBenefits.IsDirty; }
        }

        // Return unique id value for object this is to support
        // Equals(), GetHashCode(), and ToString() given to us by CSLA
        protected override object GetIdValue()
        {
            return Id;
        }


        #endregion

        
        #region Validation Rules

        //NTODO: add business rules after looking at the BusinessRules in the DB
        protected override void AddBusinessRules()
        {
            // NTODO: add validation rules
        }

        #endregion


        #region Authorization Rules

        //NTODO add the AuthorizationRules
        protected override void AddAuthorizationRules()
        {
                // NTODO: add validation rules        
        }


        public static bool CanAddObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }

        public static bool CanGetObject()
        {
            return CslaAppContext.User.IsInRole(
              "Administrator");
        }


        public static bool CanDeleteObject()
        {
            //This is to support derived class the user will still get an exception
            //if DeleteCertifiedProgram is called
            return CslaAppContext.User.IsInRole(
             "Administrator");
        }

        public static bool CanEditObject()
        {
            return CslaAppContext.User.IsInRole(
                "Administrator");
        }

        #endregion
        

        #region Factory Methods

        public static CertifiedProgram NewCertifiedProgram()
        {
            if (!CanAddObject())
            {
                throw new SecurityException("User not authorized to add a certified program");
            }
            return DataPortal.Create<CertifiedProgram>();
        }

        public static CertifiedProgram GetCertifiedProgram(int id)
        {

            if (!CanGetObject())
                throw new SecurityException("User not authorized to view certified program");

            return DataPortal.Fetch<CertifiedProgram>(new Criteria(id));
        }

        public static void DeleteCertifiedProgram(int id)
        {
            new InvalidOperationException("CertifiedPrograms are not supposed to be deleted");
        }

        protected CertifiedProgram()
        {  /* Require use of factory methods */ 
        }



        #endregion


        #region Data Access
        
        [Serializable()]
        protected class Criteria
        {
            private Int32 _id;
            public Int32 Id
            {
                get { return _id; }
            }

            public Criteria(Int32 id)
            { _id = id; }
        }

        [RunLocal()]
        protected override void DataPortal_Create()
        {
           //The id is kept as 0 it will be given to the
           //object from the DB when it is inserted into it.
           ValidationRules.CheckRules();
        }


        public override CertifiedProgram Save()
        {
            if (IsDeleted && !CanDeleteObject())
            {
                throw new System.Security.SecurityException
                    ("User not authorized to remove a Certified Program");
            }
            else if (IsNew && !CanAddObject())
            {
                throw new System.Security.SecurityException
                    ("User not authorized to add a Certified Program");
            }
            else if (!CanEditObject())
            {
                throw new System.Security.SecurityException
                    ("User not authorized to update a Certified Program");
            }
            return base.Save();
        }


        [Transactional(TransactionalTypes.Manual)]
        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();

                using(IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Certified.CertifiedProgram#Fetch";
                        command.AddParameterWithValue("CertifiedProgramID",DbType.Int32,false,criteria.Id);
                        command.Transaction = transaction;

                        using (var dr = new SafeDataReader(command.ExecuteReader()))
                        {
                            if(dr.Read())
                            {
                                _id = dr.GetInt32(dr.GetOrdinal("CertifiedProgramID"));

								var o = dr.GetValue(dr.GetOrdinal("BusinessUnitId"));
								BusinessUnitId = o == null ? new int?() : (int)o;
        
								Name = dr.GetString(dr.GetOrdinal("Name"));
								MarketingName = dr.GetString(dr.GetOrdinal("MarketingName"));
                                Text = dr.GetString(dr.GetOrdinal("Text"));
                                Active = dr.GetBoolean(dr.GetOrdinal("Active"));
                                Icon = dr.GetString(dr.GetOrdinal("Icon"));

                                // Load the child objects
                                _certifiedProgramBenefits =
                                    CertifiedProgramBenefitCollection.GetCertifiedProgramBenefitCollection(this._id);
                            }
                            else
                            {
                                throw new DataException("Missing Certified Program information"); 
                            }
                        }
                    }

                }
            }
            MarkOld();
        }

        [Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
       {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();
                
                using(IDbTransaction transaction = connection.BeginTransaction())
                {

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Certified.CertifiedProgram#Insert";
                        command.Transaction = transaction;
	                    command.AddParameterWithValue("BusinessUnitId", DbType.Int32, true, BusinessUnitId);
                        command.AddParameterWithValue("Name", DbType.String, false, Name);
						command.AddParameterWithValue("MarketingName", DbType.String, true, MarketingName);
                        command.AddParameterWithValue("Text", DbType.String, false, Text);
                        command.AddParameterWithValue("Icon", DbType.String, false, Icon);
                        // the initial state of the new certified program should be set to true
                        command.AddParameterWithValue("Active", DbType.Boolean, false, true);
                        command.AddParameterWithValue("InsertUser", DbType.String, false, CslaAppContext.User.Identity.Name);

                        //Add the out param
                        IDataParameter newId = command.AddOutParameter("CertifiedProgramID", DbType.Int32);

                        command.ExecuteNonQuery();

                        _id = (Int32)newId.Value;
                        
                        //Update the kids...

                        this.CertifiedProgramBenefits.Update(connection,transaction);
                    }
                    transaction.Commit();
                }

                MarkOld();
            }
        }

        [Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
        {
            if (!IsDirty) return;
            
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Certified.CertifiedProgram#Update";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("CertifiedProgramID", DbType.Int32, false, _id);
						// BusinessUnitId is not updateable for now.  
						// I tried to get it to work nicely on the admin page, but it was too much a hassle.  If we really need it, we can revisit.
						//command.AddParameterWithValue("BusinessUnitId", DbType.Int32, true, BusinessUnitId);
						command.AddParameterWithValue("Name", DbType.String, false, Name);
						command.AddParameterWithValue("MarketingName", DbType.String, true, MarketingName);
						command.AddParameterWithValue("Text", DbType.String, false, Text);
                        command.AddParameterWithValue("Icon", DbType.String, false, Icon);
                        command.AddParameterWithValue("Active", DbType.Boolean, false, Active);
                        command.AddParameterWithValue("UpdateUser", DbType.String, false, CslaAppContext.User.Identity.Name);

                        command.ExecuteNonQuery();

                        //Update the kids...
                        CertifiedProgramBenefits.Update(connection, transaction);
                    }
                    transaction.Commit();
                }
                MarkOld();
            }
            
        }

        #endregion


        # region DataSource
        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public CertifiedProgram Select(int certifiedProgramId)
            {
                return Exists(certifiedProgramId)? 
                    GetCertifiedProgram(certifiedProgramId) : null;
            }

	        /// <summary>
	        /// 
	        /// </summary>
	        /// <param name="name"></param>
	        /// <param name="text"></param>
	        /// <param name="businessUnitId"></param>
	        /// <param name="icon"></param>
	        /// <param name="marketingName"></param>
	        /// <returns></returns>
	        [DataObjectMethod(DataObjectMethodType.Insert)]
            public CertifiedProgram Insert(String name, String text, int? businessUnitId, string icon, string marketingName)
            {
                CertifiedProgram certifiedProgram = NewCertifiedProgram();
                certifiedProgram.Name = name;
		        certifiedProgram.BusinessUnitId = businessUnitId;
                certifiedProgram.Text = text;
                certifiedProgram.Icon = icon;
                certifiedProgram.Active = true;
                certifiedProgram.MarketingName = marketingName;
                certifiedProgram.Save();
                return certifiedProgram;
            }

            [DataObjectMethod(DataObjectMethodType.Update)]
            public void Update(Int32 certifiedProgramId, String name, string text, string icon, bool active, string marketingName)
            {
				// not allowing the update of BusinessUnitId for now.  There is a work around: delete or deactivate the program and create a new one at the new buid.
                CertifiedProgram certifiedProgram = GetCertifiedProgram(certifiedProgramId);
                certifiedProgram.Name = name;
				certifiedProgram.Text = text;
                certifiedProgram.Icon = icon;
                certifiedProgram.Active = active;
                certifiedProgram.MarketingName = marketingName;
				certifiedProgram.Save();
            }
        }


		# endregion 


        #region Exists

        public static bool Exists(Int32 id)
        {
            ExistsCommand result = 
                DataPortal.Execute<ExistsCommand>(new ExistsCommand(id));
            return result.Exists;
        }

        public static bool Exists(String name)
        {
            ExistsCommand result =
                DataPortal.Execute<ExistsCommand>(new ExistsCommand(name));
            return result.Exists;
        }



        [Serializable()]
        private class ExistsCommand : CommandBase
        {
            private Int32 _id = 0;
            private String _name = String.Empty;
            private Boolean _exists = false;

            public bool Exists
            {
                get { return _exists; }
            }

            public ExistsCommand(Int32 id)
            {
                _id = id;
            }

            public ExistsCommand(String name)
            {
                _name = name;
            }

            protected override void DataPortal_Execute()
            {

                    using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.CPOConnectionName))
                    {
                            connection.Open();
                            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                if (_id != 0)
                                {
                                    command.CommandText = "Certified.CertifiedProgram#Exists";
                                      command.AddParameterWithValue("CertifiedProgramID", DbType.Int32, false, this._id);
                                }
                                else
                                {
                                      command.CommandText = "Certified.CertifiedProgram#ExistsByName";
                                      command.AddParameterWithValue("Name", DbType.String, false, this._name);

                                }

                                // create return value parameter
                                IDataParameter parameter = command.CreateParameter();
                                parameter.ParameterName = "ReturnValue";
                                parameter.DbType = DbType.Int32;
                                parameter.Direction = ParameterDirection.ReturnValue;
                                command.Parameters.Add(parameter);
                                
                                int count = 0;
                                command.ExecuteNonQuery();
                                IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                                if (retParem != null)
                                {
                                    count = Convert.ToInt32(retParem.Value);
                                }
                                _exists = (count > 0);
                            }
                    }
            }
        }
        
    #endregion

	}
}
