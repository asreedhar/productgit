using System;
using System.Data;
using Csla;
using CslaAppContext = Csla.ApplicationContext;

namespace FirstLook.CertifiedProgram.DomainModel{

	[Serializable]
	public class MakeInfo : ReadOnlyBase<MakeInfo>
    {
        #region Business Methods
        private Int32 _id = 0;
	    private String _name = String.Empty;
	    private Int32? _certifiedProgramId = null;

		public int Id
		{
			get { return _id; }
		}

		public string Name
		{
			get { return _name; }
		}

	    public Int32? CertifiedProgramId
	    {
	        get
	        {
	            return _certifiedProgramId;
	        }
	    }

		protected override object GetIdValue()
		{
			return Id;
        }
        #endregion Business Methods

        
    #region Factory Methods

    internal static MakeInfo GetMakeInfo(IDataRecord record)
    {
      return new MakeInfo(record);
    }

    private MakeInfo()
    { /* require use of factory methods */ 
    }

    private MakeInfo(IDataRecord record)
    {
        Fetch(record);
    }

    #endregion Factory Methods

    #region Data Access

    private void Fetch(IDataRecord record)
    {
      // load values
        _id = record.GetInt32(record.GetOrdinal("MakeId"));
        _name = record.GetString(record.GetOrdinal("Name"));

        Boolean programIdIsNull = record.IsDBNull(record.GetOrdinal("CertifiedProgramID"));
        if (programIdIsNull)
        {
            _certifiedProgramId = null;
        }
        else
        {
            _certifiedProgramId = record.GetInt32(record.GetOrdinal("CertifiedProgramID"));
        }
    }

    #endregion

        
    }
}
