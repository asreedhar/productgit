using System.Collections.Generic;

namespace FirstLook.Reports.App.ReportStyleLibrary
{
    public class StyleSheet
    {
        private readonly Dictionary<string, Dictionary<string, string>> cache = new Dictionary<string, Dictionary<string, string>>();

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Style(string styleName, string attributeName)
        {
            if (cache.ContainsKey(styleName))
            {
                Dictionary<string, string> attributes = cache[styleName];

                if (attributes.ContainsKey(attributeName))
                {
                    return attributes[attributeName];
                }
                else
                {
                    string value = StyleSheets.RetrieveStyles(name, styleName, attributeName);

                    attributes.Add(attributeName, value);

                    return value;
                }
            }
            else
            {
                string value = StyleSheets.RetrieveStyles(name, styleName, attributeName);

                Dictionary<string, string> attributes = new Dictionary<string, string>();

                cache[styleName] = attributes;

                attributes.Add(attributeName, value);

                return value;
            }
        }
    }
}
