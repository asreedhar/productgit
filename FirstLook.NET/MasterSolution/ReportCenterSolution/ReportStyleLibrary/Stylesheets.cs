using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml.XPath;

namespace FirstLook.Reports.App.ReportStyleLibrary
{
    /// <summary>
    /// Maintains a synchronized cache of stylesheet definitions. Each stylesheet definition loads itself
    /// from the filesystem when the 'last write time' is updated. Otherwise, between calls you are using
    /// a cached copy of the XML document (so it should be faster).
    /// </summary>
    public class StyleSheets
    {
        private readonly static IDictionary<string, StyleSheets> cache = new Dictionary<string, StyleSheets>();

        public static String RetrieveStyles(String styleSheetName, String styleName, String styleAttribute)
        {
            string path = ConfigurationManager.AppSettings["FirstLook.Reports.App.ReportStyleLibrary.Stylesheets.Path"];

            if (string.IsNullOrEmpty(path))
            {
                string file = "ReportStyleSheet.xml";

                string[] wellKnownLocations = new string[] {
                    // IIS deployment
                    @"C:\Inetpub\wwwroot\command_center\etc\" + file,
                    // visual studio 2005 assembly directories
                    @"C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\PublicAssemblies\" + file,
                    @"C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\PrivateAssemblies\" + file,
					// 64 bit vista, wooooo vista!
                    @"C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\PrivateAssemblies\" + file,
                    // reporting services bin directory
                    @"C:\Program Files\Microsoft SQL Server\MSSQL.1\Reporting Services\ReportServer\bin\" + file,
					@"C:\Program Files\Microsoft SQL Server\MSSQL.2\Reporting Services\ReportServer\bin\" + file,
					@"C:\Program Files\Microsoft SQL Server\MSSQL.3\Reporting Services\ReportServer\bin\" + file,
					@"C:\Program Files\Microsoft SQL Server\MSSQL.4\Reporting Services\ReportServer\bin\" + file,
                    // development directory
                    @"C:\Projects\FirstLook.NET\MasterSolution\ReportCenterSolution\ReportStyleLibrary\" + file
                };

                foreach (string wellKnownLocation in wellKnownLocations)
                {
                    if (new FileInfo(wellKnownLocation).Exists)
                    {
                        path = wellKnownLocation;
                        break;
                    }
                }

                if (path == null)
                {
                    throw new InvalidOperationException("Neither supplied the path to a stylesheet and one does not exist in a well known location");
                }
            }

            if (string.IsNullOrEmpty(styleSheetName))
            {
                styleSheetName = "default";
            }

            StyleSheets styleSheets;

            lock (cache)
            {
                if (cache.Keys.Contains(path))
                {
                    styleSheets = cache[path];
                }
                else
                {
                    styleSheets = new StyleSheets(path);

                    cache.Add(path, styleSheets);
                }
            }

            return styleSheets.GetStyle(styleSheetName, styleName, styleAttribute);
        }

        private readonly string path;
        private DateTime lastWriteTime;
        private DateTime lastCheckTime;
        private XPathDocument document;

        public StyleSheets(string path)
        {
            this.path = path;

            Load();
        }

        /// <summary>
        /// Return an inline style for the supplied information (sheet, style, attribute). Refreshes the
        /// style information from the filesystem when updated. Is synchronized.
        /// </summary>
        /// <param name="styleSheetName">The name of the stylesheet</param>
        /// <param name="styleName">The name of the style</param>
        /// <param name="styleAttribute">The name of the attribute of the style</param>
        /// <returns>An inline style attribute from the stylesheet definition</returns>
        public string GetStyle(String styleSheetName, String styleName, String styleAttribute)
        {
            XPathDocument localCopy;

            lock (document)
            {
                if (IsUpdated())
                {
                    Load();
                }

                localCopy = document;
            }

            XPathNavigator navigator = localCopy.CreateNavigator();

            XPathNavigator node = navigator.SelectSingleNode("/stylesheets/stylesheet[@name='" + styleSheetName + "']/style[@name='" + styleName + "']/" + styleAttribute + "/text()");

            if (node != null)
            {
                return node.ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Checks the filesystem to see if the underlying document is more recent than the
        /// cached copy.  The check is made every 5 seconds to stop thrashing the filesystem.
        /// </summary>
        /// <returns>true if the filesystem is more recent than the cached copy</returns>
        private Boolean IsUpdated()
        {
            if (DateTime.Now.Subtract(lastCheckTime).TotalMinutes > 5)
            {
                lastCheckTime = DateTime.Now;

                return File.GetLastWriteTime(path).CompareTo(lastWriteTime) > 0;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Load the document from the filesystem. This method is not synchronized.
        /// </summary>
        private void Load()
        {
            using (Stream stream = File.OpenRead(path))
            {
                document = new XPathDocument(stream);
                lastWriteTime = File.GetLastWriteTime(path);
                lastCheckTime = lastWriteTime;
            }
        }
    }
}

