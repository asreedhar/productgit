using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class EdgeSystemUsageGaugeDataServiceTest : DataServiceTest<EdgeSystemUsageGaugeDataService>
    {
        [Test]
        public void HasOneRow()
        {
            Assert.AreEqual(1, Count());
        }
    }
}
