using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class AppraisalOverviewGaugeDataServiceTest : DataServiceTest<AppraisalOverviewGaugeDataService>
    {
        [Test]
        public void HasOneRow()
        {
            Assert.AreEqual(1, Count());
        }
    }
}
