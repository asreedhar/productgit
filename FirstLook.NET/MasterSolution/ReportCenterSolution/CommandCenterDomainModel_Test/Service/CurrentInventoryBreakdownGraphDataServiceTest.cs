using System;
using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class CurrentInventoryBreakdownGraphDataServiceTest : DataServiceTest<CurrentInventoryBreakdownGraphDataService>
    {
        protected override object GetParameterValue(string parameterName)
        {
            if (string.Compare(parameterName, "isPercentMode", StringComparison.Ordinal) == 0)
            {
                return true;
            }
            else
            {
                return base.GetParameterValue(parameterName);
            }
        }
        [Test]
        public void PercentGraph_HasRows()
        {
            Assert.Greater(Count(), 0, "Expected more than one row");
        }
    }
}
