using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class OptimalInventoryGaugeDataServiceTest : DataServiceTest<OptimalInventoryGaugeDataService>
    {
        [Test]
        public void HasOneRow()
        {
            Assert.AreEqual(1, Count());
        }
    }
}
