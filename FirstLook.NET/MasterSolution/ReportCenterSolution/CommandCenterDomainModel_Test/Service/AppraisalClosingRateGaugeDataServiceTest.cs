using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class AppraisalClosingRateGaugeDataServiceTest : DataServiceTest<AppraisalClosingRateGaugeDataService>
    {
        [Test]
        public void HasOneRow()
        {
            Assert.AreEqual(1, Count());
        }
    }
}
