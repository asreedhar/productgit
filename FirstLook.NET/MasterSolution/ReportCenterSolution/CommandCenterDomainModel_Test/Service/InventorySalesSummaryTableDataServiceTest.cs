using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class InventorySalesSummaryTableDataServiceTest : DataServiceTest<InventorySalesSummaryTableDataService>
    {
        [Test]
        public void HasRows()
        {
            Assert.Greater(Count(), 0, "Expected more than one row");
        }
    }
}
