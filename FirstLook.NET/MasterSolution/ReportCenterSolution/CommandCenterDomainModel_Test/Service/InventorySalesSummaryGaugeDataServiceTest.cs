using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class InventorySalesSummaryGaugeDataServiceTest : DataServiceTest<InventorySalesSummaryGaugeDataService>
    {
        [Test]
        public void HasOneRow()
        {
            Assert.AreEqual(1, Count());
        }
    }
}
