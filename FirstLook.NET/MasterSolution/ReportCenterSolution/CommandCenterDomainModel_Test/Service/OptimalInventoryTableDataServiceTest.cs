using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class OptimalInventoryTableDataServiceTest : DataServiceTest<OptimalInventoryTableDataService>
    {
        [Test]
        public void HasRows()
        {
            Assert.Greater(Count(), 0, "Expected more than one row");
        }
    }
}
