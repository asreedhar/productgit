using System;
using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class AppraisalOverviewTableDataServiceTest : DataServiceTest<AppraisalOverviewTableDataService>
    {
        protected override object GetParameterValue(string parameterName)
        {
            if (string.Compare("reportMode", parameterName, StringComparison.Ordinal) == 0)
            {
                return "TradeInInventoryAnalyzed";
            }
            else
            {
                return base.GetParameterValue(parameterName);
            }
        }

        [Test]
        public void TradeIn_HasRows()
        {
            Assert.Greater(Count(), 0, "Expected more than one row");
        }
    }
}
