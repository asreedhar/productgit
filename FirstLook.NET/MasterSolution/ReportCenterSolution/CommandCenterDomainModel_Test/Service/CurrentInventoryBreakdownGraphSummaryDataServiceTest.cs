using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [TestFixture]
    public class CurrentInventoryBreakdownGraphSummaryDataServiceTest : DataServiceTest<CurrentInventoryBreakdownGraphSummaryDataService>
    {
        [Test]
        public void OneRow()
        {
            Assert.AreEqual(1, Count(), "Expected one summary line record");
        }
    }
}
