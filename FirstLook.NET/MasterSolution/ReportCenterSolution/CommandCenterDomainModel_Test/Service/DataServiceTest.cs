using System;
using System.Collections;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using NUnit.Framework;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public abstract class DataServiceTest<T> where T : IDataService, new()
    {
        private BusinessUnit dealerGroup;
        private List<BusinessUnit> dealers;
        private Member member;
        private string timePeriodId;

        private T dataService;
        private IEnumerable data;

        public BusinessUnit DealerGroup
        {
            get { return dealerGroup; }
            private set { dealerGroup = value; }
        }

        public Member Member
        {
            get { return member; }
            private set { member = value; }
        }

        public List<BusinessUnit> Dealers
        {
            get { return dealers; }
            private set { dealers = value; }
        }

        public string TimePeriodId
        {
            get { return timePeriodId; }
            private set { timePeriodId = value; }
        }

        public T DataService
        {
            get { return dataService; }
            private set { dataService = value; }
        }

        public IEnumerable Data
        {
            get { return data; }
            private set { data = value; }
        }

        [SetUp]
        public void SetUp()
        {
            // build params
            DealerGroup = BusinessUnitFinder.Instance().Find(100068);
            Member = MemberFinder.Instance().Find(100697);
            Dealers = new List<BusinessUnit>();
            foreach (BusinessUnit dealer in Member.Dealers)
                if (dealer.Id % 2 == 0)
                    Dealers.Add(dealer);
            TimePeriodId = "26weeks";
            // run service
            DataService = new T();
            DataService.Initialize(GetParameterValue);
            Data = DataService.GetData(new DataServiceArguments(0,false,string.Empty,0,0));
        }

        [TearDown]
        public void TearDown()
        {
            DealerGroup = null;
            Member = null;
            Dealers = null;
            TimePeriodId = null;
            DataService = default(T);
            Data = null;
        }

        protected virtual object GetParameterValue(string parameterName)
        {
            if (string.Compare(parameterName, "dealerGroup", StringComparison.Ordinal) == 0)
            {
                return DealerGroup;
            }
            else if (string.Compare(parameterName, "dealers", StringComparison.Ordinal) == 0)
            {
                return Dealers;
            }
            else if (string.Compare(parameterName, "member", StringComparison.Ordinal) == 0)
            {
                return Member;
            }
            else if (string.Compare(parameterName, "timePeriodId", StringComparison.Ordinal) == 0)
            {
                return TimePeriodId;
            }
            else
            {
                return null;
            }
        }

        protected int Count()
        {
            IEnumerator er = Data.GetEnumerator();
            int i = 0;
            while (er.MoveNext())
                i++;
            return i;
        }
    }
}