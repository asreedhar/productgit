using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class AppraisalReviewDetails : DealerGroupPage
{
    protected List<GridViewRow> footerRows;
    private int maxNumberOfAppraisals = 0;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        base.IsAnalyicsEnabled = true;
        Page.Title = "Make-A-Deals";
    }

    protected void DealerGroup_Init(object sender, EventArgs e)
    {
        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
    }

    protected void ChartTitle_Init(object sender, EventArgs e)
    {
        ChartTitle.Text = "Make-A-Deals";
    }

    protected void AppraisalReviewDetails_Init(object sender, EventArgs e)
    {
        footerRows = new List<GridViewRow>();
    }


    protected void AppraisalReviewDetails_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
        {
            e.Row.Cells[0].CssClass += " first";
            e.Row.Cells[e.Row.Cells.Count - 1].CssClass += " last";


            if ((int)((DataRowView)e.Row.DataItem)["DrilldownRowType"] != 0)
            {
                e.Row.Cells[0].Text = "-";
                footerRows.Add(e.Row);
                e.Row.Visible = false;
            }
            else
            {
                e.Row.Cells[2].FindControl("MakeADealFigures").Visible = false;

                int numAppraisals = IsNull(((DataRowView)e.Row.DataItem)["NumberOfAppraisals"]) ? 0 : (int)((DataRowView)e.Row.DataItem)["NumberOfAppraisals"];

                ((Panel)e.Row.FindControl("MakeADealBar")).Width = numAppraisals;
                if (maxNumberOfAppraisals < numAppraisals)
                {
                    maxNumberOfAppraisals = numAppraisals;
                }
            }

            foreach(int formatColumn in new int[2] {3, 4}) {
                double v = GetDouble(e.Row.Cells[formatColumn].Text);
                if (double.IsNaN(v))
                {
                    e.Row.Cells[formatColumn].Text = "--";
                }
                else
                {
                    e.Row.Cells[formatColumn].Text = String.Format("{0:C0}", v);
                }
                if (e.Row.Cells[formatColumn].Text.Contains("("))
                {
                    e.Row.Cells[formatColumn].CssClass += " negativenumber";
                }
            }
        }
        if (e.Row.RowType.Equals(DataControlRowType.Footer))
        {
            foreach (GridViewRow footerRow in footerRows)
            {
                footerRow.Cells[2].FindControl("MakeADealFigures").Visible = true;
                footerRow.Cells[2].FindControl("MakeADealBarChart").Visible = false;
                GridViewRow r = GridViewUtilities.CopyRow(footerRow, DataControlRowType.Footer, AppraisalReviewDetails_GridView);
                e.Row.Parent.Controls.Add(r);
            }
            e.Row.Visible = false;
        }
    }

    protected void AppraisalReviewDetails_DataBound(Object sender, EventArgs e)
    {
        foreach(GridViewRow row in AppraisalReviewDetails_GridView.Rows) {
            if (row.RowType == DataControlRowType.DataRow && !IsNull(row.FindControl("MakeADealBar")) )
            {
                Panel bar = row.FindControl("MakeADealBar") as Panel;
                Label appraisalbarwithlink = row.FindControl("NumberOfAppraisalReviewsWithLink") as Label;
                Label appraisalbarwithoutlink = row.FindControl("NumberOfAppraisalReviewsWithoutLink") as Label;

                if (bar.Width.Value > 0)
                {
                    bar.Width = new Unit(bar.Width.Value / maxNumberOfAppraisals * 170);
                }
                if (appraisalbarwithlink.Width.Value > 0)
                {
                    appraisalbarwithlink.Width = new Unit(appraisalbarwithlink.Width.Value / maxNumberOfAppraisals * 170);
                    appraisalbarwithoutlink.Width = new Unit(appraisalbarwithoutlink.Width.Value / maxNumberOfAppraisals * 170);
                }
            }
        }
    }

    private double GetDouble(object o)
    {
        if (o == null || DBNull.Value.Equals(o))
        {
            return double.NaN;
        }
        double val;
        if (double.TryParse(o.ToString(), out val))
        {
            return val;
        }
        return double.NaN;
    }

    protected Int32 GetInt(object o)
    {
        if (o == null || DBNull.Value.Equals(o))
        {
            return 0;
        }
        Int32 val;
        if (Int32.TryParse(o.ToString(), out val))
        {
            return val;
        }
        return 0;

    }

    protected void AppraisalReviewDaysBack_DropDownList_OnDataBound(object sender, EventArgs e)
    {
        string daysBackParam = Request.Params["daysBack"];
        if (daysBackParam != null && !daysBackParam.Equals(String.Empty))
        {
            ListItem item = AppraisalReviewDaysBack_DropDownList.Items.FindByValue(daysBackParam);
            AppraisalReviewDaysBack_DropDownList.SelectedIndex = AppraisalReviewDaysBack_DropDownList.Items.IndexOf(item);
        }
    }

    protected void AppraisalReviewDaysBack_DropDownList_OnPreRender(object sender, EventArgs e)
    {
        AppraisalReviewDetails_TimePeriodLabel.Text = ((DropDownList)sender).SelectedItem.Text;
    }
}
