using System;
using FirstLook.DomainModel.Oltp;


public partial class Reports_DealerGroupReport : ReportPage
{

    protected override void Page_PreInit(object sender, EventArgs e)
    {
        base.Page_PreInit(sender, e);
        base.IsAnalyicsEnabled = true;
    }

	protected void Page_Load()
	{
        Page.Title = ReportControl1.ReportName;
	}

    protected void ReportViewer_Drillthrough(object sender, EventArgs e)
    {

    }

    protected override string Token()
    {
        return SoftwareSystemComponentStateFacade.DealerGroupComponentToken;
    }


}
