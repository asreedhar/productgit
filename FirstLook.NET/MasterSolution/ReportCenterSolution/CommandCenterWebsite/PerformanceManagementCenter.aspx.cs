using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.CommandCenter.DomainModel.Service;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;
using System.Collections;

public partial class PerformanceManagementCenter : DealerGroupPage
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Switch on Google Analytics. Parent page has code to render script.
        base.IsAnalyicsEnabled = true;
    }
    
    //
    // Page Load
    //
    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Remove existing meta tag
        System.Web.UI.HtmlControls.HtmlMeta meta = (System.Web.UI.HtmlControls.HtmlMeta)this.Header.FindControl("metatag");
        if (meta != null)
            meta.HttpEquiv = "X-UA-Compatible";
            meta.Content = "IE=edge"; 
            
            //this.Header.Controls.Remove(meta);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        // Add meta tag for IE compatiblity
        //System.Web.UI.HtmlControls.HtmlMeta tag = new System.Web.UI.HtmlControls.HtmlMeta();
        //tag.HttpEquiv = "X-UA-Compatible";
        //tag.Content = "IE=edge";
        //Header.Controls.Add(tag);

        
        
        Form.Attributes.Add("class", "pmc");
        Page.Title = "Pre-Owned Performance Management Center";

        int dealerGroupId = SoftwareSystemComponentState.DealerGroup.GetValue().Id;

        ReportGroupingFactory reportGroupingFactory = WebReportDefinitionFactory.NewReportGroupingFactory();

        AppraisalQuickFactsTimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById("4weeks", true);

        // set text on button
        DealershipHomeLinksButton.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name.ToUpper() + " " + DealershipHomeLinksButton.Text;

        IReportListHelper.PopulateReportList(PerformanceAndUsage, reportGroupingFactory.BuildReportTree("F6A5EA18-3F53-4aa3-9DC1-465103E821F8", dealerGroupId));
        IReportListHelper.PopulateReportList(InventoryManagement, reportGroupingFactory.BuildReportTree("AFB764A8-46D7-4ddc-A1A1-FB8003B9F37B", dealerGroupId));
        IReportListHelper.PopulateReportList(WholesaleBuyingAndSelling, reportGroupingFactory.BuildReportTree("EFAC9978-DAD2-44f6-B8D9-0A4A232C557B", dealerGroupId));
        //IReportListHelper.PopulateReportList(Appraisals, reportGroupingFactory.BuildReportTree("C1B9CA87-1E41-4859-8CE8-8957E561C21E", dealerGroupId));

        ToggleMaxTile();

        if (!HasMaxDealers)
        {
            AppraisalReviewUpdatePanel.Visible = false;
        }
    }

    private void ToggleMaxTile()
    {
        bool visible = false;
        if (HasMaxDealers)
        {
            IEnumerator en = MaxDealers.GetEnumerator();
            en.MoveNext();
                
            BusinessUnit bu = en.Current as BusinessUnit;

            MaxSettings gs = MaxSettings.GetSettings(bu.Id);
            visible = gs.HasSettings && gs.ShowGroupLevelDashboard;
        }
        MaxGroupDashboardTile.Visible = visible;
    }

    protected void ViewingAsDealerName_Load(object sender, EventArgs e)
    {
        ViewingAsDealerName_Load(BusinessUnitSet);
    }

    protected void ViewingAsDealerName_Load(ICollection<BusinessUnit> businessUnits)
    {
        int selectedBusinessUnits = businessUnits.Count;

        int totalBusinessUnits = 0;

        Member member = SoftwareSystemComponentState.DealerGroupMember();

        switch (member.MemberType)
        {
            case MemberType.User:
                totalBusinessUnits = member.Dealers.Count;
                break;
            case MemberType.AccountRepresentative:
            case MemberType.Administrator:
                totalBusinessUnits = SoftwareSystemComponentState.DealerGroup.GetValue().Dealerships().Count;
                break;
        }

        if (selectedBusinessUnits == totalBusinessUnits)
        {
            ViewingAsDealerName.Text = "Your Managed Dealerships";
        }
        else if (selectedBusinessUnits == 1)
        {
            ViewingAsDealerName.Text = CollectionHelper.First(businessUnits).Name;
        }
        else
        {
            ViewingAsDealerName.Text = string.Format("{0} of {1} Managed Dealerships", selectedBusinessUnits, totalBusinessUnits);
        }
    }

    protected void LastUpdated_Load(object sender, EventArgs e)
    {
        LastUpdated.Text = DateTime.Now.ToString("D") + " " + DateTime.Now.ToString("t");
    }

    //
    // Page Load Data Events
    //

    protected void DealerSelection_DataBound(object sender, EventArgs e)
    {
        foreach (ListItem item in DealerSelection.Items)
        {
            item.Selected = false;

            int businessUnitId = Convert.ToInt32(item.Value);

            foreach (BusinessUnit businessUnit in BusinessUnitSet)
            {
                if (businessUnit.Id.Equals(businessUnitId))
                {
                    item.Selected = true;
                    break;
                }
            }
        }
    }

    // behind the numbers table management
    protected void InventorySalesSummary_GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        DataRowView thisRowDataView = (DataRowView)(e.Row.DataItem);
        int currentcell = 4;    // an assumption is made here that all columns need an extra css class if negative
                                // (see datasource for column order and definition changes)

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            while (currentcell <= e.Row.Cells.Count)
            {
                object obj = thisRowDataView.Row.ItemArray[currentcell];
                double d = 0.0d;
                bool isDbl = false;

                if (!(obj is DBNull))
                    isDbl = double.TryParse(obj.ToString(), out d);
                
                if (isDbl && d < 0.0d)
                    // TimePeriodId in rowdataview is not part of the gridview
                    e.Row.Cells[currentcell-1].CssClass += " negativenumber";

                currentcell++;
            }
        }
    }

    protected void InventorySalesSummary_GridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        GridViewUtilities.MergeColumnHeaderCell(e.Row, "MergeColumnHeaderCell");
    }

    protected void CurrentInventorySummary_DataBound(object sender, EventArgs e)
    {
        Label label = (Label) CurrentInventorySummary.FindControl("InvChartBookVsCost");
        if (label != null)
        {
            if (DataBinder.Eval(CurrentInventorySummary.DataItem, "BookVsCost").ToString().Contains("-"))
            {
                label.CssClass = "negativenumber";
            }
        }
    }

    //
    // Page (Postback) Events
    //
    protected void DealerSelection_SelectedIndexChanged(object sender, EventArgs e)
    {
        List<int> businessUnitIds = new List<int>();

        foreach (ListItem item in DealerSelection.Items)
            if (item.Selected)
                businessUnitIds.Add(Convert.ToInt32(item.Value));

        List<BusinessUnit> removed = new List<BusinessUnit>();

        foreach (BusinessUnit businessUnit in BusinessUnitSet)
            if (!businessUnitIds.Contains(businessUnit.Id))
                removed.Add(businessUnit);

        BusinessUnitFinder finder = BusinessUnitFinder.Instance();

        List<BusinessUnit> added = new List<BusinessUnit>();

        foreach (int businessUnitId in businessUnitIds)
        {
            bool found = false;

            foreach (BusinessUnit businessUnit in BusinessUnitSet)
            {
                if (businessUnit.Id.Equals(businessUnitId))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                added.Add(finder.Find(businessUnitId));
            }
        }

        ICollection<BusinessUnit> businessUnits = BusinessUnitSet;

        foreach (BusinessUnit businessUnit in removed)
            businessUnits.Remove(businessUnit);

        foreach (BusinessUnit businessUnit in added)
            businessUnits.Add(businessUnit);

        ViewingAsDealerName_Load(businessUnits);
    }

    protected void AppraisalOverviewSummary_DataBound(object sender, EventArgs e)
    {
        object item = AppraisalOverviewSummary.DataItem;

        HyperLink link = (HyperLink) AppraisalOverviewSummary.FindControl("AppraisalQuickFacts_TradeInInventoryAnalyzed");
        if (link != null)
        {
            //link.NavigateUrl = "~/AppraisalOverview.aspx?mode=TradeInInventoryAnalyzed&TimePeriodId=" +
            //                   DataBinder.Eval(item, "TimePeriodId");
            link.NavigateUrl = "~/DealerGroupNewReport.aspx?Id=819d1e2c-e985-407f-9ccd-30a9e2c0afa9&Period=1";
        }

        HyperLink link2 = (HyperLink)AppraisalOverviewSummary.FindControl("AppraisalQuickFacts_AvgImmWholesaleProfit");
        if (link2 != null)
        {
            //link2.NavigateUrl = "~/AppraisalOverview.aspx?mode=AvgImmWholesaleProfit&TimePeriodId=" +
            //                   DataBinder.Eval(item, "TimePeriodId");
            link2.NavigateUrl = "~/DealerGroupNewReport.aspx?Id=f352752c-9d73-4832-a8c4-2734a9623ad5&Period=1";
        }

        Label highValue = (Label)AppraisalOverviewSummary.FindControl("HighAvgImmWholesaleProfit");
        if (highValue != null)
        {
            if (DataBinder.Eval(item, "WholesaleMax").ToString().Contains("-"))
            {
                highValue.CssClass = "negativenumber";
            }
        }

        Label lowValue = (Label) AppraisalOverviewSummary.FindControl("LowAvgImmWholesaleProfit");
        if (lowValue != null)
        {
            if (DataBinder.Eval(item, "WholesaleMin").ToString().Contains("-"))
            {
                lowValue.CssClass = "negativenumber";
            }
        }
    }

    protected void BusinessUnitList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            BusinessUnit dealer = e.Item.DataItem as BusinessUnit;

            HyperLink link = (HyperLink)e.Item.FindControl("EnterDealership");

            if (link != null && dealer != null)
            {
                link.NavigateUrl = "EnterDealership.aspx?dealerId=" + HttpUtility.HtmlEncode(dealer.Id.ToString());

                link.Text = HttpUtility.HtmlEncode(dealer.Name);
            }
        }
    }

    protected void AppraisalClosingRateGauge_TimePeriod_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(Page), "AppraisalClosingRateTimeRangeScript", "enhanceSelectBox('selectAppraisalClosingRateTimeRange', 'appraisalTimeSpan', 'closeAppraisalClosingRateTimeRange');", true);
        AppraisalQuickFactsTimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(((DropDownList)sender).SelectedValue, true);
        //AppraisalTimeSpanLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(((DropDownList)sender).SelectedValue, true);
    }
    protected void InventorySalesSummary_TimePeriod_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(Page), "InventorySalesSummaryTimeRangeScript", "enhanceSelectBox('selectNumbersTimeRange', 'numbersTimeSpan', 'closeSelectNumbersTimeRange');", true);
        NumbersTimeSpanText.Text = TimePeriodDataService.FindTimePeriodDescriptionById(((DropDownList)sender).SelectedValue, false);
    }

    protected void AppraisalReviewDaysBack_DropDownList_OnPreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(Page), "AppraisalReviewDaysBackScript", "enhanceSelectBox('selectAppraisalReviewDaysBack', 'appraisalReviewDaysBack', 'closeAppraisalReviewDays');", true);
        DropDownList daysBackDropDownList = (DropDownList)sender;

        MemberAppraisalReviewPreference pref = MemberAppraisalReviewPreferenceFacade.FindOrCreate(User.Identity.Name, SoftwareSystemComponentState.DealerGroup.GetValue());
        if (IsPostBack)
        {
            pref.DaysBack = Convert.ToInt32(daysBackDropDownList.SelectedValue);
            pref.Save();
        }
        else
        {
            ListItem item = daysBackDropDownList.Items.FindByValue(pref.DaysBack.ToString());
            daysBackDropDownList.SelectedIndex = daysBackDropDownList.Items.IndexOf(item);
        }

        AppraisalReviewDaysBackLabel.Text = daysBackDropDownList.SelectedItem.Text;
    }
    protected void AppraisalClosingRateGauge_DataBound(object sender, EventArgs e)
    {
        DataRowView rowView = (DataRowView)AppraisalClosingRateSummary.DataItem;
        String AppClosingGraphScript = "<script type=\"text/javascript\"> jQuery(function () {{AppraiseGraph.closed = {0};AppraiseGraph.notClosed = {1};AppraiseGraph.appraisals = {2};AppraiseGraph.link = \"{3}\";  AppraiseGraph.onLoad(); }}) </script>";
        //AppClosingGraphScript = string.Format(AppClosingGraphScript, TryParseInt(Convert.ToString(rowView["ClosedAppraisal"])), TryParseInt(Convert.ToString(rowView["NotClosedAppraisal"])), TryParseInt(Convert.ToString(rowView["Appraisal"])), "AppraisalClosingRate.aspx?timePeriodId=4weeks");
        AppClosingGraphScript = string.Format(AppClosingGraphScript, TryParseInt(Convert.ToString(rowView["ClosedAppraisal"])), TryParseInt(Convert.ToString(rowView["NotClosedAppraisal"])), TryParseInt(Convert.ToString(rowView["Appraisal"])), "DealerGroupNewReport.aspx?Id=D06528EE-05F3-4A00-B8C5-403536F103D8&Mode=ClosingRate");
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "AppraisalClosingGraph", AppClosingGraphScript, false);
    }
    protected void CurrentInventoryBreakdownChart_DataBound(object sender, EventArgs e)
    {
        DataRowView rowView = (DataRowView)CurrentInventoryBreakdownChartData.DataItem;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("<script type=\"text/javascript\">  jQuery(function ($) {var bar,bars = [];");
        int id = 0;
       
        foreach (DataRowView dr in rowView.DataView)
        {
            id += 1;
            string[] objSplit = Convert.ToString(dr["BucketName"]).Split("-".ToCharArray());
            int high=0;
            int low=0;
            if (objSplit.Length > 1)
            {
                int.TryParse(objSplit[1].ToString(), out high);
                int.TryParse(objSplit[0].ToString(), out low);
            }
            else
            {
                int.TryParse(objSplit[0].Split("+".ToCharArray())[0] , out low);
            }
            int InvCnt, GreenCnt, RedCnt, YelCnt;
            double InvVal, BookvsCost;
            int.TryParse(Convert.ToString(dr["InventoryCount"]), out InvCnt);
            int.TryParse(Convert.ToString(dr["GreenLightInventoryCount"]), out GreenCnt);
            int.TryParse(Convert.ToString(dr["RedLightInventoryCount"]), out RedCnt);
            int.TryParse(Convert.ToString(dr["YellowLightInventoryCount"]), out YelCnt);
            double.TryParse(Convert.ToString(dr["InventoryValue"]), out InvVal);
            double.TryParse(Convert.ToString(dr["BookVsCost"]), out BookvsCost);

            sb.AppendLine("bar={}; bar.RangeId = " + id + ";bar.Units = " + InvCnt + ";bar.Green = " + GreenCnt + ";bar.Yellow = " + YelCnt + ";bar.Red = " + RedCnt + ";bar.Percentage = Math.round(" + InvVal + ");bar.BookMinusCost = " + BookvsCost + ";bar.RangeLow = " + low + ";bar.RangeHigh = " + high + ";bars.push(bar);");
        }
        sb.AppendLine("var graph = CurrentInventoryGraph; graph.Setup($('#CurrentInventoryGraph'), bars, 'CurrentInventoryBreakdown.aspx?bucketIndex=');       }); </script>");
        //String AppClosingGraphScript = "<script type=\"text/javascript\"> jQuery(function () {{AppraiseGraph.closed = {0};AppraiseGraph.notClosed = {1};AppraiseGraph.appraisals = {2};AppraiseGraph.link = \"{3}\";  AppraiseGraph.onLoad(); }}) </script>";
        //AppClosingGraphScript = string.Format(AppClosingGraphScript, Convert.ToString(rowView["ClosedAppraisal"]), Convert.ToString(rowView["NotClosedAppraisal"]), Convert.ToString(rowView["Appraisal"]), "AppraisalClosingRate.aspx");
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "CurrentInventoryBreakdownChart", sb.ToString(), false);
    }
    protected void OptimalInventoryGauge_DataBound(object sender, EventArgs e)
    {
        DataRowView rowView = (DataRowView)OptimalInventoryGaugeData.DataItem;
        String OptimalInventoryGaugeScript = "<script type=\"text/javascript\"> jQuery(function () {{CoreInventoryGraph.stocked = {0};CoreInventoryGraph.optimal = {1};CoreInventoryGraph.link = 'OptimalInventory.aspx';CoreInventoryGraph.onLoad();}}); </script>";
        OptimalInventoryGaugeScript = string.Format(OptimalInventoryGaugeScript, Convert.ToString(rowView["In-Stock"]), Convert.ToString(rowView["Recommended"]));
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "OptimalInventoryGraph", OptimalInventoryGaugeScript, false);
    }
    protected int TryParseInt(string val)
    {
         int result;
         int.TryParse(val, out result);
         return result;
    }
}
    