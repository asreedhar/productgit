using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using FirstLook.Reports.App.ReportDefinitionLibrary;

/// <summary>
/// Summary description for DealerHome
/// </summary>
public abstract class DealerReportControl_Stub : System.Web.UI.UserControl
{
    protected static void PopulateReportList(IReportList list, IReportTreeNode root)
    {
        list.Title = root.Title;

        list.Reports.Clear();

        if (root.NodeType.Equals(ReportTreeNodeType.ReportGroup))
        {
            IReportGroup group = (IReportGroup)root;

            foreach (IReportTreeNode child in group.Children)
            {
                list.Reports.Add((IReportHandle)child);
            }
        }
    }

}
