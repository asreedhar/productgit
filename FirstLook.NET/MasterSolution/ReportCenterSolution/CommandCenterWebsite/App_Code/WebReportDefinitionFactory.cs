using System.Configuration;
using FirstLook.Reports.App.ReportDefinitionLibrary;

public static class WebReportDefinitionFactory
{
    public static string ReportKey =
        "FirstLook.Reports.App.ReportDefinitionLibrary.Xml.Serialization.ReportMetadata.Path";

    public static string ReportGroupingKey =
        "FirstLook.Reports.App.ReportDefinitionLibrary.Xml.Serialization.ReportGroupingMetadata.Path";

    public static ReportFactory NewReportFactory()
    {
        return ReportFactory.NewReportFactory(ConfigurationManager.AppSettings[ReportKey]);
    }

    public static ReportGroupingFactory NewReportGroupingFactory()
    {
        return ReportGroupingFactory.NewReportGroupingFactory(
            ConfigurationManager.AppSettings[ReportGroupingKey],
            NewReportFactory());
    }
}
