using System.Collections.Generic;

using FirstLook.Reports.App.ReportDefinitionLibrary;

/// <summary>
/// Summary description for IReportList
/// </summary>
public interface IReportList
{
    string Title
    {
        get;
        set;
    }

    IList<IReportHandle> Reports
    {
        get;
    }
}
