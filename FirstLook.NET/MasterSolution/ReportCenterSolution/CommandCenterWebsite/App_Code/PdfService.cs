using System.Web.Configuration;

public class PdfService
{
    public PdfService()
    {
    }

    public byte[] GeneratePdf(string pagesHTML)
    {
        PrinceXmlWebServiceProxy service = new PrinceXmlWebServiceProxy();

        service.Url = WebConfigurationManager.AppSettings["prince_xml_webservice_url"]; 
        Input input = new Input();

        input.Content = pagesHTML;
        input.ContentType = ContentType.Html;
        OutputType outputType = new OutputType();
        outputType.OutputTypeCode = OutputTypeCode.ByteStream;
        outputType.Item = new SharedFileSystem();
        Output output = new Output();
        Status status = service.generate(input, outputType, out output);
        return (byte[])output.Item;
    }
}
