using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Web;
using FirstLook.DomainModel.Oltp;
using FirstLook.Common.Core.Utilities;

public partial class Home_EnterDealership : DealerGroupPage
{
    private static readonly string maxHost = ConfigurationManager.AppSettings["max_host_name"]; 

    protected void Page_Load(object sender, EventArgs e)
    {
        // get the selected dealer

        BusinessUnit dealer = null;
        string application = Request.Params["application"];
        string dealerId = Request.QueryString["dealerId"];

        BusinessUnit dealerGroup = SoftwareSystemComponentState.DealerGroup.GetValue();
        ICollection<BusinessUnit> businessUnits = SoftwareSystemComponentState.DealerGroupMember().AccessibleDealers(dealerGroup);

        if (!string.IsNullOrEmpty(dealerId))
        {
            int businessUnitId = Convert.ToInt32(dealerId);

            foreach (BusinessUnit businessUnit in businessUnits)
                if (businessUnit.Id.Equals(businessUnitId))
                    dealer = businessUnit;
        }
        else if (string.Equals(application, "profile"))
        {
            dealer = CollectionHelper.First(businessUnits);
        }

        if (dealer == null)
            throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "User has no access to dealer {0}", dealerId));

        // determine if it is max store
        bool isMaxStore = false;

        foreach (DealerUpgrade upgrade in dealer.DealerUpgrades)
            isMaxStore |= upgrade.Equals(Upgrade.Max);

        // update software component state
        SoftwareSystemComponentStateFacade.FindOrCreate(
            User.Identity.Name,
            SoftwareSystemComponentStateFacade.DealerComponentToken,
            dealer.DealerGroup(),
            dealer);

        // set the dealer id cookie (for NextGen)
        HttpCookie cookie = new HttpCookie("dealerId", dealer.Id.ToString());
        cookie.Expires = (DateTime.Now + new TimeSpan(7, 0, 0, 0));
        Response.Cookies.Add(cookie);
        //Response.Cookies.Add(new HttpCookie("dealerId", dealer.Id.ToString()));

        // redirect to dealer application

        string url = isMaxStore ? maxHost + "/login/remote_entry" : 
                "/IMT/RemoteEntryAction.go?p.productMode=edge&p.inventoryType=used"; ;

        if (!string.IsNullOrEmpty(application))
        {
            if ("planning".Equals(application))
            {
                url = "/NextGen/InventoryPlan.go";
            }
            else if("stockingReports".Equals(application))
            {
                url = "/NextGen/OptimalInventoryStockingReports.go";
            }
			else if ("ipa".Equals(application))
			{
				url = "/pricing/?token=DEALER_SYSTEM_COMPONENT";
			}
            else if ("profile".Equals(application))
            {
                url = "/IMT/EditMemberProfileAction.go";
            }
        }

        Response.Redirect(url, true);
    }
}
