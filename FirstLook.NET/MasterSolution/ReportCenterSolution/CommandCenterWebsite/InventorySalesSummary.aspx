<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InventorySalesSummary.aspx.cs" Inherits="Home_InventorySalesSummary" MasterPageFile="~/Master_Pages/Page_Popup.master" %>

<%@ Register TagPrefix="firstlook" TagName="Email" Src="~/Controls/Common/Email.ascx" %>

<asp:Content ID="PopupHeader" ContentPlaceHolderID="Page_Header" runat="server">
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <firstlook:Email ID="Email2" OnSend="EmailClient_Send" Subject="Behind The Numbers" runat="server" />
        <h2><asp:Literal ID="DealerGroup" runat="server" OnInit="DealerGroup_Init"></asp:Literal></h2>
        <h1><asp:Literal ID="ChartTitle" runat="server" OnInit="ChartTitle_Init"></asp:Literal></h1>
    </div>
</asp:Content>

<asp:Content ID="PopupContent" ContentPlaceHolderID="Page_Body" runat="Server">
    <h1 id="graphheader"><asp:Literal ID="GraphTitle" runat="server"></asp:Literal></h1>
    <!-- inventory sales time period -->
    <cwc:AsyncObjectDataSourceControl
        ID="InventorySalesSummary_TimePeriod_DataSource"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.TimePeriodDataService"
        runat="server">
    </cwc:AsyncObjectDataSourceControl>
    <a id="DrilldownTimeSpan" class="dyna_link">
        <asp:Label ID="InventorySalesSummary_TimePeriodLabel" runat="server" />
        <asp:Image ID="Image1" SkinID="MenuIcon" CssClass="hide_nonJS" runat="server"/>
    </a>
    <div id="selectDrilldownTimeRange" class="inlinepopup selectbox" style="display:none;">
        <div id="closeDrilldownTimeRange" class="closebox right"></div>
        <h3>Time Period for Behind the Numbers:</h3>
        <asp:DropDownList
            ID="InventorySalesSummary_TimePeriod"
            DataSourceID="InventorySalesSummary_TimePeriod_DataSource"
            DataTextField="Name"
            DataValueField="Value"
            AutoPostBack="true"
            OnSelectedIndexChanged="InventorySalesSummary_TimePeriodSelectedIndexChanged"
            runat="server">
        </asp:DropDownList>
    </div>


    <!-- inventory sales -->
    <cwc:AsyncObjectDataSourceControl
        ID="InventorySalesSummary_DataSource"
        runat="server"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.InventorySalesSummaryTableDataService">
        <Parameters>
            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
            <owc:MemberParameter Name="member" Type="Object" />
            <asp:ControlParameter Name="timePeriodId" Type="String" ControlID="InventorySalesSummary_TimePeriod" />
        </Parameters>
    </cwc:AsyncObjectDataSourceControl>
    <asp:GridView ID="InventorySalesSummary_GridView" runat="server" 
		ShowFooter="true" 
		CssClass="popuptable" 
		DataSourceID="InventorySalesSummary_DataSource" 
		AutoGenerateColumns="False" 
		UseAccessibleHeader="true" 
		AllowSorting="true" 
		OnInit="InventorySalesSummary_GridView_Init" 
		OnRowDataBound="InventorySalesSummary_GridView_RowDataBound" >
        <Columns>
            <asp:BoundField
                HeaderText="Dealership"
                DataField="Dealer"
                SortExpression="Dealer" />
              <asp:BoundField
                HeaderText="In-Group Rank"
                DataField="SweetSpotRank"
                SortExpression="SweetSpotRank" />
            <asp:HyperLinkField  
                HeaderText="Sweet Spot (Day 1-30)" 
                DataTextField="SweetSpotSales" 
                DataTextFormatString="{0:0%}" 
                SortExpression="SweetSpotSales" 
                DataNavigateUrlFields="Dealer_ID" 
                DataNavigateUrlFormatString="~/DealerGroupReport.aspx?Id=DF05F8CA-11EE-4888-9529-57FC5EA59F1A&amp;ContextCode={0}" />    
            <asp:BoundField
                HeaderText="In-Group Rank"
                DataField="RetailSalesEfficiencyRank"
                SortExpression="RetailSalesEfficiencyRank" />
            <asp:HyperLinkField
                HeaderText="Retail Sales Efficiency (Day 1-60)"
                DataTextField="RetailSalesEfficiency"
                DataTextFormatString="{0:0%}"
                SortExpression="RetailSalesEfficiency"
                DataNavigateUrlFields="Dealer_ID" 
                DataNavigateUrlFormatString="~/DealerGroupReport.aspx?Id=DF05F8CA-11EE-4888-9529-57FC5EA59F1A&amp;ContextCode={0}" />
            <asp:BoundField
                HeaderText="In-Group Rank"
                DataField="RetailAvgGrossProfitGiveBackRank"
                SortExpression="RetailAvgGrossProfitGiveBackRank" />
            <asp:TemplateField HeaderText="Retail Gross Profit Give Back From Aged Wholesale Loss" SortExpression="RetailGrossProfitGiveBackfromAgedWholesaleLoss" >
                <ItemTemplate>
                    <asp:Label ID="RetailGrossProfitGiveBackLabel" runat="server" Text='<%#BuildGiveBackLink(DataBinder.Eval(Container.DataItem,"Dealer_ID"), DataBinder.Eval(Container.DataItem,"RetailGrossProfitGiveBackfromAgedWholesaleLoss"),DataBinder.Eval(Container.DataItem,"RetailGrossProfitGiveBackfromAgedWholesaleLossDollars")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:BoundField
                HeaderText="Avg. Front End Gross"
                DataField="AvgFrontEndGross"
                DataFormatString="{0:C0}"
                SortExpression="AvgFrontEndGross" />
            <asp:HyperLinkField
                HeaderText="Avg. Immediate Wholesale Profit/Loss"
                DataTextField="AvgImmediateWholesaleProfitLoss"
                DataTextFormatString="{0:C0}"
                SortExpression="AvgImmediateWholesaleProfitLoss"
                DataNavigateUrlFields="Dealer_ID" 
                DataNavigateUrlFormatString="~/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode={0}" />    
            <asp:BoundField
                HeaderText="True Retail Avg. Gross Profit (Per Retail Unit Stocked/Sold)"
                DataField="TrueRetailAvgGrossProfit"
                DataFormatString="{0:C0}"
                SortExpression="TrueRetailAvgGrossProfit"
                HtmlEncode="false" />
        </Columns>
    </asp:GridView>
    
</asp:Content>
