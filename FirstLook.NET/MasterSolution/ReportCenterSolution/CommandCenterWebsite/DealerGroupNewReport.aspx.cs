﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary.ReportDefinitionLanguage;
using MsLocalReport = Microsoft.Reporting.WebForms.LocalReport;
using MsDrillthroughEventArgs = Microsoft.Reporting.WebForms.DrillthroughEventArgs;

public partial class DealerGroupNewReport : ReportPage
{
    protected override void Page_PreInit(object sender, EventArgs e)
    {
        base.Page_PreInit(sender, e);
        base.IsAnalyicsEnabled = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack)
        //{
        Page.Header.Title = ReportControl1.ReportName;
        //}
       
    }
    protected override string Token()
    {
        return SoftwareSystemComponentStateFacade.DealerGroupComponentToken;
    }
    public void ReportControl1_Drillthrough(object sender, MsDrillthroughEventArgs e)
    {
        if (e.Report != null && ((MsLocalReport)e.Report).OriginalParametersToDrillthrough != null)
        {
            string DrillThroughBussinessId = ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[0].Values[0].ToString();
            switch (ReportControl1.ReportId)
            {
                case "D06528EE-05F3-4A00-B8C5-403536F103D8":
                     DropDownList PeriodID = ControllerUtils.FindControlRecursive(Page, "TimePeriodID") as DropDownList;
                    string otherParam = string.Empty;
                    int selectedVal = PeriodID != null ? PeriodID.SelectedIndex : 1;
                    switch (selectedVal)
                    {
                        case 4:
                            selectedVal = 13;
                            break;
                        case 5:
                            selectedVal = 6;
                            break;
                        case 6:
                            selectedVal = 7;
                            break;
                        case 7:
                            selectedVal = 12;
                            break;
                        default :
                            selectedVal += 1;
                            break;
                    }
                    otherParam = PeriodID != null ? "&Period=" + selectedVal : "";
                    DrillThrough("C4DE8552-68E4-451E-9F22-F6CEE9359D89", "&drillthrough=" + DrillThroughBussinessId + getPmrQueryString() + getModeQueryString() + otherParam);
                    break;
                case "C4DE8552-68E4-451E-9F22-F6CEE9359D89":
                    string AppraiserName =
                        ((MsLocalReport) e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&DealerId=" + DrillThroughBussinessId + "&AppraiserName=" + AppraiserName + "&name=" + ReportControl1.GetSelectedDealerName;
                    otherParam += PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";

                    DrillThrough("DFE79F27-5AB0-4BDD-9C14-56D48603759E", otherParam + getPmrQueryString() + getModeQueryString());
                    break;
                case "E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6":
                case "D57CF3D5-A075-4032-938C-8A6A2AA5B32B":
                    AppraiserName =
                       ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[3].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "PeriodID") as DropDownList;
                    //TextBox FromDate = ControllerUtils.FindControlRecursive(Page, "FromDate") as TextBox;
                    //TextBox EndDate = ControllerUtils.FindControlRecursive(Page, "EndDate") as TextBox;
                    otherParam = string.Empty;
                    otherParam = "&DealerId=" + DrillThroughBussinessId + "&AppraiserName=" + AppraiserName + "&RetailOrWholesale=R&TradeOrPurchase=";
                    otherParam += ReportControl1.ReportId.Equals("E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6") ? 1 : 2;
                    otherParam += PeriodID != null ? "&PeriodId=" + PeriodID.SelectedValue : "";
                    //otherParam += FromDate != null ? "&FromDate=" + FromDate.Text : "";
                    //otherParam += EndDate != null ? "&EndDate=" + EndDate.Text : "";

                    string reportId = ReportControl1.ReportId.Equals("E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6")
                        ? "B7A99071-736C-4532-8E5E-7E9BEBCBB830"
                        : "C930CB2B-D29F-494D-9EBF-34E172EA9C54";
                    DrillThrough(reportId, otherParam + getPmrQueryString());
                    break;
                case "F6B512EC-D492-47D3-BB7F-5A437D531816":
                    AppraiserName =
                        ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&SelectedDealerId=" + DrillThroughBussinessId + "&AppraiserName=" + AppraiserName;
                    otherParam += PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";
                    DrillThrough("F113B4BB-132F-4E60-9B12-CD46DD9CAABA", otherParam + getPmrQueryString());
                    break;
                case "BE4CD8E3-37C4-4E06-876E-0E6F9746D7A5":
                   string AgeBucketID = 
                        ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "PeriodID") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&SelectedDealerId=" + DrillThroughBussinessId + "&AgeBucketID=" + AgeBucketID;
                    otherParam += PeriodID != null ? "&PeriodID=" + PeriodID.SelectedValue : "";
                    DrillThrough("6FAC8FB3-4C8C-4208-9FCC-6894836EEAF2", otherParam + getPmrQueryString());
                    break;
                case "A4CBC38A-81C0-47D0-BC48-15A7D6F2B701":
                    string ageBucketId =
                         ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    otherParam = string.Empty;
                    otherParam = "&SelectedDealerId=" + DrillThroughBussinessId + "&AgeBucketID=" + ageBucketId;
                    DrillThrough("B4AED734-E7DE-46C1-8225-1A690DC54D35", otherParam + getPmrQueryString());
                    break;
                case "f352752c-9d73-4832-a8c4-2734a9623ad5":
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&drillthrough=" + DrillThroughBussinessId;
                    otherParam += PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";
                    DrillThrough("6ecb009f-cc55-493b-8aab-009821bbf594", otherParam + getPmrQueryString());
                    break;
                case "819d1e2c-e985-407f-9ccd-30a9e2c0afa9":
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&drillthrough=" + DrillThroughBussinessId;
                    otherParam += PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";

                    DropDownList TradeInType = ControllerUtils.FindControlRecursive(Page, "TradeInType") as DropDownList;
                    otherParam += TradeInType != null ? "&TradeInType=" + TradeInType.SelectedValue : "";
                    DrillThrough("98635938-75c3-4d30-ac69-e3352465550f", otherParam + getPmrQueryString());
                    break;
                #region RetailInventorySalesAnalysis
                case "E8E56305-3117-48CF-AB00-CD117F15AE9E":
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "PeriodID") as DropDownList;
                    TradeInType = ControllerUtils.FindControlRecursive(Page, "TradeInType") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = PeriodID != null ? "&PeriodID=" + PeriodID.SelectedValue.ToString() : "";
                    otherParam += TradeInType != null ? "&TradeInType=" + TradeInType.SelectedValue.ToString() : "";
                    DrillThrough("C82458C3-A914-4F38-8741-8AB2CCF7D5AD", "&drillthrough=" + DrillThroughBussinessId + getPmrQueryString() + otherParam );
                    break;
                #endregion
            }
            
            //VehicleLevelDrillThrough(DrillThroughBussinessId);
        }
    }

    private string getPmrQueryString()
    {
        if (Request.QueryString.HasKeys() && Request.QueryString.GetValues("type") != null &&
            Convert.ToString(Request.QueryString.GetValues("type")[0]) == "pmr")
        {
            return "&type=pmr";
        }
        return string.Empty;
    }
    private string getModeQueryString()
    {
        if (Request.QueryString.HasKeys() && Request.QueryString.GetValues("Mode") != null )
        {
            return "&Mode=" + Convert.ToString(Request.QueryString.GetValues("Mode")[0]);
        }
        return string.Empty;
    }
    private void DrillThrough(string id, string otherParameters = "")
    {
        Response.Redirect("~/DealerGroupNewReport.aspx?Id=" + id + otherParameters);
    }
}