using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Dundas.Charting.WebControl;

public partial class InventoryPricingRisk : DealerGroupPage
{
	protected List<GridViewRow> footerRows;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        base.IsAnalyicsEnabled = true;
        Page.Title = "Inventory Pricing Risk";
    }

    protected void DealerGroup_Init(object sender, EventArgs e)
    {
        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
    }

    protected void ChartTitle_Init(object sender, EventArgs e)
    {
        ChartTitle.Text = "Inventory Pricing Risk";
    }

    protected void SalesStrategyList_PreRender(object s, EventArgs e)
    {
        //SalesStrategyLabel.Text = SaleStrategyList.SelectedItem.Text;
    }

    protected void SalesStrategyList_SelectedIndexChanged(object sender, EventArgs e)
    {
        InventoryPricingRiskGridView.PageIndex = 0;
    }

    protected void PricingGraphLabelDetailToggle_Click(object sender, EventArgs e)
    {
        bool enabled = Convert.ToBoolean(PricingGraphLabelDetailEnabled.Value);

        PricingGraphLabelDetailEnabled.Value = enabled ? bool.FalseString : bool.TrueString;

        PricingGraphLabelDetailToggle_Load(sender, e);
    }

    protected void PricingGraphLabelDetailToggle_Load(object sender, EventArgs e)
    {
        bool enabled = Convert.ToBoolean(PricingGraphLabelDetailEnabled.Value);

        PricingGraphLabelDetailToggle.Text = enabled ? "Hide Units" : "Show Units";

        if (enabled)
        {
            PricingGraphLabelDetailEnabled.Value = Boolean.TrueString;
        }
        else
        {
            PricingGraphLabelDetailEnabled.Value = Boolean.FalseString;
        }
    }

    protected void PricingGraphByRisk_OnDataBound(object s, EventArgs e)
    {
        // set custom y axis labels

        ChartArea area = PricingGraphByRisk.ChartAreas[0];

        PricingGraphByRisk.BackImage = "~/App_Themes/" + Page.StyleSheetTheme + "/Images/risk-graph-bg.gif";

        for (int i = 0; i < 125; i += 25)
        {
            area.AxisY2.CustomLabels.Add(i - 10, i + 10, i + "%");
        }

        area.AxisY2.LabelStyle.Font = new Font("Arial Bold", 7);

        // create fancy labels and capture click events

        bool enabled = Convert.ToBoolean(PricingGraphLabelDetailEnabled.Value);
        int colIdx = Convert.ToInt32(ColumnIndex.Value);

        Series series = PricingGraphByRisk.Series[0];

        for (int c = 0; c < series.Points.Count; c++)
        {
            DataPoint point = series.Points[c];
            
            double pct = point.YValues[0];
            double num = point.YValues[1];
            double idx = point.YValues[2];

            point.Font = new Font("Arial Bold", 7, FontStyle.Bold);

            string text = enabled
                ? string.Format("{0} {1}\n{2:0}%", num, (Convert.ToInt32(num) == 1 ? "Unit" : "Units"), pct)
                : string.Format("{0:0}%", pct);

            // Column color should be based on risk index 
            if (c == Convert.ToInt32(Request.QueryString["RiskIndex"])-1)
			    point.Color = Color.FromArgb(255, 153, 0, 0);

            string clientScript = string.Format("JavaScript:UpdateColumnIndex('{0}','{1}','{2}');", PostBackButton.ClientID, ColumnIndex.ClientID, idx);

            point.ToolTip = num + " units";
            point.Href = clientScript;
            point.Label = text;
            point.ToolTip = text;

            AddBorder(point, colIdx - 1 == c);

            // custom x axis labels
            area.AxisX.CustomLabels.Add(c + 0.5, c + 1.5, point.AxisLabel);
            HighlightLabel(area.AxisX.CustomLabels[c], colIdx - 1 == c, Page.StyleSheetTheme);
            area.AxisX.CustomLabels[c].Href = clientScript;
        }

        area.AxisX.LabelStyle.Font = new Font("Arial Bold", 7);
    }

    private static void AddBorder(DataPoint point, bool enabled)
    {
        point.BorderStyle = enabled ? ChartDashStyle.Solid : ChartDashStyle.NotSet;
        point.BorderWidth = enabled ? 1 : 0;
        point.BorderColor = enabled ? Color.FromArgb(255, 102, 102, 102) : Color.Empty;
    }

    private static void HighlightLabel(CustomLabel cl, bool enabled, string themeName)
    {
        if (enabled)
        {
            cl.Image = "~/App_Themes/" + themeName + "/Images/risk-graph-bullet.gif";
        }
    }

    protected void PostBackButton_Click(object sender, EventArgs e)
    {
        InventoryPricingRiskGridView.PageIndex = 0;
    }

	protected void InventoryPricingRiskGridView_Init(object sender, EventArgs e)
	{
		footerRows = new List<GridViewRow>();
	}

	protected void InventoryPricingRiskGridView_RowDataBound(object s, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (TableCell th in e.Row.Cells)
            {
                if (th.HasControls())
                {
                    LinkButton lnk = (LinkButton)th.Controls[0];
                    if (lnk != null && InventoryPricingRiskGridView.SortExpression == lnk.CommandArgument)
                        th.CssClass += (InventoryPricingRiskGridView.SortDirection == SortDirection.Ascending ? "ascending" : "descending");
                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // call attention to % of market average if higher than 100%

            int idx = 2; // percentage column

			HyperLink pctHyperlink = (HyperLink)e.Row.Cells[idx].Controls[0];
			pctHyperlink.Target = "IPA";
			string pct = pctHyperlink.Text;			

            if (!string.IsNullOrEmpty(pct) && pct.EndsWith("%"))
                pct = (pct.Length == 1) ? string.Empty : pct.Substring(0, pct.Length - 1);

            if ("&nbsp;".Equals(pct))
                pct = string.Empty;

            if (!string.IsNullOrEmpty(pct))
            {
                int i;
                if (int.TryParse(pct, out i))
                    if (i > 100)
                        e.Row.Cells[idx].CssClass += " warning";
            }

			if (string.IsNullOrEmpty(pct))
			{
				pctHyperlink.Text = "N/A";
			}

            if(e.Row.Cells[5].Text.Contains("(")) {
                e.Row.Cells[5].CssClass += "negativenumber";
            }

            int[] columnsToHighlight = { 1, 2};

            GridViewUtilities.HighlightCells(e.Row, columnsToHighlight);

			if ((int)((DataRowView)e.Row.DataItem)["DrilldownRowType"] != 0)
			{
				e.Row.Cells[1].Text = "-";
				footerRows.Add(e.Row);
				e.Row.Visible = false;
			}
        }
		else if (e.Row.RowType.Equals(DataControlRowType.Footer))
		{
			foreach (GridViewRow footerRow in footerRows)
			{
				GridViewRow r = GridViewUtilities.CopyRow(footerRow, DataControlRowType.Footer, InventoryPricingRiskGridView);
				e.Row.Parent.Controls.Add(r);
			}
			e.Row.Visible = false;
		}
    }	

    protected void ColumnIndex_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string riskIndex = Request.QueryString["RiskIndex"];

            if (!string.IsNullOrEmpty(riskIndex))
            {
                int value;

                if (int.TryParse(riskIndex, out value))
                {
                    ColumnIndex.Value = riskIndex;

                    ColumnIndex_ValueChanged(sender, e);
                }
            }
        }			
    }

    protected void ColumnIndex_ValueChanged(object sender, EventArgs e)
    {
        int value;

        if (int.TryParse(ColumnIndex.Value, out value))
        {
            string headerText = string.Empty;

            if (value == 1)
                headerText = "% Inventory Units with Potential Underpricing Risk";
            else if (value == 2)
                headerText = "% Inventory Units Priced to Market";
            else if (value == 3)
                headerText = "% Inventory Units with Potential Overpricing Risk";
            else if (value == 4)
                headerText = "% Inventory Units with Zero / No Price";
            else if (value == 5)
                headerText = "% Inventory Units Not Analyzed";

            if (!string.IsNullOrEmpty(headerText))
            {
                InventoryPricingRiskGridView.Columns[2].AccessibleHeaderText = headerText;
                InventoryPricingRiskGridView.Columns[2].HeaderText = headerText;
            }
        }
    }
}
