﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASP;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary.ReportDefinitionLanguage;
using MsLocalReport = Microsoft.Reporting.WebForms.LocalReport;
using MsDrillthroughEventArgs = Microsoft.Reporting.WebForms.DrillthroughEventArgs;

public partial class DealerLevelGroupPage : ReportPage
{
    
    protected override void Page_PreInit(object sender, EventArgs e)
    {
        base.Page_PreInit(sender, e);
        base.IsAnalyicsEnabled = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack)
        //{
        Page.Header.Title = ReportControl1.ReportName;    
        //}
        
    }
    protected override string Token()
    {
        return SoftwareSystemComponentStateFacade.DealerComponentToken;
    }
    public void ReportControl1_Drillthrough(object sender, MsDrillthroughEventArgs e)
    {
        if (e.Report != null && ((MsLocalReport)e.Report).OriginalParametersToDrillthrough != null)
        {
            string pmrQueryString = string.Empty;
            if (!ReportControl1.IsDealerGroupReport)
            {
                pmrQueryString = "&type=pmr";
            }
            string DrillThroughBussinessId = ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[0].Values[0].ToString();
            switch (ReportControl1.ReportId)
            {
                case "D06528EE-05F3-4A00-B8C5-403536F103D8":
                    DropDownList PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    string otherParam = string.Empty;
                    otherParam = PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";
                    DrillThrough("C4DE8552-68E4-451E-9F22-F6CEE9359D89", "&drillthrough=" + DrillThroughBussinessId + pmrQueryString + otherParam);
                    break;
                case "C4DE8552-68E4-451E-9F22-F6CEE9359D89":
                    string AppraiserName =
                        ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&DealerId=" + DrillThroughBussinessId + "&AppraiserName=" + AppraiserName + "&name=" + ReportControl1.GetSelectedDealerName;
                    otherParam += PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";

                    DrillThrough("DFE79F27-5AB0-4BDD-9C14-56D48603759E", otherParam + pmrQueryString);
                    break;
                case "E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6":
                case "D57CF3D5-A075-4032-938C-8A6A2AA5B32B":
                     AppraiserName =
                        ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[3].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "PeriodID") as DropDownList;
                    //TextBox FromDate = ControllerUtils.FindControlRecursive(Page, "FromDate") as TextBox;
                    //TextBox EndDate = ControllerUtils.FindControlRecursive(Page, "EndDate") as TextBox;
                    otherParam = string.Empty;
                    otherParam = "&DealerId=" + DrillThroughBussinessId + "&AppraiserName=" + AppraiserName + "&RetailOrWholesale=R&TradeOrPurchase=";
                    otherParam += ReportControl1.ReportId.Equals("E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6") ? 1 : 2;
                    otherParam += PeriodID != null ? "&PeriodId=" + PeriodID.SelectedValue : "";
                    //otherParam += FromDate != null ? "&FromDate=" + FromDate.Text : "";
                    //otherParam += EndDate != null ? "&EndDate=" + EndDate.Text : "";

                    string reportId = ReportControl1.ReportId.Equals("E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6")
                        ? "B7A99071-736C-4532-8E5E-7E9BEBCBB830"
                        : "C930CB2B-D29F-494D-9EBF-34E172EA9C54";
                    DrillThrough(reportId, otherParam + pmrQueryString);
                    break;
                case "F6B512EC-D492-47D3-BB7F-5A437D531816":
                    AppraiserName =
                        ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&SelectedDealerId=" + DrillThroughBussinessId + "&AppraiserName=" + AppraiserName;
                    otherParam += PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";
                    DrillThrough("F113B4BB-132F-4E60-9B12-CD46DD9CAABA", otherParam + pmrQueryString);
                    break;
                case "BE4CD8E3-37C4-4E06-876E-0E6F9746D7A5":
                    string AgeBucketID = 
                        ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "PeriodID") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = "&SelectedDealerId=" + DrillThroughBussinessId + "&AgeBucketID=" + AgeBucketID;
                    otherParam += PeriodID != null ? "&PeriodID=" + PeriodID.SelectedValue : "";
                    DrillThrough("6FAC8FB3-4C8C-4208-9FCC-6894836EEAF2", otherParam + pmrQueryString);
                    break;
                case "A4CBC38A-81C0-47D0-BC48-15A7D6F2B701":
                    string ageBucketId =
                        ((MsLocalReport)e.Report).OriginalParametersToDrillthrough[1].Values[0];
                    otherParam = string.Empty;
                    otherParam = "&SelectedDealerId=" + DrillThroughBussinessId + "&AgeBucketID=" + ageBucketId;
                    DrillThrough("B4AED734-E7DE-46C1-8225-1A690DC54D35", otherParam + pmrQueryString);
                    break;
                case "f352752c-9d73-4832-a8c4-2734a9623ad5":
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";
                    DrillThrough("6ecb009f-cc55-493b-8aab-009821bbf594", "&drillthrough=" + DrillThroughBussinessId + pmrQueryString + otherParam);
                    break;
                case "819d1e2c-e985-407f-9ccd-30a9e2c0afa9":
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = PeriodID != null ? "&Period=" + PeriodID.SelectedValue : "";
                    DrillThrough("98635938-75c3-4d30-ac69-e3352465550f", "&drillthrough=" + DrillThroughBussinessId + pmrQueryString + otherParam);
                    break;
                #region RetailInventorySalesAnalysis
                case "E8E56305-3117-48CF-AB00-CD117F15AE9E":
                    PeriodID = ControllerUtils.FindControlRecursive(Page, "PeriodID") as DropDownList;
                    DropDownList TradeInType = ControllerUtils.FindControlRecursive(Page, "TradeInType") as DropDownList;
                    otherParam = string.Empty;
                    otherParam = PeriodID != null ? "&PeriodID=" + PeriodID.SelectedValue.ToString() : "";
                    otherParam += TradeInType != null ? "&TradeInType=" + TradeInType.SelectedValue.ToString() : "";
                    DrillThrough("C82458C3-A914-4F38-8741-8AB2CCF7D5AD", "&drillthrough=" + DrillThroughBussinessId + pmrQueryString + otherParam);
                    break;
                #endregion
            }

            //VehicleLevelDrillThrough(DrillThroughBussinessId);
        }
    }
    private void DrillThrough(string id, string otherParameters = "")
    {
        Response.Redirect("~/DealerLevelGroupPage.aspx?Id=" + id + otherParameters);
    }
}