<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AppraisalOverview.aspx.cs" Inherits="Home_AppraisalOverview" MasterPageFile="~/Master_Pages/Page_Popup.master" %>

<%@ Register TagPrefix="firstlook" TagName="Email" Src="~/Controls/Common/Email.ascx" %>

<asp:Content ID="PopupHeader" ContentPlaceHolderID="Page_Header" runat="server">
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <firstlook:Email ID="Email2" OnSend="EmailClient_Send" Subject="Appraisal Quick Facts" runat="server" />
        <h2><asp:Literal ID="DealerGroup" runat="server" OnInit="DealerGroup_Init"></asp:Literal></h2>
        <h1><asp:Literal ID="ChartTitle" runat="server" OnInit="ChartTitle_Init"></asp:Literal></h1>
    </div>
</asp:Content>

<asp:Content ID="PopupContent" ContentPlaceHolderID="Page_Body" runat="Server">
    <h1 id="graphheader"><asp:Literal ID="GraphTitle" runat="server"></asp:Literal></h1>
    <!-- appraisal time period -->
    <cwc:AsyncObjectDataSourceControl
        ID="AppraisalOverview_TimePeriod_DataSource"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.TimePeriodDataService"
        runat="server">
    </cwc:AsyncObjectDataSourceControl>
    
    <a id="DrilldownTimeSpan" class="dyna_link">
        <asp:Label ID="AppraisalClosingRate_TimePeriodLabel" runat="server" />
        <asp:Image ID="Image1" SkinID="MenuIcon" CssClass="hide_nonJS" runat="server"/>
    </a>
    <div id="selectDrilldownTimeRange" class="inlinepopup selectbox" style="display:none;">
        <div id="closeDrilldownTimeRange" class="closebox right"></div>
        <h3>Time Period for Appraisal Overview:</h3>
        <asp:DropDownList
            ID="AppraisalOverview_TimePeriod"
            DataSourceID="AppraisalOverview_TimePeriod_DataSource"
            DataTextField="Name"
            DataValueField="Value"
            AutoPostBack="true"
            OnSelectedIndexChanged="AppraisalClosingRate_TimePeriod_SelectedIndexChanged"
            runat="server">
        </asp:DropDownList>
    </div>

    <!-- appraisal overview table -->
    <cwc:AsyncObjectDataSourceControl
        ID="AppraisalOverview_DataSource"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalOverviewTableDataService"
        runat="server">
        <Parameters>
            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
            <owc:MemberParameter Name="member" Type="Object" />
            <asp:ControlParameter Name="timePeriodId" Type="String" ControlID="AppraisalOverview_TimePeriod" />
            <asp:QueryStringParameter Name="reportMode" Type="String" QueryStringField="mode" DefaultValue="TradeInInventoryAnalyzed" />
        </Parameters>
    </cwc:AsyncObjectDataSourceControl>
    <asp:GridView ID="AppraisalOverviewGrid" CssClass="popuptable" OnRowDataBound="AppraisalOverviewGrid_OnRowDataBound" runat="server" DataSourceID="AppraisalOverview_DataSource" AutoGenerateColumns="False" UseAccessibleHeader="true" AllowSorting="true" OnInit="AppraisalOverviewGrid_Init" ShowFooter="true">
        <Columns>
            <asp:BoundField DataField="TradeInInventoryAnalyzedRank" HeaderText="In-Group Rank" SortExpression="TradeInInventoryAnalyzedRank" />
            <asp:BoundField DataField="AvgImmediateWholesaleProfitRank" HeaderText="In-Group Rank" SortExpression="AvgImmediateWholesaleProfitRank" />
            <asp:BoundField DataField="Dealer" HeaderText="Dealership" SortExpression="Dealer" />
            
            <%-- Wholesale Mode --%>
            <asp:TemplateField HeaderText="Avg. Imm. Wholesale Profit/Loss" SortExpression="AvgImmediateWholesaleProfitInPeriod">
                <ItemTemplate>
                     <a href="DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=<%#Eval("Dealer_ID")%>"><asp:Label CssClass="percentbar dyna_link" runat="server" ID="WholesaleBar"></asp:Label></a>
                     <asp:Label CssClass="percentlegend" runat="server" ID="WholesaleLegend"><a href="DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode=<%#Eval("Dealer_ID")%>"><%# Eval("AvgImmediateWholesaleProfitInPeriod", "{0:C0}")%></a></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <%-- Trade-In Mode --%>
            <asp:TemplateField HeaderText="Trade-In Inventory Analyzed" SortExpression="TradeInInventoryAnalyzed">
                <ItemTemplate><div class="metric">
                            <div class="percentbar" style="width: <%# ToDouble(Eval("TradeInInventoryAnalyzed")) * 100 %>px"></div>
                            <div class="percentlegend"><%# Eval("TradeInInventoryAnalyzed", "{0:0%}")%></div>
                            </div>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:HyperLinkField  HeaderText="Appraisal Closing Rate" DataTextField="ClosingRate" DataTextFormatString="{0:0%}" SortExpression="ClosingRate" 
                    DataNavigateUrlFields="Dealer_ID" DataNavigateUrlFormatString="~/DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode={0}" />
            <asp:HyperLinkField  HeaderText="Avg. Imm. Wholesale Profit/Loss" DataTextField="AvgImmediateWholesaleProfitInPeriod" DataTextFormatString="{0:C0}" SortExpression="AvgImmediateWholesaleProfitInPeriod" 
                    DataNavigateUrlFields="Dealer_ID" DataNavigateUrlFormatString="~/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode={0}" />

        </Columns>
        
    </asp:GridView>
</asp:Content>