using System;
using System.Diagnostics;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool forbidden = true;

        string token = Request.Params["token"];

        if (User.IsInRole("Administrator") || User.IsInRole("AccountRepresentative"))
        {
            string url = null;

            if (token != null)
            {
                SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, token);

                if (state != null)
                {
                    url = DealerGroupEntryUrl(token, state.DealerGroup.GetValue().Id);
                }
            }

            if (url == null)
            {
                url = "~/Administration/ChooseDestination.aspx";
            }

            Response.Redirect(url, true);

            forbidden = false;
        }
        else if (User.IsInRole("User"))
        {
            Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

            string url = null;

            switch (member.Dealers.Count)
            {
                case 0:
                    break;
                case 1:
                    url = "~/DealerReports.aspx";
                    break;
                default:
                    url = DealerGroupEntryUrl(token, CollectionHelper.First(member.DealerGroups).Id);
                    break;
            }

            if (url != null)
            {
                Response.Redirect(url, true);
                forbidden = false;
            }
        }
        else if (User.IsInRole("Dummy"))
        {
            string sourceName = "CommandCenter";
            string logName = "ApplicationLog";

            if(!EventLog.SourceExists(sourceName))
            {
                EventLog.CreateEventSource(sourceName, logName);
            }

            EventLog log = new EventLog();
            log.Source = sourceName;
            log.WriteEntry(string.Format("User {0} is configured with MemberType of 'Dummy'", User.Identity.Name), EventLogEntryType.Error);
        }

        if (forbidden)
        {
            Response.StatusCode = 403;
            Response.Status = "403 Forbidden";
        }
    }

    private static string DealerGroupEntryUrl(string token, int dealerGroupId)
    {
        if (SoftwareSystemComponentStateFacade.DealerGroupComponentToken.Equals(token))
        {
            return DealerGroupEntryUrl(dealerGroupId);
        }
        else
        {
            return "~/DealerReports.aspx";
        }
    }

    private static string DealerGroupEntryUrl(int dealerGroupId)
    {
        DealerGroupPreferenceFinder finder = DealerGroupPreferenceFinder.Instance();

        DealerGroupPreference dealerGroupPreference = finder.FindByDealerGroup(dealerGroupId);

        string url;

        if (dealerGroupPreference.IncludePerformanceManagementCenter)
        {
            url = "~/PerformanceManagementCenter.aspx";
        }
        else
        {
            url = "~/DealerGroupReports.aspx";
        }

        return url;
    }
}
