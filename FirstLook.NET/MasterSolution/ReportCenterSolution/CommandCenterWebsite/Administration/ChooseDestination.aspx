<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChooseDestination.aspx.cs" Inherits="Administration_ChooseDestination" %>

<%@ Register Assembly="FirstLook.DomainModel.Oltp" Namespace="FirstLook.DomainModel.Oltp.WebControls" TagPrefix="owc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Choose Destination</title>
    <style type="text/css">
fieldset
{
    border: 1px solid #0066FF;
    float: left;
    padding: 5px;
    margin: 5px;
}
legend
{
    color: #0066FF;
    padding: 2px 6px
}
thead
{
    background-color: #0066FF;
}
table,td
{
    border: 1px solid black;
}
tr.table-row-selected
{
    background-color: #BBBBBB;
}
.table-row-pagination
{
    text-align: center;
}
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="DealerGroupFilter">
        <fieldset id="AdministratorMenu" runat="server">
            <legend>Menu</legend>
            <ul>
                <li>
                    <asp:HyperLink ID="ChooseUser" runat="server" NavigateUrl="~/Administration/ChooseUser.aspx">Choose a User</asp:HyperLink>
                </li>
            </ul>
        </fieldset>
        <fieldset>
            <legend>Command Center</legend>
            <div>
                <asp:Label
                    ID="DealerGroupFilterLabel"
                    AssociatedControlID="DealerGroupFilter"
                    runat="server"
                    Text="Filter Name: ">
                </asp:Label>
                <asp:TextBox
                    ID="DealerGroupFilter"
                    runat="server"
                    AutoPostBack="true"
                    OnTextChanged="DealerGroupFilter_TextChanged">
                </asp:TextBox>
            </div>
            <div>
                <asp:ObjectDataSource
                    ID="DealerGroupDataSource"
                    TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder"
                    SelectMethod="FindAllDealerGroupsForMember"
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerGroupFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGroupGridView" runat="server" DataKeyNames="Id" DataSourceID="DealerGroupDataSource" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" OnRowCommand="DealerGroupGridView_RowCommand" OnPageIndexChanged="DealerGroupGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="ID" ItemStyle-Font-Size="XX-Small" />
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:ButtonField ButtonType="Button" Text="Enter" CommandName="EnterDealerGroup" />
                        <asp:CommandField ButtonType="Button" SelectText="Show Dealers" ShowSelectButton="true" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
        <fieldset>
            <legend>Dealer Reports</legend>
            <div>
                <asp:Label
                    ID="DealerFilterLabel"
                    AssociatedControlID="DealerFilter"
                    runat="server"
                    Text="Filter Name: ">
                </asp:Label>
                <asp:TextBox
                    ID="DealerFilter"
                    runat="server"
                    AutoPostBack="true"
                    OnTextChanged="DealerFilter_TextChanged">
                </asp:TextBox>
            </div>
            <div>
                <asp:ObjectDataSource
                    ID="DealerDataSource"
                    TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder"
                    SelectMethod="FindAllDealersForMemberAndDealerGroup"
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="dealerGroupId" Type="Int32" ControlID="DealerGroupGridView" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGridView" runat="server" DataKeyNames="Id" DataSourceID="DealerDataSource" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" UseAccessibleHeader="true" OnRowCommand="DealerGridView_RowCommand" OnPageIndexChanged="DealerGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="ID" ItemStyle-Font-Size="XX-Small" />
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:ButtonField ButtonType="Button" Text="Enter" CommandName="EnterDealer" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
    </form>
</body>
</html>
