using System;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;

public partial class Administration_ChooseDestination : System.Web.UI.Page
{
    //
    // Support Methods
    //

    private static readonly string DealerGroupToken = SoftwareSystemComponentStateFacade.DealerGroupComponentToken;

    private static readonly string DealerToken = SoftwareSystemComponentStateFacade.DealerComponentToken;

    protected SoftwareSystemComponentState SoftwareSystemComponentState
    {
        get { return (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
        set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
    }

    //
    // Page Initialize
    //

    protected void Page_Init(object sender, EventArgs e)
    {
        if (User.IsInRole("Administrator") || User.IsInRole("AccountRepresentative"))
        {
            SoftwareSystemComponentState = LoadState(DealerGroupToken);
        }
        else
        {
            Response.Redirect("~/Default.aspx", true);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        AdministratorMenu.Visible = User.IsInRole("Administrator");
    }

    protected void DealerGroupFilter_TextChanged(object sender, EventArgs e)
    {
        DealerGroupGridView.SelectedIndex = -1;
        DealerFilter.Text = "";
        DealerFilter_TextChanged(sender, e);
    }

    protected void DealerGroupGridView_PageIndexChanged(object sender, EventArgs e)
    {
        DealerGroupFilter_TextChanged(sender, e);
    }

    /// <summary>
    /// Enter Command Center (Dealer Group Reports) Application.
    /// </summary>
    protected void DealerGroupGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EnterDealerGroup"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                int dealerGroupId = Convert.ToInt32(grid.Rows[selectedIndex].Cells[0].Text);

                BusinessUnit dealerGroup = BusinessUnitFinder.Instance().Find(dealerGroupId);

                if (dealerGroup != null && dealerGroup.Dealerships().Count > 1)
                {
                    SoftwareSystemComponentState state = LoadState(DealerGroupToken);
                    state.DealerGroup.SetValue(dealerGroup);
                    state.Dealer.SetValue(CollectionHelper.First(dealerGroup.Dealerships()));
                    state.Member.SetValue(null);
                    state.Save();

                    Response.Redirect("~/PerformanceManagementCenter.aspx", true);
                }
            }
        }
    }

    protected void DealerFilter_TextChanged(object sender, EventArgs e)
    {
        DealerGridView.SelectedIndex = -1;
    }

    /// <summary>
    /// Enter Dealer Reports Application.
    /// </summary>
    protected void DealerGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EnterDealer"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                int dealerId = Convert.ToInt32(grid.Rows[selectedIndex].Cells[0].Text);

                BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

                if (dealer != null)
                {
                    SoftwareSystemComponentState state = LoadState(DealerToken);
                    state.DealerGroup.SetValue(dealer.DealerGroup());
                    state.Dealer.SetValue(dealer);
                    state.Member.SetValue(null);
                    state.Save();

                    Response.Redirect("~/DealerReports.aspx", true);
                }
            }
        }
    }

    protected void DealerGridView_PageIndexChanged(object sender, EventArgs e)
    {
        DealerFilter_TextChanged(sender, e);
    }

    protected SoftwareSystemComponentState LoadState(string componentToken)
    {
        SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

        if (state == null)
        {
            Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

            SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

            state = new SoftwareSystemComponentState();
            state.AuthorizedMember.SetValue(member);
            state.SoftwareSystemComponent.SetValue(component);
        }

        return state;
    }
}
