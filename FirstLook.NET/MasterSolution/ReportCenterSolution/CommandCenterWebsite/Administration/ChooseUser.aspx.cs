using System;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;

public partial class Administration_ChooseUser : System.Web.UI.Page
{
    private static readonly string DealerGroupToken = SoftwareSystemComponentStateFacade.DealerGroupComponentToken;

    private static readonly string DealerToken = SoftwareSystemComponentStateFacade.DealerComponentToken;

    protected void Criteria_TextChanged(object sender, EventArgs e)
    {
        MemberGridView.SelectedIndex = -1;
    }

    protected void ResetCriteriaButton_Click(object sender, EventArgs e)
    {
        FirstNameFilter.Text = "";
        LastNameFilter.Text = "";
        LoginFilter.Text = "";
    }

    protected void MemberGridView_PageIndexChanged(object sender, EventArgs e)
    {
        MemberGridView.SelectedIndex = -1;
    }

    protected void MemberGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EnterCommandCenter"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                Member member = MemberFinder.Instance().Find(Convert.ToInt32(grid.Rows[selectedIndex].Cells[0].Text));

                if (member != null)
                {
                    string url;

                    if (member.Dealers.Count == 1)
                    {
                        CreateState(member, DealerToken);

                        url = "~/DealerReports.aspx";
                    }
                    else
                    {
                        CreateState(member, DealerGroupToken);

                        url = "~/PerformanceManagementCenter.aspx";
                    }

                    Response.Redirect(url, true);
                }
            }
        }
    }

    protected void CreateState(Member member, string componentToken)
    {
        SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

        if (state == null)
        {
            Member authorizedMember = MemberFinder.Instance().FindByUserName(User.Identity.Name);

            SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

            state = new SoftwareSystemComponentState();
            state.AuthorizedMember.SetValue(authorizedMember);
            state.SoftwareSystemComponent.SetValue(component);
        }

        BusinessUnit dealer = CollectionHelper.First(member.Dealers);

        state.DealerGroup.SetValue(dealer.DealerGroup());
        state.Dealer.SetValue(dealer);
        state.Member.SetValue(member);
        state.Save();
    }
}
