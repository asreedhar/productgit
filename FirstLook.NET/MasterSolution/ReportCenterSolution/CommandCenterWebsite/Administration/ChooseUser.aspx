<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChooseUser.aspx.cs" Inherits="Administration_ChooseUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Choose User</title>
    <style type="text/css">
fieldset
{
    border: 1px solid #0066FF;
    float: left;
    padding: 5px;
    margin: 5px;
}
legend
{
    color: #0066FF;
    padding: 2px 6px
}
thead
{
    background-color: #0066FF;
}
table,td
{
    border: 1px solid black;
}
tr.table-row-selected
{
    background-color: #BBBBBB;
}
.table-row-pagination
{
    text-align: center;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <fieldset>
            <legend>Criteria</legend>
            <dl>
                <dt><asp:Label
                    ID="FirstNameFilterLabel"
                    AssociatedControlID="FirstNameFilter"
                    runat="server"
                    Text="First Name: ">
                </asp:Label></dt>
                <dd><asp:TextBox
                    ID="FirstNameFilter"
                    runat="server"
                    AutoPostBack="true"
                    OnTextChanged="Criteria_TextChanged">
                </asp:TextBox></dd>
                <dt><asp:Label
                    ID="LastNameFilterLabel"
                    AssociatedControlID="LastNameFilter"
                    runat="server"
                    Text="Last Name: ">
                </asp:Label></dt>
                <dd><asp:TextBox
                    ID="LastNameFilter"
                    runat="server"
                    AutoPostBack="true"
                    OnTextChanged="Criteria_TextChanged">
                </asp:TextBox></dd>
                <dt><asp:Label
                    ID="LoginFilterLabel"
                    AssociatedControlID="LoginFilter"
                    runat="server"
                    Text="Login: ">
                </asp:Label></dt>
                <dd><asp:TextBox
                    ID="LoginFilter"
                    runat="server"
                    AutoPostBack="true"
                    OnTextChanged="Criteria_TextChanged">
                </asp:TextBox></dd>
            </dl>
            <asp:LinkButton ID="ResetCriteriaButton" runat="server" onclick="ResetCriteriaButton_Click">Reset</asp:LinkButton>
            <asp:LinkButton ID="SubmitCriteriaButton" runat="server">Submit</asp:LinkButton>
        </fieldset>
        <fieldset>
            <legend>Users</legend>
            <div>
                <asp:ObjectDataSource
                    ID="MemberDataSource"
                    TypeName="FirstLook.DomainModel.Oltp.MemberFinder"
                    SelectMethod="FindAllUsersForCriteria"
                    runat="server">
                    <SelectParameters>
                        <asp:ControlParameter Name="firstNameFilter" Type="string" ControlID="FirstNameFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter Name="lastNameFilter" Type="string" ControlID="LastNameFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter Name="userNameFilter" Type="string" ControlID="LoginFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="MemberGridView" runat="server" DataKeyNames="Id" DataSourceID="MemberDataSource" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" OnRowCommand="MemberGridView_RowCommand" OnPageIndexChanged="MemberGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="ID" ItemStyle-Font-Size="XX-Small" />
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                        <asp:BoundField DataField="UserName" HeaderText="User Name" />
                        <asp:ButtonField ButtonType="Button" Text="Enter Command Center" CommandName="EnterCommandCenter" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
    </div>
    </form>
</body>
</html>
