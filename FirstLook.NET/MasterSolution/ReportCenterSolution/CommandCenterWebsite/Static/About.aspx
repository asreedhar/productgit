﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="Static_About" Title="Pre-Owned Performance Management Center" %>
<%@ Register TagPrefix="firstlook" TagName="SiteFooter" Src="~/Controls/Common/SiteFooter.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <title>Pre-Owned Performance Management Reports</title>


    <link runat="server" enableviewstate="false" id="StaticCssLink" type="text/css" rel="stylesheet" href="static.css" onprerender="StaticCssLink_PreRender" />
    
 </head>
<body>
<div id="wrap_">
    <form id="Form1" runat="server">
    <span class="close right" onclick="window.close()">Close Window</span>
        <div id="container">
<p>Headquartered in Chicago, business intelligence &amp; performance management software pioneer First Look empowers traditionally non-analytically skilled or inclined management with the benefits of sophisticated analytical insight.
</p>

<h3>Background</h3>

<p>One of the most powerful drivers of economic growth in the 1990s was the revolution in the precision of business decision making. By introducing increasingly sophisticated analytical tools that could be operated by analytically skilled general management personnel, large corporations were able to catalyze significantly increased efficiency and profitability in their operations. At the same time, these powerful developments failed to take root beyond the ranks of already analytically skilled and inclined management.
</p>

<h3>The First Look Solution</h3>

<p>First Look empowers non-analytically skilled or inclined managers with the high level of analytical sophistication that senior Fortune 500 management harnessed to drive unprecedented efficiency and profitability. By moving beyond existing "do-it-yourself" tools and reports into sophisticated yet easy to use "do-it-for-you" tools and systems, First Look introduces the power of data driven decision making to non-analytically skilled or inclined line management.
</p>

<p>First Look's unique solution combines tools built around industry best practices with a unique and proprietary Artificial Intelligence (Expert System) approach that provides the benefits of complex analytics to managers that either lack the time, inclination or skills to perform such analyses on their own. In place of current data intensive reports, First Look Insight automatically highlights in plain English text on a single page the key findings that an analytically skilled user would obtain through exhaustive analysis of dozens of traditional data reports. The defining characteristic of all First Look products is their unique combination of exceptional quickness and ease of use with maximum analytical insight.
</p>

<p>First Look currently implements this revolutionary analytic software in the $500 billion automotive retail market. First Look’s major products for the automotive retail market include:
</p>

 <ul>
 <li> 
    <strong>Performance Dashboard powered by Insight</strong> &mdash; a revolutionary Performance Management system that enables non-analytical managers to quickly determine key areas for improvement while obtaining the penetrating insight necessary to drive performance improvement.  
 </li>
 <li>
     <strong>Variable Operations Best Practices Tools</strong> &mdash; allows managers to integrate sophisticated analysis into daily activities while making it easy to consistently execute and track compliance with industry best practices.  
 </li>
 <li>
    <strong>Dynamic Reporting Software</strong> &mdash; enables managers, even those with limited computer savvy, to quickly and easily make optimal inventory management decisions.  
 </li>
</ul>
</div>
    </form>
    <firstlook:SiteFooter id="SiteFooter" runat="server" />
</div>
</body>
</html>
