<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Static_Contact" Title="Pre-Owned Performance Management Center" %>
<%@ Register TagPrefix="firstlook" TagName="SiteFooter" Src="~/Controls/Common/SiteFooter.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <title>Pre-Owned Performance Management Reports</title>

    <link runat="server" enableviewstate="false" id="StaticCssLink" type="text/css" rel="stylesheet" href="static.css" onprerender="StaticCssLink_PreRender" />
 </head>
<body>
<div id="wrap_">
    <form id="Form1" runat="server">
    <span class="close right" onclick="window.close()">Close Window</span>
        <div id="container">
<blockquote>
   <p>
       <strong>Corporate Office</strong><br />
       815 W. Jackson Blvd.<br />
       Suite 800<br />
       Chicago, IL 60607<br />
       Toll Free &mdash; (877) 378-LOOK [378-5665]<br />
       Phone &mdash; (312) 279-1234<br />
       Fax &mdash; (312) 492-7086<br />
       <a href="mailto:helpdesk@firstlookmax.com">helpdesk@firstlookmax.com</a>
   </p> 
</blockquote>

</div>
    </form>
    <firstlook:SiteFooter id="SiteFooter" runat="server" />
</div>
</body>
</html>
