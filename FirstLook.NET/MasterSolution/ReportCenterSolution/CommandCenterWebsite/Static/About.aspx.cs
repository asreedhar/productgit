using System;
using System.Web.UI.HtmlControls;

public partial class Static_About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void StaticCssLink_PreRender(object sender, EventArgs e)
    {
        ((HtmlLink)sender).Attributes["href"] = ResolveClientUrl("~/App_Themes/" + Page.StyleSheetTheme + "/" + ((HtmlLink)sender).Attributes["href"]);

    }
}
