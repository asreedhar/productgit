var pagePrepped = false;
var numbersLink = "";

// ADJUST when DOM EL is available
Enhance('body_a', adjustDealerGroupReports);


//BrowserDetect - compressed from http://www.quirksmode.org/js/detect.html - needed to adjust offsetHeight.
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?"":e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[(function(e){return d[e]})];e=(function(){return'\\w+'});c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('a A={E:g(){6.t=6.h(6.D)||"Z k t";6.G=6.f(3.8)||6.f(3.I)||"r k G";6.s=6.h(6.z)||"r k s"},h:g(7){M(a i=0;i<7.B;i++){a 9=7[i].4;a H=7[i].q;6.l=7[i].b||7[i].2;d(9){d(9.v(7[i].5)!=-1)c 7[i].2}N d(H)c 7[i].2}},f:g(9){a p=9.v(6.l);d(p==-1)c;c S(9.U(p+6.l.B+1))},D:[{4:3.8,5:"m",b:"m/",2:"m"},{4:3.e,5:"X",2:"Y"},{q:J.K,2:"L"},{4:3.e,5:"x",2:"x"},{4:3.e,5:"O",2:"P"},{4:3.8,5:"u",2:"u"},{4:3.e,5:"w",2:"w"},{4:3.8,5:"n",2:"n"},{4:3.8,5:"F",2:"T",b:"F"},{4:3.8,5:"V",2:"o",b:"W"},{4:3.8,5:"o",2:"n",b:"o"}],z:[{4:3.j,5:"Q",2:"R"},{4:3.j,5:"y",2:"y"},{4:3.j,5:"C",2:"C"}]};A.E();',62,62,'||identity|navigator|string|subString|this|data|userAgent|dataString|var|versionSearch|return|if|vendor|searchVersion|function|searchString||platform|unknown|versionSearchString|OmniWeb|Netscape|Mozilla|index|prop|an|OS|browser|Firefox|indexOf|Camino|iCab|Mac|dataOS|BrowserDetect|length|Linux|dataBrowser|init|MSIE|version|dataProp|appVersion|window|opera|Opera|for|else|KDE|Konqueror|Win|Windows|parseFloat|Explorer|substring|Gecko|rv|Apple|Safari|An'.split('|'),0,{}))



// transform every report and drilldown link into a popup link if we're in the pmc
// register these with the PRM so that ajax loads also trigger these
Event.observe(window, 'load', function() {


	if(pagePrepped == false) {
		PreparePage();
		Sys.WebForms.PageRequestManager.getInstance().add_endRequest(PreparePage);
		Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AjaxErrorHandler);
		pagePrepped = true;
	}
	if(window.location.href.indexOf('showDealershipReports=true') > 0) {
		$('#dealer_reports').show();
	}
});

// this function handles failed ajax requests
function AjaxErrorHandler( sender, e ) {
    if (e.get_error()) {
        alert('An error has occured on your last request, please try again.\nIf the error persists please contact our help desk at 1-877-378-5665');
        e.set_errorHandled( true );
    }
}

// this function should be called every time there's a pageload event.
function PreparePage() {

	if(document.forms[0]) {
		var f = Element.extend(document.forms[0]);
	} else {
		return;
	}
	
	// kill all existing observers
	Event.unloadCache();

	if(f.hasClassName('pmc')) {
	
	    try {
		// these are for pmc
		if(SetEdgeSystemUsageGraphLinks) {
			SetEdgeSystemUsageGraphLinks();
		}
		
		} catch(e) {}
		SetInventoryChartLinks();
		SetNumbersTableLinks();
		ApplyOnClickEvents();
		EnhanceElementsInUpdatePanels();
		EnhanceInlinePopups();
		SetLinksOnTopNavMenuBar();
		ManageDealershipReportsPopup();
		
	} else if (f.hasClassName('pmr')) {
	
		// this is for pmr
		setClickEvent();
	}
	
}


function ManageDealershipReportsPopup() {

	var f = document.forms[0];
	if(f && $('DealershipReportSelectID') && $('closeDealerShipReports')) {
	
		// if we change the dealership in the dealership reports div, set the form action
		// so that the div still shows after the page reloads.
		var selectID = $('DealershipReportSelectID').value;
		Event.observe(selectID, 'change', function() {
			if(f.action.indexOf('showDealershipReports') < 0) {
				f.action += '?showDealershipReports=true';
			}
		});
		
		// if the dealership reports div is closed, remove the showing parameter from the form action
		$('closeDealerShipReports').onclick = function() {
			var theindex = f.action.indexOf('?showDealershipReports=true');
			if(theindex > 0) {
				f.action =
				 f.action.substr(0, theindex) + 
				 f.action.substring(theindex + 27);
			}
		}
	}
}


function SetLinksOnTopNavMenuBar() {
 	
 	// set links on top nav menu bar to pop up
 	if($('topnavmenubar')) {
 		$('topnavmenubar').getElementsByClassName('dyna_link').each(function(e) {
 			Event.observe(e, 'click', function() {
 				window.open(e.href, "_blank", "width=645,height=400,navigation,menubar,resizeable,scrollbars,status");
 				return false;
 			});
 		});
 	}
}


// manage inline popup tiles and their togglers
function EnhanceInlinePopups() {
    enhanceInlinePopup('DealershipHomeLinks','ShowDealershipHomeLinks', 'closeDealershipHomeLinks');
    enhanceInlinePopup('dealer_reports','ShowDealerShipReports', 'closeDealerShipReports', 'form.pmc');
    enhanceInlinePopup('DealerSelection','ShowDealerSelection', 'CloseDealerSelection');
}

function EnhanceElementsInUpdatePanels() {
    enhanceSelectBox('selectAppraisalClosingRateTimeRange', 'appraisalTimeSpan', 'closeAppraisalClosingRateTimeRange', false);
    enhanceSelectBox('selectNumbersTimeRange', 'numbersTimeSpan', 'closeSelectNumbersTimeRange', false);
    enhanceSelectBox('selectAppraisalReviewDaysBack', 'appraisalReviewDaysBack', 'closeAppraisalReviewDays', false);
}

function ApplyOnClickEvents() {

	if(document.forms[0].className && document.forms[0].className == "pmc") {
		var searchlist = $A(document.getElementsByTagName('dl'));
		searchlist = searchlist.concat($A(document.getElementsByTagName('li')));
		searchlist = searchlist.concat($A(document.getElementsByTagName('map')));
		searchlist.each(function(a) {
			var nodelist = $A(a.getElementsByTagName('a'));
			nodelist = nodelist.concat($A(a.getElementsByTagName('area')));
			nodelist.each(function(b) {
				Element.extend(b);
				if(b.hasClassName('reportlink')) {
					Event.observe(b, 'click', function(evt) {
						openReport(b.href, "reportlink");
						Event.stop(evt);
					});
				} else if(b.hasClassName('drilldownlink')) {
					Event.observe(b, 'click', function(evt) {
						openReport(b.href, "drilldownlink");
						Event.stop(evt);
					});
				}
			});
		});
	}
	
}

// make clicking on any of the numbers tables open up a popup.
function SetNumbersTableLinks() {
    if($('numbers') && $('numbers').getElementsByTagName('table')[0]) {
    	$$('#numbers table a').each(function(a) {
    	    Event.observe(a, 'click', function (event) {
    	        if (event.preventDefault) { event.preventDefault(); }
    	        if (window.event) { window.event.cancelBubble = true; }
	        	openReport(a.href, "drilldownlink");
	        	return false;
	    	});
		});
 	}
}

function SetInventoryChartLinks() {
    if($('IsPercentModePointer') && $('invchart-content')) {
	    var isPercentMode = $F($F('IsPercentModePointer'));
        var inventoryChartContainer = $('invchart-content');
        var inventoryChartMap = inventoryChartContainer.getElementsBySelector('area');
        if (inventoryChartMap) {
            inventoryChartMap.each(function (inventoryChartArea) {
                Event.observe($(inventoryChartArea), 'click', function (ev) {
                    var element = Event.element(ev);
                    openReport(element.readAttribute('href') + "&isPercentMode=" + isPercentMode, "drilldownlink");
                    return false;
                });
            });
        }
    }
}

// drilldown reports are hardcoded 760px wide, so open a window 800px wide to accomodate them.
// other reports can be wider, so open new window of same size.
function openReport(url, type) {
	var width = 0;
	var height = 0;
	if (navigator.userAgent.indexOf("MSIE") >= 0) {
		width = document.body.clientWidth;
		height = document.body.clientHeight;
	} else if(navigator.userAgent.indexOf("Firefox") >= 0 ||
	   navigator.userAgent.indexOf("Safari") >= 0 ||
	   navigator.userAgent.indexOf("Opera") >= 0 ) {
		width = window.innerWidth;
		height = window.innerHeight;
	}
	if(width == 0) {
		width = 800;
	}
	if(height == 0) {
		height = 600;
	}
	
	if(type && type=="drilldownlink") {
		window.open(url,'_blank',
			'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=800,height=600');		
	} else if(type && type=="reportlink") {
		window.open(url,'_reports',
			'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=' + width + ',height=' + height);		
	} else {
		window.open(url,'_reports',
			'location=no,status=yes,menubar=yes,toolbar=no,resizable=yes,scrollbars=yes,width=800,height=600');		
	}
}


/* assigns toggle behavior to dealership report headers */
function setClickEvent() {
    var p = $('body_b') || $('container');
    //toggle
    var togs = $A(p.getElementsByTagName('H4')).map(Element.extend);
    togs.each(function(el) {        
        var target = el.up('.group');
        if(target) {
            var img = el.down('img');
                img.openSrc = img.src;
                img.closedSrc = img.src.split('.')[0] + "_closed.gif";
            el.style.cursor = 'pointer';
            el.onclick = function() {
                target.toggleClassName('closed');
                img.src = target.hasClassName('closed') ? img.closedSrc : img.openSrc;
            }
        }
    });
}

/* applies visual enhancements to dealer reports containers 
    + sets floated containers to same height
    + removes bottom border */
function adjustDealerGroupReports(groups) {
   if( document.forms[0].className == 'pmc') { //no enhancements needed
   		return false;
   	}
    var _groups = groups || $A($('body_a').getElementsByTagName('DL'));
    //get max height of containers
    var _max = _groups.pluck('offsetHeight').max() - 10; 
    _groups.each(function(el) {
		el.style.height = _max + 'px'; //sets all container heights to max
		var _kids = $(el).immediateDescendants();
		_kids[_kids.length - 1].style.borderBottom = 'none'; //set _border-bottom="none"_ on last child
    });    
}

function SelectAllCheckBoxes(el) {
    ForEachCheckBox(el, function(node) {node.checked = true;});
}

function ClearAllCheckBoxes(el) {
    ForEachCheckBox(el, function(node) {node.checked = false;});
}

function ForEachCheckBox(child, callback) {
    var fieldset = $(child).up();
    while(fieldset.getElementsBySelector('input[type="checkbox"]').length < 1 && $(fieldset).up()) {
        fieldset = $(fieldset).up();
    }
    if (fieldset != null) {
        fieldset.getElementsBySelector('input[type="checkbox"]').each(callback);
    }
}

function ValidateDealerSelection() {

    var hasSelection = false;
    
    var checkboxes = $A($('DealerSelection').getElementsByTagName('input'));
    checkboxes.each(function(checkbox) {
        if(checkbox.checked == true) {
            hasSelection = true;
        }
    });
    if(hasSelection == false) {
        alert("Please select at least one dealership.");
        return false;
    }
    return true;
}