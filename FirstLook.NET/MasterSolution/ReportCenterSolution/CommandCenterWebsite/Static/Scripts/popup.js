/**
 * REQUIRED 
 *
*/
var pagePrepped = false;
var showRiskClientId;
var showrisk;

Event.observe(window, 'load', function() {
	if(pagePrepped == false) {
		PreparePage();
		Sys.WebForms.PageRequestManager.getInstance().add_endRequest(PreparePage);	
		}
});

function PreparePage() {
	
	// kill all existing observers
	Event.unloadCache();
	
	ManageShowHideRisk();
	CreateDropMenus();
	
	enhanceSelectBox('selectDrilldownTimeRange', 'DrilldownTimeSpan', 'closeDrilldownTimeRange');
	enhanceInlinePopup('email', 'openEmail', 'closeEmail');

	
}

function ManageShowHideRisk() {

	var invGraph = $('inventory-graph');
	if( invGraph) {


		$('inventory-graph').up().getElementsBySelector('span[class="dyna_link toggleriskdisplay hide_nonJS"]').each(function (element) {
			var uls = $A(document.body.getElementsByTagName('ul'));
			var lis = [];

			// this variable will always have the current state of whether the risk is shown or not.
			var showRiskClientId = $F('PercentBarRiskToggleClientID');
			var showrisk = $F(showRiskClientId);
			// check to see if showrisk is set in url
			if(window.location.href.indexOf('ShowRisk') > -1) {
				showrisk = 'ShowRisk';
				addShowRiskToURL();
				element.innerHTML = '(Hide Risk)';
			}

			// for each ul, see if it's a risk breakdown, and if so, hide the risk if necessary
			uls.each(function(ul) {
				Element.extend(ul);
				if(ul.hasClassName('percentbar') && ul.hasClassName('risk')) {
					$A(ul.getElementsByTagName('li')).each(function(s) {
						Element.extend(s);
						if(showrisk == 'HideRisk') {
							s.style.visibility = "hidden";
						}
						lis.push(s);
					});
				}
			});

			// capture the click on the risk toggling control on the page
			// also toggle showrisk variable
			Event.observe(element, 'click', function () {
				if(element.innerHTML.indexOf('Show') >= 0) {
					element.innerHTML = '(Hide Risk)';
					$(showRiskClientId).value = 'ShowRisk';
					addShowRiskToURL();
					lis.each(function(s) {
						s.style.visibility = "visible";
					});

				} else {
					element.innerHTML = '(Show Risk)';
					$(showRiskClientId).value = 'HideRisk';
					// remove show risk from URL so show risk state remains across pages
					$A(document.links).each(function(link){
						if(link.href.indexOf('&ShowRisk=true') > -1) {
							link.href = link.href.split('&ShowRisk=true')[0];                    
						}
					});
					document.aspnetForm.action = document.aspnetForm.action.split('&ShowRisk=true')[0];
					lis.each(function(s) {
						s.style.visibility = "hidden";
					});
				}
				return false;
			});
		});
	}
}


function addShowRiskToURL()
{
	$A(document.links).each(function(link){
		if(link.href.indexOf('CurrentInventoryBreakdown.aspx') > -1 && link.href.indexOf('ShowRisk') < 0) {
			link.href += '&ShowRisk=true';                    
		}
	});
	if(window.location.href.indexOf('ShowRisk') < 0) {
		document.aspnetForm.action += '&ShowRisk=true';
	}
}

function CreateDropMenus() {

    $$(".dropMenu").each(function(e) {
        e.getElementsBySelector("select").each(function(s) {
            s.hide();
        });
        var span = e.getElementsBySelector("span")[0];
        var slct = e.getElementsBySelector("select")[0];
        var savedwidth = span.getWidth() + 15;
        
        Event.observe(e, 'mouseover', function() {
            if(savedwidth > slct.getWidth()) {
                slct.setStyle({
                    width: savedwidth
                });
            }
            span.hide();
            slct.show();
            
            var f = function() {
                span.show();
                slct.hide();
                Event.stopObserving(slct, 'blur', f);
                Event.stopObserving(slct, 'mouseout', f);
            };
            
            Event.observe(slct, 'blur', f);
            Event.observe(slct, 'mouseout', f); 
        });
    });
}