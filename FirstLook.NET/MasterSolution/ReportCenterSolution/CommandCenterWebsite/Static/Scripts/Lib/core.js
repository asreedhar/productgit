/**
 * REQUIRED 
 *
*/

Event.observe(window, 'load', function() {
    page.loaded = true;
    page.init();
    page.observe();
    page.hideProgress();
});

var Page = Class.create();
Page.prototype = {
    initialize: function() {
        this.loaded = false;
    },
    init: function() {
        if(this._has('init')) {
            init();
        }
    },
    observe: function() {
        if(this._has('observe')) {
            observe();
        }
    },
    hideProgress: function() {
        if($('progress')) {
            new Effect.Fade('progress');
        }
    },
    _has: function(str) {
        try {
           if(str == 'init') {
                if(init && typeof init == 'function') return true;
           }
           if(str == 'observe') {
                if(observe && typeof observe == 'function') return true;
           }
        }
        catch(err) {
            return false;
        }
    } 
}
var page = new Page();


function Enhance(el,f) {
    if($(el) != null) f();
    else if (page.loaded != null) setTimeout('Enhance(\''+el+'\','+f+')',100);
}


// takes care of inline popups - opening and closing them, and hiding them initially.
// if check exists, only hide if check is present. otherwise always hide.
function enhanceInlinePopup(inline, opener, closer, check) {
	var go = 1;
	if(check && $$(check).length < 1) {
		go = 0;
	}
    if($(inline) && go == 1) {
        $(inline).hide();
    }
    if($(opener)) {
        Event.observe($(opener), 'click', function() {
        	//new Effect.SlideDown(inline, {duration: 0.2});
            $(inline).toggle();
        });
    }
    if($(closer)) {
        Event.observe($(closer), 'click', function() {
           	//new Effect.SlideUp(inline, {duration: 0.2});
            $(inline).hide();
        });
    }
}


//takes care of inline selects - opening and closing them, getting the right value, and hiding them initially.
function enhanceSelectBox(inline, opener, closer, selectValueAsLabel) {
    if($(inline) && $(opener)) {
        $(inline).hide();
        if( selectValueAsLabel)
        {
            var select = $(inline).getElementsByTagName('select')[0];
            if($(opener).innerHTML.indexOf('<IMG') > -1) {
        	    $(opener).innerHTML = select.options[select.selectedIndex].innerHTML + $(opener).innerHTML.substring($(opener).innerHTML.indexOf('<IMG'));
            } else {
        	    $(opener).innerHTML = select.options[select.selectedIndex].innerHTML;
            }
        }
        Event.observe($(opener), 'click', function() {
        	//new Effect.SlideDown(inline, {duration: 0.2});
            $(inline).show();
        });
     }
     if($(closer)) {
        Event.observe($(closer), 'click', function() {
            //new Effect.SlideUp(inline, {duration: 0.2});
            $(inline).hide();
        });
     }
}

function navigateTo(url) {
	window.location.href = url;
	return false;
}

var Util = {
    name: 'Util',
    
//BROWSER METHODS
	closeWindow: function(){
		window.close();
	},
	printPage:function(){
		window.print();
	},

//DOM METHODS
	make: function(tagname, attributes, children) {
	    // Example: make("p", ["This is a ", make("b", "bold"), " word."]);
        // If we were invoked with two arguments the attributes argument is
        // an array or string, it should really be the children arguments.
        if (arguments.length == 2 && 
            (attributes instanceof Array || typeof attributes == "string")) {
            children = attributes;
            attributes = null;
        }
        var e = document.createElement(tagname);
        // Set attributes
        if (attributes) {
            for(var name in attributes) e.setAttribute(name, attributes[name]);
        }
        // Add children, if any were specified.
        if (children != null) {
            if (children instanceof Array) {  // If it really is an array
                for(var i = 0; i < children.length; i++) { // Loop through kids
                    var child = children[i];
                    if (typeof child == "string")          // Handle text nodes
                        child = document.createTextNode(child);
                    e.appendChild(child);  // Assume anything else is a Node
                }
            }
            else if (typeof children == "string") // Handle single text child
                e.appendChild(document.createTextNode(children));
            else e.appendChild(children);         // Handle any other single child
        }
          // Finally, return the element.
        return e;
	},
	
	
//OBJECT METHODS
	getPropertyNames: function(o) {
		var r = [];
		for(name in o) r.push(name);
		return r;
	},
	copyProperties: function(/*obj*/ from, /*opt obj*/ to) {
		if(!to) to = {};
		for(p in from) to[p] = from[p];
		return to;
	},
	copyUndefinedProperties: function(/*obj*/ from, /*opt obj*/ to) {
		for(p in from) {
			if(!p in to) to[p] = from[p];			
			to[p] = from[p];
		}
	}
}