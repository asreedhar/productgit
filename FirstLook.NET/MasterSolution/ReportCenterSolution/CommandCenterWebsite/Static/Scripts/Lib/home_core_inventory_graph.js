﻿var CoreInventoryGraph = {
    // Variables
    stocked: null,
    optimal: null,
    $body: null,
    $bodyImage: null,
    $barOverlay: null,
    $toolTipContainer: null,
    $toolTip: null,
    $mainDiv: null,
    $barOverlay0: null,
    $barOverlay1: null,
    $barOverlay2: null,
    $barOverlays: null,

    // Methods
    onLoad: function () {
        var coreInvLoaded = false,
            that = this;

        Charting.$body.append('<img id="core-inventory-graph-body-image" src="Static/images/small-graph-body.gif" />');
        this.setUpToolTips();

        this.$bodyImage.load(function () {
            this.renderGraph();
            coreInvLoaded = true;
        });
        setTimeout(function () {
            if (!coreInvLoaded) {
                that.renderGraph();
            }
        }, 1500);

        this.bindEvents();
    },

    bindEvents: function () {
        var that = this;

        this.$barOverlay.bind('mouseenter', function () {
            that.showToolTip(jQuery(this).attr('idx'));
        }).bind('mouseleave', function () {
            that.hideToolTip();
        }).bind('mouseleave', function () {
            that.hideToolTip();
        }).bind('click', function () { // TODO: Clean after BlueWhale
               window.open(that.link, '_blank');
        });
    },

    renderGraph: function () {
        var total = this.optimal,
            inStock = this.stocked,
            outOfStock = total - inStock,
            bottom = 81;

        if (BrowserCheck.IsIE7) {
            bottom = 82;
        } else if (BrowserCheck.IsIE8) {
            bottom = 82;
        } else if (BrowserCheck.IsIE9) {
            bottom = 80;
        } else if (BrowserCheck.IsFirefox) {
            bottom = 78;
        }

        function get_step(t) {
            if (t >= 25) {
                return Math.ceil(t / 25) * 5;
            }
            else if (t >= 12) {
                return Math.ceil(t / 15) * 3;
            }
            else {
                return Math.ceil(t / 10) * 2;
            }
        }

        var step = get_step(total);
        this.$body.kendoChart({
            categoryAxis: {
                line: { width: 0 },
                majorGridLines: { visible: false },
                minorGridLines: { visible: false },
                categories: ['RECOMMENDED', 'IN-STOCK', 'OUT OF STOCK'],
                labels: { font: '7px Arial' }
            },
            valueAxis: {
                labels: { visible: true, font: '9px Arial' },
                line: { width: 0 },
                majorGridLines: { visible: false },
                majorUnit: total >= 10 ? step : null,
                max: Math.max(parseInt(total) + step, 10),
                min: 0,
                minorGridLines: { visible: false }
            },
            chartArea: {
                background: 'none',
                margin: { top: 5, bottom: bottom, left: 15, right: 0 }
            },
            series: [
                {
                    border: { width: 1 },
                    color: '#A2B1BF',
                    data: [total, inStock, outOfStock],
                    gap: 0.8,
                    labels: { background: 'none', color: '#666666', format: '{0}', font: 'bold 11px Arial,Helvetica,sans-serif', position: 'outsideEnd', opacity: 0 }
                }
            ],
            legend: { visible: false },
            seriesDefaults: { type: 'column', stack: true }
        });
        if (BrowserCheck.IsIE7 || BrowserCheck.IsIE8) { this.$body.children('div:first').children('shape:first').remove(); }
    },

    setUpToolTips: function () {
        this.hideToolTip();

        if (BrowserCheck.IsIE7 || BrowserCheck.IsIE8) {
            this.$barOverlay0.css('left', '61px');
            this.$barOverlay1.css('left', '128px');
            this.$barOverlay2.css('left', '198px');
        } else {
            this.$barOverlay0.css('left', '60px');
            this.$barOverlay1.css('left', '127px');
            this.$barOverlay2.css('left', '194px');
        }
        if (BrowserCheck.IsIE) {
            this.$barOverlays.css('background-color', '#fff').css('opacity', '0.01');
        }
    },

    showToolTip: function (barIndex) {
        var left, tooltipVal,
            outOfStock = this.optimal - this.stocked;

        switch (parseInt(barIndex)) {
            case 0:
                left = -127;
                tooltipVal = this.optimal;
                break;
            case 1:
                left = -60;
                tooltipVal = this.stocked;
                break;
            case 2:
                left = 5;
                tooltipVal = outOfStock;
                break;
            default:
                return;
        }

        tooltipVal = Math.round(tooltipVal * 10) / 10;
        this.$toolTip.html(tooltipVal);
        this.$toolTipContainer.css('left', left + 'px').css('visibility', 'visible').fadeIn('fast');
    },

    hideToolTip: function () {
        this.$toolTipContainer.css('visibility', 'hidden');
    }
};
