
var CurrentInventoryGraph = (function ($) {

    var layout = {
        horzSpacerProportion: 0.20,
        graphPaddingLeftSixBars: 25,
        graphPaddingRightSixBars: 15,
        graphPaddingLeft: 20,
        graphPaddingRight: 20,
        bookMinusCostLabelHeight: 15,
        percentLabelHeight: 15,
        verticalPadding: 1,
        ageLabelHeight: 21
    },
        quickPlanningUrl = "#",
        barData = null,
        root$ = null;

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function foreachBar(f) {
        var i, bar;
        for (i = 0; i < barData.length; ++i) {
            f(i, barData[i]);
        }
    }

    function calcAgeLabelText(bar) {
        var str;
        if (bar.RangeLow == null || bar.RangeHigh == null) {
            return '';
        }
        str = bar.RangeLow;
        if (bar.RangeLow > bar.RangeHigh) {
            str += '+';
        } else {
            str += '-' + bar.RangeHigh;
        }
        return str;
    }

    function calcAgeLabelImg(i, hasLabelLights) {
        var src;
        if (!hasLabelLights || i >= 2) {
            return '';
        }
        if (i == 0) {
            src = 'Static/images/dot-redyellow.gif';
        }
        else {
            src = 'Static/images/dot-green-ctr.gif';
        }
        return '<img src="' + src + '" />';
    }

    function calcQuickPlanningUrl(bar) {
        return quickPlanningUrl.replace(/rangeId=\d+/, 'rangeId=' + bar.RangeId);
    }

    function calcBookMinusCostLabelText(val) {
        return val < 0
            ? '$(' + addCommas(-val) + ')'
            : '$' + addCommas(val);
    }

    function calcToolTipText(units, red, yellow, green) {
        var str = units + ' Vehicle',
            ryg = [];

        if (green > 0) {
            ryg.push(green + ' Green');
        }
        if (yellow > 0) {
            ryg.push(yellow + ' Yellow');
        }
        if (red > 0) {
            ryg.push(red + ' Red');
        }

        ryg = ryg.join(', ');
        if (ryg.length > 0) {
            str += ': ' + ryg;
        }

        return str;
    };

    function calcContent() {
        var hasLabelLights = barData.length >= 2 && barData[0].RangeLow == barData[1].RangeLow;
        foreachBar(function (i, bar) {
            bar.tooltipText = calcToolTipText(bar.Units, bar.Red, bar.Yellow, bar.Green);

            bar.bookMinusCostLabelText = calcBookMinusCostLabelText(bar.BookMinusCost);
            bar.bookMinusCostClass = bar.BookMinusCost < 0 ? "negative" : "positive";

            bar.percentLabelText = bar.Percentage + '%';

            bar.ageLabelText = calcAgeLabelText(bar);
            bar.ageLabelImg = calcAgeLabelImg(i, hasLabelLights);

            bar.url = calcQuickPlanningUrl(bar);
        });
    }

    function normalizeHeight(gyr) {
        var ypx = 0,
            yf = 0,
            y,
            i;
        for (i = 0; i < gyr.length; ++i) {
            yf += gyr[i];
            y = Math.round(yf) - ypx;
            ypx += y;
            gyr[i] = y;
        }
    }

    function assignToGyr(gyr, bar) {
        // Position green, yellow, red boxes
        bar.redBottom = layout.ageLabelHeight + 1;
        bar.redHeight = gyr[3];
        bar.yellowBottom = bar.redBottom + bar.redHeight;
        bar.yellowHeight = gyr[2];
        bar.greenBottom = bar.yellowBottom + bar.yellowHeight;
        bar.greenHeight = gyr[1];
    }

    function calcLayout() {
        var barCount = barData.length;
        if (barCount == 6) // Make the first column line up with the vertical bar only when there are six bars.
        {
            layout.graphPaddingLeft = layout.graphPaddingLeftSixBars;
            layout.graphPaddingRight = layout.graphPaddingRightSixBars;
        }

        var totalWidth = root$.width() - layout.graphPaddingLeft - layout.graphPaddingRight,
            totalHeight = root$.height(),
            width,
            barWidth,
            horzSpacer,
            curX = layout.graphPaddingLeft,
            x1,
            x2,
            maxHeight;

        if (barCount <= 0)
            return;

        barWidth = totalWidth / (barCount + (barCount - 1) * layout.horzSpacerProportion);
        horzSpacer = barWidth * layout.horzSpacerProportion;
        maxHeight = totalHeight -
            layout.ageLabelHeight - layout.percentLabelHeight - layout.bookMinusCostLabelHeight - layout.verticalPadding * 2;

        foreachBar(function (i, bar) {
            var barheight,
                availableHeight,
                gyr = [0, 0, 0, 0],
                y1, y2, j;

            // Calculate horizontal offset
            x1 = Math.round(curX);
            curX += barWidth;
            x2 = Math.round(curX);
            curX += horzSpacer;

            // Calculate bounding panel
            bar.left = x1;
            bar.width = x2 - x1 + 1;
            bar.top = 0;
            bar.height = totalHeight;

            // Position elements
            barHeight = Math.min(maxHeight, Math.max(1, Math.ceil(maxHeight * bar.Percentage / 100)));
            bar.grayBottom = layout.ageLabelHeight;
            bar.grayHeight = Math.max(1, barHeight);
            bar.grayInitialHeight = Math.min(2, barHeight);

            bar.percentInitialBottom = bar.grayBottom + bar.grayInitialHeight + layout.verticalPadding;
            bar.percentBottom = bar.grayBottom + bar.grayHeight + layout.verticalPadding;

            bar.bookMinusCostInitialBottom = bar.percentInitialBottom + layout.percentLabelHeight + layout.verticalPadding;
            bar.bookMinusCostBottom = bar.percentBottom + layout.percentLabelHeight + layout.verticalPadding;

            bar.tooltipMinX = -bar.left + layout.graphPaddingLeft;
            bar.tooltipMaxX = totalWidth - bar.left + layout.graphPaddingLeft;

            // Calc size for green, yellow, red boxes
            availableHeight = barHeight - 2;

            if (bar.Units > 0) {
                gyr[0] = availableHeight * (bar.Units - bar.Green - bar.Yellow - bar.Red) / bar.Units;
                gyr[1] = availableHeight * bar.Green / bar.Units;
                gyr[2] = availableHeight * bar.Yellow / bar.Units;
                gyr[3] = availableHeight * bar.Red / bar.Units;
            }

            normalizeHeight(gyr);
            // Position green, yellow, red boxes
            assignToGyr(gyr, bar);
        });
    }

    function centerTooltip(tooltip$, barWidth, minX, maxX) {
        var widthTT = tooltip$.outerWidth(),
            left = (barWidth - widthTT) / 2;

        // Constrain to maximum coordinates
        if (left < minX) {
            left = minX;
        } else if (left + widthTT > maxX) {
            left = maxX - widthTT;
        }

        tooltip$.css('left', left);
    }

    function createElements() {
        foreachBar(function (i, bar) {

            bar.dollars$ = $('<div class="bookMinusCost"></div>');
            bar.dollars$.addClass(bar.bookMinusCostClass);
            bar.dollars$.text(bar.bookMinusCostLabelText);
            bar.dollars$.css({
                width: bar.width + 'px',
                bottom: bar.bookMinusCostInitialBottom + 'px'
            });

            bar.percent$ = $('<div class="percent"></div>');
            bar.percent$.text(bar.percentLabelText);
            bar.percent$.css({
                width: bar.width + 'px',
                bottom: bar.percentInitialBottom + 'px'
            });

            if (bar.grayHeight > 2) {
                bar.gray$ = $('<div class="gray"></div>');
                bar.gray$.css({
                    width: Math.max(0, bar.width - 2) + 'px', // subtract for border
                    bottom: bar.grayBottom + 'px',
                    height: Math.max(0, bar.grayInitialHeight - 2) + 'px' // subtract for border
                });
            }
            else {
                bar.gray$ = $('<div class="solidgray"></div>');
                bar.gray$.css({
                    width: Math.max(0, bar.width) + 'px',
                    bottom: bar.grayBottom + 'px',
                    height: Math.max(0, bar.grayHeight) + 'px'
                });
            }

            bar.hover$ = $('<div class="hoverContainer"></div>');
            bar.hover$.css('height', bar.height + 'px');

            bar.tooltip$ = $('<div class="tooltip"></div>');
            bar.tooltip$.text(bar.tooltipText);

            bar.red$ = $('<div class="red" />');
            bar.red$.css({
                width: Math.max(0, bar.width - 2) + 'px',
                bottom: bar.redBottom + 'px',
                height: Math.max(0, bar.redHeight) + 'px'
            });

            bar.yellow$ = $('<div class="yellow" />');
            bar.yellow$.css({
                width: Math.max(0, bar.width - 2) + 'px',
                bottom: bar.yellowBottom + 'px',
                height: Math.max(0, bar.yellowHeight) + 'px'
            });

            bar.green$ = $('<div class="green" />');
            bar.green$.css({
                width: Math.max(0, bar.width - 2) + 'px',
                bottom: bar.greenBottom + 'px',
                height: Math.max(0, bar.greenHeight) + 'px'
            });

            bar.hover$.append(bar.red$, bar.yellow$, bar.green$, bar.tooltip$);

            bar.ageLabel$ = $('<div class="ageLabel"></div>');
            if (bar.ageLabelImg) {
                bar.ageLabel$.append(bar.ageLabelImg);
            }
            bar.ageLabel$.append(bar.ageLabelText);
            bar.ageLabel$.css('width', bar.width + 'px');
            bar.panel$ = $('<a class="bar" target="popup" onclick="window.open(' + bar.url + i + ' , "mywin","left=20,top=20,width=600,height=500,toolbar=1,resizable=0"); return=false; " />');
            bar.panel$.attr('href', bar.url + i);
            bar.panel$.css({
                top: bar.top + 'px',
                height: bar.height + 'px',
                left: bar.left + 'px',
                width: bar.width + 'px'
            });
            bar.panel$.append(bar.dollars$, bar.percent$, bar.gray$, bar.ageLabel$, bar.hover$);

            root$.append(bar.panel$);

            centerTooltip(bar.tooltip$, bar.width, bar.tooltipMinX, bar.tooltipMaxX);
            bar.hover$.css({ display: 'none', visibility: 'visible' });
        });
    }

    function animateElements(continuation) {
        var duration = 'slow',
            count = 0,
            options = {
                queue: false,
                complete: function () {
                    count--;
                    if (count <= 0) {
                        continuation();
                    }
                }
            };

        foreachBar(function (i, bar) {
            if (bar.grayHeight > 2) {
                count++;
                bar.gray$.animate({ height: (bar.grayHeight - 2) + 'px' }, options);
            }
            count++;
            bar.percent$.animate({ bottom: bar.percentBottom + 'px' }, options);
            count++;
            bar.dollars$.animate({ bottom: bar.bookMinusCostBottom + 'px' }, options);
        });
    }

    function bindEvents() {
        foreachBar(function (i, bar) {
            bar.panel$.hover(function () {
                bar.panel$.css('z-index', 500);
                bar.hover$.stop(true, true).fadeIn('fast');
            },
            function () {
                bar.panel$.css('z-index', '');
                bar.hover$.stop(true, true).fadeOut('fast');
            });

        });
    }

    return {
        Setup: function (root, data, url) {
            barData = data;
            quickPlanningUrl = url || "#";
            root$ = root;
            calcContent();
            calcLayout();

            jQuery('body').append('<img id="CurrentInventoryGraph-top-body-image" src="Static/images/border-inventory-graph-body.gif" />');
            jQuery('#CurrentInventoryGraph-top-body-image').load(function () {
                createElements();
                setTimeout(function () {
                    animateElements(function () {
                        bindEvents();
                    }, 400);
                });
            });
        }
    };

})(jQuery);