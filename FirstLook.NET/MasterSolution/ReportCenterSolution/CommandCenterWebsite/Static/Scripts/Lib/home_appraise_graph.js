﻿var BrowserCheck = {
    IsIE: false,
    IsIE7: false,
    IsIE8: false,
    IsIE9: false,
    IsFirefox: false
};
var Charting = { $body: null };
var AppraiseGraph = {
    // Variables
    closed: null,
    notClosed: null,
    appraisals: null,
    percentage: null,
    $body: null,
    $bodyImage: null,
    $barOverlay: null,
    $toolTipContainer: null,
    $toolTip: null,
    $mainDiv: null,
    $barOverlay0: null,
    $barOverlay1: null,
    $barOverlay2: null,
    $barOverlays: null,

    // Methods
    onLoad: function () {

        var appraiseLoaded = false,
            that = this;

        Charting.$body.append('<img id="appraise-graph-body-image" src="Static/Images/small-graph-body.gif"/>');
        this.setUpToolTips();

        this.$bodyImage.load(function () {
            this.renderGraph();
            appraiseLoaded = true;
        });

        setTimeout(function () {
            if (!appraiseLoaded) {
                that.renderGraph();
            }
        }, 1500);

        this.bindEvents();
    },

    bindEvents: function () {
        var that = this;

        this.$barOverlay.bind('mouseenter', function () {
            that.showToolTip(jQuery(this).attr('idx'));
        }).bind('mouseleave', function () {
            that.hideToolTip();
        }).bind('mouseleave', function () {
            that.hideToolTip();
        }).bind('click', function () { // TODO: Clean after BlueWhale
            window.open(that.link, '_blank');
        });
    },

    renderGraph: function () {
        var bottom = 78;
        if (BrowserCheck.IsIE7) {
            bottom = 79;
        } else if (BrowserCheck.IsIE8) {
            bottom = 79;
        } else if (BrowserCheck.IsIE9) {
            bottom = 79;
        }

        function get_step(t) {
            if (t >= 25) {
                return Math.ceil(t / 25) * 5;
            }
            else if (t >= 12) {
                return Math.ceil(t / 15) * 3;
            }
            else {
                return Math.ceil(t / 10) * 2;
            }
        }

        var step = get_step(this.appraisals);
        this.$body.kendoChart({
            categoryAxis: {
                line: { width: 0 },
                majorGridLines: { visible: false },
                minorGridLines: { visible: false },
                categories: ['APPRAISALS', 'CLOSED', 'NOT CLOSED'],
                labels: { font: '8px Arial' }
            },
            valueAxis: {
                labels: { visible: true, font: '9px Arial' },
                line: { width: 0 },
                majorGridLines: { visible: false },
                majorUnit: this.appraisals >= 1 ? step : null,
                max: Math.max(this.appraisals + step, 10),
                min: 0,
                minorGridLines: { visible: false }
            },
            chartArea: {
                background: 'none',
                margin: { top: 5, bottom: bottom, left: 15, right: 0 }
            },
            series: [
                {
                    border: { width: 1 },
                    color: '#A2B1BF',
                    data: [this.appraisals, this.closed, this.notClosed],
                    gap: 0.8,
                    labels: { background: 'none', color: '#666666', format: '{0}', font: 'bold 11px Arial,Helvetica,sans-serif', position: 'outsideEnd', opacity: 0 }
                }
            ],
            legend: { visible: false },
            seriesDefaults: { type: 'column', stack: true }
        });
        if (BrowserCheck.IsIE7 || BrowserCheck.IsIE8) { this.$body.children('div:first').children('shape:first').remove(); }
    },

    setUpToolTips: function () {
        this.hideToolTip();

        if (BrowserCheck.IsIE7 || BrowserCheck.IsIE8) {
            this.$barOverlay0.css('left', '61px');
            this.$barOverlay1.css('left', '128px');
            this.$barOverlay2.css('left', '198px');
        } else {
            this.$barOverlay0.css('left', '60px');
            this.$barOverlay1.css('left', '127px');
            this.$barOverlay2.css('left', '194px');
        }
        if (BrowserCheck.IsIE) {
            this.$barOverlays.css('background-color', '#f44').css('opacity', '0.01');
        }
    },

    showToolTip: function (barIndex) {
        var left, tooltipVal,
            percentNotClosed = 1 - this.percentage;

        switch (parseInt(barIndex)) {
            case 0:
                left = -127;
                tooltipVal = this.appraisals;
                break;
            case 1:
                left = -60;
                tooltipVal = this.closed;
                break;
            case 2:
                left = 5;
                tooltipVal = this.notClosed;
                break;
            default:
                return;
        }

        tooltipVal = Math.round(tooltipVal * 10) / 10;
        this.$toolTip.html(tooltipVal);
        this.$toolTipContainer.css('left', left + 'px').css('visibility', 'visible').fadeIn('fast');
    },

    hideToolTip: function () {
        this.$toolTipContainer.css('visibility', 'hidden');
    }
};
