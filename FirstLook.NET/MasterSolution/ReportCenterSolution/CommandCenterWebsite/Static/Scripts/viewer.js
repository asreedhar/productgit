// scripts for Report Viewer pages
var ReportViewer = {

	els: [],
	targetDivs: []
}
// runs after page load
function init() {
    if(!$('report')) return false;
      
    //find scrolling DIV and reset overflow style
    ReportViewer.els = $('report').getElementsByTagName('DIV');
    var target = $A(ReportViewer.els).find(function(el) {
	    return el.style.overflow == 'auto';
    });
    if (target) {
        target.style.overflow = '';
    }

    // remove the hidden style from the report container div
    new Effect.Appear('report');
    
	window.setTimeout(function() { new Effect.Fade('loader'); }, 500);

    setWindowNavLinks();
}

function setWindowNavLinks() {
	var showBackLink  = window.history.length > 0;
	var showCloseLink = window.name.length > 0 && window.name != "PMR";
	if(showBackLink) {
	    if ($('backlink')) {
	        $('backlink').show();
	    }
	}
	
	if(showCloseLink) {
	if($('closelink'))
		{
		$('closelink').show();
		}
	}
	if ( $('homelink') && !showCloseLink ) {
		$('homelink').show();
	}
}


function isValidPage(pageTextBox, pageCountHidden){
    var J = jQuery.noConflict(); 
    var currentPage = J('#' + pageTextBox).attr('value');
    var pageCount = J('#' + pageCountHidden).attr('value');
    
    currentPage = parseInt(currentPage);
    pageCount = parseInt(pageCount);
    if(isNaN(currentPage) || isNaN(pageCount) || currentPage <= 0 || currentPage > pageCount){
        J('#' + pageTextBox).parent().children('.error').show().fadeOut(2000);
        return false;
    }
    
    return true;
}


//gives rollover capability to the page nav buttons
function setRolloverEvent() {
    var p = $('paging_top');
    var b = $('paging_bottom');

    if (p.down('div.tools') && b.down('div.tools')) {
        var rolloverimages = $A(p.down('div.tools').getElementsByTagName('input')).concat($A(b.down('div.tools').getElementsByTagName('input')));
        rolloverimages.each(function(el) {
            if (el.id.indexOf('FirstPage') > 0     |
                el.id.indexOf('PreviousPage') > 0  |
                el.id.indexOf('NextPage') > 0      |
                el.id.indexOf('LastPage') > 0  == true) {

                el.originalsrc = el.src;
                el.hoversrc = el.originalsrc.sub('.gif', '-active.gif'); 
                el.onmouseover = function() {
                    el.src = el.hoversrc;
                }
                el.onmouseout = function() {
                    el.src = el.originalsrc;
                }
             }
        });
    }
}




Enhance('paging_bottom',setRolloverEvent);
