using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Static_Contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void StaticCssLink_PreRender(object sender, EventArgs e)
    {
        ((HtmlLink)sender).Attributes["href"] = ResolveClientUrl("~/App_Themes/" + Page.StyleSheetTheme + "/" + ((HtmlLink)sender).Attributes["href"]);

    }
}
