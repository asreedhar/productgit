using System;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.GoogleAnalytics;

public partial class DealerReports : System.Web.UI.Page, IGAnalytics
{
    //
    // Support Methods
    //
    //private string vehicleLevelWaterReportId = "E6A6F567-2E14-433A-ABCA-0E97DA8A1595";
    private static readonly string Token = SoftwareSystemComponentStateFacade.DealerComponentToken;

    protected SoftwareSystemComponentState SoftwareSystemComponentState
    {
        get { return (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
        set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        IsAnalyicsEnabled = true;
    }

    //
    // Page Initialize
    //

    protected void Page_Init(object sender, EventArgs e)
    {
        if (IsAnalyicsEnabled)
        {
            Page.GetGoogleAnalyticsScript();
        }

        SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, Token);

        AccessController.Instance().VerifyPermissions(state, Token);

        SoftwareSystemComponentState = state;
    }

    //
    // Page Load
    //

    protected void Page_Load(object sender, EventArgs e)
    {
        Form.Attributes.Add("class", "pmr");
    }

    #region IGAnalytics Members

    public string PageTitle
    {
        get; set;
    }

    public bool IsAnalyicsEnabled
    {
        get; set;
    }

    #endregion
}
