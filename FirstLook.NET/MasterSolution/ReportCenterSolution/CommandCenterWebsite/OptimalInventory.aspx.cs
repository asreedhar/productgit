using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;

public partial class Home_OptimalInventory : DealerGroupPage
{
    protected List<GridViewRow> footerRows = new List<GridViewRow>();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        base.IsAnalyicsEnabled = true;
        Page.Title = "Optimal Inventory in Stock";
    }


    protected void DealerGroup_Init(object sender, EventArgs e)
    {
        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
    }

    protected void ChartTitle_Init(object sender, EventArgs e)
    {
        ChartTitle.Text = "Optimal Inventory in Stock";
    }

    protected void OptimalInventory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int[] columnsToHighlight = {};
            GridViewUtilities.HighlightCells(e.Row, columnsToHighlight);

            if ((int)((DataRowView)e.Row.DataItem)["DrilldownRowType"] != 0)
            {
                e.Row.Cells[0].Text = "-";
                footerRows.Add(e.Row);
                e.Row.Visible = false;
            }
        }
        if (e.Row.RowType.Equals(DataControlRowType.Footer))
        {
            foreach (GridViewRow footerRow in footerRows)
            {
                GridViewRow r = GridViewUtilities.CopyRow(footerRow, DataControlRowType.Footer, OptimalInventory);
                e.Row.Parent.Controls.Add(r);
            }
            e.Row.Visible = false;
        }

    }
}
