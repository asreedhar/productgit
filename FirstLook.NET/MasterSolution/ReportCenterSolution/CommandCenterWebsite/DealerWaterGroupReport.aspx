<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DealerWaterGroupReport.aspx.cs"
    Inherits="DealerWaterGroupReport " %>

<%@ Register TagPrefix="reports" TagName="ReportControl" Src="~/Controls/Reports/ReportControl.ascx" %>
<%@ Register TagPrefix="menu" TagName="SiteMenu" Src="~/Controls/Common/SiteMenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Dealer Group Water Report</title>
    <script type="text/javascript" src="Static/Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="Static/Scripts/Lib/prototype.js"></script>
    <script type="text/javascript" src="Static/Scripts/Lib/core.js"></script>
    <script type="text/javascript" src="Static/Scripts/Lib/scriptaculous/scriptaculous.js"></script>
    <script type="text/javascript" src="Static/Scripts/viewer.js"></script>
    <style type="text/css" id="no_script">
           .invisible_ { visibility: visible; }
           .invisible_nonJS { visibility: hidden; }
           div.hide_, .hide_block { display: block; }
           span.hide_, .hide_inline { display: inline; }
           .hide_nonJS { display: none; }
           #wrap_ .split_ { overflow: visible; }
           .dyna_link { cursor: auto; }
           .dyna_link:hover { text-decoration: none; }
           .dyna_link > a { width: 42px;text-align: center; }
          .mainlogo {float: left;margin: -63px 0 0 0;}
          .setToLeft { clear: both;float: left;margin-left: 20px;}
           #topnavmenubar 
        {
        background: url("App_Themes/Leopard/Images/gradient-top.jpg") repeat-x;
        height: 53px;
        right: 0px;
        width: 100%;
        
        padding-bottom: 3px;
        padding-top: 7px;
        float: left;
        text-align: right;
        color: #999;
        }
        .a_color{color: green;
text-decoration: none;
border-bottom: 1px solid green;
}
.grayColor {
    color: gray;
}
.a_font {font-size: 1.2em;
font-weight: bold;
font-family: Verdana, Helvetica, sans-serif;}
.a_color:hover, a:hover{ text-decoration: none;}
#paging_top { display: none;}
#paging_bottom {float: right;
padding: 11px 50px 5px 0px;}
#controls {clear: both;
padding: 19px 0px 0px 10px;
}
.SelectedDealerId {float: left;}
 {
 background-image:-webkit-linear-gradient(top, #E6E6E6, #DCDCDC, #BEBEBE);
background-image:-moz-linear-gradient(top, #E6E6E6, #DCDCDC, #BEBEBE); 
background-image:-ms-linear-gradient(top, #E6E6E6, #DCDCDC, #BEBEBE);
    /* IE8 */
-ms-filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#E6E6E6',endColorStr='#BEBEBE',GradientType=0);
    /* IE7- */
filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#E6E6E6',endColorStr='#BEBEBE',GradientType=0);   
}
.buttonexport 
{
    width: 80px;
    background: url("Static/Images/btn_export.jpg");
}
.MyStyle 
{
     width:1350px;
    margin: 0 auto 0 auto;
    background-image:-webkit-linear-gradient(top, #DFE9ED, #ffffff, #ffffff);
background-image:-moz-linear-gradient(top, #DFE9ED, #ffffff, #ffffff); 
background-image:-ms-linear-gradient(top, #DFE9ED, #ffffff, #ffffff);
    /* IE8 */
-ms-filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#DFE9ED',endColorStr='#ffffff',GradientType=0);
    /* IE7- */
filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#DFE9ED',endColorStr='#ffffff',GradientType=0);
}
#divLogo {
    background-image: url('Static/Images/logo-main-firstlook.jpg');
    width: 290px;
    height: 52px;
    float: left;
    margin-top: -63px;
}

       </style>
</head>
<body>
    <form id="form" runat="server" class="MyStyle">
    <asp:ScriptManager ID="MasterScriptManager" runat="server">
    </asp:ScriptManager>
    <div>
        <menu:SiteMenu ID="SiteMenu1" runat="server"></menu:SiteMenu>
        <div id="divLogo">
        </div>
        <div runat="server" id="PMRLabel">
            <div style="margin-top: -50px; margin-left: 300px; float: left; border-left: 2px solid grey;
                height: 25px">
            </div>
            <div id="PMR" runat="server" style="margin-top: -48px; margin-left: 320px; float: left;
                font-size: 1.3em; font-family: Verdana; color: rgb(101, 101, 101)">
                Performance Management Reports</div>
        </div>
        <%-- <asp:Image runat="server" CssClass="mainlogo" SkinID="NewLogoFirstlook" ID="Image1" /> --%>
        <asp:Panel ID="pnlNavaigation" runat="server">
            <div id="NavigationUrl" style="clear: both; float: left; padding-left: 10px;">
                <a runat="server" id="HomeUrl" class="a_color a_font">PMC</a>
                <label style="font-family: cursive; font-weight: bolder; font-size: 1.1em;" runat="server"
                    class="">
                    >
                </label>
                <a runat="server" id="FirstDrill" class="a_font">Water Report by Store</a>
                <label id="lblVehicle" style="font-family: cursive; font-weight: bolder; font-size: 1.1em;"
                    runat="server" class="hide_nonJS">
                    >
                </label>
                <a runat="server" id="SecondDrill" class="a_font hide_nonJS">Water Report by Vehicle</a>
            </div>
        </asp:Panel>
        <asp:Label runat="server" ID="PmrVehicleReport"></asp:Label>
        <reports:ReportControl runat="server" ID="ReportControl1" OnDrillThrough="ReportViewer_Drillthrough" />
    </div>
    </form>
</body>
<footer runat="server" id="PageFooter">
    <div id="Div1" style="clear:both; float: left;padding-left: 37px;">
                <em>SECURE AREA</em> | <cite>&copy;<asp:Literal ID="CopyrightYear" runat="server"></asp:Literal>
                <span>INCISENT</span> Technologies, Inc. All rights reserved.</cite>
            </div>  
</footer>
</html>
