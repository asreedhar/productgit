﻿var edgegraphlink = "";
function EdgeSystemUsageGraph$Initialize (sender) {
    var container = $('edgeusagegraph');
    if (container) {
        var links = container.getElementsByTagName('A');
        if (links.length > 0) {
            edgegraphlink = links[0].href;
            links[0].href = "#";
            if($('edgeusagegraphlegend')) {
                return;
            }
	        new Insertion.Bottom('edgeusagegraph','<ul id="edgeusagegraphlegend"></ul>');
	        [$('redsystemusage'), $('yellowsystemusage'), $('greensystemusage')].each(function(element) {
		        element.style.width = Math.floor(element.innerHTML * 194 / 100) + 'px';
		        var legenditem = '<li id="' + element.id + '-legend">' + element.innerHTML + '%</li>';
		        new Insertion.Bottom('edgeusagegraphlegend',legenditem);
		        element.innerHTML = '';
	        });
            SetEdgeSystemUsageGraphLinks();
        }
    }
    var appVer = navigator.appVersion.toLowerCase();
    var iePos = appVer.indexOf('msie');
    if (iePos != -1) {
        var maj = parseInt(appVer.substring(iePos+5, appVer.indexOf(';', iePos)));
        if (maj == 6) {
            document.getElementById('graphcover').innerHTML = '';
        }
    }
}

function SetEdgeSystemUsageGraphLinks() {
    if($('edgeusagegraph')) {
        Element.observe($('edgeusagegraph'), 'click', function() {
            openReport(edgegraphlink, "reportlink");
            return false;
        });
        Element.observe($('edgeusagegraph'), 'mouseover', function() {
            $('edgeusagegraph').style.cursor = "pointer";
        });
    }
}
Sys.Application.add_load(EdgeSystemUsageGraph$Initialize);
Sys.Application.notifyScriptLoaded();

