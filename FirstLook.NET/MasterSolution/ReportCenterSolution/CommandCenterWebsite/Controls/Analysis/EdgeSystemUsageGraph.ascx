<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EdgeSystemUsageGraph.ascx.cs" Inherits="EdgeSystemUsageGraph" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="~/Controls/Analysis/EdgeSystemUsageGraph.js" />
    </Scripts>
</asp:ScriptManagerProxy>

<cwc:AsyncObjectDataSourceControl
    ID="EdgeSystemUsage_DataSource"
    runat="server"
    TypeName="FirstLook.CommandCenter.DomainModel.Service.EdgeSystemUsageGaugeDataService">
    <Parameters>
        <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
        <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
        <owc:MemberParameter Name="member" Type="Object" />
    </Parameters>
</cwc:AsyncObjectDataSourceControl>

<h3 id="edgeusagetitle">Group EDGE System Usage</h3>

<div id="edgeusagegraph">
    <ul class="usagenumbers">
        <asp:FormView ID="EdgeSystemUsage" runat="server" DataSourceID="EdgeSystemUsage_DataSource">
            <ItemTemplate>
                <li id="redsystemusage"><%# DataBinder.Eval(Container.DataItem, "PercentRed", "{0:0}")%></li>
                <li id="yellowsystemusage"><%# DataBinder.Eval(Container.DataItem, "PercentYellow", "{0:0}")%></li>
                <li id="greensystemusage"><%# DataBinder.Eval(Container.DataItem, "PercentGreen", "{0:0}")%></li>
            </ItemTemplate>
        </asp:FormView>
    </ul>
    <div id="graphcover">
        <a href="~/DealerGroupReport.aspx?Id=A9441439-5DDE-432e-AF62-708EEC28D3BD" onclick="return false;" runat="server"><img src="~/Static/Images/EdgeSystemUsage_Chart.png" alt="EDGE system usage" runat="server" /></a>
    </div>
    
</div>

<!--[if IE 6]>
<style type="text/css">
div#graphcover {
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='Static/Images/EdgeSystemUsage_Chart.png',sizingMethod='scale');
}
</style>
<![endif]-->