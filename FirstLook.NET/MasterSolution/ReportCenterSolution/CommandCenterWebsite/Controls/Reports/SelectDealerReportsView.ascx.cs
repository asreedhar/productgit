using System;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;

public partial class Controls_Reports_SelectDealerReportsView : System.Web.UI.UserControl
{
    protected void DealershipReportSelect_DataBound(object sender, EventArgs e)
    {
        SoftwareSystemComponentState state = (SoftwareSystemComponentState) Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];

        BusinessUnit dealer = state.Dealer.GetValue();

        if (!SelectDealershipListItem(dealer.Id.ToString()))
        {
            dealer = CollectionHelper.First(state.DealerGroupMember().AccessibleDealers(state.DealerGroup.GetValue()));
            if (dealer != null)
            {
                state.Dealer.SetValue(dealer);
                state.Save();
                SelectDealershipListItem(dealer.Id.ToString());
            }
        }
    }

    private bool SelectDealershipListItem(string value)
    {
        for (int i = 0; i < DealershipReportSelect.Items.Count; i++)
        {
            if (DealershipReportSelect.Items[i].Value.Equals(value))
            {
                DealershipReportSelect.SelectedIndex = i;
                return true;
            }
        }
        return false;
    }

    protected void DealershipReportSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!DealershipReportSelect.SelectedValue.Equals("0"))
        {
            BusinessUnit dealer = BusinessUnitFinder.Instance().Find(Int32.Parse(DealershipReportSelect.SelectedValue));

            SoftwareSystemComponentState state = (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];
            state.Dealer.SetValue(dealer);
            state.Save();
        }
    }

}
