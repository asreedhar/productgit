using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.CommandCenter.DomainModel.Service;
using FirstLook.Common.Data;
using FirstLook.Common.Impersonation;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;
using FirstLook.Reports.App.ReportDefinitionLibrary.Xml;
using Microsoft.Reporting.WebForms;
using CheckBox = System.Web.UI.WebControls.CheckBox;
using Control = System.Web.UI.Control;
using Label = System.Web.UI.WebControls.Label;
using MsBookmarkNavigationEventArgs = Microsoft.Reporting.WebForms.BookmarkNavigationEventArgs;
using MsContentDisposition = Microsoft.Reporting.WebForms.ContentDisposition;
using MsDrillthroughEventArgs = Microsoft.Reporting.WebForms.DrillthroughEventArgs;
using MsLocalReport = Microsoft.Reporting.WebForms.LocalReport;
using MsProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode;
using MsReportDataSource = Microsoft.Reporting.WebForms.ReportDataSource;
using MsReportParameter = Microsoft.Reporting.WebForms.ReportParameter;
using MsReportViewer = Microsoft.Reporting.WebForms.ReportViewer;
using MsServerReport = Microsoft.Reporting.WebForms.ServerReport;
using MsWarning = Microsoft.Reporting.WebForms.Warning;
using TextBox = System.Web.UI.WebControls.TextBox;
using UserControl = System.Web.UI.UserControl;

public partial class Reports_NewReportControl : UserControl
{
    #region Report Cache Method

    private static readonly string ReportKey = "FirstLook.Reports.App.ReportDefinitionLibrary.Report";

    protected IReport TheReport()
    {
        IReport theReport = (IReport)Context.Items[ReportKey];

        if (theReport == null)
        {
            string reportPath = ConfigurationManager.AppSettings["FirstLook.Reports.App.ReportDefinitionLibrary.Xml.Serialization.ReportMetadata.Path"];
            ReportFactory factory = ReportFactory.NewReportFactory(reportPath);
            theReport = factory.FindReport(Request.Params["Id"]);
            Context.Items[ReportKey] = theReport;
        }

        return theReport;
    }

    public string ReportName
    {
        get
        {
            IReport report = TheReport();
            return report!=null? report.Name:null;
        }
    }
    public string ReportId
    {
        get
        {
            IReport report = TheReport();
            return report != null ? report.Id : null;
        }
    }

    protected string _DrillThroughId;
    public string DrillThroughId
    {
        get
        {
            if (_DrillThroughId != null)
            {
                return _DrillThroughId;
            }
            else if (_DrillThroughId == null && Request.QueryString.HasKeys())
            {
                if (Request.QueryString.GetValues("drillthrough") != null)
                {
                    _DrillThroughId = Request.QueryString.GetValues("drillthrough")[0].ToString();
                }
                else if (Request.QueryString.GetValues("DealerId") != null)
                {
                    _DrillThroughId = Request.QueryString.GetValues("DealerId")[0].ToString();
                }
               
                return _DrillThroughId;
            }
            else
            {
                return null;
            }
        }
    }

    public string GetSelectedDealerName
    {
        get
        {
           
                Control selectedDealerId = ControllerUtils.FindControlRecursive(Page, "SelectedDealerId");
                if (selectedDealerId != null && (selectedDealerId as DropDownList).SelectedValue != null)
                {
                    return (selectedDealerId as DropDownList).SelectedItem.Text;
                }
            
            
            return string.Empty;
        }
    }
    protected Boolean DrillThroughPmrReport
    {
        get
        {
            if (Request.QueryString.HasKeys() && Request.QueryString.GetValues("type") != null && Convert.ToString(Request.QueryString.GetValues("type")[0]) == "pmr")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public Boolean IsDealerGroupReport
    {
        get
        {
            if (DrillThroughPmrReport)
            {
                IsPmrReport.Value = "true";
                return  false;
            }
            else
            {
                return IsPmrReport.Value == "false" ? true : false;
            }
        }  
    } 
    //Event to track the drill through in parent
    public event DrillthroughEventHandler DrillThrough;
    public SoftwareSystemComponentState SoftwareSystemComponentState
    {
        get { return (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
        set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
    }

    private ICollection<BusinessUnit> _businessUnitSet;
    protected ICollection<BusinessUnit> BusinessUnitSet
    {
        get
        {
            if (_businessUnitSet == null)
            {
                _businessUnitSet = MemberBusinessUnitSetFacade.FindOrCreate(
            SoftwareSystemComponentState.DealerGroup.GetValue(),
            SoftwareSystemComponentState.DealerGroupMember());
                Context.Items[MemberBusinessUnitSetFacade.HttpContextKey] = _businessUnitSet;
            }
            return (ICollection<BusinessUnit>)Context.Items[MemberBusinessUnitSetFacade.HttpContextKey];
        }
         
    }
    #endregion

    /// <summary>
    /// Property stored in the HttpContext that says whether the InitComplete state was been executed.
    /// </summary>
    protected bool IsInitComplete
    {
        get { return (Context.Items.Contains("InitComplete")) ? (bool)Context.Items["InitComplete"] : false; }
        set { Context.Items["InitComplete"] = value; }
    }

    /// <summary>
    /// Property stored in the HttpContext that says whether we are to [re]bind the report datasources.
    /// </summary>
    protected bool ExecuteBindDataSource
    {
        get { return (Context.Items.Contains("BindDataSource")) ? (bool)Context.Items["BindDataSource"] : false; }
        set { Context.Items["BindDataSource"] = value; }
    }

    #region Init Page State
    /// <summary>
    /// Put the report paramter controls on the page.
    /// </summary>
    protected void ReportParameterOptions_Init(object sender, EventArgs e)
    {
        
        ReportParameterOptions.Controls.Add(ReportParametersControl(TheReport()));
    }

    protected void ReportHeaderNavigation_Init(object sender, EventArgs e)
    {
       // CreateCookieCrum(TheReport().Id);
    }
    /// <summary>
    /// Initialize the report viewer control.
    /// </summary>
    protected void ReportViewer_Init(object sender, EventArgs e)
    {
        ReportViewer.AsyncRendering = false;
        ReportViewer.ExportContentDisposition = MsContentDisposition.AlwaysAttachment;
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        IsInitComplete = true;
       
        CreateCookieCrum(TheReport().Id);    
        
        ExecuteBindDataSource = !Page.IsPostBack;
       
     }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ExecuteBindDataSource)
        {
            BindDataSource(ReportViewer, TheReport());
        }
    }

    // change report parameter

    protected void ReportParameterSubmit_Click(object sender, EventArgs e)
    {
        ExecuteBindDataSource = true;
    }

    protected void DropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Session[((DropDownList) sender).ID] = ((DropDownList) sender).SelectedValue;
        ExecuteBindDataSource = true;
    }

    #region Export Report
    /// <summary>
    /// Event handler for when the PDF export button is clicked. The formats supported in LocalProcessingMode
    /// are: Excel, IMAGE and PDF.
    /// </summary>
    /// 
    protected void ReportExportEmail_Load(object sender, EventArgs e)
    {
        EmailClient.Subject = TheReport().Name;
    }

    protected void ReportExportEmail_Click(object sender, EmailEventArgs e)
    {
        string mimeType, encoding, fileNameExtension; string[] streams; MsWarning[] warnings;

        //Asked by jen to attach file as Pdf formate
        //byte[] content = ReportViewer.LocalReport.Render(
        //    "PDF", "<DeviceInfo></DeviceInfo>",
        //    out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
        byte[] content = ReportViewer.LocalReport.Render("Excel", "<DeviceInfo><SimplePageHeaders>False</SimplePageHeaders></DeviceInfo>",
            out mimeType,out encoding, out fileNameExtension, out streams, out warnings);
        string attachmentName = Regex.Replace(TheReport().Name, "\\W|[0-9]", "") + "." + fileNameExtension;

        MailMessage message = new MailMessage();
        message.From = new MailAddress("no-reply@firstlook.biz");
        message.ReplyTo = e.ReplyTo;
        foreach (MailAddress to in e.To)
            message.To.Add(to);
        message.Subject = e.Subject;
        message.IsBodyHtml = true;
        message.Body = e.Body;
        message.Attachments.Add(new Attachment(new MemoryStream(content), attachmentName));

        new SmtpClient().Send(message);
    }

    /// <summary>
    /// Event handler for when the PDF export button is clicked.
    /// </summary>
    protected void ReportExportPDF_Click(object sender, EventArgs e)
    {
        ExportReport("PDF", "<DeviceInfo></DeviceInfo>");
    }

    /// <summary>
    /// Event handler for when the Excel export button is clicked.
    /// </summary>
    protected void ReportExportExcel_Click(object sender, EventArgs e)
    {
        ExportReport("Excel", "<DeviceInfo><SimplePageHeaders>False</SimplePageHeaders></DeviceInfo>");
    }

    /// <summary>
    /// Utility method to export the current report in a given format.
    /// </summary>
    /// <param name="format">the export format</param>
    /// <param name="deviceInfo">parameters for the formatter</param>
    protected void ExportReport(string format, string deviceInfo)
    {
        // out parameters
        string mimeType, encoding, fileNameExtension; string[] streams; MsWarning[] warnings;
        // render to byte array
        byte[] content = ReportViewer.LocalReport.Render(
            format, deviceInfo,
            out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
        // write response
        Response.Clear();
        Response.AddHeader("Content-Disposition", "attachment; filename=" + Regex.Replace(TheReport().Name, "\\W|[0-9]", "") + "." + fileNameExtension);
        Response.AddHeader("Content-Length", content.Length.ToString());
        Response.ContentType = mimeType;
        Response.BinaryWrite(content);
        Response.End();
    }
    #endregion

    // Bookmark Navigation
    protected void ReportViewer_BookmarkNavigation(object sender, MsBookmarkNavigationEventArgs e)
    {
        ReportViewer.JumpToBookmark(e.BookmarkId);
    }

    // drill through
    protected void ReportViewer_Drillthrough(object sender, MsDrillthroughEventArgs e)
    {
        DrillThrough(this, e);
        // find the drill-through report from the library
        string configurationFilename = ConfigurationManager.AppSettings["FirstLook.Reports.App.ReportDefinitionLibrary.Xml.Serialization.ReportMetadata.Path"];
        ReportFactory factory = ReportFactory.NewReportFactory(configurationFilename);

        IReport report = factory.FindReportByServerPath(e.ReportPath);

        // bind the data source

        if (e.Report is MsLocalReport)
        {
            BindLocalReport((MsLocalReport)e.Report, report);
        }
        else
        {
            BindServerReport((MsServerReport)e.Report, report);
        }

        // update the report parameter section to be hidden from view

        ReportParameterOptions.Visible = false;
        ReportParameterSubmit.Visible = false;

        // show the back to parent report button

        ReportParameterBack.Visible = true;
        ReportParameterBackText.Text = "Back to " + TheReport().Name;        
    }

    #region Bind Data Source
    protected void BindDataSource(MsReportViewer reportViewer, IReport report)
    {
        if (report.ReportProcessingLocation is ILocalReportProcessingLocation)
        {
            reportViewer.ProcessingMode = MsProcessingMode.Local;
            BindLocalReport(reportViewer.LocalReport, report);
        }
        else
        {
            reportViewer.ProcessingMode = MsProcessingMode.Remote;

            BindServerReport(reportViewer.ServerReport, report);
        }
    }

    protected void BindServerReport(MsServerReport serverReport, IReport report)
    {
        IRemoteReportProcessingLocation location = (IRemoteReportProcessingLocation)report.ReportProcessingLocation;

        try
        {
            using (ImpersonationCodeSection i = ImpersonationCredentials.FromConnectionString("AnalysisServices").LogOn())
            {
                serverReport.ReportPath = location.Path;

                serverReport.ReportServerUrl = location.ServerUrl;

                if (serverReport.IsDrillthroughReport)
                {
                    // do not need to handle the callback
                }
                else
                {
                    IList<MsReportParameter> parameters = new List<MsReportParameter>();

                    foreach (IReportParameter parameter in report.ReportParameters)
                    {
                        parameters.Add(new MsReportParameter(parameter.Name, GetParameterValue(parameter)));
                    }

                    serverReport.SetParameters(parameters);
                }

                serverReport.Refresh();
            }
        }
        catch
        {
            // can do nothing ?
        }
    }

    protected void BindLocalReport(MsLocalReport localReport, IReport report)
    {
        ILocalReportProcessingLocation location = (ILocalReportProcessingLocation)report.ReportProcessingLocation;

        localReport.LoadReportDefinition(ReportDefinitionCache.GetReportDefinition(location.File).GetReportStream());

        localReport.EnableHyperlinks = true;

        localReport.EnableExternalImages = true;

        localReport.SetBasePermissionsForSandboxAppDomain(new System.Security.PermissionSet(System.Security.Permissions.PermissionState.Unrestricted));
        //localReport.ExecuteReportInCurrentAppDomain(AppDomain.CurrentDomain.Evidence);
        //localReport.AddFullTrustModuleInSandboxAppDomain(new System.Security.Policy.StrongName(new System.Security.Permissions.StrongNamePublicKeyBlob(Nullable),"FirstLook.Reports.App.ReportStyleLibrary.StyleSheet","1.0.0"));
        //localReport.AddTrustedCodeModuleInCurrentAppDomain(System.Reflection.Assembly.GetAssembly(typeof(FirstLook.Reports.App.ReportStyleLibrary.StyleSheet)).ToString());

        if (localReport.IsDrillthroughReport)
        {
            localReport.SetParameters(localReport.OriginalParametersToDrillthrough);
        }
        else
        {
            IList<MsReportParameter> parameters = new List<MsReportParameter>();

            foreach (IReportParameter parameter in report.ReportParameters)
            {
                parameters.Add(new MsReportParameter(parameter.Name, GetParameterValue(parameter)));
            }

            localReport.SetParameters(parameters);
        }

        localReport.DataSources.Clear();

        foreach (IReportDataCommandTemplate reportDataCommandTemplate in report.ReportDataCommandTemplates)
        {
            DictionaryDataParameterValue parameterValues = new DictionaryDataParameterValue();

            foreach (IReportParameter parameter in reportDataCommandTemplate.Parameters)
            {
                if (localReport.IsDrillthroughReport)
                {
                    parameterValues[parameter.Name] =
                        GetParameterValue(parameter, localReport.OriginalParametersToDrillthrough);
                }
                else
                {
                    parameterValues[parameter.Name] = GetParameterValue(parameter);
                }
            }

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection((reportDataCommandTemplate as ReportDataCommandTemplate).DataSourceName))
            //using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(location.DataSourceName))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                DataTable table=null;
               Boolean isOlap = ChangeOlapParameter((reportDataCommandTemplate as ReportDataCommandTemplate).DataSourceName,
                    parameterValues);
                //if (CheckExistingService(report.Id, parameterValues,out table))
                //{
                    
                //}
                

               if (reportDataCommandTemplate.Name.Equals("DealerGroupID")) continue;

                if (isOlap)
                {
                    table = ReportAnalyticsClient.GetReportData<DataTable>(GetParameterList(parameterValues));
                }
                if (table == null)
                {
                        using (IDataCommand command = connection.CreateCommand(reportDataCommandTemplate, parameterValues.DataParameterValue))
                    {
                    
                            command.CommandTimeout = 200;
                            using (IDataReader reader = command.ExecuteReader())
                            {
                                table = (isOlap.Equals(true) ? DataTableMapper.ToOlapDataTable(reader, reportDataCommandTemplate.DataMap, new DataTableCallback()) : 
                                    DataTableMapper.ToDataTable(reader, reportDataCommandTemplate.DataMap, new DataTableCallback()));
                                if (isOlap)
                                {
                                    table = CheckExistingService(TheReport().Id, table);
                                    ReportAnalyticsClient.SetReportData(GetParameterList(parameterValues),table);
                                }
                            }
                        }
                    
                }
               
                localReport.DataSources.Add(new MsReportDataSource(reportDataCommandTemplate.Name, table));
            }
        }

        localReport.Refresh();
    }

    private List<string> GetParameterList(DictionaryDataParameterValue dicData)
    {
        List<string> lstObj = new List<string>();
        IEnumerator<KeyValuePair<string,object>> edata =   dicData.GetValues().GetEnumerator();

        while (edata.MoveNext())
        {
            lstObj.Add(Convert.ToString(edata.Current.Value));
        }
        return lstObj;
    }
    #endregion
    #region Methods to check available service

    protected DataTable CheckExistingService(string reportID,DataTable Table)
    {
        
        switch (reportID)
        {
            case "D06528EE-05F3-4A00-B8C5-403536F103D8":
                ICollection<BusinessUnit> accessibleDealers = GetAccessibleDealers();
                IEnumerable<BusinessUnit> selectedDealers = GetSelectedDealers();
            
            AccessClassificationParameters args = new AccessClassificationParameters(
                accessibleDealers,
                selectedDealers,
                Table);
            AddAccessClassification accessClass = new AddAccessClassification(args, AddAccessClassification.CheckAppraisalClosingRateByStoreColumn);
                Table = accessClass.ProcessDrillthrough();
            //return new AccessClassificationProcessor(args, AccessClassificationProcessor.GetDefaultNumericColumnFormat).ProcessDrillthrough();
                break;
            default:
                break;
        }
        return Table;
    }
    #endregion
    #region Report Parameter Control Builder Methods
    protected Control ReportParametersControl(IReport report)
    {
        PlaceHolder panel = new PlaceHolder();

        //bool autoPostBack = IsAutoPostBack(report.ReportParameters);
        bool autoPostBack = IsAutoPostBack(report.ReportParameters, IsDealerGroupReport);

        foreach (IReportParameter reportParameter in report.ReportParameters)
        {
            panel.Controls.Add(BuildPanel(reportParameter, autoPostBack));
        }

        ReportParameterSubmit.Visible = !autoPostBack;

        return panel;
    }

    protected static bool IsAutoPostBack(IList<IReportParameter> parameters)
    {
        int numberOfNonHiddenParameters = 0;

        foreach (IReportParameter parameter in parameters)
        {
            if (!parameter.ReportParameterInputType.Equals(ReportParameterInputType.Hidden))
            {
                numberOfNonHiddenParameters += 1;
            }
        }

        return (numberOfNonHiddenParameters <= 1);
    }

    protected static bool IsAutoPostBack(IList<IReportParameter> parameters, bool IsDealerGroupReport)
    {
        int numberOfNonHiddenParameters = 0;

        foreach (IReportParameter parameter in parameters)
        {
            if (!parameter.ReportParameterInputType.Equals(ReportParameterInputType.Hidden) && !(parameter.Name == "SelectedDealerId" && !IsDealerGroupReport)) // In code if report is not of type IsDealerGroupReport and parameter is SelectedDealerId. we are hiding a dropdown.
            {
                  numberOfNonHiddenParameters += 1;
            }
        }

        return (numberOfNonHiddenParameters <= 1);
    }

    protected string GetParameterValue(IReportParameter parameter)
    {
        if (parameter.Name.Equals("ReportTitle"))
        {
            return TheReport().Name;
        }

        string value = GetParameterValue(parameter.Name);
        if (value == null) value = GetParameterValueForReport(TheReport().Id, parameter.Name);
        if (value == null)
        {
            IReportParameterValue parameterValue = parameter.DefaultValue(GetParameterValue);
            value = (parameterValue == null ? null : parameterValue.Value);
        }

        return value;
    }

    private static string GetParameterValue(IDataParameterTemplate parameter, IEnumerable<MsReportParameter> reportParameters)
    {
        foreach (MsReportParameter reportParameter in reportParameters)
        {
            if (reportParameter.Name.Equals(parameter.Name))
            {
                foreach (string value in reportParameter.Values)
                {
                    return value;
                }
            }
        }

        return null;
    }

    protected Control BuildPanel(IReportParameter parameter, bool autoPostBack)
    {
        PlaceHolder panel = new PlaceHolder();

        Control theInput = BuildInput(parameter, autoPostBack);

        if (!parameter.ReportParameterInputType.Equals(ReportParameterInputType.Hidden) && parameter.Label != null)
        {
            panel.Controls.Add(BuildLabel(parameter, theInput.ID));
        }

        panel.Controls.Add(theInput);

        return panel;
    }

    protected static Control BuildLabel(IReportParameter parameter, string id)
    {
        if (parameter.ReportParameterInputType == ReportParameterInputType.Hidden)
        {
            throw new ReportParameterException("Hidden report parameters do not have labels");
        }
        
        Label label = new Label();
        label.CssClass = parameter.Name;
        label.Text = String.IsNullOrEmpty(parameter.Label) ? "" : parameter.Label + ": ";
        label.AssociatedControlID = id;

        return label;
    }

    protected Control BuildInput(IReportParameter parameter, bool autoPostBack)
    {
        Control control;

        switch (parameter.ReportParameterInputType)
        {
            //case ReportParameterInputType.Date:
            //    //UserControl dtCal = (UserControl)Page.LoadControl("~/Controls/Reports/NewDateTimePicker.ascx");
            //    Controls_Reports_DatePicker dtCal = (Controls_Reports_DatePicker)Context.Items["datepicker"];

            //    if (dtCal == null)
            //    {
            //        dtCal =
            //           (Controls_Reports_DatePicker)this.LoadControl("~/Controls/Reports/DatePicker.ascx");
            //        Context.Items.Add("datepicker", dtCal);
            //    }
            //    if (!(parameter.Name.ToLower().Equals("to") || parameter.Name.ToLower().Equals("enddate")))
            //    {
            //        dtCal.From.ID = parameter.Name;
            //        dtCal.From.Text = GetParameterValue(parameter.Name);
            //        control = dtCal;
            //    }
            //    else
            //    {
            //        dtCal.To.ID = parameter.Name;
            //        dtCal.To.Text = GetParameterValue(parameter.Name);
            //        control = dtCal;
            //    }
            //    break;
                
            case ReportParameterInputType.Hidden:
                HiddenField hiddenField = new HiddenField();
                hiddenField.ID = parameter.Name;
                hiddenField.Value = GetParameterValue(parameter);
                control = hiddenField;
                break;
            case ReportParameterInputType.Select:
                DropDownList dropDownList = new DropDownList();
                dropDownList.ID = parameter.Name;
                dropDownList.CssClass = parameter.Name + " seletMargin";
                
                IList<IReportParameterValue> validValues = parameter.ValidValues(this.GetParameterValue);    
               
                
                ListItem[] listItems;
                // this report need to show the Group name as well in the drop down

                if (parameter.Name == "SelectedDealerId")
                {
                    dropDownList.Visible = IsDealerGroupReport;
                    
                    listItems = new ListItem[BusinessUnitSet.Count];
                    string DrillThroughBussinessId = string.Empty;
                    if (Request.QueryString.HasKeys() && Request.QueryString.GetValues("drillthrough") != null)
                    {
                        DrillThroughBussinessId = Request.QueryString.GetValues("drillthrough")[0].ToString();
                    }
                    
                    Boolean selctedValue = false;
                    // Asked not to show GroupName for specific report on 13June14
                    int i = 0;
                    if (!ReportId.Equals("C4DE8552-68E4-451E-9F22-F6CEE9359D89"))
                    {
                        listItems = new ListItem[BusinessUnitSet.Count + 1];
                        ListItem firstListItem = new ListItem(SoftwareSystemComponentState.DealerGroup.GetValue().Name, GetSelectedDealersId());
                        firstListItem.Selected = false;
                        listItems[0] = firstListItem;
                        i = 1;

                        foreach (BusinessUnit value in BusinessUnitSet)
                        {
                            ListItem listItem = new ListItem(value.Name, value.Id.ToString());
                            listItem.Selected = DrillThroughBussinessId.Equals(value.Id.ToString()) ? true : false;
                            if (listItem.Selected) selctedValue = true;
                            listItems[i++] = listItem;
                        }

                    }
                    if (!selctedValue)
                    {
                        listItems = new ListItem[validValues.Count];
                        i = 0;
                        foreach (IReportParameterValue value in validValues)
                        {
                            ListItem listItem = new ListItem(value.Label, value.Value);
                            listItem.Selected = DrillThroughBussinessId.Equals(value.Value.ToString()) ? true : false;
                            listItems[i++] = listItem;
                        }
                    }

                    if (ReportId.Equals("6ecb009f-cc55-493b-8aab-009821bbf594") || ReportId.Equals("98635938-75c3-4d30-ac69-e3352465550f") || ReportId.Equals("C82458C3-A914-4F38-8741-8AB2CCF7D5AD")) //Added since this report was showing group name
                    {
                        listItems = new ListItem[validValues.Count];
                        i = 0;
                        foreach (IReportParameterValue value in validValues)
                        {
                            ListItem listItem = new ListItem(value.Label, value.Value);
                            listItem.Selected = DrillThroughBussinessId.Equals(value.Value) ? true : false;
                            listItems[i++] = listItem;
                        }
                    }

                    
                }
                else if (parameter.Name == "AppraiserName")
                {
                    string DrillThroughAppraiser = string.Empty;
                    if (Request.QueryString.HasKeys() && Request.QueryString.GetValues("AppraiserName") != null)
                    {
                        DrillThroughAppraiser = Request.QueryString.GetValues("AppraiserName")[0].ToString();
                    }
                    listItems = new ListItem[validValues.Count];
                    int i = 0;
                    foreach (IReportParameterValue value in validValues)
                    {
                        ListItem listItem = new ListItem(value.Label, value.Value);
                        listItem.Selected = DrillThroughAppraiser.Equals(value.Value) ? true : false;
                        listItems[i++] = listItem;
                    }
                }
                else if (parameter.Name == "TradeInType")
                {
                    string DrillThroughPeriodId = string.Empty;
                    if (Request.QueryString.HasKeys() && Request.QueryString.GetValues(parameter.Name) != null)
                    {
                        DrillThroughPeriodId = Request.QueryString.GetValues(parameter.Name)[0].ToString();
                    }
                    listItems = new ListItem[validValues.Count];
                    int i = 0;
                    foreach (IReportParameterValue value in validValues)
                    {
                        ListItem listItem = new ListItem(value.Label, value.Value);
                        listItem.Selected = string.Equals(DrillThroughPeriodId, value.Value, StringComparison.CurrentCultureIgnoreCase) ? true : false;
                        listItems[i++] = listItem;
                    }
                }
                else if (parameter.Name == "PeriodID" || parameter.Name == "Period" || parameter.Name == "TimePeriodID" || parameter.Name == "AgeBucketID")
                {
                    string DrillThroughPeriodId = string.Empty;
                    if (Request.QueryString.HasKeys() && Request.QueryString.GetValues(parameter.Name) != null)
                    {
                        DrillThroughPeriodId = Request.QueryString.GetValues(parameter.Name)[0].ToString();
                    }
                    listItems = new ListItem[validValues.Count];
                    int i = 0;
                    foreach (IReportParameterValue value in validValues)
                    {
                        ListItem listItem = new ListItem(value.Label, value.Value);
                        listItem.Selected = string.Equals(DrillThroughPeriodId, value.Value, StringComparison.CurrentCultureIgnoreCase) ? true : false;
                        listItems[i++] = listItem;
                    }
                }
                else
                {
                     listItems = new ListItem[validValues.Count];
                     int i = 0;
                        foreach (IReportParameterValue value in validValues)
                        {
                            ListItem listItem = new ListItem(value.Label, value.Value);
                            listItem.Selected = value.Selected;
                            listItems[i++] = listItem;
                        }
                }
                
               
                dropDownList.Items.AddRange(listItems);
                autoPostBack |= String.Equals(parameter.Name, "varToggleRow");
                dropDownList.AutoPostBack = autoPostBack;
                if (autoPostBack)
                {
                    dropDownList.SelectedIndexChanged += DropDown_SelectedIndexChanged;
                }
                control = dropDownList;
                break;
            case ReportParameterInputType.Text:
                TextBox textBox = new TextBox();
                textBox.ID = parameter.Name;
                textBox.CssClass = parameter.Name;
                textBox.Text = GetParameterValue(parameter);
                textBox.AutoPostBack = autoPostBack;
                control = textBox;
                break;
            case ReportParameterInputType.Checkbox:
                CheckBox checkBox = new CheckBox();
                checkBox.ID = parameter.Name;
                checkBox.CssClass = parameter.Name;
                checkBox.Checked = ( "True".Equals(GetParameterValue(parameter)) || "1".Equals(GetParameterValue(parameter)) );
                checkBox.AutoPostBack = autoPostBack;
                control = checkBox;
                break;
            default:
                throw new ArgumentException("Method not implemented for input type");
        }

        return control;
    }

    protected Boolean ChangeOlapParameter(string connectionName, DictionaryDataParameterValue paramValue)
    {
        Boolean isOlap = false;
        if (connectionName.Contains("Analysis"))
        {
            isOlap = true;
            if (paramValue.GetValues().ContainsKey("DealerGroupID"))
            {
                paramValue.GetValues()["DealerGroupID"] = "[Dealer].[Dealer Group Hierarchy].[Dealer Group].&[" + paramValue.GetValues()["DealerGroupID"] + "]";
               
            }
            //if (paramValue.GetValues().ContainsKey("Mode")) paramValue.GetValues()["Mode"] = "ClosingRate";
        }
        return isOlap;

    }

    protected string GetParameterValueForReport(string reportId, string parameterName)
    {
        if (reportId.Equals("D06528EE-05F3-4A00-B8C5-403536F103D8"))
        {
            switch (parameterName)
            {
                case "Mode":
                    return "ClosingRate";
                   
                case "TimePeriodID" :
                    return "4weeks";
            }
        }
        return null;
    }
    private string getModeQryString()
    {
        if (Request.QueryString.HasKeys() && Request.QueryString.GetValues("Mode") != null)
        {
            return "&Mode=" + Convert.ToString(Request.QueryString.GetValues("Mode")[0]);
        }
        return "&Mode=ClosingRate";
    }
    protected void CreateCookieCrum(string reportId)
    {
        
            switch (reportId)
            {
                case "D06528EE-05F3-4A00-B8C5-403536F103D8":
                    HyperLink hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    hlGroup.Text = "Appraisal Closing Rate";
                    hlGroup.CssClass = "a_font a_color beforelink";
                    hlGroup.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    Label lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    HyperLink hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    if (SoftwareSystemComponentState.DealerGroup != null &&
                        SoftwareSystemComponentState.DealerGroup.GetValue() != null)
                    {
                        hlFirstChild.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
                    }
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.CssClass = "afterlink a_font";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);
                    break;
                case "C4DE8552-68E4-451E-9F22-F6CEE9359D89": // Closing Rate By Appraiser
                   

                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    if (SoftwareSystemComponentState.DealerGroup != null &&
                       SoftwareSystemComponentState.DealerGroup.GetValue() != null)
                    {
                        hlGroup.Text =IsDealerGroupReport ? SoftwareSystemComponentState.DealerGroup.GetValue().Name : "Appraisal Closing";
                    }
                    hlGroup.CssClass = "newRow a_font a_color beforelink";
                    //hlGroup.NavigateUrl = IsDealerGroupReport ? "~/PerformanceManagementCenter.aspx" : "";
                    DropDownList PeriodId = (DropDownList)ControllerUtils.FindControlRecursive(Page, "Period");
                    string sSelectedPeriod = PeriodId != null ? "&TimePeriodID=" + ControllerUtils.FindTimePeriodID(Convert.ToInt32(PeriodId.SelectedValue)) : "";

                    hlGroup.NavigateUrl = IsDealerGroupReport ? "~/" + GetCurrentPageName() + "?Id=D06528EE-05F3-4A00-B8C5-403536F103D8" + getModeQryString() + sSelectedPeriod : "";
                    
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    //if (DrillThroughId != null)
                    //{
                    //    var businessUnit = from BusinessUnit m in BusinessUnitSet
                    //                      where m.Id.ToString().Equals(DrillThroughId)
                    //                      select m;
                    //    hlFirstChild.Text = businessUnit.FirstOrDefault() != null ? businessUnit.FirstOrDefault().Name : "";
                    //}
                    //Control selectedDealerId = ControllerUtils.FindControlRecursive(Page, "SelectedDealerId");
                    //if (selectedDealerId != null && (selectedDealerId as DropDownList).SelectedValue != null)
                    //{
                    //    hlFirstChild.Text = (selectedDealerId as DropDownList).SelectedItem.Text;
                    //}
                    hlFirstChild.Text = GetSelectedDealerName;

                    hlFirstChild.CssClass = "afterlink a_font";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />")); 
                     Label lbDescription = new Label();
                    lbDescription.Text = "Appraisal Closing Rates By Appraiser";
                    lbDescription.CssClass = "newRow a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                   
                    //Hide fields if PMR page
                    Control c = ControllerUtils.FindControlRecursive(Page,"SelectedDealerId");
                    c.Visible = IsDealerGroupReport;
                    break;
                case "DFE79F27-5AB0-4BDD-9C14-56D48603759E": //Appraiser Log
                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    if (SoftwareSystemComponentState.DealerGroup != null &&
                       SoftwareSystemComponentState.DealerGroup.GetValue() != null)
                    {
                        hlGroup.Text = IsDealerGroupReport ? SoftwareSystemComponentState.DealerGroup.GetValue().Name : "Appraisal Closing";
                    }
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    //hlGroup.NavigateUrl = IsDealerGroupReport ? "~/PerformanceManagementCenter.aspx" : "";
                    PeriodId = (DropDownList)ControllerUtils.FindControlRecursive(Page, "Period");
                    sSelectedPeriod = PeriodId != null ? "&TimePeriodID=" + ControllerUtils.FindTimePeriodID(Convert.ToInt32(PeriodId.SelectedValue)) : "";

                    hlGroup.NavigateUrl = IsDealerGroupReport ? "~/" + GetCurrentPageName() + "?Id=D06528EE-05F3-4A00-B8C5-403536F103D8" + getModeQryString() + sSelectedPeriod : "";
                    
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    DropDownList Period = ControllerUtils.FindControlRecursive(Page, "Period") as DropDownList;

                    hlFirstChild.NavigateUrl = IsDealerGroupReport ? "~/" + GetCurrentPageName() + "?Id=C4DE8552-68E4-451E-9F22-F6CEE9359D89&drillthrough=" + DrillThroughId :  "~/" + GetCurrentPageName() + "?Id=C4DE8552-68E4-451E-9F22-F6CEE9359D89&drillthrough=" + DrillThroughId + "&type=pmr";
                    hlFirstChild.NavigateUrl += Period != null ? "&Period=" + Period.SelectedValue : "";
                    if (DrillThroughId != null)
                    {
                        if (Request.QueryString.GetValues("name") != null)
                        {
                            hlFirstChild.Text = Request.QueryString.GetValues("name")[0].ToString();
                        }
                        
                        if(string.IsNullOrEmpty(hlFirstChild.Text))
                        {
                            var businessUnit = from BusinessUnit m in SoftwareSystemComponentState.DealerGroupMember().Dealers
                                              where m.Id.ToString().Equals(DrillThroughId)
                                              select m;
                            if (businessUnit.Count() < 1)
                            {
                                businessUnit = from BusinessUnit unit in BusinessUnitSet
                                    where unit.Id.ToString().Equals(DrillThroughId)
                                    select unit;
                            }
                            hlFirstChild.Text = businessUnit.FirstOrDefault() != null ? businessUnit.FirstOrDefault().Name : "";
                        }
                    }

                    hlFirstChild.CssClass = "a_font a_color afterlink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    ReportHeaderNavigation.Controls.Add(new Label(){Text = " > "});

                    HyperLink hlSecondChild = new HyperLink();
                    hlSecondChild.ID = "SecondChild";
                    hlSecondChild.NavigateUrl = "";

                    Control appraiserName = ControllerUtils.FindControlRecursive(Page, "AppraiserName");
                    if (appraiserName != null && (appraiserName as DropDownList).SelectedValue != null)
                    {
                        hlSecondChild.Text = (appraiserName as DropDownList).SelectedValue;
                    }
                    //if (Request.QueryString.GetValues("AppraiserName") != null)
                    //{
                    //    hlSecondChild.Text = Request.QueryString.GetValues("AppraiserName")[0].ToString();
                    //}

                    hlSecondChild.CssClass = "afterlink a_font";
                    ReportHeaderNavigation.Controls.Add(hlSecondChild);

                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />")); 
                    lbDescription = new Label();
                    lbDescription.Text = "Appraisal Log";
                    lbDescription.CssClass = "newRow a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                   
                    break;
                case "B3D7BCDC-8BC9-4929-BFC5-0F6C0B1AD97E":
                    lbDescription = new Label();
                    lbDescription.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    lbDescription.CssClass = "newRow a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));

                    break;
                case "E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6" :
                case "D57CF3D5-A075-4032-938C-8A6A2AA5B32B":
                    lbDescription = new Label();
                    lbDescription.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    lbDescription.CssClass = "newRow a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));

                    lbDescription = new Label();
                    lbDescription.Text = reportId.Equals("E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6") ? "Profitability By Buyer" : "Profitability By Appraiser";
                    lbDescription.CssClass = "newRow desc_ItalicFont";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    break;
                case "B7A99071-736C-4532-8E5E-7E9BEBCBB830": //Profitability By Buyer
                case "C930CB2B-D29F-494D-9EBF-34E172EA9C54": //Profitability By Appraiser
                     hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                   
                    hlGroup.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    DropDownList PeriodID = (DropDownList) ControllerUtils.FindControlRecursive(Page, "PeriodID");

                    //// Date controls loading takes place after this method call hance take from Request
                    //TextBox fromDate = (TextBox) ControllerUtils.FindControlRecursive(Page, "FromDate");
                    //TextBox endDate = (TextBox) ControllerUtils.FindControlRecursive(Page, "EndDate");
                    //if (IsPostBack)
                    //{
                    //    if (fromDate !=null) fromDate.Text = Request[fromDate.UniqueID];
                    //    if (endDate != null) endDate.Text = Request[endDate.UniqueID];
                    //}
                    if (reportId.Equals("B7A99071-736C-4532-8E5E-7E9BEBCBB830"))
                    {
                        hlGroup.NavigateUrl = "~/" + GetCurrentPageName() + "?Id=E44A8300-742C-4CB9-A8CB-3A87CAAA1DF6&type=pmr&TradeOrPurchase=1" + (PeriodID != null ? "&PeriodId=" + PeriodID.SelectedValue : "");// +(fromDate != null ? "&FromDate=" + fromDate.Text : "") + (endDate != null ? "&EndDate=" + endDate.Text : "");    
                    }
                    else
                    {
                        hlGroup.NavigateUrl = "~/" + GetCurrentPageName() + "?Id=D57CF3D5-A075-4032-938C-8A6A2AA5B32B&type=pmr&TradeOrPurchase=2" + (PeriodID != null ? "&PeriodId=" + PeriodID.SelectedValue : ""); 
                    }
                    
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    appraiserName = ControllerUtils.FindControlRecursive(Page, "AppraiserName");
                    if (appraiserName != null && (appraiserName as DropDownList).SelectedValue != null)
                    {
                        hlFirstChild.Text = (appraiserName as DropDownList).SelectedValue;
                    }
                    hlFirstChild.CssClass = "a_font afterlink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));

                    lbDescription = new Label();
                    lbDescription.Text = reportId.Equals("B7A99071-736C-4532-8E5E-7E9BEBCBB830") ? "Profitability By Buyer" : "Profitability By Appraiser";;
                    lbDescription.CssClass = "newRow desc_ItalicFont";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    break;
                case "F6B512EC-D492-47D3-BB7F-5A437D531816":
                case "F113B4BB-132F-4E60-9B12-CD46DD9CAABA":
                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                   
                    hlGroup.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    PeriodID = (DropDownList)ControllerUtils.FindControlRecursive(Page, "Period");
                    if (reportId.Equals("F113B4BB-132F-4E60-9B12-CD46DD9CAABA"))
                    {
                        hlGroup.NavigateUrl = "~/" + GetCurrentPageName() + "?SelectedDealerId=" + Request.QueryString.Get("SelectedDealerID") + "&Id=F6B512EC-D492-47D3-BB7F-5A437D531816&type=pmr" + (PeriodID != null ? "&PeriodId=" + PeriodID.SelectedValue : "");
                    }

                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    appraiserName = ControllerUtils.FindControlRecursive(Page, "AppraiserName");
                    if (appraiserName != null && (appraiserName as DropDownList).SelectedValue != null)
                    {
                        hlFirstChild.Text = (appraiserName as DropDownList).SelectedValue;
                    }
                    hlFirstChild.CssClass = "a_font afterlink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));

                    lbDescription = new Label();
                    lbDescription.Text = reportId.Equals("F6B512EC-D492-47D3-BB7F-5A437D531816") ? "Appraisal Bump Analysis" : "Bump Analysis by Appraiser";;
                    lbDescription.CssClass = "newRow desc_ItalicFont";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    break;
                case "BE4CD8E3-37C4-4E06-876E-0E6F9746D7A5":
                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    hlGroup.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    hlGroup.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = "Retail Sales Margin Report";
                    hlFirstChild.CssClass = "a_font afterlink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);
                    break;
                case "6FAC8FB3-4C8C-4208-9FCC-6894836EEAF2":

                    PeriodID = (DropDownList)ControllerUtils.FindControlRecursive(Page, "PeriodID");

                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    hlGroup.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    hlGroup.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = "Retail Sales Margin Report";
                    hlFirstChild.CssClass = "a_font a_color afterlink";
                    hlFirstChild.NavigateUrl = "~/" + GetCurrentPageName() + "?drillthrough=" + Request.QueryString.Get("drillthrough") + "&Id=BE4CD8E3-37C4-4E06-876E-0E6F9746D7A5&type=pmr" + (PeriodID != null ? "&PeriodId=" + PeriodID.SelectedValue : "");
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlSecondChild = new HyperLink();
                    hlSecondChild.ID = "SecondChild";
                    hlSecondChild.NavigateUrl = "";
                    hlSecondChild.Text = "Vehicle Pricing Details";
                    hlSecondChild.CssClass = "a_font afterlink";
                    hlSecondChild.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlSecondChild);

                    break;
                case "A4CBC38A-81C0-47D0-BC48-15A7D6F2B701": // Retail Pricing Margin
                      hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    hlGroup.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    hlGroup.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = "Retail Pricing Margin Report";
                    hlFirstChild.CssClass = "a_font afterlink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);
                    break;
            
                case "B4AED734-E7DE-46C1-8225-1A690DC54D35": // VehiclePricingDetails
                       hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    hlGroup.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    hlGroup.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = "Retail Pricing Margin Report";
                    hlFirstChild.CssClass = "a_font a_color afterlink";
                    hlFirstChild.NavigateUrl = "~/" + GetCurrentPageName() + "?drillthrough=" + Request.QueryString.Get("drillthrough") + "&Id=A4CBC38A-81C0-47D0-BC48-15A7D6F2B701&type=pmr";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > &nbsp ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlSecondChild = new HyperLink();
                    hlSecondChild.ID = "SecondChild";
                    hlSecondChild.NavigateUrl = "";
                    hlSecondChild.Text = "Vehicle Pricing Details";
                    hlSecondChild.CssClass = "a_font afterlink";
                    hlSecondChild.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlSecondChild);

                    break;
                #region Avg Immd wholesale profit and loss
                case "f352752c-9d73-4832-a8c4-2734a9623ad5":

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "GroupName";
                    if (SoftwareSystemComponentState.DealerGroup != null &&
                        SoftwareSystemComponentState.DealerGroup.GetValue() != null)
                    {
                        hlFirstChild.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
                    }
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.CssClass = "a_font a_color beforelink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlGroup = new HyperLink();
                    hlGroup.ID = "FirstChild";
                    hlGroup.Text = "Immediate Wholesale Profit/Loss";
                    hlGroup.CssClass = "afterlink a_font";
                    hlGroup.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlGroup);
                    
                    break;
                case "6ecb009f-cc55-493b-8aab-009821bbf594":

                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    if (SoftwareSystemComponentState.DealerGroup != null &&
                       SoftwareSystemComponentState.DealerGroup.GetValue() != null)
                    {
                        hlGroup.Text =SoftwareSystemComponentState.DealerGroup.GetValue().Name ;
                    }
                    hlGroup.CssClass = "newRow a_font a_color beforelink";
                    
                    PeriodId = (DropDownList)ControllerUtils.FindControlRecursive(Page, "Period");
                    sSelectedPeriod = PeriodId != null ? "&Period=" + PeriodId.SelectedValue:"";

                    hlGroup.NavigateUrl = IsDealerGroupReport ? "~/" + GetCurrentPageName() + "?Id=f352752c-9d73-4832-a8c4-2734a9623ad5" + getModeQryString() + sSelectedPeriod : "";

                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = GetSelectedDealerName;
                    hlFirstChild.CssClass = "a_font a_color beforelink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    lbDescription = new Label();
                    lbDescription.Text = "Immediate Wholesale Profit/Loss";
                    lbDescription.CssClass = "beforelink a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    
                    break;
                case "59c0e4ef-95bb-4031-8b61-28020d9e91f9": 
                    //hlGroup.NavigateUrl = IsDealerGroupReport ? "~/PerformanceManagementCenter.aspx" : "";
                    PeriodId = (DropDownList)ControllerUtils.FindControlRecursive(Page, "Period");
                    sSelectedPeriod = PeriodId != null ? "&Period=" + PeriodId.SelectedValue : "";
                    
                     hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = GetSelectedDealerName;
                    hlFirstChild.CssClass = "a_font a_color beforelink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    lbDescription = new Label();
                    lbDescription.Text = "Immediate Wholesale Profit/Loss";
                    lbDescription.CssClass = "beforelink a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    break;

                #endregion

                #region Trade Analyzer Usage
                case "819d1e2c-e985-407f-9ccd-30a9e2c0afa9":
                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "GroupName";
                    if (SoftwareSystemComponentState.DealerGroup != null &&
                        SoftwareSystemComponentState.DealerGroup.GetValue() != null)
                    {
                        hlFirstChild.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
                    }
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.CssClass = "a_font a_color beforelink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlGroup = new HyperLink();
                    hlGroup.ID = "FirstChild";
                    hlGroup.Text = "Trade Analyzer Usage";
                    hlGroup.CssClass = "afterlink a_font";
                    hlGroup.NavigateUrl = "";
                    ReportHeaderNavigation.Controls.Add(hlGroup);
                    break;
                case "98635938-75c3-4d30-ac69-e3352465550f":
                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    if (SoftwareSystemComponentState.DealerGroup != null &&
                       SoftwareSystemComponentState.DealerGroup.GetValue() != null)
                    {
                        hlGroup.Text = IsDealerGroupReport ? SoftwareSystemComponentState.DealerGroup.GetValue().Name : "Appraisal Closing";
                    }
                    hlGroup.CssClass = "newRow a_font a_color beforelink";
                    
                    PeriodId = (DropDownList)ControllerUtils.FindControlRecursive(Page, "Period");
                    sSelectedPeriod = PeriodId != null ? "&Period=" + PeriodId.SelectedValue : "";

                    DropDownList TradeInType = (DropDownList)ControllerUtils.FindControlRecursive(Page, "TradeInType");
                     

                    hlGroup.NavigateUrl = IsDealerGroupReport ? "~/" + GetCurrentPageName() + "?Id=819d1e2c-e985-407f-9ccd-30a9e2c0afa9" + getModeQryString() + sSelectedPeriod + (TradeInType != null ? "&TradeInType=" + TradeInType.SelectedValue : "") : "";

                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = GetSelectedDealerName;
                    hlFirstChild.CssClass = "a_font a_color beforelink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    lbDescription = new Label();
                    lbDescription.Text = "Trade Analyzer Usage";
                    lbDescription.CssClass = "beforelink a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    

                    break;
                case "c625a438-4429-4a8c-a3a2-366805acddf9":
                    //hlGroup.NavigateUrl = IsDealerGroupReport ? "~/PerformanceManagementCenter.aspx" : "";
                    PeriodId = (DropDownList)ControllerUtils.FindControlRecursive(Page, "Period");
                    sSelectedPeriod = PeriodId != null ? "&Period=" + PeriodId.SelectedValue : "";

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = GetSelectedDealerName;
                    hlFirstChild.CssClass = "a_font a_color beforelink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    lbDescription = new Label();
                    lbDescription.Text = "Trade Analyzer Usage";
                    lbDescription.CssClass = "beforelink a_font";
                    ReportHeaderNavigation.Controls.Add(lbDescription);
                    ReportHeaderNavigation.Controls.Add(new LiteralControl("<br />"));
                    break;

                #endregion

                #region Reatil Inventory Sales Analysis

                case "E8E56305-3117-48CF-AB00-CD117F15AE9E"://Reatil Inventory Sales Analysis(Command center)
                    hlGroup = new HyperLink();
                    hlGroup.ID = "GroupName";
                    hlGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
                    hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                    hlGroup.NavigateUrl = "~/PerformanceManagementCenter.aspx";
                    ReportHeaderNavigation.Controls.Add(hlGroup);

                    lbSeparater = new Label();
                    lbSeparater.Text = " > ";
                    ReportHeaderNavigation.Controls.Add(lbSeparater);

                    hlFirstChild = new HyperLink();
                    hlFirstChild.ID = "FirstChild";
                    hlFirstChild.NavigateUrl = "";
                    hlFirstChild.Text = "Retail Inventory Sales Analysis";
                    hlFirstChild.CssClass = "a_font afterlink";
                    ReportHeaderNavigation.Controls.Add(hlFirstChild);
                    break;

                case "C82458C3-A914-4F38-8741-8AB2CCF7D5AD"://Retail Inventory Sales Analysis(PMR)
                    if (Request.QueryString.ToString().Contains("pmr"))
                    {
                        hlGroup = new HyperLink();
                        hlGroup.ID = "GroupName";
                        hlGroup.Text = SoftwareSystemComponentState.Dealer.GetValue().Name;
                        hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                        hlGroup.NavigateUrl = "~/DealerReports.aspx";
                        ReportHeaderNavigation.Controls.Add(hlGroup);

                        lbSeparater = new Label();
                        lbSeparater.Text = " > ";
                        ReportHeaderNavigation.Controls.Add(lbSeparater);

                        hlFirstChild = new HyperLink();
                        hlFirstChild.ID = "FirstChild";
                        hlFirstChild.NavigateUrl = "";
                        hlFirstChild.Text = "Retail Inventory Sales Analysis";
                        hlFirstChild.CssClass = "a_font afterlink";
                        ReportHeaderNavigation.Controls.Add(hlFirstChild);
                    }
                    else
                    {
                        hlGroup = new HyperLink();
                        hlGroup.ID = "GroupName";
                        hlGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;                       
                        hlGroup.CssClass = "newRow a_font a_color beforelink afterlink";
                        hlGroup.NavigateUrl = "~/PerformanceManagementCenter.aspx";
                        ReportHeaderNavigation.Controls.Add(hlGroup);

                        lbSeparater = new Label();
                        lbSeparater.Text = " > ";
                        ReportHeaderNavigation.Controls.Add(lbSeparater);

                        hlFirstChild = new HyperLink();
                        DropDownList tradeInType = (DropDownList)ControllerUtils.FindControlRecursive(Page, "TradeInType");
                        DropDownList periodId = (DropDownList)ControllerUtils.FindControlRecursive(Page, "PeriodID");
                        hlFirstChild.ID = "FirstChild";
                        hlFirstChild.NavigateUrl = "";
                        hlFirstChild.Text = GetSelectedDealerName;
                        hlFirstChild.CssClass = "a_font a_color afterlink";
                        hlFirstChild.NavigateUrl = "~/" + GetCurrentPageName() + "?drillthrough=" + Request.QueryString.Get("drillthrough") + "&Id=E8E56305-3117-48CF-AB00-CD117F15AE9E" + (tradeInType != null ? "&TradeInType=" + tradeInType.SelectedValue : "") + (periodId != null ? "&PeriodID=" + periodId.SelectedValue : "");
                        ReportHeaderNavigation.Controls.Add(hlFirstChild);

                        lbSeparater = new Label();
                        lbSeparater.Text = " > ";
                        ReportHeaderNavigation.Controls.Add(lbSeparater);

                        hlSecondChild = new HyperLink();
                        hlSecondChild.ID = "SecondChild";
                        hlSecondChild.NavigateUrl = "";
                        hlSecondChild.Text = "Retail Inventory Sales Analysis";
                        hlSecondChild.CssClass = "a_font afterlink";
                        hlSecondChild.NavigateUrl = "";
                        ReportHeaderNavigation.Controls.Add(hlSecondChild);
                    }
                    break;


                #endregion

                default:
                     break;
            }
    }
    #endregion

    //
    //
    //
    protected string GetCurrentPageName()
    {
        string sPath = Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        string sRet = oInfo.Name;
        return sRet;
    }
    protected string GetParameterValue(string parameter)
    {
        string value = null;

        // try the web-controls first

        if (IsInitComplete)
        {
           
            Control c= ControllerUtils.FindControlRecursive(Page, parameter);    
           
            if (c != null)
            {
                if (c is TextBox)
                {
                    value = ((TextBox)c).Text;
                }
                else if (c is DropDownList)
                {
                    value = ((DropDownList)c).SelectedValue;
                }
                else if (c is CheckBox)
                {
                    // concert bool check box to 0 or 1 for sql
                    value = ("True".Equals( ((CheckBox)c).Checked.ToString() ) ) ? "1" : "0";
                }
                else if (c is HiddenField)
                {
                    value = ((HiddenField) (c)).Value;
                }
            }
        }

        // then the request parameters
        if (value == null)
        {
            foreach (string key in Request.QueryString.AllKeys)
            {
                if (key.Equals(parameter))
                {
                    value = Request.QueryString[key];
                    break;
                }
            }
        }

        //To check for the pingUrl 
        if (parameter.ToLower().Equals("pingurlprefix"))
        {
            Uri uri = new Uri(Request.Url.ToString());
            string url=string.Format("{0}://{1}", uri.Scheme, uri.Authority);
            url = url + "/pricing/Pages/Internet/VehiclePricingAnalyzer.aspx?";
            return url;
        }

        // special check for encoded dealerId
        bool isDealerIdParam = (parameter.Equals("DealerIDs") || parameter.Equals("DealershipID") ||
                                parameter.Equals("DealerID") || parameter.Equals("UserSelectedDealerID"));
        if (isDealerIdParam && value == null)
        {
            value = Request.QueryString["ContextCode"];
        }

        SoftwareSystemComponentState state = (SoftwareSystemComponentState) Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];

        // then the report center session
        if (value == null)
        {
            if (isDealerIdParam)
            {
                if (state.Dealer.GetValue() != null)
                {
                    value = state.Dealer.GetValue().Id.ToString();
                }
            }
            else if (parameter.Equals("DealerGroupID"))
            {
                if (state.DealerGroup.GetValue() != null)
                {
                    value = state.DealerGroup.GetValue().Id.ToString();
                }
            }
            else if (parameter.Equals("MemberId") || parameter.Equals("MemberID"))
            {
                value = state.DealerGroupMember().Id.ToString();
            }
            else if (parameter.Equals("SelectedDealerId") || parameter.Equals("SelectedDealerID"))
            {
                value = GetSelectedDealersId();
            }
            else if (parameter.Equals("AccessibleDealerId") || parameter.Equals("AccessibleDealerID"))
            {
                value = GetAccessibleDealersId();
            }
        }

        return value;
    }
    private string GetSelectedDealersId()
    {
        string value = string.Empty;
        //int memberId = SoftwareSystemComponentState.DealerGroupMember().Id;
        
        //IList<BusinessUnit> businessunitList = BusinessUnitFinder.Instance().FindAllDealershipsByDealerGroupAndMember(SoftwareSystemComponentState.DealerGroup.GetValue().Id, memberId);
        if (BusinessUnitSet != null && BusinessUnitSet.Count > 0)
        {
           
            value = string.Join(",", BusinessUnitSet.Select(x => x.Id));
            if (BusinessUnitSet.Count == 1)
            {
                value = "," + value;
            }
            
        }
        return value;
    }
    private IEnumerable<BusinessUnit> GetSelectedDealers()
    {
        //string value = string.Empty;
        //int memberId = SoftwareSystemComponentState.DealerGroupMember().Id;

        //IList<BusinessUnit> businessunitList = BusinessUnitFinder.Instance().FindAllDealershipsByDealerGroupAndMember(SoftwareSystemComponentState.DealerGroup.GetValue().Id, memberId);

        return (IEnumerable<BusinessUnit>)BusinessUnitSet;
    }
   
    private string GetAccessibleDealersId()
    {
        string value = string.Empty;
        
        ICollection<BusinessUnit> accessibleDealers = SoftwareSystemComponentState.DealerGroupMember().AccessibleDealers(SoftwareSystemComponentState.DealerGroup.GetValue());
        if (accessibleDealers != null) value = string.Join(",", accessibleDealers.Select(x => x.Id));
        return value;
    }
    private ICollection<BusinessUnit> GetAccessibleDealers()
    {
       ICollection<BusinessUnit> accessibleDealers = SoftwareSystemComponentState.DealerGroupMember().AccessibleDealers(SoftwareSystemComponentState.DealerGroup.GetValue());

       return accessibleDealers;
    }
    protected static ReportDataCommandType GetDataSourceType(string name)
    {
        return ("REPORTHOST".Equals(name)) ? ReportDataCommandType.Oltp : ReportDataCommandType.Olap;
    }

    protected void ReportParameterBackText_Load(object sender, EventArgs e)
    {
        Regex regEx = new Regex(@"[^0-9]"); // Everything that is not a number
        if (ReportParameterBackText.Visible && regEx.IsMatch(ReportParameterBackText.OnClientClick))
        {
            int drilldowndepth = int.Parse(regEx.Replace(ReportParameterBackText.OnClientClick, string.Empty));
            ReportParameterBackText.OnClientClick = string.Format("window.history.go(-{0}); return false;", ++drilldowndepth);
        }
        else
        {
            ReportParameterBackText.OnClientClick = "window.history.go(-1); return false;";
        }
    }
}
