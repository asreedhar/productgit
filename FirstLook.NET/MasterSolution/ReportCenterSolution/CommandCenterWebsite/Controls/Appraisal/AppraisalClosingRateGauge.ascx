<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppraisalClosingRateGauge.ascx.cs" Inherits="AppraisalClosingRateGauge" %>

<pwc:XhtmlGaugeContainer 
    ID="ClosingRateGauge" 
    runat="server" 
    Height="172px" 
    Width="172px" 
    BackColor="#EEEEEE" 
    DataSourceID="AppraisalClosingRateGauge_DataSource" 
    OnDataBound="AppraisalClosingRateGauge_DataBound" 
    AutoLayout="False" 
    CenteredCircle="true"
    ImageUrl="~/DundasImages/AppraisalGauge_#SEQ(5000,10)" 
    TransparentColor="238, 238, 238">
    <BackFrame FrameShape="Rectangular" />
    <Labels>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Font="Arial, 9.5px" Name="Label_Avg_1"
            Parent="CircularGauges.Default" ResizeMode="None" Text="Appraisal" TextAlignment="TopCenter"
            TextColor="0, 0, 99" BorderColor="">
            <Size Height="13" Width="100" />
            <Location X="0" Y="61" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" BorderColor="" Font="Arial, 9.5px"
            Name="Label_Avg_2" Parent="CircularGauges.Default" ResizeMode="None" Text="Closing Rate:"
            TextColor="0, 0, 99">
            <Size Height="10" Width="52" />
            <Location X="15.6" Y="69" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Font="Arial, 11px, style=Bold"
            Name="Label_Avg_Value" Parent="CircularGauges.Default" ResizeMode="None" Text="40%"
            TextAlignment="TopRight" TextColor="0, 0, 99">
            <Size Height="8" Width="24" />
            <Location X="60.8" Y="68" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Font="Arial, 7.5px" Name="Label_Max"
            Parent="CircularGauges.Default" ResizeMode="None" Text="High:" TextColor="73, 93, 126">
            <Size Height="10" Width="25" />
            <Location X="34.1772156" Y="79" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Font="Arial, 7.5px, style=Bold" Name="Label_Max_Value"
            Parent="CircularGauges.Default" ResizeMode="None" Text="100%" TextAlignment="TopRight"
            TextColor="73, 93, 126">
            <Size Height="15" Width="25" />
            <Location X="43.0379753" Y="79" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Font="Arial, 7.5px" Name="Label_Min"
            Parent="CircularGauges.Default" ResizeMode="None" Text="Low:" TextColor="73, 93, 126">
            <Size Height="10" Width="25" />
            <Location X="34.1772156" Y="85.5" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Font="Arial, 7.5px, style=Bold" Name="Label_Min_Value"
            Parent="CircularGauges.Default" ResizeMode="None" Text="0%" TextAlignment="TopRight"
            TextColor="73, 93, 126">
            <Size Height="15" Width="25" />
            <Location X="43.0379753" Y="85.5" />
        </DGWC:GaugeLabel>
    </Labels>
    <CircularGauges>
        <DGWC:CircularGauge 
        Name="Default"
        ToolTip="Appraisal Closing Rate">
            <Scales>
                <DGWC:CircularScale Name="Default" ShadowOffset="0" StartAngle="75"
                    SweepAngle="210" Width="0" Radius="42.3">
                    <LabelStyle DistanceFromScale="14" Font="Arial Black, 15.75pt, style=Italic" Placement="Cross"
                        RotateLabels="False" TextColor="DimGray" />
                    <MajorTickMark BorderWidth="0" DistanceFromScale="-12" EnableGradient="False" FillColor="DimGray"
                        Length="6" Placement="Inside" Shape="Rectangle" Width="4"  />
                    <MinorTickMark BorderWidth="0" DistanceFromScale="-8" FillColor="Gray" Width="1" Length="5" />
                </DGWC:CircularScale>
            </Scales>
            <Size Height="100" Width="100" />
            <PivotPoint X="50" Y="54" />
            <Pointers>
                <DGWC:CircularPointer 
                FillColor="Black" 
                FillGradientEndColor="Black"
                    FillGradientType="None" 
                    Name="Default" 
                    SnappingInterval="10" 
                    ValueSource="Average"
                    ShadowOffset="0"
                    CapFillColor="Silver" CapFillGradientEndColor="White" CapFillGradientType="TopBottom"
                    CapImage="~/Static/Images/gauge-center.png" CapWidth="26" />

            </Pointers>
            
            
            <BackFrame 
            FrameShape="Rectangular"
                FrameStyle="None" 
                Image="~/Controls/Appraisal/AppraisalClosingRate_Bkgd.png" />
            <Location X="0" Y="0" />
            
            
        </DGWC:CircularGauge>
    </CircularGauges>
    <Values>
        <DGWC:InputValue Name="Average" ValueFieldMember="Avg">
        </DGWC:InputValue>
        <DGWC:InputValue Name="Minimum" ValueFieldMember="Min">
        </DGWC:InputValue>
        <DGWC:InputValue Name="Maximum" ValueFieldMember="Max">
        </DGWC:InputValue>
        <DGWC:InputValue Name="Target" ValueFieldMember="Target">
        </DGWC:InputValue>
    </Values>
</pwc:XhtmlGaugeContainer>
