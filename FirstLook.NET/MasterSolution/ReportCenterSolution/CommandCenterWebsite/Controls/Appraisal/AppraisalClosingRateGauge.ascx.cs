using System;
using System.Web.UI.WebControls;

public partial class AppraisalClosingRateGauge : System.Web.UI.UserControl
{
    protected void AppraisalClosingRateGauge_DataBound(object sender, EventArgs e)
    {
        DropDownList timePeriodDropdown = ControllerUtils.FindControlRecursive(Page, "AppraisalClosingRateGauge_TimePeriod") as DropDownList;

        if ( timePeriodDropdown != null)
        {
            ClosingRateGauge.CircularGauges[0].Href = "AppraisalClosingRate.aspx?timePeriodId=" +
                                                      timePeriodDropdown.SelectedValue;
        }

        // hard coded! egads! but, i don't think this number exists anywhere in the data :(
        ClosingRateGauge.CircularGauges[0].ToolTip = "Target Appraisal Closing Rate: 40%";
        ClosingRateGauge.Labels["Label_Min_Value"].Text = String.Format("{0:0}", ClosingRateGauge.Values["Minimum"].Value) + '%';
        ClosingRateGauge.Labels["Label_Max_Value"].Text = String.Format("{0:0}", ClosingRateGauge.Values["Maximum"].Value) + '%';
        ClosingRateGauge.Labels["Label_Avg_Value"].Text = String.Format("{0:0}", ClosingRateGauge.Values["Average"].Value) + '%';

        ClosingRateGauge.CircularGauges[0].MapAreaAttributes = "class=\"drilldownlink\"";
    }
}
