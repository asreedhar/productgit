using System;

public partial class OptimalInventoryGauge : System.Web.UI.UserControl
{

    protected void OptimalInventoryGauge_DataBound(object sender, EventArgs e)
    {

        InventoryGauge.CircularGauges[0].ToolTip = "Average Optimal Inventory: " + String.Format("{0:0}", InventoryGauge.Values["AverageStore"].Value) + '%';


        InventoryGauge.Labels["Label_Min_Value"].Text = String.Format("{0:0}", InventoryGauge.Values["MinimumStore"].Value) + '%';
        InventoryGauge.Labels["Label_Max_Value"].Text = String.Format("{0:0}", InventoryGauge.Values["MaximumStore"].Value) + '%';
        InventoryGauge.Labels["Label_Avg_Value"].Text = String.Format("{0:0}", InventoryGauge.Values["AverageStore"].Value) + '%';

	    InventoryGauge.CircularGauges[0].MapAreaAttributes = "class=\"drilldownlink\"";
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
