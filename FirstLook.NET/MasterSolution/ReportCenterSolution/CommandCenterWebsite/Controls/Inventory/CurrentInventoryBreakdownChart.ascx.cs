using System;
using System.Drawing;
using Dundas.Charting.WebControl;

public partial class CurrentInventoryBreakdownChart : System.Web.UI.UserControl
{
    public delegate void CurrentInventoryBreakdownChartModeToggleEventHandler(object sender, EventArgs e);

    public event CurrentInventoryBreakdownChartModeToggleEventHandler ModeToggle;

    protected void Page_Load(object sender, EventArgs e)
    {
        Chart currInvChart = (Chart)CurrentInventoryChart;
        if (IsPostBack && CurrentInventoryGraphToggleRisk.Value.Equals("On"))
        {
            currInvChart.Series[0].Color = Color.Transparent;
        }
        if(Request.QueryString["bucketIndex"] != null)
        {
            ToggleGraphRiskColors.Visible = false;
        }
    }

    protected void CurrentInventoryChart_DataBound(object sender, EventArgs e)
    {
        Chart currentInventoryChart = (Chart)sender;
        int activeBucket = -1;
        if(Request.QueryString["bucketIndex"] != null)
        {
            activeBucket = int.Parse(Request.QueryString["bucketIndex"]);
        }

        string inventoryDataFormat = "", bucketToolTipLabel = "";
        //if (CurrentInventoryGraph_PercentageInventory_Label.Visible)
        if (IsPercentMode.Value == "true")
        {
            inventoryDataFormat = "{0:0}%";
            bucketToolTipLabel = "% Inv.";
        }
        else
        {
            inventoryDataFormat = "{0:C0}";
            bucketToolTipLabel = "Inv. $";
        }

        // iterate over each bar in the series
        for (int c = 0; c < currentInventoryChart.Series[0].Points.Count; c++)
        {

            string barToolTip = bucketToolTipLabel + "\nHigh: " + String.Format(inventoryDataFormat, currentInventoryChart.Series[4].Points[c].YValues[5]) + "\nLow: " + String.Format(inventoryDataFormat, currentInventoryChart.Series[4].Points[c].YValues[4]);

            // set the label 
            currentInventoryChart.Series[4].Points[c].Label = String.Format("{0:C0}", currentInventoryChart.Series[4].Points[c].YValues[1]) + "\n" + String.Format(inventoryDataFormat, currentInventoryChart.Series[4].Points[c].YValues[6]);
            
            
            /*
             * turns it red if negative - but since it turns the whole flaming label red, this is of no use to us. 
             * 
             * 
             if(currentInventoryChart.Series[4].Points[c].YValues[1].CompareTo(0) < 0)
                currentInventoryChart.Series[4].Points[c].FontColor = System.Drawing.Color.FromArgb(255, 192, 12, 13);
            */


            
            // set the label tool tip 
            currentInventoryChart.Series[4].Points[c].LabelToolTip = "Book Vs. Cost\nHigh: " + String.Format("{0:C0}", currentInventoryChart.Series[4].Points[c].YValues[3]) + "\nLow: " + String.Format("{0:C0}", currentInventoryChart.Series[4].Points[c].YValues[2]);

            // set the tool tip for the actual bar 
            currentInventoryChart.Series[0].Points[c].ToolTip = barToolTip;
            currentInventoryChart.Series[1].Points[c].ToolTip = barToolTip;
            currentInventoryChart.Series[2].Points[c].ToolTip = barToolTip;
            currentInventoryChart.Series[3].Points[c].ToolTip = barToolTip;
            currentInventoryChart.Series[4].Points[c].ToolTip = barToolTip;

            // adjust height of total bar so risk bars don't peek out due to rounding errors.
            currentInventoryChart.Series[0].Points[c].YValues[0] = currentInventoryChart.Series[1].Points[c].YValues[0] + currentInventoryChart.Series[2].Points[c].YValues[0] + currentInventoryChart.Series[3].Points[c].YValues[0];
        }

        if( activeBucket > -1)
        {
            currentInventoryChart.Series[0].Points[activeBucket] = new DataPoint(0, 0);
        }
    }

    protected void ToggleGraphRiskColors_Click(object sender, EventArgs e)
    {
        Chart currInvChart = (Chart)CurrentInventoryChart;
        if (CurrentInventoryGraphToggleRisk.Value.Equals("Off"))
        {
            currInvChart.Series[0].Color = Color.Transparent;
            CurrentInventoryGraphToggleRisk.Value = "On";
        }
        else
        {
            currInvChart.Series[0].Color = Color.FromArgb(255, 150, 165, 184);
            CurrentInventoryGraphToggleRisk.Value = "Off";
        }


        /* below code may seem redundant, but it's actually necessary to overcome a dundas bug 
         * where the labels get all screwed up. -tb 11/15/7 */
        if (IsPercentMode.Value.Equals("false"))
        {
            CurrentInventoryGraph_SetMode(false);
        }
        else
        {
            CurrentInventoryGraph_SetMode(true);
        }
    }

    private void CurrentInventoryGraph_SetMode(bool percentMode)
    {
        if (percentMode)
        {
            CurrentInventoryGraph_PercentageInventory_Label.Visible = true;
            CurrentInventoryGraph_PercentageInventory_Link.Visible = false;
            CurrentInventoryGraph_DollarsInventory_Label.Visible = false;
            CurrentInventoryGraph_DollarsInventory_Link.Visible = true;
        }
        else
        {
            CurrentInventoryGraph_PercentageInventory_Label.Visible = false;
            CurrentInventoryGraph_PercentageInventory_Link.Visible = true;
            CurrentInventoryGraph_DollarsInventory_Label.Visible = true;
            CurrentInventoryGraph_DollarsInventory_Link.Visible = false;
        }
    }

    protected void CurrentInventoryGraphToggleMode_Click(object sender, EventArgs e)
    {
        if( IsPercentMode.Value.Equals("true"))
        {
            IsPercentMode.Value = "false";
            CurrentInventoryGraph_SetMode(false);
        } 
        else
        {
            IsPercentMode.Value = "true";
            CurrentInventoryGraph_SetMode(true);
        }

        if( ModeToggle != null)
        {
            ModeToggle(this, EventArgs.Empty);    
        }
        
    }

    protected void CurrentInventoryChart_PreRender(object sender, EventArgs e)
    {
//        These lines change the file managment approach from the in memory solution to the write to file solution
//        this is neccessary for e-mailed reports so that they are accessable in the future.
//        this has been commented out for the 1.0 release due to network issues - it will go back in eventually
//        if ("true".Equals(Context.Items["EmailRequest"]))
//        {
//            Chart currInvChart = (Chart)CurrentInventoryChart;
//            currInvChart.ImageStorageMode = ImageStorageMode.UseHttpHandler;
//        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.QueryString["isPercentMode"] == "false")
        {
            IsPercentMode.Value = "false";
            CurrentInventoryGraph_SetMode(false);
        }
    }
}
