<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OptimalInventoryGauge.ascx.cs" Inherits="OptimalInventoryGauge" %>

<pwc:XhtmlGaugeContainer 
        ID="InventoryGauge" 
        runat="server" 
        Height="172px" 
        Width="172px" 
        BackColor="#EEEEEE" 
        DataSourceID="OptimalInventoryGauge_DataSource" 
        OnDataBound="OptimalInventoryGauge_DataBound" 
        AutoLayout="False" 
        TransparentColor="238, 238, 238" 
        ImageUrl="~/DundasImages/OptimalInventoryGauge_#SEQ(5000,10)" 
        TextAntiAliasingQuality="Normal">
    <BackFrame FrameShape="Rectangular" FrameStyle="None" />
    <Labels>
        <DGWC:GaugeLabel 
        Name="Label_Avg_1" Parent="CircularGauges.Default"
            Text="Optimal" Font="Arial, 9.5px" ResizeMode="None" TextColor="0, 0, 99" ZOrder="1" BackColor="" BackGradientEndColor="">
            <Size Height="15" Width="50" />
            <Location X="7.5" Y="46.6666679" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel 
        Name="Label_Avg_2" Parent="CircularGauges.Default"
            Text="Inventory:" Font="Arial, 9.5px" ResizeMode="None" TextColor="0, 0, 99" BackColor="" BackGradientEndColor="">
            <Size Height="12" Width="50" />
            <Location X="7.5" Y="54.5098038" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel Name="Label_Avg_Value" Parent="CircularGauges.Default"
            Text="57%" Font="Arial, 11px, style=Bold" ResizeMode="None" TextColor="0, 0, 99" BackColor="" BackGradientEndColor="">
            <Size Height="12" Width="50" />
            <Location X="30.5882359" Y="63.52941" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Name="Label_Max" Parent="CircularGauges.Default"
            Text="High:" Font="Arial, 7.5px" ResizeMode="None" TextColor="73, 93, 126">
            <Size Height="10" Width="25" />
            <Location X="25.321888" Y="75.5" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Name="Label_Max_Value" Parent="CircularGauges.Default"
            Text="100%" Font="Arial, 7.5px, style=Bold" ResizeMode="None" TextAlignment="TopRight" TextColor="73, 93, 126">
            <Size Height="15" Width="25" />
            <Location X="33.90558" Y="75.5" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Name="Label_Min" Parent="CircularGauges.Default"
            Text="Low:" Font="Arial, 7.5px" ResizeMode="None" TextColor="73, 93, 126">
            <Size Height="10" Width="25" />
            <Location X="25.321888" Y="82" />
        </DGWC:GaugeLabel>
        <DGWC:GaugeLabel BackColor="" BackGradientEndColor="" Name="Label_Min_Value" Parent="CircularGauges.Default"
            Text="0%" Font="Arial, 7.5px, style=Bold" ResizeMode="None" TextAlignment="TopRight" TextColor="73, 93, 126">
            <Size Height="15" Width="25" />
            <Location X="33.90558" Y="82" />
        </DGWC:GaugeLabel>
    </Labels>
    <CircularGauges>
        <DGWC:CircularGauge 
        Name="Default" 
        ToolTip="Optimal Inventory"
        Href="OptimalInventory.aspx">
            <Ranges>
                <DGWC:CircularRange 
                BorderColor="" 
                FillColor="" 
                FillGradientEndColor="" Name="Range1" />
            </Ranges>
            <Scales>
                <DGWC:CircularScale FillColor="Transparent"
                    Name="Default" Reversed="True" StartAngle="110" SweepAngle="228" BorderColor="" BorderStyle="NotSet" FillGradientEndColor="" Interval="25" Radius="45" ShadowOffset="0">
                    <MajorTickMark FillColor="Gray" Shape="Rectangle" BorderColor="" BorderWidth="0" Length="6" Width="3" />
                    <LabelStyle Visible="False" />
                    <MinorTickMark BorderColor="" FillColor="Silver" Interval="12.5" Length="5" Width="1" />
                    <MinimumPin BorderColor="" BorderWidth="0" Enable="True" EnableGradient="False" FillColor=""
                        GradientDensity="0" Length="0" Location="-2" Width="0">
                        <LabelStyle Font="Verdana, 15px, style=Bold, Italic" Text="E" TextColor="Gray" />
                    </MinimumPin>
                    <MaximumPin BorderColor="Transparent" BorderWidth="0" Enable="True" FillColor=""
                        Length="0" Location="-2">
                        <LabelStyle DistanceFromScale="-1" Font="Verdana, 15px, style=Bold, Italic" Text="F"
                            TextColor="White" />
                    </MaximumPin>
                </DGWC:CircularScale>
            </Scales>
            <Size Height="100" Width="100" />
            <PivotPoint X="50" Y="54" />
            <Pointers>
                <DGWC:CircularPointer CapFillColor="White" CapFillGradientEndColor="Silver" CapFillGradientType="TopBottom"
                    FillColor="Black" FillGradientEndColor="" Name="Default" ValueSource="AverageStore" FillGradientType="None" Placement="Inside" ShadowOffset="0" CapVisible="False" />
                <DGWC:CircularPointer CapImage="~/Static/Images/gauge-center.png" CapWidth="26" FillColor="RoyalBlue" FillGradientEndColor=""
                    Name="Pointer1" NeedleStyle="NeedleStyle4" ValueSource="GroupAverage" FillGradientType="None" Width="4" CapFillColor="Silver" CapFillGradientEndColor="White" CapFillGradientType="TopBottom" DistanceFromScale="6" Placement="Inside" ShadowOffset="0" />
            </Pointers>
            <BackFrame 
            BackColor="" 
            BackGradientEndColor="" 
            BackGradientType="None" 
            FrameGradientEndColor="" 
            FrameGradientType="None" FrameShape="Rectangular"
                FrameStyle="None" 
                BorderColor="" 
                Image="~/Controls/Inventory/OptimalInventoryGauge_Bkgd.png" FrameColor="" FrameWidth="0" />
            <Location X="0" Y="0" />
        </DGWC:CircularGauge>
    </CircularGauges>
    <Values>
        <DGWC:InputValue Name="AverageStore" ValueFieldMember="StoreAvg">
        </DGWC:InputValue>
        <DGWC:InputValue Name="GroupAverage" ValueFieldMember="GroupAvg">
        </DGWC:InputValue>
        <DGWC:InputValue Name="MinimumStore" ValueFieldMember="Min">
        </DGWC:InputValue>
        <DGWC:InputValue Name="MaximumStore" ValueFieldMember="Max">
        </DGWC:InputValue>
    </Values>
</pwc:XhtmlGaugeContainer>
