<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CurrentInventoryBreakdownChart.ascx.cs" Inherits="CurrentInventoryBreakdownChart" %>

<%@ Register Assembly="FirstLook.DomainModel.Oltp" Namespace="FirstLook.DomainModel.Oltp.WebControls" TagPrefix="owc" %>

    <h3>Current Inventory</h3>
    <h5>by Inventory Age in Days</h5>

    <div id="inventory-options">
    <asp:Label ID="CurrentInventoryGraph_PercentageInventory_Label" Text="% Inventory" runat="server" />                
    <asp:LinkButton ID="CurrentInventoryGraph_PercentageInventory_Link" Text="% Inventory" runat="server" visible="false"  OnClick="CurrentInventoryGraphToggleMode_Click" /> |                 
    <asp:Label ID="CurrentInventoryGraph_DollarsInventory_Label" Text="Inventory $" runat="server" visible="false" />            
    <asp:LinkButton ID="CurrentInventoryGraph_DollarsInventory_Link" Text="Inventory $" runat="server" OnClick="CurrentInventoryGraphToggleMode_Click" />                
    <asp:HiddenField ID="IsPercentMode" Value="true" runat="server" />
    <input type="hidden" id="IsPercentModePointer" value="<%= IsPercentMode.ClientID %>" />


     <script type="text/javascript" language="javascript">
      
        
        var savedElement;
        Event.observe(window, 'load', function() {
            
            if($('CurrentInventoryGraphLoadingIndicator')) {
            	
                savedElement = '<div id="CurrentInventoryGraphLoadingIndicator" class="loading">' + $('CurrentInventoryGraphLoadingIndicator').remove().innerHTML + '</div>';
                
                CurrentInventoryBreakdownChart_getOnClickFunction();
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(CurrentInventoryBreakdownChart_getOnClickFunction);
            }
        });

        function CurrentInventoryBreakdownChart_getOnClickFunction() {
        	var percentlink, dollarlink, risklink, activeInventoryModeLink;
        	
            percentlink = $('<%=CurrentInventoryGraph_PercentageInventory_Link.ClientID %>');
            dollarlink = $('<%=CurrentInventoryGraph_DollarsInventory_Link.ClientID %>');
            risklink = $('<%=ToggleGraphRiskColors.ClientID %>');

            if(dollarlink != null) {
                activeInventoryModeLink = dollarlink;
            } else if(percentlink != null) {
                activeInventoryModeLink = percentlink;
            }
            
            risklink.onclick = function() {
                new Insertion.Before($('<%=CurrentInventoryChart.ClientID %>'), savedElement);
                $('<%=CurrentInventoryChart.ClientID %>').style.display = "none";
            };
            
            activeInventoryModeLink.onclick = function() {
                new Insertion.Before($('<%=CurrentInventoryChart.ClientID %>'), savedElement);
                $('<%=CurrentInventoryChart.ClientID %>').style.display = "none";
            };
        }

    </script>
    
    <cwc:AsyncObjectDataSourceControl
        ID="CurrentInventoryBreakdownChart_DataSource"
        runat="server"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.CurrentInventoryBreakdownGraphDataService">
        <Parameters>
            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
            <owc:MemberParameter Name="member" Type="Object" />
            <asp:ControlParameter Name="IsPercentMode" Type="Boolean" ControlID="IsPercentMode" />
        </Parameters>
    </cwc:AsyncObjectDataSourceControl>
    
    </div>
    <asp:LinkButton ID="ToggleGraphRiskColors" CssClass="left" runat="server" OnClick="ToggleGraphRiskColors_Click">Show/Hide Risk</asp:LinkButton>
    
<asp:HiddenField ID="CurrentInventoryGraphToggleRisk" Value="Off" runat="server" />

<DCWC:Chart ID="CurrentInventoryChart" runat="server" BackColor="AliceBlue" BorderLineColor="" BorderLineStyle="Solid" 
    BorderLineWidth="0" DataSourceID="CurrentInventoryBreakdownChart_DataSource" BackImageAlign="Center" BackImageMode="Unscaled" Height="130px" Width="492px" EnableTheming="False" BackImage="~/Controls/Inventory/CurrentInventoryChart_Bkgd.png" AJAXZoomEnabled="False" 
    OnDataBound="CurrentInventoryChart_DataBound"
    OnPreRender="CurrentInventoryChart_PreRender"
    ImageStorageMode="UseImageUrl"
    ImageUrl="~/DundasImages/CurrentInventoryChart_#SEQ(5000,10)" >
    
    <Legends>
        <DCWC:Legend Enabled="False" LegendStyle="Row" Name="Default">
        </DCWC:Legend>
    </Legends>
    <BorderSkin PageColor="" FrameBackColor="" FrameBackImageAlign="Center" FrameBackImageMode="Unscaled" FrameBorderColor=""  />
    <Series>
        <DCWC:Series Name="Total" ValueMembersY="InventoryValue, BookVsCost, BookVsCostMin, BookVsCostMax, InventoryValueMin, InventoryValueMax, InventoryValueLabel" YValuesPerPoint="7" BorderColor="64, 64, 64" Color="150, 165, 184" Font="Arial, 10px, style=Bold" 
            ValueMemberX="BucketName" Href="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" LabelHref="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" BackImageMode="Unscaled" ChartArea="Front">
        </DCWC:Series>
        <DCWC:Series ChartType="StackedColumn" Color="192, 12, 13" Name="Red" ValueMembersY="PercentageOfRedLightInventory" Href="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" LabelHref="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" 
            ValueMemberX="BucketName" BorderColor="DimGray" LabelBorderWidth="0" LabelFormat="P0" ShadowColor="" Font="Arial, 10px, style=Bold" >
        </DCWC:Series>
        <DCWC:Series ChartType="StackedColumn" Color="240, 235, 51" Name="Yellow" Href="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" LabelHref="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX"
            ValueMembersY="PercentageOfYellowLightInventory" ValueMemberX="BucketName" BorderColor="DimGray" LabelBorderWidth="0" LabelFormat="P0" BackGradientEndColor="Gainsboro" ShadowColor="LightGray" Font="Arial, 10px, style=Bold">
        </DCWC:Series>
        <DCWC:Series ChartType="StackedColumn" Name="Green" Color="16, 202, 30" Href="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" LabelHref="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX"
                    ValueMembersY="PercentageOfGreenLightInventory, BookVsCost, BookVsCostMin, BookVsCostMax, InventoryValueMin, InventoryValueMax, InventoryValueLabel" YValuesPerPoint="7"  ValueMemberX="BucketName" BorderColor="DimGray" BackGradientEndColor="Gainsboro" ShadowColor="LightGray" Font="Arial, 10px, style=Bold">
        </DCWC:Series>
        <DCWC:Series Name="Label" ValueMembersY="InventoryValue, BookVsCost, BookVsCostMin, BookVsCostMax, InventoryValueMin, InventoryValueMax, InventoryValueLabel" YValuesPerPoint="7" BorderColor="Transparent" Color="Transparent" Font="Arial, 10px, style=Bold" 
            ValueMemberX="BucketName" Href="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" LabelHref="CurrentInventoryBreakdown.aspx?bucketIndex=#INDEX" BackImageMode="Unscaled" >
        </DCWC:Series>
    </Series>
    <ChartAreas>
        <DCWC:ChartArea BackColor="Transparent" BackGradientEndColor="238, 249, 253" BackGradientType="TopBottom"
            BorderColor="197, 219, 235" BorderStyle="Solid" BorderWidth="0" Name="Default" >
            <AxisX LabelsAutoFit="False" Margin="False" MarksNextToAxis="False" TitleAlignment="Near">
                <LabelStyle Font="Arial, 6.75pt" FontColor="MediumBlue" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorGrid LineColor="197, 219, 235" LineWidth="0" Enabled="False" LineStyle="NotSet" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorTickMark LineColor="Transparent" Size="3" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
            </AxisX>
            <AxisY LineWidth="0" Crossing="Min" IntervalAutoMode="VariableCount" IntervalType="Number" Margin="False" Maximum="100" Minimum="0" LabelsAutoFit="False">
                <LabelStyle Font="Arial, 1.5pt" FontColor="Transparent" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Number" Enabled="False" />
                <MajorGrid LineColor="197, 219, 235" LineWidth="0" Enabled="False" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Number" />
                <MajorTickMark Enabled="False" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Number" />
            </AxisY>
            <InnerPlotPosition Height="80" Width="100" Y="5" />
            <Position Height="92" Width="96" X="2" Y="6" />
            <AxisX2>
                <LabelStyle Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorTickMark Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorGrid Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
            </AxisX2>
            <AxisY2>
                <LabelStyle Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorTickMark Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorGrid Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
            </AxisY2>
        </DCWC:ChartArea>
        <DCWC:ChartArea AlignOrientation="All" AlignWithChartArea="Default" BackColor="Transparent"
            BorderColor="Transparent" Name="Front">
            <AxisX LabelsAutoFit="False" Margin="False" MarksNextToAxis="False" TitleAlignment="Near">
                <LabelStyle Font="Arial, 6.75pt" FontColor="Transparent" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorGrid LineColor="197, 219, 235" LineWidth="0" Enabled="False" LineStyle="NotSet" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorTickMark LineColor="Transparent" Size="3" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
            </AxisX>
            <AxisY LineWidth="0" Crossing="Min" IntervalAutoMode="VariableCount" IntervalType="Number" Margin="False" Maximum="100" Minimum="0" LabelsAutoFit="False">
                <LabelStyle Font="Arial, 1.5pt" FontColor="Transparent" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Number" Enabled="False" />
                <MajorGrid LineColor="197, 219, 235" LineWidth="0" Enabled="False" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Number" />
                <MajorTickMark Enabled="False" Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Number" />
            </AxisY>
            <InnerPlotPosition Height="80" Width="100" Y="5" />
            <Position Height="92" Width="96" X="2" Y="6" />
            <AxisX2>
                <LabelStyle Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorTickMark Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorGrid Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
            </AxisX2>
            <AxisY2>
                <LabelStyle Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorTickMark Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
                <MajorGrid Interval="Auto" IntervalOffset="Auto" IntervalOffsetType="Auto" IntervalType="Auto" />
            </AxisY2>
        </DCWC:ChartArea>
    </ChartAreas>
    <Titles>
        <DCWC:Title BackImageAlign="Left" Color="SteelBlue" Font="Arial, 6.75pt" Name="BenchmarkTarget"
            Text="Benchmark Target" Visible="False">
            <Position Height="8.666425" Width="25" X="2" Y="2" />
        </DCWC:Title>
    </Titles>

</DCWC:Chart>
