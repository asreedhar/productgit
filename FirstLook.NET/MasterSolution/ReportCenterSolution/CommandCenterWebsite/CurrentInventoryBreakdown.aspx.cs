using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

public partial class Home_CurrentInventoryBreakdown : DealerGroupPage
{

    protected List<GridViewRow> footerRows = new List<GridViewRow>();



    protected void DealerGroup_Init(object sender, EventArgs e)
    {
        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
    }

    protected void ChartTitle_Init(object sender, EventArgs e)
    {
        ChartTitle.Text = "Current Inventory";
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        base.IsAnalyicsEnabled = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // select a title

        int position = Int32.Parse(Request.QueryString["bucketIndex"]) + 1;

        switch (position)
        {
            case 1:
                GraphTitle1.Visible = (position == 1);
                Page.Title = "Current Inventory: Watch List";
                break;
            case 2:
                GraphTitle2.Visible = (position == 2);
                Page.Title = ChartTitle.Text + ": " + GraphTitle2.Text;
                break;
            case 3:
                GraphTitle3.Visible = (position == 3);
                Page.Title = ChartTitle.Text + ": " + GraphTitle3.Text;
                break;
            case 4:
                GraphTitle4.Visible = (position == 4);
                Page.Title = ChartTitle.Text + ": " + GraphTitle4.Text;
                break;
            case 5:
                GraphTitle5.Visible = (position == 5);
                Page.Title = ChartTitle.Text + ": " + GraphTitle5.Text;
                break;
            case 6:
                GraphTitle6.Visible = (position == 6);
                Page.Title = ChartTitle.Text + ": " + GraphTitle6.Text;
                break;

        }

        //GraphTitle1.Visible = (position == 1);
        //GraphTitle2.Visible = (position == 2);
        //GraphTitle3.Visible = (position == 3);
        //GraphTitle4.Visible = (position == 4);
        //GraphTitle5.Visible = (position == 5);
        //GraphTitle6.Visible = (position == 6);
    }

    protected void CurrentInventoryReport_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        const int templateFieldIndex = 9;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            String valuestring = string.Empty;
            
            if (e.Row.Cells[templateFieldIndex].Controls.Count > 0)
            {
                HyperLink link = e.Row.Cells[templateFieldIndex].Controls[0] as HyperLink;
                if (link != null)
                    valuestring = link.Text;
            }
            else
            {
                valuestring = e.Row.Cells[templateFieldIndex].Text;
            }

            if (valuestring.Contains("("))
            {
                e.Row.Cells[templateFieldIndex].CssClass += " negativenumber";
            }
            else
            {
                e.Row.Cells[templateFieldIndex].CssClass += " positivenumber";
            }

            int[] columnsToHighlight = {8, templateFieldIndex};

            GridViewUtilities.HighlightCells(e.Row, columnsToHighlight);

            if ((int)((DataRowView)e.Row.DataItem)["DrilldownRowType"] != 0)
            {
                e.Row.Cells[1].Text = "-";
                e.Row.Cells[3].Text = "-";
                e.Row.Cells[8].Text = "-";
                footerRows.Add(e.Row);    
                e.Row.Visible = false;
            }
            
            DataRowView rowView = (DataRowView)e.Row.DataItem;

            double widthvalue = 0;

            double pctOfInv = ToDouble(rowView["Inventory_Count_Percentage"])/100*1.4;

            double pctOfInvDollars = ToDouble(rowView["Unit_Cost_Percentage"])/100*1.4;

            if (e.Row.FindControl("LowRiskBar") != null)
            {
                widthvalue = ToDouble(rowView["Green_Light_Inventory_Count_Percentage"]) * pctOfInv;
                ((Panel)e.Row.FindControl("LowRiskBar")).Width = System.Convert.ToInt32(widthvalue);
            }
            if (e.Row.FindControl("MediumRiskBar") != null)
            {
                widthvalue = ToDouble(rowView["Yellow_Light_Inventory_Count_Percentage"]) * pctOfInv;
                ((Panel)e.Row.FindControl("MediumRiskBar")).Width = System.Convert.ToInt32(widthvalue);
            }
            if (e.Row.FindControl("HighRiskBar") != null)
            {
                widthvalue = ToDouble(rowView["Red_Light_Inventory_Count_Percentage"]) * pctOfInv;
                ((Panel)e.Row.FindControl("HighRiskBar")).Width = System.Convert.ToInt32(widthvalue);
            }
            if (e.Row.FindControl("DollarsLowRiskBar") != null)
            {
                widthvalue = ToDouble(rowView["Green_Light_Unit_Cost_Percentage"]) * pctOfInvDollars;
                ((Panel)e.Row.FindControl("DollarsLowRiskBar")).Width = System.Convert.ToInt32(widthvalue);
            }
            if (e.Row.FindControl("DollarsMediumRiskBar") != null)
            {
                widthvalue = ToDouble(rowView["Yellow_Light_Unit_Cost_Percentage"]) * pctOfInvDollars;
                ((Panel)e.Row.FindControl("DollarsMediumRiskBar")).Width = System.Convert.ToInt32(widthvalue);
            }
            if (e.Row.FindControl("DollarsHighRiskBar") != null)
            {
                widthvalue = ToDouble(rowView["Red_Light_Unit_Cost_Percentage"]) * pctOfInvDollars;
                ((Panel)e.Row.FindControl("DollarsHighRiskBar")).Width = System.Convert.ToInt32(widthvalue);
            }
        }
        
        if (e.Row.RowType.Equals(DataControlRowType.Footer))
        {
            foreach (GridViewRow footerRow in footerRows)
            {
                GridViewRow r = GridViewUtilities.CopyRow(footerRow, DataControlRowType.Footer, CurrentInventoryReport);
                e.Row.Parent.Controls.Add(r);
            }
            e.Row.Visible = false;
        }
    }

    protected void CurrentInventoryReport_Load(Object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GridView currentInventoryGraph = ((GridView)sender);

            bool isInPercentMode = ToggleModeDisplay(currentInventoryGraph);
            
            if (isInPercentMode)
            {
                currentInventoryGraph.Sort("Inventory_Count_Percentage_Rank", SortDirection.Ascending);
            }
            else
            {
                currentInventoryGraph.Sort("Unit_Cost_Percentage_Rank", SortDirection.Ascending);
            }
        }
    }


    protected void PercentageOfInventoryLink_Click(object sender, EventArgs e)
    {
        LinkButton button = (LinkButton)sender;
        SortDirection direction;
        if( PercentageOfInventorySortDirection.Value.Equals("Asc"))
        {
            direction = SortDirection.Ascending;
            PercentageOfInventorySortDirection.Value = "Desc";
        }
        else
        {
            direction = SortDirection.Descending;
            PercentageOfInventorySortDirection.Value = "Asc";
        }

        if(button.ID.Contains("Dollars")) {
            CurrentInventoryReport.Sort("Unit_Cost_Percentage_Rank", direction);
        } else {
            CurrentInventoryReport.Sort("Inventory_Count_Percentage_Rank", direction);
        }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        HiddenField isPercentModeFromParent = ControllerUtils.FindControlRecursive(Page, "IsPercentMode") as HiddenField;

        if (isPercentModeFromParent != null)
        {
            CurrentInventoryReport_DataSource.Parameters["isPercentMode"].DefaultValue = isPercentModeFromParent.Value;
        }
    }

    protected void CurrentInventoryBreakdownChart_ModeToggle(object sender, EventArgs e)
    {
        ToggleModeDisplay(CurrentInventoryReport);
    }

    private bool ToggleModeDisplay(GridView currentInventoryGridView)
    {
        Control isPercentModeFromParent = ControllerUtils.FindControlRecursive(Page, "IsPercentMode");

        bool isInPercentMode = (isPercentModeFromParent != null && isPercentModeFromParent is HiddenField &&
                    "true".Equals(((HiddenField)isPercentModeFromParent).Value));

        ControllerUtils.FindControlRecursive(Page, "CurrentInventoryBreakdownChartLegendPercent").Visible = isInPercentMode;
        ControllerUtils.FindControlRecursive(Page, "CurrentInventoryBreakdownChartLegendDollar").Visible = !isInPercentMode;

        currentInventoryGridView.Columns[1].Visible = isInPercentMode;
        currentInventoryGridView.Columns[2].Visible = isInPercentMode;
        currentInventoryGridView.Columns[3].Visible = !isInPercentMode;
        currentInventoryGridView.Columns[4].Visible = !isInPercentMode;
        currentInventoryGridView.Columns[5].Visible = !isInPercentMode;
        currentInventoryGridView.Columns[7].Visible = isInPercentMode;

        // if sorting by a no longer visible column - sort by the default now visible column
        if (currentInventoryGridView.Columns[1].SortExpression.Equals(CurrentInventoryReport.SortExpression) || 
            currentInventoryGridView.Columns[2].SortExpression.Equals(CurrentInventoryReport.SortExpression) )
        {
            currentInventoryGridView.Sort("Unit_Cost_Percentage_Rank", SortDirection.Ascending);
        }
        else if (currentInventoryGridView.Columns[3].SortExpression.Equals(CurrentInventoryReport.SortExpression) ||
                 currentInventoryGridView.Columns[4].SortExpression.Equals(CurrentInventoryReport.SortExpression) ||
                 currentInventoryGridView.Columns[5].SortExpression.Equals(CurrentInventoryReport.SortExpression))
        {
            currentInventoryGridView.Sort("Inventory_Count_Percentage_Rank", SortDirection.Ascending);
        }

        return isInPercentMode;
    }
}
