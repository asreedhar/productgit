<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DealerGroupReports.aspx.cs" Inherits="DealerGroupReports" MasterPageFile="~/Master_Pages/Base_Page.master" %>

<%@ Register TagPrefix="reports" TagName="DefinitionList" Src="~/Controls/Reports/DefinitionList.ascx" %>
<%@ Register TagPrefix="reports" TagName="UnorderedList" Src="~/Controls/Reports/UnorderedList.ascx" %>
<%@ Register TagPrefix="reports" TagName="DealershipReports" Src="~/Controls/Reports/DealershipReports.ascx" %>
<%@ Register TagPrefix="reports" TagName="SelectDealerReportsView" Src="~/Controls/Reports/SelectDealerReportsView.ascx" %>

<asp:content contentplaceholderid="HeadElements_Placeholder" runat="server">
    <script src="Static/Scripts/home.js" type="text/javascript"></script>
</asp:content>

<asp:content id="Content_A" contentplaceholderid="Body_A_PlaceHolder" runat="server">
    <div id="body_a">
    <h2>Dealer Group Reports</h2>
    <div class="a_ group_reports">
        
        <reports:DefinitionList id="PerformanceAndUsage" runat="server"></reports:DefinitionList>
    </div>
    <div class="b_ group_reports">
        <reports:DefinitionList id="InventoryManagement" runat="server"></reports:DefinitionList>
    </div>
    <div class="c_ group_reports">
        <reports:DefinitionList id="WholesaleBuyingAndSelling" runat="server"></reports:DefinitionList>
    </div>
    </div>

    <div id="body_b" class="split_">
    <div id="dealer_reports">
        <h2>
            <reports:SelectDealerReportsView id="SelectDealerReports" runat="server"></reports:SelectDealerReportsView>
        </h2>
        <reports:DealerShipReports id="DealerShipReports1" runat="server"></reports:DealerShipReports>
    </div>
    </div>
</asp:content>
