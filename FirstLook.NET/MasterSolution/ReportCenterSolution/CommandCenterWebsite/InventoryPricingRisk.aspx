<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InventoryPricingRisk.aspx.cs" Inherits="InventoryPricingRisk" MasterPageFile="~/Master_Pages/Page_Popup.master" %>

<%@ Register TagPrefix="firstlook" TagName="Email" Src="~/Controls/Common/Email.ascx" %>

<asp:Content ID="PopupHeader" ContentPlaceHolderID="Page_Header" runat="server">
    
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <firstlook:Email ID="Email2" OnSend="EmailClient_Send" Subject="Pricing Risk Alert" runat="server"/>
        <h2><asp:Literal ID="DealerGroup" runat="server" OnInit="DealerGroup_Init"></asp:Literal></h2>
        <h1><asp:Literal ID="ChartTitle" runat="server" OnInit="ChartTitle_Init"></asp:Literal></h1>
    </div>
</asp:Content>

<asp:Content ID="PopupContent" ContentPlaceHolderID="Page_Body" runat="Server">
    
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    

    
    <div id="pricing-graph">
        <h1>% Inventory</h1>
        <span class="right hide_nonJS">
            <asp:LinkButton ID="PricingGraphLabelDetailToggle" runat="server" Text="Show Units" OnLoad="PricingGraphLabelDetailToggle_Load" OnClick="PricingGraphLabelDetailToggle_Click" />
        </span>
        <span class="left hide_nonJS">
            <label for="SaleStrategyList" class="dropdown-opions-label"> Show: </label>
                <asp:DropDownList ID="SaleStrategyList" runat="server"
                        AutoPostBack="True"
                        OnPreRender="SalesStrategyList_PreRender"
                        OnSelectedIndexChanged="SalesStrategyList_SelectedIndexChanged" TabIndex="1" CssClass="dropdown-opions">
                    <asp:ListItem Text="All Inventory" Selected="true" Value="A" />
                    <asp:ListItem Text="Retail Inventory" Value="R" />
                </asp:DropDownList>
        </span>
        
        <asp:HiddenField runat="server" ID="PricingGraphLabelDetailEnabled" Value="True" />

        <cwc:AsyncObjectDataSourceControl
            ID="PricingGraphByRisk_DataSource"
            runat="server"
            TypeName="FirstLook.CommandCenter.DomainModel.Service.InventoryPricingRiskGraphDataService">
            <Parameters>
                <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                <owc:MemberParameter Name="member" Type="Object" />
                <asp:Parameter DefaultValue="2" Name="mode" Type="Int32" />
                <asp:ControlParameter Name="saleStrategy" Type="String" ControlID="SaleStrategyList" />
            </Parameters>
        </cwc:AsyncObjectDataSourceControl>
        <DCWC:Chart ID="PricingGraphByRisk" runat="server"
                AutoSize="false"
                DataSourceID="PricingGraphByRisk_DataSource"
                Width="590px"
                Height="207px"
                BackImageMode="Unscaled"
                BackImageTranspColor="Transparent"
                BackColor="Transparent"
                MapEnabled="true"
                OnDataBound="PricingGraphByRisk_OnDataBound">
            <Legends>
                <DCWC:Legend Enabled="False" LegendStyle="Row" Name="Default">
                </DCWC:Legend>
            </Legends>
            <Series>
                <DCWC:Series Name="RiskBuckets" ShadowOffset="1" ValueMembersY="Percentage, Units, RiskIndex"
                    YValuesPerPoint="3" ValueMemberX="RiskName" YAxisType="Secondary" ShadowColor="Transparent"
                    ChartArea="RiskBuckets" Font="Arial Black, 7" Color="#9caabd" ChartType="Column">
                </DCWC:Series>
            </Series>
            <ChartAreas>
                <DCWC:ChartArea Name="RiskBuckets" BackColor="Transparent">
                    <AxisX MarksNextToAxis="False" LineColor="Transparent" LabelsAutoFit="false">
                        <MajorTickMark Enabled="False" />
                        <MajorGrid Enabled="False" />
                    </AxisX>
                    <AxisY2 MarksNextToAxis="False" LineColor="Transparent" Minimum="0" Maximum="119"
                        Interval="25" IntervalType="Number">
                        <MajorTickMark Enabled="False" />
                        <MajorGrid Enabled="False" />
                    </AxisY2>
                    <Position Height="87" Width="98" X="0" Y="2" />
                </DCWC:ChartArea>
            </ChartAreas>
        </DCWC:Chart>
    </div>
    
    <script type="text/javascript" language="javascript">
        // verify this works
        function UpdateColumnIndex(button,hidden,value) {			
            $get(hidden).value = value;
            $get(button).click();
        }
    </script>
    
    <asp:HiddenField ID="ColumnIndex" runat="server" Value="3" OnLoad="ColumnIndex_Load" OnValueChanged="ColumnIndex_ValueChanged" />
    
    <asp:Button ID="PostBackButton" style="display: none;" runat="server" OnClick="PostBackButton_Click" />
    
    <div id="inventoryTable">
        <cwc:AsyncObjectDataSourceControl
            ID="InventoryPricingRiskGridView_DataSource"
            runat="server"
            TypeName="FirstLook.CommandCenter.DomainModel.Service.InventoryPricingRiskTableDataService">
            <Parameters>
                <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                <owc:MemberParameter Name="member" Type="Object" />
                <asp:Parameter DefaultValue="3" Name="mode" Type="Int32" />
                <asp:ControlParameter Name="riskIndex" Type="Int32" ControlID="ColumnIndex" />
                <asp:ControlParameter Name="saleStrategy" Type="String" ControlID="SaleStrategyList" />
            </Parameters>
        </cwc:AsyncObjectDataSourceControl>
        <asp:GridView runat="server"
            UseAccessibleHeader="true" 
            ID="InventoryPricingRiskGridView"
            DataSourceID="InventoryPricingRiskGridView_DataSource"
            CssClass="popuptable"
            AlternatingRowStyle-CssClass="odd"
            AllowPaging="false"
            AllowSorting="true"
            AutoGenerateColumns="false"
            OnInit="InventoryPricingRiskGridView_Init"
            OnRowDataBound="InventoryPricingRiskGridView_RowDataBound"
            ShowFooter="true">
            <HeaderStyle CssClass="center" />
            <Columns>
                <asp:BoundField 
                    AccessibleHeaderText="Dealership" 
                    HeaderText="Dealership" 
                    DataField="Dealer" 
                    SortExpression="Dealer" />                
                <asp:BoundField 
                    AccessibleHeaderText="In-Group Rank"
                    HeaderText="In-Group Rank"
                    DataField="Rank" 
                    SortExpression="Rank" />                   
                <asp:HyperLinkField
					 AccessibleHeaderText="% Inventory Units with Potential Overpricing Risk"
					 HeaderText="% Inventory Units with Potential Overpricing Risk"
					 SortExpression="Percentage"
					 DataTextField="Percentage"
					 DataTextFormatString="{0:0'%'}" 
					 DataNavigateUrlFields="Dealer_ID"
					 DataNavigateUrlFormatString="~/EnterDealership.aspx?dealerId={0}&application=ipa" />
                <asp:BoundField 
                    AccessibleHeaderText="Vehicle Count" 
                    HeaderText="Vehicle Count" 
                    DataField="Units_Risk"
                    DataFormatString="{0:0}" 
                    SortExpression="Units_Risk"
                    HtmlEncode="false" />
                <asp:BoundField 
                    AccessibleHeaderText="Unit Cost" 
                    HeaderText="Unit Cost" 
                    DataField="UnitCost" 
                    DataFormatString="{0:C0}" 
                    SortExpression="UnitCost"
                    HtmlEncode="false" />
                <asp:BoundField 
                    AccessibleHeaderText="Book versus Cost" 
                    HeaderText="Book vs Cost" 
                    DataField="BookVersusCost"
                    DataFormatString="{0:C0}"
                    SortExpression="BookVersusCost"
                    HtmlEncode="false" />               
            </Columns>
        </asp:GridView>
        
    </div>
                
</asp:Content>