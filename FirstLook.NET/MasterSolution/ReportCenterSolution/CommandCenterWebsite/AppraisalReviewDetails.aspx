<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AppraisalReviewDetails.aspx.cs" Inherits="AppraisalReviewDetails" MasterPageFile="~/Master_Pages/Page_Popup.master" %>

<%@ Register TagPrefix="firstlook" TagName="Email" Src="~/Controls/Common/Email.ascx" %>

<asp:Content ID="PopupHeader" ContentPlaceHolderID="Page_Header" runat="server">
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <firstlook:Email ID="Email2" OnSend="EmailClient_Send" Subject="Make-A-Deal Alert" runat="server" />
        <h2><asp:Literal ID="DealerGroup" runat="server" OnInit="DealerGroup_Init"></asp:Literal></h2>
        <h1><asp:Literal ID="ChartTitle" runat="server" OnInit="ChartTitle_Init"></asp:Literal></h1>
    </div>
</asp:Content>

<asp:Content ID="PopupContent" ContentPlaceHolderID="Page_Body" runat="Server">
    <h1 id="graphheader"><asp:Literal ID="GraphTitle" runat="server"></asp:Literal></h1>
    <!-- Appraisal Review time period -->
    <cwc:AsyncObjectDataSourceControl
        ID="AppraisalReviewTimePeriodDataService_DataSource"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalReviewTimePeriodDataService"
        runat="server">
    </cwc:AsyncObjectDataSourceControl>
    <a id="DrilldownTimeSpan" class="dyna_link">
        <asp:Label ID="AppraisalReviewDetails_TimePeriodLabel" runat="server" />
        <asp:Image ID="Image1" SkinID="MenuIcon" CssClass="hide_nonJS" runat="server"/>
    </a>
    <div id="selectDrilldownTimeRange" class="inlinepopup selectbox" style="display:none;">
        <div id="closeDrilldownTimeRange" class="closebox right"></div>
        <h3>Time period for Make-A-Deals:</h3>
        <asp:DropDownList 
            ID="AppraisalReviewDaysBack_DropDownList" 
            DataSourceID="AppraisalReviewTimePeriodDataService_DataSource"
            DataTextField="Label"
            DataValueField="Value"
            AutoPostBack="true"
            OnPreRender="AppraisalReviewDaysBack_DropDownList_OnPreRender"
            OnDataBound="AppraisalReviewDaysBack_DropDownList_OnDataBound"
            runat="server">
        </asp:DropDownList>
    </div>

    <!-- Appraisal Review details -->
    <cwc:AsyncObjectDataSourceControl
        ID="AppraisalReviewDetails_DataSource"
        runat="server"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalReviewTableDataService">
        <Parameters>
            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
            <owc:MemberParameter Name="member" Type="Object" />
            <asp:ControlParameter Name="daysBack" Type="String" ControlID="AppraisalReviewDaysBack_DropDownList" />
        </Parameters>
    </cwc:AsyncObjectDataSourceControl>
    <asp:GridView ID="AppraisalReviewDetails_GridView" 
        ShowFooter="true" 
        CssClass="popuptable" 
        runat="server" 
        DataSourceID="AppraisalReviewDetails_DataSource" 
        AutoGenerateColumns="False" 
        UseAccessibleHeader="true" 
        AllowSorting="true"
        OnInit="AppraisalReviewDetails_Init"
        OnRowDataBound="AppraisalReviewDetails_RowDataBound"
        OnDataBound="AppraisalReviewDetails_DataBound">
        <Columns>
            <asp:BoundField
                HeaderText="In-Group Rank"
                DataField="InGroupRank"
                SortExpression="InGroupRank" />
            <asp:BoundField
                HeaderText="Dealership"
                DataField="Dealer"
                SortExpression="Dealer" />
            <asp:TemplateField HeaderText="# Make-A-Deals/# Appraisals">
                <ItemTemplate>
                    <asp:Panel ID="MakeADealBarChart" CssClass="metric" runat="server">
                                <asp:Panel id="MakeADealBar" class="targetbar" runat="server">
                                </asp:Panel>
                                <asp:Panel Id="NumberOfAppraisalsBar" class="targetlegend" runat="server"><%# IsNull(Eval("NumberOfAppraisals")) ? "--" : Eval("NumberOfAppraisals")%></li>
                                    <ul class="bargraph">
                                    <li>
                                            <asp:HyperLink ID="NumberOfAppraisalReviewsBarHyperlink" 
                                            runat="server"
                                            NavigateUrl='<%# Eval("Dealer_ID", "~/EnterDealership.aspx?dealerId={0}") %>'
                                            Visible='<%# (IsNull(Eval("NumberOfAppraisalReviews")) || Eval("Dealer").Equals("Whole group")) ? false : true %>'>
                                                <asp:Label ID="NumberOfAppraisalReviewsWithLink"
                                                    CssClass="percentbar"
                                                    runat="server" />
                                            </asp:HyperLink>
                                            <asp:Label ID="NumberOfAppraisalReviewsWithoutLink"
                                                Visible='<%# (IsNull(Eval("NumberOfAppraisalReviews")) || Eval("Dealer").Equals("Whole group")) ? true : false %>'
                                                CssClass="percentbar"
                                                runat="server" />
                                    </li>
                                    <li class="percentlegend">
                                        <asp:HyperLink ID="NumberOfAppraisalReviewsItemHyperlink" 
                                            runat="server"
                                            NavigateUrl='<%# Eval("Dealer_ID", "~/EnterDealership.aspx?dealerId={0}") %>'
                                            Visible='<%# (IsNull(Eval("NumberOfAppraisalReviews")) || Eval("Dealer").Equals("Whole group")) ? false : true %>'
                                            Text='<%# Eval("NumberOfAppraisalReviews") %>'
                                            />
                                        <asp:Label ID="NumberOfAppraisalReviewsItemHyperlinkLess" 
                                            runat="server"
                                            Visible='<%# (IsNull(Eval("NumberOfAppraisalReviews")) || Eval("Dealer").Equals("Whole group")) ? true : false %>'
                                            Text='<%# IsNull(Eval("NumberOfAppraisalReviews")) ? "--" : Eval("NumberOfAppraisalReviews")%>'/>
                                    </li>
                                    </ul>
                                </asp:Panel>
  
                    </asp:Panel>
                    <asp:Panel ID="MakeADealFigures" runat="server">
                        <asp:Label Text='<%# IsNull(Eval("NumberOfAppraisals")) ? "--" : Eval("NumberOfAppraisalReviews") + "/" + Eval("NumberOfAppraisals")%>' runat="server" ID="MakeADealTotals" />
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Avg. Appraisal vs. Book" SortExpression="AvgAppraisalVsBookValue" DataField="AvgAppraisalVsBookValue" DataFormatString="{0:C0}" />
            <asp:BoundField HeaderText="Avg. Appraisal vs. Naaa" SortExpression="AvgAppraisalVsNaaaValue" DataField="AvgAppraisalVsNaaaValue" DataFormatString="{0:C0}" />
        </Columns>
    </asp:GridView>
</asp:Content>
