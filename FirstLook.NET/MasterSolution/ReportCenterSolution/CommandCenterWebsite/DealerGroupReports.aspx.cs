using System;
using FirstLook.Reports.App.ReportDefinitionLibrary;

public partial class DealerGroupReports : DealerGroupPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Form.Attributes.Add("class", "pmr");

        int businessUnitID = SoftwareSystemComponentState.DealerGroup.GetValue().Id;

        ReportGroupingFactory reportGroupingFactory = WebReportDefinitionFactory.NewReportGroupingFactory();

        IReportListHelper.PopulateReportList(PerformanceAndUsage, reportGroupingFactory.BuildReportTree("F6A5EA18-3F53-4aa3-9DC1-465103E821F8", businessUnitID));
        IReportListHelper.PopulateReportList(InventoryManagement, reportGroupingFactory.BuildReportTree("AFB764A8-46D7-4ddc-A1A1-FB8003B9F37B", businessUnitID));
        IReportListHelper.PopulateReportList(WholesaleBuyingAndSelling, reportGroupingFactory.BuildReportTree("EFAC9978-DAD2-44f6-B8D9-0A4A232C557B", businessUnitID));
    }
}
