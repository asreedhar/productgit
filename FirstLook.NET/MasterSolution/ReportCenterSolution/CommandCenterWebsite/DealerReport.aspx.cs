using System;
using FirstLook.DomainModel.Oltp;

public partial class Reports_DealerReport : ReportPage
{
    protected override void Page_PreInit(object sender, EventArgs e)
    {
        base.Page_PreInit(sender, e);
        base.IsAnalyicsEnabled = true;
    }

    protected void ReportViewer_Drillthrough(object sender, EventArgs e)
    {
        
    }

    protected void Page_Load(object sender,EventArgs e)
    {
        Page.Title = ReportControl1.ReportName;

        if (Request.QueryString["Id"] == "D8676024-5EE8-47b7-AD81-153CFBDAD3C9#4")
        {
            Page.Title = "Deal Log by Buyer";
        }else
        {
            if (Request.QueryString["Id"] == "D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E" || Request.QueryString["Id"] == "D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E#3")
            {
                Page.Title= "Closed: Trade-Ins Appraised";
            }
        else
            {
                if (Request.QueryString["Id"] == "D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E#5")
                {
                    Page.Title = "Not Closed: Trade-Ins Appraised";
                }
            }
        }

    }
    
    protected override string Token()
    {
        return SoftwareSystemComponentStateFacade.DealerComponentToken;
    }
}
