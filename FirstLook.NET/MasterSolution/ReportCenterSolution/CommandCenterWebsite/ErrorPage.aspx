<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorPage.aspx.cs" MasterPageFile="~/Master_Pages/Base_Page.master" Inherits="Home_ErrorPage" %>
<%@ Register TagPrefix="menu" TagName="SiteMenu" Src="~/Controls/Common/SiteMenu.ascx" %>


<asp:content id="Scripts" contentplaceholderid="HeadElements_Placeholder" runat="server">
    <script src="Static/Scripts/home.js" type="text/javascript"></script>
</asp:content>
<asp:Content ID="DealerGroupDashboardInfo" ContentPlaceHolderID="Header_B_Placeholder" runat="server">
<menu:SiteMenu ID="Menu" runat="server" />

</asp:Content>

<asp:Content ID="PageBody" ContentPlaceHolderID="Body_A_PlaceHolder" runat="server">
    <div id="container">
<div id="body_c">

        <div>
            <h2 runat="server" id="ErrorTitle"></h2>
            <p runat="server" id="ErrorDescription"></p>
            <p runat="server" id="ErrorClose" visible="false">If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665. Click <a href="javascript:window.close()">here</a> to close the window.</p>
            <p runat="server" id="ErrorTarget" visible="false">You can <asp:HyperLink runat="server" ID="PageLink">enter the application</asp:HyperLink> if you like. If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.</p>
            <p runat="server" id="ErrorHome" visible="false">You can <asp:HyperLink runat="server" ID="HomeLink">return home</asp:HyperLink> if you like. If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.</p>
        </div>

</div>
</div>
</asp:Content>