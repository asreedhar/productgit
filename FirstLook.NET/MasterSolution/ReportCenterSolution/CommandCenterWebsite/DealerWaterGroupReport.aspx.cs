using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASP;
using Dundas.Gauges.WebControl;
using FirstLook.DomainModel.Oltp;
using MsLocalReport = Microsoft.Reporting.WebForms.LocalReport;
using MsDrillthroughEventArgs = Microsoft.Reporting.WebForms.DrillthroughEventArgs;

public partial class DealerWaterGroupReport :ReportPage
{
    private string storeLevelWaterReport = "Group Level Store level Water Report";
    //private string vehicleLevelWaterReport = "Group Level Vehicle level Water Report";
    private string storeLevelWaterReportId = "EEED0D2B-148F-4688-BBAA-C74F6C8AFDE8";
    private string vehicleLevelWaterReportId = "E6A6F567-2E14-433A-ABCA-0E97DA8A1595";
    protected override void Page_PreInit(object sender, EventArgs e)
    {
        base.Page_PreInit(sender, e);
        base.IsAnalyicsEnabled = true;
        SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, Token());

        BusinessUnitSet = MemberBusinessUnitSetFacade.FindOrCreate(
            SoftwareSystemComponentState.DealerGroup.GetValue(),
            SoftwareSystemComponentState.DealerGroupMember());
    }
    protected SoftwareSystemComponentState SoftwareSystemComponentState
    {
        get { return (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
        set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
    }
    protected ICollection<BusinessUnit> BusinessUnitSet
    {
        get { return (ICollection<BusinessUnit>)Context.Items[MemberBusinessUnitSetFacade.HttpContextKey]; }
        set { Context.Items[MemberBusinessUnitSetFacade.HttpContextKey] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = ReportControl1.ReportId == storeLevelWaterReportId ? "Water Report By Store" : "Water Report By Vehicle";
        //***** Case id:30351 : Standerdized header on PMR reports only

        if (Request.QueryString.ToString().Contains("pmr"))
        {
            if (Request.QueryString.ToString().Contains("cp=cc"))
            {
                SiteMenu1.FindControl("backLink").Visible = true;
                this.PMRLabel.Visible = true;
                SiteMenu1.backButtonPostBackUrl = "/command_center/PerformanceManagementCenter.aspx";
            }
            else if (Request.QueryString.ToString().Contains("cp=dr"))
            {
                SiteMenu1.FindControl("backLink").Visible = true;
                this.PMRLabel.Visible = true;
                SiteMenu1.backButtonPostBackUrl = "/command_center/DealerReports.aspx";
            }
        }
        else
        {
            SiteMenu1.FindControl("backLink").Visible = false;
            this.PMRLabel.Visible = false;
        }
        //*****
        if (!IsPostBack)
        {
            CopyrightYear.Text = DateTime.Now.Year.ToString();

            if (ReportControl1.ReportId == storeLevelWaterReportId)
            {
                
                HomeUrl.InnerText = SoftwareSystemComponentState.DealerGroup.GetValue().Name.ToString() + " Command Center" ;
                HomeUrl.HRef = "~/PerformanceManagementCenter.aspx";
                FirstDrill.InnerText = "Water Report By Store";

                setAttribute(SecondDrill, "class", "hide_nonJS");
                setAttribute(lblVehicle, "class", "hide_nonJS");
               
            }
            else //if (ReportControl1.ReportName == vehicleLevelWaterReport)
            {
                
                if (IsDealerReport())
                {
                    pnlNavaigation.Visible = false;
                    PmrVehicleReport.Text = "Water Report by Vehicle > " + SoftwareSystemComponentState.Dealer.GetValue().Name.ToString();
                    PmrVehicleReport.CssClass = "a_font setToLeft ";
                }
                else
                { 
                    HomeUrl.InnerText = SoftwareSystemComponentState.DealerGroup.GetValue().Name.ToString() + " Command Center";
                    HomeUrl.HRef = "~/PerformanceManagementCenter.aspx";
               
                    FirstDrill.HRef = "~/DealerWaterGroupReport.aspx?Id=" + storeLevelWaterReportId;
                    setAttribute(FirstDrill, "class", "a_color a_font");
                    setAttribute(SecondDrill, "class", "a_font");
                    setAttribute(lblVehicle, "class", "");
                }
               

            }
                //.DealerGroup.GetValue().Name.ToUpper()
            //Check for Export button
            LinkButton lbExportToExcel = (LinkButton) ReportControl1.FindControl("ReportExportExcel");
            if(lbExportToExcel != null) 
            {
                lbExportToExcel.Text = "Export";
                lbExportToExcel.Width = 42;
                //lbExportToExcel.Parent 
                
                //ReportControl1.FindControl("ReportNavigationTop").Visible = false;
            }
            //Hide Pdf button
            LinkButton lbExportToPdf = (LinkButton)ReportControl1.FindControl("ReportExportPDF");
            if (lbExportToPdf != null)
            {
                lbExportToPdf.Parent.Visible = false;
                lbExportToPdf.Visible = false;
                //lbExportToPdf.Width = 0;
            }
            
        }
        
    }

    public void ReportViewer_Drillthrough(object sender, MsDrillthroughEventArgs e)
    {
        if (e.Report != null && ((MsLocalReport) e.Report).OriginalParametersToDrillthrough != null)
        {
            string DrillThroughBussinessId = ((MsLocalReport) e.Report).OriginalParametersToDrillthrough[0].Values[0].ToString();
            VehicleLevelDrillThrough(DrillThroughBussinessId);
        }
        
        
    }

    private void VehicleLevelDrillThrough(string drillThroughID)
    {
        Response.Redirect("~/DealerWaterGroupReport.aspx?Id=" + vehicleLevelWaterReportId + "&drillthrough=" + drillThroughID);
    }
    
    private void setAttribute(HtmlControl cl, string attr, string val)
    {
        cl.Attributes.Remove(attr);
        cl.Attributes.Add(attr,val);
    }
    //private string GetSelectedDealersId()
    //{
    //    string value = string.Empty;
    //    int memberId =  SoftwareSystemComponentState.DealerGroupMember().Id ;

    //    IList<BusinessUnit> businessunitList = BusinessUnitFinder.Instance().FindAllDealershipsByDealerGroupAndMember(SoftwareSystemComponentState.DealerGroup.GetValue().Id,memberId);
    //    if(businessunitList!=null) value = string.Join(",",businessunitList.Select(x => x.Id));
    //    return value;
    //}

    //private string GetAccessibleDealersId()
    //{
    //    string value = string.Empty;
    //    ICollection<BusinessUnit> accessibleDealers = SoftwareSystemComponentState.DealerGroupMember().AccessibleDealers(SoftwareSystemComponentState.DealerGroup.GetValue());
    //    if (accessibleDealers != null) value = string.Join(",", accessibleDealers.Select(x => x.Id));
    //    return value;
    //}
    protected override string Token()
    {
         string parentCallingPage = Convert.ToString(System.Web.HttpContext.Current.Request.ServerVariables["HTTP_REFERER"] as object)
                                                .ToUpper();
        string reportType = Request.QueryString.GetValues("cp") == null
            ? ""
            : Request.QueryString.GetValues("cp")[0];
        //if (parentCallingPage.Contains("DEALERREPORT") && reportType == "pmr")
        if (reportType == "dr")
        {
            return SoftwareSystemComponentStateFacade.DealerComponentToken;
        }
        return SoftwareSystemComponentStateFacade.DealerGroupComponentToken;
    }

    protected Boolean IsDealerReport()
    {
        Boolean isDealerReport = false;
        if (System.Web.HttpContext.Current != null &&
            Convert.ToString(System.Web.HttpContext.Current.Request.ServerVariables["HTTP_REFERER"] as object)
                        .ToLower()
                        .Contains("performance") || Convert.ToString(System.Web.HttpContext.Current.Request.ServerVariables["HTTP_REFERER"] as object)
                        .ToLower()
                        .Contains("dealerreports"))
        {
            isDealerReport= true;
        }
        else if (Request.QueryString.HasKeys() && Request.QueryString.GetValues("type") != null && Request.QueryString.GetValues("type")[0].ToString() == "pmr")
        {
            isDealerReport = true;
        }
        else
        {
            isDealerReport = false;
        }
        return isDealerReport;
    }

}
