<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OptimalInventory.aspx.cs" MasterPageFile="~/Master_Pages/Page_Popup.master" Inherits="Home_OptimalInventory" %>

<%@ Register TagPrefix="firstlook" TagName="Email" Src="~/Controls/Common/Email.ascx" %>

<asp:Content ID="PopupHeader" ContentPlaceHolderID="Page_Header" runat="server">
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <firstlook:Email ID="Email1" OnSend="EmailClient_Send" Subject="Optimal Inventory" runat="server" />
        <h2><asp:Literal ID="DealerGroup" runat="server" OnInit="DealerGroup_Init"></asp:Literal></h2>
        <h1><asp:Literal ID="ChartTitle" runat="server" OnInit="ChartTitle_Init"></asp:Literal></h1>
    </div>
</asp:Content>

<asp:Content ID="PopupContent" ContentPlaceHolderID="Page_Body" runat="Server">

    <h1 id="graphheader"><asp:Literal ID="GraphTitle" runat="server"></asp:Literal></h1>
    
    <cwc:AsyncObjectDataSourceControl
        runat="server"
        ID="OptimalInventory_DataSource"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.OptimalInventoryTableDataService">
        <Parameters>
            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
            <owc:MemberParameter Name="member" Type="Object" />
        </Parameters>
    </cwc:AsyncObjectDataSourceControl>
    
    <asp:GridView ID="OptimalInventory" CssClass="popuptable" runat="server" DataSourceID="OptimalInventory_DataSource" AutoGenerateColumns="False" UseAccessibleHeader="true" AllowSorting="true"
        OnRowDataBound="OptimalInventory_RowDataBound" ShowFooter="true">
        <Columns>
            <asp:BoundField DataField="OptimalInventoryRank" HeaderText="In-Group Rank" SortExpression="OptimalInventoryRank" />
            <asp:BoundField DataField="Dealer" HeaderText="Dealership" SortExpression="Dealer"  />
            <asp:TemplateField HeaderText="Optimal Inventory in Stock" SortExpression="OptimalInventory">
                <ItemTemplate>
                    <ul class="metric dyna_link" onclick="navigateTo('EnterDealership.aspx?dealerId=<%# Eval("Dealer_ID") %>&amp;application=stockingReports');">
                        <li class="percentbargreen" style="width: <%# (double)(Eval("OptimalInventory")==null ? Eval("OptimalInventory") : 0.0) * 1.4d %>px"></li>
                        <li class="percentlegend"><%# Eval("OptimalInventory", "{0:0}")%>%</li>
                    </ul>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Units(Target)" SortExpression="OptimallyStockedUnits">
                <ItemTemplate>
                    <%# Eval("OptimallyStockedUnits", "{0:N0}") %> (out of <%# Eval("OptimalUnits", "{0:N0}")%>)
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>
