<%@ Page Language="C#" MasterPageFile="~/Master_Pages/Page_Report.master" AutoEventWireup="true" CodeFile="DealerGroupReport.aspx.cs" Inherits="Reports_DealerGroupReport" Title="Pre-Owned Performance Management Center" ValidateRequest="false" EnableEventValidation="false"%>

<%@ Register TagPrefix="reports" TagName="ReportControl" Src="~/Controls/Reports/ReportControl.ascx" %>

<asp:content ID="DealerGroupReport_Page_Scripts" contentplaceholderid="Page_Scripts" runat="server">
    <script type="text/javascript" src="Static/Scripts/Lib/scriptaculous/scriptaculous.js"></script>  
    <script type="text/javascript" src="Static/Scripts/viewer.js"></script>
</asp:content>

<asp:Content ID="DealerGroupReport_Page_Body" ContentPlaceHolderID="PageBody" runat="Server">
                <span class="right">
					<span id="homelink" style="display: none"><a id="A1" href="~/DealerGroupReports.aspx" runat="server">Home</a> | </span>
					<span id="backlink" style="display: none;"><a href="javascript:history.back()">Back</a> | </span>
					<span id="closelink" style="display: none"><a href="javascript:window.close();">Close window</a></span>
                </span>
                <asp:Image runat="server" CssClass="left" SkinID="LogoFirstlook" ID="Logo" />
    <reports:ReportControl runat="server" ID="ReportControl1" OnDrillThrough="ReportViewer_Drillthrough"/>
</asp:Content>
