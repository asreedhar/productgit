﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Pages/Base_NewReport.master" AutoEventWireup="true" CodeFile="DealerLevelGroupPage.aspx.cs" Inherits="DealerLevelGroupPage" %>

<%@ Register TagPrefix="reports" TagName="ReportControl" Src="~/Controls/Reports/NewReportControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript" src="Static/Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="Static/Scripts/Lib/prototype.js" ></script>
    <script type="text/javascript" src="Static/Scripts/Lib/core.js"></script>
    <script type="text/javascript" src="Static/Scripts/Lib/scriptaculous/scriptaculous.js"></script>
    <script type="text/javascript" src="Static/Scripts/viewer.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
        <reports:ReportControl ID="ReportControl1" OnDrillThrough="ReportControl1_Drillthrough" runat="server"/>
</asp:Content>

