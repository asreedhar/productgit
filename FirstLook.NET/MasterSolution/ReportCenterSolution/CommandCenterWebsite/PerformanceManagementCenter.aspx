<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PerformanceManagementCenter.aspx.cs" Inherits="PerformanceManagementCenter" 
    MasterPageFile="~/Master_Pages/Base_Page.master" EnableViewState="false" %>

<%@ OutputCache Location="None" %>

<%@ Register TagPrefix="reports" TagName="DefinitionList" Src="~/Controls/Reports/DefinitionList.ascx" %>
<%@ Register TagPrefix="reports" TagName="UnorderedList" Src="~/Controls/Reports/UnorderedList.ascx" %>
<%@ Register TagPrefix="appraisal" TagName="AppraisalClosingRateGauge" Src="~/Controls/Appraisal/AppraisalClosingRateGauge.ascx" %>
<%@ Register TagPrefix="inventory" TagName="OptimalInventoryGauge" Src="~/Controls/Inventory/OptimalInventoryGauge.ascx" %>
<%@ Register TagPrefix="inventory" TagName="CurrentInventoryBreakdownChart" Src="~/Controls/Inventory/CurrentInventoryBreakdownChart.ascx" %>
<%@ Register TagPrefix="reports" TagName="DealershipReports" Src="~/Controls/Reports/DealershipReports.ascx" %>
<%@ Register TagPrefix="reports" TagName="SelectDealerReportsView" Src="~/Controls/Reports/SelectDealerReportsView.ascx" %>
<%@ Register TagPrefix="analysis" TagName="EdgeSystemUsageGraph" Src="~/Controls/Analysis/EdgeSystemUsageGraph.ascx" %>
<%@ Register TagPrefix="menu" TagName="SiteMenu" Src="~/Controls/Common/SiteMenu.ascx" %>

<asp:content id="Scripts" contentplaceholderid="HeadElements_Placeholder" runat="server">
        <script src="Static/Scripts/home.js" type="text/javascript"></script>
    <script src="Static/Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="Static/Scripts/Lib/kendo.js" type="text/javascript"></script>
    <script src="Static/Scripts/Lib/bargraph_home.js" type="text/javascript"></script>
    <script src="Static/Scripts/Lib/home_appraise_graph.js" type="text/javascript"></script>
    <script src="Static/Scripts/Lib/home_core_inventory_graph.js" type="text/javascript"></script>
    <script src="Static/Scripts/Lib/home_current_inventory_graph2.js" type="text/javascript"></script>
    <link href="Static/bar_home.css" rel="stylesheet" type="text/css" />
</asp:content>

<asp:Content ID="DealerGroupDashboardInfo" ContentPlaceHolderID="Header_B_Placeholder" runat="server">

<menu:SiteMenu ID="Menu" runat="server" />
<img id="FLMain" src="Static/Images/logo-main-firstlook.jpg" style="float:left;margin:-66px 0 0 0;" />
    <div style="width:90%;">
        <em style="float: left;">See Data For:</em>
        <em style="float: right;margin-right: 230px;">Visit Store</em>
        <h3 class="dyna_link em_link" id="ShowDealerSelection" style="clear: both;"><asp:Literal ID="ViewingAsDealerName" runat="server" OnLoad="ViewingAsDealerName_Load"></asp:Literal><asp:Image SkinID="MenuIcon" runat="server" /></h3>
        <div id="DealerSelection" class="inlinepopup"  style="display:none;">
            <div id="CloseDealerSelection" class="closebox right"></div>
            <h3>Select Dealerships:</h3>
            <div class="controls">
                <input type="submit" id="ConfirmSelection" class="right" onclick="return ValidateDealerSelection();" value="Submit" />
                <a class="dyna_link" onclick="SelectAllCheckBoxes(this); return false;">Select All</a> | 
                <a class="dyna_link" onclick="ClearAllCheckBoxes(this); return false;">Clear All</a> 
            </div>
            <asp:ObjectDataSource
                id="DealerSelection_DataSource"
                TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder"
                SelectMethod="FindAllDealershipsByDealerGroupAndMember"
                runat="server">
                <SelectParameters>
                    <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                    <owc:MemberParameter Name="member" Type="Object" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:CheckBoxList ID="DealerSelection" runat="server" DataSourceID="DealerSelection_DataSource" OnDataBound="DealerSelection_DataBound" DataTextField="Name" DataValueField="Id" OnSelectedIndexChanged="DealerSelection_SelectedIndexChanged">
            </asp:CheckBoxList>
        </div>
        
        <h3 class="dyna_link hide_nonJS right" id="ShowDealershipHomeLinks">
                <asp:Label ID="DealershipHomeLinksButton" Text="DEALERSHIPS" OnClientClick="return false;" runat="server"/>
                <%-- <a class="dyna_link hide_nonJS button" onclick="return false;">DEALERSHIPS</a>--%><asp:Image ID="Image4" SkinID="MenuIcon" runat="server" />
            </h3>
            <div id="DealershipHomeLinks" class="inlinepopup" style="display:none;">
            <div id="closeDealershipHomeLinks" class="closebox right"></div>
            <h3><asp:Literal ID="Literal1" runat="server"></asp:Literal> Dealerships:</h3>
            <ul>
                <asp:ObjectDataSource
                    ID="BusinessUnitDataSource"
                    TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder"
                    SelectMethod="FindAllDealershipsByDealerGroupAndMember"
                    runat="server">
                    <SelectParameters>
                        <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                        <owc:MemberParameter Name="member" Type="Object" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Repeater ID="BusinessUnitList" DataSourceID="BusinessUnitDataSource" runat="server" OnItemDataBound="BusinessUnitList_ItemDataBound">
                    <ItemTemplate>
                        <li><asp:HyperLink ID="EnterDealership" runat="server" Target="_STORE"></asp:HyperLink></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <br />
        <span class="updated" style="display:none;" >Updated: <asp:Literal ID="LastUpdated" runat="server" OnLoad="LastUpdated_Load"></asp:Literal></span>
    </div>
    
</asp:Content>

<asp:content id="Content_A" contentplaceholderid="Body_A_PlaceHolder" runat="server">
    <div id="body_a">
    <div class="a_" id="appraisal">
        <!-- appraisal time period -->
        <cwc:AsyncObjectDataSourceControl
            ID="AppraisalClosingRateGauge_TimePeriod_DataSource"
            TypeName="FirstLook.CommandCenter.DomainModel.Service.TimePeriodDataService"
            runat="server">
        </cwc:AsyncObjectDataSourceControl>
    

    <asp:UpdatePanel ID="UpdateAppraisalColumn" UpdateMode="Conditional" runat="server" >
        <ContentTemplate>
        <%--<a id="appraisalTimeSpan" class="dyna_link">
          <span id="appraisalTimeSpanText">
                <asp:Label ID="AppraisalTimeSpanLabel" runat="server" />
            </span>
            <asp:Image ID="Image1" SkinID="MenuIcon" CssClass="hide_nonJS" runat="server"/>
        </a>--%>

        <div id="selectAppraisalClosingRateTimeRange" class="inlinepopup selectbox" style="display:none;">
        <div id="closeAppraisalClosingRateTimeRange" class="closebox right"></div>
        <h3>Time Period for Appraisal Closing Rate:
        </h3>
        <asp:UpdateProgress ID="UpdateAppraisalColumnProgressWheel" AssociatedUpdatePanelID="UpdateAppraisalColumn" runat="server">
            <ProgressTemplate>
                <script language="javascript" type="text/javascript">
                var AppraisalClosingRateGauge_savedFunction;
                Event.observe(window, 'load', function() {
                    AppraisalClosingRateGauge_savedFunction = $('<%= AppraisalClosingRateGauge_TimePeriod.ClientID %>').onchange;
                    AppraisalClosingRateGauge_getOnChangeFunction();
                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AppraisalClosingRateGauge_getOnChangeFunction);
                    
                });
                function AppraisalClosingRateGauge_getOnChangeFunction() {
                    $('<%= AppraisalClosingRateGauge_TimePeriod.ClientID %>').onchange = function() {
                        $('<%= AppraisalClosingRateGauge_TimePeriod.ClientID %>').style.display = 'none';
                        AppraisalClosingRateGauge_savedFunction();
                        return true;
                    }
                }
                </script>
                <div class="loading">
                <asp:Image ID="UpdateAppraisalColumnProgressWheelImage" SkinID="ProgressWheel" runat="server"/>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        
            <asp:DropDownList
                ID="AppraisalClosingRateGauge_TimePeriod"
                DataSourceID="AppraisalClosingRateGauge_TimePeriod_DataSource"
                DataTextField="Name"
                DataValueField="Value"
                AutoPostBack="true"
                Width="230px"
                OnPreRender="AppraisalClosingRateGauge_TimePeriod_PreRender"
                runat="server">
            </asp:DropDownList>
        </div>


        <!-- appraisal closing rate gauge -->
        <div class="columnitem">
            <cwc:AsyncObjectDataSourceControl
                ID="AppraisalClosingRateGauge_DataSource"
                runat="server"
                
                TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalClosingRateGaugeDataService">
                <Parameters>
                    <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                    <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                    <owc:MemberParameter Name="member" Type="Object" />
                    <asp:ControlParameter ControlID="AppraisalClosingRateGauge_TimePeriod" Name="timePeriodId" Type="String" />
                </Parameters>
            </cwc:AsyncObjectDataSourceControl>
              <asp:formview ID="AppraisalClosingRateSummary" runat="server" DataSourceID="AppraisalClosingRateGauge_DataSource" OnDataBound="AppraisalClosingRateGauge_DataBound">
       <ItemTemplate>
        <div id="appraise-graph-div">
  
             <%-- <script type="text/javascript">
                  var link = escape("/reporting/trade_closing_rate_report?date_0=12%2F21%2F2013&date_1=01%2F17%2F2014")
              </script>--%>
  
              <div id="appraise-graph-arc">
              <img src="Static/Images/small-graph-arc.gif" id="appraise-graph-arc-image" />
               
              </div>
              <div id="appraise-graph-body" class="shadow">
              </div>
              <div id="appraise-graph-footer-wrapper">
                <div id="appraise-graph-footer">
                  Average Previous 4 Weeks<br />
                </div>
                <div id="appraise-graph-closing-rate">
                  <span id="appraise-graph-closing-rate-percentage" ><%# TryParseInt(Convert.ToString(DataBinder.Eval(Container.DataItem, "Avg", "{0:0}")))%></span>% Appraisal Closing Rate
                  <br />
                  <div style="font-family:Arial, Helvetica, Verdana, sans-serif; font-size:11px;">
                  High: <%# DataBinder.Eval(Container.DataItem, "Max", "{0:0%}")%> &nbsp; Low: <%# DataBinder.Eval(Container.DataItem, "Min", "{0:0%}")%>
                  </div>
                </div>
                <div id="appraise-graph-tooltip-container">
                  <div id="appraise-graph-tooltip"></div>
                </div>
              </div>
  
              <div class="appraise-graph-baroverlay" id="appraise-graph-baroverlay0" idx="0"></div>
              <div class="appraise-graph-baroverlay" id="appraise-graph-baroverlay1" idx="1"></div>
              <div class="appraise-graph-baroverlay" id="appraise-graph-baroverlay2" idx="2"></div>
                        
        </div>
       </ItemTemplate>
           
     </asp:formview>
         <%--<appraisal:AppraisalClosingRateGauge runat="server" Visible="true"/>--%>
        <!-- appraisal quick facts -->
        <div class="columnitem">
            <cwc:AsyncObjectDataSourceControl
                ID="AppraisalOverviewSummary_DataSource"
                runat="server"
                TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalOverviewGaugeDataService">
                <Parameters>
                    <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                    <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                    <owc:MemberParameter Name="member" Type="Object" />
                    <asp:ControlParameter ControlID="AppraisalClosingRateGauge_TimePeriod" Name="timePeriodId" Type="String" />
                </Parameters>
            </cwc:AsyncObjectDataSourceControl>
            <div id="appraise-facts">
                <h3>APPRAISAL QUICK FACTS</h3>
                <div class="group">
                <em><asp:Label id="AppraisalQuickFactsTimePeriodLabel" runat="server"/></em>
                <asp:FormView ID="AppraisalOverviewSummary" runat="server" DataSourceID="AppraisalOverviewSummary_DataSource" OnDataBound="AppraisalOverviewSummary_DataBound">       
                    <ItemTemplate>
                    <ul>
                        <li>
                        <h5>
                        <asp:HyperLink ID="AppraisalQuickFacts_TradeInInventoryAnalyzed" CssClass="drilldownlink" ToolTip="Trade-In Inventory Analyzed" Target="_QuickFacts" runat="server">Trade-In Inventory Analyzed: <%# DataBinder.Eval(Container.DataItem, "TradeInAvg", "{0:0%}")%></asp:HyperLink>
                        </h5>
                        High: <%# DataBinder.Eval(Container.DataItem, "TradeInMax", "{0:0%}")%> &nbsp; Low: <%# DataBinder.Eval(Container.DataItem, "TradeInMin", "{0:0%}")%>
                        </li>
                        <li>
                        <h5>
                        <asp:HyperLink ID="AppraisalQuickFacts_AvgImmWholesaleProfit" CssClass="drilldownlink" ToolTip="Average Immediate Wholesale Profit" Target="_QuickFacts" runat="server">Avg. Imm. Wholesale Profit: <%# DataBinder.Eval(Container.DataItem, "WholesaleAvg", "{0:c0}")%></asp:HyperLink>
                        </h5>
                        High: <asp:Label ID="HighAvgImmWholesaleProfit" runat="server" ><%# DataBinder.Eval(Container.DataItem, "WholesaleMax", "{0:$#,##0}")%></asp:Label> &nbsp; Low: <asp:Label ID="LowAvgImmWholesaleProfit" runat="server" ><%# DataBinder.Eval(Container.DataItem, "WholesaleMin", "{0:$#,##0}")%></asp:Label>
                        </li>
                    </ul>
                    </ItemTemplate>
                </asp:FormView>
                </div>
            </div>
        </div>
        </ContentTemplate>
        </asp:UpdatePanel>
        
        <!-- Make A Deals -->
        <cwc:AsyncObjectDataSourceControl
            ID="AppraisalReviewTimePeriodDataService_DataSource"
            TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalReviewTimePeriodDataService"
            runat="server">
        </cwc:AsyncObjectDataSourceControl>
        
<div class="columnitem" id="makeADeal">
    <asp:UpdatePanel ID="AppraisalReviewUpdatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <h3>Make-A-Deals&nbsp;</h3>
            <a id="appraisalReviewDaysBack" class="dyna_link" style="display:none;">
                <span id="appraisalReviewDaysBackText">
                    <asp:Label ID="AppraisalReviewDaysBackLabel" runat="server" />
                </span>
                <asp:Image ID="Image3" SkinID="MenuIcon" CssClass="hide_nonJS" runat="server"/>
            </a>
            <div class="group">
            <div id="selectAppraisalReviewDaysBack" class="inlinepopup selectbox" style="display:none;">
                <div id="closeAppraisalReviewDays" class="closebox right"></div>
                <asp:UpdateProgress ID="AppraisalReviewUpdateProgress" AssociatedUpdatePanelID="AppraisalReviewUpdatePanel" runat="server">
                    <ProgressTemplate>
                        <script language="javascript" type="text/javascript">
                            var AppraisalReviewWidget_savedFunction;
                            Event.observe(window, 'load', function() {
                                AppraisalReviewWidget_savedFunction = $('<%= AppraisalReviewDaysBack_DropDownList.ClientID %>').onchange;
                                AppraisalReviewWidget_getOnChangeFunction();
                                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(AppraisalReviewWidget_getOnChangeFunction);
                                
                            });
                            function AppraisalReviewWidget_getOnChangeFunction() {
                                $('<%= AppraisalReviewDaysBack_DropDownList.ClientID %>').onchange = function() {
                                    $('<%= AppraisalReviewDaysBack_DropDownList.ClientID %>').style.display = 'none';
                                    AppraisalReviewWidget_savedFunction();
                                    return true;
                                }
                            }
                        </script>
                        <div class="loading">
                            <asp:Image ID="AppraisalReviewUpdateProgressWheelImage" SkinID="ProgressWheel" runat="server"/>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                
                <asp:DropDownList 
                    ID="AppraisalReviewDaysBack_DropDownList" 
                    DataSourceID="AppraisalReviewTimePeriodDataService_DataSource"
                    DataTextField="Label"
                    DataValueField="Value"
                    AutoPostBack="true"
                    OnPreRender="AppraisalReviewDaysBack_DropDownList_OnPreRender"
                    Width="200px"
                    runat="server">
                </asp:DropDownList>
            </div>
            <cwc:AsyncObjectDataSourceControl
                ID="AppraisalReviewGroupSummary_DataSource"
                runat="server"
                TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalReviewSummaryWidgetDataService">
                <Parameters>
                    <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                    <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                    <owc:MemberParameter Name="member" Type="Object" />
                    <asp:ControlParameter ControlID="AppraisalReviewDaysBack_DropDownList" Name="daysBack" Type="String" />
                </Parameters>
            </cwc:AsyncObjectDataSourceControl>
            <div>
                <asp:FormView ID="AppraisalReviewSummary" runat="server" DataSourceID="AppraisalReviewGroupSummary_DataSource">
                        <ItemTemplate>
                        <ul>
                            <li>
                            <h5>
                            <a href="AppraisalReviewDetails.aspx?daysBack=<%#Eval("DaysBack")%>" class="drilldownlink">Average Make-A-Deals on deck: <%# DataBinder.Eval(Container.DataItem, "Avg", "{0:0}") %></a>
                            </h5>
                            High: <%# DataBinder.Eval(Container.DataItem, "Max", "{0:0}")%> &nbsp; Low: <%# DataBinder.Eval(Container.DataItem, "Min", "{0:0}")%>
                            </li>
                        </ul>
                        </ItemTemplate>
                </asp:FormView>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

        <div class="columnitem">
            <analysis:EdgeSystemUsageGraph id="EdgeSystemUsageGraph1" runat="server"></analysis:EdgeSystemUsageGraph>
        </div>
        <div class="group_reports columnitem">
            <reports:DefinitionList id="PerformanceAndUsage" runat="server"></reports:DefinitionList>
        </div>
    </div>
</div>
    <div class="b_"  id="inventory">
        <div id="inventory-graph">
                 
             <%--    <asp:UpdateProgress ID="CurrentInventoryBreakdownGraphProgessWheel" AssociatedUpdatePanelID="CurrentInventoryBreakdownGraph" runat="server">
                    <ProgressTemplate>

                        <div id="CurrentInventoryGraphLoadingIndicator" class="loading">
                        <asp:Image ID="CurrentInventoryGraphProgressWheelImage" SkinID="ProgressWheel" runat="server"/>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                
            <asp:UpdatePanel ID="CurrentInventoryBreakdownGraph" UpdateMode="Conditional" runat="server" >
            

                <ContentTemplate>
                    <!-- current inventory graph -->
                    <div id="invchart-content">
                        <inventory:CurrentInventoryBreakdownChart runat="server" /></div> <-- DO NOT INSERT A RETURN HERE. it messes up IE; i'm not even kidding. -tb 
                </ContentTemplate>
            </asp:UpdatePanel> --%>
            <asp:HiddenField ID="IsPercentMode" Value="true" runat="server" />
            <cwc:AsyncObjectDataSourceControl ID="CurrentInventoryBreakdownChart_DataSource"
                runat="server" TypeName="FirstLook.CommandCenter.DomainModel.Service.CurrentInventoryBreakdownGraphDataService">
                <Parameters>
                    <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                    <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                    <owc:MemberParameter Name="member" Type="Object" />
                    <asp:ControlParameter Name="IsPercentMode" Type="Boolean" ControlID="IsPercentMode" />
                </Parameters>
            </cwc:AsyncObjectDataSourceControl>
            <asp:FormView ID="CurrentInventoryBreakdownChartData" runat="server" DataSourceID="CurrentInventoryBreakdownChart_DataSource"
                OnDataBound="CurrentInventoryBreakdownChart_DataBound">

            </asp:FormView>  
             <div id="center">
                        <div id="topcenter">
                            <div class="centergauge">
                                <div id="inventory-graph-new">
                                    <div id="CurrentInventoryGraph-wrapper">
                                        <div id="CurrentInventoryGraph-top-arc">
                                            <img src="Static/images/border-inventory-arc.gif" />
                                        </div>
                                        <div id="CurrentInventoryGraph-top-title" class="shadow">
                                            <img src="Static/images/border-inventory-graph-title.gif" />
                                        </div>
                                        <div id="CurrentInventoryGraph-top-container">
                                            <div id="CurrentInventoryGraph-graph-container">
                                                <div id="CurrentInventoryGraph" class="shadow">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
            <!-- current inventory summary -->
            <cwc:AsyncObjectDataSourceControl
                ID="CurrentInventorySummary_DataSource"
                runat="server"
                TypeName="FirstLook.CommandCenter.DomainModel.Service.CurrentInventoryBreakdownGraphSummaryDataService">
                <Parameters>
                    <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                    <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                    <owc:MemberParameter Name="member" Type="Object" />
                </Parameters>
            </cwc:AsyncObjectDataSourceControl>
            <asp:FormView ID="CurrentInventorySummary" runat="server" DataSourceID="CurrentInventorySummary_DataSource" OnDataBound="CurrentInventorySummary_DataBound">
                <ItemTemplate>
                <ul style="background-color: #E3ECF4;width: 474px;position:relative; margin: 5px 0 0 5px;padding: 14px 10px 10px 10px;-webkit-box-shadow: 2px 2px 5px #b1b5b5;box-shadow: 2px 2px 5px #b1b5b5;-moz-box-shadow: 2px 2px 1px #b1b5b5;">
                    <li><strong><%# DataBinder.Eval(Container.DataItem, "VehiclesInStock", "{0:0}")%></strong> Vehicles </li>
                    <li><strong><%# DataBinder.Eval(Container.DataItem, "AvgDaysSupply", "{0:0}")%></strong> Days Supply</li>
                    <li><strong><%# DataBinder.Eval(Container.DataItem, "TotalInventoryDollars", "{0:C0}")%></strong> in Inventory</li>
                    <li>Book vs Cost: <strong><asp:Label ID="InvChartBookVsCost" CssClass="positivenumber" runat="server" ><%# DataBinder.Eval(Container.DataItem, "BookVsCost", "{0:C0}")%></asp:Label></strong></li>
                </ul>
                </ItemTemplate>
            </asp:FormView> 
        </div>
        

        <asp:UpdatePanel ID="BehindTheNumbers" UpdateMode="Conditional" runat="server" >
            <ContentTemplate>
                <div id="numbers" class="columnitem">
                    <!-- inventory sales time period -->
                    <cwc:AsyncObjectDataSourceControl
                        ID="InventorySalesSummary_TimePeriod_DataSource"
                        TypeName="FirstLook.CommandCenter.DomainModel.Service.TimePeriodDataService"
                        runat="server">
                    </cwc:AsyncObjectDataSourceControl>

                    <a id="numbersTimeSpan" class="dyna_link" style="display:none;">
                      	<span id="numbersTimeSpanText">
                            <asp:Label ID="NumbersTimeSpanText" runat="server" />
                      	</span>
                		<asp:Image ID="Image1" SkinID="MenuIcon" CssClass="hide_nonJS" runat="server"/>
                    </a>
                    
                    <div id="selectNumbersTimeRange" class="inlinepopup selectbox" style="display:none;">
                        <div id="closeSelectNumbersTimeRange" class="closebox right"></div>
                        <h3>Time Period for Behind the Numbers Metrics:</h3>
                        <asp:UpdateProgress ID="BehindTheNumbersProgessWheel" AssociatedUpdatePanelID="BehindTheNumbers" runat="server">
                            <ProgressTemplate>
                                <script language="javascript" type="text/javascript">
                                    var InventorySalesSummary_savedFunction;
                                    Event.observe(window, 'load', function () {
                                        InventorySalesSummary_savedFunction = $('<%= InventorySalesSummary_TimePeriod.ClientID %>').onchange;
                                        InventorySalesSummary_getOnChangeFunction();
                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(InventorySalesSummary_getOnChangeFunction);

                                    });
                                    function InventorySalesSummary_getOnChangeFunction() {
                                        $('<%= InventorySalesSummary_TimePeriod.ClientID %>').onchange = function () {
                                            $('<%= InventorySalesSummary_TimePeriod.ClientID %>').style.display = 'none';
                                            InventorySalesSummary_savedFunction();
                                            return true;
                                        }
                                    }
                                </script>
                                <div class="loading">
                                <asp:Image ID="BehindTheNumbersProgressWheelImage" SkinID="ProgressWheel" runat="server"/>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress> 
                    <asp:DropDownList
                        ID="InventorySalesSummary_TimePeriod"
                        DataSourceID="InventorySalesSummary_TimePeriod_DataSource"
                        DataTextField="Name"
                        Width="230px"
                        DataValueField="Value"
                        AutoPostBack="true"
                        OnPreRender="InventorySalesSummary_TimePeriod_PreRender"
                        runat="server">
                    </asp:DropDownList>
                    </div>

                    <!-- inventory sales -->
                    <h2 class="inventoryTableHeader">Behind The Numbers
                    
                    
                    </h2>
                    <cwc:AsyncObjectDataSourceControl
                        ID="InventorySalesSummary_DataSource"
                        runat="server"
                        TypeName="FirstLook.CommandCenter.DomainModel.Service.InventorySalesSummaryGaugeDataService">
                        <Parameters>
                            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                            <owc:MemberParameter Name="member" Type="Object" />
                            <asp:ControlParameter ControlID="InventorySalesSummary_TimePeriod" Name="timePeriodId" Type="String" />
                        </Parameters>
                    </cwc:AsyncObjectDataSourceControl>
                    <div class="inventoryTable">
                    <asp:GridView ID="InventorySalesSummary_GridView" runat="server" AutoGenerateColumns="false" DataSourceID="InventorySalesSummary_DataSource" UseAccessibleHeader="true" OnRowDataBound="InventorySalesSummary_GridView_RowDataBound" OnRowCreated="InventorySalesSummary_GridView_RowCreated">
                        <Columns>
                            <asp:HyperLinkField
                                AccessibleHeaderText="Sweet Spot (DAY 1-30)"
                                HeaderText="Sweet Spot<br /><dfn>(DAY 1-30)</dfn>"
                                DataNavigateUrlFields="TimePeriodId"
                                DataNavigateUrlFormatString="InventorySalesSummary.aspx?timePeriodId={0}"
                                DataTextField="SalesBeforeDay30"
                                DataTextFormatString="{0:0%}" />
                            <asp:HyperLinkField
                                AccessibleHeaderText="Retail Sales Efficiency (DAY 1-60)"
                                HeaderText="Retail Sales<br />Efficiency<br /><dfn>(DAY 1-60)</dfn>"
                                DataNavigateUrlFields="TimePeriodId"
                                DataNavigateUrlFormatString="InventorySalesSummary.aspx?timePeriodId={0}"
                                DataTextField="SalesBeforeDay60"
                                DataTextFormatString="{0:0%}" />
                            <asp:TemplateField
                                HeaderText="Retail Gross Profit<br />Give Back From<br />Aged Wholesale Loss">
                                <ItemTemplate>
                                    <a href='InventorySalesSummary.aspx?timePeriodId=<%#Eval("TimePeriodId")%>' title="Percentage in dollars"><%#Eval("RetailGrossProfitGiveBackPercent", "{0:0%}")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:HyperLinkField
                                AccessibleHeaderText="MergeColumnHeaderCell"
                                HeaderText="MergeColumnHeaderCell"
                                DataNavigateUrlFields="TimePeriodId"
                                DataNavigateUrlFormatString="InventorySalesSummary.aspx?timePeriodId={0}"
                                DataTextField="RetailGrossProfitGiveBackDollars"
                        DataTextFormatString="{0:C0}" />
                            <asp:HyperLinkField
                                AccessibleHeaderText="Avg. Front End Gross"
                                HeaderText="Avg. Front<br />End Gross"
                                DataNavigateUrlFields="TimePeriodId"
                                DataNavigateUrlFormatString="InventorySalesSummary.aspx?timePeriodId={0}"
                                DataTextField="AvgFrontEndGross"
                        DataTextFormatString="{0:C0}" />
                            <asp:HyperLinkField
                                AccessibleHeaderText="Avg. Immediate Wholesale Profit/Loss"
                                HeaderText="Avg. Immediate<br /> Wholesale<br /> Profit/Loss"
                                DataNavigateUrlFields="TimePeriodId"
                                DataNavigateUrlFormatString="InventorySalesSummary.aspx?timePeriodId={0}"
                                DataTextField="AvgImmWholesaleLoss"
                        DataTextFormatString="{0:C0}" />
                            <asp:HyperLinkField
                                AccessibleHeaderText="True Retail Avg. Gross Profit (PER RETAIL UNIT STOCKED/SOLD)"
                                HeaderText="True Retail Avg.<br />Gross Profit<br /><dfn>(PER RETAIL UNIT<br />STOCKED/SOLD)</dfn>"
                                DataNavigateUrlFields="TimePeriodId"
                                DataNavigateUrlFormatString="InventorySalesSummary.aspx?timePeriodId={0}"
                                DataTextField="TrueRetailAvgGross"
                        DataTextFormatString="{0:C0}" />
                        </Columns>
                    </asp:GridView>
                    </div>
                    
                    
                    <h2 class="inventoryTableHeader">Inventory Pricing Risk</h2>
                    <cwc:AsyncObjectDataSourceControl
                        ID="InventoryPricingRiskSummary_DataSource"
                        runat="server"
                        TypeName="FirstLook.CommandCenter.DomainModel.Service.InventoryPricingRiskSummaryDataService">
                        <Parameters>
                            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                            <owc:MemberParameter Name="member" Type="Object" />
                            <asp:Parameter DefaultValue="1" Name="mode" Type="Int32" />
                        </Parameters>
                    </cwc:AsyncObjectDataSourceControl>
                    <div class="inventoryTable">
                    <asp:GridView ID="InventoryPricingRisk_GridView" runat="server" AutoGenerateColumns="false" DataSourceID="InventoryPricingRiskSummary_DataSource" UseAccessibleHeader="true">
                        <Columns>
                            <asp:TemplateField  AccessibleHeaderText="Potential Underpricing Risk"
                                                HeaderText="Potential<br />Underpricing Risk">
                                <ItemTemplate>
                                    <a href="InventoryPricingRisk.aspx?RiskIndex=1" title="Potential Underpricing Risk" target="_InventoryPricingRisk"><%# Eval("Percentage_UnderpricingRisk", "{0:0%}") %><br />(<%# Eval("Units_UnderpricingRisk")%>Vehicles)</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  AccessibleHeaderText="Priced at Market"
                                                HeaderText="Priced<br />at Market">
                                <ItemTemplate>
                                    <a href="InventoryPricingRisk.aspx?RiskIndex=2" title="Priced at Market" target="_InventoryPricingRisk"><%# Eval("Percentage_PricedToMarket", "{0:0%}") %><br />(<%# Eval("Units_PricedToMarket")%>Vehicles)</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  AccessibleHeaderText="Potential Overpricing Risk"
                                                HeaderText="Potential<br />Overpricing Risk">
                                <ItemTemplate>
                                    <a href="InventoryPricingRisk.aspx?RiskIndex=3" title="Potential Overpricing Risk" target="_InventoryPricingRisk"><%# Eval("Percentage_OverpricingRisk", "{0:0%}") %><br />(<%# Eval("Units_OverpricingRisk")%>Vehicles)</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  AccessibleHeaderText="Zero/No Price Vehicles"
                                                HeaderText="Zero/No Price<br />Vehicles">
                                <ItemTemplate>
                                    <a href="InventoryPricingRisk.aspx?RiskIndex=4" title="Zero/No Price Vehicles" target="_InventoryPricingRisk"><%# Eval("Percentage_NoPrice", "{0:0%}") %><br />(<%# Eval("Units_NoPrice")%>Vehicles)</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  AccessibleHeaderText="Not Analyzed"
                                                HeaderText="Not Analyzed">
                                <ItemTemplate>
                                    <a href="InventoryPricingRisk.aspx?RiskIndex=5" title="Not Analyzed" target="_InventoryPricingRisk"><%# Eval("Percentage_NotAnalyzed", "{0:0%}")%><br />(<%# Eval("Units_NotAnalyzed")%>Vehicles)</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    </div>

                    
                </div>
                <div id="maxcolumn" class="columnitem">
                    <asp:Panel ID="MaxGroupDashboardTile" CssClass="MaxGroupDashboardTile" runat="server">
                        <h2 class="inventoryTableHeader"><img src="App_Themes/Leopard/Images/max.png" alt="MAX" /> INSIGHTS Group Dashboard</h2>
                        <div class="inventoryTable">
                            <table class="maxTable">
                                <tr>
                                    <td colspan="2"><h3>Click Here to View Alerts for Analysis of</h3></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="AnalysisLinkContainer Left">
                                            <a href="/merchandising/GroupDashboard.aspx">Gaps in your<br />Online Inventory</a>
                                        </div>
                                        <div class="AnalysisLinkContainer Right">
                                            <a href="/merchandising/GroupDashboard.aspx">Your Online<br />Vendor Performance</a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="group_reports columnitem">
            <reports:DefinitionList id="InventoryManagement" runat="server"></reports:DefinitionList>
        </div>
    </div>
    
    </div>
    <div class="c_"  id="purchasing">
        <!-- optimal inventory -->
        <cwc:AsyncObjectDataSourceControl
            ID="OptimalInventoryGauge_DataSource"
            runat="server"
            TypeName="FirstLook.CommandCenter.DomainModel.Service.OptimalInventoryGaugeDataService">
            <Parameters>
                <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
                <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
                <owc:MemberParameter Name="member" Type="Object" />
            </Parameters>
        </cwc:AsyncObjectDataSourceControl>
        <%--<inventory:OptimalInventoryGauge runat="server" />--%>
         <asp:formview ID="OptimalInventoryGaugeData" runat="server" DataSourceID="OptimalInventoryGauge_DataSource" OnDataBound="OptimalInventoryGauge_DataBound">
         <ItemTemplate>
             <div id="core-inventory-graph-div">
                 <div id="core-inventory-graph-arc">
                     <img id="core-inventory-graph-arc-image" src="Static/images/small-graph-arc.gif" />
                 </div>
                 <div id="core-inventory-graph-body" class="shadow">
                 </div>
                 <div id="core-inventory-graph-closing-rate">
                     <div id="core-inventory-graph-closing-rate-div1">
                         Core Inventory
                     </div>
                     <div id="core-inventory-graph-closing-rate-div2">
                         <span id="core-inventory-graph-closing-rate-total"><%# DataBinder.Eval(Container.DataItem, "Out of Stock", "{0:0}") %></span> Units Out of Stock
                     </div>
                     <div id="core-inventory-graph-tooltip-container">
                         <div id="core-inventory-graph-tooltip">
                         </div>
                     </div>
                 </div>
                 <div class="core-inventory-graph-baroverlay" id="core-inventory-graph-baroverlay0"
                     idx="0">
                 </div>
                 <div class="core-inventory-graph-baroverlay" id="core-inventory-graph-baroverlay1"
                     idx="1">
                 </div>
                 <div class="core-inventory-graph-baroverlay" id="core-inventory-graph-baroverlay2"
                     idx="2">
                 </div>
             </div>
         </ItemTemplate>
       </asp:formview>
        
        <div class="group_reports columnitem">
            <reports:DefinitionList id="WholesaleBuyingAndSelling" runat="server"></reports:DefinitionList>
        </div>
    </div>
   
    <div id="body_b" class="split_">
    <%--<div class="view_options">--%>
    <div class="view_optionsGroup">
            
        <h4 class="dyna_link em_link hide_nonJS" id="ShowDealerShipReports">DEALER REPORTS<asp:Image ID="Image2" SkinID="MenuIcon" runat="server"/></h4>
        
    </div>
    <div id="dealer_reports" style="display:none;">
        <div id="closeDealerShipReports" class="closebox right"></div>
        <h2 id="reportselector">
            <reports:SelectDealerReportsView id="SelectDealerReports" runat="server"></reports:SelectDealerReportsView>
        </h2>
        <reports:DealerShipReports id="DealerShipReports1" runat="server"></reports:DealerShipReports>
    </div>
    </div>
        
</asp:content>
