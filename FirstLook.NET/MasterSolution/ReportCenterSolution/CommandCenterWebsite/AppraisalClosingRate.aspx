<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AppraisalClosingRate.aspx.cs" MasterPageFile="~/Master_Pages/Page_Popup.master" Inherits="Home_AppraisalClosingRate" %>

<%@ Register TagPrefix="firstlook" TagName="Email" Src="~/Controls/Common/Email.ascx" %>

<asp:Content ID="PopupHeader" ContentPlaceHolderID="Page_Header" runat="server">
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <firstlook:Email ID="EmailClient" OnSend="EmailClient_Send" Subject="Appraisal Closing Rate" runat="server" />
        <h2><asp:Literal ID="DealerGroup" runat="server" OnInit="DealerGroup_Init"></asp:Literal></h2>
        <h1><asp:Literal ID="ChartTitle" runat="server" OnInit="ChartTitle_Init"></asp:Literal></h1>
    </div>
</asp:Content>

<asp:Content ID="PopupContent" ContentPlaceHolderID="Page_Body" runat="Server">
    <h1 id="graphheader"><asp:Literal ID="GraphTitle" runat="server"></asp:Literal></h1>
    
    <!-- appraisal time period -->
    <cwc:AsyncObjectDataSourceControl
        ID="AppraisalClosingRate_TimePeriod_DataSource"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.TimePeriodDataService"
        runat="server">
    </cwc:AsyncObjectDataSourceControl>
    
    <span id="DrilldownTimeSpan" class="dyna_link">
        <asp:Label ID="AppraisalClosingRate_TimePeriodLabel" runat="server" />
        <asp:Image ID="Image1" SkinID="MenuIcon" CssClass="hide_nonJS" runat="server"/>
    </span>
    <div id="selectDrilldownTimeRange" class="inlinepopup selectbox" style="display:none;">
        <div id="closeDrilldownTimeRange" class="closebox right"></div>
        <h3>Time Period for Appraisal Closing Rate:</h3>
        <asp:DropDownList
            ID="AppraisalClosingRate_TimePeriod"
            DataSourceID="AppraisalClosingRate_TimePeriod_DataSource"
            DataTextField="Name"
            DataValueField="Value"
            OnSelectedIndexChanged="AppraisalClosingRate_TimePeriod_SelectedIndexChanged"
            AutoPostBack="true"
            runat="server">
        </asp:DropDownList>
    </div>
    
    <!-- appraisal report -->
    
    <cwc:AsyncObjectDataSourceControl
        ID="AppraisalClosingRate_DataSource"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.AppraisalClosingRateTableDataService"
        runat="server">
        <Parameters>
            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
            <owc:DealerSelectionParameter Name="dealers" Type="Object" />
            <owc:MemberParameter Name="member" Type="Object" />
            <asp:ControlParameter ControlID="AppraisalClosingRate_TimePeriod" Name="timePeriodId" Type="String" />
        </Parameters>
    </cwc:AsyncObjectDataSourceControl>
    
    <asp:GridView ID="AppraisalClosingRate" CssClass="popuptable" runat="server" DataSourceID="AppraisalClosingRate_DataSource" OnInit="AppraisalClosingRate_Init" OnRowDataBound="AppraisalClosingRate_RowDataBound" AutoGenerateColumns="False" UseAccessibleHeader="true" AllowSorting="true" GridLines="None" CellSpacing="-1" ShowFooter="true">
        <Columns>
            <asp:BoundField DataField="ClosingRateRank" HeaderText="In-Group Rank" SortExpression="ClosingRateRank" />
            <asp:BoundField DataField="Dealer" HeaderText="Dealership" SortExpression="Dealer" />
            <asp:TemplateField HeaderText="Appraisal Closing Rate / Target" SortExpression="ClosingRate" >
                <ItemTemplate>
                    
                        <ul class="metric">
                        <li class="targetbar" style="width: <%# 60 * 1.4 %>px"></li>
                        <li class="targetlegend">target: 60%</li>
                            <li>
                                <ul class="bargraph">
                                <li><a href="DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=<%#Eval("Dealer_ID")%>"><span  class="percentbar dyna_link" style="width: <%# IsNull(Eval("ClosingRate")) ? 0.0 : ((double) Eval("ClosingRate") * 100.0 * 1.4) %>px" ></span></a></li>
                                <li class="percentlegend"><a href="DealerGroupReport.aspx?Id=D0D7E0DD-B2BB-4851-AC07-E0F99ABBD27E&amp;ContextCode=<%#Eval("Dealer_ID")%>"><%# Eval("ClosingRate", "{0:p0}").ToString().Trim().Equals(string.Empty) ? "0%" : Eval("ClosingRate", "{0:p0}") %></a></li>
                                </ul>
                            </li>
                        </ul>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TradeInInventoryAnalyzed" HeaderText="Trade Analyzer Usage" SortExpression="TradeInInventoryAnalyzed" DataFormatString="{0:0%}" HtmlEncode="false"/>
            <asp:HyperLinkField  HeaderText="Avg. Imm. Wholesale Profit/Loss" DataTextField="AvgImmediateWholesaleProfitInPeriod" DataTextFormatString="{0:C0}" SortExpression="AvgImmediateWholesaleProfitInPeriod" 
                             DataNavigateUrlFields="Dealer_ID" DataNavigateUrlFormatString="~/DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;ContextCode={0}" />
        </Columns>
    </asp:GridView>
</asp:Content>
