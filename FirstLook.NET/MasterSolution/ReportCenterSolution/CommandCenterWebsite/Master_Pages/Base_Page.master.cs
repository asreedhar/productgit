using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;

public partial class Master_Pages_Base_Page : MasterPage
{
    protected void DealerStoreNickname_Init(object sender, EventArgs e)
    {
        SoftwareSystemComponentState state = (SoftwareSystemComponentState) Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];
        
        if( state !=null)
        {
            // setup the dealer or dealer-group name

            if (state.SoftwareSystemComponent.GetValue().Token.Equals(SoftwareSystemComponentStateFacade.DealerComponentToken))
            {
                //DealerStoreNickname.Text = HttpUtility.HtmlEncode(state.Dealer.GetValue().Name);
            }
            else
            {
                //DealerStoreNickname.Text = HttpUtility.HtmlEncode(state.DealerGroup.GetValue().Name);
            }

        }
    }

}
