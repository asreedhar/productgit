﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;

public partial class Master_Pages_Base_NewReport : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Footer.Controls.Add(new PageFooter());
        CopyrightYear.Text = DateTime.Now.Year.ToString();

        //***** Case id:30351 : Standerdized header on PMR reports only
        if (Request.QueryString.ToString().Contains("pmr"))
        {
            if (Request.Path.ToString().Contains("DealerGroupNewReport"))
            {
                SiteMenu1.FindControl("backLink").Visible = true;
                this.PMRLabel.Visible = true;
                SiteMenu1.backButtonPostBackUrl = "/command_center/PerformanceManagementCenter.aspx";
            }
            else if (Request.Path.ToString().Contains("DealerLevelGroupPage"))
            {
                SiteMenu1.FindControl("backLink").Visible = true;
                this.PMRLabel.Visible = true;
                SiteMenu1.backButtonPostBackUrl = "/command_center/DealerReports.aspx";
            }
        }
        else
        {
            SiteMenu1.FindControl("backLink").Visible = false;
            this.PMRLabel.Visible = false; 
        }
        //********
    }
}
