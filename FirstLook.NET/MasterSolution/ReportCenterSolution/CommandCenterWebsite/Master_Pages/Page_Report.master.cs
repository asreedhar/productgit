using System;

using System.Web.UI.HtmlControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.Web;

public partial class Master_Pages_Page_Report : System.Web.UI.MasterPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (PageFooter.Controls.Contains(CopyrightYear))
        {
            CopyrightYear.Text = DateTime.Now.Year.ToString();
        }
    }
    protected void ViewerCssLink_PreRender(object sender, EventArgs e)
    {
        ((HtmlGenericControl)sender).Attributes["href"] = ResolveClientUrl("~/App_Themes/" + Page.StyleSheetTheme + "/" + ((HtmlGenericControl)sender).Attributes["href"]);

    }
 
                
}
