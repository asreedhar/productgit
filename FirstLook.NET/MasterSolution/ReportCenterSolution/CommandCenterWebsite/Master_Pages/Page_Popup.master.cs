using System;
using System.Data;
using FirstLook.DomainModel.Oltp;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Master_Pages_Popup : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentDateTime.Text = "Printed: " + String.Format("{0:g}", DateTime.Now);
        CurrentUser.Text = "Printed by: " + 
            ((SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]).AuthorizedMember.GetValue().FirstName
        + " " + ((SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]).AuthorizedMember.GetValue().LastName;
        CopyrightNotice.Text = "Copyright &copy; " + DateTime.Now.Year.ToString() + " First Look, LLC. All Rights Reserved";
    }

    protected void PopupCssLink_PreRender(object sender, EventArgs e)
    {
        ((HtmlGenericControl)sender).Attributes["href"] = ResolveClientUrl("~/App_Themes/" + Page.StyleSheetTheme + "/" + ((HtmlGenericControl)sender).Attributes["href"]);
       
    }
}
