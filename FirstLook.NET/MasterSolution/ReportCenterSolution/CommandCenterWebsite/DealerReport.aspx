<%@ Page Language="C#" MasterPageFile="~/Master_Pages/Page_Report.master" AutoEventWireup="true" CodeFile="DealerReport.aspx.cs" Inherits="Reports_DealerReport" Title="Pre-Owned Performance Management Center" ValidateRequest="false" EnableEventValidation="false"%>

<%@ Register TagPrefix="reports" TagName="ReportControl" Src="~/Controls/Reports/ReportControl.ascx" %>

<asp:content ID="DealerReport_Page_Scripts" contentplaceholderid="Page_Scripts" runat="server">
    <script type="text/javascript" src="Static/Scripts/viewer.js"></script>
</asp:content>

<asp:Content ID="DealerReport_Page_Body" ContentPlaceHolderID="PageBody" runat="Server">
                <span class="right">
					<span id="homelink" style="display: none"><a id="A1" href="~/DealerReports.aspx" runat="server">Home</a> | </span>
					<span id="backlink" style="display: none;"><a href="javascript:history.back()">Back</a> | </span>
					<span id="closelink" style="display: none"><a href="javascript:window.close();">Close window</a></span>
                </span>
                <asp:Image runat="server" CssClass="left" SkinID="LogoFirstlook" ID="Logo" />
    <reports:ReportControl ID="ReportControl1" runat="server" OnDrillThrough="ReportViewer_Drillthrough"/>
</asp:Content>
