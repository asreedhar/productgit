<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CurrentInventoryBreakdown.aspx.cs" MasterPageFile="~/Master_Pages/Page_Popup.master" Inherits="Home_CurrentInventoryBreakdown"%>

<%@ Register TagPrefix="inventory" TagName="CurrentInventoryBreakdownChart" Src="~/Controls/Inventory/CurrentInventoryBreakdownChart.ascx" %>
<%@ Register TagPrefix="firstlook" TagName="Email" Src="~/Controls/Common/Email.ascx" %>

<asp:Content ID="PopupHeader" ContentPlaceHolderID="Page_Header" runat="server">
    <div id="pageheader">
        <a href="#" onclick="window.close();" class="close hide_nonJS"> Close Window</a><br />
        <firstlook:Email ID="Email2" OnSend="EmailClient_Send" Subject="Current Inventory Breakdown" runat="server" />
        <h2><asp:Literal ID="DealerGroup" runat="server" OnInit="DealerGroup_Init"></asp:Literal></h2>
        <h1><asp:Literal ID="ChartTitle" runat="server" OnInit="ChartTitle_Init"></asp:Literal></h1>
    </div>
</asp:Content>

<asp:Content ID="PopupContent" ContentPlaceHolderID="Page_Body" runat="Server">
    
    <div id="inventory-graph" class="cib">
        <inventory:CurrentInventoryBreakdownChart OnModeToggle="CurrentInventoryBreakdownChart_ModeToggle" runat="server" />
        <asp:Label ID="CurrentInventoryBreakdownChartLegendPercent" CssClass="inventory-graph-legend" runat="server" Visible="true" Text="Water: Unit Cost less Book Value.<br />% Inventory: Percentage of In-Group units in each bucket." />
        <asp:Label ID="CurrentInventoryBreakdownChartLegendDollar" CssClass="inventory-graph-legend" runat="server" Visible="false" Text="Water: Unit Cost less Book value.<br />Inventory $: Total dollars in each bucket." />
    </div>

    <h1 id="graphheader">
    <asp:Literal ID="GraphTitle1" runat="server" Visible="false" Text="0-29 Day Old Red and Yellow Light Inventory" />
    <asp:Literal ID="GraphTitle2" runat="server" Visible="false" Text="0-29 Day Old Green Light Inventory" />
    <asp:Literal ID="GraphTitle3" runat="server" Visible="false" Text="30-39 Day Old Inventory" />
    <asp:Literal ID="GraphTitle4" runat="server" Visible="false" Text="40-49 Day Old Inventory" />
    <asp:Literal ID="GraphTitle5" runat="server" Visible="false" Text="50-59 Day Old Inventory" />
    <asp:Literal ID="GraphTitle6" runat="server" Visible="false" Text="60+ Day Old Inventory" />
    </h1>
    
    <asp:UpdatePanel ID="CurrentInventoryBreakdownDataGridUpdatePanel" UpdateMode="Conditional" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CurrentInventoryReport" EventName="Sorting"/>
        </Triggers>
        <ContentTemplate>
    <cwc:AsyncObjectDataSourceControl
        ID="CurrentInventoryReport_DataSource"
        runat="server"
        TypeName="FirstLook.CommandCenter.DomainModel.Service.CurrentInventoryBreakdownTableDataService">
        <Parameters>
            <owc:DealerGroupParameter Name="dealerGroup" Type="Object" />
            <owc:DealerSelectionParameter Name="dealers" Type="Object" MemberBusinessUnitSetType="CommandCenterDealerGroupSelection" />
            <owc:MemberParameter Name="member" Type="Object" />
            <asp:QueryStringParameter Name="bucketIndex" Type="Int32" QueryStringField="bucketIndex" DefaultValue="0" />
            <asp:Parameter Name="isPercentMode" Type="Boolean" DefaultValue="true" />
        </Parameters>
    </cwc:AsyncObjectDataSourceControl>
    <asp:HiddenField ID="PercentageOfInventorySortDirection" Value="Desc" runat="server" />
    <asp:HiddenField ID="PercentBarRiskToggle" Value="HideRisk" runat="server" />
    <input type="hidden" id="PercentBarRiskToggleClientID" value="<%= PercentBarRiskToggle.ClientID %>" />
        
        <asp:GridView ID="CurrentInventoryReport" DataSourceID="CurrentInventoryReport_DataSource" CssClass="popuptable" runat="server" AutoGenerateColumns="False" UseAccessibleHeader="true" AllowSorting="true" CellSpacing="-1" GridLines="None" ShowFooter="true"
            OnRowDataBound="CurrentInventoryReport_RowDataBound" 
            OnLoad="CurrentInventoryReport_Load" >
        <Columns>
            <asp:BoundField DataField="Dealer" HeaderText="Dealership" SortExpression="Dealer"  />
            
            <%-- Inventory Units % Mode --%>
                <asp:BoundField DataField="Inventory_Count_Percentage_Rank" HeaderText="In-Group Rank" SortExpression="Inventory_Count_Percentage_Rank" />
            
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:LinkButton ID="PercentageOfInventoryLink" runat="server" onclick="PercentageOfInventoryLink_Click">% Inventory</asp:LinkButton>
                    <span class="dyna_link toggleriskdisplay hide_nonJS">(Show Risk)</span>
                </HeaderTemplate>
                <ItemTemplate>
                    <ul class="metric dyna_link" onclick="navigateTo('EnterDealership.aspx?dealerId=<%# Eval("Dealer_ID") %>&amp;application=planning');">
                        <li>
                                    <%--  the following line is invalid xhtml, since ids cannot contain spaces. will fix when i have time. -tb --%>
                            <ul class="percentbar risk">
                                <li class="percentbar highriskbar" title="<%# String.Format("{0:N0}",Eval("Red_Light_Inventory_Count_Percentage"))%>% high risk" alt="<%# String.Format("{0:N0}",Eval("Red_Light_Inventory_Count_Percentage"))%>% high risk"><asp:Panel CssClass="percentbar highriskbar" id="HighRiskBar" runat="server"></asp:Panel></li>
                                <li class="percentbar mediumriskbar" title="<%# String.Format("{0:N0}",Eval("Yellow_Light_Inventory_Count_Percentage"))%>% medium risk" alt="<%# String.Format("{0:N0}",Eval("Yellow_Light_Inventory_Count_Percentage"))%>% medium risk"><asp:Panel id="MediumRiskBar" runat="server"></asp:Panel></li>
                                <li class="percentbar lowriskbar" title="<%# String.Format("{0:N0}",Eval("Green_Light_Inventory_Count_Percentage"))%>% low risk" alt="<%# String.Format("{0:N0}",Eval("Green_Light_Inventory_Count_Percentage"))%>% low risk"><asp:Panel id="LowRiskBar" runat="server"></asp:Panel></li>
                            </ul>
                        </li>
                        <li class="percentlegend"><%# Eval("Inventory_Count_Percentage", "{0:0}") %>%</li>
                    </ul>
                </ItemTemplate>
            </asp:TemplateField>
            
            <%-- Inventory $ Mode --%>
                <asp:BoundField DataField="Unit_Cost_Percentage_Rank" HeaderText="In-Group Rank" SortExpression="Unit_Cost_Percentage_Percentage_Rank" />
            <asp:TemplateField HeaderText="Unit_Cost_Percentage" SortExpression="Unit_Cost_Percentage">
                <HeaderTemplate>
                    <asp:LinkButton ID="PercentageOfInventoryDollarsLink" runat="server" onclick="PercentageOfInventoryLink_Click">% Inventory $</asp:LinkButton>
                    <span class="dyna_link toggleriskdisplay hide_nonJS">(Show Risk)</span>
                </HeaderTemplate>
                <ItemTemplate>
                    <ul class="metric dyna_link"  onclick="navigateTo('EnterDealership.aspx?dealerId=<%# Eval("Dealer_ID") %>&amp;application=planning');">
                        <li>
                            <ul class="percentbar risk">
                                <li class="percentbar highriskbar" title="<%# String.Format("{0:N0}",Eval("Red_Light_Unit_Cost_Percentage"))%>% high risk" alt="<%# String.Format("{0:N0}",Eval("Red_Light_Unit_Cost_Percentage"))%>% high risk"><asp:Panel CssClass="percentbar highriskbar" id="DollarsHighRiskBar" runat="server"></asp:Panel></li>
                                <li class="percentbar mediumriskbar" title="<%# String.Format("{0:N0}",Eval("Yellow_Light_Unit_Cost_Percentage"))%>% medium risk" alt="<%# String.Format("{0:N0}",Eval("Yellow_Light_Unit_Cost_Percentage"))%>% medium risk"><asp:Panel id="DollarsMediumRiskBar" runat="server"></asp:Panel></li>
                                <li class="percentbar lowriskbar" title="<%# String.Format("{0:N0}",Eval("Green_Light_Unit_Cost_Percentage"))%>% low risk" alt="<%# String.Format("{0:N0}",Eval("Green_Light_Unit_Cost_Percentage"))%>% low risk"><asp:Panel id="DollarsLowRiskBar" runat="server"></asp:Panel></li>
                            </ul>
                        </li>
                        <li class="percentlegend"><%# Eval("Unit_Cost_Percentage", "{0:0}")%>%</li>
                    </ul>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:BoundField DataField="Unit_Cost" HeaderText="Inventory $" SortExpression="Unit_Cost" DataFormatString="{0:C0}" HtmlEncode="false" />
                <asp:BoundField DataField="Inventory_Count" HeaderText="Vehicle Count" DataFormatString="{0:N0}" HtmlEncode="False" SortExpression="Inventory_Count" />
                <asp:BoundField DataField="Average_Cost_Per_Unit" HeaderText="Avg $ per Unit" DataFormatString="{0:C0}" HtmlEncode="False" SortExpression="Average_Cost_Per_Unit" />
                
                <asp:BoundField DataField="BookVsCost_Rank" HeaderText="In-Group Rank" SortExpression="BookVsCost_Rank" />            
                <asp:HyperLinkField HeaderText="Book vs Cost" DataTextField="BookVsCost" DataTextFormatString="{0:C0}" SortExpression="BookVsCost" 
                         DataNavigateUrlFields="Dealer_ID" DataNavigateUrlFormatString="~/DealerGroupReport.aspx?Id=C5ABD675-96ED-4117-AB47-22DF072CF27F&amp;ContextCode={0}" />
        
        </Columns>
    </asp:GridView>
        
         </ContentTemplate>    
    </asp:UpdatePanel>
            
</asp:Content>
