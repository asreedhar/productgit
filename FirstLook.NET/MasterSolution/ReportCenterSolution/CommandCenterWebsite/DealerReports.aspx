<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DealerReports.aspx.cs" 
    Inherits="DealerReports" 
    MasterPageFile="~/Master_Pages/Base_Page.master" %>
<%@ Register TagPrefix="reports" TagName="DealershipReports" Src="~/Controls/Reports/DealershipReports.ascx" %>

<asp:content contentplaceholderid="HeadElements_Placeholder" runat="server">
    <script src="Static/Scripts/home.js" type="text/javascript"></script>
</asp:content>


<asp:content ID="Dealer_Reports" contentplaceholderid="Body_A_PlaceHolder" runat="server">
    <div id="container">
    <reports:DealershipReports id="DealerShipReports1" runat="server"></reports:DealershipReports>
    </div>
</asp:content>
