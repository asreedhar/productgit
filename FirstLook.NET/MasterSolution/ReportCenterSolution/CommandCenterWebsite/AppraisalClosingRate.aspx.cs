using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using FirstLook.CommandCenter.DomainModel.Service;

public partial class Home_AppraisalClosingRate : DealerGroupPage
{
    protected List<GridViewRow> footerRows;

    protected void DealerGroup_Init(object sender, EventArgs e)
    {
        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
    }

    protected void ChartTitle_Init(object sender, EventArgs e)
    {
        ChartTitle.Text = "Appraisal Closing Rates";
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        base.IsAnalyicsEnabled = true;
        Page.Title = "Appraisal Closing Rates";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string timePeriod = Request.QueryString["timePeriodId"];

        if (AppraisalClosingRate_TimePeriod.SelectedValue != timePeriod && !"".Equals(AppraisalClosingRate_TimePeriod.SelectedValue))
        {
            timePeriod = AppraisalClosingRate_TimePeriod.SelectedValue;
        }
        AppraisalClosingRate_TimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(timePeriod, true);
        AppraisalClosingRate_TimePeriod.SelectedValue = timePeriod;
    }

    protected void AppraisalClosingRate_Init(object sender, EventArgs e)
    {
        footerRows = new List<GridViewRow>();
    }
    protected void AppraisalClosingRate_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].CssClass += " first";

            if ((e.Row.Cells[4].Controls[0] as HyperLink).Text.Contains("("))
            {
                e.Row.Cells[4].CssClass += " negativenumber";
            }

            int[] columnsToHighlight = {};
            GridViewUtilities.HighlightCells(e.Row, columnsToHighlight);


            if ((int)((DataRowView)e.Row.DataItem)["DrilldownRowType"] != 0)
            {
                e.Row.Cells[0].Text = "-";
                footerRows.Add(e.Row);
                e.Row.Visible = false;
            }
        }
        if (e.Row.RowType.Equals(DataControlRowType.Footer))
        {
            foreach (GridViewRow footerRow in footerRows)
            {
                GridViewRow r = GridViewUtilities.CopyRow(footerRow, DataControlRowType.Footer, AppraisalClosingRate);
                e.Row.Parent.Controls.Add(r);
            }
            e.Row.Visible = false;
        }
    }

    protected void AppraisalClosingRate_TimePeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedIndex = ((DropDownList)sender).SelectedValue;
        AppraisalClosingRate_TimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(selectedIndex, true);
    }
}
