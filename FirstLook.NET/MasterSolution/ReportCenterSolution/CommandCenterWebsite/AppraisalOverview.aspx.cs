using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using FirstLook.CommandCenter.DomainModel.ObjectModel;
using FirstLook.CommandCenter.DomainModel.Service;

public partial class Home_AppraisalOverview : DealerGroupPage
{
    // (width of table cell) - LegendReservedMargin = WidthReservedForGraph.
    protected static int WidthReservedForGraph = 230;
    protected static int LegendReservedMargin = 65;
    
    // instance vars
    protected double Range;
    protected double Normalizer;
    protected List<GridViewRow> footerRows;


    protected void DealerGroup_Init(object sender, EventArgs e)
    {
        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
    }

    protected void ChartTitle_Init(object sender, EventArgs e)
    {
        if (AppraisalOverviewMode.AvgImmWholesaleProfit.ToString().Equals(GetMode()))
        {
            ChartTitle.Text = "Average Immediate Wholesale Profit";
            Page.Title = "Average Immediate Wholesale Profit";
        }
        else
        {
            ChartTitle.Text = "Trade-In Inventory Analyzed";
            Page.Title = "Trade-In Inventory Analyzed";
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        base.IsAnalyicsEnabled = true;
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string timePeriod = Request.QueryString["timePeriodId"];
        if (AppraisalOverview_TimePeriod.SelectedValue != timePeriod && !"".Equals(AppraisalOverview_TimePeriod.SelectedValue) )
        {
            timePeriod = AppraisalOverview_TimePeriod.SelectedValue;
        }
        AppraisalOverview_TimePeriod.SelectedValue = timePeriod;
        AppraisalClosingRate_TimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(timePeriod, true);
    }

    protected void AppraisalOverviewGrid_Init(object sender, EventArgs e)
    {
        footerRows = new List<GridViewRow>();

        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;

        // 2 modes for the table - default to TradeInInventoryAnalyzed Mode
        if (AppraisalOverviewMode.AvgImmWholesaleProfit.ToString().Equals(GetMode()))
        {
            AppraisalOverviewGrid.Columns[0].Visible = false;
            AppraisalOverviewGrid.Columns[5].Visible = false;
            AppraisalOverviewGrid.Columns[6].Visible = false;
        }
        else
        {
            AppraisalOverviewGrid.Columns[1].Visible = false;
            AppraisalOverviewGrid.Columns[3].Visible = false;
            //AppraisalOverviewGrid.Columns[5].Visible = false;
        }
    }


    protected void AppraisalOverviewGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label abargraph;
        Label alegend;

        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.FindControl("WholesaleBar") != null)
        {
            double Min, Max, Avg;

            e.Row.Cells[3].CssClass = "bargraphcell";

            DataRowView rowView = (DataRowView)e.Row.DataItem;

            Min = (double)rowView["MinAvgImmediateWholesaleProfit"];
            Max = (double)rowView["MaxAvgImmediateWholesaleProfit"];
            Avg = (double)rowView["AvgImmediateWholesaleProfitInPeriod"];

            if (e.Row.RowIndex == 0)
            {

                if (Min >= 0)
                {
                    Range = Max;
                }
                else
                {
                    Range = Max - Min;
                }
                Normalizer = WidthReservedForGraph / Range;
            }

            double CalculatedWidth = (Avg * Normalizer);
            abargraph = (Label)e.Row.FindControl("WholesaleBar");
            alegend = (Label)e.Row.FindControl("WholesaleLegend");


            // if row is positive
            if (CalculatedWidth >= 0)
            {
                double MarginLeft;
                if (Min > 0)
                {
                    MarginLeft = 0;
                }
                else
                {
                    MarginLeft = -Min * Normalizer;
                    //MarginLeft += LegendReservedMargin;
                }
                
                abargraph.Style["margin-left"] = MarginLeft + "px";
            }

            // if row is negative
            else
            {
                double MarginLeft;

                MarginLeft = -Min * Normalizer;
                MarginLeft += CalculatedWidth;
                

                CalculatedWidth *= -1;
                abargraph.CssClass = "negativepercentbar dyna_link";
                abargraph.Style["margin-left"] = MarginLeft + "px";
                alegend.CssClass += " negativenumber";
            }

            abargraph.Style["width"] = CalculatedWidth + "px";

            int[] columnsToHighlight = {};
            GridViewUtilities.HighlightCells(e.Row, columnsToHighlight);

            if ((int)((DataRowView)e.Row.DataItem)["DrilldownRowType"] != 0)
            {
                e.Row.Cells[0].Text = "-";
                e.Row.Cells[1].Text = "-";
                footerRows.Add(e.Row);
                e.Row.Visible = false;
            }

        }
        if (e.Row.RowType.Equals(DataControlRowType.Footer))
        {
            GridViewUtilities.SortRowsByColumn(footerRows, 2);
            foreach (GridViewRow footerRow in footerRows)
            {
                GridViewRow r = GridViewUtilities.CopyRow(footerRow, DataControlRowType.Footer, AppraisalOverviewGrid);
                e.Row.Parent.Controls.Add(r);
            }
            e.Row.Visible = false;
        }

        if (AppraisalOverviewMode.TradeInInventoryAnalyzed.ToString().Equals(GetMode()))
        {
            // if there are negative numbers in the 7th column, add negative number stylin'.
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                if((e.Row.Cells[6].Controls[0] as HyperLink).Text.Contains("(")) {
                    e.Row.Cells[6].CssClass += " negativenumber";
                }
            }
        }   
    }

    protected string GetMode()
    {
        string mode = "TradeInInventoryAnalyzed";

        if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
        {
            mode = Request.QueryString["mode"];
        }

        return mode;
    }

    protected void AppraisalClosingRate_TimePeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedIndex = ((DropDownList)sender).SelectedValue;
        AppraisalClosingRate_TimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(selectedIndex, true);
    }
}
