using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using FirstLook.CommandCenter.DomainModel.Service;

public partial class Home_InventorySalesSummary : DealerGroupPage
{


    protected List<GridViewRow> footerRows;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        base.IsAnalyicsEnabled = true;
        Page.Title = "Behind the Numbers";
    }

    protected void DealerGroup_Init(object sender, EventArgs e)
    {
        DealerGroup.Text = SoftwareSystemComponentState.DealerGroup.GetValue().Name;
    }

    protected void ChartTitle_Init(object sender, EventArgs e)
    {
        ChartTitle.Text = "Behind the Numbers";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string timePeriod = Request.QueryString["timePeriodId"];
        if (InventorySalesSummary_TimePeriod.SelectedValue != timePeriod && !"".Equals(InventorySalesSummary_TimePeriod.SelectedValue))
        {
            timePeriod = InventorySalesSummary_TimePeriod.SelectedValue;
        }
        InventorySalesSummary_TimePeriod.SelectedValue = timePeriod;
        InventorySalesSummary_TimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(timePeriod, false);
    }

    protected void InventorySalesSummary_GridView_Init(object sender, EventArgs e)
    {
        footerRows = new List<GridViewRow>();
    }

    protected void InventorySalesSummary_GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int currentcell = 1;
        DataRowView thisRowDataView = (DataRowView)(e.Row.DataItem);
        
        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
        {
            while(currentcell < e.Row.Cells.Count)
            {
                if (e.Row.Cells[currentcell].Text.Contains("-") || e.Row.Cells[currentcell].Text.Contains("(") || (thisRowDataView != null && currentcell == 6 && ToDouble(thisRowDataView.Row.ItemArray[6]) < 0.0d))
                {
                    e.Row.Cells[currentcell].CssClass += " negativenumber";
                }
                currentcell++;
            }
            int[] columnsToHighlight = { 1, 2, 5, 6 };
            GridViewUtilities.HighlightCells(e.Row, columnsToHighlight);

            if ((int)((DataRowView)e.Row.DataItem)["DrilldownRowType"] != 0)
            {
                e.Row.Cells[1].Text = "-";
                e.Row.Cells[3].Text = "-";
                e.Row.Cells[5].Text = "-";
                footerRows.Add(e.Row);
                e.Row.Visible = false;
            }
        }
        else if (e.Row.RowType.Equals(DataControlRowType.Footer))
        {
            foreach (GridViewRow footerRow in footerRows)
            {
                GridViewRow r = GridViewUtilities.CopyRow(footerRow, DataControlRowType.Footer, InventorySalesSummary_GridView);
                e.Row.Parent.Controls.Add(r);
            }
            e.Row.Visible = false;
        }
    }

    protected void InventorySalesSummary_TimePeriodSelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedIndex = ((DropDownList)sender).SelectedValue;
        InventorySalesSummary_TimePeriodLabel.Text = TimePeriodDataService.FindTimePeriodDescriptionById(selectedIndex, false);

    }

    protected static string BuildGiveBackLink(object dealerId, object percentageLoss, object dollarLoss)
    {
        string id = dealerId as string;
        double ploss = percentageLoss is double ? Convert.ToDouble(percentageLoss) : 0.0d;
        double dloss = dollarLoss is double ? Convert.ToDouble(dollarLoss) : 0.0d;

        if (id != null && !string.Empty.Equals(id))
        {
            return string.Format("<a title=\"Percentage in dollars\" href=\"DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;DealerID={0}\">{1:0%}</a><br />"
                + "<a href=\"DealerGroupReport.aspx?Id=E6953D64-6F0C-49f6-94F3-6E6A39C06FD3&amp;DealerID={0}\">{2:C0}</a>", id, ploss, dloss);
        }
        else
        {
            return string.Format("{0:0%}<br />{1:C0}", ploss, dloss);
        }
    }
}
