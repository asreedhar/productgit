﻿
namespace FirstLook.Reports.GoogleAnalytics
{
    /// <summary>
    /// Summary description for IGAnalytics
    /// This have all the required methods and properties
    /// for Google Analytics implementation
    /// </summary>
    public interface IGAnalytics
    {
        string PageTitle { get; set; }
        bool IsAnalyicsEnabled { get; set; }
    }
}

