﻿using System.Configuration;
using System.IO;
using System.Reflection;
using System.Web.UI;


namespace FirstLook.Reports.GoogleAnalytics
{
    public static class GoogleAnalyticsPageExtension
    {
        public static void GetGoogleAnalyticsScript(this Page page)
        {
            string script;
            Assembly asm = Assembly.GetExecutingAssembly();
            using (
                TextReader textReader =
                    new StreamReader(asm.GetManifestResourceStream("FirstLook.Reports.GoogleAnalytics.GoogleAnalyticsScript.txt")))
            {
                script = textReader.ReadToEnd();
                //Format issue with script. Replacing manually
                //script = String.Format(script, ConfigurationManager.AppSettings["ga"]);
                script = script.Replace("{0}", ConfigurationManager.AppSettings["ga"]);

                if (!page.ClientScript.IsClientScriptBlockRegistered(typeof(Page), "googleAnalytics"))
                {
                    page.ClientScript.RegisterClientScriptBlock(page.GetType(), "googleAnalytics", script,true);
                }
            }
            
        }
    }
}
