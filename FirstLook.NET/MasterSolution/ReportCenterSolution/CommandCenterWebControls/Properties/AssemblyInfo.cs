﻿using System.Reflection;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CommandCenterWebControls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("CommandCenterWebControls")]

[assembly: TagPrefix("FirstLook.CommandCenter.WebControls", "pwc")]