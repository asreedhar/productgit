using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dundas.Gauges.WebControl;

namespace FirstLook.CommandCenter.WebControls
{
    public class XhtmlGaugeContainer : GaugeContainer
    {
        private bool centeredCircle = false;

        [DefaultValue("false"), Description("When true the MAP is a centered circle")]
        public bool CenteredCircle
        {
            get { return centeredCircle;  }
            set { centeredCircle = value; }
        }

        protected override void Render(HtmlTextWriter output)
        {
            StringBuilder buffer = new StringBuilder();
            
            XhtmlTextWriter html = new XhtmlTextWriter(new StringWriter(buffer));

            base.Render(html);

            string original = buffer.ToString();

            string replacement = original;

            // find the map and close the area tags

            Match map = Regex.Match(replacement, "<MAP NAME=\"([^\"]*)\">", RegexOptions.IgnoreCase);

            string usemap = null;

            if (map.Success)
            {
                usemap = map.Groups[1].Value;

                if (centeredCircle && Width.Type.Equals(UnitType.Pixel) && Width.Value > 0.0d)
                {
                    int radius = Convert.ToInt32(Width.Value) / 2;

                    Match match = new Regex("<AREA ([^>]*)>", RegexOptions.IgnoreCase).Match(original);

                    if (match.Success)
                    {
                        string area = match.Groups[0].Value;

                        Dictionary<string, string> entries = ParseAttributes(match.Groups[1].Value);

                        StringBuilder b = new StringBuilder();

                        b.Append("<area");

                        foreach (string key in entries.Keys)
                        {
                            b.Append(" ");

                            if (key.ToLower().Equals("shape"))
                            {
                                b.Append("shape=\"circle\"");
                            }
                            else if (key.ToLower().Equals("coords"))
                            {
                                b.Append(string.Format("coords=\"{0},{0},{0}\"", radius));
                            }
                            else
                            {
                                b.Append(key).Append("=\"").Append(entries[key]).Append("\"");
                            }
                        }

                        b.Append(" />");

                        replacement = original.Replace(area, b.ToString());
                    }
                }
                else
                {
                    replacement = Regex.Replace(original, "<AREA ([^>]*)>", "<area $1 />", RegexOptions.IgnoreCase);
                }
            }

            // find the div and build a replacement img tag

            Match m = new Regex("<div ([^>]*)></div>", RegexOptions.IgnoreCase).Match(replacement);

            Regex src = new Regex("src='([^']*)'", RegexOptions.IgnoreCase);

            string div = null;
            
            Dictionary<string,string> attrs = new Dictionary<string,string>();

            if (m.Success)
            {
                attrs = ParseAttributes(m.Groups[1].Value);

                if (attrs.ContainsKey("style"))
                {
                    Match s = src.Match(attrs["style"]);

                    if (s.Success)
                    {
                        div = m.Groups[0].Value;

                        attrs["src"] = s.Groups[1].Value;
                    }
                }
            }

            // replace the div with an img tag

            if (div != null)
            {
                string img;

                // if an email/print request, just rewrite the gauge as an image
                if ("true".Equals(Context.Items["EmailRequest"]) )
                {
                    img = string.Format(
                        "<img id=\"{0}\" name=\"{1}\" src=\"{2}\" width=\"{3}\" height=\"{4}\" usemap=\"#{5}\" />",
                        attrs["id"],
                        attrs["name"],
                        attrs["src"],
                        Width,
                        Height,
                        usemap);
                }
                else if (usemap != null)
                {
                    img = string.Format(
                        "<img id=\"{0}\" name=\"{1}\" src=\"{2}\" width=\"{3}\" height=\"{4}\" style=\"filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{5}');\" usemap=\"#{6}\" />",
                        attrs["id"],
                        attrs["name"],
                        ResolveUrl("~/App_Themes/" + Page.StyleSheetTheme + "/Images/transparent.gif"),
                        Width,
                        Height,
                        attrs["src"],
                        usemap);
                }
                else
                {
                    img = string.Format(
                        "<img id=\"{0}\" name=\"{1}\" src=\"{2}\" width=\"{3}\" height=\"{4}\" style=\"filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{5}');\" />",
                        attrs["id"],
                        attrs["name"],
                        ResolveUrl("~/App_Themes/" + Page.StyleSheetTheme + "/Images/transparent.gif"),
                        Width,
                        Height,
                        attrs["src"]);
                }
                
                replacement = replacement.Replace(div, img);
            }

            output.Write(replacement);
        }

        private static Dictionary<string,string> ParseAttributes(string text)
        {
            Dictionary<string, string> attrs = new Dictionary<string, string>();

            Match attr = new Regex("(\\w+)=\"([^\"]*)\"", RegexOptions.IgnoreCase).Match(text);

            while (attr.Success)
            {
                attrs[attr.Groups[1].Value] = attr.Groups[2].Value;

                attr = attr.NextMatch();
            }

            return attrs;
        }
    }
}