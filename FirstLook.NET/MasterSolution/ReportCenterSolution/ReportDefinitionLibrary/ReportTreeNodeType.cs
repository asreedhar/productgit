namespace FirstLook.Reports.App.ReportDefinitionLibrary
{
    public enum ReportTreeNodeType
    {
        ReportGroup,
        ReportHandle,
    }
}
