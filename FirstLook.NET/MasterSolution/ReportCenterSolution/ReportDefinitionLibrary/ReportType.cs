using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Reports.App.ReportDefinitionLibrary
{
    public enum ReportType
    {
        /// <summary>
        /// Dealership level report
        /// </summary>
        Dealer,
        /// <summary>
        /// Dealer Group level report
        /// </summary>
        DealerGroup,
    }
}
