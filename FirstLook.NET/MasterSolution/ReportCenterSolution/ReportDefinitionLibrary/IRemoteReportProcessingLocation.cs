using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Reports.App.ReportDefinitionLibrary
{
    public interface IRemoteReportProcessingLocation : IReportProcessingLocation
    {
        string Path
        {
            get;
        }

        Uri ServerUrl
        {
            get;
        }
    }
}
