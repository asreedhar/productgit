using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Reports.App.ReportDefinitionLibrary
{
    public enum ReportParameterInputType
    {
        Text,
        Checkbox,
        Radio,
        Password,
        Hidden,
        Select,
    }
}
