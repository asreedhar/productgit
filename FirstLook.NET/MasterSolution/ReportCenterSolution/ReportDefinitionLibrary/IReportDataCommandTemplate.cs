using System.Collections.Generic;
using FirstLook.Common.Data;

namespace FirstLook.Reports.App.ReportDefinitionLibrary
{
    public interface IReportDataCommandTemplate : IDataCommandTemplate
    {
        string Name
        {
            get;
        }

        ReportDataCommandType ReportDataSourceType(ResolveDataSourceType dsResolver);
    }
}
