using System.Collections;
using System.Globalization;
using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class InventorySalesSummaryGaugeDataService : InventorySalesSummaryDataService<InventorySalesSummaryGaugeDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];
            summaryTable.Columns.Add(new DataColumn("TimePeriodId", typeof(string), string.Format(CultureInfo.InvariantCulture, "'{0}'", Parameters.TimePeriodId)));
            //summaryTable.Columns.Add(new DataColumn("SalesBeforeDay30", typeof(double), processor.FormatExpression("Avg(Child({0}).SweetSpotSales)")));
            //summaryTable.Columns.Add(new DataColumn("SalesBeforeDay60", typeof(double), processor.FormatExpression("Avg(Child({0}).RetailSalesEfficiency)")));
            //summaryTable.Columns.Add(new DataColumn("RetailGrossProfitGiveBackPercent", typeof(double), processor.FormatExpression("Avg(Child({0}).RetailGrossProfitGiveBackfromAgedWholesaleLoss)")));
            //summaryTable.Columns.Add(new DataColumn("RetailGrossProfitGiveBackDollars", typeof(double), processor.FormatExpression("Avg(Child({0}).RetailGrossProfitGiveBackfromAgedWholesaleLossDollars)")));
            //summaryTable.Columns.Add(new DataColumn("AvgFrontEndGross", typeof(double), processor.FormatExpression("Avg(Child({0}).AvgFrontEndGross)")));
            //summaryTable.Columns.Add(new DataColumn("AvgImmWholesaleLoss", typeof(double), processor.FormatExpression("Avg(Child({0}).AvgImmediateWholesaleProfitLoss)")));
            //summaryTable.Columns.Add(new DataColumn("TrueRetailAvgGross", typeof(double), processor.FormatExpression("Avg(Child({0}).TrueRetailAvgGrossProfit)")));

            summaryTable.Columns.Add(new DataColumn("SalesBeforeDay30", typeof(double), processor.FormatExpression("Sum(Child({0}).[Retail_Sales_Days_0to29])/(Sum(Child({0}).[Total_Number_of_Sales])-ISNULL(Sum(Child({0}).[Immediate_Wholesale_Sales]),0.0))")));
            summaryTable.Columns.Add(new DataColumn("SalesBeforeDay60", typeof(double), processor.FormatExpression("Sum(Child({0}).[Retail_Sales_Days_0to59])/(Sum(Child({0}).[Total_Number_of_Sales])-ISNULL(Sum(Child({0}).[Immediate_Wholesale_Sales]),0.0))")));
            summaryTable.Columns.Add(new DataColumn("RetailGrossProfitGiveBackPercent", typeof(double), processor.FormatExpression("ISNULL(Sum(Child({0}).[Aged_wholesale_ProfitLoss]),0.0)/Sum(Child({0}).[Total_Retail_Gross])")));
            summaryTable.Columns.Add(new DataColumn("RetailGrossProfitGiveBackDollars", typeof(double), processor.FormatExpression("ISNULL(Sum(Child({0}).[Aged_wholesale_ProfitLoss]),0.0)")));
            summaryTable.Columns.Add(new DataColumn("AvgFrontEndGross", typeof(double), processor.FormatExpression("Sum(Child({0}).[Total_Retail_Gross])/Sum(Child({0}).RetailSalesCount)")));
            summaryTable.Columns.Add(new DataColumn("AvgImmWholesaleLoss", typeof(double), processor.FormatExpression("Sum(Child({0}).[Gross_for_vehicles_sold_wholesale_in_days_0to29])/Sum(Child({0}).[Number_of_vehicles_sold_wholesale_in_days_0to29])")));
            //summaryTable.Columns.Add(new DataColumn("TrueRetailAvgGross", typeof(double), processor.FormatExpression("(ISNULL(Sum(Child({0}).[Total_Retail_Gross]),0.0) + ISNULL(Sum(Child({0}).[Total_Immediate_Wholesale_ProfitLoss]),0.0) + ISNULL(Sum(Child({0}).[Total_Aged_Wholesale_ProfitLoss]),0.0))/Sum(Child({0}).[RetailSalesCount])")));
            summaryTable.Columns.Add(new DataColumn("TrueRetailAvgGross", typeof(double), processor.FormatExpression("(ISNULL(Sum(Child({0}).[Total_Retail_Gross]),0.0) + ISNULL(Sum(Child({0}).[Total_Aged_Wholesale_ProfitLoss]),0.0))/Sum(Child({0}).[RetailSalesCount])")));
            
            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                "TimePeriodId", "SalesBeforeDay30", "SalesBeforeDay60",
                "RetailGrossProfitGiveBackPercent", "RetailGrossProfitGiveBackDollars",
                "AvgFrontEndGross", "AvgImmWholesaleLoss",
                "TrueRetailAvgGross").DefaultView;
        }
    }
}
