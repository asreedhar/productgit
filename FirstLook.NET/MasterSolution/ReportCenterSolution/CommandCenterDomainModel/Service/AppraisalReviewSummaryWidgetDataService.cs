using System.Collections;
using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalReviewSummaryWidgetDataService : AppraisalReviewDataService<AppraisalReviewSummaryWidgetDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];
            summaryTable.Columns.Add(new DataColumn("Min", typeof(double), processor.FormatExpression("Min(Child({0}).NumberOfAppraisalReviews)")));
            summaryTable.Columns.Add(new DataColumn("Max", typeof(double), processor.FormatExpression("Max(Child({0}).NumberOfAppraisalReviews)")));
            summaryTable.Columns.Add(new DataColumn("Avg", typeof(double), processor.FormatExpression("Sum(Child({0}).NumberOfAppraisalReviews) / Count(Child({0}).Dealer_ID)")));
            summaryTable.Columns.Add(new DataColumn("DaysBack", typeof(double), processor.FormatExpression("Max(Child({0}).DaysBack)")));

            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                "Min", "Max", "Avg", "DaysBack").DefaultView;
        }
    }
}
