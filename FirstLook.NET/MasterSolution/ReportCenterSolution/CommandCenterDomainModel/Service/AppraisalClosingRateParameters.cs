namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalClosingRateParameters : CoreDataServiceParameters
    {
        private string timePeriodId;

        public string TimePeriodId
        {
            get { return timePeriodId; }
            set { timePeriodId = value; }
        }
    }
}