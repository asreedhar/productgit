﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AddAccessClassification    
    {
        protected enum AccessClassification
        {
            Accessible = 1,
            Selected   = 2
        }
       protected enum DrilldownRowType
        {
            LineItem,
            UnselectedManagedStore,
            UnmanagedStore,
            WholeGroup
        }
        public delegate string NumericColumnFormatDelegate(string columnName);

        public static string GetDefaultNumericColumnFormat(string columnName)
        {
            return "ISNULL(Avg(Child({0}).{1}), 0.0)";
        }

        public const string AccessClassificationTableName = "AccessClassification";
        public const string AccessClassificationColumnName = "AccessClassification";
        public const string GroupSummaryTableName = "GroupSummary";
        public const string GroupSummaryColumnName = "GroupSummary";
        public const string RowCountColumnName = "__RowCount__";
        public const string RowTypeColumnName = "DrilldownRowType";

        private readonly AccessClassificationParameters parameters;
        private readonly NumericColumnFormatDelegate columnFormat;

        public AddAccessClassification(AccessClassificationParameters param, NumericColumnFormatDelegate columnFormat)
        {
            this.parameters = param;
            this.columnFormat = columnFormat;
        }
        public DataView ProcessDrillthrough(string sortExpression)
        {
            DataView view = ProcessDrillthrough().DefaultView;
            if (!string.IsNullOrEmpty(sortExpression))
                view.Sort = sortExpression;
            return view;
        }

        public DataTable ProcessDrillthrough()
        {
            AddAccessClassificationColumns();
            // fill a data set
            DataSet dataSet = FillDataSet();

            // add calculated avg columns to AccessClassification table
            AddNumericColumns(dataSet);

            // take a snapshot of the selected line items
            DataTable result = SubTable(dataSet.Tables[parameters.ReportDataSetName], AccessibleAndSelectedRowFilter());

            // accessible not selected summary row
            AppendSummaryRow(dataSet, result, AccessClassificationTableName, AccessibleRowFilter(),
                             "Other dealerships you manage",
                             DrilldownRowType.UnselectedManagedStore);

            // not accessible summary row
            AppendSummaryRow(dataSet, result, AccessClassificationTableName, NotAccessibleRowFilter(),
                             "Other in-group dealerships",
                             DrilldownRowType.UnmanagedStore);

            // whole group summary row
            AppendSummaryRow(dataSet, result, GroupSummaryTableName, string.Format(CultureInfo.InvariantCulture, "{0} = 0", GroupSummaryColumnName),
                             "Whole group",
                             DrilldownRowType.WholeGroup);
            return result;
        }

        public DataSet FillDataSet()
        {
            ProcessDataTable();

            DataTable classificationTable = AccessClassificationTable();

            DataTable groupSummaryTable = GroupSummaryTable();

            DataSet set = new DataSet();
            set.Locale = CultureInfo.InvariantCulture;
            set.Tables.Add(parameters.ReportDataSet);
            set.Tables.Add(classificationTable);
            set.Tables.Add(groupSummaryTable);
            set.Relations.Add(
                new DataRelation(parameters.AccessClassificationRelationName,
                                 classificationTable.Columns[AccessClassificationColumnName],
                                 parameters.ReportDataSet.Columns[AccessClassificationColumnName],
                                 false));
            set.Relations.Add(
                new DataRelation(parameters.GroupSummaryRelationName,
                                 groupSummaryTable.Columns[GroupSummaryColumnName],
                                 parameters.ReportDataSet.Columns[GroupSummaryColumnName],
                                 false));

            return set;
        }

        protected void ProcessDataTable()
        {
            // add row classification and type
            foreach (DataRow row in parameters.ReportDataSet.Rows)
            {
                row[AccessClassificationColumnName] = (int)ClassifyDataRow(row);
                row[RowTypeColumnName] = DrilldownRowType.LineItem;
            }

            // add group summary foreign key
            parameters.ReportDataSet.Columns.Add(new DataColumn(GroupSummaryColumnName, typeof(int), "0"));
        }

        protected AccessClassification ClassifyDataRow(DataRow row)
        {
            AccessClassification classification = 0;

            int dealerId = Int32.Parse(row["Dealer_ID"].ToString(), CultureInfo.InvariantCulture);

            foreach (BusinessUnit accessibleDealer in parameters.AccessibleDealers)
            {
                if (accessibleDealer.Id == dealerId)
                {
                    classification |= AccessClassification.Accessible;

                    foreach (BusinessUnit selectedDealer in parameters.SelectedDealers)
                    {
                        if (selectedDealer.Id == dealerId)
                        {
                            classification |= AccessClassification.Selected;

                            break;
                        }
                    }

                    break;
                }
            }

            return classification;
        }

        protected static DataTable AccessClassificationTable()
        {
            DataTable table = new DataTable(AccessClassificationTableName);
            table.Locale = CultureInfo.InvariantCulture;
            table.Columns.Add(AccessClassificationColumnName, typeof(int));
            table.Columns.Add("Description", typeof(string));

            DataRow notAccessible = table.NewRow();
            notAccessible[AccessClassificationColumnName] = 0;
            notAccessible["Description"] = "Not Accessible";

            table.Rows.Add(notAccessible);

            DataRow accessibleAndSelected = table.NewRow();
            accessibleAndSelected[AccessClassificationColumnName] = (int)(AccessClassification.Accessible | AccessClassification.Selected);
            accessibleAndSelected["Description"] = "Accessible and Selected";

            table.Rows.Add(accessibleAndSelected);

            DataRow accessible = table.NewRow();
            accessible[AccessClassificationColumnName] = (int)AccessClassification.Accessible;
            accessible["Description"] = "Accessible";

            table.Rows.Add(accessible);

            return table;
        }

        protected static DataTable GroupSummaryTable()
        {
            DataTable table = new DataTable(GroupSummaryTableName);
            table.Locale = CultureInfo.InvariantCulture;
            table.Columns.Add(GroupSummaryColumnName, typeof(int));

            DataRow singleton = table.NewRow();
            singleton[GroupSummaryColumnName] = 0;

            table.Rows.Add(singleton);

            return table;
        }

        protected static List<DataColumn> NumericColumns(DataTable table)
        {
            List<DataColumn> columnNames = new List<DataColumn>();

            foreach (DataColumn column in table.Columns)
            {
                string columnName = column.ColumnName;

                bool isAccessClassification = columnName.Equals(AccessClassificationColumnName);

                bool isGroupSummary = columnName.Equals(GroupSummaryColumnName);

                bool isRowType = columnName.Equals(RowTypeColumnName);

                bool isRowId = columnName.Equals("__RowID__");

                bool isRowCount = columnName.Equals(RowCountColumnName);

                if (isAccessClassification || isGroupSummary || isRowType || isRowId || isRowCount)
                    continue;

                switch (Type.GetTypeCode(column.DataType))
                {
                    case TypeCode.Boolean:
                        break;
                    case TypeCode.Char:
                        break;
                    case TypeCode.DateTime:
                        break;
                    case TypeCode.DBNull:
                        break;
                    case TypeCode.Empty:
                        break;
                    case TypeCode.Object:
                        break;
                    case TypeCode.String:
                        break;
                    default:
                        columnNames.Add(column);
                        break;
                }
            }

            return columnNames;
        }

        protected void AddNumericColumns(DataSet dataSet)
        {
            DataTable classificationTable = dataSet.Tables[AccessClassificationTableName];

            DataTable groupTable = dataSet.Tables[GroupSummaryTableName];

            DataTable dataTable = dataSet.Tables[parameters.ReportDataSetName];

            string fmtCount = "ISNULL(Count(Child({0}).Dealer), 0.0)";

            bool first = true;

            foreach (DataColumn column in NumericColumns(dataTable))
            {
                string columnName = column.ColumnName;

                string columnExpression;

                if (first)
                {
                    columnExpression = string.Format(CultureInfo.InvariantCulture, fmtCount, parameters.AccessClassificationRelationName);

                    classificationTable.Columns.Add(new DataColumn(RowCountColumnName, typeof(int), columnExpression));

                    columnExpression = string.Format(CultureInfo.InvariantCulture, fmtCount, parameters.GroupSummaryRelationName);

                    groupTable.Columns.Add(new DataColumn(RowCountColumnName, typeof(int), columnExpression));

                    first = false;
                }

                if (!classificationTable.Columns.Contains(columnName))
                {
                   
                    columnExpression = string.Format(CultureInfo.InvariantCulture, columnFormat(columnName), parameters.AccessClassificationRelationName, columnName);
                    
                    classificationTable.Columns.Add(new DataColumn(columnName, column.DataType, columnExpression));
                }

                if (!groupTable.Columns.Contains(columnName))
                {
                    columnExpression = string.Format(CultureInfo.InvariantCulture, columnFormat(columnName), parameters.GroupSummaryRelationName, columnName);

                    groupTable.Columns.Add(new DataColumn(columnName, column.DataType, columnExpression));
                }
            }
        }

        public static string checkSalesSummaryColumn(string columnName)
        {
       
                switch (columnName)
                {
                    case "SweetSpotSales":
                        return "ISNULL(Sum(Child({0}).[Retail_Sales_Days_0to29])/(Sum(Child({0}).[Total_Number_of_Sales])-Sum(Child({0}).[Immediate_Wholesale_Sales])),0.0)";
                    case "RetailSalesEfficiency":
                        return "ISNULL(Sum(Child({0}).[Retail_Sales_Days_0to59])/(Sum(Child({0}).[Total_Number_of_Sales])-Sum(Child({0}).[Immediate_Wholesale_Sales])),0.0)";
                    case "RetailGrossProfitGiveBackfromAgedWholesaleLoss":
                        return "ISNULL(Sum(Child({0}).[Aged_wholesale_ProfitLoss])/Sum(Child({0}).[Total_Retail_Gross]),0.0)";
                    case "RetailGrossProfitGiveBackfromAgedWholesaleLossDollars":
                        return "ISNULL(Sum(Child({0}).[Aged_wholesale_ProfitLoss]),0.0)";
                    case "AvgFrontEndGross":
                        return "ISNULL(Sum(Child({0}).[Total_Retail_Gross])/Sum(Child({0}).RetailSalesCount),0.0)";
                    case "AvgImmediateWholesaleProfitLoss":
                        return "ISNULL(Sum(Child({0}).[Gross_for_vehicles_sold_wholesale_in_days_0to29])/Sum(Child({0}).[Number_of_vehicles_sold_wholesale_in_days_0to29]),0.0)";
                    case "TrueRetailAvgGrossProfit":
                        //return "ISNULL((ISNULL(Sum(Child({0}).[Total_Retail_Gross]),0.0) + ISNULL(Sum(Child({0}).[Total_Immediate_Wholesale_ProfitLoss]),0.0) + ISNULL(Sum(Child({0}).[Total_Aged_Wholesale_ProfitLoss]),0.0))/Sum(Child({0}).[RetailSalesCount]),0.0)";
                        return "ISNULL((ISNULL(Sum(Child({0}).[Total_Retail_Gross]),0.0) + ISNULL(Sum(Child({0}).[Total_Aged_Wholesale_ProfitLoss]),0.0))/Sum(Child({0}).[RetailSalesCount]),0.0)";
                    default:
                        return "ISNULL(Avg(Child({0}).{1}), 0.0)";
                }
           
        }
        public static string CheckAppraisalClosingRateByStoreColumn(string columnName)
        {

            switch (columnName)
            {
                case "ClosingRateRank":
                    return "500";
                case "TradeInInventoryAnalyzedRank":
                    return "500";
                case "AvgImmediateWholesaleProfitRank":
                    return "500";
                case "ClosingRate":
                    return "IIF(Sum(Child({0}).[AppraisalFactCountInPeriodNew])>0,ISNULL(ISNULL(Sum(Child({0}).[ClosedAppraisalCountNew]),0.0)*100/Sum(Child({0}).[AppraisalFactCountInPeriodNew]),0.0),0.0)";
                case "TradeInInventoryAnalyzed":
                    return "IIF(Sum(Child({0}).[InventoryReceivedFactCount])>0,ISNULL(ISNULL(Sum(Child({0}).[TradeAnalyzedCount]),0.0)*100/Sum(Child({0}).[InventoryReceivedFactCount]),0.0),0.0)";
                case "AvgImmediateWholesaleProfitInPeriod":
                    return "IIF(Sum(Child({0}).[Number_of_vehicles_sold_wholesale_in_days_0to29])>0,ISNULL(Sum(Child({0}).[Gross_for_vehicles_sold_wholesale_in_days_0to29])/Sum(Child({0}).[Number_of_vehicles_sold_wholesale_in_days_0to29]),0.0),0.0)";
                default:
                    return "ISNULL(Avg(Child({0}).{1}), 0.0)";
            }

        }
        protected static void AppendSummaryRow(DataSet dataSet, DataTable result, string tableName, string filter, string name, DrilldownRowType rowType)
        {
            DataTable dataTable = dataSet.Tables[tableName];
            dataTable.Locale = result.Locale;

            bool hasRowCountColumn = dataTable.Columns.Contains(RowCountColumnName);

            List<DataColumn> numericColumns = NumericColumns(dataTable);

            foreach (DataRow row in dataTable.Select(filter))
            {
                object rowCount = hasRowCountColumn ? row[RowCountColumnName] : null;

                if (!hasRowCountColumn || (rowCount != DBNull.Value && ((int)rowCount) > 0))
                {
                    DataRow summaryRow = result.NewRow();
                    summaryRow["Dealer_Group"] = name;
                    summaryRow["Dealer"] = name;
                    summaryRow[RowTypeColumnName] = rowType;
                    summaryRow["__RowID__"] = Int32.MaxValue - ((int)rowType) - 1;

                    foreach (DataColumn column in numericColumns)
                    {
                        summaryRow[column.ColumnName] = row[column.ColumnName];
                    }

                    result.Rows.Add(summaryRow);
                }
            }
        }

        internal static DataTable SubTable(DataTable table, string filterExpression, params string[] columnNames)
        {
            DataView view = new DataView(table);
            view.RowFilter = filterExpression;
            if (columnNames.Length == 0)
                return view.ToTable(table.TableName);
            else
                return view.ToTable(table.TableName, false, columnNames);
        }

        internal static string NotAccessibleRowFilter()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} = {1}", AccessClassificationColumnName, 0);
        }

        internal static string AccessibleRowFilter()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} = {1}",
                                 AccessClassificationColumnName,
                                 (int)AccessClassification.Accessible);
        }

        internal static string AccessibleAndSelectedRowFilter()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} = {1}",
                                 AccessClassificationColumnName,
                                 (int)(AccessClassification.Accessible | AccessClassification.Selected));
        }

        internal string FormatExpression(string expression, object value)
        {
            return string.Format(CultureInfo.InvariantCulture, expression, parameters.AccessClassificationRelationName, value);
        }

        internal string FormatExpression(string expression)
        {
            return string.Format(CultureInfo.InvariantCulture, expression, parameters.AccessClassificationRelationName);
        }

        protected void AddAccessClassificationColumns()
        {
            // Dont do anything if columns exists
            try
            {
                parameters.ReportDataSet.Columns.AddRange(columns);
            }
            catch 
            {
                
            }
            
        }
        private readonly DataColumn[] columns = new DataColumn[]
                {
                    new DataColumn(AccessClassificationColumnName, typeof (int)),
                    new DataColumn(RowTypeColumnName, typeof (DrilldownRowType))
                };
      }
}
