using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Core;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalReviewTimePeriodDataService : AbstractDataService<bool>
    {
        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            // no parameters
        }

        protected override bool Parameters
        {
            get { return true; }
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            IDictionary<string, object> parameterValues = new Dictionary<string, object>();

            DataTable table = ReportHelper.LoadReportDataTable(
                "A1237BC3-5566-450e-ACC9-BA6CBC409289",
                "AppraisalReviewDetailsDaysBackDataSet",
                parameterValues,
                new DataTableCallback());

            table.TableName = "AppraisalReviewDetailsDaysBackDataSet";

            table.Columns.Add("Label", typeof(string));
            foreach(DataRow row in table.Rows)
            {
                row["Label"] = FindNameByValue((int)row["Value"]);
            }

            return table.DefaultView;
        }

        public static string FindNameByValue(int value)
        {
            if (value == 1)
                return String.Format(CultureInfo.InvariantCulture, "Previous {0} day", value);
            else
                return String.Format(CultureInfo.InvariantCulture, "Previous {0} days", value);
        }
    }
}
