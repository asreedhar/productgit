using System.Collections;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class InventoryPricingRiskTableDataService : InventoryPricingRiskDataService<InventoryPricingRiskSummaryDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            return CreateProcessor(GetNumericColumnFormat).ProcessDrillthrough(arguments.SortExpression);
        }

        public override bool CanSort
        {
            get { return true; }
        }

        internal static string GetNumericColumnFormat(string columnName)
        {
            if (string.Compare(columnName, "HasUpgrade") == 0)
            {
                return "Count(Child({0}).{1})";
            }
            else if (string.Compare(columnName, "Units_Total") == 0 ||
                     string.Compare(columnName, "Units_Risk") == 0 ||
                     string.Compare(columnName, "UnitCost") == 0 ||
                     string.Compare(columnName, "BookVersusCost") == 0)
            {
                return "Sum(Child({0}).{1})";
            }
            else if (string.Compare(columnName, "Percentage") == 0)
            {
                //return "Sum(child({0}).{1})/Sum(child({0}).{1})";
                //"IIF(Sum(Child({0}).[InventoryReceivedFactCount])>0,ISNULL(ISNULL(Sum(Child({0}).[TradeAnalyzedCount]),0.0)/Sum(Child({0}).[InventoryReceivedFactCount]),0.0),0.0)")));
                return "IIF(Sum(Child({0}).[Units_Total])>0,ISNULL(Sum(Child({0}).[Units_Risk])*100/Sum(Child({0}).[Units_Total]),0.0),0.0)";

            }
            else
            {
                return AccessClassificationProcessor.GetDefaultNumericColumnFormat(columnName);
            }
        }
    }
}
