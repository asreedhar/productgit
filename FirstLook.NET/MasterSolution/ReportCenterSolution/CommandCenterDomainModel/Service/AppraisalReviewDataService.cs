using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.CommandCenter.DomainModel.Service;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalReviewDataService<TService> : AccessClassificationDataService<TService, AppraisalReviewParameters>
        where TService : AppraisalReviewDataService<TService>, new()
    {
        private AppraisalReviewParameters _parameters;

        private const string reportDataSetName = "AppraisalReviewDetailsDataSet";

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            AppraisalReviewParameters parameters = new AppraisalReviewParameters();
            parameters.DealerGroup = (BusinessUnit)parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");
            parameters.DaysBack = Convert.ToInt32((string)parameterCallback("daysBack"), CultureInfo.InvariantCulture);

            Initialize(parameters);
        }

        protected override void Initialize(AppraisalReviewParameters parameters)
        {
            _parameters = parameters;
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            throw new NotImplementedException("The method or operation is not implemented on 'AppraisalReviewDataService.GetData'.");
        }

        protected override AppraisalReviewParameters Parameters
        {
            get { return _parameters; }
        }

        protected override DataTable LoadDataTable()
        {
            IDictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = Parameters.DealerGroup.Id;
            parameterValues["DaysBack"] = Parameters.DaysBack;

            DataTable table = ReportHelper.LoadReportDataTable(
                "A1237BC3-5566-450e-ACC9-BA6CBC409289",
                reportDataSetName,
                parameterValues,
                new AccessClassificationProcessor.AccessClassificationCallback());

            table.TableName = reportDataSetName;

            return table;
        }
    }
}
