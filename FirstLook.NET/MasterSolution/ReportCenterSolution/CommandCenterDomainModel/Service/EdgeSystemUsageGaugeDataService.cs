using System.Collections;
using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class EdgeSystemUsageGaugeDataService : EdgeSystemUsageDataService<EdgeSystemUsageGaugeDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];
            summaryTable.Columns.Add(new DataColumn("PercentRed", typeof(double), processor.FormatExpression("Sum(Child({0}).RedCount) * 100.0 / Count(Child({0}).RedCount)")));
            summaryTable.Columns.Add(new DataColumn("PercentYellow", typeof(double), processor.FormatExpression("Sum(Child({0}).YellowCount) * 100.0 / Count(Child({0}).YellowCount)")));
            summaryTable.Columns.Add(new DataColumn("PercentGreen", typeof(double), processor.FormatExpression("Sum(Child({0}).GreenCount) * 100.0 / Count(Child({0}).GreenCount)")));

            // return single expected row
            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                "PercentRed", "PercentYellow", "PercentGreen").DefaultView;
        }
    }
}
