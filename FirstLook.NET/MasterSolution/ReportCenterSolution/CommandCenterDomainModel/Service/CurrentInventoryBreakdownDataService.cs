using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.CommandCenter.DomainModel.Service;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class CurrentInventoryBreakdownDataService<TService> : AccessClassificationDataService<TService, CurrentInventoryBreakdownParameters>
        where TService : CurrentInventoryBreakdownDataService<TService>, new()
    {
        private CurrentInventoryBreakdownParameters _parameters;

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            CurrentInventoryBreakdownParameters parameters = new CurrentInventoryBreakdownParameters();
            parameters.DealerGroup = (BusinessUnit)parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");

            object bucketIndex = parameterCallback("bucketIndex");
            if (bucketIndex != null)
            {
                parameters.BucketIndex = (int)bucketIndex;
            }

            object isPercentMode = parameterCallback("isPercentMode");
            if (isPercentMode != null)
            {
                parameters.IsPercentMode = (bool)isPercentMode;
            }

            Initialize(parameters);
        }

        protected override void Initialize(CurrentInventoryBreakdownParameters parameters)
        {
            _parameters = parameters;
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            throw new NotImplementedException("The method or operation is not implemented on 'CurrentInventoryBreakdownDataService'.");
        }

        protected override CurrentInventoryBreakdownParameters Parameters
        {
            get { return _parameters; }
        }

        protected string ReportDataSetName
        {
            get
            {
                string name;

                if (Parameters.IsGraph)
                {
                    if (Parameters.IsSummary)
                    {
                        DealerGroupPreferenceFinder finder = DealerGroupPreferenceFinder.Instance();
                        DealerGroupPreference dealerGroupPreference = finder.FindByDealerGroup(Parameters.DealerGroup.Id);

                        name = dealerGroupPreference.ExcludeWholesaleFromDaysSupply ? "CurrentInventoryBreakdownMetricsDataSetExcludingWholesale" : "CurrentInventoryBreakdownMetricsDataSet";
                    }
                    else
                    {
                        name = "CurrentInventoryBreakdownBucketsDataSet";
                    }
                }
                else
                {
                    name = "CurrentInventoryBreakdownDataSet";
                }

                return name;
            }
        }

        protected override DataTable LoadDataTable()
        {
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = "[Dealer].[Dealer Group Hierarchy].[Dealer Group].&[" + Parameters.DealerGroup.Id + "]";
            parameterValues["BucketIndex"] = Parameters.BucketIndex;
            DataTable dataTable = null;
            List<string> param = new List<string>();

            try
            {
                param = ReportAnalyticsClient.GetParameterList(parameterValues);
                string reportId = Parameters.IsGraph
                    ? "DD46FC3F-E1C4-4bf2-95CA-F9DFC273EE45"
                    : "F8C04839-5950-48b1-B6AA-E2A7181FD034";
                param.Add(reportId);

                dataTable = ReportAnalyticsClient.GetReportData<DataTable>(param);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }

            if (dataTable == null)
            {
                if (Parameters.IsGraph)
                {
                    dataTable = ReportHelper.LoadReportDataTable("DD46FC3F-E1C4-4bf2-95CA-F9DFC273EE45", ReportDataSetName, parameterValues, new AccessClassificationProcessor.AccessClassificationCallback());
                }
                else
                {
                    dataTable = ReportHelper.LoadReportDataTable("F8C04839-5950-48b1-B6AA-E2A7181FD034", ReportDataSetName, parameterValues, new AccessClassificationProcessor.AccessClassificationCallback());
                }

                dataTable.TableName = ReportDataSetName;
                try
                {
                    ReportAnalyticsClient.SetReportData(ReportAnalyticsClient.GetParameterList(parameterValues), dataTable);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }
            }
            return dataTable;
        }
    }
}
