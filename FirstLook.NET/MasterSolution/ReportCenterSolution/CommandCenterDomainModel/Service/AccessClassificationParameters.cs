using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AccessClassificationParameters
    {
        private readonly IEnumerable<BusinessUnit> accessibleDealers;
        private readonly IEnumerable<BusinessUnit> selectedDealers;
        private readonly DataTable reportDataSet;

        public AccessClassificationParameters(
            IEnumerable<BusinessUnit> accessibleDealers,
            IEnumerable<BusinessUnit> selectedDealers,
            DataTable reportDataSet)
        {
            this.accessibleDealers = accessibleDealers;
            this.selectedDealers = selectedDealers;
            this.reportDataSet = reportDataSet;
        }

        public IEnumerable<BusinessUnit> AccessibleDealers
        {
            get { return accessibleDealers; }
        }

        public IEnumerable<BusinessUnit> SelectedDealers
        {
            get { return selectedDealers; }
        }

        public DataTable ReportDataSet
        {
            get { return reportDataSet; }
        }

        public string ReportDataSetName
        {
            get { return ReportDataSet.TableName; }
        }

        public string AccessClassificationRelationName
        {
            get { return string.Format(CultureInfo.InvariantCulture, "{0}_AccessClassification", ReportDataSetName); }
        }

        public string GroupSummaryRelationName
        {
            get { return string.Format(CultureInfo.InvariantCulture, "{0}_GroupSummary", ReportDataSetName); }
        }
    }
}
