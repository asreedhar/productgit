using System;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    [Flags]
    internal enum AccessClassification
    {
        Accessible = 1,
        Selected   = 2
    }
}
