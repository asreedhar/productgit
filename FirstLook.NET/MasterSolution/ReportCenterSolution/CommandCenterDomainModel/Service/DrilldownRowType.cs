namespace FirstLook.CommandCenter.DomainModel.Service
{
    internal enum DrilldownRowType
    {
        LineItem,
        UnselectedManagedStore,
        UnmanagedStore,
        WholeGroup
    }
}
