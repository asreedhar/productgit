using System.Collections;
using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class CurrentInventoryBreakdownGraphSummaryDataService : CurrentInventoryBreakdownDataService<CurrentInventoryBreakdownGraphDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];
            summaryTable.Columns.Add(new DataColumn("AvgDaysSupply", typeof(double), processor.FormatExpression("Avg(Child({0}).AvgDaysSupply)")));
            summaryTable.Columns.Add(new DataColumn("VehiclesInStock", typeof(double), processor.FormatExpression("Sum(Child({0}).VehiclesInStock)")));
            summaryTable.Columns.Add(new DataColumn("TotalInventoryDollars", typeof(double), processor.FormatExpression("Sum(Child({0}).TotalInventoryDollars)")));
            summaryTable.Columns.Add(new DataColumn("BookVsCost", typeof(double), processor.FormatExpression("Sum(Child({0}).BookVsCost)")));

            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                "AvgDaysSupply", "VehiclesInStock", "TotalInventoryDollars", "BookVsCost").DefaultView;
        }

        protected override void Initialize(CurrentInventoryBreakdownParameters parameters)
        {
            parameters.IsGraph = true;
            parameters.IsSummary = true;

            base.Initialize(parameters);
        }
    }
}
