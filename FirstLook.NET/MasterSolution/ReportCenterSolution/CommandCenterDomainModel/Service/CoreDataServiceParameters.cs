using System.Collections.Generic;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class CoreDataServiceParameters
    {
        private BusinessUnit dealerGroup;
        private IEnumerable<BusinessUnit> dealers;
        private Member member;

        public BusinessUnit DealerGroup
        {
            get { return dealerGroup; }
            set { dealerGroup = value; }
        }

        public IEnumerable<BusinessUnit> Dealers
        {
            get { return dealers; }
            set { dealers = value; }
        }

        public Member Member
        {
            get { return member; }
            set { member = value; }
        }
    }
}
