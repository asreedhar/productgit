using System.Collections.Generic;
using System.Data;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public abstract class AccessClassificationDataService<TService, TParameters> : AbstractDataService<TParameters>
        where TService : AccessClassificationDataService<TService,TParameters>, new()
        where TParameters : CoreDataServiceParameters
    {
        protected abstract void Initialize(TParameters parameters);

        protected abstract DataTable LoadDataTable();

        internal AccessClassificationProcessor CreateProcessor()
        {
            return CreateProcessor(AccessClassificationProcessor.GetDefaultNumericColumnFormat);
        }

        internal AccessClassificationProcessor CreateProcessor(AccessClassificationProcessor.NumericColumnFormatDelegate formatDelegate)
        {
            ICollection<BusinessUnit> accessibleDealers = Parameters.Member.AccessibleDealers(Parameters.DealerGroup);
            IEnumerable<BusinessUnit> selectedDealers = Parameters.Dealers;
            DataTable table = LoadDataTable();
            AccessClassificationParameters args = new AccessClassificationParameters(
                accessibleDealers,
                selectedDealers,
                table);
            return new AccessClassificationProcessor(args, formatDelegate);
        }
    }
}