using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalOverviewDataService<TService> : AccessClassificationDataService<TService, AppraisalOverviewParameters>
        where TService : AppraisalOverviewDataService<TService>, new()
    {
        private AppraisalOverviewParameters _parameters;

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            AppraisalOverviewParameters parameters = new AppraisalOverviewParameters();
            parameters.DealerGroup = (BusinessUnit)parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");
            parameters.TimePeriodId = (string)parameterCallback("timePeriodId");
            parameters.Mode = (string)parameterCallback("reportMode");

            if (string.IsNullOrEmpty(parameters.Mode))
                parameters.Mode = "TradeInInventoryAnalyzed";

            Initialize(parameters);
        }

        protected override void Initialize(AppraisalOverviewParameters parameters)
        {
            _parameters = parameters;
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            throw new NotImplementedException("The method or operation is not implemented on 'AppraisalOverviewDataService.GetData'.");
        }

        protected override AppraisalOverviewParameters Parameters
        {
            get { return _parameters; }
        }

        protected override DataTable LoadDataTable()
        {
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = "[Dealer].[Dealer Group Hierarchy].[Dealer Group].&[" + Parameters.DealerGroup.Id + "]";
            parameterValues["TimePeriodID"] = Parameters.TimePeriodId;
            parameterValues["Mode"] = Parameters.Mode;
            DataTable dataTable = null;

            try
            {
                dataTable = ReportAnalyticsClient.GetReportData<DataTable>(ReportAnalyticsClient.GetParameterList(parameterValues));
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }

            if (dataTable == null)
            {
                dataTable = ReportHelper.LoadReportDataTable(
                   "B8F993A3-214D-42fd-A73C-581C884AE12F",
                   "AppraisalOverviewDataSet",
                   parameterValues,
                   new AccessClassificationProcessor.AccessClassificationCallback());

                dataTable.TableName = "AppraisalOverviewDataSet";
                try
                {
                    ReportAnalyticsClient.SetReportData(ReportAnalyticsClient.GetParameterList(parameterValues), dataTable);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }
            }
            return dataTable;
        }
    }
}
