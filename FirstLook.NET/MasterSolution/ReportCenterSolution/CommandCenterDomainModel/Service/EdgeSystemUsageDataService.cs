using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.CommandCenter.DomainModel.Service;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class EdgeSystemUsageDataService<TService> : AccessClassificationDataService<TService, EdgeSystemUsageParameters>
        where TService : EdgeSystemUsageDataService<TService>, new()
    {
        private EdgeSystemUsageParameters _parameters;

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            EdgeSystemUsageParameters parameters = new EdgeSystemUsageParameters();
            parameters.DealerGroup = (BusinessUnit) parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");
            Initialize(parameters);
        }

        protected override void Initialize(EdgeSystemUsageParameters parameters)
        {
            _parameters = parameters;
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            throw new NotImplementedException("The method or operation is not implemented on 'EdgeSystemUsageDataService.GetData'.");
        }

        protected override EdgeSystemUsageParameters Parameters
        {
            get { return _parameters; }
        }

        protected override DataTable LoadDataTable()
        {
            IDictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = Parameters.DealerGroup.Id;
            parameterValues["MemberID"] = Parameters.Member.Id;


            DataTable table = ReportHelper.LoadReportDataTable(
                "DF5CE4A2-12FB-4f0a-94AC-F39884462161",
                "EdgeSystemUsageGraphDataSet",
                parameterValues,
                new AccessClassificationProcessor.AccessClassificationCallback());

            table.TableName = "EdgeSystemUsageGraphDataSet";

            return table;
        }
    }
}
