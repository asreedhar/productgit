namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class CurrentInventoryBreakdownParameters : CoreDataServiceParameters
    {
        private int  bucketIndex;
        private bool isPercentMode;
        private bool isGraph;
        private bool isSummary;

        public int BucketIndex
        {
            get { return bucketIndex; }
            set { bucketIndex = value; }
        }

        public bool IsPercentMode
        {
            get { return isPercentMode; }
            set { isPercentMode = value; }
        }

        public bool IsGraph
        {
            get { return isGraph; }
            set { isGraph = value; }
        }

        public bool IsSummary
        {
            get { return isSummary; }
            set { isSummary = value; }
        }
    }
}
