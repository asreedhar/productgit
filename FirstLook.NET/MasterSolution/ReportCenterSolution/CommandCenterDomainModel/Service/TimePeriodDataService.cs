using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using FirstLook.CommandCenter.DomainModel.ObjectModel;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class TimePeriodDataService : AbstractDataService<bool>
    {
        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            // no parameters
        }

        protected override bool Parameters
        {
            get { return true; }
        }

        /// <summary>
        /// Return a list of time periods.  The value of the id's for a given time period
        /// MUST match the ID's the mdx queries expect. RDL's tied to these ids:
        /// Apprasisal Closing Rate Gauge.rdl
        /// </summary>
        /// <returns></returns>
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            List<TimePeriod> timePeriods = new List<TimePeriod>();
            timePeriods.Add(new TimePeriod("Average previous 4 weeks", "4weeks"));
            timePeriods.Add(new TimePeriod("Average previous 8 weeks", "8weeks"));
            timePeriods.Add(new TimePeriod("Average previous 13 weeks", "13weeks"));
            timePeriods.Add(new TimePeriod("Average previous 26 weeks", "26weeks"));
            timePeriods.Add(new TimePeriod("Average Year To Date", "avgYTD"));
            timePeriods.Add(new TimePeriod("Current Month", "curMonth"));
            timePeriods.Add(new TimePeriod("Average prior month", "avgPriorMonth"));
            timePeriods.Add(new TimePeriod("Average prior 3 months", "avgPrior3Month"));
            return timePeriods;
        }

        public static string FindTimePeriodDescriptionById(string id, bool useOffset)
        {
            string description;
            switch (id)
            {
                case "4weeks":
                    //description = "Average previous 4 weeks ending: " + EndingDate(useOffset);
                    //Below added code  shows the Previous four weeks ending date
                    description = "Previous 4 weeks ending: " + EndingDate(useOffset);
                    break;
                case "8weeks":
                    //description = "Average previous 8 weeks ending: " + EndingDate(useOffset);
                    //Below added code  shows the Previous eight weeks ending date
                    description = "Previous 8 weeks ending: " + EndingDate(useOffset);
                    break;
                case "13weeks":
                    //description = "Average previous 13 weeks ending: " + EndingDate(useOffset);
                    //Below added code  shows the Previous thirteen weeks ending date
                    description = "Previous 13 weeks ending: " + EndingDate(useOffset);
                    break;
                case "26weeks":
                    //description = "Average previous 26 weeks ending: " + EndingDate(useOffset);
                    //Below added code  shows the Previous twenty six weeks ending date
                    description = "Previous 26 weeks ending: " + EndingDate(useOffset);
                    break;
                case "avgYTD":
                    description = "Average Year To Date ending: " + EndingDate(false);
                    break;
                case "curMonth":
                    description = "Current Month ending: " + DateTime.Now.ToString("d", CultureInfo.CurrentCulture);
                    break;
                case "avgPriorMonth":
                    description = "Average prior month ending: " + new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1).ToString("d", CultureInfo.CurrentCulture);
                    break;
                case "avgPrior3Month":
                    description = "Average prior 3 months ending: " + new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1).ToString("d", CultureInfo.CurrentCulture);
                    break;
                default:
                    description = "Invalid date specified";
                    break;
            }
            return description;
        }

        /// <summary>
        /// <para>Computes the ending date either with the 2 week offset or not.</para>
        /// <para>If the 2 weeks offset is used, subtract 2 weeks, 14 days, from the end date.</para>
        /// <para>The End date should be yesterday, to include a full set of data and aggregations.</para>
        /// <para>These dates should match Edgescorecard and MAX end dates.</para>
        /// </summary>
        /// <param name="useOffset"></param>
        /// <returns>A string representation of the end date.</returns>
        private static string EndingDate(bool useOffset)
        {
            DateTime endDate = DateTime.Now.AddDays(-1);
            //Below code is commented to get the last four weeks ending date i.e yesterdays date
            //if (useOffset)
            //{
            //    //endDate = endDate - new TimeSpan(14, 0, 0, 0);
            //    endDate = endDate - new TimeSpan(14, 0, 0, 0);
            //}
            return endDate.ToString("d", CultureInfo.CurrentUICulture);
        }
    }
}
