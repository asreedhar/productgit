using System;
using System.Collections;
using System.Data;
using System.Globalization;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class CurrentInventoryBreakdownGraphDataService : CurrentInventoryBreakdownDataService<CurrentInventoryBreakdownGraphDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable dataTable = AccessClassificationProcessor.SubTable(
                dataSet.Tables[ReportDataSetName],
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter());

            // create (reference) bucket table
            DataTable bucketTable = BucketTable();

            // create a new data set using the mdx buckets
            DataSet set = new DataSet();
            set.Locale = dataSet.Locale;
            set.Tables.Add(dataTable);
            set.Tables.Add(bucketTable);
            set.Relations.Add(
                new DataRelation(BucketRelationName,
                                 bucketTable.Columns[MdxBucketNameColumnName],
                                 dataTable.Columns[MdxBucketNameColumnName],
                                 false));

            // switch on output type
            if (Parameters.IsPercentMode)
            {
                bucketTable.Columns.Add(new DataColumn("InventoryValue", typeof(double), Percent("Inventory_Count", "Dealer_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("InventoryValueLabel", typeof(double), Percent("Inventory_Count", "Dealer_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("PercentageOfRedLightInventory", typeof(double), Percent("Red_Light_Inventory_Count", "Dealer_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("PercentageOfYellowLightInventory", typeof(double), Percent("Yellow_Light_Inventory_Count", "Dealer_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("PercentageOfGreenLightInventory", typeof(double), Percent("Green_Light_Inventory_Count", "Dealer_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("InventoryValueMin", typeof(double), Min("Inventory_Count_Bucket_Percentage")));
                bucketTable.Columns.Add(new DataColumn("InventoryValueMax", typeof(double), Max("Inventory_Count_Bucket_Percentage")));
                bucketTable.Columns.Add(new DataColumn("RedLightInventoryCount", typeof(double), Sum("Red_Light_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("YellowLightInventoryCount", typeof(double), Sum("Yellow_Light_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("GreenLightInventoryCount", typeof(double), Sum("Green_Light_Inventory_Count")));
                bucketTable.Columns.Add(new DataColumn("InventoryCount", typeof(double), Sum("Inventory_Count")));
                
            }
            else
            {
                bucketTable.Columns.Add(new DataColumn("InventoryValue", typeof(double), Percent("Unit_Cost", "Dealer_Total_Unit_Cost")));
                bucketTable.Columns.Add(new DataColumn("InventoryValueLabel", typeof(double), Sum("Unit_Cost")));
                bucketTable.Columns.Add(new DataColumn("PercentageOfRedLightInventory", typeof(double), Percent("Red_Light_Unit_Cost", "Dealer_Total_Unit_Cost")));
                bucketTable.Columns.Add(new DataColumn("PercentageOfYellowLightInventory", typeof(double), Percent("Yellow_Light_Unit_Cost", "Dealer_Total_Unit_Cost")));
                bucketTable.Columns.Add(new DataColumn("PercentageOfGreenLightInventory", typeof(double), Percent("Green_Light_Unit_Cost", "Dealer_Total_Unit_Cost")));

                bucketTable.Columns.Add(new DataColumn("InventoryValueMin", typeof(double), Min("Unit_Cost")));
                bucketTable.Columns.Add(new DataColumn("InventoryValueMax", typeof(double), Max("Unit_Cost")));
            }

            bucketTable.Columns.Add(new DataColumn("BookVsCost", typeof(double), Sum("BookVsCost")));
            bucketTable.Columns.Add(new DataColumn("BookVsCostMin", typeof(double), Min("BookVsCost")));
            bucketTable.Columns.Add(new DataColumn("BookVsCostMax", typeof(double), Max("BookVsCost")));

            // return table
            return new DataView(bucketTable).ToTable(
                false, new string[]
                       {
                           "BucketName", "InventoryValue", "InventoryValueLabel",
                           "PercentageOfRedLightInventory",
                           "PercentageOfYellowLightInventory",
                           "PercentageOfGreenLightInventory",
                           "BookVsCost", "BookVsCostMin", "BookVsCostMax",
                           "InventoryValueMin", "InventoryValueMax", "RedLightInventoryCount", "YellowLightInventoryCount","GreenLightInventoryCount","InventoryCount"
                       }).DefaultView;
        }

        protected override void Initialize(CurrentInventoryBreakdownParameters parameters)
        {
            parameters.IsGraph = true;
            parameters.IsSummary = false;

            base.Initialize(parameters);
        }

        protected string BucketRelationName
        {
            get { return string.Format(CultureInfo.InvariantCulture, "{0}_Bucket", ReportDataSetName); }
        }

        protected string Min(string columnName)
        {
            return string.Format(CultureInfo.InvariantCulture, "Min(Child({0}).{1})", BucketRelationName, columnName);
        }

        protected string Max(string columnName)
        {
            return string.Format(CultureInfo.InvariantCulture, "Max(Child({0}).{1})", BucketRelationName, columnName);
        }

        private string Sum(string columnName)
        {
            return string.Format(CultureInfo.InvariantCulture, "Sum(Child({0}).{1})", BucketRelationName, columnName);
        }

        private string Percent(string numeratorColumnName, string denominatorColumnName)
        {
            return string.Format(CultureInfo.InvariantCulture, "Sum(Child({0}).{1}) / Sum(Child({0}).{2}) * 100", BucketRelationName, numeratorColumnName, denominatorColumnName);
        }

        private const string MdxBucketTableName = "Buckets";
        private const string BucketNameColumnName = "BucketName";
        private const string MdxBucketNameColumnName = "Age_Bucket_Preference_Range_ID";

        private static DataTable BucketTable()
        {
            DataTable table = new DataTable(MdxBucketTableName);
            table.Locale = CultureInfo.InvariantCulture;
            table.Columns.Add(MdxBucketNameColumnName, typeof(string));
            table.Columns.Add(BucketNameColumnName, typeof(string));

            string[] names = new string[]
                {
                    "0-29", "0-29", "30-39", "40-49", "50-59", "60+"
                };

            for (int i = 0; i < names.Length; i++)
            {
                DataRow row = table.NewRow();
                row[MdxBucketNameColumnName] = Convert.ToString(i + 1, CultureInfo.InvariantCulture);
                row[BucketNameColumnName] = names[i];
                table.Rows.Add(row);
            }

            return table;
        }
    }
}
