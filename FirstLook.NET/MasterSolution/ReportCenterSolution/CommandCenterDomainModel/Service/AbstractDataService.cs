using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    /// <summary>
    /// A helper class that implements the plubming of most DataService implementations
    /// </summary>
    /// <typeparam name="TParameters">The DataService parameter type</typeparam>
    public abstract class AbstractDataService<TParameters> : IDataService
    {
        public abstract void Initialize(DataServiceParameterCallback parameterCallback);

        public abstract IEnumerable GetData(DataServiceArguments arguments);

        protected abstract TParameters Parameters { get; }

        public IAsyncResult BeginGetData(DataServiceArguments arguments, AsyncCallback callback, object state)
        {
            AsyncResult<IEnumerable> ar = new AsyncResult<IEnumerable>(callback, state);

            AsyncState<Pair<DataServiceArguments, TParameters>, IEnumerable> args = new AsyncState<Pair<DataServiceArguments, TParameters>, IEnumerable>(
                new Pair<DataServiceArguments, TParameters>(arguments, Parameters), ar);

            ThreadPool.QueueUserWorkItem(GetDataHelper, args);

            return ar;
        }

        public IEnumerable EndGetData(IAsyncResult result)
        {
            AsyncResult<IEnumerable> ar = (AsyncResult<IEnumerable>)result;

            return ar.EndInvoke();
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void GetDataHelper(object stateInfo)
        {
            AsyncState<Pair<DataServiceArguments, TParameters>, IEnumerable> state = (AsyncState<Pair<DataServiceArguments, TParameters>, IEnumerable>)stateInfo;

            try
            {
                state.Result.SetAsCompleted(GetData(state.Parameters.First), false);
            }
            catch (Exception e)
            {
                state.Result.SetAsCompleted(e, false);
            }
        }

        public virtual bool CanPage
        {
            get { return false; }
        }

        public virtual bool CanRetrieveTotalRowCount
        {
            get { return false; }
        }

        public virtual bool CanSort
        {
            get { return false; }
        }
    }
}
