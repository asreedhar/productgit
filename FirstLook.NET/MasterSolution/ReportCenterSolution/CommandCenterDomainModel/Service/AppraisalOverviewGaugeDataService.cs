using System.Collections;
using System.Data;
using System.Globalization;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalOverviewGaugeDataService : AppraisalOverviewDataService<AppraisalOverviewGaugeDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];
            summaryTable.Columns.Add(new DataColumn("TradeInMin", typeof(double), processor.FormatExpression("Min(Child({0}).TradeInInventoryAnalyzed)")));
            summaryTable.Columns.Add(new DataColumn("TradeInMax", typeof(double), processor.FormatExpression("Max(Child({0}).TradeInInventoryAnalyzed)")));
            //summaryTable.Columns.Add(new DataColumn("TradeInAvg", typeof(double), processor.FormatExpression("Avg(Child({0}).TradeInInventoryAnalyzed)")));
            summaryTable.Columns.Add(new DataColumn("TradeInAvg", typeof(double), processor.FormatExpression
                ("IIF(Sum(Child({0}).[InventoryReceivedFactCount])>0,ISNULL(ISNULL(Sum(Child({0}).[TradeAnalyzedCount]),0.0)/Sum(Child({0}).[InventoryReceivedFactCount]),0.0),0.0)")));
            summaryTable.Columns.Add(new DataColumn("WholesaleMin", typeof(double), processor.FormatExpression("Min(Child({0}).AvgImmediateWholesaleProfitInPeriod)")));
            summaryTable.Columns.Add(new DataColumn("WholesaleMax", typeof(double), processor.FormatExpression("Max(Child({0}).AvgImmediateWholesaleProfitInPeriod)")));
            summaryTable.Columns.Add(new DataColumn("WholesaleAvg", typeof(double), processor.FormatExpression
                ("IIF(Sum(Child({0}).[Number_of_vehicles_sold_wholesale_in_days_0to29])>0,ISNULL(ISNULL(Sum(Child({0}).[Gross_for_vehicles_sold_wholesale_in_days_0to29]),0.0)/Sum(Child({0}).[Number_of_vehicles_sold_wholesale_in_days_0to29]),0.0),0.0)")));
            //summaryTable.Columns.Add(new DataColumn("WholesaleAvg", typeof(double), processor.FormatExpression("Avg(Child({0}).AvgImmediateWholesaleProfitInPeriod)")));
            summaryTable.Columns.Add(new DataColumn("TimePeriodId", typeof(string), string.Format(CultureInfo.InvariantCulture, "'{0}'", Parameters.TimePeriodId)));

            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                "TradeInMin", "TradeInMax", "TradeInAvg", "WholesaleMin", "WholesaleMax", "WholesaleAvg", "TimePeriodId").DefaultView;
        }
    }
}
