using System.Collections;
using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class OptimalInventoryGaugeDataService : OptimalInventoryDataService<OptimalInventoryGaugeDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];
            summaryTable.Columns.Add(new DataColumn("Min", typeof(double), processor.FormatExpression("Min(Child({0}).OptimalInventory)")));
            summaryTable.Columns.Add(new DataColumn("Max", typeof(double), processor.FormatExpression("Max(Child({0}).OptimalInventory)")));
            summaryTable.Columns.Add(new DataColumn("StoreAvg", typeof(double), processor.FormatExpression("Avg(Child({0}).OptimalInventory)")));
            summaryTable.Columns.Add(new DataColumn("GroupAvg", typeof(double), processor.FormatExpression("( Sum(Child({0}).OptimallyStockedUnits) / Sum(Child({0}).OptimalUnits) ) * 100.0")));
            summaryTable.Columns.Add(new DataColumn("Recommended", typeof(double), processor.FormatExpression("Sum(Child({0}).OptimalUnits)")));
            summaryTable.Columns.Add(new DataColumn("In-Stock", typeof(double), processor.FormatExpression("Sum(Child({0}).OptimallyStockedUnits)")));
            summaryTable.Columns.Add(new DataColumn("Out of Stock", typeof(double), processor.FormatExpression("Sum(Child({0}).OptimalUnits)-Sum(Child({0}).OptimallyStockedUnits)")));

            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                "Min", "Max", "StoreAvg", "GroupAvg", "Recommended", "In-Stock", "Out of Stock").DefaultView;
           
        }
    }
}
