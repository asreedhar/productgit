using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public abstract class InventoryPricingRiskDataService<TService> : AccessClassificationDataService<TService, InventoryPricingRiskParameters>
        where TService : InventoryPricingRiskDataService<TService>, new()
    {
        protected static readonly string[] RiskValues = new string[]
            {
                "UnderpricingRisk", "PricedToMarket", "OverpricingRisk", "NoPrice", "NotAnalyzed"
            };

        protected static readonly string[] RiskNames = new string[]
            {
                "Underpricing Risk", "Priced To Market", "Overpricing Risk", "Zero / No Price", "Not Analyzed"
            };

        private InventoryPricingRiskParameters _parameters = new InventoryPricingRiskParameters();

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            InventoryPricingRiskParameters parameters = new InventoryPricingRiskParameters();
            parameters.DealerGroup = (BusinessUnit)parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");

            object mode = parameterCallback("mode");
            if (mode != null)
            {
                parameters.Mode = (int)mode;
            }

            object riskIndex = parameterCallback("riskIndex");
            if (riskIndex != null)
            {
                parameters.RiskIndex = (int)riskIndex;
            }

            object saleStrategy = parameterCallback("saleStrategy");
            if (saleStrategy != null)
            {
                parameters.SaleStrategy = (string) saleStrategy;
            }

            Initialize(parameters);
        }

        protected override void Initialize(InventoryPricingRiskParameters parameters)
        {
            _parameters = parameters;
        }

        protected string ReportDataSetName()
        {
            string dataSetName;

            if (Parameters.Mode == 1)
            {
                dataSetName = "RiskSummary";
            }
            else if (Parameters.Mode == 2)
            {
                dataSetName = "RiskGraph";
            }
            else if (Parameters.Mode == 3)
            {
                dataSetName = "RiskTable";

                if (!Parameters.RiskIndex.HasValue)
                {
                    throw new InvalidOperationException("When Mode = 3 RiskIndex must be specified (valid values are 1 through 5 inclusive)");
                }
            }
            else
            {
                throw new InvalidOperationException("Mode not initialized");
            }

            return dataSetName;
        }

        protected override DataTable LoadDataTable()
        {
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = Parameters.DealerGroup.Id;
            parameterValues["Mode"] = Parameters.Mode;

            string dataSetName = ReportDataSetName();

            if (Parameters.Mode == 2 || Parameters.Mode == 3)
            {
                parameterValues["SaleStrategy"] = Parameters.SaleStrategy;
            }

            if (Parameters.Mode == 3)
            {
                parameterValues["RiskIndex"] = Parameters.RiskIndex.Value;
            }

            DataTable table = ReportHelper.LoadReportDataTable("E5C18A20-9108-43d1-88B9-7B35D6B6A8B5", dataSetName, parameterValues, new AccessClassificationProcessor.AccessClassificationCallback());

            table.TableName = dataSetName;

            return table;
        }

        protected override InventoryPricingRiskParameters Parameters
        {
            get { return _parameters; }
        }
    }
}
