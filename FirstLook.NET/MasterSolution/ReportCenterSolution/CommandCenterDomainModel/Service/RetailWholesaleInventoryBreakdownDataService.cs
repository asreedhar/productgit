using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class RetailWholesaleInventoryBreakdownDataService<TService> : AccessClassificationDataService<TService, RetailWholesaleInventoryBreakdownParameters>
        where TService : RetailWholesaleInventoryBreakdownDataService<TService>, new()
    {
        private RetailWholesaleInventoryBreakdownParameters _parameters;

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            RetailWholesaleInventoryBreakdownParameters parameters = new RetailWholesaleInventoryBreakdownParameters();
            parameters.DealerGroup = (BusinessUnit)parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");

            Initialize(parameters);
        }

        protected override void Initialize(RetailWholesaleInventoryBreakdownParameters parameters)
        {
            _parameters = parameters;
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            throw new NotImplementedException("The method or operation is not implemented on 'RetailWholesaleInventoryBreakdownDataService'.");
        }

        protected override RetailWholesaleInventoryBreakdownParameters Parameters
        {
            get { return _parameters; }
        }

        protected string ReportDataSetName
        {
            get
            {
                return "RetailWholesaleInventoryBreakdownDataSet";
            }
        }

        protected override DataTable LoadDataTable()
        {
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = "[Dealer].[Dealer Group Hierarchy].[Dealer Group].&[" + Parameters.DealerGroup.Id + "]";

            DataTable table = ReportHelper.LoadReportDataTable("8FEEDBF6-0179-463C-B3DD-B63D0F6ECC47", ReportDataSetName, parameterValues, new AccessClassificationProcessor.AccessClassificationCallback());
            
            table.TableName = ReportDataSetName;

            return table;
        }
    }
}
