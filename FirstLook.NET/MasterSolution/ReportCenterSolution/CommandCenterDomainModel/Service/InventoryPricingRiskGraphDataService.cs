using System.Collections;
using System.Data;
using System.Globalization;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class InventoryPricingRiskGraphDataService : InventoryPricingRiskDataService<InventoryPricingRiskSummaryDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            string reportDataSetName = ReportDataSetName();

            // extract the values

            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable dataTable = AccessClassificationProcessor.SubTable(
                dataSet.Tables[reportDataSetName],
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter());

            // create the risk table

            DataTable riskTable = RiskTable();

            // create a new data set

            DataSet set = new DataSet();
            set.Locale = dataSet.Locale;
            set.Tables.Add(dataTable);
            set.Tables.Add(riskTable);
            set.Relations.Add(
                new DataRelation(BucketRelationName(),
                                 riskTable.Columns[RiskTableIndexColumn],
                                 dataTable.Columns[RiskTableIndexColumn],
                                 false));

            riskTable.Columns.Add(new DataColumn("Units", typeof(double), Sum("Units")));
            riskTable.Columns.Add(new DataColumn("Percentage", typeof(double), Percent("Units", "Total")));

            return new DataView(riskTable).ToTable(
                false, new string[] { "RiskIndex", "RiskName", "Units", "Percentage" }).DefaultView;
        }

        private string _bucketRelationName;

        private string BucketRelationName()
        {
            if (string.IsNullOrEmpty(_bucketRelationName))
                _bucketRelationName = string.Format(CultureInfo.InvariantCulture, "{0}_Bucket", ReportDataSetName());
            return _bucketRelationName;
        }

        private string Sum(string columnName)
        {
            return string.Format(CultureInfo.InvariantCulture, "Sum(Child({0}).{1})", BucketRelationName(), columnName);
        }

        private string Percent(string numeratorColumnName, string denominatorColumnName)
        {
            return string.Format(CultureInfo.InvariantCulture, "Sum(Child({0}).{1}) / Sum(Child({0}).{2}) * 100", BucketRelationName(), numeratorColumnName, denominatorColumnName);
        }

        private const string RiskTableName = "RiskValues";
        private const string RiskTableNameColumn = "RiskName";
        private const string RiskTableIndexColumn = "RiskIndex";

        private static DataTable RiskTable()
        {
            DataTable table = new DataTable(RiskTableName);
            table.Locale = CultureInfo.InvariantCulture;
            table.Columns.Add(RiskTableNameColumn, typeof(string));
            table.Columns.Add(RiskTableIndexColumn, typeof(byte));

            for (int i = 0, l = RiskNames.Length; i < l; i++)
            {
                DataRow row = table.NewRow();
                row[RiskTableIndexColumn] = (byte) i + 1;
                row[RiskTableNameColumn] = RiskNames[i];
                table.Rows.Add(row);
            }

            return table;
        }
    }
}
