using System.Collections;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class InventorySalesSummaryTableDataService : InventorySalesSummaryDataService<InventorySalesSummaryTableDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            return CreateProcessor(AccessClassificationProcessor.checkSalesSummaryColumn).ProcessDrillthrough(arguments.SortExpression);
        }

        public override bool CanSort
        {
            get { return true; }
        }
    }
}
