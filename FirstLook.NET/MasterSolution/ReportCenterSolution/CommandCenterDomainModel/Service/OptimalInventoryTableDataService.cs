using System.Collections;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class OptimalInventoryTableDataService : OptimalInventoryDataService<OptimalInventoryGaugeDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            return CreateProcessor().ProcessDrillthrough(arguments.SortExpression);
        }

        public override bool CanSort
        {
            get { return true; }
        }
    }
}
