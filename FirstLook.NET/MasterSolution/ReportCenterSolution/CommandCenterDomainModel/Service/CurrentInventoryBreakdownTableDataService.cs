using System.Collections;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class CurrentInventoryBreakdownTableDataService : CurrentInventoryBreakdownDataService<CurrentInventoryBreakdownTableDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            return CreateProcessor(CurrentInventoryBreakdownTableDataService.GetColumnFormat).ProcessDrillthrough(arguments.SortExpression);
        }

        public override bool CanSort
        {
            get { return true; }
        }

        protected override void Initialize(CurrentInventoryBreakdownParameters parameters)
        {
            parameters.IsGraph = false;
            parameters.IsSummary = false;

            base.Initialize(parameters);
        }

        protected static string GetColumnFormat(string columnName)
        {
            if (columnName.Equals("Inventory_Count_Percentage"))
            {
                return "Sum(Child({0}).Inventory_Count) / Sum(Child({0}).Dealer_Inventory_Count) * 100";
            }
            else if (columnName.Equals("Average_Cost_Per_Unit"))
            {
                return "Sum(Child({0}).Unit_Cost) / Sum(Child({0}).Inventory_Count)";
            }
            else if (columnName.Equals("Unit_Cost")
                        || columnName.Equals("Inventory_Count")
                        || columnName.Equals("BookVsCost"))
            {
                return "Sum(Child({0}).{1})";
            }
            else
            {
                return AccessClassificationProcessor.GetDefaultNumericColumnFormat(columnName);
            }
        }
    }
}
