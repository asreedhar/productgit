using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.CommandCenter.DomainModel.Service;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class OptimalInventoryDataService<TService> : AccessClassificationDataService<TService, OptimalInventoryParameters>
        where TService : OptimalInventoryDataService<TService>, new()
    {
        private OptimalInventoryParameters _parameters;

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            OptimalInventoryParameters parameters = new OptimalInventoryParameters();
            parameters.DealerGroup = (BusinessUnit)parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");
            Initialize(parameters);
        }

        protected override void Initialize(OptimalInventoryParameters parameters)
        {
            _parameters = parameters;
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            throw new NotImplementedException("The method or operation is not implemented on 'OptimalInventoryDataService.GetData'.");
        }

        protected override OptimalInventoryParameters Parameters
        {
            get { return _parameters; }
        }

        protected override DataTable LoadDataTable()
        {
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = "[Dealer].[Dealer Group Hierarchy].[Dealer Group].&[" + Parameters.DealerGroup.Id + "]";
            DataTable dataTable = null;

            try
            {
                dataTable = ReportAnalyticsClient.GetReportData<DataTable>(ReportAnalyticsClient.GetParameterList(parameterValues));
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }

            if (dataTable == null)
            {
                dataTable = ReportHelper.LoadReportDataTable(
                   "E6B1F6F1-DB36-45ac-BA32-359025BACB3A",
                   "OptimalInventoryDataSet",
                   parameterValues,
                   new AccessClassificationProcessor.AccessClassificationCallback());

                dataTable.TableName = "OptimalInventoryDataSet";
                try
                {
                    ReportAnalyticsClient.SetReportData(ReportAnalyticsClient.GetParameterList(parameterValues), dataTable);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }
            }
            return dataTable;
        }
    }
}
