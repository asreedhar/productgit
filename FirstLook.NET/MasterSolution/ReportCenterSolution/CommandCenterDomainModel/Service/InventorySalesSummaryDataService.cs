using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class InventorySalesSummaryDataService<TService> : AccessClassificationDataService<TService, InventorySalesSummaryParameters>
        where TService : InventorySalesSummaryDataService<TService>, new()
    {
        private InventorySalesSummaryParameters _parameters;

        public override void Initialize(DataServiceParameterCallback parameterCallback)
        {
            InventorySalesSummaryParameters parameters = new InventorySalesSummaryParameters();
            parameters.DealerGroup = (BusinessUnit)parameterCallback("dealerGroup");
            parameters.Dealers = (IEnumerable<BusinessUnit>)parameterCallback("dealers");
            parameters.Member = (Member)parameterCallback("member");
            parameters.TimePeriodId = (string)parameterCallback("timePeriodId");
            Initialize(parameters);
        }

        protected override void Initialize(InventorySalesSummaryParameters parameters)
        {
            _parameters = parameters;
        }

        protected override InventorySalesSummaryParameters Parameters
        {
            get { return _parameters; }
        }

        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            throw new NotImplementedException("The method or operation is not implemented on 'InventorySalesSummaryDataService.GetData'.");
        }

        protected override DataTable LoadDataTable()
        {
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerGroupID"] = "[Dealer].[Dealer Group Hierarchy].[Dealer Group].&[" + Parameters.DealerGroup.Id + "]";
            parameterValues["TimePeriodID"] = Parameters.TimePeriodId;
            DataTable dataTable = null;

            try
            {
                dataTable = ReportAnalyticsClient.GetReportData<DataTable>(ReportAnalyticsClient.GetParameterList(parameterValues));
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
            if (dataTable == null)
            {
                dataTable = ReportHelper.LoadReportDataTable(
                   "C0531E20-220F-4f8a-BC67-BBD633B5B1E9",
                   "InventorySalesSummaryDataSet",
                   parameterValues,
                   new AccessClassificationProcessor.AccessClassificationCallback());

                dataTable.TableName = "InventorySalesSummaryDataSet";
                try
                {
                    ReportAnalyticsClient.SetReportData(ReportAnalyticsClient.GetParameterList(parameterValues), dataTable);
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }
            }
            return dataTable;
        }
    }
}
