using System.Collections;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalReviewTableDataService : AppraisalReviewDataService<AppraisalReviewTableDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            return CreateProcessor(AppraisalReviewTableDataService.GetColumnFormat).ProcessDrillthrough(arguments.SortExpression);
        }

        public override bool CanSort
        {
            get { return true; }
        }

        protected static string GetColumnFormat(string columnName)
        {
            if (columnName.Equals("NumberOfAppraisalReviews")
                || columnName.Equals("NumberOfAppraisals")
                || columnName.Equals("NumberOfAppraisalReviewsPrevious")
                || columnName.Equals("NumberOfAppraisalsPrevious"))
            {
                return "Sum(Child({0}).{1})";
            }
            else
            {
                return AccessClassificationProcessor.GetDefaultNumericColumnFormat(columnName);
            }
        }
    }
}
