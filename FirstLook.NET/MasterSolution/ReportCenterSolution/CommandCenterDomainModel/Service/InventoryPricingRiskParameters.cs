using System;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class InventoryPricingRiskParameters : CoreDataServiceParameters
    {
        private int  mode;
        private int? riskIndex;
        private string saleStrategy;

        public int Mode
        {
            get { return mode; }
            set
            {
                if (value < 1 || value > 3)
                    throw new ArgumentOutOfRangeException("value", value, "Must be 1, 2 or 3");
                mode = value;
            }
        }

        public int? RiskIndex
        {
            get { return riskIndex; }
            set
            {
                if (value < 1 || value > 5)
                    throw new ArgumentOutOfRangeException("value", value, "Must be between 1 and 5");
                riskIndex = value;
            }
        }

        public string SaleStrategy
        {
            get { return saleStrategy; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("value", "SaleStrategy must be 'A' or 'R' (not null)");
                if (!(string.Compare("A", value) == 0 || string.Compare("R", value) == 0))
                    throw new ArgumentOutOfRangeException("value", value, "SaleStrategy must be 'A' or 'R'");
                saleStrategy = value;
            }
        }
    }
}
