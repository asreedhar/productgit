using System.Collections;
using System.Data;
using System.Globalization;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalClosingRateGaugeDataService : AppraisalClosingRateDataService<AppraisalClosingRateGaugeDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];
            summaryTable.Columns.Add(new DataColumn("Min", typeof(double), processor.FormatExpression("Min(Child({0}).ClosingRate)")));
            summaryTable.Columns.Add(new DataColumn("Max", typeof(double), processor.FormatExpression("Max(Child({0}).ClosingRate)")));
            summaryTable.Columns.Add(new DataColumn("Target", typeof(double), "50"));
            summaryTable.Columns.Add(new DataColumn("TimePeriodId", typeof(string), string.Format(CultureInfo.InvariantCulture, "'{0}'", Parameters.TimePeriodId)));
            summaryTable.Columns.Add(new DataColumn("Appraisal", typeof(double), processor.FormatExpression("Sum(Child({0}).[Appraisal_Fact_Count_In_Period])")));
            summaryTable.Columns.Add(new DataColumn("ClosedAppraisal", typeof(double), processor.FormatExpression("Sum(Child({0}).[Total_Closed_Appraisal_Count])")));
            summaryTable.Columns.Add(new DataColumn("NotClosedAppraisal", typeof(double), processor.FormatExpression("Sum(Child({0}).[Total_Not_Closed_Appraisal_Count])")));
            summaryTable.Columns.Add(new DataColumn("Avg", typeof(double), processor.FormatExpression("(Sum(Child({0}).[Total_Closed_Appraisal_Count])/Sum(Child({0}).[Appraisal_Fact_Count_In_Period])) * 100.0")));
            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                "Min", "Max", "Target", "Avg", "TimePeriodId", "Appraisal", "ClosedAppraisal", "NotClosedAppraisal").DefaultView;
        }
    }
}
