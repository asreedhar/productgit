using System.Diagnostics;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalReviewParameters : CoreDataServiceParameters
    {
        private int daysBack = 1;

        public int DaysBack
        {
            get { return daysBack; }
            set 
            {
                Debug.Assert(daysBack >= 1, "The number of days back is expected to be greater than or equal to 1!");
                daysBack = value;
            }
        }
    }
}
