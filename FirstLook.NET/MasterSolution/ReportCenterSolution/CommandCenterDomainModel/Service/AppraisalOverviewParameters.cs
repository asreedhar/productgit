namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class AppraisalOverviewParameters : CoreDataServiceParameters
    {
        private string timePeriodId;
        private string mode;

        public string TimePeriodId
        {
            get { return timePeriodId; }
            set { timePeriodId = value; }
        }

        public string Mode
        {
            get { return mode; }
            set { mode = value; }
        }
    }
}
