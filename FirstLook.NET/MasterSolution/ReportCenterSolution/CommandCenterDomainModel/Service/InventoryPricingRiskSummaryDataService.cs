using System.Collections;
using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.CommandCenter.DomainModel.Service
{
    public class InventoryPricingRiskSummaryDataService : InventoryPricingRiskDataService<InventoryPricingRiskSummaryDataService>
    {
        public override IEnumerable GetData(DataServiceArguments arguments)
        {
            AccessClassificationProcessor processor = CreateProcessor();

            DataSet dataSet = processor.FillDataSet();

            DataTable summaryTable = dataSet.Tables[AccessClassificationProcessor.AccessClassificationTableName];

            string[] columns = new string[RiskValues.Length*2];

            for (int i = 0, l = RiskValues.Length; i < l; i++)
            {
                int x = 2*i, y = 2*i + 1;

                columns[x] = "Percentage_" + RiskValues[i];
                columns[y] = "Units_" + RiskValues[i];

                summaryTable.Columns.Add(new DataColumn(columns[x], typeof(double), processor.FormatExpression("Sum(Child({0}).{1}) / Sum(Child({0}).Units)", columns[y])));
                summaryTable.Columns.Add(new DataColumn(columns[y], typeof(double), processor.FormatExpression("Sum(Child({0}).{1})", columns[y])));
            }

            return AccessClassificationProcessor.SubTable(
                summaryTable,
                AccessClassificationProcessor.AccessibleAndSelectedRowFilter(),
                columns).DefaultView;
        }
    }
}
