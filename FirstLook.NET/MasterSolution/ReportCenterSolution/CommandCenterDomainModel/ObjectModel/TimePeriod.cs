namespace FirstLook.CommandCenter.DomainModel.ObjectModel
{
    public class TimePeriod
    {
        public TimePeriod()
        {
            _name = "";
            _value = "";
        }

        public TimePeriod(string name, string value)
        {
            _name = name;
            _value = value;
        }

        private string _name;
        private string _value;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

}
