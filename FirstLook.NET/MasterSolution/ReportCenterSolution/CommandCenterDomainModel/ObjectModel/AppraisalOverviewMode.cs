using System.Diagnostics.CodeAnalysis;

namespace FirstLook.CommandCenter.DomainModel.ObjectModel
{
    public enum AppraisalOverviewMode
    {
        TradeInInventoryAnalyzed,

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Imm")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Avg")]
        AvgImmWholesaleProfit
    } 
}
