using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Web.Services;
using System.ComponentModel;
using System.Xml;
using NADAVehicle_Data;

namespace NadaWebService
{
    [WebService(Namespace = "http://www.firstlook.biz/NadaWebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class NadaWebService : WebService
    {
        private readonly string _ConnString;

        private readonly string _XMLConfig;

        private int _period;

        private readonly NADAVehicle_System.Vehicle _vehicleSystem;
  
        public NadaWebService()
        {
            _XMLConfig = ConfigurationManager.AppSettings["NADAProcedureConfig"];
            _ConnString = ConfigurationManager.ConnectionStrings["VehicleUC"].ConnectionString;
            _vehicleSystem = new NADAVehicle_System.Vehicle(_ConnString, _XMLConfig);
        }

        #region Public WebService Methods

        [WebMethod]
        public XmlNode GetNADARegions()
        {
            return new XmlDataDocument(_vehicleSystem.getRegionStates(LatestPeriod()));
        }

        [WebMethod]
        public string GetNADAPublishPeriod()
        {
            return LatestPeriod().ToString();
        }

        [WebMethod]
        public XmlNode DoVinLookup(String vin)
        {
            return new XmlDataDocument(_vehicleSystem.getVehiclesFromVin(LatestPeriod(), vin));    
        }

        [WebMethod]
        public XmlNode GetEquipmentOptions(int regionId, int uid, String vin)
        {
            int period = LatestPeriod();
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getVehicleAccessory(period, uid, regionId);
            VehicleUI_Data vinSpecific = _vehicleSystem.getVinVehicleAccessory(_currentVehicleDataStore, period, vin, uid);
            return new XmlDataDocument(vinSpecific);
        }

        [WebMethod]
        public decimal GetMileageAdjustment(int uid, int mileage, int regionId)
        {
            decimal mileageAdj;

            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getVehicles(LatestPeriod(), uid);
           DataRow dr_Vehicle = _currentVehicleDataStore.Tables[VehicleUI_Data.VEHICLES_TABLE].Rows[0];
            _currentVehicleDataStore.Tables[VehicleUI_Data.VEHICLE_VALUES_TABLE].Clear();
            _currentVehicleDataStore = _vehicleSystem.getVehicleValues(_currentVehicleDataStore, LatestPeriod(), uid, regionId);

             _currentVehicleDataStore.Tables[VehicleUI_Data.MILEAGE_VALUES_TABLE].Clear();
             _currentVehicleDataStore = _vehicleSystem.getVehicleMileage(_currentVehicleDataStore, LatestPeriod(), (int)dr_Vehicle[VehicleUI_Data.VEHICLE_YEAR_FIELD], dr_Vehicle[VehicleUI_Data.MILEAGE_CODE_FIELD].ToString());

            if (mileage > 0)
            {
                mileageAdj = NADAVehicle_System.Vehicle.getMileageAdj(_currentVehicleDataStore, mileage);
            }
            else
            {
                mileageAdj = 0;
            }

            return mileageAdj;
        }

 
        [WebMethod]
        public XmlNode GetValues(int uid, int mileage, int regionId)
        {
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getVehicleValues(LatestPeriod(), uid, regionId);
            return new XmlDataDocument(_currentVehicleDataStore);
        }

        #region Manual Bookout Methods

        [WebMethod]
        public XmlNode GetYears()
        {
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getYears(LatestPeriod());
            return new XmlDataDocument(_currentVehicleDataStore);
        }

        [WebMethod]
        public XmlNode GetMakes(int year)
        {
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getMakes(LatestPeriod(), year);
            return new XmlDataDocument(_currentVehicleDataStore);
        }

        [WebMethod]
        public XmlNode GetModel(int year, int makeId)
        {
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getSeries(LatestPeriod(), year, makeId);
            return new XmlDataDocument(_currentVehicleDataStore);
        }

        [WebMethod]
        public XmlNode GetTrim(int year, int makeId, int modelId)
        {
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getBodies(LatestPeriod(), year, makeId, modelId);
            return new XmlDataDocument(_currentVehicleDataStore);
        }

        [WebMethod]
        public XmlNode GetEquipmentOptions_NoVin(int trimId, int regionId)
        {
            int period = LatestPeriod();
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getVehicleAccessory(period, trimId, regionId);
            return new XmlDataDocument(_currentVehicleDataStore);
        }

        [WebMethod]
        public XmlNode GetValues_NoVin(int trimId, int regionId, int mileage)
        {
            VehicleUI_Data _currentVehicleDataStore = _vehicleSystem.getVehicles(LatestPeriod(), trimId);
            DataRow dr_Vehicle = _currentVehicleDataStore.Tables[VehicleUI_Data.VEHICLES_TABLE].Rows[0];
            _currentVehicleDataStore.Tables[VehicleUI_Data.VEHICLE_VALUES_TABLE].Clear();
            _currentVehicleDataStore = _vehicleSystem.getVehicleValues(_currentVehicleDataStore, LatestPeriod(), (int)dr_Vehicle[VehicleUI_Data.UID_FIELD], regionId);
        
            return new XmlDataDocument(_currentVehicleDataStore);
        }

        /// <summary>
        /// </summary>
        /// <param name="uid">bodyID or trimID are the same as uid - March 2009</param>
        /// <returns></returns>
        [WebMethod]
        public XmlNode GetMutuallyExclusiveOptionsList(int uid)
        {
            VehicleUI_Data _currenctVehicleDataStore = _vehicleSystem.getVehicleAccessoryMultExcludes(LatestPeriod(), uid);
            return new XmlDataDocument(_currenctVehicleDataStore);
        }

        [WebMethod]
        public XmlNode GetPackageIncludedOptionsList(int uid)
        {
            VehicleUI_Data _currenctVehicleDataStore = _vehicleSystem.getVehicleAccessoryPackageIncludes(LatestPeriod(), uid);
            return new XmlDataDocument(_currenctVehicleDataStore);
        }

        #endregion

        #endregion

        #region Period Util
        private int LatestPeriod()
        {
            if (_period == 0)
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["VehicleUC"];
                string providerName = string.IsNullOrEmpty(settings.ProviderName) ? "System.Data.SqlClient" : settings.ProviderName;
                DbProviderFactory factory = DbProviderFactories.GetFactory(providerName);
                using (IDbConnection conn = factory.CreateConnection())
                {
                    conn.ConnectionString = settings.ConnectionString;
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    using (IDbCommand command = conn.CreateCommand())
                    {
                        command.CommandText = "SELECT MAX(Period) Period FROM dbo.Vehicles";
                        command.CommandType = CommandType.Text;
                        using(IDataReader reader = command.ExecuteReader())
                        {
                            if(reader.Read())
                            {
                                _period = reader.GetInt32(reader.GetOrdinal("Period"));
                            }
                        }
                    }
                }
            }

            if (_period == 0)
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["Period"]);
            }
            else
            {
                return _period;
            }
        }
        #endregion
    }
}
