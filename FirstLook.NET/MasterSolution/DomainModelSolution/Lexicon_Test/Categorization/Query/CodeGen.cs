using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class CodeGen
	{
		private readonly DataSet _catalogDataSet;

		public CodeGen(DataSet catalogDataSet)
		{
			_catalogDataSet = catalogDataSet.Copy();

			SetUpPrimaryKeys();

			SetUpForeignKeys();
		}

		protected internal DataSet CatalogDataSet
		{
			get { return _catalogDataSet; }
		}

		private void SetUpPrimaryKeys()
		{
            AddPrimaryKey("ModelYear");
            AddPrimaryKey("Make");
            AddPrimaryKey("Line");
            AddPrimaryKey("Segment");
            AddPrimaryKey("BodyType");
            AddPrimaryKey("ModelFamily");
            AddPrimaryKey("VinPattern");
            AddPrimaryKey("Series");
            AddPrimaryKey("Model");
            AddPrimaryKey("PassengerDoor");
			AddPrimaryKey("Engine");
			AddPrimaryKey("FuelType");
			AddPrimaryKey("DriveTrain");
			AddPrimaryKey("Transmission");
            AddPrimaryKey("Configuration");
            AddPrimaryKey("ModelConfiguration");

            DataTable mcm = CatalogDataSet.Tables["ModelConfigurationMapping"];

            mcm.PrimaryKey = new DataColumn[] { mcm.Columns["CatalogEntryId"] };

            DataTable vpm = CatalogDataSet.Tables["ModelConfigurationMapping"];

            vpm.PrimaryKey = new DataColumn[] { mcm.Columns["VinPatternId"], mcm.Columns["ModelConfigurationId"] };
		}

		private void AddPrimaryKey(string tableName)
		{
			DataTable table = CatalogDataSet.Tables[tableName];

			table.PrimaryKey = new DataColumn[] { table.Columns["Id"] };
		}

		private void SetUpForeignKeys()
		{
            // model

			AddForeignKey("Make", "Model", true);
            AddForeignKey("Line", "Model", true);
            AddForeignKey("ModelFamily", "Model", true);
            AddForeignKey("Series", "Model", true);
            AddForeignKey("Segment", "Model", true);
            AddForeignKey("BodyType", "Model", true);

            // configuration

            AddForeignKey("PassengerDoor", "Configuration", true);
            AddForeignKey("Engine", "Configuration", true);
            AddForeignKey("FuelType", "Configuration", true);
            AddForeignKey("DriveTrain", "Configuration", true);
            AddForeignKey("Transmission", "Configuration", true);

            // model configuration

            AddForeignKey("Model", "ModelConfiguration", false);
            AddForeignKey("Configuration", "ModelConfiguration", false);

            // vin pattern mapping

            AddForeignKey("Model", "VinPatternMapping", false);
            AddForeignKey("Configuration", "VinPatternMapping", false);
            AddForeignKey("ModelConfiguration", "VinPatternMapping", false);
		}

		private void AddForeignKey(string parentTableName, string childTableName, bool copyName)
		{
			DataTable parent = CatalogDataSet.Tables[parentTableName];

			DataTable child = CatalogDataSet.Tables[childTableName];

			string relationshipName = parentTableName + "_" + childTableName;

			CatalogDataSet.Relations.Add(
				new DataRelation(relationshipName,
								 parent.Columns["Id"],
								 child.Columns[parentTableName + "Id"],
								 false));

            if (copyName)
            {
                child.Columns.Add(parentTableName, typeof (string), "Parent(" + relationshipName + ").Name");
            }
		}

		public CodeCompileUnit CreateTestCases()
		{
			CodeCompileUnit compileUnit = new CodeCompileUnit();

			CodeNamespace testNamespace = new CodeNamespace("FirstLook.DomainModel.Lexicon.Categorization.Query");

			compileUnit.Namespaces.Add(testNamespace);

			testNamespace.Imports.Add(new CodeNamespaceImport("System"));
			testNamespace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
			testNamespace.Imports.Add(new CodeNamespaceImport("System.Data"));
			testNamespace.Imports.Add(new CodeNamespaceImport("NUnit.Framework"));

			string testClassName = string.Format(
				CultureInfo.InvariantCulture,
				"Graph{0}{1}{2}",
				CatalogDataSet.Tables["ModelYear"].Rows[0]["Name"],
				CatalogDataSet.Tables["Make"].Rows[0]["Name"],
				CatalogDataSet.Tables["Line"].Rows[0]["Name"]);

			testClassName = Regex.Replace(testClassName, "[^0-9a-zA-Z_]", string.Empty);

			string testDataSetName = string.Format(
				CultureInfo.InvariantCulture,
				"DataSet-{0}{1}{2}",
				CatalogDataSet.Tables["ModelYear"].Rows[0]["Name"],
				CatalogDataSet.Tables["Make"].Rows[0]["Name"],
				CatalogDataSet.Tables["Line"].Rows[0]["Name"]);

			testDataSetName = Regex.Replace(testDataSetName, "[^0-9a-zA-Z_-]", string.Empty);

			CodeTypeDeclaration testClass = new CodeTypeDeclaration(testClassName);

			testNamespace.Types.Add(testClass);

			testClass.CustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference("TestFixture")));
			testClass.Members.Add(new CodeMemberField("Categorization.Catalog", "catalog"));
			testClass.Members.Add(CreateTestFixtureSetUp(testDataSetName));
			testClass.Members.Add(CreateTestFixtureTearDown());
			testClass.Members.AddRange(CreatePartitionTests());
			testClass.Members.Add(CreateGraphNodeDiscriminatorPredicateClass());

			return compileUnit;
		}

		private static CodeMemberMethod CreateTestFixtureSetUp(string testDataSetName)
		{
			CodeMemberMethod testFixtureSetUp = new CodeMemberMethod();
			testFixtureSetUp.Name = "FixtureSetUp";
			testFixtureSetUp.Attributes = MemberAttributes.Public;
			testFixtureSetUp.CustomAttributes.Add(
				new CodeAttributeDeclaration(
					new CodeTypeReference("TestFixtureSetUp")));
			testFixtureSetUp.Statements.Add(
				new CodeVariableDeclarationStatement(
					new CodeTypeReference("DataSet"),
					"catalogDataSet",
					new CodeObjectCreateExpression(
						"System.Data.DataSet",
						new CodePrimitiveExpression("Categorization"))));
			testFixtureSetUp.Statements.Add(
				new CodeMethodInvokeExpression(
					new CodeVariableReferenceExpression("catalogDataSet"),
					"ReadXml",
					new CodePrimitiveExpression(testDataSetName + ".xml")));
			testFixtureSetUp.Statements.Add(
				new CodeVariableDeclarationStatement(
					new CodeTypeReference(typeof(int)),
					"modelYearId",
					new CodeCastExpression(
						typeof(int),
						new CodeIndexerExpression(
							new CodeIndexerExpression(
								new CodePropertyReferenceExpression(
									new CodeIndexerExpression(
										new CodePropertyReferenceExpression(
											new CodeVariableReferenceExpression("catalogDataSet"),
											"Tables"),
										new CodePrimitiveExpression("ModelYear")),
									"Rows"),
								new CodePrimitiveExpression(0)),
							new CodePrimitiveExpression("Id")))));
			testFixtureSetUp.Statements.Add(
				new CodeVariableDeclarationStatement(
					new CodeTypeReference(typeof(int)),
					"makeId",
					new CodeCastExpression(
						typeof(int),
						new CodeIndexerExpression(
							new CodeIndexerExpression(
								new CodePropertyReferenceExpression(
									new CodeIndexerExpression(
										new CodePropertyReferenceExpression(
											new CodeVariableReferenceExpression("catalogDataSet"),
											"Tables"),
										new CodePrimitiveExpression("Make")),
									"Rows"),
								new CodePrimitiveExpression(0)),
							new CodePrimitiveExpression("Id")))));
			testFixtureSetUp.Statements.Add(
				new CodeVariableDeclarationStatement(
					new CodeTypeReference(typeof(int)),
					"lineId",
					new CodeCastExpression(
						typeof(int),
						new CodeIndexerExpression(
							new CodeIndexerExpression(
								new CodePropertyReferenceExpression(
									new CodeIndexerExpression(
										new CodePropertyReferenceExpression(
											new CodeVariableReferenceExpression("catalogDataSet"),
											"Tables"),
										new CodePrimitiveExpression("Line")),
									"Rows"),
								new CodePrimitiveExpression(0)),
							new CodePrimitiveExpression("Id")))));
			testFixtureSetUp.Statements.Add(
				new CodeAssignStatement(
					new CodeFieldReferenceExpression(
						new CodeThisReferenceExpression(),
						"catalog"),
					new CodeMethodInvokeExpression(
						new CodeTypeReferenceExpression("Catalog"),
						"GetCatalog",
						new CodeExpression[]
							{
								new CodeVariableReferenceExpression("modelYearId"),
								new CodeVariableReferenceExpression("makeId"),
								new CodeVariableReferenceExpression("lineId"),
								new CodeMethodInvokeExpression(
									new CodeVariableReferenceExpression("catalogDataSet"),
									"CreateDataReader",
									new CodeExpression[0])
							})));
			return testFixtureSetUp;
		}

		private static CodeMemberMethod CreateTestFixtureTearDown()
		{
			CodeMemberMethod testFixtureTearDown = new CodeMemberMethod();
			testFixtureTearDown.Name = "FixtureTearDown";
			testFixtureTearDown.Attributes = MemberAttributes.Public;
			testFixtureTearDown.CustomAttributes.Add(
				new CodeAttributeDeclaration(
					new CodeTypeReference("TestFixtureTearDown")));
			testFixtureTearDown.Statements.Add(
				new CodeAssignStatement(
					new CodeFieldReferenceExpression(
						new CodeThisReferenceExpression(),
						"catalog"),
					new CodePrimitiveExpression(null)));
			return testFixtureTearDown;
		}

        /* private CodeTypeMemberCollection CreatePartialOrderTests()
        {
            CodeTypeMemberCollection tests = new CodeTypeMemberCollection();

            DataTable vinPatterns = CatalogDataSet.Tables["VinPattern"];

            foreach (DataRow row in vinPatterns.Rows)
            {
                string vinPattern = (string) row["VinPattern"];

                List<int> candidate = new List<int>();

                List<int> complement = new List<int>();

                GetPartialOrder(vinPattern, candidate, complement);

                int partitionCount = 0;

                if (candidate.Count > 0) partitionCount++;

                if (complement.Count > 0) partitionCount++;

                CodeMemberMethod testPartialOrder = new CodeMemberMethod();
                testPartialOrder.Name = "VinPattern_" + vinPattern + "PartialOrder";
                testPartialOrder.Attributes = MemberAttributes.Public;
                testPartialOrder.CustomAttributes.Add(
                    new CodeAttributeDeclaration(
                        new CodeTypeReference("Test")));
                testPartialOrder.Statements.Add(
                    new CodeVariableDeclarationStatement(
                        "Categorization.VinPattern",
                        "item",
                        new CodeMethodInvokeExpression(
                            new CodePropertyReferenceExpression(
                                new CodeFieldReferenceExpression(
                                    new CodeThisReferenceExpression(),
                                    "catalog"),
                                "VinPatterns"),
                            "GetItem",
                            new CodePrimitiveExpression(vinPattern)))
                    );
                testPartialOrder.Statements.Add(
                    new CodeMethodInvokeExpression(
                        new CodeTypeReferenceExpression("Assert"),
                        "IsNotNull",
                        new CodeVariableReferenceExpression("item")));
                testPartialOrder.Statements.Add(
                    new CodeMethodInvokeExpression(
                        new CodeTypeReferenceExpression("Assert"),
                        "IsNotNull",
                        new CodePropertyReferenceExpression(
                            new CodeVariableReferenceExpression("item"),
                            "ModelConfiguration")));
                testPartialOrder.Statements.Add(
                    new CodeVariableDeclarationStatement(
                        "Graph",
                        "graph",
                        new CodeObjectCreateExpression(
                            "Graph",
                            new CodeFieldReferenceExpression(
                                new CodeThisReferenceExpression(),
                                "catalog"))));
                testPartialOrder.Statements.Add(
                    new CodeMethodInvokeExpression(
                        new CodeVariableReferenceExpression("graph"),
                        "PartialOrder",
                        new CodeVariableReferenceExpression("item"),
                        new CodePropertyReferenceExpression(
                            new CodeVariableReferenceExpression("item"),
                            "ModelConfiguration")));
                testPartialOrder.Statements.Add(
                    new CodeVariableDeclarationStatement(
                        "List<GraphNode>",
                        "nodes",
                        new CodeMethodInvokeExpression(
                            new CodeVariableReferenceExpression("graph"),
                            "GatherLeaves")));
                testPartialOrder.Statements.Add(
                    new CodeMethodInvokeExpression(
                        new CodeTypeReferenceExpression("Assert"),
                        "IsNotNull",
                        new CodeVariableReferenceExpression("nodes")));
                testPartialOrder.Statements.Add(
                    new CodeMethodInvokeExpression(
                        new CodeTypeReferenceExpression("Assert"),
                        "AreEqual",
                        new CodePrimitiveExpression(partitionCount),
                        new CodePropertyReferenceExpression(
                            new CodeVariableReferenceExpression("nodes"),
                            "Count")));

                testPartialOrder.Statements.AddRange(AddPartitionVariables("candidate", "Candidate"));

                testPartialOrder.Statements.AddRange(AddPartitionVariables("complement", "Complement"));

                testPartialOrder.Statements.Add(new CodeVariableDeclarationStatement(typeof(int), "i"));

                testPartialOrder.Statements.Add(new CodeVariableDeclarationStatement(typeof(int), "l"));

                AddPartialOrderTest(candidate, testPartialOrder, "candidate", true);

                AddPartialOrderTest(complement, testPartialOrder, "complement", false);
				
                tests.Add(testPartialOrder);
            }

            return tests;
        }

        private static void AddPartialOrderTest(IList<int> catalogIds, CodeMemberMethod testPartialOrder, string variableName, bool isCandidate)
        {
            if (catalogIds.Count > 0)
            {
                CodePrimitiveExpression[] ids = new CodePrimitiveExpression[catalogIds.Count];

                for (int i = 0, l = ids.Length; i < l; i++)
                {
                    ids[i] = new CodePrimitiveExpression(catalogIds[i]);
                }

                testPartialOrder.Statements.Add(
                    new CodeVariableDeclarationStatement(
                        typeof (int[]),
                        variableName + "CatalogIds",
                        new CodeArrayCreateExpression(
                            typeof (int),
                            ids)));

                testPartialOrder.Statements.Add(
                    new CodeAssignStatement(
                        new CodeVariableReferenceExpression("l"),
                        new CodePrimitiveExpression(ids.Length)));

                testPartialOrder.Statements.Add(
                    new CodeIterationStatement(
                        new CodeAssignStatement(
                            new CodeVariableReferenceExpression("i"),
                            new CodePrimitiveExpression(0)),
                        new CodeBinaryOperatorExpression(
                            new CodeVariableReferenceExpression("i"),
                            CodeBinaryOperatorType.LessThan,
                            new CodeVariableReferenceExpression("l")),
                        new CodeAssignStatement(
                            new CodeVariableReferenceExpression("i"),
                            new CodeBinaryOperatorExpression(
                                new CodeVariableReferenceExpression("i"),
                                CodeBinaryOperatorType.Add,
                                new CodePrimitiveExpression(1))),
                        new CodeStatement[]
                            {
                                new CodeVariableDeclarationStatement(
                                    "ModelConfiguration",
                                    "configuration",
                                    new CodeMethodInvokeExpression(
                                            new CodeFieldReferenceExpression(
                                                new CodeThisReferenceExpression(),
                                                "catalog"),
                                            "FindModelConfiguration",
                                            new CodeArrayIndexerExpression(
                                                new CodeVariableReferenceExpression(variableName + "CatalogIds"),
                                                new CodeVariableReferenceExpression("i")))),
                                new CodeExpressionStatement(
                                    new CodeMethodInvokeExpression(
                                        new CodeTypeReferenceExpression("Assert"),
                                        isCandidate ? "IsTrue" : "IsFalse",
                                        new CodeMethodInvokeExpression(
                                            new CodeVariableReferenceExpression("candidateConfigurations"),
                                            "Contains",
                                            new CodeVariableReferenceExpression("configuration")))),
                                new CodeExpressionStatement(
                                    new CodeMethodInvokeExpression(
                                        new CodeTypeReferenceExpression("Assert"),
                                        isCandidate ? "IsFalse" : "IsTrue",
                                        new CodeMethodInvokeExpression(
                                            new CodeVariableReferenceExpression("complementConfigurations"),
                                            "Contains",
                                            new CodeVariableReferenceExpression("configuration"))))
                            }
                        ));
            }
        }

        private static CodeStatementCollection AddPartitionVariables(string variableName, string name)
        {
            CodeStatementCollection vars = new CodeStatementCollection();

            vars.Add(new CodeVariableDeclarationStatement(
                        "GraphNode",
                        variableName,
                        new CodeMethodInvokeExpression(
                            new CodeVariableReferenceExpression("nodes"),
                            "Find",
                            new CodeDelegateCreateExpression(
                                new CodeTypeReference("Predicate<GraphNode>"),
                                new CodeObjectCreateExpression(
                                    "GraphNodeDiscriminatorPredicate",
                                    new CodePrimitiveExpression(name)),
                                "Match"))));

            vars.Add(new CodeVariableDeclarationStatement(
                        "List<ModelConfiguration>",
                        variableName + "Configurations",
                        new CodePropertyReferenceExpression(
                            new CodeVariableReferenceExpression(variableName),
                            "ModelConfigurations")));

            return vars;
        }

        private void GetPartialOrder(string referencePattern, ICollection<int> candidate, ICollection<int> complement)
		{
			DataTable vinPatternTable = CatalogDataSet.Tables["VinPattern"];

			foreach (DataRow candidatePatternRow in vinPatternTable.Rows)
			{
				string candidatePattern = (string) candidatePatternRow["VinPattern"];

				DataRow[] childRows = candidatePatternRow.GetChildRows(vinPatternTable.ChildRelations[0]);

				if (string.Equals(referencePattern, candidatePattern))
				{
					AppendCatalogIds(candidate, childRows); // optimization
				}
				else
				{
					DataTable referenceTable = GetCatalogView(referencePattern);

					DataTable candidateTable = GetCatalogView(candidatePattern);

					if (candidateTable.Rows.Count > referenceTable.Rows.Count)
					{
						AppendCatalogIds(complement, childRows); // optimization
					}
					else
					{
						// NOTE TO OTHERS: DataTable Merge Sucks Ass.

						bool exists = true;

						foreach (DataRow candidateRow in candidateTable.Rows)
						{
							if (referenceTable.Rows.Find(candidateRow.ItemArray) == null)
							{
								exists = false;
								break;
							}
						}

						if (exists)
						{
							AppendCatalogIds(candidate, childRows);
						}
						else
						{
							AppendCatalogIds(complement, childRows);
						}
					}
				}
			}
		}

		private static void AppendCatalogIds(ICollection<int> catalogIds, IEnumerable<DataRow> rows)
		{
			foreach (DataRow row in rows)
			{
				int catalogId = (int) row["Id"];

				if (!catalogIds.Contains(catalogId))
				{
					catalogIds.Add(catalogId);
				}
			}
		}

        private DataTable GetCatalogView(string pattern)
		{
			DataView view = new DataView(CatalogDataSet.Tables["CatalogEntry"]);

			view.RowFilter = "VinPattern = '" + pattern + "' AND ISNULL(ParentId,-1) >= 0";

			DataTable table = view.ToTable(
				true,
				new string[]
					{
						"ModelId", "SegmentId", "BodyTypeId", "SeriesId",
						"PassengerDoorId", "FuelTypeId", "EngineId", "DriveTrainId", "TransmissionId"
					});

			DataColumnCollection columns = table.Columns;

			table.PrimaryKey = new DataColumn[]
				{
					columns["ModelId"], columns["SegmentId"], columns["BodyTypeId"], columns["SeriesId"],
					columns["PassengerDoorId"], columns["FuelTypeId"], columns["EngineId"], columns["DriveTrainId"], columns["TransmissionId"]
				};

			return table;
		} */

        private CodeTypeMemberCollection CreatePartitionTests()
		{
			CodeTypeMemberCollection tests = new CodeTypeMemberCollection();
			tests.Add(CreatePartitionTest("Segment"));
			tests.Add(CreatePartitionTest("BodyType"));
			return tests;
		}

		private CodeMemberMethod CreatePartitionTest(string parentTableName)
		{
			CodeMemberMethod testPartition = new CodeMemberMethod();
			testPartition.Name = parentTableName + "_Partition";
			testPartition.Attributes = MemberAttributes.Public;
			testPartition.CustomAttributes.Add(
				new CodeAttributeDeclaration(
					new CodeTypeReference("Test")));
			testPartition.Statements.Add(
					new CodeVariableDeclarationStatement(
						"Graph",
						"graph",
						new CodeObjectCreateExpression(
							"Graph",
							new CodeFieldReferenceExpression(
								new CodeThisReferenceExpression(),
								"catalog"))));
			testPartition.Statements.Add(
				new CodeVariableDeclarationStatement(
					"ModelConfigurationPropertyAccessor<" + parentTableName + ">",
					"accessor",
					new CodePropertyReferenceExpression(
						new CodeTypeReferenceExpression("ModelConfigurationPropertyAccessors"),
						parentTableName + "Accessor")));
			testPartition.Statements.Add(
				new CodeMethodInvokeExpression(
					new CodeVariableReferenceExpression("graph"),
					"Partition",
					new CodeVariableReferenceExpression("accessor")));
			testPartition.Statements.Add(
				new CodeVariableDeclarationStatement(
					"List<GraphNode>",
					"nodes",
					new CodeMethodInvokeExpression(
						new CodeVariableReferenceExpression("graph"),
						"GatherLeaves")));

			DataTable parentTable = CatalogDataSet.Tables[parentTableName];

			testPartition.Statements.Add(
				new CodeMethodInvokeExpression(
					new CodeTypeReferenceExpression("Assert"),
					"AreEqual",
					new CodePrimitiveExpression(parentTable.Rows.Count),
					new CodePropertyReferenceExpression(
						new CodeVariableReferenceExpression("nodes"),
						"Count")));

			testPartition.Statements.Add(
					new CodeVariableDeclarationStatement(
						"List<ModelConfiguration>",
						"actual"));

			testPartition.Statements.Add(new CodeVariableDeclarationStatement(typeof(int), "i"));

			testPartition.Statements.Add(new CodeVariableDeclarationStatement(typeof(int), "l"));

			foreach (DataRow row in parentTable.Rows)
			{
				string partitionName = (string) row["Name"];
				string partitionVariableName = Regex.Replace(partitionName, "[^0-9a-zA-Z_]", string.Empty);
				string partitionMatchName = "match" + partitionVariableName;
				
				partitionVariableName = "_" + partitionVariableName; // handle variables that start with a letter like 2DrCar
				
				testPartition.Statements.Add(
					new CodeVariableDeclarationStatement(
						"Predicate<GraphNode>",
						partitionMatchName,
						new CodeDelegateCreateExpression(
							new CodeTypeReference("Predicate<GraphNode>"),
							new CodeObjectCreateExpression(
								"GraphNodeDiscriminatorPredicate",
								new CodePrimitiveExpression(partitionName)),
							"Match")));
				testPartition.Statements.Add(
					new CodeVariableDeclarationStatement(
						"GraphNode",
						partitionVariableName,
						new CodeMethodInvokeExpression(
							new CodeVariableReferenceExpression("nodes"),
							"Find",
							new CodeVariableReferenceExpression(partitionMatchName))));
				testPartition.Statements.Add(
					new CodeMethodInvokeExpression(
						new CodeTypeReferenceExpression("Assert"),
						"IsNotNull",
						new CodeVariableReferenceExpression(partitionVariableName)));

				// count(*) too

				testPartition.Statements.Add(
					new CodeAssignStatement(
						new CodeVariableReferenceExpression("actual"),
						new CodePropertyReferenceExpression(
							new CodeVariableReferenceExpression(partitionVariableName),
							"ModelConfigurations")));

                DataTable modelTable = CatalogDataSet.Tables["Model"];

				DataRow[] modelRows = row.GetChildRows(parentTable.ChildRelations[0]);

			    List<DataRow> modelConfigurationRows = new List<DataRow>();

			    foreach (DataRow modelRow in modelRows)
                {
                    modelConfigurationRows.AddRange(modelRow.GetChildRows(modelTable.ChildRelations[0]));
                }

			    testPartition.Statements.Add(
					new CodeAssignStatement(
						new CodeVariableReferenceExpression("l"),
                        new CodePrimitiveExpression(modelConfigurationRows.Count)));

                CodePrimitiveExpression[] ids = new CodePrimitiveExpression[modelConfigurationRows.Count];

				for (int i = 0, l = ids.Length; i < l; i++)
				{
                    ids[i] = new CodePrimitiveExpression(modelConfigurationRows[i]["Id"]);
				}

				testPartition.Statements.Add(
					new CodeVariableDeclarationStatement(
						typeof(int[]),
						partitionVariableName + "CatalogIds",
						new CodeArrayCreateExpression(
							typeof(int),
							ids)));

				testPartition.Statements.Add(
					new CodeIterationStatement(
						new CodeAssignStatement(
							new CodeVariableReferenceExpression("i"),
							new CodePrimitiveExpression(0)),
						new CodeBinaryOperatorExpression(
							new CodeVariableReferenceExpression("i"),
							CodeBinaryOperatorType.LessThan,
							new CodeVariableReferenceExpression("l")),
						new CodeAssignStatement(
							new CodeVariableReferenceExpression("i"),
							new CodeBinaryOperatorExpression(
								new CodeVariableReferenceExpression("i"),
								CodeBinaryOperatorType.Add,
								new CodePrimitiveExpression(1))),
						new CodeExpressionStatement(
							new CodeMethodInvokeExpression(
								new CodeTypeReferenceExpression("Assert"),
								"Contains",
								new CodeMethodInvokeExpression(
									new CodeFieldReferenceExpression(
										new CodeThisReferenceExpression(),
										"catalog"),
									"FindModelConfiguration",
									new CodeArrayIndexerExpression(
										new CodeVariableReferenceExpression(partitionVariableName + "CatalogIds"),
										new CodeVariableReferenceExpression("i"))),
								new CodeVariableReferenceExpression("actual")))));
			}

			return testPartition;
		}

		private static CodeTypeDeclaration CreateGraphNodeDiscriminatorPredicateClass()
		{
			CodeTypeDeclaration predicate = new CodeTypeDeclaration("GraphNodeDiscriminatorPredicate");

			predicate.TypeAttributes = TypeAttributes.NestedAssembly;

			predicate.Members.Add(new CodeMemberField(typeof(string), "name"));

			CodeConstructor constructor = new CodeConstructor();

			constructor.Attributes = MemberAttributes.Public;
			constructor.Parameters.Add(new CodeParameterDeclarationExpression(typeof(string), "name"));
			constructor.Statements.Add(new CodeAssignStatement(
										new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), "name"),
										new CodeVariableReferenceExpression("name")));

			predicate.Members.Add(constructor);

			CodeMemberMethod match = new CodeMemberMethod();
			match.Name = "Match";
			match.ReturnType = new CodeTypeReference(typeof(bool));
			match.Attributes = MemberAttributes.Public;
			match.Parameters.Add(new CodeParameterDeclarationExpression("GraphNode", "match"));
			match.Statements.Add(
				new CodeConditionStatement(
					new CodeMethodInvokeExpression(
						new CodeTypeReferenceExpression(typeof (string)),
						"Equals",
						new CodePropertyReferenceExpression(
							new CodePropertyReferenceExpression(
								new CodeVariableReferenceExpression("match"),
								"Discriminator"),
							"Name"),
						new CodeFieldReferenceExpression(
							new CodeThisReferenceExpression(),
							"name")),
					new CodeStatement[]
						{
							new CodeMethodReturnStatement(new CodePrimitiveExpression(true))
						},
					new CodeStatement[]
						{
							new CodeMethodReturnStatement(new CodePrimitiveExpression(false))
						}
					));

			predicate.Members.Add(match);

			return predicate;
		}
	}
}
