using System;
using System.Collections;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;

namespace FirstLook.DomainModel.Olap.WebControls
{
    public class NationalAuctionPanel : CompositeDataBoundControl
    {
        #region Events

        private static readonly object EventAreaChanged = new object();
        private static readonly object EventPeriodChanged = new object();
        private static readonly object EventSeriesChanged = new object();

        public event EventHandler<NationalAuctionEventArgs<int>> SelectedAreaChanged
        {
            add
            {
                Events.AddHandler(EventAreaChanged, value);
            }
            remove
            {
                Events.RemoveHandler(EventAreaChanged, value);
            }
        }

        protected void RaiseSelectedAreaChanged(int areaId)
        {
            EventHandler<NationalAuctionEventArgs<int>> handler = Events[EventAreaChanged] as EventHandler<NationalAuctionEventArgs<int>>;
            if (handler != null)
                handler(this, new NationalAuctionEventArgs<int>(areaId));
            DataBindChildControls();
        }

        public event EventHandler<NationalAuctionEventArgs<int>> SelectedPeriodChanged
        {
            add
            {
                Events.AddHandler(EventPeriodChanged, value);
            }
            remove
            {
                Events.RemoveHandler(EventPeriodChanged, value);
            }
        }

        protected void RaiseSelectedPeriodChanged(int periodId)
        {
            EventHandler<NationalAuctionEventArgs<int>> handler = Events[EventPeriodChanged] as EventHandler<NationalAuctionEventArgs<int>>;
            if (handler != null)
                handler(this, new NationalAuctionEventArgs<int>(periodId));
            DataBindChildControls();
        }

        public event EventHandler<NationalAuctionEventArgs<string>> SelectedSeriesChanged
        {
            add
            {
                Events.AddHandler(EventSeriesChanged, value);
            }
            remove
            {
                Events.RemoveHandler(EventSeriesChanged, value);
            }
        }
        
        protected void RaiseSelectedSeriesChanged(string vic)
        {
            EventHandler<NationalAuctionEventArgs<string>> handler = Events[EventSeriesChanged] as EventHandler<NationalAuctionEventArgs<string>>;
            if (handler != null)
                handler(this, new NationalAuctionEventArgs<string>(vic));
            DataBindChildControls();
        }

        #endregion

        #region Properties

        private bool showReportLink = true;
        private bool raiseChangeEvents = true;
        private bool childControlsCreated = false;
        private bool isDirty = true;

        public bool ShowReportLink
        {
            get
            {
                return showReportLink;
            }
            set
            {
                if (value != showReportLink)
                {
                    showReportLink = value;
                }
            }
        }

        public string Vin
        {
            get
            {
                string text = ViewState["Vin"] as string;
                if (string.IsNullOrEmpty(text))
                    return string.Empty;
                return text;
            }
            private set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ViewState["Vin"] = value;
                }
            }
        }

        public string Vic
        {
            get
            {
                string text = ViewState["Vic"] as string;
                if (string.IsNullOrEmpty(text))
                    return string.Empty;
                return text;
            }
            private set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ViewState["Vic"] = value;
                    isDirty = true;
                    if (raiseChangeEvents)
                    {
                        RaiseSelectedSeriesChanged(Vic);
                    }
                }
            }
        }

        public int? AreaId
        {
            get
            {
                return (int?)ViewState["AreaId"];
            }
            private set
            {
                if (value.HasValue)
                {
                    if (Nullable.Compare(AreaId, value) != 0)
                    {
                        ViewState["AreaId"] = value;
                        isDirty = true;
                        if (raiseChangeEvents)
                        {
                            RaiseSelectedAreaChanged(value.Value);
                        }
                    }
                }
                else
                {
                    ViewState.Remove("AreaId");
                }    
            }
        }

        public int? PeriodId
        {
            get
            {
                return (int?)ViewState["PeriodId"];
            }
            private set
            {
                if (value.HasValue)
                {
                    if (Nullable.Compare(PeriodId, value) != 0)
                    {
                        ViewState["PeriodId"] = value;
                        isDirty = true;
                        if (raiseChangeEvents)
                        {
                            RaiseSelectedPeriodChanged(value.Value);
                        }
                    }
                }
                else
                {
                    ViewState.Remove("PeriodId");
                }
            }
        }

        public int? Mileage
        {
            get
            {
                return (int?)ViewState["Mileage"];
            }
            private set
            {
                if (Nullable.Compare(Mileage, value) != 0)
                {
                    ViewState["Mileage"] = value;
                }
            }
        }

        #endregion

        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {
            raiseChangeEvents = false;

            int records = 0;

            if (dataBinding)
            {
                if (dataSource != null)
                {
                    IEnumerator en = dataSource.GetEnumerator();

                    if (en.MoveNext())
                    {
                        NationalAuctionPanelArgs args = (NationalAuctionPanelArgs)en.Current;

                        Vin = args.Vin;
                        Vic = args.Vic;
                        Mileage = args.Mileage;
                        AreaId = args.AreaId;
                        PeriodId = args.PeriodId;

                        records = 1;
                    }
                }
            }
            else
            {
                records = string.IsNullOrEmpty(Vin) ? 0 : 1;
            }

            if (!childControlsCreated)
            {
                childControlsCreated = true;

                CreateForm();

                CreateResultsTable();
            }
            else
            {
                Controls.Add(_input);

                Controls.Add(_table);
            }
            
            DataBindChildControls();

            raiseChangeEvents = true;

            return records;
        }

        private void DataBindChildControls()
        {
            if (!isDirty) return;

            _seriesLabel.Visible = true;
            _seriesList.Visible = true;

            NationalAuctionSeriesList series = NationalAuctionSeriesList.GetSeriesList(Vin);

            _seriesList.DataSource = series;
            _seriesList.DataTextField = "Name";
            _seriesList.DataValueField = "Vic";
            _seriesList.DataBind();

            NationalAuctionReportingAreaList areas = NationalAuctionReportingAreaList.GetReportingAreaList();

            _areaList.DataSource = areas;
            _areaList.DataTextField = "Name";
            _areaList.DataValueField = "Id";
            _areaList.DataBind();

            NationalAuctionReportingPeriodList times = NationalAuctionReportingPeriodList.GetReportingPeriodList();

            _timeList.DataSource = times;
            _timeList.DataTextField = "Name";
            _timeList.DataValueField = "Id";
            _timeList.DataBind();

            NationalAuctionReport report = NationalAuctionReport.GetAuctionReport(new NationalAuctionReportCriteria(
                                                                                      Vin,
                                                                                      series.GetSeries(Vic),
                                                                                      areas.GetReportingArea(AreaId),
                                                                                      times.GetReportingPeriod(PeriodId),
                                                                                      Mileage));

            // description

            _titleText.Text = report.VehicleDescription.ToString();

            // row 0

            if (report.OverallReport.SalePrice.SampleSize > 0)
            {
                _r0[2].Text = string.Format(CultureInfo.CurrentUICulture, "Average of {0}", report.OverallReport.SalePrice.SampleSize);
            }

            if (report.MileageReport.VehicleCount > 0)
            {
                _r0[4].Text = string.Format(CultureInfo.CurrentUICulture, "{0} Vehicles with Similar Mileage", report.MileageReport.VehicleCount);
            }

            // row 1

            _r1[0].Text = string.Format(CultureInfo.CurrentCulture, "{0:c0}", report.OverallReport.SalePrice.Maximum);
            _r1[1].Text = string.Format(CultureInfo.CurrentCulture, "{0:c0}", report.OverallReport.SalePrice.Average);
            _r1[2].Text = string.Format(CultureInfo.CurrentCulture, "{0:c0}", report.OverallReport.SalePrice.Minimum);
            _r1[3].Text = string.Format(CultureInfo.CurrentCulture, "{0:c0}", report.MileageReport.AverageSalePrice);

            // row 2

            _r2[0].Text = string.Format(CultureInfo.CurrentCulture, "{0:0,0}", report.OverallReport.Mileage.Maximum);
            _r2[1].Text = string.Format(CultureInfo.CurrentCulture, "{0:0,0}", report.OverallReport.Mileage.Average);
            _r2[2].Text = string.Format(CultureInfo.CurrentCulture, "{0:0,0}", report.OverallReport.Mileage.Minimum);
            _r2[3].Text = string.Format(CultureInfo.CurrentCulture, "{0:0,0}", string.Format("{0:###,##0}-{1:###,###}", report.ReportCriteria.LowMileage, report.ReportCriteria.HighMileage));

            if (ShowReportLink)
            {
                _reportLink.NavigateUrl = CreateAuctionReportUrl();
            }

            isDirty = false;
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        private Table _table;
        private Panel _input;
        private Literal _titleText;
        private DropDownList _timeList;
        private DropDownList _areaList;
        private DropDownList _seriesList;
        private HyperLink _reportLink;
        private Label _seriesLabel;
        private TableCell[] _r0;
        private TableCell[] _r1;
        private TableCell[] _r2;

		private const string ReportLinkText = "View Auction Report";
		private const string MMRLinkText = "MMR";

        private string CreateAuctionReportUrl()
        {
            int c = 0;
            StringBuilder sb = new StringBuilder("/support/Reports/AuctionTransactions.aspx");
            if (!string.IsNullOrEmpty(Vin))
                sb.Append(++c == 1 ? "?" : "&").Append("VIN=").Append(Vin);
            if (!string.IsNullOrEmpty(Vic))
                sb.Append(++c == 1 ? "?" : "&").Append("VIC=").Append(Vic);
            if (AreaId.HasValue)
                sb.Append(++c == 1 ? "?" : "&").Append("AreaID=").Append(AreaId.Value);
            if (PeriodId.HasValue)
                sb.Append(++c == 1 ? "?" : "&").Append("PeriodID=").Append(PeriodId.Value);
            if (Mileage.HasValue)
                sb.Append(++c == 1 ? "?" : "&").Append("Mileage=").Append(Mileage.Value);

            return sb.ToString();
        }

        private void CreateForm()
        {
            _input = new Panel();
            _input.CssClass = "InputContainer";
            Controls.Add(_input);

            if (ShowReportLink) {

                _reportLink = new HyperLink();
                _reportLink.CssClass = "ReportLink";
                _reportLink.NavigateUrl = CreateAuctionReportUrl();
                _reportLink.Text = ReportLinkText;
                _reportLink.Target = "AuctionReport";
                _input.Controls.Add(_reportLink); 
                
                HyperLink mmrLink = new HyperLink();
                mmrLink.CssClass = "ReportLink";
                mmrLink.NavigateUrl = string.Format(
                    CultureInfo.InvariantCulture,
                    "https://www.manheim.com/members/internetmmr/?vin={0}",
                    Vin);
                mmrLink.Text = MMRLinkText;
                mmrLink.Target = "MMRReport";
                _input.Controls.Add(mmrLink); 
            }

            HtmlGenericControl titlePara = new HtmlGenericControl("h3");
            _input.Controls.Add(titlePara);

            _titleText = new Literal();
            titlePara.Controls.Add(_titleText);

            HtmlGenericControl fieldSetContainer = new HtmlGenericControl("div");
            _input.Controls.Add(fieldSetContainer);

            HtmlGenericControl fieldSet = new HtmlGenericControl("fieldset");
            fieldSetContainer.Controls.Add(fieldSet);

            HtmlGenericControl dl = new HtmlGenericControl("dl");
            fieldSet.Controls.Add(dl);
            CreateSeriesDropDown(dl);
            CreateAreaDropDown(dl);
            CreateTimeDropDown(dl);
        }

        private void CreateSeriesDropDown(Control list)
        {
            HtmlGenericControl dt = new HtmlGenericControl("dt");
            list.Controls.Add(dt);

            Label label = new Label();
            label.Text = "Series: ";
            label.AssociatedControlID = "SeriesDropDown";
            dt.Controls.Add(label);

            HtmlGenericControl dd = new HtmlGenericControl("dd");
            list.Controls.Add(dd);

            _seriesList = new DropDownList();
            _seriesList.ID = "SeriesDropDown";
            _seriesList.AutoPostBack = true;
            _seriesList.DataBound += SeriesList_DataBound;
            _seriesList.SelectedIndexChanged += SeriesList_SelectedIndexChanged;
            dd.Controls.Add(_seriesList);

            _seriesLabel = new Label();
            _seriesLabel.Visible = false;
            dd.Controls.Add(_seriesLabel);
        }

        private void CreateAreaDropDown(Control list)
        {
            HtmlGenericControl dt = new HtmlGenericControl("dt");
            list.Controls.Add(dt);

            Label label = new Label();
            label.Text = "Area: ";
            label.AssociatedControlID = "AreaDropDown";
            dt.Controls.Add(label);

            HtmlGenericControl dd = new HtmlGenericControl("dd");
            list.Controls.Add(dd);

            _areaList = new DropDownList();
            _areaList.ID = "AreaDropDown";
            _areaList.AutoPostBack = true;
            _areaList.DataBound += AreaList_DataBound;
            _areaList.SelectedIndexChanged += AreaList_SelectedIndexChanged;
            dd.Controls.Add(_areaList);
        }

        private void CreateTimeDropDown(Control list)
        {
            HtmlGenericControl dt = new HtmlGenericControl("dt");
            list.Controls.Add(dt);

            Label label = new Label();
            label.Text = "Time Period: ";
            label.AssociatedControlID = "TimeDropDown";
            dt.Controls.Add(label);

            HtmlGenericControl dd = new HtmlGenericControl("dd");
            list.Controls.Add(dd);

            _timeList = new DropDownList();
            _timeList.ID = "TimeDropDown";
            _timeList.AutoPostBack = true;
            _timeList.DataBound += TimeList_DataBound;
            _timeList.SelectedIndexChanged += TimeList_SelectedIndexChanged;
            dd.Controls.Add(_timeList);
        }

        private void SeriesList_DataBound(object sender, EventArgs e)
        {
            if (_seriesList.Items.Count == 1)
            {
                _seriesLabel.Visible = true;
                _seriesLabel.Text = _seriesList.Items[0].Text;
                _seriesList.Visible = false;
            }
            else
            {
                _seriesLabel.Visible = false;
                _seriesList.Visible = true;
                foreach (ListItem item in _seriesList.Items)
                    item.Selected = item.Value.Equals(Vic);
            }
        }

        private void AreaList_DataBound(object sender, EventArgs e)
        {
            foreach (ListItem item in _areaList.Items)
                item.Selected = (Nullable.Compare(Int32Helper.ToNullableInt32(item.Value), AreaId) == 0);
        }

        private void TimeList_DataBound(object sender, EventArgs e)
        {
            foreach (ListItem item in _timeList.Items)
                item.Selected = (Nullable.Compare(Int32Helper.ToNullableInt32(item.Value), PeriodId) == 0);
        }

        private void SeriesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_seriesList.SelectedIndex != -1)
            {
                Vic = _seriesList.Items[_seriesList.SelectedIndex].Value;
            }
        }

        private void AreaList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_areaList.SelectedIndex != -1)
            {
                AreaId = Int32Helper.ToNullableInt32(_areaList.Items[_areaList.SelectedIndex].Value);
            }
        }

        private void TimeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_timeList.SelectedIndex != -1)
            {
                PeriodId = Int32Helper.ToNullableInt32(_timeList.Items[_timeList.SelectedIndex].Value);
            }
        }

        private void CreateResultsTable()
        {
            _table = new Table();
            _table.CssClass = "ResultsTable";
            Controls.Add(_table);

            TableRow _thead = new TableRow();
            _thead.TableSection = TableRowSection.TableHeader;
            _table.Rows.Add(_thead);

            _r0 = new TableHeaderCell[5];
            for (int c = 0; c < 5; c++)
				_r0[c] = new TableHeaderCell();

            _r0[0].Text = string.Empty;
            _r0[1].Text = "High";
            _r0[2].Text = "Average";
            _r0[3].Text = "Low";
            _r0[4].Text = "Vehicles with Similar Mileage";

            _thead.Cells.AddRange(_r0);

            TableRow _tbody0 = new TableRow();
            _tbody0.TableSection = TableRowSection.TableBody;
            _table.Rows.Add(_tbody0);

			TableHeaderCell _r1Header = new TableHeaderCell();
            _r1 = new TableCell[4];
            for (int c = 0; c < 4; c++)
                _r1[c] = new TableCell();

			_r1Header.Text = "Sale Price";
            _r1[0].Text = "--";
            _r1[1].Text = "--";
            _r1[2].Text = "--";
            _r1[3].Text = "--";

			_tbody0.Cells.Add(_r1Header);
            _tbody0.Cells.AddRange(_r1);

            TableRow _tbody1 = new TableRow();
            _tbody1.TableSection = TableRowSection.TableBody;
            _table.Rows.Add(_tbody1);

			TableHeaderCell _r2Header = new TableHeaderCell();
            _r2 = new TableCell[4];
            for (int c = 0; c < 4; c++)
                _r2[c] = new TableCell();

			_r2Header.Text = "Related Avg. Mileage";
            _r2[0].Text = "--";
            _r2[1].Text = "--";
            _r2[2].Text = "--";
            _r2[3].Text = "--";

			_tbody1.Cells.Add(_r2Header);
            _tbody1.Cells.AddRange(_r2);

			TableRow _tfoot = new TableRow();
			_tfoot.TableSection = TableRowSection.TableFooter;
			_table.Rows.Add(_tfoot);

			TableCell copyright = new TableCell();
			copyright.ColumnSpan = 5;
			copyright.Text = "*NAAA Regions have been grouped to reflect industry standard national regions.";
			_tfoot.Cells.Add(copyright);
        }
    }
}
