using System;

namespace FirstLook.DomainModel.Olap.WebControls
{
    public class NationalAuctionEventArgs<T> : EventArgs
    {
        private readonly T _value;

        public NationalAuctionEventArgs(T value)
        {
            _value = value;
        }

        public T Value
        {
            get { return _value; }
        }
    }
}
