namespace FirstLook.DomainModel.Olap.WebControls
{
    public class NationalAuctionPanelDataSource
    {
        public NationalAuctionPanelArgs SelectByVin(string vin)
        {
            return new NationalAuctionPanelArgs(vin);
        }

        public NationalAuctionPanelArgs SelectByAll(string vin, string vic, int? mileage, int? areaId, int? periodId)
        {
            NationalAuctionPanelArgs args = new NationalAuctionPanelArgs(vin);

            if (!string.IsNullOrEmpty(vic))
                args.Vic = vic;
            args.Mileage = mileage;
            args.AreaId = areaId;
            args.PeriodId = periodId;

            return args;
        }
    }
}
