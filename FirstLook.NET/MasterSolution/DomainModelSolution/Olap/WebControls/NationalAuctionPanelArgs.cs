using System;

namespace FirstLook.DomainModel.Olap.WebControls
{
    [Serializable]
    public class NationalAuctionPanelArgs
    {
        private string _vin;
        private string _vic;
        private int? _mileage;
        private int? _areaId;
        private int? _periodId;

        public NationalAuctionPanelArgs(string vin)
        {
            _vin = vin;
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public string Vic
        {
            get { return _vic; }
            set { _vic = value; }
        }

        public int? AreaId
        {
            get { return _areaId; }
            set { _areaId = value; }
        }

        public int? PeriodId
        {
            get { return _periodId; }
            set { _periodId = value; }
        }

        public int? Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }
    }
}