using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Olap
{
    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "Implements IList<T>")]
    [Serializable]
    public class NationalAuctionReportingPeriodList : ReadOnlyListBase<NationalAuctionReportingPeriodList, NationalAuctionReportingPeriod>
    {
        public NationalAuctionReportingPeriod GetReportingPeriod(int? periodId)
        {
            foreach (NationalAuctionReportingPeriod period in this)
                if (periodId == period.Id)
                    return period;
            return this[0];
        }

        #region Factory Methods

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Long running operation")]
        public static NationalAuctionReportingPeriodList GetReportingPeriodList()
        {
            return DataPortal.Fetch<NationalAuctionReportingPeriodList>(new Criteria());
        }

        private NationalAuctionReportingPeriodList()
        {
            /* require use of factory methods */
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria", Justification = "Required by CSLA")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            Fetch();
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.OlapDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "NationalAuction.ReportingPeriodList#Fetch";
                    
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(new NationalAuctionReportingPeriod(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
