using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Olap
{
    /// <summary>
    /// Reports the average sale price of the vehicles sold with a given series in a region
    /// over a time period having a certain mileage band.
    /// </summary>
    [Serializable]
    public class NationalAuctionMileageReport : ReadOnlyBase<NationalAuctionMileageReport>
    {
        #region Business Methods

        private NationalAuctionReportCriteria reportCriteria;
        private decimal? averageSalePrice;
        private int vehicleCount;

        public NationalAuctionReportCriteria ReportCriteria
        {
            get { return reportCriteria; }
        }

        public decimal? AverageSalePrice
        {
            get { return averageSalePrice; }
        }

        public int VehicleCount
        {
            get { return vehicleCount; }
        }

        protected override object GetIdValue()
        {
            return reportCriteria;
        }

        #endregion

        #region Data Access

        public static NationalAuctionMileageReport GetNationalAuctionMileageReport(NationalAuctionReportCriteria criteria)
        {
            return DataPortal.Fetch<NationalAuctionMileageReport>(criteria);
        }

        private NationalAuctionMileageReport()
        {
            /* require use of factory methods */
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void DataPortal_Fetch(NationalAuctionReportCriteria criteria)
        {
            Fetch(criteria);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void Fetch(NationalAuctionReportCriteria criteria)
        {
            reportCriteria = criteria;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.OlapDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "NationalAuction.MileageReport#Fetch";

                    criteria.AddMileageReportCriteria(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            averageSalePrice = reader.GetDecimal(reader.GetOrdinal("AvgSalePrice"));
                            vehicleCount = reader.GetInt32(reader.GetOrdinal("VehicleCount"));
                        }
                    }
                }
            }
        }

        #endregion
    }
}
