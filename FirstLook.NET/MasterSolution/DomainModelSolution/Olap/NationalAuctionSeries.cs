using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Csla;

namespace FirstLook.DomainModel.Olap
{
    [Serializable]
    public class NationalAuctionSeries : ReadOnlyBase<NationalAuctionSeries>
    {
        #region Business Methods

        private readonly string _vic;
        private readonly int _modelYear;
        private readonly string _make;
        private readonly string _series;
        private readonly string _body;

        public string Vic
        {
            get { return _vic; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
        }

        public string Make
        {
            get { return _make; }
        }

        public string Series
        {
            get { return _series; }
        }

        public string Body
        {
            get { return _body; }
        }

        public string Name
        {
            get
            {
                return string.Format(CultureInfo.InvariantCulture, "{0} {1}", Series, Body);
            }
        }

        protected override object GetIdValue()
        {
            return _vic;
        }

        #endregion

        #region Constructors

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        internal NationalAuctionSeries(IDataRecord record)
        {
            _vic = record.GetString(record.GetOrdinal("VIC"));
            _modelYear = record.GetInt32(record.GetOrdinal("ModelYear"));
            _make = record.GetString(record.GetOrdinal("Make"));
            _series = record.GetString(record.GetOrdinal("Series"));
            _body = record.GetString(record.GetOrdinal("Body"));
        }

        #endregion
    }
}
