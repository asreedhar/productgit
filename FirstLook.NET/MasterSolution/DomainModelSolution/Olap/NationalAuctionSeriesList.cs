using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Olap
{
    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "Implements IList<T>")]
    [Serializable]
    public class NationalAuctionSeriesList : ReadOnlyListBase<NationalAuctionSeriesList,NationalAuctionSeries>
    {
        public NationalAuctionSeries GetSeries(string vic)
        {
            foreach (NationalAuctionSeries series in this)
                if (vic.Equals(series.Vic))
                    return series;
            return null;
        }

        #region Factory Methods

        public static NationalAuctionSeriesList GetSeriesList(string vin)
        {
            if (string.IsNullOrEmpty(vin))
                throw new ArgumentNullException("vin", "VIN cannot be null nor empty");

            if (vin.Length != 17)
                throw new ArgumentException("VIN must be 17 characters in length", "vin");

            return DataPortal.Fetch<NationalAuctionSeriesList>(new Criteria(vin));
        }

        private NationalAuctionSeriesList()
        {
            /* require use of factory methods */
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly string vin;

            public Criteria(string vin)
            {
                this.vin = vin;
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
            public string Vin
            {
                get { return vin; }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            Fetch(criteria.Vin);
        }


        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void Fetch(string vin)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.OlapDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "NationalAuction.SeriesList#Fetch";
                    
                    SimpleQuery.AddWithValue(command, "@vin", vin, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(new NationalAuctionSeries(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
