using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Olap
{
    [Serializable]
    public class NationalAuctionReportCriteria : IEquatable<NationalAuctionReportCriteria>
    {
        private readonly NationalAuctionReportingPeriod _period;
        private readonly NationalAuctionReportingArea _area;
        private readonly NationalAuctionSeries _series;
        private readonly string _vin;
        private readonly int? _mileage;

        public NationalAuctionReportCriteria(string vin, NationalAuctionSeries series, NationalAuctionReportingArea area, NationalAuctionReportingPeriod period, int? mileage)
        {
            _vin = vin;
            _series = series;
            _area = area;
            _period = period;
            _mileage = mileage;
        }

        public NationalAuctionReportingPeriod Period
        {
            get { return _period; }
        }

        public NationalAuctionReportingArea Area
        {
            get { return _area; }
        }

        public NationalAuctionSeries Series
        {
            get { return _series; }
        }

        public string Vin
        {
            get { return _vin; }
        }

        public int? Mileage
        {
            get { return _mileage; }
        }

        public int? LowMileage
        {
            get
            {
                return ((Mileage / 10000) * 10000);
            }
        }

        public int? HighMileage
        {
            get
            {
                return LowMileage + 9999;
            }
        }

        public bool Equals(NationalAuctionReportCriteria value)
        {
            if (value == null) return false;
            if (!Equals(_period, value._period)) return false;
            if (!Equals(_area, value._area)) return false;
            if (!Equals(_series, value._series)) return false;
            if (!Equals(_vin, value._vin)) return false;
            if (!Equals(_mileage, value._mileage)) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as NationalAuctionReportCriteria);
        }

        public override int GetHashCode()
        {
            int result = _period != null ? _period.GetHashCode() : 0;
            result = 29*result + (_area != null ? _area.GetHashCode() : 0);
            result = 29*result + (_series != null ? _series.GetHashCode() : 0);
            result = 29*result + (_vin != null ? _vin.GetHashCode() : 0);
            result = 29*result + _mileage.GetHashCode();
            return result;
        }

        internal virtual void AddOverallReportCriteria(IDbCommand command)
        {
            SimpleQuery.AddWithValue(command, "@Vin", Vin, DbType.String);
            if (Series == null)
            {
                SimpleQuery.AddNullValue(command, "@Vic", DbType.String);
            }
            else
            {
                SimpleQuery.AddWithValue(command, "@Vic", Series.Vic, DbType.String);
            }
            SimpleQuery.AddWithValue(command, "@AreaId", Area.Id, DbType.Int32);
            SimpleQuery.AddWithValue(command, "@PeriodId", Period.Id, DbType.Int32);
        }

        internal void AddMileageReportCriteria(IDbCommand command)
        {
            AddOverallReportCriteria(command);

            if (LowMileage.HasValue)
                SimpleQuery.AddWithValue(command, "@LowMileage", LowMileage.Value, DbType.Int32);
            else
                SimpleQuery.AddNullValue(command, "@LowMileage", DbType.Int32);

            if (HighMileage.HasValue)
                SimpleQuery.AddWithValue(command, "@HighMileage", HighMileage.Value, DbType.Int32);
            else
                SimpleQuery.AddNullValue(command, "@HighMileage", DbType.Int32);
        }
    }
}