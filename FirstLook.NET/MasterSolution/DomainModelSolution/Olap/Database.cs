using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace FirstLook.DomainModel.Olap
{
    static class Database
    {
        /// <summary>
        /// This is where all our reports currently live.  Ironically, we use the legacy name
        /// <code>ReportHost</code> rather than the actual database name <code>Reports</code>.
        /// Eternally sorry for all and any confusion that results from this mess.  Simon.
        /// </summary>
        private const string olapDatabase = "ReportHost";

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        public static string OlapDatabase
        {
            get { return olapDatabase; }
        }
    }
}
