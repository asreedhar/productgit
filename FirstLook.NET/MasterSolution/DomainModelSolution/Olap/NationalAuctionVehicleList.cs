using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Olap
{
    [SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "Implements IList<T>")]
    [Serializable]
    public class NationalAuctionVehicleList : ReadOnlyListBase<NationalAuctionVehicleList, NationalAuctionVehicle>, IReportTotalRowCount
    {
        private static readonly string[] Columns = new string[] {
            "RowNumber",
	        "SaleDate",
	        "RegionName",
	        "SaleTypeName",
	        "Series",
	        "SalePrice",
	        "Mileage",
	        "Engine",
	        "Transmission"
        };

        #region Business Methods

        private NationalAuctionReportCriteria _reportCriteria;
        private int _totalRowCount;

        #region IReportTotalRowCount Members

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        public int TotalRowCount
        {
            get { return _totalRowCount; }
            private set { _totalRowCount = value; }
        }

        #endregion

        public NationalAuctionReportCriteria ReportCriteria
        {
            get { return _reportCriteria; }
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Long running operation")]
        public static NationalAuctionVehicleList GetVehicleList(NationalAuctionReportCriteria reportCriteria, string sortColumns, int maximumRows, int startRowIndex)
        {
            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero.");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater than zero.");

            if (string.IsNullOrEmpty(sortColumns))
            {
                sortColumns = "RowNumber";
            }
            else
            {
                bool hasValidColumn = false;
                foreach (string column in Columns)
                    hasValidColumn |= sortColumns.StartsWith(column, StringComparison.OrdinalIgnoreCase);
                if (!hasValidColumn)
                    throw new ArgumentOutOfRangeException("sortColumns", sortColumns, "Not a recognized column name");
            }

            return DataPortal.Fetch<NationalAuctionVehicleList>(new Criteria(reportCriteria, sortColumns, maximumRows, startRowIndex));
        }

        [Serializable]
        class Criteria
        {
            private readonly NationalAuctionReportCriteria _reportCriteria;
            private readonly string _sortColumns;
            private readonly int _maximumRows;
            private readonly int _startRowIndex;

            public Criteria(NationalAuctionReportCriteria reportCriteria, string sortColumns, int maximumRows, int startRowIndex)
            {
                _reportCriteria = reportCriteria;
                _sortColumns = sortColumns;
                _maximumRows = maximumRows;
                _startRowIndex = startRowIndex;
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
            public NationalAuctionReportCriteria ReportCriteria
            {
                get { return _reportCriteria; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
            public string SortColumns
            {
                get { return _sortColumns; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
            public int MaximumRows
            {
                get { return _maximumRows; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
            public int StartRowIndex
            {
                get { return _startRowIndex; }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            Fetch(criteria.ReportCriteria, criteria.SortColumns, criteria.MaximumRows, criteria.StartRowIndex);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void Fetch(NationalAuctionReportCriteria reportCriteria, string sortColumns, int maximumRows, int startRowIndex)
        {
            _reportCriteria = reportCriteria;

            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.OlapDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "NationalAuction.VehicleList#Fetch";

                    reportCriteria.AddOverallReportCriteria(command);

                    SimpleQuery.AddWithValue(command, "@SortColumns", sortColumns, DbType.String);
                    SimpleQuery.AddWithValue(command, "@MaximumRows", maximumRows, DbType.Int32);
                    SimpleQuery.AddWithValue(command, "@StartRowIndex", startRowIndex, DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(new NationalAuctionVehicle(reader));
                        }

                        reader.NextResult();

                        if (reader.Read())
                        {
                            TotalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                        }
                        
                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
