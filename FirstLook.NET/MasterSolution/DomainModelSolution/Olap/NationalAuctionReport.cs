using System;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.DomainModel.Olap
{
    [Serializable]
    public class NationalAuctionReport : ReadOnlyBase<NationalAuctionReport>
    {
        private NationalAuctionReportCriteria _reportCriteria;
        private NationalAuctionVehicleDescription _vehicleDescription;
        private NationalAuctionOverallReport _overallReport;
        private NationalAuctionMileageReport _mileageReport;

        public NationalAuctionReportCriteria ReportCriteria
        {
            get { return _reportCriteria; }
        }

        public NationalAuctionVehicleDescription VehicleDescription
        {
            get { return _vehicleDescription; }
        }

        public NationalAuctionOverallReport OverallReport
        {
            get { return _overallReport; }
        }

        public NationalAuctionMileageReport MileageReport
        {
            get { return _mileageReport; }
        }

        protected override object GetIdValue()
        {
            return _reportCriteria;
        }

        #region Data Access

        public static NationalAuctionReport GetAuctionReport(NationalAuctionReportCriteria criteria)
        {
            return DataPortal.Fetch<NationalAuctionReport>(criteria);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void DataPortal_Fetch(NationalAuctionReportCriteria criteria)
        {
            Fetch(criteria);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void Fetch(NationalAuctionReportCriteria criteria)
        {
            _reportCriteria = criteria;
            _vehicleDescription = NationalAuctionVehicleDescription.GetVehicleDescription(criteria.Vin);
            _overallReport = NationalAuctionOverallReport.GetNationalAuctionOverallReport(criteria);
            _mileageReport = NationalAuctionMileageReport.GetNationalAuctionMileageReport(criteria);
        }

        #endregion
    }
}
