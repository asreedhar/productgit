using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Core;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Olap
{
    /// <summary>
    /// Reports summary information (min, avg and max) on the price and mileage of vehicles sold
    /// with a given series in a region over a time period.
    /// </summary>
    [Serializable]
    public class NationalAuctionOverallReport : ReadOnlyBase<NationalAuctionOverallReport>
    {
        private NationalAuctionReportCriteria reportCriteria;
        private SampleSummary<decimal> salePrice;
        private SampleSummary<int> mileage;

        public NationalAuctionReportCriteria ReportCriteria
        {
            get { return reportCriteria; }
        }

        public SampleSummary<decimal> SalePrice
        {
            get { return salePrice; }
        }

        public SampleSummary<int> Mileage
        {
            get { return mileage; }
        }

        protected override object GetIdValue()
        {
            return reportCriteria;
        }

        public static NationalAuctionOverallReport GetNationalAuctionOverallReport(NationalAuctionReportCriteria criteria)
        {
            return DataPortal.Fetch<NationalAuctionOverallReport>(criteria);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void DataPortal_Fetch(NationalAuctionReportCriteria criteria)
        {
            Fetch(criteria);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void Fetch(NationalAuctionReportCriteria criteria)
        {
            reportCriteria = criteria;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.OlapDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "NationalAuction.OverallReport#Fetch";

                    criteria.AddOverallReportCriteria(command);
                    
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            salePrice = new SampleSummary<decimal>(
                                reader.GetDecimal(reader.GetOrdinal("MinSalePrice")),
                                reader.GetDecimal(reader.GetOrdinal("AvgSalePrice")),
                                reader.GetDecimal(reader.GetOrdinal("MaxSalePrice")),
                                reader.GetInt32(reader.GetOrdinal("VehicleCount")));

                            mileage = new SampleSummary<int>(
                                reader.GetInt32(reader.GetOrdinal("MinMileage")),
                                reader.GetInt32(reader.GetOrdinal("AvgMileage")),
                                reader.GetInt32(reader.GetOrdinal("MaxMileage")),
                                reader.GetInt32(reader.GetOrdinal("VehicleCount")));
                        }
                        else
                        {
                            salePrice = new SampleSummary<decimal>();
                            mileage = new SampleSummary<int>();
                        }
                    }
                }
            }
        }
    }
}
