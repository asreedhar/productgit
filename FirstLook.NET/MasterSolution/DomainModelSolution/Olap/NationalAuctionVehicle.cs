using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.DomainModel.Olap
{
    [Serializable]
    public class NationalAuctionVehicle : ReadOnlyBase<NationalAuctionVehicle>
    {
        #region Business Methods

        private readonly int _rowNumber;
        private readonly DateTime _saleDate;
        private readonly string _regionName;
        private readonly string _saleTypeName;
        private readonly string _series;
        private readonly decimal _salePrice;
        private readonly int _mileage;
        private readonly string _engine;
        private readonly string _transmission;

        public string Transmission
        {
            get { return _transmission; }
        }

        public string Engine
        {
            get { return _engine; }
        }

        public int Mileage
        {
            get { return _mileage; }
        }

        public decimal SalePrice
        {
            get { return _salePrice; }
        }

        public string Series
        {
            get { return _series; }
        }

        public string SaleTypeName
        {
            get { return _saleTypeName; }
        }

        public string RegionName
        {
            get { return _regionName; }
        }

        public DateTime SaleDate
        {
            get { return _saleDate; }
        }

        public int RowNumber
        {
            get { return _rowNumber; }
        }

        protected override object GetIdValue()
        {
            return RowNumber;
        }

        #endregion

        #region Constructors

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        internal NationalAuctionVehicle(IDataRecord record)
        {
            _rowNumber = record.GetInt32(record.GetOrdinal("RowNumber"));
            _saleDate = record.GetDateTime(record.GetOrdinal("SaleDate"));
            _regionName = record.GetString(record.GetOrdinal("RegionName"));
            _saleTypeName = record.GetString(record.GetOrdinal("SaleTypeName"));
            _series = record.GetString(record.GetOrdinal("Series"));
            _salePrice = record.GetDecimal(record.GetOrdinal("SalePrice"));
            _mileage = record.GetInt32(record.GetOrdinal("Mileage"));
            _engine = record.GetString(record.GetOrdinal("Engine"));
            _transmission = record.GetString(record.GetOrdinal("Transmission"));
        }

        #endregion
    }
}
