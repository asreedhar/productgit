using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.DomainModel.Olap
{
    [Serializable]
    public class NationalAuctionReportingPeriod : ReadOnlyBase<NationalAuctionReportingPeriod>
    {
        #region Business Methods

        private readonly int id;
        private readonly string name;

        public int Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Constructors

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        internal NationalAuctionReportingPeriod(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
        }

        #endregion
    }
}
