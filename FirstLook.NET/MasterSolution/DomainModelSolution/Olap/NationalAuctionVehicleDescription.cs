using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Olap
{
    [Serializable]
    public class NationalAuctionVehicleDescription : ReadOnlyBase<NationalAuctionVehicleDescription>
    {
        #region Business Methods

        private int _id;
        private string _make;
        private string _model;
        private int _modelYear;

        public string Make
        {
            get { return _make; }
        }

        public string Model
        {
            get { return _model; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} {1} {2}", ModelYear, Make, Model);
        }

        #endregion

        #region DataAccess

        public static NationalAuctionVehicleDescription GetVehicleDescription(string vin)
        {
            return DataPortal.Fetch<NationalAuctionVehicleDescription>(new Criteria(vin));
        }

        [Serializable]
        class Criteria
        {
            private readonly string vin;

            public Criteria(string vin)
            {
                this.vin = vin;
            }

            public string Vin
            {
                get { return vin; }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            Fetch(criteria.Vin);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA Reflection")]
        private void Fetch(string vin)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.OlapDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "NationalAuction.VehicleDescription#Fetch";

                    SimpleQuery.AddWithValue(command, "@Vin", vin, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _id = reader.GetInt32(reader.GetOrdinal("Id"));
                            _make = reader.GetString(reader.GetOrdinal("Make"));
                            _model = reader.GetString(reader.GetOrdinal("Model"));
                            _modelYear = reader.GetInt32(reader.GetOrdinal("ModelYear"));
                        }
                    }
                }
            }
        }

        #endregion
    }
}
