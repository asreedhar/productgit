using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using FirstLook.DomainModel.Lexicon.Categorization;

namespace FirstLook.DomainModel.Lexicon.WinForm
{
	public partial class LexiconTestForm : Form
	{
		private Catalog _catalog;
		private DataSet _catalogDataSet;

		public LexiconTestForm()
		{
			InitializeComponent();

			Load += LexiconTestForm_Load;
		}

		void LexiconTestForm_Load(object sender, EventArgs e)
		{
			modelYearBindingSource.BindingComplete += ModelYearBindingSource_BindingComplete;
			makeBindingSource.BindingComplete += MakeBindingSource_BindingComplete;
			lineBindingSource.BindingComplete += LineBindingSource_BindingComplete;

			modelYearBindingSource.DataSource = ModelYearList.GetModelYears();

			modelYearBindingSource.CurrentChanged += ModelYearBindingSource_CurrentChanged;
			makeBindingSource.CurrentChanged += MakeBindingSource_CurrentChanged;

			fetchCatalogButton.Click += FetchCatalogButton_Click;

			openToolStripMenuItem.Click += OpenToolStripMenuItem_Click;
			saveToolStripMenuItem.Click += SaveToolStripMenuItem_Click;
			exitToolStripMenuItem.Click += ExitToolStripMenuItem_Click;

			graphTestsToolStripMenuItem.Click += GraphTestsToolStripMenuItem_Click;
		}

		void GraphTestsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			GenerateGraphTests();
		}

		private void GenerateGraphTests()
		{
			if (CatalogDataSet != null)
			{
				saveTestCaseFileDialog.FileName = string.Format(CultureInfo.InvariantCulture, "Graph{0}{1}{2}.cs", SelectedModelYearText(), SelectedMakeText(), SelectedLineText());

				saveTestCaseFileDialog.FileName = Regex.Replace(saveTestCaseFileDialog.FileName, "[^0-9a-zA-Z_.-]", string.Empty);

				if (saveTestCaseFileDialog.ShowDialog() == DialogResult.OK)
				{
                    if (File.Exists(saveTestCaseFileDialog.FileName))
                    {
                        File.Delete(saveTestCaseFileDialog.FileName);
                    }

					using (FileStream stream = File.OpenWrite(saveTestCaseFileDialog.FileName))
					{
						using (StreamWriter writer = new StreamWriter(stream))
						{
							CodeCompileUnit testCases = new Categorization.Query.CodeGen(CatalogDataSet).CreateTestCases();

							CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");

							CodeGeneratorOptions options = new CodeGeneratorOptions();
							options.BlankLinesBetweenMembers = true;
							options.BracingStyle = "C";
							options.ElseOnClosing = false;
							options.IndentString = "    ";

							ICodeGenerator generator = provider.CreateGenerator(writer);

							generator.GenerateCodeFromCompileUnit(testCases, writer, options);
						}
					}
				}
			}
		}

		#region Data Binding

		void ModelYearBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
		{
			if (HasModelYears())
			{
				modelYearComboBox.SelectedIndex = 0;

                makeBindingSource.DataSource = MakeList.GetMakes(SelectedModelYear(), SelectedModelYear());
			}
		}

		void MakeBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
		{
			if (HasMakes())
			{
				makeComboBox.SelectedIndex = 0;

				lineBindingSource.DataSource = LineList.GetLines(SelectedModelYear(), SelectedMake());
			}
		}

		void LineBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
		{
			if (HasLines())
			{
				lineComboBox.SelectedIndex = 0;
			}
		}

		void ModelYearBindingSource_CurrentChanged(object sender, EventArgs e)
		{
			if (HasModelYears())
			{
				makeBindingSource.DataSource = MakeList.GetMakes(SelectedModelYear(), SelectedModelYear());
			}
		}

		void MakeBindingSource_CurrentChanged(object sender, EventArgs e)
		{
			if (HasMakes())
			{
				lineBindingSource.DataSource = LineList.GetLines(SelectedModelYear(), SelectedMake());
			}
		}

		void FetchCatalogButton_Click(object sender, EventArgs e)
		{
			if (HasModelYears() && HasMakes() && HasLines())
			{
				using (IDbConnection connection = Database.CreateConnection(Database.CategorizationDatabase))
				{
					if (connection.State != ConnectionState.Open)
						connection.Open();

					using (IDbCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.StoredProcedure;
						command.CommandText = "Categorization.Catalog#Fetch";

						Database.AddWithValue(command, "ModelYear", SelectedModelYear(), DbType.Int32);
						Database.AddWithValue(command, "MakeId", SelectedMake(), DbType.Int32);
						Database.AddWithValue(command, "LineId", SelectedLine(), DbType.Int32);

						using (IDataReader reader = command.ExecuteReader())
						{
							DataSet dataSet = new DataSet("Catalog");

							dataSet.Load(
								reader,
								LoadOption.OverwriteChanges,
								new string[]
									{
										"ModelYear",
										"Make",
										"Line",
										"Segment",
										"BodyType",
										"ModelFamily",
										"VinPattern",
                                        "Series",
                                        "Model",
										"PassengerDoor",
										"Engine",
										"FuelType",
										"DriveTrain",
										"Transmission",
										"Configuration",
                                        "ModelConfiguration",
                                        "ModelConfigurationMapping",
                                        "VinPatternMapping"
									});

							CatalogDataSet = dataSet;

							Catalog = Catalog.GetCatalog(SelectedModelYear(), SelectedMake(), SelectedLine(), dataSet.CreateDataReader());

                            modelBindingSource.DataSource = Catalog.Models.ToPresentation();

                            configurationBindingSource.DataSource = Catalog.Configurations.ToPresentation();
						}
					}
				}
			}
		}

		#endregion

		#region Menu Strip Event Handlers
		
		void OpenToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (openDataSetFileDialog.ShowDialog() == DialogResult.OK)
			{
				modelYearBindingSource.BindingComplete -= ModelYearBindingSource_BindingComplete;
				makeBindingSource.BindingComplete -= MakeBindingSource_BindingComplete;
				lineBindingSource.BindingComplete -= LineBindingSource_BindingComplete;

				modelYearBindingSource.CurrentChanged -= ModelYearBindingSource_CurrentChanged;
				makeBindingSource.CurrentChanged -= MakeBindingSource_CurrentChanged;

				CatalogDataSet = new DataSet();
				CatalogDataSet.ReadXml(openDataSetFileDialog.FileName);

				modelYearBindingSource.DataSource = ModelYearList.GetModelYears(CatalogDataSet.Tables["ModelYear"].CreateDataReader());
				makeBindingSource.DataSource = MakeList.GetMakes(CatalogDataSet.Tables["Make"].CreateDataReader());
				lineBindingSource.DataSource = LineList.GetLines(CatalogDataSet.Tables["Line"].CreateDataReader());

				modelYearComboBox.SelectedIndex = 0;
				makeComboBox.SelectedIndex = 0;
				lineComboBox.SelectedIndex = 0;

				Catalog = Catalog.GetCatalog(SelectedModelYear(), SelectedMake(), SelectedLine(), CatalogDataSet.CreateDataReader());

			    modelBindingSource.DataSource = Catalog.Models.ToPresentation();

			    configurationBindingSource.DataSource = Catalog.Configurations.ToPresentation();
			}
		}

		void SaveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (CatalogDataSet != null)
			{
				saveDataSetFileDialog.FileName = string.Format(
					CultureInfo.InvariantCulture,
					"DataSet-{0}{1}{2}",
					SelectedModelYearText(),
					SelectedMakeText(),
					SelectedLineText());

				saveDataSetFileDialog.FileName = Regex.Replace(saveDataSetFileDialog.FileName, "[^0-9a-zA-Z_]", string.Empty);

				if (saveDataSetFileDialog.ShowDialog() == DialogResult.OK)
				{
					CatalogDataSet.WriteXml(saveDataSetFileDialog.FileName, XmlWriteMode.WriteSchema);
				}
			}
		}

		void ExitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close();
		}

		#endregion

		#region Helper Methods

		private bool HasModelYears()
		{
			return modelYearComboBox.Items.Count > 0;
		}

		private bool HasMakes()
		{
			return makeComboBox.Items.Count > 0;
		}

		private bool HasLines()
		{
			return lineComboBox.Items.Count > 0;
		}

		private int SelectedModelYear()
		{
			return (int)modelYearComboBox.SelectedValue;
		}

		private string SelectedModelYearText()
		{
			return ((ModelYear)modelYearComboBox.SelectedItem).Name;
		}

		private int SelectedMake()
		{
			return (int)makeComboBox.SelectedValue;
		}

		private string SelectedMakeText()
		{
			return ((Make)makeComboBox.SelectedItem).Name;
		}

		private int SelectedLine()
		{
			return (int)lineComboBox.SelectedValue;
		}

		private string SelectedLineText()
		{
			return ((Line)lineComboBox.SelectedItem).Name;
		}

		#endregion

		#region Properties
		
		internal Catalog Catalog
		{
			get { return _catalog; }
			set { _catalog = value; }
		}

		internal DataSet CatalogDataSet
		{
			get { return _catalogDataSet; }
			set { _catalogDataSet = value; }
		}

		#endregion

	}
}