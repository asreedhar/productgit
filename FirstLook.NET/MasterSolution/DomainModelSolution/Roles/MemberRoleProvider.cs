using System.Collections.Specialized;
using System.Collections.Generic;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.Security;

using FirstLook.DomainModel.Oltp;

namespace FirstLook.DomainModel.Roles
{
    public class MemberRoleProvider : RoleProvider
    {
        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "CustomRoleProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "First Look Role provider");
            }

            base.Initialize(name, config);

            if (string.IsNullOrEmpty(config["applicationName"]))
            {
                _applicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            }
            else
            {
                _applicationName = config["applicationName"];
            }
        }

        private string _applicationName;

        public override string ApplicationName
        {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
        }

        public override void CreateRole(string roleName)
        {
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            return true;
        }

        public override string[] GetAllRoles()
        {
            return new string[] {
            "Dummy",
            "Administrator",
            "User",
            "AccountRepresentative"
        };
        }

        public override string[] GetRolesForUser(string username)
        {
            Member member = MemberFacade.FindByUserName(username);

            if (member == null)
            {
                throw new ArgumentException("User does not exist: " + username);
            }

            return new string[] { member.MemberType.ToString() };
        }

        public override string[] GetUsersInRole(string roleName)
        {
            try
            {
                MemberFinder finder = MemberFinder.Instance();

                MemberType memberType = (MemberType)Enum.Parse(typeof(MemberType), roleName);

                IList<Member> members = finder.Find(finder.CreateWhereClause("MemberType", memberType));

                string[] users = new string[members.Count];

                int i = 0;

                foreach (Member member in members)
                {
                    users[i++] = member.UserName;
                }

                return users;
            }
            catch (ArgumentException ae)
            {
                throw new ArgumentException("No such role: " + roleName, ae);
            }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            Member member = MemberFacade.FindByUserName(username);

            if (member == null)
            {
                throw new ArgumentException("User does not exist: " + username);
            }

            try
            {
                MemberType memberType = (MemberType)Enum.Parse(typeof(MemberType), roleName);

                return member.MemberType == memberType;
            }
            catch (ArgumentException ae)
            {
                throw new ArgumentException("No such role: " + roleName, ae);
            }
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public override bool RoleExists(string roleName)
        {
            try
            {
                Enum.Parse(typeof(MemberType), roleName);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return new string[] { };
        }
    }
}
