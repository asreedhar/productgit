
using System.Diagnostics.CodeAnalysis;

// deliberately few classes
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.DomainModel.Lexicon")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.DomainModel.Lexicon.Categorization.WebControls")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.DomainModel.Lexicon.Serialization")]

// all are lists and extend from binding list; the name is correct

[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.BodyTypeList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.ConfigurationList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.DriveTrainList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.EngineList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.FuelTypeList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.IEquivalenceClassList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelConfigurationList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelFamilyList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.PassengerDoorList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.SegmentList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.SeriesList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.TransmissionList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Categorization.VinPatternList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.BodyTypeList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntryList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.DriveTrainList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.EngineList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.FuelTypeList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.LineList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.MakeList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelYearList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.PassengerDoorList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.Search")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.SegmentList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.SeriesList")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.TransmissionList")]

// CSLA reflected code (the methods are used)

[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.BodyType.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Catalog.#GetModelConfigurations(FirstLook.DomainModel.Lexicon.Categorization.VinPattern)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Configuration.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.DriveTrain.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Engine.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.FuelType.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Model.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelConfiguration.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelConfigurationMapping.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelConfigurationMapping.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelFamily.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.PassengerDoor.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Segment.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Series.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Transmission.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.VinPattern.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.VinPatternMapping.#.ctor(FirstLook.DomainModel.Lexicon.Serialization.SerializationData)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.VinPatternMapping.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogSelection.#set_Item(FirstLook.DomainModel.Lexicon.Categorization.Model,System.Boolean)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.BodyType.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.BodyType.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.BodyType.#GetBodyType(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.BodyTypeList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.BodyTypeList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.BodyTypeList.#GetBodyTypes(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog+Criteria.#get_LineId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog+Criteria.#get_MakeId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog+Criteria.#get_ModelId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog+Criteria.#get_ModelYearId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog.#DataPortal_Fetch(FirstLook.DomainModel.Lexicon.Decoding.Catalog+Criteria)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog.#Fetch(System.Int32,System.Int32,System.Int32,System.Nullable`1<System.Int32>)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog.#MoveToNextResultSet(System.Data.IDataReader,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog.#VerifyNextResultSet(System.Data.IDataReader,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog.#VerifyThisResultSet(System.Data.IDataReader,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntry.#.ctor(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntry.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntry.#GetCatalogEntry(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntry.#GetInt32(System.Data.IDataRecord,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntryList.#.ctor(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntryList.#Fetch(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.CatalogEntryList.#GetCatalogEntries(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.DriveTrain.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.DriveTrain.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.DriveTrain.#GetDriveTrain(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.DriveTrainList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.DriveTrainList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.DriveTrainList.#GetDriveTrains(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Engine.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Engine.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Engine.#GetEngine(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.EngineList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.EngineList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.EngineList.#GetEngines(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.FuelType.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.FuelType.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.FuelType.#GetFuelType(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.FuelTypeList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.FuelTypeList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.FuelTypeList.#GetFuelTypes(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Line.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Line.#GetLine(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.LineList+Criteria.#get_MakeId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.LineList+Criteria.#get_ModelYearId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.LineList.#DataPortal_Fetch(FirstLook.DomainModel.Lexicon.Decoding.LineList+Criteria)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.LineList.#Fetch(System.Int32,System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Make.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Make.#GetMake(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.MakeList+ModelYearCriteria.#get_Id()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.MakeList.#DataPortal_Fetch(FirstLook.DomainModel.Lexicon.Decoding.MakeList+ModelYearCriteria)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.MakeList.#Fetch(System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Model.#GetModel(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList+Criteria.#get_LineId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList+Criteria.#get_MakeId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList+Criteria.#get_ModelYearId()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList.#.ctor(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList.#DataPortal_Fetch(FirstLook.DomainModel.Lexicon.Decoding.ModelList+Criteria)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList.#Fetch(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList.#GetModels(FirstLook.DomainModel.Lexicon.Decoding.Catalog,System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelYear.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelYear.#GetModelYear(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelYearList.#DataPortal_Fetch(FirstLook.DomainModel.Lexicon.Decoding.ModelYearList+Criteria)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelYearList.#Fetch()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.PassengerDoor.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.PassengerDoor.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.PassengerDoor.#GetPassengerDoor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.PassengerDoorList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.PassengerDoorList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.PassengerDoorList.#GetPassengerDoors(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Search+Criteria.#get_Id()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Search.#DataPortal_Fetch(FirstLook.DomainModel.Lexicon.Decoding.Search+Criteria)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Search.#Fetch(System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Segment.#.ctor(System.Data.IDataRecord,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Segment.#Fetch(System.Data.IDataRecord,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Segment.#GetSegment(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Segment.#GetSegment(System.Data.IDataRecord,System.String)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.SegmentList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.SegmentList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.SegmentList.#GetSegments(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Series.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Series.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Series.#GetSeries(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.SeriesList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.SeriesList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.SeriesList.#GetSeries(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Transmission.#.ctor(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Transmission.#Fetch(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Transmission.#GetTransmission(System.Data.IDataRecord)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.TransmissionList.#.ctor(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.TransmissionList.#Fetch(System.Data.IDataReader)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.TransmissionList.#GetTransmissions(System.Data.IDataReader)")]

// these are methods required by the interface

[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Series.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Engine.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Transmission.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Make.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Segment.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.PassengerDoor.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.Line.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.VinPattern.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.FuelType.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelFamily.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.DriveTrain.#GetObjectState()")]
[assembly: SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.ModelYear.#GetObjectState()")]

// object data sources need a no args contructor and these are such sources to wrap csla classes

[assembly: SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList+DataSource")]
[assembly: SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.MakeList+DataSource")]
[assembly: SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelYearList+DataSource")]
[assembly: SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog+DataSource")]
[assembly: SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "FirstLook.DomainModel.Lexicon.Decoding.LineList+DataSource")]

[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelList+DataSource.#GetModels(System.Int32,System.Int32,System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.MakeList+DataSource.#GetMakes(System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.ModelYearList+DataSource.#GetModelYears()")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog+DataSource.#GetCatalogEntries(System.Int32,System.Int32,System.Int32,System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.Catalog+DataSource.#GetCatalogEntries(System.Int32,System.Int32,System.Int32)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Decoding.LineList+DataSource.#GetLines(System.Int32,System.Int32)")]

// yes it is complex syntax (nested generics) but it does not warrant a new concrete type

[assembly: SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.CatalogPartition.#Transmissions")]
[assembly: SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.CatalogPartition.#FuelTypes")]
[assembly: SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.CatalogPartition.#DriveTrains")]
[assembly: SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.CatalogPartition.#Engines")]
[assembly: SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.CatalogPartition.#Models")]
[assembly: SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "FirstLook.DomainModel.Lexicon.Categorization.CatalogPartition.#PassengerDoors")]
