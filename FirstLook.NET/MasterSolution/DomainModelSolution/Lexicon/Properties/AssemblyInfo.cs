﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Lexicon")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("Lexicon")]

// Grant internal access to other libraries
[assembly: InternalsVisibleTo("FirstLook.DomainModel.Lexicon_Test")]
[assembly: InternalsVisibleTo("FirstLook.DomainModel.Lexicon_Test_WinForm")]