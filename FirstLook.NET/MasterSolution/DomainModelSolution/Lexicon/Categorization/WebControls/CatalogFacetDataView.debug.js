Type.registerNamespace('FirstLook.DomainModel.Lexicon.Categorization.WebControls');

FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView = function(element) {
	FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.initializeBase(this, [element]);
	this.delegates = {};
	var props = [
	"vin_fly_out_button_css_class",
	"vin_attribute_css_class",
	"vin_fly_out_border_break_css_class",
	"vin_fly_out_css_class",
	"vin_attribute_open_css_class",
	"vin_attribute_hover_css_class",
	"vin_fly_out_preview_button_css_class",
	"open_vin_fly_out",
	// ============
	// = Internal =
	// ============
	"has_mouse_over",
	"vin_attribute_mouse_over_el",
	"vin_attributes"
	];

	for (var i = 0, l = props.length; i < l; i++) {
		this.create_getter_setter(props[i]);
	};
};

FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.prototype = {
	initialize: function() {
		FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.callBaseMethod(this, "initialize");
	},
	updated: function() {
		var el = this.get_element();

		this.delegates['element_property_change'] = Function.createDelegate(this, this.onPropertyChanged);
		this.delegates['element_mouseover'] = Function.createDelegate(this, this.onMouseOver);
		this.delegates['element_mouseout'] = Function.createDelegate(this, this.onMouseOut);
		this.delegates['element_click'] = Function.createDelegate(this, this.onClick);

		this.add_handler(el, 'mouseover', this.delegates['element_mouseover']);
		this.add_handler(el, 'mouseout', this.delegates['element_mouseout']);
		this.add_handler(el, 'click', this.delegates['element_click']);

		this.add_propertyChanged(this.delegates['element_property_change']);

		FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.callBaseMethod(this, "updated");
	},
	dispose: function() {
		this.remove_handlers();

		this.remove_propertyChanged(this.delegates['element_property_change']);

		this.delegates = undefined;

		FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.callBaseMethod(this, "dispose");
	},
	// ==================
	// = Start: Methods =
	// ==================
	get_vin_attributes: function() {
		if ( !! !this._vin_attributes) {
			var dls = this.get_element().getElementsByTagName('dl');
			this._vin_attributes = [];
			for (var i = 0, l = dls.length; i < l; i++) {
				if (dls[i].className.indexOf(this.get_vin_attribute_css_class()) != -1) {
					this._vin_attributes.push(dls[i]);
				}
			};
		};

		return this._vin_attributes;
	},
	reset_hover_class: function() {
		var vin_attributes = this.get_vin_attributes();
		for (var i = 0, l = vin_attributes.length; i < l; i++) {
			Sys.UI.DomElement.removeCssClass(vin_attributes[i], this.get_vin_attribute_hover_css_class());
		}
	},
	reset_open_class: function() {
		var vin_attributes = this.get_vin_attributes();
		for (var i = 0, l = vin_attributes.length; i < l; i++) {
			Sys.UI.DomElement.removeCssClass(vin_attributes[i], this.get_vin_attribute_open_css_class());
		}
	},
	get_vin_attribute_from_event_target: function(target) {
		var dl;
		if (target.tagName.toUpperCase() == "DD" || target.tagName.toUpperCase() == "DT") {
			dl = target.parentElement;
		} else
		if (target.tagName.toUpperCase() == "LI" && target.parentElement.parentElement.parentElement) {
			dl = target.parentElement.parentElement.parentElement;
		}

		if ( !! dl && dl.className.indexOf(this.get_vin_attribute_css_class()) != -1) {
			return dl;
		}
	},
	get_vin_open_button_from_vin_attribute: function(vin_attribute) {
		var inputs = vin_attribute.getElementsByTagName("input");

		for (var i = 0, l = inputs.length; i < l; i++) {
			if (inputs[i].className.indexOf(this.get_vin_fly_out_button_css_class()) != -1) {
				return inputs[i];
			}
		};
	},
	// = End: Methods =
	// =================
	// = Start: Events =
	// =================
	onPropertyChanged: function(sender, args) {
		var property = args.get_propertyName(),
		source_class = sender.get_name();

		if (property == "vin_attribute_mouse_over_el" && !!this.get_open_vin_fly_out()) {
			this.reset_hover_class();
			if ( !! this.get_vin_attribute_mouse_over_el()) {
				Sys.UI.DomElement.addCssClass(this.get_vin_attribute_mouse_over_el(), this.get_vin_attribute_hover_css_class());
			}
		}
	},
	onMouseOver: function(e) {
		if ( !! !e.target) {
			return;
		}

		var vin_attribute = this.get_vin_attribute_from_event_target(e.target);

		if ( !! vin_attribute) {
			this.set_vin_attribute_mouse_over_el(vin_attribute);
		};

		this.set_has_mouse_over(true);
	},
	onMouseOut: function(e) {
		if ( !! !e.target) {
			return;
		}

		var vin_attribute = this.get_vin_attribute_from_event_target(e.target);

		if ( !! vin_attribute) {
			this.set_vin_attribute_mouse_over_el(null);
		};

		this.set_has_mouse_over(false);
	},
	onClick: function(e) {
		if ( !! !e.target) {
			return;
		}

		var vin_attribute = this.get_vin_attribute_from_event_target(e.target),
		vin_attrivute_button;

		if ( !! vin_attribute) {
			vin_attrivute_button = this.get_vin_open_button_from_vin_attribute( vin_attribute );
			if ( !!vin_attrivute_button && !!vin_attrivute_button.click ) {
				vin_attrivute_button.click();
			}
		}
	},
	// = End: Events =	
	create_getter_setter: function(property) {
		// =================================================================
		// = Generic Function to create AJAX.net style getters and setters =
		// =================================================================
		if (this["get_" + property] === undefined) {
			this["get_" + property] = (function(thisProp) {
				return function get_property() {
					return this[thisProp];
				};
			})("_" + property);
		};
		if (this["set_" + property] === undefined) {
			this["set_" + property] = (function(thisProp, raiseType) {
				return function set_property(value) {
					if (value !== this[thisProp]) {
						this[thisProp] = value;
						this.raisePropertyChanged(raiseType);
					};
				};
			})("_" + property, property);
		};
	},
	add_handler: function(el, type, fn) {
		if (this._eventCache === undefined) {
			this._eventCache = [];
		}
		this._eventCache.push([el, type, fn]);
		$addHandler(el, type, fn);
	},
	remove_handlers: function() {
		if (this._eventCache !== undefined) {
			for (var i = 0, l = this._eventCache.length; i < l; i++) {
				$removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
				this._eventCache[i][0] = undefined;
				this._eventCache[i][1] = undefined;
				this._eventCache[i][2] = undefined;
			};
			delete this._eventCache;
		};
	},
	fire_event: function(el, ev) {
		if (document.createEvent) {
			var new_event = document.createEvent('HTMLEvents');
			new_event.initEvent(ev, true, false);

			el.dispatchEvent(new_event);
		}
		else if (document.createEventObject) {
			el.fireEvent('on' + ev);
		}
	}
};

if (FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.registerClass != undefined)
 FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.registerClass('FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView', Sys.UI.Behavior);