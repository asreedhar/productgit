using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Lexicon.Categorization.Query;

[assembly: WebResource("FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.js", "text/javascript")]
[assembly: WebResource("FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.debug.js", "text/javascript")]

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
    [
    ParseChildren(false),
    PersistChildren(false)
    ]
    public class CatalogFacetDataView : DataBoundControl, IPostBackDataHandler, IScriptControl
    {
        #region Properties

        private ScriptManager _manager;

        private Catalog _catalog;

        private Catalog Catalog
        {
            get
            {
                return _catalog;
            }
            set
            {
                if (_catalog != value)
                {
                    _catalog = value;
                }
            }
        }

        private List<Type> _hasNullAttribute;

        private List<Type> HasNullAttribute
        {
            get
            {
                if (_hasNullAttribute == null)
                {
                    _hasNullAttribute = new List<Type>();
                }

                return _hasNullAttribute;
            }
        }

        private static Dictionary<string, Type> _vinAttributeMap;

        private static Dictionary<string, Type> VinAttributeMap
        {
            get
            {
                if (_vinAttributeMap == null)
                {
                    _vinAttributeMap = new Dictionary<string, Type>();
                    _vinAttributeMap.Add(SR.VinAttributeModelFamilyID, typeof(ModelFamily));
                    _vinAttributeMap.Add(SR.VinAttributeBodyTypeID, typeof(BodyType));
                    _vinAttributeMap.Add(SR.VinAttributesSegmentID, typeof(Segment));
                    _vinAttributeMap.Add(SR.VinAttributePassengerDoorsID, typeof(PassengerDoor));
                    _vinAttributeMap.Add(SR.VinAttributeFuelTypeID, typeof(FuelType));
                    _vinAttributeMap.Add(SR.VinAttributesDriveTrainID, typeof(DriveTrain));
                    _vinAttributeMap.Add(SR.VinAttributesTranmissionID, typeof(Transmission));
                    _vinAttributeMap.Add(SR.VinAttributesEngineID, typeof(Engine));
                    _vinAttributeMap.Add(SR.VinAttributesSeriesID, typeof(Series));
                }

                return _vinAttributeMap;
            }
        }

        private static Dictionary<Type, SelectionType> _attributeTypeMap;

        private static Dictionary<Type, SelectionType> AttributeTypeMap
        {
            get
            {
                if (_attributeTypeMap == null)
                {
                    _attributeTypeMap = new Dictionary<Type, SelectionType>();
                    _attributeTypeMap.Add(typeof(ModelFamily), SelectionType.ModelFamily);
                    _attributeTypeMap.Add(typeof(Segment), SelectionType.Segment);
                    _attributeTypeMap.Add(typeof(BodyType), SelectionType.BodyType);
                    _attributeTypeMap.Add(typeof(PassengerDoor), SelectionType.PassengerDoor);
                    _attributeTypeMap.Add(typeof(FuelType), SelectionType.FuelType);
                    _attributeTypeMap.Add(typeof(DriveTrain), SelectionType.DriveTrain);
                    _attributeTypeMap.Add(typeof(Transmission), SelectionType.Transmission);
                    _attributeTypeMap.Add(typeof(Engine), SelectionType.Engine);
                    _attributeTypeMap.Add(typeof(Series), SelectionType.Series);
                }

                return _attributeTypeMap;
            }
        }

        private Dictionary<SelectionType, IList<int?>> _attributeDelta;

        private Dictionary<SelectionType, IList<int?>> AttributeDelta
        {
            get
            {
                if (_attributeDelta == null)
                {
                    _attributeDelta = new Dictionary<SelectionType, IList<int?>>();
                }

                return _attributeDelta;
            }
        }

        private Dictionary<int, bool> _selection;

        private Dictionary<int, bool> Selection
        {
            get
            {
                if (_selection == null)
                {
                    _selection = new Dictionary<int, bool>();
                }

                return _selection;
            }
        }

        private int? _modelConfigurationId;

        public int? ModelConfigurationId
        {
            get
            {
                return _modelConfigurationId;
            }
            set
            {
                if (_modelConfigurationId != value)
                {
                    _modelConfigurationId = value;
                }
            }
        }

        private string _displayFlyoutVinAttribute;

        private Facet _facet;

        private Facet Facet
        {
            get
            {
                if (_facet == null)
                {
                    _facet = new Facet();

                    if (Catalog != null)
                    {
                        _facet.Initialize(Catalog.ModelConfigurations);

                        _facet.DataBind(FacetDataBoundViewState);

                        ManageFacetDelta();
                    }

                    _facet.SelectionChanged += FacetSelectionChanged;
                    _facet.EmptySelectionSet += FacetSelectionEmpty;
                    _facet.SelectionUnchanged += FacetSelectionUnChanged;
                }

                return _facet;
            }
        }

        private string DisplayFlyoutVinAttribute
        {
            get
            {
                return _displayFlyoutVinAttribute;
            }
            set
            {
                if (value != _displayFlyoutVinAttribute)
                {
                    _displayFlyoutVinAttribute = value;
                }
            }
        }

        private string _vinPattern;

        public string VinPattern
        {
            get
            {
                return _vinPattern;
            }
            set
            {
                if (value != _vinPattern)
                {
                    _vinPattern = value;
                }
            }
        }

        public string ModelConfigurationFilter
        {
            get
            {
                if (ModelConfigurations.Count == 0) return "-1";

                StringBuilder sb = new StringBuilder();

                foreach (ModelConfiguration modelConfiguration in ModelConfigurations)
                {
                    sb.Append(modelConfiguration.Id).Append(',');
                }

                return sb.ToString(0, sb.Length - 1);
            }
        }

        public IList<ModelConfiguration> ModelConfigurations
        {
            get
            {
                return Facet.IncludedFacetNode.ModelConfigurations;
            }
        }

        private bool _autoGenerateRestoreButton;
        public bool AutoGenerateRestoreButton
        {
            get
            {
                return _autoGenerateRestoreButton;
            }
            set
            {
                if (_autoGenerateRestoreButton != value)
                {
                    _autoGenerateRestoreButton = value;
                }
            }
        }

        private bool _autoGenerateSaveButton;
        public bool AutoGenerateSaveButton
        {
            get { return _autoGenerateSaveButton; }
            set
            {
                if (_autoGenerateSaveButton != value)
                {
                    _autoGenerateSaveButton = value;
                }
            }
        }

        private bool _autoGenerateResetButton;
        public bool AutoGenerateResetButton
        {
            get { return _autoGenerateResetButton; }
            set
            {
                if (_autoGenerateResetButton != value)
                {
                    _autoGenerateResetButton = value;
                }
            }
        }

        private string _flyoutPreviewButtonText;
        public string FlyoutPreviewButtonText
        {
            get
            {
                return string.IsNullOrEmpty(_flyoutPreviewButtonText) ? SR.VinFlyOutPreviewButtonValue : _flyoutPreviewButtonText;
            }
            set
            {
                if (value == SR.VinFlyOutPreviewButtonValue && value != _flyoutPreviewButtonText)
                {
                    _flyoutPreviewButtonText = value;
                }
            }
        }

        #endregion

        #region Events

        private static readonly object EventCatalogDataBound = new object();

        private static readonly object EventCatalogSelectionChanged = new object();

        private static readonly object EventCatalogSelectionMessage = new object();

        private static readonly object EventCatalogSelectionUpdated = new object();

        private static readonly object EventCatalogSelectionReset = new object();

        private static readonly object EventCatalogSelectionRestore = new object();

        public event EventHandler<CatalogDataBoundEventArgs> CatalogDataBound
        {
            add { Events.AddHandler(EventCatalogDataBound, value); }
            remove { Events.RemoveHandler(EventCatalogDataBound, value); }
        }

        protected virtual void OnCatalogDataBound(CatalogDataBoundEventArgs e)
        {
            EventHandler<CatalogDataBoundEventArgs> handler = (EventHandler<CatalogDataBoundEventArgs>)Events[EventCatalogDataBound];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<CatalogSelectionEventArgs> CatalogSelectionChanged
        {
            add { Events.AddHandler(EventCatalogSelectionChanged, value); }
            remove { Events.RemoveHandler(EventCatalogSelectionChanged, value); }
        }

        protected virtual void OnCatalogSelectionChanged(CatalogSelectionEventArgs e)
        {
            EventHandler<CatalogSelectionEventArgs> handler = (EventHandler<CatalogSelectionEventArgs>)Events[EventCatalogSelectionChanged];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<CatalogSelectionMessageEventArgs> CatalogSelectionMessage
        {
            add { Events.AddHandler(EventCatalogSelectionMessage, value); }
            remove { Events.RemoveHandler(EventCatalogSelectionMessage, value); }
        }

        protected virtual void OnCatalogSelectionMessage(CatalogSelectionMessageEventArgs e)
        {
            EventHandler<CatalogSelectionMessageEventArgs> handler = (EventHandler<CatalogSelectionMessageEventArgs>)Events[EventCatalogSelectionMessage];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler CatalogSelectionUpdated
        {
            add { Events.AddHandler(EventCatalogSelectionUpdated, value); }
            remove { Events.RemoveHandler(EventCatalogSelectionUpdated, value); }
        }

        protected virtual void OnCatalogSelectionUpdated(EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[EventCatalogSelectionUpdated];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler CatalogSelectionReset
        {
            add { Events.AddHandler(EventCatalogSelectionReset, value); }
            remove { Events.RemoveHandler(EventCatalogSelectionReset, value); }
        }

        protected virtual void OnCatalogSelectionReset(EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[EventCatalogSelectionReset];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler CatalogSelectionRestore
        {
            add { Events.AddHandler(EventCatalogSelectionRestore, value); }
            remove { Events.RemoveHandler(EventCatalogSelectionRestore, value); }
        }

        protected virtual void OnCatalogSelectionRestore(EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[EventCatalogSelectionRestore];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Methods

        protected void HandleEvents(string commandName)
        {
            // Short Circuit if no Command Name
            if (string.IsNullOrEmpty(commandName))
                return;

            if (commandName.Equals(SR.SaveButtonName))
            {
                OnCatalogSelectionUpdated(new EventArgs());

                DisplayFlyoutVinAttribute = string.Empty;
            }
            else if (commandName.Equals(SR.RestoreButtonName))
            {
                OnCatalogSelectionRestore(new EventArgs());

                DisplayFlyoutVinAttribute = string.Empty;
            }
            else if (commandName.Equals(SR.ResetButtonName))
            {
                OnCatalogSelectionReset(new EventArgs());

                DisplayFlyoutVinAttribute = string.Empty;
            }
            else if (commandName.Equals(SR.VinFlyOutCancelButtonName))
            {
                DisplayFlyoutVinAttribute = string.Empty;
            }
            else if (commandName.Equals(SR.VinFlyOutPreviewButtonName))
            {
                HandleChangedAttributes(VinAttributeMap[DisplayFlyoutVinAttribute], _setAttributes);

                DisplayFlyoutVinAttribute = string.Empty;
            }
        }

        private void HandleChangedAttributes(Type type, IList<string> attributes)
        {           
            if (type == typeof(ModelFamily))
            {
                HandleAddRemove(SR.VinAttributeModelFamilyID, attributes, Catalog.ModelFamilies);
            }
            else if (type == typeof(Series))
            {
                HandleAddRemove(SR.VinAttributesSeriesID, attributes, Catalog.Series);
            }
            else if (type == typeof(BodyType))
            {
                HandleAddRemove(SR.VinAttributeBodyTypeID, attributes, Catalog.BodyTypes);
            }
            else if (type == typeof(PassengerDoor))
            {
                HandleAddRemove(SR.VinAttributePassengerDoorsID, attributes, Catalog.PassengerDoors);
            }
            else if (type == typeof(FuelType))
            {
                HandleAddRemove(SR.VinAttributeFuelTypeID, attributes, Catalog.FuelTypes);
            }
            else if (type == typeof(DriveTrain))
            {
                HandleAddRemove(SR.VinAttributesDriveTrainID, attributes, Catalog.DriveTrains);
            }
            else if (type == typeof(Transmission))
            {
                HandleAddRemove(SR.VinAttributesTranmissionID, attributes, Catalog.Transmissions);
            }
            else if (type == typeof(Engine))
            {
                HandleAddRemove(SR.VinAttributesEngineID, attributes, Catalog.Engines);
            }
            else if (type == typeof(Segment))
            {
                HandleAddRemove(SR.VinAttributesSegmentID, attributes, Catalog.Segments);
            }
        }

        private void HandleChangedAttribute(Type type, INameable attribute)
        {
            IList<string> attributeKeys = new List<string>();

            string prefix = string.Empty;

            if (type == typeof(ModelFamily))
            {
                prefix = SR.VinAttributeModelFamilyID;
                for (int i = 0; i < Catalog.ModelFamilies.Count; i++)
                {
                    if (attribute == Catalog.ModelFamilies[i] || Facet.IncludedFacetNode[type].Contains(Catalog.ModelFamilies[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(Series))
            {
                prefix = SR.VinAttributesSeriesID;
                for (int i = 0; i < Catalog.Series.Count; i++)
                {
                    if (attribute == Catalog.Series[i] || Facet.IncludedFacetNode[type].Contains(Catalog.Series[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(BodyType))
            {
                prefix = SR.VinAttributeBodyTypeID;
                for (int i = 0; i < Catalog.BodyTypes.Count; i++)
                {
                    if (attribute == Catalog.BodyTypes[i] || Facet.IncludedFacetNode[type].Contains(Catalog.BodyTypes[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(PassengerDoor))
            {
                prefix = SR.VinAttributePassengerDoorsID;
                for (int i = 0; i < Catalog.PassengerDoors.Count; i++)
                {
                    if (attribute == Catalog.PassengerDoors[i] || Facet.IncludedFacetNode[type].Contains(Catalog.PassengerDoors[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(FuelType))
            {
                prefix = SR.VinAttributeFuelTypeID;
                for (int i = 0; i < Catalog.FuelTypes.Count; i++)
                {
                    if (attribute == Catalog.FuelTypes[i] || Facet.IncludedFacetNode[type].Contains(Catalog.FuelTypes[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(DriveTrain))
            {
                prefix = SR.VinAttributesDriveTrainID;
                for (int i = 0; i < Catalog.DriveTrains.Count; i++)
                {
                    if (attribute == Catalog.DriveTrains[i] || Facet.IncludedFacetNode[type].Contains(Catalog.DriveTrains[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(Transmission))
            {
                prefix = SR.VinAttributesDriveTrainID;
                for (int i = 0; i < Catalog.Transmissions.Count; i++)
                {
                    if (attribute == Catalog.Transmissions[i] || Facet.IncludedFacetNode[type].Contains(Catalog.Transmissions[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(Engine))
            {
                prefix = SR.VinAttributesEngineID;
                for (int i = 0; i < Catalog.Engines.Count; i++)
                {
                    if (attribute == Catalog.Engines[i] || Facet.IncludedFacetNode[type].Contains(Catalog.Engines[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }
            else if (type == typeof(Segment))
            {
                prefix = SR.VinAttributesSegmentID;
                for (int i = 0; i < Catalog.Segments.Count; i++)
                {
                    if (attribute == Catalog.Segments[i] || Facet.IncludedFacetNode[type].Contains(Catalog.Segments[i]))
                    {
                        attributeKeys.Add(prefix + IdSeparator + i);
                    }
                }
            }

            HandleChangedAttributes(type, attributeKeys);
        }

        private bool inHandleAddRemove = false;

        private void HandleAddRemove<TKey>(string prefix, IList<string> included, IEnumerable<TKey> full) where TKey : INameable<TKey>
        {
            if( inHandleAddRemove )
                return;

            try
            {
                inHandleAddRemove = true;

                bool selectAll = included.Count == 0; /// Nothing is Everything, PM

                List<int?> toAdd = new List<int?>();

                for (int i = 0, l = included.Count; i < l; i++)
                {
                    string suffix = included[i].Substring(prefix.Length + 1);

                    if (suffix == "a")
                    {
                        selectAll = true;

                        break;
                    }
                    if (suffix == "n")
                    {
                        toAdd.Add(null);
                    }
                    else
                    {
                        int idx;

                        if (int.TryParse(suffix, out idx))
                        {
                            toAdd.Add(idx);
                        }
                    }
                }

                if (HasNullAttribute.Contains(typeof(TKey)) && (selectAll || toAdd.Contains(null)))
                {
                    Facet.Add<TKey>(null);
                    AddAttributeDelta(typeof(TKey), null);
                }
                else
                {
                    Facet.Remove<TKey>(null);
                    RemoveAttributeDelta(typeof(TKey), null);
                }

                List<TKey> itemsToAdd = new List<TKey>();

                int j = 0;
                foreach (TKey item in full)
                {
                    bool isItemSelected = toAdd.Contains(j);

                    if (isItemSelected || selectAll)
                    {
                        itemsToAdd.Add(item);
                        AddAttributeDelta(typeof(TKey), j);
                        if (typeof(TKey) == typeof(Series))
                        {
                            foreach (TKey key in full)
                            {
                                if (!Equals(key, null) && key.Name == item.Name)
                                {
                                    itemsToAdd.Add(key);
                                }
                            }
                        }
                    }
                    else
                    {
                        RemoveAttributeDelta(typeof(TKey), j);
                    }

                    j++;
                }

                foreach (TKey item in full)
                {
                    if (itemsToAdd.Contains(item))
                    {
                        Facet.Add(item);
                    }
                }

                foreach (TKey item in full)
                {
                    if (!itemsToAdd.Contains(item))
                    {
                        Facet.Remove(item);
                    }
                }

                ManageFacetDelta();
            }
            finally
            {
                inHandleAddRemove = false;
            }
        }

        private void AddAttributeDelta(Type type, int? i)
        {
            SelectionType selectionType = AttributeTypeMap[type];

            if (!AttributeDelta.ContainsKey(selectionType))
            {
                AttributeDelta[selectionType] = new List<int?>();
            }
            if (!AttributeDelta[selectionType].Contains(i))
            {
                AttributeDelta[selectionType].Add(i);
            }
        }

        private void RemoveAttributeDelta(Type type, int? i)
        {
            SelectionType selectionType = AttributeTypeMap[type];

            if (AttributeDelta.ContainsKey(selectionType))
            {
                AttributeDelta[selectionType].Remove(i);
                if (AttributeDelta[selectionType].Count == 0)
                {
                    AttributeDelta.Remove(selectionType);
                }
            }
        }

        private void FacetSelectionChanged(object sender, FacetConfigurationEventArgs e)
        {
            Selection[e.ModelConfiguration] = e.Selected;

            CatalogSelectionEventArgs args = new CatalogSelectionEventArgs(e.ModelConfiguration, e.Selected);

            OnCatalogSelectionChanged(args);
        }

        private void FacetSelectionUnChanged(object sender, FacetSelectionEventArgs e)
        {
            bool cancel = false;

            if (e.Selected)
            {
                bool sendMessage = false;

                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("{0} is only available with:\r\n", e.Attribute.Name);

                foreach (KeyValuePair<Type, IList<INameable>> attributes in e.PossibleAttributes)
                {
                    if (attributes.Value.Count == 1)
                    {
                        foreach (INameable nameable in attributes.Value)
                        {
                            sendMessage = true;
                            sb.AppendFormat("{0}, ", nameable.Name);
                            HandleChangedAttribute(attributes.Key, nameable);
                        }

                        cancel = true;
                    }
                }

                if (sendMessage)
                {
                    CatalogSelectionMessageEventArgs eventArgs = new CatalogSelectionMessageEventArgs(sb.ToString().TrimEnd(',',' '));

                    OnCatalogSelectionMessage(eventArgs); 
                }
            }

            e.Cancel = cancel;
        }
        
        private void FacetSelectionEmpty(object sender, FacetSelectionEventArgs e)
        {
            bool cancel = true;

            foreach (KeyValuePair<Type, IList<INameable>> attributes in e.PossibleAttributes)
            {
                if (attributes.Value.Count == 1)
                {
                    foreach (INameable nameable in attributes.Value)
                    {
                        HandleChangedAttribute(attributes.Key, nameable);
                    }

                    cancel = false;
                }
            }

            string message = string.Format("Removing {0} will result in an empty search, please make another choice", e.Attribute.Name);

            CatalogSelectionMessageEventArgs eventArgs = new CatalogSelectionMessageEventArgs(message);

            OnCatalogSelectionMessage(eventArgs);

            e.Cancel = cancel;
        }

        private void ManageFacetDelta()
        {
            foreach (KeyValuePair<SelectionType, IList<int?>> attribute in AttributeDelta)
            {
                foreach (int? i in attribute.Value)
                {
                    if (i.HasValue)
                    {
                        switch (attribute.Key)
                        {
                            case SelectionType.ModelFamily:
                                if (!Facet.IncludedFacetNode[typeof(ModelFamily)].Contains(Catalog.ModelFamilies[i.Value]))
                                    Facet.IncludedFacetNode[typeof(ModelFamily)].Add(Catalog.ModelFamilies[i.Value]);
                                break;
                            case SelectionType.Segment:
                                if (!Facet.IncludedFacetNode[typeof(Segment)].Contains(Catalog.Segments[i.Value]))
                                    Facet.IncludedFacetNode[typeof(Segment)].Add(Catalog.Segments[i.Value]);
                                break;
                            case SelectionType.BodyType:
                                if (!Facet.IncludedFacetNode[typeof(BodyType)].Contains(Catalog.BodyTypes[i.Value]))
                                    Facet.IncludedFacetNode[typeof(BodyType)].Add(Catalog.BodyTypes[i.Value]);
                                break;
                            case SelectionType.PassengerDoor:
                                if (!Facet.IncludedFacetNode[typeof(PassengerDoor)].Contains(Catalog.PassengerDoors[i.Value]))
                                    Facet.IncludedFacetNode[typeof(PassengerDoor)].Add(Catalog.PassengerDoors[i.Value]);
                                break;
                            case SelectionType.FuelType:
                                if (!Facet.IncludedFacetNode[typeof(FuelType)].Contains(Catalog.FuelTypes[i.Value]))
                                    Facet.IncludedFacetNode[typeof(FuelType)].Add(Catalog.FuelTypes[i.Value]);
                                break;
                            case SelectionType.DriveTrain:
                                if (!Facet.IncludedFacetNode[typeof(DriveTrain)].Contains(Catalog.DriveTrains[i.Value]))
                                    Facet.IncludedFacetNode[typeof(DriveTrain)].Add(Catalog.DriveTrains[i.Value]);
                                break;
                            case SelectionType.Transmission:
                                if (!Facet.IncludedFacetNode[typeof(Transmission)].Contains(Catalog.Transmissions[i.Value]))
                                    Facet.IncludedFacetNode[typeof(Transmission)].Add(Catalog.Transmissions[i.Value]);
                                break;
                            case SelectionType.Engine:
                                if (!Facet.IncludedFacetNode[typeof(Engine)].Contains(Catalog.Engines[i.Value]))
                                    Facet.IncludedFacetNode[typeof(Engine)].Add(Catalog.Engines[i.Value]);
                                break;
                            case SelectionType.Series:
                                if (!Facet.IncludedFacetNode[typeof(Series)].Contains(Catalog.Series[i.Value]))
                                    Facet.IncludedFacetNode[typeof(Series)].Add(Catalog.Series[i.Value]);
                                break;
                        }
                    }
                    else
                    {
                        switch (attribute.Key)
                        {
                            case SelectionType.ModelFamily:
                                if (!Facet.IncludedFacetNode[typeof (ModelFamily)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (ModelFamily)].Add(null);
                                break;
                            case SelectionType.Segment:
                                if (!Facet.IncludedFacetNode[typeof (Segment)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (Segment)].Add(null);
                                break;
                            case SelectionType.BodyType:
                                if (!Facet.IncludedFacetNode[typeof (BodyType)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (BodyType)].Add(null);
                                break;
                            case SelectionType.PassengerDoor:
                                if (!Facet.IncludedFacetNode[typeof (PassengerDoor)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (PassengerDoor)].Add(null);
                                break;
                            case SelectionType.FuelType:
                                if (!Facet.IncludedFacetNode[typeof (FuelType)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (FuelType)].Add(null);
                                break;
                            case SelectionType.DriveTrain:
                                if (!Facet.IncludedFacetNode[typeof (DriveTrain)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (DriveTrain)].Add(null);
                                break;
                            case SelectionType.Transmission:
                                if (!Facet.IncludedFacetNode[typeof (Transmission)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (Transmission)].Add(null);
                                break;
                            case SelectionType.Engine:
                                if (!Facet.IncludedFacetNode[typeof (Engine)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (Engine)].Add(null);
                                break;
                            case SelectionType.Series:
                                if (!Facet.IncludedFacetNode[typeof (Series)].Contains(null))
                                    Facet.IncludedFacetNode[typeof (Series)].Add(null);
                                break;
                        }
                    }
                }
            }
        }

        public IEnumerable<int> FindCatalogEntryId(int modelConfigurationId)
        {
            return Catalog.FindCatalogEntryId(modelConfigurationId);
        }

        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }

        #region Render Methods

        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Div; }
        }

        protected override string TagName
        {
            get { return "div"; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!DesignMode)
            {
                _manager = ScriptManager.GetCurrent(Page);

                if (_manager != null)
                {
                    _manager.RegisterScriptControl(this);
                }
                else
                {
                    throw new InvalidOperationException("You must have a ScriptManager!");
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                _manager.RegisterScriptDescriptors(this);
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (Catalog != null)
            {
                RenderCatalogFacetControls(writer);
            }
        }

        private void RenderCatalogFacetControls(HtmlTextWriter writer)
        {
            if (AutoGenerateRestoreButton)
            {
                RenderRestoreButton(writer);
            }

            if (Catalog.ModelFamilies.Count > 1)
            {
                RenderModelFamilytAttributes(writer);
            }

            RenderSegmentAttributes(writer);

            RenderPassengarDoorsAttributes(writer);

            RenderFuelTypeAttributes(writer);

            RenderDriveTrainAttributes(writer);

            RenderTranmissionAttributes(writer);

            RenderEngineAttributes(writer);

            RenderSeriesAttributes(writer);

            writer.RenderBeginTag(HtmlTextWriterTag.Br);

            writer.RenderEndTag();

            if (AutoGenerateResetButton)
            {
                RenderResetButton(writer);
            }

            if (AutoGenerateSaveButton)
            {
                RenderSaveButton(writer);
            }
        }

        private void RenderRestoreButton(HtmlTextWriter writer)
        {
            RenderButton(writer, SR.RestoreButtonName, SR.RestoreButtonCssClass, SR.RestoreButtonValue,
                         SR.RestoreButtonType);
        }

        private void RenderResetButton(HtmlTextWriter writer)
        {
            RenderButton(writer, SR.ResetButtonName, SR.ResetButtonCssClass, SR.ResetButtonValue, SR.ResetButtonType);
        }

        private void RenderSaveButton(HtmlTextWriter writer)
        {
            RenderButton(writer, SR.SaveButtonName, SR.SaveButtonCssClass, SR.SaveButtonValue, SR.SaveButtonType);
        }

        private void RenderModelFamilytAttributes(HtmlTextWriter writer)
        {
            const string id = SR.VinAttributeModelFamilyID;

            const string title = SR.VinAttributesModleFamilyTitle;

            RenderVinAttributes(writer, title, id, Catalog.ModelFamilies);
        }

        private void RenderSegmentAttributes(HtmlTextWriter writer)
        {
            if (Catalog.Segments.Count == 1 && (Catalog.Segments[0].IsTruck || Catalog.Segments[0].IsVan))
            {
                const string id = SR.VinAttributeBodyTypeID;

                const string title = SR.VinAttributesBodyTypeTitle;

                RenderVinAttributes(writer, title, id, Catalog.BodyTypes);
            }
            else
            {
                const string id = SR.VinAttributesSegmentID;

                const string title = SR.VinAttributesSegmentTitle;

                RenderVinAttributes(writer, title, id, Catalog.Segments);
            }
        }

        private void RenderPassengarDoorsAttributes(HtmlTextWriter writer)
        {
            const string id = SR.VinAttributePassengerDoorsID;

            const string title = SR.VinAttributePassengerDoorsTitle;

            RenderVinAttributes(writer, title, id, Catalog.PassengerDoors);
        }

        private void RenderFuelTypeAttributes(HtmlTextWriter writer)
        {
            const string id = SR.VinAttributeFuelTypeID;

            const string title = SR.VinAttributeFuelTypeTitle;

            RenderVinAttributes(writer, title, id, Catalog.FuelTypes);
        }

        private void RenderDriveTrainAttributes(HtmlTextWriter writer)
        {
            const string id = SR.VinAttributesDriveTrainID;

            const string title = SR.VinAttributesDriveTrainTitle;

            RenderVinAttributes(writer, title, id, Catalog.DriveTrains);
        }

        private void RenderTranmissionAttributes(HtmlTextWriter writer)
        {
            const string id = SR.VinAttributesTranmissionID;

            const string title = SR.VinAttributesTranmissionTitle;

            RenderVinAttributes(writer, title, id, Catalog.Transmissions);
        }

        private void RenderEngineAttributes(HtmlTextWriter writer)
        {
            const string id = SR.VinAttributesEngineID;

            const string title = SR.VinAttributesEngineTitle;

            RenderVinAttributes(writer, title, id, Catalog.Engines);
        }

        private void RenderSeriesAttributes(HtmlTextWriter writer)
        {
            const string id = SR.VinAttributesSeriesID;

            const string title = SR.VinAttributesSeriesTitle;

            RenderVinAttributes(writer, title, id, Catalog.Series);
        }

        private void RenderVinAttributes<TKey>(HtmlTextWriter writer, string title, string id, IEnumerable<TKey> attributes) where TKey : INameable<TKey>
        {
            bool hasFlyout = id.Equals(DisplayFlyoutVinAttribute);

            List<INameable> included = Facet.IncludedFacetNode[typeof(TKey)];

            List<INameable> excluded = Facet.ExcludedFacetNode[typeof(TKey)];

            bool hasFlyOutButton = !HasSingleValue(included, excluded);

            string cssClass = hasFlyout
                                   ? SR.VinAttributeCssClass + " " + SR.VinAttributeOpenCssClass
                                   : SR.VinAttributeCssClass;

            if (hasFlyOutButton)
            {
                cssClass += " has_flyout";
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Dl);

            writer.Indent++;

            writer.RenderBeginTag(HtmlTextWriterTag.Dt);

            writer.Write(title);

            writer.RenderEndTag(); // Dt

            writer.WriteLineNoTabs(string.Empty);

            writer.RenderBeginTag(HtmlTextWriterTag.Dd);

            if (included.Count == 0 && excluded.Count == 0)
            {
                throw new ArgumentException("Must Have At Least One Vin Attribute");
            }

            if (Facet.HasSingleValue(typeof(TKey)))
            {
                writer.Write(GetNameForNameable(included[0], title));
            }
            else if (Facet.IsEverythingSelected(typeof(TKey), HasNullAttribute.Contains(typeof(TKey))))
            {
                writer.Write(SR.VinAttrivuteAllSelectedPrefix + title);
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                writer.Indent++;

                List<string> renderedName = new List<string>();

                ArrayList.Adapter(included).Sort(new NameableComparer());

                foreach (INameable value in included)
                {
                    string name = GetNameForNameable(value, title);

                    if (!renderedName.Contains(name))
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Li);

                        writer.Write(name);

                        writer.RenderEndTag(); // Li 
                    }

                    renderedName.Add(name);
                }                

                writer.Indent--;

                writer.RenderEndTag(); // Ul

                writer.WriteLineNoTabs(string.Empty);
            }

            if (hasFlyout)
            {
                RenderVinFlyOutButton(writer, title, id);

                RenderFlyOut(writer, title, id, attributes);
            }
            else if (hasFlyOutButton)
            {
                RenderVinFlyOutButton(writer, title, id);
            }

            writer.RenderEndTag(); // Dd

            writer.Indent--;

            writer.RenderEndTag(); // Dl

            writer.WriteLineNoTabs(string.Empty);
        }

        private static string GetNameForNameable(INameable nameable, string title)
        {
            return nameable == null ? "Other " + title : string.IsNullOrEmpty(nameable.Name) ? "Base " + title : nameable.Name;
        }

        private static bool HasSingleValue(IList<INameable> a, IList<INameable> b)
        {
            return (a.Count == 1 && b.Count == 0) ||
                   (b.Count == 1 && a.Count == 0) ||
                   (a.Count == 1 && b.Count == 1 && a[0].Equals(b[0]));
        }
        
        private void RenderFlyOut<TKey>(HtmlTextWriter writer, string title, string id, IEnumerable<TKey> attributes) where TKey : INameable<TKey>
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.VinFlyOutCssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, CreateId(SR.VinFlyOutID + IdSeparator + id));

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.VinFlyOutContentCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            RenderAnyCheckbox<TKey>(writer, title, id + IdSeparator + "a");

            writer.RenderEndTag(); // Li

            int idx = 0;

            /// Because Trim May have repeating values (one for each Model Family) we only show attributes with a unique name, 
            /// and ensure that if one attribute is checked they check box is checked
            Dictionary<string, List<INameable<TKey>>> attributesToRender = new Dictionary<string, List<INameable<TKey>>>();

            foreach (INameable<TKey> attribute in attributes)
            {
                if (!attributesToRender.ContainsKey(attribute.Name))
                {
                    List<INameable<TKey>> list = new List<INameable<TKey>>();
                    list.Add(attribute);
                    attributesToRender.Add(attribute.Name, list);
                }
                else
                {
                    attributesToRender[attribute.Name].Add(attribute);
                }
            }

            foreach (KeyValuePair<string, List<INameable<TKey>>> toRender in attributesToRender)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                RenderCheckbox(writer, id + IdSeparator + idx, title, toRender.Value.ToArray());

                idx += toRender.Value.Count;

                writer.RenderEndTag(); // Li
            }

            if (HasNullAttribute.Contains(typeof(TKey)))
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                RenderNullCheckbox<TKey>(writer, title, id + IdSeparator + "n");

                writer.RenderEndTag(); // Li
            }
            
            writer.RenderEndTag(); // Ul

            RenderVinFlyOutPreviewButton(writer);

            RenderVinFlyOutCancleButton(writer);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.VinFlyOutBorderBreakCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderEndTag();

            writer.RenderEndTag(); // Div

            writer.RenderEndTag(); // Div
        }

        private void RenderCheckbox<TKey>(HtmlTextWriter writer, string idSuffix, string title, params INameable<TKey>[] attribute) where TKey : INameable<TKey>
        {
            string id = CreateId(SR.VinFlyOutCheckboxNamePrefix + IdSeparator + idSuffix);            

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");

            bool isChecked = false;

            foreach (INameable<TKey> nameable in attribute)
            {
                if (Facet.IsSelected(nameable))
                {
                    isChecked = true;
                }    
            }            

            if (isChecked)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked");
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Value, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            if (typeof(TKey) == typeof(PassengerDoor))
            {
                writer.Write(attribute[0].Name + " Doors");
            }
            else
            {
                if (string.IsNullOrEmpty(attribute[0].Name))
                {
                    writer.Write("Base " + title);
                }
                else
                {
                    writer.Write(attribute[0].Name);
                }
            }

            writer.RenderEndTag(); // Label
        }

        private void RenderNullCheckbox<TKey>(HtmlTextWriter writer, string title, string idSuffix) where TKey : INameable<TKey>
        {
            string id = CreateId(SR.VinFlyOutCheckboxNamePrefix + IdSeparator + idSuffix);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");

            if (Facet.IncludedFacetNode[typeof(TKey)].Contains(null))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked");
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Value, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write("Other " + title);

            writer.RenderEndTag(); // Label
        }

        private void RenderAnyCheckbox<TKey>(HtmlTextWriter writer, string title, string idSuffix) where TKey : INameable<TKey>
        {
            string id = CreateId(SR.VinFlyOutCheckboxNamePrefix + IdSeparator + idSuffix);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");

            writer.AddAttribute(HtmlTextWriterAttribute.Value, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.VinFlyOutSelectAllTitlePrefix + title);

            writer.RenderEndTag(); // Label
        }

        private void RenderVinFlyOutButton(HtmlTextWriter writer, string title, string idSuffix)
        {
            string id = CreateId(SR.VinFlyOutButtonNamePrefix + IdSeparator + idSuffix);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, SR.VinFlyOutButtonType);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.VinFlyOutButtonCssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Value, SR.VinFlyOutButtonValuePrefix + title);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input
        }

        private void RenderVinFlyOutPreviewButton(HtmlTextWriter writer)
        {
            RenderButton(writer, SR.VinFlyOutPreviewButtonName, SR.VinFlyOutPreviewButtonCssClass,
                         FlyoutPreviewButtonText, SR.VinFlyOutPreviewButtonType);
        }

        private void RenderVinFlyOutCancleButton(HtmlTextWriter writer)
        {
            RenderButton(writer, SR.VinFlyOutCancelButtonName, SR.VinFlyOutCancelButtonCssClass,
                         SR.VinFlyOutCancelButtonValue, SR.VinFlyOutCancelButtonType);
        }

        private void RenderButton(HtmlTextWriter writer, string name, string cssClass, string value, string type)
        {
            string id = CreateId(name);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ButtonWrapCssClass + " " + cssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, type);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Value, value);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.RenderEndTag(); // Span
        }

        private string CreateId(string s)
        {
            return UniqueID + IdSeparator + s;
        }

        #endregion

        #endregion

        #region DataBinding

        protected override void PerformDataBinding(IEnumerable data)
        {
            if (data != null)
            {
                IEnumerator en = data.GetEnumerator();

                if (en.MoveNext())
                {
                    Catalog = (Catalog)en.Current;

                    Catalog.Series.Sort();
                    Catalog.Engines.Sort();
                    Catalog.Transmissions.Sort();
                    Catalog.DriveTrains.Sort();
                    Catalog.FuelTypes.Sort();
                    Catalog.PassengerDoors.Sort();
                    Catalog.Segments.Sort();
                    Catalog.ModelFamilies.Sort();

                    CollectNullAttributes();
                    
                    Selection.Clear();

                    Facet.Initialize(Catalog.ModelConfigurations);

                    Facet.DataBind(FacetDataBoundViewState);                    

                    Facet.DataBind(FacetNodeDataBound);

                    foreach (ModelConfiguration configuration in ModelConfigurations)
                    {
                        if (configuration.Model.ModelFamily == null)
                        {
                            AddAttributeDelta(typeof(ModelFamily), null);
                        }
                        if (configuration.Model.Segment == null)
                        {
                            AddAttributeDelta(typeof(Segment), null);
                        }
                        if (configuration.Configuration.PassengerDoor == null)
                        {
                            AddAttributeDelta(typeof(PassengerDoor), null);
                        }
                        if (configuration.Configuration.FuelType == null)
                        {
                            AddAttributeDelta(typeof(FuelType), null);
                        }
                        if (configuration.Configuration.Transmission == null)
                        {
                            AddAttributeDelta(typeof(Transmission), null);
                        }
                        if (configuration.Configuration.Engine == null)
                        {
                            AddAttributeDelta(typeof(Engine), null);
                        }
                        if (configuration.Model.Series == null)
                        {
                            AddAttributeDelta(typeof(Series), null);
                        }
                    }

                    ManageFacetDelta();
                }
            }
        }

        private void CollectNullAttributes()
        {
            bool nullModelFamily = false;
            bool nullSegment = false;
            bool nullPassengerDoors = false;
            bool nullFuelType = false;
            bool nullTransmission = false;
            bool nullEngine = false;
            bool nullSeries = false;

            foreach (ModelConfiguration configuration in Catalog.ModelConfigurations)
            {
                nullModelFamily |= configuration.Model.ModelFamily == null;
                nullSegment |= configuration.Model.Segment == null;
                nullPassengerDoors |= configuration.Configuration.PassengerDoor == null;
                nullFuelType |= configuration.Configuration.FuelType == null;
                nullTransmission |= configuration.Configuration.Transmission == null;
                nullEngine |= configuration.Configuration.Engine == null;
                nullSeries |= configuration.Model.Series == null;
            }

            if (nullModelFamily)
                HasNullAttribute.Add(typeof (ModelFamily));
            if (nullSegment)
                HasNullAttribute.Add(typeof (Segment));
            if (nullPassengerDoors)
                HasNullAttribute.Add(typeof (PassengerDoor));
            if (nullFuelType)
                HasNullAttribute.Add(typeof (FuelType));
            if (nullTransmission)
                HasNullAttribute.Add(typeof (Transmission));
            if (nullEngine)
                HasNullAttribute.Add(typeof (Engine));
            if (nullSeries)
                HasNullAttribute.Add(typeof (Series));
        }

        private void FacetNodeDataBound(object sender, FacetDataBoundEventArgs args)
        {
            CatalogDataBoundEventArgs alt = new CatalogDataBoundEventArgs(args.ModelConfigurationId);

            OnCatalogDataBound(alt);

            if (alt.Selected)
            {
                Selection[args.ModelConfigurationId] = true;

                args.Selected = true;
            }

            if (alt.Target)
            {
                ModelConfigurationId = args.ModelConfigurationId;
            }
        }

        private void FacetDataBoundViewState(object sender, FacetDataBoundEventArgs args)
        {
            if (Selection.ContainsKey(args.ModelConfigurationId))
            {
                args.Selected = Selection[args.ModelConfigurationId];
            }
        }

        #endregion

        #region IStateManager

        private readonly StateBag _catalogBag = new StateBag();

        protected override void TrackViewState()
        {
            base.TrackViewState();

            ((IStateManager)_catalogBag).TrackViewState();
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                Triplet state = savedState as Triplet;

                if (state != null)
                {
                    base.LoadViewState(state.First);

                    object[] propertyState = (object[])state.Second;

                    int idx = 0;

                    _modelConfigurationId = (int?)propertyState[idx++];
                    _vinPattern = (string)propertyState[idx++];
                    _selection = (Dictionary<int, bool>)propertyState[idx++];
                    _displayFlyoutVinAttribute = (string)propertyState[idx++];
                    _attributeDelta = (Dictionary<SelectionType, IList<int?>>)propertyState[idx++];
                    _hasNullAttribute = (List<Type>) propertyState[idx];

                    ((IStateManager)_catalogBag).TrackViewState();
                    ((IStateManager)_catalogBag).LoadViewState(state.Third);
                    _catalog = Catalog.GetCatalog(new StateBagSerializationData(_catalogBag));
                }
                else
                {
                    base.LoadViewState(savedState);
                }
            }
        }

        protected override object SaveViewState()
        {
            _catalog.GetObjectData(new StateBagSerializationData(_catalogBag));

            object catalogState = ((IStateManager)_catalogBag).SaveViewState();

            object[] propertyState = new object[6];

            int idx = 0;

            propertyState[idx++] = _modelConfigurationId;
            propertyState[idx++] = _vinPattern;
            propertyState[idx++] = _selection;
            propertyState[idx++] = _displayFlyoutVinAttribute;
            propertyState[idx++] = _attributeDelta;
            propertyState[idx] = _hasNullAttribute;

            return new Triplet(base.SaveViewState(), propertyState, catalogState);
        }

        #endregion

        #region IPostBackDataHandler

        private string _commandName;
        private readonly List<string> _setAttributes = new List<string>();

        protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            if (Catalog != null)
            {
                int idOffset = UniqueID.Length + 1;

                if (postDataKey.StartsWith(UniqueID, StringComparison.InvariantCulture) && !postDataKey.Equals(UniqueID))
                {
                    string controlKey = postDataKey.Substring(idOffset);

                    if (string.Equals(controlKey, SR.SaveButtonName) ||
                        string.Equals(controlKey, SR.RestoreButtonName) ||
                        string.Equals(controlKey, SR.ResetButtonName) ||
                        string.Equals(controlKey, SR.VinFlyOutPreviewButtonName) ||
                        string.Equals(controlKey, SR.VinFlyOutCancelButtonName))
                    {
                        _commandName = controlKey;

                        return true;
                    }

                    foreach (KeyValuePair<string, Type> map in VinAttributeMap)
                    {
                        string vinFlyoutKey = SR.VinFlyOutButtonNamePrefix + IdSeparator + map.Key;

                        if (vinFlyoutKey.Equals(controlKey))
                        {
                            string vinFlyoutValue = controlKey.Substring(SR.VinFlyOutButtonNamePrefix.Length + 1);

                            DisplayFlyoutVinAttribute = (vinFlyoutValue.Equals(DisplayFlyoutVinAttribute)) ? null : vinFlyoutValue;

                            return true;
                        }
                    }

                    string attributePrefix = SR.VinFlyOutCheckboxNamePrefix + IdSeparator;

                    if (controlKey.StartsWith(attributePrefix))
                    {
                        _setAttributes.Add(controlKey.Substring(attributePrefix.Length));

                        return true;
                    }
                }
            }

            return false;
        }

        protected void RaisePostDataChangedEvent()
        {
            HandleEvents(_commandName);

            _commandName = null;
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }

        #endregion

        #region IScriptControl

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor catalogFacetDescriptor =
                new ScriptBehaviorDescriptor(
                    "FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView", ClientID);

            catalogFacetDescriptor.AddElementProperty("open_vin_fly_out", CreateId(SR.VinFlyOutID + IdSeparator + DisplayFlyoutVinAttribute));

            catalogFacetDescriptor.AddProperty("vin_fly_out_button_css_class", SR.VinFlyOutButtonCssClass);
            catalogFacetDescriptor.AddProperty("vin_attribute_css_class", SR.VinAttributeCssClass);
            catalogFacetDescriptor.AddProperty("vin_fly_out_border_break_css_class", SR.VinFlyOutBorderBreakCssClass);
            catalogFacetDescriptor.AddProperty("vin_fly_out_css_class", SR.VinFlyOutCssClass);
            catalogFacetDescriptor.AddProperty("vin_attribute_open_css_class", SR.VinAttributeOpenCssClass);
            catalogFacetDescriptor.AddProperty("vin_attribute_hover_css_class", SR.VinAttributeHoverCssClass);
            catalogFacetDescriptor.AddProperty("vin_fly_out_preview_button_css_class", SR.VinFlyOutPreviewButtonCssClass);

            return new ScriptDescriptor[] { catalogFacetDescriptor };
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new[]
                       {
                           new ScriptReference(
                               "FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView.js",
                               "FirstLook.DomainModel.Lexicon")
                       };
        }

        #endregion

        #region InnerClasses

        private static class SR
        {
            public const string ButtonWrapCssClass = "button";

            #region RestoreButton
            public const string RestoreButtonName = "restore_precision_defaults";

            public const string RestoreButtonCssClass = "restore_precision_defaults";

            public const string RestoreButtonValue = "Restore Precision Defaults";

            public const string RestoreButtonType = "submit";
            #endregion

            #region ResetButton
            public const string ResetButtonName = "reset_precision_changes";

            public const string ResetButtonCssClass = "reset_precision_changes";

            public const string ResetButtonValue = "cancel and return";

            public const string ResetButtonType = "submit";
            #endregion

            #region SaveButton
            public const string SaveButtonName = "save_precision_changes";

            public const string SaveButtonCssClass = "save_precision_changes";

            public const string SaveButtonValue = "Save Precision Search";

            public const string SaveButtonType = "submit";
            #endregion

            #region VinAttributes

            public const string VinAttributeCssClass = "search_results";

            public const string VinAttributeOpenCssClass = "highlight";

            public const string VinAttributeHoverCssClass = "hover";

            public const string VinAttributeModelFamilyID = "MF";

            public const string VinAttributesSegmentID = "S";

            public const string VinAttributeBodyTypeID = "B";

            public const string VinAttributePassengerDoorsID = "D";

            public const string VinAttributeFuelTypeID = "F";

            public const string VinAttributesDriveTrainID = "DT";

            public const string VinAttributesTranmissionID = "T";

            public const string VinAttributesEngineID = "E";

            public const string VinAttributesSeriesID = "SR";

            public const string VinAttributesModleFamilyTitle = "Model Family";

            public const string VinAttributesSegmentTitle = "Segment";

            public const string VinAttributesBodyTypeTitle = "Body Type";

            public const string VinAttributePassengerDoorsTitle = "Doors";

            public const string VinAttributeFuelTypeTitle = "Fuel Type";

            public const string VinAttributesDriveTrainTitle = "Drive Train";

            public const string VinAttributesTranmissionTitle = "Transmission";

            public const string VinAttributesEngineTitle = "Engine";

            public const string VinAttributesSeriesTitle = "Trim";

            public const string VinAttrivuteAllSelectedPrefix = "Any ";

            #endregion

            #region VinFlyOutButton
            public const string VinFlyOutButtonNamePrefix = "open_vin_attribute";

            public const string VinFlyOutButtonCssClass = "open_vin_attribute";

            public const string VinFlyOutButtonValuePrefix = "Open ";

            public const string VinFlyOutButtonType = "submit";
            #endregion

            #region VinFlyOut

            public const string VinFlyOutCssClass = "vin_fly_out";

            public const string VinFlyOutID = "FlyOut";

            public const string VinFlyOutContentCssClass = "content";

            public const string VinFlyOutSelectAllTitlePrefix = "Any ";

            public const string VinFlyOutCheckboxNamePrefix = "select";

            public const string VinFlyOutPreviewButtonName = "preview_market_listings";

            public const string VinFlyOutPreviewButtonCssClass = "preview_market_listings";

            public const string VinFlyOutPreviewButtonValue = "Preview Market Listings";

            public const string VinFlyOutPreviewButtonType = "submit";

            public const string VinFlyOutCancelButtonName = "cancel_market_listings";

            public const string VinFlyOutCancelButtonCssClass = "cancel_market_listings";

            public const string VinFlyOutCancelButtonValue = "cancel";

            public const string VinFlyOutCancelButtonType = "submit";

            public const string VinFlyOutBorderBreakCssClass = "fly_out_border_break";

            #endregion
        }

        #endregion
    }
}
