using System.Collections;

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
	internal class StateBagSerializationData : SerializationData
	{
		public StateBagSerializationData(IDictionary dictionary) : base(dictionary)
		{
		}

		protected override object MakeKey(string name)
		{
			return name; // was hoping to use IndexedString :(
		}

		protected override IDictionary MakeValue(IExplicitlySerializable value)
		{
			IDictionary table = new Hashtable();

            if (value != null)
			    value.GetObjectData(NewSerializationData(table));

			return table;
		}

		protected override SerializationData NewSerializationData(IDictionary value)
		{
			return new StateBagSerializationData(value);
		}
	}
}
