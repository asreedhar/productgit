using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
	public class CatalogDataBoundEventArgs : EventArgs
	{
		private readonly int modelConfigurationId;
		private bool selected;
		private bool target;

		internal CatalogDataBoundEventArgs(int modelConfigurationId)
		{
			this.modelConfigurationId = modelConfigurationId;
		}

		public int ModelConfigurationId
		{
			get { return modelConfigurationId; }
		}

		public bool Selected
		{
			get { return selected; }
			set { selected = value; }
		}

		public bool Target
		{
			get { return target; }
			set { target = value; }
		}
	}
}
