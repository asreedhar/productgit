using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Lexicon.Categorization.Query;
using Table=FirstLook.DomainModel.Lexicon.Categorization.Query.Table;
using TableCell=FirstLook.DomainModel.Lexicon.Categorization.Query.TableCell;
using TableRow=FirstLook.DomainModel.Lexicon.Categorization.Query.TableRow;

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
    [ParseChildren(false),
     PersistChildren(false)]
	public class CatalogDataView : DataBoundControl, IPostBackDataHandler, IPostBackEventHandler
	{
		#region Events

		private static readonly object EventCatalogDataBound = new object();

		private static readonly object EventCatalogSelectionChanged = new object();

        private static readonly object EventCatalogSelectionUpdated = new object();

		public event EventHandler<CatalogDataBoundEventArgs> CatalogDataBound
		{
			add
			{
				Events.AddHandler(EventCatalogDataBound, value);
			}
			remove
			{
				Events.RemoveHandler(EventCatalogDataBound, value);
			}
		}

		protected virtual void OnCatalogDataBound(CatalogDataBoundEventArgs e)
		{
			EventHandler<CatalogDataBoundEventArgs> handler = (EventHandler<CatalogDataBoundEventArgs>)Events[EventCatalogDataBound];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<CatalogSelectionEventArgs> CatalogSelectionChanged
		{
			add
			{
				Events.AddHandler(EventCatalogSelectionChanged, value);
			}
			remove
			{
				Events.RemoveHandler(EventCatalogSelectionChanged, value);
			}
		}

		protected virtual void OnCatalogSelectionChanged(CatalogSelectionEventArgs e)
		{
			EventHandler<CatalogSelectionEventArgs> handler = (EventHandler<CatalogSelectionEventArgs>)Events[EventCatalogSelectionChanged];
			if (handler != null)
			{
				handler(this, e);
			}
		}

        public event EventHandler CatalogSelectionUpdated
        {
            add
            {
                Events.AddHandler(EventCatalogSelectionUpdated, value);
            }
            remove
            {
                Events.RemoveHandler(EventCatalogSelectionUpdated, value);
            }
        }

        protected virtual void OnCatalogSelectionUpdated(EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[EventCatalogSelectionUpdated];
            if (handler != null)
            {
                handler(this, e);
            }
        }

		#endregion

		#region ViewState

		private readonly StateBag _catalogBag = new StateBag();

		protected override void LoadViewState(object savedState)
		{
			if (savedState != null)
			{
				Pair state = savedState as Pair;

				if (state != null)
				{
					base.LoadViewState(state.First);
					((IStateManager)_catalogBag).TrackViewState();
					((IStateManager)_catalogBag).LoadViewState(state.Second);
					_catalog = Catalog.GetCatalog(new StateBagSerializationData(_catalogBag));
				}
				else
				{
					base.LoadViewState(savedState);
				}
			}
		}

		protected override object SaveViewState()
		{
			if (_catalog != null)
			{
				_catalog.GetObjectData(new StateBagSerializationData(_catalogBag));
				object obj0 = base.SaveViewState();
				object obj1 = ((IStateManager)_catalogBag).SaveViewState();
				return new Pair(obj0, obj1);
			}
			return base.SaveViewState();
		}

		protected override void TrackViewState()
		{
			base.TrackViewState();

			((IStateManager)_catalogBag).TrackViewState();
		}

		#endregion

		#region Data Binding

		protected override void PerformDataBinding(IEnumerable data)
		{
			if (data != null)
			{
				IEnumerator en = data.GetEnumerator();

				if (en.MoveNext())
				{
					Catalog = (Catalog) en.Current;

					Selection.Clear();

					Tables.DataBind(Table_DataBound);
				}
			}
		}

		private void Table_DataBound(object sender, TableDataBoundEventArgs args)
		{
			CatalogDataBoundEventArgs alt = new CatalogDataBoundEventArgs(args.ModelConfigurationId);

			OnCatalogDataBound(alt);

			if (alt.Selected)
			{
				Selection[args.ModelConfigurationId] = true;

				args.Selected = true;
			}
			
			if (alt.Target)
			{
				ModelConfigurationId = args.ModelConfigurationId;
			}
		}

		private void Table_DataBoundViewState(object sender, TableDataBoundEventArgs args)
		{
			if (Selection.ContainsKey(args.ModelConfigurationId))
			{
				args.Selected = Selection[args.ModelConfigurationId];
			}
		}

		private void Table_SelectionChanged(object sender, TableSelectionEventArgs<ModelConfiguration> args)
		{
			Selection[args.Value.Id] = args.Selected;

			OnCatalogSelectionChanged(new CatalogSelectionEventArgs(args.Value.Id, args.Selected));
		}

		#endregion

		#region Properties

		private Catalog _catalog;

		private TableCollection _tables;

		private Catalog Catalog
		{
			get
			{
				return _catalog;
			}
			set
			{
				if (_catalog != value)
				{
					_catalog = value;

					if (_tables != null)
					{
						_tables.SelectionChanged -= Table_SelectionChanged;
						_tables = null;
					}
				}
			}
		}

		private TableCollection Tables
		{
			get
			{
				if (_tables == null)
				{
					if (Catalog != null)
					{
						int? id = ModelConfigurationId.HasValue
						          	? ModelConfigurationId.Value
						          	: (CatalogEntryId.HasValue
						          	   	? FindModelConfigurationId(CatalogEntryId.Value)
						          	   	: null);
						if (!id.HasValue)
						{
							throw new ArgumentException("Neither ModelConfigurationId or CatalogEntryId available");
						}
						else
						{
							_tables = TableCollection.GetTables(Catalog, Catalog.VinPatterns.GetItem(VinPattern), Catalog.ModelConfigurations.GetItem(id.Value));
							_tables.DataBind(Table_DataBoundViewState);
							_tables.SelectionChanged += Table_SelectionChanged;
						}
					}
				}

				return _tables;
			}
		}

        public bool ShowUpdatedButton
        {
            get
            {
                object value = ViewState["ShowUpdatedButton"];
                if (value == null)
                    return false;
                return (bool) value;
            }
            set
            {
                if (value != ShowUpdatedButton)
                    ViewState["ShowUpdatedButton"] = value;
            }
        }
		
        private int? ModelConfigurationId
		{
			get
			{
				return (int?) ViewState["ModelConfigurationId"];
			}
			set
			{
				ViewState["ModelConfigurationId"] = value;
			}
		}

		private Dictionary<int, bool> Selection
		{
			get
			{
				Dictionary<int, bool> values = ViewState["Selection"] as Dictionary<int, bool>;
				if (values == null)
				{
					values = new Dictionary<int, bool>();
					ViewState["Selection"] = values;
				}
				return values;
			}
		}

		public string VinPattern
		{
			get
			{
				return ((ViewState["VinPattern"] as string) ?? string.Empty);
			}
			set
			{
				ViewState["VinPattern"] = value;
			}
		}

		public int? CatalogEntryId
		{
			get
			{
				return (int?) ViewState["CatalogEntryId"];
			}
			set
			{
				ViewState["CatalogEntryId"] = value;
			}
		}

        private static int? CadidateClosureModelId;

        private int DisplayTableId
        {
            get
            {
            	object value = ViewState["DisplayTableId"];
				if (value == null)
					return 1;
            	return (int) value;
            }
        	set
            {
				if (value != DisplayTableId)
				{
					ViewState["DisplayTableId"] = value;
				}
            }
        }

		#endregion

		#region Helpers

		public int? FindModelConfigurationId(int catalogEntryId)
		{
			ModelConfiguration modelConfiguration = Catalog.FindModelConfiguration(catalogEntryId);
			if (modelConfiguration == null)
				return null;
			return modelConfiguration.Id;
		}
		
		public IEnumerable<int> FindCatalogEntryId(int modelConfigurationId)
		{
			return Catalog.FindCatalogEntryId(modelConfigurationId);
		}

		public void GetSelections(EventHandler<CatalogSelectionEventArgs> handler)
		{
			if (handler != null)
			{
				Tables.GetSelections(
					delegate(object sender, TableSelectionEventArgs<ModelConfiguration> args)
					{
						handler(sender, new CatalogSelectionEventArgs(args.Value.Id, args.Selected));
					});
			}
		}

		private static string FormatTableTitle(TableName name)
        {
		    string text = "unknown";

		    bool useSegment = (name.Segment == null)
				? false
				: (!name.Segment.IsTruck && !name.Segment.IsVan);

            if (useSegment)
            {
                text = string.Format("{0} {1}", name.ModelFamily.Name, name.Segment.Name);
            }
            else
            {
                if (name.ModelFamily != null)
                {
                    if ((name.BodyType == null) ? false : !name.ModelFamily.Name.EndsWith(name.BodyType.Name)) // stops certain truck from having double cab types, PM
                    {
                        text = string.Format("{0} {1}", name.ModelFamily.Name, name.BodyType.Name);
                    }
                    else
                    {
                        text = name.ModelFamily.Name;
                    } 
                }
            }

            return text;
        }

		#endregion

		#region Web Control

		#region Web Control Properties

        private static readonly string[] ColumnCssClass = new string[] { "col_trims", "col_engines", "col_tranmissions", "col_drive_types", "col_fuel_types", "col_doors" };

        private const string SearchSummarySectionId = "search_summary";
        private const string CandidateClosureSectionId = "my_equipment";
        private const string AdditionalEquipmentSectionId = "additional_equipment";

        private const string EquipmentTableCssClass = "equipment";
        
        private const string SelectAllSegmentCssClass = "select_all";
        private const string TableListCssClass = "segments";
        private const string CurrentTableListItemCssClass = "current_segment";
        private const string CandidateClosureSummaryCssClass = "summary";
        private const string SelfClearingCssClass = "clearfix";

        private const HtmlTextWriterTag SectionTitleTag = HtmlTextWriterTag.H3;
        private const string CandidateClosureSectionTitleText = "My Matched Equipment";
        private const string SearchSummarySectionTitleText = "Additional Search Summary";
        private const string AdditionalEquipmentSectionTitleText = "Additional Search Equipment";
        private const string IncludisionSummarySuffix = " with";
        private const string ExcludisionSummarySuffix = " without";

        private const string NotSearchedText = "Precision Search Not Modified";

        private const string InputHtmlType = "submit";
        private const string OnStateCssClass = "all_selected";
        private const string OffStateCssClass = "none_selected";
        private const string ImpliedStateCssClass = "some_selected";
        private const string InactiveOnStateCssClass = "selected_by_default";

        private const string SaveButtonText = "Update Search";
        private const string SaveButtonID = "_save";
        private const string SaveButtonCssClass = "save_search";

        private const string ChangeSegmentEventPrefix = "c";
        private const string SelectTableEventPrefix = "s";
        private const string DeSelectTableEventPrefix = "d";

        #endregion

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		protected override string TagName
		{
			get
			{
				return "div";
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			Page.RegisterRequiresPostBack(this);
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (Catalog != null)
			{
				RenderCatalogControls(writer, Tables);
			}
		}

		private void RenderCatalogControls(HtmlTextWriter writer, List<Table> tables)
		{
			writer.Indent++;

		    CadidateClosureModelId = tables[0].Name.ModelFamily.Id;

			RenderCanidateClosureSection(writer, tables[0]);

			List<Table> additional = tables.GetRange(1, tables.Count - 1);

            if (additional.Count > 0)
            {
                RenderSearchSummarySection(writer, additional);

                AdditionalEquipmentSection(writer, additional); 
            }

			writer.Indent--;
		}

        private void RenderCanidateClosureSection(HtmlTextWriter writer, Table table)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, CandidateClosureSectionId);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SelfClearingCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.Indent++;

            writer.AddAttribute(HtmlTextWriterAttribute.Class, CandidateClosureSummaryCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.Indent++;

            writer.RenderBeginTag(SectionTitleTag);

            writer.Write(CandidateClosureSectionTitleText);

            writer.RenderEndTag(); // SectionTitleTag

            TableSelectionSummary summary = table.GetSelectionSummary();

            if (summary.HasSummary)
            {
                RenderSummaryList(writer, table);   
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.P);

                string innerText = FormatTableTitle(table.Name);

                writer.Write(innerText);

                writer.RenderEndTag(); // P
            }

            writer.Indent--;

            writer.RenderEndTag(); // Div => CandidateClosureSummary

            writer.Indent--;

            RenderEquipmentTable(writer, table);

            writer.RenderEndTag(); // Div => CandidateClosureSection

            writer.WriteLine();
        }

        private void RenderSearchSummarySection(HtmlTextWriter writer, IList<Table> tables)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, SearchSummarySectionId);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.Indent++;

            writer.RenderBeginTag(SectionTitleTag);

            writer.Write(SearchSummarySectionTitleText);

            writer.RenderEndTag(); // SectionTitleTag

            RenderSummaryLists(writer, tables);

            writer.Indent--;

            writer.RenderEndTag(); // Div => SearchSummarySectionId
        }
        
        private void AdditionalEquipmentSection(HtmlTextWriter writer, IList<Table> tables)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, AdditionalEquipmentSectionId);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.Indent++;

            writer.RenderBeginTag(SectionTitleTag);

            writer.Write(AdditionalEquipmentSectionTitleText);

            writer.RenderEndTag(); // SectionTitleTag

            writer.WriteLine();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, TableListCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Ol);

            writer.Indent++;

            for (int i = 0, l = tables.Count; i < l; i++)
            {
                if (DisplayTableId == tables[i].Id)
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, CurrentTableListItemCssClass);
                
                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                writer.Indent++;

                RenderHeader(writer, tables[i]);

                TableSelectionSummary summary = tables[i].GetSelectionSummary();

                if (summary.HasAll)
                    RenderDeselectAllAnchor(writer, tables[i]);
                else
                    RenderSelectAllAnchor(writer, tables[i]);

                if (DisplayTableId == tables[i].Id)
                    RenderEquipmentTable(writer, tables[i]);

                writer.Indent--;

                writer.RenderEndTag();
            }

            writer.Indent--;

            writer.RenderEndTag(); // OL => TableListCssClass

            writer.Indent--;

            writer.RenderEndTag(); // Div => AdditionalEquipmentSectionId
        }

        private void RenderHeader(HtmlTextWriter writer, Table table)
        {
            PostBackOptions options = new PostBackOptions(this, string.Format("{0}{1}", ChangeSegmentEventPrefix,CreateId(table)));

            if (Page.Form != null)
                options.AutoPostBack = true;

            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, Page.ClientScript.GetPostBackEventReference(options, true));

            writer.RenderBeginTag(HtmlTextWriterTag.H4);

            string headerText;

            if (table.Equals(Tables[1]) && table.Name.ModelFamily.Id == CadidateClosureModelId)
            {
                headerText = string.Format("Include Other {0}", FormatTableTitle(table.Name));
            }
            else
            {
                headerText = string.Format("Include {0}", FormatTableTitle(table.Name));
            }

            writer.Write(headerText);

            writer.RenderEndTag();

            writer.WriteLine();
        }

        private void RenderSummaryLists(HtmlTextWriter writer, IList<Table> tables)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, TableListCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Ol);

            writer.Indent++;

            writer.WriteLine();

            bool hasSummary = false;

            for (int i = 0, l = tables.Count; i < l; i++)
            {
                TableSelectionSummary summary = tables[i].GetSelectionSummary();

                if (summary.HasSummary)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);

                    writer.Indent++;

                    RenderSummaryList(writer, tables[i]);

                    writer.Indent--;

                    writer.RenderEndTag(); // Li

                    writer.WriteLine();

                    hasSummary = true;
                }
            }

            if (!hasSummary)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                writer.RenderBeginTag(HtmlTextWriterTag.H4);

                writer.Write(NotSearchedText);

                writer.RenderEndTag(); // H4

                writer.RenderEndTag(); // Li
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                RenderSaveButton(writer);

                writer.RenderEndTag();
            }

            writer.Indent--;

            writer.RenderEndTag(); // OL

            writer.WriteLine();
        }
        
        private static void RenderSummaryList(HtmlTextWriter writer, Table table)
		{
			int row = 0;

        	TableSelectionSummary summary = table.GetSelectionSummary();

            string innerText = FormatTableTitle(table.Name);

            if (innerText.EndsWith("s"))
            {
                innerText += "es";
            }
            else
            {
                innerText += "s";
            }

            if (summary.HasAll)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H6);

                writer.Write(string.Format("All {0}", innerText));

                writer.RenderEndTag();
            }
            else
            {
                writer.RenderBeginTag(HtmlTextWriterTag.H6);

                writer.Write(innerText);

                if (table.TableType == TableType.CandidateClosure)
                {
                    writer.Write(ExcludisionSummarySuffix);
                }
                else
                {
                    writer.Write(IncludisionSummarySuffix);
                }

                writer.RenderEndTag();

                foreach (IList<INameable> list in summary)
                {
                    row++;

                    if (list.Count == 0)
                    {
                        continue;
                    }

                    writer.RenderBeginTag(HtmlTextWriterTag.Dl);

                    writer.Indent++;

                    writer.WriteLine();

                    writer.RenderBeginTag(HtmlTextWriterTag.Dt);

                    writer.Write(table.Columns[row - 1].Name + ":");

                    writer.RenderEndTag(); // Dt

                    writer.RenderBeginTag(HtmlTextWriterTag.Dd);

                    for (int i = 0, l = list.Count; i < l; i++)
                    {
                        if (i > 0)
                        {
                            writer.Write(", ");
                        }

                        writer.Write(list[i].Name);
                    }

                    writer.RenderEndTag(); // Dd

                    writer.Indent--;

                    writer.RenderEndTag(); // Dl
                } 
            }			
		}

		private void RenderEquipmentTable(HtmlTextWriter writer, Table table)
		{
			string equipmentCssClass = EquipmentTableCssClass;

			writer.AddAttribute(HtmlTextWriterAttribute.Class, equipmentCssClass);

			writer.RenderBeginTag(HtmlTextWriterTag.Table);
			writer.Indent++;
			writer.WriteLine();

			writer.RenderBeginTag(HtmlTextWriterTag.Thead);
			writer.Indent++;
			writer.WriteLine();

			writer.RenderBeginTag(HtmlTextWriterTag.Tr);
			writer.Indent++;
			writer.WriteLine();

            for (int i = 0, l = table.Columns.Length; i < l; i++)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, ColumnCssClass[i]);
                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write(table.Columns[i].Name);
                writer.RenderEndTag();
                writer.WriteLine();
            }

			writer.RenderEndTag();
			writer.Indent--;
			writer.WriteLine();

			writer.RenderEndTag();
			writer.Indent--;
			writer.WriteLine();

			writer.RenderBeginTag(HtmlTextWriterTag.Tbody);
			writer.Indent++;
			writer.WriteLine();

			for (int rowIndex = 0, rowCount = table.Rows.Count; rowIndex < rowCount; rowIndex++)
			{
				writer.RenderBeginTag(HtmlTextWriterTag.Tr);
				writer.Indent++;
				writer.WriteLine();

				for (int colIndex = 0, colCount = TableRow.ColumnCount; colIndex < colCount; colIndex++)
				{
					TableCell cell = table.Rows[rowIndex][colIndex];

					if (cell != null)
					{
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, ColumnCssClass[colIndex]);

                        writer.RenderBeginTag(HtmlTextWriterTag.Td);

						string id = CreateId(table, rowIndex, colIndex);

						bool enabled = (table.Columns[colIndex].Cells.Count > 1 || (table.Rows.Count == 1 && table.TableType != TableType.CandidateClosure));

						RenderInputTag(writer, id, cell.State, enabled);

						RenderLabel(writer, id, cell.Value.Name, cell.State, enabled);

                        if (enabled && table.TableType != TableType.CandidateClosure)
                        {
                            RenderCharacterization(writer, cell.Characterization);
                        }

					    writer.RenderEndTag();
						writer.WriteLine();
					}
                    else
					{
					    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.RenderEndTag();
					    writer.WriteLine();
					}
				}

				writer.RenderEndTag();
				writer.Indent--;
				writer.WriteLine();
			}
			
			writer.RenderEndTag();
			writer.Indent--;
			writer.WriteLine();

			writer.Indent--;
			writer.WriteLine();
			writer.RenderEndTag();
		}

    	private void RenderSelectAllAnchor(HtmlTextWriter writer, Table table)
    	{
            writer.AddAttribute(HtmlTextWriterAttribute.Id, CreateId(table, SelectTableEventPrefix));
            
            writer.AddAttribute(HtmlTextWriterAttribute.Name, CreateId(table, SelectTableEventPrefix));

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SelectAllSegmentCssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            string innerText = string.Format("select all {0}", FormatTableTitle(table.Name));

            writer.AddAttribute(HtmlTextWriterAttribute.Value, innerText);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();
    	}

        private void RenderDeselectAllAnchor(HtmlTextWriter writer, Table table)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, CreateId(table, DeSelectTableEventPrefix));

            writer.AddAttribute(HtmlTextWriterAttribute.Name, CreateId(table, DeSelectTableEventPrefix));

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SelectAllSegmentCssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            string innerText = string.Format("deselect all {0}", FormatTableTitle(table.Name));

            writer.AddAttribute(HtmlTextWriterAttribute.Value, innerText);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();
        }
        
        private string CreateId(Table table, int rowIndex, int colIndex)
    	{
    		return UniqueID + "_" + table.Id + "_" + rowIndex + "_" + colIndex;
    	}

        private string CreateId(Table table)
        {
            return UniqueID + "_" + table.Id;
        }

		private string CreateId(Table table, String EventSuffix)
		{
		    return UniqueID + "_" + table.Id + "_" + EventSuffix;
		}
        
        private static void RenderInputTag(HtmlTextWriter writer, string id, TableCellState state, bool enabled)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

			writer.AddAttribute(HtmlTextWriterAttribute.Type, InputHtmlType);

			writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            if (!enabled)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, InactiveOnStateCssClass);
                writer.AddAttribute(HtmlTextWriterAttribute.Disabled, "disabled");
            }
            else
            {
                if (state == TableCellState.Off)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Select All");
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, "Select All");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, OffStateCssClass);
                }
                else if (state == TableCellState.Implied)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Deselect All");
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, "Deselect All");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, ImpliedStateCssClass);
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Deselect All");
                    writer.AddAttribute(HtmlTextWriterAttribute.Value, "Deselect All");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, OnStateCssClass);
                }
            }

			writer.RenderBeginTag(HtmlTextWriterTag.Input);

			writer.RenderEndTag();
		}

		private static void RenderLabel(HtmlTextWriter writer, string id, string text, TableCellState state, bool enabled)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.For, id);            

            if (!enabled)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, InactiveOnStateCssClass);
            }
            else
            {
                if (state == TableCellState.Off)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, OffStateCssClass);
                }
                else if (state == TableCellState.Implied)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, ImpliedStateCssClass);
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, OnStateCssClass);
                }
            }

			writer.RenderBeginTag(HtmlTextWriterTag.Label);
			writer.Write(text);
			writer.RenderEndTag();
		}

        private static void RenderCharacterization(HtmlTextWriter writer, TableCellCharacterization characterization)
        {
            string CssClass = string.Empty;
            string InnerText = string.Empty;
            if ((characterization & TableCellCharacterization.Primary) == TableCellCharacterization.Primary &&
                (characterization & TableCellCharacterization.Secondary) == TableCellCharacterization.Secondary)
            {
                CssClass = "rate_3";
                InnerText = "***";
            }
            else if ((characterization & TableCellCharacterization.Primary) == TableCellCharacterization.Primary)
            {
                CssClass = "rate_2";
                InnerText = "**";
            }
            else if ((characterization & TableCellCharacterization.Secondary) == TableCellCharacterization.Secondary)
            {
                CssClass = "rate_1";
                InnerText = "*";
            }

            if (!string.IsNullOrEmpty(CssClass) && !string.IsNullOrEmpty(InnerText))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass);

                writer.RenderBeginTag(HtmlTextWriterTag.Span);

                writer.Write(InnerText);

                writer.RenderEndTag();
            }
        }

        private void RenderSaveButton(HtmlTextWriter writer)
        {
            if (ShowUpdatedButton)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, SaveButtonCssClass);

                writer.RenderBeginTag(HtmlTextWriterTag.Label);

                writer.AddAttribute(HtmlTextWriterAttribute.Id, UniqueID + SaveButtonID);

                writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID + SaveButtonID);

                writer.AddAttribute(HtmlTextWriterAttribute.Value, SaveButtonText);                

                writer.AddAttribute(HtmlTextWriterAttribute.Type, InputHtmlType);

                writer.RenderBeginTag(HtmlTextWriterTag.Input);

                writer.RenderEndTag(); 

                writer.RenderEndTag();
            }
        }

        #endregion

		#region IPostBackDataHandler Members

    	private TableCell _cell = null;
        private string _selectAllId;
        private string _deselectAllId;

		protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			if (Catalog != null)
			{
				TableCollection tables = Tables;

                for (int tableIndex = 0, tableCount = tables.Count; tableIndex < tableCount; tableIndex++)
                {
                    Table table = tables[tableIndex];

                    for (int rowIndex = 0, rowCount = table.Rows.Count; rowIndex < rowCount; rowIndex++)
                    {
                        for (int colIndex = 0, colCount = TableRow.ColumnCount; colIndex < colCount; colIndex++)
                        {
                            TableCell cell = table.Rows[rowIndex][colIndex];

                            if (cell != null)
                            {
                                string id = CreateId(table, rowIndex, colIndex);

                                string value = postCollection[id];

                                if (!string.IsNullOrEmpty(value))
                                {
                                    _cell = cell;

                                    return true;
                                }
                            }
                        }
                    }

                    // Test for Select All
                    string selectAllId = CreateId(table, SelectTableEventPrefix);

                    string selectAllValue = postCollection[selectAllId];

                    if (!string.IsNullOrEmpty(selectAllValue))
                    {
                        _selectAllId = selectAllId;

                        return true;
                    }

                    // Text for Deslect All
                    string deselectAllId = CreateId(table, DeSelectTableEventPrefix);

                    string deselectAllValue = postCollection[deselectAllId];

                    if (!string.IsNullOrEmpty(deselectAllValue))
                    {
                        _deselectAllId = deselectAllId;

                        return true;
                    }
                }

			    if (!string.IsNullOrEmpty(postCollection[UniqueID + SaveButtonID]))
                {
                    OnCatalogSelectionUpdated(new EventArgs());

                    return false;
                }
			}

			return false;
		}

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            TableCollection tables = Tables;

            for (int i = 0, l = tables.Count; i < l; i++)
            {
                string tableId = CreateId(tables[i]);

                if (string.Format("{0}{1}", ChangeSegmentEventPrefix, tableId) == eventArgument)
                    DisplayTableId = tables[i].Id;

                else if (string.Format("{0}{1}", SelectTableEventPrefix, tableId) == eventArgument)
                    tables[i].SetState(TableCellState.On);

                else if (string.Format("{0}{1}", DeSelectTableEventPrefix, tableId) == eventArgument)
                    tables[i].SetState(TableCellState.Off);
            }
        }

		protected void RaisePostDataChangedEvent()
		{
            if (_cell != null)
            {
                if (_cell.State == TableCellState.Off)
                {
                    _cell.State = TableCellState.On;
                }
                else
                {
                    _cell.State = TableCellState.Off;
                } 
            }
            else  if (!string.IsNullOrEmpty(_selectAllId) || !string.IsNullOrEmpty(_deselectAllId))
            {
                TableCollection tables = Tables;
                foreach (Table table in tables)
                {
                    string selectAllId = CreateId(table, SelectTableEventPrefix);
                    string deselectAllId = CreateId(table, DeSelectTableEventPrefix);

                    if (selectAllId == _selectAllId)
                    {
                        table.SetState(TableCellState.On);
                    }

                    if (deselectAllId == _deselectAllId)
                    {
                        table.SetState(TableCellState.Off);
                    }
                }
            }
		}

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			return LoadPostData(postDataKey, postCollection);
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			RaisePostDataChangedEvent();
		}

		#endregion
	}
}