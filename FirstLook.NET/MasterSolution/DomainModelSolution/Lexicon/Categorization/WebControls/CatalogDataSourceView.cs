using System;
using System.Collections;
using System.Web.UI;

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
    internal class CatalogDataSourceView : DataSourceView
    {
        private readonly CatalogDataSource _owner;

        public CatalogDataSourceView(CatalogDataSource owner, string viewName) : base(owner, viewName)
        {
            this._owner = owner;
        }

        protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
        {
            arguments.RaiseUnsupportedCapabilitiesError(this);

            return new Catalog[] {GetCatalog()};
        }

        internal Catalog GetCatalog()
        {
            return Catalog.GetCatalog(
                _owner.GetSelectedModelYearId(),
                _owner.GetSelectedMakeId(),
                _owner.GetSelectedLineId());
        }

        internal void RaiseChangedEvent()
        {
            OnDataSourceViewChanged(EventArgs.Empty);
        }
    }
}