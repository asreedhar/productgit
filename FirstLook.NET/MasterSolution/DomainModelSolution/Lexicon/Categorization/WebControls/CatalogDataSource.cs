using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
	[
	DefaultProperty("Parameters"),
	ParseChildren(true),
	PersistChildren(false)
	]
	public class CatalogDataSource : DataSourceControl
	{
		public static readonly string DefaultViewName = "Catalog";

		private ParameterCollection _parameters;

		[
		DefaultValue(null),
		Editor(typeof(ParameterCollectionEditor), typeof(UITypeEditor)),
		MergableProperty(false),
		PersistenceMode(PersistenceMode.InnerProperty),
		Category("Data")
		]
		public ParameterCollection SelectParameters
		{
			get
			{
				if (_parameters == null)
				{
					_parameters = new ParameterCollection();
					_parameters.ParametersChanged += this.OnParametersChanged;
					if (IsTrackingViewState)
					{
						((IStateManager)_parameters).TrackViewState();
					}
				}
				return _parameters;
			}
		}

		private void OnParametersChanged(object sender, EventArgs e)
		{
			CatalogView.RaiseChangedEvent();
		}

		internal int GetSelectedModelYearId()
		{
			if (_parameters != null)
			{
				Parameter modelYearParameter = _parameters["ModelYearId"];
				if (modelYearParameter != null)
				{
					IOrderedDictionary parameterValues = _parameters.GetValues(Context, this);
					return (int) parameterValues[modelYearParameter.Name];
				}
			}
			throw new ArgumentException("Missing ModelYearId");
		}

		internal int GetSelectedMakeId()
		{
			if (_parameters != null)
			{
				Parameter makeParameter = _parameters["MakeId"];
				if (makeParameter != null)
				{
					IOrderedDictionary parameterValues = _parameters.GetValues(Context, this);
					return (int)parameterValues[makeParameter.Name];
				}
			}
			throw new ArgumentException("Missing MakeId");
		}

		internal int GetSelectedLineId()
		{
			if (_parameters != null)
			{
				Parameter lineParameter = _parameters["LineId"];
				if (lineParameter != null)
				{
					IOrderedDictionary parameterValues = _parameters.GetValues(Context, this);
					return (int)parameterValues[lineParameter.Name];
				}
			}
			throw new ArgumentException("Missing LineId");
		}

		private CatalogDataSourceView _catalogView;

		private CatalogDataSourceView CatalogView
		{
			get
			{
				if (_catalogView == null)
				{
					_catalogView = new CatalogDataSourceView(this, DefaultViewName);
				}
				return _catalogView;
			}
		}

		protected override DataSourceView GetView(string viewName)
		{
			if (string.IsNullOrEmpty(viewName) || string.Compare(viewName, DefaultViewName, StringComparison.OrdinalIgnoreCase) == 0)
			{
				return CatalogView;
			}
			throw new ArgumentOutOfRangeException("viewName");
		}

		protected override ICollection GetViewNames()
		{
			return new string[] { DefaultViewName };
		}

		protected override void OnInit(EventArgs e)
		{
			Page.LoadComplete += OnPageLoadComplete;
		}

		private void OnPageLoadComplete(object sender, EventArgs e)
		{
			SelectParameters.UpdateValues(Context, this);
		}

		protected override void LoadViewState(object savedState)
		{
			object baseState = null;

			if (savedState != null)
			{
				Pair p = (Pair)savedState;
				baseState = p.First;
				if (p.Second != null)
				{
					((IStateManager)SelectParameters).LoadViewState(p.Second);
				}
			}

			base.LoadViewState(baseState);
		}

		protected override object SaveViewState()
		{
			object baseState = base.SaveViewState();
			object parameterState = null;
			if (_parameters != null)
			{
				parameterState = ((IStateManager)_parameters).SaveViewState();
			}
			if ((baseState != null) || (parameterState != null))
			{
				return new Pair(baseState, parameterState);
			}
			return null;
		}

		protected override void TrackViewState()
		{
			base.TrackViewState();
			if (_catalogView != null)
			{
				if (_parameters != null)
				{
					((IStateManager) _parameters).TrackViewState();
				}
			}
		}
	}
}
