﻿using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
    public class CatalogSelectionMessageEventArgs : EventArgs
    {
        private readonly string _message;

        public CatalogSelectionMessageEventArgs(string message)
        {
            _message = message;
        }

        public string Message
        {
            get { return _message; }
        }
    }
}
