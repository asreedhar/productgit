using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.WebControls
{
	public class CatalogSelectionEventArgs : EventArgs
	{
		private readonly int _modelConfigurationId;
		private readonly bool _selected;
		
		internal CatalogSelectionEventArgs(int modelConfigurationId, bool selected)
		{
			_selected = selected;
			_modelConfigurationId = modelConfigurationId;
		}

		public int ModelConfigurationId
		{
			get { return _modelConfigurationId; }
		}

		public bool Selected
		{
			get { return _selected; }
		}
	}
}
