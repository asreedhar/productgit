using System;
using System.Collections;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
    public interface INameable
    {
        string Name { get; }
    }

	public interface INameable<TKey> : INameable where TKey : INameable<TKey>
	{
	}

    public class NameableComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            INameable a = (INameable)x, b = (INameable)y;

            if (a == null && b == null)
            {
                return 0;
            }
            if (a == null)
            {
                return 1;
            }
            if (b == null)
            {
                return -1;
            }

            int returnValue = a.Name.CompareTo(b.Name);

            if (string.IsNullOrEmpty(a.Name) || string.IsNullOrEmpty(b.Name))
            {
                return -returnValue;
            }
            return returnValue;
        }
    }
}
