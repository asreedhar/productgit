using System;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class ModelConfigurationList : ReadOnlyListBase<ModelConfigurationList, ModelConfiguration>, IExplicitlySerializable
	{
		#region Business Methods

		public ModelConfiguration GetItem(int id)
		{
			foreach (ModelConfiguration configuration in this)
				if (configuration.Id == id)
					return configuration;
			return null;
		}

        public static List<KeyValuePair<TKey, List<ModelConfiguration>>> MapProperty<TKey>(IEnumerable<ModelConfiguration> values, ModelConfigurationPropertyAccessor<TKey> accessor) where TKey : class, INameable
        {
            Dictionary<TKey, List<ModelConfiguration>> map = new Dictionary<TKey, List<ModelConfiguration>>();

            foreach (ModelConfiguration value in values)
            {
                TKey key = accessor(value);

                if (key != null)
                {
                    if (map.ContainsKey(key))
                    {
                        map[key].Add(value);
                    }
                    else
                    {
                        List<ModelConfiguration> list = new List<ModelConfiguration>();
                        list.Add(value);
                        map[key] = list;
                    }
                }
            }

            List<KeyValuePair<TKey, List<ModelConfiguration>>> pairs = new List<KeyValuePair<TKey, List<ModelConfiguration>>>();

            foreach (KeyValuePair<TKey, List<ModelConfiguration>> pair in map)
            {
                pairs.Add(pair);
            }

            pairs.Sort(
                delegate(KeyValuePair<TKey, List<ModelConfiguration>> x, KeyValuePair<TKey, List<ModelConfiguration>> y)
                {
                    return x.Key.Name.CompareTo(y.Key.Name);
                });

            return pairs;
        }
		
		#endregion

		#region Factory Methods

		internal static ModelConfigurationList GetModelConfigurations(Catalog catalog, IDataReader reader)
		{
			if (catalog == null)
				throw new ArgumentNullException("catalog");
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new ModelConfigurationList(catalog, reader);
		}

		private ModelConfigurationList(Catalog catalog, IDataReader reader)
		{
			Fetch(catalog, reader);
		}

		#endregion

		#region Data Access

		private void Fetch(Catalog catalog, IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(ModelConfiguration.GetModelConfiguration(catalog, reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal ModelConfigurationList(SerializationData data)
		{
			IEnumerable<ModelConfiguration> items = data.GetValues<ModelConfiguration>("Items");
			IEnumerator<ModelConfiguration> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
