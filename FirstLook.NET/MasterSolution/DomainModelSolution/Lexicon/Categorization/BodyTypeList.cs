using System;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class BodyTypeList : ReadOnlyListBase<BodyTypeList, BodyType>, IExplicitlySerializable
	{
		#region Business Methods

		public BodyType GetItem(int id)
		{
			foreach (BodyType bodyType in this)
				if (bodyType.Id == id)
					return bodyType;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static BodyTypeList GetBodyTypes(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new BodyTypeList(reader);
		}

		private BodyTypeList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(BodyType.GetBodyType(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal BodyTypeList(SerializationData data)
		{
			IEnumerable<BodyType> items = data.GetValues<BodyType>("Items");
			IEnumerator<BodyType> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
