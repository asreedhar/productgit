using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
    [Serializable]
    public class MakeList : ReadOnlyListBase<MakeList, Make>
    {
        #region Business Methods

        /// <summary>
        /// Get a make for a given id.
        /// </summary>
        /// <param name="id">Identifier of a make.</param>
        /// <returns>A make object for the id.</returns>
        public Make GetItem(int id)
        {
            foreach (Make make in this)
            {
                if (make.Id == id)
                {
                    return make;
                }
            }
            return null;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Get a list of makes from the given data reader.
        /// </summary>
        /// <param name="reader">Reader for database values.</param>
        /// <returns>A list of makes.</returns>
        internal static MakeList GetMakes(IDataReader reader)
        {
            return new MakeList(reader);
        }

        /// <summary>
        /// Get a list of makes between the low and high model years.
        /// </summary>
        /// <param name="lowModelYear">Year earlier than which we do not want makes.</param>
        /// <param name="highModelYear">Year later than which we do not want makes.</param>
        /// <returns>A list of makes.</returns>
        public static MakeList GetMakes(int lowModelYear, int highModelYear)
        {
            return DataPortal.Fetch<MakeList>(new ModelYearCriteria(lowModelYear, highModelYear));
        }

        /// <summary>
        /// Data source for retrieving a list of makes from the database.
        /// </summary>
        public class DataSource
        {
            /// <summary>
            /// Get all makes in a given year range.
            /// </summary>
            /// <param name="lowModelYear">Year earlier than which we do not want makes.</param>
            /// <param name="highModelYear">Year later than which we do not want makes.</param>
            /// <returns>A list of makes.</returns>
            public MakeList GetMakes(int lowModelYear, int highModelYear)
            {
                return MakeList.GetMakes(lowModelYear, highModelYear);
            }
        }

        /// <summary>
        /// Criteria by which to find a list of makes in the database.
        /// </summary>
        [Serializable]
        private class ModelYearCriteria
        {
            private readonly int _lowModelYear;  // Year earlier than which we do not want makes.
            private readonly int _highModelYear; // Year later than which we do not want makes.

            public ModelYearCriteria(int lowModelYear, int highModelYear)
            {
                _lowModelYear = lowModelYear;
                _highModelYear = highModelYear;
            }

            public int LowModelYear
            {
                get { return _lowModelYear; }
            }

            public int HighModelYear
            {
                get { return _highModelYear; }
            }
        }

        /// <summary>
        /// Private constructor to force creation to go through factory methods.
        /// </summary>
        private MakeList()
        {            
        }

        /// <summary>
        /// Fetch a list of makes from database values. Private constructor to force creation to go through factory
        /// methods.
        /// </summary>
        /// <param name="reader"></param>
        private MakeList(IDataReader reader)
        {
            Fetch(reader);
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Fetch a list of makes from values retrieved from the database.
        /// </summary>
        /// <param name="criteria">Criteria by which to find these makes.</param>
        private void DataPortal_Fetch(ModelYearCriteria criteria)
        {
            Fetch(criteria.LowModelYear, criteria.HighModelYear);
        }

        /// <summary>
        /// Fetch a list of makes from values retrieved from the database.
        /// </summary>
        /// <param name="lowModelYear">Year earlier than which we do not want makes.</param>
        /// <param name="highModelYear">Year later than which we do not want makes.</param>
        private void Fetch(int lowModelYear, int highModelYear)
        {
            using (IDbConnection connection = Database.CreateConnection(Database.CategorizationDatabase))
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Categorization.MakeList#Fetch";

                    Database.AddWithValue(command, "LowModelYear", lowModelYear, DbType.Int32);
                    Database.AddWithValue(command, "HighModelYear", highModelYear, DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        /// <summary>
        /// Fetch  a list of makes from values retrieved from the database.
        /// </summary>
        /// <param name="reader">List of makes retrieved from the database.</param>
        private void Fetch(IDataReader reader)
        {
            IsReadOnly = false;
            while (reader.Read())
            {
                Add(Make.GetMake(reader));
            }
            IsReadOnly = true;
        }

        #endregion
    }
}