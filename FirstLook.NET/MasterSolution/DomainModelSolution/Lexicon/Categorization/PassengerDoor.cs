using System;
using System.Collections;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class PassengerDoor : ReadOnlyBase<PassengerDoor>, INameable<PassengerDoor>, IExplicitlySerializable
	{
		#region Business Methods

		private int id;
		private string name;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		public ICollection GetObjectState()
		{
			return new object[] { id, name };
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static PassengerDoor GetPassengerDoor(IDataRecord record)
		{
			return new PassengerDoor(record);
		}

		private PassengerDoor(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationName = "N";

		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, id);
			data.AddValue(SerializationName, name);
		}

		internal PassengerDoor(SerializationData data)
		{
			id = data.GetInt32(SerializationId);
			name = data.GetString(SerializationName);
		}

		#endregion
	}
}
