using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class EngineList : ReadOnlyListBase<EngineList, Engine>, IExplicitlySerializable
	{
		#region Business Methods

		public Engine GetItem(int id)
		{
			foreach (Engine engine in this)
				if (engine.Id == id)
					return engine;
			return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }    

		#endregion

		#region Factory Methods

		internal static EngineList GetEngines(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new EngineList(reader);
		}

		private EngineList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Engine.GetEngine(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal EngineList(SerializationData data)
		{
			IEnumerable<Engine> items = data.GetValues<Engine>("Items");
			IEnumerator<Engine> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
