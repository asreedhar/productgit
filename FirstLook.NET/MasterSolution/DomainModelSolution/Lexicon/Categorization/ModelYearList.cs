using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
    [Serializable]
    public class ModelYearList : ReadOnlyListBase<ModelYearList, ModelYear>
    {
        #region Business Methods

        public ModelYear GetItem(int id)
        {
            foreach (ModelYear modelYear in this)
                if (modelYear.Id == id)
                    return modelYear;
            return null;
        }

        #endregion

        #region Factory Methods

        internal static ModelYearList GetModelYears(IDataReader reader)
        {
            return new ModelYearList(reader);
        }

        public static ModelYearList GetModelYears()
        {
            return DataPortal.Fetch<ModelYearList>(new Criteria());
        }

        [Serializable]
        private class Criteria
        {
        }

        public class DataSource
        {
            public ModelYearList GetModelYears()
            {
                return ModelYearList.GetModelYears();
            }
        }

        private ModelYearList()
        {
            /* force use of factory methods */
        }

        private ModelYearList(IDataReader reader)
        {
            Fetch(reader);
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(Criteria criteria)
        {
            Fetch();
        }

        private void Fetch()
        {
            using (IDbConnection connection = Database.CreateConnection(Database.CategorizationDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Categorization.ModelYearList#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        private void Fetch(IDataReader reader)
        {
            IsReadOnly = false;
            while (reader.Read())
            {
                Add(ModelYear.GetModelYear(reader));
            }
            IsReadOnly = true;
        }

        #endregion
    }
}