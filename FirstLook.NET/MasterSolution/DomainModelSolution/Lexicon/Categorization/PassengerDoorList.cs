using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class PassengerDoorList : ReadOnlyListBase<PassengerDoorList, PassengerDoor>, IExplicitlySerializable
	{
		#region Business Methods

		public PassengerDoor GetItem(int id)
		{
			foreach (PassengerDoor passengerDoor in this)
				if (passengerDoor.Id == id)
					return passengerDoor;
			return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }    

		#endregion

		#region Factory Methods

		internal static PassengerDoorList GetPassengerDoors(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new PassengerDoorList(reader);
		}

		private PassengerDoorList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(PassengerDoor.GetPassengerDoor(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal PassengerDoorList(SerializationData data)
		{
			IEnumerable<PassengerDoor> items = data.GetValues<PassengerDoor>("Items");
			IEnumerator<PassengerDoor> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
