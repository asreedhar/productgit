using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class ModelList : ReadOnlyListBase<ModelList, Model>, IExplicitlySerializable
	{
		#region Business Methods

		public Model GetItem(int id)
		{
			foreach (Model model in this)
				if (model.Id == id)
					return model;
			return null;
		}

        public IList<Model.Presentation> ToPresentation()
        {
            List<Model.Presentation> items = new List<Model.Presentation>();
            foreach (Model item in this)
            {
                items.Add(item.ToPresentation());
            }
            return items;
        }

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }        

		#endregion

		#region Factory Methods

		internal static ModelList GetModels(Catalog catalog, IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new ModelList(catalog, reader);
		}

		private ModelList(Catalog catalog, IDataReader reader)
		{
			Fetch(catalog, reader);
		}

		#endregion

		#region Data Access

		private void Fetch(Catalog catalog, IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Model.GetModel(catalog, reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal ModelList(SerializationData data)
		{
			IEnumerable<Model> items = data.GetValues<Model>("Items");
			IEnumerator<Model> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
