using System;
using System.Collections;
using System.Data;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	internal class ModelConfigurationMapping : IExplicitlySerializable
	{
		#region Business Methods

		private int modelConfigurationId;
		private int catalogEntryId;

		public int ModelConfigurationId
		{
			get { return modelConfigurationId; }
		}

		public int CatalogEntryId
		{
			get { return catalogEntryId; }
		}

		public ICollection GetObjectState()
		{
			return new object[] { modelConfigurationId, catalogEntryId};
		}

		#endregion

		#region Factory Methods

		internal static ModelConfigurationMapping GetModelConfigurationMapping(IDataRecord record)
		{
			return new ModelConfigurationMapping(record);
		}

		private ModelConfigurationMapping(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			modelConfigurationId = record.GetInt32(record.GetOrdinal("ModelConfigurationId"));
			catalogEntryId = record.GetInt32(record.GetOrdinal("CatalogEntryId"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationModelConfigurationId = "M";
		private const string SerializationCatalogEntryId = "C";
		
		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationModelConfigurationId, modelConfigurationId);
			data.AddValue(SerializationCatalogEntryId, catalogEntryId);
		}

		internal ModelConfigurationMapping(SerializationData data)
		{
			modelConfigurationId = data.GetInt32(SerializationModelConfigurationId);
			catalogEntryId = data.GetInt32(SerializationCatalogEntryId);
		}

		#endregion
	}
}
