namespace FirstLook.DomainModel.Lexicon.Categorization
{
	public delegate T ModelConfigurationPropertyAccessor<T>(ModelConfiguration modelConfiguration)
		where T : class, INameable;
}
