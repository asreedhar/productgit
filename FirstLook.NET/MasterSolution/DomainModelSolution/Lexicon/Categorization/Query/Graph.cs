using System;
using System.Collections.Generic;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	internal class Graph : GraphNode
	{
		private readonly IList<Type> _discriminators = new List<Type>();

		private readonly Catalog _catalog;

		public Graph(Catalog catalog) : base (new List<ModelConfiguration>(catalog.ModelConfigurations))
		{
			_catalog = catalog;
		}

		public void Sort(ModelConfiguration modelConfiguration)
		{
			GraphNode child = Find(modelConfiguration);

			if (child != null && child.Parent != null)
			{
				GraphNode parent = child.Parent;

				while (parent.Parent != null)
				{
					child = parent;

					parent = parent.Parent;

					if (parent.Children.Remove(child))
					{
						parent.Children.Insert(0, child);
					}
					else
					{
						break;
					}
				}
			}
		}

		public List<GraphNode> GatherLeaves()
		{
			return GatherLeaves(this, new List<GraphNode>());
		}

		private static List<GraphNode> GatherLeaves(GraphNode node, List<GraphNode> leaves)
		{
			if (node.Children.Count == 0)
			{
				leaves.Add(node);
			}
			else
			{
				foreach (GraphNode child in node.Children)
				{
					GatherLeaves(child, leaves);
				}
			}

			return leaves;
		}

		/// <remarks>
		/// Since we have VinPatternMapping in the Catalog class why on earth do we need to perform this
		/// operation?  Surely the result of this method is simply the set of model configurations
		/// associated with a vin pattern in the VinPatternMappingList?  Was I carzy or have I forgotten
		/// something?
		/// </remarks>
		public void PartialOrder(VinPattern reference, ModelConfiguration modelConfiguration)
		{
			// Cache mapping from VinPattern to ModelConfiguration

			List<KeyValuePair<VinPattern, ICollection<ModelConfiguration>>> map = new List<KeyValuePair<VinPattern, ICollection<ModelConfiguration>>>();

			ICollection<ModelConfiguration> values = null;

			foreach (VinPattern vinPattern in _catalog.VinPatterns)
			{
				ICollection<ModelConfiguration> configurations = vinPattern.GetModelConfigurations();

				if (vinPattern.Equals(reference))
				{
					values = configurations;
				}

				map.Add(new KeyValuePair<VinPattern, ICollection<ModelConfiguration>>(vinPattern, configurations));
			}

			// Remove reference VinPattern and those VinPatterns whose set is larger than that of the reference

			map.RemoveAll(
				delegate(KeyValuePair<VinPattern, ICollection<ModelConfiguration>> match)
					{
						return match.Key.Equals(reference) || match.Value.Count > values.Count;
					});

			// Keep list of transitive closure of the partial ordering

			List<ModelConfiguration> closure = new List<ModelConfiguration>();

			closure.Add(reference.ModelConfiguration);

			closure.AddRange(values);

			// perform transitive closure over the partial ordering

			foreach (KeyValuePair<VinPattern, ICollection<ModelConfiguration>> pair in map)
			{
				if (TrueForAll(pair.Value, delegate(ModelConfiguration match)
				                           	{
				                           		return Exists(values,
				                           		              delegate(ModelConfiguration value)
				                           		              	{
				                           		              		return match.Equals(value);
				                           		              	});
				                           	}))
				{
					if (!closure.Contains(pair.Key.ModelConfiguration))
					{
						closure.Add(pair.Key.ModelConfiguration);
					}
					
					foreach (ModelConfiguration value in pair.Value)
					{
						if (!closure.Contains(value))
						{
							closure.Add(value);
						}
					}
				}
			}

			// seperate out candidates from complement

			Partition(Find(modelConfiguration), PartialOrderAccessor(closure));
		}

		private static ModelConfigurationPropertyAccessor<INameable> PartialOrderAccessor(ICollection<ModelConfiguration> closure)
		{
			return delegate(ModelConfiguration modelConfiguration)
			       	{
			       		return closure.Contains(modelConfiguration)
							? PartialOrderSet.Candidate
							: PartialOrderSet.Complement;
			       	};
		}

		public void Partition<T>(ModelConfigurationPropertyAccessor<T> accessor) where T : class, INameable
		{
			if (_discriminators.Contains(typeof(T)))
			{
				throw new ArgumentException("Cannot partition by an existing discriminator", "accessor");
			}
			else
			{
				_discriminators.Add(typeof(T));
			}

			Partition(this, accessor);
		}

		private static void Partition<T>(GraphNode node, ModelConfigurationPropertyAccessor<T> accessor) where T : class, INameable
		{
			if (node.Children.Count == 0)
			{
				Dictionary<T, List<ModelConfiguration>> partitions = new Dictionary<T, List<ModelConfiguration>>();

				foreach (ModelConfiguration modelConfiguration in node.ModelConfigurations)
				{
					T value = accessor(modelConfiguration);

					if (value != null)
					{
						if (!partitions.ContainsKey(value))
						{
							partitions[value] = new List<ModelConfiguration>();
						}

						partitions[value].Add(modelConfiguration);
					}
				}

				foreach (KeyValuePair<T, List<ModelConfiguration>> pair in partitions)
				{
					foreach (ModelConfiguration modelConfiguration in pair.Value)
					{
						node.ModelConfigurations.Remove(modelConfiguration);
					}

					GraphNode newPartition = new GraphNode(node, typeof(T), pair.Key, pair.Value);

					node.Children.Add(newPartition);

					node.Children.Sort(delegate(GraphNode x, GraphNode y) { return x.Discriminator.Name.CompareTo(y.Discriminator.Name); });
				}
			}
			else
			{
				foreach (GraphNode child in node.Children)
				{
					Partition(child, accessor);
				}
			}
		}

		private static bool TrueForAll<T>(IEnumerable<T> list, Predicate<T> match)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			foreach (T item in list)
			{
				if (!match(item))
				{
					return false;
				}
			}
			return true;
		}

		private static bool Exists<T>(IEnumerable<T> list, Predicate<T> match)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			foreach (T item in list)
			{
				if (match(item))
				{
					return true;
				}
			}
			return false;
		}

		class PartialOrderSet : INameable
		{
			public static readonly PartialOrderSet Candidate = new PartialOrderSet("Candidate");
			public static readonly PartialOrderSet Complement = new PartialOrderSet("Complement");

			private readonly string name;

			private PartialOrderSet(string name)
			{
				this.name = name;
			}

			public string Name
			{
				get { return name; }
			}
		}
	}
}
