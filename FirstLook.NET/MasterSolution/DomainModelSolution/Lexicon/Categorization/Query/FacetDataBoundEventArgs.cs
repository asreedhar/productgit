using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
    public class FacetDataBoundEventArgs : EventArgs
    {
        private readonly int modelConfigurationId;
        private bool selected;

        internal FacetDataBoundEventArgs(int modelConfigurationId)
        {
            this.modelConfigurationId = modelConfigurationId;
        }

        public int ModelConfigurationId
        {
            get { return modelConfigurationId; }
        }

        public bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }
    }
}
