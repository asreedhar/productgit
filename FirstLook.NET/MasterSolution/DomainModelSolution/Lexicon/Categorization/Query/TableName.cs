namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableName
	{
		private readonly ModelFamily modelFamily;
		private readonly Segment segment;
		private readonly BodyType bodyType;

		internal TableName(ModelFamily modelFamily, Segment segment, BodyType bodyType)
		{
			this.modelFamily = modelFamily;
			this.segment = segment;
			this.bodyType = bodyType;
		}

		public ModelFamily ModelFamily
		{
			get { return modelFamily; }
		}

		public Segment Segment
		{
			get { return segment; }
		}

		public BodyType BodyType
		{
			get { return bodyType; }
		}
	}
}
