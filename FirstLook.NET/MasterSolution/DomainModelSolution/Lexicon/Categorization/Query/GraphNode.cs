using System;
using System.Collections.Generic;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	internal class GraphNode
	{
		private readonly List<GraphNode> children = new List<GraphNode>();
		private readonly GraphNode parent;
		private readonly Type discriminatorType;
		private readonly INameable discriminator;
		private readonly List<ModelConfiguration> modelConfigurations = new List<ModelConfiguration>();

		protected GraphNode(List<ModelConfiguration> modelConfigurations)
		{
			this.modelConfigurations = modelConfigurations;
		}

		// I want this to be protected too but it cannot be so ...
		internal GraphNode(GraphNode parent, Type discriminatorType, INameable discriminator, List<ModelConfiguration> modelConfigurations)
		{
			this.parent = parent;
			this.discriminatorType = discriminatorType;
			this.discriminator = discriminator;
			this.modelConfigurations = modelConfigurations;
		}

		public string Name
		{
			get
			{
				if (parent == null)
				{
					return string.Empty;
				}
				else
				{
					return discriminator.Name;
				}
			}
		}

		public string FullName
		{
			get
			{
				if (parent == null)
				{
					return string.Empty;
				}
				else if (parent.parent == null)
				{
					return Name;
				}
				else
				{
					return parent.FullName + " " + Name; // left recursive string concatenation = slow ?
				}
			}
		}

		public GraphNode Parent
		{
			get { return parent; }
		}

		public Type DiscriminatorType
		{
			get { return discriminatorType; }
		}

		public INameable Discriminator
		{
			get { return discriminator; }
		}

		public List<GraphNode> Children
		{
			get { return children; }
		}

		public List<ModelConfiguration> ModelConfigurations
		{
			get { return modelConfigurations; }
		}

		protected GraphNode Find(ModelConfiguration modelConfiguration)
		{
			if (modelConfigurations.Contains(modelConfiguration))
			{
				return this;
			}
			else
			{
				foreach (GraphNode child in children)
				{
					GraphNode node = child.Find(modelConfiguration);

					if (node != null)
					{
						return node;
					}
				}

				return null;
			}
		}
	}
}
