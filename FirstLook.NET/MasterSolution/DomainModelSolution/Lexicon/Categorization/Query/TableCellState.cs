using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public enum TableCellState
	{
		Off,
		On,
		Implied
	}
}
