using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableRow
	{
		public const int ColumnCount = 6;

		private readonly EventHandler<TableSelectionEventArgs<TableCell>> handler;

		private readonly int rowIndex;

		private readonly TableCell[] cells = new TableCell[ColumnCount];

		public TableRow(int rowIndex, EventHandler<TableSelectionEventArgs<TableCell>> handler)
		{
			this.rowIndex = rowIndex;
			this.handler = handler;
		}

		internal TableCell NewCell(int column, INameable value, TableCellCharacterization characterization)
		{
			return (cells[column] = new TableCell(rowIndex, column, value, characterization, handler));
		}

		public int RowIndex
		{
			get { return rowIndex; }
		}

		public TableCell this[int colIndex]
		{
			get
			{
				return cells[colIndex];
			}
		}

		public TableCell Series
		{
			get { return cells[0]; }
		}

		public TableCell Engine
		{
			get { return cells[1]; }
		}

		public TableCell Transmission
		{
			get { return cells[2]; }
		}

		public TableCell DriveTrain
		{
			get { return cells[3]; }
		}

		public TableCell FuelType
		{
			get { return cells[4]; }
		}

		public TableCell PassengerDoor
		{
			get { return cells[5]; }
		}
	}
}
