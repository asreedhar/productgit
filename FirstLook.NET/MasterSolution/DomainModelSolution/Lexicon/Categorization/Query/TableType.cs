using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public enum TableType
	{
		CandidateClosure,
		CandidateComplement,
		Spectator
	}
}
