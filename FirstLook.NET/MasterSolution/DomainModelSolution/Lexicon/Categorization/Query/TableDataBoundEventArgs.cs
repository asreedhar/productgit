using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableDataBoundEventArgs : EventArgs
	{
		private readonly int modelConfigurationId;
		private bool selected;

		internal TableDataBoundEventArgs(int modelConfigurationId)
		{
			this.modelConfigurationId = modelConfigurationId;
		}

		public int ModelConfigurationId
		{
			get { return modelConfigurationId; }
		}

		public bool Selected
		{
			get { return selected; }
			set { selected = value; }
		}
	}
}
