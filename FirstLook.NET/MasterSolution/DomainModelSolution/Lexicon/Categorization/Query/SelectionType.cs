namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
    public enum SelectionType
    {
        ModelFamily,
        Segment,
        BodyType,
        PassengerDoor,
        FuelType,
        DriveTrain,
        Transmission,
        Engine,
        Series
    }
}