using System;
using System.Collections.Generic;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
    public class Facet
    {
        public void Initialize(IList<ModelConfiguration> modelConfigurations)
        {
            _modelConfigurations.Clear();

            foreach (ModelConfiguration modelConfiguration in modelConfigurations)
            {
                _modelConfigurations.Add(modelConfiguration);
            }
        }

        private readonly IList<ModelConfiguration> _modelConfigurations = new List<ModelConfiguration>();

        private static Type[] _types;

        private static Type[] Types
        {
            get
            {
                if (_types == null)
                {
                    _types = new[]
                                 {
                                     typeof(ModelFamily),
                                     typeof(Segment),
                                     typeof(BodyType),
                                     typeof(PassengerDoor),
                                     typeof(FuelType),
                                     typeof(DriveTrain),
                                     typeof(Transmission),
                                     typeof(Engine),
                                     typeof(Series)
                                 };
                }

                return _types;
            }
        }

        private Dictionary<int, bool> _selected;

        private Dictionary<int, bool> Selection
        {
            get
            {
                if (_selected == null)
                {
                    _selected = new Dictionary<int, bool>();
                }

                return _selected;
            }
        }

        private FacetNode _includedFacetNode;
        private FacetNode _excludedFacetNode;

        public FacetNode IncludedFacetNode
        {
            get
            {
                if (_includedFacetNode == null)
                {
                    _includedFacetNode = new FacetNode(new List<ModelConfiguration>());
                }

                return _includedFacetNode;
            }
        }

        public FacetNode ExcludedFacetNode
        {
            get
            {
                if (_excludedFacetNode == null)
                {
                    _excludedFacetNode = new FacetNode(_modelConfigurations);
                }

                return _excludedFacetNode;
            }
        }

        public bool IsSelected<TKey>(INameable<TKey> attribute) where TKey : INameable<TKey>
        {
            return _includedFacetNode[typeof (TKey)].Contains(attribute);
        }

        public bool HasSingleValue(Type type)
        {
            List<INameable> excluded = _excludedFacetNode[type];
            List<INameable> included = _includedFacetNode[type];

            int excludedCount = excluded.Count;
            int includedCount = included.Count;

            if (excludedCount == 1 && includedCount == 1)
            {
                return (Equals(excluded[0].Name, included[0].Name));
            }

            if (includedCount != 0 && includedCount + excludedCount <= 1)
            {
                return true;
            }

            return false;
        }

        public bool IsEverythingSelected(Type type)
        {
            List<INameable> excluded = _excludedFacetNode[type];
            List<INameable> included = _includedFacetNode[type];

            int excludedCount = excluded.Count;
            int includedCount = included.Count;

            if (excludedCount == 0 || includedCount == 0)
                return true;

            foreach (INameable node in excluded)
                if (!included.Contains(node))
                    return false;

            return true;
        }

        public bool IsEverythingSelected(Type type, bool hasNull)
        {
            bool returnValue = IsEverythingSelected(type);

            if (hasNull && returnValue)
            {
                List<INameable> included = _includedFacetNode[type];

                returnValue = included.Contains(null);
            }

            return returnValue;
        }

        public void DataBind(EventHandler<FacetDataBoundEventArgs> handler)
        {
            if (handler == null)
            {
                throw new ArgumentNullException("handler");
            }

            foreach (ModelConfiguration configuration in _modelConfigurations)
            {
                FacetDataBoundEventArgs args = new FacetDataBoundEventArgs(configuration.Id);

                handler(this, args);

                Selection[configuration.Id] = args.Selected;
            }

            HandleSelectedChange();
        }

        internal event EventHandler<FacetConfigurationEventArgs> SelectionChanged;
        internal event EventHandler<FacetSelectionEventArgs> SelectionUnchanged;
        internal event EventHandler<FacetSelectionEventArgs> EmptySelectionSet;

        private void HandleSelectedChange()
        {
            IList<ModelConfiguration> includedModels = new List<ModelConfiguration>();
            IList<ModelConfiguration> excludedModels = new List<ModelConfiguration>();

            foreach (ModelConfiguration configuration in _modelConfigurations)
            {
                if (Selection[configuration.Id])
                {
                    includedModels.Add(configuration);
                }
                else
                {
                    excludedModels.Add(configuration);
                }
            }

            // damn stupid list does not implement equals (at all)

            if (!Equals(IncludedFacetNode.ModelConfigurations, includedModels))
            {
                _includedFacetNode = new FacetNode(includedModels);
            }

            if (!Equals(ExcludedFacetNode.ModelConfigurations, excludedModels))
            {
                _excludedFacetNode = new FacetNode(excludedModels);
            }
        }

        private bool HandleNoEffectSelection<TKey>(INameable attribute, bool selectionState) where TKey : INameable<TKey>
        {
            if (SelectionUnchanged != null)
            {
                IList<ModelConfiguration> possibleConfigurations = ExcludedFacetNode[attribute];

                FacetNode possibleNodes = new FacetNode(possibleConfigurations);

                Dictionary<Type, IList<INameable>> attributes = new Dictionary<Type, IList<INameable>>();

                int totalNumOfModelConfigurations = possibleNodes.ModelConfigurations.Count;

                // Find all Required, not already selected, PM
                foreach (Type type in Types)
                {
                    foreach (INameable nameable in possibleNodes[type])
                    {
                        bool notIncluded = !IncludedFacetNode[type].Contains(nameable);
                        bool hardRequirement = totalNumOfModelConfigurations == possibleNodes[nameable].Count;
                        if (hardRequirement && notIncluded)
                        {
                            if (!attributes.ContainsKey(type))
                            {
                                attributes[type] = new List<INameable>();
                            }

                            attributes[type].Add(nameable);
                        }
                    } 
                }

                FacetSelectionEventArgs args = new FacetSelectionEventArgs(attributes, attribute, selectionState,
                                                           typeof(TKey));

                SelectionUnchanged(this, args);

                return args.Cancel;
            }

            return false;
        }

        private bool HandleEmptyIncludedSet<TKey>(INameable attribute, bool selectionState) where TKey : INameable<TKey>
        {
            if (EmptySelectionSet != null)
            {
                IList<ModelConfiguration> possibleConfigurations = IncludedFacetNode[attribute];

                FacetNode possibleNodes = new FacetNode(possibleConfigurations);

                int totalNumOfModelConfigurations = possibleNodes.ModelConfigurations.Count;

                Dictionary<Type, IList<INameable>> attributes = new Dictionary<Type, IList<INameable>>();

                // Find all Required, not already selected, PM
                foreach (Type type in Types)
                {
                    foreach (INameable nameable in possibleNodes[type])
                    {
                        bool notIncluded = !IncludedFacetNode[type].Contains(nameable);
                        bool hardRequirement = totalNumOfModelConfigurations == possibleNodes[nameable].Count;
                        if (hardRequirement && notIncluded)
                        {
                            if (!attributes.ContainsKey(type))
                            {
                                attributes[type] = new List<INameable>();
                            }

                            attributes[type].Add(nameable);
                        }
                    }
                }

                FacetSelectionEventArgs args = new FacetSelectionEventArgs(attributes, attribute, selectionState,
                                                                           typeof (TKey));

                EmptySelectionSet(this, args);

                return args.Cancel;
            }

            return false;
        }

        private static bool Equals(ICollection<ModelConfiguration> a, ICollection<ModelConfiguration> b)
        {
            if (a.Count != b.Count)
            {
                return false;
            }

            foreach (ModelConfiguration i in a)
            {
                bool exists = false;

                foreach (ModelConfiguration j in b)
                {
                    if (i.Equals(j))
                    {
                        exists = true;

                        break;
                    }
                }

                if (!exists)
                {
                    return false;
                }
            }

            return true;
        }

        public void Add<TKey>(INameable<TKey> attribute) where TKey : INameable<TKey>
        {
            if (_includedFacetNode[typeof (TKey)].Contains(attribute))
                return;

            _includedFacetNode[typeof (TKey)].Add(attribute);            

            List<ModelConfiguration> configurations = new List<ModelConfiguration>();

            foreach (ModelConfiguration configuration in _excludedFacetNode.ModelConfigurations)
            {
                if (Contains(IncludedFacetNode[typeof (ModelFamily)], configuration.Model.ModelFamily) &&
                    Contains(IncludedFacetNode[typeof (Segment)], configuration.Model.Segment) &&
                    Contains(IncludedFacetNode[typeof (BodyType)], configuration.Model.BodyType) &&
                    Contains(IncludedFacetNode[typeof (PassengerDoor)], configuration.Configuration.PassengerDoor) &&
                    Contains(IncludedFacetNode[typeof (FuelType)], configuration.Configuration.FuelType) &&
                    Contains(IncludedFacetNode[typeof (DriveTrain)], configuration.Configuration.DriveTrain) &&
                    Contains(IncludedFacetNode[typeof (Transmission)], configuration.Configuration.Transmission) &&
                    Contains(IncludedFacetNode[typeof (Engine)], configuration.Configuration.Engine) &&
                    Contains(IncludedFacetNode[typeof (Series)], configuration.Model.Series))
                {
                    configurations.Add(configuration);
                }
            }

            if (configurations.Count > 0)
            {
                foreach (ModelConfiguration configuration in configurations)
                {
                    Selection[configuration.Id] = true;

                    if (SelectionChanged != null)
                    {
                        FacetConfigurationEventArgs args = new FacetConfigurationEventArgs(configuration.Id, true);

                        SelectionChanged(this, args);
                    }
                }

                HandleSelectedChange();
            }
            else
            {
                HandleNoEffectSelection<TKey>(attribute, true);
            }
        }

        private static bool Contains(IEnumerable<INameable> items, INameable value)
        {
            foreach (INameable item in items)
            {
                if ((item == null && value == null) ||
                    (item != null && value != null && Equals(item.Name, value.Name)))
                {
                    return true;
                }
            }

            return false;
        }

        public void Remove<TKey>(INameable<TKey> attribute) where TKey : INameable<TKey>
        {
            if (!_includedFacetNode[typeof (TKey)].Contains(attribute))
                return;

            _includedFacetNode[typeof (TKey)].Remove(attribute);

            List<ModelConfiguration> configurations = new List<ModelConfiguration>();

            bool selectionHasChanged = false;

            foreach (ModelConfiguration configuration in _includedFacetNode.ModelConfigurations)
            {
                if (Contains(configuration, attribute))
                {
                    selectionHasChanged = true;
                    configurations.Add(configuration);
                }
            }

            if (selectionHasChanged)
            {
                foreach (ModelConfiguration configuration in configurations)
                {
                    Selection[configuration.Id] = false;

                    bool hasSelections = Selection.ContainsValue(true);

                    if (!hasSelections)
                    {
                        selectionHasChanged = HandleEmptyIncludedSet<TKey>(attribute, false);
                        Selection[configuration.Id] = !selectionHasChanged;
                    }

                    if (SelectionChanged != null)
                    {
                        FacetConfigurationEventArgs args = new FacetConfigurationEventArgs(configuration.Id, Selection[configuration.Id]);

                        SelectionChanged(this, args);
                    }
                }                
                
                if (selectionHasChanged)
                {
                    HandleSelectedChange();    
                }
            }
            else
            {
                HandleNoEffectSelection<TKey>(attribute, false);
            }
        }

        private static bool Contains<TKey>(ModelConfiguration configuration, INameable<TKey> attribute) where TKey : INameable<TKey>
        {
            bool contains = false;

            if (typeof(TKey) == typeof(ModelFamily))
            {
                contains = configuration.Model.ModelFamily == attribute;
            }
            else if (typeof (TKey) == typeof (Segment))
            {
                contains = configuration.Model.Segment == attribute;
            }
            else if (typeof(TKey) == typeof(BodyType))
            {
                contains = configuration.Model.BodyType == attribute;
            }
            else if (typeof (TKey) == typeof (PassengerDoor))
            {
                contains = configuration.Configuration.PassengerDoor == attribute;
            }
            else if (typeof (TKey) == typeof (FuelType))
            {
                contains = configuration.Configuration.FuelType == attribute;
            }
            else if (typeof (TKey) == typeof (DriveTrain))
            {
                contains = configuration.Configuration.DriveTrain == attribute;
            }
            else if (typeof (TKey) == typeof (Transmission))
            {
                contains = configuration.Configuration.Transmission == attribute;
            }
            else if (typeof (TKey) == typeof (Engine))
            {
                contains = configuration.Configuration.Engine == attribute;
            }
            else if (typeof (TKey) == typeof (Series))
            {
                contains = configuration.Model.Series == attribute;                    
            }
            return contains;
        }
    }
}