using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableCell
	{
		private readonly int row;
		private readonly int col;
		private readonly INameable value;
		private readonly EventHandler<TableSelectionEventArgs<TableCell>> handler;
		private readonly TableCellCharacterization characterization;
		private TableCellState state;

		internal TableCell(int row, int col, INameable value, TableCellCharacterization characterization, EventHandler<TableSelectionEventArgs<TableCell>> handler)
		{
			this.row = row;
			this.col = col;
			this.value = value;
			this.handler = handler;
			this.characterization = characterization;
		}

		public int Row
		{
			get { return row; }
		}

		public int Col
		{
			get { return col; }
		}

		public INameable Value
		{
			get { return value; }
		}

		public TableCellCharacterization Characterization
		{
			get { return characterization; }
		}

		public TableCellState State
		{
			get
			{
				return state;
			}
			set
			{
				if (state != value)
				{
					if (value == TableCellState.Implied)
					{
						throw new ArgumentException("'Implied' is invalid argument value", "value");
					}

					state = value;

					OnStateChanged();
				}
			}
		}

		private void OnStateChanged()
		{
			if (state == TableCellState.On)
			{
				handler(this, new TableSelectionEventArgs<TableCell>(this, true));
			}
			else if (state == TableCellState.Off)
			{
				handler(this, new TableSelectionEventArgs<TableCell>(this, false));
			}
		}

		internal void RaiseStateChanged(TableCellState item)
		{
			state = item;
		}
	}
}
