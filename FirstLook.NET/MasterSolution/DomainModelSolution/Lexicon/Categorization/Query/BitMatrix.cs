using System;
using System.Collections;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	[Serializable]
	internal class BitMatrix
	{
		private readonly BitArray _bits;
		private readonly int _columns;
		private readonly int _rows;
		
		public BitMatrix(int columns, int rows)
		{
			if (columns < 0)
				throw new ArgumentOutOfRangeException("columns", columns, "Must be greater than 0");
			if (rows < 0)
				throw new ArgumentOutOfRangeException("rows", rows, "Must be greater than 0");
			_bits = new BitArray(rows * columns, false);
			_columns = columns;
			_rows = rows;
		}

		public int Columns
		{
			get { return _columns; }
		}

		public int Rows
		{
			get { return _rows; }
		}

		public bool this[int column, int row]
		{
			get
			{
				if (column < 0)
					throw new ArgumentOutOfRangeException("column", column, "Must be greater than 0");
				if (row < 0)
					throw new ArgumentOutOfRangeException("row", row, "Must be greater than 0");
				if (column >= _columns)
					throw new ArgumentOutOfRangeException("column", column, "Must be less than Columns");
				if (row >= _rows)
					throw new ArgumentOutOfRangeException("row", row, "Must be less than Rows");
				return _bits[row*_columns + column];
			}
			set
			{
				if (column < 0)
					throw new ArgumentOutOfRangeException("column", column, "Must be greater than 0");
				if (row < 0)
					throw new ArgumentOutOfRangeException("row", row, "Must be greater than 0");
				if (column >= _columns)
					throw new ArgumentOutOfRangeException("column", column, "Must be less than Columns");
				if (row >= _rows)
					throw new ArgumentOutOfRangeException("row", row, "Must be less than Rows");
				_bits[row*_columns + column] = value;
			}
		}
	}
}
