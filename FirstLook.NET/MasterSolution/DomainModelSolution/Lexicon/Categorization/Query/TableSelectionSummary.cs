using System.Collections.Generic;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableSelectionSummary : IEnumerable<IList<INameable>>
	{
		private readonly List<IList<INameable>> attributes = new List<IList<INameable>>(TableRow.ColumnCount);

	    private readonly bool _hasSummary;

	    private readonly bool _hasAll;

		internal TableSelectionSummary(Table table)
		{
			List<IList<INameable>> ons = new List<IList<INameable>>(TableRow.ColumnCount);
			List<IList<INameable>> offs = new List<IList<INameable>>(TableRow.ColumnCount);
			List<IList<INameable>> implieds = new List<IList<INameable>>(TableRow.ColumnCount);

			for (int colIndex = 0, colCount = TableRow.ColumnCount; colIndex < colCount; colIndex++)
			{
				List<INameable> on = new List<INameable>();
				List<INameable> off = new List<INameable>();
				List<INameable> implied = new List<INameable>();

				for (int rowIndex = 0, rowCount = table.Rows.Count; rowIndex < rowCount; rowIndex++)
				{
					TableCell cell = table.Rows[rowIndex][colIndex];

					if (cell != null)
					{
						if (cell.State == TableCellState.On)
						{
							on.Add(cell.Value);
						}
						else if (cell.State == TableCellState.Implied)
						{
							implied.Add(cell.Value);
						}
						else if (cell.State == TableCellState.Off)
						{
							off.Add(cell.Value);
						}
					}
				}

				ons.Add(on);
				offs.Add(off);
				implieds.Add(implied);
			}

			List<IList<INameable>> list = table.TableType == TableType.CandidateClosure ? offs : ons;

			if (list.Exists(delegate(IList<INameable> match) { return match.Count > 0; }))
			{
				attributes.AddRange(list);

                if (!offs.Exists(delegate(IList<INameable> match) { return match.Count > 0; }) &&
                !implieds.Exists(delegate(IList<INameable> match) { return match.Count > 0; }))
                {
                    _hasAll = true;
                }
			}
			else
			{
				attributes.AddRange(implieds);
			}

            _hasSummary = attributes.Exists(delegate(IList<INameable> match) { return match.Count > 0; });
		}

	    public bool HasAll
	    {
	        get
	        {
	            return _hasAll;
	        }
	    }

	    public bool HasSummary
	    {
	        get
	        {
                return _hasSummary;
	        }
	    }

		public IList<INameable> Series
		{
			get { return attributes[0]; }
		}

		public IList<INameable> Engine
		{
			get { return attributes[1]; }
		}

		public IList<INameable> Transmission
		{
			get { return attributes[2]; }
		}

		public IList<INameable> DriveTrain
		{
			get { return attributes[3]; }
		}

		public IList<INameable> FuelType
		{
			get { return attributes[4]; }
		}

		public IList<INameable> PassengerDoor
		{
			get { return attributes[5]; }
		}

		#region IEnumerable<IEnumerable<INameable>> Members

		public IEnumerator<IList<INameable>> GetEnumerator()
		{
			return attributes.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return attributes.GetEnumerator();
		}

		#endregion
	}
}
