using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	[Flags]
	public enum TableCellCharacterization
	{
		None = 0,
		Primary = 1,
		Secondary = 2
	}
}
