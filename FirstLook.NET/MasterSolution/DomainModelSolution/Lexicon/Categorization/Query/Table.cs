using System;
using System.Collections;
using System.Collections.Generic;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class Table
	{
		internal static Table NewTable(int id, TableName name, TableType tableType, List<ModelConfiguration> modelConfigurations)
		{
			modelConfigurations.Sort(delegate(ModelConfiguration x, ModelConfiguration y) { return x.Id.CompareTo(y.Id); });

			List<KeyValuePair<Series, List<ModelConfiguration>>> series = ModelConfigurationList.MapProperty<Series>(modelConfigurations, ModelConfigurationPropertyAccessors.SeriesAccessor);
            List<KeyValuePair<Engine, List<ModelConfiguration>>> engines = ModelConfigurationList.MapProperty<Engine>(modelConfigurations, ModelConfigurationPropertyAccessors.EngineAccessor);
            List<KeyValuePair<Transmission, List<ModelConfiguration>>> transmissions = ModelConfigurationList.MapProperty<Transmission>(modelConfigurations, ModelConfigurationPropertyAccessors.TransmissionAccessor);
            List<KeyValuePair<DriveTrain, List<ModelConfiguration>>> driveTrains = ModelConfigurationList.MapProperty<DriveTrain>(modelConfigurations, ModelConfigurationPropertyAccessors.DriveTrainAccessor);
            List<KeyValuePair<FuelType, List<ModelConfiguration>>> fuelTypes = ModelConfigurationList.MapProperty<FuelType>(modelConfigurations, ModelConfigurationPropertyAccessors.FuelTypeAccessor);
            List<KeyValuePair<PassengerDoor, List<ModelConfiguration>>> doors = ModelConfigurationList.MapProperty<PassengerDoor>(modelConfigurations, ModelConfigurationPropertyAccessors.PassengerDoorAccessor);

			int rowCount = Math.Max(series.Count, engines.Count);
			rowCount = Math.Max(rowCount, transmissions.Count);
			rowCount = Math.Max(rowCount, driveTrains.Count);
			rowCount = Math.Max(rowCount, fuelTypes.Count);
			rowCount = Math.Max(rowCount, doors.Count);

			Table table = new Table(id, name, tableType, modelConfigurations.ToArray(), rowCount);

			for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
			{
				TableRow row = table.NewRow();

				NewCell(table, row, 0, series);
				NewCell(table, row, 1, engines);
				NewCell(table, row, 2, transmissions);
				NewCell(table, row, 3, driveTrains);
				NewCell(table, row, 4, fuelTypes);
				NewCell(table, row, 5, doors);
			}

			return table;
		}

		private static void NewCell<T>(Table table, TableRow row, int col, IList<KeyValuePair<T, List<ModelConfiguration>>> rows) where T : INameable
		{
			if (row.RowIndex < rows.Count)
			{
				KeyValuePair<T, List<ModelConfiguration>> pair = rows[row.RowIndex];

				table.Initialize(col, row.NewCell(col, pair.Key, Characterize(pair.Value)), pair.Value);
			}
		}

		private static TableCellCharacterization Characterize(IEnumerable<ModelConfiguration> modelConfigurations)
		{
			TableCellCharacterization characterization = TableCellCharacterization.None;

			foreach (ModelConfiguration modelConfiguration in modelConfigurations)
			{
				if (modelConfiguration.IsPartial)
					characterization |= TableCellCharacterization.Secondary;
				else
					characterization |= TableCellCharacterization.Primary;
			}

			return characterization;
		}

		internal event EventHandler<TableSelectionEventArgs<ModelConfiguration>> SelectionChanged;

		private readonly int id;
		private readonly TableName name;
		private readonly List<TableRow> rows = new List<TableRow>();
		private readonly TableColumn[] cols = new TableColumn[]
			{
				new TableColumn(0, "Trim"), 
				new TableColumn(1, "Engine"),
				new TableColumn(2, "Transmission"),
				new TableColumn(3, "Drive Type"),
				new TableColumn(4, "Fuel Type"),
				new TableColumn(5, "Doors"),
			};
		private readonly ModelConfiguration[] modelConfigurations;
		private readonly BitMatrix matrix; // column:Index(TableCell), row:Index(modelConfigurations)
		private readonly BitArray values; // column:Index(modelConfigurations)
		private readonly TableType tableType;
	    private TableSelectionSummary tableSelectionSummary;
		
		private Table(int id, TableName name, TableType tableType, ModelConfiguration[] modelConfigurations, int rowCount)
		{
			values = new BitArray(modelConfigurations.Length, false);
			matrix = new BitMatrix(rowCount*TableRow.ColumnCount, modelConfigurations.Length);
			rows = new List<TableRow>(rowCount);

			this.id = id;
			this.name = name;
			this.tableType = tableType;
			this.modelConfigurations = modelConfigurations;
		}

		public int Id
		{
			get { return id; }
		}

		public TableName Name
		{
			get { return name; }
		}

		public TableType TableType
		{
			get { return tableType; }
		}

		public IList<TableRow> Rows
		{
			get { return rows; }
		}

		public TableColumn[] Columns
		{
			get { return cols; }
		}

		public void DataBind(EventHandler<TableDataBoundEventArgs> handler)
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}

			for (int mRowIndex = 0, mRowCount = matrix.Rows; mRowIndex < mRowCount; mRowIndex++)
			{
				TableDataBoundEventArgs args = new TableDataBoundEventArgs(modelConfigurations[mRowIndex].Id);

				handler(this, args);

				values[mRowIndex] = args.Selected;
			}

			RefreshTableCellState();
		}

		public void GetSelections(EventHandler<TableSelectionEventArgs<ModelConfiguration>> handler)
		{
			if (handler != null)
			{
				for (int mRowIndex = 0, mRowCount = matrix.Rows; mRowIndex < mRowCount; mRowIndex++)
				{
					TableSelectionEventArgs<ModelConfiguration> args = new TableSelectionEventArgs<ModelConfiguration>(modelConfigurations[mRowIndex], values[mRowIndex]);

					handler(this, args);
				}
			}
		}

		public TableSelectionSummary GetSelectionSummary()
		{
            if (tableSelectionSummary == null)
            {
                tableSelectionSummary = new TableSelectionSummary(this); 
            }
			return tableSelectionSummary;
		}

		public void SetState(TableCellState state)
		{
			bool enabled = state == TableCellState.On;

			BitArray changes = new BitArray(matrix.Rows, false);

			for (int mRowIndex = 0, mRowCount = matrix.Rows; mRowIndex < mRowCount; mRowIndex++)
			{
				if (values[mRowIndex] != enabled)
				{
					values[mRowIndex] = enabled;

					changes[mRowIndex] = true;
				}
			}

			RefreshTableCellState();

			if (SelectionChanged != null)
			{
				for (int mRowIndex = 0, mRowCount = matrix.Rows; mRowIndex < mRowCount; mRowIndex++)
				{
					if (changes[mRowIndex])
					{
						TableSelectionEventArgs<ModelConfiguration> args = new TableSelectionEventArgs<ModelConfiguration>(modelConfigurations[mRowIndex], enabled);

						SelectionChanged(this, args);
					}
				}
			}
		}

		internal TableRow NewRow()
		{
			TableRow row = new TableRow(rows.Count, TableCellSelectionHandler);
			rows.Add(row);
			return row;
		}

		private void RefreshTableCellState()
		{
			for (int mColIndex = 0, mColCount = matrix.Columns; mColIndex < mColCount; mColIndex++)
			{
				int tRow = mColIndex / TableRow.ColumnCount;
				int tCol = mColIndex % TableRow.ColumnCount;

				TableCell cell = rows[tRow][tCol];

				if (cell != null)
				{
					cell.RaiseStateChanged(CalculateState(mColIndex));
				}
			}
		}

		private static int Cell(int tRow, int tCol)
		{
			return tRow * TableRow.ColumnCount + tCol;
		}

		internal void TableCellSelectionHandler(object sender, TableSelectionEventArgs<TableCell> args)
		{
			SetBit(args.Value.Row, args.Value.Col, args.Selected);
		}

		internal void Initialize(int col, TableCell cell, List<ModelConfiguration> configurations)
		{
			cols[col].AddCell(cell);

			foreach (ModelConfiguration configuration in configurations)
			{
				for (int mRowIndex = 0, mRowCount = matrix.Rows; mRowIndex < mRowCount; mRowIndex++)
				{
					if (configuration.Equals(modelConfigurations[mRowIndex]))
					{
						matrix[Cell(cell.Row, cell.Col), mRowIndex] = true;

						break;
					}
				}
			}
		}

		internal void SetBit(int tRow, int tCol, bool value)
		{
			for (int mRowIndex = 0, mRowCount = modelConfigurations.Length; mRowIndex < mRowCount; mRowIndex++)
			{
				if (matrix[Cell(tRow,tCol), mRowIndex])
				{
					if (values[mRowIndex] != value)
					{
						values[mRowIndex] = value;

						RaiseModelConfigurationSelectionChanged(mRowIndex, value);
					}
				}
			}
		}

		private TableCellState CalculateState(int mColIndex)
		{
			int configurations = 0, selections = 0;

			for (int mRowIndex = 0, mRowCount = matrix.Rows; mRowIndex < mRowCount; mRowIndex++)
			{
				if (matrix[mColIndex, mRowIndex])
				{
					configurations++;

					if (values[mRowIndex])
					{
						selections++;
					}
				}
			}

			if (configurations > 0)
			{
				if (selections == 0)
				{
					return TableCellState.Off;
				}
				else if (selections < configurations)
				{
					return TableCellState.Implied;
				}
				else
				{
					return TableCellState.On;
				}
			}
			else
			{
				return TableCellState.Off;
			}
		}

		private void RaiseModelConfigurationSelectionChanged(int mRowIndex, bool selected)
		{
			// update cell states

			for (int mColIndex = 0, mColCount = matrix.Columns; mColIndex < mColCount; mColIndex++)
			{
				if (matrix[mColIndex, mRowIndex])
				{
					int tRow = mColIndex/TableRow.ColumnCount;
					int tCol = mColIndex%TableRow.ColumnCount;
					rows[tRow][tCol].RaiseStateChanged(CalculateState(mColIndex));
				}
			}

			// raise event for those who are listening

			if (SelectionChanged != null)
			{
				TableSelectionEventArgs<ModelConfiguration> args = new TableSelectionEventArgs<ModelConfiguration>(modelConfigurations[mRowIndex], selected);

				SelectionChanged(this, args);
			}
		}
	}
}
