using System;
using System.Collections.Generic;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableCollection : List<Table>, IDisposable
	{
		// L1's that cross tables are here and we listen to L2 selections and when all are
		// on the L1 becomes available

		public static TableCollection GetTables(Catalog catalog, VinPattern vinPattern, ModelConfiguration modelConfiguration)
		{
			if (catalog == null)
				throw new ArgumentNullException("catalog");

			if (vinPattern == null)
				throw new ArgumentNullException("vinPattern");

			if (modelConfiguration == null)
				throw new ArgumentNullException("modelConfiguration");

			Graph graph = new Graph(catalog);

			graph.Partition((ModelConfigurationPropertyAccessor<ModelFamily>)ModelConfigurationPropertyAccessors.ModelFamilyAccessor);

			if (modelConfiguration.Model.Segment.IsTruck || modelConfiguration.Model.Segment.IsVan)
			{
				graph.Partition((ModelConfigurationPropertyAccessor<BodyType>)ModelConfigurationPropertyAccessors.BodyTypeAccessor);
			}
			else
			{
				graph.Partition((ModelConfigurationPropertyAccessor<Segment>)ModelConfigurationPropertyAccessors.SegmentAccessor);
			}

			graph.PartialOrder(vinPattern, modelConfiguration);

			graph.Sort(modelConfiguration);

			List<GraphNode> nodes = graph.GatherLeaves();

			TableCollection tables = new TableCollection(nodes.Count);

			int row = 0;

			foreach (GraphNode node in nodes)
			{
				TableType tableType = (row == 0)
				                      	? TableType.CandidateClosure
				                      	: (row == 1)
				                      	  	? TableType.CandidateComplement
				                      	  	: TableType.Spectator;

				Stack<INameable> names = new Stack<INameable>();

				GraphNode parent = node;

				while (!(parent is Graph))
				{
					names.Push(parent.Discriminator);

					parent = parent.Parent;
				}

				ModelFamily modelFamily = names.Pop() as ModelFamily;

				Segment segment = null;

				BodyType bodyType = null;

				if (modelConfiguration.Model.Segment.IsTruck)
				{
					bodyType = names.Pop() as BodyType;
				}
				else
				{
					segment = names.Pop() as Segment;
				}
				
				TableName name = new TableName(modelFamily, segment, bodyType);

				List<ModelConfiguration> configurations = node.ModelConfigurations;

				Table table = Table.NewTable(tables.Count, name, tableType, configurations);

				tables.Add(table);

				table.SelectionChanged += tables.Table_Selection;

				row++;
			}

			return tables;
		}

		public event EventHandler<TableSelectionEventArgs<ModelConfiguration>> SelectionChanged;

		private TableCollection(int capacity) : base(capacity)
		{
		}
		
		private void Table_Selection(object sender, TableSelectionEventArgs<ModelConfiguration> args)
		{
			if (SelectionChanged != null)
			{
				SelectionChanged(sender, args);
			}
		}

		public void DataBind(EventHandler<TableDataBoundEventArgs> handler)
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}

			foreach (Table table in this)
			{
				table.DataBind(handler);
			}
		}

		public void GetSelections(EventHandler<TableSelectionEventArgs<ModelConfiguration>> handler)
		{
			if (handler != null)
			{
				foreach (Table table in this)
				{
					table.GetSelections(handler);
				}
			}
		}

		public void SetTableState(string id, TableCellState state)
		{
			if (state == TableCellState.Implied)
			{
				throw new ArgumentException("Cannot set table to 'Implied' state", "state");
			}

			int index;
			
			if (int.TryParse(id, out index))
			{
				if (index < 0 || index >= Count)
				{
					throw new ArgumentOutOfRangeException("id", index, "Out of bounds");
				}
				else
				{
					this[index].SetState(state);
				}
			}
			else
			{
				throw new ArgumentException("Not an integer", "id");
			}
		}

		#region IDisposable Members

		private bool _disposed;

		public void Dispose()
		{
			Dispose(true);
		}

		private void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				try
				{
					if (disposing)
					{
						foreach (Table table in this)
						{
							table.SelectionChanged -= Table_Selection;
						}

						Clear();
					}
				}
				finally
				{
					_disposed = true;
				}
			}
		}

		#endregion
	}
}
