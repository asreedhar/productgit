using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
    class FacetConfigurationEventArgs : EventArgs
    {
        private readonly int _modelConfiguration;
        private readonly bool _selected;

        public FacetConfigurationEventArgs(int value, bool selected)
        {
            _modelConfiguration = value;
            _selected = selected;
        }

        public int ModelConfiguration
        {
            get { return _modelConfiguration; }
        }

        public bool Selected
        {
            get { return _selected;  }
        }
    }
}
