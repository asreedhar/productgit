using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableColumn
	{
		private readonly List<TableCell> cells = new List<TableCell>();
		private readonly int columnIndex;
		private readonly string name;

		public TableColumn(int columnIndex, string name)
		{
			this.columnIndex = columnIndex;
			this.name = name;
		}

		public string Name
		{
			get { return name; }
		}

		public int ColumnIndex
		{
			get { return columnIndex; }
		}

		public ReadOnlyCollection<TableCell> Cells
		{
			get { return cells.AsReadOnly(); }
		}

		internal void AddCell(TableCell cell)
		{
			cells.Add(cell);
		}
	}
}
