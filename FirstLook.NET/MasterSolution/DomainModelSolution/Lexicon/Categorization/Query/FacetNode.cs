using System;
using System.Collections.Generic;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
    public class FacetNode
    {
        public FacetNode(IList<ModelConfiguration> modelConfigurations)
        {
            _modelConfigurations = modelConfigurations;

            HandleModelConfigurationChange();
        }

        #region Properties

        private readonly Dictionary<Type, List<INameable>> _lists = new Dictionary<Type, List<INameable>>();

        private readonly IList<ModelConfiguration> _modelConfigurations;

        private readonly Dictionary<INameable, List<ModelConfiguration>> _typeModelConfigurations = new Dictionary<INameable, List<ModelConfiguration>>();

        private readonly Dictionary<ModelConfiguration, List<INameable>> _modelConfigurationNameables = new Dictionary<ModelConfiguration, List<INameable>>();

        public List<INameable> this[Type type]
        {
            get
            {
                if (_lists.ContainsKey(type))
                {
                    return _lists[type];
                }

                List<INameable> list = new List<INameable>();

                _lists[type] = list;

                return list;
            }
        }

        public IList<ModelConfiguration> this[INameable nameable]
        {
            get
            {
                if (_typeModelConfigurations.ContainsKey(nameable))
                {
                    return _typeModelConfigurations[nameable];
                }

                List<ModelConfiguration> list = new List<ModelConfiguration>();

                _typeModelConfigurations[nameable] = list;

                return list;
            }
        }

        public IList<INameable> this[ModelConfiguration modelConfiguration]
        {
            get
            {
                if (_modelConfigurationNameables.ContainsKey(modelConfiguration))
                {
                    return _modelConfigurationNameables[modelConfiguration];
                }

                List<INameable> list = new List<INameable>();

                _modelConfigurationNameables[modelConfiguration] = list;

                return list;
            }
        }

        public IList<ModelConfiguration> ModelConfigurations
        {
            get { return _modelConfigurations; }
        }

        #endregion

        private void HandleModelConfigurationChange()
        {
            List<KeyValuePair<ModelFamily, List<ModelConfiguration>>> modelFamilies = ModelConfigurationList.MapProperty<ModelFamily>(ModelConfigurations, ModelConfigurationPropertyAccessors.ModelFamilyAccessor);
            List<KeyValuePair<BodyType, List<ModelConfiguration>>> bodyTypes = ModelConfigurationList.MapProperty<BodyType>(ModelConfigurations, ModelConfigurationPropertyAccessors.BodyTypeAccessor);
            List<KeyValuePair<Series, List<ModelConfiguration>>> series = ModelConfigurationList.MapProperty<Series>(ModelConfigurations, ModelConfigurationPropertyAccessors.SeriesAccessor);
            List<KeyValuePair<Engine, List<ModelConfiguration>>> engines = ModelConfigurationList.MapProperty<Engine>(ModelConfigurations, ModelConfigurationPropertyAccessors.EngineAccessor);
            List<KeyValuePair<Transmission, List<ModelConfiguration>>> transmissions = ModelConfigurationList.MapProperty<Transmission>(ModelConfigurations, ModelConfigurationPropertyAccessors.TransmissionAccessor);
            List<KeyValuePair<DriveTrain, List<ModelConfiguration>>> driveTrains = ModelConfigurationList.MapProperty<DriveTrain>(ModelConfigurations, ModelConfigurationPropertyAccessors.DriveTrainAccessor);
            List<KeyValuePair<FuelType, List<ModelConfiguration>>> fuelTypes = ModelConfigurationList.MapProperty<FuelType>(ModelConfigurations, ModelConfigurationPropertyAccessors.FuelTypeAccessor);
            List<KeyValuePair<PassengerDoor, List<ModelConfiguration>>> doors = ModelConfigurationList.MapProperty<PassengerDoor>(ModelConfigurations, ModelConfigurationPropertyAccessors.PassengerDoorAccessor);
            List<KeyValuePair<Segment, List<ModelConfiguration>>> segments = ModelConfigurationList.MapProperty<Segment>(ModelConfigurations, ModelConfigurationPropertyAccessors.SegmentAccessor);

            foreach (KeyValuePair<ModelFamily, List<ModelConfiguration>> list in modelFamilies)
            {
                this[typeof(ModelFamily)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<BodyType, List<ModelConfiguration>> list in bodyTypes)
            {
                this[typeof(BodyType)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<Series, List<ModelConfiguration>> list in series)
            {
                this[typeof(Series)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<Engine, List<ModelConfiguration>> list in engines)
            {
                this[typeof(Engine)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<Transmission, List<ModelConfiguration>> list in transmissions)
            {
                this[typeof(Transmission)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<DriveTrain, List<ModelConfiguration>> list in driveTrains)
            {
                this[typeof(DriveTrain)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<FuelType, List<ModelConfiguration>> list in fuelTypes)
            {
                this[typeof(FuelType)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<PassengerDoor, List<ModelConfiguration>> list in doors)
            {
                this[typeof(PassengerDoor)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }

            foreach (KeyValuePair<Segment, List<ModelConfiguration>> list in segments)
            {
                this[typeof(Segment)].Add(list.Key);
                AddTypeModelConfiguration(list.Key, list.Value);
            }
        }

        private void AddTypeModelConfiguration(INameable nameable, IEnumerable<ModelConfiguration> modelConfigurations)
        {
            foreach (ModelConfiguration mc in modelConfigurations)
            {
                this[nameable].Add(mc);
                this[mc].Add(nameable);
            }
        }
    }
}