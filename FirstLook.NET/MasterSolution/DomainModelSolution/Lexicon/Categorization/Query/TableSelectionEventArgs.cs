using System;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
	public class TableSelectionEventArgs<T> : EventArgs
	{
		private readonly T value;
		private readonly bool selected;

		public TableSelectionEventArgs(T value, bool selected)
		{
			this.value = value;
			this.selected = selected;
		}

		public T Value
		{
			get { return value; }
		}

		public bool Selected
		{
			get { return selected; }
		}
	}
}
