﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace FirstLook.DomainModel.Lexicon.Categorization.Query
{
    class FacetSelectionEventArgs : CancelEventArgs
    {
        public FacetSelectionEventArgs(Dictionary<Type, IList<INameable>> possibleAttributes, INameable attribute, bool selected, Type type)
        {
            _possibleAttributes = possibleAttributes;
            _attribute = attribute;
            _selected = selected;
            _type = type;
        }

        private readonly Dictionary<Type, IList<INameable>> _possibleAttributes;
        private readonly INameable _attribute;
        private readonly bool _selected;
        private readonly Type _type;

        public FacetSelectionEventArgs(bool cancel, Dictionary<Type, IList<INameable>> possibleAttributes, INameable attribute, bool selected, Type type) : base(cancel)
        {
            _possibleAttributes = possibleAttributes;
            _attribute = attribute;
            _selected = selected;
            _type = type;
        }

        public Dictionary<Type, IList<INameable>> PossibleAttributes
        {
            get { return _possibleAttributes; }
        }

        public INameable Attribute
        {
            get { return _attribute; }
        }

        public bool Selected
        {
            get { return _selected; }
        }

        public Type Type
        {
            get { return _type; }
        }
    }
}
