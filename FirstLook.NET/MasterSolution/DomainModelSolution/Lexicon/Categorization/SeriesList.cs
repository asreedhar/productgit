using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class SeriesList : ReadOnlyListBase<SeriesList, Series>, IExplicitlySerializable
	{
		#region Business Methods

		public Series GetItem(int id)
		{
			foreach (Series series in this)
				if (series.Id == id)
					return series;
			return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }    

		#endregion

		#region Factory Methods

		internal static SeriesList GetSeries(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new SeriesList(reader);
		}

		private SeriesList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Series.GetSeries(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal SeriesList(SerializationData data)
		{
			IEnumerable<Series> items = data.GetValues<Series>("Items");
			IEnumerator<Series> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
