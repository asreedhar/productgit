using System;
using System.Collections;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class ModelFamily : ReadOnlyBase<ModelFamily>, INameable<ModelFamily>, IExplicitlySerializable
	{
		#region Business Methods

		private int    _id;   // Model family id.
		private string _name; // Model family name.

		public int Id
		{
			get { return _id; }
		}

		public string Name
		{
			get { return _name; }
		}

		public ICollection GetObjectState()
		{
			return new object[] { _id, _name };
		}

		protected override object GetIdValue()
		{
			return _id;
		}

		#endregion

		#region Factory Methods

        /// <summary>
        /// Create a model family from values retrieved from the database.
        /// </summary>
        /// <param name="record">Model family retrieved from the database.</param>
        /// <returns>A new model family object.</returns>
		internal static ModelFamily GetModelFamily(IDataRecord record)
		{
			return new ModelFamily(record);
		}

        /// <summary>
        /// Private constructor to force creation to go through factory methods.
        /// </summary>
        /// <param name="record">Model family values retrieved from the database.</param>
		private ModelFamily(IDataRecord record)
		{
			Fetch(record);
		}

        private ModelFamily()
        {
            /* csla needs parameterless constructor */
        }

		#endregion

		#region Data Access

        /// <summary>
        /// Populate this model family with values retrieved from the database.
        /// </summary>
        /// <param name="record">Model family values retrieved from the database.</param>
		private void Fetch(IDataRecord record)
		{
			_id = record.GetInt32(record.GetOrdinal("Id"));
			_name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationName = "N";

        /// <summary>
        /// Add this objects values to the serialization data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, _id);
			data.AddValue(SerializationName, _name);
		}

        /// <summary>
        /// Create a make object from serialized data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		internal ModelFamily(SerializationData data)
		{
			_id = data.GetInt32(SerializationId);
			_name = data.GetString(SerializationName);
		}

		#endregion
	}
}
