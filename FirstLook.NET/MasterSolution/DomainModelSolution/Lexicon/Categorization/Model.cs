using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class Model : ReadOnlyBase<Model>, INameable<Model>, IExplicitlySerializable
	{
		#region Business Methods

		[NonSerialized]
		private Catalog catalog;
		private int id;
		private int? segmentId;
		private int? bodyTypeId;
		private int modelFamilyId;
		private int? seriesId;

		public int Id
		{
			get { return id; }
		}

		public Make Make
		{
			get
			{
				return catalog.Make;
			}
		}

		public Line Line
		{
			get
			{
				return catalog.Line;
			}
		}

		public Series Series
		{
			get
			{
				if (seriesId.HasValue)
					return catalog.Series.GetItem(seriesId.Value);
				return null;
			}
		}

		public bool IsPartial
		{
			get
			{
                return (!seriesId.HasValue || !bodyTypeId.HasValue || !segmentId.HasValue);
			}
		}

		public BodyType BodyType
		{
			get
			{
				if (bodyTypeId.HasValue)
					return catalog.BodyTypes.GetItem(bodyTypeId.Value);
				return null;
			}
		}

		public Segment Segment
		{
			get
			{
                if (segmentId.HasValue)
                    return catalog.Segments.GetItem(segmentId.Value);
                return null;
			}
		}

		public IList<Configuration> Configurations
		{
			get
			{
				return catalog.GetConfigurations(this);
			}
		}

		public ModelFamily ModelFamily
		{
			get
			{
				return catalog.ModelFamilies.GetItem(modelFamilyId);
			}
		}

		public string Name
		{
			get
			{
			    string text = (segmentId.HasValue)
			                      ? ((Segment.IsTruck || Segment.IsVan)
			                             ? (bodyTypeId.HasValue ? BodyType.Name : "Unknown")
			                             : Segment.Name)
			                      : "Unknown";

				if (seriesId.HasValue)
				{
					return string.Format(CultureInfo.InvariantCulture, "{0} {1} {2} {3}", Make.Name, ModelFamily.Name, text, Series.Name);
				}

				return string.Format(CultureInfo.InvariantCulture, "{0} {1} {2}", Make.Name, ModelFamily.Name, text);
			}
		}

        public Presentation ToPresentation()
        {
            return new Presentation(this);
        }

		public ICollection<ModelConfiguration> GetModelConfigurations()
		{
			return catalog.GetModelConfigurations(this);
		}

		public ICollection<Configuration> GetConfigurations(VinPattern vinPattern)
		{
			return catalog.GetConfigurations(this, vinPattern);
		}

		protected override object GetIdValue()
		{
			return id;
		}

		internal void SetCatalog(Catalog catalog)
		{
			this.catalog = catalog;
		}

		#endregion

		#region Factory Methods

		internal static Model GetModel(Catalog catalog, IDataRecord record)
		{
			return new Model(catalog, record);
		}

		private Model(Catalog catalog, IDataRecord record)
		{
			this.catalog = catalog;

			Fetch(record);
		}

		#endregion

		#region Data Access
		
		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			bodyTypeId = GetInt32(record, "BodyTypeId");
            segmentId = GetInt32(record, "SegmentId");
			modelFamilyId = record.GetInt32(record.GetOrdinal("ModelFamilyId"));
			seriesId = GetInt32(record, "SeriesId");
		}

		private static int? GetInt32(IDataRecord record, string column)
		{
			int ordinal = record.GetOrdinal(column);
			if (record.IsDBNull(ordinal))
				return null;
			return record.GetInt32(ordinal);
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValue("Id", id);
			data.AddValue("SegmentId", segmentId);
			data.AddValue("BodyTypeId", bodyTypeId);
			data.AddValue("ModelFamilyId", modelFamilyId);
			data.AddValue("SeriesId", seriesId);
		}

		internal Model(SerializationData data)
		{
			id = data.GetInt32("Id");
            segmentId = data.GetNullable<int>("SegmentId");
			bodyTypeId = data.GetNullable<int>("BodyTypeId");
			modelFamilyId = data.GetInt32("ModelFamilyId");
			seriesId = data.GetNullable<int>("SeriesId");
		}

		#endregion

        public class Presentation
        {
            private readonly string _make;
            private readonly string _line;
            private readonly string _modelFamily;
            private readonly string _series;
            private readonly string _segment;
            private readonly string _bodyType;

            public Presentation(Model model)
            {
                _make = model.Make.Name;
                _line = model.Line.Name;
                _modelFamily = model.ModelFamily.Name;
                _series = (model.seriesId.HasValue) ? model.Series.Name : string.Empty;
                _segment = (model.segmentId.HasValue) ? model.Segment.Name : string.Empty;
                _bodyType = (model.bodyTypeId.HasValue) ? model.BodyType.Name : string.Empty; ;
            }

            public string Make
            {
                get { return _make; }
            }

            public string Line
            {
                get { return _line; }
            }

            public string ModelFamily
            {
                get { return _modelFamily; }
            }

            public string Series
            {
                get { return _series; }
            }

            public string Segment
            {
                get { return _segment; }
            }

            public string BodyType
            {
                get { return _bodyType; }
            }
        }
	}
}
