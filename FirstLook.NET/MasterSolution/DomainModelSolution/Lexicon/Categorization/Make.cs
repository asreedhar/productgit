using System;
using System.Collections;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{   
	[Serializable]
	public class Make : ReadOnlyBase<Make>, INameable<Make>, IExplicitlySerializable
	{
		#region Business Methods

		private int    _id;   // Id of the make.
		private string _name; // Name of the make.

		public int Id
		{
			get { return _id; }
		}

		public string Name
		{
			get { return _name; }
		}

		public ICollection GetObjectState()
		{
			return new object[] { _id, _name };
		}

		protected override object GetIdValue()
		{
			return _id;
		}

		#endregion

		#region Factory Methods

        /// <summary>
        /// Factory method for creating a make. Forces a make to be created with database values.
        /// </summary>
        /// <param name="record">Make values retrieved from the database.</param>
        /// <returns>A new make object.</returns>
		internal static Make GetMake(IDataRecord record)
		{
			return new Make(record);
		}

        /// <summary>
        /// Private constructor to force object creation to go through factory methods.
        /// </summary>
        /// <param name="record">Make values retrieved from the database.</param>
		private Make(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

        /// <summary>
        /// Populate this make object with database values.
        /// </summary>
        /// <param name="record">Make values retrieved from the database.</param>
		private void Fetch(IDataRecord record)
		{
			_id = record.GetInt32(record.GetOrdinal("Id"));
			_name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationName = "N";

        /// <summary>
        /// Add this objects values to the serialization data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, _id);
			data.AddValue(SerializationName, _name);
		}

        /// <summary>
        /// Create a make object from serialized data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		internal Make(SerializationData data)
		{
			_id = data.GetInt32(SerializationId);
			_name = data.GetString(SerializationName);
		}

		#endregion
	}
}
