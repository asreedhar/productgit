using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class Segment : ReadOnlyBase<Segment>, INameable<Segment>, IExplicitlySerializable
	{
		#region Business Methods

		private int    _id;   // Id of the vehicle segment.
		private string _name; // Vehicle segment name.

		public int Id
		{
			get { return _id; }
		}

		public string Name
		{
			get { return _name; }
		}

	    public bool IsTruck
	    {
	        get
	        {
	            return Id == 2;
	        }
	    }

		public bool IsVan
		{
			get
			{
				return Id == 5;
			}
		}

		protected override object GetIdValue()
		{
			return _id;
		}

		#endregion

		#region Factory Methods

        /// <summary>
        /// Factory method for creating a segment. Forces a segment to be created with database values.
        /// </summary>
        /// <param name="record">Segment values retrieved from the database.</param>
        /// <returns>A new segment object.</returns>
		internal static Segment GetSegment(IDataRecord record)
		{
			return new Segment(record);
		}

        /// <summary>
        /// Private constructor to force object creation to go through factory methods.
        /// </summary>
        /// <param name="record">Segment values retrieved from the database.</param>
		private Segment(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

        /// <summary>
        /// Populate this segment object with database values.
        /// </summary>
        /// <param name="record"></param>
		private void Fetch(IDataRecord record)
		{
			_id = record.GetInt32(record.GetOrdinal("Id"));
			_name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationName = "N";

        /// <summary>
        /// Add this objects values to the serialization data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, _id);
			data.AddValue(SerializationName, _name);
		}

        /// <summary>
        /// Create a segment object from serialized data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		internal Segment(SerializationData data)
		{
			_id = data.GetInt32(SerializationId);
			_name = data.GetString(SerializationName);
		}

		#endregion
	}
}
