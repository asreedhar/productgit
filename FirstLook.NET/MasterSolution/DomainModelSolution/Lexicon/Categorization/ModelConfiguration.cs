using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class ModelConfiguration : ReadOnlyBase<ModelConfiguration>, IExplicitlySerializable
	{
		#region Business Methods

		[NonSerialized]
		private Catalog catalog;
		private int id;
		private int modelId;
		private int configurationId;

		public int Id
		{
			get { return id; }
		}

		public Model Model
		{
			get
			{
				return catalog.Models.GetItem(modelId);
			}
		}

		public Configuration Configuration
		{
			get
			{
				return catalog.Configurations.GetItem(configurationId);
			}
		}

		public bool IsPartial
		{
			get
			{
				return Model.IsPartial || Configuration.IsPartial;
			}
		}

		public ICollection<VinPattern> GetVinPatterns()
		{
			return catalog.GetVinPatterns(this);
		}

		public string Name
		{
			get
			{
				return string.Format(CultureInfo.InvariantCulture, "{0} {1}", Model.Name, Configuration.Name);
			}
		}

		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "{{{0} {1}}}", Id, Name);
		}

		internal void SetCatalog(Catalog catalog)
		{
			this.catalog = catalog;
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static ModelConfiguration GetModelConfiguration(Catalog catalog, IDataRecord record)
		{
			return new ModelConfiguration(catalog, record);
		}

		private ModelConfiguration(Catalog catalog, IDataRecord record)
		{
			this.catalog = catalog;

			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			modelId = record.GetInt32(record.GetOrdinal("ModelId"));
			configurationId = record.GetInt32(record.GetOrdinal("ConfigurationId"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationModelId = "M";
		private const string SerializationModelConfigurationId = "C";

		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, id);
			data.AddValue(SerializationModelId, modelId);
			data.AddValue(SerializationModelConfigurationId, configurationId);
		}

		internal ModelConfiguration(SerializationData data)
		{
			id = data.GetInt32(SerializationId);
			modelId = data.GetInt32(SerializationModelId);
			configurationId = data.GetInt32(SerializationModelConfigurationId);
		}

		#endregion
	}
}
