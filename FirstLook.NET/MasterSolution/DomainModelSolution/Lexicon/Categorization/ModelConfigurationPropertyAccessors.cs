namespace FirstLook.DomainModel.Lexicon.Categorization
{
	public static class ModelConfigurationPropertyAccessors
	{
		public static ModelFamily ModelFamilyAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Model.ModelFamily;
		}

		public static Segment SegmentAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Model.Segment;
		}

		public static BodyType BodyTypeAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Model.BodyType;
		}

		public static Model ModelAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Model;
		}

		public static Series SeriesAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Model.Series;
		}

		public static Engine EngineAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Configuration.Engine;
		}

		public static FuelType FuelTypeAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Configuration.FuelType;
		}

		public static DriveTrain DriveTrainAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Configuration.DriveTrain;
		}

		public static Transmission TransmissionAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Configuration.Transmission;
		}

		public static PassengerDoor PassengerDoorAccessor(ModelConfiguration modelConfiguration)
		{
			return modelConfiguration.Configuration.PassengerDoor;
		}
	}
}