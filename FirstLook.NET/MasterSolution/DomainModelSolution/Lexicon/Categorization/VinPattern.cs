using System;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class VinPattern : ReadOnlyBase<VinPattern>, IExplicitlySerializable
	{
		#region Business Methods

		[NonSerialized]
		private Catalog catalog;
		private int id;
		private string text;
		private int modelConfigurationId;

		public int Id
		{
			get { return id; }
		}

		public string Text
		{
			get { return text; }
		}

		public ModelConfiguration ModelConfiguration
		{
			get
			{
				return catalog.ModelConfigurations.GetItem(modelConfigurationId);
			}
		}

		public ICollection<ModelConfiguration> GetModelConfigurations()
		{
			return catalog.GetModelConfigurations(this);
		}

		protected override object GetIdValue()
		{
			return text;
		}

		internal void SetCatalog(Catalog catalog)
		{
			this.catalog = catalog;
		}

		#endregion

		#region Factory Methods

		internal static VinPattern GetVinPattern(Catalog catalog, IDataRecord record)
		{
			return new VinPattern(catalog, record);
		}

		private VinPattern(Catalog catalog, IDataRecord record)
		{
			this.catalog = catalog;
			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			text = record.GetString(record.GetOrdinal("Text"));
			modelConfigurationId = record.GetInt32(record.GetOrdinal("ModelConfigurationId"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationText = "T";
		private const string SerializationModelConfiguration = "M";

		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, id);
			data.AddValue(SerializationText, text);
			data.AddValue(SerializationModelConfiguration, modelConfigurationId);
		}

		internal VinPattern(SerializationData data)
		{
			id = data.GetInt32(SerializationId);
			text = data.GetString(SerializationText);
			modelConfigurationId = data.GetInt32(SerializationModelConfiguration);
		}

		#endregion
	}
}
