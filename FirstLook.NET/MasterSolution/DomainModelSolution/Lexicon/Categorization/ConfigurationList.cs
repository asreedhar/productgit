using System;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class ConfigurationList : ReadOnlyListBase<ConfigurationList, Configuration>, IExplicitlySerializable
	{
		#region Business Methods

		public Configuration GetItem(int id)
		{
			foreach (Configuration configuration in this)
				if (configuration.Id == id)
					return configuration;
			return null;
		}

        public IList<Configuration.Presentation> ToPresentation()
        {
            List<Configuration.Presentation> items = new List<Configuration.Presentation>();
            foreach (Configuration item in this)
            {
                items.Add(item.ToPresentation());
            }
            return items;
        }

		#endregion

		#region Factory Methods

		internal static ConfigurationList GetConfigurations(Catalog catalog, IDataReader reader)
		{
			if (catalog == null)
				throw new ArgumentNullException("catalog");
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new ConfigurationList(catalog, reader);
		}

		private ConfigurationList(Catalog catalog, IDataReader reader)
		{
			Fetch(catalog, reader);
		}

		#endregion

		#region Data Access

		private void Fetch(Catalog catalog, IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Configuration.GetConfiguration(catalog, reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal ConfigurationList(SerializationData data)
		{
			IEnumerable<Configuration> items = data.GetValues<Configuration>("Items");
			IEnumerator<Configuration> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
