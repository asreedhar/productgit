using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class TransmissionList : ReadOnlyListBase<TransmissionList, Transmission>, IExplicitlySerializable
	{
		#region Business Methods

		public Transmission GetItem(int id)
		{
			foreach (Transmission transmission in this)
				if (transmission.Id == id)
					return transmission;
			return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }    

		#endregion

		#region Factory Methods

		internal static TransmissionList GetTransmissions(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new TransmissionList(reader);
		}

		private TransmissionList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Transmission.GetTransmission(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal TransmissionList(SerializationData data)
		{
			IEnumerable<Transmission> items = data.GetValues<Transmission>("Items");
			IEnumerator<Transmission> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
