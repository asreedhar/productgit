using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class DriveTrainList : ReadOnlyListBase<DriveTrainList, DriveTrain>, IExplicitlySerializable
	{
		#region Business Methods

		public DriveTrain GetItem(int id)
		{
			foreach (DriveTrain driveTrain in this)
				if (driveTrain.Id == id)
					return driveTrain;
			return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }    

		#endregion

		#region Factory Methods

		internal static DriveTrainList GetDriveTrains(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new DriveTrainList(reader);
		}

		private DriveTrainList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(DriveTrain.GetDriveTrain(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal DriveTrainList(SerializationData data)
		{
			IEnumerable<DriveTrain> items = data.GetValues<DriveTrain>("Items");
			IEnumerator<DriveTrain> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
