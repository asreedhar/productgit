using System;
using System.Collections.Generic;
using System.Data;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	internal class ModelConfigurationMappingList : List<ModelConfigurationMapping>, IExplicitlySerializable
	{
		#region Factory Methods

		internal static ModelConfigurationMappingList GetModelConfigurationMappings(IDataReader reader)
		{
			return new ModelConfigurationMappingList(reader);
		}

		private ModelConfigurationMappingList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			while (reader.Read())
			{
				Add(ModelConfigurationMapping.GetModelConfigurationMapping(reader));
			}
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", this);
		}

		internal ModelConfigurationMappingList(SerializationData data)
		{
			IEnumerable<ModelConfigurationMapping> items = data.GetValues<ModelConfigurationMapping>("Items");
			IEnumerator<ModelConfigurationMapping> en = items.GetEnumerator();
			while (en.MoveNext())
			{
				Add(en.Current);
			}
		}

		#endregion
	}
}
