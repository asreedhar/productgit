using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
    [Serializable]
    public class LineList : ReadOnlyListBase<LineList, Line>
    {
        #region Business Methods

        public Line GetItem(int id)
        {
            foreach (Line line in this)
                if (line.Id == id)
                    return line;
            return null;
        }

        #endregion

        #region Factory Methods

        internal static LineList GetLines(IDataReader reader)
        {
            return new LineList(reader);
        }

        public static LineList GetLines(int modelYear, int makeId)
        {
            return DataPortal.Fetch<LineList>(new Criteria(modelYear, makeId));
        }

        [Serializable]
        private class Criteria
        {
            private readonly int _modelYear;
            private readonly int _makeId;

            public Criteria(int modelYear, int makeId)
            {
                _modelYear = modelYear;
                _makeId = makeId;
            }

            public int ModelYear
            {
                get { return _modelYear; }
            }

            public int MakeId
            {
                get { return _makeId; }
            }
        }

        public class DataSource
        {
            public LineList GetLines(int modelYear, int makeId)
            {
                return LineList.GetLines(modelYear, makeId);
            }
        }

        private LineList()
        {
            /* force use of factory methods */
        }

        private LineList(IDataReader reader)
        {
            Fetch(reader);
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(Criteria criteria)
        {
            Fetch(criteria.ModelYear, criteria.MakeId);
        }

        private void Fetch(int modelYear, int makeId)
        {
            using (IDbConnection connection = Database.CreateConnection(Database.CategorizationDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Categorization.LineList#Fetch";

                    Database.AddWithValue(command, "ModelYear", modelYear, DbType.Int32);
                    Database.AddWithValue(command, "MakeId", makeId, DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        private void Fetch(IDataReader reader)
        {
            IsReadOnly = false;
            while (reader.Read())
            {
                Add(Line.GetLine(reader));
            }
            IsReadOnly = true;
        }

        #endregion
    }
}