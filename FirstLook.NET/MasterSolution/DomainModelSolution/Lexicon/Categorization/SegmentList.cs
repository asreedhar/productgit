using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class SegmentList : ReadOnlyListBase<SegmentList, Segment>, IExplicitlySerializable
	{
		#region Business Methods

        /// <summary>
        /// Get a segment object for a given id.
        /// </summary>
        /// <param name="id">Id of the segment to retrieve.</param>
        /// <returns>A segment object.</returns>
		public Segment GetItem(int id)
		{
            foreach (Segment segment in this)
            {
                if (segment.Id == id)
                {
                    return segment;
                }
            }
            return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }    

		#endregion

		#region Factory Methods

        /// <summary>
        /// Get a list of segments from database values.
        /// </summary>
        /// <param name="reader">List of segments retrieved from the database.</param>
        /// <returns>A list of segment objects.</returns>
		internal static SegmentList GetSegments(IDataReader reader)
		{
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            return new SegmentList(reader);
		}

        /// <summary>
        /// Get a list of segments. Will hand off execution to the data portal.
        /// </summary>        
        /// <returns>A list of segments.</returns>
        public static SegmentList GetSegments()
        {
            return DataPortal.Fetch<SegmentList>();
        }

        /// <summary>
        /// Data source for retrieving a list of makes from the database.
        /// </summary>
        public class DataSource
        {
            /// <summary>
            /// Get all segments in the database.
            /// </summary>            
            /// <returns>A list of segments.</returns>
            public SegmentList GetSegments()
            {
                return SegmentList.GetSegments();
            }
        }

        /// <summary>
        /// Private constructor to force object creation to go through factory methods.
        /// </summary>
        /// <param name="reader">Segment list values retrieved from the database.</param>
		private SegmentList(IDataReader reader)
		{
			Fetch(reader);
		}

        /// <summary>
        /// Private constructor to force object creation to go through factory methods.
        /// </summary>
        private SegmentList()
        {            
        }

		#endregion

		#region Data Access

        /// <summary>
        /// Fetch a list of makes from values retrieved from the database.
        /// </summary>        
        private void DataPortal_Fetch()
        {
            Fetch();
        }

        /// <summary>
        /// Populate this segment list with segments retrieved from database values.
        /// </summary>
        private void Fetch()
        {
            using (IDbConnection connection = Database.CreateConnection(Database.CategorizationDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Categorization.SegmentList#Fetch";                    

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }            
        }

        /// <summary>
        /// Populate this segment list with segments retrieved from database values.
        /// </summary>
        /// <param name="reader">Segment list values retrieved from the database.</param>
		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Segment.GetSegment(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

        /// <summary>
        /// Add this objects values to the serialization data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

        /// <summary>
        /// Create a list of segment objects from serialized data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		internal SegmentList(SerializationData data)
		{
			IEnumerable<Segment> items = data.GetValues<Segment>("Items");
			IEnumerator<Segment> en = items.GetEnumerator();
			
            IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
