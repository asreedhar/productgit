using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class FuelTypeList : ReadOnlyListBase<FuelTypeList, FuelType>, IExplicitlySerializable
	{
		#region Business Methods

		public FuelType GetItem(int id)
		{
			foreach (FuelType fuelType in this)
				if (fuelType.Id == id)
					return fuelType;
			return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }    

		#endregion

		#region Factory Methods

		internal static FuelTypeList GetFuelTypes(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new FuelTypeList(reader);
		}

		private FuelTypeList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(FuelType.GetFuelType(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal FuelTypeList(SerializationData data)
		{
			IEnumerable<FuelType> items = data.GetValues<FuelType>("Items");
			IEnumerator<FuelType> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
