using System;
using System.Data;
using System.Globalization;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class Configuration : ReadOnlyBase<Configuration>, IExplicitlySerializable
	{
		#region Business Methods

		[NonSerialized]
		private Catalog catalog;
		private int id;
		private int? passengerDoorId;
		private int? fuelTypeId;
		private int? engineId;
		private int? driveTrainId;
		private int? transmissionId;

		public int Id
		{
			get { return id; }
		}

		public ModelYear ModelYear
		{
			get
			{
				return catalog.ModelYear;
			}
		}

		public PassengerDoor PassengerDoor
		{
			get
			{
				if (passengerDoorId.HasValue)
					return catalog.PassengerDoors.GetItem(passengerDoorId.Value);
				return null;
			}
		}

		public FuelType FuelType
		{
			get
			{
				if (fuelTypeId.HasValue)
					return catalog.FuelTypes.GetItem(fuelTypeId.Value);
				return null;
			}
		}

		public Engine Engine
		{
			get
			{
				if (engineId.HasValue)
					return catalog.Engines.GetItem(engineId.Value);
				return null;
			}
		}

		public DriveTrain DriveTrain
		{
			get
			{
				if (driveTrainId.HasValue)
					return catalog.DriveTrains.GetItem(driveTrainId.Value);
				return null;
			}
		}

		public Transmission Transmission
		{
			get
			{
				if (transmissionId.HasValue)
					return catalog.Transmissions.GetItem(transmissionId.Value);
				return null;
			}
		}

		public bool IsPartial
		{
			get
			{
				return !(passengerDoorId.HasValue ||
				         fuelTypeId.HasValue ||
				         engineId.HasValue ||
				         driveTrainId.HasValue ||
				         transmissionId.HasValue);
			}
		}

		public string Name
		{
			get
			{
				return string.Format(
					CultureInfo.InvariantCulture,
					"{0} {1} {2} {3} {4}",
					passengerDoorId.HasValue ? PassengerDoor.Name : "-",
					fuelTypeId.HasValue ? FuelType.Name : "-",
					engineId.HasValue ? Engine.Name : "-",
					driveTrainId.HasValue ? DriveTrain.Name : "-",
					transmissionId.HasValue ? Transmission.Name : "-");
			}
		}

        public Presentation ToPresentation()
        {
            return new Presentation(this);
        }

		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "{{{0} {1}}}", Id, Name);
		}

		protected override object GetIdValue()
		{
			return id;
		}

		internal void SetCatalog(Catalog catalog)
		{
			this.catalog = catalog;
		}

		#endregion

		#region Factory Methods

		internal static Configuration GetConfiguration(Catalog catalog, IDataRecord reader)
		{
			return new Configuration(catalog, reader);
		}

		private Configuration(Catalog catalog, IDataRecord reader)
		{
			this.catalog = catalog;

			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			passengerDoorId = GetInt32(record, "PassengerDoorId");
			fuelTypeId = GetInt32(record, "FuelTypeId");
			engineId = GetInt32(record, "EngineId");
			driveTrainId = GetInt32(record, "DriveTrainId");
			transmissionId = GetInt32(record, "TransmissionId");
		}

		private static int? GetInt32(IDataRecord record, string column)
		{
			int ordinal = record.GetOrdinal(column);
			if (record.IsDBNull(ordinal))
				return null;
			return record.GetInt32(ordinal);
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationDoor = "P";
		private const string SerializationFuelType = "F";
		private const string SerializationEngine = "E";
		private const string SerializationDriveTrain = "D";
		private const string SerializationTransmission = "T";

		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, id);
			data.AddValue(SerializationDoor, passengerDoorId);
			data.AddValue(SerializationFuelType, fuelTypeId);
			data.AddValue(SerializationEngine, engineId);
			data.AddValue(SerializationDriveTrain, driveTrainId);
			data.AddValue(SerializationTransmission, transmissionId);
		}

		internal Configuration(SerializationData data)
		{
			id = data.GetInt32(SerializationId);
			passengerDoorId = data.GetNullable<int>(SerializationDoor);
			fuelTypeId = data.GetNullable<int>(SerializationFuelType);
			engineId = data.GetNullable<int>(SerializationEngine);
			driveTrainId = data.GetNullable<int>(SerializationDriveTrain);
			transmissionId = data.GetNullable<int>(SerializationTransmission);
		}

		#endregion

        public class Presentation
        {
            private readonly string _passengerDoor;
            private readonly string _fuelType;
            private readonly string _engine;
            private readonly string _driveTrain;
            private readonly string _transmission;

            public Presentation(Configuration configuration)
            {
                _passengerDoor = (configuration.passengerDoorId.HasValue) ? configuration.PassengerDoor.Name : string.Empty;
                _fuelType = (configuration.fuelTypeId.HasValue) ? configuration.FuelType.Name : string.Empty; ;
                _engine = (configuration.engineId.HasValue) ? configuration.Engine.Name : string.Empty; ;
                _driveTrain = (configuration.driveTrainId.HasValue) ? configuration.DriveTrain.Name : string.Empty; ;
                _transmission = (configuration.transmissionId.HasValue) ? configuration.Transmission.Name : string.Empty;
            }

            public string PassengerDoor
            {
                get { return _passengerDoor; }
            }

            public string FuelType
            {
                get { return _fuelType; }
            }

            public string Engine
            {
                get { return _engine; }
            }

            public string DriveTrain
            {
                get { return _driveTrain; }
            }

            public string Transmission
            {
                get { return _transmission; }
            }
        }
	}
}
