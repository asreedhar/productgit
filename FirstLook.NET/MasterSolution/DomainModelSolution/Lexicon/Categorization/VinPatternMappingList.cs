using System;
using System.Collections.Generic;
using System.Data;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	internal class VinPatternMappingList : List<VinPatternMapping>, IExplicitlySerializable
	{
		#region Business Methods

		public List<VinPatternMapping> GetItems(ModelConfiguration modelConfiguration)
		{
			return FindAll(
				delegate(VinPatternMapping mapping)
				{
					return mapping.ModelConfigurationId == modelConfiguration.Id;
				});
		}

		public List<VinPatternMapping> GetItems(VinPattern vinPattern)
		{
			return FindAll(
				delegate(VinPatternMapping mapping)
				{
					return mapping.VinPatternId == vinPattern.Id;
				});
		}

		public List<VinPatternMapping> GetItems(VinPattern vinPattern, Model model)
		{
			return FindAll(
				delegate(VinPatternMapping mapping)
				{
					return mapping.VinPatternId == vinPattern.Id && mapping.ModelId == model.Id;
				});
		}

		#endregion

		#region Factory Methods

		internal static VinPatternMappingList GetVinPatternMappings(IDataReader reader)
		{
			return new VinPatternMappingList(reader);
		}

		private VinPatternMappingList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			while (reader.Read())
			{
				Add(VinPatternMapping.GetVinPatternMapping(reader));
			}
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", this);
		}

		internal VinPatternMappingList(SerializationData data)
		{
			IEnumerable<VinPatternMapping> items = data.GetValues<VinPatternMapping>("Items");
			IEnumerator<VinPatternMapping> en = items.GetEnumerator();
			while (en.MoveNext())
			{
				Add(en.Current);
			}
		}

		#endregion
	}
}
