using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class ModelFamilyList : ReadOnlyListBase<ModelFamilyList, ModelFamily>, IExplicitlySerializable
	{
		#region Business Methods

        /// <summary>
        /// Get a model family object from this model family list for the given id.
        /// </summary>
        /// <param name="id">Identifier of a model family.</param>
        /// <returns>Model family object for the given id, or null if it is not present in the list.</returns>
		public ModelFamily GetItem(int id)
		{
			foreach (ModelFamily modelType in this)
            {
                if (modelType.Id == id)
                {
                    return modelType;
                }
            }
			return null;
		}

        public void Sort()
        {
            IsReadOnly = false;
            ArrayList.Adapter(this).Sort(new NameableComparer());
            IsReadOnly = true;
        }

		#endregion

		#region Factory Methods

        /// <summary>
        /// Get a list of model families for the given year range, make and line. Will hand off execution to the data
        /// portal for establishing the database connection, etc.
        /// </summary>
        /// <param name="lowModelYear">Year earlier than which we do not want model families.</param>
        /// <param name="highModelYear">Year later than which we do not want model families.</param>
        /// <param name="makeId">Make by which to find model families.</param>
        /// <param name="lineId">Line by which to find model families.</param>
        /// <returns>A list of model family objects.</returns>
        public static ModelFamilyList GetModelFamilies(int lowModelYear, int highModelYear, int makeId, int lineId)
        {
            return DataPortal.Fetch<ModelFamilyList>(new Criteria(lowModelYear, highModelYear, makeId, lineId));
        }

        public static ModelFamilyList GetModelFamilies(int lowModelYear, int highModelYear, int makeId)
        {
            return DataPortal.Fetch<ModelFamilyList>(new Criteria(lowModelYear, highModelYear, makeId));
        }

        public static ModelFamilyList EmptyList()
        {
            return new ModelFamilyList();
        }

        /// <summary>
        /// Get a list of model families from the already retrieved database values.
        /// </summary>
        /// <param name="reader">Data reader from an already executed query.</param>
        /// <returns>A list of model family objects.</returns>
		internal static ModelFamilyList GetModelFamilies(IDataReader reader)
		{
			if (reader == null)
			{
			    throw new ArgumentNullException("reader");
			}
			return new ModelFamilyList(reader);
		}

        /// <summary>
        /// Populate this list with model family objects. Private to force construction to go through factory methods.
        /// </summary>
        /// <param name="reader">Data reader from an already executed query.</param>
		private ModelFamilyList(IDataReader reader)
		{
			Fetch(reader);
		}

        private ModelFamilyList()
        {
            /* csla needs parameterless constructor */
        }

        /// <summary>
        /// Criteria by which to find our list of model families. Includes a model year range, a make and a line.
        /// </summary>
        [Serializable]
        private class Criteria
        {
            private readonly int _lowModelYear;  // Year earlier than which we do not want model families.
            private readonly int _highModelYear; // Year later than which we do not want model families.
            private readonly int _makeId;        // Make by which to find model families.
            private readonly int? _lineId;       // Line by which to find model families.

            public Criteria(int lowModelYear, int highModelYear, int makeId, int lineId)
            {
                _lowModelYear = lowModelYear;
                _highModelYear = highModelYear;
                _makeId = makeId;
                _lineId = lineId;
            }

            public Criteria(int lowModelYear, int highModelYear, int makeId)
            {
                _lowModelYear = lowModelYear;
                _highModelYear = highModelYear;
                _makeId = makeId;
            }

            public int LowModelYear
            {
                get { return _lowModelYear; }
            }

            public int HighModelYear
            {
                get { return _highModelYear; }
            }

            public int MakeId
            {
                get { return _makeId; }
            }

            public int? LineId
            {
                get { return _lineId; }
            }
        }

		#endregion

		#region Data Access

        /// <summary>
        /// Execute the query that will populate this list of model families. Called by the data portal.
        /// </summary>
        /// <param name="criteria">Criteria by which to find model families.</param>
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDbConnection connection = Database.CreateConnection(Database.CategorizationDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Categorization.ModelFamilyList#Fetch";

                    Database.AddWithValue(command, "LowModelYear", criteria.LowModelYear, DbType.Int32);
                    Database.AddWithValue(command, "HighModelYear", criteria.HighModelYear, DbType.Int32);
                    Database.AddWithValue(command, "MakeId", criteria.MakeId, DbType.Int32);

                    if (criteria.LineId.HasValue)
                    {
                        Database.AddWithValue(command, "LineId", criteria.LineId, DbType.Int32);
                    }
                    else
                    {
                        Database.AddNullValue(command, "LineId", DbType.Int32);
                    }                    

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        /// <summary>
        /// Populate this model family list with values retrieved from the database.
        /// </summary>
        /// <param name="reader">Data reader for retrieved values.</param>
		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(ModelFamily.GetModelFamily(reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

        /// <summary>
        /// Add this objects values to the serialization data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

        /// <summary>
        /// Create a make object from serialized data.
        /// </summary>
        /// <param name="data">Serialization data.</param>
		internal ModelFamilyList(SerializationData data)
		{
			IEnumerable<ModelFamily> items = data.GetValues<ModelFamily>("Items");
			IEnumerator<ModelFamily> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
