using System;
using System.Collections;
using System.Data;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	internal class VinPatternMapping : IExplicitlySerializable
	{
		#region Business Methods

		private int vinPatternId;
		private int modelId;
		private int configurationId;
		private int modelConfigurationId;

		public int VinPatternId
		{
			get { return vinPatternId; }
		}

		public int ModelId
		{
			get { return modelId; }
		}

		public int ConfigurationId
		{
			get { return configurationId; }
		}

		public int ModelConfigurationId
		{
			get { return modelConfigurationId; }
		}

		public ICollection GetObjectState()
		{
			return new object[] { vinPatternId, modelId, configurationId, modelConfigurationId };
		}

		#endregion

		#region Factory Methods

		internal static VinPatternMapping GetVinPatternMapping(IDataRecord record)
		{
			return new VinPatternMapping(record);
		}

		private VinPatternMapping(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			vinPatternId = record.GetInt32(record.GetOrdinal("VinPatternId"));
			modelId = record.GetInt32(record.GetOrdinal("ModelId"));
			configurationId = record.GetInt32(record.GetOrdinal("ConfigurationId"));
			modelConfigurationId = record.GetInt32(record.GetOrdinal("ModelConfigurationId"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationVinPatternId = "V";
		private const string SerializationModelId = "M";
		private const string SerializationConfigurationId = "C";
		private const string SerializationModelConfigurationId = "I";

		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationVinPatternId, vinPatternId);
			data.AddValue(SerializationModelId, modelId);
			data.AddValue(SerializationConfigurationId, configurationId);
			data.AddValue(SerializationModelConfigurationId, modelConfigurationId);
		}

		internal VinPatternMapping(SerializationData data)
		{
			vinPatternId = data.GetInt32(SerializationVinPatternId);
			modelId = data.GetInt32(SerializationModelId);
			configurationId = data.GetInt32(SerializationConfigurationId);
			modelConfigurationId = data.GetInt32(SerializationModelConfigurationId);
		}

		#endregion
	}
}
