using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
    [Serializable]
    public class Catalog : ReadOnlyBase<Catalog>, IExplicitlySerializable
    {
        #region Business Methods

        private Criteria _id;
        private ModelYear _modelYear;
        private Make _make;
        private Line _line;
        private SegmentList _segments;
        private BodyTypeList _bodyTypes;
        private ModelFamilyList _modelFamilies;
        private SeriesList _series;
        private PassengerDoorList _passengerDoors;
        private EngineList _engines;
        private FuelTypeList _fuelTypes;
        private DriveTrainList _driveTrains;
        private TransmissionList _transmissions;
        private VinPatternList _vinPatterns;
        private VinPatternMappingList _vinPatternMappings;
        private ModelList _models;
        private ConfigurationList _configurations;
        private ModelConfigurationList _modelConfigurations;
        private ModelConfigurationMappingList _modelConfigurationMappings;

        public ModelYear ModelYear
        {
            get { return _modelYear; }
        }

        public Make Make
        {
            get { return _make; }
        }

        public Line Line
        {
            get { return _line; }
        }

        public SegmentList Segments
        {
            get { return _segments; }
        }

        public BodyTypeList BodyTypes
        {
            get { return _bodyTypes; }
        }

        public ModelFamilyList ModelFamilies
        {
            get { return _modelFamilies; }
        }

        public SeriesList Series
        {
            get { return _series; }
        }

        public PassengerDoorList PassengerDoors
        {
            get { return _passengerDoors; }
        }

        public EngineList Engines
        {
            get { return _engines; }
        }

        public FuelTypeList FuelTypes
        {
            get { return _fuelTypes; }
        }

        public DriveTrainList DriveTrains
        {
            get { return _driveTrains; }
        }

        public TransmissionList Transmissions
        {
            get { return _transmissions; }
        }

        public VinPatternList VinPatterns
        {
            get { return _vinPatterns; }
        }

        public ModelList Models
        {
            get { return _models; }
        }

        public ModelConfigurationList ModelConfigurations
        {
            get { return _modelConfigurations; }
        }

        internal ConfigurationList Configurations
        {
            get { return _configurations; }
        }

        public ModelConfiguration FindModelConfiguration(int catalogEntryId)
        {
            ModelConfigurationMapping mapping = _modelConfigurationMappings.Find(
                delegate(ModelConfigurationMapping item) { return item.CatalogEntryId == catalogEntryId; });

            if (mapping == null)
            {
                return null;
            }
            else
            {
                return ModelConfigurations.GetItem(mapping.ModelConfigurationId);
            }
        }

        public IEnumerable<int> FindCatalogEntryId(int modelConfigurationId)
        {
            List<ModelConfigurationMapping> mapping = _modelConfigurationMappings.FindAll(
                delegate(ModelConfigurationMapping item) { return item.ModelConfigurationId == modelConfigurationId; });

            List<int> values = new List<int>();

            mapping.ForEach(
                delegate(ModelConfigurationMapping item) { values.Add(item.CatalogEntryId); });

            return values;
        }

        internal List<ModelConfiguration> GetModelConfigurations(Model model)
        {
            List<ModelConfiguration> results = new List<ModelConfiguration>();
            foreach (ModelConfiguration modelConfiguration in _modelConfigurations)
            {
                if (modelConfiguration.Model.Equals(model))
                {
                    results.Add(modelConfiguration);
                }
            }
            return results;
        }

        internal List<Configuration> GetConfigurations(Model model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            List<Configuration> result = new List<Configuration>();

            foreach (ModelConfiguration modelConfiguration in _modelConfigurations)
            {
                if (modelConfiguration.Model.Equals(model))
                {
                    result.Add(modelConfiguration.Configuration);
                }
            }

            return result;
        }

        internal List<Configuration> GetConfigurations(Model model, VinPattern vinPattern)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            if (vinPattern == null)
            {
                throw new ArgumentNullException("vinPattern");
            }

            List<VinPatternMapping> mappings = _vinPatternMappings.GetItems(vinPattern, model);

            List<Configuration> result = new List<Configuration>();

            foreach (Configuration configuration in _configurations)
            {
                if (mappings.Exists(
                    delegate(VinPatternMapping mapping) { return mapping.ConfigurationId == configuration.Id; }))
                    result.Add(configuration);
            }

            return result;
        }

        internal List<ModelConfiguration> GetModelConfigurations(VinPattern vinPattern)
        {
            if (vinPattern == null)
            {
                throw new ArgumentNullException("vinPattern");
            }

            List<VinPatternMapping> mappings = _vinPatternMappings.GetItems(vinPattern);

            List<ModelConfiguration> result = new List<ModelConfiguration>();

            foreach (VinPatternMapping mapping in mappings)
            {
                foreach (ModelConfiguration configuration in _modelConfigurations)
                {
                    if (configuration.Id == mapping.ModelConfigurationId)
                    {
                        result.Add(configuration);
                    }
                }
            }

            return result;
        }

        internal ICollection<VinPattern> GetVinPatterns(ModelConfiguration modelConfiguration)
        {
            List<VinPatternMapping> mappings = _vinPatternMappings.GetItems(modelConfiguration);

            List<VinPattern> patterns = new List<VinPattern>();

            foreach (VinPattern pattern in _vinPatterns)
            {
                if (mappings.Exists(delegate(VinPatternMapping match) { return match.VinPatternId == pattern.Id; }))
                {
                    patterns.Add(pattern);
                }
            }

            return patterns;
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        protected override void OnDeserialized(StreamingContext context)
        {
            foreach (VinPattern vinPattern in _vinPatterns)
            {
                vinPattern.SetCatalog(this);
            }

            foreach (Model model in _models)
            {
                model.SetCatalog(this);
            }

            foreach (Configuration configuration in _configurations)
            {
                configuration.SetCatalog(this);
            }

            foreach (ModelConfiguration modelConfiguration in _modelConfigurations)
            {
                modelConfiguration.SetCatalog(this);
            }
        }
        
        #endregion

        #region Factory Methods

        internal static Catalog GetCatalog(int modelYear, int makeId, int lineId, IDataReader reader)
        {
            return new Catalog(new Criteria(modelYear, makeId, lineId), reader);
        }

        public static Catalog GetCatalog(int modelYear, int makeId, int lineId)
        {
            return DataPortal.Fetch<Catalog>(new Criteria(modelYear, makeId, lineId));
        }

        public static Catalog GetCatalog(SerializationData data)
        {
            return new Catalog(data);
        }

        private Catalog()
        {
            /* force use of factory methods */
        }

        private Catalog(Criteria criteria, IDataReader reader)
        {
            _id = criteria;

            Fetch(reader);
        }

        [Serializable]
        private class Criteria : IEquatable<Criteria>, IExplicitlySerializable
        {
            private readonly int _modelYear;
            private readonly int _makeId;
            private readonly int _lineId;

            public Criteria(int modelYear, int makeId, int lineId)
            {
                _modelYear = modelYear;
                _makeId = makeId;
                _lineId = lineId;
            }

            public int ModelYear
            {
                get { return _modelYear; }
            }

            public int MakeId
            {
                get { return _makeId; }
            }

            public int LineId
            {
                get { return _lineId; }
            }

            public bool Equals(Criteria criteria)
            {
                if (criteria == null) return false;
                if (_modelYear != criteria._modelYear) return false;
                if (_makeId != criteria._makeId) return false;
                if (_lineId != criteria._lineId) return false;
                return true;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;
                return Equals(obj as Criteria);
            }

            public override int GetHashCode()
            {
                int result = _modelYear;
                result = 29*result + _makeId;
                result = 29*result + _lineId;
                return result;
            }

            #region IExplicitlySerializable Members

            public void GetObjectData(SerializationData data)
            {
                data.AddValue("ModelYear", _modelYear);
                data.AddValue("MakeId", _makeId);
                data.AddValue("LineId", _lineId);
            }

            internal Criteria(SerializationData data)
            {
                _modelYear = data.GetInt32("ModelYear");
                _makeId = data.GetInt32("MakeId");
                _lineId = data.GetInt32("LineId");
            }

            #endregion
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(Criteria criteria)
        {
            _id = criteria;

            Fetch(criteria.ModelYear, criteria.MakeId, criteria.LineId);
        }

        private void Fetch(int modelYearId, int makeId, int lineId)
        {
            using (IDbConnection connection = Database.CreateConnection(Database.CategorizationDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Categorization.Catalog#Fetch";

                    Database.AddWithValue(command, "ModelYear", modelYearId, DbType.Int32);
                    Database.AddWithValue(command, "MakeId", makeId, DbType.Int32);
                    Database.AddWithValue(command, "LineId", lineId, DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        private void Fetch(IDataReader reader)
        {
            VerifyThisResultSet(reader, "Expected 1st result set to contain a single ModelYear");

            _modelYear = ModelYear.GetModelYear(reader);

            VerifyNextResultSet(reader, "Expected 2nd result set to contain a single Make");

            _make = Make.GetMake(reader);

            VerifyNextResultSet(reader, "Expected 3rd result set to contain a single Line");

            _line = Line.GetLine(reader);

            MoveToNextResultSet(reader, "Expected 4th result set (SegmentList)");

            _segments = SegmentList.GetSegments(reader);

            MoveToNextResultSet(reader, "Expected 5th result set (BodyTypeList)");

            _bodyTypes = BodyTypeList.GetBodyTypes(reader);

            MoveToNextResultSet(reader, "Expected 6th result set (ModelFamilyList)");

            _modelFamilies = ModelFamilyList.GetModelFamilies(reader);

            MoveToNextResultSet(reader, "Expected 7th result set (VinPatternList)");

            _vinPatterns = VinPatternList.GetVinPatterns(this, reader);

            MoveToNextResultSet(reader, "Expected 8th result set (SeriesList)");

            _series = SeriesList.GetSeries(reader);

            MoveToNextResultSet(reader, "Expected 9th result set (ModelList)");

            _models = ModelList.GetModels(this, reader);

            MoveToNextResultSet(reader, "Expected 10th result set (PassengerDoorList)");

            _passengerDoors = PassengerDoorList.GetPassengerDoors(reader);

            MoveToNextResultSet(reader, "Expected 11th result set (EngineList)");

            _engines = EngineList.GetEngines(reader);

            MoveToNextResultSet(reader, "Expected 12th result set (FuelTypeList)");

            _fuelTypes = FuelTypeList.GetFuelTypes(reader);

            MoveToNextResultSet(reader, "Expected 13th result set (DriveTrainList)");

            _driveTrains = DriveTrainList.GetDriveTrains(reader);

            MoveToNextResultSet(reader, "Expected 14th result set (TransmissionList)");

            _transmissions = TransmissionList.GetTransmissions(reader);

            MoveToNextResultSet(reader, "Expected 15th result set (ConfigurationList)");

            _configurations = ConfigurationList.GetConfigurations(this, reader);

            MoveToNextResultSet(reader, "Expected 16th result set (ModelConfigurationList)");

            _modelConfigurations = ModelConfigurationList.GetModelConfigurations(this, reader);

            MoveToNextResultSet(reader, "Expected 17th result set (ModelConfigurationMappingList)");

            _modelConfigurationMappings = ModelConfigurationMappingList.GetModelConfigurationMappings(reader);

            MoveToNextResultSet(reader, "Expected 18th result set (VinPatternMappingList)");

            _vinPatternMappings = VinPatternMappingList.GetVinPatternMappings(reader);
        }

        private static void VerifyThisResultSet(IDataReader reader, string exceptionMessage)
        {
            if (!reader.Read())
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }

        private static void VerifyNextResultSet(IDataReader reader, string exceptionMessage)
        {
            MoveToNextResultSet(reader, exceptionMessage);

            VerifyThisResultSet(reader, exceptionMessage);
        }

        private static void MoveToNextResultSet(IDataReader reader, string exceptionMessage)
        {
            if (!reader.NextResult())
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }

        #endregion

        #region IExplicitlySerializable Members

        public void GetObjectData(SerializationData data)
        {
            data.AddValue("Id", _id);
            data.AddValue("ModelYear", _modelYear);
            data.AddValue("Make", _make);
            data.AddValue("Line", _line);
            data.AddValue("Segments", _segments);
            data.AddValue("BodyTypes", _bodyTypes);
            data.AddValue("ModelFamilies", _modelFamilies);
            data.AddValue("Series", _series);
            data.AddValue("PassengerDoors", _passengerDoors);
            data.AddValue("Engines", _engines);
            data.AddValue("FuelTypes", _fuelTypes);
            data.AddValue("DriveTrains", _driveTrains);
            data.AddValue("Transmissions", _transmissions);
            data.AddValue("VinPatterns", _vinPatterns);
            data.AddValue("VinPatternMappings", _vinPatternMappings);
            data.AddValue("Models", _models);
            data.AddValue("Configurations", _configurations);
            data.AddValue("ModelConfigurations", _modelConfigurations);
            data.AddValue("ModelConfigurationMappings", _modelConfigurationMappings);
        }

        internal Catalog(SerializationData data)
        {
            _id = (Criteria) data.GetValue("Id", typeof (Criteria));
            _modelYear = (ModelYear) data.GetValue("ModelYear", typeof (ModelYear));
            _make = (Make) data.GetValue("Make", typeof (Make));
            _line = (Line) data.GetValue("Line", typeof (Line));
            _segments = (SegmentList) data.GetValue("Segments", typeof (SegmentList));
            _bodyTypes = (BodyTypeList) data.GetValue("BodyTypes", typeof (BodyTypeList));
            _modelFamilies = (ModelFamilyList) data.GetValue("ModelFamilies", typeof (ModelFamilyList));
            _series = (SeriesList) data.GetValue("Series", typeof (SeriesList));
            _passengerDoors = (PassengerDoorList) data.GetValue("PassengerDoors", typeof (PassengerDoorList));
            _engines = (EngineList) data.GetValue("Engines", typeof (EngineList));
            _fuelTypes = (FuelTypeList) data.GetValue("FuelTypes", typeof (FuelTypeList));
            _driveTrains = (DriveTrainList) data.GetValue("DriveTrains", typeof (DriveTrainList));
            _transmissions = (TransmissionList) data.GetValue("Transmissions", typeof (TransmissionList));
            _vinPatterns = (VinPatternList) data.GetValue("VinPatterns", typeof (VinPatternList));
            _vinPatternMappings =
                (VinPatternMappingList) data.GetValue("VinPatternMappings", typeof (VinPatternMappingList));
            _models = (ModelList) data.GetValue("Models", typeof (ModelList));
            _configurations = (ConfigurationList) data.GetValue("Configurations", typeof (ConfigurationList));
            _modelConfigurations =
                (ModelConfigurationList) data.GetValue("ModelConfigurations", typeof (ModelConfigurationList));
            _modelConfigurationMappings =
                (ModelConfigurationMappingList)
                data.GetValue("ModelConfigurationMappings", typeof (ModelConfigurationMappingList));
            OnDeserialized(new StreamingContext(StreamingContextStates.Persistence));
        }

        #endregion
    }
}