using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class BodyType : ReadOnlyBase<BodyType>, INameable<BodyType>, IExplicitlySerializable
	{
		#region Business Methods

		private int id;
		private string name;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static BodyType GetBodyType(IDataRecord record)
		{
			return new BodyType(record);
		}

		private BodyType(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion

		#region IExplicitlySerializable Members

		private const string SerializationId = "I";
		private const string SerializationName = "N";

		public void GetObjectData(SerializationData data)
		{
			data.AddValue(SerializationId, id);
			data.AddValue(SerializationName, name);
		}

		internal BodyType(SerializationData data)
		{
			id = data.GetInt32(SerializationId);
			name = data.GetString(SerializationName);
		}

		#endregion
	}
}
