using System;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Categorization
{
	[Serializable]
	public class VinPatternList : ReadOnlyListBase<VinPatternList, VinPattern>, IExplicitlySerializable
	{
		#region Business Methods

		public VinPattern GetItem(string pattern)
		{
			foreach (VinPattern vinPattern in this)
				if (string.Equals(vinPattern.Text, pattern))
					return vinPattern;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static VinPatternList GetVinPatterns(Catalog catalog, IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new VinPatternList(catalog, reader);
		}

		private VinPatternList(Catalog catalog, IDataReader reader)
		{
			Fetch(catalog, reader);
		}

		#endregion

		#region Data Access

		private void Fetch(Catalog catalog, IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(VinPattern.GetVinPattern(catalog, reader));
			}
			IsReadOnly = true;
		}

		#endregion

		#region IExplicitlySerializable Members

		public void GetObjectData(SerializationData data)
		{
			data.AddValues("Items", Items);
		}

		internal VinPatternList(SerializationData data)
		{
			IEnumerable<VinPattern> items = data.GetValues<VinPattern>("Items");
			IEnumerator<VinPattern> en = items.GetEnumerator();
			IsReadOnly = false;
			while (en.MoveNext())
			{
				Add(en.Current);
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
