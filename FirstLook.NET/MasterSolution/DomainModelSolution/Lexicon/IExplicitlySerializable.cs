namespace FirstLook.DomainModel.Lexicon
{
	/// <summary>
	/// We expect implementing classes to have a constructor that takes an
	/// SerializationData object.
	/// </summary>
	public interface IExplicitlySerializable
	{
		void GetObjectData(SerializationData data);
	}
}
