using System;
using System.Data;
using Csla;
using FirstLook.DomainModel.Lexicon;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class MakeList : ReadOnlyListBase<MakeList,Make>
	{
		#region Business Methods

		public Make GetItem(int id)
		{
			foreach (Make make in this)
				if (make.Id == id)
					return make;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static MakeList GetMakes(IDataReader reader)
		{
			return new MakeList(reader);
		}

		public static MakeList GetMakes(int modelYearId)
		{
			return DataPortal.Fetch<MakeList>(new ModelYearCriteria(modelYearId));
		}

		public class DataSource
		{
			public MakeList GetMakes(int modelYearId)
			{
				return MakeList.GetMakes(modelYearId);
			}
		}

		[Serializable]
		class ModelYearCriteria
		{
			private readonly int id;

			public ModelYearCriteria(int id)
			{
				this.id = id;
			}

			public int Id
			{
				get { return id; }
			}
		}

		private MakeList()
		{
			/* force use of factory methods */
		}

		private MakeList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void DataPortal_Fetch(ModelYearCriteria criteria)
		{
			Fetch(criteria.Id);
		}

		private void Fetch(int modelYearId)
		{
			using (IDbConnection connection = Database.CreateConnection(Database.DecodingDatabase))
			{
				if (connection.State != ConnectionState.Open)
					connection.Open();

				using (IDbCommand command = connection.CreateCommand())
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Decoding.MakeList#Fetch";

					Database.AddWithValue(command, "ModelYearId", modelYearId, DbType.Int32);
					
					using (IDataReader reader = command.ExecuteReader())
					{
						Fetch(reader);
					}
				}
			}
		}

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Make.GetMake(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
