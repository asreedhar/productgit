using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class DriveTrainList : ReadOnlyListBase<DriveTrainList, DriveTrain>
	{
		#region Business Methods

		public DriveTrain GetItem(int id)
		{
			foreach (DriveTrain driveTrain in this)
				if (driveTrain.Id == id)
					return driveTrain;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static DriveTrainList GetDriveTrains(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new DriveTrainList(reader);
		}

		private DriveTrainList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(DriveTrain.GetDriveTrain(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
