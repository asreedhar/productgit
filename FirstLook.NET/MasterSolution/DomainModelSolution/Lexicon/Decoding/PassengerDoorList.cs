using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class PassengerDoorList : ReadOnlyListBase<PassengerDoorList, PassengerDoor>
	{
		#region Business Methods

		public PassengerDoor GetItem(int id)
		{
			foreach (PassengerDoor passengerDoor in this)
				if (passengerDoor.Id == id)
					return passengerDoor;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static PassengerDoorList GetPassengerDoors(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new PassengerDoorList(reader);
		}

		private PassengerDoorList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(PassengerDoor.GetPassengerDoor(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
