using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class Series : ReadOnlyBase<Series>
	{
		#region Business Methods

		private int id;
		private string name;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static Series GetSeries(IDataRecord record)
		{
			return new Series(record);
		}

		private Series(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion
	}
}
