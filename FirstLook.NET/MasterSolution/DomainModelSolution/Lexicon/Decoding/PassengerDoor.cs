using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class PassengerDoor : ReadOnlyBase<PassengerDoor>
	{
		#region Business Methods

		private int id;
		private string name;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static PassengerDoor GetPassengerDoor(IDataRecord record)
		{
			return new PassengerDoor(record);
		}

		private PassengerDoor(IDataRecord record)
		{
			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion
	}
}
