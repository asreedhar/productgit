using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class BodyTypeList : ReadOnlyListBase<BodyTypeList, BodyType>
	{
		#region Business Methods

		public BodyType GetItem(int id)
		{
			foreach (BodyType bodyType in this)
				if (bodyType.Id == id)
					return bodyType;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static BodyTypeList GetBodyTypes(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new BodyTypeList(reader);
		}

		private BodyTypeList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(BodyType.GetBodyType(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
