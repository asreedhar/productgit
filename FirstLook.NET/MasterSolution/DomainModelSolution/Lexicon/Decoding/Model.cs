using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class Model : ReadOnlyBase<Model>
	{
		#region Business Methods

		[NonSerialized]
		private Catalog catalog;
		private int id;
		private string name;
		private int segmentId;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		public string FullName
		{
			get { return Name + " " + Segment.Name; }
		}

		public virtual Segment Segment
		{
			get { return catalog.Segments.GetItem(segmentId); }
		}

		internal void SetCatalog(Catalog catalog)
		{
			this.catalog = catalog;
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static Model GetModel(Catalog catalog, IDataRecord record)
		{
			return new Model(catalog, record);
		}

		protected internal Model(Catalog catalog, IDataRecord record)
		{
			this.catalog = catalog;

			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
			segmentId = record.GetInt32(record.GetOrdinal("SegmentId"));
		}

		#endregion
	}
}
