using System;
using System.Data;
using Csla;
using FirstLook.DomainModel.Lexicon;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class ModelList : ReadOnlyListBase<ModelList, Model>
	{
		#region Business Methods

		public Model GetItem(int id)
		{
			foreach (Model model in this)
				if (model.Id == id)
					return model;
			return null;
		}

		#endregion

		#region Factory Methods

		public static ModelList GetModels(int modelYearId, int makeId, int lineId)
		{
			return DataPortal.Fetch<ModelList>(new Criteria(modelYearId, makeId, lineId));
		}

		internal static ModelList GetModels(Catalog catalog, IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new ModelList(catalog, reader);
		}

		private ModelList()
		{
			
		}

		private ModelList(Catalog catalog, IDataReader reader)
		{
			Fetch(catalog, reader);
		}

		[Serializable]
		private class Criteria
		{
			private readonly int modelYearId;
			private readonly int makeId;
			private readonly int lineId;

			public Criteria(int modelYearId, int makeId, int lineId)
			{
				this.modelYearId = modelYearId;
				this.makeId = makeId;
				this.lineId = lineId;
			}

			public int ModelYearId
			{
				get { return modelYearId; }
			}

			public int MakeId
			{
				get { return makeId; }
			}

			public int LineId
			{
				get { return lineId; }
			}
		}

		public class DataSource
		{
			public ModelList GetModels(int modelYearId, int makeId, int lineId)
			{
				return ModelList.GetModels(modelYearId, makeId, lineId);
			}
		}

		#endregion

		#region Data Access

		private void DataPortal_Fetch(Criteria criteria)
		{
			using (IDbConnection connection = Database.CreateConnection(Database.DecodingDatabase))
			{
				if (connection.State != ConnectionState.Open)
					connection.Open();

				using (IDbCommand command = connection.CreateCommand())
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Decoding.ModelList#Fetch";

					Database.AddWithValue(command, "ModelYearId", criteria.ModelYearId, DbType.Int32);
					Database.AddWithValue(command, "MakeId", criteria.MakeId, DbType.Int32);
					Database.AddWithValue(command, "LineId", criteria.LineId, DbType.Int32);

					using (IDataReader reader = command.ExecuteReader())
					{
						IsReadOnly = false;
						while (reader.Read())
						{
							Add(StandaloneModel.GetModel(reader));
						}
						IsReadOnly = true;
					}
				}
			}
		}

		[Serializable]
		private class StandaloneModel : Model
		{
			private readonly Segment segment;

			internal static StandaloneModel GetModel(IDataRecord record)
			{
				return new StandaloneModel(record);
			}

			private StandaloneModel(IDataRecord record) : base(null, record)
			{
				segment = Segment.GetSegment(record, "Segment");
			}

			public override Segment Segment
			{
				get { return segment; }
			}
		}

		private void Fetch(Catalog catalog, IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Model.GetModel(catalog, reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
