using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class Segment : ReadOnlyBase<Segment>
	{
		#region Business Methods

		private int id;
		private string name;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static Segment GetSegment(IDataRecord record)
		{
			return new Segment(record, string.Empty);
		}

		internal static Segment GetSegment(IDataRecord record, string prefix)
		{
			return new Segment(record, prefix);
		}

		private Segment(IDataRecord record, string prefix)
		{
			Fetch(record, prefix);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record, string prefix)
		{
			id = record.GetInt32(record.GetOrdinal(prefix+"Id"));
			name = record.GetString(record.GetOrdinal(prefix + "Name"));
		}

		#endregion
	}
}
