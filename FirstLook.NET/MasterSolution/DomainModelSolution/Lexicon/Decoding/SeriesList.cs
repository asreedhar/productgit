using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class SeriesList : ReadOnlyListBase<SeriesList, Series>
	{
		#region Business Methods

		public Series GetItem(int id)
		{
			foreach (Series series in this)
				if (series.Id == id)
					return series;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static SeriesList GetSeries(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new SeriesList(reader);
		}

		private SeriesList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Series.GetSeries(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
