using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class SegmentList : ReadOnlyListBase<SegmentList, Segment>
	{
		#region Business Methods

		public Segment GetItem(int id)
		{
			foreach (Segment segment in this)
				if (segment.Id == id)
					return segment;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static SegmentList GetSegments(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new SegmentList(reader);
		}

		private SegmentList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Segment.GetSegment(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
