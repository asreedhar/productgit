using System;
using System.Data;
using System.Text;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class CatalogEntry : ReadOnlyBase<CatalogEntry>
	{
		#region Business Methods

		[NonSerialized]
		private Catalog catalog;
		private int id;
		private int? parentId;
		private int modelId;
		private int segmentId;
		private int? bodyTypeId;
		private int? seriesId;
		private int? passengerDoorId;
		private int? fuelTypeId;
		private int? engineId;
		private int? driveTrainId;
		private int? transmissionId;
		private string vinPattern;

		public int Id
		{
			get { return id; }
		}

		public int? ParentId
		{
			get { return parentId; }
		}

		public ModelYear ModelYear
		{
			get { return catalog.ModelYear; }
		}

		public Make Make
		{
			get { return catalog.Make; }
		}

		public Line Line
		{
			get { return catalog.Line; }
		}

		public Model Model
		{
			get { return catalog.Models.GetItem(modelId); }
		}

		public Segment Segment
		{
			get { return catalog.Segments.GetItem(segmentId); }
		}

		public BodyType BodyType
		{
			get
			{
				if (bodyTypeId.HasValue)
					return catalog.BodyTypes.GetItem(bodyTypeId.Value);
				return null;
			}
		}

		public Series Series
		{
			get
			{
				if (seriesId.HasValue)
					return catalog.Series.GetItem(seriesId.Value);
				return null;
			}
		}

		public PassengerDoor PassengerDoor
		{
			get
			{
				if (passengerDoorId.HasValue)
					return catalog.PassengerDoors.GetItem(passengerDoorId.Value);
				return null;
			}
		}

		public FuelType FuelType
		{
			get
			{
				if (fuelTypeId.HasValue)
					return catalog.FuelTypes.GetItem(fuelTypeId.Value);
				return null;
			}
		}

		public Engine Engine
		{
			get
			{
				if (engineId.HasValue)
					return catalog.Engines.GetItem(engineId.Value);
				return null;
			}
		}

		public DriveTrain DriveTrain
		{
			get
			{
				if (driveTrainId.HasValue)
					return catalog.DriveTrains.GetItem(driveTrainId.Value);
				return null;
			}
		}

		public Transmission Transmission
		{
			get
			{
				if (transmissionId.HasValue)
					return catalog.Transmissions.GetItem(transmissionId.Value);
				return null;
			}
		}

		public string Name
		{
			get
			{
				StringBuilder sb = new StringBuilder();

				if (seriesId.HasValue)
				{
					sb.Append(" ").Append(Series.Name);
				}

				if (bodyTypeId.HasValue)
				{
					if (driveTrainId.HasValue && !BodyType.Name.Contains(DriveTrain.Name))
					{
						sb.Append(" ").Append(DriveTrain.Name).Append(" ").Append(BodyType.Name);
					}
					else
					{
						sb.Append(" ").Append(BodyType.Name);
					}
				}
				else if (driveTrainId.HasValue)
				{
					sb.Append(" ").Append(DriveTrain.Name);
				}
				
				sb.Append(" (");

				if (engineId.HasValue)
				{
					sb.Append(" ").Append(Engine.Name);
				}

				if (fuelTypeId.HasValue)
				{
					sb.Append(" ").Append(FuelType.Name);
				}

				if (transmissionId.HasValue)
				{
					sb.Append(" ").Append(Transmission.Name);
				}
				
				sb.Append(")");

				return sb.ToString(1, sb.Length - 1);
			}
		}

		public string VinPattern
		{
			get { return vinPattern; }
		}

		internal void SetCatalog(Catalog catalog)
		{
			this.catalog = catalog;
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static CatalogEntry GetCatalogEntry(Catalog catalog, IDataRecord record)
		{
			return new CatalogEntry(catalog, record);
		}

		private CatalogEntry(Catalog catalog, IDataRecord record)
		{
			this.catalog = catalog;

			Fetch(record);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			parentId = GetInt32(record, "ParentId");
			modelId = record.GetInt32(record.GetOrdinal("ModelId"));
			segmentId = record.GetInt32(record.GetOrdinal("SegmentId"));
			bodyTypeId = GetInt32(record, "BodyTypeId");
			seriesId = GetInt32(record, "SeriesId");
			passengerDoorId = GetInt32(record, "PassengerDoorId");
			fuelTypeId = GetInt32(record, "FuelTypeId");
			engineId = GetInt32(record, "EngineId");
			driveTrainId = GetInt32(record, "DriveTrainId");
			transmissionId = GetInt32(record, "TransmissionId");
			vinPattern = record.GetString(record.GetOrdinal("VinPattern"));
		}

		private static int? GetInt32(IDataRecord record, string column)
		{
			int ordinal = record.GetOrdinal(column);
			if (record.IsDBNull(ordinal))
				return null;
			return record.GetInt32(ordinal);
		}

		#endregion

		public class Presentation
		{
			private readonly CatalogEntry entry;

			internal Presentation(CatalogEntry entry)
			{
				this.entry = entry;
			}

			public int Id
			{
				get { return entry.Id; }
			}

			public int? ParentId
			{
				get { return entry.ParentId; }
			}

			public string ModelYear
			{
				get { return entry.ModelYear.Name; }
			}

			public string Make
			{
				get { return entry.Make.Name; }
			}

			public string Line
			{
				get { return entry.Line.Name; }
			}

			public string Model
			{
				get { return entry.Model.Name; }
			}

			public string Segment
			{
				get
				{
					return (entry.Segment == null)
						? string.Empty
						: entry.Segment.Name;
				}
			}

			public string BodyType
			{
				get
				{
					return (entry.BodyType == null)
						? string.Empty
						: entry.BodyType.Name;
				}
			}

			public string Series
			{
				get
				{
					return (entry.Series == null)
						? string.Empty 
						: entry.Series.Name;
				}
			}

			public string PassengerDoor
			{
				get
				{
					return (entry.PassengerDoor == null)
						? string.Empty
						: entry.PassengerDoor.Name;
				}
			}

			public string FuelType
			{
				get
				{
					return (entry.FuelType == null)
						? string.Empty
						: entry.FuelType.Name;
				}
			}

			public string Engine
			{
				get
				{
					return (entry.Engine == null)
						? string.Empty
						: entry.Engine.Name;
				}
			}

			public string DriveTrain
			{
				get
				{
					return (entry.DriveTrain == null)
						? string.Empty
						: entry.DriveTrain.Name;
				}
			}

			public string Transmission
			{
				get
				{
					return (entry.Transmission == null)
						? string.Empty
						: entry.Transmission.Name;
				}
			}

			public string VinPattern
			{
				get { return entry.VinPattern; }
			}
		}
	}
}