using System;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class CatalogEntryList : ReadOnlyListBase<CatalogEntryList, CatalogEntry>
	{
		#region Business Methods

		public IEnumerable<CatalogEntry> LevelOne
		{
			get
			{
				List<CatalogEntry> levelOne = new List<CatalogEntry>();
				foreach (CatalogEntry catalogEntry in this)
				{
					if (!catalogEntry.ParentId.HasValue)
					{
						levelOne.Add(catalogEntry);
					}
				}
				return levelOne;
			}
		}

		public IEnumerable<CatalogEntry> LevelTwo
		{
			get
			{
				List<CatalogEntry> levelTwo = new List<CatalogEntry>();
				foreach (CatalogEntry catalogEntry in this)
				{
					if (catalogEntry.ParentId.HasValue)
					{
						levelTwo.Add(catalogEntry);
					}
				}
				return levelTwo;
			}
		}

		public IList<CatalogEntry.Presentation> PresentationEntries()
		{
			return PresentationEntries(false);
		}

		public IList<CatalogEntry.Presentation> PresentationEntries(bool levelTwoOnly)
		{
			List<CatalogEntry.Presentation> values = new List<CatalogEntry.Presentation>();

			foreach (CatalogEntry entry in this)
			{
				if (!levelTwoOnly || entry.ParentId.HasValue)
				{
					values.Add(new CatalogEntry.Presentation(entry));
				}
			}

			return values;
		}

		#endregion

		#region Factory Methods

		internal static CatalogEntryList GetCatalogEntries(Catalog catalog, IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new CatalogEntryList(catalog, reader);
		}

		private CatalogEntryList(Catalog catalog, IDataReader reader)
		{
			Fetch(catalog, reader);
		}

		#endregion

		#region Data Access

		private void Fetch(Catalog catalog, IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(CatalogEntry.GetCatalogEntry(catalog, reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
