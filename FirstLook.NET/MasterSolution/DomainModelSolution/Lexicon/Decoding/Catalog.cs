using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
    [Serializable]
    public class Catalog : ReadOnlyBase<Catalog>
    {
        #region Business Methods

        private Criteria _id;
        private ModelYear _modelYear;
        private Make _make;
        private Line _line;
        private SegmentList _segments;
        private BodyTypeList _bodyTypes;
        private ModelList _models;
        private SeriesList _series;
        private PassengerDoorList _passengerDoors;
        private EngineList _engines;
        private FuelTypeList _fuelTypes;
        private DriveTrainList _driveTrains;
        private TransmissionList _transmissions;
        private CatalogEntryList _entries;

        public ModelYear ModelYear
        {
            get { return _modelYear; }
        }

        public Make Make
        {
            get { return _make; }
        }

        public Line Line
        {
            get { return _line; }
        }

        public SegmentList Segments
        {
            get { return _segments; }
        }

        public BodyTypeList BodyTypes
        {
            get { return _bodyTypes; }
        }

        public ModelList Models
        {
            get { return _models; }
        }

        public SeriesList Series
        {
            get { return _series; }
        }

        public PassengerDoorList PassengerDoors
        {
            get { return _passengerDoors; }
        }

        public EngineList Engines
        {
            get { return _engines; }
        }

        public FuelTypeList FuelTypes
        {
            get { return _fuelTypes; }
        }

        public DriveTrainList DriveTrains
        {
            get { return _driveTrains; }
        }

        public TransmissionList Transmissions
        {
            get { return _transmissions; }
        }

        public CatalogEntryList Entries
        {
            get { return _entries; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        protected override void OnDeserialized(StreamingContext context)
        {
            foreach (Model model in _models)
            {
                model.SetCatalog(this);
            }

            foreach (CatalogEntry entry in _entries)
            {
                entry.SetCatalog(this);
            }
        }

        #endregion

        #region Factory Methods

        internal static Catalog GetCatalog(int modelYearId, int makeId, int lineId, IDataReader reader)
        {
            return new Catalog(new Criteria(modelYearId, makeId, lineId, null, null), reader);
        }

        public static Catalog GetCatalog(int modelYearId, int makeId, int lineId)
        {
            return DataPortal.Fetch<Catalog>(new Criteria(modelYearId, makeId, lineId, null, null));
        }

        public static Catalog GetCatalog(int modelYearId, int makeId, int lineId, int modelId)
        {
            return DataPortal.Fetch<Catalog>(new Criteria(modelYearId, makeId, lineId, modelId, null));
        }

        public static Catalog GetCatalog(int modelYearId, int makeId, int lineId, int modelId, int vehicleCatalogId)
        {
            return DataPortal.Fetch<Catalog>(new Criteria(modelYearId, makeId, lineId, modelId, vehicleCatalogId));
        }

        private Catalog()
        {
            /* hide constructor so people have to use the factory methods */
        }

        private Catalog(Criteria criteria, IDataReader reader) : this()
        {
            _id = criteria;

            Fetch(reader);
        }

        [Serializable]
        protected class Criteria : IEquatable<Criteria>
        {
            private readonly int _modelYearId;
            private readonly int _makeId;
            private readonly int _lineId;
            private readonly int? _modelId;
            private readonly int? _vehicleCatalogId;

            public Criteria(int modelYearId, int makeId, int lineId, int? modelId, int? vehicleCatalogId)
            {
                _modelYearId = modelYearId;
                _makeId = makeId;
                _lineId = lineId;
                _modelId = modelId;
                _vehicleCatalogId = vehicleCatalogId;
            }

            public int ModelYearId
            {
                get { return _modelYearId; }
            }

            public int MakeId
            {
                get { return _makeId; }
            }

            public int LineId
            {
                get { return _lineId; }
            }

            public int? ModelId
            {
                get { return _modelId; }
            }

            public int? VehicleCatalogId
            {
                get { return _vehicleCatalogId; }
            }

            public static bool operator !=(Criteria criteria1, Criteria criteria2)
            {
                return !Equals(criteria1, criteria2);
            }

            public static bool operator ==(Criteria criteria1, Criteria criteria2)
            {
                return Equals(criteria1, criteria2);
            }

            public bool Equals(Criteria criteria)
            {
                if (criteria == null) return false;
                if (_modelYearId != criteria._modelYearId) return false;
                if (_makeId != criteria._makeId) return false;
                if (_lineId != criteria._lineId) return false;
                if (!Equals(_modelId, criteria._modelId)) return false;
                if (!Equals(_vehicleCatalogId, criteria._vehicleCatalogId)) return false;
                return true;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;
                return Equals(obj as Criteria);
            }

            public override int GetHashCode()
            {
                int result = _modelYearId;
                result = 29*result + _makeId;
                result = 29*result + _lineId;
                result = 29*result + _modelId.GetHashCode();
                result = 29*result + _vehicleCatalogId.GetHashCode();
                return result;
            }
        }

        public class DataSource
        {
            public IEnumerable<CatalogEntry> GetCatalogEntries(int modelYear, int makeId, int lineId)
            {
                return GetCatalog(modelYear, makeId, lineId).Entries;
            }

            public IEnumerable<CatalogEntry> GetCatalogEntries(int modelYear, int makeId, int lineId, int modelId)
            {
                if (modelId == 0)
                {
                    return GetCatalog(modelYear, makeId, lineId).Entries;
                }

                return GetCatalog(modelYear, makeId, lineId, modelId).Entries;
            }

            public IEnumerable<CatalogEntry> GetCatalogEntries(int modelYear, int makeId, int lineId, int modelId,
                                                               int vehicleCatalogId)
            {
                if (modelId == 0)
                {
                    return GetCatalog(modelYear, makeId, lineId).Entries;
                }

                if (vehicleCatalogId == 0)
                {
                    return GetCatalog(modelYear, makeId, lineId, modelId).Entries;
                }

                Catalog catalog = GetCatalog(modelYear, makeId, lineId, modelId, vehicleCatalogId);

                foreach (CatalogEntry entry in catalog.Entries)
                {
                    if (entry.Id == vehicleCatalogId)
                    {
                        if (entry.ParentId.HasValue)
                        {
                            return catalog.Entries.LevelTwo;
                        }
                    }
                }

                return catalog.Entries;
            }
        }

        #endregion

        #region Data Access

        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            _id = criteria;

            Fetch(criteria.ModelYearId, criteria.MakeId, criteria.LineId, criteria.ModelId, criteria.VehicleCatalogId);
        }

        private void Fetch(int modelYearId, int makeId, int lineId, int? modelId, int? vehicleCatalogId)
        {
            using (IDbConnection connection = Database.CreateConnection(Database.DecodingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Decoding.Catalog#Fetch";

                    Database.AddWithValue(command, "ModelYearId", modelYearId, DbType.Int32);
                    Database.AddWithValue(command, "MakeId", makeId, DbType.Int32);
                    Database.AddWithValue(command, "LineId", lineId, DbType.Int32);

                    if (modelId.HasValue)
                    {
                        Database.AddWithValue(command, "ModelId", modelId.Value, DbType.Int32);
                    }
                    else
                    {
                        Database.AddNullValue(command, "ModelId", DbType.Int32);
                    }

                    if (vehicleCatalogId.HasValue)
                    {
                        Database.AddWithValue(command, "VehicleCatalogId", vehicleCatalogId.Value, DbType.Int32);
                    }
                    else
                    {
                        Database.AddNullValue(command, "VehicleCatalogId", DbType.Int32);
                    }

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        private void Fetch(IDataReader reader)
        {
            VerifyThisResultSet(reader, "Expected 1st result set to contain a single ModelYear");

            _modelYear = ModelYear.GetModelYear(reader);

            VerifyNextResultSet(reader, "Expected 2nd result set to contain a single Make");

            _make = Make.GetMake(reader);

            VerifyNextResultSet(reader, "Expected 3rd result set to contain a single Line");

            _line = Line.GetLine(reader);

            MoveToNextResultSet(reader, "Expected 4th result set (SegmentList)");

            _segments = SegmentList.GetSegments(reader);

            MoveToNextResultSet(reader, "Expected 5th result set (BodyTypeList)");

            _bodyTypes = BodyTypeList.GetBodyTypes(reader);

            MoveToNextResultSet(reader, "Expected 6th result set (ModelList)");

            _models = ModelList.GetModels(this, reader);

            MoveToNextResultSet(reader, "Expected 7th result set (SeriesList)");

            _series = SeriesList.GetSeries(reader);

            MoveToNextResultSet(reader, "Expected 8th result set (PassengerDoorList)");

            _passengerDoors = PassengerDoorList.GetPassengerDoors(reader);

            MoveToNextResultSet(reader, "Expected 9th result set (EngineList)");

            _engines = EngineList.GetEngines(reader);

            MoveToNextResultSet(reader, "Expected 10th result set (FuelTypeList)");

            _fuelTypes = FuelTypeList.GetFuelTypes(reader);

            MoveToNextResultSet(reader, "Expected 11th result set (DriveTrainList)");

            _driveTrains = DriveTrainList.GetDriveTrains(reader);

            MoveToNextResultSet(reader, "Expected 11th result set (TransmissionList)");

            _transmissions = TransmissionList.GetTransmissions(reader);

            MoveToNextResultSet(reader, "Expected 12th result set (CatalogEntryList)");

            _entries = CatalogEntryList.GetCatalogEntries(this, reader);
        }

        private static void VerifyThisResultSet(IDataReader reader, string exceptionMessage)
        {
            if (!reader.Read())
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }

        private static void VerifyNextResultSet(IDataReader reader, string exceptionMessage)
        {
            MoveToNextResultSet(reader, exceptionMessage);

            VerifyThisResultSet(reader, exceptionMessage);
        }

        private static void MoveToNextResultSet(IDataReader reader, string exceptionMessage)
        {
            if (!reader.NextResult())
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }

        #endregion
    }
}