using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class TransmissionList : ReadOnlyListBase<TransmissionList, Transmission>
	{
		#region Business Methods

		public Transmission GetItem(int id)
		{
			foreach (Transmission transmission in this)
				if (transmission.Id == id)
					return transmission;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static TransmissionList GetTransmissions(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new TransmissionList(reader);
		}

		private TransmissionList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Transmission.GetTransmission(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
