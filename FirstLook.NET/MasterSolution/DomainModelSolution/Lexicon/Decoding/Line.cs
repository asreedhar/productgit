using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class Line : ReadOnlyBase<Line>
	{
		#region Business Methods
		
		private readonly int id;
		private readonly string name;

		public int Id
		{
			get { return id; }
		}

		public string Name
		{
			get { return name; }
		}

		protected override object GetIdValue()
		{
			return id;
		}

		#endregion

		#region Factory Methods

		internal static Line GetLine(IDataRecord record)
		{
			return new Line(record);
		}

		private Line(IDataRecord record)
		{
			id = record.GetInt32(record.GetOrdinal("Id"));
			name = record.GetString(record.GetOrdinal("Name"));
		}

		#endregion
	}
}
