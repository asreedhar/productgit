using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class EngineList : ReadOnlyListBase<EngineList, Engine>
	{
		#region Business Methods

		public Engine GetItem(int id)
		{
			foreach (Engine engine in this)
				if (engine.Id == id)
					return engine;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static EngineList GetEngines(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new EngineList(reader);
		}

		private EngineList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(Engine.GetEngine(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
