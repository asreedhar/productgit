using System;
using System.Data;
using Csla;

namespace FirstLook.DomainModel.Lexicon.Decoding
{
	[Serializable]
	public class FuelTypeList : ReadOnlyListBase<FuelTypeList, FuelType>
	{
		#region Business Methods

		public FuelType GetItem(int id)
		{
			foreach (FuelType fuelType in this)
				if (fuelType.Id == id)
					return fuelType;
			return null;
		}

		#endregion

		#region Factory Methods

		internal static FuelTypeList GetFuelTypes(IDataReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");
			return new FuelTypeList(reader);
		}

		private FuelTypeList(IDataReader reader)
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			IsReadOnly = false;
			while (reader.Read())
			{
				Add(FuelType.GetFuelType(reader));
			}
			IsReadOnly = true;
		}

		#endregion
	}
}
