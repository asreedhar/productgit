using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;

namespace FirstLook.DomainModel.Lexicon
{
	public static class Database
	{
		private const string decodingDatabase = "Market";

        private const string categorizationDatabase = "VehicleCatalog";

        public static string CategorizationDatabase
        {
            get { return categorizationDatabase; }
        }

		public static string DecodingDatabase
		{
			get { return decodingDatabase; }
		}

		public static DbConnection CreateConnection(string database)
		{
			ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[database];

            if (settings == null)
            {
                string message = string.Format(
                    CultureInfo.InvariantCulture,
                    "Connection string for database '{0}' not present!",
                    database);

                throw new DataException(message);
            }

		    DbProviderFactory providerFactory = DbProviderFactories.GetFactory(settings.ProviderName);

			DbConnection connection = providerFactory.CreateConnection();

			connection.ConnectionString = settings.ConnectionString;

			return connection;
		}

		public static void AddWithValue(IDbCommand command, string parameterName, object parameterValue, DbType dbType)
		{
			IDbDataParameter parameter = command.CreateParameter();
			parameter.ParameterName = parameterName;
			parameter.DbType = dbType;
			parameter.Value = parameterValue;
			command.Parameters.Add(parameter);
		}

		public static void AddNullValue(IDbCommand command, string parameterName, DbType dbType)
		{
			IDbDataParameter parameter = command.CreateParameter();
			parameter.ParameterName = parameterName;
			parameter.DbType = dbType;
			parameter.Value = DBNull.Value;
			SqlParameter sqlParameter = parameter as SqlParameter;
			if (sqlParameter != null)
				sqlParameter.IsNullable = true;
			command.Parameters.Add(parameter);
		}

		public static IDbDataParameter AddOutParameter(IDbCommand command, string parameterName, DbType dbType)
		{
			IDbDataParameter parameter = command.CreateParameter();
			parameter.ParameterName = parameterName;
			parameter.DbType = dbType;
			parameter.Direction = ParameterDirection.Output;
			command.Parameters.Add(parameter);
			return parameter;
		}

		public static void AddArrayParameter(IDbCommand command, string name, DbType type, bool nullable, Array value)
		{
			IDbDataParameter paramVersion = command.CreateParameter();
			paramVersion.DbType = type;
			paramVersion.ParameterName = name;
			paramVersion.Size = value.Length;
			paramVersion.Value = value;

			SqlParameter paramVersionSql = paramVersion as SqlParameter;

			if (paramVersionSql != null)
			{
				paramVersionSql.IsNullable = nullable;
			}

			command.Parameters.Add(paramVersion);
		}

		public static IDataParameter AddArrayOutParameter(IDbCommand command, string name, DbType type, bool nullable, int size)
		{
			IDbDataParameter parameter = command.CreateParameter();
			parameter.DbType = type;
			parameter.Direction = ParameterDirection.Output;
			parameter.ParameterName = name;
			parameter.Size = size;

			SqlParameter sqlParameter = parameter as SqlParameter;

			if (sqlParameter != null)
			{
				sqlParameter.IsNullable = nullable;
			}

			command.Parameters.Add(parameter);

			return parameter;
		}
	}
}
