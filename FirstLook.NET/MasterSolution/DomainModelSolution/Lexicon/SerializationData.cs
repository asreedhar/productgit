using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace FirstLook.DomainModel.Lexicon
{
	public abstract class SerializationData
	{
		private static readonly Type[] ConstructorArgs = new Type[] { typeof(SerializationData) };

		private readonly IDictionary dictionary;

		protected SerializationData(IDictionary dictionary)
		{
			this.dictionary = dictionary;
		}

		protected abstract object MakeKey(string name);

		protected abstract IDictionary MakeValue(IExplicitlySerializable value);

		protected abstract SerializationData NewSerializationData(IDictionary value);

		public void AddValue(string name, int value)
		{
			dictionary.Add(MakeKey(name), value);
		}

		public void AddValue<T>(string name, Nullable<T> value) where T : struct
		{
			dictionary.Add(MakeKey(name), value);
		}

		public void AddValue(string name, string value)
		{
			dictionary.Add(MakeKey(name), value);
		}

		public void AddValue(string name, IExplicitlySerializable value)
		{
			dictionary.Add(MakeKey(name), MakeValue(value));
		}

		public void AddValues<T>(string name, IEnumerable<T> values) where T : IExplicitlySerializable
		{
			ArrayList list = new ArrayList();
			foreach (T value in values)
			{
				list.Add(MakeValue(value));
			}
			dictionary.Add(MakeKey(name), list);
		}

		public int GetInt32(string name)
		{
			return (int) dictionary[MakeKey(name)];
		}

		public Nullable<T> GetNullable<T>(string name) where T : struct
		{
			return (Nullable<T>) dictionary[MakeKey(name)];
		}

		public string GetString(string name)
		{
			return (string) dictionary[MakeKey(name)];
		}

		public object GetValue(string name, Type type)
		{
			ConstructorInfo constructorInfo = type.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static, null, ConstructorArgs, null);

			return constructorInfo.Invoke(new object[] {NewSerializationData((IDictionary) dictionary[MakeKey(name)])});
		}

		public IEnumerable<T> GetValues<T>(string name) where T : IExplicitlySerializable
		{
			ConstructorInfo constructorInfo = typeof(T).GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static, null, ConstructorArgs, null);
			IList<T> list = new List<T>();
			ArrayList values = (ArrayList) dictionary[MakeKey(name)];
			foreach (IDictionary value in values)
			{
				list.Add((T) constructorInfo.Invoke(new object[] {NewSerializationData(value)}));
			}
			return list;
		}
	}
}
