using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    public class BusinessUnitFinder : Finder<BusinessUnit>
    {
        private static readonly BusinessUnitFinder finder = new BusinessUnitFinder();

        /// <summary>
        /// Please do not get ride of me... I'm needed for the ObjectDataSource on ChooseDealer.aspx, thanks PM
        /// </summary>
        static BusinessUnitFinder() { /* lazy singleton */ }

        public static BusinessUnitFinder Instance()
        {
            return finder;
        }

        public BusinessUnit Find(int id)
        {
            return FindFirst(CreateWhereClause("Id", id));
        }

        public BusinessUnit FindDealerGroupByDealership(int dealershipId)
        {
            string commandText = @"
                SELECT
            " + GetRowDataGateway().ColumnList("BU1") + @"
                FROM
		                dbo.BusinessUnit BU1
                JOIN	dbo.BusinessUnitRelationship BR1 ON BU1.BusinessUnitID = BR1.ParentID
                JOIN	dbo.BusinessUnit BU2 ON BU2.BusinessUnitID = BR1.BusinessUnitID
                WHERE
	                    BU1.BusinessUnitTypeID = 3
                AND     BU2.BusinessUnitTypeID = 4
                AND	    BU2.BusinessUnitID = @BusinessUnitID
                ORDER
                BY      BU1.BusinessUnit";

            return First(Find(commandText, CreateBindingList("BusinessUnitID", dealershipId, DbType.Int32, false)));
        }

        public IList<BusinessUnit> FindAllDealershipsByDealerGroup(int dealerGroupId)
        {
            return FindAllDealershipsByDealerGroup(dealerGroupId, null);
        }

        public IList<BusinessUnit> FindAllDealershipsByDealerGroup(int dealerGroupId, string filter)
        {
            string commandText = @"
                SELECT
            " + GetRowDataGateway().ColumnList("BU1") + @"
                FROM
		                dbo.BusinessUnit BU1
                JOIN	dbo.BusinessUnitRelationship BR1 ON BU1.BusinessUnitID = BR1.BusinessUnitID
                JOIN	dbo.BusinessUnit BU2 ON BU2.BusinessUnitID = BR1.ParentID
                JOIN	dbo.DealerPreference DP1 ON DP1.BusinessUnitID = BU1.BusinessUnitID
                WHERE
	                    BU1.BusinessUnitTypeID = 4
                AND	    BU1.Active = 1 
                AND     DP1.GoLiveDate IS NOT NULL
                AND	    BU2.BusinessUnitID = @ParentBusinessUnitID
                AND     (@Filter IS NULL OR @Filter = '' OR UPPER(BU1.BusinessUnit) LIKE UPPER(@Filter))
                ORDER
                BY      BU1.BusinessUnit";
            
            string cleanFilter = null;

            if (!string.IsNullOrEmpty(filter) && Regex.IsMatch(filter.Trim(), "^[a-zA-Z0-9 ]*$"))
            {
                cleanFilter = filter.Trim() + "%";
            }

            IList<RowDataGatewayBinding> parameterList = CreateBindingList("ParentBusinessUnitID", dealerGroupId, DbType.Int32, false);
            parameterList.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, parameterList);
        }

        public IList<BusinessUnit> FindAllDealershipsByDealerGroupAndMember(BusinessUnit dealerGroup, Member member)
        {
            return FindAllDealershipsByDealerGroupAndMember(dealerGroup.Id, member.Id, null);
        }

        public IList<BusinessUnit> FindAllDealershipsByDealerGroupAndMember(int dealerGroupId, int memberId)
        {
            return FindAllDealershipsByDealerGroupAndMember(dealerGroupId, memberId, null);
        }

        public IList<BusinessUnit> FindAllDealershipsByDealerGroupAndMember(int dealerGroupId, int memberId, string filter)
        {
            string commandText = @"
                SELECT
            " + GetRowDataGateway().ColumnList("BU1") + @"
                FROM
		                dbo.BusinessUnit BU1
                JOIN	dbo.BusinessUnitRelationship BR1 ON BU1.BusinessUnitID = BR1.BusinessUnitID
                JOIN	dbo.BusinessUnit BU2 ON BU2.BusinessUnitID = BR1.ParentID
                JOIN	(
		                    SELECT	BusinessUnitID
		                    FROM	[IMT].dbo.MemberAccess
		                    WHERE	MemberID = @MemberID
	                    UNION ALL
		                    SELECT	B.BusinessUnitID
		                    FROM	[IMT].dbo.BusinessUnitRelationship B CROSS JOIN [IMT].dbo.Member M
		                    WHERE	B.ParentID = @DealerGroupID
		                    AND		M.MemberID = @MemberID
		                    AND		M.MemberType = 1
	                    ) MA1 ON MA1.BusinessUnitID = BU1.BusinessUnitID
                JOIN	dbo.DealerPreference DP1 ON DP1.BusinessUnitID = BU1.BusinessUnitID
                WHERE
	                    BU1.BusinessUnitTypeID = 4
                AND	    BU1.Active = 1 
                AND     DP1.GoLiveDate IS NOT NULL
                AND	    BU2.BusinessUnitID = @DealerGroupID
                AND     (@Filter IS NULL OR @Filter = '' OR UPPER(BU1.BusinessUnit) LIKE UPPER(@Filter))
                ORDER
                BY      BU1.BusinessUnit";

            string cleanFilter = null;

            if (!string.IsNullOrEmpty(filter) && Regex.IsMatch(filter.Trim(), "^[a-zA-Z0-9 ]*$"))
            {
                cleanFilter = filter.Trim() + "%";
            }

            IList<RowDataGatewayBinding> parameterList = CreateBindingList("DealerGroupID", dealerGroupId, DbType.Int32, false);
            parameterList.Add(new RowDataGatewayBinding("MemberID", memberId, DbType.Int32, false));
            parameterList.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, parameterList);
        }

        public IList<BusinessUnit> FindAllDealershipsByDealerGroupAndMember(int dealerGroupId, string login, string filter=null)
        {
            string commandText = @"
                SELECT
            " + GetRowDataGateway().ColumnList("BU1") + @"
                FROM
		                dbo.BusinessUnit BU1
                JOIN	dbo.BusinessUnitRelationship BR1 ON BU1.BusinessUnitID = BR1.BusinessUnitID
                JOIN	dbo.BusinessUnit BU2 ON BU2.BusinessUnitID = BR1.ParentID
                JOIN	(
		                    SELECT	BusinessUnitID
		                    FROM	[IMT].dbo.MemberAccess MA
                            JOIN    [IMT].dbo.Member M ON M.MemberID = MA.MemberID
		                    WHERE	M.Login = @Login
	                    UNION ALL
		                    SELECT	B.BusinessUnitID
		                    FROM	[IMT].dbo.BusinessUnitRelationship B CROSS JOIN [IMT].dbo.Member M
		                    WHERE	B.ParentID = @DealerGroupID
		                    AND		M.Login = @Login
		                    AND		M.MemberType = 1
	                    ) MA1 ON MA1.BusinessUnitID = BU1.BusinessUnitID
                JOIN	dbo.DealerPreference DP1 ON DP1.BusinessUnitID = BU1.BusinessUnitID
                WHERE
	                    BU1.BusinessUnitTypeID = 4
                AND	    BU1.Active = 1 
                AND     DP1.GoLiveDate IS NOT NULL
                AND	    BU2.BusinessUnitID = @DealerGroupID
                AND     (@Filter IS NULL OR @Filter = '' OR UPPER(BU1.BusinessUnit) LIKE UPPER(@Filter))
                ORDER
                BY      BU1.BusinessUnit";

            string cleanFilter = null;

            if (!string.IsNullOrEmpty(filter) && Regex.IsMatch(filter.Trim(), "^[a-zA-Z0-9 ]*$"))
            {
                cleanFilter = filter.Trim() + "%";
            }

            IList<RowDataGatewayBinding> parameterList = CreateBindingList("DealerGroupID", dealerGroupId, DbType.Int32, false);
            parameterList.Add(new RowDataGatewayBinding("Login", login, DbType.String, false));
            parameterList.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, parameterList);
        }

        public BusinessUnit FindDealerGroupByUserName(string userName)
        {
            return First(FindDealerGroupsByUserNameAndMemberType(userName, MemberType.User, null));
        }

        public IList<BusinessUnit> FindDealerGroupsByUserNameAndMemberType(string userName, MemberType memberType, string filter)
        {
            string commandText =
                @"
                    WITH UserDealerGroups AS
                    (
                        SELECT	M1.Login, BU2.BusinessUnitID
                        FROM	dbo.BusinessUnit BU1
                        JOIN	dbo.BusinessUnitRelationship BR1 ON BU1.BusinessUnitID = BR1.BusinessUnitID
                        JOIN	dbo.BusinessUnit BU2 ON BU2.BusinessUnitID = BR1.ParentID
                        JOIN	dbo.MemberAccess MA1 ON MA1.BusinessUnitID = BU1.BusinessUnitID
                        JOIN	dbo.Member M1 ON MA1.MemberID = M1.MemberID
                        JOIN	dbo.DealerPreference DP1 ON DP1.BusinessUnitID = BU1.BusinessUnitID
                        WHERE
                                BU1.BusinessUnitTypeID = 4
                        AND     BU2.BusinessUnitTypeID = 3
                        AND	    BU1.Active = 1
                        AND     DP1.GoLiveDate IS NOT NULL
                        AND		M1.MemberType = @MemberType
                        AND     (@Filter IS NULL OR @Filter = '' OR UPPER(BU2.BusinessUnit) LIKE UPPER(@Filter))
                        GROUP
                        BY		M1.Login, BU2.BusinessUnitID
                    )
                    SELECT "
                + GetRowDataGateway().ColumnList("B") + @"
                    FROM    dbo.BusinessUnit B
                    JOIN    UserDealerGroups G ON B.BusinessUnitID = G.BusinessUnitID
                    WHERE   G.Login = @Login
                ";

            string cleanFilter = null;

            if (!string.IsNullOrEmpty(filter) && Regex.IsMatch(filter.Trim(), "^[a-zA-Z0-9 ]*$"))
            {
                cleanFilter = filter.Trim() + "%";
            }

            IList<RowDataGatewayBinding> bindings = CreateBindingList("Login", userName, DbType.String, false);
            bindings.Add(new RowDataGatewayBinding("MemberType", (int) memberType, DbType.Int32, false));
            bindings.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, bindings);
        }

        public IList<BusinessUnit> FindAllDealerGroups(string filter)
        {
            string commandText = @"
                SELECT 
            " + GetRowDataGateway().ColumnList("B") + @"
                FROM
                        dbo.BusinessUnit B
                JOIN    (
                            SELECT  B.BusinessUnitID, COUNT(*) NumberOfDealerships
                            FROM    BusinessUnit B
                            JOIN    BusinessUnitRelationship R ON B.BusinessUnitID = R.ParentID
                            JOIN    BusinessUnit C ON C.BusinessUnitID = R.BusinessUnitID
                            JOIN    DealerPreference D ON D.BusinessUnitID = C.BusinessUnitID
			                JOIN	(
						                SELECT	BusinessUnitID, COUNT(*) NumberOfMembers
						                FROM	MemberAccess M
						                GROUP
						                BY		BusinessUnitID
						                HAVING	COUNT(*) > 0
					                ) E ON E.BusinessUnitID = C.BusinessUnitID
                            WHERE   C.Active = 1
                            AND     C.BusinessUnitTypeID = 4
                            AND     B.Active = 1
                            AND     B.BusinessUnitTypeID = 3 
                            AND     D.GoLiveDate IS NOT NULL
                            AND     (@Filter IS NULL OR @Filter = '' OR UPPER(B.BusinessUnit) LIKE UPPER(@Filter))
                            GROUP
                            BY      B.BusinessUnitID
                            HAVING  COUNT(*) > 0
                        ) X ON X.BusinessUnitID = B.BusinessUnitID
                WHERE
                        B.BusinessUnitTypeID = 3
                AND	    B.Active = 1
                ORDER
                BY      B.BusinessUnit";

            string cleanFilter = null;

            if (!string.IsNullOrEmpty(filter) && Regex.IsMatch(filter.Trim(), "^[a-zA-Z0-9 ]*$"))
            {
                cleanFilter = filter.Trim() + "%";
            }

            return Find(commandText, CreateBindingList("Filter", cleanFilter, DbType.String, true));
        }

        #region CommandCenter/Administration/ChooseDestination.aspx

        public ICollection<BusinessUnit> FindAllDealerGroupsForMember(Member member, string filter)
        {
            if (member.MemberType.Equals(MemberType.Administrator))
            {
                return FindAllDealerGroups(filter);
            }
            else
            {
                return FindDealerGroupsByUserNameAndMemberType(member.UserName, member.MemberType, filter);
            }
        }

        public ICollection<BusinessUnit> FindAllDealersForMemberAndDealerGroup(Member member, int dealerGroupId, string filter)
        {
            if (member.MemberType.Equals(MemberType.Administrator))
            {
                return FindAllDealershipsByDealerGroup(dealerGroupId, filter);
            }
            else
            {
                return FindAllDealershipsByDealerGroupAndMember(dealerGroupId, member.Id, filter);
            }
        } 

        #endregion
    }
}
