using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable]
    public class Owner : ReadOnlyBase<Owner>
    {
        #region Business Methods

        private string _handle;
        private string _name;
        private int _id;
        private int _ownerEntityId;
        private OwnerEntityType _ownerEntityType;

        public OwnerEntityType OwnerEntityType
        {
            get { return _ownerEntityType; }
        }

        public int OwnerEntityId
        {
            get { return _ownerEntityId; }
        }

        public int Id
        {
            get { return _id; }
        }

        public string Handle
        {
            get { return _handle; }
        }

        public string Name
        {
            get { return _name; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Factory Methods

        private Owner()
        {
            /* force use of factory methods */
        }

        public static Owner GetOwner(string handle)
        {
            if (string.IsNullOrEmpty(handle))
            {
                throw new ArgumentNullException("handle");
            }
            return DataPortal.Fetch<Owner>(new HandleCriteria(handle));
        }

        public static Owner GetOwner(int dealerId)
        {
            return DataPortal.Fetch<Owner>(new IdCriteria(dealerId));
        }

        [Serializable]
        public class DataSource
        {
            public Owner Select(string handle)
            {
                return GetOwner(handle);
            }
        }

        #endregion

        #region Data Access

        protected virtual void DataPortal_Fetch(HandleCriteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.Owner#FetchByHandle";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.Handle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new Exception("No such Owner!");
                        }
                    }
                }
            }
        }

        protected virtual void DataPortal_Fetch(IdCriteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.Owner#FetchByDealerId";
                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new Exception("No such Owner!");
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            _id = record.GetInt32(record.GetOrdinal("Id"));
            _handle = record.GetString(record.GetOrdinal("Handle"));
            _name = record.GetString(record.GetOrdinal("Name"));
            _ownerEntityId = record.GetInt32(record.GetOrdinal("OwnerEntityId"));
            _ownerEntityType =
                (OwnerEntityType)
                Enum.ToObject(typeof (OwnerEntityType), record.GetInt32(record.GetOrdinal("OwnerEntityTypeId")));
        }

        [Serializable]
        protected class HandleCriteria
        {
            private readonly string _handle;

            public HandleCriteria(string handle)
            {
                _handle = handle;
            }

            public string Handle
            {
                get { return _handle; }
            }
        }

        [Serializable]
        protected class IdCriteria
        {
            private readonly int _id;

            public IdCriteria(int id)
            {
                _id = id;
            }

            public int Id
            {
                get { return _id; }
            }
        }

        #endregion
    }
}