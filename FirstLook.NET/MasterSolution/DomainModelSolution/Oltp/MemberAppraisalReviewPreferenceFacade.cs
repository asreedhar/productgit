namespace FirstLook.DomainModel.Oltp
{
    public static class MemberAppraisalReviewPreferenceFacade
    {
        private static readonly MemberAppraisalReviewPreferenceFinder Finder = MemberAppraisalReviewPreferenceFinder.Instance();

        public static MemberAppraisalReviewPreference FindOrCreate(string userName, BusinessUnit businessUnit)
        {
            Member member = MemberFinder.Instance().FindByUserName(userName);
            MemberAppraisalReviewPreference pref = Finder.Find(member.Id, businessUnit.Id);

            if (pref == null)
            {
                pref = new MemberAppraisalReviewPreference(member, businessUnit);
                pref.Save();
            }

            return pref;
        }
    }
}
