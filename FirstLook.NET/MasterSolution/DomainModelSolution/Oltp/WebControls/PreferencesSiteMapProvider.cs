using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using System.Security.Principal;
using System.Web;

namespace FirstLook.DomainModel.Oltp.WebControls
{
    class PreferencesSiteMapProvider : StaticSiteMapProvider
    {

        private readonly string[] AllRoles = new string[] { "*" };

		private SiteMapNode rootNode;

		[MethodImpl(MethodImplOptions.Synchronized)]
		public override SiteMapNode BuildSiteMap()
		{
			lock (this)
			{
                if (rootNode != null)
                    return rootNode;

				SiteMapNode tmpRootNode = new SiteMapNode(this,
														  "RootNode",
														  null,
														  "Root Node",
														  "The Root Node of the SiteMap but is not part of the navigation");
				tmpRootNode.Roles = AllRoles;

				SiteMapNode targets =
                    new SiteMapNode(this, "Targets", "/preferences/targets/appraisal_closing_rate", "Targets");
				targets.Roles = AllRoles;
				AddNode(targets, tmpRootNode);


			    SiteMapNode makeADeal =
			        new HasUpgradeSiteMapNode(this, "Make a Deal", "/preferences/make_a_deal/preferences", "Make a Deal",
			                                  string.Empty, true, 21, false);
                makeADeal.Roles = AllRoles;
                AddNode(makeADeal, tmpRootNode);

                // The #DefaultPage is needed to prevent duplicate URL's in the SiteMapNode, PM
			    SiteMapNode internetAdvertisingAccelerator =
                    new OwnerHandleSiteMapNode(this, "Internet Advertising Accelerator", "/pricing/Pages/Preferences/",
                                    "Internet Advertising Accelerator", true, 19, false);
			    internetAdvertisingAccelerator.Roles = AllRoles;
                AddNode(internetAdvertisingAccelerator, tmpRootNode);

			    AddNodes(new SiteMapNode[]
			                 {
                                 new OwnerHandleSiteMapNode(this, "Advertisement Generator",
			                                     "/pricing/Pages/Preferences/AdvertisementGenerator.aspx",
			                                     "Advertisement Generator", true, 19, false),
                                 new OwnerHandleSiteMapNode(this, "Snippet Library",
			                                     "/pricing/Pages/Preferences/TextFragments.aspx",
			                                     "Snippet Library", true, 19, false),
                                 new OwnerHandleSiteMapNode(this, "Equipment Settings",
			                                     "/pricing/Pages/Preferences/EquipmentSettings.aspx",
			                                     "Equipment Settings", true, 19, false)
			                 },     internetAdvertisingAccelerator);

                SiteMapNode marketing = new OwnerHandleSiteMapNode(this, "MAX Merchandising",
                                                               "/pricing/Pages/MaxMarketing/Preferences/Dealer/Default.aspx",
                                                               "MAX Merchandising", true, 23, false)
                                            {Roles = AllRoles};
			    AddNode(marketing, tmpRootNode);

			    AddNodes(new SiteMapNode[]
			                 {
			                     new OwnerHandleSiteMapNode(this, "Equipment",
			                                                "/pricing/Pages/MaxMarketing/Preferences/Dealer/EquipmentPreferences.aspx",
			                                                "Equipment", true, 23, false),
                                 new OwnerHandleSiteMapNode(this, "Consumer Highlights",
                                                            "/pricing/Pages/MaxMarketing/Preferences/Dealer/ConsumerHighlightsDealerPreferences.aspx",
                                                            "Consumer Highlights", true, 23, false),
                                 new OwnerHandleSiteMapNode(this, "360&deg; Pricing Analysis",
			                                                "/pricing/Pages/MaxMarketing/Preferences/Dealer/MarketAnalysisDealerPreferences.aspx",
			                                                "360&deg; Pricing Analysis", true, 23, false),
                                 new OwnerHandleSiteMapNode(this, "Market Listings",
			                                                "/pricing/Pages/MaxMarketing/Preferences/Dealer/MarketListingDealerPreference.aspx",
			                                                "Market Listings", true, 23, false),
                                 new OwnerHandleSiteMapNode(this, "Additional Settings",
			                                                "/pricing/Pages/MaxMarketing/Preferences/Dealer/DealerGeneralPreferences.aspx",
			                                                "Additional Settings", true, 23, false)
			                 }, marketing);

                SiteMapNode cpo = new OwnerHandleSiteMapNode(this, "Certified Program",
                                                             "/CertifiedProgram/Dealer/Pages/",
                                                             "Certified Program", true, null, false);
			    cpo.Roles = AllRoles;
                AddNode(cpo, tmpRootNode);

                AddNodes(new SiteMapNode[]
			                 {
								 // public OwnerHandleSiteMapNode(SiteMapProvider provider, string key, string url, string title, bool needsNormalUser, int? needsUpgrade, bool needsStores)
			                     new OwnerHandleSiteMapNode(this, "Manufacturer Certified",
			                                                "/CertifiedProgram/Dealer/Pages/CertifiedPrograms.aspx",
			                                                "Manufacturer Certified", true, null, false),
			                     new OwnerHandleSiteMapNode(this, "Dealer Certified",
			                                                "/CertifiedProgram/Dealer/Pages/DealerCertifiedPrograms.aspx",
			                                                "Dealer Certified", true, null, false)
			                 }, cpo);

				rootNode = tmpRootNode;

				return rootNode;
			}
		}

		protected override SiteMapNode GetRootNodeCore()
		{
			lock (this)
			{
				if (rootNode == null)
				{
					BuildSiteMap();
				}
				return rootNode;
			}
		}

        private void AddNodes(IEnumerable<SiteMapNode> nodes, SiteMapNode parent)
        {
            foreach (SiteMapNode node in nodes)
            {
                node.Roles = AllRoles;
                AddNode(node, parent);
            }
        }

        class OwnerHandleSiteMapNode : SiteMapNode
        {
            private readonly bool needsNormalUser;
            private readonly int? needsUpgrade;
            private readonly bool needsStores;
            private string ownerHandle;

            public OwnerHandleSiteMapNode(SiteMapProvider provider, string key, string url, string title, bool needsNormalUser, int? needsUpgrade, bool needsStores)
                : base(provider, key, url, title)
            {
                this.needsNormalUser = needsNormalUser;
                this.needsUpgrade = needsUpgrade;
                this.needsStores = needsStores;
            }

            public override bool IsAccessibleToUser(HttpContext context)
            {
                bool isAccessible =  base.IsAccessibleToUser(context);

                if (isAccessible)
                {
                    ownerHandle = context.Request.Params["oh"];

                    if (string.IsNullOrEmpty(ownerHandle))
                    {
                        isAccessible = false;
                    }
                }

                if (isAccessible)
                {
                    SoftwareSystemComponentState state = (SoftwareSystemComponentState)context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];
                    Member member;
                    BusinessUnit dealer;

                    if (state != null)
                    {
                        dealer = state.Dealer.GetValue();
                        member = state.DealerGroupMember();
                    }
                    else
                    {
                        dealer = (BusinessUnit)context.Items[BusinessUnit.HttpContextKey];
                        member = (Member)context.Items[MemberFacade.HttpContextKey];
                    }

                    if (needsStores)
                    {
                        IPrincipal user = context.User;
                        isAccessible &= (
                                        user.IsInRole("Administator") ||
                                        user.IsInRole("AccountRepresentative") ||
                                        (member != null && member.Dealers.Count > 1)
                                    );
                    }

                    if (needsNormalUser)
                    {
                        isAccessible &= (member != null && !member.HasRestrictedAccess());
                    }

                    if (needsUpgrade.HasValue)
                    {
                        isAccessible &= (dealer != null && dealer.HasDealerUpgrade((Upgrade)needsUpgrade.Value));
                    }
                }

                return isAccessible;
            }

            public override string Url
            {
                get
                {
                    string returnValue = base.Url;
                    if (!string.IsNullOrEmpty(ownerHandle))
                    {
                        string parameterSeperator = returnValue.Contains("?") ? "&" : "?";

                        if (returnValue.Contains("#"))
                        {
                            int indexOfPound = base.Url.IndexOf("#", StringComparison.OrdinalIgnoreCase);
                            returnValue = returnValue.Substring(0, indexOfPound) + parameterSeperator + "oh=" +
                                          ownerHandle +
                                          returnValue.Substring(indexOfPound, returnValue.Length - indexOfPound);
                        }
                        else
                        {
                            returnValue += parameterSeperator + "oh=" + ownerHandle;
                        }    
                    }

                    return returnValue;
                }
                set
                {
                    base.Url = value;
                }
            }
        }

        class HasUpgradeSiteMapNode : SiteMapNode
		{
			private readonly bool needsNormalUser;
			private readonly int? needsUpgrade;
			private readonly bool needsStores;

			public HasUpgradeSiteMapNode(SiteMapProvider provider, string name, string url, bool needsNormalUser, int? needsUpgrade, bool needsStores)
				: this(provider, name, url, name, name, needsNormalUser, needsUpgrade, needsStores)
			{
			}

			public HasUpgradeSiteMapNode(SiteMapProvider provider, string key, string url, string title, string description, bool needsNormalUser, int? needsUpgrade, bool needsStores)
				: base(provider, key, url, title, description)
			{
				this.needsNormalUser = needsNormalUser;
				this.needsUpgrade = needsUpgrade;
				this.needsStores = needsStores;
			}

			public override bool IsAccessibleToUser(HttpContext context)
			{
				bool accessible = base.IsAccessibleToUser(context);

				if (accessible)
				{
					SoftwareSystemComponentState state = (SoftwareSystemComponentState)context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];
					Member member;
					BusinessUnit dealer;

					if (state != null)
					{
						dealer = state.Dealer.GetValue();
						member = state.DealerGroupMember();
					}
					else
					{
						dealer = (BusinessUnit)context.Items[BusinessUnit.HttpContextKey];
						member = (Member)context.Items[MemberFacade.HttpContextKey];
					}

					if (needsStores)
					{
						IPrincipal user = context.User;
						accessible &= (
										user.IsInRole("Administator") ||
										user.IsInRole("AccountRepresentative") ||
										(member != null && member.Dealers.Count > 1)
									);
					}

					if (needsNormalUser)
					{
						accessible &= (member != null && !member.HasRestrictedAccess());
					}

					if (needsUpgrade.HasValue)
					{
						accessible &= (dealer != null && dealer.HasDealerUpgrade((Upgrade) needsUpgrade.Value));
					}
				}

				return accessible;
			}
		}        
    }
}
