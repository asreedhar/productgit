using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace FirstLook.DomainModel.Oltp.WebControls
{
    public class HedgehogTracking : Control 
    {
        /// <summary>
        /// When true the hedgehog bootstrap script is included.  The bootstrap script populates the
        /// member id from a hidden form field named 'system_identifier'. Is false by default.
        /// </summary>
        [Category("Behavior"), DefaultValue(false)]
        public bool Bootstrap
        {
            get
            {
                object value = ViewState["Bootstrap"];
                if (value == null)
                    return false;
                return (bool) value;
            }
            set
            {
                if (Bootstrap != value)
                    ViewState["Bootstrap"] = value;
            }
        }

        /// <summary>
        /// When true the hedgehog script to enhance all page elements is included.  Otherwise, the
        /// enhance-by-attribute hedgehog script is included.  Is true by default.
        /// </summary>
        [Category("Behavior"), DefaultValue(true)]
        public bool EnhanceAll
        {
            get
            {
                object value = ViewState["EnhanceAll"];
                if (value == null)
                    return true;
                return (bool) value;
            }
            set
            {
                if (Bootstrap != value)
                    ViewState["EnhanceAll"] = value;
            }
        }

        /// <summary>
        /// The member identifier of the authenticated user.
        /// </summary>
        private int? MemberID
        {
            get
            {
                return (int?) ViewState["MemberID"];
            }
            set
            {
                if (Nullable.Compare(MemberID,value) != 0)
                {
                    ViewState["MemberID"] = value;
                }
            }
        }

        /// <summary>
        /// Find or retrieve the member identifier when the control is first loaded.
        /// </summary>
        /// <param name="e"></param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Failure to find a user is not fatal")]
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Page.IsPostBack)
            {
                if (!MemberID.HasValue)
                {
                    Member member = Context.Items[MemberFacade.HttpContextKey] as Member;

                    if (member == null)
                    {
                        SoftwareSystemComponentState state =
                            Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] as
                            SoftwareSystemComponentState;

                        if (state != null)
                        {
                            member = state.AuthorizedMember.GetValue();
                        }
                    }

                    if (member == null)
                    {
                        if (!string.IsNullOrEmpty(Context.User.Identity.Name))
                        {
                            try
                            {
                                member = MemberFinder.Instance().FindByUserName(Context.User.Identity.Name);

                                Context.Items[MemberFacade.HttpContextKey] = member;
                            }
                            catch
                            {
                                // ignore the error
                            }
                        }
                    }

                    if (member != null)
                    {
                        MemberID = member.Id;
                    }
                }
            }
        }

        /// <summary>
        /// Add the hedgehog scripts to the HTML HEAD element.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Bootstrap)
                AddJavaScript("/hedgehog/hedgehog-bootstrap.js");

            AddJavaScript("/hedgehog/hedgehog.js");

            if (EnhanceAll)
                AddJavaScript("/hedgehog/hedgehog-enhance-all.js");
            else
                AddJavaScript("/hedgehog/hedgehog-enhance-attribute.js");

            if (MemberID.HasValue)
            {
                HtmlGenericControl control = new HtmlGenericControl("script");
                control.Attributes["type"] = "text/javascript";
                control.InnerHtml = string.Format(CultureInfo.InvariantCulture, "\n<!--\nif (typeof(Hedgehog) == 'object') {{ Hedgehog.SystemIdentifier = {0}; }}\n// -->\n", MemberID.Value);

                Page.Header.Controls.Add(control);
            }
        }

        private void AddJavaScript(string src)
        {
            HtmlGenericControl script = new HtmlGenericControl("script");
            script.Attributes["src"] = src;
            script.Attributes["language"] = "javascript";
            script.Attributes["type"] = "text/javascript";

            Page.Header.Controls.Add(script);
        }
    }
}