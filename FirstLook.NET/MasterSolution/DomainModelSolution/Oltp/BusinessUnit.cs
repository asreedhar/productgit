using System;
using System.Collections.Generic;
using System.Data;

using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, TableAttribute(Database = "IMT", Name = "BusinessUnit")]
    public class BusinessUnit : ActiveRecordBase<BusinessUnit>
    {
        public static readonly string HttpContextKey = "BusinessUnit";

        #region Properties
        private int id;
        private string name;
        private string shortName;
        private string zipCode;
        private BusinessUnitType businessUnitType;
        private bool active;
        private string businessUnitCode;

        [IdAttribute, ColumnAttribute(Name = "BusinessUnitID", DbType = DbType.Int32)]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "BusinessUnit", DbType = DbType.String)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [ColumnAttribute(Name = "BusinessUnitShortName", DbType = DbType.String)]
        public string ShortName
        {
            get { return shortName; }
            set { shortName = value; }
        }

        [ColumnAttribute(Name = "ZipCode", DbType = DbType.String, Nullable=true)]
        public string ZipCode
        {
            get { return zipCode;  }
            set { zipCode = value; }
        }

        [ColumnAttribute(Name = "BusinessUnitTypeID", DbType = DbType.Int32)]
        public BusinessUnitType BusinessUnitType
        {
            get { return businessUnitType; }
            set { businessUnitType = value; }
        }

        [ColumnAttribute(Name = "Active", DbType = DbType.Boolean, SelectCast = "CAST(Active AS BIT)", UpdateCast = "CAST(Active AS TINYINT)")]
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        [ColumnAttribute(Name = "BusinessUnitCode", DbType = DbType.String)]
        public string BusinessUnitCode
        {
            get { return businessUnitCode; }
            set { businessUnitCode = value; }
        }

        #endregion
        
        #region Associations
        private readonly ManyToManyCollection<BusinessUnit, DealerUpgrade> dealerUpgrades;

        public ICollection<DealerUpgrade> DealerUpgrades
        {
            get { return dealerUpgrades; }
        }

        

        #endregion

        #region Constructors
        public BusinessUnit()
        {
            dealerUpgrades = new DealerUpgradeCollection(this, "DealerUpgrade");
        }
        #endregion

        protected override BusinessUnit This
        {
            get { return this; }
        }

        public IList<BusinessUnit> Dealerships()
        {
            return BusinessUnitFinder.Instance().FindAllDealershipsByDealerGroup(Id);
        }

        public BusinessUnit DealerGroup()
        {
            return BusinessUnitFinder.Instance().FindDealerGroupByDealership(Id);
        }

        public bool HasDealerUpgrade(Upgrade dealerUpgradeCode)
        {
            bool hasDealerUpgrade = false;

            foreach (DealerUpgrade dealerUpgrade in DealerUpgrades)
            {
                hasDealerUpgrade |= dealerUpgrade.Equals(dealerUpgradeCode);
            }

            return hasDealerUpgrade;
        }

        public DealerPreference DealerPreference
        {
            get
            {
                if (_dealerPreference == null)
                {
                    _dealerPreference = new DealerPreference(id);                       
                }
                return _dealerPreference;
            }
        }
        private DealerPreference _dealerPreference;

        [Serializable]
        class DealerUpgradeCollection : ManyToManyCollection<BusinessUnit,DealerUpgrade>
        {
            public DealerUpgradeCollection(BusinessUnit dealer, string tableName)
                : base(dealer, tableName)
            {
            }

            public override bool IsReadOnly
            {
                get { return true; }
            }

            protected override string JoinStatement()
            {
                return @"
                    SELECT
                " + GetRowDataGateway().ColumnList("U") + @"
                    FROM
		                    dbo.lu_DealerUpgrade U
                    JOIN	dbo.DealerUpgrade DU ON DU.DealerUpgradeCD = U.DealerUpgradeCD
                    WHERE
                            DU.BusinessUnitID = @Value
                    AND     DU.Active = 1
                    AND     DU.EffectiveDateActive = 1
                    ORDER
                    BY      U.DealerUpgradeDESC";
            }
        }
    }
}
