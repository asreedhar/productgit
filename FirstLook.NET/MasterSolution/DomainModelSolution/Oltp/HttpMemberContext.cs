using System.Web;
using FirstLook.Common.Core.Membership;

namespace FirstLook.DomainModel.Oltp
{
    public class HttpMemberContext : IMemberContext
    {
        public IMember Current
        {
            get
            {
                if (HttpContext.Current == null)
                    return null;

                var username = HttpContext.Current.User.Identity.Name;
                return MemberFinder.Instance().FindByUserName(username);
            }
        }
    }
}