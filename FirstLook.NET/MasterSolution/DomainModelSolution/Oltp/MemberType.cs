using System;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable]
    public enum MemberType
    {
        Dummy = 0,
        Administrator = 1,
        User = 2,
        AccountRepresentative = 3,
        SalesPerson=4       
    }
}
