using System;
using System.Diagnostics.CodeAnalysis;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable]
    [SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue", Justification = "Is a reference value where zero is not-valid")]
    public enum BusinessUnitType
    {
        Corporate = 1,
        Region = 2,
        DealerGroup = 3,
        Dealer = 4,
        FirstLook = 5
    }
}
