using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, Table(Database = "IMT", Name = "MemberBusinessUnitSet")]
    public class MemberBusinessUnitSet : ActiveRecordBase<MemberBusinessUnitSet>
    {
        #region Fields
        #pragma warning disable 0169

        [ColumnAttribute(Name = "MemberID", DbType = DbType.Int32)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        
        private int memberId;
        

        [ColumnAttribute(Name = "BusinessUnitID", DbType = DbType.Int32)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        private int businessUnitId;

        #pragma warning restore 0169
        #endregion

        #region Properties
        private int? id;
        private MemberBusinessUnitSetType businessUnitSetType;

        [IdAttribute, ColumnAttribute(Name = "MemberBusinessUnitSetID", DbType = DbType.Int32)]
        public int? Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "MemberBusinessUnitSetTypeID", DbType = DbType.Int32)]
        public MemberBusinessUnitSetType BusinessUnitSetType
        {
            get { return businessUnitSetType; }
            set { businessUnitSetType = value; }
        } 
        #endregion

        #region Associations
        private readonly ManyToOne<int?, MemberBusinessUnitSet, Member> member;
        private readonly ManyToOne<int?, MemberBusinessUnitSet, BusinessUnit> businessUnit;
        private readonly ManyToManyCollection<MemberBusinessUnitSet, BusinessUnit> businessUnits;

        public ISingleValuedAssociation<BusinessUnit> BusinessUnit
        {
            get { return businessUnit; }
        }

        public ISingleValuedAssociation<Member> Member
        {
            get { return member; }
        }

        public ICollection<BusinessUnit> BusinessUnits
        {
            get { return businessUnits; }
        } 
        #endregion

        #region Constructors
        public MemberBusinessUnitSet()
        {
            member = new ManyToOne<int?, MemberBusinessUnitSet, Member>("memberId", this);
            businessUnit = new ManyToOne<int?, MemberBusinessUnitSet, BusinessUnit>("businessUnitId", this);
            businessUnits = new ManyToManyCollection<MemberBusinessUnitSet, BusinessUnit>(this, "MemberBusinessUnitSetItem");
        } 
        #endregion

        protected override MemberBusinessUnitSet This
        {
            get { return this; }
        }
    }
}
