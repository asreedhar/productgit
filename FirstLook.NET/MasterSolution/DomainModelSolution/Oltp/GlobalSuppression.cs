
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Oltp", Justification = "None: Should be renamed BusinessObjects (or the like)")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "namespace", Target = "FirstLook.DomainModel.Oltp", MessageId = "Oltp", Justification = "None: Should be renamed BusinessObjects (or the like)")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "namespace", Target = "FirstLook.DomainModel.Oltp.WebControls", MessageId = "Oltp", Justification = "None: Should be renamed BusinessObjects (or the like)")]
