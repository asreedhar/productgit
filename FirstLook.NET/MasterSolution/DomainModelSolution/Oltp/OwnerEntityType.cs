using System;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable]
    public enum OwnerEntityType
    {
        NotDefined,
        Dealer,
        Seller
    }
}