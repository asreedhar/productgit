using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    internal class LicenseFinder : Finder<License>
    {
        private static readonly LicenseFinder finder = new LicenseFinder();

        private LicenseFinder()
        {
        }

        public static LicenseFinder Instance()
        {
            return finder;
        }

        public License FindByName(string name)
        {
            License license = FindFirst(CreateWhereClause("Name", name));
            if(license == null)
            {
                throw new ActiveRecordException("The license " + name + " does not exist in the system.");
            }
            return license;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "None: Confirm with Benson we can delete this method.")]
        public License FindForMember(string licenseName, int memberId)
        {
            string commandText = @"
                SELECT MLA.LicenseID, L.Name
                FROM dbo.MemberLicenseAcceptance MLA
                JOIN dbo.License L
                    ON MLA.LicenseID = L.LicenseID
                WHERE L.Name = @Name
                AND MLA.MemberID = @MemberID
            ";

            IList<RowDataGatewayBinding> bindingList = CreateBindingList("MemberID", memberId, DbType.Int32, false);
            bindingList.Add(new RowDataGatewayBinding("Name", licenseName, DbType.String, true));

            IList<License> licenses = Find(commandText, bindingList);

            License license = null;
            foreach (License li in licenses)
            {
                license = li;
            }

            return license;
        }

    }
}
