namespace FirstLook.DomainModel.Oltp
{
    /// <summary>
    /// A utility class for license handling.
    /// </summary>
    public static class LicenseFacade
    {
        private static readonly LicenseFinder finder = LicenseFinder.Instance();

        public static void AcceptLicense(string licenseName, string userName)
        {
            License license = finder.FindByName(licenseName);
            Member member = MemberFacade.FindByUserName(userName);
            license.Accept(member);
        }

        public static bool HadAcceptedLicense(string licenseName, string userName)
        {
            License license = finder.FindByName(licenseName);
            Member member = MemberFacade.FindByUserName(userName);
            return license.IsAcceptedBy(member);
        }
    }
}
