using System.Collections.Generic;
using System.Data;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    public class MemberBusinessUnitSetFinder : Finder<MemberBusinessUnitSet>
    {
        private static readonly MemberBusinessUnitSetFinder finder = new MemberBusinessUnitSetFinder();

        private MemberBusinessUnitSetFinder()
        {
        }

        public static MemberBusinessUnitSetFinder Instance()
        {
            return finder;
        }

        public MemberBusinessUnitSet FindByBusinessUnitAndMemberAndSetType(MemberBusinessUnitSetType type, BusinessUnit businessUnit, Member member)
        {
            IDictionary<string, object> whereClause = CreateWhereClause("businessUnitId", businessUnit.Id);
            whereClause.Add("memberId", member.Id);
            whereClause.Add("BusinessUnitSetType", type);
            return FindFirst(whereClause);
        }

        public MemberBusinessUnitSet FindByUserNameAndSetType(string userName, MemberBusinessUnitSetType type)
        {
            string commandText = @"
                SELECT  " + GetRowDataGateway().ColumnList("S") + @"
                FROM    dbo.MemberBusinessUnitSet S
                JOIN    dbo.Member M ON M.MemberID = S.MemberID
                WHERE   M.Login = @Login
                AND     S.MemberBusinessUnitSetTypeID = @MemberBusinessUnitSetTypeID
            ";

            IList<RowDataGatewayBinding> bindings = CreateBindingList("Login", userName, DbType.String, false);
            bindings.Add(new RowDataGatewayBinding("MemberBusinessUnitSetTypeID", type, DbType.Int32, false));

            return First(Find(commandText, bindings));
        }
    }
}
