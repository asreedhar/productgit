using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    public class DealerGroupPreferenceFinder : Finder<DealerGroupPreference>
    {
        private static readonly DealerGroupPreferenceFinder finder = new DealerGroupPreferenceFinder();

        private DealerGroupPreferenceFinder()
        {
        }

        public static DealerGroupPreferenceFinder Instance()
        {
            return finder;
        }

        public DealerGroupPreference FindByDealerGroup(int dealerGroupId)
        {
            return First(Find(CreateWhereClause("DealerGroupId", dealerGroupId)));
        }
    }
}
