using System;
using System.Data;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, TableAttribute(Database = "FLDW", Name = "Period_D")]
    public class ReportingPeriod : ActiveRecordBase<ReportingPeriod>
    {
        #region Properties
        private int id;
        private string name;

        [IdAttribute, ColumnAttribute(Name = "PeriodId", DbType = DbType.Int32)]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "Description", DbType = DbType.String)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion

        protected override ReportingPeriod This
        {
            get { return this; }
        }
    }
}
