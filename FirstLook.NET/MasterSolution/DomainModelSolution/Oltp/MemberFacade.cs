namespace FirstLook.DomainModel.Oltp
{
    public static class MemberFacade
    {
        public static readonly string HttpContextKey = "Member";

        private static readonly MemberFinder Finder = MemberFinder.Instance();

        public static Member FindByUserName(string userName)
        {
            return Finder.FindByUserName(userName);
        }
    }
}
