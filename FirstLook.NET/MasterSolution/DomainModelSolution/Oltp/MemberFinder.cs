using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    public class MemberFinder : Finder<Member>
    {
        private static readonly MemberFinder finder = new MemberFinder();

        /*A private constructor would make sense here since this is following the singleton pattern but
        *a public constructor is needed for ObjectDataSource which uses this object.
        */
        public MemberFinder()
        {
        }

        public static MemberFinder Instance()
        {
            return finder;
        }

        public Member Find(int id)
        {
            return FindFirst(CreateWhereClause("Id", id));
        }

        public Member FindByUserName(string userName)
        {
            return FindFirst(CreateWhereClause("UserName", userName));
        }

        public Member FindByEmailAddress(string emailAddress)
        {
            return FindFirst(CreateWhereClause("EmailAddress", emailAddress));
        }

        public Member FindByUserName(string userName, bool allowImpersonation, string softwareSystemComponentToken)
        {
            Member member = FindByUserName(userName);

            if (allowImpersonation)
            {
                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(softwareSystemComponentToken);

                if (member.MemberType.Equals(MemberType.Administrator) || member.MemberType.Equals(MemberType.AccountRepresentative))
                {
                    SoftwareSystemComponentState componentState = SoftwareSystemComponentStateFinder.Instance().FindByAuthorizedMemberAndSoftwareSystemComponent(member, component);

                    if (componentState.Member.GetValue() != null)
                    {
                        member = componentState.Member.GetValue();
                    }
                }
            }

            return member;
        }

        public IList<Member> FindAllByDealerGroup(int dealerGroupId, string filter)
        {
            string commandText = @"
                SELECT DISTINCT
            " + GetRowDataGateway().ColumnList("M") + @"
                FROM
		                dbo.Member M
                JOIN	dbo.MemberAccess A ON A.MemberID = M.MemberID
                JOIN    dbo.BusinessUnit BU ON A.BusinessUnitID = BU.BusinessUnitID
                JOIN    dbo.BusinessUnitRelationship BR ON BR.BusinessUnitID = BU.BusinessUnitID
                WHERE
                	    BR.ParentID = @BusinessUnitID
                AND     (@Filter IS NULL OR @Filter = '' OR UPPER(M.Login) LIKE UPPER(@Filter))
                ORDER
                BY      M.FirstName, M.LastName";

            IList<RowDataGatewayBinding> bindingList = CreateBindingList("BusinessUnitID", dealerGroupId, DbType.Int32, false);
            bindingList.Add(new RowDataGatewayBinding("Filter", CleanFilter(filter), DbType.String, true));

            return Find(commandText, bindingList);
        }

        public IList<Member> FindAllUsersForCriteria(string firstNameFilter, string lastNameFilter, string userNameFilter)
        {
            string commandText = @"
                SELECT
            " + GetRowDataGateway().ColumnList("M") + @"
                FROM
                        dbo.Member M
                WHERE
                        M.MemberType = 2
                AND     (@FirstNameFilter IS NULL OR @FirstNameFilter = '' OR UPPER(M.FirstName) LIKE UPPER(@FirstNameFilter))
                AND     (@LastNameFilter IS NULL OR @LastNameFilter = '' OR UPPER(M.LastName) LIKE UPPER(@LastNameFilter))
                AND     (@LoginFilter IS NULL OR @LoginFilter = '' OR UPPER(M.Login) LIKE UPPER(@LoginFilter))
                ORDER
                BY      M.FirstName, M.LastName";

            IList<RowDataGatewayBinding> bindingList = CreateBindingList("FirstNameFilter", CleanFilter(firstNameFilter), DbType.String, true);
            bindingList.Add(new RowDataGatewayBinding("LastNameFilter", CleanFilter(lastNameFilter), DbType.String, true));
            bindingList.Add(new RowDataGatewayBinding("LoginFilter", CleanFilter(userNameFilter), DbType.String, true));

            return Find(commandText, bindingList);
        }

        private static string CleanFilter(string filter)
        {
            string cleanFilter = null;

            if (!string.IsNullOrEmpty(filter) && Regex.IsMatch(filter.Trim(), "^[a-zA-Z0-9 ]*$"))
            {
                cleanFilter = filter.Trim() + "%";
            }

            return cleanFilter;
        }
    }
}
