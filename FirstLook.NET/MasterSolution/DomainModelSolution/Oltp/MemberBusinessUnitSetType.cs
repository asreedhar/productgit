
using System;
using System.Diagnostics.CodeAnalysis;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable]
    [SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue", Justification = "Is a reference value collection with no zero value")]
    public enum MemberBusinessUnitSetType
    {
        CommandCenterDealerGroupSelection = 1
    }
}
