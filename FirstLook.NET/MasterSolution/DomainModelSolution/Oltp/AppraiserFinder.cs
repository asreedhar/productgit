using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    public class AppraiserFinder : Finder<Appraiser>
    {
        private static readonly AppraiserFinder finder = new AppraiserFinder();

        static AppraiserFinder()
        {
        }

        public static AppraiserFinder Instance()
        {
            return finder;
        }

        #region DealerGroupNewReport

        public IList<Appraiser> FindAppraisersByDealerId(int dealerId)
        {
            string commandText = @"
                SELECT DISTINCT tcr.AppraiserName,tcr.BusinessUnitID
                FROM [HAL].dbo.TradeClosingRateReport tcr
                INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID
                WHERE  tcr.BusinessUnitID=@BusinessUnitID
                AND  tcr.AppraiserName IS NOT NULL  AND tcr.AppraiserName<>'firstName lastName' AND tcr.AppraiserName <> '-' 
                AND  REPLACE(Tcr.AppraiserName, SUBSTRING(Tcr.AppraiserName, PATINDEX('%[^a-zA-Z0-9 '''''']%', Tcr.AppraiserName), 1), '')<>''
                AND A.AppraisalSourceID=1
                union 
                select 'No Appraiser',0                
                union 
                select 'All Appraisers',1";

            IList<RowDataGatewayBinding> parameterList = CreateBindingList("BusinessUnitID", dealerId, DbType.Int32, false);
            //parameterList.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, parameterList);
        }

        public IList<Appraiser> FindAppraiserNameForProfitabilityByAppraiser(int dealerId)
        {
            string commandText = @"
                SELECT DISTINCT AV.AppraiserName,I.BusinessUnitID
                FROM [FLDW].dbo.Inventory AS I   
                JOIN [FLDW].dbo.VehicleSale AS VS ON I.InventoryID = VS.InventoryID   AND I.InventoryType = 2
                JOIN [FLDW].dbo.Vehicle AS V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID   
                LEFT JOIN [imt].dbo.Appraisals A 
                ON I.BusinessUnitID = A.BusinessUnitID AND V.vehicleId = A.vehicleId 
                LEFT JOIN FLDW.DBO.Appraisal_F AF 
                ON I.BusinessUnitID = AF.BusinessUnitID AND V.vehicleId = AF.vehicleId  
                LEFT JOIN [FLDW].dbo.AppraisalValues AV 
                on A.AppraisalId = AV.AppraisalID And A.AppraisalID=AF.AppraisalID and  AF.Value = AV.Value 
                and AF.AppraisalValueID=AV.AppraisalValueID  
                where I.BusinessUnitID = @BusinessUnitID
                AND  AV.AppraiserName IS NOT NULL  AND AV.AppraiserName<>'firstName lastName' AND AV.AppraiserName <> '-' 
                AND  REPLACE(AV.AppraiserName, SUBSTRING(AV.AppraiserName, PATINDEX('%[^a-zA-Z0-9 '''''']%', AV.AppraiserName), 1), '')<>''
                AND I.TradeOrPurchase =2
                union 
                select 'No Appraiser',0                
                union 
                select 'All Appraisers',1";

            IList<RowDataGatewayBinding> parameterList = CreateBindingList("BusinessUnitID", dealerId, DbType.Int32, false);
            //parameterList.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, parameterList);
        }
        public IList<Appraiser> FindBuyersByDealerId(int dealerId)
        {
            string commandText = @"
               SELECT distinct (P.FirstName + ' ' + P.LastName) AppraiserName,I.BusinessUnitID 
                FROM   
                [FLDW].dbo.Inventory AS I   
                JOIN [FLDW].dbo.VehicleSale AS VS ON I.InventoryID = VS.InventoryID  
                JOIN [FLDW].dbo.Vehicle AS V ON I.BusinessUnitID = V.BusinessUnitID 
                AND I.VehicleID = V.VehicleID   
                LEFT JOIN [imt].dbo.Appraisals A 
                ON I.BusinessUnitID = A.BusinessUnitID AND V.vehicleId = A.vehicleId 
                LEFT JOIN FLDW.DBO.Appraisal_F AF 
                ON I.BusinessUnitID = AF.BusinessUnitID AND V.vehicleId = AF.vehicleId  
                LEFT JOIN [FLDW].dbo.AppraisalValues AV 
                on A.AppraisalId = AV.AppraisalID And A.AppraisalID=AF.AppraisalID and 
                AF.Value = AV.Value and AF.AppraisalValueID=AV.AppraisalValueID  
                LEFT Join imt.dbo.Person P 
                on P.PersonID=A.SelectedBuyerId
                WHERE  I.businessunitid = @BusinessUnitID
                AND I.tradeorpurchase = 1 
                AND P.FirstName IS NOT NULL 
                AND Ltrim(Rtrim(P.FirstName)) <> '' 
                AND (P.FirstName + ' ' + P.LastName) <> 'firstName lastName'
                AND ASCII(ltrim(rtrim(P.FirstName))) <> 160
                union 
                select 'No Buyer',0                
                union 
                select 'All Buyers',1";

            IList<RowDataGatewayBinding> parameterList = CreateBindingList("BusinessUnitID", dealerId, DbType.Int32, false);
            //parameterList.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, parameterList);
        }

        public IList<Appraiser> FindBumpedAppraisersByDealerId(int dealerId,int periodId)
        {
            string commandText = @"
                SELECT DISTINCT AV1.AppraiserName AS AppraiserName,
				 A.BusinessUnitId  AS BusinessUnitID                    
                from AppraisalValues AV
                INNER JOIN IMT..Appraisals A on A.AppraisalID=AV.AppraisalID
                INNER JOIN  AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID 
		             AND AV.SequenceNumber>0 AND AV1.SequenceNumber>0 
		             AND AV.Value>0
		             AND AV1.Value>0 
		             AND AV.SequenceNumber=AV1.SequenceNumber+1 AND AV.Value<>AV1.Value 
	            WHERE 
		            A.AppraisalTypeID=1 AND A.AppraisalSourceID=1 AND A.BusinessUnitID=@BusinessUnitID
                    AND A.DateCreated Between (Select StartDate from IMT.dbo.GetDatesFromPeriodId(@PeriodID,0)) and (Select EndDate from IMT.dbo.GetDatesFromPeriodId(@PeriodID,0))		           
		            AND AV1.AppraiserName IS NOT NULL 
						                             AND LTRIM(RTRIM(AV1.AppraiserName))<>'' 
						                             AND AV1.AppraiserName<>'firstName lastName' 
						                             AND ASCII(ltrim(rtrim(AV1.AppraiserName))) <> 160	 
						                 
                group by     
                AV1.AppraisalID
                ,AV1.AppraisalValueID
                ,A.BusinessUnitId,AV1.AppraiserName 
                union 
                select 'No Appraiser',0                
                union 
                select 'All Appraisers',1
                order by BusinessUnitID, AV1.AppraiserName";

            IList<RowDataGatewayBinding> parameterList = CreateBindingList("BusinessUnitID", dealerId, DbType.Int32, false);
            parameterList.Add(new RowDataGatewayBinding("PeriodID",periodId,DbType.Int32,false));
            //parameterList.Add(new RowDataGatewayBinding("Filter", cleanFilter, DbType.String, true));

            return Find(commandText, parameterList);
        }
        #endregion
    }
}
