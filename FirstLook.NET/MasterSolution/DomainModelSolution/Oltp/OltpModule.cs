using Autofac;
using FirstLook.Common.Core.Membership;

namespace FirstLook.DomainModel.Oltp
{
    public class OltpModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HttpMemberContext>().As<IMemberContext>();
        }
    }
}