﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, Table(Database = "IMT", Name = "TradeClosingRateReport")]
   public class Appraiser : ActiveRecordBase<Appraiser>
    {
         private int id;
        [IdAttribute, ColumnAttribute(Name = "BusinessUnitID", DbType = DbType.Int32)]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string appraiserName;
        [ColumnAttribute(Name = "AppraiserName", DbType = DbType.String)]
        public string AppraiserName
        {
            get { return appraiserName; }
            set { appraiserName = value; }
        }

        protected override Appraiser This
        {
            get { return this; }
        }
    }
}
