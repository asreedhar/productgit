﻿using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.DomainModel.Oltp
{    
    [Serializable]
    public class DealerPreference
    {
        public int DealerId
        {
            get { return _id; }
        }
        private readonly int _id;
        
        public bool ShowInTransitInventoryForm
        {
            get { return _showInTransitInventoryForm; }
        }
        private bool _showInTransitInventoryForm;

        public bool UseLotPrice
        {
            get { return _useLotPrice; }
        }

        private bool _useLotPrice;

        public DealerPreference(int dealerId)
        {
            _id = dealerId;
            Fetch();
        }

        private void Fetch()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT ShowInTransitInventoryForm, UseLotPrice FROM dbo.DealerPreference WHERE BusinessUnitID = " + _id;                    

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _showInTransitInventoryForm = reader.GetBoolean(reader.GetOrdinal("ShowInTransitInventoryForm"));
                            _useLotPrice = Convert.ToBoolean(reader.GetByte(reader.GetOrdinal("UseLotPrice")));
                        }
                        else
                        {
                            throw new Exception("No such dealer preference!");
                        }
                    }
                }
            }
            
        }
    }
}
