using System;
using System.Data;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, TableAttribute(Database = "IMT", Name = "License")]
    public class License : ActiveRecordBase<License>
    {
        public const string JDPowerLicenseName = "JD Power Used Car Market License";

        #region Fields

        private int id;
        private string name;

        #endregion

        #region Properties

        [IdAttribute, ColumnAttribute(Name = "LicenseID", DbType = DbType.Int32)]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "Name", DbType = DbType.String)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        #endregion

        #region Associations

        private readonly ManyToManyCollection<License, Member> members;

        #endregion

        /// <summary>
        /// Records the license as Accepted by the member.  
        /// 
        /// This method is not thread safe with IsAcceptedBy.
        /// </summary>
        /// <param name="member"></param>
        public void Accept(Member member)
        {
            if(!members.Contains(member))
                members.Add(member);
        }

        /// <summary>
        /// Finds if a member has already accepted this License.
        /// 
        /// This method is not thread safe.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public bool IsAcceptedBy(Member member)
        {
            return members.Contains(member);
        }

        public License() : base()
        {
            this.members = new ManyToManyCollection<License, Member>(this, "MemberLicenseAcceptance");
        }

        protected override License This
        {
            get { return this; }
        }
    }
}
