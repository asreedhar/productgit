using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, Table(Database = "IMT", Name = "SoftwareSystemComponentState")]
    public class SoftwareSystemComponentState : ActiveRecordBase<SoftwareSystemComponentState>
    {
        #region Fields
        #pragma warning disable 0169, 0649

        [ColumnAttribute(Name = "SoftwareSystemComponentID", DbType = DbType.Int32)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        private int? softwareSystemComponentId;

        [ColumnAttribute(Name = "AuthorizedMemberID", DbType = DbType.Int32)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        private int? authorizedMemberId;

        [ColumnAttribute(Name = "DealerGroupID", DbType = DbType.Int32)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        private int? dealerGroupId;

        [ColumnAttribute(Name = "DealerID", DbType = DbType.Int32, Nullable = true)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        private int? dealerId;

        [ColumnAttribute(Name = "MemberID", DbType = DbType.Int32, Nullable = true)]
        private int? memberId;

        #pragma warning restore 0169, 0649
        #endregion

        #region Properties
        private int? id;

        [IdAttribute, ColumnAttribute(Name = "SoftwareSystemComponentStateID", DbType = DbType.Int32)]
        public int? Id
        {
            get { return id; }
            set { id = value; }
        }
        #endregion

        #region Associations
        private readonly ManyToOne<int?, SoftwareSystemComponentState, SoftwareSystemComponent> softwareSystemComponent;
        private readonly ManyToOne<int?, SoftwareSystemComponentState, Member> authorizedMember;
        private readonly ManyToOne<int?, SoftwareSystemComponentState, BusinessUnit> dealerGroup;
        private readonly ManyToOne<int?, SoftwareSystemComponentState, BusinessUnit> dealer;
        private readonly ManyToOne<int?, SoftwareSystemComponentState, Member> member;

        public ISingleValuedAssociation<SoftwareSystemComponent> SoftwareSystemComponent
        {
            get { return softwareSystemComponent; }
        }

        public ISingleValuedAssociation<Member> AuthorizedMember
        {
            get { return authorizedMember; }
        }

        public ISingleValuedAssociation<BusinessUnit> DealerGroup
        {
            get { return dealerGroup; }
        }

        public ISingleValuedAssociation<BusinessUnit> Dealer
        {
            get { return dealer; }
        }

        public ISingleValuedAssociation<Member> Member
        {
            get { return member; }
        }
        #endregion

        public SoftwareSystemComponentState()
        {
            softwareSystemComponent = new ManyToOne<int?, SoftwareSystemComponentState, SoftwareSystemComponent>("softwareSystemComponentId", this);
            authorizedMember = new ManyToOne<int?, SoftwareSystemComponentState, Member>("authorizedMemberId", this);
            dealerGroup = new ManyToOne<int?, SoftwareSystemComponentState, BusinessUnit>("dealerGroupId", this);
            dealer = new ManyToOne<int?, SoftwareSystemComponentState, BusinessUnit>("dealerId", this);
            member = new ManyToOne<int?, SoftwareSystemComponentState, Member>("memberId", this);
        }

        protected override SoftwareSystemComponentState This
        {
            get { return this; }
        }

        public Member DealerGroupMember()
        {
            return (memberId == null) ? AuthorizedMember.GetValue() : Member.GetValue();
        }
    }
}
