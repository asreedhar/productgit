using System.Collections.Generic;

namespace FirstLook.DomainModel.Oltp
{
    public static class MemberBusinessUnitSetFacade
    {
        public static readonly string HttpContextKey = "BusinessUnitSet";

        private static readonly MemberBusinessUnitSetFinder Finder = MemberBusinessUnitSetFinder.Instance();

        private static readonly MemberBusinessUnitSetType Type = MemberBusinessUnitSetType.CommandCenterDealerGroupSelection;

        public static ICollection<BusinessUnit> FindOrCreate(BusinessUnit dealerGroup, Member member)
        {
            MemberBusinessUnitSet businessUnitSet = Finder.FindByBusinessUnitAndMemberAndSetType(Type, dealerGroup, member);

            if (businessUnitSet == null)
            {
                businessUnitSet = new MemberBusinessUnitSet();
                businessUnitSet.BusinessUnitSetType = Type;
                businessUnitSet.BusinessUnit.SetValue(dealerGroup);
                businessUnitSet.Member.SetValue(member);
                businessUnitSet.Save();

                switch (member.MemberType)
                {
                    case MemberType.User:
                        foreach (BusinessUnit dealer in member.Dealers)
                            businessUnitSet.BusinessUnits.Add(dealer);
                        break;
                    case MemberType.AccountRepresentative:
                    case MemberType.Administrator:
                        foreach (BusinessUnit dealer in dealerGroup.Dealerships())
                            businessUnitSet.BusinessUnits.Add(dealer);
                        break;
                }
            }

            return businessUnitSet.BusinessUnits;
        }
    }
}
