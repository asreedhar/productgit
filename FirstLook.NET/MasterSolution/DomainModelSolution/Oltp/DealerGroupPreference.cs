using System;
using System.Data;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, Table(Database = "IMT", Name = "DealerGroupPreference")]
    public class DealerGroupPreference : ActiveRecordBase<DealerGroupPreference>
    {
        private int? id;
        private int? dealerGroupId;
        private bool includePerformanceManagementCenter;
        private bool excludeWholesaleFromDaysSupply;

        [IdAttribute, ColumnAttribute(Name = "DealerGroupPreferenceID", DbType = DbType.Int32)]
        public int? Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "BusinessUnitID", DbType = DbType.Int32)]
        public int? DealerGroupId
        {
            get { return dealerGroupId; }
            set { dealerGroupId = value; }
        }

        [ColumnAttribute(Name = "IncludePerformanceManagementCenter", DbType = DbType.Boolean)]
        public bool IncludePerformanceManagementCenter
        {
            get { return includePerformanceManagementCenter; }
            set { includePerformanceManagementCenter = value; }
        }

        [ColumnAttribute(Name = "ExcludeWholesaleFromDaysSupply", DbType = DbType.Boolean)]
        public bool ExcludeWholesaleFromDaysSupply
        {
            get { return excludeWholesaleFromDaysSupply; }
            set { excludeWholesaleFromDaysSupply = value; }
        }

        protected override DealerGroupPreference This
        {
            get { return this; }
        }
    }
}
