using System;
using System.Collections.Generic;
using System.Data;

using FirstLook.Common.ActiveRecord;
using FirstLook.Common.Core.Membership;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, TableAttribute(Database = "IMT", Name = "Member")]
    public class Member : ActiveRecordBase<Member>, IMember
    {
        #region Properties
        private int id;
        private string userName;
        private string firstName;
        private string lastName;
        private MemberType memberType;
        private string emailAddress;

        [IdAttribute, ColumnAttribute(Name = "MemberID", DbType = DbType.Int32)]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "Login", DbType = DbType.String)]
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        [ColumnAttribute(Name = "FirstName", DbType = DbType.String)]
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        [ColumnAttribute(Name = "LastName", DbType = DbType.String)]
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        [ColumnAttribute(Name = "MemberType", DbType = DbType.Int32)]
        public MemberType MemberType
        {
            get { return memberType; }
            set { memberType = value; }
        }

        [ColumnAttribute(Name = "EmailAddress", DbType = DbType.String, Nullable = true)]
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }
        #endregion

        #region Associations
        private readonly ManyToManyCollection<Member, BusinessUnit> dealers;
        private readonly ManyToManyCollection<Member, BusinessUnit> dealerGroups;
        private readonly ManyToManyCollection<Member, Role> roles;

        public ICollection<BusinessUnit> Dealers
        {
            get { return dealers; }
        }

        public ICollection<BusinessUnit> DealerGroups
        {
            get { return dealerGroups; }
        }

        public ICollection<Role> Roles
        {
            get { return roles; }
        }
        #endregion

        #region Constructors
        public Member()
        {
            dealers = new DealerCollection(this, "MemberAccess");
            dealerGroups = new DealerGroupCollection(this, "MemberAccess");
            roles = new ManyToManyCollection<Member, Role>(this, "MemberRole");
        }
        #endregion

        protected override Member This
        {
            get { return this; }
        }

        public ICollection<BusinessUnit> AccessibleDealers(BusinessUnit dealerGroup)
        {
            ICollection<BusinessUnit> accessibleDealers;
            switch (memberType)
            {
                case MemberType.Administrator:
                    accessibleDealers = dealerGroup.Dealerships();
                    break;
                case MemberType.AccountRepresentative:
                    accessibleDealers = BusinessUnitFinder.Instance().FindAllDealershipsByDealerGroupAndMember(dealerGroup.Id, Id, null);
                    break;
                default:
                    accessibleDealers = Dealers;
                    break;
            }
            return accessibleDealers;
        }

        public bool HasRestrictedAccess()
        {
            return (HasRole(2) || !(HasRole(2) || HasRole(3) || HasRole(4)));
        }

        private bool HasRole(int roleId)
        {
            bool hasRole = false;

            foreach (Role role in Roles)
            {
                hasRole |= (role.Id == roleId);
            }

            return hasRole;
        }

        #region CustomAssociations
        [Serializable]
        class DealerCollection : ManyToManyCollection<Member, BusinessUnit>
        {
            public DealerCollection(Member member, string tableName)
                : base(member, tableName)
            {
            }

            public override bool IsReadOnly
            {
                get { return true; }
            }

            protected override string JoinStatement()
            {
                return @"
                    SELECT
                " + GetRowDataGateway().ColumnList("BU1") + @"
                    FROM
		                    dbo.BusinessUnit BU1
                    JOIN	dbo.MemberAccess MA1 ON MA1.BusinessUnitID = BU1.BusinessUnitID
                    JOIN	dbo.DealerPreference DP1 ON DP1.BusinessUnitID = BU1.BusinessUnitID
                    WHERE
	                        BU1.BusinessUnitTypeID = 4
                    AND	    BU1.Active = 1 
                    AND     DP1.GoLiveDate IS NOT NULL
                    AND	    MA1.MemberID = @Value
                    ORDER
                    BY      BU1.BusinessUnit";
            }
        }

        [Serializable]
        class DealerGroupCollection : ManyToManyCollection<Member, BusinessUnit>
        {
            public DealerGroupCollection(Member member, string tableName)
                : base(member, tableName)
            {
            }

            public override bool IsReadOnly
            {
                get { return true; }
            }

            public override void Add(BusinessUnit value)
            {
                throw new NotSupportedException("DealerGroupCollection is a read-only collection");
            }

            public override void Clear()
            {
                throw new NotSupportedException("DealerGroupCollection is a read-only collection");
            }

            public override bool Remove(BusinessUnit value)
            {
                throw new NotSupportedException("DealerGroupCollection is a read-only collection");
            }

            protected override string JoinStatement()
            {
                return @"
                    SELECT  DISTINCT
                " + GetRowDataGateway().ColumnList("BU2") +
                        @"
                    FROM
		                    dbo.BusinessUnit BU1
                    JOIN	dbo.BusinessUnitRelationship BR1 ON BU1.BusinessUnitID = BR1.BusinessUnitID
                    JOIN	dbo.BusinessUnit BU2 ON BU2.BusinessUnitID = BR1.ParentID
                    JOIN	dbo.MemberAccess MA1 ON MA1.BusinessUnitID = BU1.BusinessUnitID
                    JOIN	dbo.DealerPreference DP1 ON DP1.BusinessUnitID = BU1.BusinessUnitID
                    WHERE
	                        BU1.BusinessUnitTypeID = 4
                    AND     BU2.BusinessUnitTypeID = 3
                    AND	    BU1.Active = 1 
                    AND     DP1.GoLiveDate IS NOT NULL
                    AND	    MA1.MemberID = @Value
                    ORDER
                    BY      BU2.BusinessUnit";
            }
        } 
        #endregion
    }
}
