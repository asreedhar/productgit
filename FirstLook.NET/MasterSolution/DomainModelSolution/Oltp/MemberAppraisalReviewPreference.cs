using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    /// <summary>
    /// Holds the information for Member level AppraisalReview preference any BusinessUnit level.
    /// </summary>
    [Serializable, Table(Database = "IMT", Name = "MemberAppraisalReviewPreferences")]
    public class MemberAppraisalReviewPreference : ActiveRecordBase<MemberAppraisalReviewPreference>
    {
        #region Fields
        #pragma warning disable 0169
        
        [ColumnAttribute(Name = "MemberID", DbType = DbType.Int32, Nullable = false)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        private int? memberId;

        [ColumnAttribute(Name = "BusinessUnitID", DbType = DbType.Int32, Nullable = false)]
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Justification = "Foreign key used by ActiveRecord library")]
        private int? businessUnitId;

        #pragma warning restore 0169
        #endregion

        #region Properties

        private int? id;
        private int daysBack = 10;

        [IdAttribute, ColumnAttribute(Name = "MemberAppraisalReviewPreferenceID", DbType = DbType.Int32, Nullable = false)]
        public int? Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "DaysBack", DbType = DbType.Int32, Nullable = false)]
        public int DaysBack
        {
            get { return daysBack; }
            set { daysBack = value; }
        }

        #endregion

        #region Associations

        private readonly ManyToOne<int?, MemberAppraisalReviewPreference, Member> member;
        private readonly ManyToOne<int?, MemberAppraisalReviewPreference, BusinessUnit> businessUnit;

        public ISingleValuedAssociation<Member> Member
        {
            get { return member; }
        }

        public ISingleValuedAssociation<BusinessUnit> BusinessUnit
        {
            get { return businessUnit; }
        }

        #endregion

        public MemberAppraisalReviewPreference()
        {
            member = new ManyToOne<int?, MemberAppraisalReviewPreference, Member>("memberId", this);
            businessUnit = new ManyToOne<int?, MemberAppraisalReviewPreference, BusinessUnit>("businessUnitId", this);
            daysBack = 10;
        }

        public MemberAppraisalReviewPreference(Member member, BusinessUnit businessUnit) : this()
        {
            Member.SetValue(member);
            BusinessUnit.SetValue(businessUnit);
        }

        protected override MemberAppraisalReviewPreference This
        {
            get { return this; }
        }
    }
}
