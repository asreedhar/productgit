using System;
using System.Data;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    [Serializable, TableAttribute(Database = "IMT", Name = "lu_DealerUpgrade")]
    public class DealerUpgrade : ActiveRecordBase<DealerUpgrade>
    {
        private int id;
        private string name;

        [IdAttribute, ColumnAttribute(Name = "DealerUpgradeCD", DbType = DbType.Int32, SelectCast = "CAST(DealerUpgradeCD AS INT)", UpdateCast = "CAST(@DealerUpgradeCD AS TINYINT)")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [ColumnAttribute(Name = "DealerUpgradeDESC", DbType = DbType.String)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected override DealerUpgrade This
        {
            get { return this; }
        }

        public bool Equals(Upgrade code)
        {
            return id == (int)code;
        }
    }
}
