using System.Collections.Generic;
using System.Data;
using FirstLook.Common.ActiveRecord;

namespace FirstLook.DomainModel.Oltp
{
    public class MemberAppraisalReviewPreferenceFinder : Finder<MemberAppraisalReviewPreference>
    {
        private static readonly MemberAppraisalReviewPreferenceFinder finder = new MemberAppraisalReviewPreferenceFinder();

        private MemberAppraisalReviewPreferenceFinder()
        {
        }

        public static MemberAppraisalReviewPreferenceFinder Instance()
        {
            return finder;
        }

        /// <summary>
        /// Retrieves the AppraisalReviewPreference for a Member for a particular BusinessUnit
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="businessUnitId">A DealerID or a DealerGroupID</param>
        /// <returns></returns>
        public MemberAppraisalReviewPreference Find(int memberId, int businessUnitId)
        {
            string commandText = @"
                SELECT M.*
                FROM dbo.MemberAppraisalReviewPreferences M
                WHERE M.MemberId = @MemberID
                AND M.BusinessUnitID = @BusinessUnitID
            ";

            IList<RowDataGatewayBinding> bindingList = CreateBindingList("MemberID", memberId, DbType.Int32, false);
            bindingList.Add(new RowDataGatewayBinding("BusinessUnitID", businessUnitId, DbType.Int32, false));

            return First(Find(commandText, bindingList));
        }

    }
}
