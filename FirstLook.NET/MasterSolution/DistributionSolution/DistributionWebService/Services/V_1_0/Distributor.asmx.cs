using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Web.Services;
using System.Web.Services.Protocols;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.TransferObjects.V_1_0;
using System.Reflection;

namespace FirstLook.Distribution.WebService.Services.V_1_0
{
    /// <summary>
    /// Distribution web service. Can distribute messages as well as poll the status of existing publications.
    /// </summary>
    [WebService(Namespace = "http://services.firstlook.biz/Distribution/1.0/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class Distributor : System.Web.Services.WebService
    {
        #region Properties

        /// <summary>
        /// Logging provider
        /// </summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Identity of the user calling the web service.
        /// </summary>
        public UserIdentity UserIdentity { get; set; }

        /// <summary>
        /// Distributor that will be used to act on the passed-in messages.
        /// </summary>        
        private readonly IDistributor _distributor;

        #endregion

        #region Construction

        /// <summary>
        /// Populate this web service with the registered distributor.
        /// </summary>
        public Distributor()
        {
            _distributor = RegistryFactory.GetResolver().Resolve<IDistributor>();
        }

        #endregion

        #region Distribute Methods

        /// <summary>
        /// Distribute a message to the providers associated with the message.
        /// </summary>
        /// <param name="message">Message to publish.</param>
        /// <returns>Details on all the submissions associated with the publication.</returns>
        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]        
        public SubmissionDetails Distribute(Message message)
        {
            Log.InfoFormat("Started Distribute({0})", message);
            var startTime = DateTime.UtcNow;

            try
            {
                Utility.SetIdentity(UserIdentity);

                var details = _distributor.Distribute(message);

                Log.InfoFormat("Completed Distribute(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
                return details;
            }
            catch (Exception exception)
            {
                Log.Error("Error in Distribute()", exception);
                SoapException soapException = Utility.HandleException(exception, Context.Request, message);
                throw soapException;
            }
        }

        #endregion

        #region Status Methods

        /// <summary>
        /// Check the status of submissions associated with a given publication.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>List of submissions statuses associated with the given publication.</returns>        
        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public Collection<SubmissionStatus> PublicationStatus(int publicationId)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                return _distributor.Status(publicationId);
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }            
        }

        /// <summary>
        /// Check the status of submissions associated with a dealer and vehicle. Only returns submissions from today.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleType">Vehicle type.</param>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>List of submission statuses associated with the dealer and vehicle.</returns>
        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public Collection<SubmissionStatus> VehicleStatus(int dealerId, VehicleType vehicleType, int vehicleId)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                return _distributor.Status(dealerId, vehicleId, vehicleType);
            }            
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }
        }

        #endregion       
    }
}
