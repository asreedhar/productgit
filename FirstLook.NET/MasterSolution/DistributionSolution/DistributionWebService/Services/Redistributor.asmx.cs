﻿using System;
using System.ComponentModel;
using System.Web.Services;
using System.Web.Services.Protocols;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Distributors;

namespace FirstLook.Distribution.WebService.Services
{
    /// <summary>
    /// Redistribution web service. Accepts a submission to be redistributed.
    /// </summary>
    [WebService(Namespace = "http://services.firstlook.biz/Redistribution/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class Redistributor : System.Web.Services.WebService
    {
        #region Properties

        /// <summary>
        /// Identity of the user calling the web service.
        /// </summary>
        public UserIdentity UserIdentity { get; set; }

        /// <summary>
        /// Distributor that will be used to act on the passed-in messages.
        /// </summary>        
        private readonly IDistributor _distributor;

        #endregion

        #region Construction

        /// <summary>
        /// Populate this web service with the registered distributor.
        /// </summary>
        public Redistributor()
        {
            _distributor = RegistryFactory.GetResolver().Resolve<IDistributor>();
        }

        #endregion

        #region Redistribute Methods

        /// <summary>
        /// Redistribute failed submissions that have not yet reached the maximum number of retries.
        /// </summary>      
        /// <returns>How many submissions were redistributed.</returns>
        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public int  Redistribute()
        {
            try
            {    
                Utility.SetIdentity(UserIdentity);
                return _distributor.Redistribute();
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);                 
                throw soapException;
            }
        }

        #endregion
    }
}
