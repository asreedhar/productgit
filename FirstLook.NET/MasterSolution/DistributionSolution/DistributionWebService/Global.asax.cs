using System;
using System.Reflection;
using System.Web;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Services.AutoUplink;
using FirstLook.Distribution.DomainModel.Services.AutoTrader;
using FirstLook.Distribution.DomainModel.Services.eBiz;
using FirstLook.Distribution.DomainModel.Services.GetAuto;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Services.ADP;
using log4net;
using log4net.Config;
using ILog = log4net.ILog;

//using ILog = FirstLook.Common.Core.Logging.ILog;

namespace FirstLook.Distribution.WebService
{
    /// <summary>
    /// Defines the methods, properties, and events that are common to all distribution web services.
    /// </summary>
    public class DistributionServicesApp : HttpApplication
    {
        private static ILog _log;

        /// <summary>
        /// Called at the start of the application.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Application_Start(object sender, EventArgs e)
        {
            // Setup logging
            XmlConfigurator.Configure();
            _log = LogManager.GetLogger(typeof(DistributionServicesApp).FullName);
            _log.InfoFormat("Application_Start() for Distribution.Service version {0} has been called.", Assembly.GetExecutingAssembly().GetName().Version);

            IRegistry registry = RegistryFactory.GetRegistry();

            // Data gateways.
            registry.Register<IPublicationDataGateway, PublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway,     AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  SubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     VehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IAppraisedVehicleDataGateway, AppraisedVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IDistributor,            Distributor>(ImplementationScope.Shared);

            // Bindings.
            registry.Register<IBindingFactory,    BindingFactory>(ImplementationScope.Shared);
            registry.Register<IAutoUplinkService, vehicleinventory1Service>(ImplementationScope.Shared);
            registry.Register<IAutoTraderService, InventoryService>(ImplementationScope.Shared);
            registry.Register<IEBizService,       XMLListener>(ImplementationScope.Shared);
            registry.Register<IGetAutoService,    CvdApiWebService>(ImplementationScope.Shared);
            registry.Register<IADPService, VehicleSaveService>(ImplementationScope.Shared);

            // Queue and queue handling.
            registry.Register<IQueue,     MemoryQueue>(ImplementationScope.Shared);
            registry.Register<IPublisher, ThreadedPublisher>(ImplementationScope.Shared);

            // Exceptions.
            registry.Register<IExceptionLogger, ExceptionLogger>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Called at the end of the application.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Application_End(object sender, EventArgs e)
        {
            _log.InfoFormat("Application_End() for Distribution.Service version {0} has been called.", Assembly.GetExecutingAssembly().GetName().Version);
            RegistryFactory.GetRegistry().Dispose();
        }
    }
}