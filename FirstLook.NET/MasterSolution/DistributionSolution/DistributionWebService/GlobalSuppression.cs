﻿using System.Diagnostics.CodeAnalysis;

/* =================================== */
/* = Global Application Suppressions = */
/* =================================== */

[assembly: SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers", Scope = "member", Target = "FirstLook.Distribution.WebService.Global.#Application_Start(System.Object,System.EventArgs)", Justification = "Method signature is required as is.")]
[assembly: SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers", Scope = "member", Target = "FirstLook.Distribution.WebService.Global.#Application_End(System.Object,System.EventArgs)", Justification = "Method signature is required as is.")]

/* =============================== */
/* = Service Naming Suppressions = */
/* =============================== */

[assembly: SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "namespace", Target = "FirstLook.Distribution.WebService.Services.V_1_0", Justification = "Underscores are necessary to the versioning of the service.")]
