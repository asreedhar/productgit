-- !!!!!!!!!!!!!!!! CHANGE THIS VALUE TO BE THE NUMBER OF DAYS BACK TO REVIEW. !!!!!!!!!!!!!!!!
DECLARE @DaysBack INT
SET @DaysBack = 10

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get the submission errors from a configurable number of days prior to today.
 * 
 *	Parameters: 
 *  @DaysBack 
 * 						
 * ------------------------------------------------------------------------------------------ */

SELECT     
    BUP.BusinessUnit AS 'Dealer Group',
    BU.BusinessUnit  AS 'Dealer',  
    IA.StockNumber   AS 'Stock Number',    
    SS.Name          AS 'Status',    
    ET.Description   AS 'FirstLook Error Description',
    P.Name           AS 'Provider',
    CASE 			      
        WHEN P.ProviderID = 1 THEN ATET.Description
		WHEN P.ProviderID = 2 OR P.ProviderID = 3 THEN AUET.Description
		WHEN P.ProviderID = 4 THEN GAET.Description
		WHEN P.ProviderID = 5 THEN EBET.Description
        ELSE 'Not Applicable'
    END              AS 'Provider Error Description',
    SE.SubmissionID  AS 'Submission ID',        
    ME.Login         AS 'User',    
    SE.OccuredOn     AS 'Error Occured On'
FROM 
    IMT.Distribution.SubmissionError SE
JOIN
    IMT.Distribution.Submission S ON S.SubmissionID = SE.SubmissionID
JOIN
    IMT.Distribution.SubmissionErrorType ET ON ET.ErrorTypeID = SE.ErrorTypeID
JOIN
    IMT.Distribution.Provider P ON P.ProviderID = S.ProviderID
JOIN 
    IMT.Distribution.SubmissionStatus SS ON SS.SubmissionStatusID = S.SubmissionStatusID    
JOIN 
    IMT.Distribution.Publication PU ON PU.PublicationID = S.PublicationID
JOIN
    IMT.Distribution.Message M ON M.MessageID = PU.MessageID
	
-- User

JOIN
    IMT.dbo.Member ME ON ME.MemberID = PU.InsertUserID
	
-- Inventory
	
JOIN
    FLDW.dbo.InventoryActive IA ON IA.InventoryID = M.VehicleEntityID    		
	
-- Business Unit / Group
	
JOIN
    IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = M.DealerID
LEFT JOIN 
    IMT.dbo.BusinessUnitType BUT ON BUT.BusinessUnitTypeID = BU.BusinessUnitTypeID    
LEFT JOIN
    IMT.dbo.BusinessUnitRelationship BUR ON BUR.BusinessUnitID = BU.BusinessUnitID
LEFT JOIN
    IMT.dbo.BusinessUnit BUP ON BUP.BusinessUnitID = BUR.ParentID    

-- Error tables	
	
LEFT JOIN 
    IMT.Distribution.AutoUplink_Error AUE ON AUE.ErrorID = SE.ErrorID
LEFT JOIN 
    IMT.Distribution.AutoUplink_ErrorType AUET ON AUET.ErrorTypeID = AUE.ErrorTypeID
LEFT JOIN 
    IMT.Distribution.AutoTrader_Error ATE ON ATE.ErrorID = SE.ErrorID
LEFT JOIN
    IMT.Distribution.AutoTrader_ErrorType ATET ON ATET.ErrorTypeID = ATE.ErrorTypeID
LEFT JOIN
	IMT.Distribution.eBiz_Error EBE ON EBE.ErrorID = SE.ErrorID
LEFT JOIN
	IMT.Distribution.eBiz_ErrorTYPE EBET ON EBET.ErrorTypeID = EBE.ErrorTypeID
LEFT JOIN
	IMT.Distribution.GetAuto_Error GAE ON GAE.ErrorID = SE.ErrorID
LEFT JOIN
	IMT.Distribution.GetAuto_ErrorTYPE GAET ON GAET.ErrorTypeID = GAE.ErrorTypeID
WHERE 
    SE.OccuredOn > DATEADD(DAY, -@DaysBack, GETDATE())
ORDER BY
    SE.OccuredOn DESC
    