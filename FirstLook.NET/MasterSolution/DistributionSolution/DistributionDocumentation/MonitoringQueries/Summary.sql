-- !!!!!!!!!!!!!!!! CHANGE THIS VALUE TO THE # OF DAYS BACK YOU WANT TO GO. !!!!!!!!!!!!!!!!
DECLARE @DaysBack int
SET @DaysBack = 7

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get the number of submissions in each state in the last N number of days.
 *  
 * ------------------------------------------------------------------------------------------ */   

SELECT 
	ST.Name AS 'Status', 
	COUNT(S.SubmissionStatusID) AS 'Count'
FROM 
	IMT.Distribution.Submission S
JOIN 
	IMT.Distribution.SubmissionStatus ST ON ST.SubmissionStatusID = S.SubmissionStatusID
WHERE 
	S.PushedOnQueue >= DATEADD(DAY, -@DaysBack, GETDATE())
GROUP BY 
	ST.Name