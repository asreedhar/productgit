-- !!!!!!!!!!!!!!!! CHANGE THIS VALUE TO BE THE NUMBER OF DAYS BACK TO REVIEW. !!!!!!!!!!!!!!!!
DECLARE @DaysBack INT
SET @DaysBack = 7

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get the submissions that have been retried in the last X number of days.
 * 
 *	Parameters: 
 *  @DaysBack 
 * 						
 * ------------------------------------------------------------------------------------------ */

DECLARE @Retries TABLE
(
	PublicationID INT,
	ProviderID INT
)

-- Get the publication and provider combos that have had multiple attempts.

INSERT INTO @Retries
SELECT DISTINCT
	PublicationID,
	ProviderID
FROM
	IMT.Distribution.Submission
WHERE
	Attempt > 1

-- Get the details on these retries.	
	
SELECT	
	BUP.BusinessUnit AS 'Dealer Group',
    BU.BusinessUnit  AS 'Dealer',  
    IA.StockNumber   AS 'Stock Number',    
	S.SubmissionID,
	S.PublicationID,
	PR.Name AS 'Provider',
	S.Attempt,
	CASE
		WHEN (S.MessageTypes & 3) = 3 THEN 'Price + Description'
		WHEN (S.MessageTypes & 2) = 2 THEN 'Description'
		WHEN (S.MessageTypes & 1) = 1 THEN 'Price'
		ELSE 'n/a'
	END AS 'Message Types',
	ST.Name AS 'Status',
	S.PushedOnQueue AS 'Queued',	
	S.ResponseReceived	AS 'Response Received'
FROM
	@Retries R
JOIN	
	IMT.Distribution.Submission S ON S.PublicationID = R.PublicationID AND S.ProviderID = R.ProviderID
JOIN
	IMT.Distribution.Publication P ON P.PublicationID = S.PublicationID
JOIN
	IMT.Distribution.Message M ON M.MessageID = P.MessageID
JOIN
	IMT.Distribution.SubmissionStatus ST ON ST.SubmissionStatusID = S.SubmissionStatusID
JOIN
	IMT.Distribution.Provider PR ON PR.ProviderID = S.ProviderID
	
-- Inventory

JOIN
    FLDW.dbo.InventoryActive IA ON IA.InventoryID = M.VehicleEntityID  

-- Business Unit / Group	

JOIN
    IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = M.DealerID
LEFT JOIN 
    IMT.dbo.BusinessUnitType BUT ON BUT.BusinessUnitTypeID = BU.BusinessUnitTypeID    
LEFT JOIN
    IMT.dbo.BusinessUnitRelationship BUR ON BUR.BusinessUnitID = BU.BusinessUnitID
LEFT JOIN
    IMT.dbo.BusinessUnit BUP ON BUP.BusinessUnitID = BUR.ParentID
	
WHERE
	S.PushedOnQueue >= DATEADD(DD, -@DaysBack, GetDate())
ORDER BY
	S.PublicationID DESC,
	PR.Name ASC,
	S.Attempt DESC