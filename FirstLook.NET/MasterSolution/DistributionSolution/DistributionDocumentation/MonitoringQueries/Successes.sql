-- !!!!!!!!!!!!!!!! CHANGE THIS VALUE TO BE THE NUMBER OF DAYS BACK TO REVIEW. !!!!!!!!!!!!!!!!
DECLARE @DaysBack INT
SET @DaysBack = 10

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get the submission successes from a configurable number of days prior to today.
 * 
 *	Parameters: 
 *  @DaysBack 
 * 						
 * ------------------------------------------------------------------------------------------ */

SELECT 
    SS.OccuredOn AS 'Occured On',
    BUP.BusinessUnit AS 'Dealer Group',
    BU.BusinessUnit AS 'Dealer',  
    IA.StockNumber AS 'Stock Number',
    P.Name AS Provider,
    SS.SubmissionID,
    ST.Name AS SubmissionStatus,      
    CASE
        WHEN SS.MessageTypes = 1 THEN 'Price'
        WHEN SS.MessageTypes = 2 THEN 'Description'
        WHEN SS.MessageTypes = 3 THEN 'Price + Description'
        WHEN SS.MessageTypes = 4 THEN 'Mileage'
        WHEN SS.MessageTypes = 5 THEN 'Price + Mileage'
        WHEN SS.MessageTypes = 6 THEN 'Description + Mileage'
        WHEN SS.MessageTypes = 7 THEN 'Price + Description + Mileage'
        ELSE ''
    END AS 'MessageTypesSent',
    ME.Login AS InsertUser,
    PU.InsertDate AS InsertDate    
FROM 
    IMT.Distribution.SubmissionSuccess SS
JOIN
    IMT.Distribution.Submission S ON S.SubmissionID = SS.SubmissionID
JOIN
    IMT.Distribution.Provider P ON P.ProviderID = S.ProviderID
JOIN 
    IMT.Distribution.SubmissionStatus ST ON ST.SubmissionStatusID = S.SubmissionStatusID    
JOIN 
    IMT.Distribution.Publication PU ON PU.PublicationID = S.PublicationID
JOIN
    IMT.Distribution.Message M ON M.MessageID = PU.MessageID
JOIN
    IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = M.DealerID
JOIN
    IMT.dbo.Member ME ON ME.MemberID = PU.InsertUserID    
JOIN
    FLDW.dbo.InventoryActive IA ON IA.InventoryID = M.VehicleEntityID  
LEFT JOIN 
    IMT.dbo.BusinessUnitType BUT ON BUT.BusinessUnitTypeID = BU.BusinessUnitTypeID    
LEFT JOIN
    IMT.dbo.BusinessUnitRelationship BUR ON BUR.BusinessUnitID = BU.BusinessUnitID
LEFT JOIN
    IMT.dbo.BusinessUnit BUP ON BUP.BusinessUnitID = BUR.ParentID    
WHERE 
    SS.OccuredOn > DATEADD(DAY, -@DaysBack, GETDATE())
ORDER BY
    SS.OccuredOn DESC