-- !!!!!!!!!!!!!!!! CHANGE THIS VALUE TO THE # OF DAYS BACK YOU WANT TO GO. !!!!!!!!!!!!!!!!
DECLARE @DaysBack int
SET @DaysBack = 7

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get the exceptions in the last X number of days.
 *  
 * ------------------------------------------------------------------------------------------ */   
 
 SELECT * 
 FROM IMT.Distribution.Exception
 WHERE ExceptionTime > DATEADD(DAY, -@DaysBack, GETDATE())
 ORDER BY ExceptionID DESC
 