/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get the number of exceptions per day, all time.
 *  
 * ------------------------------------------------------------------------------------------ */   
 
select
	datepart(yyyy, ExceptionTime) as Year,
	datepart(mm, ExceptionTime) as Month,
	datepart(dd, ExceptionTime) as Day,
	count(*) as Exceptions
from 
	IMT.Distribution.Exception
group by
	datepart(yyyy, ExceptionTime),
	datepart(mm, ExceptionTime),
	datepart(dd, ExceptionTime)
order by
	datepart(yyyy, ExceptionTime) desc,
	datepart(mm, ExceptionTime) desc,
	datepart(dd, ExceptionTime) desc 