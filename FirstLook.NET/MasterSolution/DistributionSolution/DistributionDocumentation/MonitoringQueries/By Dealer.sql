-- !!!!!!!!!!!!!!!! CHANGE THIS VALUE TO THE DEALER ID YOU WANT TO SEE. IS SAME AS 'BUSINESS UNIT ID'. !!!!!!!!!!!!!!!!
DECLARE @DealerID int
SET @DealerID = 101590

-- !!!!!!!!!!!!!!!! CHANGE THIS VALUE TO THE # OF DAYS BACK YOU WANT TO GO. !!!!!!!!!!!!!!!!
DECLARE @DaysBack int
SET @DaysBack = 7

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get submissions for a given dealer, as well as what parts of the submission passed or 
 *  failed. Set the dealer id (aka business unit id) above.
 *
 * ------------------------------------------------------------------------------------------ */      

SELECT           
    @DealerID              AS 'Dealer ID',
    IA.StockNumber         AS 'Stock Number', 
    P.Name                 AS 'Provider',
    ST.Name                AS 'Status',    
    COALESCE(PR.Value, '') AS 'New Price',	    
    CASE        
        WHEN PR.Value IS NULL THEN 'n/a'
        WHEN (SS.MessageTypes & 1) = 1 THEN 'Yes'
		WHEN (SE.MessageTypes & 1) = 1 THEN 'No'
		ELSE 'Superseded'
    END AS 'Price Succeeded',
	COALESCE(CT.Text, '') AS 'New Description',
    CASE        
        WHEN CT.Text IS NULL THEN 'n/a'
        WHEN (SS.MessageTypes & 2) = 2 THEN 'Yes'
		WHEN (SE.MessageTypes & 2) = 2 THEN 'No'
		ELSE 'Superseded'
    END AS 'Description Succeeded',   
    --COALESCE(SETY.Description, 'n/a') AS 'Failure Reason',    
	--COALESCE(EX.Message, 'n/a') AS 'Exception Message',
    S.SubmissionID     AS 'Submission ID',
    S.PublicationID    AS 'Publication ID',     
	S.Attempt          AS 'Attempt',
    S.PushedOnQueue    AS 'Pushed On Queue',
    S.PoppedOffQueue   AS 'Popped Off Queue',
    S.SubmissionSent   AS 'Submission Sent',
    S.ResponseReceived AS 'Response Received'
FROM
    IMT.Distribution.Submission S    
JOIN
    IMT.Distribution.SubmissionStatus ST ON ST.SubmissionStatusID = S.SubmissionStatusID
JOIN
    IMT.Distribution.Provider P ON P.ProviderID = S.ProviderID
JOIN
    IMT.Distribution.Publication PUB ON PUB.PublicationID = S.PublicationID
JOIN
    IMT.Distribution.Message M ON M.MessageID = PUB.MessageID

-- Message Parts	
	
LEFT JOIN
    IMT.Distribution.Price PR ON PR.MessageID = M.MessageID
LEFT JOIN
    IMT.Distribution.Advertisement_Content AC ON AC.MessageID = M.MessageID
LEFT JOIN
    IMT.Distribution.ContentText CT ON CT.ContentID = AC.ContentID

-- Inventory	
	
JOIN
    FLDW.dbo.InventoryActive IA ON IA.InventoryID = M.VehicleEntityID 

-- Errors / Exceptions	
	
LEFT JOIN
    IMT.Distribution.SubmissionError SE ON SE.SubmissionID = S.SubmissionID
LEFT JOIN
    IMT.Distribution.SubmissionErrorType SETY ON SETY.ErrorTypeID = SE.ErrorTypeID    
LEFT JOIN
	IMT.Distribution.[Exception] EX ON EX.SubmissionID = S.SubmissionID	
	
-- Successes
	
LEFT JOIN
	IMT.Distribution.SubmissionSuccess SS ON SS.SubmissionID = S.SubmissionID
	
WHERE    
    M.DealerID = @DealerID
--AND
	--S.PushedOnQueue >= DATEADD(DD, -@DaysBack, GETDATE())
ORDER BY 
    S.PushedOnQueue DESC
 