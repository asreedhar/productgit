/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Get the number of exceptions that have occured today.
 *  
 * ------------------------------------------------------------------------------------------ */   

select 
	count(*) as Exceptions
from 
	IMT.Distribution.Exception
where 
	ExceptionTime >= dateadd(dd, datediff(dd, 0, getdate()), 0)