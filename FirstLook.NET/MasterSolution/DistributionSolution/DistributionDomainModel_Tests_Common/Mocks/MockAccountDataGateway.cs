using System.Collections.Generic;
using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the account data gateway. Will capture which methods were called and when. Return values for functions
    /// that have them are, generally, configurable.
    /// </summary>
    public class MockAccountDataGateway : DataGateway<Account>, IAccountDataGateway
    {
        #region Status Tracking
                              
        // Flags to signal if a given method was called.
        public static bool DealerExistsCalled;
        public static bool DealerHasAultecExtractCalled;            
        public static bool DeleteCalled;
        public static bool ExistsCalled;
        public static bool FetchAccountPreferencesCalled;
        public static bool FetchCalled;                
        public static bool FetchProviderCalled;
        public static bool FetchProvidersCalled;
        public static bool FetchProviderMessageTypesCalled;
        public static bool InsertCalled;
        public static bool SaveAccountPreferencesCalled;
        public static bool SaveAccountPreferenceCalled;
        public static bool SaveCalled;
        public static bool SupportsMessageTypeCalled;
        public static bool UpdateCalled;
        public static bool UserIdentityExistsCalled;

        // Counts of how many times a given method was called since the last reset.
        public static int NumberDealerExistsCalled;
        public static int NumberDealerHasAultecExtractCalled;            
        public static int NumberDeleteCalled;
        public static int NumberExistsCalled;
        public static int NumberFetchAccountPreferencesCalled;
        public static int NumberFetchCalled;        
        public static int NumberFetchProviderCalled;
        public static int NumberFetchProvidersCalled;
        public static int NumberFetchProviderMessageTypesCalled;
        public static int NumberInsertCalled;
        public static int NumberSaveAccountPreferencesCalled;
        public static int NumberSaveAccountPreferenceCalled;
        public static int NumberSaveCalled;
        public static int NumberSupportsMessageTypeCalled;
        public static int NumberUpdateCalled;
        public static int NumberUserIdentityExistsCalled;

        // Miscellaneous status tracking.
        public static bool   DoesAccountExist;
        public static bool   DoesDealerExist;                
        public static bool   DoesDealerHaveAultecExtract;
        public static bool   DoesUserIdentityExist;
        public static string DealershipCode;
        public static bool   IsProviderMessageTypeSupported;
        public static string Password;
        public static Collection<Provider> Providers;                

        /// <summary>
        /// Reset the values of all the status veriables.
        /// </summary>
        public static void Reset()
        {
            DealerExistsCalled = false;
            DealerHasAultecExtractCalled = false;
            DeleteCalled = false;
            ExistsCalled = false;
            FetchAccountPreferencesCalled = false;
            FetchCalled = false;            
            FetchProviderCalled = false;
            FetchProvidersCalled = false;
            FetchProviderMessageTypesCalled = false;
            InsertCalled = false;
            SaveAccountPreferencesCalled = false;
            SaveAccountPreferenceCalled = false;
            SaveCalled = false;            
            SupportsMessageTypeCalled = false;
            UpdateCalled = false;
            UserIdentityExistsCalled = false;

            NumberDealerExistsCalled = 0;
            NumberDealerHasAultecExtractCalled = 0;
            NumberDeleteCalled = 0;
            NumberExistsCalled = 0;
            NumberFetchAccountPreferencesCalled = 0;
            NumberFetchCalled = 0;            
            NumberFetchProviderCalled = 0;
            NumberFetchProvidersCalled = 0;
            NumberFetchProviderMessageTypesCalled = 0;
            NumberInsertCalled = 0;
            NumberSaveAccountPreferencesCalled = 0;
            NumberSaveAccountPreferenceCalled = 0;
            NumberSaveCalled = 0;
            NumberSupportsMessageTypeCalled = 0;
            NumberUpdateCalled = 0;
            NumberUserIdentityExistsCalled = 0;

            DoesAccountExist = true;
            DoesDealerExist = true;
            DoesDealerHaveAultecExtract = false;
            DoesUserIdentityExist = true;
            DealershipCode = "000";
            IsProviderMessageTypeSupported = true;
            Password = "";

            Provider autoUplinkProvider = new Provider(2, "AutoUplink");
            Provider autoTraderProvider = new Provider(1, "AutoTrader");
            Providers = new Collection<Provider> { autoUplinkProvider, autoTraderProvider };            
        }

        #endregion

        #region Account

        /// <summary>
        /// Delete the given account from the database. Does nothing.
        /// </summary>
        /// <param name="value">Account to delete. Ignored.</param>
        protected override void DeleteSelf(Account value)
        {
            DeleteCalled = true;            
            NumberDeleteCalled++;
        }

        /// <summary>
        /// Does account information exist for the given provider and dealer? The return value is configurable but 
        /// defaults to true.
        /// </summary>
        /// <param name="providerId">Provider identifier. Ignored.</param>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <returns>Configurable. Defaults to true.</returns>
        public bool Exists(int providerId, int dealerId)
        {
            ExistsCalled = true;            
            NumberExistsCalled++;

            return DoesAccountExist;
        }

        /// <summary>
        /// Fetch an account for the given provider and dealer identifiers. Just returns a new account for the given
        /// provider and dealer.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Account object set with the given values.</returns>
        public Account Fetch(int providerId, int dealerId)
        {
            FetchCalled = true;            
            NumberFetchCalled++;

            Account account = Account.NewAccount(providerId, dealerId);
            account.DealershipCode = DealershipCode;
            account.Password = Password;
            return account;
        }

        /// <summary>
        /// Insert the given account to the database. Does nothing.
        /// </summary>
        /// <param name="value">Account to insert. Ignored.</param>
        protected override void Insert(Account value)
        {
            InsertCalled = true;            
            NumberInsertCalled++;
        }

        /// <summary>
        /// Update the given account to the database. Does nothing.
        /// </summary>
        /// <param name="value">Account to update. Ignored.</param>
        protected override void Update(Account value)
        {
            UpdateCalled = true;            
            NumberUpdateCalled++;
        }

        #endregion

        #region Account Preferences

        /// <summary>
        /// Fetch account preferences. Does nothing.
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <returns>Empty list.</returns>
        public AccountPreferenceDictionary FetchAccountPreferences(int dealerId)
        {
            FetchAccountPreferencesCalled = true;           
            NumberFetchAccountPreferencesCalled++;

            AccountPreferenceDictionary preferences = new AccountPreferenceDictionary(dealerId, "Bogus Dealer Name");
            
            Provider provider1 = new Provider(1, "Provider 1");
            Provider provider2 = new Provider(2, "Provider 2");            

            Collection<MessageTypePreference> preference = new Collection<MessageTypePreference>();
            preference.Add(new MessageTypePreference(1, MessageTypes.Price, true));
            preference.Add(new MessageTypePreference(1, MessageTypes.Description, true));
            preferences[provider1] = preference;

            preference = new Collection<MessageTypePreference>();
            preference.Add(new MessageTypePreference(2, MessageTypes.Price, true));
            preference.Add(new MessageTypePreference(2, MessageTypes.Price | MessageTypes.Description, true));
            preferences[provider2] = preference;

            return preferences;
        }        

        /// <summary>
        /// Save the given account preferences. Does nothing.
        /// </summary>
        /// <param name="accountPreferences">Account preferences. Ignored.</param>
        public void SaveAccountPreferences(AccountPreferenceDictionary accountPreferences)
        {
            SaveAccountPreferencesCalled = true;
			NumberSaveAccountPreferencesCalled++;
        }

        /// <summary>
        /// Save the given account preference for a single provider. Does nothing.
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <param name="providerId">Provider identifier. Ignored.</param>
        /// <param name="preference">Account preference to save. Ignored.</param>
        public void SaveAccountPreference(int dealerId, int providerId, IList<MessageTypePreference> preference)
        {
            SaveAccountPreferenceCalled = true;
			NumberSaveAccountPreferenceCalled++;
        }

        /// <summary>
        /// Does the account support the given message types? Check consists of three parts:
        /// 1. Is there active account information for the dealer with the provider?
        /// 2. Does the provider support the message type?
        /// 3. If the provider does support the message type, has the dealer enabled this setting?         
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <param name="providerId">Provider identifier. Ignored.</param>
        /// <param name="messageTypes">Message type bit field. Will be filtered to the supported subset type. Ignored.</param>
        /// <returns>Configurable. Defaults to true.</returns>
        public bool IsSupported(int dealerId, int providerId, ref MessageTypes messageTypes)
        {
            SupportsMessageTypeCalled = true;
            NumberSupportsMessageTypeCalled++;
            return IsProviderMessageTypeSupported;
        }

        #endregion

        #region Dealer

        /// <summary>
        /// Does a record exist for the given dealer identifier? The return value is configurable, but defaults to 
        /// true.
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <returns>Configurable. Defaults to true.</returns>
        public bool DealerExists(int dealerId)
        {
            DealerExistsCalled = true;
			NumberDealerExistsCalled++;
            return DoesDealerExist;
        }

        /// <summary>
        /// Check whether the dealer with the given identifier will need to update 'Internet Price' as opposed to just
        /// 'Price' because they are setup with Aultec for active extraction. Looks in IMT.Extract.DealerConfiguration.
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <returns>True if Aultec is enabled, false otherwise. Ignored.</returns>
        public bool DealerRequiresInternetPriceUpdate(int dealerId)
        {
            DealerHasAultecExtractCalled = true;
            NumberDealerHasAultecExtractCalled++;
            return DoesDealerHaveAultecExtract;
        }

        public Dealer GetDealer(int dealerId)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Provider

        /// <summary>
        /// Fetch the provider with the given identifier. Returns a dummy provider for the same identifier.
        /// </summary>
        /// <param name="providerId">Provider identifier. Ignored.</param>
        /// <returns>Dummy provider with the given identifier..</returns>
        public Provider FetchProvider(int providerId)
        {
            FetchProviderCalled = true;            
            NumberFetchProviderCalled++;

            return new Provider(providerId, "Test Provider");
        }

        /// <summary>
        /// Fetch all providers. Return value is configurable, but defaults to AutoUplink and AutoTrader.
        /// </summary>
        /// <returns>Configurable. Defaults to AutoUplink and AutoTrader.</returns>
        public Collection<Provider> FetchProviders()
        {
            FetchProvidersCalled = true;            
            NumberFetchProvidersCalled++;

            return Providers;
        }

        #endregion

        #region User Identity

        /// <summary>
        /// Check if the user identity is valid.
        /// </summary>        
        /// <param name="userName">User name from the identity to be checked.</param>
        /// <returns>True if it is valid, false otherwise.</returns>
        public bool UserIdentityExists(string userName)
        {            
            UserIdentityExistsCalled = true;            
            NumberUserIdentityExistsCalled++;

            return DoesUserIdentityExist;
        }

        #endregion


        #region IAccountDataGateway Members


        public Account FetchADPAccount(int dealerId)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
