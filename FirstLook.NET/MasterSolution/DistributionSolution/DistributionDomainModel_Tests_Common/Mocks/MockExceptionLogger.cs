using System;
using FirstLook.Distribution.DomainModel.Data;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock for exception logging. Does not do anything other than record what was called.
    /// </summary>
    public class MockExceptionLogger : IExceptionLogger
    {
        #region Status Tracking

        // Tracker for what was called.
        public static bool RecordCalled;
        public static bool FetchCalled;

        /// <summary>
        /// Reset the values of the static status-tracking variables.
        /// </summary>
        public static void Reset()
        {
            RecordCalled = false;
            FetchCalled = false;
        }

        #endregion

        #region Exception Logging

        /// <summary>
        /// Record an exception. Does nothing.
        /// </summary>
        /// <param name="exception">Exception to record. Ignored.</param>
        public void Record(Exception exception)
        {
            RecordCalled = true;
            return;
        }

        /// <summary>
        /// Record an exception with the incoming request message that likely caused it. Does nothing.
        /// </summary>
        /// <param name="exception">Exception to record. Ignored.</param>
        /// <param name="message">Incoming request message. Ignored.</param>
        public void Record(Exception exception, string message)
        {
            RecordCalled = true;
            return;
        }

        /// <summary>
        /// Record an exception for a specific exception. Does nothing.
        /// </summary>
        /// <param name="exception">Exception to record. Ignored.</param>
        /// <param name="submissionId">Submission identifier. Ignored.</param>
        public void Record(Exception exception, int submissionId)
        {
            RecordCalled = true;
            return;
        }

        /// <summary>
        /// Fetch any exception tied to the publication with the given identifier.
        /// </summary>
        /// <param name="submissionId">Submission identifier. Ignored.</param>
        /// <returns>Empty string.</returns>
        public string Fetch(int submissionId)
        {
            FetchCalled = true;
            return string.Empty;
        }

        #endregion
    }
}
