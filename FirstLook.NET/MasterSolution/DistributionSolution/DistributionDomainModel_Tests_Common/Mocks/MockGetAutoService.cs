﻿using System;
using FirstLook.Distribution.DomainModel.Services.GetAuto;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the GetAuto web service.
    /// </summary>
    public class MockGetAutoService : IGetAutoService
    {
        #region Status Tracking

        // Were the given functions called?
        public static bool UpdateVehiclePriceCalled;
        public static bool UpdateVehicleCommentsCalled;
        public static bool GetAllVehicleCalled;

        // Return code we wish to return on a given web service call.
        public static ReturnResult UpdatePriceResult;
        public static ReturnResult UpdateCommentResult;
        public static VehicleInfo[] DealerVehicles;        

        // Should we simulate a SOAP exception?
        public static bool ThrowsException;

        /// <summary>
        /// Reset static status variables to default values.
        /// </summary>
        public static void Reset()
        {
            UpdateVehiclePriceCalled = false;
            UpdateVehicleCommentsCalled = false;
            GetAllVehicleCalled = false;

            UpdatePriceResult = ReturnResult.Success;
            UpdateCommentResult = ReturnResult.Success;
            DealerVehicles = null;

            ThrowsException = false;
        }

        #endregion

        #region Service

        /// <summary>
        /// Mock of the 'get all vehicles' web service call. Returns a configurable number of vehicles.
        /// </summary>
        /// <param name="DealerLotKey">Dealer lot key. Ignored.</param>
        /// <returns>Configurable collection of vehicle return objects. Defaults to null.</returns>
        public VehicleInfo[] GetAllVehicles(int DealerLotKey)
        {
            GetAllVehicleCalled = true;
            return DealerVehicles;
        }

        /// <summary>
        /// Mock of the price-updating web service call. Returns an object created from the passed-in values.
        /// </summary>
        /// <param name="VehicleKey">Vehicle key. Returned back to user.</param>
        /// <param name="Price">Price. Returned back to user.</param>
        /// <param name="PriceType">Type of the price. Returned back to user.</param>
        /// <returns>Object created from parameters, with configurable result that defaults to 'Success'.</returns>
        public VehicleInfo UpdateVehiclePrice(int VehicleKey, int Price, PriceType PriceType)
        {
            UpdateVehiclePriceCalled = true;

            if (ThrowsException)
            {
                throw new Exception();
            }

            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.VehicleKey = VehicleKey;
            vehicleInfo.Prices = new PriceInfo[1];

            vehicleInfo.Prices[0] = new PriceInfo();
            vehicleInfo.Prices[0].PriceType = PriceType;
            vehicleInfo.Prices[0].Price = Price;

            vehicleInfo.Result = UpdatePriceResult;
            return vehicleInfo;
        }

        /// <summary>
        /// Mock of the comment-updating web service call. Returns a configurable result.
        /// </summary>
        /// <param name="VehicleKey">Vehicle key. Ignored.</param>
        /// <param name="VehicleComments">Vehicle comments. Ignored.</param>
        /// <returns>Configurable, but defaults to 'Success'.</returns>
        public UpdateVehicleCommentsReturn UpdateVehicleComments(int VehicleKey, string VehicleComments)
        {
            UpdateVehicleCommentsCalled = true;

            if (ThrowsException)
            {
                throw new Exception();
            }

            UpdateVehicleCommentsReturn returnVal = new UpdateVehicleCommentsReturn();            
            returnVal.Result = UpdateCommentResult;
            return returnVal;
        }

        #endregion
    }
}
