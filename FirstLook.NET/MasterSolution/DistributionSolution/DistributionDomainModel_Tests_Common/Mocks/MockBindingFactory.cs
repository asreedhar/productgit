using System;
using FirstLook.Distribution.DomainModel.Bindings;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the binding factory. Always returns a mock binding.
    /// </summary>
    public class MockBindingFactory : IBindingFactory
    {
        #region Status Tracking

        public static bool ThrowException;

        /// <summary>
        /// Other mocks implement a reset function to reinitialize static status tracking variables. There's no need
        /// for that here, but to maintain consistency with other mocks, it is implemented.
        /// </summary>
        public static void Reset()
        {
            ThrowException = false;
        }

        #endregion

        /// <summary>
        /// Get the binding for the provider with the given identifier.
        /// </summary>
        /// <param name="providerId">Provider identifier. Ignored.</param>
        /// <returns>Mock binding.</returns>
        public IBinding GetBinding(int providerId)
        {
            if (ThrowException)
            {
                throw new Exception();
            }
            return new MockBinding();
        }
    }
}
