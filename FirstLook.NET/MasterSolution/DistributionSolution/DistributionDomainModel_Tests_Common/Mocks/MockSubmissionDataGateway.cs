using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the submission data gateway. Will capture which methods were called and when.
    /// </summary>
    public class MockSubmissionDataGateway : DataGateway<Submission>, ISubmissionDataGateway
    {
        #region Status Tracking

        // Flags to signal if a given method was called.
        public static bool FetchFailuresCalled;
        public static bool FetchCalled;
        public static bool FetchByVehicleCalled;
        public static bool FetchErrorCalled;
        public static bool FetchSupersedingPriceCalled;
        public static bool FetchSuccessCalled;
        public static bool InsertCalled;
        public static bool FetchStaleTypesCalled;
        public static bool LogErrorCalled;
        public static bool LogStaleCalled;
        public static bool LogSuccessCalled;
        public static bool UpdateCalled;

        // Counts of how many times a given method was called since the last reset.
        public static int NumberFetchFailuresCalled;
        public static int NumberFetchCalled;
        public static int NumberFetchByVehicleCalled;
        public static int NumberFetchErrorCalled;
        public static int NumberFetchSuccessCalled;
        public static int NumberFetchSupersedingPriceCalled;
        public static int NumberInsertCalled;
        public static int NumberFetchStaleTypesCalled;
        public static int NumberLogErrorCalled;
        public static int NumberLogStaleCalled;
        public static int NumberLogSuccessCalled;
        public static int NumberUpdateCalled;        

        // Should we return true or false for IsStale()?        
        public static MessageTypes StaleTypes;
        public static int NumberSubmissionsToFetch;        
        public static Collection<FirstLookError> Errors;
        public static Collection<int> Successes;
        public static Price SupersedingPrice;

        /// <summary>
        /// Reset static status variables to default values.
        /// </summary>
        public static void Reset()
        {
            FetchFailuresCalled = false;
            FetchCalled = false;
            FetchByVehicleCalled = false;
            FetchErrorCalled = false;
            FetchSuccessCalled = false;
            FetchSupersedingPriceCalled = false;
            InsertCalled = false;
            FetchStaleTypesCalled = false;
            LogErrorCalled = false;
            LogStaleCalled = false;
            LogSuccessCalled = false;
            UpdateCalled = false;

            NumberFetchFailuresCalled = 0;
            NumberFetchCalled = 0;
            NumberFetchByVehicleCalled = 0;
            NumberFetchErrorCalled = 0;
            NumberFetchSuccessCalled = 0;
            NumberFetchSupersedingPriceCalled = 0;
            NumberInsertCalled = 0;
            NumberFetchStaleTypesCalled = 0;
            NumberLogErrorCalled = 0;
            NumberLogStaleCalled = 0;
            NumberLogSuccessCalled = 0;
            NumberUpdateCalled = 0;
            
            StaleTypes = 0;
            NumberSubmissionsToFetch = 3;            
            Errors = new Collection<FirstLookError>();
            Successes = new Collection<int>();            
        }

        #endregion

        #region Submission

        /// <summary>
        /// Fetch the submissions tied to the publication with the given identifier. Returns a list of three dummy 
        /// submissions.
        /// </summary>
        /// <param name="publicationId">Publication identifier. Ignored.</param>
        /// <returns>List of dummy submissions.</returns>
        public IList<Submission> Fetch(int publicationId)
        {
            FetchCalled = true;                        
            NumberFetchCalled++;

            Collection<Submission> submissions = new Collection<Submission>();
            for (int i = 0; i < NumberSubmissionsToFetch; i++)
            {
                submissions.Add(Submission.NewSubmission(i + 1, i + 1, MessageTypes.Price));    
            }            
            return submissions;
        }

        /// <summary>
        /// Fetch the submissions tied to the publication that is defined by the given dealer and vehicle. Returns a 
        /// list of three dummy submissions.
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <param name="vehicleEntityId">Vehicle identifier. Ignored.</param>
        /// <param name="vehicleEntityType">Vehicle type. Ignored.</param>
        /// <returns>List of dummy submissions.</returns>
        public IList<Submission> Fetch(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType)
        {
            FetchByVehicleCalled = true;                        
            NumberFetchByVehicleCalled++;

            Collection<Submission> submissions = new Collection<Submission>();
            for (int i = 0; i < NumberSubmissionsToFetch; i++)
            {
                submissions.Add(Submission.NewSubmission(i + 1, i + 1, MessageTypes.Price));
            }
            return submissions;
        }

        /// <summary>
        /// Fetch the failed submissions that we should try to distribute.
        /// </summary>        
        /// <param name="maxAttempts">Only retrieve submissions that have been attempted fewer than this many times.</param>
        /// <param name="maxDaysBack">Maximum number of days back to fetch failures.</param>
        /// <returns>Submission that should be redistributed.</returns>
        public IList<Submission> FetchFailuresToRetry(int maxAttempts, int maxDaysBack)
        {
            FetchFailuresCalled = true;
            NumberFetchFailuresCalled++;

            Random random = new Random();

            Collection<Submission> submissions = new Collection<Submission>();
            for (int i = 0; i < NumberSubmissionsToFetch; i++)
            {
                submissions.Add(Submission.NewSubmission(i + 1, i + 1, MessageTypes.Price, random.Next(maxAttempts)));
            }
            return submissions;
        }        

        /// <summary>
        /// Insert the given submission to the database. Does nothing.
        /// </summary>
        /// <param name="value">Submission to insert. Ignored.</param>
        protected override void Insert(Submission value)
        {
            InsertCalled = true;            
            NumberInsertCalled++;
        }

        /// <summary>
        /// Update the given submission to the database. Does nothing.
        /// </summary>
        /// <param name="value">Submission to update. Ignored.</param>
        protected override void Update(Submission value)
        {
            UpdateCalled = true;            
            NumberUpdateCalled++;
        }

        #endregion    
    
        #region Stale

        /// <summary>
        /// Get the stale and valid message types for a submission.
        /// </summary>
        /// <param name="submissionId">Submission to check the message types of. Ignored.</param>
        /// <returns>The message types that are stale. Configurable. Defaults to 0.</returns>
        public MessageTypes FetchStaleTypes(int submissionId)
        {
            FetchStaleTypesCalled = true;
            NumberFetchStaleTypesCalled++;
            
            return StaleTypes;
        }

        /// <summary>
        /// Log that the message types represented by the given bit field were stale for the given submission.        
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="messageTypes">Message types that are stale.</param>
        public void LogStale(int submissionId, int messageTypes)
        {
            LogStaleCalled = true;
            NumberLogStaleCalled++;
        }        

        /// <summary>
        /// Fetch the price that is more recent than the one tied to the submission with the given identifier.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Price.</returns>
        public Price FetchSupersedingPrice(int submissionId)
        {
            FetchSupersedingPriceCalled = true;
            NumberFetchSupersedingPriceCalled++;

            if (SupersedingPrice == null)
            {
                Random random = new Random();
                return new Price(PriceType.Internet, random.Next());           
            }
            return SupersedingPrice;
        }

        #endregion

        #region Error

        /// <summary>        
        /// Fetch any error messages associated with the given submissions. Return value is configurable, but defaults
        /// to an empty string.
        /// </summary>
        /// <param name="submissionId">Submissions to set the error messages of. Ignored.</param>
        /// <returns>Configurable. Defaults to empty string.</returns>
        public Collection<FirstLookError> FetchErrors(int submissionId)
        {
            FetchErrorCalled = true;            
            NumberFetchErrorCalled++;

            return Errors;
        }

        /// <summary>
        /// Log to the database an error that occurred to a submission. Does nothing.
        /// </summary>
        /// <param name="firstLookError">Error to save to the database. Ignored.</param>        
        public void LogError(FirstLookError firstLookError)
        {
            LogErrorCalled = true;            
            NumberLogErrorCalled++;
        }

        #endregion

        #region Success

        /// <summary>
        /// Fetch the message types that were successfully handled by the provider for the submission with the given
        /// identifier. Return value is configurable, but defaults to an empty collection.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Configurable. Defaults to an empty collection.</returns>
        public Collection<int> FetchSuccesses(int submissionId)
        {
            FetchSuccessCalled = true;            
            NumberFetchSuccessCalled++;

            return Successes;
        }

        /// <summary>
        /// Log that the message types represented by the given bit field were successful for the given submission.        
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="messageTypes">Message types that were accepted.</param>
        public void LogSuccess(int submissionId, int messageTypes)
        {
            LogSuccessCalled = true;            
            NumberLogSuccessCalled++;
        }

        #endregion
    }
}
