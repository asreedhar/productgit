using System;
using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock definition of the binding to a provider.
    /// </summary>    
    public class MockBinding : IBinding
    {
        #region Status Tracking

        // Flags to signal if a given method was called.
        public static bool CalledPublish;
        public static int  NumberCalledPublish;

        // Miscellaneous status tracking.
        public static Collection<MessageTypes> SupportedTypes = new Collection<MessageTypes>();

        /// <summary>
        /// Reset the status tracking elements of this mock class.
        /// </summary>
        public static void Reset()
        {
            CalledPublish = false;
            NumberCalledPublish = 0;

            SupportedTypes.Clear();
            SupportedTypes.Add(MessageTypes.Price);
            SupportedTypes.Add(MessageTypes.Description);
        }

        #endregion
        
        #region Binding

        /// <summary>
        /// The types of messages that this binding is capable of sending. Configurable.
        /// </summary>
        public Collection<MessageTypes> SupportedMessageTypes
        {
            get { return SupportedTypes; }
        }

        /// <summary>
        /// Distribute a submission of a message using the given account credentials.
        /// </summary>        
        /// <param name="submission">Submission to submit.</param>
        public void Publish(ISubmission submission)
        {
            CalledPublish = true;
            NumberCalledPublish++;
        }

        #endregion
    }
}
