using FirstLook.Distribution.DomainModel.Distributors;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the submitter.
    /// </summary>
    public class MockPublisher : Publisher
    {
        #region Status Tracking

        public static bool PublishCalled;
        public static int NumberPublishCalled;

        /// <summary>
        /// Reset static status variables to default values.
        /// </summary>
        public static void Reset()
        {
            PublishCalled = false;
            NumberPublishCalled = 0;
        }

        #endregion

        /// <summary>
        /// Take an item off the queue and submit it to a provider binding directly, without having to go to through
        /// the thread pool.
        /// </summary>
        public override void Publish()
        {
            PublishCalled = true;
            NumberPublishCalled++;
            Publish(null);
        }
    }
}
