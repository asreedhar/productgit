using System;
using System.Net;
using FirstLook.Distribution.DomainModel.Services.AutoTrader;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the AutoTrader web service.
    /// </summary>
    public class MockAutoTraderService : IAutoTraderService
    {
        #region Status Tracking

        // Were the given functions called?        
        public static bool UpdateCarSalePriceCalled;
        public static bool SetCredentialsCalled;

        // What should the result be?
        public static bool ThrowsException;

        // Miscellaneous.
        public static ICredentials ServiceCredentials;

        /// <summary>
        /// Reset the static status variables to their default values.
        /// </summary>
        public static void Reset()
        {
            SetCredentialsCalled = false;
            UpdateCarSalePriceCalled = false;
            ThrowsException = false;
        }

        #endregion

        #region Service

        /// <summary>
        /// Update the sale price of a car. Return value is configurable, but defaults to "Succes".
        /// </summary>
        /// <param name="carInfo">Dealer id, VIN, comments and price.</param>
        /// <returns>Configurable. Defaults to "Success".</returns>
        public string updateCar(updateCarInfo carInfo)
        {
            UpdateCarSalePriceCalled = true;

            if (ThrowsException)
            {
                throw new Exception("AutoTrader exception.");
            }
            return "Success";
        }

        /// <summary>
        /// Credentials used to access the AutoTrader web service. Does nothing other than track when Set is called.
        /// Get is not supported.
        /// </summary>
        public ICredentials Credentials
        {
            get
            {
                throw new NotSupportedException();
            }            
            set
            {
                SetCredentialsCalled = true;
                ServiceCredentials = value;
            }
        }

        #endregion
    }
}
