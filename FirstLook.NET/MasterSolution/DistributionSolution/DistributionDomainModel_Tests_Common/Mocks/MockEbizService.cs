﻿using FirstLook.Distribution.DomainModel.Services.eBiz;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the eBiz web service.
    /// </summary>
    public class MockEBizService : IEBizService
    {
        #region Status Tracking

        // Were the given functions called?
        public static bool SendVehicleCalled;
        public static bool UpdatePriceCalled;
        public static bool UpdateDescriptionCalled;

        // What should the result be?
        public static string Response;

        /// <summary>
        /// Reset the static status variables to their default values.
        /// </summary>
        public static void Reset()
        {
            SendVehicleCalled = false;
            UpdatePriceCalled = false;
            UpdateDescriptionCalled = false;
            Response = "<status>Success</status>";
        }

        #endregion

        #region Service

        /// <summary>
        /// Send vehicle information for the account with the given credentials. Return value is configurable.
        /// </summary>
        /// <param name="id">FirstLook identifier. Ignored.</param>
        /// <param name="auth">FirstLook guid. Ignored.</param>
        /// <param name="xmlRequest">Xml payload. Ignored.</param>
        /// <returns>Configurable.</returns>
        public string SendVehicle(string id, string auth, string xmlRequest)
        {
            SendVehicleCalled = true;

            if (xmlRequest.Contains("updateprice"))
            {
                UpdatePriceCalled = true;
            }
            else if (xmlRequest.Contains("updatedescription"))
            {
                UpdateDescriptionCalled = true;
            }

            return Response;
        }

        #endregion
    }
}
