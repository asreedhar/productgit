using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Distributors;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the queue interface. Will capture which methods were called and when.
    /// </summary>
    public class MockQueue : IQueue
    {
        #region Status Tracking

        // Flags to signal if a given method was called.
        public static bool PushCalled;
        public static bool PopCalled;

        // Counts of how many times a given method was called since the last reset.
        public static int NumberPushCalled;
        public static int NumberPopCalled;

        // Miscellaneous.
        public static int QueueSize;

        /// <summary>
        /// Reset the values of all the status veriables.
        /// </summary>
        public static void Reset()
        {
            PushCalled = false;
            PopCalled = false;

            NumberPushCalled = 0;
            NumberPopCalled = 0;

            QueueSize = 0;
        }

        #endregion

        #region Queue

        /// <summary>
        /// Push a submission onto the queue. Does nothing.
        /// </summary>
        /// <param name="submission">Submission to push onto the queue. Ignored.</param>
        public void Push(Submission submission)
        {
            PushCalled = true;            
            NumberPushCalled++;
            return;
        }

        /// <summary>
        /// Pop a submission off the queue. Returns a dummy submission.
        /// </summary>
        /// <returns>Dummy submission.</returns>
        public Submission Pop()
        {
            PopCalled = true;            
            NumberPopCalled++;
            return Submission.NewSubmission(1, 1, MessageTypes.Price);
        }

        /// <summary>
        /// Get the number of entries on the queue. Return value is configurable, but defaults to 0.
        /// </summary>
        /// <returns>Configurable. Defaults to 0.</returns>
        public int Count()
        {
            return QueueSize;
        }

        #endregion
    }
}
