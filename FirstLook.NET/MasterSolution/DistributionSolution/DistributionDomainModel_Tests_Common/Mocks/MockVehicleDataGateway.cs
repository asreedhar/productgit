using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the inventory data gateway.
    /// </summary>
    public class MockVehicleDataGateway : IVehicleDataGateway
    {
        #region Status Tracking

        public static bool VehicleExists;
        public static string Vin;
        public static int Year;
        public static string Make;
        public static string Model;

        /// <summary>        
        /// Reset the values of the static status-tracking variables.        
        /// </summary>
        public static void Reset()
        {
            VehicleExists = true;
            Vin = "Test VIN";
            Year = 2010;
            Make = "Make";
            Model = "Model";
        }

        #endregion

        #region Inventory

        /// <summary>
        /// Fetch the VIN for the vehicle represented by the given identifier. Return value is configurable, but 
        /// defaults to "Test VIN".
        /// </summary>
        /// <param name="inventoryId">Identifier of the vehicle to fetch the VIN for. Ignored.</param>
        /// <returns>Configurable. Defaults to "Test VIN".</returns>
        public Inventory FetchInventory(int inventoryId)
        {
            Inventory inventory = new Inventory(inventoryId);
            inventory.Vin = Vin;
            inventory.Year = Year;
            inventory.Make = Make;
            inventory.Model = Model;
            return inventory;
        }

        /// <summary>
        /// Check if inventory with the given identifier and type exists. Return value is configurable, but defaults to
        /// true.
        /// </summary>
        /// <param name="inventoryId">Vehicle entity identifier. Ignored.</param>
        /// <returns>Configurable. Defaults to true.</returns>
        public bool InventoryExists(int inventoryId)
        {
            return VehicleExists;
        }

        #endregion

        #region IVehicleDataGateway Members


        public ADPInventory FetchADPInventory(int inventoryId, int dealerId)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
