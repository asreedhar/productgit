using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the publication data gateway. Will capture which methods were called and when.
    /// </summary>
    public class MockPublicationDataGateway : DataGateway<Publication>, IPublicationDataGateway
    {
        #region Status Tracking

        // Flags to signal if a given method was called.
        public static bool ExistsCalled;
        public static bool ExistsByVehicleCalled;
        public static bool FetchCalled;
        public static bool FetchByVehicleCalled;
        public static bool InsertCalled;
        public static bool UpdateCalled;

        // Counts of how many times a given method was called since the last reset.
        public static int NumberExistsCalled;
        public static int NumberExistsByVehicleCalled;
        public static int NumberFetchCalled;
        public static int NumberFetchByVehicleCalled;
        public static int NumberInsertCalled;
        public static int NumberUpdateCalled;

        // XML file to read values in from on fetch.
        public static string InputDataFile;

        // Miscellaneous.
        public static bool PublicationExists;
        public static PublicationState PublicationStatus;

        /// <summary>
        /// Reset static status variables to default values.
        /// </summary>
        public static void Reset()
        {
            ExistsCalled = false;
            ExistsByVehicleCalled = false;
            FetchCalled = false;
            FetchByVehicleCalled = false;
            InsertCalled = false;
            UpdateCalled = false;

            NumberExistsCalled = 0;
            NumberExistsByVehicleCalled = 0;
            NumberFetchCalled = 0;
            NumberFetchByVehicleCalled = 0;
            NumberInsertCalled = 0;
            NumberUpdateCalled = 0;

            InputDataFile = "TestMessage.xml";
            PublicationExists = true;
            PublicationStatus = PublicationState.NotDefined;
        }

        #endregion

        #region Publication

        /// <summary>
        /// Does a publication exist for the given identifier? Return value is configurable, but defaults to true.
        /// </summary>
        /// <param name="publicationId">Publication identifier. Ignored.</param>        
        /// <returns>Configurable. Defaults to true.</returns>
        public bool Exists(int publicationId)
        {
            ExistsCalled = true;                        
            NumberExistsCalled++;

            return PublicationExists;
        }

        /// <summary>
        /// Does a publication exist for the given identifiers? Return value is configurable, but defaults to true.
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <param name="vehicleEntityId">Vehicle identifier. Ignored.</param>
        /// <param name="vehicleEntityType">Vehicle type. Ignored.</param>
        /// <returns>Configurable. Defaults to true.</returns>
        public bool Exists(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType)
        {
            ExistsByVehicleCalled = true;            
            NumberExistsByVehicleCalled++;

            return PublicationExists;
        }

        /// <summary>
        /// Fetch the publication with the given identifier. Return value is configurable based on the xml file read to
        /// populate the publication with.
        /// </summary>
        /// <param name="publicationId">Publication identifier. Ignored.</param>
        /// <returns>Configurable. Defaults to the message in TestMessage.xml.</returns>
        public Publication Fetch(int publicationId)
        {
            FetchCalled = true;            
            NumberFetchCalled++;

            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml(InputDataFile);            
            return Publication.NewPublication(message);
        }

        /// <summary>
        /// Fetch the parts of the publication's message that match the given types.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <param name="messageTypes">Message types to retrive.</param>
        /// <returns>Parts of message that match the given types, if applicable.</returns>
        public Message Fetch(int publicationId, MessageTypes messageTypes)
        {
            FetchCalled = true;
            NumberFetchCalled++;

            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml(InputDataFile);
            message = TestUtilities.FilterMessageByType(message, messageTypes);            
            return Publication.NewPublication(message).Message;
        }

        /// <summary>
        /// Fetch the publication with the given identifier. Does nothing and returns null.
        /// </summary>
        /// <param name="dealerId">Dealer identifier. Ignored.</param>
        /// <param name="vehicleEntityId">Vehicle identifier. Ignored.</param>
        /// <param name="vehicleEntityType">Vehicle type. Ignored.</param>
        /// <returns>Null.</returns>
        public Publication Fetch(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType)
        {
            FetchByVehicleCalled = true;            
            NumberFetchByVehicleCalled++;

            return null;
        }

        /// <summary>
        /// Insert the given publication. Does nothing.
        /// </summary>
        /// <param name="value">Publication object. Ignored.</param>
        protected override void Insert(Publication value)
        {
            InsertCalled = true;            
            NumberInsertCalled++;
            PublicationStatus = value.PublicationStatus;
        }

        /// <summary>
        /// Update the given publication. Does nothing.
        /// </summary>
        /// <param name="value">Publication object. Ignored.</param>
        protected override void Update(Publication value)
        {
            UpdateCalled = true;            
            NumberUpdateCalled++;
            PublicationStatus = value.PublicationStatus;
        }

        #endregion
    }
}
