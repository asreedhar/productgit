using System;
using FirstLook.Distribution.DomainModel.Services.AutoUplink;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Mocks
{
    /// <summary>
    /// Mock of the AutoUplink web service.
    /// </summary>
    public class MockAutoUplinkService : IAutoUplinkService
    {
        #region Status Tracking

        // Were the given functions called?
        public static bool InternetPriceUpdateCalled;
        public static bool PriceUpdateCalled;        
        public static bool CommentUpdateCalled;
        public static bool EnhancedVehicleHighlightsUpdateCalled;

        // Return code we wish to return on a given web service call.
        public static string ExpectedInternetPriceReturnCode;
        public static string ExpectedPriceReturnCode;
        public static string ExpectedCommentReturnCode;
        public static string EnhancedVehicleHighlightsReturnCode;

        // Should we simulate a SOAP exception?
        public static bool ThrowException;

        /// <summary>
        /// Reset static status variables to default values.
        /// </summary>
        public static void Reset()
        {
            InternetPriceUpdateCalled       = false;
            PriceUpdateCalled               = false;            
            CommentUpdateCalled             = false;
            EnhancedVehicleHighlightsUpdateCalled = false;
            ExpectedInternetPriceReturnCode = "101";
            ExpectedPriceReturnCode         = "101";
            ExpectedCommentReturnCode       = "101";
            EnhancedVehicleHighlightsReturnCode = "101";
            ThrowException                  = false;
        }

        #endregion

        #region Service

        /// <summary>
        /// Mock of the price update web service call. Returns the expected return code.
        /// </summary>
        /// <param name="VehicleInventoryAuth">Authentication details. Ignored.</param>
        /// <param name="VIN">Vehicle identifier. Ignored.</param>
        /// <param name="Price">New price. Ignored.</param>        
        /// <returns>Expected return code.</returns>
        public string PriceUpdate(WSAuth VehicleInventoryAuth, string VIN, double Price)
        {
            PriceUpdateCalled = true;

            if (ThrowException)
            {
                throw new Exception();
            }
                        
            return ExpectedPriceReturnCode;
        }

        /// <summary>
        /// Mock of the internet price update web service call. Returns the expected return code.
        /// </summary>
        /// <param name="VehicleInventoryAuth">Authentication details. Ignored.</param>
        /// <param name="VIN">Vehicle identifier. Ignored.</param>
        /// <param name="InternetPrice">New internet price. Ignored.</param>
        /// <returns>Expected return code.</returns>
        public string InternetPriceUpdate(WSAuth VehicleInventoryAuth, string VIN, double InternetPrice)
        {
            InternetPriceUpdateCalled = true;

            if (ThrowException)
            {
                throw new Exception();
            }

            return ExpectedInternetPriceReturnCode;
        }

        /// <summary>
        /// Mock of the comment update web service call. Returns the expected return code.
        /// </summary>        
        /// <param name="VehicleInventoryAuth">Authentication details. Ignored.</param>
        /// <param name="VIN">Vehicle identifier. Ignored.</param>
        /// <param name="Comment">New description. Ignored.</param>
        /// <returns>Expected return code.</returns>        
        public string CommentUpdate(WSAuth VehicleInventoryAuth, string VIN, string Comment)
        {
            CommentUpdateCalled = true;

            if (ThrowException)
            {
                throw new Exception();
            }

            return ExpectedCommentReturnCode;
        }

        public string EnhancedVehicleHighlightsUpdate(WSAuth VehicleInventoryAuth, string VIN, string EnhancedVehicleHighlights)
        {
            EnhancedVehicleHighlightsUpdateCalled = true;

            if (ThrowException)
            {
                throw new Exception();
            }

            return EnhancedVehicleHighlightsReturnCode;
        }

        #endregion
    }
}
