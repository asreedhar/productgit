using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.DomainModel.Bindings;

namespace FirstLook.Distribution.DomainModel.Tests.Common.Utility
{
    /// <summary>
    /// Setup work common to many tests.
    /// </summary>
    public static class TestUtilities
    {       
        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Read a message transfer object from the xml file with the given filename.
        /// </summary>
        /// <param name="fileName">Name of the xml file to read.</param>
        /// <returns>Message transfer object.</returns>
        public static TransferObjects.V_1_0.Message ParseMessageFromXml(string fileName)
        {
            System.Xml.Serialization.XmlSerializer serializer =
                new System.Xml.Serialization.XmlSerializer(typeof(TransferObjects.V_1_0.Message));

            string resourceName = "FirstLook.Distribution.DomainModel.Tests.Common.Resources." + fileName;

            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream stream = assembly.GetManifestResourceStream(resourceName);
            
            if (stream == null)
            {
                throw new ArgumentException("Resource for fileName not found.");
            }

            TextReader reader = new StreamReader(stream);
            return (TransferObjects.V_1_0.Message)serializer.Deserialize(reader);
        }

        /// <summary>
        /// Filter the given message to only include body parts that match those in the given message types.
        /// </summary>
        /// <param name="message">Message to filter.</param>
        /// <param name="messageTypes">Message types to filter by.</param>
        /// <returns>Filtered message.</returns>
        public static TransferObjects.V_1_0.Message FilterMessageByType(TransferObjects.V_1_0.Message message, 
                                                                        Entities.MessageTypes messageTypes)
        {
            TransferObjects.V_1_0.Message newMessage = new TransferObjects.V_1_0.Message();
            newMessage.From    = message.From;
            newMessage.Subject = message.Subject;
            newMessage.Body    = new TransferObjects.V_1_0.MessageBody();

            foreach (TransferObjects.V_1_0.BodyPart bodyPart in message.Body.Parts)
            {
                // If the message has an advertisement, and those are allowed add it.
                if (bodyPart.BodyPartType == TransferObjects.V_1_0.BodyPartType.Advertisement &&
                    (messageTypes & Entities.MessageTypes.Description) == Entities.MessageTypes.Description)
                {
                    newMessage.Body.Parts.Add(bodyPart);
                }
                // If the message has a price, and those are allowed, add it.
                else if (bodyPart.BodyPartType == TransferObjects.V_1_0.BodyPartType.Price &&
                    (messageTypes & Entities.MessageTypes.Price) == Entities.MessageTypes.Price)
                {
                    newMessage.Body.Parts.Add(bodyPart);
                }
                // If the message has a price, and those are allowed, add it.
                else if (bodyPart.BodyPartType == TransferObjects.V_1_0.BodyPartType.VehicleInformation &&
                    (messageTypes & Entities.MessageTypes.Mileage) == Entities.MessageTypes.Mileage)
                {
                    newMessage.Body.Parts.Add(bodyPart);
                }
            }
            
            return newMessage;
        }

        /// <summary>
        /// Get a dealer and vehicle from the database. For use in integration testing.
        /// </summary>
        /// <param name="fileName">Name of the xml file to read.</param>
        /// <returns>Message transfer object with valid identifiers for dealer and vehicle.</returns>        
        public static TransferObjects.V_1_0.Message GetMessageWithValidIds(string fileName)
        {
            TransferObjects.V_1_0.Message message = ParseMessageFromXml(fileName);

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT TOP 1 BU.BusinessUnitID, IA.InventoryID " +
                                          "FROM dbo.BusinessUnit BU " +
                                          "JOIN FLDW.dbo.InventoryActive IA ON IA.BusinessUnitID = bu.BusinessUnitID " +
                                          "JOIN FLDW.dbo.Vehicle V ON V.BusinessUnitID = IA.BusinessUnitID AND V.VehicleID = IA.VehicleID " +
                                          "JOIN IMT.dbo.MakeModelGrouping MMG ON MMG.MakeModelGroupingID = V.MakeModelGroupingID " +
                                          "WHERE IA.InventoryActive=1";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected dealer and vehicle from the database, but got none.");
                        }

                        message.From.Id = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                        message.Subject.Id = reader.GetInt32(reader.GetOrdinal("InventoryID"));
                        message.Subject.VehicleType = TransferObjects.V_1_0.VehicleType.Inventory;
                    }
                }
            }
            return message;
        }

        /// <summary>
        /// Randomize the value of a price in the given message.
        /// </summary>                
        public static void RandomizePrice(TransferObjects.V_1_0.Message message)
        {
            Random random = new Random();

            foreach (TransferObjects.V_1_0.BodyPart bodyPart in message.Body.Parts)
            {
                TransferObjects.V_1_0.Price price = bodyPart as TransferObjects.V_1_0.Price;

                if (price != null)
                {
                    price.Value = random.Next(999999);
                }
            }
        }     

        #endregion

        #region Distribution Enabling

        /// <summary>
        /// Turn distribution as a whole on.
        /// </summary>
        public static void EnableDistribution()
        {
            ConfigurationManager.AppSettings["EnableDistribution"] = "1";
        }

        /// <summary>
        /// Turn distribution off as a whole.
        /// </summary>
        public static void DisableDistribution()
        {
            ConfigurationManager.AppSettings["EnableDistribution"] = "0";
        }

        /// <summary>
        /// Turns distribution on for the given dealer and provider in the database.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="providerId">Provider identifier.</param>
        public static void EnableDistributionForProvider(int dealerId, int providerId)
        {
            // Turn it on in the config files.
            switch ((BindingType)providerId)
            {
                case BindingType.Aultec:
                    ConfigurationManager.AppSettings["EnableAultec"] = "1";
                    break;

                case BindingType.AutoUplink:
                    ConfigurationManager.AppSettings["EnableAutoUplink"] = "1";
                    break;

                case BindingType.AutoTrader:
                    ConfigurationManager.AppSettings["EnableAutoTrader"] = "1";
                    break;

                case BindingType.GetAuto:
                    ConfigurationManager.AppSettings["EnableGetAuto"] = "1";
                    break;

                case BindingType.eBiz:
                    ConfigurationManager.AppSettings["EnableEbiz"] = "1";
                    break;

                default:
                    throw new ArgumentException("Unrecognized provider.");    
            }
            
            // Turn it on in the database.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }                
                
                using (IDbCommand command = connection.CreateCommand())
                {                    
                    command.CommandType = CommandType.Text;
                
                    // Turn on distribution for this dealer / provider.                    
                    command.CommandText =
                        "IF NOT EXISTS (SELECT 1 FROM Distribution.Account WHERE DealerID=" + dealerId + " AND ProviderID=" + providerId + ") " +
                        "EXEC Distribution.Account#Insert " + dealerId + ", " + providerId + ", NULL, NULL, '555', 1, 'bfung' " +
                        "ELSE EXEC Distribution.Account#Update " + dealerId + ", " + providerId + ", NULL, NULL, '555', 1, 'bfung'";

                    command.ExecuteNonQuery();
                
                    // Turn on all the message types for this dealer / provider.
                    for (int i = 1; i <= 3; i++)
                    {                        
                        command.CommandText =
                            "IF EXISTS " +
                            "(SELECT 1 FROM Distribution.ProviderMessageType WHERE ProviderID=" + providerId + " AND MessageTypes=" + i + ") " +
                            "IF NOT EXISTS " +
                            "(SELECT 1 FROM Distribution.AccountPreferences WHERE DealerID=" + dealerId + " AND ProviderID=" + providerId + " AND MessageTypes=" + i + ") " +
                            "INSERT INTO Distribution.AccountPreferences (DealerID, ProviderID, MessageTypes) VALUES (" + dealerId + ", " + providerId + ", " + i + ")";

                        command.ExecuteNonQuery();
                    }                                                                                          
                }                
            }                                    
        }        

        /// <summary>
        /// Disable distribution for the given dealer and provider.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="providerId">Provider identifier.</param>
        public static void DisableDistributionForProvider(int dealerId, int providerId)
        {
            // Turn it off in the config files.
            switch ((BindingType)providerId)
            {
                case BindingType.Aultec:
                    ConfigurationManager.AppSettings["EnableAultec"] = "0";
                    break;

                case BindingType.AutoUplink:
                    ConfigurationManager.AppSettings["EnableAutoUplink"] = "0";
                    break;

                case BindingType.AutoTrader:
                    ConfigurationManager.AppSettings["EnableAutoTrader"] = "0";
                    break;

                case BindingType.GetAuto:
                    ConfigurationManager.AppSettings["EnableGetAuto"] = "0";
                    break;

                case BindingType.eBiz:
                    ConfigurationManager.AppSettings["EnableEbiz"] = "0";
                    break;

                default:
                    throw new ArgumentException("Unrecognized provider.");
            }

            // Turn it off in the database.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {                    
                    command.CommandType = CommandType.Text;
                    
                    // Turn off distribution for this dealer / provider.                    
                    command.CommandText =
                        "EXEC Distribution.Account#Delete " + dealerId + ", " + providerId + ", 'bfung'";

                    command.ExecuteNonQuery();

                    // Delete all the message type preferences for this dealer / provider.
                    command.CommandText =
                        "DELETE FROM Distribution.AccountPreferences " +                        
                        "WHERE DealerId=" + dealerId + " AND ProviderID=" + providerId;

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Dealer Identifiers

        /// <summary>
        /// Get the identifier of a valid dealer from the database.
        /// </summary>
        /// <returns>Valid dealer identifier.</returns>
        public static int GetValidDealerIdentifier()
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;                    
                    command.CommandText =
                        "SELECT TOP 1 BusinessUnitID FROM dbo.BusinessUnit";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected dealer identifier from the database, but got none.");
                        }
                        return reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                    }
                }
            }
        }

        /// <summary>
        /// Get an identifier that does not belong to any dealer in the database.
        /// </summary>
        /// <returns>Invalid dealer identifier.</returns>
        public static int GetInvalidDealerIdentifier()
        {
            int invalidId = int.MaxValue;

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                while (true)
                {                
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText =
                            "SELECT 1 BusinessUnitID FROM dbo.BusinessUnit " +
                            "WHERE BusinessUnitID=" + invalidId;

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.Read())
                            {
                                return invalidId;
                            }
                            // Don't let it loop forever, not that this is realistic.
                            if (invalidId == int.MaxValue - 100)
                            {
                                throw new ApplicationException("Could not find a bogus dealer identifier.");
                            }
                            invalidId--;
                        }
                    }
                }
            }
        }

        #endregion        
    }
}
