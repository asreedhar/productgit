﻿using System;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using log4net;
using log4net.Config;

namespace FirstLook.Distribution.WebApplication
{
    public class Global : System.Web.HttpApplication
    {
        private static ILog _log;

        protected void Application_Start(object sender, EventArgs e)
        {
            // Setup logging
            XmlConfigurator.Configure();
            _log = LogManager.GetLogger(typeof(Global).FullName);
            _log.InfoFormat("Application_Start() for Distribution.WebApp version {0} has been called.", Assembly.GetExecutingAssembly().GetName().Version);

            RegistryFactory.GetRegistry().Register<IAccountDataGateway, AccountDataGateway>(ImplementationScope.Shared);

            //Added for fault reporting (health montioring) integration

            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Fault.DomainModel.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

        }

        protected void Application_End(object sender, EventArgs e)
        {
            _log.InfoFormat("Application_End() for Distribution.WebApp version {0} has been called.", Assembly.GetExecutingAssembly().GetName().Version);
            RegistryFactory.GetRegistry().Dispose();
        }
    }
}