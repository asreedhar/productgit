﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Data;

namespace FirstLook.Distribution.WebApplication
{
    public partial class Account : System.Web.UI.Page
    {
        protected void AccountData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridView parent = sender as GridView;
            if (parent != null)
            {
                HiddenField providerIdHiddenField = parent.Rows[e.RowIndex].FindControl("ProviderIdHiddenField") as HiddenField;
                if (providerIdHiddenField != null)
                {
                    e.NewValues["ProviderId"] = providerIdHiddenField.Value;
                }


            }
        }

        protected void AccountData_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            
            if (e.Exception != null)
            {
                HandleException(e.Exception);

                e.ExceptionHandled = true;
            }
            else
            {
                if (Convert.ToInt32(e.NewValues["ProviderId"]) == (int)BindingType.ADP && e.NewValues.Count > 0 && Convert.ToBoolean(e.NewValues[0]))
                {
                    new AccountDataGateway().SaveADPPriceMapping(Convert.ToInt32(Request.Params["DealerID"]), ddlADPPriceList.SelectedValue);
                }
            }
        }

        /// <summary>
        /// Only allow editing of the Password Field for eBiz
        /// </summary>
        /// <remarks>
        /// Uses row specific and order specific data to correctly find the BoundField.
        /// Please update the const's at the function head to match the gridview.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AccountData_RowEdting(object sender, GridViewEditEventArgs e)
        {
            const string rowLabel = "eBiz";
            const int rowLableCellIdx = 1;
            const int passwordControlIdx = 2;

            var gridViewRow = AccountData.Rows[e.NewEditIndex];
            bool isReadOnly = !gridViewRow.Cells[rowLableCellIdx].Text.Equals(rowLabel);

            var passwordCell = gridViewRow.Controls[passwordControlIdx] as DataControlFieldCell;
            if (passwordCell != null)
            {
                var passwordField = passwordCell.ContainingField as BoundField;
                if (passwordField != null)
                {
                    passwordField.ReadOnly = isReadOnly;
                }
            }

        }

        protected ListItemCollection GetMessageTypeChoices(object dataItem, string accountprovidermessagetypes)
        {
            DataRowView rw = dataItem as DataRowView;
            ListItemCollection listItemCollection = new ListItemCollection();
            if (rw != null)
            {
                DataView view = rw.CreateChildView(accountprovidermessagetypes);
                DataTable table = view.ToTable();
                foreach (DataRow row in table.Rows)
                {
                    listItemCollection.Add(new ListItem((string)row["MessageTypeDescription"]));
                }
            }
            return listItemCollection;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            ErrorList.Items.Clear();
            ErrorList.Visible = false;
           
            if(!IsPostBack)
            {
                List<ADPPriceType> priceTypes = new AccountDataGateway().FetchADPPriceType();
                ddlADPPriceList.DataTextField = "Description";
                ddlADPPriceList.DataValueField = "TargetId";
                ddlADPPriceList.DataSource = priceTypes;
                ddlADPPriceList.DataBind();

                DomainModel.Entities.Account account = null;
                try
                {
                    account = new AccountDataGateway().FetchADPAccount(Convert.ToInt32(Convert.ToInt32(Request.Params["DealerID"])));
                }
                catch (Exception ex)
                {

                }

                if (account != null)
                {
                    ddlADPPriceList.Text = account.SelectedValue;
                }
            }
            
        }

        private void HandleException(Exception exception)
        {
            if (!string.IsNullOrEmpty(exception.Message))
            {
                ErrorList.Items.Add(new ListItem(exception.Message));
            }

            if (exception.InnerException != null &&
                !string.IsNullOrEmpty(exception.InnerException.Message))
            {
                ErrorList.Items.Add(new ListItem(exception.InnerException.Message));
            }

            ErrorList.Visible = ErrorList.Items.Count > 0;
        }

        protected void AccountData_Cancel(Object sender, GridViewCancelEditEventArgs e)
        {
                DomainModel.Entities.Account account = null;
                try
                {
                    account =
                        new AccountDataGateway().FetchADPAccount(
                            Convert.ToInt32(Convert.ToInt32(Request.Params["DealerID"])));
                }
                catch (Exception ex)
                {

                }

                if (account != null)
                {
                    ddlADPPriceList.Text = account.SelectedValue;
                }
            
        }
    }
}
