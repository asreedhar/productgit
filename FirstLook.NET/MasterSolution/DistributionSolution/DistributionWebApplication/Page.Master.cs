﻿using System;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;

namespace FirstLook.Distribution.WebApplication
{
    public partial class Page : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            int id;

            if (int.TryParse(Request.QueryString["DealerId"], out id))
            {
                IAccountDataGateway accountDataGateway = RegistryFactory.GetResolver().Resolve<IAccountDataGateway>();

                if (!accountDataGateway.DealerExists(id))
                {
                    Response.Redirect("~/ErrorPage.aspx?ErrorCode=410");
                }
            }
            else
            {
                Response.Redirect("~/ErrorPage.aspx?ErrorCode=410");
            }
        }
    }
}
