﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="FirstLook.Distribution.WebApplication.Account" %>

<%@ OutputCache Location="None" VaryByParam="None" %>


<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" ID="BodyContent" runat="server">
    <script type="text/javascript" language="javascript">
    
        $(document).ready(function () {
            /*Get Table row with ADP provider text*/
            var tableRow = $("td").filter(function () {
                return $(this).text().toUpperCase() == "ADP";
            }).closest("tr");

            if (tableRow.find('td:first input[type="checkbox"]')[0].disabled) {
                //Add Price type Label
                var divDropdown = $('#divADPPriceList').find('option[selected="selected"]');
                var spanPrice = tableRow.find('span:contains("Price")');
                spanPrice[0].innerHTML=spanPrice[0].innerHTML + ' (' + divDropdown[0].innerHTML + ')';
            } else {
                //Show dropdown
                if (tableRow.find('td:first input[type="checkbox"]')[0].checked) {
                    AddPriceRadioButtonClickedEventForADP();
                    AddRowWithDropdown();
                    $('input[type="radio"][value!="Price"]').click(function () {
                        RemoveADPDropDownRow();
                    });
                }
                //Get check box and radio button
                tableRow.find('td:first input[type="checkbox"]').click(function () {
                    if (this.checked) {
                        AddPriceRadioButtonClickedEventForADP();
                        AddRowWithDropdown();
                        $('input[type="radio"][value!="Price"]').click(function () {
                            RemoveADPDropDownRow();
                        });
                    } else {
                        RemoveADPDropDownRow();
                        $('input[type="radio"][value~="Price"]').unbind('click');
                    }

                });
            }
        }
        );

        function AddRowWithDropdown() {
              var optionButtonRow = $('input[type="radio"][value~="Price"]');

              if (optionButtonRow.length > 0 && optionButtonRow[0].checked) {
                  if ($('.adpPriceType').length === 1) {
                      optionButtonRow.closest('tr').after('<tr><td>' + $('#divADPPriceList')[0].innerHTML + '</td><tr>');
                  }
              }
        }

        function AddPriceRadioButtonClickedEventForADP  () {
            var optionButtonRow = $('input[type="radio"][value~="Price"]');
            optionButtonRow.click(function () {
                AddRowWithDropdown(optionButtonRow.closest("tr"));
            });
        }

        function RemoveADPDropDownRow() {
            if ($('.adpPriceType').closest("tr").length > 0) {
                $('.adpPriceType').closest("tr").remove();
            }
        }
    </script>

    <dwc:AccountDataSource ID="AccountDataSource" runat="server">
        <Parameters>
            <asp:QueryStringParameter Name="DealerId" Type="Int32" QueryStringField="DealerId" />
        </Parameters>
    </dwc:AccountDataSource>
    
    <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList">
    </asp:BulletedList>
    
    <asp:GridView ID="AccountData" runat="server"
            DataSourceID="AccountDataSource"
            AutoGenerateColumns="false"
            OnRowUpdating="AccountData_RowUpdating"
            OnRowUpdated="AccountData_RowUpdated"
            OnRowEditing="AccountData_RowEdting"
            OnRowCancelingEdit="AccountData_Cancel">
        <Columns>
            <asp:CheckBoxField DataField="Active" 
                    HeaderText="" 
                    HeaderStyle-CssClass="col_active"
                    ItemStyle-CssClass="col_active" />
            <asp:BoundField DataField="ProviderName" ReadOnly="True" 
                    HeaderText="Export To:" 
                    HeaderStyle-CssClass="col_provider"
                    ItemStyle-CssClass="col_provider" />
            <asp:BoundField DataField="Password"
                    HeaderText="Client GUID:" 
                    HeaderStyle-CssClass="col_password"
                    ItemStyle-CssClass="col_password" />
            <asp:BoundField DataField="DealerCode" 
                    HeaderText="Dealer Code" 
                    HeaderStyle-CssClass="col_dealer_code"
                    ItemStyle-CssClass="col_dealer_code" />
            <asp:TemplateField HeaderText="Sending:">
                <ItemTemplate>
                    <asp:Label ID="MessageTypeDescriptionLabel" Text='<%# Eval("MessageTypeDescription") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:RadioButtonList ID="MessageTypesRadioButtonList" runat="server"
                        SelectedValue='<%# Bind("MessageTypeDescription") %>'
                        DataSource='<%# GetMessageTypeChoices(Container.DataItem, "AccountProviderMessageTypes")%>'>
                    </asp:RadioButtonList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField 
                    HeaderStyle-CssClass="col_controls"
                    ItemStyle-CssClass="col_controls">
                <ItemTemplate>
                    <asp:LinkButton ID="EditButton" runat="server"
                        CommandName="Edit" 
                        Text="Edit" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:HiddenField ID="ProviderIdHiddenField" runat="server"
                        Value='<%# Eval("ProviderId") %>' />
                    <asp:Button ID="SaveButton" runat="server"
                        CommandName="Update"
                        Text="Save" />
                    <asp:LinkButton ID="CancelButton" runat="server"
                        CommandName="Cancel"
                        Text="Cancel" />
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    
    <div id="divADPPriceList" style="display: none">
        <label for="ddlADPPriceList"> Target : </label>
        <asp:DropDownList runat="server" ID="ddlADPPriceList" EnableViewState="True" CssClass="adpPriceType"/>    
    </div>
    
</asp:Content>
