﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Web.Services.Protocols;
using System.Xml;

namespace FirstLook.Distribution.DomainModel.Extensions
{
    public class TraceSoapExtension : SoapExtension
    {
        Stream _oldStream;
        Stream _newStream;
       
        public static string XmlRequest
        {
            get
            {
                return CallContext.GetData("xmlRequest") as string;
            }
            set
            {
                CallContext.SetData("xmlRequest", value);
            }
        }

        public static string XmlResponse
        {
            get
            {
                return CallContext.GetData("xmlResponse") as string;
            }
            set
            {
                CallContext.SetData("xmlResponse", value);
            }
        }
        

        // Save the Stream representing the SOAP request or SOAP response into
        // a local memory buffer.
        public override Stream ChainStream(Stream stream)
        {
            _oldStream = stream;
            _newStream = new MemoryStream();
            return _newStream;
        }


        // When the SOAP extension is accessed for the first time, the XML Web
        // service method it is applied to is accessed to store the file
        // name passed in, using the corresponding SoapExtensionAttribute.    
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return attribute;
        }

        // The SOAP extension was configured to run using a configuration file
        // instead of an attribute applied to a specific XML Web service
        // method.
        public override object GetInitializer(Type WebServiceType)
        {
            return null;
        }

        // Receive the file names,logtypemode stored by GetInitializer and store it in a
        // member variables for this specific instance.
        public override void Initialize(object initializer)
        {

        }

        //  If the SoapMessageStage is such that the SoapRequest or
        //  SoapResponse is still in the SOAP format to be sent or received,
        //  save it out to a file.
        public override void ProcessMessage(SoapMessage message)
        {
                switch (message.Stage)
                {
                    case SoapMessageStage.BeforeSerialize:
                        break;
                    case SoapMessageStage.AfterSerialize:
                        XmlRequest = GetString(_newStream);
                        CopyStream(_newStream, _oldStream);
                        break;
                    case SoapMessageStage.BeforeDeserialize:
                        CopyStream(_oldStream, _newStream);
                        XmlResponse = GetString(_newStream);
                        _newStream.Position = 0;
                        break;
                    case SoapMessageStage.AfterDeserialize:
                        break;
                    default:
                        throw new Exception("invalid stage");
                }
        }

        /// <summary>
        /// Copies a stream.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        private void CopyStream(Stream from, Stream to)
        {
            TextReader reader = new StreamReader(from);
            TextWriter writer = new StreamWriter(to);
            writer.WriteLine(reader.ReadToEnd());
            writer.Flush();
        }

        /// <summary>
        /// Returns the XML representation of the Soap Envelope in the supplied stream.
        /// Resets the position of stream to zero.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private XmlDocument GetSoapEnvelope(Stream stream)
        {
            XmlDocument xml = new XmlDocument();
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            xml.LoadXml(reader.ReadToEnd());
            stream.Position = 0;
            return xml;
        }

        public string GetString(Stream stream)
        {
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            string data = reader.ReadToEnd();
            stream.Position = 0;
            return data;
        }

    }
}
