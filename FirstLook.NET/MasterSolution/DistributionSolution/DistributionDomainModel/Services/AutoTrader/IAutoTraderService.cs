using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace FirstLook.Distribution.DomainModel.Services.AutoTrader
{
    public interface IAutoTraderService
    {
        /// <remarks/>
        [SoapRpcMethod("", RequestNamespace = "http://www.autotrader.com/web-services/inventory",
            ResponseNamespace = "http://www.autotrader.com/web-services/inventory", Use = SoapBindingUse.Literal)]
        [return: XmlElement("status")]
        string updateCar(updateCarInfo carInfo);

        ICredentials Credentials { get; set; }
    }

    public partial class InventoryService : IAutoTraderService
    {
    }
}