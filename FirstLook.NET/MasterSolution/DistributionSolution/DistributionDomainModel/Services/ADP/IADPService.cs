﻿
namespace FirstLook.Distribution.DomainModel.Services.ADP
{
    public interface IADPService
    {
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("",
            RequestNamespace = "http://www.dmotorworks.com/pip-vehicle",
            ResponseNamespace = "http://www.dmotorworks.com/pip-vehicle",
            Use = System.Web.Services.Description.SoapBindingUse.Literal,
            ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        vehicleReadResult read(authenticationToken arg0, dealerId arg1, vehicleReadRequest arg2); 

        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("",
            RequestNamespace = "http://www.dmotorworks.com/pip-vehicle",
            ResponseNamespace = "http://www.dmotorworks.com/pip-vehicle",
            Use = System.Web.Services.Description.SoapBindingUse.Literal,
            ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        vehicleSaveResult update(authenticationToken arg0,dealerId arg1, vehicle arg2, fileType arg3, bool arg3Specified);
    }

    public partial class VehicleSaveService : IADPService
    {

    }
}
