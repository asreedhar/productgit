using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace FirstLook.Distribution.DomainModel.Services.GetAuto
{
    public interface IGetAutoService
    {
        /// <remarks/>
        [SoapDocumentMethod("http://DealerSpecialties.com/GetAllVehicles",
            RequestNamespace = "http://DealerSpecialties.com/", ResponseNamespace = "http://DealerSpecialties.com/",
            Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
        [return: XmlArrayItem(IsNullable = false)]
        VehicleInfo[] GetAllVehicles(int DealerLotKey);

        /// <remarks/>
        [SoapDocumentMethod("http://DealerSpecialties.com/UpdateVehiclePrice",
            RequestNamespace = "http://DealerSpecialties.com/", ResponseNamespace = "http://DealerSpecialties.com/",
            Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
        VehicleInfo UpdateVehiclePrice(int VehicleKey, int Price, PriceType PriceType);

        /// <remarks/>
        [SoapDocumentMethod("http://DealerSpecialties.com/UpdateVehicleComments",
            RequestNamespace = "http://DealerSpecialties.com/", ResponseNamespace = "http://DealerSpecialties.com/",
            Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
        UpdateVehicleCommentsReturn UpdateVehicleComments(int VehicleKey, string VehicleComments);
    }

    public partial class CvdApiWebService : IGetAutoService
    {
    }
}