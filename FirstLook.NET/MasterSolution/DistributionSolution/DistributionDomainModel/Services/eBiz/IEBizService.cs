using System.Web.Services.Description;
using System.Web.Services.Protocols;

namespace FirstLook.Distribution.DomainModel.Services.eBiz
{
    public interface IEBizService
    {
        /// <remarks/>
        [SoapDocumentMethod("http://www.ebizautos.com/SendVehicle", RequestNamespace = "http://www.ebizautos.com/",
            ResponseNamespace = "http://www.ebizautos.com/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        string SendVehicle(string id, string auth, string xmlRequest);
    }

    public partial class XMLListener : IEBizService
    {
    }
}