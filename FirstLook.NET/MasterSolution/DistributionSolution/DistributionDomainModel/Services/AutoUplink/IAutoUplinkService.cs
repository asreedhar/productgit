using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace FirstLook.Distribution.DomainModel.Services.AutoUplink
{
    public interface IAutoUplinkService
    {
        /// <remarks/>
        [SoapDocumentMethod("", RequestNamespace = "https://services.aultec.com/VehicleInventory",
            ResponseNamespace = "https://services.aultec.com/VehicleInventory", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [return: XmlElement("PriceUpdateReturn")]
        string PriceUpdate(WSAuth VehicleInventoryAuth, string VIN, double Price);

        [SoapDocumentMethod("", RequestNamespace = "https://services.aultec.com/VehicleInventory",
            ResponseNamespace = "https://services.aultec.com/VehicleInventory", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [return: XmlElement("InternetPriceUpdateReturn")]
        string InternetPriceUpdate(WSAuth VehicleInventoryAuth, string VIN, double InternetPrice);

        /// <remarks/>
        [SoapDocumentMethod("", RequestNamespace = "https://services.aultec.com/VehicleInventory",
            ResponseNamespace = "https://services.aultec.com/VehicleInventory", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [return: XmlElement("CommentUpdateReturn")]
        string CommentUpdate(WSAuth VehicleInventoryAuth, string VIN, string Comment);

        [SoapDocumentMethod("", RequestNamespace = "https://services.aultec.com/VehicleInventory",
            ResponseNamespace = "https://services.aultec.com/VehicleInventory", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [return: XmlElement("EnhancedVehicleHighlightsUpdateReturn")]
        string EnhancedVehicleHighlightsUpdate(WSAuth VehicleInventoryAuth, string VIN, string EnhancedVehicleHighlights);
    }

    public partial class vehicleinventory1Service : IAutoUplinkService
    {
    }
}