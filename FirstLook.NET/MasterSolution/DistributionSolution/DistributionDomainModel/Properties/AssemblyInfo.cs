﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FirstLook.Distribution.DomainModel")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("FirstLook.Distribution.DomainModel")]

// Make internals visible to test project
[assembly: InternalsVisibleTo("FirstLook.Distribution.DomainModel.Tests.Integration")]

[assembly: InternalsVisibleTo("FirstLook.Distribution.DomainModel.Tests.Unit")]

[assembly: InternalsVisibleTo("FirstLook.Distribution.DomainModel.Tests.Common")]