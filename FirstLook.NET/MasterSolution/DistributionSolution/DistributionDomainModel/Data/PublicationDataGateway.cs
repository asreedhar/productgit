using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Threading;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Gateway for data operations on a publication and its member types.
    /// </summary>
    public class PublicationDataGateway : DataGateway<Publication>, IPublicationDataGateway
    {
        #region Exist Methods

        /// <summary>
        /// Check if a publication exists for the given identifier.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>True if it exists, false otherwise.</returns>
        public bool Exists(int publicationId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Publication#Exists";

                    command.AddParameter("PublicationID", DbType.Int32, publicationId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetInt32(reader.GetOrdinal("PublicationExists")) == 1 ? true : false;
                        }
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Check if a publication exists for the given dealer and vehicle identifiers.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleEntityType">Vehicle type.</param>        
        /// <returns>True if it exists, false otherwise.</returns>
        public bool Exists(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Publication#ExistsByDealerVehicle";

                    command.AddParameter("DealerID",            DbType.Int32, dealerId);
                    command.AddParameter("VehicleEntityID",     DbType.Int32, vehicleEntityId);
                    command.AddParameter("VehicleEntityTypeID", DbType.Int32, (int)vehicleEntityType);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetInt32(reader.GetOrdinal("PublicationExists")) == 1 ? true : false;
                        }
                        return false;
                    }
                }
            }
        }

        #endregion

        #region Fetch Methods

        /// <summary>
        /// Fetch a publication from the database by its identifier.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>Publication object.</returns>
        public Publication Fetch(int publicationId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Publication#Fetch";
                    
                    command.AddParameter("PublicationID", DbType.Int32, publicationId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        return FetchPublication(reader);
                    }
                }
            }
        }

        /// <summary>
        /// Fetch the parts of the publication's message that match the given types.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <param name="messageTypes">Message types to retrive.</param>
        /// <returns>Parts of message that match the given types, if applicable.</returns>
        public Message Fetch(int publicationId, MessageTypes messageTypes)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Publication#Fetch";

                    command.AddParameter("PublicationID", DbType.Int32, publicationId);
                    command.AddParameter("MessageTypes",  DbType.Int32, messageTypes);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        return FetchPublication(reader).Message;
                    }
                }
            }
        }

        /// <summary>
        /// Fetch a publication from the database by its dealer and vehicle identifiers.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleEntityId">Vehicle entity type identifier.</param>
        /// <param name="vehicleEntityType">Vehicle entity identifier.</param>        
        /// <returns>Publication object.</returns>
        public Publication Fetch(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Publication#FetchByDealerVehicle";

                    command.AddParameter("DealerID",            DbType.Int32, dealerId);
                    command.AddParameter("VehicleEntityID",     DbType.Int32, vehicleEntityId);
                    command.AddParameter("VehicleEntityTypeID", DbType.Int32, (int)vehicleEntityType);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        return FetchPublication(reader);
                    }
                }
            }
        }

        /// <summary>
        /// Fetch publication results from the data reader.
        /// </summary>
        /// <param name="reader">Database values.</param>
        /// <returns>Publication object.</returns>
        private static Publication FetchPublication(IDataReader reader)
        {
            if (!reader.Read())
            {
                throw new DataException("Expected publication results from database; got none.");
            }

            int publicationId = reader.GetInt32(reader.GetOrdinal("PublicationID"));
            PublicationState publicationStatus =
                (PublicationState)reader.GetInt32(reader.GetOrdinal("PublicationStatusID"));
            Message message = FetchMessage(reader);

            return new Publication(publicationId, message, publicationStatus);
        }

        /// <summary>
        /// Fetch message results from the data reader.
        /// </summary>
        /// <param name="reader">Database values.</param>
        /// <returns>Message object.</returns>
        private static Message FetchMessage(IDataReader reader)
        {
            if (!reader.NextResult() || !reader.Read())
            {
                throw new DataException("Expected message results from database; got none.");
            }

            int messageId = reader.GetInt32(reader.GetOrdinal("MessageID"));
            
            Dealer dealer = new Dealer(reader.GetInt32(reader.GetOrdinal("DealerID")));
            
            Vehicle vehicle = new Vehicle(reader.GetInt32(reader.GetOrdinal("VehicleEntityID")),
                                          (VehicleEntityType)reader.GetInt32(reader.GetOrdinal("VehicleEntityTypeID")));
            
            ReadOnlyCollection<IBodyPart> body = FetchBody(reader);

            Message message = new Message(dealer, vehicle, body) {Id = messageId};
            return message;
        }

        /// <summary>
        /// Fetch the body parts that define a message from the data reader.
        /// </summary>
        /// <param name="reader">Database values.</param>
        /// <returns>List of body parts.</returns>
        private static ReadOnlyCollection<IBodyPart> FetchBody(IDataReader reader)
        {            
            Collection<IBodyPart> bodyParts = new Collection<IBodyPart>();

            FetchPrice(reader, bodyParts);
            FetchVehicleInformation(reader, bodyParts);
            FetchAdvertisement(reader, bodyParts);
            FetchAppraisalvalue(reader, bodyParts);

            return new ReadOnlyCollection<IBodyPart>(bodyParts);            
        }

        /// <summary>
        /// Fetch any price body parts tied to a message from the data reader.
        /// </summary>
        /// <param name="reader">Database values.</param>
        /// <param name="bodyParts">List to add price body parts to.</param>
        private static void FetchPrice(IDataReader reader, ICollection<IBodyPart> bodyParts)
        {
            if (!reader.NextResult())
            {
                throw new DataException("Expected price result set; got none.");
            }

            while (reader.Read())
            {
                Price price = new Price((PriceType)reader.GetInt32(reader.GetOrdinal("PriceTypeID")),
                                        reader.GetInt32(reader.GetOrdinal("Value")));
                bodyParts.Add(price);
            }
        }

        /// <summary>
        /// Fetch vehicle information body part tied to a message from the data reader.
        /// </summary>
        /// <param name="reader">Database values.</param>
        /// <param name="bodyParts">List to add vehicle information body part to.</param>
        private static void FetchVehicleInformation(IDataReader reader, ICollection<IBodyPart> bodyParts)
        {
            if (!reader.NextResult())
            {
                throw new DataException("Expected vehicle information result set; got none.");
            }

            if (reader.Read())
            {
                VehicleInformation info = new VehicleInformation(reader.GetInt32(reader.GetOrdinal("Mileage")));
                bodyParts.Add(info);
            }
        }

        /// <summary>
        /// Fetch advertisement content body parts tied to a message from the data reader.
        /// </summary>
        /// <param name="reader">Database values.</param>
        /// <param name="bodyParts">List to add advertisement content to.</param>
        private static void FetchAdvertisement(IDataReader reader, ICollection<IBodyPart> bodyParts)
        {
            if (!reader.NextResult())
            {
                throw new DataException("Expected advertisement content result set; got none.");
            }

            Collection<Content> advertisementContent = new Collection<Content>();
            while (reader.Read())
            {
                ContentType type = (ContentType)reader.GetInt32(reader.GetOrdinal("ContentTypeID"));
                switch (type)
                {
                    case ContentType.Text:
                        string text;
                        if (reader.IsDBNull(reader.GetOrdinal("Content")))
                        {
                            text = string.Empty;
                        }
                        else
                        {
                            text = reader.GetString(reader.GetOrdinal("Content"));
                        }
                        advertisementContent.Add(new TextContent(text));
                        break;

                    default:
                        throw new DataException("Unrecognized content type fetched from database.");
                }
            }

            if (advertisementContent.Count > 0)
            {
                Advertisement advertisement = new Advertisement(new ReadOnlyCollection<Content>(advertisementContent));
                bodyParts.Add(advertisement);
            }
        }


        private static void FetchAppraisalvalue(IDataReader reader, ICollection<IBodyPart> bodyParts)
        {
            if (!reader.NextResult())
            {
                throw new DataException("Expected price result set; got none.");
            }

            while (reader.Read())
            {
                AppraisalValue appraisalValue = new AppraisalValue(reader.GetInt32(reader.GetOrdinal("Value")));
                bodyParts.Add(appraisalValue);
            }
        }

        #endregion

        #region Insert Methods

        /// <summary>
        /// Insert a publication - and its message, and its vehicle, and its body parts -- into the database.
        /// </summary>
        /// <param name="value">Publication to insert.</param>        
        protected override void Insert(Publication value)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    InsertVehicle(transaction, connection, value.Message.Vehicle);
                    InsertMessage(transaction, connection, value.Message);                    
                    InsertPublication(transaction, connection, value);

                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Insert a vehicle into the database if it does not already exist there.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="vehicle">Vehicle to insert if it does not exist.</param>        
        private static void InsertVehicle(IDbTransaction transaction, IDbConnection connection, Vehicle vehicle)
        {
            bool vehicleExists;
            
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.CommandText = "Distribution.Vehicle#Exists";

                command.AddParameter("VehicleEntityID",     DbType.Int32, vehicle.VehicleEntityId);
                command.AddParameter("VehicleEntityTypeID", DbType.Int32, (int)vehicle.VehicleEntityType);                

                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        vehicleExists = reader.GetInt32(reader.GetOrdinal("VehicleExists")) == 1 ? true : false;
                    }
                    else
                    {
                        vehicleExists = false;
                    }
                }
            }

            if (!vehicleExists)
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Transaction = transaction;
                    command.CommandText = "Distribution.Vehicle#Insert";

                    string insertUser = Thread.CurrentPrincipal.Identity.Name;

                    command.AddParameter("VehicleEntityID",     DbType.Int32, vehicle.VehicleEntityId);
                    command.AddParameter("VehicleEntityTypeID", DbType.Int32, (int) vehicle.VehicleEntityType);
                    command.AddParameter("InsertUser",          DbType.String, insertUser);
                    
                    command.ExecuteNonQuery();                    
                }
            }
        }

        /// <summary>
        /// Insert a message into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="message">Message to insert.</param>
        private static void InsertMessage(IDbTransaction transaction, IDbConnection connection, Message message)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.CommandText = "Distribution.Message#Insert";

                string insertUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameter("DealerID",            DbType.Int32,  message.Dealer.Id);
                command.AddParameter("VehicleEntityID",     DbType.Int32,  message.Vehicle.VehicleEntityId);
                command.AddParameter("VehicleEntityTypeID", DbType.Int32,  (int)message.Vehicle.VehicleEntityType);
                command.AddParameter("InsertUser",          DbType.String, insertUser);
                command.AddParameter("MessageTypes",        DbType.Int32,  message.MessageTypes);

                IDbDataParameter parameter = command.OutParameter("MessageID", DbType.Int32);
                command.ExecuteNonQuery();
                message.Id = (int)parameter.Value;
            }

            InsertBodyParts(transaction, connection, message);
        }

        /// <summary>
        /// Insert a publication into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="publication">Publication to insert.</param>
        private static void InsertPublication(IDbTransaction transaction, IDbConnection connection, Publication publication)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.CommandText = "Distribution.Publication#Insert";

                string insertUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameter("MessageID",           DbType.Int32,  publication.Message.Id);
                command.AddParameter("PublicationStatusID", DbType.Int32,  (int)publication.PublicationStatus);
                command.AddParameter("InsertUser",          DbType.String, insertUser);                

                IDbDataParameter parameter = command.OutParameter("PublicationID", DbType.Int32);
                command.ExecuteNonQuery();
                publication.Id = (int)parameter.Value;
            }
        }

        /// <summary>
        /// Insert a message's body parts into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="message">The message whose body parts we will insert.</param>
        private static void InsertBodyParts(IDbTransaction transaction, IDbConnection connection, Message message)
        {
            foreach (IBodyPart bodyPart in message.Body)
            {
                VehicleInformation vehicleInfo   = bodyPart as VehicleInformation;
                Price              price         = bodyPart as Price;
                Advertisement      advertisement = bodyPart as Advertisement;
                AppraisalValue appraisalValue= bodyPart as AppraisalValue;

                if (vehicleInfo != null)
                {
                    InsertVehicleInformation(transaction, connection, message.Id, vehicleInfo);
                }
                else if (price != null)
                {
                    InsertPrice(transaction, connection, message.Id, price);
                }
                else if (advertisement != null)
                {
                    InsertAdvertisement(transaction, connection, message.Id, advertisement);
                }
                else if (appraisalValue != null)
                {
                    InsertAppraisalValue(transaction, connection, message.Id, appraisalValue);
                }
                else
                {
                    throw new ArgumentException("Unrecognized body part type.");
                }
            }
        }

        /// <summary>
        /// Insert vehicle information for a message into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="messageId">Identifier of the message who owns the vehicle information.</param>
        /// <param name="vehicleInfo">Vehicle information.</param>
        private static void InsertVehicleInformation(IDbTransaction transaction, IDbConnection connection, 
                                                     int messageId, VehicleInformation vehicleInfo)            
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.CommandText = "Distribution.VehicleInformation#Insert";                

                command.AddParameter("MessageID",  DbType.Int32, messageId);
                command.AddParameter("Mileage",    DbType.Int32, vehicleInfo.Mileage);                
                
                command.ExecuteNonQuery();                
            }            
        }

        /// <summary>
        /// Insert price information for a message into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="messageId">Message identifier.</param>
        /// <param name="price">Price information.</param>
        private static void InsertPrice(IDbTransaction transaction, IDbConnection connection, int messageId, Price price)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.CommandText = "Distribution.Price#Insert";                

                command.AddParameter("MessageID",   DbType.Int32, messageId);
                command.AddParameter("PriceTypeID", DbType.Int32, (int)price.PriceType);
                command.AddParameter("Value",       DbType.Int32, price.Value);
                
                command.ExecuteNonQuery();                
            }
        }
        
        /// <summary>
        /// Insert an advertisement into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="messageId">Message identifier.</param>
        /// <param name="advertisement">Advertisement.</param>
        private static void InsertAdvertisement(IDbTransaction transaction, IDbConnection connection, int messageId,
                                                Advertisement advertisement)
        {
            foreach (Content content in advertisement.Contents)
            {
                switch (content.ContentType)
                {
                    case ContentType.Text:
                        TextContent textContent = content as TextContent;
                        InsertTextContent(transaction, connection, messageId, textContent);
                        break;

                    default:
                        throw new ArgumentException("Cannot insert unrecognized content type");
                }
            }
            
        }

        /// <summary>
        /// Insert a piece of text content into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="messageId">Message identifier.</param>
        /// <param name="content">Text content.</param>
        private static void InsertTextContent(IDbTransaction transaction, IDbConnection connection, int messageId, 
                                              TextContent content)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.CommandText = "Distribution.ContentText#Insert";
               
                command.AddParameter("MessageID", DbType.Int32, messageId);

                if (string.IsNullOrEmpty(content.Text))
                {
                    command.AddParameter("Text", DbType.String, DBNull.Value);
                }
                else
                {
                    command.AddParameter("Text", DbType.String, content.Text);
                }

                command.ExecuteNonQuery();
            }
        }


        /// <summary>
        /// Insert AppraisalValue information for a message into the database.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="connection">Database connection.</param>
        /// <param name="messageId">Message identifier.</param>
        /// <param name="AppraisalValue">AppraisalValue information.</param>
        private static void InsertAppraisalValue(IDbTransaction transaction, IDbConnection connection, int messageId, AppraisalValue appraisalValue)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.CommandText = "Distribution.AppraisalValue#Insert";

                command.AddParameter("MessageID", DbType.Int32, messageId);
                command.AddParameter("Value", DbType.Int32, appraisalValue.Value);

                command.ExecuteNonQuery();
            }
        }

        #endregion

        #region Update Methods

        /// <summary>
        /// Update the publication in the database. Will only update the publication status.
        /// </summary>
        /// <param name="value">Publication to update.</param>
        protected override void Update(Publication value)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;                    
                    command.CommandText = "Distribution.Publication#Update";

                    command.AddParameter("PublicationID",       DbType.Int32, value.Id);
                    command.AddParameter("PublicationStatusID", DbType.Int32, (int)value.PublicationStatus);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}
