using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Interface that defines vehicle data operations. Does not inherit from IDataGateway since this interface 
    /// provides only for read operations, and as such all the write operations required to implement IDataGateway
    /// are irrelevant.
    /// </summary>
    public interface IVehicleDataGateway
    {        
        /// <summary>
        /// Fetch inventory details for the car with the given identifier.
        /// </summary>
        /// <param name="inventoryId">Inventory identifier.</param>
        /// <returns>Inventory details.</returns>
        Inventory FetchInventory(int inventoryId);

        /// <summary>
        /// Check if inventory with the given identifier and type exists.
        /// </summary>
        /// <param name="inventoryId">Vehicle entity identifier.</param>        
        /// <returns>True if the vehicle exists, false otherwise.</returns>
        bool InventoryExists(int inventoryId);

        /// <summary>
        /// Method to fetch Invenory data for ADP.
        /// </summary>
        /// <param name="inventoryId">Inventory ID key</param>
        /// <returns>Object of ADP inventory. details to pass to ADP service.</returns>
        ADPInventory FetchADPInventory(int inventoryId, int dealerId);
    }
}
