﻿using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    public interface IAppraisedVehicleDataGateway
    {
        /// <summary>
        /// Retrives vehicle details from vehicle ID
        /// </summary>
        /// <param name="appraisalId">Vehicle ID to get vehicle details</param>
        /// <returns>Object of Inventory with Vehicle details</returns>
        Inventory FetchVehicle(int appraisalId);

        bool IsVehicleExists(int vehicleEntityId, int vehicleEntityTypeId);
    }
}
