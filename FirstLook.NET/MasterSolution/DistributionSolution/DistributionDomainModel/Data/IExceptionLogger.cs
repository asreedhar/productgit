using System;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Interface for exception logging.
    /// </summary>
    public interface IExceptionLogger
    {
        /// <summary>
        /// Record an exception.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        void Record(Exception exception);

        /// <summary>
        /// Record an exception with the incoming request message that likely caused it.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        /// <param name="message">Incoming request message.</param>
        void Record(Exception exception, string message);

        /// <summary>
        /// Record an exception for a specific exception.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        /// <param name="submissionId">Identifier of the submission tied to the exception.</param>
        void Record(Exception exception, int submissionId);

        /// <summary>
        /// Fetch any exception tied to the publication with the given identifier.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Exception message if one exists; false otherwise.</returns>
        string Fetch(int submissionId);
    }
}
