using System;
using System.Data;
using System.Diagnostics;
using System.Threading;
using FirstLook.Common.Core.Data;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Logs exceptions to the database.
    /// </summary>
    public class ExceptionLogger : IExceptionLogger
    {
        /// <summary>
        /// Record an exception without any incoming request message or particular submission's identifier.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        public void Record(Exception exception)
        {
            Insert(exception,null, null);
        }

        /// <summary>
        /// Record an exception with the incoming request message that likely caused it.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        /// <param name="message">Incoming request message.</param>
        public void Record(Exception exception, string message)
        {
            Insert(exception, message, null);
        }

        /// <summary>
        /// Record an exception for the given submission.
        /// </summary>
        /// <param name="exception">Exception to record.</param>
        /// <param name="submissionId">Identifier of the submission tied to the exception.</param>
        public void Record(Exception exception, int submissionId)
        {
            Insert(exception, null, submissionId);
        }

        /// <summary>
        /// Fetch any exception tied to the submission with the given identifier.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Exception message if one exists; false otherwise.</returns>
        public string Fetch(int submissionId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Exception#Fetch";

                    command.AddParameter("SubmissionID", DbType.Int32, submissionId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetString(reader.GetOrdinal("Message"));
                        }
                        return null;
                    }
                }
            }
        }

        /// <summary>
        /// Log an exception to the database.        
        /// </summary>
        /// <remarks>
        /// If the exception happened prior to submissions being queued, then it was likely because validation failed
        /// on the incoming request message, and so the message will be logged. After the submissions have been queued,
        /// we no longer retain the incoming message. Therefore, if the exception occurs during submission processing, 
        /// the submission identifier will be logged instead.        
        /// </remarks>
        /// <param name="exception">Exception to log.</param>
        /// <param name="request">Optional incoming message that caused exception.</param>
        /// <param name="submissionId">Optional submission identifier tied to the exception.</param>
        private static void Insert(Exception exception, string request, int? submissionId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Exception#Insert";

                    string exceptionUser = Thread.CurrentPrincipal.Identity.Name;

                    command.AddParameter("MachineName",   DbType.String, GetMachineNameWithAssert());
                    command.AddParameter("Message",       DbType.String, exception.Message);
                    command.AddParameter("ExceptionType", DbType.String, exception.GetType().ToString());
                    command.AddParameter("Details",       DbType.String, new StackTrace(exception).ToString());
                    
                    if (!string.IsNullOrEmpty(request))
                    {
                        command.AddParameter("RequestMessage", DbType.String, request);
                    }
                    else
                    {
                        command.AddParameter("RequestMessage", DbType.String, DBNull.Value);
                    }

                    if (submissionId.HasValue)
                    {
                        command.AddParameter("SubmissionID", DbType.Int32, submissionId.Value);
                    }
                    else
                    {
                        command.AddParameter("SubmissionID", DbType.String, DBNull.Value);
                    }                    

                    command.AddParameter("ExceptionUser", DbType.String, exceptionUser);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Get the name of the machine on which this exception occurred.
        /// </summary>
        /// <returns>Name of the machine this code is running on.</returns>        
        private static string GetMachineNameWithAssert()
        {
            return Environment.MachineName;
        }        
    }
}
