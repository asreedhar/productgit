using System;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Gateway that defines data operations for a given entity object type.
    /// </summary>
    /// <typeparam name="T">Entity type.</typeparam>
    public abstract class DataGateway<T> : IDataGateway<T> where T : Entity
    {
        /// <summary>
        /// Delete the given object from the data layer.
        /// </summary>
        /// <param name="value">Object to delete.</param>
        public void Delete(T value)
        {
            value.MarkDeleted();

            Save(value);
        }

        /// <summary>
        /// Delete the given object from the data layer.
        /// </summary>
        /// <param name="value">Object to delete.</param>
        protected virtual void DeleteSelf(T value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Insert an object into the data layer.
        /// </summary>
        /// <param name="value">Value to insert.</param>
        protected virtual void Insert(T value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Save the given object in the data layer.
        /// </summary>
        /// <param name="value">Object to save.</param>
        public void Save(T value)
        {
            if (value.IsDeleted)
            {
                if (!value.IsNew)
                {
                    DeleteSelf(value);
                }
                value.MarkNew();
            }
            else
            {
                if (value.IsNew)
                {
                    Insert(value);
                }
                else
                {
                    Update(value);
                }
                value.MarkOld();
            }
        }

        /// <summary>
        /// Update an object in the data layer.
        /// </summary>
        /// <param name="value">Object to update.</param>
        protected virtual void Update(T value)
        {
            throw new NotSupportedException();
        }                      
    }
}
