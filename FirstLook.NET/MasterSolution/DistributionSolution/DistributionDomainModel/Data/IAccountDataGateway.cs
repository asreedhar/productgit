using System.Collections.Generic;
using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Interface that defines account data operations - including operations on dealers, providers and account 
    /// preferences.
    /// </summary>
    public interface IAccountDataGateway : IDataGateway<Account>
    {
        #region Account

        /// <summary>
        /// Check if an account exists for the given provider and dealer.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if it exists, false otherwise.</returns>
        bool Exists(int providerId, int dealerId);

        /// <summary>
        /// Fetch the account for the given provider and dealer.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if it exists, false otherwise.</returns>
        Account Fetch(int providerId, int dealerId);

        /// <summary>
        /// Fetch account details for ADP provider. as 
        /// </summary>
        /// <param name="dealerId"></param>
        /// <returns></returns>
        Account FetchADPAccount(int dealerId);

        #endregion

        #region Account Preferences

        /// <summary>
        /// Fetch the account preferences for a dealer. Account preferences are the message types the dealer has 
        /// enabled per provider.
        /// </summary>
        /// <param name="dealerId">Identifier of the dealer whose account preferences should be fetched.</param>
        /// <returns>Account preferences.</returns>
        AccountPreferenceDictionary FetchAccountPreferences(int dealerId);


        /// <summary>
        /// Save the given account preferences. Will delete existing preferences for the dealer and replace them with 
        /// the ones provided. Account preferences are the message types the dealer has enabled per provider.
        /// </summary>
        /// <param name="accountPreferences">Account preferences to save.</param>
        void SaveAccountPreferences(AccountPreferenceDictionary accountPreferences);        

        /// <summary>
        /// Save the given account preference for a single provider. Will delete existing preferences for the dealer 
        /// and replace them with the ones provided. Account preferences are the message types the dealer has enabled
        /// per provider.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="preference">Account preference to save.</param>
        void SaveAccountPreference(int dealerId, int providerId, IList<MessageTypePreference> preference);

        /// <summary>
        /// Does the account support the given message types? Check consists of three parts:
        /// 1. Is there active account information for the dealer with the provider?
        /// 2. Does the provider support the message type?
        /// 3. If the provider does support the message type, has the dealer enabled this setting?         
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="messageTypes">Message type bit field. Will be filtered to the supported subset type.</param>
        /// <returns>True if the account supports this, false otherwise.</returns>
        bool IsSupported(int dealerId, int providerId, ref MessageTypes messageTypes);

        #endregion

        #region Dealer

        /// <summary>
        /// Verify that a record exists for the given dealer identifier.        
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if a dealer exists for that identifier.</returns>
        bool DealerExists(int dealerId);

        /// <summary>
        /// Check whether the dealer with the given identifier will need to update 'Internet Price' as opposed to just
        /// 'Price' because they are setup with Aultec for active extraction. Looks in IMT.Extract.DealerConfiguration.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if Aultec is enabled, false otherwise.</returns>
        bool DealerRequiresInternetPriceUpdate(int dealerId);

        /// <summary>
        /// Returns Dealer Details by dealer Id
        /// </summary>
        /// <param name="dealerId"></param>
        /// <returns>Object of Dealer</returns>
        Dealer GetDealer(int dealerId);
        #endregion        
    
        #region Provider

        /// <summary>
        /// Fetch the provider with the given identifier.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <returns>Provider object.</returns>
        Provider FetchProvider(int providerId);

        /// <summary>
        /// Fetch all providers.
        /// </summary>
        /// <returns>Collection of providers.</returns>
        Collection<Provider> FetchProviders();

        #endregion

        #region User Identity

        /// <summary>
        /// Check if the user identity is valid.
        /// </summary>        
        /// <param name="userName">User name from the identity to be checked.</param>
        /// <returns>True if it is valid, false otherwise.</returns>
        bool UserIdentityExists(string userName);

        #endregion
    }
}