using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Interface that defines publication data operations.
    /// </summary>
    public interface IPublicationDataGateway : IDataGateway<Publication>
    {
        /// <summary>
        /// Check if a publication exists for the given identifier.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>True if it exists, false otherwise.</returns>
        bool Exists(int publicationId);

        /// <summary>
        /// Check if a publication exists for the given dealer and vehicle identifiers.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleEntityType">Vehice type.</param>
        /// <returns>True if it exists, false otherwise.</returns>
        bool Exists(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType);

        /// <summary>
        /// Fetch the publication with the given identifier.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>Publication.</returns>
        Publication Fetch(int publicationId);

        /// <summary>
        /// Fetch the parts of the publication's message that match the given types.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <param name="messageTypes">Message types to retrive.</param>
        /// <returns>Parts of message that match the given types, if applicable.</returns>
        Message Fetch(int publicationId, MessageTypes messageTypes);

        /// <summary>
        /// Fetch a publication by the given dealer and vehicle identifiers.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleEntityType">Vehicle type.</param>
        /// <returns>Publication object.</returns>
        Publication Fetch(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType);
    }
}