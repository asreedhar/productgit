using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Gateway for data operations on inventory.
    /// </summary>
    public class VehicleDataGateway : IVehicleDataGateway
    {
        /// <summary>
        /// Fetch inventory details for the car with the given identifier.
        /// </summary>
        /// <param name="inventoryId">Inventory identifier.</param>
        /// <returns>Inventory details.</returns>
        public Inventory FetchInventory(int inventoryId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Inventory#Fetch";

                    command.AddParameter("InventoryID", DbType.Int32, inventoryId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected inventory data from database, but received none.");
                        }

                        Inventory inventory = new Inventory(inventoryId);
                        inventory.Vin = reader.GetString(reader.GetOrdinal("VIN"));
                        inventory.Year = reader.GetInt32(reader.GetOrdinal("VehicleYear"));
                        inventory.Make = reader.GetString(reader.GetOrdinal("Make"));
                        inventory.Model = reader.GetString(reader.GetOrdinal("Model"));

                        return inventory;
                    }
                }
            }
        }

        /// <summary>
        /// Check if inventory with the given identifier exists.
        /// </summary>
        /// <param name="inventoryId">Vehicle entity identifier.</param>        
        /// <returns>True if the vehicle exists, false otherwise.</returns>
        public bool InventoryExists(int inventoryId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Inventory#Fetch";

                    command.AddParameter("InventoryID", DbType.Int32, inventoryId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        return reader.Read();
                    }
                }
            }
        }

        /// <summary>
        /// Fetch ADP inventory Data from ADP configuration table
        /// </summary>
        /// <param name="inventoryId">Inventory ID</param>
        /// <param name="dealerId">Business Unit ID</param>
        /// <returns></returns>
        public ADPInventory FetchADPInventory(int inventoryId, int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.ADP_Inventory#Fetch";

                    command.AddParameter("InventoryID", DbType.Int32, inventoryId);
                    command.AddParameter("BusinessUnitId", DbType.Int32, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected inventory data from database, but received none.");
                        }

                        ADPInventory inventory = new ADPInventory();
                        inventory.Vin = reader.GetString(reader.GetOrdinal("Vin"));
                        inventory.DealerId = reader.GetString(reader.GetOrdinal("3PA Dealer Id"));
                        inventory.StockNumber = reader.GetString(reader.GetOrdinal("StockNumber"));

                        return inventory;
                    }
                }
            }
        }


    }
}
