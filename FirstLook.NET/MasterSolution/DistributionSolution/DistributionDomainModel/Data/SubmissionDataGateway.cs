using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.AutoTrader;
using FirstLook.Distribution.DomainModel.Bindings.AutoUplink;
using FirstLook.Distribution.DomainModel.Bindings.eBiz;
using FirstLook.Distribution.DomainModel.Bindings.GetAuto;
using FirstLook.Distribution.DomainModel.Bindings.HomeNet;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Bindings.eLead;
using FirstLook.Distribution.DomainModel.Bindings.ADP;
using FirstLook.Common.Core.Registry;


namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Gateway for data operations on submissions.
    /// </summary>
    public class SubmissionDataGateway : DataGateway<Submission>, ISubmissionDataGateway
    {
        #region Submission

        /// <summary>
        /// Fetch the submissions tied to the publication with the given identifier.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>List of submissions.</returns>
        public IList<Submission> Fetch(int publicationId)
        {
            Collection<Submission> submissions = new Collection<Submission>();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Submission#Fetch";

                    command.AddParameter("PublicationID", DbType.Int32, publicationId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int submissionId = reader.GetInt32(reader.GetOrdinal("SubmissionID"));
                            int providerId = reader.GetInt32(reader.GetOrdinal("ProviderID"));
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            int attempt = reader.GetInt32(reader.GetOrdinal("Attempt"));

                            MessageTypes messageTypes = 0;
                            if (!reader.IsDBNull(reader.GetOrdinal("MessageTypes")))
                            {
                                messageTypes = (MessageTypes)reader.GetInt32(reader.GetOrdinal("MessageTypes"));
                            }

                            submissions.Add(new Submission(submissionId, providerId, publicationId, messageTypes, status, attempt));
                        }
                    }
                }
            }

            return submissions;
        }

        /// <summary>
        /// Fetch the submissions tied to the publication for the given dealer and vehicle.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>        
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleEntityType">Vehicle type.</param>
        /// <returns>List of submissions.</returns>
        public IList<Submission> Fetch(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType)
        {
            Collection<Submission> submissions = new Collection<Submission>();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Submission#FetchByDealerVehicle";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);
                    command.AddParameter("VehicleEntityID", DbType.Int32, vehicleEntityId);
                    command.AddParameter("VehicleEntityTypeID", DbType.Int32, (int)vehicleEntityType);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int submissionId = reader.GetInt32(reader.GetOrdinal("SubmissionID"));
                            int providerId = reader.GetInt32(reader.GetOrdinal("ProviderID"));
                            int publicationId = reader.GetInt32(reader.GetOrdinal("PublicationID"));
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            int attempt = reader.GetInt32(reader.GetOrdinal("Attempt"));

                            MessageTypes messageTypes = 0;
                            if (!reader.IsDBNull(reader.GetOrdinal("MessageTypes")))
                            {
                                messageTypes = (MessageTypes)reader.GetInt32(reader.GetOrdinal("MessageTypes"));
                            }

                            submissions.Add(new Submission(submissionId, providerId, publicationId, messageTypes, status, attempt));
                        }
                    }
                }
            }

            return submissions;
        }

        /// <summary>
        /// Fetch the failed submissions that we should try to distribute.
        /// </summary>        
        /// <param name="maxAttempts">Only retrieve submissions that have been attempted fewer than this many times.</param>
        /// <param name="maxDaysBack">Maximum number of days back to fetch failures.</param>
        /// <returns>Submission that should be redistributed.</returns>
        public IList<Submission> FetchFailuresToRetry(int maxAttempts, int maxDaysBack)
        {
            Collection<Submission> submissions = new Collection<Submission>();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Submission#FetchFailuresToRetry";

                    command.AddParameter("MaxAttempts", DbType.Int32, maxAttempts);
                    command.AddParameter("MaxDaysBack", DbType.Int32, maxDaysBack);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int submissionId = reader.GetInt32(reader.GetOrdinal("SubmissionID"));
                            int providerId = reader.GetInt32(reader.GetOrdinal("ProviderID"));
                            int publicationId = reader.GetInt32(reader.GetOrdinal("PublicationID"));
                            int attempt = reader.GetInt32(reader.GetOrdinal("Attempt"));
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));

                            MessageTypes messageTypes = 0;
                            if (!reader.IsDBNull(reader.GetOrdinal("MessageTypes")))
                            {
                                messageTypes = (MessageTypes)reader.GetInt32(reader.GetOrdinal("MessageTypes"));
                            }

                            submissions.Add(new Submission(submissionId, providerId, publicationId, messageTypes, status, attempt));
                        }
                        return submissions;
                    }
                }
            }
        }

        /// <summary>
        /// Insert the given submission into the database.
        /// </summary>
        /// <param name="value">Submission to insert.</param>
        protected override void Insert(Submission value)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Submission#Insert";

                    command.AddParameter("PublicationID", DbType.Int32, value.PublicationId);
                    command.AddParameter("ProviderID", DbType.Int32, value.ProviderId);
                    command.AddParameter("SubmissionStatusID", DbType.Int32, SubmissionState.Waiting);
                    command.AddParameter("MessageTypes", DbType.Int32, (int)value.MessageTypes);
                    command.AddParameter("Attempt", DbType.Int32, value.Attempt);

                    IDbDataParameter parameter = command.OutParameter("SubmissionId", DbType.Int32);
                    command.ExecuteNonQuery();
                    value.Id = (int)parameter.Value;
                }
            }
        }

        /// <summary>
        /// Update the given submission in the database.
        /// </summary>
        /// <param name="value">Submission to update.</param>
        protected override void Update(Submission value)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Submission#Update";

                    command.AddParameter("SubmissionID", DbType.Int32, value.Id);
                    command.AddParameter("SubmissionStatusID", DbType.Int32, (int)value.SubmissionState);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Stale

        /// <summary>
        /// Get the stale and valid message types for a submission.
        /// </summary>
        /// <param name="submissionId">Submission to check the message types of.</param>
        /// <returns>The message types that are stale.</returns>
        public MessageTypes FetchStaleTypes(int submissionId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Submission#FetchStaleTypes";

                    command.AddParameter("SubmissionID", DbType.Int32, submissionId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected stale and valid data types.");
                        }
                        return (MessageTypes)reader.GetInt32(reader.GetOrdinal("StaleTypes"));
                    }
                }
            }
        }

        /// <summary>
        /// Log that the message types represented by the given bit field were stale for the given submission.        
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="messageTypes">Message types that are stale.</param>
        public void LogStale(int submissionId, int messageTypes)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.SubmissionStale#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, submissionId);
                    command.AddParameter("MessageTypes", DbType.Int32, messageTypes);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Fetch the price that is more recent than the one tied to the submission with the given identifier.
        /// </summary>
        /// <remarks>
        /// Currently, this only exists because AutoTrader cannot send Description on its own. If there is a request
        /// for AutoTrader that has both Price and Description, and then a newer submission with just Price makes the
        /// first request's price stale, we want to still be able to send the Description. Thus, we need to be able to
        /// query the most recent price.        
        /// </remarks>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Price.</returns>
        public Price FetchSupersedingPrice(int submissionId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Submission#FetchSupersedingPrice";

                    command.AddParameter("SubmissionID", DbType.Int32, submissionId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int value = reader.GetInt32(reader.GetOrdinal("Value"));
                            PriceType type = (PriceType)reader.GetInt32(reader.GetOrdinal("PriceTypeID"));

                            return new Price(type, value);
                        }
                        return null;
                    }
                }
            }
        }

        #endregion

        #region Error

        /// <summary>
        /// Fetch any error messages associated with the given submissions.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        public Collection<FirstLookError> FetchErrors(int submissionId)
        {
            Collection<FirstLookError> errors = new Collection<FirstLookError>();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.SubmissionError#Fetch";

                    command.AddParameter("SubmissionID", DbType.Int32, submissionId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int errorId = reader.GetInt32(reader.GetOrdinal("ErrorID"));
                            int errorType = reader.GetInt32(reader.GetOrdinal("ErrorTypeID"));
                            string error = reader.GetString(reader.GetOrdinal("Description"));
                            bool concernsUser = reader.GetBoolean(reader.GetOrdinal("ConcernsUser"));
                            int messageType = reader.GetInt32(reader.GetOrdinal("MessageTypes"));
                            DateTime occurredOn = reader.GetDateTime(reader.GetOrdinal("OccuredOn"));

                            errors.Add(new FirstLookError(errorId, submissionId, (FirstLookErrorType)errorType, error,
                                                          concernsUser, messageType, occurredOn));
                        }
                    }
                }
            }
            return errors;
        }

        /// <summary>
        /// Log to the database an error that occurred to a submission.
        /// </summary>
        /// <param name="firstLookError">Error to save to the database.</param>        
        public void LogError(FirstLookError firstLookError)
        {
            // AutoUplink / Aultec.
            AutoUplinkError autoUplinkError = firstLookError as AutoUplinkError;
            if (autoUplinkError != null)
            {
                LogAutoUplinkError(autoUplinkError);
                return;
            }

            // AutoTrader.
            AutoTraderError autoTraderError = firstLookError as AutoTraderError;
            if (autoTraderError != null)
            {
                LogAutoTraderError(autoTraderError);
                return;
            }

            // GetAuto.
            GetAutoError getAutoError = firstLookError as GetAutoError;
            if (getAutoError != null)
            {
                LogGetAutoError(getAutoError);
                return;
            }

            // eBiz.
            eBizError eBizError = firstLookError as eBizError;
            if (eBizError != null)
            {
                LogEBizTraderError(eBizError);
                return;
            }

            // eLead.
            eLeadError eLeadError = firstLookError as eLeadError;
            if (eLeadError != null)
            {
                LogELeadTraderError(eLeadError);
                return;
            }

            // ADP.
            ADPError adpError = firstLookError as ADPError;
            if (adpError != null)
            {
                LogAdpError(adpError);
                return;
            }

            // HomeNet.
            var homeNetError = firstLookError as HomeNetError;
            if (homeNetError != null)
            {
                LogHomeNetError(homeNetError);
                return;
            }

            throw new ArgumentException("Unrecognized error type.");
        }

        /// <summary>
        /// Log to the database an AutoUplink-specific error.
        /// </summary>
        /// <param name="autoUplinkError">Error to insert.</param>
        private static void LogAutoUplinkError(AutoUplinkError autoUplinkError)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.AutoUplink_Error#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, autoUplinkError.SubmissionId);
                    command.AddParameter("FirstLookErrorTypeID", DbType.Int32, (int)autoUplinkError.FirstLookErrorType);
                    command.AddParameter("AutoUplinkErrorTypeID", DbType.Int32, (int)autoUplinkError.AutoUplinkErrorType);
                    command.AddParameter("AutoUplinkErrorCode", DbType.String, autoUplinkError.AutoUplinkErrorCode);
                    command.AddParameter("MessageTypes", DbType.Int32, autoUplinkError.MessageType);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Log to the database an AutoTrader-specific error.
        /// </summary>
        /// <param name="autoTraderError">Error to insert.</param>
        private static void LogAutoTraderError(AutoTraderError autoTraderError)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.AutoTrader_Error#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, autoTraderError.SubmissionId);
                    command.AddParameter("FirstLookErrorTypeID", DbType.Int32, (int)autoTraderError.FirstLookErrorType);
                    command.AddParameter("AutoTraderErrorTypeID", DbType.Int32, (int)autoTraderError.AutoTraderErrorType);
                    command.AddParameter("MessageTypes", DbType.Int32, autoTraderError.MessageType);

                    if (!string.IsNullOrEmpty(autoTraderError.ErrorMessage))
                    {
                        command.AddParameter("ErrorMessage", DbType.String, autoTraderError.ErrorMessage);
                    }

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Log to the database a GetAuto-specific error.
        /// </summary>
        /// <param name="getAutoError">Error to insert.</param>
        private static void LogGetAutoError(GetAutoError getAutoError)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.GetAuto_Error#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, getAutoError.SubmissionId);
                    command.AddParameter("FirstLookErrorTypeID", DbType.Int32, (int)getAutoError.FirstLookErrorType);
                    command.AddParameter("GetAutoErrorTypeID", DbType.Int32, (int)getAutoError.GetAutoErrorType);
                    command.AddParameter("MessageTypes", DbType.Int32, getAutoError.MessageType);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Log to the database an eBiz-specific error.
        /// </summary>
        /// <param name="eBizError">Error to insert.</param>
        private static void LogEBizTraderError(eBizError eBizError)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.eBiz_Error#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, eBizError.SubmissionId);
                    command.AddParameter("FirstLookErrorTypeID", DbType.Int32, (int)eBizError.FirstLookErrorType);
                    command.AddParameter("eBizErrorTypeID", DbType.Int32, (int)eBizError.eBizErrorType);
                    command.AddParameter("MessageTypes", DbType.Int32, eBizError.MessageType);

                    if (!string.IsNullOrEmpty(eBizError.ErrorMessage))
                    {
                        command.AddParameter("ErrorMessage", DbType.String, eBizError.ErrorMessage);
                    }

                    command.ExecuteNonQuery();
                }
            }
        }


        /// <summary>
        /// Log eLead web service errors in Database
        /// </summary>
        /// <param name="eLeadError"></param>
        private static void LogELeadTraderError(eLeadError eLeadError)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.eLead_Error#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, eLeadError.SubmissionId);
                    command.AddParameter("FirstLookErrorTypeID", DbType.Int32, (int)eLeadError.FirstLookErrorType);
                    command.AddParameter("eLeadErrorTypeID", DbType.Int32, (int)eLeadError.eLeadErrorType);
                    command.AddParameter("MessageTypes", DbType.Int32, eLeadError.MessageType);

                    if (!string.IsNullOrEmpty(eLeadError.ErrorMessage))
                    {
                        command.AddParameter("ErrorMessage", DbType.String, eLeadError.ErrorMessage);
                    }

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Log ADP web service errors in Database
        /// </summary>
        /// <param name="adpError"></param>
        private static void LogAdpError(ADPError adpError)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.ADP_Error#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, adpError.SubmissionId);
                    command.AddParameter("FirstLookErrorTypeID", DbType.Int32, (int)adpError.FirstLookErrorType);
                    command.AddParameter("ADPErrorTypeID", DbType.Int32, (int)adpError.ADPErrorType);
                    command.AddParameter("MessageTypes", DbType.Int32, adpError.MessageType);

                    if (!string.IsNullOrEmpty(adpError.ErrorMessage))
                    {
                        command.AddParameter("ErrorMessage", DbType.String, adpError.ErrorMessage);
                    }

                    if (!string.IsNullOrEmpty(adpError.ErrorMessage))
                    {
                        command.AddParameter("Request", DbType.String, adpError.Request);
                    }

                    command.ExecuteNonQuery();
                }
            }
        }


        /// <summary>
        /// Log HomeNet web service errors in Database
        /// </summary>
        /// <param name="homeNetError"></param>
        private static void LogHomeNetError(HomeNetError homeNetError)
        {
            using (var connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.HomeNet_Error#Insert";
                    command.AddParameter("SubmissionID", DbType.Int32, homeNetError.SubmissionId);
                    command.AddParameter("FirstLookErrorTypeID", DbType.Int32, (int)homeNetError.FirstLookErrorType);
                    command.AddParameter("HomeNetErrorType", DbType.Int32, (int)homeNetError.HomeNetErrorType);
                    command.AddParameter("MessageTypes", DbType.Int32, homeNetError.MessageType);
                    if (!string.IsNullOrEmpty(homeNetError.ErrorMessage))
                        command.AddParameter("ErrorMessage", DbType.String, homeNetError.ErrorMessage);
                    command.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region Success

        /// <summary>
        /// Fetch which message types were succesful for the given submission.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Collection of succesful message types.</returns>
        public Collection<int> FetchSuccesses(int submissionId)
        {
            Collection<int> messageTypes = new Collection<int>();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.SubmissionSuccess#Fetch";

                    command.AddParameter("SubmissionID", DbType.Int32, submissionId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            messageTypes.Add(reader.GetInt32(reader.GetOrdinal("MessageTypes")));
                        }
                    }
                }
            }

            return messageTypes;
        }

        /// <summary>
        /// Log that the message types represented by the given bit field were successful for the given submission.        
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="messageTypes">Message types that were accepted.</param>
        public void LogSuccess(int submissionId, int messageTypes)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.SubmissionSuccess#Insert";

                    command.AddParameter("SubmissionID", DbType.Int32, submissionId);
                    command.AddParameter("MessageTypes", DbType.Int32, messageTypes);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoggingADPSuccess(int submissionId, int dealerid, string stocknumber, string xmlRequest, string xmlResponse)
        {
            try
            {
                using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Distribution.SaveADPSuccessEvents";

                        command.AddParameter("SubmissionID", DbType.Int32, submissionId);
                        command.AddParameter("DealerId", DbType.Int32, dealerid);
                        command.AddParameter("StockNumber", DbType.String, stocknumber);
                        command.AddParameter("XMLRequest", DbType.String, xmlRequest);
                        command.AddParameter("XMLResponse", DbType.String, xmlResponse);


                        command.ExecuteNonQuery();


                    }
                }
            }
            catch (Exception exception)
            {
                IExceptionLogger logger = RegistryFactory.GetResolver().Resolve<IExceptionLogger>();
                logger.Record(exception, submissionId);
            }
        }

        #endregion
    }
}
