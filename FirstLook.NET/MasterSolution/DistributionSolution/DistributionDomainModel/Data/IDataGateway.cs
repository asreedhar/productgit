using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Interface for data gateways.
    /// </summary>
    /// <typeparam name="T">Object type for which data operations will be peformed.</typeparam>
    public interface IDataGateway<T> where T : Entity
    {
        /// <summary>
        /// Save an object of type T to the data layer.
        /// </summary>
        /// <param name="value">Obect to save.</param>
        void Save(T value);

        /// <summary>
        /// Delete an object of type T from the data layer.
        /// </summary>
        /// <param name="value">Object to delete.</param>
        void Delete(T value);
    }
}
