﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    public class AppraisedVehicleDataGateway:IAppraisedVehicleDataGateway
    {
        #region IAppraisedVehicleDataGateway Members

        /// <summary>
        /// Retrives vehicle details from vehicle ID
        /// </summary>
        /// <param name="appraisalId">appraisal ID to get vehicle details</param>
        /// <returns>Object of Inventory with Vehicle details</returns>
        public Inventory FetchVehicle(int appraisalId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Vehicle#Fetch";

                    command.AddParameter("AppraisalID", DbType.Int32, appraisalId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected vehicle data from database, but received none.");
                        }

                        Inventory inventory = new Inventory(appraisalId);
                        inventory.Vin = reader.GetString(reader.GetOrdinal("VIN"));
                        inventory.Year = reader.GetInt32(reader.GetOrdinal("VehicleYear"));

                        return inventory;
                    }
                }
            }  
        }


        public bool IsVehicleExists(int vehicleEntityId, int vehicleEntityTypeId)
        {
            bool vehicleExists=false;

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Vehicle#Exists";

                    command.AddParameter("VehicleEntityID", DbType.Int32, vehicleEntityId);
                    command.AddParameter("VehicleEntityTypeID", DbType.Int32, vehicleEntityTypeId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            vehicleExists = reader.GetInt32(reader.GetOrdinal("VehicleExists")) == 1 ? true : false;
                        }
                    }
                }
            }
            return vehicleExists;
        }
        #endregion
    }
}
