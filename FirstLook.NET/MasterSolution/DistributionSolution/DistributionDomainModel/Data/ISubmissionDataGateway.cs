using System.Collections.Generic;
using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Interface that defines submission data operations.
    /// </summary>
    public interface ISubmissionDataGateway : IDataGateway<Submission>
    {
        #region Submission

        /// <summary>
        /// Fetch the submissions tied to the publication with the given identifier.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>List of submissions.</returns>
        IList<Submission> Fetch(int publicationId);

        /// <summary>
        /// Fetch the submissions tied to the publication that is defined by the given dealer and vehicle.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleEntityType">Vehicle type.</param>
        /// <returns>List of submissions.</returns>
        IList<Submission> Fetch(int dealerId, int vehicleEntityId, VehicleEntityType vehicleEntityType);        

        /// <summary>
        /// Fetch the failed submissions that we should try to distribute.
        /// </summary>        
        /// <param name="maxAttempts">Only retrieve submissions that have been attempted fewer than this many times.</param>
        /// <param name="maxDaysBack">Maximum number of days back to fetch failures.</param>
        /// <returns>Submission that should be redistributed.</returns>
        IList<Submission> FetchFailuresToRetry(int maxAttempts, int maxDaysBack);

        #endregion

        #region Stale

        /// <summary>
        /// Get the stale and valid message types for a submission.
        /// </summary>
        /// <param name="submissionId">Submission to check the message types of.</param>
        /// <returns>The message types that are stale.</returns>
        MessageTypes FetchStaleTypes(int submissionId);

        /// <summary>
        /// Log that the message types represented by the given bit field were stale for the given submission.        
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="messageTypes">Message types that are stale.</param>
        void LogStale(int submissionId, int messageTypes);

        /// <summary>
        /// Fetch the price that is more recent than the one tied to the submission with the given identifier.
        /// </summary>
        /// <remarks>
        /// Currently, this only exists because AutoTrader cannot send Description on its own. If there is a request
        /// for AutoTrader that has both Price and Description, and then a newer submission with just Price makes the
        /// first request's price stale, we want to still be able to send the Description. Thus, we need to be able to
        /// query the most recent price.        
        /// </remarks>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Price.</returns>
        Price FetchSupersedingPrice(int submissionId);

        #endregion                

        #region Error

        /// <summary>        
        /// Fetch any error messages associated with the given submission.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Collection of errors.</returns>
        Collection<FirstLookError> FetchErrors(int submissionId);

        /// <summary>
        /// Log to the database an error that occurred to a submission.
        /// </summary>
        /// <param name="firstLookError">Error to save to the database.</param>        
        void LogError(FirstLookError firstLookError);

        #endregion   
        
        #region Success

        /// <summary>
        /// Fetch which message types were succesful for the given submission.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <returns>Collection of succesful message types.</returns>
        Collection<int> FetchSuccesses(int submissionId);

        /// <summary>
        /// Log that the message types represented by the given bit field were successful for the given submission.        
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="messageTypes">Message types that were accepted.</param>
        void LogSuccess(int submissionId, int messageTypes);

        #endregion
    }
}
