using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Threading;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Data
{
    /// <summary>
    /// Gateway for data operations on accounts, including operations on dealers, providers and account preferences.  
    /// </summary>
    public class AccountDataGateway : DataGateway<Account>, IAccountDataGateway
    {
        #region Account

        /// <summary>
        /// Delete the given account from the database.
        /// </summary>
        /// <param name="value">Account to delete.</param>
        protected override void DeleteSelf(Account value)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Account#Delete";

                    string deleteUser = Thread.CurrentPrincipal.Identity.Name;

                    command.AddParameter("DealerID", DbType.Int32, value.DealerId);
                    command.AddParameter("ProviderID", DbType.Int32, value.ProviderId);
                    command.AddParameter("DeleteUser", DbType.String, deleteUser);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Check if an account exists for the given provider and dealer.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if it exists, false otherwise.</returns>
        public bool Exists(int providerId, int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Account#Exists";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);
                    command.AddParameter("ProviderID", DbType.Int32, providerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetInt32(reader.GetOrdinal("AccountExists")) == 1 ? true : false;
                        }
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Fetch the account for the given provider and dealer.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if it exists, false otherwise.</returns>
        public Account Fetch(int providerId, int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Account#Fetch";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);
                    command.AddParameter("ProviderID", DbType.Int32, providerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected account data from database, but received none.");
                        }

                        string userName = string.Empty;
                        if (!reader.IsDBNull(reader.GetOrdinal("UserName")))
                        {
                            userName = reader.GetString(reader.GetOrdinal("UserName"));
                        }

                        string password = string.Empty;
                        if (!reader.IsDBNull(reader.GetOrdinal("Password")))
                        {
                            password = reader.GetString(reader.GetOrdinal("Password"));
                        }

                        string dealershipCode = string.Empty;
                        if (!reader.IsDBNull(reader.GetOrdinal("DealershipCode")))
                        {
                            dealershipCode = reader.GetString(reader.GetOrdinal("DealershipCode"));
                        }

                        bool active = reader.GetBoolean(reader.GetOrdinal("Active"));

                        return Account.GetAccount(dealerId, providerId, userName, password, active, dealershipCode);
                    }
                }
            }
        }

        public Account FetchADPAccount(int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.ADP_Account#Fetch";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected account data from database, but received none.");
                        }
                        
                        int targetId = Convert.ToInt16(reader.GetNullableInt32("TargetId"));
                        int targetTypeId = Convert.ToInt16(reader.GetNullableInt32("TargetTypeId"));
                        string description = reader.GetString("Description");
                        string selectedValue = reader.GetString("SelectedValue");
                        return Account.GetAccount(targetId, targetTypeId, description,selectedValue);
                    }
                }
            }
        }

        /// <summary>
        /// Insert the given account into the database.
        /// </summary>
        /// <param name="value">Account to insert.</param>
        protected override void Insert(Account value)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Account#Insert";

                    string insertUser = Thread.CurrentPrincipal.Identity.Name;

                    command.AddParameter("DealerID", DbType.Int32, value.DealerId);
                    command.AddParameter("ProviderID", DbType.Int32, value.ProviderId);

                    if (!string.IsNullOrEmpty(value.UserName))
                    {
                        command.AddParameter("UserName", DbType.String, value.UserName);
                    }

                    if (!string.IsNullOrEmpty(value.Password))
                    {
                        command.AddParameter("Password", DbType.String, value.Password);
                    }

                    // Validate Aultec.
                    if (value.ProviderId == (int)BindingType.Aultec)
                    {
                        if (string.IsNullOrEmpty(value.DealershipCode))
                        {
                            throw new ArgumentException("Dealer Code must be set for Aultec.");
                        }
                    }
                    // Validate AutoUplink.
                    else if (value.ProviderId == (int)BindingType.AutoUplink)
                    {
                        if (string.IsNullOrEmpty(value.DealershipCode))
                        {
                            throw new ArgumentException("Dealer Code must be set for AutoUplink.");
                        }
                    }
                    // Validate AutoTrader.
                    else if (value.ProviderId == (int)BindingType.AutoTrader)
                    {
                        if (string.IsNullOrEmpty(value.DealershipCode))
                        {
                            throw new ArgumentException("Dealer Code must be set for AutoTrader.");
                        }

                        long tempLong;
                        if (!long.TryParse(value.DealershipCode, out tempLong))
                        {
                            throw new ArgumentException("Dealer Code must be a number for AutoTrader");
                        }
                    }

                    if (!string.IsNullOrEmpty(value.DealershipCode))
                    {
                        command.AddParameter("DealershipCode", DbType.String, value.DealershipCode);
                    }

                    command.AddParameter("Active", DbType.Boolean, value.Active);
                    command.AddParameter("InsertUser", DbType.String, insertUser);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Update the given account in the database.
        /// </summary>
        /// <param name="value">Account to update.</param>
        protected override void Update(Account value)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Account#Update";

                    string updateUser = Thread.CurrentPrincipal.Identity.Name;

                    command.AddParameter("DealerID", DbType.Int32, value.DealerId);
                    command.AddParameter("ProviderID", DbType.Int32, value.ProviderId);

                    if (!string.IsNullOrEmpty(value.UserName))
                    {
                        command.AddParameter("UserName", DbType.String, value.UserName);
                    }

                    if (!string.IsNullOrEmpty(value.Password))
                    {
                        command.AddParameter("Password", DbType.String, value.Password);
                    }

                    // Validate Aultec.
                    if (value.ProviderId == (int)BindingType.Aultec)
                    {
                        if (string.IsNullOrEmpty(value.DealershipCode))
                        {
                            throw new ArgumentException("Dealer Code must be set for Aultec.");
                        }
                    }
                    // Validate AutoUplink.
                    else if (value.ProviderId == (int)BindingType.AutoUplink)
                    {
                        if (string.IsNullOrEmpty(value.DealershipCode))
                        {
                            throw new ArgumentException("Dealer Code must be set for AutoUplink.");
                        }
                    }
                    // Validate AutoTrader.
                    else if (value.ProviderId == (int)BindingType.AutoTrader)
                    {
                        if (string.IsNullOrEmpty(value.DealershipCode))
                        {
                            throw new ArgumentException("Dealer Code must be set for AutoTrader.");
                        }

                        long tempLong;
                        if (!long.TryParse(value.DealershipCode, out tempLong))
                        {
                            throw new ArgumentException("Dealer Code must be a number for AutoTrader");
                        }
                    }

                    if (!string.IsNullOrEmpty(value.DealershipCode))
                    {
                        command.AddParameter("DealershipCode", DbType.String, value.DealershipCode);
                    }

                    command.AddParameter("Active", DbType.Boolean, value.Active);
                    command.AddParameter("UpdateUser", DbType.String, updateUser);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Account Preferences

        /// <summary>
        /// Delete the account preferences for a dealer. Account preferences tie a dealer to the types of messages
        /// supported by each of its associated providers.
        /// </summary>
        /// <param name="dealerId">Identifier of the dealer whose account preferences should be deleted.</param>
        protected static void DeleteAccountPreferences(int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.AccountPreferences#Delete";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Delete the account preferences for a dealer and provider. Account preferences tie a dealer to the types of
        /// messages supported by each of its associated providers.
        /// </summary>
        /// <param name="dealerId"></param>
        /// <param name="providerId"></param>
        protected static void DeleteAccountPreference(int dealerId, int providerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.AccountPreference#Delete";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);
                    command.AddParameter("ProviderID", DbType.Int32, providerId);

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Fetch the account preferences for a dealer. Account preferences are the message types the dealer has 
        /// enabled per provider.
        /// </summary>
        /// <param name="dealerId">Identifier of the dealer whose account preferences should be fetched.</param>
        /// <returns>Account preferences.</returns>
        public AccountPreferenceDictionary FetchAccountPreferences(int dealerId)
        {
            AccountPreferenceDictionary accountPreferences;

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                string dealerName;

                // First get the name of the dealer from the db.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "dbo.Dealer#Fetch";

                    command.AddParameter("Id", DbType.Int32, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected dealer data data from database, but received none.");
                        }
                        dealerName = reader.GetString(reader.GetOrdinal("Name"));
                    }
                }

                accountPreferences = new AccountPreferenceDictionary(dealerId, dealerName);

                // Next, get all the preferences.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.AccountPreferences#Fetch";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Provider.                            
                            int providerId = reader.GetInt32(reader.GetOrdinal("ProviderID"));
                            string providerName = reader.GetString(reader.GetOrdinal("ProviderName"));
                            Provider provider = new Provider(providerId, providerName);

                            // Message type preference.
                            int messageTypes = reader.GetInt32(reader.GetOrdinal("MessageTypes"));
                            bool enabled = reader.GetInt32(reader.GetOrdinal("Enabled")) == 1 ? true : false;

                            MessageTypePreference preference =
                                new MessageTypePreference(providerId, (MessageTypes)messageTypes, enabled);

                            // Add to the account preferences.
                            accountPreferences.Add(provider, preference);
                        }
                    }
                }
            }

            return accountPreferences;
        }

        /// <summary>
        /// Insert the given account preferences into the database. Account preferences tie a dealer to the type of 
        /// messages supported by each of its associated providers.
        /// </summary>
        /// <param name="accountPreferences">Account preferences to insert.</param>
        protected static void InsertAccountPreferences(AccountPreferenceDictionary accountPreferences)
        {
            foreach (KeyValuePair<Provider, Collection<MessageTypePreference>> pair in accountPreferences)
            {
                InsertAccountPreference(accountPreferences.DealerId, pair.Value);
            }
        }

        /// <summary>
        /// Insert the given account preferences for a single provider into the database. Account preferences tie a 
        /// dealer to the type of messages supported by each of its associated providers.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="accountPreference">Preferences for a single provider to insert.</param>
        protected static void InsertAccountPreference(int dealerId, IList<MessageTypePreference> accountPreference)
        {
            foreach (MessageTypePreference preference in accountPreference)
            {
                if (preference.Enabled)
                {
                    using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
                    {
                        if (connection.State == ConnectionState.Closed)
                        {
                            connection.Open();
                        }

                        using (IDbCommand command = connection.CreateCommand())
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "Distribution.AccountPreferences#Insert";

                            command.AddParameter("DealerID", DbType.Int32, dealerId);
                            command.AddParameter("ProviderID", DbType.Int32, preference.ProviderId);
                            command.AddParameter("MessageTypes", DbType.Int32, preference.MessageTypes);

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Save the given account preferences. Will delete existing preferences for the dealer and replace them with 
        /// the ones provided. Account preferences are the message types the dealer has enabled per provider.
        /// </summary>
        /// <param name="accountPreferences">Account preferences to save.</param>
        public void SaveAccountPreferences(AccountPreferenceDictionary accountPreferences)
        {
            DeleteAccountPreferences(accountPreferences.DealerId);
            InsertAccountPreferences(accountPreferences);
        }

        /// <summary>
        /// Save the given account preference for a single provider. Will delete existing preferences for the dealer 
        /// and replace them with the ones provided. Account preferences are the message types the dealer has enabled
        /// per provider.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="preference">Account preference to save.</param>
        public void SaveAccountPreference(int dealerId, int providerId, IList<MessageTypePreference> preference)
        {
            DeleteAccountPreference(dealerId, providerId);
            InsertAccountPreference(dealerId, preference);
        }

        /// <summary>
        /// Does the account support the given message types? Check consists of three parts:
        /// 1. Is there active account information for the dealer with the provider?
        /// 2. Does the provider support the message type?
        /// 3. If the provider does support the message type, has the dealer enabled this setting? 
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="messageTypes">Message type bit field. Will be filtered to the supported subset type.</param>
        /// <returns>True if the account supports this, false otherwise.</returns>
        public bool IsSupported(int dealerId, int providerId, ref MessageTypes messageTypes)
        {
            bool supported;

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Account#MessageTypeEnabled";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);
                    command.AddParameter("ProviderID", DbType.Int32, providerId);
                    command.AddParameter("MessageTypes", DbType.Int32, messageTypes);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected account status results from database but received none.");
                        }

                        supported = reader.GetInt32(reader.GetOrdinal("Supports")) == 1 ? true : false;

                        if (!reader.NextResult() || !reader.Read())
                        {
                            throw new DataException("Expected filtered message type results from database, but received none.");
                        }

                        messageTypes = (MessageTypes)reader.GetInt32(reader.GetOrdinal("FilteredMessageTypes"));
                    }
                }
            }
            return supported;
        }


        public void SaveADPPriceMapping(int DealerID, string ADPTargetId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.ADP_DealerMapping#Insert";

                    command.AddParameter("DealerId", DbType.Int32, DealerID);
                    command.AddParameter("ADPTargetId", DbType.String, ADPTargetId);

                    command.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region Dealer

        /// <summary>
        /// Verify that a database record exists for the given dealer identifier.        
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if a dealer exists for that identifier.</returns>
        public bool DealerExists(int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Dealer#Exists";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetInt32(reader.GetOrdinal("DealerExists")) == 1 ? true : false;
                        }
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Check whether the dealer with the given identifier will need to update 'Internet Price' as opposed to just
        /// 'Price' because they are setup with Aultec for active extraction. Looks in IMT.Extract.DealerConfiguration.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>True if Aultec is enabled, false otherwise.</returns>
        public bool DealerRequiresInternetPriceUpdate(int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Extract.DealerConfigurationCollection#Fetch";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);

                    try
                    {
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.GetString(reader.GetOrdinal("DestinationName")) == "AULTec")
                                {
                                    return reader.GetBoolean(reader.GetOrdinal("Active"));
                                }
                            }
                        }
                    }
                    // The procedure will throw an exception if there is no row for the given dealer. That's not an
                    // exception we need to concern ourselves with - it just means we should return false.
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
            return false;
        }


        public Dealer GetDealer(int dealerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Dealer#Fetch";

                    command.AddParameter("DealerID", DbType.Int32, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected dealer data from database, but received none.");
                        }

                        string businessUnit = string.Empty;
                        if (!reader.IsDBNull(reader.GetOrdinal("BusinessUnit")))
                        {
                            businessUnit = reader.GetString(reader.GetOrdinal("BusinessUnit"));
                        }

                        string businessUnitShortName = string.Empty;
                        if (!reader.IsDBNull(reader.GetOrdinal("BusinessUnitShortName")))
                        {
                            businessUnitShortName = reader.GetString(reader.GetOrdinal("BusinessUnitShortName"));
                        }

                        string businessUnitCode = string.Empty;
                        if (!reader.IsDBNull(reader.GetOrdinal("BusinessUnitCode")))
                        {
                            businessUnitCode = reader.GetString(reader.GetOrdinal("BusinessUnitCode"));
                        }

                        bool active = Convert.ToBoolean(reader.GetValue(reader.GetOrdinal("Active")));

                        return new Dealer(dealerId, businessUnitCode, businessUnit, businessUnitShortName, active);
                    }
                }
            }
        }
        #endregion

        #region Provider

        /// <summary>
        /// Fetch the provider with the given identifier. Relevant to submissions because of when this is called in the
        /// flow of control.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <returns>Provider object.</returns>
        public Provider FetchProvider(int providerId)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Provider#Fetch";

                    command.AddParameter("ProviderID", DbType.Int32, providerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected provider information, but received none.");
                        }
                        return new Provider(providerId, reader.GetString(reader.GetOrdinal("Name")));
                    }
                }
            }
        }

        /// <summary>
        /// Fetch all providers.
        /// </summary>
        /// <returns>Collection of providers.</returns>
        public Collection<Provider> FetchProviders()
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.Providers#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Collection<Provider> providers = new Collection<Provider>();

                        while (reader.Read())
                        {
                            int providerId = reader.GetInt32(reader.GetOrdinal("ProviderID"));
                            string providerName = reader.GetString(reader.GetOrdinal("Name"));

                            providers.Add(new Provider(providerId, providerName));
                        }
                        return providers;
                    }
                }
            }
        }

        public List<ADPPriceType> FetchADPPriceType()
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Distribution.ADP_PriceType#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        List<ADPPriceType> priceTypes = new List<ADPPriceType>();

                        while (reader.Read())
                        {
                            string targetId = reader.GetString("TargetId");
                            string desc = reader.GetString(reader.GetOrdinal("Description"));

                            priceTypes.Add(new ADPPriceType() { TargetId = targetId, Description = desc });
                        }
                        return priceTypes;
                    }
                }
            }
        }
        #endregion

        #region User Identity

        /// <summary>
        /// Check if the user identity is valid.
        /// </summary>        
        /// <param name="userName">User name from the identity to be checked.</param>
        /// <returns>True if it is valid, false otherwise.</returns>
        public bool UserIdentityExists(string userName)
        {
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "dbo.Member#Exists";

                    command.AddParameter("UserName", DbType.String, userName);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("Exists"));
                        }
                        return false;
                    }
                }
            }
        }

        #endregion
    }
}
