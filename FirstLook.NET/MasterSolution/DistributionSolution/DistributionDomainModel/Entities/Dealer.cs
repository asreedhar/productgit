using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Dealer that defines the "from" of a message.
    /// </summary>
    public class Dealer
    {
        #region Properties

        /// <summary>
        /// Dealer identifier (a.k.a. business unit identifier).
        /// </summary>
        public int Id { get; private set; }


        /// <summary>
        /// DealerCode (Also known as Business Unit Code)
        /// </summary>
        public string DealerCode { get; set; }

        /// <summary>
        /// BusinessUnit Name
        /// </summary>
        public string BusinessUnit { get; set; }

        public string BusinessUnitShortName { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this dealer object with the given identifier.
        /// </summary>
        /// <param name="id">Dealer identifier.</param>
        protected internal Dealer(int id)
        {            
            Id = id;
        }

        public Dealer(int dealerId, string dealerCode, string businessUnit, string businessUnitShortName, bool isActive)
        {
            this.Id = dealerId;
            this.DealerCode = dealerCode;
            this.BusinessUnit = businessUnit;
            this.BusinessUnitShortName = businessUnitShortName;
            this.IsActive = isActive;

        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new dealer from the given transfer object. Will verify that the identifier of the given dealer is
        /// a valid one.
        /// Validates the following conditions:
        /// - Dealer must exist in the database.
        /// </summary>
        /// <param name="dealer">Dealer transfer object.</param>
        /// <returns>Dealer object.</returns>
        public static Dealer NewDealer(TransferObjects.V_1_0.Dealer dealer)
        {
            IAccountDataGateway accountDataGateway = RegistryFactory.GetResolver().Resolve<IAccountDataGateway>();
            if (!accountDataGateway.DealerExists(dealer.Id))
            {
                throw new InvalidDealerIdException();
            }

            return new Dealer(dealer.Id);
        }

        #endregion
    }
}
