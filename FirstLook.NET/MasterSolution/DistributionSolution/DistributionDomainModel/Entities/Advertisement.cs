using System.Collections.Generic;
using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Advertisement body part of a message. Advertisements are collections of content - text, images, whathaveyou.
    /// </summary>
    public class Advertisement : IBodyPart
    {
        #region Properties

        /// <summary>
        /// Get the type of this body part.
        /// </summary>
        public BodyPartType BodyPartType
        {
            get { return BodyPartType.Advertisement; }
        }

        /// <summary>
        /// Determine what kind of messages this body part supports.
        /// </summary>                
        public MessageTypes MessageTypes
        {
            get
            {
                MessageTypes messageTypes = MessageTypes.None;

                foreach (Content content in Contents)
                {
                    switch (content.ContentType)
                    {
                        // Text description.
                        case ContentType.Text:
                            messageTypes |= MessageTypes.Description;
                            break;
                    }
                }

                return messageTypes;
            }
        }

        /// <summary>
        /// Collection of content associated with this advertisement.
        /// </summary>
        public ReadOnlyCollection<Content> Contents { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this advertisement with the given collection of contents. Protected internal to force object 
        /// creation to go through factory methods.
        /// </summary>
        /// <param name="contents">Collection of content.</param>
        protected internal Advertisement(ReadOnlyCollection<Content> contents)
        {
           Contents = contents;
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new advertisement from the given transfer object.
        /// Validates the following conditions:
        /// - Advertisement must have some comment if it is in the message.
        /// </summary>
        /// <param name="advertisement">Advertisement transfer object.</param>
        /// <returns>Advertisement object.</returns>
        internal static Advertisement NewAdvertisement(TransferObjects.V_1_0.Advertisement advertisement)
        {            
            if (advertisement.Contents == null || advertisement.Contents.Count == 0)
            {
                throw new ContentEmptyException();
            }

            List<Content> contents = new List<Content>();

            foreach (TransferObjects.V_1_0.Content content in advertisement.Contents)
            {
                contents.Add(Content.NewContent(content));
            }

            return new Advertisement(new ReadOnlyCollection<Content>(contents));
        }

        #endregion
    }
}
