namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Status of a submission.
    /// </summary>
    public enum SubmissionState
    {
        /// <summary>
        /// Undefined, unrecognized submission status.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// Submission is queued and ready.
        /// </summary>
        Waiting = 1,

        /// <summary>
        /// Submission is being processed.
        /// </summary>
        Processing = 2,

        /// <summary>
        /// Submission has been sent to its provider.
        /// </summary>
        Submitted = 3,

        /// <summary>
        /// Provider accepted the submission.
        /// </summary>        
        Accepted = 4,

        /// <summary>
        /// Provider rejected the submission.
        /// </summary>
        Rejected = 5,

        /// <summary>
        /// Submission has been retracted.
        /// </summary>
        Retracted = 6,

        /// <summary>
        /// Submission is stale and has been retracted.
        /// </summary>
        Superseded = 7,

        /// <summary>
        /// Submission consists of a message that is not supported by a provider.
        /// </summary>
        NotSupported = 8
    }
}
