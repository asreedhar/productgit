namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Enumeration of available vehicle types.
    /// </summary>
    public enum VehicleEntityType
    {
        /// <summary>
        /// Undefined, unrecognized vehicle type.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// Inventory.
        /// </summary>
        Inventory = 1,

        /// <summary>
        /// Appraisal.
        /// </summary>
        Appraisal= 2
    }
}
