namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Types of content in an advertisement body part.
    /// </summary>
    public enum ContentType
    {
        /// <summary>
        /// Undefined content type.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// Text content.
        /// </summary>
        Text = 1
    }
}
