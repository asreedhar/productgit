namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Interface for a message's body parts.
    /// </summary>
    public interface IBodyPart
    {
        /// <summary>
        /// Get the type of this body part.
        /// </summary>
        BodyPartType BodyPartType { get; }

        /// <summary>        
        /// Determine what kind of messages this body part supports.
        /// </summary>        
        MessageTypes MessageTypes { get; }
    }
}