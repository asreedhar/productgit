using System;
using System.Collections.ObjectModel;
using System.Configuration;
using FirstLook.Distribution.DomainModel.Bindings;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Provider that a message may be published with.
    /// </summary>
    public class Provider
    {
        #region Properties

        /// <summary>
        /// Provider identifier.
        /// </summary>      
        public int Id { get; private set; }

        /// <summary>
        /// Name of the provider.
        /// </summary>
        public string Name { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this provider with the given identifier and name.
        /// </summary>
        /// <param name="id">Provider identifier.</param>
        /// <param name="name">Name of the provider.</param>
        public Provider(int id, string name)
        {
            Id = id;
            Name = name;
        }

        #endregion       
 
        #region Utility Methods
     
        /// <summary>
        /// Get the enabled providers for the dealer with the given identifier.
        /// </summary>
        /// <remarks>
        /// This utility function exists to replace a database call that was made every time a request was received.
        /// This is a pointless kind of database hit - it's something that hardly ever changes and is not worth nailing
        /// the database for every half second.
        /// </remarks>
        /// <param name="dealerId">Dealer identifier to check against the dealer filter.</param>
        /// <returns>Collection of providers that have been enabled in the config files.</returns>
        public static Collection<Provider> EnabledProviders(int dealerId)
        {
            Collection<Provider> enabledProviders = new Collection<Provider>();

            // Check if the provided dealer identifier matches the filter that might be in the config file.
            string dealerFilter = ConfigurationManager.AppSettings["DealerFilter"];
            if (!string.IsNullOrEmpty(dealerFilter) && int.Parse(dealerFilter) != dealerId)
            {
                return enabledProviders;
            }

            // Aultec.
            if (ConfigurationManager.AppSettings["EnableAultec"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.Aultec, "Aultec"));
            }
            // AutoUplink.
            if (ConfigurationManager.AppSettings["EnableAutoUplink"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.AutoUplink, "AutoUplink"));
            }
            // AutoTrader.
            if (ConfigurationManager.AppSettings["EnableAutoTrader"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.AutoTrader, "AutoTrader"));
            }
            // GetAuto.
            if (ConfigurationManager.AppSettings["EnableGetAuto"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.GetAuto, "GetAuto"));
            }
            // eBiz.
            if (ConfigurationManager.AppSettings["EnableEbiz"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.eBiz, "eBiz"));
            }

            // eLead.
            if (ConfigurationManager.AppSettings["EnableElead"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.eLead, "eLead"));
            }

             // ADP.
            if (ConfigurationManager.AppSettings["EnableADP"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.ADP, "ADP"));
            }

            // HomeNet.
            if (ConfigurationManager.AppSettings["EnableHomeNet"] == "1")
            {
                enabledProviders.Add(new Provider((int)BindingType.HomeNet, "HomeNet"));
            }
            
            return enabledProviders;
        }    

        #endregion

        #region Comparison Methods

        /// <summary>
        /// Is this provider equal to the given provider?
        /// </summary>
        /// <param name="obj">Provider object.</param>
        /// <returns>True if the provider identifiers are equal; false otherwise.</returns>
        public override bool Equals(Object obj)
        {
            Provider provider = obj as Provider;
            if (provider == null)
            {
                return false;
            }

            return Id == provider.Id;
        }

        /// <summary>
        /// Overridden hash code function because Equals() is overriden.
        /// </summary>
        /// <returns>Provider identifier.</returns>
        public override int GetHashCode()
        {
            return Id;
        }

        #endregion
    }
}
