using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FirstLook.Distribution.DomainModel.Entities
{      
    /// <summary>   
    /// A dealer's preferences for all message type combinations supported by all providers.    
    /// </summary>
    public class AccountPreferenceDictionary : Dictionary<Provider, Collection<MessageTypePreference>>
    {
        #region Properties

        /// <summary>
        /// Identifier of the dealer whose account preferences these are.
        /// </summary>
        public int DealerId { get; private set; }

        /// <summary>
        /// Name of the dealer whose account preferences these are.
        /// </summary>
        public string DealerName { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Initialize these account preferences as being for the given dealer.
        /// </summary>
        /// <param name="dealerId">Dealer whose account preferences these are.</param>
        /// <param name="dealerName">Name of the dealer whose account preferences these are.</param>
        public AccountPreferenceDictionary(int dealerId, string dealerName)
        {
            DealerId = dealerId;
            DealerName = dealerName;
        }

        #endregion

        /// <summary>
        /// Add a message type preference to these account preferences.
        /// </summary>
        /// <param name="provider">Provider for whom we are adding a message type preference.</param>
        /// <param name="messageTypePreference">Message type preference.</param>
        public void Add(Provider provider, MessageTypePreference messageTypePreference)
        {
            if (!ContainsKey(provider))
            {
                this[provider] = new Collection<MessageTypePreference>();
            }
            this[provider].Add(messageTypePreference);
        }
    }
}
