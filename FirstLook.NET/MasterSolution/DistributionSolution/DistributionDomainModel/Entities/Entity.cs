using FirstLook.Common.Core;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Base class for all distribution entities.
    /// </summary>
    public abstract class Entity : PropertyChangeSupport
    {        
        #region Status Checking

        /// <summary>
        /// Is this entity new?
        /// </summary>
        public bool IsNew { get; private set; }

        /// <summary>
        /// Has the entity been marked for deletion?
        /// </summary>
        public bool IsDeleted { get; private set; }

        /// <summary>
        /// Is the entity dirty?
        /// </summary>
        public virtual bool IsDirty { get; private set; }

        /// <summary>
        /// Create a new entity.
        /// </summary>
        protected Entity()
        {
            IsDirty = true;
            IsNew = true;
        }

        #endregion

        #region Status Marking

        /// <summary>
        /// Mark this entity as new.
        /// </summary>
        protected internal void MarkNew()
        {
            IsNew = true;

            IsDeleted = false;

            MarkDirty();
        }

        /// <summary>
        /// Mark this entity as old.
        /// </summary>
        protected internal void MarkOld()
        {
            IsNew = false;

            MarkClean();
        }

        /// <summary>
        /// Mark this entity as being up for deletion.
        /// </summary>
        protected internal void MarkDeleted()
        {
            IsDeleted = true;

            MarkDirty();
        }

        /// <summary>
        /// Mark this entity as dirty.
        /// </summary>
        protected void MarkDirty()
        {
            IsDirty = true;
        }

        /// <summary>
        /// Mark this entity as clean.
        /// </summary>
        protected void MarkClean()
        {
            IsDirty = false;
        }

        #endregion

        #region Property Change Support

        /// <summary>
        /// Mark this object as dirty when a property has been changed.
        /// </summary>
        /// <param name="propertyName">Property that is changing.</param>
        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            MarkDirty();
        }

        #endregion
    }
}
