using System.Collections.Generic;
using System.Collections.ObjectModel;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Publication of a message to a provider.
    /// </summary>
    public class Publication : Entity
    {
        #region Properties

        /// <summary>
        /// Publication identifier.
        /// </summary>
        public int Id { get; protected internal set; }

        /// <summary>
        /// The message that is being published.
        /// </summary>
        public Message Message { get; private set; }

        /// <summary>
        /// The status of this publication.
        /// </summary>
        public PublicationState PublicationStatus { get; private set; }

        /// <summary>
        /// The submissions associated with this publication - one each for every provider associated with the type 
        /// of the message.
        /// </summary>
        public ReadOnlyCollection<Submission> Submissions
        {
            get { return new ReadOnlyCollection<Submission>(_submissions); }
        }
        readonly IList<Submission> _submissions = new List<Submission>();

        #endregion

        #region Construction

        /// <summary>
        /// Populate this object with the given values of an existing publication.
        /// </summary>
        /// <param name="id">Publication identifier.</param>
        /// <param name="message">Message to publish.</param>        
        /// <param name="publicationStatus">Status of this publication.</param>
        protected internal Publication(int id, Message message, PublicationState publicationStatus)
        {
            Id = id;
            Message = message;
            PublicationStatus = publicationStatus;

            MarkOld();
        }

        /// <summary>
        /// Populate this object with the values of a new publication.        
        /// </summary>
        /// <param name="message">Message tp publish.</param>
        private Publication(Message message)
        {
            Message = message;
            PublicationStatus = PublicationState.Offline;

            MarkNew();
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a publication from the given transfer object.
        /// </summary>
        /// <param name="message">Message transfer object.</param>
        /// <returns>Publication object.</returns>
        public static Publication NewPublication(TransferObjects.V_1_0.Message message)
        {
            if (message == null)
            {
                throw new MessageNullException();
            }

            return new Publication(Message.NewMessage(message));
        }

        #endregion        

        #region Status Marking

        /// <summary>
        /// Mark a publication as being offline.
        /// </summary>
        public void Retract()
        {
            PublicationStatus = PublicationState.Offline;

            IPublicationDataGateway _gateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            _gateway.Save(this);
        }       
 
        /// <summary>
        /// Mark a publication as being online.
        /// </summary>
        public void Online()
        {
            PublicationStatus = PublicationState.Online;

            IPublicationDataGateway _gateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            _gateway.Save(this);
        }

        /// <summary>
        /// The publication had no applicable providers, and was not distributed.
        /// </summary>
        public void NotDistributed()
        {
            PublicationStatus = PublicationState.NotApplicable;

            IPublicationDataGateway _gateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            _gateway.Save(this);
        }

        #endregion

        #region Submission Creation

        /// <summary>
        /// Create a submission of the publication's message to the provider. This submission is the first attempt at
        /// submitting this message.        
        /// </summary>
        /// <param name="providerId">Identifier of the provider to publish the message with.</param>
        /// <param name="messageTypes">Message types that will be submitted to the provider.</param>        
        /// <returns>Submission object.</returns>
        public Submission CreateSubmission(int providerId, MessageTypes messageTypes)
        {
            return CreateSubmission(providerId, messageTypes, 1);
        }

        /// <summary>
        /// Create a submission that is the Nth attempt of submitting the publication's message to the provider. 
        /// </summary>
        /// <param name="providerId">Identifier of the provider to publish the message with.</param>
        /// <param name="messageTypes">Message types that will be submitted to the provider.</param>        
        /// <param name="attempt">What attempt is this at submitting the message to the provider?</param>
        /// <returns>New submission.</returns>
        public Submission CreateSubmission(int providerId, MessageTypes messageTypes, int attempt)
        {
            Submission submission = Submission.NewSubmission(providerId, Id, messageTypes, attempt);
            _submissions.Add(submission);
            return submission;
        }

        #endregion
    }
}
