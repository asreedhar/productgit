using System;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Submission of a publication to a third party provider.
    /// </summary>
    public class Submission : Entity, ISubmission
    {
        #region Properties

        /// <summary>
        /// The data gateway that will be used in interacting with the persistence layer.
        /// </summary>
        private readonly ISubmissionDataGateway _gateway;

        /// <summary>
        /// Submission identifier.
        /// </summary>
        public int Id { get; protected internal set; }

        /// <summary>
        /// Identifier of the publication tied to the submission.
        /// </summary>
        public int PublicationId { get; private set; }

        /// <summary>
        /// Identifier of the provider the submission will be published to.
        /// </summary>
        public int ProviderId { get; private set; }

        /// <summary>
        /// Parts of the message that are being submitted.
        /// </summary>
        public MessageTypes MessageTypes { get; private set; }

        /// <summary>
        /// Parts of the message that will not be sent because they are stale.
        /// </summary>
        public MessageTypes StaleMessageTypes { get; private set; }

        /// <summary>
        /// State of the submission.
        /// </summary>
        public SubmissionState SubmissionState { get; private set; }

        /// <summary>
        /// The number of attempts that have been made to submit the publication to the provider.
        /// </summary>
        public int Attempt { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this object with the values of an existing submission.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="providerId">Identifier of the provider.</param>        
        /// <param name="publicationId">Identifier of the publication.</param>
        /// <param name="messageTypes">Parts of the message that are being submitted.</param>
        /// <param name="submissionState">Status of the submission.</param>               
        /// <param name="attempt">What attempt is this to submit the publication to the provider?</param>        
        protected internal Submission(int submissionId, int providerId, int publicationId, MessageTypes messageTypes,
                                      SubmissionState submissionState, int attempt)
        {
            Id                = submissionId;
            ProviderId        = providerId;
            PublicationId     = publicationId;
            MessageTypes      = messageTypes;
            StaleMessageTypes = 0;
            SubmissionState   = submissionState;
            Attempt           = attempt;            
            _gateway          = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a new submission from the given values.
        /// </summary>
        /// <param name="providerId">Identifier of the provider.</param>        
        /// <param name="publicationId">Identifier of the publication.</param>        
        /// <param name="messageTypes">Parts of the message that are being submitted.</param>        
        /// <returns>Submission object.</returns>
        public static Submission NewSubmission(int providerId, int publicationId, MessageTypes messageTypes)
        {
            return new Submission(-1, providerId, publicationId, messageTypes, SubmissionState.Waiting, 1);
        }

        /// <summary>
        /// Create a new submission as a new attempt of a previous submission.
        /// </summary>
        /// <param name="providerId">Identifier of the provider.</param>        
        /// <param name="publicationId">Identifier of the publication.</param>        
        /// <param name="messageTypes">Parts of the message that are being submitted.</param> 
        /// <param name="attempt">What attempt this is to submit the publication to the provider.</param>       
        /// <returns>Submission object.</returns>
        public static Submission NewSubmission(int providerId, int publicationId, MessageTypes messageTypes, int attempt)
        {
            return new Submission(-1, providerId, publicationId, messageTypes, SubmissionState.Waiting, attempt);
        }

        #endregion

        #region Status Checking

        /// <summary>
        /// Filter out and log any stale message types. A message type is stale if there is a more recent submission
        /// of the same type.
        /// </summary>
        public void FilterStaleTypes()
        {            
            MessageTypes staleTypes = _gateway.FetchStaleTypes(Id);

            if (staleTypes < 0 || staleTypes > MessageTypes)
            {
                throw new ApplicationException("Check for stale message types returned invalid output.");
            }
            if (staleTypes > 0)
            {
                _gateway.LogStale(Id, (int)staleTypes);
            }
            
            StaleMessageTypes = staleTypes;                     
            MessageTypes = (MessageTypes)(MessageTypes - staleTypes);            
        }

        #endregion

        #region Status Marking

        /// <summary>
        /// Mark this submission as being processed, and update the database.
        /// </summary>
        public void InProgress()
        {
            SubmissionState = SubmissionState.Processing;
            _gateway.Save(this);
        }

        /// <summary>
        /// Mark this submission as having been superceded (i.e. it's stale), and update the database.
        /// </summary>
        public void Superseded()
        {
            SubmissionState = SubmissionState.Superseded;
            _gateway.Save(this);
        }

        /// <summary>
        /// Mark this submission as having been submitted to a provider, and update the database.
        /// </summary>
        public void Submitted()
        {
            SubmissionState = SubmissionState.Submitted;
            _gateway.Save(this);
        }

        /// <summary>
        /// Mark this submission as having been rejected by the provider, and update the database.
        /// </summary>
        public void Rejected()
        {
            SubmissionState = SubmissionState.Rejected;
            _gateway.Save(this);
        }

        /// <summary>
        /// Mark the submission as having been accepted by the provider, and update the database.
        /// </summary>        
        public void Accepted()
        {
            SubmissionState = SubmissionState.Accepted;
            _gateway.Save(this);
        }

        /// <summary>
        /// Mark the submission as not being supported by the provider.
        /// </summary>
        public void NotSupported()
        {
            SubmissionState = SubmissionState.NotSupported;
            _gateway.Save(this);
        }

        #endregion              
    }
}
