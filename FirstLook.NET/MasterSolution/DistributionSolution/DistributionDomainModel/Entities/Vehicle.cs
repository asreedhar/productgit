using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Vehicle that is the subject of the message to be published.
    /// </summary>
    public class Vehicle
    {
        #region Properties

        /// <summary>
        /// Vehicle identifier.
        /// </summary>
        public int VehicleEntityId { get; protected set; }

        /// <summary>
        /// Vehicle type - e.g. inventory.
        /// </summary>
        public VehicleEntityType VehicleEntityType { get; protected set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this vehicle with the given type and identifier.
        /// </summary>
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleEntityType">Vehicle type.</param>                
        protected internal Vehicle(int vehicleEntityId, VehicleEntityType vehicleEntityType)
        {            
            VehicleEntityId = vehicleEntityId;
            VehicleEntityType = vehicleEntityType;
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new vehicle object from the given transfer object.
        /// Validates the following conditions:
        /// - Vehicle must not have an undefined type.
        /// - Vehicle type must be inventory.
        /// - Inventory vehicle must exist in the database.
        /// </summary>
        /// <remarks>
        /// <para>
        /// FB 13010: Extra validation to check that vehicle identifier exists in the database is now turned off since
        /// Wanamaker may send ads for vehicles that have already been sold and are therefore not active.                
        /// </para>
        /// <para>
        /// FB 14311: I am now disregarding FB 13010, and re-enabling validating a vehicle is active inventory. This is 
        /// done because non-active inventory was failing anyway in the binding code on the db lookup for the vin. 
        /// Better to have it fail here during validation with an error that is more appropriate.
        /// </para>
        /// <para>
        /// FB 15840: The saga continues. Since we are now more closely monitoring Distribution exceptions, having a 
        /// billion thrown for non-inventory vehicles being sent by Max Ad is a problem. So a different approach is now
        /// necessary.
        /// </para>
        /// </remarks>
        /// <param name="vehicle">Vehicle transfer object.</param>
        /// <returns>Vehicle object.</returns>
        public static Vehicle NewVehicle(TransferObjects.V_1_0.Vehicle vehicle)
        {            
            // Only vehicles that say they are active inventory are accepted.
            if (vehicle.VehicleType == TransferObjects.V_1_0.VehicleType.Inventory)
            {
                IVehicleDataGateway vehicleDataGateway = RegistryFactory.GetResolver().Resolve<IVehicleDataGateway>();
                
                // Verify if the vehicle actually is active inventory.
                if (!vehicleDataGateway.InventoryExists(vehicle.Id))
                {
                    return new Vehicle(vehicle.Id, VehicleEntityType.NotDefined);
                }

                return new Vehicle(vehicle.Id, VehicleEntityType.Inventory);
            }
            else
            {
                if (vehicle.VehicleType == TransferObjects.V_1_0.VehicleType.Appraisal)
                {
                    return new Vehicle(vehicle.Id,VehicleEntityType.Appraisal);
                }
            }

            throw new UnknownVehicleTypeException();
        }

        #endregion
    }
}
