namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Status of a publication.
    /// </summary>
    public enum PublicationState
    {
        /// <summary>
        /// Undefined, unrecognized status.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// The publication is published and online.
        /// </summary>
        Online = 1,

        /// <summary>
        /// The publication is not online.
        /// </summary>
        Offline = 2,

        /// <summary>
        /// The publication had no applicable providers, and was not distributed.
        /// </summary>
        NotApplicable = 3
    }
}
