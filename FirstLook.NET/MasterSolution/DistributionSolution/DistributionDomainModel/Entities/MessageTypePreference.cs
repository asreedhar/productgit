namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>    
    /// A dealer's preference for one specific message type combination for one specific provider.
    /// </remarks>
    public class MessageTypePreference
    {
        #region Properties

        /// <summary>
        /// Provider identifier.
        /// </summary>
        public int ProviderId { get; private set; }

        /// <summary>
        /// Specific combination of message types supported by a provider.
        /// </summary>
        public MessageTypes MessageTypes { get; private set; }

        /// <summary>
        /// String representation of the message types (for front-end display purposes).
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Whether or not the dealer has enabled distribution for this combination of message types.
        /// </summary>
        public bool Enabled { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this object with the provided message types bit field, string, and whether it was enabled.
        /// </summary>
        /// <param name="providerId">Identifier of the provider who supports the given message types.</param>
        /// <param name="messageTypes">Combination of message types.</param>        
        /// <param name="enabled">Has the dealer enabled this combination of message types?</param>
        public MessageTypePreference(int providerId, MessageTypes messageTypes, bool enabled)
        {
            ProviderId = providerId;
            MessageTypes = messageTypes;
            Description = Message.ParseMessageTypesToString((int)messageTypes);
            Enabled = enabled;
        }

        #endregion
    }
}