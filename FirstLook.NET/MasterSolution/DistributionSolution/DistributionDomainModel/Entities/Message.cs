using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Message the is being published to a provider.
    /// </summary>
    public class Message
    {
        #region Properties

        /// <summary>
        /// Message identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Dealer who wishes to publish this message.
        /// </summary>
        public Dealer Dealer { get; private set; }

        /// <summary>
        /// Vehicle that a message is being published for.
        /// </summary>
        public Vehicle Vehicle { get; private set; }

        /// <summary>
        /// The various components that define this message.
        /// </summary>
        public ReadOnlyCollection<IBodyPart> Body { get; private set; }

        /// <summary>
        /// The types of messages supported by the body parts.
        /// </summary>
        public MessageTypes MessageTypes { get; private set; }

        #endregion        

        #region Construction

        /// <summary>
        /// Populate this message with the given values. Will also determine the types of messages supported by the
        /// body parts.
        /// </summary>
        /// <param name="dealer">Dealer that wishes to publish this message.</param>
        /// <param name="vehicle">Vehicle that the message is being published for.</param>        
        /// <param name="body">Components that define this message.</param>
        protected internal Message(Dealer dealer, Vehicle vehicle, ReadOnlyCollection<IBodyPart> body)
        {
            Dealer = dealer;
            Vehicle = vehicle;            
            Body = body;
            MessageTypes = GetMessageTypes(body);
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new message from the values in the given transfer object.
        /// Validates the following conditions:
        /// - Message must not be null.
        /// - Dealer must not be null.
        /// - Subject must not be null.
        /// - Body and its parts must not be null.
        /// - Body parts must be of recognized types.
        /// </summary>
        /// <param name="message">Message transfer object.</param>
        /// <returns>Message object.</returns>
        public static Message NewMessage(TransferObjects.V_1_0.Message message)
        {            
            if (message == null)
            {
                throw new MessageNullException();
            }
            
            if (message.From == null)
            {
                throw new DealerNullException();
            }
            
            if (message.Subject == null)
            {
                throw new VehicleNullException();
            }
            
            if (message.Body == null)
            {
                throw new BodyNullException();
            }
            
            if (message.Body.Parts == null || message.Body.Parts.Count == 0)
            {
                throw new BodyEmptyException();
            }

            Collection<IBodyPart> list = new Collection<IBodyPart>();

            foreach (TransferObjects.V_1_0.BodyPart part in message.Body.Parts)
            {
                IBodyPart item;

                switch (part.BodyPartType)
                {
                    // Advertisement.
                    case TransferObjects.V_1_0.BodyPartType.Advertisement:
                        TransferObjects.V_1_0.Advertisement a = (TransferObjects.V_1_0.Advertisement) part;
                        item = Advertisement.NewAdvertisement(a);
                        break;

                    // Price.
                    case TransferObjects.V_1_0.BodyPartType.Price:
                        TransferObjects.V_1_0.Price p = (TransferObjects.V_1_0.Price) part;
                        item = Price.NewPrice(p);
                        break;

                    // Vehicle information.
                    case TransferObjects.V_1_0.BodyPartType.VehicleInformation:
                        TransferObjects.V_1_0.VehicleInformation v = (TransferObjects.V_1_0.VehicleInformation) part;
                        item = VehicleInformation.NewVehicleInformation(v);
                        break;
                    
                    //Appraisal Value
                    case TransferObjects.V_1_0.BodyPartType.AppraisalValue:
                        TransferObjects.V_1_0.AppraisalValue appraisalValue = (TransferObjects.V_1_0.AppraisalValue) part;
                        item = AppraisalValue.NewAppraisalValue(appraisalValue);
                        break;
                    // Unrecongized.
                    default:
                        throw new UnknownBodyPartTypeException();
                }

                list.Add(item);
            }

            return new Message(Dealer.NewDealer(message.From),
                               Vehicle.NewVehicle(message.Subject),
                               new ReadOnlyCollection<IBodyPart>(list));
        }

        #endregion

        #region Message Type Methods


        /// <summary>
        /// Get a price body part from this message, if it exists.
        /// </summary>
        /// <remarks>
        /// If there are multiple price body parts in this message, this will return the first one it comes across, 
        /// whichever one that might be (i.e. there's no guarantees on ordering).
        /// </remarks>
        /// <returns>Price body part if one exists, or null if it doesnt.</returns>
        public Price GetPrice()
        {
            foreach (IBodyPart bodyPart in Body)
            {
                Price price = bodyPart as Price;
                if (price != null)
                {
                    return price;
                }
            }
            return null;
        }

        /// <summary>
        /// Get a vehicle information body part from this message, if it exists.
        /// </summary>
        /// <remarks>
        /// If there are multiple vehicle information body parts in this message, this will return the first one it 
        /// comes across, whichever one that might be (i.e. there's no guarantees on ordering).
        /// </remarks>
        /// <returns>Vehicle information body part if one exists, or null if it doesnt.</returns>
        public VehicleInformation GetVehicleInformation()
        {
            foreach (IBodyPart bodyPart in Body)
            {
                VehicleInformation vehicleInformation = bodyPart as VehicleInformation;
                if (vehicleInformation != null)
                {
                    return vehicleInformation;
                }
            }
            return null;
        }

        /// <summary>
        /// Get a text content body part from this message, if it exists.
        /// </summary>
        /// <remarks>
        /// If there are multiple text content body parts in this message, this will return the first one it comes 
        /// across, whichever one that might be (i.e. there's no guarantees on ordering).
        /// </remarks>
        /// <returns>Text content body part if one exists, or null if it doesnt.</returns>
        public TextContent GetDescription()
        {
            foreach (IBodyPart bodyPart in Body)
            {
                Advertisement advertisement = bodyPart as Advertisement;
                if (advertisement != null)
                {
                    foreach (Content content in advertisement.Contents)
                    {
                        TextContent textContent = content as TextContent;
                        if (textContent != null)
                        {
                            return textContent;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get a second text content body part from this message, if it exists.
        /// </summary>
        /// <remarks>
        /// Looks for a text content right after a first text content which represents html text. 
        /// </remarks>
        /// <returns>Text content body part if one exists, or null if it doesnt.</returns>
        public TextContent GetHtmlDescription()
        {
            foreach (IBodyPart bodyPart in Body)
            {
                Advertisement advertisement = bodyPart as Advertisement;
                if (advertisement != null)
                {
                    for (int x = 0; x < advertisement.Contents.Count; x++)
                    {
                        var textContent = advertisement.Contents[x] as TextContent;
                        if (textContent != null) //found first
                        {
                            if (x + 1 < advertisement.Contents.Count && advertisement.Contents[x + 1] is TextContent)
                                return advertisement.Contents[x + 1] as TextContent;
                            
                            break;
                        }
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// Get a appraisal value body part from this message, if it exists.
        /// </summary>
        /// <remarks>
        /// If there are multiple price body parts in this message, this will return the first one it comes across, 
        /// whichever one that might be (i.e. there's no guarantees on ordering).
        /// </remarks>
        /// <returns>AppraisalValue body part if one exists, or null if it doesnt.</returns>
        public AppraisalValue GetAppraisalValue()
        {
            foreach (IBodyPart bodyPart in Body)
            {
                AppraisalValue appraisalValue = bodyPart as AppraisalValue;
                if (appraisalValue != null)
                {
                    return appraisalValue;
                }
            }
            return null;
        }

        /// <summary>
        /// Determine what type of messages are supported by the given body parts. For example, an advertisement body
        /// part could support text, images, video, etc. A vehicle information body part could support mileage, color,
        /// trim, etc.
        /// </summary>
        /// <param name="body">Message body parts.</param>
        /// <returns>Message types supported by the body parts.</returns>
        public static MessageTypes GetMessageTypes(IEnumerable<IBodyPart> body)
        {
            MessageTypes messageTypes = MessageTypes.None;

            foreach (IBodyPart bodyPart in body)
            {
                switch (bodyPart.BodyPartType)
                {                    
                    case BodyPartType.Advertisement:
                        messageTypes |= MessageTypes.Description;
                        break;

                    case BodyPartType.Price:
                        messageTypes |= MessageTypes.Price;
                        break;

                    case BodyPartType.VehicleInformation:
                        messageTypes |= MessageTypes.Mileage;
                        break;

                    case BodyPartType.AppraisalValue:
                        messageTypes |= MessageTypes.AppraisalValue;
                        break;

                    default:
                        throw new UnknownBodyPartTypeException();
                }
            }
            return messageTypes;
        }        

        /// <summary>
        /// Parse the given message type bit field into a string representation of all the message types contained 
        /// therein.        
        /// </summary>
        /// <param name="messageTypes">Message type bit field to decompose.</param>
        /// <returns>String representation.</returns>
        public static string ParseMessageTypesToString(int messageTypes)
        {
            StringBuilder typeNames = new StringBuilder();
            foreach (MessageTypes msgTypes in Enum.GetValues(typeof(MessageTypes)))
            {
                if (msgTypes == MessageTypes.None)
                {
                    continue;
                }

                if ((messageTypes & (int)msgTypes) == (int)msgTypes)
                {
                    if(typeNames.Length > 0)
                    {
                        typeNames.Append(" and ");
                    }
                    typeNames.Append(Enum.GetName(typeof (MessageTypes), msgTypes));
                }
            }

            return typeNames.ToString();
        }

        /// <summary>
        /// Parse the given message type bit field into a collection of individual message types.
        /// </summary>
        /// <param name="messageTypes">Message type bit field to decompose.</param>
        /// <returns>Collection of individual message types that composed the bit field.</returns>
        public static Collection<MessageTypes> ParseMessageTypesToList(int messageTypes)
        {
            Collection<MessageTypes> messageTypeCollection = new Collection<MessageTypes>();

            // Price.
            if ((messageTypes & (int)MessageTypes.Price) == (int)MessageTypes.Price)
            {
                messageTypeCollection.Add(MessageTypes.Price);
            }

            // Description.
            if ((messageTypes & (int)MessageTypes.Description) == (int)MessageTypes.Description)
            {
                messageTypeCollection.Add(MessageTypes.Description);
            }

            // Mileage.
            if ((messageTypes & (int)MessageTypes.Mileage) == (int)MessageTypes.Mileage)
            {
                messageTypeCollection.Add(MessageTypes.Mileage);
            }

            // Appraisal Value.
            if ((messageTypes & (int)MessageTypes.AppraisalValue) == (int)MessageTypes.AppraisalValue)
            {
                messageTypeCollection.Add(MessageTypes.AppraisalValue);
            }

            return messageTypeCollection;
        }

        #endregion
    }
}
