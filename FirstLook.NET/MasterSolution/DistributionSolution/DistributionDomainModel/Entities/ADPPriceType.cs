﻿
namespace FirstLook.Distribution.DomainModel.Entities
{
    public class ADPPriceType
    {
        public string Description { get; set; }

        public string TargetId { get; set; }

        public int TargetTypeId { get; set; }
    }
}
