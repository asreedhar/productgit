namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Interface for submissions.
    /// </summary>
    public interface ISubmission
    {
        #region Properties

        /// <summary>
        /// Submission identifier.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Identifier of the publication tied to the submission.
        /// </summary>
        int PublicationId { get; }

        /// <summary>
        /// Identifier of the provider the submission will be given to.
        /// </summary>
        int ProviderId { get; }

        /// <summary>
        /// Parts of the message that are being sent to the provider.
        /// </summary>
        MessageTypes MessageTypes { get; }

        /// <summary>
        /// Parts of the message that will not be sent because they are stale.
        /// </summary>
        MessageTypes StaleMessageTypes { get; }

        /// <summary>
        /// State of the submission.
        /// </summary>
        SubmissionState SubmissionState { get; }

        /// <summary>
        /// What attempt is this to submit a publication with a provider?
        /// </summary>
        int Attempt { get; }  

        #endregion

        #region Status Checking

        /// <summary>
        /// Filter out and log any stale message types. A message type is stale if there is a more recent submission
        /// of the same type.
        /// </summary>
        void FilterStaleTypes();

        #endregion

        #region Status Marking

        /// <summary>
        /// Mark the submission as being in progress.
        /// </summary>
        void InProgress();

        /// <summary>
        /// Mark the submission as having been superceded.
        /// </summary>
        void Superseded();

        /// <summary>
        /// Mark the submission as having been submitted.
        /// </summary>
        void Submitted();

        /// <summary>
        /// Mark the submission as having been rejected.
        /// </summary>
        void Rejected();

        /// <summary>
        /// Mark the submission as having been accepted.
        /// </summary>        
        void Accepted();

        /// <summary>
        /// Mark the submission as not being supported by a provider.
        /// </summary>
        void NotSupported();

        #endregion
    }
}