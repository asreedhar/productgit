using System;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Enumeration of available message types. Combinable. Values must be powers of 2.
    /// </summary>
    [Flags]
    public enum MessageTypes
    {
        /// <summary>
        /// Undefined message type.
        /// </summary>
        None = 0,

        /// <summary>
        /// Price message.
        /// </summary>
        Price = 1,

        /// <summary>
        /// Text description
        /// </summary>
        Description = 2,

        /// <summary>
        /// Vehicle mileage
        /// </summary>
        Mileage = 4,

        /// <summary>
        /// Appraisal Value.
        /// </summary>
        AppraisalValue = 8

    }
}
