namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Inventory information for a vehicle.
    /// </summary>
    public class Inventory : Vehicle
    {
        #region Properties        

        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; internal set; }

        /// <summary>
        /// Year.
        /// </summary>
        public int Year { get; internal set; }

        /// <summary>
        /// Make.
        /// </summary>
        public string Make { get; internal set; }

        /// <summary>
        /// Model.
        /// </summary>
        public string Model { get; internal set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this inventory item with the given vehicle identifier.
        /// </summary>
        /// <param name="vehicleEntityId">Vehicle entity identifier.</param>
        protected internal Inventory(int vehicleEntityId)
            : base(vehicleEntityId, VehicleEntityType.Inventory)
        {
        }        

        #endregion
    }
}
