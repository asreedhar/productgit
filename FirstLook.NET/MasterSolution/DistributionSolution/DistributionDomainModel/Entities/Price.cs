using System.Configuration;
using System.Globalization;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Price information that is a part of the body of the message to be published.
    /// </summary>
    public class Price : IBodyPart
    {
        #region Properties

        /// <summary>
        /// Get the type of this body part.
        /// </summary>
        public BodyPartType BodyPartType
        {
            get { return BodyPartType.Price; }
        }

        /// <summary>
        /// Determine what kind of messages this body part supports.
        /// </summary>                
        public MessageTypes MessageTypes
        {
            get
            {
                MessageTypes messageTypes = MessageTypes.None;

                switch (PriceType)
                {
                    // All price types are the same right now.
                    case PriceType.Internet:
                    case PriceType.Lot:
                    case PriceType.Appraisal:
                    case PriceType.NotDefined:
                        messageTypes |= MessageTypes.Price;
                        break;
                }

                return messageTypes;
            }
        }

        /// <summary>
        /// Type of the price; for example: internet, lot, etc.
        /// </summary>                
        public PriceType PriceType { get; private set; }

        /// <summary>
        /// Value of the price.
        /// </summary>
        public int Value { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this price object with the given value of the given type. Protected internal to force object 
        /// creation to go through factory methods.
        /// </summary>
        /// <param name="priceType">Type of the price.</param>
        /// <param name="value">Price value.</param>
        protected internal Price(PriceType priceType, int value)
        {
            PriceType = priceType;
            Value = value;
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new price object from the values of the given transfer object.
        /// Validates the following conditions:
        /// - Price must not be above $999,999.
        /// - Price must not be below $0.
        /// - Price must be of a defined type.
        /// </summary>
        /// <param name="price">Price transfer object.</param>
        /// <returns>Price object.</returns>
        internal static Price NewPrice(TransferObjects.V_1_0.Price price)
        {
            string maxString = ConfigurationManager.AppSettings["MaximumPrice"];
            int maxPrice = int.Parse(maxString, CultureInfo.InvariantCulture);

            string minString = ConfigurationManager.AppSettings["MinimumPrice"];
            int minPrice = int.Parse(minString, CultureInfo.InvariantCulture);

            if (price.Value > maxPrice)
            {
                throw new PriceHighException();
            }
            if (price.Value < minPrice)
            {
                throw new PriceLowException();
            }
            if (price.PriceType == TransferObjects.V_1_0.PriceType.NotDefined)
            {
                throw new UnknownPriceTypeException();
            }

            return new Price((PriceType) price.PriceType, price.Value);
        }

        #endregion
    }
}
