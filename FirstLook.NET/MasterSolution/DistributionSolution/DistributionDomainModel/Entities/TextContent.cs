namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// A piece of text content.
    /// </summary>
    public class TextContent : Content
    {
        #region Properties

        /// <summary>
        /// Get the type of this content.
        /// </summary>
        public override ContentType ContentType
        {
            get { return ContentType.Text; }
        }

        /// <summary>
        /// Text that defines this content.
        /// </summary>
        public string Text { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this object with the given text.
        /// </summary>
        /// <param name="text">Text content.</param>
        protected internal TextContent(string text)
        {            
            Text = text;
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new piece of text content from the given text.
        /// </summary>
        /// <remarks>
        /// This used to throw a TextEmptyException on empty text being passed in - this is no longer the case. Empty
        /// descriptions are now allowed. FOGBUGZ 11256.
        /// </remarks>
        /// <param name="textContent">Text content transfer object.</param>
        /// <returns>Text content object.</returns>
        internal static TextContent NewTextContent(TransferObjects.V_1_0.TextContent textContent)
        {
            if (string.IsNullOrEmpty(textContent.Text) || textContent.Text.Trim().Length == 0)
            {
                // throw new TextEmptyException();
                return new TextContent(string.Empty);
            }

            return new TextContent(textContent.Text);
        }

        #endregion
    }
}
