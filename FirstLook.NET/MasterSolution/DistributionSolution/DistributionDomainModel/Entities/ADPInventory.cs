﻿
namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Inventory details for ADP
    /// </summary>
    public class ADPInventory
    {
        public string DealerId { get; set; }

        public string StockNumber { get; set; }

        public string Vin { get; set; }

    }
}
