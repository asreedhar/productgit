namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Interface for accounts.
    /// </summary>
    public interface IAccount
    {
        /// <summary>
        /// Dealer who has an account with a provider.
        /// </summary>
        int DealerId { get; }

        /// <summary>
        /// Provider that the dealer has an account with.
        /// </summary>
        int ProviderId { get; }

        /// <summary>
        /// The username for a dealer with a provider.
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        /// The password for a dealer with a provider.
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// Dealership code to the provider.
        /// </summary>
        string DealershipCode { get; set; }

        /// <summary>
        /// Is the account active with the provider?
        /// </summary>
        bool Active { get; set; }
    }
}