﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    public class AppraisalValue: IBodyPart
    {
        #region Properties

        /// <summary>
        /// Get the type of this body part.
        /// </summary>
        public BodyPartType BodyPartType
        {
            get { return BodyPartType.AppraisalValue; }
        }

        /// <summary>
        /// Determine what kind of messages this body part supports.
        /// </summary>                
        public MessageTypes MessageTypes
        {
            get
            {
                return MessageTypes.AppraisalValue;
            }
        }

       
        /// <summary>
        /// Value of the price.
        /// </summary>
        public int Value { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this price object with the given value of the given type. Protected internal to force object 
        /// creation to go through factory methods.
        /// </summary>
        /// <param name="value">Price value.</param>
        protected internal AppraisalValue(int value)
        {
            Value = value;
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new appraisal Value object from the values of the given transfer object.
        /// Validates the following conditions:
        /// - appraisal Value must not be above $999,999.
        /// - appraisal Value must not be below $0.
        /// </summary>
        /// <param name="appraisalValue">AppraisalValue transfer object.</param>
        /// <returns>AppraisalValue object.</returns>
        internal static AppraisalValue NewAppraisalValue(TransferObjects.V_1_0.AppraisalValue appraisalValue)
        {
            string maxString = ConfigurationManager.AppSettings["MaximumAppraisalValue"];
            int maxAppraisalValue = int.Parse(maxString, CultureInfo.InvariantCulture);

            string minString = ConfigurationManager.AppSettings["MinimumAppraisalValue"];
            int minAppraisalValue = int.Parse(minString, CultureInfo.InvariantCulture);

            if (appraisalValue.Value > maxAppraisalValue)
            {
                throw new PriceHighException();
            }
            if (appraisalValue.Value < minAppraisalValue)
            {
                throw new PriceLowException();
            }

            return new AppraisalValue(appraisalValue.Value);
        }

        #endregion
    }
}
