using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Abstract base class for advertisement content.
    /// </summary>
    public abstract class Content
    {
        #region Properties

        /// <summary>
        /// Content type.
        /// </summary>
        public abstract ContentType ContentType { get; }        

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Factory method for creating new pieces of content from a transfer object.    
        /// Validates the following conditions:
        /// - Content must be of type 'text' (since nothing else is supported yet).    
        /// </summary>
        /// <param name="content">Content transfer object.</param>
        /// <returns>Content object of derived type.</returns>
        internal static Content NewContent(TransferObjects.V_1_0.Content content)
        {
            switch (content.ContentType)
            {
                // Text content.
                case TransferObjects.V_1_0.ContentType.Text:                    
                    return TextContent.NewTextContent((TransferObjects.V_1_0.TextContent)content);

                // Unrecognized.
                default:
                    throw new UnknownContentTypeException();
            }
        }

        #endregion
    }
}
