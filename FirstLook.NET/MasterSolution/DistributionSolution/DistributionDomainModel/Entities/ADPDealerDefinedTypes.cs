﻿
namespace FirstLook.Distribution.DomainModel.Entities
{
    public enum ADPDealerDefinedTypes
    {
        DealerDefined1 = 1,
        DealerDefined2 = 2,
        DealerDefined3 = 3,
        DealerDefined4 = 4,
        DealerDefined5 = 5,
        DealerDefined6 = 6,
        DealerDefined7 = 7,
        DealerDefined8 = 8    
    }
}
