namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Enumeration for available price types.
    /// </summary>
    public enum PriceType
    {
        /// <summary>
        /// Undefined, unrecognized price type.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// Lot price.
        /// </summary>
        Lot = 1,

        /// <summary>
        /// Internet offer price.
        /// </summary>
        Internet = 2,

        /// <summary>
        /// Appraisal
        /// </summary>
        Appraisal = 3
    }
}
