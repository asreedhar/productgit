using System.Configuration;
using System.Globalization;
using FirstLook.Distribution.DomainModel.Exceptions;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// Vehicle information that is a part of the body of the message to be published.
    /// </summary>
    public class VehicleInformation : IBodyPart
    {
        #region Properties

        /// <summary>
        /// Get the type of this body part.
        /// </summary>
        public BodyPartType BodyPartType
        {
            get { return BodyPartType.VehicleInformation; }
        }

        /// <summary>
        /// Determine what kind of messages this body part supports.
        /// </summary>                
        public MessageTypes MessageTypes
        {
            get
            {
                if (_mileage.HasValue)
                {
                    return MessageTypes.Mileage;
                }

                // Vehicle info messages currently aren't used.
                return MessageTypes.None;
            }
        }

        /// <summary>
        /// Mileage of the vehicle being published.
        /// </summary>
        public int Mileage
        {
            get { return _mileage.HasValue ? _mileage.Value : 0; }
        }
        private readonly int? _mileage;

        #endregion

        #region Construction

        /// <summary>
        /// Populate this vehicle information with the given mileage. Protected internal to force object creation to 
        /// go through factory methods.
        /// </summary>
        /// <param name="mileage">Vehicle mileage.</param>
        protected internal VehicleInformation(int mileage)
        {
            _mileage = mileage;
        }

        #endregion

        #region V_1_0 Transfer Object Parsing

        /// <summary>
        /// Create a new vehicle information object from the given transfer object.
        /// Validates the following conditions:
        /// - Mileage must not be above 999,999 miles.
        /// - Mileage must not be below 0 miles.
        /// </summary>
        /// <param name="value">Vehicle information transfer object.</param>
        /// <returns>Vehicle information object.</returns>
        public static VehicleInformation NewVehicleInformation(TransferObjects.V_1_0.VehicleInformation value)
        {            
            string maxString = ConfigurationManager.AppSettings["MaximumMileage"];
            int maxMileage = int.Parse(maxString, CultureInfo.InvariantCulture);

            string minString = ConfigurationManager.AppSettings["MinimumMileage"];
            int minMileage = int.Parse(minString, CultureInfo.InvariantCulture);

            if (value.Mileage > maxMileage)
            {
                throw new MileageHighException();
            }
            if (value.Mileage < minMileage)
            {
                throw new MileageLowException();
            }

            return new VehicleInformation(value.Mileage);
        }

        #endregion       
    }
}
