using System.Security.Cryptography;

namespace FirstLook.Distribution.DomainModel.Entities
{
    /// <summary>
    /// The credentials for a dealer to a provider.
    /// </summary>
    public class Account : Entity, IAccount
    {
        #region Properties

        /// <summary>
        /// Identifier of the dealer whose account information this is.
        /// </summary>
        public int DealerId { get; private set; }

        /// <summary>
        /// Identifier of the provider the dealer will connect to.
        /// </summary>
        public int ProviderId { get; private set; }

        /// <summary>
        /// Username for the dealer with the provider.
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set
            {
                if (!Equals(UserName, value))
                {
                    Assignment(delegate { _userName = value; }, "UserName");
                }
            }
        }
        private string _userName;

        /// <summary>
        /// Password for the dealer with the provider.
        /// </summary>
        public string Password
        {
            get { return _password; }
            set
            {
                if (!Equals(Password, value))
                {
                    Assignment(delegate { _password = value; }, "Password");
                }
            }
        }
        private string _password;

        /// <summary>
        /// Dealership identifier to the provider.
        /// </summary>
        public string DealershipCode
        {
            get { return _dealershipCode; }
            set
            {
                if (!Equals(DealershipCode, value))
                {
                    Assignment(delegate { _dealershipCode = value; }, "DealershipCode");
                }
            }
        }
        private string _dealershipCode;

        /// <summary>
        /// Whether the account is currently active or not.
        /// </summary>
        public bool Active
        {
            get { return _active; }
            set
            {
                if (!Equals(Active, value))
                {
                    Assignment(delegate { _active = value; }, "Active");
                }
            }
        }
        private bool _active;

        #region ADPProperties
        public int TargetId { get; private set; }

        public int TargetTypeId { get; private set; }

        public string Description { get; private set; }

        public string SelectedValue { get; private set; }

        #endregion
        #endregion

        #region Construction

        /// <summary>
        /// Populate this account with the given provider and dealer identifiers. Private to force object creation to
        /// go through factory methods.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>        
        private Account(int providerId, int dealerId)
        {
            ProviderId = providerId;
            DealerId = dealerId;

            MarkNew();
        }

        /// <summary>
        /// Populate this account with the values of an existing account. Private to force object creation to go 
        /// through factory methods.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="userName">Username for the dealer to the provider.</param>
        /// <param name="password">Password for the dealer to the provider.</param>
        /// <param name="active">Whether the account is currently active or not.</param>
        /// <param name="dealershipCode">Dealership code to the provider.</param>
        private Account(int providerId, int dealerId, string userName, string password, bool active, string dealershipCode)
        {
            ProviderId = providerId;
            DealerId = dealerId;
            _userName = userName;
            _password = password;
            _active = active;
            _dealershipCode = dealershipCode;

            MarkOld();
        }


        /// <summary>
        /// New Account constructor for ADP as it requires stockNo
        /// </summary>
        /// <param name="targetId">ADP Target field configured for a dealer</param>
        /// <param name="targetTypeId">Field Type(dealer defined or Price type)</param>
        /// <param name="description">Description of Target</param>
        /// <param name="selectedValue">Dropdown selected value</param>
        private Account(int targetId, int targetTypeId, string description, string selectedValue)
        {
            this.TargetId = targetId;
            this.Description = description;
            this.TargetTypeId = targetTypeId;
            this.SelectedValue = selectedValue;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a new account for the given provider and dealer identifiers.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <returns>Account object.</returns>
        public static Account NewAccount(int providerId, int dealerId)
        {
            return new Account(providerId, dealerId);
        }

        /// <summary>
        /// Create an object with the values of an existing account from the given parameters.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="providerId">Provider identifier.</param>
        /// <param name="userName">Username for the dealer to the provider.</param>
        /// <param name="password">Password for the dealer to the provider.</param>
        /// <param name="active">Whether the account is currently active or not.</param>
        /// <param name="dealershipCode">Dealership code to the provider.</param>
        /// <returns>Account object.</returns>
        public static Account GetAccount(int dealerId, int providerId, string userName, string password, bool active, string dealershipCode)
        {
            return new Account(providerId, dealerId, userName, password, active, dealershipCode);
        }

        public static Account GetAccount(int targetId, int targetTypeId, string description, string selectedValue)
        {
            return new Account(targetId, targetTypeId, description,selectedValue);
        }
        #endregion
    }
}
