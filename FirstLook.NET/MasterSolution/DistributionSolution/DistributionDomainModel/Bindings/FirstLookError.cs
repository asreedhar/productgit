using System;

namespace FirstLook.Distribution.DomainModel.Bindings
{
    /// <summary>
    /// Translation of provider-specific errors into a common, FirstLook format.
    /// </summary>
    public class FirstLookError
    {
        #region Properties

        /// <summary>
        /// Identifier of this error.
        /// </summary>
        public int ErrorId { get; protected set; }

        /// <summary>
        /// Identifier of the submission that received an error.
        /// </summary>
        public int SubmissionId { get; protected set; }

        /// <summary>
        /// Type of the error the submission ran into.
        /// </summary>
        public FirstLookErrorType FirstLookErrorType { get; protected set; }

        /// <summary>
        /// String message for the error type.
        /// </summary>
        public string FirstLookErrorMessage { get; protected set; }

        /// <summary>
        /// Does this error concern the user?
        /// </summary>
        public bool ConcernsUser { get; protected set; }

        /// <summary>
        /// Types of messages that failed.
        /// </summary>
        public int MessageType { get; protected set; }

        /// <summary>
        /// When did this error happen?
        /// </summary>
        public DateTime OccurredOn { get; protected set; }

        #endregion

        #region Construction

        /// <summary>
        /// Create a FirstLook error. To be used by the world-at-large.
        /// </summary>
        /// <param name="errorId">Error identifier.</param>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="firstLookErrorType">Error type.</param>
        /// <param name="firstLookErrorMessage">String message of the error type.</param>
        /// <param name="messageType">Message type that failed.</param>        
        /// <param name="concernsUser">Does this concern the user?</param>
        /// <param name="occurredOn">When the error occurred.</param>
        internal FirstLookError(int errorId, int submissionId, FirstLookErrorType firstLookErrorType, 
                                string firstLookErrorMessage, bool concernsUser, int messageType, DateTime occurredOn)
        {
            ErrorId               = errorId;
            SubmissionId          = submissionId;
            FirstLookErrorType    = firstLookErrorType;
            FirstLookErrorMessage = firstLookErrorMessage;
            MessageType           = messageType;
            OccurredOn            = occurredOn;
            ConcernsUser          = concernsUser;
        }

        /// <summary>
        /// Create a FirstLook error. To be used by derived classes.
        /// </summary>
        /// <param name="submissionId">Submission identifier.</param>
        /// <param name="messageType">Message type that failed.</param>        
        protected FirstLookError(int submissionId, int messageType)
        {
            SubmissionId = submissionId;
            MessageType  = messageType;
        }        

        #endregion
    }
}