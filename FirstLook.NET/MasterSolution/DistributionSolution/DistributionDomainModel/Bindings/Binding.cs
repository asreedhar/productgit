using System;
using System.Collections.ObjectModel;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Bindings
{
    /// <summary>
    /// Abstract base class defining a binding to a provider.
    /// </summary>    
    public abstract class Binding : IBinding
    {
        /// <summary>
        /// The types of messages that this binding is capable of sending.
        /// </summary>
        public abstract Collection<MessageTypes> SupportedMessageTypes { get; }

        /// <summary>
        /// Publish a provider-specific message to a provider.
        /// </summary>        
        /// <param name="submission">Submission to give provider.</param>
        protected abstract void Submit(ISubmission submission);        

        /// <summary>
        /// Does the given submission have any message types that this binding is capable of sending?
        /// </summary>
        /// <param name="submission">Submission to check.</param>
        /// <returns>True if the submission can be sent by this binding, false otherwise.</returns>
        protected bool IsSubmissionSupported(ISubmission submission)
        {            
            foreach (MessageTypes supportedMessageTypes in SupportedMessageTypes)
            {
                if ((supportedMessageTypes & submission.MessageTypes) == supportedMessageTypes)
                {
                    return true;
                }
            }
            return false;
        }        

        /// <summary>
        /// Publish a submission of a message. Will verify that the submission is supported before continuing 
        /// processing.
        /// </summary>        
        /// <param name="submission">Submission to submit.</param>
        public void Publish(ISubmission submission)
        {
            try
            {
                // Does the binding support the message types of this submission?
                if (!IsSubmissionSupported(submission))
                {
                    submission.NotSupported();
                    return;
                }

                // Filter the stale message types. If the entire submission is stale, mark it as superseded.
                submission.FilterStaleTypes();                                               
                if (submission.MessageTypes == 0)
                {
                    submission.Superseded();
                    return;
                }

                // Process the submission.
                submission.InProgress();                
                Submit(submission);
            }
            catch (Exception exception)
            {
                submission.Rejected();

                IExceptionLogger logger = RegistryFactory.GetResolver().Resolve<IExceptionLogger>();
                logger.Record(exception, submission.Id);                
            }
        }
    }
}
