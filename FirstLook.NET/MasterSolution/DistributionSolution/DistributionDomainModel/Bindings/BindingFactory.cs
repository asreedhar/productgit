using System;
using FirstLook.Distribution.DomainModel.Bindings.ADP;
using FirstLook.Distribution.DomainModel.Bindings.AutoTrader;
using FirstLook.Distribution.DomainModel.Bindings.AutoUplink;
using FirstLook.Distribution.DomainModel.Bindings.eBiz;
using FirstLook.Distribution.DomainModel.Bindings.GetAuto;
using FirstLook.Distribution.DomainModel.Bindings.eLead;
using FirstLook.Distribution.DomainModel.Bindings.HomeNet;

namespace FirstLook.Distribution.DomainModel.Bindings
{
    /// <summary>
    /// Factory for getting a provider for the given identifier.
    /// </summary>
    public class BindingFactory : IBindingFactory
    {
        /// <summary>
        /// Get the binding for the provider with the given identifier. Maps provider identifiers directly to 
        /// BindingType values.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <returns>Binding to provider.</returns>
        public IBinding GetBinding(int providerId)
        {
            switch ((BindingType)providerId)
            {
                // AutoTrader.
                case BindingType.AutoTrader:
                    return new AutoTraderBinding();

                // AutoUplink / Aultec.
                case BindingType.Aultec:
                case BindingType.AutoUplink:
                    return new AutoUplinkBinding();

                // GetAuto.
                case BindingType.GetAuto:
                    return new GetAutoBinding();

                // eBiz.
                case BindingType.eBiz:
                    return new eBizBinding();

                // eLead.
                case BindingType.eLead:
                    return new eLeadBinding();

                //ADP
                case BindingType.ADP:
                    return new ADPBinding();

                //ADP
                case BindingType.HomeNet:
                    return new HomeNetBinding();

                // Unrecognized.
                default:
                    throw new ArgumentException("Unrecognized provider identifier.");
            }
        }
    }
}
