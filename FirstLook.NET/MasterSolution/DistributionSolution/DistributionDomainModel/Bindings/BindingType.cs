namespace FirstLook.Distribution.DomainModel.Bindings
{
    /// <summary>
    /// Enumeration of supported provider bindings.
    /// </summary>
    public enum BindingType
    {
        /// <summary>
        /// Unrecognized binding.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// Binding to AutoTrader.
        /// </summary>
        AutoTrader = 1,

        /// <summary>
        /// Binding to AutoUplink.
        /// </summary>
        AutoUplink = 2,

        /// <summary>
        /// Binding to Aultec. Goes to same binding as AutoUplink.
        /// </summary>
        Aultec = 3,

        /// <summary>
        /// Binding to GetAuto.
        /// </summary>
        GetAuto = 4,

        /// <summary>
        /// Binding to eBiz.
        /// </summary>
        eBiz = 5,

        /// <summary>
        /// Binding to eLead
        /// </summary>
        eLead = 6,

        /// <summary>
        /// Binding to ADP
        /// </summary>
        ADP = 7,

        /// <summary>
        /// Binding to HomeNet
        /// </summary>
        HomeNet = 8
    }
}
