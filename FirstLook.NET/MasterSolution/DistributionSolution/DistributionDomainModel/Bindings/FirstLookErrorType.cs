namespace FirstLook.Distribution.DomainModel.Bindings
{
    /// <summary>
    /// Types of errors that may be returned from a provider.
    /// </summary>
    public enum FirstLookErrorType
    {
        /// <summary>
        /// Unrecognized error type.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// Generic rejection.
        /// </summary>
        GenericRejection = 1,

        /// <summary>
        /// Generic system level error.
        /// </summary>
        SystemError = 2,

        /// <summary>
        /// The field to be updated was locked by the provider.
        /// </summary>
        FieldLocked = 3,

        /// <summary>
        /// Field has the same value.
        /// </summary>
        FieldSameValue = 4,

        /// <summary>
        /// Authentication failed with the provider.
        /// </summary>
        AuthenticationError = 5,
        
        /// <summary>
        /// Vehicle was not found in the provider's system.
        /// </summary>
        VehicleNotFound = 6,

        /// <summary>
        /// Vehicle had been deleted in the provider's system.
        /// </summary>
        VehicleDeleted = 7,

        /// <summary>
        /// User is not authorized to perform the given action.
        /// </summary>
        NotAuthorized = 8,

        /// <summary>
        /// Dealer was not found in the provider's system.
        /// </summary>
        DealerNotFound = 9,

        /// <summary>
        /// Dealer was not tied to the vehicle with the given VIN in the provider's system.
        /// </summary>
        DealerVinMismatch = 10,

        /// <summary>
        /// Something the provider required was not present.
        /// </summary>
        MissingField = 11,

        /// <summary>
        /// A value given to the provider was not valid.
        /// </summary>
        InvalidField = 12,
    }
}