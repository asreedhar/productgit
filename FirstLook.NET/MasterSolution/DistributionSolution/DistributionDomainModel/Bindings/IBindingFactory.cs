namespace FirstLook.Distribution.DomainModel.Bindings
{
    /// <summary>
    /// Interface for a binding factory. Abstracted out in this way to allow for mock implementations.
    /// </summary>
    public interface IBindingFactory
    {
        /// <summary>
        /// Get the binding for the provider with the given identifier.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        /// <returns>Provider binding</returns>
        IBinding GetBinding(int providerId);
    }
}
