﻿
namespace FirstLook.Distribution.DomainModel.Bindings.eLead
{
    /// <summary>
    /// TODO : Update Class after eLead provides details of response
    /// </summary>
    public class eLeadError:FirstLookError
    {
         #region Properties

        /// <summary>
        /// Type of this eLead error.
        /// </summary>
        public eLeadErrorType eLeadErrorType { get; private set; }

        /// <summary>
        /// The xml that was returned by the eLead service.
        /// </summary>
        public string ErrorMessage { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this error object with the correct error type values.
        /// </summary>
        /// <param name="submissionId">Submission that had the error.</param>
        /// <param name="messageType">Message type that failed.</param>
        /// <param name="errorMesage">Error message returned from eLead.</param>
        public eLeadError(int submissionId, int messageType, string errorMesage) : 
            base(submissionId, messageType)
        {
            if (errorMesage == null)
            {
                errorMesage = string.Empty;
            }

            ErrorMessage = errorMesage;

            // The vin was not found on an active inventory vehicle for the specified dealer
            if (errorMesage.Contains("vehicle not found"))
            {
                eLeadErrorType = eLeadErrorType.VehicleNotFound;
                FirstLookErrorType = FirstLookErrorType.VehicleNotFound;
                ConcernsUser = true;
            }
            // The DealerId does not match any in eLead system
            else if (errorMesage.Contains("invalid dealer"))
            {
                eLeadErrorType = eLeadErrorType.InvalidDealer;
                FirstLookErrorType = FirstLookErrorType.NotAuthorized;
                ConcernsUser = true;
            }
            // Bad credentials were sent for the dealer.
            else if (errorMesage.Contains("not authenticated"))
            {
                eLeadErrorType = eLeadErrorType.NotAuthenticated;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            // Could not parse the AppraisalValue parameter as a decimal
            else if (errorMesage.Contains("invalid appraisal value"))
            {
                eLeadErrorType = eLeadErrorType.InvalidAppraisalValue;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            //The message body was not valid json object
            else if (errorMesage.Contains("malformed json"))
            {
                eLeadErrorType = eLeadErrorType.MalformedJson;
                FirstLookErrorType = FirstLookErrorType.SystemError;
                ConcernsUser = true;
            }
            // Other error
            else if (ErrorMessage.Contains("internal server error"))
            {
                eLeadErrorType = eLeadErrorType.InternalError;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            else if (ErrorMessage.Contains("Vin is required"))
            {
                eLeadErrorType = eLeadErrorType.InternalError;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            else if (ErrorMessage.Contains("DealerId is required"))
            {
                eLeadErrorType = eLeadErrorType.InternalError;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            else if (ErrorMessage.Contains("AppraisalValue is required"))
            {
                eLeadErrorType = eLeadErrorType.InternalError;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            // Unrecognized.
            else
            {
                eLeadErrorType = eLeadErrorType.Unrecognized;
                FirstLookErrorType = FirstLookErrorType.Unrecognized;
                ConcernsUser = true;
            }
        }

        #endregion
    }
}
