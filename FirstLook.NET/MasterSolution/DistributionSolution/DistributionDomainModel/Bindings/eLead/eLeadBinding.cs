﻿using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Bindings.eLead
{
    /// <summary>
    /// Binding class for eLead DMS system
    /// </summary>
    public class eLeadBinding : Binding
    {
        #region Properties

        //private IELeadService _webService;
        /// <summary>
        /// Collection of Message types supported for Distribution by eLead
        /// </summary>
        public override System.Collections.ObjectModel.Collection<Entities.MessageTypes> SupportedMessageTypes
        {
            get { return _supportedMessageTypes; }
        }
        /// <summary>
        /// Message type that eLead binding is supported to Send
        /// </summary>
        private readonly Collection<MessageTypes> _supportedMessageTypes;

        /// <summary>
        /// Data Gateway for Account
        /// </summary>
        private readonly IAccountDataGateway _accountDataGateway;

        /// <summary>
        /// Data Gateway for Publilcations
        /// </summary>
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data Gateway for Submission
        /// </summary>
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Data Gateway for Inventory
        /// </summary>
        private readonly IAppraisedVehicleDataGateway _appraisedVehicleDataGateway;
        #endregion

        #region Constructor
        public eLeadBinding()
        {
            _supportedMessageTypes = new Collection<MessageTypes>();
            _supportedMessageTypes.Add(MessageTypes.AppraisalValue);

            IResolver resolver = RegistryFactory.GetResolver();

            //_webService = resolver.Resolve<IELeadService>();
            _accountDataGateway = resolver.Resolve<IAccountDataGateway>();
            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _submissionDataGateway = resolver.Resolve<ISubmissionDataGateway>();
            _appraisedVehicleDataGateway = resolver.Resolve<IAppraisedVehicleDataGateway>();
        }
        #endregion
        
        #region Methods

        #region SubmitMethods
        /// <summary>
        /// Submit eLead Submissions to eLead web service
        /// </summary>
        /// <param name="submission">Submissions to be submitted to eLead service</param>
        protected override void Submit(ISubmission submission)
        {
            // Message parts to be sent.
            Message message = _publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);
            AppraisalValue appraisalValue = message.GetAppraisalValue();


            // Vehicle details.
            //Inventory calls has Vehicle details, after discussion we decided to Use same inventory class instead of creating new one.
            Inventory vehicleDeails = _appraisedVehicleDataGateway.FetchVehicle(message.Vehicle.VehicleEntityId);
            
            // Account credentials.
            string id =ConfigurationManager.AppSettings["eLeadId"];
            string auth = ConfigurationManager.AppSettings["eLeadPassword"];
            string eLeadURI = ConfigurationManager.AppSettings["eLeadURI"];
            

            // Mark the submission as sent.
            submission.Submitted();

            bool success = true;

            // Send Appraisal Value to eLead!
            if (appraisalValue != null)
            {

                WebRequest request = WebRequest.Create(eLeadURI);

                string jsonRequest = GeneratePriceRequestJson(vehicleDeails.Vin, appraisalValue.Value, message.Dealer.Id);
               
                request.Headers.Add("Username", id);
                request.Headers.Add("Password", auth);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = jsonRequest.Length;
                StreamWriter stream = new StreamWriter(request.GetRequestStream());
                stream.Write(jsonRequest);
                stream.Flush();
                stream.Close();
                
                WebResponse response =request.GetResponse();

                StreamReader streamReader = new StreamReader(response.GetResponseStream());

                string responseData = streamReader.ReadToEnd();

                if (IsSuccess(responseData))
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.AppraisalValue);
                }
                else
                {
                    _submissionDataGateway.LogError(new eLeadError(submission.Id, (int)MessageTypes.AppraisalValue, responseData));
                    success = false;
                }

                response.Close();
                streamReader.Close();
            }


            // Mark the submission.
            if (success)
            {
                submission.Accepted();
            }
            else
            {
                submission.Rejected();
            }
        }

        #endregion

        #region UtilityMethods
        /// <summary>
        /// Test response from web service for success message
        /// </summary>
        /// <param name="response">Response text received from web service</param>
        /// <returns>True if</returns>
        protected bool IsSuccess(string response)
        {
            return response.Contains("success");
        }

        private string GeneratePriceRequestJson(string vin, float price, int dealerId)
        {
            Dealer dealer=_accountDataGateway.GetDealer(dealerId);

            string json = "{\"Vin\":\"" + vin + "\"," +
              "\"DealerId\":\""+dealer.DealerCode+"\"," +
              "\"AppraisalValue\":\"" + price + "\"}";

            return json;
            
        }

        #endregion
        #endregion
    }
}
