﻿
namespace FirstLook.Distribution.DomainModel.Bindings.eLead
{
    //TODO : Update Class after eLead provides details of response
    /// <summary>
    /// Types of eBiz-related errors.
    /// </summary>
    public enum eLeadErrorType
    {
        /// <summary>
        /// The vin was not found on an active inventory vehicle for the specified dealer.
        /// </summary>
        VehicleNotFound = 0,

        /// <summary>
        /// The DealerId does not match any in our system
        /// </summary>
        InvalidDealer = 1,

        /// <summary>
        /// Check to make sure you are posting the correct user name and password
        /// </summary>
        NotAuthenticated = 2,

        /// <summary>
        /// Could not parse the AppraisalValue parameter as a decimal
        /// </summary>
        InvalidAppraisalValue = 3,

        /// <summary>
        /// The message body could not be parsed as a valid json object
        /// </summary>
        MalformedJson = 4,

        /// <summary>
        /// An internal error has occured.
        /// </summary>
        InternalError = 5,

        /// <summary>
        /// The Vin parameter is missing
        /// </summary>
        VinRequired = 6,

        /// <summary>
        ///The DealerId parameter is missing
        /// </summary>
        DealerIdRequired = 7,


        /// <summary>
        /// The AppraisalValue parameter is missing
        /// </summary>
        AppraisalValueRequired = 8,

        /// <summary>
        /// Unrecognized error
        /// </summary>
        Unrecognized = 9,

    }
}
