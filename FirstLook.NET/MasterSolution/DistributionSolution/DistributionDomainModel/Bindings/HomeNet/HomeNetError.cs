﻿
namespace FirstLook.Distribution.DomainModel.Bindings.HomeNet
{
    /// <summary>
    /// TODO : Update Class after HomeNet provides details of response
    /// </summary>
    public class HomeNetError : FirstLookError
    {
        public string ErrorMessage { get; private set; }

        public HomeNetErrorType HomeNetErrorType { get; private set; }

        public HomeNetError(int submissionId, int messageType, string exception, HomeNetErrorType homeNetErrorType) :
            base(submissionId, messageType)
        {
            FirstLookErrorType = FirstLookErrorType.SystemError;
            HomeNetErrorType = homeNetErrorType;
            ErrorMessage = exception;
            ConcernsUser = true;
        }
    }
}
