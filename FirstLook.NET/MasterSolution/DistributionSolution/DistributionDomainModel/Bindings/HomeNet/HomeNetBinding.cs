﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.com.homenetiol.services;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Bindings.HomeNet
{
    /// <summary>
    /// Binding class for HomeNet DMS system
    /// </summary>
    public class HomeNetBinding : Binding
    {
        #region Properties

        /// <summary>
        /// Collection of Message types supported for Distribution by HomeNet
        /// </summary>
        public override Collection<MessageTypes> SupportedMessageTypes
        {
            get { return _supportedMessageTypes; }
        }
        /// <summary>
        /// Message type that HomeNEt binding is supported to Send
        /// </summary>
        private readonly Collection<MessageTypes> _supportedMessageTypes;

        /// <summary>
        /// Data Gateway for Publilcations
        /// </summary>
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data Gateway for Submission
        /// </summary>
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Data Gateway for Inventory
        /// </summary>
        private readonly IVehicleDataGateway _vehicleDataGateway;

        /// <summary>
        /// Data gateway for accounts.
        /// </summary>
        private readonly IAccountDataGateway _accountDataGateway;
        #endregion

        #region Constructor
        public HomeNetBinding()
        {
            _supportedMessageTypes = new Collection<MessageTypes> { MessageTypes.Price, MessageTypes.Description };

            var resolver = RegistryFactory.GetResolver();
            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _submissionDataGateway = resolver.Resolve<ISubmissionDataGateway>();
            _vehicleDataGateway = resolver.Resolve<IVehicleDataGateway>();
            _accountDataGateway = resolver.Resolve<IAccountDataGateway>();
        }
        #endregion

        #region Methods

        #region SubmitMethods
        /// <summary>
        /// Submit HomeNet Submissions to HomeNet web service
        /// </summary>
        /// <param name="submission">Submissions to be submitted to eLead service</param>
        protected override void Submit(ISubmission submission)
        {


            // Message parts to be sent.
            var message = _publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);

            var price = message.GetPrice();
            var description = message.GetDescription();

            // Vehicle details.
            var inventory = _vehicleDataGateway.FetchInventory(message.Vehicle.VehicleEntityId);


            var account = _accountDataGateway.Fetch(submission.ProviderId, message.Dealer.Id);
            if (string.IsNullOrEmpty(account.DealershipCode))
                throw new DataException("Expected dealership code for HomeNet.");
            var dealerKey = account.DealershipCode;

            // Account credentials.
            var userName = ConfigurationManager.AppSettings["HomeNetUserName"];
            var password = ConfigurationManager.AppSettings["HomeNetPassword"];
            var integrationToken = ConfigurationManager.AppSettings["HomeNetIntegrationToken"];

            // Mark the submission as sent.
            submission.Submitted();

            var success = false;

            try
            {
                InventoryUpdateResponse response;
                using (var gateway = new Gateway())
                {
                    var vehicle = new VehicleUpdateRequest { VIN = inventory.Vin };
                    var requests = new List<VehicleFieldUpdateRequest>();
                    if (price != null)
                    {
                        var updateRequest = new VehicleFieldUpdateRequest
                                            {
                                                Field = "sellingprice",
                                                Value = Convert.ToString(price.Value)
                                            };
                        requests.Add(updateRequest);
                    }
                    if (description != null)
                    {
                        var updateRequest = new VehicleFieldUpdateRequest { Field = "description2", Value = description.Text };
                        requests.Add(updateRequest);
                    }
                    vehicle.FieldsToUpdate = requests.ToArray();
                    response = gateway.UpdateInventory(integrationToken, dealerKey, userName, password, new[] { vehicle });
                }
                success = response.IsSuccess && response.Result == InventoryUpdateResponseKind.OK;
                if (!success)
                    _submissionDataGateway.LogError(new HomeNetError(submission.Id, (int)MessageTypes.Price, response.Result.ToString(), (HomeNetErrorType)response.Result));

            }
            catch (Exception ex)
            {
                _submissionDataGateway.LogError(new HomeNetError(submission.Id, (int)MessageTypes.Price, ex.Message, HomeNetErrorType.UnknownError));
            }
            // Mark the submission.
            if (success)
                submission.Accepted();
            else
                submission.Rejected();
        }

        #endregion


        #endregion
    }
}

