﻿
namespace FirstLook.Distribution.DomainModel.Bindings.HomeNet
{
    //TODO : Update Class after eLead provides details of response
    /// <summary>
    /// Types of eBiz-related errors.
    /// </summary>
    public enum HomeNetErrorType
    {

        /// <remarks/>
        Ok,

        /// <remarks/>
        InvalidIntegrationToken,

        /// <remarks/>
        IntegrationTokenNotFound,

        /// <remarks/>
        InvalidPartner,

        /// <remarks/>
        InvalidDealerId,

        /// <remarks/>
        NoDealerFound,

        /// <remarks/>
        NoUserPermissionFound,

        /// <remarks/>
        NoVehiclesProvided,

        /// <remarks/>
        InvalidUser,

        /// <remarks/>
        UnknownError,
    }
}
