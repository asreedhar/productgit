﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Web.Services.Protocols;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Services.GetAuto;

namespace FirstLook.Distribution.DomainModel.Bindings.GetAuto
{
    /// <summary>
    /// Binding to the GetAuto web service.
    /// </summary>
    /// <remarks>
    /// Authentication is IP-based, so there are no credentials needed.
    /// Additionally, no specific dealers or vehicles were made available - instead we are to query their test service
    /// for dealers and vehicles to use.
    /// </remarks>
    public class GetAutoBinding : Binding
    {
        #region Properties

        /// <summary>
        /// Web service to connect to.
        /// </summary>
        private readonly IGetAutoService _webService;

        /// <summary>
        /// The types of messages that this binding is capable of sending.
        /// </summary>
        public override Collection<MessageTypes> SupportedMessageTypes
        {
            get { return _supportedMessageTypes; }
        }
        private readonly Collection<MessageTypes> _supportedMessageTypes;

        /// <summary>
        /// Data gateway for accounts.
        /// </summary>
        private readonly IAccountDataGateway _accountDataGateway;

        /// <summary>
        /// Data gateway for publication.
        /// </summary>
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data gateway for submissions.
        /// </summary>
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Date gateway for inventory.
        /// </summary>
        private readonly IVehicleDataGateway _vehicleDataGateway;

        #endregion

        #region Construction

        /// <summary>
        /// Populate the web service and data gateways from the registry, and set the types of supported messages.
        /// </summary>
        public GetAutoBinding()
        {            
            _supportedMessageTypes = new Collection<MessageTypes>();
            _supportedMessageTypes.Add(MessageTypes.Price);
            _supportedMessageTypes.Add(MessageTypes.Description);

            IResolver resolver = RegistryFactory.GetResolver();

            _webService             = resolver.Resolve<IGetAutoService>();
            ((SoapHttpClientProtocol)_webService).Url =
                Properties.Settings.Default.FirstLook_Distribution_DomainModel_pwstest_vehicledata_com_CvdApiWebService;
            _accountDataGateway     = resolver.Resolve<IAccountDataGateway>();
            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _submissionDataGateway  = resolver.Resolve<ISubmissionDataGateway>();
            _vehicleDataGateway     = resolver.Resolve<IVehicleDataGateway>();
        }

        #endregion

        #region Submission

        /// <summary>
        /// Publish a message to GetAuto.
        /// </summary>        
        /// <param name="submission">Submission to give GetAuto.</param>
        protected override void Submit(ISubmission submission)
        {            
            // Message parts to be sent.
            Message message = _publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);
            Price price = message.GetPrice();
            TextContent description = message.GetDescription();

            // Inventory details.
            Inventory inventory = _vehicleDataGateway.FetchInventory(message.Vehicle.VehicleEntityId);

            // Fetch account information.            
            Account account = _accountDataGateway.Fetch(submission.ProviderId, message.Dealer.Id);                       
            if (string.IsNullOrEmpty(account.DealershipCode))
            {
                throw new DataException("Expected dealership code for GetAuto.");
            }            
            int dealerKey = int.Parse(account.DealershipCode);
            int vehicleKey = FetchVehicleKey(dealerKey, inventory.Vin);

            bool success = true;

            // Mark the submission as sent.
            submission.Submitted();

            // Price!
            if (price != null)
            {
                VehicleInfo result = _webService.UpdateVehiclePrice(vehicleKey, price.Value, Services.GetAuto.PriceType.Price);
                
                if (result.Result == ReturnResult.Success)
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Price);
                }
                else
                {
                    _submissionDataGateway.LogError(new GetAutoError(submission.Id, (int)MessageTypes.Price, result.Result));
                    success = false;
                }
            }

            // Decsription!
            if (description != null)
            {                                               
                UpdateVehicleCommentsReturn result = _webService.UpdateVehicleComments(vehicleKey, description.Text);

                if (result.Result == ReturnResult.Success)
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Description);
                }
                else
                {
                    _submissionDataGateway.LogError(new GetAutoError(submission.Id, (int)MessageTypes.Description, result.Result));
                    success = false;
                }
            }

            // Mark the submission.
            if (success)
            {
                submission.Accepted();
            }
            else
            {
                submission.Rejected();
            }  
        }

        #endregion

        #region Utility

        /// <summary>
        /// Get the full list of vehicles for a dealer from GetAuto. Then parse the results to find the one with the 
        /// given VIN.
        /// </summary>
        /// <param name="dealerKey">Dealership identifier with GetAuto.</param>
        /// <param name="vin">VIN of the vehicle we wish to get an identifier for.</param>
        /// <returns>Vehicle identifier with GetAuto.</returns>
        private int FetchVehicleKey(int dealerKey, string vin)
        {            
            VehicleInfo[] vehicles = _webService.GetAllVehicles(dealerKey);

            if (vehicles != null)
            {
                foreach (VehicleInfo vehicle in vehicles)
                {
                    if (vehicle.Vin == vin)
                    {
                        return vehicle.VehicleKey;
                    }
                }
            }
            throw new ArgumentException("Could not find vehicle key for given VIN with GetAuto.");
        }

        #endregion
    }
}
