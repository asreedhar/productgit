﻿namespace FirstLook.Distribution.DomainModel.Bindings.GetAuto
{
    /// <summary>
    /// Types of GetAuto-related errors.
    /// </summary>
    public enum GetAutoErrorType
    {
        /// <summary>
        /// Unrecongized error type.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// Method call failed.
        /// </summary>
        MethodFailed = 1,

        /// <summary>
        /// Security-releated error.
        /// </summary>
        SecurityFailed = 2,

        /// <summary>
        /// Object is null.
        /// </summary>
        ObjectIsNull = 3,

        /// <summary>
        ///  Validation error.
        /// </summary>
        ValidationFailed = 4,

        /// <summary>
        /// Record not found.
        /// </summary>
        RecordNotFound = 5,
        
        /// <summary>
        /// Invalid dealer identifier.
        /// </summary>
        InvalidDealerLotKey = 6,

        /// <summary>
        /// Vehicle exists error.
        /// </summary>
        VehicleExists = 7,

        /// <summary>
        /// Missing required field.
        /// </summary>
        MissingRequiredField = 8,

        /// <summary>
        /// Type of vehicle not supported.
        /// </summary>
        VehicleTypeNotSupported = 9,

        /// <summary>
        /// Invalid token used when authenticating.
        /// </summary>
        InvalidAuthenticationToken = 10,

        /// <summary>
        /// Expired token for authentication.
        /// </summary>
        ExpiredAuthenticationToken = 11,

        /// <summary>
        /// Dealer does not have access to the feature.
        /// </summary>
        DealerNoFeature = 12,

        /// <summary>
        /// Vehicle does not have access to the feature.
        /// </summary>
        VehicleNoFeature = 13
    }
}
