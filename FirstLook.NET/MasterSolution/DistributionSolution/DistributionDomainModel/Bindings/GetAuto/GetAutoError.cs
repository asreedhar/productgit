﻿using FirstLook.Distribution.DomainModel.Services.GetAuto;

namespace FirstLook.Distribution.DomainModel.Bindings.GetAuto
{
    /// <summary>
    /// An GetAuto-specific error.
    /// </summary>    
    public class GetAutoError : FirstLookError
    {
        #region Properties

        /// <summary>
        /// Type of this GetAuto error.
        /// </summary>
        public GetAutoErrorType GetAutoErrorType { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this error object with the correct error type values.
        /// </summary>                
        public GetAutoError(int submissionId, int messageType, ReturnResult errorResult) : base(submissionId, messageType)
        {
            switch (errorResult)
            {
                // Method failed.
                case ReturnResult.MethodFailed:
                    GetAutoErrorType = GetAutoErrorType.MethodFailed;
                    FirstLookErrorType = FirstLookErrorType.SystemError;
                    break;

                // Object is null.
                case ReturnResult.ObjectIsNull:
                    GetAutoErrorType = GetAutoErrorType.ObjectIsNull;
                    FirstLookErrorType = FirstLookErrorType.GenericRejection;
                    break;

                // Validation failed.
                case ReturnResult.ValidationFailed:
                    GetAutoErrorType = GetAutoErrorType.ValidationFailed;
                    FirstLookErrorType = FirstLookErrorType.InvalidField;
                    break;

                // Record not found.
                case ReturnResult.RecordNotFound:
                    GetAutoErrorType = GetAutoErrorType.RecordNotFound;
                    FirstLookErrorType = FirstLookErrorType.InvalidField;
                    break;

                // Invalid dealer lot key.
                case ReturnResult.InvalidDealerLotKey:
                    GetAutoErrorType = GetAutoErrorType.InvalidDealerLotKey;
                    FirstLookErrorType = FirstLookErrorType.DealerNotFound;
                    break;

                // Vehicle exists.
                case ReturnResult.VehicleExists:
                    GetAutoErrorType = GetAutoErrorType.VehicleExists;
                    FirstLookErrorType = FirstLookErrorType.InvalidField;
                    break;

                // Required.
                case ReturnResult.Required:
                    GetAutoErrorType = GetAutoErrorType.MissingRequiredField;
                    FirstLookErrorType = FirstLookErrorType.MissingField;
                    break;

                // Vehicle type not supported.
                case ReturnResult.VehicleTypeNotSupported:
                    GetAutoErrorType = GetAutoErrorType.VehicleTypeNotSupported;
                    FirstLookErrorType = FirstLookErrorType.InvalidField;
                    break;

                // Invalid authentication token.
                case ReturnResult.InvalidAuthenticationToken:
                    GetAutoErrorType = GetAutoErrorType.InvalidAuthenticationToken;
                    FirstLookErrorType = FirstLookErrorType.AuthenticationError;
                    break;

                // Expired authentication token.
                case ReturnResult.ExpiredAuthenticationToken:
                    GetAutoErrorType = GetAutoErrorType.ExpiredAuthenticationToken;
                    FirstLookErrorType = FirstLookErrorType.AuthenticationError;
                    break;

                // Dealer no feature.
                case ReturnResult.DealerNoFeature:
                    GetAutoErrorType = GetAutoErrorType.DealerNoFeature;
                    FirstLookErrorType = FirstLookErrorType.NotAuthorized;
                    break;

                // Vehicle no feature.
                case ReturnResult.VehicleNoFeature:
                    GetAutoErrorType = GetAutoErrorType.VehicleNoFeature;
                    FirstLookErrorType = FirstLookErrorType.NotAuthorized;
                    break;

                // Unrecognized.
                default:
                    GetAutoErrorType = GetAutoErrorType.Unrecognized;
                    FirstLookErrorType = FirstLookErrorType.Unrecognized;
                    break;
            }            
        }

        #endregion
    }
}
