﻿using System;
using System.Text.RegularExpressions;

namespace FirstLook.Distribution.DomainModel.Bindings.ADP
{
    public class ADPError : FirstLookError
    {
        #region Properties

        /// <summary>
        /// Type of this ADP error code.
        /// </summary>
        public int ADPErrorType { get; private set; }

        /// <summary>
        /// The xml that was returned by the eBiz service.
        /// </summary>
        public string ErrorMessage { get; private set; }

        public string Request { get; private set; }
        #endregion

        #region Construction

        /// <summary>
        /// Populate this error object with the correct error type values.
        /// </summary>
        /// <param name="submissionId">Submission that had the error.</param>
        /// <param name="messageType">Message type that failed.</param>
        /// <param name="request">Request that was sent</param>
        /// <param name="errorMesage">Error message returned from ADP</param>
        /// <param name="resultCode">Error Code returned from ADP</param>
        public ADPError(int submissionId, int messageType, string request, string soapMesage, string errorMessage) :
            base(submissionId, messageType)
        {
            if (request == null)
            {
                request = string.Empty;
            }

            if (errorMessage == null)
            {
                errorMessage = string.Empty;
            }

            string[] numbers = Regex.Split(errorMessage, @"\D+");

            ErrorMessage = soapMesage;
            Request = request;
            ADPErrorType = Convert.ToInt32(numbers[0]);
        }

        #endregion
    }
}
