﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Net;
using System.Web.Services.Protocols;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Extensions;
using FirstLook.Distribution.DomainModel.Services.ADP;

namespace FirstLook.Distribution.DomainModel.Bindings.ADP
{
    /// <summary>
    /// Binding to ADP web service
    /// </summary>
    public class ADPBinding : Binding
    {
        #region Properties

        /// <summary>
        /// eBiz web service.
        /// </summary>
        private readonly IADPService _webService;

        /// <summary>
        /// The types of messages that this binding is capable of sending.
        /// </summary>
        public override Collection<MessageTypes> SupportedMessageTypes
        {
            get { return _supportedMessageTypes; }
        }
        private readonly Collection<MessageTypes> _supportedMessageTypes;

        /// <summary>
        /// Data gateway for accounts.
        /// </summary>
        private readonly IAccountDataGateway _accountDataGateway;

        /// <summary>
        /// Data gateway for publication.
        /// </summary>
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data gateway for submissions.
        /// </summary>
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Date gateway for inventory.
        /// </summary>
        private readonly IVehicleDataGateway _vehicleDataGateway;

        #endregion

        #region Construction

        /// <summary>
        /// Populate the web service and data gateways from the registry, and set the types of supported messages.
        /// </summary>
        public ADPBinding()
        {
            _supportedMessageTypes = new Collection<MessageTypes>();
            _supportedMessageTypes.Add(MessageTypes.Price);
            _supportedMessageTypes.Add(MessageTypes.Description);

            IResolver resolver = RegistryFactory.GetResolver();

            _webService = resolver.Resolve<IADPService>();
            ((SoapHttpClientProtocol)_webService).Url =
                Properties.Settings.Default.FirstLook_Distribution_DomainModel_Services_ADP_VehicleSaveService;

            _accountDataGateway = resolver.Resolve<IAccountDataGateway>();
            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _submissionDataGateway = resolver.Resolve<ISubmissionDataGateway>();
            _vehicleDataGateway = resolver.Resolve<IVehicleDataGateway>();

        }

        #endregion

        #region Submission

        /// <summary>
        /// Publish a provider-specific message to a provider.
        /// </summary>        
        /// <param name="submission">Submission to give provider.</param>
        protected override void Submit(ISubmission submission)
        {
            // Message parts to be sent.
            Message message = _publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);
            Price price = message.GetPrice();

            // Vehicle details.
            ADPInventory inventory = _vehicleDataGateway.FetchADPInventory(message.Vehicle.VehicleEntityId, message.Dealer.Id);


            // Account credentials.
            string id = ConfigurationManager.AppSettings["ADPId"];
            string psw = ConfigurationManager.AppSettings["ADPPassword"];

            authenticationToken auth = new authenticationToken() { password = psw, username = id };

            Account account = _accountDataGateway.FetchADPAccount(message.Dealer.Id);

            // Mark the submission as sent.
            submission.Submitted();

            bool success = true;

            // Send price to eBiz!
            if (price != null)
            {
                try
                {
                    vehicleReadResult readResult = ReadVehicle(auth, inventory.DealerId, inventory.StockNumber, submission.Id,message.Dealer.Id);

                    if (readResult.code == resultCode.success)
                    {
                        if (account.TargetTypeId == (int)ADPFieldTypes.PriceType)
                        {
                            vehiclePrice[] submitPrice = new vehiclePrice[1];
                            submitPrice[0] = new vehiclePrice()
                                             {
                                                 price = price.Value,
                                                 vehiclePricingType = (vehiclePricingType)account.TargetId,
                                                 priceSpecified = true,
                                                 vehiclePricingTypeSpecified = true
                                             };
                            readResult.vehicle.vehicle1.vehiclePrice = submitPrice;
                        }
                        else
                        {
                            if (account.TargetTypeId == (int)ADPFieldTypes.DealerField)
                            {
                                switch (account.TargetId)
                                {
                                    case (int)ADPDealerDefinedTypes.DealerDefined1:
                                        readResult.vehicle.dealer.dealerDefined1 = price.Value.ToString();
                                        break;
                                    case (int)ADPDealerDefinedTypes.DealerDefined2:
                                        readResult.vehicle.dealer.dealerDefined2 = price.Value.ToString();
                                        break;
                                    case (int)ADPDealerDefinedTypes.DealerDefined3:
                                        readResult.vehicle.dealer.dealerDefined3 = price.Value.ToString();
                                        break;
                                    case (int)ADPDealerDefinedTypes.DealerDefined4:
                                        readResult.vehicle.dealer.dealerDefined4 = price.Value.ToString();
                                        break;
                                    case (int)ADPDealerDefinedTypes.DealerDefined5:
                                        readResult.vehicle.dealer.dealerDefined5 = price.Value.ToString();
                                        break;
                                    case (int)ADPDealerDefinedTypes.DealerDefined6:
                                        readResult.vehicle.dealer.dealerDefined6 = price.Value.ToString();
                                        break;
                                    case (int)ADPDealerDefinedTypes.DealerDefined7:
                                        readResult.vehicle.dealer.dealerDefined7 = price.Value.ToString();
                                        break;
                                    case (int)ADPDealerDefinedTypes.DealerDefined8:
                                        readResult.vehicle.dealer.dealerDefined8 = price.Value.ToString();
                                        break;
                                }

                            }
                        }

                        vehicleSaveResult vehicleSaveResult = _webService.update(auth,
                            new dealerId() { id = inventory.DealerId }, readResult.vehicle, fileType.CARINV, true);
                        new SubmissionDataGateway().LoggingADPSuccess(submission.Id, message.Dealer.Id, inventory.StockNumber, TraceSoapExtension.XmlRequest, TraceSoapExtension.XmlResponse);

                       
                        if (vehicleSaveResult.code == resultCode.success)
                        {
                            _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Price);
                        }
                        else
                        {
                            _submissionDataGateway.LogError(new ADPError(submission.Id, (int)MessageTypes.Price,
                                TraceSoapExtension.XmlRequest, TraceSoapExtension.XmlResponse, vehicleSaveResult.message));
                            success = false;
                        }
                    }
                    else
                    {
                        _submissionDataGateway.LogError(new ADPError(submission.Id, (int)MessageTypes.Price,
                            TraceSoapExtension.XmlRequest, TraceSoapExtension.XmlResponse, readResult.message));
                        success = false;
                    }
                }
                catch (Exception ex)
                {
                    _submissionDataGateway.LogError(new ADPError(submission.Id, (int)MessageTypes.Price,
                           TraceSoapExtension.XmlRequest, ex.Message, ex.Message));
                    success = false;
                }
            }

            // Mark the submission.
            if (success)
            {
                submission.Accepted();
            }
            else
            {
                submission.Rejected();
            }
        }

        #endregion

        #region Utility

        private vehicleReadResult ReadVehicle(authenticationToken auth, string dealerId, string stockNo, int submissionId, int dealercode)
        {
            vehicleReadResult result = _webService.read(auth, new dealerId() { id = dealerId },
                             new vehicleReadRequest() { fileType = fileType.CARINV, carInvStockNo = stockNo, fileTypeSpecified = true });

            new SubmissionDataGateway().LoggingADPSuccess(submissionId, dealercode, stockNo, TraceSoapExtension.XmlRequest, TraceSoapExtension.XmlResponse);

            return result;
        
        }
        #endregion
    }

}
