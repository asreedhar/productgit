using System.Globalization;

namespace FirstLook.Distribution.DomainModel.Bindings.AutoUplink
{
    /// <summary>
    /// An AutoUplink-specific error.
    /// </summary>
    public class AutoUplinkError : FirstLookError
    {
        #region Properties

        /// <summary>
        /// Type of this AutoUplink error.
        /// </summary>
        public AutoUplinkErrorType AutoUplinkErrorType { get; private set; }

        /// <summary>
        /// AutoUplink's actual error return code.
        /// </summary>
        public string AutoUplinkErrorCode { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this error object with the correct error type values.
        /// </summary>
        /// <remarks>The concerns user field should be pulled from the db.</remarks>
        /// <param name="submissionId">Submission that had the error.</param>
        /// <param name="messageType">Message type that failed.</param>
        /// <param name="autoUplinkErrorCode">AutoUplink-specific error code.</param>        
        public AutoUplinkError(int submissionId, int messageType, string autoUplinkErrorCode) :
            base(submissionId, messageType)
        {
            AutoUplinkErrorCode = autoUplinkErrorCode;

            switch (int.Parse(autoUplinkErrorCode, CultureInfo.InvariantCulture))
            {
                // Vehicle not found.
                case 301:
                    AutoUplinkErrorType = AutoUplinkErrorType.VehicleNotFound;
                    FirstLookErrorType = FirstLookErrorType.VehicleNotFound;
                    ConcernsUser = true;
                    break;

                // Vehicle deleted.
                case 302:
                    AutoUplinkErrorType = AutoUplinkErrorType.VehicleDeleted;
                    FirstLookErrorType = FirstLookErrorType.VehicleDeleted;
                    ConcernsUser = true;
                    break;

                // Field has same value.
                case 303:
                    AutoUplinkErrorType = AutoUplinkErrorType.FieldHasSameValue;
                    FirstLookErrorType = FirstLookErrorType.FieldSameValue;
                    ConcernsUser = false;
                    break;

                // Field locked.
                case 304:
                    AutoUplinkErrorType = AutoUplinkErrorType.FieldLocked;
                    FirstLookErrorType = FirstLookErrorType.FieldLocked;
                    ConcernsUser = true;
                    break;

                // Authentication error.
                case 400:
                case 401:
                    AutoUplinkErrorType = AutoUplinkErrorType.AuthenticationError;
                    FirstLookErrorType = FirstLookErrorType.AuthenticationError;
                    ConcernsUser = true;
                    break;

                // System error.
                case 500:
                case 501:
                case 510:
                    AutoUplinkErrorType = AutoUplinkErrorType.SystemError;
                    FirstLookErrorType = FirstLookErrorType.SystemError;
                    ConcernsUser = true;
                    break;

                // Unrecognized.
                default:
                    AutoUplinkErrorType = AutoUplinkErrorType.Unrecognized;
                    FirstLookErrorType = FirstLookErrorType.Unrecognized;
                    ConcernsUser = true;
                    break;
            }
        }

        #endregion
    }
}
