namespace FirstLook.Distribution.DomainModel.Bindings.AutoUplink
{
    /// <summary>
    /// Types of AutoUplink-related errors.
    /// </summary>
    public enum AutoUplinkErrorType
    {
        /// <summary>
        /// Unrecognized error type.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// Vehicle was not found in AutoUplink's system.
        /// </summary>
        VehicleNotFound = 1,

        /// <summary>
        /// Vehicle had been deleted in AutoUplink's system.
        /// </summary>
        VehicleDeleted = 2,

        /// <summary>
        /// Field that we are trying to update already has that value.
        /// </summary>
        FieldHasSameValue = 3,

        /// <summary>
        /// Field has been locked by AutoUplink.
        /// </summary>
        FieldLocked = 4,

        /// <summary>
        /// Authentication with AutoUplink failed.
        /// </summary>
        AuthenticationError = 5,

        /// <summary>
        /// Generic system error.
        /// </summary>
        SystemError = 6
    }
}
