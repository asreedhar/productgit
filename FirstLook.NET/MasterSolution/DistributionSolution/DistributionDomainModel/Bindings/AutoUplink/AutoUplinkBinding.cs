using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Services.AutoUplink;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Bindings.AutoUplink
{
    /// <summary>
    /// Binding to the AutoUplink web service. The Aultec provider is also handled by this binding.    
    /// </summary>    
    /// <remarks>
    /// Credentials for the test user:
    ///  - Dealership code: 7054
    ///  - Username: FirstLookWS
    ///  - Password: f*tj$e3e
    /// </remarks>
    public class AutoUplinkBinding : Binding
    {
        #region Properties

        /// <summary>
        /// Web service to connect to.
        /// </summary>
        private readonly IAutoUplinkService _webService;

        /// <summary>
        /// The types of messages that this binding is capable of sending.
        /// </summary>
        public override Collection<MessageTypes> SupportedMessageTypes
        {
            get { return _supportedMessageTypes; }
        }
        private readonly Collection<MessageTypes> _supportedMessageTypes;

        /// <summary>
        /// Data gateway for accounts.
        /// </summary>
        private readonly IAccountDataGateway _accountDataGateway;

        /// <summary>
        /// Data gateway for publication.
        /// </summary>
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data gateway for submissions.
        /// </summary>
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Date gateway for inventory.
        /// </summary>
        private readonly IVehicleDataGateway _vehicleDataGateway;

        #endregion

        #region Construction

        /// <summary>
        /// Populate the web service and data gateways from the registry, and set the types of supported messages.
        /// </summary>
        public AutoUplinkBinding()
        {
            _supportedMessageTypes = new Collection<MessageTypes>();
            _supportedMessageTypes.Add(MessageTypes.Price);
            _supportedMessageTypes.Add(MessageTypes.Description);

            IResolver resolver = RegistryFactory.GetResolver();

            _webService             = resolver.Resolve<IAutoUplinkService>();
            _accountDataGateway     = resolver.Resolve<IAccountDataGateway>();
            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _submissionDataGateway  = resolver.Resolve<ISubmissionDataGateway>();
            _vehicleDataGateway     = resolver.Resolve<IVehicleDataGateway>();
        }

        #endregion

        #region Submission

        /// <summary>
        /// Publish a message to AutoUplink.
        /// </summary>        
        /// <param name="submission">Submission to update with results of call to provider.</param>
        protected override void Submit(ISubmission submission)
        {            
            // Message parts to be sent.
            Message message = _publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);
            Price price = message.GetPrice();
            TextContent description = message.GetDescription();
            TextContent htmlDescription = message.GetHtmlDescription();

            // Inventory details.
            Inventory inventory = _vehicleDataGateway.FetchInventory(message.Vehicle.VehicleEntityId);

            // Account credentials.
            string userName = ConfigurationManager.AppSettings["AutoUplinkUsername"];
            string password = ConfigurationManager.AppSettings["AutoUplinkPassword"];
            Account account = _accountDataGateway.Fetch(submission.ProviderId, message.Dealer.Id);
            
            if (string.IsNullOrEmpty(account.DealershipCode))
            {
                throw new DataException("Expected dealership code for AutoUplink.");
            }                       
            
            WSAuth auth = new WSAuth { DealerID = account.DealershipCode,
                                       UserName = userName,
                                       Password = password };

            string returnCode;
            bool success = true;

            // Mark the submission as sent.
            submission.Submitted();

            // Price!
            if (price != null)
            {
                // Price field that is updated depends on who the price is going to (AutoUplink vs. Aultec) and on a 
                // setting called 'extract config'.
                if ((BindingType) submission.ProviderId == BindingType.Aultec &&
                    _accountDataGateway.DealerRequiresInternetPriceUpdate(message.Dealer.Id))
                {
                    returnCode = _webService.InternetPriceUpdate(auth, inventory.Vin, price.Value);
                }
                else
                {
                    returnCode = _webService.PriceUpdate(auth, inventory.Vin, price.Value);
                }

                // Log successes / errors.
                if (IsReturnCodeSuccess(returnCode))
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int) MessageTypes.Price);
                }
                else
                {
                    _submissionDataGateway.LogError(new AutoUplinkError(submission.Id, (int) MessageTypes.Price, returnCode));
                    success = false;
                }
            }

            // Description!
            if (description != null)
            {                
                returnCode = _webService.CommentUpdate(auth, inventory.Vin, description.Text);

                // Log successes / errors.
                if (IsReturnCodeSuccess(returnCode))
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Description);                    
                }
                else
                {
                    _submissionDataGateway.LogError(new AutoUplinkError(submission.Id, (int)MessageTypes.Description, returnCode));
                    success = false;
                }
            }

            // HtmlDescription!
            if (htmlDescription != null)
            {
                returnCode = _webService.EnhancedVehicleHighlightsUpdate(auth, inventory.Vin, htmlDescription.Text);

                // Log successes / errors.
                if (IsReturnCodeSuccess(returnCode))
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Description);
                }
                else
                {
                    _submissionDataGateway.LogError(new AutoUplinkError(submission.Id, (int)MessageTypes.Description, returnCode));
                    success = false;
                }    
            }

            // Mark the submission.
            if (success)
            {
                submission.Accepted();
            } 
            else
            {
                submission.Rejected();
            }
        }

        #endregion

        #region Utility

        /// <summary>
        /// Check if the return code from AutoUplink signals a success or error.
        /// </summary>        
        /// <param name="returnCode">Return code to check.</param>
        /// <returns>True if the return code signals a success, false if it's an error.</returns>
        private static bool IsReturnCodeSuccess(string returnCode)
        {
            switch (returnCode)
            {                                
                case "101":  // Success.
                case "303":  // Field has same value.
                    return true;

                case "301":  // Vehicle not found.
                case "302":  // Vehicle deleted.
                case "304":  // Field locked.
                case "400":  // Authentication error.
                case "401":  // Authentication error.
                case "500":  // System error.
                case "501":  // System error.
                case "510":  // System error.   
                    return false;

                default:
                    return false;
                    //throw new ResponseCodeException(returnCode);
            }
        }

        #endregion                      
    }
}
