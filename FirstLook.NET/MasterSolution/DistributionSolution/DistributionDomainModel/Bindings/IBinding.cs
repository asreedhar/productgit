using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Bindings
{
    /// <summary>
    /// Interface that defines the binding to an external provider.
    /// </summary>
    public interface IBinding
    {
        /// <summary>
        /// The types of messages that this binding is capable of sending.
        /// </summary>
        Collection<MessageTypes> SupportedMessageTypes { get; }

        /// <summary>
        /// Publish a message using the given account credentials.
        /// </summary>        
        /// <param name="submission">Submission.</param>
        void Publish(ISubmission submission);
    }
}
