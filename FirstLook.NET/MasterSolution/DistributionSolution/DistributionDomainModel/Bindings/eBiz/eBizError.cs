﻿namespace FirstLook.Distribution.DomainModel.Bindings.eBiz
{
    /// <summary>
    /// An eBiz-specific error.
    /// </summary>
    public class eBizError : FirstLookError
    {
        #region Properties

        /// <summary>
        /// Type of this eBiz error.
        /// </summary>
        public eBizErrorType eBizErrorType { get; private set; }

        /// <summary>
        /// The xml that was returned by the eBiz service.
        /// </summary>
        public string ErrorMessage { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this error object with the correct error type values.
        /// </summary>
        /// <param name="submissionId">Submission that had the error.</param>
        /// <param name="messageType">Message type that failed.</param>
        /// <param name="errorMesage">Error message returned from eBiz.</param>
        public eBizError(int submissionId, int messageType, string errorMesage) : 
            base(submissionId, messageType)
        {
            if (errorMesage == null)
            {
                errorMesage = string.Empty;
            }

            ErrorMessage = errorMesage;

            // Vehicle doesn't belong to dealer.
            if (errorMesage.Contains("VIN") && errorMesage.Contains("does not exist in account"))
            {
                eBizErrorType = eBizErrorType.VehicleNotFound;
                FirstLookErrorType = FirstLookErrorType.VehicleNotFound;
                ConcernsUser = true;
            }
            // Bad credentials were sent for FirstLook.
            else if (errorMesage.Contains("Invalid vendor authorization."))
            {
                eBizErrorType = eBizErrorType.InvalidVendorAuthorization;
                FirstLookErrorType = FirstLookErrorType.NotAuthorized;
                ConcernsUser = true;
            }
            // Bad credentials were sent for the dealer.
            else if (errorMesage.Contains("Invalid account authorization."))
            {
                eBizErrorType = eBizErrorType.InvalidAccountAuthorization;
                FirstLookErrorType = FirstLookErrorType.NotAuthorized;
                ConcernsUser = true;
            }
            // Only one item allowed.
            else if (errorMesage.Contains("More than one item was sent in a single packet."))
            {
                eBizErrorType = eBizErrorType.OnlyOneItem;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            // Internal error.
            else if (errorMesage.Contains("Internal error."))
            {
                eBizErrorType = eBizErrorType.InternalError;
                FirstLookErrorType = FirstLookErrorType.SystemError;
                ConcernsUser = true;
            }
            // Bad identifier.
            else if (ErrorMessage.Contains("Id param should be integer"))
            {
                eBizErrorType = eBizErrorType.BadIdParameter;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }
            // Unrecognized.
            else
            {
                eBizErrorType = eBizErrorType.Unrecognized;
                FirstLookErrorType = FirstLookErrorType.Unrecognized;
                ConcernsUser = true;
            }
        }

        #endregion
    }
}
