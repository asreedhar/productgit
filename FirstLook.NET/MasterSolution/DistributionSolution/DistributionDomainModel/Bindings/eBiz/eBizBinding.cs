﻿using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Text;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Services.eBiz;

namespace FirstLook.Distribution.DomainModel.Bindings.eBiz
{
    /// <summary>
    /// Binding to the eBiz web service.
    /// </summary>
    /// <remarks>
    /// Credentials for the test user:
    ///  - Dealership ID: 8307
    ///  - Dealership GUID: 8C141453-F40B-42AA-8411-73F2B11B6147    
    /// </remarks>
    public class eBizBinding : Binding
    {
        #region Properties

        /// <summary>
        /// eBiz web service.
        /// </summary>
        private readonly IEBizService _webService;

        /// <summary>
        /// The types of messages that this binding is capable of sending.
        /// </summary>
        public override Collection<MessageTypes> SupportedMessageTypes
        {
            get { return _supportedMessageTypes; }
        }
        private readonly Collection<MessageTypes> _supportedMessageTypes;

        /// <summary>
        /// Data gateway for accounts.
        /// </summary>
        private readonly IAccountDataGateway _accountDataGateway;

        /// <summary>
        /// Data gateway for publication.
        /// </summary>
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data gateway for submissions.
        /// </summary>
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Date gateway for inventory.
        /// </summary>
        private readonly IVehicleDataGateway _vehicleDataGateway;        

        #endregion

        #region Construction

        /// <summary>
        /// Populate the web service and data gateways from the registry, and set the types of supported messages.
        /// </summary>
        public eBizBinding()
        {
            _supportedMessageTypes = new Collection<MessageTypes>();
            _supportedMessageTypes.Add(MessageTypes.Price);
            _supportedMessageTypes.Add(MessageTypes.Description);

            IResolver resolver = RegistryFactory.GetResolver();

            _webService             = resolver.Resolve<IEBizService>();
            _accountDataGateway     = resolver.Resolve<IAccountDataGateway>();
            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _submissionDataGateway  = resolver.Resolve<ISubmissionDataGateway>();
            _vehicleDataGateway     = resolver.Resolve<IVehicleDataGateway>();  
        }

        #endregion

        #region Submission

        /// <summary>
        /// Publish a provider-specific message to a provider.
        /// </summary>        
        /// <param name="submission">Submission to give provider.</param>
        protected override void Submit(ISubmission submission)
        {
            // Message parts to be sent.
            Message message = _publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);
            Price price = message.GetPrice();
            TextContent description = message.GetDescription();

            // Vehicle details.
            Inventory inventory = _vehicleDataGateway.FetchInventory(message.Vehicle.VehicleEntityId);

            // Account credentials.
            string id = ConfigurationManager.AppSettings["eBizId"];
            string auth = ConfigurationManager.AppSettings["eBizPassword"];
            Account account = _accountDataGateway.Fetch(submission.ProviderId, message.Dealer.Id);

            if (string.IsNullOrEmpty(account.DealershipCode))
            {
                throw new DataException("Expected dealership code for eBiz.");
            }

            // Mark the submission as sent.
            submission.Submitted();

            bool success = true;

            // Send price to eBiz!
            if (price != null)
            {
                string requestXml = GeneratePriceRequest(account, inventory, price.Value);                
                string responseXml = _webService.SendVehicle(id, auth, requestXml);

                if (IsSuccess(responseXml))
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Price);
                }
                else
                {
                    _submissionDataGateway.LogError(new eBizError(submission.Id, (int)MessageTypes.Price, responseXml));
                    success = false;
                }
            }

            // Send description to eBiz!
            if (description != null)
            {
                string requestXml = GenerateDescriptionRequest(account, inventory, description.Text);
                string responseXml = _webService.SendVehicle(id, auth, requestXml);

                if (IsSuccess(responseXml))
                {
                    _submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Description);
                }
                else
                {
                    _submissionDataGateway.LogError(new eBizError(submission.Id, (int)MessageTypes.Description, responseXml));
                    success = false;
                }
            }

            // Mark the submission.
            if (success)
            {
                submission.Accepted();
            }
            else
            {
                submission.Rejected();
            }
        }

        #endregion

        #region Utility

        /// <summary>
        /// Generate the price request xml string.
        /// </summary>
        /// <remarks>
        /// I really tried to use the SyndicationFeed-way of generating the rss xml, but by god it's awful. It became
        /// such a pain in the ass to do such a simple thing. Therefore, I just make the xml directly. -- Christian
        /// </remarks>
        /// <param name="account">Account details.</param>
        /// <param name="inventory">Inventory details.</param>
        /// <param name="price">Price.</param>
        /// <returns>String-formatted rss feed.</returns>
        protected string GeneratePriceRequest(Account account, Inventory inventory, int price)
        {
            StringBuilder builder = new StringBuilder(600);
            
            builder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            builder.Append("<rss version=\"2.0\" xmlns:e=\"http://XML.eBizAutos.com\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            builder.Append("<channel>");
            builder.Append("<title /><link /><description />");
            builder.Append("<e:request_type>updateprice</e:request_type>");
            builder.Append("<item><title /><description />");
            builder.Append("<g:vin>");
            builder.Append(inventory.Vin);
            builder.Append("</g:vin>");
            builder.Append("<e:client_id>");
            builder.Append(account.DealershipCode);
            builder.Append("</e:client_id>");
            builder.Append("<guid isPermaLink=\"false\">");
            builder.Append(account.Password);
            builder.Append("</guid>");
            builder.Append("<g:price>");
            builder.Append(price);
            builder.Append("</g:price>");
            builder.Append("</item></channel></rss>");

            return builder.ToString();
        }

        /// <summary>
        /// Generate the description request xml string.
        /// </summary>
        /// <remarks>
        /// I really tried to use the SyndicationFeed-way of generating the rss xml, but by god it's awful. It became
        /// such a pain in the ass to do such a simple thing. Therefore, I just make the xml directly. -- Christian
        /// </remarks>
        /// <param name="account">Account details.</param>
        /// <param name="inventory">Inventory details.</param>
        /// <param name="description">Description.</param>
        /// <returns>String-formatted rss feed.</returns>
        protected string GenerateDescriptionRequest(Account account, Inventory inventory, string description)
        {
            StringBuilder builder = new StringBuilder(600);

            builder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            builder.Append("<rss version=\"2.0\" xmlns:e=\"http://XML.eBizAutos.com\" xmlns:g=\"http://base.google.com/ns/1.0\">");
            builder.Append("<channel>");
            builder.Append("<title /><link /><description />");
            builder.Append("<e:request_type>updatedescription</e:request_type>");
            builder.Append("<item><title />");
            builder.Append("<g:vin>");
            builder.Append(inventory.Vin);
            builder.Append("</g:vin>");
            builder.Append("<e:client_id>");
            builder.Append(account.DealershipCode);
            builder.Append("</e:client_id>");
            builder.Append("<guid isPermaLink=\"false\">");
            builder.Append(account.Password);
            builder.Append("</guid>");
            builder.Append("<description>");
            builder.Append(description);
            builder.Append("</description>");
            builder.Append("</item></channel></rss>");

            return builder.ToString();
        }        

        /// <summary>
        /// Does the response from eBiz signal success?
        /// </summary>
        /// <param name="response">Response xml.</param>
        /// <returns>True if the request was successfull, false otherwise.</returns>
        protected bool IsSuccess(string response)
        {
            return !string.IsNullOrEmpty(response) && response.Contains("<status>Success</status>");
        }

        #endregion
    }
}
