﻿namespace FirstLook.Distribution.DomainModel.Bindings.eBiz
{
    /// <summary>
    /// Types of eBiz-related errors.
    /// </summary>
    public enum eBizErrorType
    {
        /// <summary>
        /// Unrecognized.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// Vehicle doesn't exist for the dealer.
        /// </summary>
        VehicleNotFound = 1,

        /// <summary>
        /// The Auth code does not match the one provided for the third party, or an invalid Auth code was sent.
        /// </summary>
        InvalidVendorAuthorization = 2,

        /// <summary>
        /// The GUID code does not match the one provided for the account, or an invalid GUID code was sent.
        /// </summary>
        InvalidAccountAuthorization = 3,

        /// <summary>
        /// More than one item was sent in a single packet.
        /// </summary>
        OnlyOneItem = 4,

        /// <summary>
        /// An internal error has occured.
        /// </summary>
        InternalError = 5,

        /// <summary>
        /// Id param should be integer.
        /// </summary>
        BadIdParameter = 6

    }
}
