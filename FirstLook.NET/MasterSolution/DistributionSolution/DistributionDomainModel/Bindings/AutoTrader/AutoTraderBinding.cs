using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Services.AutoTrader;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Bindings.AutoTrader
{
    /// <summary>
    /// Binding to the AutoTrader web service.
    /// </summary>
    /// <remarks>
    /// Credentials for the test user:
    ///  - Dealership code: 69385
    ///  - Username: firstlook
    ///  - Password: 91QeGwLe8AxTM
    /// VIN for testing:
    ///  - VIN: 1G1AP14P967628886
    /// </remarks>
    public class AutoTraderBinding : Binding
    {
        #region Properties

        /// <summary>
        /// Web service to connect to.
        /// </summary>
        private readonly IAutoTraderService _webService;

        /// <summary>
        /// The types of messages that this binding is capable of sending.
        /// </summary>
        public override Collection<MessageTypes> SupportedMessageTypes
        {
            get { return _supportedMessageTypes; }
        }
        private readonly Collection<MessageTypes> _supportedMessageTypes;

        /// <summary>
        /// Data gateway for accounts.
        /// </summary>
        private readonly IAccountDataGateway _accountDataGateway;

        /// <summary>
        /// Data gateway for publication.
        /// </summary>
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data gateway for submissions.
        /// </summary>
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Date gateway for inventory.
        /// </summary>
        private readonly IVehicleDataGateway _vehicleDataGateway;        

        #endregion

        #region Construction

        /// <summary>
        /// Populate the web service and data gateways from the registry, and set the types of supported messages.
        /// </summary>
        public AutoTraderBinding()
        {
            _supportedMessageTypes = new Collection<MessageTypes>();
            _supportedMessageTypes.Add(MessageTypes.Price);
            _supportedMessageTypes.Add(MessageTypes.Price | MessageTypes.Description);

            IResolver resolver = RegistryFactory.GetResolver();

            _webService             = resolver.Resolve<IAutoTraderService>();
            _accountDataGateway     = resolver.Resolve<IAccountDataGateway>();
            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _submissionDataGateway  = resolver.Resolve<ISubmissionDataGateway>();
            _vehicleDataGateway     = resolver.Resolve<IVehicleDataGateway>();            
        }

        #endregion

        #region Submission

        /// <summary>
        /// Publish a message to AutoTrader.
        /// </summary>        
        /// <param name="submission">Submission to give to AutoTrader.</param>
        protected override void Submit(ISubmission submission)
        {            
            // Message parts to be sent.
            Message message = _publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);
            Price price = message.GetPrice();
            TextContent description = message.GetDescription();

            if (price == null || price.PriceType != PriceType.Internet)
            {
                // If the price is stale, fetch the most recent successfully sent price. Done because AutoTrader cannot 
                // send Description on its own.
                if ((submission.StaleMessageTypes & MessageTypes.Price) == MessageTypes.Price)
                {
                    price = _submissionDataGateway.FetchSupersedingPrice(submission.Id);
                    if (price == null)
                    {
                        throw new DataException("Could not find superseding price.");
                    }
                }
                else
                {
                    throw new DataException("All calls to AutoTrader's web service must have an Internet Price.");
                }
            }

            // Inventory details.
            Inventory inventory = _vehicleDataGateway.FetchInventory(message.Vehicle.VehicleEntityId);            

            // Account credentials.
            string userName = ConfigurationManager.AppSettings["AutoTraderUsername"];
            string password = ConfigurationManager.AppSettings["AutoTraderPassword"];
            Account account = _accountDataGateway.Fetch(submission.ProviderId, message.Dealer.Id);            
                  
            if (string.IsNullOrEmpty(account.DealershipCode))
            {
                throw new DataException("Expected dealership code for AutoTrader.");
            }
                        
            // Configure the service.
            ServicePointManager.Expect100Continue = false;
            _webService.Credentials = new NetworkCredential(userName, password);

            updateCarInfo carInfo = new updateCarInfo();
            carInfo.dealerId = long.Parse(account.DealershipCode, CultureInfo.InvariantCulture);
            carInfo.vin = inventory.Vin;
            carInfo.price = price.Value;

            if (description != null)
            {
                carInfo.comments = ProcessHtmlTags(description.Text);
            }
                                   
            try
            {
                // Mark the submission as sent.
                submission.Submitted();

                // Send to AutoTrader!
                string returnStr = _webService.updateCar(carInfo);

                if (returnStr == "Success")
                {
                    submission.Accepted();
                    _submissionDataGateway.LogSuccess(submission.Id, (int)submission.MessageTypes);
                }
                // Specs say the only valid return types are "Success" or an exception. This should never be called.
                else
                {
                    throw new Exception(returnStr);
                }                
            }            
            catch(Exception exception)
            {
                submission.Rejected();
                _submissionDataGateway.LogError(new AutoTraderError(submission.Id, (int)submission.MessageTypes, exception));
            }
        }

        #endregion

        #region Utility

        /// <summary>
        /// Escape AutoTrader advertisement text with a CDATA block to prevent AutoTrader's parser from stripping HTML
        /// line breaks out.
        /// </summary>
        /// <param name="description">Description text to process.</param>
        /// <returns>Processed description text.</returns>
        public static string ProcessHtmlTags(string description)
        {
            return "<![CDATA[" + description + "]]>";
        }

        #endregion
    }
}
