namespace FirstLook.Distribution.DomainModel.Bindings.AutoTrader
{
    /// <summary>
    /// Types of AutoTrader-related errors.
    /// </summary>
    public enum AutoTraderErrorType
    {
        /// <summary>
        /// Unrecognized error type.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// User is not authorized.
        /// </summary>
        NotAuthorized = 1,
       
        /// <summary>
        /// Dealer not found for the given identifier.
        /// </summary>
        DealerNotFound = 2,

        /// <summary>
        /// Vehicle not found for the given VIN.
        /// </summary>
        VehicleNotFound = 3,

        /// <summary>
        /// Dealer does not match with the given VIN.
        /// </summary>
        DealerVinMismatch = 4,
        
        /// <summary>
        /// Generic system error.
        /// </summary>
        SystemError = 5,
        
        /// <summary>
        /// VIN is required but was not supplied.
        /// </summary>
        VinRequired = 6,

        /// <summary>
        /// VIN is not valid.
        /// </summary>
        VinInvalid = 7,

        /// <summary>
        /// Price is not valid.
        /// </summary>
        PriceInvalid = 8
    }
}
