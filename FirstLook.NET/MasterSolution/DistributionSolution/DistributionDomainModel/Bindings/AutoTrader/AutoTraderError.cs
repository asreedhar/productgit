using System;

namespace FirstLook.Distribution.DomainModel.Bindings.AutoTrader
{
    /// <summary>
    /// An AutoTrader-specific error.
    /// </summary>
    public class AutoTraderError : FirstLookError
    {
        #region Properties

        /// <summary>
        /// Type of this AutoTrader error.
        /// </summary>
        public AutoTraderErrorType AutoTraderErrorType { get; private set; }

        /// <summary>
        /// Rejection message returned by AutoTrader.
        /// </summary>
        public string ErrorMessage { get; private set; }

        #endregion

        #region Construction

        /// <summary>
        /// Populate this error object with the correct error type values.
        /// </summary>
        /// <remarks>The concerns user field should be pulled from the db.</remarks>
        /// <param name="submissionId">Submission that had the error.</param>
        /// <param name="exception">AutoTrader-returned exception.</param>
        /// <param name="messageType">Message types that failed.</param>
        public AutoTraderError(int submissionId, int messageType, Exception exception) : 
            base(submissionId, messageType)
        {                        
            ErrorMessage = exception.Message;            

            // Not authorized.
            if (ErrorMessage.Contains("is not authorized for this vehicle price update"))
            {
                AutoTraderErrorType = AutoTraderErrorType.NotAuthorized;
                FirstLookErrorType = FirstLookErrorType.NotAuthorized;
                ConcernsUser = true;
            }

            // Dealer not found.
            else if (ErrorMessage.Contains("Not able to locate Dealer with Id"))
            {
                AutoTraderErrorType = AutoTraderErrorType.DealerNotFound;
                FirstLookErrorType = FirstLookErrorType.DealerNotFound;
                ConcernsUser = true;
            }

            // Vehicle with VIN not found.
            else if (ErrorMessage.Contains("Vehicle for this VIN") && ErrorMessage.Contains("not found"))
            {
                AutoTraderErrorType = AutoTraderErrorType.VehicleNotFound;
                FirstLookErrorType = FirstLookErrorType.VehicleNotFound;
                ConcernsUser = true;
            }

            // Dealer / VIN mismatch.
            else if (ErrorMessage.Contains("does not match with the given VIN"))
            {
                AutoTraderErrorType = AutoTraderErrorType.DealerVinMismatch;
                FirstLookErrorType = FirstLookErrorType.DealerVinMismatch;
                ConcernsUser = true;
            }

            // System error.
            else if (ErrorMessage.Contains("SystemError"))
            {
                AutoTraderErrorType = AutoTraderErrorType.SystemError;
                FirstLookErrorType = FirstLookErrorType.SystemError;
                ConcernsUser = true;
            }

            // VIN required.
            else if (ErrorMessage.Contains("Field Required (VIN)"))
            {
                AutoTraderErrorType = AutoTraderErrorType.VinRequired;
                FirstLookErrorType = FirstLookErrorType.MissingField;
                ConcernsUser = true;
            }

            // Invalid VIN.
            else if (ErrorMessage.Contains("Data Not Valid (VIN)"))
            {
                AutoTraderErrorType = AutoTraderErrorType.VinInvalid;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }

            // Invalid price.
            else if (ErrorMessage.Contains("Data Not Valid (PRICE)"))
            {
                AutoTraderErrorType = AutoTraderErrorType.PriceInvalid;
                FirstLookErrorType = FirstLookErrorType.InvalidField;
                ConcernsUser = true;
            }

            // Unrecognized.
            else
            {
                AutoTraderErrorType = AutoTraderErrorType.Unrecognized;
                FirstLookErrorType = FirstLookErrorType.Unrecognized;
                ConcernsUser = true;
            }
        }

        #endregion
    }
}
