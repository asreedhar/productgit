using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Distributors
{
    /// <summary>
    /// Interface that defines a submission queue.
    /// </summary>
    public interface IQueue
    {
        /// <summary>
        /// Push a submission onto the queue. Must be thread safe.
        /// </summary>
        /// <param name="submission">Submission to push onto the queue.</param>
        void Push(Submission submission);

        /// <summary>
        /// Pop a submission off the queue. Must be thread safe.
        /// </summary>
        /// <returns>Submission popped off the queue.</returns>
        Submission Pop();

        /// <summary>
        /// Get the number of entries on the queue. Must be thread safe.
        /// </summary>
        /// <returns>Queue size.</returns>
        int Count();
    }
}
