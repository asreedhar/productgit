using System.Threading;

namespace FirstLook.Distribution.DomainModel.Distributors
{
    /// <summary>
    /// Publisher that will use the thread pool for handling submissions.
    /// </summary>
    public class ThreadedPublisher : Publisher
    {
        /// <summary>
        /// Queue the work of publishing a submission with the thread pool.
        /// </summary>
        public override void Publish()
        {
            ThreadPool.QueueUserWorkItem(Publish);
        }
    }
}
