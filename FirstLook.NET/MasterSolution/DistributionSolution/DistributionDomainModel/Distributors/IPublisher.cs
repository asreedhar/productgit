namespace FirstLook.Distribution.DomainModel.Distributors
{
    /// <summary>
    /// Interface that defines a publisher of submissions.
    /// </summary>
    public interface IPublisher
    {
        /// <summary>
        /// Pop a submission off the queue and begin its processing.
        /// </summary>
        void Publish();
    }
}
