using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Reflection;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.TransferObjects.V_1_0;
using Message = FirstLook.Distribution.DomainModel.Entities.Message;
using Provider = FirstLook.Distribution.DomainModel.Entities.Provider;

namespace FirstLook.Distribution.DomainModel.Distributors
{
    /// <summary>
    /// Message distribution. Will accept a message and queue a submission for it with every applicable provider. Also
    /// provides the ability to poll the status of an existing submission. Also provides the ability to redistribute
    /// failed submissions.
    /// </summary>
    public class Distributor : IDistributor
    {
        #region Properties

        /// <summary>
        /// Logging provider
        /// </summary>
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Data gateway for publications.
        /// </summary>        
        private readonly IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Data gateway for accounts.
        /// </summary>        
        private readonly IAccountDataGateway _accountDataGateway;

        /// <summary>
        /// Data gateway for submissions.
        /// </summary>        
        private readonly ISubmissionDataGateway _submissionDataGateway;

        /// <summary>
        /// Data gateway for exceptions.
        /// </summary>
        private readonly IExceptionLogger _exceptionDataGateway;

        /// <summary>
        /// Submission queue.
        /// </summary>        
        private readonly IQueue _queue;

        /// <summary>
        /// The publisher who takes a submission off the queue and publishes it with the provider.        
        /// </summary>
        private readonly IPublisher _publisher;

        #endregion

        #region Construction

        /// <summary>
        /// Populate the data gateways, the queue and the publisher with the values registered in the registry.
        /// </summary>
        public Distributor()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            _publicationDataGateway = resolver.Resolve<IPublicationDataGateway>();
            _accountDataGateway = resolver.Resolve<IAccountDataGateway>();
            _submissionDataGateway = resolver.Resolve<ISubmissionDataGateway>();
            _exceptionDataGateway = resolver.Resolve<IExceptionLogger>();
            _queue = resolver.Resolve<IQueue>();
            _publisher = resolver.Resolve<IPublisher>();
        }

        #endregion

        #region V_1_0 Distributing Methods

        /// <summary>
        /// Log the incoming message and queue a submission for it with every applicable provider.        
        /// </summary>
        /// <param name="message">Message to distribute.</param>
        /// <returns>Details on which providers the message will be published with.</returns>
        public TransferObjects.V_1_0.SubmissionDetails Distribute(TransferObjects.V_1_0.Message message)
        {
            // Every publication that comes through is logged, whether it is distributed or not.
            Publication publication = Publication.NewPublication(message);

            //Check for eLead. If dealer is not associated with eLead publication will not be saved.
            if (!IsDistributionEnabled(message.From.Id, publication))
            {
                publication = null;
                return new TransferObjects.V_1_0.SubmissionDetails();
            }

            _publicationDataGateway.Save(publication);

            TransferObjects.V_1_0.SubmissionDetails details =
                new TransferObjects.V_1_0.SubmissionDetails { PublicationId = publication.Id };

            // Check that the vehicle is valid.
            if (publication.Message.Vehicle.VehicleEntityType == VehicleEntityType.NotDefined)
            {
                publication.NotDistributed();
                return details;
            }

            bool distributed = false;

            // Distribution is typically disabled on test environments.
            if (ConfigurationManager.AppSettings["EnableDistribution"] == "1")
            {
                // Get the providers that have been enabled in the config files for this dealer.
                foreach (Provider provider in Provider.EnabledProviders(message.From.Id))
                {
                    MessageTypes supportedTypes = publication.Message.MessageTypes;

                    // Verify the dealer has active account info with the provider.
                    if (_accountDataGateway.IsSupported(message.From.Id, provider.Id, ref supportedTypes))
                    {
                        distributed = true;

                        // Create a submission for only the supported message types.
                        Submission submission = publication.CreateSubmission(provider.Id, supportedTypes);
                        _submissionDataGateway.Save(submission);

                        // Queue and publish!
                        _queue.Push(submission);
                        _publisher.Publish();

                        details.Providers.Add(new TransferObjects.V_1_0.Provider { Id = provider.Id, Name = provider.Name });
                    }
                }
            }
            if (!distributed)
            {
                publication.NotDistributed();
            }
            return details;
        }

        #endregion

        #region V_1_0 Polling Methods

        /// <summary>
        /// Check the status of a publication's submissions.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>Collection of submission statuses.</returns>
        public Collection<TransferObjects.V_1_0.SubmissionStatus> Status(int publicationId)
        {
            IList<Submission> submissions = _submissionDataGateway.Fetch(publicationId);
            return GetResults(submissions);
        }

        /// <summary>
        /// Check the status of a publication's submissions. Submissions are found for the dealer and vehicle 
        /// identifiers, and are only retrieved if they were made today.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleType">Type of the vehicle.</param>       
        /// <returns>Collection of submission statuses.</returns>
        public Collection<TransferObjects.V_1_0.SubmissionStatus> Status(int dealerId, int vehicleEntityId,
                                                                         TransferObjects.V_1_0.VehicleType vehicleType)
        {
            IList<Submission> submissions = _submissionDataGateway.Fetch(dealerId,
                                                                         vehicleEntityId,
                                                                         (VehicleEntityType)vehicleType);
            return GetResults(submissions);
        }

        /// <summary>
        /// Extract the statuses of the given collection of submissions.        
        /// </summary>
        /// <param name="submissions">Collection of submissions.</param>
        /// <returns>Collection of submission statuses.</returns>
        private Collection<TransferObjects.V_1_0.SubmissionStatus> GetResults(IEnumerable<Submission> submissions)
        {
            Collection<TransferObjects.V_1_0.SubmissionStatus> results =
                new Collection<TransferObjects.V_1_0.SubmissionStatus>();

            foreach (Submission submission in submissions)
            {
                // State.
                TransferObjects.V_1_0.SubmissionStatus status =
                    new TransferObjects.V_1_0.SubmissionStatus { State = (TransferObjects.V_1_0.SubmissionState)submission.SubmissionState };

                // Provider.
                Provider provider = _accountDataGateway.FetchProvider(submission.ProviderId);
                status.Provider = new TransferObjects.V_1_0.Provider { Id = provider.Id, Name = provider.Name };

                // Errors.
                Collection<FirstLookError> errors = _submissionDataGateway.FetchErrors(submission.Id);
                foreach (FirstLookError error in errors)
                {
                    string messageTypeString = Message.ParseMessageTypesToString(error.MessageType);
                    Collection<MessageTypes> messageTypesCollection = Message.ParseMessageTypesToList(error.MessageType);

                    TransferObjects.V_1_0.SubmissionError submissionError =
                        new TransferObjects.V_1_0.SubmissionError(messageTypesCollection, messageTypeString, error.FirstLookErrorMessage);
                    status.Errors.Add(submissionError);
                }

                // Successes.
                Collection<int> successes = _submissionDataGateway.FetchSuccesses(submission.Id);
                foreach (int messageTypes in successes)
                {
                    string messageTypeString = Message.ParseMessageTypesToString(messageTypes);
                    Collection<MessageTypes> messageTypesCollection = Message.ParseMessageTypesToList(messageTypes);

                    TransferObjects.V_1_0.SubmissionSuccess success =
                        new TransferObjects.V_1_0.SubmissionSuccess(messageTypesCollection, messageTypeString);

                    status.Successes.Add(success);
                }

                // Exceptions.
                string exceptionMessage = _exceptionDataGateway.Fetch(submission.Id);
                if (exceptionMessage != null)
                {
                    status.Message = exceptionMessage;
                }

                results.Add(status);
            }

            return results;
        }

        #endregion

        #region Redistribution Methods

        /// <summary>
        /// Redistribute failed submissions that have not yet reached the maximum number of retries.
        /// </summary>           
        /// <returns>The number of redistributed submissions.</returns>     
        public int Redistribute()
        {
            int count = 0;

            // Distribution is typically disabled on test environments.
            if (ConfigurationManager.AppSettings["EnableDistribution"] == "1")
            {
                int maxAttempts = int.Parse(ConfigurationManager.AppSettings["MaxRedistributionAttempts"]);
                int maxDaysBack = int.Parse(ConfigurationManager.AppSettings["MaxRedistributionDaysBack"]);

                // Fetch all submission to be resent.
                IList<Submission> submissions = _submissionDataGateway.FetchFailuresToRetry(maxAttempts, maxDaysBack);

                foreach (Submission failure in submissions)
                {
                    Publication publication = _publicationDataGateway.Fetch(failure.PublicationId);
                    MessageTypes supportedTypes = publication.Message.MessageTypes;

                    // Verify the dealer has active account info with the provider.
                    if (_accountDataGateway.IsSupported(publication.Message.Dealer.Id, failure.ProviderId, ref supportedTypes))
                    {
                        // Create a submission for only the supported message types.
                        Submission submission = publication.CreateSubmission(failure.ProviderId, supportedTypes, failure.Attempt + 1);
                        _submissionDataGateway.Save(submission);

                        // Queue and publish!
                        _queue.Push(submission);
                        _publisher.Publish();

                        count++;
                    }
                }
            }
            return count;
        }

        #endregion

        #region UtilityMethods
        /// <summary>
        /// This method will check if eLead distribution is enabled for dealer before saving Publication in database.
        /// Assumtions: If Vehicle entity type is Appraisal then the request is for eLead.
        /// </summary>
        /// <param name="dealerID"></param>
        /// <param name="publication"></param>
        /// <returns></returns>
        private bool IsDistributionEnabled(int dealerID, Publication publication)
        {
            if (publication.Message.Vehicle.VehicleEntityType == VehicleEntityType.Appraisal)
            {
                // Get the providers that have been enabled in the config files for this dealer.
                MessageTypes supportedTypes = publication.Message.MessageTypes;

                // Verify the dealer has active account info with the provider.
                return _accountDataGateway.IsSupported(dealerID, (int)BindingType.eLead, ref supportedTypes);
            }

            return true;

        }

        #endregion
    }
}
