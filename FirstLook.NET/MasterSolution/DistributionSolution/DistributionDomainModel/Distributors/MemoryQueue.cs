using System.Collections.Generic;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Distributors
{
    /// <summary>
    /// In-memory submission queue.
    /// </summary>
    public class MemoryQueue : IQueue
    {
        /// <summary>
        /// Submission queue.
        /// </summary>
        static private readonly Queue<Submission> _queue = new Queue<Submission>();        

        /// <summary>
        /// Push a submission onto the queue. Thread safe.
        /// </summary>
        /// <param name="submission">Submission to push onto the queue.</param>
        public void Push(Submission submission)
        {
            lock (_queue)
            {
                _queue.Enqueue(submission);   
            }            
        }

        /// <summary>
        /// Pop a submission off the queue. Thread safe.
        /// </summary>
        /// <returns>Submission popped off the queue. Null if the queue is empty.</returns>
        public Submission Pop()
        {
            Submission submission = null;

            lock (_queue)
            {
                if (_queue.Count > 0)
                {
                    submission = _queue.Dequeue();
                }
            }
            return submission;
        }

        /// <summary>
        /// Get the number of entries on the queue. Thread safe.
        /// </summary>
        /// <returns>Queue size.</returns>
        public int Count()
        {
            int count;

            lock (_queue)
            {
                count = _queue.Count;
            }
            return count;
        }
    }
}
