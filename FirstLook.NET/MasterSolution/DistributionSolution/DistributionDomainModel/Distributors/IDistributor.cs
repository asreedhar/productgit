using System.Collections.ObjectModel;

namespace FirstLook.Distribution.DomainModel.Distributors
{
    /// <summary>
    /// Interface the defines a distributor of messages.
    /// </summary>
    public interface IDistributor
    {
        #region V_1_0 Distributing Methods

        /// <summary>
        /// Log the incoming message and queue a submission for it with every applicable provider.        
        /// </summary>
        /// <param name="message">Message to distribute.</param>
        /// <returns>Details on which providers the message will be published with.</returns>
        TransferObjects.V_1_0.SubmissionDetails Distribute(TransferObjects.V_1_0.Message message);

        #endregion        

        #region V_1_0 Polling Methods

        /// <summary>
        /// Check the status of a publication's submissions.
        /// </summary>
        /// <param name="publicationId">Publication identifier.</param>
        /// <returns>Collection of submission statuses.</returns>
        Collection<TransferObjects.V_1_0.SubmissionStatus> Status(int publicationId);

        /// <summary>
        /// Check the status of a publication's submissions. Submissions are found for the dealer and vehicle 
        /// identifiers, and are only retrieved if they were made today.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vehicleEntityId">Vehicle identifier.</param>
        /// <param name="vehicleType">Type of the vehicle.</param>       
        /// <returns>Collection of submission statuses.</returns>
        Collection<TransferObjects.V_1_0.SubmissionStatus> Status(int dealerId, int vehicleEntityId, 
                                                                  TransferObjects.V_1_0.VehicleType vehicleType);

        #endregion

        #region Redistribution Methods

        /// <summary>
        /// Redistribute failed submissions that have not yet reached the maximum number of retries.
        /// </summary>           
        /// <returns>The number of redistributed submissions.</returns>
        int Redistribute();

        #endregion
    }
}
