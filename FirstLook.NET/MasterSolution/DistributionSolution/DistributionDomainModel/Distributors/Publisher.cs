using System;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.Distributors
{
    /// <summary>
    /// Abstract base class for a publisher of submissions.
    /// </summary>
    public abstract class Publisher : IPublisher
    {
        /// <summary>
        /// Pop a submission off the queue and begin its processing.
        /// </summary>
        public abstract void Publish();

        /// <summary>
        /// Pop a submission off the queue and pass it on to the correct provider binding. If there is no submission
        /// to publish on the queue, return.
        /// </summary>
        /// <remarks>
        /// Method signature is designed to conform to what the ThreadPool requires for queueing user work items.
        /// </remarks>        
        /// <param name="stateInfo">Optional state information. Ignored.</param>
        protected static void Publish(Object stateInfo)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            try
            {
                IQueue queue = resolver.Resolve<IQueue>();

                Submission submission = queue.Pop();
                if (submission == null)
                {
                    return;
                }

                IBindingFactory bindingFactory = resolver.Resolve<IBindingFactory>();
                IBinding binding = bindingFactory.GetBinding(submission.ProviderId);
                binding.Publish(submission);
            }
            catch (Exception exception)
            {
                IExceptionLogger logger = resolver.Resolve<IExceptionLogger>();
                logger.Record(exception);
            }            
        }
    }
}
