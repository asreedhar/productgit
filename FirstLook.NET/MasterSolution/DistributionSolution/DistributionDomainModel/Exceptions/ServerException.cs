using System;

namespace FirstLook.Distribution.DomainModel.Exceptions
{
    /// <summary>
    /// Abstract base class for exceptions caused by the application code.
    /// </summary>
    [Serializable]
    public abstract class ServerException : Exception
    {
        /// <summary>
        /// Create a server exception for the given message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        protected ServerException(string message)
            : base(message)
        {            
        }
    }

    /// <summary>
    /// Exception for when the registry is asked to resolve something that was not registered.
    /// </summary>
    [Serializable]
    public class RegistryException : ServerException
    {
        /// <summary>
        /// Create a registry exception for the given contract.
        /// </summary>
        /// <param name="contractName">Contract name.</param>
        public RegistryException(string contractName)
            : base("The type " + contractName + " does not exist in the registry.")
        {
        }
    }
}
