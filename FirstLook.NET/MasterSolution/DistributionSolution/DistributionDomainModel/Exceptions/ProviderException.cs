using System;

namespace FirstLook.Distribution.DomainModel.Exceptions
{
    /// <summary>
    /// Abstract base class for exceptions caused by provider-related errors.
    /// </summary>
    [Serializable]
    public abstract class ProviderException : Exception
    {
        /// <summary>
        /// Create a provider exception from the given message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        protected ProviderException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Exception for when a provider returns a response code that we don't recognize.
    /// </summary>
    [Serializable]
    public class ResponseCodeException : ProviderException
    {
        /// <summary>
        /// Create a response code exception from the given response code.
        /// </summary>
        /// <param name="responseCode">Response code that caused the exception.</param>
        public ResponseCodeException(string responseCode)
            :
            base("Unrecognized response code from provider (" + responseCode + ").")
        {
        }
    }

    /// <summary>
    /// Exception for when a submission is to be given to an unhandled provider.
    /// </summary>
    [Serializable]
    public class UnhandledProviderException : ProviderException
    {
        /// <summary>
        /// Create an unhandled provider exception for the provider with the given identifier.
        /// </summary>
        /// <param name="providerId">Provider identifier.</param>
        public UnhandledProviderException(int providerId)
            : base("Provider with id " + providerId + " is not supported.")
        {
        }
    }
}
