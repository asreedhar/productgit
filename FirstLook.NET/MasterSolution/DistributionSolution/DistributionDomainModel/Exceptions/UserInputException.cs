using System;

namespace FirstLook.Distribution.DomainModel.Exceptions
{
    /// <summary>
    /// Abstract base class for exceptions caused by bad user input.
    /// </summary>
    [Serializable]
    public abstract class UserInputException : Exception
    {
        /// <summary>
        /// Create an exception with the given message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        protected UserInputException(string message)
            : base(message)
        {
        }
    }

    #region Message Exceptions

    /// <summary>
    /// Exception for when a passed-in message is null.
    /// </summary>
    [Serializable]
    public class MessageNullException : UserInputException
    {
        /// <summary>
        /// Create a 'message null' exception.
        /// </summary>
        public MessageNullException()
            : base("Message cannot be null.")
        {
        }        
    }

    /// <summary>
    /// Exception for when a passed-in message has no body parts appropriate to the provider it was given to.
    /// </summary>
    [Serializable]
    public class MessageTypeNotSupportedByProviderException : UserInputException
    {
        /// <summary>
        /// Create a 'message type not supported by provider' exception.
        /// </summary>
        /// <param name="provider"></param>
        public MessageTypeNotSupportedByProviderException(string provider) 
            : base("Message type not supported by " + provider)
        {
        }
    }

    #endregion

    #region Dealer Exceptions

    /// <summary>
    /// Exception for when a passed-in message has no dealer specified.
    /// </summary>
    [Serializable]
    public class DealerNullException : UserInputException
    {
        /// <summary>
        /// Create a 'dealer null' exception.
        /// </summary>
        public DealerNullException()
            : base("Dealer must be specified in message.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has an invalid dealer identifier.
    /// </summary>
    [Serializable]
    public class InvalidDealerIdException : UserInputException
    {
        /// <summary>
        /// Create an 'invalid dealer id' excpetion.
        /// </summary>
        public InvalidDealerIdException()
            : base("No dealer exists for the provided identifier.")
        {
        }
    }

    #endregion

    #region Vehicle Exceptions

    /// <summary>
    /// Exception for when a passed-in message has no vehicle specified.
    /// </summary>
    [Serializable]
    public class VehicleNullException : UserInputException
    {
        /// <summary>
        /// Create a 'vehicle null' exception.
        /// </summary>
        public VehicleNullException()
            : base("Vehicle must be specified in message.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a vehicle with an unknown type.
    /// </summary>
    [Serializable]
    public class UnknownVehicleTypeException : UserInputException
    {
        /// <summary>
        /// Create an 'unknown vehicle type' exception.
        /// </summary>
        public UnknownVehicleTypeException()
            : base("Vehicle must be of recognized type.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a non-inventory vehicle.
    /// </summary>
    [Serializable]
    public class NonInventoryVehicleException : UserInputException
    {
        public NonInventoryVehicleException() 
            : base("Non-inventory vehicles are not currently supported.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has an identifier for a vehicle that doesn't exist in our system.
    /// </summary>
    [Serializable]
    public class InvalidVehicleIdException : UserInputException
    {
        public InvalidVehicleIdException(int vehicleId) 
            : base("No vehicle exists for the provided identifier. [" + vehicleId +"]")
        {            
        }
    }

    #endregion

    #region Body Exceptions

    /// <summary>
    /// Exception for when a passed-in message has no body.
    /// </summary>
    [Serializable]
    public class BodyNullException : UserInputException
    {
        /// <summary>
        /// Create a 'body null' exception.
        /// </summary>
        public BodyNullException()
            : base("Body must be specified in message.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has no body parts.
    /// </summary>
    [Serializable]
    public class BodyEmptyException : UserInputException
    {
        /// <summary>
        /// Create a 'body empty' exception.
        /// </summary>
        public BodyEmptyException()
            : base("Body must have at least one body part in message.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a body part of an unknown type.
    /// </summary>
    [Serializable]
    public class UnknownBodyPartTypeException : UserInputException
    {
        /// <summary>
        /// Create an 'unknown body part type' exception.
        /// </summary>
        public UnknownBodyPartTypeException()
            : base("Message body part must be of recognized type.")
        {
        }
    }

    #endregion

    #region Advertisement Exceptions

    /// <summary>
    /// Exception for when a passed-in message has an advertisement with no pieces of content.
    /// </summary>
    [Serializable]
    public class ContentEmptyException : UserInputException
    {
        /// <summary>
        /// Create a 'content empty' exception.
        /// </summary>
        public ContentEmptyException()
            : base("Advertisement must have at least one piece of content.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a piece of content with an unknown type.
    /// </summary>
    [Serializable]
    public class UnknownContentTypeException : UserInputException
    {
        /// <summary>
        /// Create an 'unknown content' exception.
        /// </summary>
        public UnknownContentTypeException()
            : base("Advertisement content must be of recognized type.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a piece of text content that is empty.
    /// </summary>
    [Serializable]
    public class TextEmptyException : UserInputException
    {
        /// <summary>
        /// Create a 'text empty' exception.
        /// </summary>
        public TextEmptyException()
            : base("An advertisement's text cannot be empty.")
        {
        }
    }

    #endregion

    #region Price Exceptions

    /// <summary>
    /// Exception for when a passed-in message has a price that exceeds the maximum value.
    /// </summary>
    [Serializable]
    public class PriceHighException : UserInputException
    {
        /// <summary>
        /// Create a 'price high' exception.
        /// </summary>
        public PriceHighException()
            : base("Price exceeds maximum allowable value.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a price that deceeds the minimum value.
    /// </summary>
    [Serializable]
    public class PriceLowException : UserInputException
    {
        /// <summary>
        /// Create a 'price low' exception.
        /// </summary>
        public PriceLowException()
            : base("Price is below minimum allowable value.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a price with an unknown type.
    /// </summary>
    [Serializable]
    public class UnknownPriceTypeException : UserInputException
    {
        /// <summary>
        /// Create an 'unknown price type' exception.
        /// </summary>
        public UnknownPriceTypeException()
            : base("Price must be of recognized type.")
        {
        }
    }

    #endregion

    #region Vehicle Information Exceptions

    /// <summary>
    /// Exception for when a passed-in message has a mileage that exceeds the maximum value.
    /// </summary>
    [Serializable]
    public class MileageHighException : UserInputException
    {
        /// <summary>
        /// Create a 'mileage high' exception.
        /// </summary>
        public MileageHighException()
            : base("Mileage exceeds maximum allowable value.")
        {
        }
    }

    /// <summary>
    /// Exception for when a passed-in message has a mileage that deceeds the minimum value.
    /// </summary>
    [Serializable]
    public class MileageLowException : UserInputException
    {
        /// <summary>
        /// Create a 'mileage low' exception.
        /// </summary>
        public MileageLowException()
            : base("Mileage is below minimum allowable value.")
        {
        }
    }

    #endregion

    #region Identity Exceptions

    /// <summary>
    /// Exception for when the passed-in user identity is not valid.
    /// </summary>
    [Serializable]
    public class UserIdentityException : UserInputException
    {
        public UserIdentityException() : base("User identity is not valid.")
        {
        }
    }

    #endregion
}
