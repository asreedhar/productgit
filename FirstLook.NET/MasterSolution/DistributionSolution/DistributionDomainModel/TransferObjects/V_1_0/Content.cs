using System;
using System.Xml.Serialization;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Abstract base class for advertisement content.
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(TextContent))]
    public abstract class Content
    {        
        /// <summary>
        /// Content type.
        /// </summary>
        public abstract ContentType ContentType { get; }
    }

    /// <summary>
    /// Text content in an advertisement.
    /// </summary>
    [Serializable]
    public class TextContent : Content
    {
        ///<summary>
        /// Text content.
        ///</summary>
        public string Text { get; set; }
        
        /// <summary>
        /// Content type.
        /// </summary>
        public override ContentType ContentType
        {
            get { return ContentType.Text; }
        }
    }
}
