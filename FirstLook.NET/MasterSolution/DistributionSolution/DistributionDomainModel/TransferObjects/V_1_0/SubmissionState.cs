using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// States a submission can be in.
    /// </summary>
    [Serializable]
    public enum SubmissionState
    {
        /// <summary>
        /// Undefined submission state.
        /// </summary>
        NotDefined = 0,
        
        /// <summary>
        /// Submission is queued and waiting to be processed.
        /// </summary>
        Waiting = 1,
        
        /// <summary>
        /// Submission is being processed.
        /// </summary>
        Processing = 2,

        /// <summary>
        /// Submission has been submitted to a provider.
        /// </summary>
        Submitted = 3,

        /// <summary>
        /// Submission was accepted by a provider.
        /// </summary>
        Accepted = 4,

        /// <summary>
        /// Submission was rejected by a provider.
        /// </summary>
        Rejected = 5,

        /// <summary>
        /// Submission was retracted (i.e. taken offline by the dealer).
        /// </summary>
        Retracted = 6,

        /// <summary>
        /// Submission was superseded (i.e. a newer submission for the same message types and provider entered the 
        /// system before this submission could be sent out).
        /// </summary>
        Superseded = 7,

        /// <summary>
        /// Submission consists of a message that is not supported by a provider.
        /// </summary>
        NotSupported = 8
    }
}
