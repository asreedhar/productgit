using System;
using System.Collections.ObjectModel;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// The current status of a submission. If the submission has already been processed by the provider, this includes
    /// the successful and unsuccessful message types, as well as any error messages.    
    /// </summary>
    [Serializable]
    public class SubmissionStatus
    {
        ///<summary>
        /// Provider the submission was submitted to.
        ///</summary>
        public Provider Provider { get; set; }

        ///<summary>
        /// State of the submission.
        ///</summary>
        public SubmissionState State { get; set; }

        ///<summary>
        /// Errors associated with this submission.
        ///</summary>
        public Collection<SubmissionError> Errors
        {
            get { return _errors; }
        }
        private readonly Collection<SubmissionError> _errors;

        ///<summary>
        /// Successes associated with this submission.
        ///</summary>
        public Collection<SubmissionSuccess> Successes
        {
            get { return _successes; }
        }
        private readonly Collection<SubmissionSuccess> _successes;

        /// <summary>
        /// Exception messages tied to this submission.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Create a new submission status.
        /// </summary>
        public SubmissionStatus()
        {
            _successes = new Collection<SubmissionSuccess>();
            _errors = new Collection<SubmissionError>();
            Message = string.Empty;
        }
    }

    /// <summary>
    /// Details on the successful transfer of the given message types to a provider.
    /// </summary>
    [Serializable]
    public class SubmissionSuccess
    {
        ///<summary>
        /// Message types that were successfully sent.
        ///</summary>
        public Collection<MessageTypes> MessageTypes { get; internal set; }

        ///<summary>
        /// String description of the message types that were successfully sent.
        ///</summary>
        public string Description { get; internal set; }

        /// <summary>
        /// Create a new submission success. Empty public constructor needed for serialization since a custom 
        /// constructor has been defined.
        /// </summary>
        public SubmissionSuccess()
        {            
        }

        /// <summary>
        /// Create a new submission success.
        /// </summary>
        /// <param name="messageTypes">Message types that were successfully sent.</param>
        /// <param name="description">Description of the message types successfully sent.</param>
        public SubmissionSuccess(Collection<MessageTypes> messageTypes, string description)
        {
            MessageTypes = messageTypes;
            Description = description;
        }
    }

    /// <summary>
    /// Details on the unsuccessful transfer of the given message types to a provider.
    /// </summary>
    [Serializable]
    public class SubmissionError
    {
        /// <summary>
        /// Message types that were not successfully sent.
        /// </summary>
        public Collection<MessageTypes> MessageTypes { get; internal set; }

        /// <summary>
        /// String description of the message types that were not successfully sent.
        /// </summary>
        public string Description { get; internal set; }

        ///<summary>
        /// Error message associated with a failed submission.
        ///</summary>
        public string ErrorMessage { get; internal set; }

        /// <summary>
        /// Create a new submission error. Empty public constructor needed for serialization since a custom 
        /// constructor has been defined.        
        /// </summary>
        public SubmissionError()
        {            
        }

        /// <summary>
        /// Create a new submission error.
        /// </summary>
        /// <param name="messageTypes">Message types that were not successfully sent.</param>
        /// <param name="description">Description of the message types that were not successfully sent.</param>
        /// <param name="errorMessage">Error message.</param>
        public SubmissionError(Collection<MessageTypes> messageTypes, string description, string errorMessage)
        {
            MessageTypes = messageTypes;
            Description = description;
            ErrorMessage = errorMessage;
        }
    }
}
