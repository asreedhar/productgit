using System;
using System.Collections.ObjectModel;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Details of the submissions created for a message. Only contains the superficial data of what providers a 
    /// message will be sent out to, and nothing related to the success or failure of the submissions.
    /// </summary>
    [Serializable]
    public class SubmissionDetails
    {
        ///<summary>
        /// Identifier of the publication associated with the message that was distributed.
        ///</summary>
        public int PublicationId { get; set; }

        ///<summary>
        /// Collection of providers a message was distributed to.
        ///</summary>
        public Collection<Provider> Providers
        {
            get { return _providers; }
        }
        private readonly Collection<Provider> _providers = new Collection<Provider>();        
    }
}
