using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// The message that is to be distributed.
    /// </summary>
    [Serializable]
    public class Message
    {
        ///<summary>
        /// Dealer who this message is from.
        ///</summary>
        public Dealer From { get; set; }

        ///<summary>
        /// Vehicle that is the subject of this message.
        ///</summary>
        public Vehicle Subject { get; set; }

        ///<summary>
        /// Body of this message. The body is a collection of parts that define what's to be distributed. Can be price
        /// or mileage or text description or whathaveyou.
        ///</summary>
        public MessageBody Body { get; set; }
    }
}
