using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// The details of a provider that a message has been distributed to.
    /// </summary>
    [Serializable]
    public class Provider
    {
        /// <summary>
        /// Provider identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Provider name.
        /// </summary>
        public string Name { get; set; }
    }
}
