using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Types of parts in a message's body.
    /// </summary>
    [Serializable]
    public enum BodyPartType
    {
        /// <summary>
        /// Undefined body part type.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// Price body part.
        /// </summary>
        Price = 1,

        /// <summary>
        /// Advertisement body part.
        /// </summary>
        Advertisement = 2,

        /// <summary>
        /// Vehicle information body part.
        /// </summary>
        VehicleInformation = 3,

        /// <summary>
        /// Appraisa lValue
        /// </summary>
        AppraisalValue = 4
    }
}
