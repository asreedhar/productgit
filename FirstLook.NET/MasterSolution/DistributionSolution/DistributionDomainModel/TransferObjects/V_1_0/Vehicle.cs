using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Vehicle for which informatino is being submitted to a provider.
    /// </summary>
    [Serializable]
    public class Vehicle
    {
        ///<summary>
        /// Vehicle type.
        ///</summary>
        public VehicleType VehicleType { get; set; }

        ///<summary>
        /// Vehicle identifier.
        ///</summary>
        public int Id { get; set; }
    }
}
