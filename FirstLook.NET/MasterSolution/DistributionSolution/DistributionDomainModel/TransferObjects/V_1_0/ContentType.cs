using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Types of content in an advertisement body part.
    /// </summary>
    [Serializable]
    public enum ContentType
    {
        /// <summary>
        /// Undefined content type.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// Text content.
        /// </summary>
        Text = 1
    }
}
