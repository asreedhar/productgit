using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Types of vehicles.
    /// </summary>
    /// <remarks>
    /// We do not currently distribute anything for appraisal vehicles, and so no 'Appraisal' type is defined.
    /// </remarks>
    [Serializable]
    public enum VehicleType
    {
        /// <summary>
        /// Undefined vehicle type.
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// Inventory.
        /// </summary>
        Inventory = 1,

        /// <summary>
        /// Appraisal
        /// </summary>
        Appraisal = 2
    }
}