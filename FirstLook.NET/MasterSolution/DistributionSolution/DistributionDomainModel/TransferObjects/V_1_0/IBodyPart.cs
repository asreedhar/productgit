namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Interface for a part in a message's body.
    /// </summary>
    public interface IBodyPart
    {
        /// <summary>
        /// Body part type.
        /// </summary>
        BodyPartType BodyPartType { get; }
    }
}
