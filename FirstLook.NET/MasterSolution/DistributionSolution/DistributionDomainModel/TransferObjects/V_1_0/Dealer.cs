using System;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Dealer that defines the "from" of a message.
    /// </summary>
    [Serializable]
    public class Dealer
    {
        /// <summary>
        /// Dealer identifier.
        /// </summary>
        public int Id { get; set; }
    }
}
