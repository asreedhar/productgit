using System;
using System.Collections.ObjectModel;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Body of a message. The body is a collection of parts that define what's to be distributed. Can be price or 
    /// mileage or text description or whathaveyou.
    /// </summary>
    [Serializable]
    public class MessageBody
    {
        /// <summary>
        /// Message body parts.
        /// </summary>
        public Collection<BodyPart> Parts
        {
            get { return _parts; }            
        }
        private readonly Collection<BodyPart> _parts = new Collection<BodyPart>();        
    }
}
