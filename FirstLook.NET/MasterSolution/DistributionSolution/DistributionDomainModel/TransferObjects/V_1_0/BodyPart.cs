using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace FirstLook.Distribution.DomainModel.TransferObjects.V_1_0
{
    /// <summary>
    /// Abstract base class for the parts that define a message's body.
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(VehicleInformation))]
    [XmlInclude(typeof(Price))]
    [XmlInclude(typeof(Advertisement))]
    [XmlInclude(typeof(AppraisalValue))]
    public abstract class BodyPart
    {
        /// <summary>
        /// Body part type.
        /// </summary>
        public abstract BodyPartType BodyPartType { get; }        
    }

    /// <summary>
    /// Vehicle information body part.
    /// </summary>
    [Serializable]
    public class VehicleInformation : BodyPart
    {
        /// <summary>
        /// Vehicle mileage.
        /// </summary>
        public int Mileage { get; set; }

        /// <summary>
        /// Body part type.
        /// </summary>
        public override BodyPartType BodyPartType
        {
            get { return BodyPartType.VehicleInformation; }
        }
    }

    /// <summary>
    /// Types of prices.
    /// </summary>
    [Serializable]
    public enum PriceType
    {
        /// <summary>
        /// Undefined price type.
        /// </summary>
        NotDefined,

        /// <summary>
        /// MSRP.
        /// </summary>
        Lot,

        /// <summary>
        /// Internet-offered price.
        /// </summary>
        Internet,

        /// <summary>
        /// Appraisal value
        /// </summary>
        Appraisalvalue
    }

    /// <summary>
    /// Price body part.
    /// </summary>
    [Serializable]
    public class Price : BodyPart
    {
        ///<summary>
        /// Type of the price -- MSRP, lot, etc.
        ///</summary>
        public PriceType PriceType { get; set; }

        ///<summary>
        /// Price value.
        ///</summary>
        public int Value { get; set; }

        /// <summary>
        /// Body part type.
        /// </summary>
        public override BodyPartType BodyPartType
        {
            get { return BodyPartType.Price; }
        }
    }

    /// <summary>
    /// Advertisement body part. An advertisement is a collection of content.
    /// </summary>
    [Serializable]
    public class Advertisement : BodyPart
    {
        /// <summary>
        /// Contents that define the advertisement (e.g. text description, images, etc).
        /// </summary>
        public Collection<Content> Contents
        {
            get { return _contents; }            
        }
        private readonly Collection<Content> _contents = new Collection<Content>();

        /// <summary>
        /// Body part type.
        /// </summary>
        public override BodyPartType BodyPartType
        {
            get { return BodyPartType.Advertisement; }
        }
    }

    /// <summary>
    /// Appraisal Value body part.
    /// </summary>
    [Serializable]
    public class AppraisalValue : BodyPart
    {
        
        ///<summary>
        /// Price value.
        ///</summary>
        public int Value { get; set; }

        /// <summary>
        /// Body part type.
        /// </summary>
        public override BodyPartType BodyPartType
        {
            get { return BodyPartType.AppraisalValue; }
        }
    }
}
