using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Integration.Persistence
{    
    /// <summary>
    /// Test the account database code.
    /// </summary>
    [TestFixture]
    public class AccountDataGatewayTest
    {
        #region Initialization

        private AccountDataGateway _accountDataGateway;
        private int _validDealerId;
        private int _invalidDealerId;

        /// <summary>
        /// Setup work common to the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            _accountDataGateway = new AccountDataGateway();
            _validDealerId = TestUtilities.GetValidDealerIdentifier();
            _invalidDealerId = TestUtilities.GetInvalidDealerIdentifier();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {            
        }

        #endregion

        #region Account Tests
        
        /// <summary>
        /// Test the creation / fetching / updating / deleting of an account.
        /// </summary>        
        [Test]
        [ExpectedException(typeof(DataException))]
        public void AccountLifecycleTest()
        {            
            const int providerId = 2;
            const string dealershipCode = "Test Dealership Code";

            // Clear out existing account info for the given dealer and provider.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "ALTER TABLE Distribution.Account DISABLE TRIGGER TR_D_Account";
                    command.ExecuteNonQuery();

                    command.CommandText =
                        "DELETE FROM Distribution.Account " +
                        "WHERE DealerID = " + _validDealerId + " " +
                        "AND ProviderID = " + providerId;
                    command.ExecuteNonQuery();

                    command.CommandText =
                        "ALTER TABLE Distribution.Account ENABLE TRIGGER TR_D_Account";
                    command.ExecuteNonQuery();
                }
            }

            // Save the new account info to the database.
            Account account = Account.NewAccount(providerId, _validDealerId);
            account.UserName = "Test User";
            account.Password = "Test Password";
            account.DealershipCode = dealershipCode;
            _accountDataGateway.Save(account);

            // Now fetch that account back.
            Account fetchedAccount = _accountDataGateway.Fetch(providerId, _validDealerId);

            // Compare the two.
            Assert.AreEqual(account.ProviderId, fetchedAccount.ProviderId);
            Assert.AreEqual(account.DealerId, fetchedAccount.DealerId);
            Assert.AreEqual(account.UserName, fetchedAccount.UserName);
            Assert.AreEqual(account.Password, fetchedAccount.Password);
            Assert.AreEqual(account.DealershipCode, fetchedAccount.DealershipCode);

            // Delete that account.
            _accountDataGateway.Delete(account);

            // Create an account with null username, password and dealership codes.
            account = Account.NewAccount(providerId, _validDealerId);
            account.DealershipCode = dealershipCode;
            _accountDataGateway.Save(account);

            // Now fetch that account again.
            fetchedAccount = _accountDataGateway.Fetch(providerId, _validDealerId);

            // Compare the fetched account.
            Assert.AreEqual(string.Empty, fetchedAccount.UserName);
            Assert.AreEqual(string.Empty, fetchedAccount.Password);
            Assert.AreEqual(dealershipCode, fetchedAccount.DealershipCode);   

            // Now delete it again.
            _accountDataGateway.Delete(account);

            // And finally verify that it's gone - should throw an exception.            
            _accountDataGateway.Fetch(providerId, _validDealerId);            
        }        

        #endregion

        #region Account Preferences Tests

        /// <summary>
        /// Test the deleting / fetching / saving of account preferences.
        /// </summary>
        [Test]
        public void AccountPreferenceTest()
        {
            // Set account preferences for the valid dealer identifier.
            AccountPreferenceDictionary prefs = new AccountPreferenceDictionary(_validDealerId, "Bogus Dealer Name");
            Provider provider = new Provider(2, "");
            
            MessageTypePreference pref1 = new MessageTypePreference(provider.Id, (MessageTypes)2, true);
            MessageTypePreference pref2 = new MessageTypePreference(provider.Id, (MessageTypes)1, true);
            MessageTypePreference pref3 = new MessageTypePreference(provider.Id, (MessageTypes)3, true);

            prefs.Add(provider, pref1);
            prefs.Add(provider, pref2);
            prefs.Add(provider, pref3);

            // Save account preferences.
            _accountDataGateway.SaveAccountPreferences(prefs);

            // Fetch these account preferences.
            AccountPreferenceDictionary fetchedPrefs = _accountDataGateway.FetchAccountPreferences(_validDealerId);

            // Save a single row.
            _accountDataGateway.SaveAccountPreference(_validDealerId, provider.Id, fetchedPrefs[provider]);

            // Fetch the preferences again.
            fetchedPrefs = _accountDataGateway.FetchAccountPreferences(_validDealerId);

            // Check the dealer identifiers.
            Assert.AreEqual(prefs.DealerId, fetchedPrefs.DealerId);

            // Check the account preferences.
            foreach (KeyValuePair<Provider, Collection<MessageTypePreference>> pref in prefs)
            {
                if (!fetchedPrefs.ContainsKey(pref.Key))
                {
                    Assert.Fail();
                }
                else
                {
                    Collection<MessageTypePreference> messageTypeList = pref.Value;
                    Collection<MessageTypePreference> fetchedMessageTypeList = fetchedPrefs[pref.Key];

                    Assert.AreEqual(messageTypeList.Count, fetchedMessageTypeList.Count);

                    foreach (MessageTypePreference preference in messageTypeList)
                    {
                        bool matchedPref = false;

                        foreach (MessageTypePreference fetchedPreference in fetchedMessageTypeList)
                        {
                            if (preference.MessageTypes == fetchedPreference.MessageTypes &&
                                preference.Description == fetchedPreference.Description &&
                                preference.ProviderId == fetchedPreference.ProviderId &&
                                preference.Enabled == fetchedPreference.Enabled)
                            {
                                matchedPref = true;
                            }
                        }

                        if (!matchedPref)
                        {
                            Assert.Fail();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Test that account preferences explode if fetched for a bogus dealer.
        /// </summary>
        [Test]
        [ExpectedException(typeof(SqlException))]
        public void AccountPreferenceFailsOnBadDealer()
        {
            _accountDataGateway.FetchAccountPreferences(_invalidDealerId);
        }

        #endregion

        #region Dealer Tests

        /// <summary>
        /// Test that the Distribution.Dealer#Exists proc behaves as expected.
        /// </summary>
        [Test]
        public void DealerExistsTest()
        {
            Assert.IsFalse(_accountDataGateway.DealerExists(_invalidDealerId));
            Assert.IsTrue(_accountDataGateway.DealerExists(_validDealerId));
        }

        /// <summary>
        /// Test that the check for whether the dealer is in Extract.DealerConfiguration works.
        /// </summary>
        [Test]
        public void DealerHasAultecExtraction()
        {            
            // Ensure the database is ready for the test.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Ensure there's AutoUplink account data.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;                    
                    command.CommandText =
                        "IF NOT EXISTS" +
                            "(SELECT * FROM Extract.DealerConfiguration " +
                            "WHERE BusinessUnitID = " + _validDealerId + " " +
                            "AND DestinationName = 'AULTec') " +
                        "INSERT INTO Extract.DealerConfiguration " +
                        "(BusinessUnitID, DestinationName, ExternalIdentifier, StartDate, Active, IncludeHistorical, InsertDate, InsertUser, UpdateDate, UpdateUser)" +
                        "VALUES (" + _validDealerId + ", 'AULTec', 'IDENTIFIER', getdate(), 1, 0, getdate(), 'bfung', getdate(), 'bfung') " +
                        "ELSE UPDATE Extract.DealerConfiguration SET Active=1 WHERE BusinessUnitID=" + _validDealerId;

                    command.ExecuteNonQuery();
                }
            }

            Assert.IsTrue(_accountDataGateway.DealerRequiresInternetPriceUpdate(_validDealerId));

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Ensure there's AutoUplink account data.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "UPDATE Extract.DealerConfiguration SET Active = 0 WHERE BusinessUnitID=" + _validDealerId;

                    command.ExecuteNonQuery();
                }
            }

            Assert.IsFalse(_accountDataGateway.DealerRequiresInternetPriceUpdate(_validDealerId));

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Ensure there's AutoUplink account data.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "DELETE FROM Extract.DealerConfiguration WHERE BusinessUnitID=" + _validDealerId;

                    command.ExecuteNonQuery();
                }
            }

            Assert.IsFalse(_accountDataGateway.DealerRequiresInternetPriceUpdate(_validDealerId));
        }

        #endregion

        #region User Identity Tests

        /// <summary>
        /// Test that the checks on the validity of usernames works as expected.
        /// </summary>
        [Test]
        public void UserIdentityExistsTest()
        {
            string validUserName;            

            // Get a valid username from the database.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT TOP 1 Login FROM dbo.Member ORDER BY MemberID DESC";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            validUserName = reader.GetString(reader.GetOrdinal("Login"));
                        }
                        else
                        {
                            Assert.Fail("Expected valid user name, but didn't get one. DRAT.");
                            return;
                        }
                    }
                }
            }

            Assert.IsTrue(_accountDataGateway.UserIdentityExists(validUserName));

            string bogusUserName = Guid.NewGuid().ToString();
            Assert.IsFalse(_accountDataGateway.UserIdentityExists(bogusUserName));
        }

        #endregion
    }
}
