using System.Data;
using System.Security.Principal;
using System.Threading;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Integration.Persistence
{
    /// <summary>
    /// Test that publication data is properly saved to the database.
    /// </summary>
    [TestFixture]
    public class PublicationDataGatewayTest
    {
        #region Initialization

        private IPublicationDataGateway _publicationDataGateway;

        /// <summary>
        /// Setup work common to the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<IAccountDataGateway,     AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, PublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     VehicleDataGateway>(ImplementationScope.Shared);

            _publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();

            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("bfung", "SOAP"), new string[0]);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        #endregion

        #region Publication Tests

        /// <summary>
        /// Test that a message with empty text is stored correctly to the database.
        /// FOGBUGZ 11256.
        /// </summary>
        [Test]
        public void EmptyTextTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TextEmpty.xml");
            Publication publication = Publication.NewPublication(messageTO);

            _publicationDataGateway.Save(publication);

            // Check if the text is null in the database.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
               
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "select ct.Text " +
                        "from Distribution.ContentText ct " +
                        "join Distribution.Advertisement_Content ac on ac.ContentID = ct.ContentID " +
                        "join Distribution.Message m on m.MessageID = ac.MessageID " +
                        "join Distribution.Publication p on p.MessageID = m.MessageID " +
                        "where p.PublicationID = " + publication.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail("Expected publication from the database.");
                        }

                        Assert.IsTrue(reader.IsDBNull(reader.GetOrdinal("Text")));
                    }
                }
            }

            // Now go refetch the publication and make sure the text is empty.
            Publication publication2 = _publicationDataGateway.Fetch(publication.Id);

            Message message = publication2.Message;
            TextContent textContent = null;
            
            foreach (IBodyPart bodyPart in message.Body)
            {
                Advertisement advertisement = bodyPart as Advertisement;
                if (advertisement != null)
                {
                    foreach (Content content in advertisement.Contents)
                    {
                        textContent = content as TextContent;
                        if (textContent != null)
                        {
                            Assert.AreEqual(string.Empty, textContent.Text);
                        }
                    }
                }
            }

            if (textContent == null)
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// BUGZID: 15840
        /// Test that a message with a vehicle that is not actually active inventory has the vehicle marked correctly
        /// as "Not Defined".
        /// </summary>
        [Test]
        public void NonInventoryVehicleTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            messageTO.Subject.Id = -999999999;
            Publication publication = Publication.NewPublication(messageTO);

            _publicationDataGateway.Save(publication);

            // Get the vehicle type of this newly saved publication.
            // Get the identifier of this newly saved publication.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Test the data in the publication and message tables.                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "SELECT V.VehicleEntityTypeID " +
                        "FROM Distribution.Vehicle V " +
                        "JOIN Distribution.Message M ON M.VehicleEntityID = V.VehicleEntityID " +
                        "JOIN Distribution.Publication P ON P.MessageID = M.MessageID " +
                        "WHERE P.PublicationID = " + publication.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.IsFalse(true);
                        }

                        Assert.AreEqual((int)VehicleEntityType.NotDefined, 
                                        reader.GetInt32(reader.GetOrdinal("VehicleEntityTypeID")));
                    }
                }
            }
        }

        /// <summary>
        /// BUGZID: 16441
        /// Test the unique constraint on the table IMT.Distribution.Vehicle. Vehicles should be unique by identifier
        /// and type, not just identifier. Previously, if a vehicle came in as Inventory, and then later as Not 
        /// Inventory (e.g. the vehicle had already been sold and was no longer active inventory), the logging of the 
        /// non-inventory vehicle would fail because there already was a vehicle in the table with that identifier.        
        /// </summary>
        [Test]
        public void VehiclesOfDifferentTypesTest()
        {
            int vehicleId;

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Get a test vehicle id.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT MIN(VehicleEntityID) AS VehicleEntityID FROM Distribution.Vehicle";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail();
                            return;
                        }
                        vehicleId = reader.GetInt32(reader.GetOrdinal("VehicleEntityID"));
                    }
                }

                vehicleId--;

                // Add the test vehicle as inventory.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "INSERT INTO Distribution.Vehicle " +
                        "(VehicleEntityID, VehicleEntityTypeID, InsertUserID, InsertDate) " +
                        "VALUES (" + vehicleId +", 1, 100000, GETDATE())";

                    command.ExecuteNonQuery();
                }
            }

            // Save the vehicle. Hopefully the identifier we chose will not be active inventory. This will cause the 
            // system to try to save it as non-active inventory.
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            messageTO.Subject.Id = vehicleId;
            Publication publication = Publication.NewPublication(messageTO);
            _publicationDataGateway.Save(publication);

            // Make sure the vehicle is in the database.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Test the data in the publication and message tables.                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "SELECT V.VehicleEntityTypeID " +
                        "FROM Distribution.Vehicle V " +
                        "JOIN Distribution.Message M ON M.VehicleEntityID = V.VehicleEntityID " +
                        "JOIN Distribution.Publication P ON P.MessageID = M.MessageID " +
                        "WHERE P.PublicationID = " + publication.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.IsFalse(true);
                        }

                        Assert.AreEqual((int)VehicleEntityType.NotDefined,
                                        reader.GetInt32(reader.GetOrdinal("VehicleEntityTypeID")));
                    }
                }
            }
        }

        /// <summary>
        /// Test that a publication is correctly updated when the status is changed.
        /// </summary>
        [Test]
        public void PublicationLifecycleTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            Publication publication = Publication.NewPublication(messageTO);

            _publicationDataGateway.Save(publication);

            int publicationId;

            // Get the identifier of this newly saved publication.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Test the data in the publication and message tables.                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "SELECT TOP 1 PublicationID " +
                        "FROM Distribution.Publication " +
                        "ORDER BY PublicationID DESC";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.IsFalse(true);
                        }

                        publicationId = reader.GetInt32(reader.GetOrdinal("PublicationID"));
                    }
                }
            }

            // Fetch the publication with that identifier.            
            Publication fetchedPublication = _publicationDataGateway.Fetch(publicationId);

            // Test that the saved publication is the same as the fetched publication.
            Assert.AreEqual(publication.Id,                   fetchedPublication.Id);
            Assert.AreEqual(publication.PublicationStatus,    fetchedPublication.PublicationStatus);
            Assert.AreEqual(publication.Message.Id,           fetchedPublication.Message.Id);
            Assert.AreEqual(publication.Message.MessageTypes, fetchedPublication.Message.MessageTypes);
            Assert.AreEqual(publication.Message.Dealer.Id,    fetchedPublication.Message.Dealer.Id);            
            
            Assert.AreEqual(publication.Message.Vehicle.VehicleEntityId, fetchedPublication.Message.Vehicle.VehicleEntityId);
            Assert.AreEqual(publication.Message.Vehicle.VehicleEntityType, fetchedPublication.Message.Vehicle.VehicleEntityType);

            Advertisement advertisement = null;
            Price price = null;
            VehicleInformation vehicleInformation = null;            

            foreach (IBodyPart bodyPart in publication.Message.Body)
            {
                switch (bodyPart.BodyPartType)
                {
                    case BodyPartType.Advertisement:
                        advertisement = bodyPart as Advertisement;
                        break;

                    case BodyPartType.Price:
                        price = bodyPart as Price;
                        break;

                    case BodyPartType.VehicleInformation:
                        vehicleInformation = bodyPart as VehicleInformation;
                        break;
                }                
            }

            if (advertisement == null || price == null || vehicleInformation == null)
            {
                Assert.Fail("Message values were not present as expected.");                
            }

            Advertisement fetchedAdvertisement = null;
            Price fetchedPrice = null;
            VehicleInformation fetchedVehicleInformation = null;

            foreach (IBodyPart bodyPart in fetchedPublication.Message.Body)
            {
                switch (bodyPart.BodyPartType)
                {
                    case BodyPartType.Advertisement:
                        fetchedAdvertisement = bodyPart as Advertisement;
                        break;

                    case BodyPartType.Price:
                        fetchedPrice = bodyPart as Price;
                        break;

                    case BodyPartType.VehicleInformation:
                        fetchedVehicleInformation = bodyPart as VehicleInformation;
                        break;
                }
            }

            if (fetchedAdvertisement == null || fetchedPrice == null || fetchedVehicleInformation == null)
            {
                Assert.Fail("Message values were not retrieved from the database.");
            }

            Assert.AreEqual(price.PriceType, fetchedPrice.PriceType);
            Assert.AreEqual(price.Value, fetchedPrice.Value);
            Assert.AreEqual(vehicleInformation.Mileage, fetchedVehicleInformation.Mileage);

            TextContent textContent = null;
            TextContent fetchedTextContent = null;

            foreach (Content content in advertisement.Contents)
            {                
                textContent = content as TextContent;
                if (textContent != null)
                {
                    break;
                }
            }

            foreach (Content content in fetchedAdvertisement.Contents)
            {
                fetchedTextContent = content as TextContent;
                if (fetchedTextContent != null)
                {
                    break;
                }
            }

            if (textContent == null || fetchedTextContent == null)
            {
                Assert.Fail("Text content values were not retrieved from the database.");
            }

            Assert.AreEqual(textContent.Text, fetchedTextContent.Text);

            // Update the publication and check that it was correctly saved.
            publication.Online();

            // Get the identifier of this newly saved publication.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Test the data in the publication and message tables.                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "SELECT TOP 1 PublicationStatusID " +
                        "FROM Distribution.Publication " +
                        "WHERE PublicationID = " + publication.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.IsFalse(true);
                        }

                        Assert.AreEqual((int)PublicationState.Online, reader.GetInt32(reader.GetOrdinal("PublicationStatusID")));
                    }
                }
            }
        }

        #endregion

        #region Status Change Tests

        /// <summary>
        /// Test that a publication is correctly marked as 'Offline' when told to retract.
        /// </summary>
        [Test]
        public void RetractTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            Publication publication = Publication.NewPublication(messageTO);            
            _publicationDataGateway.Save(publication);

            publication.Retract();
            
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT PublicationStatusID FROM Distribution.Publication WHERE PublicationID=" + publication.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            PublicationState status = (PublicationState)reader.GetInt32(reader.GetOrdinal("PublicationStatusID"));
                            Assert.AreEqual(PublicationState.Offline, status);
                        }
                        else
                        {
                            Assert.Fail("Expected publication from the database, but it wasn't there.");                            
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Test that a publication is correctly marked as 'Online' when told to go online.
        /// </summary>
        [Test]
        public void OnlineTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            Publication publication = Publication.NewPublication(messageTO);            
            _publicationDataGateway.Save(publication);

            publication.Online();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT PublicationStatusID FROM Distribution.Publication WHERE PublicationID=" + publication.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            PublicationState status = (PublicationState)reader.GetInt32(reader.GetOrdinal("PublicationStatusID"));
                            Assert.AreEqual(PublicationState.Online, status);
                        }
                        else
                        {
                            Assert.Fail("Expected publication from the database, but it wasn't there.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Test that a publication is correctly marked as 'Not Applicable' when told to be not distributed.
        /// </summary>
        [Test]
        public void NotDistributedTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            Publication publication = Publication.NewPublication(messageTO);            
            _publicationDataGateway.Save(publication);

            publication.NotDistributed();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT PublicationStatusID FROM Distribution.Publication WHERE PublicationID=" + publication.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            PublicationState status = (PublicationState)reader.GetInt32(reader.GetOrdinal("PublicationStatusID"));
                            Assert.AreEqual(PublicationState.NotApplicable, status);
                        }
                        else
                        {
                            Assert.Fail("Expected publication from the database, but it wasn't there.");
                        }
                    }
                }
            }
        }

        #endregion
    }
}
