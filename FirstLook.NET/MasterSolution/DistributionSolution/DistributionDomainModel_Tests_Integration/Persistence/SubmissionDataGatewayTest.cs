using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Security.Principal;
using System.Threading;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.AutoTrader;
using FirstLook.Distribution.DomainModel.Bindings.AutoUplink;
using FirstLook.Distribution.DomainModel.Bindings.GetAuto;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Services.GetAuto;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Integration.Persistence
{
    /// <summary>
    /// Test that submissions are properly handled in the database.
    /// </summary>
    [TestFixture]
    public class SubmissionDataGatewayTest
    {
        #region Initialization

        /// <summary>
        /// Setup work common to all tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<IAccountDataGateway,     AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, PublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  SubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     VehicleDataGateway>(ImplementationScope.Shared);

            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("bfung", "SOAP"), new string[0]);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        #endregion

        #region Submission Tests

        /// <summary>
        /// Test that a submission is saved to the database properly.
        /// </summary>
        [Test]
        public void SubmissionSaveTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            publicationDataGateway.Save(publication);

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();
                Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);                
                submissionDataGateway.Save(submission);

                // Test the data in the publication and message tables.                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "SELECT * " +
                        "FROM Distribution.Submission " +
                        "WHERE SubmissionID = " + submission.Id;                        

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.IsFalse(true);
                        }

                        Assert.AreEqual(publication.Id, reader.GetInt32(reader.GetOrdinal("PublicationID")));
                        Assert.AreEqual((int)(MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage), 
                            reader.GetInt32(reader.GetOrdinal("MessageTypes")));
                    }
                }
            }            
        }

        /// <summary>
        /// Test that a submission which is given a more limited set of messages types than what is in the xml it is
        /// created from accurately saves and retrieves those limited message parts.
        /// </summary>
        /// <remarks>
        /// Fogbugz 12530.
        /// </remarks>
        [Test]
        public void MessageFilteringTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            publicationDataGateway.Save(publication);

            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();
            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price);
            submissionDataGateway.Save(submission);

            Message message = publicationDataGateway.Fetch(submission.PublicationId, submission.MessageTypes);

            Price price = message.GetPrice();
            VehicleInformation vehicleInformation = message.GetVehicleInformation();
            TextContent textContent = message.GetDescription();

            Assert.IsNotNull(price);
            Assert.IsNull(vehicleInformation);
            Assert.IsNull(textContent);
        }
       
        /// <summary>
        /// Test that the submission stale check works by putting multiple submissions into the database one after the 
        /// other and checking which are stale. Submissions without different message types cannot make each other 
        /// stale.
        /// </summary>
        [Test]
        public void SubmissionStaleTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);
            Publication publication2 = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);
            Thread.Sleep(100); // To cause separation.
            publicationDataGateway.Save(publication2);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);
            Submission submission2 = Submission.NewSubmission(2, publication2.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            submissionDataGateway.Save(submission);
            Thread.Sleep(100); // To cause separation.
            submissionDataGateway.Save(submission2);

            MessageTypes staleTypes1 = submissionDataGateway.FetchStaleTypes(submission.Id);
            MessageTypes staleTypes2 = submissionDataGateway.FetchStaleTypes(submission2.Id);

            Assert.IsTrue(staleTypes1 == (MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage));
            Assert.IsTrue(staleTypes2 == 0);

            Publication publication3 = Publication.NewPublication(messageTO);
            Publication publication4 = Publication.NewPublication(messageTO);

            publicationDataGateway.Save(publication3);
            Thread.Sleep(100); // To cause separation.
            publicationDataGateway.Save(publication4);

            Submission submission3 = Submission.NewSubmission(2, publication3.Id, MessageTypes.Price);
            Submission submission4 = Submission.NewSubmission(2, publication4.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            submissionDataGateway.Save(submission3);
            Thread.Sleep(100); // To cause separation.
            submissionDataGateway.Save(submission4);

            staleTypes1 = submissionDataGateway.FetchStaleTypes(submission.Id);
            staleTypes2 = submissionDataGateway.FetchStaleTypes(submission2.Id);
            MessageTypes staleTypes3 = submissionDataGateway.FetchStaleTypes(submission3.Id);
            MessageTypes staleTypes4 = submissionDataGateway.FetchStaleTypes(submission4.Id);

            Assert.IsTrue(staleTypes1 == (MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage));
            Assert.IsTrue(staleTypes2 == (MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage));
            Assert.IsTrue(staleTypes3 == MessageTypes.Price);
            Assert.IsTrue(staleTypes4 == 0);
        }         

        /// <summary>
        /// Test that the superseding price is correctly returned when called.
        /// </summary>
        [Test]
        public void SupersedingPriceTest()
        {
            TransferObjects.V_1_0.Message messageTO1 = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            TransferObjects.V_1_0.Message messageTO2 = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            TransferObjects.V_1_0.Message messageTO3 = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            TestUtilities.RandomizePrice(messageTO1);
            Thread.Sleep(100); // To cause separation in the random number generator.
            TestUtilities.RandomizePrice(messageTO2);
            Thread.Sleep(100); // To cause separation in the random number generator.
            TestUtilities.RandomizePrice(messageTO3);
              
            Publication publication1 = Publication.NewPublication(messageTO1);
            Publication publication2 = Publication.NewPublication(messageTO2);
            Publication publication3 = Publication.NewPublication(messageTO3);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();            

            publicationDataGateway.Save(publication1);
            publicationDataGateway.Save(publication2);
            publicationDataGateway.Save(publication3);

            Submission submission1 = Submission.NewSubmission(2, publication1.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);
            Submission submission2 = Submission.NewSubmission(2, publication2.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);
            Submission submission3 = Submission.NewSubmission(2, publication3.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            submissionDataGateway.Save(submission1);
            Thread.Sleep(100); // To cause separation.
            submissionDataGateway.Save(submission2);
            Thread.Sleep(100); // To cause separation.
            submissionDataGateway.Save(submission3);

            Price price0 = submissionDataGateway.FetchSupersedingPrice(submission1.Id);
            Price price1 = publication1.Message.GetPrice();
            Price price2 = publication2.Message.GetPrice();
            Price price3 = publication3.Message.GetPrice();

            Assert.IsNotNull(price0);
            Assert.IsNotNull(price1);
            Assert.IsNotNull(price2);
            Assert.IsNotNull(price3);

            Assert.AreNotEqual(price1.Value, price2.Value);
            Assert.AreNotEqual(price1.Value, price3.Value);
            Assert.AreEqual(price0.Value, price3.Value);
        }

        #endregion

        #region Submission Status Tests

        /// <summary>
        /// Check that a submission can be correctly set as 'In Progress'.
        /// </summary>
        [Test]
        public void InProgressTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);            

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);            

            submissionDataGateway.Save(submission);

            submission.InProgress();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT SubmissionStatusID FROM Distribution.Submission WHERE SubmissionID=" + submission.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            Assert.AreEqual(SubmissionState.Processing, status);
                        }
                        else
                        {
                            Assert.Fail("Expected submission from the database, but it wasn't there.");
                        }
                    }
                }
            }            
        }

        /// <summary>
        /// Check that a submission can be correctly set as 'Superseded'.
        /// </summary>
        [Test]
        public void SupersededTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);            

            submissionDataGateway.Save(submission);

            submission.Superseded();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT SubmissionStatusID FROM Distribution.Submission WHERE SubmissionID=" + submission.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            Assert.AreEqual(SubmissionState.Superseded, status);
                        }
                        else
                        {
                            Assert.Fail("Expected submission from the database, but it wasn't there.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check that a submission can be correctly set as 'Submitted'.
        /// </summary>
        [Test]
        public void SubmittedTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);            

            submissionDataGateway.Save(submission);

            submission.Submitted();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT SubmissionStatusID FROM Distribution.Submission WHERE SubmissionID=" + submission.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            Assert.AreEqual(SubmissionState.Submitted, status);
                        }
                        else
                        {
                            Assert.Fail("Expected submission from the database, but it wasn't there.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check that a submission can be correctly set as 'Rejected'.
        /// </summary>
        [Test]
        public void RejectedTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);            

            submissionDataGateway.Save(submission);

            submission.Rejected();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT SubmissionStatusID FROM Distribution.Submission WHERE SubmissionID=" + submission.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            Assert.AreEqual(SubmissionState.Rejected, status);
                        }
                        else
                        {
                            Assert.Fail("Expected submission from the database, but it wasn't there.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check that a submission can be correctly set as 'Accepted'.
        /// </summary>
        [Test]
        public void AcceptedTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);            

            submissionDataGateway.Save(submission);

            submission.Accepted();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT SubmissionStatusID FROM Distribution.Submission WHERE SubmissionID=" + submission.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            Assert.AreEqual(SubmissionState.Accepted, status);
                        }
                        else
                        {
                            Assert.Fail("Expected submission from the database, but it wasn't there.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check that a submission can be correctly set as 'NotSupported'.
        /// </summary>
        [Test]
        public void NotSupportedTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);            

            submissionDataGateway.Save(submission);

            submission.NotSupported();

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT SubmissionStatusID FROM Distribution.Submission WHERE SubmissionID=" + submission.Id;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SubmissionState status = (SubmissionState)reader.GetInt32(reader.GetOrdinal("SubmissionStatusID"));
                            Assert.AreEqual(SubmissionState.NotSupported, status);
                        }
                        else
                        {
                            Assert.Fail("Expected submission from the database, but it wasn't there.");
                        }
                    }
                }
            }
        }

        #endregion

        #region Error Tests

        /// <summary>
        /// Test that an AutoUplink error is correctly inserted into the database.
        /// </summary>
        [Test]
        public void LogAutoUplinkErrorTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);            

            submissionDataGateway.Save(submission);
            
            AutoUplinkError autoUplinkError = new AutoUplinkError(submission.Id, 7, "401");
            submission.Rejected();
            submissionDataGateway.Save(submission);
            submissionDataGateway.LogError(autoUplinkError);

            Collection<FirstLookError> fetchedErrors = submissionDataGateway.FetchErrors(submission.Id);

            Assert.AreEqual(1, fetchedErrors.Count);
            Assert.AreEqual(fetchedErrors[0].FirstLookErrorMessage, "Authentication Error");
        }

        /// <summary>
        /// Test that an AutoTrader error is correctly inserted into the database.
        /// </summary>
        [Test]
        public void LogAutoTraderErrorTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(1, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            submissionDataGateway.Save(submission);

            Exception exception = new Exception("Field Required (VIN)");
            AutoTraderError autoTraderError = new AutoTraderError(submission.Id, 7, exception);
            submission.Rejected();
            submissionDataGateway.Save(submission);
            submissionDataGateway.LogError(autoTraderError);

            Collection<FirstLookError> fetchedErrors = submissionDataGateway.FetchErrors(submission.Id);

            Assert.AreEqual(1, fetchedErrors.Count);
            Assert.AreEqual(fetchedErrors[0].FirstLookErrorMessage, "Missing Field");
        }

        /// <summary>
        /// Test that a GetAuto error is correctly inserted into the database.
        /// </summary>
        [Test]
        public void LogGetAutoErrorTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(4, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            submissionDataGateway.Save(submission);

            GetAutoError getAutoError = new GetAutoError(submission.Id, 7, ReturnResult.InvalidDealerLotKey);
            submission.Rejected();
            submissionDataGateway.Save(submission);
            submissionDataGateway.LogError(getAutoError);

            Collection<FirstLookError> fetchedErrors = submissionDataGateway.FetchErrors(submission.Id);

            Assert.AreEqual(1, fetchedErrors.Count);
            Assert.AreEqual(fetchedErrors[0].FirstLookErrorMessage, "Dealer Not Found");
        }

        #endregion

        #region Success Tests

        /// <summary>
        /// Test that a success is correctly inserted into the database.
        /// </summary>
        [Test]
        public void LogSuccessTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            Publication publication = Publication.NewPublication(messageTO);

            IPublicationDataGateway publicationDataGateway = RegistryFactory.GetResolver().Resolve<IPublicationDataGateway>();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            publicationDataGateway.Save(publication);

            Submission submission = Submission.NewSubmission(2, publication.Id, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            submissionDataGateway.Save(submission);            

            submissionDataGateway.LogSuccess(submission.Id, (int)MessageTypes.Price);

            Collection<int> fetchedSuccesses = submissionDataGateway.FetchSuccesses(submission.Id);

            Assert.AreEqual(1, fetchedSuccesses.Count);
            Assert.AreEqual(fetchedSuccesses[0], (int)MessageTypes.Price);
        }

        #endregion
    }
}
