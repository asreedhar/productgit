﻿using System;
using System.Data;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Integration.Persistence
{
    /// <summary>
    /// Integration tests on the exception logging functionality.
    /// </summary>
    [TestFixture]
    public class ExceptionLoggerTest
    {
        #region Initialization

        /// <summary>
        /// Setup work common to all tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("bfung", "SOAP"), new string[0]);
            registry.Register<IAccountDataGateway,    AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway, SubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,    VehicleDataGateway>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        #endregion

        #region Exception Logging Tests

        /// <summary>
        /// Test that an exception is properly saved and retrieved when not given a request message or submission 
        /// identifier.
        /// </summary>
        [Test]
        public void ExceptionTest()
        {
            Random random = new Random(DateTime.Now.Millisecond);
            int randomNumber = random.Next();

            Exception exception = new Exception(randomNumber.ToString());            
            ExceptionLogger logger = new ExceptionLogger();
            logger.Record(exception);
            
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT TOP 1 Message FROM Distribution.Exception ORDER BY ExceptionTime DESC";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail("Exception was not logged to the database.");
                        }
                        try
                        {
                            int fetchedNumber = int.Parse(reader.GetString(reader.GetOrdinal("Message")));
                            Assert.AreEqual(randomNumber, fetchedNumber);
                        }
                        catch (Exception)
                        {
                            Assert.Fail("Exception message did not match expected value.");
                        }                        
                    }
                }
            }
        }

        /// <summary>
        /// Test that an exception is properly saved and retrieved when given a request message.
        /// </summary>
        [Test]
        public void ExceptionWithMessageTest()
        {
            ExceptionLogger logger = new ExceptionLogger();

            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            
            // Serialize the message into a string.            
            StringBuilder stringBuilder = new StringBuilder();
            StringWriter writer = new StringWriter(stringBuilder);
            XmlSerializer serializer = new XmlSerializer(typeof(TransferObjects.V_1_0.Message));
            serializer.Serialize(writer, messageTO);

            // Save an exception with a random number (to help with verifying it).
            Random random = new Random(DateTime.Now.Millisecond);
            int randomNumber = random.Next();
            Exception exception = new Exception(randomNumber.ToString());
            logger.Record(exception, writer.ToString());

            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT TOP 1 Message, RequestMessage FROM Distribution.Exception ORDER BY ExceptionTime DESC";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail("Exception was not logged to the database.");
                        }
                        try
                        {
                            int fetchedNumber = int.Parse(reader.GetString(reader.GetOrdinal("Message")));
                            Assert.AreEqual(randomNumber, fetchedNumber);

                            string requestMessage = reader.GetString(reader.GetOrdinal("RequestMessage"));
                            Assert.AreEqual(requestMessage, writer.ToString());
                        }
                        catch (Exception)
                        {
                            Assert.Fail("Exception message did not match expected value.");
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Test that an exception is properly saved and retrieved when given a submission identifier..
        /// </summary>
        [Test]
        public void ExceptionWithSubmissionTest()
        {
            ExceptionLogger logger = new ExceptionLogger();
            PublicationDataGateway pubDataGateway = new PublicationDataGateway();
            ISubmissionDataGateway submissionDataGateway = RegistryFactory.GetResolver().Resolve<ISubmissionDataGateway>();

            // Save a publication to the database so we have one to associate with an exception.
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            Publication publication = Publication.NewPublication(messageTO);                        
            pubDataGateway.Save(publication);

            Submission submission = publication.CreateSubmission(1, publication.Message.MessageTypes);
            submissionDataGateway.Save(submission);

            // Save an exception with a random number (to help with verifying it).
            Random random = new Random(DateTime.Now.Millisecond);
            int randomNumber = random.Next();
            Exception exception = new Exception(randomNumber.ToString());            
            logger.Record(exception, submission.Id);

            // Fetch the exception from the db and compare.
            string exceptionMessage = logger.Fetch(submission.Id);
            try
            {
                int fetchedNumber = int.Parse(exceptionMessage);
                Assert.AreEqual(randomNumber, fetchedNumber);
            }
            catch (Exception)
            {
                Assert.Fail("Exception message did not match expected value.");
            }
        }

        #endregion
    }
}
