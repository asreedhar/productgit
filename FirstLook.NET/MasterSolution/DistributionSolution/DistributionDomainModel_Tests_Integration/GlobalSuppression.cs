﻿using System.Diagnostics.CodeAnalysis;


/* ====================== */
/* = Nunit Suppressions = */
/* ====================== */

[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.AccountDataGatewayTest.#FixtureTeardown()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.PublicationDataGatewayTest.#FixtureTeardown()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.SubmissionDataGatewayTest.#LogSuccessTest()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.SubmissionDataGatewayTest.#LogErrorTest()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.SubmissionDataGatewayTest.#SubmissionSaveTest()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.SubmissionDataGatewayTest.#FixtureTeardown()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.SubmissionDataGatewayTest.#SubmissionStaleTest()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence.SubmissionDataGatewayTest.#FixtureSetup()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.EndToEnd.EndToEndTest.#FullEndToEndTest()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.EndToEnd.EndToEndTest.#FixtureTeardown()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.EndToEnd.EndToEndTest.#FixtureSetup()", Justification = "Method signature is need by Nunit.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.EndToEnd.EndToEndTest.#TestSetup()", Justification = "Method signature is need by Nunit.")]

/* ========================== */
/* = Namespace Suppressions = */
/* ========================== */

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.Persistence", Justification = "Namespace is left intentionally limited.")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "FirstLook.Distribution.DomainModel.Tests.Integration.EndToEnd", Justification = "Namespace is left intentionally limited.")]