﻿using System.Configuration;
using System.Data;
using System.Threading;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.Services.AutoTrader;
using FirstLook.Distribution.DomainModel.Services.AutoUplink;
using FirstLook.Distribution.DomainModel.Services.GetAuto;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Integration.EndToEnd
{
    /// <summary>
    /// Test the redistribution process.
    /// </summary>
    [TestFixture]
    public class RedistributorTest
    {
        #region Initialization

        /// <summary>
        /// Setup work common to the following tests. Only mock used is for the AutoUplink web service.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            // Data gateways.
            registry.Register<IAccountDataGateway,     AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, PublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  SubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     VehicleDataGateway>(ImplementationScope.Shared);

            // Bindings.
            registry.Register<IBindingFactory,    BindingFactory>(ImplementationScope.Shared);
            registry.Register<IAutoUplinkService, MockAutoUplinkService>(ImplementationScope.Shared);
            registry.Register<IAutoTraderService, MockAutoTraderService>(ImplementationScope.Shared);
            registry.Register<IGetAutoService,    MockGetAutoService>(ImplementationScope.Shared);

            // Distributors and queues..
            registry.Register<IDistributor, Distributor>(ImplementationScope.Shared);
            registry.Register<IQueue,       MemoryQueue>(ImplementationScope.Shared);
            registry.Register<IPublisher,   MockPublisher>(ImplementationScope.Shared);

            // Exceptions.
            registry.Register<IExceptionLogger, ExceptionLogger>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry and disabling distribution.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
            TestUtilities.DisableDistribution();
        }

        /// <summary>
        /// Reset our mocks prior to every test run.
        /// </summary>
        [SetUp]
        public void TestSetup()
        {
            MockPublisher.Reset();
            MockAutoUplinkService.Reset();
            MockAutoTraderService.Reset();            
        }

        #endregion        

        /// <summary>
        /// Test that redistribution works. Will verify that: the right submissions are picked up; that the right 
        /// submissions get marked stale; that the max number of attempts is respected; that a successful submission
        /// is not picked up for redistribution.
        /// </summary>
        [Test]
        public void RedistributionTest()
        {
            // This test operates under the assumption that these configuration variables are set to these values.
            Assert.AreEqual("5", ConfigurationManager.AppSettings["MaxRedistributionAttempts"]);
            Assert.AreEqual("10", ConfigurationManager.AppSettings["MaxRedistributionDaysBack"]);

            // Make all submissions in the database too old to be processed so that our focus is narrowed.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "UPDATE Distribution.Submission SET PushedOnQueue = DATEADD(yy, -20, GETDATE())";
                    command.ExecuteNonQuery();                    
                }
            }

            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            // Turn on distribution for one provider.
            TestUtilities.EnableDistribution();            
            TestUtilities.EnableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoUplink);

            // Set AutoUplink to reject everything.
            MockAutoUplinkService.ExpectedPriceReturnCode = "500";

            // Distribute the message three times.
            WebService.Services.V_1_0.Distributor distributor = new WebService.Services.V_1_0.Distributor();
            TransferObjects.V_1_0.SubmissionDetails details1 = distributor.Distribute(messageTO);
            TransferObjects.V_1_0.SubmissionDetails details2 = distributor.Distribute(messageTO);
            TransferObjects.V_1_0.SubmissionDetails details3 = distributor.Distribute(messageTO);

            string publicationIds = "(" + details1.PublicationId + ", " + details2.PublicationId + ", " + details3.PublicationId + ") ";

            // Test that the submissions were all rejected like they should have been.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT SubmissionStatusID " + 
                                          "FROM Distribution.Submission " + 
                                          "WHERE PublicationID in " + publicationIds +
                                          "AND ProviderID = 2";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {                            
                            Assert.AreEqual(5, reader.GetInt32(reader.GetOrdinal("SubmissionStatusID")));
                        }
                    }
                }
            }

            // Redistribute. Three submissions should have been retried.
            // Sleep after to allow time for processing.
            WebService.Services.Redistributor redistributor = new WebService.Services.Redistributor();
            int redistributionCount = redistributor.Redistribute();            
            Assert.AreEqual(3, redistributionCount);
            Thread.Sleep(500);
            
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Test that the first two submissions are stale (superseded by the third submission).
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT COUNT(*) AS Count " +
                                          "FROM Distribution.Submission " +
                                          "WHERE PublicationID in " + publicationIds +
                                          "AND ProviderID = 2 AND Attempt = 2 AND SubmissionStatusID = 7";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail();
                        }                        
                        Assert.AreEqual(2, reader.GetInt32(reader.GetOrdinal("Count")));
                    }
                }

                // Test that the newest submission is not stale, but rejected.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT COUNT(*) AS Count " +
                                          "FROM Distribution.Submission " +
                                          "WHERE PublicationID = " + details3.PublicationId + " " +
                                          "AND ProviderID = 2 AND Attempt = 2 AND SubmissionStatusID = 5";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail();
                        }
                        Assert.AreEqual(1, reader.GetInt32(reader.GetOrdinal("Count")));                        
                    }
                }                
            }

            // Redistribute again - only one submission should be picked up for its 3rd attempt.
            redistributionCount = redistributor.Redistribute();
            Assert.AreEqual(1, redistributionCount);
            Thread.Sleep(500);

            // Redistribute again - only one submission should be picked up for its 4th attempt.
            redistributionCount = redistributor.Redistribute();
            Assert.AreEqual(1, redistributionCount);
            Thread.Sleep(500);

            // Redistribute again - only one submission should be picked up for its 5th attempt.
            redistributionCount = redistributor.Redistribute();
            Assert.AreEqual(1, redistributionCount);
            Thread.Sleep(500);

            // Redistribute again - no submissions should be picked up.
            redistributionCount = redistributor.Redistribute();
            Assert.AreEqual(0, redistributionCount);
            Thread.Sleep(500);

            // Redistribute again - check again for the hell of it.
            redistributionCount = redistributor.Redistribute();
            Assert.AreEqual(0, redistributionCount);
            Thread.Sleep(500);
            
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                // Verify only one submission was handled 5 times.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT COUNT(*) AS Count FROM Distribution.Submission WHERE " +
                                          "PublicationID in " + publicationIds + 
                                          "AND ProviderID = 2 AND Attempt = 5";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail();
                        }                        
                        Assert.AreEqual(1, reader.GetInt32(reader.GetOrdinal("Count")));
                    }
                }
                // Verify nothing was done more than 5 times.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT COUNT(*) AS Count FROM Distribution.Submission WHERE " +
                                          "PublicationID in " + publicationIds +
                                          "AND ProviderID = 2 AND Attempt > 5";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            Assert.Fail();
                        }                        
                        Assert.AreEqual(0, reader.GetInt32(reader.GetOrdinal("Count")));
                    }
                }

                // Distribute a message and set it to pass. Give it a half second to process.
                MockAutoUplinkService.ExpectedPriceReturnCode = "101";
                distributor.Distribute(messageTO);
                Thread.Sleep(500);

                // Verify nothing was picked up for redistribution.
                redistributionCount = redistributor.Redistribute();
                Assert.AreEqual(0, redistributionCount);
            }
        }
    }
}
