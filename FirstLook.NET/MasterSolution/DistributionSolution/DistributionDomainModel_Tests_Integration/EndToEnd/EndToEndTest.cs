using System.Collections.ObjectModel;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Services.AutoUplink;
using FirstLook.Distribution.DomainModel.Services.AutoTrader;
using FirstLook.Distribution.DomainModel.Services.eBiz;
using FirstLook.Distribution.DomainModel.Services.GetAuto;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;
using Provider=FirstLook.Distribution.DomainModel.TransferObjects.V_1_0.Provider;

namespace FirstLook.Distribution.DomainModel.Tests.Integration.EndToEnd
{
    /// <summary>
    /// Test the full distribution of a message. Only two components are mocked: the third party web services, so that 
    /// messages do not actually get sent out; and the publisher, to force messges to be processed synchronously. 
    /// Every other component is live.
    /// </summary>
    /// <remarks>
    /// Because in general we do not want to accidentally distribute messages to third parties, a safeguard has been 
    /// implemented in <see cref="Distributor"/> that disables submission creation on any environment where the DEBUG 
    /// constant is defined (i.e. all non-production environments). Even though these tests cannot distribute messages 
    /// because the web services are mocked, they still must obey this safeguard. So, these tests are marked as 
    /// 'Ignored' when run on test environments.
    /// </remarks>
    [TestFixture]
    public class EndToEndTest
    {
        #region Initialization

        /// <summary>
        /// Setup work common to the following tests. Only mock used is for the AutoUplink web service.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            // Data gateways.
            registry.Register<IAccountDataGateway,     AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, PublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  SubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     VehicleDataGateway>(ImplementationScope.Shared);

            // Bindings.
            registry.Register<IBindingFactory,    BindingFactory>(ImplementationScope.Shared);
            registry.Register<IAutoUplinkService, MockAutoUplinkService>(ImplementationScope.Shared);
            registry.Register<IAutoTraderService, MockAutoTraderService>(ImplementationScope.Shared);
            registry.Register<IGetAutoService,    MockGetAutoService>(ImplementationScope.Shared);
            registry.Register<IEBizService,       MockEBizService>(ImplementationScope.Shared);            

            // Distributors and queues..
            registry.Register<IDistributor, Distributor>(ImplementationScope.Shared);
            registry.Register<IQueue,       MemoryQueue>(ImplementationScope.Shared);
            registry.Register<IPublisher,   MockPublisher>(ImplementationScope.Shared);

            // Exceptions.
            registry.Register<IExceptionLogger, ExceptionLogger>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry and disabling distribution.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Reset our mocks prior to every test run.
        /// </summary>
        [SetUp]
        public void TestSetup()
        {
            MockPublisher.Reset();
            MockAutoUplinkService.Reset();
            MockAutoTraderService.Reset();
            MockGetAutoService.Reset();
            MockEBizService.Reset();

            TestUtilities.DisableDistribution();
        }

        #endregion

        /// <summary>
        /// Give a message to the web service, and verify that the mock third party web services were called.
        /// </summary>        
        [Test]
        public void FullEndToEndTest()
        {                        
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            
            // Turn on distribution.
            TestUtilities.EnableDistribution();
            TestUtilities.EnableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoTrader);
            TestUtilities.EnableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoUplink);
           
            // Call web service to distribute message.
            WebService.Services.V_1_0.Distributor distributor = new WebService.Services.V_1_0.Distributor();
            distributor.Distribute(messageTO);

            // Test that the web services were actually called.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.CommentUpdateCalled);
            Assert.IsTrue(MockAutoTraderService.UpdateCarSalePriceCalled);            
        }

        /// <summary>
        /// BUGZID: 15840
        /// Give a message with an invalid vehicle identifier to the web service, and verify that the mock third party 
        /// web services were not called and that the publication was marked as 'NotAppicable'.
        /// </summary>        
        [Test]
        public void InactiveInventoryTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            messageTO.Subject.Id = -99999999;

            // Turn on distribution.
            TestUtilities.EnableDistribution();
            TestUtilities.EnableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoTrader);
            TestUtilities.EnableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoUplink);

            // Call web service to distribute message.
            WebService.Services.V_1_0.Distributor distributor = new WebService.Services.V_1_0.Distributor();
            TransferObjects.V_1_0.SubmissionDetails details = distributor.Distribute(messageTO);

            // Test that the web services were actually called.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);
            Assert.IsFalse(MockAutoTraderService.UpdateCarSalePriceCalled);

            // Test that the publication status is correct.            
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT PublicationStatusID FROM Distribution.Publication WHERE " +
                                          "PublicationID = " + details.PublicationId;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int publicationStatusId = reader.GetInt32(reader.GetOrdinal("PublicationStatusID"));
                            Assert.AreEqual((int)PublicationState.NotApplicable, publicationStatusId);
                        }
                        else
                        {
                            Assert.Fail("Couldn't read publication from the database.");
                        }
                    }
                }
            }    
        }

        /// <summary>
        /// Test that nothing is distributed to AutoTrader if it is turned off.
        /// </summary>
        [Test]
        public void DisableAutoTraderTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            // Turn on distribution.
            TestUtilities.EnableDistribution();
            TestUtilities.DisableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoTrader);
            TestUtilities.EnableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoUplink);

            // Call web service to distribute message.
            WebService.Services.V_1_0.Distributor distributor = new WebService.Services.V_1_0.Distributor();
            TransferObjects.V_1_0.SubmissionDetails details = distributor.Distribute(messageTO);

            // Verify no AutoTrader submissions were created.
            foreach (Provider provider in details.Providers)
            {
                if (provider.Id == (int)BindingType.AutoTrader)
                {
                    Assert.Fail("A submission was created for AutoTrader when it should not have been.");
                }
            }

            // Test that the web services were actually called.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.CommentUpdateCalled);
            Assert.IsFalse(MockAutoTraderService.UpdateCarSalePriceCalled);       
        }

        /// <summary>
        /// Test that nothing is distributed to AutoTrader if it is turned off.
        /// </summary>
        [Test]
        public void DisableAutoUplinkTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");

            // Turn on distribution.
            TestUtilities.EnableDistribution();
            TestUtilities.EnableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoTrader);
            TestUtilities.DisableDistributionForProvider(messageTO.From.Id, (int)BindingType.AutoUplink);

            // Call web service to distribute message.
            WebService.Services.V_1_0.Distributor distributor = new WebService.Services.V_1_0.Distributor();
            TransferObjects.V_1_0.SubmissionDetails details = distributor.Distribute(messageTO);

            // Verify no AutoUplink submissions were created.
            foreach (Provider provider in details.Providers)
            {
                if (provider.Id == (int)BindingType.AutoUplink)
                {
                    Assert.Fail("A submission was created for AutoUplink when it should not have been.");
                }
            }

            // Test that the web services were actually called.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);
            Assert.IsTrue(MockAutoTraderService.UpdateCarSalePriceCalled);
        }

        /// <summary>
        /// Test that when distribution is not enabled, no submissions are created.
        /// </summary>
        [Test]
        public void DistributionDisabledTest()
        {
            TransferObjects.V_1_0.Message messageTO = TestUtilities.GetMessageWithValidIds("TestMessage.xml");
            WebService.Services.V_1_0.Distributor distributor = new WebService.Services.V_1_0.Distributor();

            TransferObjects.V_1_0.SubmissionDetails details = distributor.Distribute(messageTO);

            // Test that no submissions were created.
            Collection<TransferObjects.V_1_0.SubmissionStatus> submissions = distributor.PublicationStatus(details.PublicationId);
            Assert.AreEqual(0, submissions.Count);

            // Test that the web services were never called.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);
            Assert.IsFalse(MockAutoTraderService.UpdateCarSalePriceCalled);   

            // Test that the publication status is correct.
            using (IDbConnection connection = Database.ConfigurationManagerConnection("IMT"))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT PublicationStatusID FROM Distribution.Publication WHERE " + 
                                          "PublicationID = " + details.PublicationId;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int publicationStatusId = reader.GetInt32(reader.GetOrdinal("PublicationStatusID"));
                            Assert.AreEqual((int)PublicationState.NotApplicable, publicationStatusId);
                        }
                        else
                        {
                            Assert.Fail("Couldn't read publication from the database.");
                        }
                    }
                }
            }                        
        }
    }    
}
