﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FirstLook.Distribution.DomainModel.Tests.Unit")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("FirstLook.Distribution.DomainModel.Tests.Unit")]
[assembly: AssemblyCompany("Firstlook, LLC")]
[assembly: AssemblyCopyright("Copyright © Firstlook, LLC 2007")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyTrademark("")]

[assembly: ComVisible(false)]
#if !NOT_CLS_COMPLIANT
[assembly: CLSCompliant(true)]
#endif
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyConfiguration("")]