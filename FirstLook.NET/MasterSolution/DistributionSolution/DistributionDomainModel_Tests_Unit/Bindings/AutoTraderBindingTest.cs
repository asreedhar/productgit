using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.AutoTrader;
using FirstLook.Distribution.DomainModel.Services.AutoTrader;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Bindings
{
    /// <summary>
    /// Test that the AutoTrader binding code correctly handles input and calls the right web service functions.    
    /// </summary>
    [TestFixture]
    public class AutoTraderBindingTest
    {
        #region Initialization

        private IBinding _autoTraderBinding;

        private Submission _submission;

        /// <summary>
        /// Setup work common to all the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<IAutoTraderService,      MockAutoTraderService>(ImplementationScope.Shared);            
            registry.Register<ISubmissionDataGateway,  MockSubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway,     MockAccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     MockVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, MockPublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger,        MockExceptionLogger>(ImplementationScope.Shared);

            _autoTraderBinding = new AutoTraderBinding();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Setup work to be done prior to every test. Resets the static status tracking variables for the auto uplink
        /// binding and submission mocks.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockAutoTraderService.Reset();            
            MockSubmissionDataGateway.Reset();
            MockAccountDataGateway.Reset();
            MockVehicleDataGateway.Reset();
            MockPublicationDataGateway.Reset();
            MockExceptionLogger.Reset();
        }

        #endregion

        #region Success Tests

        /// <summary>
        /// Test that a message with both a price and comment update is handled correctly.
        /// </summary>
        [Test]
        public void PriceAndCommentMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            MockAutoTraderService.ThrowsException = false;            

            _autoTraderBinding.Publish(_submission);

            // Check that the price message and comment message were correctly handled.
            Assert.IsTrue(MockAutoTraderService.UpdateCarSalePriceCalled);            

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that successes were logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);            
        }

        /// <summary>
        /// Test that a price messag is handled correctly.
        /// </summary>
        [Test]
        public void PriceMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "PriceMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price);

            MockAutoTraderService.ThrowsException = false;

            _autoTraderBinding.Publish(_submission);

            // Check that the price message was handled.
            Assert.IsTrue(MockAutoTraderService.UpdateCarSalePriceCalled);            

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that success was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);            
        }

        #endregion

        #region Failure Tests

        /// <summary>
        /// Test that a message with both a price and comment update fails correctly when a failure return code is 
        /// received from the web service.
        /// </summary>
        public void PriceAndCommentMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            MockAutoTraderService.ThrowsException = true;

            _autoTraderBinding.Publish(_submission);

            // Check that the message was handled.
            Assert.IsTrue(MockAutoTraderService.UpdateCarSalePriceCalled);            

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that the error was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);            

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a price message fails correctly when a failure return code is received from the web service.
        /// </summary>
        [Test]
        public void PriceMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "PriceMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price);

            MockAutoTraderService.ThrowsException = true;            

            _autoTraderBinding.Publish(_submission);

            // Check that the price message was handled, and that there was no attempt to process comments.
            Assert.IsTrue(MockAutoTraderService.UpdateCarSalePriceCalled);            

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that the error is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);            

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        #endregion

        #region Utility Tests

        /// <summary>
        /// Test that text with certain HTML tags is properly escaped so that AutoTrader's parser doesn't strip it out.
        /// </summary>
        [Test]
        public void EscapeDescriptionTest()
        {
            const string testText = "<p>Hello.<br/>How are you?<br>I am fine.</br></p>";
            string newText = AutoTraderBinding.ProcessHtmlTags(testText);

            Assert.AreEqual("<![CDATA[<p>Hello.<br/>How are you?<br>I am fine.</br></p>]]>", newText);
        }

        #endregion

        #region Miscellaneous Tests

        /// <summary>
        /// Test that a submission is marked as 'NotSupported' when the message it is given has a type that is not
        /// handled by AutoTrader.
        /// </summary>
        [Test]
        public void MessageNotSupportedTest()
        {
            MockPublicationDataGateway.InputDataFile = "VehicleInfoMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Mileage);

            _autoTraderBinding.Publish(_submission);

            // Check that no attempt was made to process the message.
            Assert.IsFalse(MockAutoTraderService.UpdateCarSalePriceCalled);            

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.NotSupported);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a submission is marked as 'NotSupported' when the message it is given has a type that is not
        /// handled by AutoTrader.
        /// </summary>
        [Test]
        public void CommentMessageNotSupported()
        {
            MockPublicationDataGateway.InputDataFile = "TextContentMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Description);

            _autoTraderBinding.Publish(_submission);

            // Check that no attempt was made to process the message.
            Assert.IsFalse(MockAutoTraderService.UpdateCarSalePriceCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.NotSupported);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a stale submission is not processed, and that it's status is set correctly.
        /// </summary>
        [Test]
        public void StaleTest()
        {
            const MessageTypes types = MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage;

            MockSubmissionDataGateway.StaleTypes = types;

            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, types);

            _autoTraderBinding.Publish(_submission);

            // Check that no attempt was made to process the message.
            Assert.IsFalse(MockAutoTraderService.UpdateCarSalePriceCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Superseded);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }      
  
        /// <summary>
        /// Test that if the price is stale that an attempt is made to get the superseding price.
        /// </summary>
        [Test]
        public void SupersedingPriceTest()
        {
            const MessageTypes types = MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage;

            MockSubmissionDataGateway.StaleTypes = MessageTypes.Price;

            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, types);

            _autoTraderBinding.Publish(_submission);

            // Check that an attempt was made to get the newer price.
            Assert.IsTrue(MockSubmissionDataGateway.FetchSupersedingPriceCalled);

            // Check that the message was processed.
            Assert.IsTrue(MockAutoTraderService.UpdateCarSalePriceCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that a success is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
        }

        #endregion
    }
}
