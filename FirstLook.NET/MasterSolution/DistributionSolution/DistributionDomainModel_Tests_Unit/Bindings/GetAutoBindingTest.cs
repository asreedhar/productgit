﻿using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.GetAuto;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Services.GetAuto;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Bindings
{
    /// <summary>
    /// Test that the GetAuto binding code correctly handles input and calls the right web service functions.    
    /// </summary>
    [TestFixture]
    public class GetAutoBindingTest
    {
        #region Initialization

        private IBinding _getAutoBinding;

        private Submission _submission;

        /// <summary>
        /// Setup work common to all the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<IGetAutoService,         MockGetAutoService>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  MockSubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway,     MockAccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     MockVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, MockPublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger,        MockExceptionLogger>(ImplementationScope.Shared);

            _getAutoBinding = new GetAutoBinding();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Setup work to be done prior to every test. Resets the static status tracking variables for the auto uplink
        /// binding and submission mocks.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockGetAutoService.Reset();
            MockSubmissionDataGateway.Reset();
            MockAccountDataGateway.Reset();
            MockVehicleDataGateway.Reset();
            MockPublicationDataGateway.Reset();
            MockExceptionLogger.Reset();

            // Setup the vehicle list.
            const string vin = "TEST VIN";
            MockVehicleDataGateway.Vin = vin;

            VehicleInfo[] vehicleInfoCollection = new VehicleInfo[2];

            VehicleInfo vehicleInfo1 = new VehicleInfo();
            vehicleInfo1.Vin = vin;
            vehicleInfo1.VehicleKey = 1234;
            vehicleInfoCollection[0] = vehicleInfo1;

            VehicleInfo vehicleInfo2 = new VehicleInfo();
            vehicleInfo2.Vin = vin + " 2";
            vehicleInfo2.VehicleKey = 5678;
            vehicleInfoCollection[1] = vehicleInfo2;

            MockGetAutoService.DealerVehicles = vehicleInfoCollection;
        }

        #endregion

        #region Success Tests

        /// <summary>
        /// Test that a price message is handled correctly.
        /// </summary>
        [Test]
        public void PriceMessagePassesTest()
        {            
            MockPublicationDataGateway.InputDataFile = "PriceMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Price);
                        
            MockGetAutoService.ThrowsException = false;
            MockGetAutoService.UpdatePriceResult = ReturnResult.Success;
            
            _getAutoBinding.Publish(_submission);

            // Check that the 'get vehicle' call was made.
            Assert.IsTrue(MockGetAutoService.GetAllVehicleCalled);

            // Check that the price message was handled.
            Assert.IsTrue(MockGetAutoService.UpdateVehiclePriceCalled);
            Assert.IsFalse(MockGetAutoService.UpdateVehicleCommentsCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that success was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a comment message is handled correctly.
        /// </summary>
        [Test]
        public void CommentMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "TextContentMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Description);

            const string vin = "TEST VIN";
            MockGetAutoService.ThrowsException = false;
            MockGetAutoService.UpdatePriceResult = ReturnResult.Success;
            MockVehicleDataGateway.Vin = vin;

            VehicleInfo[] vehicleInfoCollection = new VehicleInfo[2];

            VehicleInfo vehicleInfo1 = new VehicleInfo();
            vehicleInfo1.Vin = vin;
            vehicleInfo1.VehicleKey = 1234;
            vehicleInfoCollection[0] = vehicleInfo1;

            VehicleInfo vehicleInfo2 = new VehicleInfo();
            vehicleInfo2.Vin = vin + " 2";
            vehicleInfo2.VehicleKey = 5678;
            vehicleInfoCollection[1] = vehicleInfo2;

            MockGetAutoService.DealerVehicles = vehicleInfoCollection;

            _getAutoBinding.Publish(_submission);

            // Check that the 'get vehicle' call was made.
            Assert.IsTrue(MockGetAutoService.GetAllVehicleCalled);

            // Check that the comment message was handled.
            Assert.IsTrue(MockGetAutoService.UpdateVehicleCommentsCalled);
            Assert.IsFalse(MockGetAutoService.UpdateVehiclePriceCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that success was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a price and comment message is handled correctly.
        /// </summary>
        [Test]
        public void PriceAndCommentsMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Price | MessageTypes.Description);

            const string vin = "TEST VIN";
            MockGetAutoService.ThrowsException = false;
            MockGetAutoService.UpdatePriceResult = ReturnResult.Success;
            MockVehicleDataGateway.Vin = vin;

            VehicleInfo[] vehicleInfoCollection = new VehicleInfo[2];

            VehicleInfo vehicleInfo1 = new VehicleInfo();
            vehicleInfo1.Vin = vin;
            vehicleInfo1.VehicleKey = 1234;
            vehicleInfoCollection[0] = vehicleInfo1;

            VehicleInfo vehicleInfo2 = new VehicleInfo();
            vehicleInfo2.Vin = vin + " 2";
            vehicleInfo2.VehicleKey = 5678;
            vehicleInfoCollection[1] = vehicleInfo2;

            MockGetAutoService.DealerVehicles = vehicleInfoCollection;

            _getAutoBinding.Publish(_submission);

            // Check that the 'get vehicle' call was made.
            Assert.IsTrue(MockGetAutoService.GetAllVehicleCalled);

            // Check that the comment message was handled.
            Assert.IsTrue(MockGetAutoService.UpdateVehicleCommentsCalled);
            Assert.IsTrue(MockGetAutoService.UpdateVehiclePriceCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that success was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
            
        }

        #endregion

        #region Failure Tests

        /// <summary>
        /// Test that a failed price message is handled correctly.
        /// </summary>
        [Test]
        public void PriceMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "PriceMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Price);

            MockGetAutoService.ThrowsException = false;
            MockGetAutoService.UpdatePriceResult = ReturnResult.MethodFailed;

            _getAutoBinding.Publish(_submission);

            // Check that the 'get vehicle' call was made.
            Assert.IsTrue(MockGetAutoService.GetAllVehicleCalled);

            // Check that the price message was handled.
            Assert.IsTrue(MockGetAutoService.UpdateVehiclePriceCalled);
            Assert.IsFalse(MockGetAutoService.UpdateVehicleCommentsCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that an error was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a failed comment message is handled correctly.
        /// </summary>
        [Test]
        public void CommentMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "TextContentMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Description);

            MockGetAutoService.ThrowsException = false;
            MockGetAutoService.UpdateCommentResult = ReturnResult.MethodFailed;

            _getAutoBinding.Publish(_submission);

            // Check that the 'get vehicle' call was made.
            Assert.IsTrue(MockGetAutoService.GetAllVehicleCalled);

            // Check that the comment message was handled.
            Assert.IsFalse(MockGetAutoService.UpdateVehiclePriceCalled);
            Assert.IsTrue(MockGetAutoService.UpdateVehicleCommentsCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that an error was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a failed price and comment message is handled correctly.
        /// </summary>
        [Test]
        public void PriceAndCommentMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Price | MessageTypes.Description);

            MockGetAutoService.ThrowsException = false;
            MockGetAutoService.UpdatePriceResult = ReturnResult.MethodFailed;
            MockGetAutoService.UpdateCommentResult = ReturnResult.MethodFailed;            

            _getAutoBinding.Publish(_submission);

            // Check that the 'get vehicle' call was made.
            Assert.IsTrue(MockGetAutoService.GetAllVehicleCalled);

            // Check that the price and comment messages were handled.
            Assert.IsTrue(MockGetAutoService.UpdateVehiclePriceCalled);
            Assert.IsTrue(MockGetAutoService.UpdateVehicleCommentsCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that an error was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Check that if a vehicle is not found in the list of vehicles returned from GetAuto, that we fail properly.
        /// </summary>
        [Test]
        public void VehicleNotFoundTest()
        {
            MockGetAutoService.DealerVehicles = null;

            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Price | MessageTypes.Description);

            MockGetAutoService.ThrowsException = false;
            MockGetAutoService.UpdatePriceResult = ReturnResult.Success;
            MockGetAutoService.UpdateCommentResult = ReturnResult.Success;

            _getAutoBinding.Publish(_submission);

            // Check that the 'get vehicle' call was made.
            Assert.IsTrue(MockGetAutoService.GetAllVehicleCalled);

            // Check that the price and comment messages were handled.
            Assert.IsFalse(MockGetAutoService.UpdateVehiclePriceCalled);
            Assert.IsFalse(MockGetAutoService.UpdateVehicleCommentsCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that an exception was logged.
            Assert.IsTrue(MockExceptionLogger.RecordCalled);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        #endregion

        #region Miscellaneous Tests

        /// <summary>
        /// Test that a submission is marked as 'NotSupported' when the message it is given has a type that is not
        /// handled by AutoTrader.
        /// </summary>
        [Test]
        public void MessageNotSupportedTest()
        {
            MockPublicationDataGateway.InputDataFile = "VehicleInfoMessage.xml";
            _submission = Submission.NewSubmission(4, 1, MessageTypes.Mileage);

            _getAutoBinding.Publish(_submission);

            // Check that no attempt was made to process the message.
            Assert.IsFalse(MockGetAutoService.UpdateVehiclePriceCalled);
            Assert.IsFalse(MockGetAutoService.UpdateVehicleCommentsCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.NotSupported);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        #endregion
    }
}
