﻿using System.Configuration;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.eBiz;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Services.eBiz;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Bindings
{
    /// <summary>
    /// Test that the eBiz binding code correctly handles input and calls the right web service functions.    
    /// </summary>
    [TestFixture]
    public class eBizBindingTest
    {
        #region Initialization

        private IBinding _eBizBinding;

        private Submission _submission;

        /// <summary>
        /// Setup work common to all the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<IEBizService,            MockEBizService>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  MockSubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway,     MockAccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     MockVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, MockPublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger,        MockExceptionLogger>(ImplementationScope.Shared);

            ConfigurationManager.AppSettings["eBizId"] = "170";
            ConfigurationManager.AppSettings["eBizPassword"] = "4C4E5C0E-DB43-4BF3-94DE-6148A655ACBE";  

            _eBizBinding = new eBizBinding();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Setup work to be done prior to every test. Resets the static status tracking variables for the auto uplink
        /// binding and submission mocks.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockEBizService.Reset();
            MockSubmissionDataGateway.Reset();
            MockAccountDataGateway.Reset();
            MockVehicleDataGateway.Reset();
            MockPublicationDataGateway.Reset();
            MockExceptionLogger.Reset();
        }

        #endregion

        #region Success Tests

        /// <summary>
        /// Test that the binding works correctly for price and comment messages.
        /// </summary>
        [Test]
        public void PriceAndCommentMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price | MessageTypes.Description);            

            _eBizBinding.Publish(_submission);

            // Check that an attempt was made to process the message.
            Assert.IsTrue(MockEBizService.SendVehicleCalled);
            Assert.IsTrue(MockEBizService.UpdatePriceCalled);
            Assert.IsTrue(MockEBizService.UpdateDescriptionCalled);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that a success is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that the binding works correctly for price messages.
        /// </summary>
        [Test]
        public void PriceMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price);

            _eBizBinding.Publish(_submission);

            // Check that an attempt was made to process the message.
            Assert.IsTrue(MockEBizService.SendVehicleCalled);
            Assert.IsTrue(MockEBizService.UpdatePriceCalled);
            Assert.IsFalse(MockEBizService.UpdateDescriptionCalled);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that a success is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that the binding works correctly for price messages.
        /// </summary>
        [Test]
        public void CommentMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Description);

            _eBizBinding.Publish(_submission);

            // Check that an attempt was made to process the message.
            Assert.IsTrue(MockEBizService.SendVehicleCalled);
            Assert.IsTrue(MockEBizService.UpdateDescriptionCalled);
            Assert.IsFalse(MockEBizService.UpdatePriceCalled);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that a success is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
        }

        #endregion

        #region Failure Tests

        /// <summary>
        /// Test that the binding works correctly for price and comment messages.
        /// </summary>
        [Test]
        public void PriceAndCommentMessageFailTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price | MessageTypes.Description);

            MockEBizService.Response = "Invalid vendor authorization.";

            _eBizBinding.Publish(_submission);

            // Check that an attempt was made to process the message.
            Assert.IsTrue(MockEBizService.SendVehicleCalled);
            Assert.IsTrue(MockEBizService.UpdatePriceCalled);
            Assert.IsTrue(MockEBizService.UpdateDescriptionCalled);

            // Check that an error is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that the binding works correctly for price messages.
        /// </summary>
        [Test]
        public void PriceMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Price);

            MockEBizService.Response = "Invalid vendor authorization.";

            _eBizBinding.Publish(_submission);

            // Check that an attempt was made to process the message.
            Assert.IsTrue(MockEBizService.SendVehicleCalled);
            Assert.IsTrue(MockEBizService.UpdatePriceCalled);
            Assert.IsFalse(MockEBizService.UpdateDescriptionCalled);

            // Check that an error is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that the binding works correctly for price messages.
        /// </summary>
        [Test]
        public void CommentMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(1, 1, MessageTypes.Description);

            MockEBizService.Response = "Invalid vendor authorization.";

            _eBizBinding.Publish(_submission);

            // Check that an attempt was made to process the message.
            Assert.IsTrue(MockEBizService.SendVehicleCalled);
            Assert.IsTrue(MockEBizService.UpdateDescriptionCalled);
            Assert.IsFalse(MockEBizService.UpdatePriceCalled);

            // Check that an error is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        #endregion

        #region Miscellaneous Tests

        /// <summary>
        /// Test that a submission created from a subset of the message types available in the incoming message request
        /// only has its filtered types sent to the provider.
        /// </summary>
        [Test]
        public void FilteredMessageTypeTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Mileage);

            _eBizBinding.Publish(_submission);

            // Check that no attempt was made to process either prices or comments.
            Assert.IsFalse(MockEBizService.UpdatePriceCalled);
            Assert.IsFalse(MockEBizService.UpdateDescriptionCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.NotSupported);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);

        }

        /// <summary>
        /// Test that a submission is marked as 'NotSupported' when the message it is given has a type that is not
        /// handled by AutoUplink.
        /// </summary>
        [Test]
        public void MessageNotSupportedTest()
        {
            MockPublicationDataGateway.InputDataFile = "VehicleInfoMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Mileage);

            _eBizBinding.Publish(_submission);

            // Check that no attempt was made to process either prices or comments.
            Assert.IsFalse(MockEBizService.UpdatePriceCalled);
            Assert.IsFalse(MockEBizService.UpdateDescriptionCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.NotSupported);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a stale submission is not processed, and that it's status is set correctly.
        /// </summary>
        [Test]
        public void StaleTest()
        {
            const MessageTypes types = MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage;

            MockSubmissionDataGateway.StaleTypes = types;

            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, types);

            _eBizBinding.Publish(_submission);

            // Check that no attempt was made to process either prices or comments.
            Assert.IsFalse(MockEBizService.UpdatePriceCalled);
            Assert.IsFalse(MockEBizService.UpdateDescriptionCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Superseded);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        } 

        #endregion
    }
}
