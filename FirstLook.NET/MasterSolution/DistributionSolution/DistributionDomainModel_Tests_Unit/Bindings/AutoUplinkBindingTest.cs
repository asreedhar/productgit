using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.AutoUplink;
using FirstLook.Distribution.DomainModel.Services.AutoUplink;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Bindings
{
    /// <summary>
    /// Test that the AutoUplink binding code correctly handles input and calls the right web service functions.
    /// </summary>
    [TestFixture]
    public class AutoUplinkBindingTest
    {
        #region Initialization
        
        private IBinding _autoUplinkBinding;
        
        private Submission _submission;

        /// <summary>
        /// Setup work common to all the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<IAutoUplinkService,      MockAutoUplinkService>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  MockSubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway,     MockAccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     MockVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, MockPublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger,        MockExceptionLogger>(ImplementationScope.Shared);

            _autoUplinkBinding = new AutoUplinkBinding();            
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        } 

        /// <summary>
        /// Setup work to be done prior to every test. Resets the static status tracking variables for the auto uplink
        /// binding and submission mocks.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockAutoUplinkService.Reset();            
            MockSubmissionDataGateway.Reset();
            MockAccountDataGateway.Reset();
            MockVehicleDataGateway.Reset();
            MockPublicationDataGateway.Reset();
            MockExceptionLogger.Reset();
        }

        #endregion

        #region Success Tests

        /// <summary>
        /// Test that a message with both a price and comment update is handled correctly.
        /// </summary>
        [Test]
        public void PriceAndCommentMessagePassesTest()
        {            
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            MockAutoUplinkService.ExpectedPriceReturnCode   = "101";
            MockAutoUplinkService.ExpectedCommentReturnCode = "101";
                                
            _autoUplinkBinding.Publish(_submission);

            // Check that the price message and comment message were correctly handled.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that successes were logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
            Assert.AreEqual(2, MockSubmissionDataGateway.NumberLogSuccessCalled);
        }

        /// <summary>
        /// Test that a price messag is handled correctly.
        /// </summary>
        [Test]
        public void PriceMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "PriceMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Price);

            MockAutoUplinkService.ExpectedPriceReturnCode   = "101";
            MockAutoUplinkService.ExpectedCommentReturnCode = "401";

            _autoUplinkBinding.Publish(_submission);

            // Check that the price message was handled, and that there was no attempt to process comments.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that success was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
            Assert.AreEqual(1, MockSubmissionDataGateway.NumberLogSuccessCalled);
        }

        /// <summary>
        /// Test that a price messag is handled correctly.
        /// </summary>
        [Test]
        public void CommentMessagePassesTest()
        {
            MockPublicationDataGateway.InputDataFile = "TextContentMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Description);

            MockAutoUplinkService.ExpectedPriceReturnCode   = "401";
            MockAutoUplinkService.ExpectedCommentReturnCode = "101";

            _autoUplinkBinding.Publish(_submission);

            // Check that the comment message was handled, and that there was no attempt to process prices.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Accepted);

            // Check that no error was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that success was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogSuccessCalled);
            Assert.AreEqual(1, MockSubmissionDataGateway.NumberLogSuccessCalled);
        }

        #endregion

        #region Failure Tests

        /// <summary>
        /// Test that a message with both a price and comment update fails correctly when a failure return code is 
        /// received from the web service.
        /// </summary>
        public void PriceAndCommentMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            MockAutoUplinkService.ExpectedPriceReturnCode   = "401";
            MockAutoUplinkService.ExpectedCommentReturnCode = "401";

            _autoUplinkBinding.Publish(_submission);

            // Check that the price message was handled and that, because of the failure, no attempt was made to 
            // process the coment message.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that the error was logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);
            Assert.AreEqual(2, MockSubmissionDataGateway.NumberLogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }


        /// <summary>
        /// Test that a price message fails correctly when a failure return code is received from the web service.
        /// </summary>
        [Test]
        public void PriceMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "PriceMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Price);

            MockAutoUplinkService.ExpectedPriceReturnCode   = "401";
            MockAutoUplinkService.ExpectedCommentReturnCode = "101";

            _autoUplinkBinding.Publish(_submission);
            
            // Check that the price message was handled, and that there was no attempt to process comments.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);            

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that the error is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);
            Assert.AreEqual(1, MockSubmissionDataGateway.NumberLogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a comment message fails correctly when a failure return code is received from the web service.
        /// </summary>
        [Test]
        public void CommentMessageFailsTest()
        {
            MockPublicationDataGateway.InputDataFile = "TextContentMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Description);

            MockAutoUplinkService.ExpectedPriceReturnCode   = "101";
            MockAutoUplinkService.ExpectedCommentReturnCode = "401";

            _autoUplinkBinding.Publish(_submission);

            // Check that the comment message was handled, and that there was no attempt to process prices.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that the error is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);
            Assert.AreEqual(1, MockSubmissionDataGateway.NumberLogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }        

        /// <summary>
        /// Test if the web service returns an exception that it is correctly caught and logged.
        /// </summary>
        [Test]
        public void ExceptionCaughtTest()
        {
            MockAutoUplinkService.ThrowException = true;

            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            _autoUplinkBinding.Publish(_submission);

            // Check that the price message was handled, and that there was no attempt to process comments.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);

            // Check that the exception is correctly logged.
            Assert.IsTrue(MockExceptionLogger.RecordCalled);

            // Check that the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);
        }

        /// <summary>
        /// Test that an unrecognized return code is handled like any other error return code.
        /// </summary>
        [Test]
        public void UnrecognizedReturnCodeTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            MockAutoUplinkService.ExpectedPriceReturnCode   = "999";
            MockAutoUplinkService.ExpectedCommentReturnCode = "999";

            _autoUplinkBinding.Publish(_submission);

            // Check that the comment message was handled, and that there was no attempt to process prices.
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.CommentUpdateCalled);

            // Check that no exception is logged.
            Assert.IsFalse(MockExceptionLogger.RecordCalled);

            // Check that the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Rejected);

            // Check that the error is logged.
            Assert.IsTrue(MockSubmissionDataGateway.LogErrorCalled);
            Assert.AreEqual(2, MockSubmissionDataGateway.NumberLogErrorCalled);

            // Check that no success was logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        #endregion

        #region Miscellaneous Tests

        /// <summary>
        /// Test that a submission created from a subset of the message types available in the incoming message request
        /// only has its filtered types sent to the provider.
        /// </summary>
        [Test]
        public void FilteredMessageTypeTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Mileage);

            _autoUplinkBinding.Publish(_submission);

            // Check that no attempt was made to process either prices or comments.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.NotSupported);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
            
        }

        /// <summary>
        /// Test that a submission is marked as 'NotSupported' when the message it is given has a type that is not
        /// handled by AutoUplink.
        /// </summary>
        [Test]
        public void MessageNotSupportedTest()
        {
            MockPublicationDataGateway.InputDataFile = "VehicleInfoMessage.xml";
            _submission = Submission.NewSubmission(2, 1, MessageTypes.Mileage);

            _autoUplinkBinding.Publish(_submission);

            // Check that no attempt was made to process either prices or comments.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.NotSupported);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }

        /// <summary>
        /// Test that a stale submission is not processed, and that it's status is set correctly.
        /// </summary>
        [Test]
        public void StaleTest()
        {
            const MessageTypes types = MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage;

            MockSubmissionDataGateway.StaleTypes = types;

            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission(2, 1, types);

            _autoUplinkBinding.Publish(_submission);

            // Check that no attempt was made to process either prices or comments.
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.CommentUpdateCalled);

            // Check the submission status is set correctly.
            Assert.AreEqual(_submission.SubmissionState, SubmissionState.Superseded);

            // Check that no error is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogErrorCalled);

            // Check that no success is logged.
            Assert.IsFalse(MockSubmissionDataGateway.LogSuccessCalled);
        }        

        /// <summary>
        /// Test that price is updated in the right way depending on how the dealer is setup. If the dealer is calling
        /// Aultec, and not AutoUplink, then InternetPrice should be updated instead of just Price when the dealer has
        /// the Aultec extract turned on. In all other cases, Price and not InternetPrice should be updated.
        /// </summary>
        [Test]
        public void AultecExtractDealerTest()
        {
            MockPublicationDataGateway.InputDataFile = "TestMessage.xml";
            _submission = Submission.NewSubmission((int)BindingType.Aultec, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);
            
            // Publish with extract status turned on - only internet price should be updated.
            MockAccountDataGateway.DoesDealerHaveAultecExtract = true;
            _autoUplinkBinding.Publish(_submission);
            Assert.IsTrue(MockAutoUplinkService.InternetPriceUpdateCalled);
            Assert.IsFalse(MockAutoUplinkService.PriceUpdateCalled);               
            
            // Try with extract turned off - only normal price should be updated.
            MockAutoUplinkService.Reset();
            MockAccountDataGateway.DoesDealerHaveAultecExtract = false;
            _autoUplinkBinding.Publish(_submission);
            Assert.IsFalse(MockAutoUplinkService.InternetPriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            MockAutoUplinkService.Reset();

            // Try with an AutoUplink submission and extract on - should only update normal price.
            MockAutoUplinkService.Reset();
            _submission = Submission.NewSubmission((int)BindingType.AutoUplink, 1, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);
            MockAccountDataGateway.DoesDealerHaveAultecExtract = true;
            _autoUplinkBinding.Publish(_submission);
            Assert.IsFalse(MockAutoUplinkService.InternetPriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);
            
            // Last one - AutoUplink submission with extract off - should only update normal price.
            MockAutoUplinkService.Reset();
            MockAccountDataGateway.DoesDealerHaveAultecExtract = false;
            _autoUplinkBinding.Publish(_submission);
            Assert.IsFalse(MockAutoUplinkService.InternetPriceUpdateCalled);
            Assert.IsTrue(MockAutoUplinkService.PriceUpdateCalled);    
        }

        #endregion
    }
}
