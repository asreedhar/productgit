﻿using System.Configuration;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.eLead;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using FirstLook.Distribution.DomainModel.TransferObjects.V_1_0;
using NUnit.Framework;
using System;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using System.Collections.ObjectModel;
using PriceType = FirstLook.Distribution.DomainModel.TransferObjects.V_1_0.PriceType;
using FirstLook.Distribution.DomainModel.Distributors;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Bindings
{
    [TestFixture]
    public class eLeadBindingTest
    {
        #region Initialization

        private IBinding _eLeadBinding;
        private MockPublicationDataGateway _mockPublicationDataGateway;
        private SubmissionDataGateway _submissionDataGateway;
        private Submission _submission;
        private AccountDataGateway _accountDataGateway;
        private PublicationDataGateway _publicationDataGateway;
        /// <summary>
        /// Constant for Positive Dealer ID
        /// </summary>
        private const int FromDealerIDPositive = 105239;
        /// <summary>
        /// Constant for Provider ID
        /// </summary>
        private const int ProviderID = 6;

        /// <summary>
        /// Constant For Negative test of dealer ID
        /// </summary>
        public const int FromDealerIDNegative = 100024;
        /// <summary>
        /// Setup work common to all the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            //registry.Register<IeLeadService, MockeLeadService>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway, SubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway, AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway, VehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, PublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IAppraisedVehicleDataGateway, AppraisedVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IQueue, MemoryQueue>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger, ExceptionLogger>(ImplementationScope.Shared);
            registry.Register<IPublisher, ThreadedPublisher>(ImplementationScope.Shared);

            ConfigurationManager.AppSettings["eLeadId"] = "Firstlook";
            ConfigurationManager.AppSettings["eLeadPassword"] = "5AAC4CCA-9289-4DE1-87BF-85E0F9D196D9";
            ConfigurationManager.AppSettings["eLeadURI"] = "http://test.eleadcrm.com/i46/fresh/eLead-V45/elead_track/WebServices/FirstLook/PostAppraisal.ashx";
            ConfigurationManager.AppSettings["MaximumAppraisalValue"] = "999999";
            ConfigurationManager.AppSettings["MinimumAppraisalValue"] = "0";
            ConfigurationManager.AppSettings["EnableDistribution"] = "1";
            ConfigurationManager.AppSettings["EnableElead"] = "1";
              

            _eLeadBinding = new eLeadBinding();
            _mockPublicationDataGateway = new MockPublicationDataGateway();
            _submissionDataGateway = new SubmissionDataGateway();
            _accountDataGateway = new AccountDataGateway();
            _publicationDataGateway=new PublicationDataGateway();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Setup work to be done prior to every test. Resets the static status tracking variables for the auto uplink
        /// binding and submission mocks.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockPublicationDataGateway.Reset();
        }

        #endregion

        #region Positive Tests

        /// <summary>
        /// Test that the binding works correctly for price and comment messages.
        /// </summary>
        [Test]
        public void SendAppraisalValueTest()
        {
            try
            {
               // TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestELeadMessage.xml");
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 105239 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 15239083, VehicleType = VehicleType.Appraisal };
                TransferObjects.V_1_0.AppraisalValue appraisalValue = new TransferObjects.V_1_0.AppraisalValue() {Value = 15000 };
                
                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body=new MessageBody();

                message.Body.Parts.Add(appraisalValue);
                message.From = dl;
                message.Subject = vl;
                //new Distributor().Distribute(message);
                Publication publication = Publication.NewPublication(message);
                _publicationDataGateway.Save(publication);
                _submission = Submission.NewSubmission(ProviderID, publication.Id, MessageTypes.AppraisalValue);
                _submissionDataGateway.Save(_submission);
                //// Check that an attempt was made to process the message.
                _eLeadBinding.Publish(_submission);

                //Check if Data is Submitted successfuly in database. 
                Assert.IsTrue(1 == _submissionDataGateway.FetchSuccesses(_submission.Id).Count);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Negative Tests

        /// <summary>
        /// Test that the binding works correctly for price and comment messages.
        /// </summary>
        [Test]
        public void SendAppraisalValueFailTest()
        {
            try
            {
                // TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestELeadMessage.xml");
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 105239 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 25320918, VehicleType = VehicleType.Appraisal };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { PriceType = PriceType.Appraisalvalue, Value = 20000 };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                Publication publication = Publication.NewPublication(message);
                _publicationDataGateway.Save(publication);
                _submission = Submission.NewSubmission(ProviderID, publication.Id, MessageTypes.Price);
                _submissionDataGateway.Save(_submission);

                //// Check that an attempt was made to process the message.
                _eLeadBinding.Publish(_submission);

                //Check if Data is Submitted successfuly in database. 
                Assert.IsTrue(1 == _submissionDataGateway.FetchErrors(_submission.Id).Count);


                //Check if Submission if it is marked as rejected
                Assert.IsTrue(_submissionDataGateway.FetchFailuresToRetry(5, 1).Count(sub => sub.Id == _submission.Id)>=1);  
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Miscellaneous Tests

        [Test]
        public void ProviderSupportPositiveTest()
        {
            try
            {
                // TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestELeadMessage.xml");
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 105239 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 25320918, VehicleType = VehicleType.Appraisal };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { PriceType = PriceType.Appraisalvalue, Value = 20000 };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                Publication publication = Publication.NewPublication(message);
                MessageTypes supportedTypes = publication.Message.MessageTypes;

                // Verify the dealer has active account info with the provider.
                Assert.IsTrue(_accountDataGateway.IsSupported(FromDealerIDPositive, ProviderID, ref supportedTypes));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [Test]
        public void ProviderSupportNegativeTest()
        {
            try
            {
                // TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestELeadMessage.xml");
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 105239 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 25320918, VehicleType = VehicleType.Appraisal };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { PriceType = PriceType.Appraisalvalue, Value = 20000 };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                Publication publication = Publication.NewPublication(message);
                MessageTypes supportedTypes = publication.Message.MessageTypes;

                // Verify the dealer has active account info with the provider.
                Assert.IsFalse(_accountDataGateway.IsSupported(FromDealerIDNegative, ProviderID, ref supportedTypes));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [Test]
        public void DistributePositiveSuportDealer()
        {
            
            try
            {
               // TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestELeadMessage.xml");
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = FromDealerIDNegative };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 15239083, VehicleType = VehicleType.Appraisal };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { PriceType = PriceType.Appraisalvalue, Value = 20000 };
                
                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body=new MessageBody();
                
                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                SubmissionDetails submissionDetails=new Distributor().Distribute(message);
                //_eLeadBinding.Publish(_submission);

                //Check if Data is Submitted successfuly in database. 
                Assert.IsTrue(submissionDetails.PublicationId==0);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region UtilityMethods
        #endregion
    }
}
