﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Services.Protocols;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Bindings.ADP;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Extensions;
using FirstLook.Distribution.DomainModel.Services.ADP;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using FirstLook.Distribution.DomainModel.TransferObjects.V_1_0;
using NUnit.Framework;
using PriceType = FirstLook.Distribution.DomainModel.TransferObjects.V_1_0.PriceType;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Bindings
{
    [TestFixture]
    public class ADPBindingTest
    {
        #region Initialization

        private IBinding _ADPBinding;
        private MockPublicationDataGateway _mockPublicationDataGateway;
        private SubmissionDataGateway _submissionDataGateway;
        private Submission _submission;
        private AccountDataGateway _accountDataGateway;
        private PublicationDataGateway _publicationDataGateway;

        private IADPService _webService;
        /// <summary>
        /// Constant for Positive Dealer ID
        /// </summary>
        private const int FromDealerIDPositive = 101620;
        /// <summary>
        /// Constant for Provider ID
        /// </summary>
        private const int ProviderID = 7;

        /// <summary>
        /// Constant For Negative test of dealer ID
        /// </summary>
        public const int FromDealerIDNegative = 100024;
        /// <summary>
        /// Setup work common to all the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            //registry.Register<IADPService, MockADPService>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway, SubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway, AccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway, VehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, PublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IAppraisedVehicleDataGateway, AppraisedVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<IQueue, MemoryQueue>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger, ExceptionLogger>(ImplementationScope.Shared);
            registry.Register<IPublisher, ThreadedPublisher>(ImplementationScope.Shared);
            registry.Register<IADPService, VehicleSaveService>(ImplementationScope.Shared);

            ConfigurationManager.AppSettings["ADPId"] = "firstlook";
            ConfigurationManager.AppSettings["ADPPassword"] = "0001399d6a";
            ConfigurationManager.AppSettings["ADPURI"] = "https://stage-3pa.dmotorworks.com/pip-vehicle/services/VehicleInsertUpdate?wsdl";
            ConfigurationManager.AppSettings["MaximumPrice"] = "999999";
            ConfigurationManager.AppSettings["MinimumPrice"] = "0";
            ConfigurationManager.AppSettings["EnableDistribution"] = "1";
            ConfigurationManager.AppSettings["EnableADP"] = "1";
            ConfigurationManager.AppSettings["EnableeBiz"] = "1";

            IResolver resolver = RegistryFactory.GetResolver();

            _ADPBinding = new ADPBinding();
            _mockPublicationDataGateway = new MockPublicationDataGateway();
            _submissionDataGateway = new SubmissionDataGateway();
            _accountDataGateway = new AccountDataGateway();
            _publicationDataGateway = new PublicationDataGateway();

            _webService = resolver.Resolve<IADPService>();
            ((SoapHttpClientProtocol)_webService).Url =
                Properties.Settings.Default.FirstLook_Distribution_DomainModel_Services_ADP_VehicleSaveService;
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Setup work to be done prior to every test. Resets the static status tracking variables for the auto uplink
        /// binding and submission mocks.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockPublicationDataGateway.Reset();
        }

        #endregion

        #region Positive Tests

        /// <summary>
        /// Test that the binding works correctly for price and comment messages.
        /// </summary>
        [Test]
        public void SendADPInventoryValueTest()
        {
            try
            {
                //TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestADPMessage.xml");
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 106732 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 34434926, VehicleType = VehicleType.Inventory };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { Value = 18550, PriceType = PriceType.Internet };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                //new Distributor().Distribute(message);
                Publication publication = Publication.NewPublication(message);
                _publicationDataGateway.Save(publication);
                _submission = Submission.NewSubmission(ProviderID, publication.Id, MessageTypes.Price);
                _submissionDataGateway.Save(_submission);
                //// Check that an attempt was made to process the message.
                _ADPBinding.Publish(_submission);

                //Check if Data is Submitted successfuly in database. 
                Assert.IsTrue(1 == _submissionDataGateway.FetchSuccesses(_submission.Id).Count);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Negative Tests

        /// <summary>
        /// Test that the binding works correctly for price and comment messages.
        /// </summary>
        [Test]
        public void SendAppraisalValueFailTest()
        {
            try
            {
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 101620 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 15239083, VehicleType = VehicleType.Inventory };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { Value = 15000, PriceType = PriceType.Internet };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                //new Distributor().Distribute(message);
                Publication publication = Publication.NewPublication(message);
                _publicationDataGateway.Save(publication);
                _submission = Submission.NewSubmission(ProviderID, publication.Id, MessageTypes.Price);
                _submissionDataGateway.Save(_submission);
                //// Check that an attempt was made to process the message.
                _ADPBinding.Publish(_submission);

                //Check if Submission if it is marked as rejected
                Assert.IsTrue(_submissionDataGateway.FetchFailuresToRetry(5, 1).Count(sub => sub.Id == _submission.Id) >= 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Miscellaneous Tests

        [Test]
        public void ProviderSupportPositiveTest()
        {
            try
            {
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 101620 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 15239083, VehicleType = VehicleType.Inventory };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { Value = 15000, PriceType = PriceType.Internet };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                Publication publication = Publication.NewPublication(message);
                MessageTypes supportedTypes = publication.Message.MessageTypes;

                // Verify the dealer has active account info with the provider.
                Assert.IsTrue(_accountDataGateway.IsSupported(FromDealerIDPositive, ProviderID, ref supportedTypes));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [Test]
        public void ProviderSupportNegativeTest()
        {
            try
            {
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 101620 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 15239083, VehicleType = VehicleType.Inventory };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { Value = 15000, PriceType = PriceType.Internet };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                Publication publication = Publication.NewPublication(message);
                MessageTypes supportedTypes = publication.Message.MessageTypes;

                // Verify the dealer has active account info with the provider.
                Assert.IsFalse(_accountDataGateway.IsSupported(FromDealerIDNegative, ProviderID, ref supportedTypes));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [Test]
        public void DistributePositiveSuportDealer()
        {

            try
            {
                TransferObjects.V_1_0.Dealer dl = new TransferObjects.V_1_0.Dealer() { Id = 101620 };
                TransferObjects.V_1_0.Vehicle vl = new TransferObjects.V_1_0.Vehicle() { Id = 15239083, VehicleType = VehicleType.Inventory };
                TransferObjects.V_1_0.Price price = new TransferObjects.V_1_0.Price() { Value = 15000, PriceType = PriceType.Internet };

                TransferObjects.V_1_0.Message message = new TransferObjects.V_1_0.Message();
                message.Body = new MessageBody();

                message.Body.Parts.Add(price);
                message.From = dl;
                message.Subject = vl;
                SubmissionDetails submissionDetails = new Distributor().Distribute(message);
                //_ADPBinding.Publish(_submission);

                //Check if Data is Submitted successfuly in database. 
                Assert.IsTrue(submissionDetails.PublicationId == 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region UtilityMethods

        private vehicleReadResult ReadVehicle(authenticationToken auth, string dealerId, string stockNo)
        {
            vehicleReadResult result=  _webService.read(auth, new dealerId() { id = dealerId },
                             new vehicleReadRequest() { carInvStockNo = stockNo,fileTypeSpecified = true,fileType = fileType.CARINV});

            string request=TraceSoapExtension.XmlRequest.ToString();
            return result;
        }

        #endregion
    }

}
