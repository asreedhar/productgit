﻿using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Bindings
{
    /// <summary>
    /// Tests helper methods on the abstract class Binding.
    /// </summary>
    [TestFixture]
    public class BindingTest
    {
        /// <summary>
        /// Setup work common to all the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            RegistryFactory.GetRegistry().Register<IAccountDataGateway, MockAccountDataGateway>(ImplementationScope.Shared);
            RegistryFactory.GetRegistry().Register<IVehicleDataGateway, MockVehicleDataGateway>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Setup work to be done prior to every test. Resets the static status tracking variables for the auto uplink
        /// binding and submission mocks.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockAccountDataGateway.Reset();
            MockVehicleDataGateway.Reset();
        }

        [Test]
        public void FilterDescriptionFromFullMessageTest()
        {
            /*TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            Publication publication = Publication.NewPublication(message);
            Message theMessage = publication.Message;
            
            Assert.AreEqual(MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage, theMessage.MessageTypes);

            AccountPreferenceDictionary accountPreferenceDictionary = new AccountPreferenceDictionary(0, "blah");
            int providerId = 1;
            Provider provider = new Provider(providerId, "Provider");
            accountPreferenceDictionary.Add(provider, new MessageTypePreference(providerId, MessageTypes.Price, false));
            accountPreferenceDictionary.Add(provider, new MessageTypePreference(providerId, MessageTypes.Description, true));
            //by default, if the MessageType is missing as a preference, it is disabled, as in this case with MessageTypes.Mileage.
            MockAccountDataGateway.AccountPreferenceDictionary = accountPreferenceDictionary;

            Message filteredMsg = Binding.GetFilteredMsg(Registry.Resolve<IAccountDataGateway>(), providerId, theMessage);

            Assert.AreEqual(MessageTypes.Description, filteredMsg.MessageTypes);*/
        }

        [Test]
        public void ExactMatchMessageTest()
        {
            /*
            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TextContentPriceMessage.xml");
            Publication publication = Publication.NewPublication(message);
            Message theMessage = publication.Message;

            Assert.AreEqual(MessageTypes.Price | MessageTypes.Description, theMessage.MessageTypes);

            AccountPreferenceDictionary accountPreferenceDictionary = new AccountPreferenceDictionary(0, "blah");
            int providerId = 1;
            Provider provider = new Provider(providerId, "Provider");
            accountPreferenceDictionary.Add(provider, new MessageTypePreference(providerId, MessageTypes.Price, true));
            accountPreferenceDictionary.Add(provider, new MessageTypePreference(providerId, MessageTypes.Description, true));
            MockAccountDataGateway.AccountPreferenceDictionary = accountPreferenceDictionary;

            Message filteredMsg = Binding.GetFilteredMsg(Registry.Resolve<IAccountDataGateway>(), providerId, theMessage);

            Assert.AreEqual(MessageTypes.Description | MessageTypes.Price, filteredMsg.MessageTypes);*/
        }

        [Test]
        public void PreserveMessageTest()
        {/*
            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            Publication publication = Publication.NewPublication(message);
            Message theMessage = publication.Message;

            Assert.AreEqual(MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage, theMessage.MessageTypes);

            AccountPreferenceDictionary accountPreferenceDictionary = new AccountPreferenceDictionary(0, "blah");
            int providerId = 1;
            Provider provider = new Provider(providerId, "Provider");
            accountPreferenceDictionary.Add(provider, new MessageTypePreference(providerId, MessageTypes.Description, true));
            MockAccountDataGateway.AccountPreferenceDictionary = accountPreferenceDictionary;

            //used the wrong providerId
            Message filteredMsg = Binding.GetFilteredMsg(Registry.Resolve<IAccountDataGateway>(), 7, theMessage);

            Assert.AreEqual(MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage, filteredMsg.MessageTypes);*/
        }
    }
}
