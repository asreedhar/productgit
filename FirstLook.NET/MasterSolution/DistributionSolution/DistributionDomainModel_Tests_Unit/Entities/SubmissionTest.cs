using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Entities
{
    /// <summary>
    /// Test submissions.
    /// </summary>
    [TestFixture]
    public class SubmissionTest
    {
        #region Initialization

        /// <summary>
        /// Setup work common to the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            RegistryFactory.GetRegistry().Register<ISubmissionDataGateway, MockSubmissionDataGateway>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Reset the status tracking variables between tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {            
            MockSubmissionDataGateway.Reset();
        }

        #endregion

        /// <summary>
        /// Test that a submission moves through the expected states when submitted.
        /// </summary>
        [Test]
        public void StateChangeTest()
        {            
            Submission submission = Submission.NewSubmission(0, 0, MessageTypes.Price);

            submission.Accepted();
            Assert.AreEqual(MockSubmissionDataGateway.InsertCalled, true);

            submission.Rejected();
            Assert.AreEqual(MockSubmissionDataGateway.NumberUpdateCalled, 1);

            submission.Submitted();
            Assert.AreEqual(MockSubmissionDataGateway.NumberUpdateCalled, 2);

            submission.Superseded();
            Assert.AreEqual(MockSubmissionDataGateway.NumberUpdateCalled, 3);

            submission.InProgress();
            Assert.AreEqual(MockSubmissionDataGateway.NumberUpdateCalled, 4);

            submission.NotSupported();
            Assert.AreEqual(MockSubmissionDataGateway.NumberUpdateCalled, 5);
        }

        /// <summary>
        /// Test that when a submission is checked for being stale that the data gateway is called.
        /// </summary>
        [Test]
        public void IsStaleTest()
        {
            Submission submission = Submission.NewSubmission(0, 0, MessageTypes.Price);

            submission.FilterStaleTypes();

            Assert.IsTrue(MockSubmissionDataGateway.FetchStaleTypesCalled);
        }
    }
}
