using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Entities
{
    /// <summary>
    /// Test the entity base class correctly handles state changes.
    /// </summary>
    [TestFixture]
    public class EntityTest
    {                
        #region Initialization

        private MockAccountDataGateway _mockAccountDataGateway;

        /// <summary>
        /// Setup work common to all of the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            RegistryFactory.GetRegistry().Register<IAccountDataGateway, MockAccountDataGateway>(ImplementationScope.Shared);

            _mockAccountDataGateway = (MockAccountDataGateway)RegistryFactory.GetResolver().Resolve<IAccountDataGateway>();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        } 

        /// <summary>
        /// Reset the status tracking variables between tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockAccountDataGateway.Reset();
        }

        #endregion

        /// <summary>
        /// Test that when an entity is created and then saved, that the gateway calls Insert.
        /// </summary>
        [Test]
        public void InsertTest()
        {            
            Account account = Account.NewAccount(1, 1);

            _mockAccountDataGateway.Save(account);

            Assert.IsTrue(MockAccountDataGateway.InsertCalled);            
        }

        /// <summary>
        /// Test that when an entity is changed and then saved, that the gateway calls Update.
        /// </summary>
        [Test]
        public void UpdateTest()
        {
            Account account = Account.GetAccount(1, 1, "", "", true, "");            
            account.UserName = "Test";

            _mockAccountDataGateway.Save(account);

            Assert.IsTrue(MockAccountDataGateway.UpdateCalled);
        }

        /// <summary>
        /// Test that when an entity is marked for deletion and then saved, that the gateway called Delete.
        /// </summary>
        [Test]
        public void DeleteTest()
        {
            Account account = Account.GetAccount(1, 1, "", "", true, "");

            _mockAccountDataGateway.Delete(account);

            Assert.IsTrue(MockAccountDataGateway.DeleteCalled);
        }
    }
}
