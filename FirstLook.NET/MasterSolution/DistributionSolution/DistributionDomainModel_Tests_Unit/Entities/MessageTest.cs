using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Exceptions;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Entities
{
    /// <summary>
    /// Test the parsing of message values from user input.
    /// </summary>
    [TestFixture]
    public class MessageTest
    {
        #region Initialization

        private TransferObjects.V_1_0.Message _messageTO;

        /// <summary>
        /// Setup work common to the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            RegistryFactory.GetRegistry().Register<IAccountDataGateway, MockAccountDataGateway>(ImplementationScope.Shared);
            RegistryFactory.GetRegistry().Register<IVehicleDataGateway, MockVehicleDataGateway>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Reset the status tracking variables between tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockAccountDataGateway.Reset();
            MockVehicleDataGateway.Reset();
        }

        #endregion

        #region Type Tests

        /// <summary>
        /// Test that the type of a message is correctly determined.
        /// </summary>
        [Test]
        public void MessageTypeDeterminationTest()
        {
            // Test a message with three body part types.
            _messageTO = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            DomainModel.Entities.Message message = DomainModel.Entities.Message.NewMessage(_messageTO);                        
            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Description | 
                                                  DomainModel.Entities.MessageTypes.Price | 
                                                  DomainModel.Entities.MessageTypes.Mileage);

            // Test a message with description and price.
            _messageTO = TestUtilities.ParseMessageFromXml("TextContentPriceMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);
            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Description |
                                                  DomainModel.Entities.MessageTypes.Price);

            // Test a message with description and mileage.
            _messageTO = TestUtilities.ParseMessageFromXml("TextContentVehicleInfoMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Description |
                                                  DomainModel.Entities.MessageTypes.Mileage);

            // Test a message with price and mileage.
            _messageTO = TestUtilities.ParseMessageFromXml("PriceVehicleInfoMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Price |
                                                  DomainModel.Entities.MessageTypes.Mileage);

            // Test a message with just price.
            _messageTO = TestUtilities.ParseMessageFromXml("PriceMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Price);

            // Test a message with just description.
            _messageTO = TestUtilities.ParseMessageFromXml("TextContentMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Description);

            // Test a message with just mileage.
            _messageTO = TestUtilities.ParseMessageFromXml("VehicleInfoMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);
            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Mileage);
        }

        /// <summary>
        /// Test that the type of a message is correctly decomposed into a string.
        /// </summary>
        [Test]
        public void MessageTypeDecompositionTest()
        {
            // Test a message with three body part types.
            _messageTO = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            DomainModel.Entities.Message message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            string typeString = DomainModel.Entities.Message.ParseMessageTypesToString((int)message.MessageTypes);
            Assert.AreEqual(typeString, "Price and Description and Mileage");

            // Test a message with description and price.
            _messageTO = TestUtilities.ParseMessageFromXml("TextContentPriceMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            typeString = DomainModel.Entities.Message.ParseMessageTypesToString((int)message.MessageTypes);
            Assert.AreEqual(typeString, "Price and Description");

            // Test a message with description and mileage.
            _messageTO = TestUtilities.ParseMessageFromXml("TextContentVehicleInfoMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            typeString = DomainModel.Entities.Message.ParseMessageTypesToString((int)message.MessageTypes);
            Assert.AreEqual(typeString, "Description and Mileage");

            // Test a message with price and mileage.
            _messageTO = TestUtilities.ParseMessageFromXml("PriceVehicleInfoMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            typeString = DomainModel.Entities.Message.ParseMessageTypesToString((int)message.MessageTypes);
            Assert.AreEqual(typeString, "Price and Mileage");

            // Test a message with just price.
            _messageTO = TestUtilities.ParseMessageFromXml("PriceMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            typeString = DomainModel.Entities.Message.ParseMessageTypesToString((int)message.MessageTypes);
            Assert.AreEqual(typeString, "Price");

            // Test a message with just description.
            _messageTO = TestUtilities.ParseMessageFromXml("TextContentMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            typeString = DomainModel.Entities.Message.ParseMessageTypesToString((int)message.MessageTypes);
            Assert.AreEqual(typeString, "Description");

            // Test a message with just vehicle information.
            _messageTO = TestUtilities.ParseMessageFromXml("VehicleInfoMessage.xml");
            message = DomainModel.Entities.Message.NewMessage(_messageTO);            
            typeString = DomainModel.Entities.Message.ParseMessageTypesToString((int)message.MessageTypes);
            Assert.AreEqual(typeString, "Mileage");
        }

        #endregion

        #region Parsing Tests

        /// <summary>
        /// Test that a message transfer object is correctly parsed into an entity message.
        /// </summary>
        [Test]
        public void TransferObjectParseTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            DomainModel.Entities.Message message = DomainModel.Entities.Message.NewMessage(_messageTO);

            // Check the dealer / vehicle / number of body parts.
            Assert.AreEqual(_messageTO.From.Id, message.Dealer.Id);            
            Assert.AreEqual(_messageTO.Subject.Id, message.Vehicle.VehicleEntityId);            
            Assert.AreEqual(_messageTO.Body.Parts.Count, message.Body.Count);
            
            // Get the transfer object body parts.
            TransferObjects.V_1_0.Advertisement advertisementTO = null;
            TransferObjects.V_1_0.Price priceTO = null;
            TransferObjects.V_1_0.VehicleInformation vehicleInfoTO = null;            

            foreach (TransferObjects.V_1_0.BodyPart bodyPartTO in _messageTO.Body.Parts)
            {
                switch (bodyPartTO.BodyPartType)
                {
                    case TransferObjects.V_1_0.BodyPartType.Advertisement:
                        advertisementTO = bodyPartTO as TransferObjects.V_1_0.Advertisement;
                        break;

                    case TransferObjects.V_1_0.BodyPartType.Price:
                        priceTO = bodyPartTO as TransferObjects.V_1_0.Price;
                        break;

                    case TransferObjects.V_1_0.BodyPartType.VehicleInformation:
                        vehicleInfoTO = bodyPartTO as TransferObjects.V_1_0.VehicleInformation;
                        break;
                }
            }

            if (advertisementTO == null || priceTO == null || vehicleInfoTO == null)
            {
                Assert.Fail("Expected transfer object message parts were not found.");
                return;
            }

            // Get the entity body parts.
            DomainModel.Entities.Advertisement advertisement = null;
            DomainModel.Entities.Price price = null;
            DomainModel.Entities.VehicleInformation vehicleInfo = null;

            foreach (DomainModel.Entities.IBodyPart bodyPart in message.Body)
            {
                switch (bodyPart.BodyPartType)
                {
                    case DomainModel.Entities.BodyPartType.Advertisement:
                        advertisement = bodyPart as DomainModel.Entities.Advertisement;
                        break;

                    case DomainModel.Entities.BodyPartType.Price:
                        price = bodyPart as DomainModel.Entities.Price;
                        break;

                    case DomainModel.Entities.BodyPartType.VehicleInformation:
                        vehicleInfo = bodyPart as DomainModel.Entities.VehicleInformation;
                        break;                    
                }
            }
            
            if (advertisement == null || price == null || vehicleInfo == null)
            {
                Assert.Fail("Expected message body parts were not found.");
                return;
            }

            // Compare price / mileage / amount of advertisement content.
            Assert.AreEqual(priceTO.Value, price.Value);            
            Assert.AreEqual(vehicleInfoTO.Mileage, vehicleInfo.Mileage);                 
            Assert.AreEqual(advertisementTO.Contents.Count, advertisement.Contents.Count);

            // Get the text content transfer object.
            TransferObjects.V_1_0.TextContent textContentTO = null;

            foreach (TransferObjects.V_1_0.Content contentTO in advertisementTO.Contents)
            {
                if (contentTO.ContentType == TransferObjects.V_1_0.ContentType.Text)
                {
                    textContentTO = contentTO as TransferObjects.V_1_0.TextContent;
                }
            }

            if (textContentTO == null)
            {
                Assert.Fail("Expected text content transfer object was not found.");
                return;
            }

            // Get the text content entity.
            DomainModel.Entities.TextContent textContent = null;

            foreach (DomainModel.Entities.Content content in advertisement.Contents)
            {
                if (content.ContentType == DomainModel.Entities.ContentType.Text)
                {
                    textContent = content as DomainModel.Entities.TextContent;
                }
            }

            if (textContent == null)
            {
                Assert.Fail("Expected text content entity was not found.");
                return;
            }

            // Compare text content values.
            Assert.AreEqual(textContentTO.Text, textContent.Text);
        }

        #endregion

        #region Message Tests

        /// <summary>
        /// Test that the message type is correctly determined.
        /// </summary>
        [Test]
        public void MessageTypeTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            DomainModel.Entities.Message message = DomainModel.Entities.Message.NewMessage(_messageTO);

            Assert.AreEqual(message.MessageTypes, DomainModel.Entities.MessageTypes.Description |
                                                  DomainModel.Entities.MessageTypes.Price |
                                                  DomainModel.Entities.MessageTypes.Mileage);
        }

        /// <summary>
        /// Test that an exception is thrown when a null message is passed in.
        /// </summary>
        [Test]
        [ExpectedException(typeof(MessageNullException))]
        public void MessageNullTest()
        {                                           
            DomainModel.Entities.Message.NewMessage(null);                
        }

        #endregion

        #region Dealer Tests

        /// <summary>
        /// Test that an exception is thrown when a null dealer is passed in.
        /// </summary>
        [Test]
        [ExpectedException(typeof(DealerNullException))]
        public void DealerNullTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("DealerNull.xml");            
            DomainModel.Entities.Message.NewMessage(_messageTO);            
        }       

        #endregion

        #region Vehicle Tests

        /// <summary>
        /// Test that an exception is thrown when a null vehicle is passed in.
        /// </summary>
        [Test]
        [ExpectedException(typeof(VehicleNullException))]
        public void VehicleNullTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("VehicleNull.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);                         
        }

        /// <summary>
        /// Test that an exception is thrown when a vehicle has an unknown type.
        /// </summary>
        [Test]
        [ExpectedException(typeof(UnknownVehicleTypeException))]
        public void UnknownVehicleTypeTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("UnknownVehicleType.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        /// <summary>
        /// BUGZID: 15840
        /// Test that an exception is thrown when a vehicle claiming to be inventory comes in with an identifier that
        /// is not active inventory.
        /// </summary>        
        [Test]        
        public void NonActiveInventoryTest()
        {   
            MockVehicleDataGateway.VehicleExists = false;
 
            _messageTO = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            DomainModel.Entities.Message message = DomainModel.Entities.Message.NewMessage(_messageTO);

            Assert.AreEqual(message.Vehicle.VehicleEntityType, DomainModel.Entities.VehicleEntityType.NotDefined);
        }

        #endregion

        #region Body Tests

        /// <summary>
        /// Test that an exception is thrown when a null body is passed in.
        /// </summary>
        [Test]
        [ExpectedException(typeof(BodyNullException))]
        public void BodyNullTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("BodyNull.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        /// <summary>
        /// Test that an exception is thrown when an empty body is passed in.
        /// </summary>
        [Test]
        [ExpectedException(typeof(BodyEmptyException))]
        public void BodyEmptyTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("BodyEmpty.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        #endregion

        #region Advertisement Tests

        /// <summary>
        /// Test that an exception is thrown when an advertisement has no content.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ContentEmptyException))]
        public void ContentEmptyTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("ContentEmpty.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        /// <summary>
        /// Test that an exception is thrown when a text content object has no text.
        /// </summary>
        /// <remarks>
        /// FOGBUGZ 11256: Test originally expected an exception to be thrown, but empty descriptions are now allowed.        
        /// </remarks>
        [Test]        
        public void TextEmptyTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("TextEmpty.xml");
            DomainModel.Entities.Message message = DomainModel.Entities.Message.NewMessage(_messageTO);

            DomainModel.Entities.TextContent textContent = null;

            // Go find the empty text.
            foreach (DomainModel.Entities.IBodyPart bodyPart in message.Body)
            {
                DomainModel.Entities.Advertisement advertisement = bodyPart as DomainModel.Entities.Advertisement;
                if (advertisement != null)
                {
                    foreach (DomainModel.Entities.Content content in advertisement.Contents)
                    {
                        textContent = content as DomainModel.Entities.TextContent;
                        if (textContent != null)                            
                        {
                            Assert.AreEqual(string.Empty, textContent.Text);
                        }
                    }
                }
            }

            if (textContent == null)
            {
                Assert.Fail("Expected text content in the advertisement, but found none.");
            }            
        }

        /// <summary>
        /// Test that an exception is thrown when a text content object has just whitespace for text.
        /// </summary>
        /// <remarks>
        /// FOGBUGZ 11256: This test is no longer relevant.
        /// </remarks>
        [Test]
        [ExpectedException(typeof(TextEmptyException))]
        public void TextWhiteSpaceTest()
        {
            // THIS TEST IS IGNORED!
            Assert.Ignore("This test is no longer relevant due to FB 11256.");

            _messageTO = TestUtilities.ParseMessageFromXml("TextEmpty.xml");

            // Get the advertisement transfer object.
            TransferObjects.V_1_0.Advertisement advertisementTO = null;
            foreach (TransferObjects.V_1_0.BodyPart bodyPartTO in _messageTO.Body.Parts)
            {
                if (bodyPartTO.BodyPartType == TransferObjects.V_1_0.BodyPartType.Advertisement)
                {
                    advertisementTO = bodyPartTO as TransferObjects.V_1_0.Advertisement;
                }
            }
            if (advertisementTO == null)
            {
                Assert.Fail();
            }

            // Get the text content transfer object.
            TransferObjects.V_1_0.TextContent textContentTO = null;
            foreach (TransferObjects.V_1_0.Content contentTO in advertisementTO.Contents)
            {
                if (contentTO.ContentType == TransferObjects.V_1_0.ContentType.Text)
                {
                    textContentTO = contentTO as TransferObjects.V_1_0.TextContent;
                }
            }
            if (textContentTO == null)
            {
                Assert.Fail();
            }

            // Set the text content to just whitespace.
            textContentTO.Text = " ";

            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        #endregion               

        #region Price Tests

        /// <summary>
        /// Test that an exception is thrown when a price is too high.
        /// </summary>
        [Test]
        [ExpectedException(typeof(PriceHighException))]
        public void PriceHighTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("PriceHigh.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        /// <summary>
        /// Test that an exception is thrown when a price is too low.
        /// </summary>
        [Test]
        [ExpectedException(typeof(PriceLowException))]
        public void PriceLowTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("PriceLow.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        /// <summary>
        /// Test that an exception is thrown when a price has an unknown type.
        /// </summary>
        [Test]
        [ExpectedException(typeof(UnknownPriceTypeException))]
        public void UnknownPriceTypeTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("UnknownPriceType.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        /// <summary>
        /// Test that a message is correctly handled when it is given multiple prices to parse.
        /// </summary>
        [Test]
        public void MultiplePricesTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("MultiplePrices.xml");
            DomainModel.Entities.Message message = DomainModel.Entities.Message.NewMessage(_messageTO);
            
            int pricesCount = 0;

            foreach (DomainModel.Entities.IBodyPart bodyPart in message.Body)
            {
                if (bodyPart.BodyPartType == DomainModel.Entities.BodyPartType.Price)
                {
                    pricesCount++;
                }
            }

            Assert.AreEqual(2, pricesCount);            
        }

        #endregion

        #region Vehicle Information Tests

        /// <summary>
        /// Test that an exception is thrown when a price is too high.
        /// </summary>
        [Test]
        [ExpectedException(typeof(MileageHighException))]
        public void MileageHighTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("MileageHigh.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        /// <summary>
        /// Test that an exception is thrown when a price is too low.
        /// </summary>
        [Test]
        [ExpectedException(typeof(MileageLowException))]
        public void MileageLowTest()
        {
            _messageTO = TestUtilities.ParseMessageFromXml("MileageLow.xml");
            DomainModel.Entities.Message.NewMessage(_messageTO);
        }

        #endregion           
    }
}
