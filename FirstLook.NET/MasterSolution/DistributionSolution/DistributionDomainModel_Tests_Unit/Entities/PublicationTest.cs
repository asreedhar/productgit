using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Exceptions;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Entities
{
    /// <summary>
    /// Test publication values.
    /// </summary>
    [TestFixture]
    public class PublicationTest
    {
        #region Initialization

        /// <summary>
        /// Setup work common to the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<IAccountDataGateway,     MockAccountDataGateway>(ImplementationScope.Shared);
            registry.Register<IPublicationDataGateway, MockPublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     MockVehicleDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  MockSubmissionDataGateway>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Reset the status tracking variables between tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockAccountDataGateway.Reset();
            MockPublicationDataGateway.Reset();
            MockSubmissionDataGateway.Reset();
            MockVehicleDataGateway.Reset();
        }

        #endregion

        #region Parsing Tests

        /// <summary>
        /// Test that an exception is thrown when a new publication is created with a null message. Tests further 
        /// handling possible bad input as it relates to the message transfer object are handled by MessageTests.        
        /// </summary>
        [Test]
        [ExpectedException(typeof(MessageNullException))]
        public void TransferObjectParseTest()
        {            
            Publication.NewPublication(null);
        }

        #endregion

        #region Status Changing Tests

        /// <summary>
        /// Test that the status of a publication is properly changed when telling it to retract.
        /// </summary>
        [Test]
        public void RetractionTest()
        {
            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            Publication publication = Publication.NewPublication(message);

            publication.Retract();

            Assert.AreEqual(publication.PublicationStatus, PublicationState.Offline);
        }

        /// <summary>
        /// Test that the status of a publication is properly changed when telling it to go online.
        /// </summary>
        [Test]
        public void OnlineTest()
        {
            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            Publication publication = Publication.NewPublication(message);

            publication.Online();

            Assert.AreEqual(publication.PublicationStatus, PublicationState.Online);
        }

        /// <summary>
        /// Test that the status of a publication is properly changed when telling it that it was not distributed.
        /// </summary>
        [Test]
        public void NotDistributedTest()
        {
            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            Publication publication = Publication.NewPublication(message);

            publication.NotDistributed();

            Assert.AreEqual(publication.PublicationStatus, PublicationState.NotApplicable);   
        }

        #endregion

        #region Submission Creation Tests

        /// <summary>
        /// Test that submissions are created correctly for a publication.
        /// </summary>
        [Test]
        public void SubmissionCreationTest()
        {            
            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");
            Publication publication = Publication.NewPublication(message);

            publication.CreateSubmission(2, MessageTypes.Price | MessageTypes.Description | MessageTypes.Mileage);

            Assert.AreEqual(1, publication.Submissions.Count);
        }

        #endregion
    }
}
