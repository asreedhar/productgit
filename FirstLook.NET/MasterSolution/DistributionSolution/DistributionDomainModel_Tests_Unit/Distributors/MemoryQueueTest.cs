using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Distributors
{
    /// <summary>
    /// Tests that the in-memory queue implementation is correct.
    /// </summary>
    [TestFixture]
    public class MemoryQueueTest
    {
        #region Initialization

        private readonly IQueue _queue = new MemoryQueue();

        /// <summary>
        /// Setup work common to all of the following tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            RegistryFactory.GetRegistry().Register<ISubmissionDataGateway, MockSubmissionDataGateway>(ImplementationScope.Shared);
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Reset the status tracking variables between tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockSubmissionDataGateway.Reset();                        
        }

        #endregion

        /// <summary>
        /// Throw some submissions onto the memory queue, and then pop them off and make sure we get what we expected.
        /// </summary>
        [Test]
        public void PushPopTest()
        {
            Submission submission1 = Submission.NewSubmission(2 ,1, MessageTypes.Price);
            Submission submission2 = Submission.NewSubmission(2, 2, MessageTypes.Price);
            Submission submission3 = Submission.NewSubmission(2, 3, MessageTypes.Price);

            _queue.Push(submission1);
            _queue.Push(submission2);
            _queue.Push(submission3);

            Assert.AreEqual(3, _queue.Count());

            Submission submission4 = _queue.Pop();
            Assert.AreEqual(submission1, submission4);

            Submission submission5 = _queue.Pop();
            Assert.AreEqual(submission2, submission5);

            Submission submission6 = _queue.Pop();
            Assert.AreEqual(submission3, submission6);

            Submission submission7 = _queue.Pop();
            Assert.IsNull(submission7);

            Assert.AreEqual(0, _queue.Count());
        }
    }
}
