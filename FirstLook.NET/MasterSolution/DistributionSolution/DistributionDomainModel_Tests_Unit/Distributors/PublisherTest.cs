using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Distributors
{
    /// <summary>
    /// Publisher unit tests.
    /// </summary>
    [TestFixture]
    public class PublisherTest
    {
        #region Initialization

        Publisher _publisher;

        /// <summary>
        /// Setup the mock gateways and the publisher prior to running any tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            // Mocks.
            registry.Register<IPublicationDataGateway, MockPublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway,     MockAccountDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  MockSubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IBinding,                MockBinding>(ImplementationScope.Shared);
            registry.Register<IBindingFactory,         MockBindingFactory>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger,        MockExceptionLogger>(ImplementationScope.Shared);

            // The publisher and queue. It may seem antithetical that the focus of our testing is replaced by a mock,
            // but really what we want to test is the Publish() functionality in the Publisher abstract base class. The
            // ThreadedPublisher class does only one thing - give the threadpool a pointer to the Publish() method in
            // the base class. Therefore, ThreadedPublisher is not registered here since it would just lead to testing
            // of the framework. The MockPublisher calls the Publish() method of its base class directly.
            registry.Register<IPublisher, MockPublisher>(ImplementationScope.Shared);
            registry.Register<IQueue,     MemoryQueue>(ImplementationScope.Shared);

            _publisher = (Publisher)RegistryFactory.GetResolver().Resolve<IPublisher>();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Reset the status tracking variables between tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            MockPublicationDataGateway.Reset();
            MockAccountDataGateway.Reset();
            MockSubmissionDataGateway.Reset();
            MockBinding.Reset();
            MockBindingFactory.Reset();
            MockPublisher.Reset();
        }

        #endregion

        /// <summary>
        /// Test that the publisher will pop a submission off the queue and hand it off to the correct binding.
        /// </summary>
        [Test]
        public void SubmitTest()
        {
            Submission submission = Submission.NewSubmission(1, 1, MessageTypes.Price);

            // Push the dummy submission on the queue.
            IQueue queue = RegistryFactory.GetResolver().Resolve<IQueue>();
            queue.Push(submission);
            
            _publisher.Publish();            

            // Ensure that the binding was called.
            Assert.IsTrue(MockBinding.CalledPublish);
        }

        /// <summary>
        /// FB 13796: Test that exceptions that arise during Publish() are handled.
        /// </summary>        
        [Test]
        public void HandledExceptionTest()
        {
            Submission submission = Submission.NewSubmission(1, 1, MessageTypes.Price);

            // Push the dummy submission on the queue.
            IQueue queue = RegistryFactory.GetResolver().Resolve<IQueue>();
            queue.Push(submission);

            MockBindingFactory.ThrowException = true;
            _publisher.Publish();

            // Check that binding was not called, and that an exception was logged.
            Assert.IsFalse(MockBinding.CalledPublish);
            Assert.IsTrue(MockExceptionLogger.RecordCalled);
        }
    }
}
