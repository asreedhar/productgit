using System.Collections.ObjectModel;
using System.Configuration;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Bindings;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Distributors;
using FirstLook.Distribution.DomainModel.Tests.Common.Mocks;
using FirstLook.Distribution.DomainModel.Tests.Common.Utility;
using NUnit.Framework;

namespace FirstLook.Distribution.DomainModel.Tests.Unit.Distributors
{
    /// <summary>
    /// Distributor unit tests.
    /// </summary>
    [TestFixture]
    public class DistributorTest
    {
        #region Initialization
        
        private Distributor _distributor;

        /// <summary>
        /// Setup the mock gateways and the publisher prior to running any tests.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            // Mocks.
            registry.Register<IPublicationDataGateway, MockPublicationDataGateway>(ImplementationScope.Shared);
            registry.Register<IAccountDataGateway,     MockAccountDataGateway>(ImplementationScope.Shared);
            registry.Register<ISubmissionDataGateway,  MockSubmissionDataGateway>(ImplementationScope.Shared);
            registry.Register<IBindingFactory,         MockBindingFactory>(ImplementationScope.Shared);
            registry.Register<IPublisher,              MockPublisher>(ImplementationScope.Shared);
            registry.Register<IExceptionLogger,        MockExceptionLogger>(ImplementationScope.Shared);
            registry.Register<IVehicleDataGateway,     MockVehicleDataGateway>(ImplementationScope.Shared);

            // The distributor and queue.
            registry.Register<IDistributor, Distributor>(ImplementationScope.Shared);
            registry.Register<IQueue,       MemoryQueue>(ImplementationScope.Shared);

            _distributor = (Distributor)RegistryFactory.GetResolver().Resolve<IDistributor>();
        }

        /// <summary>
        /// Tear down this test fixture by clearing out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        /// <summary>
        /// Reset the status tracking variables between tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {            
            MockPublicationDataGateway.Reset();
            MockAccountDataGateway.Reset();
            MockSubmissionDataGateway.Reset();
            MockBindingFactory.Reset();
            MockPublisher.Reset();
            MockExceptionLogger.Reset();
            MockVehicleDataGateway.Reset();
        }

        #endregion

        #region Distribute Tests

        /// <summary>
        /// Test that the distributer does the following in order: inserts the publication; inserts one submission per 
        /// provider for this message; checks if account info exists; pushes onto queue.
        /// </summary>
        /// <remarks>
        /// Because distributing messages has been turned off for every environment except Release, these tests are no 
        /// longer able to be run.
        /// </remarks>
        [Test]
        public void DistributeTest()
        {
            ConfigurationManager.AppSettings["EnableDistribution"] = "1";
            ConfigurationManager.AppSettings["EnableAutoUplink"] = "1";
            ConfigurationManager.AppSettings["EnableAutoTrader"] = "1";

            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");

            // Signal the mock to allow all message types through.
            MockAccountDataGateway.IsProviderMessageTypeSupported = true;

            _distributor.Distribute(message);
                        
            // A publication should be saved.
            Assert.IsTrue(MockPublicationDataGateway.InsertCalled);                        

            // The mocks are configured so that submissions will be created for two providers.            
            Assert.AreEqual(2, MockAccountDataGateway.NumberSupportsMessageTypeCalled);
            Assert.AreEqual(2, MockSubmissionDataGateway.NumberInsertCalled);
            Assert.AreEqual(2, MockPublisher.NumberPublishCalled);

            ConfigurationManager.AppSettings["EnableDistribution"] = "0";
            ConfigurationManager.AppSettings["EnableAutoUplink"] = "0";
            ConfigurationManager.AppSettings["EnableAutoTrader"] = "0";
        }

        /// <summary>
        /// Test that the distributer does not distribute a message that is not supported by any provider.
        /// </summary>
        [Test]
        public void NotDistributedTest()
        {
            ConfigurationManager.AppSettings["EnableDistribution"] = "1";
            ConfigurationManager.AppSettings["EnableAutoUplink"] = "1";
            ConfigurationManager.AppSettings["EnableAutoTrader"] = "1";

            TransferObjects.V_1_0.Message message = TestUtilities.ParseMessageFromXml("TestMessage.xml");

            // Signal the mock to deny all message types.
            MockAccountDataGateway.IsProviderMessageTypeSupported = false;

            _distributor.Distribute(message);

            // A publication should be both inserted and updated.
            Assert.IsTrue(MockPublicationDataGateway.InsertCalled);
            Assert.IsTrue(MockPublicationDataGateway.UpdateCalled);            

            Assert.AreEqual(DomainModel.Entities.PublicationState.NotApplicable, 
                            MockPublicationDataGateway.PublicationStatus);

            ConfigurationManager.AppSettings["EnableDistribution"] = "0";
            ConfigurationManager.AppSettings["EnableAutoUplink"] = "0";
            ConfigurationManager.AppSettings["EnableAutoTrader"] = "0";
        }

        #endregion

        #region Status Tests

        /// <summary>
        /// Test that status gets the right submission details.
        /// </summary>
        [Test]
        public void StatusByPublicationId()
        {
            // The provider identifier given to Status() is ignored below.
            Collection<TransferObjects.V_1_0.SubmissionStatus> statuses = _distributor.Status(1);
            
            // The mock submission data gateway is configured to return the statuses of three submissions.
            Assert.AreEqual(3, statuses.Count);

            // The identifiers of the statuses are tied to their ordinal position.
            Assert.AreEqual(1, statuses[0].Provider.Id);
            Assert.AreEqual(2, statuses[1].Provider.Id);
            Assert.AreEqual(3, statuses[2].Provider.Id);

            // For each submission, details are fetched on their successes, errors, etc.
            Assert.AreEqual(3, MockAccountDataGateway.NumberFetchProviderCalled);
            Assert.AreEqual(3, MockSubmissionDataGateway.NumberFetchErrorCalled);
            Assert.AreEqual(3, MockSubmissionDataGateway.NumberFetchSuccessCalled);
        }

        /// <summary>
        /// Test that status gets the right submission details.
        /// </summary>
        [Test]
        public void StatusByDealerVehicle()
        {
            // The identifier given to Status() is ignored below.
            Collection<TransferObjects.V_1_0.SubmissionStatus> statuses =
                _distributor.Status(1, 1, TransferObjects.V_1_0.VehicleType.Inventory);

            // The mock submission data gateway is configured to return the statuses of three submissions.
            Assert.AreEqual(3, statuses.Count);

            // The identifiers of the statuses are tied to their ordinal position.
            Assert.AreEqual(1, statuses[0].Provider.Id);
            Assert.AreEqual(2, statuses[1].Provider.Id);
            Assert.AreEqual(3, statuses[2].Provider.Id);

            // For each submission, details are fetched on their successes, errors, etc.
            Assert.AreEqual(3, MockAccountDataGateway.NumberFetchProviderCalled);
            Assert.AreEqual(3, MockSubmissionDataGateway.NumberFetchErrorCalled);
            Assert.AreEqual(3, MockSubmissionDataGateway.NumberFetchSuccessCalled);
        }

        #endregion
    }
}
