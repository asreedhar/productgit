﻿using System.Security.Principal;
using System.ServiceProcess;
using System.Threading;

namespace FirstLook.Distribution.RedistributorService
{
    /// <summary>
    /// Windows service that polls the database for failed submissions and attempts to redistribute them, up to a 
    /// maximum number of retries.
    /// </summary>
    public partial class RedistributorService : ServiceBase
    {
        /// <summary>
        /// Thread to handle redistribution.
        /// </summary>
        private Thread redistributorThread;

        /// <summary>
        /// Processor that will call the Distribution web service.
        /// </summary>
        private readonly RedistributionProcessor processor;

        /// <summary>
        /// Initialize the service, and set the identity of the caller to be the 'admin' user.
        /// </summary>
        public RedistributorService()
        {
            InitializeComponent();
            processor = new RedistributionProcessor();
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("admin", "SOAP"), new string[0]);
        }

        /// <summary>
        /// When the redistribution service starts, start the processor.
        /// </summary>
        /// <param name="args">Arguments. Ignored.</param>
        protected override void OnStart(string[] args)
        {            
            redistributorThread = new Thread(processor.Process);
            processor.Start();
            redistributorThread.Start();
        }

        /// <summary>
        /// When the redistribution service stops, signal the processor to stop and wait on the thread joining.
        /// </summary>
        protected override void OnStop()
        {
            processor.Stop();
            redistributorThread.Join();            
        }
    }
}
