﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using FirstLook.Distribution.RedistributorService.RedistributorWebService;

namespace FirstLook.Distribution.RedistributorService
{
    /// <summary>
    /// Processor that signals to the Distribution web service to resend failures.
    /// </summary>
    public class RedistributionProcessor
    {
        /// <summary>
        /// The distribution service that will handle the retrying of submissions.
        /// </summary>
        private readonly Redistributor _redistributor;      

        /// <summary>
        /// Sentinel used to track whether processing should continue;
        /// </summary>
        private int _lifeCounter;

        /// <summary>
        /// Name of the redistribution service.
        /// </summary>
        private const string _sourceName = "FirstLook Redistribution Service";       

        /// <summary>
        /// Initialize the redistribution web service.
        /// </summary>
        public RedistributionProcessor()
        {                        
            _redistributor = new Redistributor();
        }        

        /// <summary>
        /// Resets the sentinel to restart processing.
        /// </summary>
        public void Start()
        {            
            if (_lifeCounter == 0)
            {
                Interlocked.Increment(ref _lifeCounter);
            }
        }

        /// <summary>
        /// Adjusts the sentinel to stop the processor.
        /// </summary>
        public void Stop()
        {            
            if (_lifeCounter == 1)
            {
                Interlocked.Decrement(ref _lifeCounter);
            }
        }        

        /// <summary>
        /// Tell the distribution web service to redistribute failed submissions every so many seconds. Will log how
        /// many submissions were redistributed, and whether or not there were any exceptions.
        /// </summary>
        public void Process()
        {
            int sleepSeconds = int.Parse(ConfigurationManager.AppSettings["SleepSeconds"]);
            
            while (_lifeCounter > 0)
            {
                try
                {                                    
                    int count = _redistributor.Redistribute();
                    EventLog.WriteEntry(_sourceName, "Redistributed " + count + (count == 1 ? " submission." : " submissions."));
                }
                catch (Exception exception)
                {
                    EventLog.WriteEntry(_sourceName, "EXCEPTION: " + exception);                                       
                }

                Thread.Sleep(sleepSeconds * 1000);
            }
        }
    }
}
