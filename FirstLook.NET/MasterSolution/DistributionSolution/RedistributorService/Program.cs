﻿using System.ServiceProcess;

namespace FirstLook.Distribution.RedistributorService
{
    /// <summary>
    /// Harness for running the Redistributor service.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the Redistributor windows service.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun = new ServiceBase[] { new RedistributorService() };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
