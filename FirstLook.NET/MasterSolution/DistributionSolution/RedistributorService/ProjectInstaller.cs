﻿using System.ComponentModel;
using System.Configuration.Install;


namespace FirstLook.Distribution.RedistributorService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
