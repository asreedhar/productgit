﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

namespace FirstLook.Distribution.WebControls.Account
{
    [ParseChildren(true), PersistChildren(false)]
    public class AccountDataSource : DataSourceControl
    {
        public AccountDataView AccountDataView
        {
            get { return new AccountDataView(this, SR.ViewName); }
        }

        private int? _dealerId;

        public int DealerId
        {
            get
            {
                if (!_dealerId.HasValue)
                {
                    object value = GetParameterValueFor(SR.DealerIdParameterName);

                    _dealerId = value is int ? (int)value : -1;
                }
                return _dealerId.Value;
            }
        }

        #region Overrides of DataSourceControl

        protected override DataSourceView GetView(string viewName)
        {
            if (string.IsNullOrEmpty(viewName) || (string.Compare(viewName, SR.ViewName, StringComparison.OrdinalIgnoreCase) == 0))
            {
                return AccountDataView;
            }
            throw new ArgumentOutOfRangeException("viewName");
        }

        protected override System.Collections.ICollection GetViewNames()
        {
            return new ReadOnlyCollection<string>(new[] { SR.ViewName });
        }

        #region Parameters
        private ParameterCollection _parameters;

        protected bool HasParameters
        {
            get { return _parameters != null; }
        }

        private object GetParameterValueFor(string parameterName)
        {
            object value = null;

            if (HasParameters)
            {
                Parameter namedParameter = Parameters[parameterName];

                if (namedParameter != null)
                {
                    IOrderedDictionary valueDictionary = Parameters.GetValues(Context, this);

                    value = valueDictionary[namedParameter.Name];
                }
            }

            return value;
        }

        [
            DefaultValue(null),
            Editor(typeof(ParameterCollectionEditor), typeof(UITypeEditor)),
            MergableProperty(false),
            PersistenceMode(PersistenceMode.InnerProperty),
            Category("Data")
        ]
        public ParameterCollection Parameters
        {
            get
            {
                if (_parameters == null)
                {
                    _parameters = new ParameterCollection();

                    _parameters.ParametersChanged += OnParametersChanged;

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_parameters).TrackViewState();
                    }
                }
                return _parameters;
            }
        }

        private void OnParametersChanged(object sender, EventArgs e)
        {
            AccountDataView.RaiseChangedEvent();
        }

        protected override void OnInit(EventArgs e)
        {
            Page.LoadComplete += OnPageLoadComplete;
        }

        private void OnPageLoadComplete(object sender, EventArgs e)
        {
            Parameters.UpdateValues(Context, this);
        }
        #endregion

        #endregion

        #region SR

        private static class SR
        {
            public const string ViewName = "AccountInfo";
            public const string DealerIdParameterName = "DealerId";
        }

        #endregion
    }
}
