﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Web.UI;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.DomainModel.Data;
using FirstLook.Distribution.DomainModel.Entities;

namespace FirstLook.Distribution.WebControls.Account
{
    public class AccountDataView : DataSourceView
    {
        public AccountDataView(IDataSource owner, string viewName)
            : base(owner, viewName)
        {
            _owner = owner as AccountDataSource;
        }

        private readonly AccountDataSource _owner;

        private int DealerId
        {
            get
            {
                return _owner.DealerId;
            }
        }

        private IAccountDataGateway _accountGateway;
        private IAccountDataGateway AccountGateway
        {
            get
            {
                if (_accountGateway == null)
                {
                    _accountGateway = RegistryFactory.GetResolver().Resolve<IAccountDataGateway>();
                }
                return _accountGateway;
            }
        }

        private readonly Dictionary<string, Type> _columns = new Dictionary<string, Type>
                                                                 {
                                                            {SR.ColumnProviderId, typeof(int)},
                                                            {SR.ColumnActive, typeof (bool)},
                                                            {SR.ColumnProviderName, typeof(string)},
                                                            {SR.ColumnDealerCode, typeof(string)},
                                                            {SR.ColumnMessageTypeDescription, typeof(string)},
                                                            {SR.ColumnPassword, typeof(string)}
                                                        };

        private readonly List<string> _accountEditableColumns = new List<string>
                                                                    {
                                                                        SR.ColumnActive,
                                                                        SR.ColumnDealerCode,
                                                                        SR.ColumnPassword
                                                                    };

        private readonly List<string> _preferenceEditableColumns = new List<string>
                                                                       {
                                                                        SR.ColumnMessageTypeDescription
                                                                    };

        private DataSet _accountInfoDataSet;
        private DataSet AccountInfoDataSet
        {
            get
            {
                if (_accountInfoDataSet == null)
                {
                    _accountInfoDataSet = new DataSet("AccountInfoDataSet") { Locale = CultureInfo.CurrentUICulture };

                    DataTable accountDataTable = new DataTable(SR.AccountsTableName) { Locale = CultureInfo.CurrentUICulture };
                    foreach (KeyValuePair<string, Type> pair in _columns)
                    {
                        accountDataTable.Columns.Add(new DataColumn(pair.Key, pair.Value));
                    }

                    DataTable providerMessageTypes = new DataTable(SR.ProviderMessageTypesTableName) { Locale = CultureInfo.CurrentUICulture };
                    providerMessageTypes.Columns.Add(SR.ColumnProviderId, typeof(int));
                    providerMessageTypes.Columns.Add(SR.ColumnMessageTypeDescription, typeof(string));

                    _accountInfoDataSet.Tables.Add(accountDataTable);
                    _accountInfoDataSet.Tables.Add(providerMessageTypes);
                    _accountInfoDataSet.Relations.Add("AccountProviderMessageTypes",
                                                      _accountInfoDataSet.Tables[SR.AccountsTableName].Columns[
                                                          SR.ColumnProviderId],
                                                      _accountInfoDataSet.Tables[SR.ProviderMessageTypesTableName].Columns[
                                                          SR.ColumnProviderId]);
                    _accountInfoDataSet.Relations[0].Nested = true;
                }

                return _accountInfoDataSet;
            }
        }

        #region Overrides of DataSourceView

        protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
        {
            _accountInfoDataSet = null;

            IList<Provider> providers = AccountGateway.FetchProviders();
            AccountPreferenceDictionary accountsPreferences = AccountGateway.FetchAccountPreferences(DealerId);

            DataTable accountInfoDataTable = AccountInfoDataSet.Tables[SR.AccountsTableName];

            foreach (Provider provider in providers)
            {
                DataRow dataRow = accountInfoDataTable.NewRow();
                dataRow[SR.ColumnProviderId] = provider.Id;
                dataRow[SR.ColumnProviderName] = provider.Name;

                accountInfoDataTable.Rows.Add(dataRow);

                if (AccountGateway.Exists(provider.Id, DealerId))
                {
                    IAccount account = AccountGateway.Fetch(provider.Id, DealerId);
                    dataRow[SR.ColumnActive] = account.Active;
                    dataRow[SR.ColumnDealerCode] = account.DealershipCode;
                    dataRow[SR.ColumnPassword] = account.Password;
                }

                if (accountsPreferences.ContainsKey(provider))
                {
                    IList<MessageTypePreference> providerPreference = accountsPreferences[provider];

                    DataTable providerMessageTypeDataTable = AccountInfoDataSet.Tables[SR.ProviderMessageTypesTableName];
                    
                    //find the max flag value that's enabled
                    //for example, "Price And Description" = 3, but "Price" will also be enabled ("Price = 1")
                    int maxMessageTypeFlag = (int)MessageTypes.None;
                    string messageTypeDescription = Enum.GetName(typeof(MessageTypes), MessageTypes.None);

                    //Add the default None row; the database didn't store None as a valid entry.
                    DataRow noneOptionRow = providerMessageTypeDataTable.NewRow();
                    noneOptionRow[SR.ColumnProviderId] = provider.Id;
                    noneOptionRow[SR.ColumnMessageTypeDescription] = messageTypeDescription;
                    providerMessageTypeDataTable.Rows.Add(noneOptionRow);

                    foreach (MessageTypePreference preference in providerPreference)
                    {
                        int messageTypeFlag = (int)preference.MessageTypes;
                        if (preference.Enabled && messageTypeFlag > maxMessageTypeFlag)
                        {
                            maxMessageTypeFlag = messageTypeFlag;
                            messageTypeDescription = preference.Description;
                        }

                        DataRow row = providerMessageTypeDataTable.NewRow();
                        row[SR.ColumnProviderId] = provider.Id;
                        row[SR.ColumnMessageTypeDescription] = preference.Description;
                        providerMessageTypeDataTable.Rows.Add(row);
                    }

                    dataRow[SR.ColumnMessageTypeDescription] = messageTypeDescription;
                }
            }

            return accountInfoDataTable.AsDataView();
        }

        protected override int ExecuteUpdate(IDictionary keys, IDictionary values, IDictionary oldValues)
        {
            bool updateAccount = false;
            bool updatePreferences = false;
            bool shouldUpdate = false;
            int returnValue = 0;

            foreach (var key in oldValues.Keys)
            {
                if (values.Contains(key))
                {
                    shouldUpdate |= values[key] != oldValues[key];
                }
            }

            if (shouldUpdate)
            {
                foreach (DictionaryEntry key in values)
                {
                    string name = key.Key as string;
                    if (!string.IsNullOrEmpty(name))
                    {
                        updateAccount |= _accountEditableColumns.Contains(name);
                        updatePreferences |= _preferenceEditableColumns.Contains(name);
                    }
                }

                if (updateAccount || updatePreferences)
                {
                    int providerId = int.Parse((string)values[SR.ColumnProviderId], CultureInfo.InvariantCulture);

                    if (updateAccount)
                    {
                        SaveAccount(values, providerId);

                        returnValue++;
                    }

                    if (updatePreferences)
                    {
                        SaveAccountPreference(values, providerId);

                        returnValue++;
                    }
                }
            }

            return returnValue;
        }

        private void SaveAccountPreference(IDictionary values, int providerId)
        {
            AccountPreferenceDictionary accountsPreferences = AccountGateway.FetchAccountPreferences(DealerId);

            foreach (KeyValuePair<Provider, Collection<MessageTypePreference>> preference in accountsPreferences)
            {
                if (preference.Key.Id != providerId)
                {
                    continue;
                }

                Collection<MessageTypes> supportedMessageTypes = new Collection<MessageTypes>();
                List<MessageTypePreference> messageTypePreferences = new List<MessageTypePreference>();
                MessageTypePreference thePreference = null;
                foreach (MessageTypePreference messageTypePreference in preference.Value)
                {
                    supportedMessageTypes.Add(messageTypePreference.MessageTypes);

                    if (messageTypePreference.Description.Equals(values[SR.ColumnMessageTypeDescription]))
                    {
                        thePreference = new MessageTypePreference(providerId,
                                                          messageTypePreference.MessageTypes,
                                                          true);

                        messageTypePreferences.Add(thePreference);
                    }
                }

                //need to get all the other MessageTypes that are included in the flag value, messageTypePreference.MessageTypes
                //ex: "Price And Description" option (value=3) needs to also save Price (value=1) and Description (value=2).
                if (thePreference != null)
                {
                    MessageTypes chosen = thePreference.MessageTypes;
                    foreach (MessageTypes messageTypes in Enum.GetValues(typeof(MessageTypes)))
                    {
                        if (messageTypes == MessageTypes.None)
                        {
                            continue;
                        }

                        //don't double add the already chosen messageType
                        if ((messageTypes != chosen)
                            && ((messageTypes & chosen) == messageTypes)
                            && supportedMessageTypes.Contains(messageTypes))
                        {
                            messageTypePreferences.Add(new MessageTypePreference(providerId,
                                                          messageTypes,
                                                          true));
                        }
                    }
                }

                AccountGateway.SaveAccountPreference(DealerId, providerId, messageTypePreferences);
            }
        }

        private void SaveAccount(IDictionary values, int providerId)
        {
            DomainModel.Entities.Account account;

            bool active = (bool)values[SR.ColumnActive];
            string userName = string.Empty;
            string dealershipCode = (string)values[SR.ColumnDealerCode];
            string password = (string)values[SR.ColumnPassword];

            if (AccountGateway.Exists(providerId, DealerId))
            {
                account = DomainModel.Entities.Account.GetAccount(DealerId, providerId, userName, password, active, dealershipCode);
            }
            else
            {
                account = DomainModel.Entities.Account.NewAccount(providerId, DealerId);
                account.Active = active;
                account.UserName = userName;
                account.DealershipCode = dealershipCode;
            }

            AccountGateway.Save(account);
        }

        protected override int ExecuteDelete(IDictionary keys, IDictionary oldValues)
        {
            throw new NotImplementedException();
        }

        internal void RaiseChangedEvent()
        {
            OnDataSourceViewChanged(EventArgs.Empty);
        }
        #endregion

        #region SR
        private static class SR
        {
            public const string AccountsTableName = "AccountInfoTable";
            public const string ProviderMessageTypesTableName = "ProviderMessageTypes";
            public const string ColumnProviderId = "ProviderId";
            public const string ColumnProviderName = "ProviderName";
            public const string ColumnActive = "Active";
            public const string ColumnDealerCode = "DealerCode";
            public const string ColumnMessageTypeDescription = "MessageTypeDescription";
            public const string ColumnPassword = "Password";
        }
        #endregion
    }
}
