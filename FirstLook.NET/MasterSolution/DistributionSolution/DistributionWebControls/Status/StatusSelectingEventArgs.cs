﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Distribution.WebControls.Status
{
    public class StatusSelectingEventArgs : CancelEventArgs
    {
        private readonly IOrderedDictionary _inputParameters;
        private readonly DataSourceSelectArguments _arguments;

        public IOrderedDictionary InputParameters
        {
            get { return _inputParameters; }
        }

        public DataSourceSelectArguments Arguments
        {
            get { return _arguments; }
        }

        public StatusSelectingEventArgs(IOrderedDictionary inputParameters, DataSourceSelectArguments arguments)
        {
            _inputParameters = inputParameters;
            _arguments = arguments;
        }
    }
}