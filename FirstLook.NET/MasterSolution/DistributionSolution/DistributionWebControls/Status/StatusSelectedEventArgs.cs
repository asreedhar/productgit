﻿using System;
using System.Collections;

namespace FirstLook.Distribution.WebControls.Status
{
    public class StatusSelectedEventArgs : EventArgs
    {
        private readonly Exception _exception;
        private readonly IDictionary _outputParameters;
        private readonly object _returnValue;

        public StatusSelectedEventArgs(object returnValue, IDictionary outputParameters)
            : this(returnValue, outputParameters, null)
        {
        }

        public StatusSelectedEventArgs(object returnValue, IDictionary outputParameters, Exception exception)
        {
            _returnValue = returnValue;
            _outputParameters = outputParameters;
            _exception = exception;
        }

        public Exception Exception
        {
            get { return _exception; }
        }

        public bool ExceptionHandled { get; set; }

        public IDictionary OutputParameters
        {
            get { return _outputParameters; }
        }

        public object ReturnValue
        {
            get { return _returnValue; }
        }
    }
}