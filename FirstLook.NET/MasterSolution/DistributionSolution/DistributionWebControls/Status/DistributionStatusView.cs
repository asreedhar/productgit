﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Distribution.WebService.Client.Distributor;

namespace FirstLook.Distribution.WebControls.Status
{
    public class DistributionStatusView : DataBoundControl, IPostBackEventHandler, INamingContainer
    {
        #region Properties

        private string _errorMessage;
        private bool _hasError;
        private string _text;

        public string Text
        {
            get { return _text; }
            set
            {
                if (value != _text)
                {
                    _text = value;
                }
            }
        }

        public bool HasError { get { return _hasError; } }
        public string ErrorMessage { get { return _errorMessage; } }

        public string CommandName { get; set; }
        public string CommandArgument { get; set; }

        public string OnClientClick { get; set; }

        #endregion

        #region Events
        private static readonly object EventClick = new object();
        private static readonly object EventCommand = new object();
        private static readonly object EventError = new object();

        public event EventHandler Click
        {
            add { Events.AddHandler(EventClick, value); }
            remove { Events.RemoveHandler(EventClick, value); }
        }

        protected virtual void OnClick(EventArgs eventArgs)
        {
            EventHandler handler = Events[EventClick] as EventHandler;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }

        public event CommandEventHandler Command
        {
            add { Events.AddHandler(EventCommand, value);}
            remove { Events.RemoveHandler(EventCommand, value);}
        }

        protected virtual void OnCommand(CommandEventArgs eventArgs)
        {
            CommandEventHandler handler = Events[EventCommand] as CommandEventHandler;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }

        public event EventHandler Error
        {
            add { Events.AddHandler(EventError, value); }
            remove { Events.RemoveHandler(EventError, value); }
        }

        protected virtual void OnError(EventArgs eventArgs)
        {
            EventHandler handler = Events[EventError] as EventHandler;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }

        #endregion

        #region Rendering
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Span;
            }
        }
        protected override string TagName
        {
            get
            {
                return "span";
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(CssClass))
            {
                CssClass += " " + SR.CssClass;
            }
            else
            {
                CssClass = SR.CssClass;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass);

            writer.RenderBeginTag(TagKey);

            RenderLabel(writer);

            writer.RenderEndTag();
        }

        private void RenderLabel(HtmlTextWriter writer)
        {
            string labelId = UniqueID + IdSeparator + SR.LabelIdSuffix;
            string buttonId = UniqueID;
            string toolTip = string.IsNullOrEmpty(ToolTip) ? SR.DefaultToolTip : ToolTip;

            if (!string.IsNullOrEmpty(OnClientClick))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Onclick, OnClientClick);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Id, labelId);
            writer.AddAttribute(HtmlTextWriterAttribute.Title, ErrorMessage);
            writer.AddAttribute(HtmlTextWriterAttribute.For, buttonId);
            if (!string.IsNullOrEmpty(toolTip))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Title, toolTip);
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            RenderButton(writer);
            writer.RenderEndTag(); // Label
        }

        private void RenderButton(HtmlTextWriter writer)
        {
            string buttonId = UniqueID;
            string text = string.IsNullOrEmpty(_text) ? SR.DefaultText : _text;
	
            writer.AddAttribute(HtmlTextWriterAttribute.Id, buttonId);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, text);
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, Page.ClientScript.GetPostBackEventReference(this, string.Empty) + ";return false;");

            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();    
        }

        #endregion

        #region DataBinding
        protected override void PerformDataBinding(IEnumerable data)
        {
            if (data != null)
            {
                IEnumerator en = data.GetEnumerator();

                string exceptionMessage = null;
                bool moreThanOne = false;

                while (en.MoveNext())
                {
                    object submissionStatus = en.Current;

                    SubmissionState status = (SubmissionState) Enum.ToObject(typeof (SubmissionState), DataBinder.GetPropertyValue(submissionStatus, SR.StateField));

                    if (status == SubmissionState.Rejected)
                    {
                        _hasError = true;
                        if (string.IsNullOrEmpty(_errorMessage))
                        {
                            _errorMessage = string.Empty;
                        }                        
                                         
                        // Get submission errors.
                        SubmissionStatus submissionStatusObj = (SubmissionStatus)submissionStatus;
                        SubmissionError[] errors = submissionStatusObj.Errors;                        
                        foreach (SubmissionError error in errors)
                        {
                            if (moreThanOne)
                            {
                                _errorMessage += "\n";
                            }
                            _errorMessage += submissionStatusObj.Provider.Name + " failed to update ";
                            _errorMessage += error.Description;
                            _errorMessage += " (" + error.ErrorMessage + ")";
                            moreThanOne = true;
                        }
                       
                        // Get exception.
                        string message = DataBinder.GetPropertyValue(submissionStatus, SR.MessageField, "{0}");
                        if (!string.IsNullOrEmpty(message))
                        {
                            exceptionMessage = message;
                        }
                    }
                }

                // Exception is pulled out of the above loop, since the exception message would be the same for all 
                // the submission statuses above, and we only want to display it once.
                if (!string.IsNullOrEmpty(exceptionMessage))
                {
                    if (moreThanOne)
                    {
                        _errorMessage += "\n";
                    }
                    _errorMessage += "An exception occurred during processing. Please contact a customer service representative.";
                }
            }

            Visible = _hasError;
        }
        #endregion

        #region Implementation of IPostBackEventHandler

        [SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate", Justification = "This method is amazing.  You get a complaint if you don't include the protected one, but when you do, it tells you to use events.")]
        protected void RaisePostBackEvent(string eventArgument)
        {
            OnClick(new EventArgs());
            RaiseBubbleEvent(this, new CommandEventArgs(CommandName, eventArgument));
        }

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            if (string.IsNullOrEmpty(eventArgument))
            {
                eventArgument = _errorMessage;
            }
            RaisePostBackEvent(eventArgument); 
        }

        #endregion

        #region StateManager
        protected override object SaveViewState()
        {
            object[] state = new object[5];
            int idx = 0;

            state[idx++] = _hasError;
            state[idx++] = _errorMessage;
            state[idx++] = _text;
            state[idx++] = CommandName;
            state[idx] = CommandArgument;

            return new Pair(base.SaveViewState(), state);
        }

        protected override void LoadViewState(object savedState)
        {
            Pair pair = savedState as Pair;
            if (pair != null)
            {
                base.LoadViewState(pair.First);

                object[] state = pair.Second as object[];
                int idx = 0;

                if (state != null)
                {
                    _hasError = (bool)state[idx++];
                    _errorMessage = (string)state[idx++];
                    _text = (string)state[idx++];
                    CommandName = (string)state[idx++];
                    CommandArgument = (string)state[idx]; 
                }
            }
            else
            {
                base.LoadViewState(savedState); 
            }
        }
        #endregion

        #region StringResources
        private static class SR
        {
            public const string CssClass = "export_error";

            public const string LabelIdSuffix = "Label";
            //public const string ButtonIdSuffix = "Button";

            public const string DefaultToolTip = "Export Error";
            public const string DefaultText = "Export Error";

            public const string MessageField = "Message";
            public const string ErrorsField = "Errors";
            public const string StateField = "State";
        }
        #endregion

        
    }
}