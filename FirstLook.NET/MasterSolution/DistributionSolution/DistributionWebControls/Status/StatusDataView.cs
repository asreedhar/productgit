using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Web.UI;
using FirstLook.Distribution.WebService.Client.Distributor;

namespace FirstLook.Distribution.WebControls.Status
{
    public class StatusDataView : DataSourceView, IDisposable
    {
        public StatusDataView(DistributionStatusDataSource owner, string viewName)
            : base(owner, viewName)
        {
            _owner = owner;
        }

        #region Properties

        private readonly DistributionStatusDataSource _owner;

        private Distributor _service;
        private Distributor Service
        {
            get
            {
                if (_service == null)
                {
                    var userIdentity = new UserIdentity {UserName = Thread.CurrentPrincipal.Identity.Name};

                    _service = new Distributor {UserIdentityValue = userIdentity};
                }

                return _service;
            }
        }

        #endregion

        #region Events
        private static readonly object EventSelected = new object();
        private static readonly object EventSelecting = new object();

        public event EventHandler<StatusSelectedEventArgs> Selected
        {
            add { Events.AddHandler(EventSelected, value); }
            remove { Events.RemoveHandler(EventSelected, value); }
        }

        protected virtual void OnSelected(StatusSelectedEventArgs e)
        {
            EventHandler<StatusSelectedEventArgs> handler = Events[EventSelected] as EventHandler<StatusSelectedEventArgs>;
            if (handler != null)
            {
                handler(_owner, e);
            }
        }

        public event EventHandler<StatusSelectingEventArgs> Selecting
        {
            add { Events.AddHandler(EventSelecting, value); }
            remove { Events.RemoveHandler(EventSelecting, value); }
        }

        protected virtual void OnSelecting(StatusSelectingEventArgs e)
        {
            EventHandler<StatusSelectingEventArgs> handler = Events[EventSelecting] as EventHandler<StatusSelectingEventArgs>;
            if (handler != null)
            {
                handler(_owner, e);
            }
        }


        internal void RaiseChangedEvent()
        {
            OnDataSourceViewChanged(EventArgs.Empty);
        }
        #endregion


        private ICollection<SubmissionStatus> GetVehicleStatus(int dealerId, VehicleType vehicleType, int vehicleId)
        {
            ICollection<SubmissionStatus> submissionDetailes = new Collection<SubmissionStatus>();

            bool raisedStatusEvent = false;

            try
            {
                submissionDetailes = Service.VehicleStatus(dealerId, vehicleType, vehicleId);
            }
            catch (Exception exception)
            {
                StatusSelectedEventArgs statusSelectedEventArgs = new StatusSelectedEventArgs(false, new OrderedDictionary(), exception);

                statusSelectedEventArgs.ExceptionHandled = null != statusSelectedEventArgs.Exception as SocketException ||
                                                           null != statusSelectedEventArgs.Exception as WebException;

                OnSelected(statusSelectedEventArgs);

                if (!statusSelectedEventArgs.ExceptionHandled)
                {
                    throw;
                }

                raisedStatusEvent = true;
            }
            finally
            {
                if (!raisedStatusEvent)
                {
                    StatusSelectedEventArgs statusSelectedEventArgs = new StatusSelectedEventArgs(true, new OrderedDictionary());

                    OnSelected(statusSelectedEventArgs); 
                }
            }

            return submissionDetailes;
        }

        private static VehicleType GetVehicleType(bool isInventory)
        {
            return isInventory ? VehicleType.Inventory : VehicleType.NotDefined;
        }

        #region Overrides of DataSourceView

        protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
        {
            arguments.RaiseUnsupportedCapabilitiesError(this);

            // get values from DataSource
            int dealerId = _owner.DealerId;
            int vehicleId = _owner.VehicleId;
            VehicleType vehicleType = GetVehicleType(_owner.IsInventory); 

            OrderedDictionary parameters = new OrderedDictionary(StringComparer.OrdinalIgnoreCase)
                                               {
                                                   {SR.DealerIdParameterName, dealerId},
                                                   {SR.VehicleIdParameterName, vehicleId},
                                                   {SR.IsInventoryParameterName, vehicleType}
                                               };

            StatusSelectingEventArgs statusSelectingEventArgs = new StatusSelectingEventArgs(parameters, arguments);

            // Raise Selecting Event
            OnSelecting(statusSelectingEventArgs);

            // Update Selected Values
            if (statusSelectingEventArgs.Cancel)
            {
                return null;
            }

            if (!dealerId.Equals(statusSelectingEventArgs.InputParameters[SR.DealerIdParameterName]))
            {
                dealerId = (int)statusSelectingEventArgs.InputParameters[SR.DealerIdParameterName];
            }
            if (!vehicleId.Equals(statusSelectingEventArgs.InputParameters[SR.VehicleIdParameterName]))
            {
                vehicleId = (int)statusSelectingEventArgs.InputParameters[SR.VehicleIdParameterName];
            }
            if (!_owner.IsInventory.Equals(statusSelectingEventArgs.InputParameters[SR.IsInventoryParameterName]))
            {
                vehicleType = GetVehicleType((bool)statusSelectingEventArgs.InputParameters[SR.IsInventoryParameterName]);
            }

            return GetVehicleStatus(dealerId, vehicleType, vehicleId);
        }

        #endregion

        #region SR
        private static class SR
        {
            public const string DealerIdParameterName = "DealerId";
            public const string VehicleIdParameterName = "VehicleId";
            public const string IsInventoryParameterName = "IsInventory";
        }
        #endregion

        #region Implementation of IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_service != null)
                {
                    _service.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}