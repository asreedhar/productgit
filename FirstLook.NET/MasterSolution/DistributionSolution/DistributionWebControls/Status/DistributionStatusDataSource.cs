﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.Design.WebControls;
using System.Web.UI.WebControls;

[assembly: CLSCompliant(true)]

namespace FirstLook.Distribution.WebControls.Status
{
    [ParseChildren(true), PersistChildren(false)]
    public class DistributionStatusDataSource : DataSourceControl
    {
        [Category("Data"), Description("ObjectDataSource_Selected")]
        public event EventHandler<StatusSelectedEventArgs> Selected
        {
            add
            {
                StatusStatusDataView.Selected += value;
            }
            remove
            {
                StatusStatusDataView.Selected -= value;
            }
        }

        [Category("Data"), Description("ObjectDataSource_Selecting")]
        public event EventHandler<StatusSelectingEventArgs> Selecting
        {
            add
            {
                StatusStatusDataView.Selecting += value;
            }
            remove
            {
                StatusStatusDataView.Selecting -= value;
            }
        }

        private StatusDataView _statusStatusDataView;
        protected StatusDataView StatusStatusDataView
        {
            get
            {
                if (_statusStatusDataView == null)
                {
                    _statusStatusDataView = new StatusDataView(this, SR.ViewName);
                }
                return _statusStatusDataView;
            }
        }

        private ParameterCollection _parameters;

        protected bool HasParameters
        {
            get { return _parameters != null; }
        }

        private int _vehicleId;
        private int _dealerId = -1;
        private bool? _isInventory;

        public int VehicleId
        {
            get
            {
                if (_vehicleId == -1)
                {
                    object value = GetParameterValueFor(SR.VehicleIdParameterName);

                    _vehicleId = value is int ? (int)value : -1;
                }

                return _vehicleId;
            }
        }

        public int DealerId
        {
            get
            {
                if (_dealerId == -1)
                {
                    object value = GetParameterValueFor(SR.DealerIdParameterName);

                    _dealerId = value is int ? (int) value : -1;
                }

                return _dealerId;
            }
        }

        public bool IsInventory
        {
            get
            {
                if (!_isInventory.HasValue)
                {
                    object value = GetParameterValueFor(SR.IsInventoryParameterName);

                    _isInventory = value is bool ? (bool) value : false;
                }

                return _isInventory.GetValueOrDefault();
            }
        }

        private object GetParameterValueFor(string parameterName)
        {
            object value = null;

            if (HasParameters)
            {
                Parameter namedParameter = Parameters[parameterName];

                if (namedParameter != null)
                {
                    IOrderedDictionary valueDictionary = Parameters.GetValues(Context, this);

                    value = valueDictionary[namedParameter.Name];
                }
            }

            return value;
        }

        [
            DefaultValue(null),
            Editor(typeof(ParameterCollectionEditor), typeof(UITypeEditor)),
            MergableProperty(false),
            PersistenceMode(PersistenceMode.InnerProperty),
            Category("Data")
        ]
        public ParameterCollection Parameters
        {
            get
            {
                if (_parameters == null)
                {
                    _parameters = new ParameterCollection();

                    _parameters.ParametersChanged += OnParametersChanged;

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_parameters).TrackViewState();
                    }
                }
                return _parameters;
            }
        }

        private void OnParametersChanged(object sender, EventArgs e)
        {
            StatusStatusDataView.RaiseChangedEvent();
        }

        protected override void OnInit(EventArgs e)
        {
            Page.LoadComplete += OnPageLoadComplete;
        }

        private void OnPageLoadComplete(object sender, EventArgs e)
        {
            Parameters.UpdateValues(Context, this);
        }

        #region Overrides of DataSourceControl

        protected override DataSourceView GetView(string viewName)
        {
            if (string.IsNullOrEmpty(viewName) || (string.Compare(viewName, SR.ViewName, StringComparison.OrdinalIgnoreCase) == 0))
            {
                return StatusStatusDataView;
            }
            throw new ArgumentOutOfRangeException("viewName");
        }

        protected override ICollection GetViewNames()
        {
            return new ReadOnlyCollection<string>(new[] {SR.ViewName});
        }

        #endregion

        #region SR
        private static class SR
        {
            public const string ViewName = "Status";
            public const string DealerIdParameterName = "DealerId";
            public const string VehicleIdParameterName = "VehicleId";
            public const string IsInventoryParameterName = "IsInventory";
        }
        #endregion
    }
}