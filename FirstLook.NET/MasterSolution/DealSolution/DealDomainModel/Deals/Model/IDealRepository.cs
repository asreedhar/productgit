﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    public interface IDealRepository
    {
        #region Reference Information

        /// <summary>
        /// First edition is a fixed list for all clients.  Second edition should allow each client to have their own motives.
        /// </summary>
        /// <returns></returns>
        IList<DealMotive> DealMotives();

        IList<DealStatus> DealStatuses();

        IList<InventoryStrategy> InventoryStrategies();

        IList<NoteType> NoteTypes();

        IList<PriceType> PriceTypes();

        #endregion

        #region Instance Information

        Page<Deal> Search(IBroker broker, Deal example, PageArguments pagination);

        Deal Fetch(IBroker broker, int id);

        Deal Save(IBroker broker, IPrincipal principal, Deal deal);
        
        #endregion
    }    
}
