﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    /// <summary>
    /// <list>
    ///     <item>Open</item>
    ///     <item>Closed
    ///         <list>
    ///             <item>Won</item>
    ///             <item>Lost</item>
    ///             <item>Not Interested</item>
    ///         </list>
    ///     </item>
    /// </list>
    /// </summary>
    [Serializable]
    public class DealStatus
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }

        public bool IsOpen { get; internal set; }

        public bool IsClosed { get; internal set; }
    }
}
