﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;
using FirstLook.Deal.DomainModel.Employees.Model;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    [Serializable]
    public class Price : BusinessObject
    {
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public int RevisionNumber { get; protected internal set; } // same as sequence

        public int Id { get; protected internal set; }

        #region Properties

        private DateTime _created;

        public DateTime Created
        {
            get { return _created; }
            set { Let(Inspect.NameOf((Price p) => p.Created), ref _created, value); }
        }

        private PriceType _priceType;

        public PriceType PriceType
        {
            get { return _priceType; }
            set { Let(Inspect.NameOf((Price p) => p.PriceType), ref _priceType, value); }
        }

        private decimal _value;

        public decimal Value
        {
            get { return _value; }
            set { Let(Inspect.NameOf((Price p) => p.Value), ref _value, value); }
        }

        private Employee _employee;

        public Employee Employee
        {
            get { return _employee; }
            set { Let(Inspect.NameOf((Price p) => p.Employee), ref _employee, value); }
        }

        #endregion

        #region Validation

        private ValidationRules<Price> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Price>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Price>>()));
        }

        #endregion
    }
}
