﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    /// <summary>
    /// <list>
    ///     <item>
    ///         <term>Purchase</term>
    ///         <description>The dealer is buying the vehicle from someone</description>
    ///     </item>
    ///     <item>
    ///         <term>Sale</term>
    ///         <description>The dealer is selling the vehicle to someone</description>
    ///     </item>
    /// </list>
    /// </summary>
    [Serializable]
    public class TransactionType
    {
        public int Id { get; protected internal set; }

        public string Name { get; internal set; }
    }
}
