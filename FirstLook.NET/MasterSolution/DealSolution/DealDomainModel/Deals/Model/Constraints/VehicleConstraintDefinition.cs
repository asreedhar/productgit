﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;

namespace FirstLook.Deal.DomainModel.Deals.Model.Constraints
{
    public class VehicleConstraintDefinition : ConstraintDefinition<Vehicle>
    {
        public VehicleConstraintDefinition()
        {
            PropertyConstraints.Add(
                   Inspect.NameOf((Vehicle x) => x.Identification),
                   new List<IConstraint<Vehicle>>
                    {
                        new PropertyConstraint<Vehicle, ClientVehicleIdentification>(
                            x => x.Identification,
                            new RequiredConstraint<ClientVehicleIdentification>())
                    });

            PropertyConstraints.Add(
                   Inspect.NameOf((Vehicle x) => x.TransactionType),
                   new List<IConstraint<Vehicle>>
                    {
                        new PropertyConstraint<Vehicle, TransactionType>(
                            x => x.TransactionType,
                            new RequiredConstraint<TransactionType>())
                    });

            PropertyConstraints.Add(
                   Inspect.NameOf((Vehicle x) => x.InventoryStrategy),
                   new List<IConstraint<Vehicle>>
                    {
                        new PropertyConstraint<Vehicle, InventoryStrategy>(
                            x => x.InventoryStrategy,
                            new RequiredConstraint<InventoryStrategy>())
                    });

            PropertyConstraints.Add(
                   Inspect.NameOf((Vehicle x) => x.Prices),
                   new List<IConstraint<Vehicle>>
                    {
                        new PropertyConstraint<Vehicle, IList<Price>>(
                            x => x.Prices,
                            new CompositeCollectionConstraint<Price>())
                    });

            PropertyConstraints.Add(
                   Inspect.NameOf((Vehicle x) => x.Notes),
                   new List<IConstraint<Vehicle>>
                    {
                        new PropertyConstraint<Vehicle, IList<Note>>(
                            x => x.Notes,
                            new CompositeCollectionConstraint<Note>()),
                        new PropertyConstraint<Vehicle, IList<Note>>(
                            x => x.Notes,
                            new OneNoteType()),
                    });
        }

        class OneNoteType : IConstraint<IList<Note>>
        {
            private readonly string _resourceKey;

            public OneNoteType() : this("deal.constraint.note.duplicate")
            {
            }

            public OneNoteType(string resourceKey)
            {
                _resourceKey = resourceKey;
            }

            public bool IsSatisfiedBy(IList<Note> value)
            {
                if (value == null)
                {
                    return true;
                }

                if (value.GroupBy(x => ((x.NoteType == null) ? 0 : x.NoteType.Id)).Any(y => y.Count() > 1))
                {
                    return false;
                }

                return true;
            }

            public string ResourceKey
            {
                get { return _resourceKey; }
            }

            public bool StopProcessing
            {
                get { return false; }
            }
        }
    }
}
