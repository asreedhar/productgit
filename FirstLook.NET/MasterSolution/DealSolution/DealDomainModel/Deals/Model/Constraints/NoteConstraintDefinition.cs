﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;

namespace FirstLook.Deal.DomainModel.Deals.Model.Constraints
{
    public class NoteConstraintDefinition : ConstraintDefinition<Note>
    {
        public NoteConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Note x) => x.NoteType),
                new List<IConstraint<Note>>
                    {
                        new PropertyConstraint<Note, NoteType>(
                            x => x.NoteType,
                            new RequiredConstraint<NoteType>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Note x) => x.Text),
                new List<IConstraint<Note>>
                    {
                        new PropertyConstraint<Note, string>(
                            x => x.Text,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Note, string>(
                            x => x.Text,
                            new BetweenLengthConstraint(1,250))
                    });
        }
    }
}
