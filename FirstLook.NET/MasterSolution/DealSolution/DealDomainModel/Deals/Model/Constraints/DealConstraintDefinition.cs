﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Deal.DomainModel.Parties.Model;

namespace FirstLook.Deal.DomainModel.Deals.Model.Constraints
{
    /// <remarks>
    /// Missing object constraints (at least one vehicle with a transaction type of 'buy').
    /// </remarks>
    public class DealConstraintDefinition : ConstraintDefinition<Deal>
    {
        public DealConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Deal x) => x.DealMotive),
                new List<IConstraint<Deal>>
                    {
                        new PropertyConstraint<Deal, DealMotive>(
                            x => x.DealMotive,
                            new RequiredConstraint<DealMotive>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Deal x) => x.DealMotive),
                new List<IConstraint<Deal>>
                    {
                        new PropertyConstraint<Deal, DealStatus>(
                            x => x.DealStatus,
                            new RequiredConstraint<DealStatus>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Deal x) => x.SalesPerson),
                new List<IConstraint<Deal>>
                    {
                        new PropertyConstraint<Deal, Employee>(
                            x => x.SalesPerson,
                            new CompositeConstraint<Employee>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Deal x) => x.Parties),
                new List<IConstraint<Deal>>
                    {
                        new PropertyConstraint<Deal, IList<Party>>(
                            x => x.Parties,
                            new RequiredConstraint<IList<Party>>()),
                        new PropertyConstraint<Deal, IList<Party>>(
                            x => x.Parties,
                            new CompositeCollectionConstraint<Party>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Deal x) => x.Vehicles),
                new List<IConstraint<Deal>>
                    {
                        new PropertyConstraint<Deal, IList<Vehicle>>(
                            x => x.Vehicles,
                            new RequiredConstraint<IList<Vehicle>>()),
                        new PropertyConstraint<Deal, IList<Vehicle>>(
                            x => x.Vehicles,
                            new CompositeCollectionConstraint<Vehicle>())
                    });
        }
    }
}
