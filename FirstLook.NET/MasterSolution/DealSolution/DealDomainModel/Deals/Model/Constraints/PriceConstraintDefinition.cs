﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;
using FirstLook.Deal.DomainModel.Employees.Model;

namespace FirstLook.Deal.DomainModel.Deals.Model.Constraints
{
    public class PriceConstraintDefinition : ConstraintDefinition<Price>
    {
        public PriceConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Price x) => x.PriceType),
                new List<IConstraint<Price>>
                    {
                        new PropertyConstraint<Price, PriceType>(
                            x => x.PriceType,
                            new RequiredConstraint<PriceType>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Price x) => x.Value),
                new List<IConstraint<Price>>
                    {
                        new PropertyConstraint<Price, decimal>(
                            x => x.Value,
                            new MinValueConstraint<decimal>(0))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Price x) => x.Employee),
                new List<IConstraint<Price>>
                    {
                        new PropertyConstraint<Price, Employee>(
                            x => x.Employee,
                            new RequiredConstraint<Employee>())
                    });
        }
    }
}
