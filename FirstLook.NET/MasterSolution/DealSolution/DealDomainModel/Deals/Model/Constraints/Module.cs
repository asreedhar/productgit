﻿using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Deals.Model.Constraints
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IConstraintDefinition<Vehicle>, VehicleConstraintDefinition>(ImplementationScope.Shared);
            registry.Register<IConstraintDefinition<Price>, PriceConstraintDefinition>(ImplementationScope.Shared);
        }
    }
}
