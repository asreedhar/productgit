﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    /// <summary>
    /// <list>
    /// <item>Appraisal Price</item>
    /// <item>Customer Offer</item>
    /// <item>Reconditioning Cost</item>
    /// </list>
    /// </summary>
    [Serializable]
    public class PriceType
    {
        public int Id { get; protected internal set; }

        public string Name { get; internal set; }
    }
}
