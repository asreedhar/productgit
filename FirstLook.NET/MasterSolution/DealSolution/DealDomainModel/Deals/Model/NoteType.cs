﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    /// <summary>
    /// <list>
    /// <item>Reconditioning</item>
    /// <item>Customer Offer</item>
    /// </list>
    /// </summary>
    [Serializable]
    public class NoteType
    {
        public int Id { get; protected internal set; }

        public string Name { get; internal set; }
    }
}
