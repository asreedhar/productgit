﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    [Serializable]
    public class Vehicle : BusinessObject
    {
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public int RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        public int ClientVehicleId { get; protected internal set; }
        
        public int DealId { get; protected internal set; }
        
        public int ReferenceId { get; protected internal set; }
        
        public string Vin { get; protected internal set; }

        #region Properties
        
        private ClientVehicleIdentification _identification;

        public ClientVehicleIdentification Identification
        {
            get { return _identification; }
            set { Let(Inspect.NameOf((Vehicle p) => p.Identification), ref _identification, value); }
        }

        private TransactionType _transactionType;

        public TransactionType TransactionType
        {
            get { return _transactionType; }
            set { Let(Inspect.NameOf((Vehicle p) => p.TransactionType), ref _transactionType, value); }
        }

        private InventoryStrategy _inventoryStrategy;

        public InventoryStrategy InventoryStrategy
        {
            get { return _inventoryStrategy; }
            set { Let(Inspect.NameOf((Vehicle p) => p.InventoryStrategy), ref _inventoryStrategy, value); }
        }

        private readonly BusinessObjectList<Price> _prices = new BusinessObjectList<Price>();

        protected BusinessObjectList<Price> PricesBase
        {
            get { return _prices; }
        }

        public IList<Price> Prices
        {
            get { return PricesBase; }
        }

        private readonly BusinessObjectList<Note> _notes = new BusinessObjectList<Note>();

        protected BusinessObjectList<Note> NotesBase
        {
            get { return _notes; }
        }

        public IList<Note> Notes
        {
            get { return NotesBase; }
        }

        #endregion

        #region Validation

        protected override bool IsDirty
        {
            get
            {
                return base.IsDirty
                       || _prices.IsDirty
                       || _notes.IsDirty;
            }
        }

        private ValidationRules<Vehicle> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Vehicle>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Vehicle>>()));
        }

        #endregion
    }
}
