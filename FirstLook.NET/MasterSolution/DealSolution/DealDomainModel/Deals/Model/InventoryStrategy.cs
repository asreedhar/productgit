﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    /// <summary>
    /// <list>
    ///     <item>Undecided (DEFAULT)</item>
    ///     <item>Retail</item>
    ///     <item>Wholesale
    ///         <list>
    ///             <list>In Group</list>
    ///             <list>Auction /  Wholesaler</list>
    ///         </list>
    ///     </item>
    /// </list>
    /// </summary>
    [Serializable]
    public class InventoryStrategy
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }

        public bool IsRetail { get; protected internal set; }

        public bool IsWholesale { get; protected internal set; }
    }
}
