﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    [Serializable]
    public class Note : BusinessObject
    {
        public Note() : base()
        {}

        public Note(Note copyNote) : this()
        {
            throw new NotImplementedException();
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }


        public int RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        #region Properties
        
        private NoteType _noteType;

        public NoteType NoteType
        {
            get { return _noteType; }
            set { Let(Inspect.NameOf((Note p) => p.NoteType), ref _noteType, value); }
        }

        private string _text;

        public string Text
        {
            get { return _text; }
            set { Let(Inspect.NameOf((Note p) => p.Text), ref _text, value); }
        }

        #endregion

        #region Validation

        private ValidationRules<Note> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Note>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Note>>()));
        }

        #endregion
    }
}
