﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Deal.DomainModel.Parties.Model;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    [Serializable]
    public class Deal : BusinessObject
    {
        public Deal() : base()
        {
        }

        public Deal(Deal copyDeal) : base(copyDeal)
        {
            throw new NotImplementedException();
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public int RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        #region Properties

        #region Summary

        public DateTime Created { get; protected internal set; }

        public DateTime Modified { get; protected internal set; }

        public bool IsReadOnly { get; protected internal set; }

        #endregion

        private DealMotive _dealMotive;

        public DealMotive DealMotive
        {
            get { return _dealMotive; }
            set { Let(Inspect.NameOf((Deal p) => p.DealMotive), ref _dealMotive, value); }
        }

        private DealStatus _dealStatus;

        public DealStatus DealStatus
        {
            get { return _dealStatus; }
            set { Let(Inspect.NameOf((Deal p) => p.DealStatus), ref _dealStatus, value); }
        }

        private Employee _salesperson;

        public Employee SalesPerson
        {
            get { return _salesperson; }
            set { Let(Inspect.NameOf((Deal p) => p.SalesPerson), ref _salesperson, value); }
        }

        private readonly BusinessObjectList<Party> _parties = new BusinessObjectList<Party>();

        protected BusinessObjectList<Party> PartiesBase
        {
            get { return _parties; }
        }

        public IList<Party> Parties
        {
            get { return PartiesBase; }
        }

        private readonly BusinessObjectList<Vehicle> _vehicles = new BusinessObjectList<Vehicle>();

        protected BusinessObjectList<Vehicle> VehiclesBase
        {
            get { return _vehicles; }
        }

        public IList<Vehicle> Vehicles
        {
            get { return VehiclesBase; }
        }

        #endregion

        #region Validation

        protected override bool IsDirty
        {
            get
            {
                return base.IsDirty
                    || _parties.IsDirty
                    || _vehicles.IsDirty;
            }
        }

        private ValidationRules<Deal> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Deal>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Deal>>()));
        }

        #endregion
    }
}
