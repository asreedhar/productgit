﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Model
{
    /// <summary>
    /// <list>
    /// <item>Trade</item>
    /// <item>Purchase</item>
    /// <item>"Dealer Defined"</item>
    /// </list>
    /// </summary>
    [Serializable]
    public class DealMotive
    {
        public int Id { get; protected internal set; }

        public string Name { get; protected internal set; }

        public bool IsRetail { get; protected internal set; }

        public bool IsWholesale { get; protected internal set; }

    }
}
