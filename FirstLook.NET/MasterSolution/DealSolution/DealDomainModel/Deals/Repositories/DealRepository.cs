﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Gateways;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Deal.DomainModel.Deals.Repositories.Mappers;

namespace FirstLook.Deal.DomainModel.Deals.Repositories
{
    public class DealRepository : RepositoryBase, IDealRepository
    {

        /// <summary>
        /// Initializ a deal mapper responsible for speaking with the different table gateways.
        /// </summary>
        private readonly DealMapper _mapper = new DealMapper();

        public IList<DealMotive> DealMotives()
        {
            return DoInSession(() => _mapper.Motives());
        }

        public IList<DealStatus> DealStatuses()
        {
            return DoInSession(() => _mapper.Statuses());
        }

        public IList<Model.InventoryStrategy> InventoryStrategies()
        {
            return DoInSession(() => _mapper.InventoryStrategies());
        }

        public IList<NoteType> NoteTypes()
        {
            return DoInSession(() => _mapper.NoteTypes());
        }

        public IList<PriceType> PriceTypes()
        {
            return DoInSession(() => _mapper.PriceTypes());
        }

        public Page<Model.Deal> Search(IBroker broker, Model.Deal example, PageArguments pagination)
        {
            return DoInSession(() => _mapper.Search(broker, example, pagination));
        }

        public Model.Deal Fetch(IBroker broker, int id)
        {
            return DoInSession(() => _mapper.Fetch(broker, id));
        }

        public Model.Deal Save(IBroker broker, IPrincipal principal, Model.Deal deal)
        {
            return DoInTransaction(session => OnBeginTransaction(session, principal), () => (_mapper.Save(broker, principal, deal)));
        }


        #region Miscellaneous

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        private void OnBeginTransaction(IDataSession session, IPrincipal principal)
        {
            var audit = session.Items["Audit"] as Audit;

            if (audit != null) return;

            audit = Resolve<IRepository>().Create(principal);
            session.Items["Audit"] = audit;
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do Nothing
        }

        #endregion Miscellaneous
    }
}
