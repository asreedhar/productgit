﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class NoteTypeGateway : GatewayBase
    {
        public IList<NoteType> FetchAll()
        {

            string key = CreateCacheKey("noteTypes");
            IList<NoteType> entity = Cache.Get(key) as List<NoteType>;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                entity = new List<NoteType>();
                using (IDataReader reader = datastore.NoteTypes_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<NoteType > serializer = Resolve<ISerializer<NoteType>>();
                        entity.Add(serializer.Deserialize((IDataRecord)reader));
                    }

                    Remember(key, entity);
                }
            }
            return entity;
        }

    }
}
