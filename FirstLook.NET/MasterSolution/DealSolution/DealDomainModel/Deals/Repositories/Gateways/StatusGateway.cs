﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    /// <summary>
    /// Status gateway.
    /// </summary>
    class StatusGateway : GatewayBase
    {      

        public IList<DealStatus> FetchAll()
        {

            string key = CreateCacheKey("statuses");
            IList<DealStatus> entity = Cache.Get(key) as List<DealStatus>;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                entity = new List<DealStatus>();
                using (IDataReader reader = datastore.Statuses_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<DealStatus> serializer = Resolve<ISerializer<DealStatus>>();
                        entity.Add(serializer.Deserialize((IDataRecord)reader));
                    }

                    Remember(key, entity);
                }
            }
            return entity;
        }

        public DealStatus Fetch(int statusId)
        {
            string key = CreateCacheKey(statusId);
            DealStatus entity = Cache.Get(key) as DealStatus;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                using (IDataReader reader = datastore.Status_Fetch(statusId))
                {
                    if (reader.Read())
                    {
                        ISerializer<DealStatus> serializer = Resolve<ISerializer<DealStatus>>();
                        entity = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, entity);
                    }

                    Remember(key, entity);
                }
            }
            return entity;

        }

    }
}
