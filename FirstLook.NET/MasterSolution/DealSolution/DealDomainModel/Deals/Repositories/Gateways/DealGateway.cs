﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class DealGateway : AssociationGateway
    {

        public IList<DealEntity> FetchAll(IBroker broker)
        {
            IList<DealEntity> entity = new List<DealEntity>();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Deals_Fetch(broker.Id))
            {
                while (reader.Read())
                {
                    entity.Add(Fetch(broker, reader.GetInt32(reader.GetOrdinal("DealID"))));
                }
            }

            return entity;
        }

        public DealEntity Fetch(IBroker broker, int dealId)
        {
            DealEntity entity = new DealEntity();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Deal_Fetch(dealId, broker.Id))
            {
                while (reader.Read())
                {
                    ISerializer<DealEntity> serializer = Resolve<ISerializer<DealEntity>>();
                    entity = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return entity;
        }

        public AuditRow Delete(IBroker broker, DealEntity dealEntity)
        {
            return Delete(dealEntity.AuditRow);
        }


        public DealEntity Insert(IBroker broker)
        {
            IDealDatastore datastore = Resolve<IDealDatastore>();
            DealEntity dealEntity;

            AuditRow row = Insert();

            using (IDataReader reader = datastore.Deal_Insert(broker.Id, row.Id))
            {
                if (reader.Read())
                {
                    ISerializer<DealEntity> serializer = Resolve<ISerializer<DealEntity>>();

                    dealEntity = serializer.Deserialize((IDataRecord)reader);

                    dealEntity.AuditRow = row;

                    return dealEntity;
                }

                throw new ArgumentException("Could not create a deal from the argument dealEntity", "dealEntity");
            }

        }
    }
}
