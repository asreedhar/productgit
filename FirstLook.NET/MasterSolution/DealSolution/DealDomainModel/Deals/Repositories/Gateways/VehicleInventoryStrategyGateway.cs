﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class VehicleInventoryStrategyGateway : AssociationGateway
    {      
        public VehicleInventoryStrategyEntity Fetch(int vehicleId)
        {
            VehicleInventoryStrategyEntity entity = new VehicleInventoryStrategyEntity();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Vehicle_InventoryStrategy_Fetch(vehicleId))
            {
                if (reader.Read())
                {
                    ISerializer<VehicleInventoryStrategyEntity> serializer = Resolve<ISerializer<VehicleInventoryStrategyEntity>>();
                    entity = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return entity;
        }

        public void Delete(VehicleInventoryStrategyEntity vehicleInventoryStrategyEntity)
        {
            Association association = FetchAssociation(vehicleInventoryStrategyEntity.AssociationId);

            AuditRow row = Delete(association.AuditRow);

            InsertAssociation(association.Id, row.Id);
        }
    }
}
