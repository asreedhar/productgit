﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    /// <summary>
    /// InventoryStrategy gateway.
    /// </summary>
    class InventoryStrategyGateway : GatewayBase
    {      
        public IList<InventoryStrategy> FetchAll()
        {
            string key = CreateCacheKey("inventorystrategies");
            IList<InventoryStrategy> entity = Cache.Get(key) as List<InventoryStrategy>;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                entity = new List<InventoryStrategy>();
                using (IDataReader reader = datastore.InventoryStrategies_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<InventoryStrategy> serializer = Resolve<ISerializer<InventoryStrategy>>();
                        entity.Add(serializer.Deserialize((IDataRecord)reader));
                    }

                    Remember(key, entity);
                }
            }
            return entity;

        }

        public InventoryStrategy Fetch(int inventoryStrategyId)
        {
            string key = CreateCacheKey(inventoryStrategyId);
            InventoryStrategy entity = Cache.Get(key) as InventoryStrategy;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                using (IDataReader reader = datastore.InventoryStrategy_Fetch(inventoryStrategyId))
                {
                    if (reader.Read())
                    {
                        ISerializer<InventoryStrategy> serializer = Resolve<ISerializer<InventoryStrategy>>();
                        entity = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, entity);
                    }

                    Remember(key, entity);
                }
            }
            return entity;

        }



    }
}
