﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class VehiclePriceGateway : GatewayBase
    {      
        public IList<int> Fetch(IBroker broker, int vehicleId)
        {
            IList<int> entity = new List<int>();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Vehicle_Price_Fetch(vehicleId))
            {
                while (reader.Read())
                {
                    entity.Add(reader.GetInt32(reader.GetOrdinal("PriceID")));
                }
            }

            return entity;
        }
    }
}
