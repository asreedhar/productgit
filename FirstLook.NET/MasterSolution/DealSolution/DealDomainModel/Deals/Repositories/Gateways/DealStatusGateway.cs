﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class DealStatusGateway : AssociationGateway
    {      
        public DealStatusEntity Fetch(IBroker broker, int dealId)
        {
            DealStatusEntity entity = new DealStatusEntity();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Deal_Status_Fetch(dealId))
            {
                if (reader.Read())
                {
                    ISerializer<DealStatusEntity> serializer = Resolve<ISerializer<DealStatusEntity>>();
                    entity = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return entity;
        }

        public void Delete(DealStatusEntity dealStatus)
        {
            Association association = FetchAssociation(dealStatus.AssociationId);

            AuditRow row = Delete(association.AuditRow);

            InsertAssociation(association.Id, row.Id);
        }

        public DealStatusEntity Insert(int dealId, int statusId)
        {
            IDealDatastore datastore = Resolve<IDealDatastore>();

            Association association = InsertAssociation();

            datastore.Deal_Status_Insert(dealId, statusId, association.Id);

            return new DealStatusEntity
            {
                AssociationId = association.Id,
                DealId = dealId,
                StatusId = statusId
            };
        }
    }
}
