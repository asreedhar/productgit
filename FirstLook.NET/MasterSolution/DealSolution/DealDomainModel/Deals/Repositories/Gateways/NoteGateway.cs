﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class NoteGateway : GatewayBase
    {      
        public Note Fetch(int noteId)
        {

            IDealDatastore datastore = Resolve<IDealDatastore>();
            Note entity = null;

            using (IDataReader reader = datastore.Note_Fetch(noteId))
            {
                if(reader.Read())
                {
                    ISerializer<Note> noteSerializer = Resolve<ISerializer<Note>>();
                    ISerializer<NoteType> noteTypeSerializer = Resolve<ISerializer<NoteType>>();

                    entity = noteSerializer.Deserialize((IDataRecord)reader);
                    entity.NoteType = noteTypeSerializer.Deserialize((IDataRecord) reader);
                }

            }

            return entity;
        }

    }
}
