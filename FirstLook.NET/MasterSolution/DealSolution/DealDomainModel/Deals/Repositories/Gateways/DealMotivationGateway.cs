﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class DealMotivationGateway : AssociationGateway
    {      
       
        public DealMotiveEntity Fetch(IBroker broker, int dealId)
        {
            DealMotiveEntity entity = new DealMotiveEntity();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Deal_Motivation_Fetch(dealId))
            {
                if(reader.Read())
                {
                    ISerializer<DealMotiveEntity> serializer = Resolve<ISerializer<DealMotiveEntity>>();
                    entity = serializer.Deserialize((IDataRecord)reader);
                }
                
            }

            return entity;
        }


        public void Delete(DealMotiveEntity dealMotive)
        {
            Association association = FetchAssociation(dealMotive.AssociationId);

            AuditRow row = Delete(association.AuditRow);

            InsertAssociation(association.Id, row.Id);
        }

        public DealMotiveEntity Insert(int dealId, int motivationId)
        {
            IDealDatastore datastore = Resolve<IDealDatastore>();

            Association association = InsertAssociation();

            datastore.Deal_Motivation_Insert(dealId, motivationId, association.Id);

            return new DealMotiveEntity
            {
                AssociationId = association.Id,
                DealId = dealId,
                MotivationId = motivationId
            };
        }
    }
}
