﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;


namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class VehicleGateway : AssociationGateway
    {      
        public IList<VehicleEntity> FetchAll(IBroker broker, int dealId)
        {
            IList<VehicleEntity> entity = new List<VehicleEntity>();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Vehicle_FetchAll(dealId))
            {
                while (reader.Read())
                {
                    ISerializer<VehicleEntity> serializer = Resolve<ISerializer<VehicleEntity>>();
                    entity.Add(serializer.Deserialize((IDataRecord)reader));
                }
            }

            return entity;
        }

        public VehicleEntity Fetch(IBroker broker, int vehicleId)
        {
            VehicleEntity entity = new VehicleEntity();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Vehicles_Fetch(vehicleId))
            {
                while (reader.Read())
                {
                    ISerializer<VehicleEntity> serializer = Resolve<ISerializer<VehicleEntity>>();
                    entity = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return entity;
        }

        public VehicleEntity Insert(int clientVehicleId, int dealId)
        {
            IDealDatastore datastore = Resolve<IDealDatastore>();
            VehicleEntity vehicleEntity;

            AuditRow row = Insert();

            using (IDataReader reader = datastore.Vehicle_Insert(clientVehicleId, dealId, row.Id))
            {
                if (reader.Read())
                {
                    ISerializer<VehicleEntity> serializer = Resolve<ISerializer<VehicleEntity>>();

                    vehicleEntity = serializer.Deserialize((IDataRecord)reader);

                    vehicleEntity.AuditRow = row;

                    return vehicleEntity;
                }

                throw new ArgumentException("Could not create a vehicle entity from the arguments", "vehicleEntity");
            }

        }

        public void Delete(int clientVehicleId, int dealId)
        {
        }

    }
}
