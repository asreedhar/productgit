﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class VehicleTransactionTypeGateway : AssociationGateway
    {      
        public VehicleTransactionTypeEntity Fetch(int vehicleId)
        {
            VehicleTransactionTypeEntity entity = new VehicleTransactionTypeEntity();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Vehicle_TransactionType_Fetch(vehicleId))
            {
                if (reader.Read())
                {
                    ISerializer<VehicleTransactionTypeEntity> serializer = Resolve<ISerializer<VehicleTransactionTypeEntity>>();
                    entity = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return entity;
        }

        public void Delete(VehicleTransactionTypeEntity vehicleInventoryStrategyEntity)
        {
            Association association = FetchAssociation(vehicleInventoryStrategyEntity.AssociationId);

            AuditRow row = Delete(association.AuditRow);

            InsertAssociation(association.Id, row.Id);
        }
    }
}
