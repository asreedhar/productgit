﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Employees.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    /// <summary>
    /// Status gateway.
    /// </summary>
    class PriceGateway : GatewayBase
    {      
        /// <summary>
        /// Fetch price based on ID.
        /// </summary>
        /// <returns>Price</returns>
        public Price Fetch(IBroker broker, int priceId)
        {

            IDealDatastore datastore = Resolve<IDealDatastore>();
            Price entity = null;

            using (IDataReader reader = datastore.Price_Fetch(priceId))
            {

                if (reader.Read())                    
                {
                    ISerializer<Price> priceSerializer = Resolve<ISerializer<Price>>();
                    ISerializer<PriceType> priceTypeSerializer = Resolve<ISerializer<PriceType>>();
                    IEmployeeRepository repository = RegistryFactory.GetResolver().Resolve<IEmployeeRepository>();

                    entity = priceSerializer.Deserialize((IDataRecord)reader);
                    entity.PriceType = priceTypeSerializer.Deserialize((IDataRecord) reader);
                    entity.Employee = repository.Fetch(broker, reader.GetInt32("EmployeeID", 0));
                }

            }

            return entity;
        }

        public void Delete(IBroker broker, int priceId)
        {
            //Insert delete audit row
        }

    }
}
