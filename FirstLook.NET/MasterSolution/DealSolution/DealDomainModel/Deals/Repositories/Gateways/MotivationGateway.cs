﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    /// <summary>
    /// Motivation gateway.
    /// </summary>
    class MotivationGateway : GatewayBase
    {      
        
        public IList<DealMotive> FetchAll()
        {

            string key = CreateCacheKey("motives");
            IList<DealMotive> entity = Cache.Get(key) as List<DealMotive>;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                entity = new List<DealMotive>();
                using (IDataReader reader = datastore.Motives_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<DealMotive> serializer = Resolve<ISerializer<DealMotive>>();
                        entity.Add(serializer.Deserialize((IDataRecord)reader));
                    }

                    Remember(key, entity);
                }
            }
            return entity;
        }

        public DealMotive Fetch(int motivationId)
        {
            string key = CreateCacheKey(motivationId);
            DealMotive entity = Cache.Get(key) as DealMotive;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                using (IDataReader reader = datastore.Motivation_Fetch(motivationId))
                {
                    if (reader.Read())
                    {
                        ISerializer<DealMotive> serializer = Resolve<ISerializer<DealMotive>>();
                        entity = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, entity);
                    }

                    Remember(key, entity);
                }
            }
            return entity;

        }

    }
}
