﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class DealPartyGateway : AssociationGateway
    {      
        public IList<DealPartyEntity> Fetch(IBroker broker, int dealId)
        {
            IList<DealPartyEntity> entity = new List<DealPartyEntity>();
            IDealDatastore datastore = Resolve<IDealDatastore>();

            using (IDataReader reader = datastore.Deal_Party_Fetch(dealId))
            {
                while (reader.Read())
                {
                    ISerializer<DealPartyEntity> serializer = Resolve<ISerializer<DealPartyEntity>>();
                    entity.Add(serializer.Deserialize((IDataRecord)reader));
                }
            }

            return entity;
        }

        public void Delete(DealPartyEntity dealParty)
        {
            Association association = FetchAssociation(dealParty.AssociationId);

            AuditRow row = Delete(association.AuditRow);

            InsertAssociation(association.Id, row.Id);
        }

        public DealPartyEntity Insert(int dealId, int partyId)
        {
            IDealDatastore datastore = Resolve<IDealDatastore>();

            Association association = InsertAssociation();

            datastore.Deal_Party_Insert(dealId, partyId, association.Id);

            return new DealPartyEntity
            {
                AssociationId = association.Id,
                DealId = dealId,
                PartyId = partyId
            };
        }
    }
}
