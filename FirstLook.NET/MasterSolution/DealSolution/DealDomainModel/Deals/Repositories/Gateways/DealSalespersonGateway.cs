﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class DealSalespersonGateway : AssociationGateway
    {             
        public SalespersonEntity Fetch(IBroker broker, int dealId)
        {
            IDealDatastore datastore = Resolve<IDealDatastore>();
            SalespersonEntity entity = null;

            using (IDataReader reader = datastore.Salesperson_Fetch(dealId))
            {
                if(reader.Read())
                {
                    ISerializer<SalespersonEntity> serializer = Resolve<ISerializer<SalespersonEntity>>();
                    entity = serializer.Deserialize((IDataRecord)reader);
                }
            }

            return entity;
        }

        public void Delete(SalespersonEntity dealSalesperson)
        {
            Association association = FetchAssociation(dealSalesperson.AssociationId);

            AuditRow row = Delete(association.AuditRow);

            InsertAssociation(association.Id, row.Id);
        }

        public SalespersonEntity Insert(int dealId, int employeeId)
        {
            IDealDatastore datastore = Resolve<IDealDatastore>();

            Association association = InsertAssociation();

            datastore.Salesperson_Insert(dealId, employeeId, association.Id);

            return new SalespersonEntity
                       {
                           AssociationId = association.Id,
                           DealId = dealId, 
                           EmployeeId = employeeId
                       };
        }

    }
}
