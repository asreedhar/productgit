﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Gateways
{
    class PriceTypeGateway : GatewayBase
    {      
        public IList<PriceType> FetchAll()
        {

            string key = CreateCacheKey("priceTypes");
            IList<PriceType> entity = Cache.Get(key) as List<PriceType>;

            if (entity == null)
            {
                IDealDatastore datastore = Resolve<IDealDatastore>();
                entity = new List<PriceType>();
                using (IDataReader reader = datastore.PriceTypes_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<PriceType> serializer = Resolve<ISerializer<PriceType>>();
                        entity.Add(serializer.Deserialize((IDataRecord)reader));
                    }

                    Remember(key, entity);
                }
            }
            return entity;
        }

    }
}
