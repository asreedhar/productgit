﻿using System;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    [Serializable]
    public class VehicleTransactionTypeEntity : TransactionType
    {
        public int DealId { get; set; }

        public int TransactionTypeId { get; set; }

        public int AssociationId { get; set; }
    }
}
