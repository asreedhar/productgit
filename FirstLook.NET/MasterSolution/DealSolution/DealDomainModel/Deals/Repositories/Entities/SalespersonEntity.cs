﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    [Serializable]
    public class SalespersonEntity 
    {
        public int DealId { get; set; }

        public int EmployeeId { get; set; }

        public int AssociationId { get; set; }
    }
}
