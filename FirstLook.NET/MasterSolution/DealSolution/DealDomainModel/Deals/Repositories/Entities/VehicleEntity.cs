﻿using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    public class VehicleEntity : Vehicle
    {
        protected internal AuditRow AuditRow { get; set; }
    }
}
