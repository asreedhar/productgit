﻿using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;


namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    [Serializable]
    public class DealEntity : Model.Deal
    {
        protected internal AuditRow AuditRow { get; set; }
    }
}
