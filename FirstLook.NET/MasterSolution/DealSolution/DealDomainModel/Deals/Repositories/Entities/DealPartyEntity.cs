﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    [Serializable]
    public class DealPartyEntity  
    {
        public int DealId { get; set; }

        public int PartyId { get; set; }

        public int AssociationId { get; set; }
    }
}
