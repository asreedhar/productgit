﻿using System;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    [Serializable]
    public class DealMotiveEntity : DealMotive
    {
        public int DealId { get; set; }

        public int MotivationId { get; set; }

        public int AssociationId { get; set; }
    }
}
