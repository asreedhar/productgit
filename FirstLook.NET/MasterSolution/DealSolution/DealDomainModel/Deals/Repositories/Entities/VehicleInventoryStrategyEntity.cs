﻿using System;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    [Serializable]
    public class VehicleInventoryStrategyEntity : InventoryStrategy
    {
        public int DealId { get; set; }

        public int StrategyId { get; set; }

        public int AssociationId { get; set; }
    }
}
