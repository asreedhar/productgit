﻿using System;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Entities
{
    [Serializable]
    public class DealStatusEntity : DealStatus
    {
        public int DealId { get; set; }

        public int StatusId { get; set; }

        public int AssociationId { get; set; }
    }
}
