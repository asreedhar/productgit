﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;
using FirstLook.Deal.DomainModel.Deals.Repositories.Gateways;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Gateways;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Mappers
{
    class DealMapper
    {
        private readonly StatusGateway _status = new StatusGateway();
        private readonly MotivationGateway _motives = new MotivationGateway();
        private readonly InventoryStrategyGateway _inventoryStrategy = new InventoryStrategyGateway();
        private readonly NoteTypeGateway _noteType = new NoteTypeGateway();
        private readonly PriceTypeGateway _priceType = new PriceTypeGateway();
        private readonly DealSalespersonGateway _salesperson = new DealSalespersonGateway();
        private readonly DealMotivationGateway _dealMotivation = new DealMotivationGateway();
        private readonly DealStatusGateway _dealStatus = new DealStatusGateway();
        private readonly DealPartyGateway _dealParty = new DealPartyGateway();
        private readonly PartyGateway _partyGateway = new PartyGateway();
        private readonly VehicleGateway _vehicleGateway = new VehicleGateway();
        private readonly VehicleInventoryStrategyGateway _vehicleInventoryStrategy = new VehicleInventoryStrategyGateway();
        private readonly VehicleNoteGateway _vehicleNote = new VehicleNoteGateway();
        private readonly NoteGateway _note = new NoteGateway();
        private readonly VehiclePriceGateway _vehiclePrice = new VehiclePriceGateway();
        private readonly PriceGateway _price = new PriceGateway();
        private readonly VehicleTransactionTypeGateway _vehicleTransactionType = new VehicleTransactionTypeGateway();
        private readonly DealGateway _deal = new DealGateway();

 
        /// <summary>
        /// Fetch statuses.
        /// </summary>
        /// <returns>List of supported statuses.</returns>
        public IList<DealStatus> Statuses()
        {
            return _status.FetchAll();
        }

        /// <summary>
        /// Fetch motives.
        /// </summary>
        /// <returns>List of supported motives.</returns>
        public IList<DealMotive> Motives()
        {
            return _motives.FetchAll();
        }

        /// <summary>
        /// Fetch inventory strategies.
        /// </summary>
        /// <returns>List of supported inventory strategies.</returns>
        public IList<InventoryStrategy> InventoryStrategies()
        {
            return _inventoryStrategy.FetchAll();
        }

        /// <summary>
        /// Fetch note types.
        /// </summary>
        /// <returns>List of supported note types.</returns>
        public IList<NoteType> NoteTypes()
        {
            return _noteType.FetchAll();
        }

        /// <summary>
        /// Fetch price types.
        /// </summary>
        /// <returns>List of supported price types.</returns>
        public IList<PriceType> PriceTypes()
        {
            return _priceType.FetchAll();
        }
     

        public Page<Model.Deal> Search(IBroker broker, Model.Deal example, PageArguments pagination)
        {
            
            IDealDatastore deals = Resolve<IDealDatastore>();
            IList<Model.Deal> items = new List<Model.Deal>();

            IDataReader reader;

            using( reader = deals.Deals_FetchByExample(example, pagination))
            {
                while(reader.Read())
                {
                    items.Add(Map(Fetch(broker, reader.GetInt32(reader.GetOrdinal("DealID")))));
                }
            }

            int totalRowCount = 0;
            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                }
            }

            return new Page<Model.Deal>
            {
                Arguments = pagination,
                Items = items,
                TotalRowCount = totalRowCount
            };

        } 

        public DealEntity Fetch(IBroker broker, int dealId)
        {
            IDealDatastore deals = Resolve<IDealDatastore>();
            using (IDataReader reader = deals.Deal_Fetch(dealId, broker.Id))
            {
                if (reader.Read())
                {
                    ISerializer<DealEntity> se = Resolve<ISerializer<DealEntity>>();

                    DealEntity entity = se.Deserialize((IDataRecord)reader);

                    Complete(broker, entity);

                    return entity;
                }
            }

            return null;
        }

        public DealEntity Save(IBroker broker, IPrincipal principal, Model.Deal deal)
        {
            IDealDatastore deals = Resolve<IDealDatastore>();

            if (deal == null || !deal.IsValid)
            {
                throw new ArgumentException("Invalid deal", "deal");
            }

            Model.Deal model = deal;

            DealEntity entity = model as DealEntity;

             // if @model is not an entity lets try and load it and merge property values

            if (entity == null)
            {
                entity = Fetch(broker, deal.Id);

                if (entity != null)
                {
                    Merge(model, entity);

                    if (!entity.IsValid)
                    {
                        throw new ArgumentException("Invalid deal", "deal");
                    }
                }
            }

            Model.Deal target = entity ?? model;

            IPersisted persistedTarget = target;

            if (!persistedTarget.IsDirty)
            {
                return entity;
            }

            if (entity == null)
            {
                entity = _deal.Insert(broker);
                _salesperson.Insert(entity.Id, target.SalesPerson.Id);

                foreach(Party party in target.Parties)
                {
                    _dealParty.Insert(target.Id, party.Id);
                    entity.Parties.Add(party);
                }

                foreach(Vehicle vehicle in target.Vehicles)
                {
                    _vehicleGateway.Insert(vehicle.ClientVehicleId, entity.Id);
                    entity.Vehicles.Add(vehicle);
                }

                _dealMotivation.Insert(target.Id, target.DealMotive.Id);
                entity.DealMotive = target.DealMotive;

                _dealStatus.Insert(entity.Id, target.DealStatus.Id);
                entity.DealStatus = target.DealStatus;
            }
            else
            {
                //update the deal
            }

            return entity;

        }

        protected DealEntity Complete(IBroker broker, DealEntity entity)
        {
            IEmployeeRepository employee = Resolve<IEmployeeRepository>();

            SalespersonEntity salespersonEntity = _salesperson.Fetch(broker, entity.Id);
            entity.SalesPerson = employee.Fetch(broker, salespersonEntity.EmployeeId);
            entity.DealMotive = _dealMotivation.Fetch(broker, entity.Id);
            entity.DealStatus = _dealStatus.Fetch(broker, entity.Id);

            foreach (DealPartyEntity dealPartyEntity in _dealParty.Fetch(broker, entity.Id))
            {
                entity.Parties.Add(_partyGateway.Fetch(broker, dealPartyEntity.PartyId));
            }

            foreach (VehicleEntity vehicleEntity in _vehicleGateway.FetchAll(broker,entity.Id))
            {
                entity.Vehicles.Add(CompleteVehicle(broker, vehicleEntity));
            }

            return entity;
        }


        protected VehicleEntity CompleteVehicle(IBroker broker, VehicleEntity entity)
        {

            Client.DomainModel.Vehicles.Repositories.VehicleRepository vehicleRepository = Resolve<Client.DomainModel.Vehicles.Repositories.VehicleRepository>();

            entity.InventoryStrategy = _vehicleInventoryStrategy.Fetch(entity.Id);
            
            foreach(int noteId in _vehicleNote.Fetch(broker, entity.Id))
            {
                entity.Notes.Add(_note.Fetch(noteId));
            }

            foreach( int priceId in _vehiclePrice.Fetch(broker, entity.Id))
            {
                entity.Prices.Add(_price.Fetch(broker, priceId));
            }

            entity.TransactionType = _vehicleTransactionType.Fetch(entity.Id);

            entity.Identification = vehicleRepository.Identification(broker, vehicleRepository.Identification(entity.Vin)); 

            //entity.AuditRow

            return entity;
        }


        protected Model.Deal Map(DealEntity dealEntity)
        {
            Model.Deal deal = new Model.Deal
                       {
                           Created = dealEntity.Created,
                           DealMotive = dealEntity.DealMotive,
                           DealStatus = dealEntity.DealStatus,
                           Id = dealEntity.Id,
                           Modified = dealEntity.Modified,
                           SalesPerson = dealEntity.SalesPerson,
                           RevisionNumber = dealEntity.RevisionNumber,
                           IsReadOnly = dealEntity.IsReadOnly
                       };

            foreach(Vehicle v in dealEntity.Vehicles)
            {
                deal.Vehicles.Add(v);
            }

            foreach(Party p in dealEntity.Parties)
            {
                deal.Parties.Add(p);
            }

            return deal;
        }


        private static void Merge(Model.Deal src, DealEntity dst)
        {
            IPersisted ps = src, pd = dst;

            if (ps.IsDeleted)
            {
                pd.MarkDeleted();
            }
            else
            {

                dst.DealMotive = src.DealMotive;
                dst.DealStatus = src.DealStatus;
                dst.Created = src.Created;
                dst.Modified = src.Modified;
                
                Merge(src.Parties, dst.Parties);
                Merge(src.Vehicles, dst.Vehicles);

                dst.SalesPerson = src.SalesPerson;
            }
        }

        
        private static void Merge(IEnumerable<Party> src, IList<Party> dst)
        {
            foreach (Party r in src.Where(s => !dst.Any(d => d.Id == s.Id)))
            {
                dst.Add(r);
            }

            foreach (Party r in dst.Where(d => !src.Any(s => s.Id == d.Id)))
            {
                dst.Remove(r);
            }
        }

        private static void Merge(IEnumerable<Vehicle> src, IList<Vehicle> dst)
        {
            foreach (Vehicle r in src.Where(s => !dst.Any(d => d.Id == s.Id)))
            {
                dst.Add(r);
            }

            foreach (Vehicle r in dst.Where(d => !src.Any(s => s.Id == d.Id)))
            {
                dst.Remove(r);
            }
        }

        protected T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
