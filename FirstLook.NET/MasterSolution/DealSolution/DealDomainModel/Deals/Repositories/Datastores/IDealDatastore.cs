using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Datastores
{
    public interface IDealDatastore
    {
        /// <summary>
        /// Return from Deal.Deal and
        /// Deal.Properties
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        IDataReader Deal_Fetch(int dealId, int clientId);

        /// <summary>
        /// Create new DealId
        /// </summary>
        /// <param name="created"></param>
        /// <param name="modified"></param>
        /// <param name="isReadOnly"></param>
        /// <param name="revisionNumber"></param>
        /// <param name="clientId"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Deal_Insert(int clientId, int auditRowId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="accessed"></param>
        /// <param name="created"></param>
        /// <param name="modified"></param>
        /// <param name="isReadOnly"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Deal_Fetch</returns>
        IDataReader Properties_Insert(int dealId, DateTime accessed, DateTime created,
                                      DateTime modified, bool isReadOnly, int revisionNumber);

        /// <summary>
        /// Return all Party Ids linked 
        /// to Deal with Id = dealId
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        IDataReader Deal_Party_Fetch(int dealId);

        /// <summary>
        /// Link a Deal to a Party
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="partyId"></param>
        /// <param name="assocationId"></param>
        void Deal_Party_Insert(int dealId, int partyId, int assocationId);

        /// <summary>
        /// Return all Employee Ids linked 
        /// to Deal with Id = dealId
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        IDataReader Salesperson_Fetch(int dealId);

        /// <summary>
        /// Link a Deal to an Employee
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="employeeId"></param>
        /// <param name="assocationId"></param>
        void Salesperson_Insert(int dealId, int employeeId, int assocationId);

        /// <summary>
        /// Return Status data of 
        /// Deal with Id = dealId.
        /// Joins Deal_Status and Status
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        IDataReader Deal_Status_Fetch(int dealId);

        /// <summary>
        /// Link Deal to Status
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="statusId"></param>
        /// <param name="assocationId"></param>
        void Deal_Status_Insert(int dealId, int statusId, int assocationId);


        /// <summary>
        /// Return Motivation Data of 
        /// Deal with Id = dealId.
        /// Joins Deal_Motivation and Motivation
        /// </summary>
        /// <param name="dealId"></param>
        /// <returns></returns>
        IDataReader Deal_Motivation_Fetch(int dealId);

        /// <summary>
        /// Link Deal to Motivation
        /// </summary>
        /// <param name="dealId"></param>
        /// <param name="motivationId"></param>
        /// <param name="assocationId"></param>
        void Deal_Motivation_Insert(int dealId, int motivationId, int assocationId);

        /// <summary>
        /// Return Vehicle TransactionType Id linked 
        /// to Vehicle with Id = vehicleId
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        IDataReader Vehicle_TransactionType_Fetch(int vehicleId);

        /// <summary>
        /// Link a Vehicle to a Vehicle TransactionType
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="transactionTypeId"></param>
        /// <param name="assocationId"></param>
        void Vehicle_TransactionType_Insert(int vehicleId, int transactionTypeId, 
            int assocationId);

        /// <summary>
        /// Return Vehicle Inventory Strategy Id linked 
        /// to Vehicle with Id = vehicleId
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        IDataReader Vehicle_InventoryStrategy_Fetch(int vehicleId);

        /// <summary>
        /// Link a Vehicle to a Vehicle Inventory Strategy
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="inventoryStrategyId"></param>
        /// <param name="assocationId"></param>
        void Vehicle_InventoryStrategy_Insert(int vehicleId, int inventoryStrategyId,
            int assocationId);

        /// <summary>
        /// Return Vehicle Price Ids linked 
        /// to Vehicle with Id = vehicleId
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        IDataReader Vehicle_Price_Fetch(int vehicleId);

        /// <summary>
        /// Link a Vehicle to a Vehicle Price
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="inventoryStrategyId"></param>
        /// <param name="assocationId"></param>
        void Vehicle_Price_Insert(int vehicleId, int priceId);

        /// <summary>
        /// Return all Vehicle Note Ids linked 
        /// to Vehicle with Id = vehicleId
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        IDataReader Vehicle_Note_Fetch(int vehicleId);

        /// <summary>
        /// Link a Vehicle to a Vehicle Note
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="noteId"></param>
        void Vehicle_Note_Insert(int vehicleId, int noteId);

        /// <summary>
        /// Update a link to Vehicle and Vehicle Note
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="noteId"></param>
        void Vehicle_Note_Update(int vehicleId, int noteId);

        /// <summary>
        /// Unlink a Vehicle to a Vehicle Note
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="noteId"></param>
        void Vehicle_Note_Delete(int vehicleId, int noteId);

        /// <summary>
        /// Fetch from Deal.InventoryStrategy
        /// </summary>
        /// <param name="inventoryStrategyId"></param>
        /// <returns></returns>
        IDataReader InventoryStrategy_Fetch(int inventoryStrategyId);

        /// <summary>
        /// Insert into Deal.InventoryStrategy
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isRetail"></param>
        /// <param name="isWholesale"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader InventoryStrategy_Insert(string name, bool isRetail, bool isWholesale);

        /// <summary>
        /// Fetch from Deal.Note
        /// </summary>
        /// <param name="noteId"></param>
        /// <returns></returns>
        IDataReader Note_Fetch(int noteId);
        
        /// <summary>
        /// Insert into Deal.Note
        /// </summary>
        /// <param name="noteTypeId"></param>
        /// <param name="text"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Note_Insert(int noteTypeId, string text);

        /// <summary>
        /// Update Deal.Note
        /// </summary>
        /// <param name="noteTypeId"></param>
        /// <param name="text"></param>
        void Note_Update(int noteId, int noteTypeId, string text);

        /// <summary>
        /// Delete from Deal.Note
        /// </summary>
        /// <param name="noteId"></param>
        void Note_Delete(int noteId);

        /// <summary>
        /// Fetch from Deal.Price
        /// </summary>
        /// <param name="priceId"></param>
        /// <returns></returns>
        IDataReader Price_Fetch(int priceId);

        /// <summary>
        /// Insert new price for vehicle
        /// </summary>
        /// <param name="priceTypeId"></param>
        /// <param name="employeeId"></param>
        /// <param name="value"></param>
        /// <param name="auditRowId"></param>
        /// <returns></returns>
        IDataReader Price_Insert(int priceTypeId, int employeeId, decimal value,
                                 int auditRowId);

        /// <summary>
        /// Fetch deal motivation
        /// </summary>
        /// <param name="motivationId"></param>
        /// <returns></returns>
        IDataReader Motivation_Fetch(int motivationId);

        /// <summary>
        /// Create new deal motivation
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isRetail"></param>
        /// <param name="isWholesale"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Motivation_Insert(string name, bool isRetail, bool isWholesale);

        /// <summary>
        /// Return Vehicle with Id = vehicleId
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        IDataReader Vehicle_Fetch(int clientId, int vehicleId);

        /// <summary>
        /// Return all Vehicles associated with
        /// Deal with Id = dealId
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        IDataReader Vehicle_FetchAll(int dealId);

        /// <summary>
        /// Create new Vehicle
        /// </summary>
        /// <param name="clientVehicleId"></param>
        /// <param name="dealId"></param>
        /// <param name="auditRowId"></param>
        /// <returns></returns>
        IDataReader Vehicle_Insert(int clientVehicleId, int dealId, int auditRowId);


        IDataReader Statuses_Fetch();
        IDataReader Motives_Fetch();
        IDataReader InventoryStrategies_Fetch();
        IDataReader NoteTypes_Fetch();
        IDataReader PriceTypes_Fetch();
        IDataReader Status_Fetch(int statusId);
        IDataReader Vehicles_Fetch(int dealId);
        IDataReader Deals_Fetch(int clientId);
        IDataReader Deals_FetchByExample(Model.Deal example, PageArguments pagination);

    }
}