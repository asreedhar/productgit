using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Datastores
{
    internal class DealDatastore : Datastore, IDealDatastore
    {
        public IDataReader Deal_Fetch(int dealId, int clientId)
        {
            const string queryName = Prefix + ".DealDatastore_Deal_Fetch_Id.txt";
            return Query(new[] { "Deal" },
                         queryName,
                         new Parameter("DealID", dealId, DbType.Int32),
                         new Parameter("ClientID", clientId, DbType.Int32));
        }

        public IDataReader Deal_Insert(int clientId, int auditRowId)
        {
            const string queryNameInsert = Prefix + ".DealDatastore_Deal_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".DealDatastore_Deal_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                new Parameter("DealID", clientId, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            return Query(new[] {"Contact"}, queryNameFetchIdentity);
        }

        public IDataReader Properties_Insert(int dealId, DateTime accessed, DateTime created,
                                      DateTime modified, bool isReadOnly, int revisionNumber)
        {
            throw new Exception();
        }

        public IDataReader Deal_Party_Fetch(int dealId)
        {
            const string queryName = Prefix + ".DealDatastore_Deal_Party_Fetch_Id.txt";
            return Query(new[] { "Deal_Party" },
                         queryName,
                         new Parameter("DealID", dealId, DbType.Int32));
        }

        public void Deal_Party_Insert(int dealId, int partyId, int assocationId)
        {
            const string queryName = Prefix + ".DealDatastore_Deal_Party_Insert.txt";

            NonQuery(queryName,
                new Parameter("DealID", dealId, DbType.Int32),
                new Parameter("PartyID", partyId, DbType.Int32),
                new Parameter("AssociationID", assocationId, DbType.Int32));
        }

        public IDataReader Salesperson_Fetch(int dealId)
        {
            const string queryName = Prefix + ".DealDatastore_Salesperson_Fetch_Id.txt";
            return Query(new[] { "Salesperson" },
                         queryName,
                         new Parameter("DealID", dealId, DbType.Int32));
        }

        public void Salesperson_Insert(int dealId, int employeeId, int assocationId)
        {
            const string queryName = Prefix + ".DealDatastore_Salesperson_Insert.txt";

            NonQuery(queryName,
                new Parameter("DealID", dealId, DbType.Int32),
                new Parameter("EmployeeID", employeeId, DbType.Int32),
                new Parameter("AssociationID", assocationId, DbType.Int32));
        }

        public IDataReader Deal_Status_Fetch(int dealId)
        {
            const string queryName = Prefix + ".DealDatastore_Deal_Status_Fetch_Id.txt";
            return Query(new[] { "Deal_Status" },
                         queryName,
                         new Parameter("DealID", dealId, DbType.Int32));
        }

        public void Deal_Status_Insert(int dealId, int statusId, int assocationId)
        {
            const string queryName = Prefix + ".DealDatastore_Deal_Status_Insert.txt";

            NonQuery(queryName,
                new Parameter("DealID", dealId, DbType.Int32),
                new Parameter("StatusID", statusId, DbType.Int32),
                new Parameter("AssociationID", assocationId, DbType.Int32));
        }

        public IDataReader Deal_Motivation_Fetch(int dealId)
        {
            const string queryName = Prefix + ".DealDatastore_Deal_Motivation_Fetch_Id.txt";
            return Query(new[] { "Deal_Motivation" },
                         queryName,
                         new Parameter("DealID", dealId, DbType.Int32));
        }

        public void Deal_Motivation_Insert(int dealId, int motivationId, int assocationId)
        {
            const string queryName = Prefix + ".DealDatastore_Deal_Motivation_Insert.txt";

            NonQuery(queryName,
                new Parameter("DealID", dealId, DbType.Int32),
                new Parameter("MotivationID", motivationId, DbType.Int32),
                new Parameter("AssociationID", assocationId, DbType.Int32));
        }

        public IDataReader Vehicle_TransactionType_Fetch(int vehicleId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_TransactionType_Fetch_Id.txt";
            return Query(new[] { "Vehicle_TransactionType" },
                         queryName,
                         new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void Vehicle_TransactionType_Insert(int vehicleId, int transactionTypeId, int assocationId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_TransactionType_Insert.txt";

            NonQuery(queryName,
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("TransactionTypeID", transactionTypeId, DbType.Byte),
                new Parameter("AssociationID", assocationId, DbType.Int32));
        }

        public IDataReader Vehicle_InventoryStrategy_Fetch(int vehicleId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_InventoryStrategy_Fetch_Id.txt";
            return Query(new[] { "Vehicle_InventoryStrategy" },
                         queryName,
                         new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void Vehicle_InventoryStrategy_Insert(int vehicleId, int inventoryStrategyId, 
            int assocationId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_InventoryStrategy_Insert.txt";

            NonQuery(queryName,
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("InventoryStrategyID", inventoryStrategyId, DbType.Byte),
                new Parameter("AssociationID", assocationId, DbType.Int32));
        }

        public IDataReader Vehicle_Price_Fetch(int vehicleId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_Price_Fetch_Id.txt";
            return Query(new[] { "Vehicle_InventoryStrategy" },
                         queryName,
                         new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void Vehicle_Price_Insert(int vehicleId, int priceId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_Price_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleID", vehicleId, DbType.Int32),
                     new Parameter("PriceID", priceId, DbType.Byte));
        }

        public IDataReader Vehicle_Note_Fetch(int vehicleId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_Note_Fetch_Id.txt";
            return Query(new[] { "Vehicle_InventoryStrategy" },
                         queryName,
                         new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void Vehicle_Note_Insert(int vehicleId, int noteId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_Note_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleID", vehicleId, DbType.Int32),
                     new Parameter("NoteID", noteId, DbType.Byte));
        }

        public void Vehicle_Note_Update(int vehicleId, int noteId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_Note_Update.txt";

            NonQuery(queryName,
                     new Parameter("VehicleID", vehicleId, DbType.Int32),
                     new Parameter("NoteID", noteId, DbType.Byte));
        }

        public void Vehicle_Note_Delete(int vehicleId, int noteId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_Note_Delete.txt";

            NonQuery(queryName,
                     new Parameter("VehicleID", vehicleId, DbType.Int32),
                     new Parameter("NoteID", noteId, DbType.Byte));
        }

        public IDataReader InventoryStrategy_Fetch(int inventoryStrategyId)
        {
            const string queryName = Prefix + ".DealDatastore_InventoryStrategy_Fetch_Id.txt";
            return Query(new[] { "InventoryStrategy" },
                         queryName,
                         new Parameter("InventoryStrategyID", inventoryStrategyId, DbType.Int32));
        }

        public IDataReader InventoryStrategy_Insert(string name, bool isRetail, bool isWholesale)
        {
            const string queryNameInsert = Prefix + ".DealDatastore_InventoryStrategy_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".DealDatastore_InventoryStrategy_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                new Parameter("Name", name, DbType.String),
                new Parameter("IsRetail", isRetail, DbType.Boolean),
                new Parameter("IsRetail", isWholesale, DbType.Boolean));

            return Query(new[] { "InventoryStrategy" }, queryNameFetchIdentity);
        }

        public IDataReader Note_Fetch(int noteId)
        {
            const string queryName = Prefix + ".DealDatastore_Note_Fetch_Id.txt";
            return Query(new[] { "Note" },
                         queryName,
                         new Parameter("NoteID", noteId, DbType.Int32));
        }

        public IDataReader Note_Insert(int noteTypeId, string text)
        {
            const string queryNameInsert = Prefix + ".DealDatastore_Note_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".DealDatastore_Note_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                new Parameter("NoteTypeID", noteTypeId, DbType.Byte),
                new Parameter("Text", text, DbType.String));

            return Query(new[] { "Note" }, queryNameFetchIdentity);
        }

        public void Note_Update(int noteId, int noteTypeId, string text)
        {
            const string queryName = Prefix + ".DealDatastore_Note_Update.txt";

            NonQuery(queryName,
                new Parameter("NoteID", noteId, DbType.Int32),
                new Parameter("NoteTypeID", noteTypeId, DbType.Byte),
                new Parameter("Text", text, DbType.String));

        }

        public void Note_Delete(int noteId)
        {
            const string queryName = Prefix + ".DealDatastore_Note_Delete.txt";

            NonQuery(queryName,
                new Parameter("NoteID", noteId, DbType.Int32));
        }

        public IDataReader Price_Fetch(int priceId)
        {
            const string queryName = Prefix + ".DealDatastore_Price_Fetch_Id.txt";
            return Query(new[] { "Price" },
                         queryName,
                         new Parameter("PriceID", priceId, DbType.Int32));
        }

        public IDataReader Price_Insert(int priceTypeId, int employeeId, 
            decimal value, int auditRowId)
        {
            const string queryNameInsert = Prefix + ".DealDatastore_Price_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".DealDatastore_Price_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                new Parameter("PriceTypeID", priceTypeId, DbType.Byte),
                new Parameter("EmployeeID", employeeId, DbType.Int32),
                new Parameter("Value", value, DbType.Decimal),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            return Query(new[] { "Price" }, queryNameFetchIdentity);
        }

        public IDataReader Motivation_Fetch(int motivationId)
        {
            const string queryName = Prefix + ".DealDatastore_Motivation_Fetch_Id.txt";
            return Query(new[] { "Motivation" },
                         queryName,
                         new Parameter("MotivationID", motivationId, DbType.Int32));
        }

        public IDataReader Motivation_Insert(string name, bool isRetail, bool isWholesale)
        {
            const string queryNameInsert = Prefix + ".DealDatastore_Motivation_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".DealDatastore_Motivation_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                new Parameter("Name", name, DbType.String),
                new Parameter("IsRetail", isRetail, DbType.Boolean),
                new Parameter("IsWholesale", isWholesale, DbType.Boolean));

            return Query(new[] { "Motivation" }, queryNameFetchIdentity);
        }

        public IDataReader Vehicle_Fetch(int clientId, int vehicleId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_Fetch_Id.txt";
            return Query(new[] { "Vehicle" },
                         queryName,
                         new Parameter("MotivationID", vehicleId, DbType.Int32),
                         new Parameter("ClientID", clientId, DbType.Int32));
        }

        public IDataReader Vehicle_FetchAll(int dealId)
        {
            const string queryName = Prefix + ".DealDatastore_Vehicle_FetchAll.txt";
            return Query(new[] { "Vehicle" },
                         queryName,
                         new Parameter("DealID", dealId, DbType.Int32));
        }

        public IDataReader Vehicle_Insert(int clientVehicleId, int dealId, int auditRowId)
        {
            const string queryNameInsert = Prefix + ".DealDatastore_Vehicle_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".DealDatastore_Vehicle_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                new Parameter("ClientVehicleID", clientVehicleId, DbType.Int32),
                new Parameter("DealID", dealId, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            return Query(new[] { "Vehicle" }, queryNameFetchIdentity);
        }

        public IDataReader Statuses_Fetch()
        {
            const string queryName = Prefix + ".DealDatastore_Statuses_Fetch.txt";
            return Query(new[] { "Statuses" }, queryName);
        }

        public IDataReader Motives_Fetch()
        {
            const string queryName = Prefix + ".DealDatastore_Motives_Fetch.txt";
            return Query(new[] { "Motives" }, queryName);
        }

        public IDataReader InventoryStrategies_Fetch()
        {
            const string queryName = Prefix + ".DealDatastore_InventoryStrategies_Fetch.txt";
            return Query(new[] { "InventoryStrategies" }, queryName);
        }

        public IDataReader NoteTypes_Fetch()
        {
            const string queryName = Prefix + ".DealDatastore_NoteTypes_Fetch.txt";
            return Query(new[] { "NoteTypes" }, queryName);
        }

        public IDataReader PriceTypes_Fetch()
        {
            const string queryName = Prefix + ".DealDatastore_PriceTypes_Fetch.txt";
            return Query(new[] { "PriceTypes" }, queryName);
        }

        public IDataReader Status_Fetch(int statusId)
        {
            throw new NotImplementedException();
        }

        public IDataReader Vehicles_Fetch(int dealId)
        {
            throw new NotImplementedException();
        }

        public IDataReader Deals_Fetch(int clientId)
        {
            throw new NotImplementedException();
        }

        public IDataReader Deals_FetchByExample(Model.Deal example, PageArguments pagination)
        {
            throw new NotImplementedException();
        }
    }
}