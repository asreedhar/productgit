using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Datastores
{
    internal abstract class Datastore : SessionDataStore
    {
        #region Miscellaneous

        /// <summary>
        /// Base location of sql scripts.
        /// </summary>
        internal const string Prefix = "FirstLook.Deal.DomainModel.Deals.Repositories.Resources";

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion Miscellaneous
    }
}