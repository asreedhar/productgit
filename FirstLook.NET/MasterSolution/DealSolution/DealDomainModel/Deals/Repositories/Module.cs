﻿using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Deals.Repositories
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();
            registry.Register<Serializers.Module>();

            registry.Register<IDealRepository, DealRepository>(ImplementationScope.Shared);
        }

        #endregion
    }
}