﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<DealMotiveEntity>, DealMotiveSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<DealEntity>, DealSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<DealStatusEntity>, DealStatusSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<VehicleInventoryStrategyEntity>, InventoryStrategySerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<VehicleEntity>, VehicleSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Price>, PriceSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Note>, NoteSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<NoteType>, NoteTypeSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<PriceType>, PriceTypeSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<VehicleTransactionTypeEntity>, TransactionTypeSerializer>(ImplementationScope.Shared);
        }
    }
}
