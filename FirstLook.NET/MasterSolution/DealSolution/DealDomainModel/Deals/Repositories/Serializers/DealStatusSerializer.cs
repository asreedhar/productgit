﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a deal status.
    /// </summary>
    public class DealStatusSerializer : Serializer<DealStatusEntity>
    {
        /// <summary>
        /// Deserialize to create a deal status business object.
        /// </summary>
        /// <param name="record">Record to deserialize</param>
        /// <returns>DealStatus object.</returns>
        public override DealStatusEntity Deserialize(IDataRecord record)
        {
            return new DealStatusEntity
            {
                Id = record.GetInt32("StatusID", 0),
                Name = record.GetString("StatusName"),
                IsOpen = record.GetBoolean(record.GetOrdinal("IsOpen")),
                IsClosed = record.GetBoolean(record.GetOrdinal("IsClosed")),
                AssociationId = record.GetInt32("AssociationID", 0),
                DealId = record.GetInt32("DealID", 0)
    
            };

        }
    }
}
