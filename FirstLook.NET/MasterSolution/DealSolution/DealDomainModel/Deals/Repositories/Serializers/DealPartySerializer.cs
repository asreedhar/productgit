﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    public class DealPartySerializer : Serializer<DealPartyEntity>
    {
        public override DealPartyEntity Deserialize(IDataRecord record)
        {
            return new DealPartyEntity
            {
                AssociationId = record.GetInt32("AssociationID", 0),
                DealId = record.GetInt32("DealID",0),
                PartyId = record.GetInt32("PartyID", 0)

            };

        }
    }
}
