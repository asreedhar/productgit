﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a deal note type.
    /// </summary>
    public class NoteTypeSerializer : Serializer<NoteType>
    {
        /// <summary>
        /// Deserialize to create a NoteType business object.
        /// </summary>
        /// <param name="record">Record to deserialize</param>
        /// <returns>NoteType object.</returns>
        public override NoteType Deserialize(IDataRecord record)
        {
            return new NoteType
            {
                Id = record.GetInt32("NoteTypeID", 0),
                Name = record.GetString("NoteTypeName")
            };
                        

        }
    }
}
