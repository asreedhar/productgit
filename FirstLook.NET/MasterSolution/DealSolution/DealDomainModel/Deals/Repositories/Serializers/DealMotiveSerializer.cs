﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a deal motive.
    /// </summary>
    public class DealMotiveSerializer : Serializer<DealMotiveEntity>
    {
        /// <summary>
        /// Deserialize to create a deal motive business object.
        /// </summary>
        /// <param name="record">Record to deserialize</param>
        /// <returns>DealMotive object.</returns>
        public override DealMotiveEntity Deserialize(IDataRecord record)
        {
            return new DealMotiveEntity
            {
                Id = record.GetInt32("MotivationID", 0),
                Name = record.GetString("MotivationName"),
                IsRetail = record.GetBoolean(record.GetOrdinal("IsRetail")),
                IsWholesale = record.GetBoolean(record.GetOrdinal("IsWholesale")),
                AssociationId = record.GetInt32("AssociationID", 0),
                DealId = record.GetInt32("DealID",0)

            };

        }
    }
}
