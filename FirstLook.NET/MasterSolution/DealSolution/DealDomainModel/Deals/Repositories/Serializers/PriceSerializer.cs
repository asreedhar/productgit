﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    public class PriceSerializer : Serializer<Price>
    {
        public override Price Deserialize(IDataRecord record)
        {
            return new Price
            {
                Id = record.GetInt32(record.GetOrdinal("PriceID")),
                Value = record.GetDecimal(record.GetOrdinal("Value")),
                Created = record.GetDateTime(record.GetOrdinal("Created")) //,
//              RevisionNumber = record.GetInt32(record.GetOrdinal("RevisionNumber")    TODO: I believe revision number should be part of the table, which it currently is not
            };
        }
    }
}
