﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a deal note.
    /// </summary>
    public class NoteSerializer : Serializer<Note>
    {
        /// <summary>
        /// Deserialize to create a deal note business object.
        /// </summary>
        /// <param name="record">Record to deserialize</param>
        /// <returns>Deal Note object.</returns>
        public override Note Deserialize(IDataRecord record)
        {
            return new Note
            {
                Id = record.GetInt32("NoteID", 0),
                Text = record.GetString("Text")
            };
                        

        }
    }
}
