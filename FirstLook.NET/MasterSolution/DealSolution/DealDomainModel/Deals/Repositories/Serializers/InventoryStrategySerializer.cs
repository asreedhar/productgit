﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    /// <summary>
    /// Deserialize an inventory strategy.
    /// </summary>
    public class InventoryStrategySerializer : Serializer<VehicleInventoryStrategyEntity>
    {
        /// <summary>
        /// Deserialize to create a deal inventory strategy business object.
        /// </summary>
        /// <param name="record">Record to deserialize</param>
        /// <returns>InventoryStrategy object.</returns>
        public override VehicleInventoryStrategyEntity Deserialize(IDataRecord record)
        {
            return new VehicleInventoryStrategyEntity
            {
                Id = record.GetInt32("InventoryStrategyID", 0),
                Name = record.GetString("InventoryStrategyName"),
                IsRetail = record.GetBoolean(record.GetOrdinal("IsRetail")),
                IsWholesale = record.GetBoolean(record.GetOrdinal("IsWholesale")),
                AssociationId = record.GetInt32("AssociationID", 0),
                DealId = record.GetInt32("DealID", 0)

            };
                               

        }
    }
}
