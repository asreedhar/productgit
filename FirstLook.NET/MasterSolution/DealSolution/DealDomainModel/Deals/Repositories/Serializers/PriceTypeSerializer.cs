﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a PriceType.
    /// </summary>
    public class PriceTypeSerializer : Serializer<PriceType>
    {
        /// <summary>
        /// Deserialize to create a PriceType business object.
        /// </summary>
        /// <param name="record">Record to deserialize</param>
        /// <returns>PriceType object.</returns>
        public override PriceType Deserialize(IDataRecord record)
        {
            return new PriceType
            {
                Id = record.GetInt32("PriceTypeID", 0),
                Name = record.GetString("PriceTypeName"),
            };
                        

        }
    }
}
