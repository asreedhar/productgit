﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    /// <summary>
    /// Deserialize an transaction type business object.
    /// </summary>
    public class TransactionTypeSerializer : Serializer<VehicleTransactionTypeEntity>
    {
        /// <summary>
        /// Deserialize to create a deal TransactionType business object.
        /// </summary>
        /// <param name="record">Record to deserialize</param>
        /// <returns>TransactionType object.</returns>
        public override VehicleTransactionTypeEntity Deserialize(IDataRecord record)
        {
            return new VehicleTransactionTypeEntity
            {
                Id = record.GetInt32("TransactionTypeID", 0),
                Name = record.GetString("TransactionTypeName"),
                AssociationId = record.GetInt32("AssociationID", 0),
                DealId = record.GetInt32("DealID", 0)
            };                                

        }
    }
}
