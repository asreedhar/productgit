﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    public class VehicleSerializer : Serializer<VehicleEntity>
    {
        public override VehicleEntity Deserialize(IDataRecord record)
        {
            return new VehicleEntity
            {                
                Id = record.GetInt32(record.GetOrdinal("DealVehicleID")),
                ClientVehicleId = record.GetInt32(record.GetOrdinal("VehicleID")),  
                DealId = record.GetInt32(record.GetOrdinal("DealID")),
                ReferenceId = record.GetInt32(record.GetOrdinal("ReferenceID")),
                Vin = record.GetString(record.GetOrdinal("VIN"))
            };
        }
    }
}