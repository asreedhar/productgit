﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Deals.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Deals.Repositories.Serializers
{
    public class DealSerializer : Serializer<DealEntity>
    {
        public override DealEntity Deserialize(IDataRecord record)
        {

            return new DealEntity
            {                
                Id = record.GetInt32(record.GetOrdinal("DealID")),
                Created = record.GetDateTime(record.GetOrdinal("Created")),
                RevisionNumber = record.GetInt32(record.GetOrdinal("RevisionNumber")),
                Modified = record.GetDateTime(record.GetOrdinal("Modified"))
            };
        }
    }
}