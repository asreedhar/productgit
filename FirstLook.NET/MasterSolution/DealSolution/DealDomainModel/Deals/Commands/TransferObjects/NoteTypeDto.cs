﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class NoteTypeDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
