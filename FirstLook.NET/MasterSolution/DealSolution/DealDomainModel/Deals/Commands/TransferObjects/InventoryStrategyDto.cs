﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class InventoryStrategyDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsRetail { get; set; }

        public bool IsWholesale { get; set; }
    }
}
