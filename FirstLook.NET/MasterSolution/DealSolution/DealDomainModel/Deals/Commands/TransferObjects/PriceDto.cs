﻿using System;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class PriceDto
    {
        public int RevisionNumber { get; set; }

        public int Id { get; set; }

        public DateTime Created { get; set; }

        public PriceTypeDto PriceType { get; set; }

        public decimal Value { get; set; }

        public EmployeeDto Employee { get; set; }
    }
}
