﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class PriceTypeDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
