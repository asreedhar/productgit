﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class DealStatusDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsOpen { get; set; }

        public bool IsClosed { get; set; }
    }
}
