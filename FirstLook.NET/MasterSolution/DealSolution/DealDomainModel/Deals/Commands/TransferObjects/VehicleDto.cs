﻿using System;
using System.Collections.Generic;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class VehicleDto
    {
        public TransactionTypeDto TransactionType { get; set; }

        public InventoryStrategyDto InventoryStrategy { get; set; }

        public int Id { get; set; }
        
        public int ClientVehicleId { get; set; }
        
        public int DealId { get; set; }
        
        public int ReferenceId { get; set; }
        
        public string VIN { get; set; }

        public List<PriceDto> Prices { get; set; }

        public List<NoteDto> Notes { get; set; }
    }
}
