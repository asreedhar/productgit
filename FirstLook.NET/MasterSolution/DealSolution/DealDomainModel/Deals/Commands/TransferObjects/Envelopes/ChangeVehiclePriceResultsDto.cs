﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ChangeVehiclePriceResultsDto : ResultsDto
    {
        public ChangeVehiclePriceArgumentsDto Arguments { get; set; }

        public int DealRevisionNumber { get; set; }

        public PriceDto Price { get; set; }
    }
}
