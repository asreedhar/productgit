﻿using System;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReferenceInformationResultsDto : ResultsDto
    {
        public List<DealMotiveDto> DealMotives { get; set; }

        public List<DealStatusDto> DealStatuses { get; set; }

        public List<PriceTypeDto> PriceTypes { get; set; }

        public List<NoteTypeDto> NoteTypes { get; set; }

        public List<InventoryStrategyDto> InventoryStrategies { get; set; }
    }
}
