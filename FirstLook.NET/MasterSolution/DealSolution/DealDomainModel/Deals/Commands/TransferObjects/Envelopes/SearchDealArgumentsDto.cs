﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SearchDealArgumentsDto : PaginatedArgumentsDto, IAuthorizationArgumentsDto
    {
        public AuthorizationDto Authorization { get; set; }

        public Guid Broker { get; set; }

        public DealDto Deal { get; set; }
    }
}
