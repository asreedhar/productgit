﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ChangeVehiclePriceArgumentsDto : ArgumentsDto
    {
        public int DealId { get; set; }

        public int DealRevisionNumber { get; set; }

        public PriceDto Price { get; set; }

        public string Vin { get; set; }
    }
}
