﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SearchDealResultsDto : PaginatedResultsDto, IAuthorizationResultsDto
    {
        public SearchDealArgumentsDto Arguments { get; set; }

        public TokenStatusDto AuthorizationStatus { get; set; }

        public List<DealDto> Deals { get; set; }
    }
}
