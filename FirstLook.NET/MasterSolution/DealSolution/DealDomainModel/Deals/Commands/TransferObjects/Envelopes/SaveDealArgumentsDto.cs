﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaveDealArgumentsDto : ArgumentsDto
    {
        public DealDto Deal { get; set; }
    }
}
