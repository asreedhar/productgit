﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaveDealResultsDto : ResultsDto
    {
        public SaveDealArgumentsDto Arguments { get; set; }

        public DealDto Deal { get; set; }
    }
}
