﻿using System;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class NoteDto
    {
        public int RevisionNumber { get; set; }

        public int Id { get; set; }

        public NoteTypeDto NoteType { get; set; }

        public string Text { get; set; }
    }
}
