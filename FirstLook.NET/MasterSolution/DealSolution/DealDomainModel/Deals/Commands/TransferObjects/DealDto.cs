﻿using System;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects
{
    [Serializable]
    public class DealDto
    {
        #region Identity
        
        public int RevisionNumber { get; set; }

        public int Id { get; set; }

        #endregion

        #region Summary

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public bool IsReadOnly { get; set; }

        #endregion

        public DealMotiveDto DealMotive { get; set; }

        public DealStatusDto DealStatus { get; set; }

        public EmployeeDto Salesperson { get; set; }

        public List<VehicleDto> Vehicles { get; set; }

        public List<PartyDto> Parties { get; set; }

    }
}
