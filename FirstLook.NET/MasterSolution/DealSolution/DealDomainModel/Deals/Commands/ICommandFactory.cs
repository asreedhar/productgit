﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands
{
    public interface ICommandFactory
    {
        ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand();

        ICommand<SearchDealResultsDto, SearchDealArgumentsDto> CreateSearchDealCommand();

        ICommand<SaveDealResultsDto, IIdentityContextDto<SaveDealArgumentsDto>> CreateSaveDealCommand();

        ICommand<ChangeVehiclePriceResultsDto, IIdentityContextDto<ChangeVehiclePriceArgumentsDto>> CreateChangeVehiclePriceCommand();

    }
}
