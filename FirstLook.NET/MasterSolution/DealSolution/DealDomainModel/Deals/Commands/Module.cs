﻿using FirstLook.Deal.DomainModel.Deals.Commands.Impl;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Deals.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}