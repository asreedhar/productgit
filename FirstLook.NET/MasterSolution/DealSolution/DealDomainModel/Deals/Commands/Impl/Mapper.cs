﻿using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Deals.Model;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Parties.Model;

namespace FirstLook.Deal.DomainModel.Deals.Commands.Impl
{
    public class Mapper
    {
        /// <summary>
        /// Map a vehicle to a vehicledto
        /// </summary>
        /// <param name="vehicle">Vehicle to map</param>
        /// <returns>Vehicle dto</returns>
        public static VehicleDto Map(Vehicle vehicle)
        {
            if (vehicle == null)
            {
                return null;
            }

            List<PriceDto> prices = new List<PriceDto>();

            foreach( Price price in vehicle.Prices )
            {
                prices.Add( Map(price) );
            }

            return new VehicleDto
            {
                Id = vehicle.Id,
                ClientVehicleId = vehicle.ClientVehicleId,
                DealId = vehicle.DealId,
                ReferenceId = vehicle.ReferenceId,
                VIN = vehicle.Identification.Vehicle.Vin,
                Prices = prices,
                InventoryStrategy = Map(vehicle.InventoryStrategy),
                Notes = ((List<NoteDto>)Map(vehicle.Notes)),
                TransactionType = Map(vehicle.TransactionType)
            };
        }

        /// <summary>
        /// Map a list of notes to a list of notedtos
        /// </summary>
        /// <param name="notes">Notes to map</param>
        /// <returns>List of note dtos</returns>
        public static IList<NoteDto> Map(IList<Note> notes)
        {
            if(notes == null)
            {
                return null;
            }
            IList<NoteDto> noteDtos = new List<NoteDto>();

            for (int count = 0; count < notes.Count; count++)
            {
                noteDtos.Add(Map(notes[count]));
            }

            return noteDtos;

        }


        /// <summary>
        /// Map a note to a notedto
        /// </summary>
        /// <param name="note">Note to map</param>
        /// <returns>A note dto</returns>
        public static NoteDto Map(Note note)
        {
            if (note == null)
            {
                return null;
            }

            return new NoteDto
                       {
                           Id = note.Id,
                           NoteType = Map(note.NoteType),
                           RevisionNumber = note.RevisionNumber,
                           Text = note.Text
                       };
        }

        /// <summary>
        /// Map a transaction type to a transaction type dto
        /// </summary>
        /// <param name="transactionType">Transaction type to map</param>
        /// <returns>Transaction type dto</returns>
        public static TransactionTypeDto Map(TransactionType transactionType)
        {
            if(transactionType == null)
            {
                return null;
            }

            return new TransactionTypeDto
                       {
                           Id = transactionType.Id,
                           Name = transactionType.Name
                       };

        }

        /// <summary>
        /// Map a vehicle dto to a vehicle
        /// </summary>
        /// <param name="vehicleDto">Vehicle dto to map</param>
        /// <returns>A vehicle</returns>
        public static Vehicle Map(VehicleDto vehicleDto)
        {
            List<Price> prices = new List<Price>();

            Vehicle vehicle = new Vehicle
            {
                Id = vehicleDto.Id,
                ClientVehicleId = vehicleDto.ClientVehicleId,
                DealId = vehicleDto.DealId,
                Identification = new Client.DomainModel.Vehicles.Model.ClientVehicleIdentification
                {                    
                    //Id = vehicleDto.ClientVehicleId,
                }
            };

            //vehicle.Prices.Clear;

            foreach( PriceDto priceDto in vehicleDto.Prices )
            {
                vehicle.Prices.Add( Map(priceDto) );
            }

            return vehicle;
        }

        /// <summary>
        /// Map a price to a price dto
        /// </summary>
        /// <param name="price">price to map</param>
        /// <returns>a price dto</returns>
        public static PriceDto Map(Price price)
        {
            if (price == null)
            {
                return null;
            }

            return new PriceDto
            {
                Id = price.Id,
                Employee = Employees.Commands.Impl.Mapper.Map(price.Employee),
                Created = price.Created,
                PriceType = Map(price.PriceType),
                RevisionNumber = price.RevisionNumber,
                Value = price.Value
            };
        }

        /// <summary>
        /// Map a pricedto to a price
        /// </summary>
        /// <param name="priceDto">price dto to map</param>
        /// <returns>a price</returns>
        public static Price Map(PriceDto priceDto)
        {
            return new Price
            {
                Id = priceDto.Id,
                Value = priceDto.Value
            };
        }

        /// <summary>
        /// Map a deal motive to a deal motive dto
        /// </summary>
        /// <param name="motive">motive to map</param>
        /// <returns>a deal motive dto</returns>
        public static DealMotiveDto Map(DealMotive motive)
        {
            if (motive == null)
            {
                return null;
            }
            return new DealMotiveDto
                       {
                           Id = motive.Id,
                           IsRetail = motive.IsRetail,
                           IsWholesale = motive.IsWholesale,
                           Name = motive.Name

                       };
        }

        /// <summary>
        /// Map a list of motives to a list of dto motives
        /// </summary>
        /// <param name="motives">list of motives to map</param>
        /// <returns>list of motive dtos</returns>
        public static IList<DealMotiveDto> Map(List<DealMotive> motives)
        {
            IList<DealMotiveDto> motiveDtos = new List<DealMotiveDto>();

            for (int count = 0; count < motives.Count; count++)
            {
                motiveDtos.Add(Map(motives[count]));
            }

            return motiveDtos;
        }

        /// <summary>
        /// Map a status to a status dto
        /// </summary>
        /// <param name="status">status to map</param>
        /// <returns>a status dto</returns>
        public static DealStatusDto Map(DealStatus status)
        {
            if (status == null)
            {
                return null;
            }
            return new DealStatusDto
            {
                Id = status.Id,
                IsClosed = status.IsClosed,
                IsOpen = status.IsOpen,
                Name = status.Name
            };
        }

        /// <summary>
        /// map a list of statuses to a list of status dtos
        /// </summary>
        /// <param name="statuses">statuses to map</param>
        /// <returns>list of status dtos</returns>
        public static IList<DealStatusDto> Map(List<DealStatus> statuses)
        {
            IList<DealStatusDto> statusDtos = new List<DealStatusDto>();

            for (int count = 0; count < statuses.Count; count++)
            {
                statusDtos.Add(Map(statuses[count]));
            }

            return statusDtos;
        }

        /// <summary>
        /// map an inventory strategy to an inventory strategy dto
        /// </summary>
        /// <param name="strategy">strategy to map</param>
        /// <returns>a strategy dto</returns>
        public static InventoryStrategyDto Map(InventoryStrategy strategy)
        {
            if (strategy == null)
            {
                return null;
            }
            return new InventoryStrategyDto
            {
                Id = strategy.Id,
                IsRetail = strategy.IsRetail,
                IsWholesale = strategy.IsWholesale,
                Name = strategy.Name
            };
        }

        /// <summary>
        /// map a list of strategies to a list of strategy dtos
        /// </summary>
        /// <param name="strategies">strategies to map</param>
        /// <returns>list of strategy dtos</returns>
        public static IList<InventoryStrategyDto> Map(List<InventoryStrategy> strategies)
        {
            IList<InventoryStrategyDto> strategyDtos = new List<InventoryStrategyDto>();

            for (int count = 0; count < strategies.Count; count++)
            {
                strategyDtos.Add(Map(strategies[count]));
            }

            return strategyDtos;
        }

        /// <summary>
        /// map a note type to a note type dto
        /// </summary>
        /// <param name="noteType">note type to map</param>
        /// <returns>a note type dto</returns>
        public static NoteTypeDto Map(NoteType noteType)
        {
            if (noteType == null)
            {
                return null;
            }
            return new NoteTypeDto
            {
                Id = noteType.Id,
                Name = noteType.Name
            };
        }

        /// <summary>
        /// map a list of notetypes to a list of notetype dtos
        /// </summary>
        /// <param name="noteTypes">notetypes to map</param>
        /// <returns>list of notetype dtos</returns>
        public static IList<NoteTypeDto> Map(List<NoteType> noteTypes)
        {
            IList<NoteTypeDto> noteTypeDtos = new List<NoteTypeDto>();

            for (int count = 0; count < noteTypes.Count; count++)
            {
                noteTypeDtos.Add(Map(noteTypes[count]));
            }

            return noteTypeDtos;
        }

        /// <summary>
        /// map a price type to a price type dto
        /// </summary>
        /// <param name="priceType">price type to map</param>
        /// <returns>a price type dto</returns>
        public static PriceTypeDto Map(PriceType priceType)
        {
            if (priceType == null)
            {
                return null;
            }
            return new PriceTypeDto
            {
                Id = priceType.Id,
                Name = priceType.Name
            };
        }

        /// <summary>
        /// map a list of price types to a list of price type dtos
        /// </summary>
        /// <param name="priceTypes">list of price types to map</param>
        /// <returns>list of price type dtos</returns>
        public static IList<PriceTypeDto> Map(List<PriceType> priceTypes)
        {
            IList<PriceTypeDto> priceTypeDtos = new List<PriceTypeDto>();

            for (int count = 0; count < priceTypes.Count; count++)
            {
                priceTypeDtos.Add(Map(priceTypes[count]));
            }

            return priceTypeDtos;
        }

        /// <summary>
        /// map a deal dto to a deal
        /// </summary>
        /// <param name="dealDto">deal dto to map</param>
        /// <returns>a deal</returns>
        public static Model.Deal Map(DealDto dealDto)
        {
            if(dealDto == null)
            {
                return null;
            }

            Model.Deal deal =
                new Model.Deal
                       {
                            Created = dealDto.Created,
                            DealMotive = Map(dealDto.DealMotive),
                            DealStatus = Map(dealDto.DealStatus),
                            Id = dealDto.Id,
                            IsReadOnly = dealDto.IsReadOnly,
                            Modified = dealDto.Modified,
                            RevisionNumber = dealDto.RevisionNumber,
                            SalesPerson = Employees.Commands.Impl.Mapper.Map(dealDto.Salesperson)

                       };


            if (dealDto.Parties != null)
            {
                foreach (PartyDto partyDto in dealDto.Parties)
                {
                    deal.Parties.Add(Parties.Commands.Impl.Mapper.Map(partyDto));
                }
            }

            if (dealDto.Vehicles != null)
            {
                foreach (VehicleDto vehicleDto in dealDto.Vehicles)
                {
                    deal.Vehicles.Add(Map(vehicleDto));
                }
            }

            return deal;
        }

        /// <summary>
        /// map a deal motive dto to a deal motive
        /// </summary>
        /// <param name="dealMotiveDto">motive dto to map</param>
        /// <returns>a deal motive</returns>
        public static DealMotive Map(DealMotiveDto dealMotiveDto)
        {
            if(dealMotiveDto == null)
            {
                return null;
            }

            return new DealMotive
                       {
                           Id = dealMotiveDto.Id,
                           IsRetail = dealMotiveDto.IsRetail,
                           IsWholesale = dealMotiveDto.IsWholesale,
                           Name = dealMotiveDto.Name
                       };
        }

        /// <summary>
        /// map a deal status dto to a deal status
        /// </summary>
        /// <param name="dealStatusDto">deal status dto to map</param>
        /// <returns>a deal status</returns>
        public static DealStatus Map(DealStatusDto dealStatusDto)
        {
            if (dealStatusDto == null)
            {
                return null;
            }

            return new DealStatus
            {
                Id = dealStatusDto.Id,
                IsClosed = dealStatusDto.IsClosed,
                IsOpen = dealStatusDto.IsOpen,
                Name = dealStatusDto.Name
            };
        }

        /// <summary>
        /// map a list of vehicle status dtos to a list of vehicles
        /// </summary>
        /// <param name="vehicleDtos">list of vehicle status dtos to map</param>
        /// <returns>list of vehicles</returns>
        public static IList<Vehicle> Map(IList<VehicleDto> vehicleDtos)
        {
            if (vehicleDtos == null)
            {
                return null;
            }

            IList<Vehicle> vehicles = new List<Vehicle>();

            for (int count = 0; count < vehicleDtos.Count; count++)
            {
                vehicles.Add(Map(vehicleDtos[count]));
            }

            return vehicles;
            
        }

        /// <summary>
        /// map a deal to a deal dto
        /// </summary>
        /// <param name="deal">deal to map</param>
        /// <returns>a dealdto</returns>
        public static DealDto Map(Model.Deal deal)
        {
            if(deal == null)
            {
                return null;
            }

            DealDto dealDto = new DealDto
                                  {
                                      Created = deal.Created,
                                      DealMotive = Map(deal.DealMotive),
                                      DealStatus = Map(deal.DealStatus),
                                      Id = deal.Id,
                                      IsReadOnly = deal.IsReadOnly,
                                      Modified = deal.Modified,
                                      RevisionNumber = deal.RevisionNumber,
                                      Salesperson = Employees.Commands.Impl.Mapper.Map(deal.SalesPerson)
                                  };

            if (deal.Parties != null)
            {
                foreach (Party party in deal.Parties)
                {
                    dealDto.Parties.Add(Parties.Commands.Impl.Mapper.Map(party));
                }
            }

            if (deal.Vehicles != null)
            {
                foreach (Vehicle vehicle in deal.Vehicles)
                {
                    dealDto.Vehicles.Add(Map(vehicle));
                }
            }

            return dealDto;

        }

    }
}
