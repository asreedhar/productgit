﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Deals.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand()
        {
            return new ReferenceInformationCommand();
        }

        public ICommand<SearchDealResultsDto, SearchDealArgumentsDto> CreateSearchDealCommand()
        {
            return new SearchDealCommand();
        }

        public ICommand<ChangeVehiclePriceResultsDto, IIdentityContextDto<ChangeVehiclePriceArgumentsDto>> CreateChangeVehiclePriceCommand()
        {
            return new ChangeVehiclePriceCommand();
        }

        public ICommand<SaveDealResultsDto, IIdentityContextDto<SaveDealArgumentsDto>> CreateSaveDealCommand()
        {
            return new SaveDealCommand(); 
        }

    }
}
