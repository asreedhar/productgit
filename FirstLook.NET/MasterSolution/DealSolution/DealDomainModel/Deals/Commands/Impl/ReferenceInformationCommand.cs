﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Commands.Impl
{
    public class ReferenceInformationCommand : ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto>
    {
        public ReferenceInformationResultsDto Execute(ReferenceInformationArgumentsDto parameters)
        {
            IDealRepository dealRepository = RegistryFactory.GetResolver().Resolve<IDealRepository>();

            //Call on the repository to fetch reference information
            return new ReferenceInformationResultsDto
            {
                DealMotives = (List<DealMotiveDto>)Mapper.Map((List<DealMotive>)dealRepository.DealMotives()),
                DealStatuses = (List<DealStatusDto>)Mapper.Map((List<DealStatus>)dealRepository.DealStatuses()),
                InventoryStrategies = (List<InventoryStrategyDto>)Mapper.Map((List<InventoryStrategy>)dealRepository.InventoryStrategies()),
                NoteTypes = (List<NoteTypeDto>)Mapper.Map((List<NoteType>)dealRepository.NoteTypes()),
                PriceTypes = (List<PriceTypeDto>)Mapper.Map((List<PriceType>)dealRepository.PriceTypes()),
            };
        }
    }
}
