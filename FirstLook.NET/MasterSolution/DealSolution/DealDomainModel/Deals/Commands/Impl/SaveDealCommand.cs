﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Deals.Model;

namespace FirstLook.Deal.DomainModel.Deals.Commands.Impl
{
    public class SaveDealCommand : ICommand<SaveDealResultsDto, IIdentityContextDto<SaveDealArgumentsDto>>
    {
        public SaveDealResultsDto Execute(IIdentityContextDto<SaveDealArgumentsDto> parameters)
        {
            var repository = RegistryFactory.GetResolver().Resolve<IDealRepository>();
            //var identity = parameters.Identity;
            var args = parameters.Arguments;

            // Fetch the broker and principal user.
            IBroker broker = Broker.Get("bfung", "CAS", args.Broker);   // Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);     // TODO
            IPrincipal principal = Principal.Get("bfung", "CAS");        // Principal.Get(identity.Name, identity.AuthorityName);

            Model.Deal deal = repository.Save(broker, principal, Mapper.Map(args.Deal));

            return new SaveDealResultsDto
                       {
                           Arguments = args,
                           Deal = Mapper.Map(deal)
                       };
        }
    }
}
