﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IEmployeeDatastore, EmployeeDatastore>(ImplementationScope.Shared);
        }
    }
}
