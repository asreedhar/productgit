﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Datastores
{
    internal class EmployeeDatastore : SessionDataStore, IEmployeeDatastore
    {
        internal const string Prefix = "FirstLook.Deal.DomainModel.Employees.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public IDataReader FetchRoles()
        {
            const string queryName = Prefix + ".EmployeeDatastore_Role_Fetch.txt";

            return Query(
                new[] { "Role" },
                queryName);
        }

        public IDataReader FetchEmployeeRoles(int employeeId)
        {
            const string queryName = Prefix + ".EmployeeDatastore_EmployeeRole_Fetch_Id.txt";

            return Query(
                new[] { "Employee_Role" },
                queryName,
                new Parameter("EmployeeID", employeeId, DbType.Int32));
        }

        public IDataReader Insert(int clientId, int personId, int associationId)
        {
            const string insert = Prefix + ".EmployeeDatastore_Employee_Insert.txt";

            const string select = Prefix + ".EmployeeDatastore_Employee_Fetch_Identity.txt";

            NonQuery(insert,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("PersonID", personId, DbType.Int32),
                new Parameter("AssociationID", associationId, DbType.Int32));

            return Query(new[] {"Employee"}, select);
        }

        public IDataReader Fetch(int clientId, int employeeId)
        {
            const string queryName = Prefix + ".EmployeeDatastore_Employee_Fetch_Id.txt";

            return Query(
                new[] {"Employee"},
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("EmployeeID", employeeId, DbType.Int32));
        }

        public IDataReader Search(
            int clientId, 
            bool isBuyer, bool isAppraiser, bool isSalesperson,
            string givenName, string middleName, string familyName, int? titleId,
            string occupation, string genderCode, string maritalStatusCode, DateTime? birthDate,
            PageArguments pagination)
        {
            const string queryName = Prefix + ".EmployeeDatastore_Employee_Fetch_Example.txt";

            return Query(new[] { "Employee" },
                         queryName,
                         P("IsBuyer", isBuyer),
                         P("IsAppraiser", isAppraiser),
                         P("IsSalesperson", isSalesperson),
                         P("Name_Given", givenName),
                         P("Name_Middle", middleName),
                         P("Name_Family", familyName),
                         P("Demographics_BirthDate", birthDate),
                         P("Demographics_Occupation", occupation),
                         P("Demographics_Gender", genderCode),
                         P("Demographics_MaritalStatus", maritalStatusCode),
                         P("Demographics_Title", titleId),
                         P("SortExpression", pagination.SortExpression),
                         P("StartRowIndex", pagination.StartRowIndex),
                         P("MaximumRows", pagination.MaximumRows));
        }

        static Parameter P(string name, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new Parameter(name, null, DbType.String);
            }

            return new Parameter(name, value, DbType.String);
        }

        static Parameter P(string name, DateTime? value)
        {
            if (!value.HasValue || value == DateTime.MinValue)
            {
                return new Parameter(name, null, DbType.DateTime);
            }

            return new Parameter(name, value, DbType.DateTime);
        }

        static Parameter P(string name, int? value)
        {
            if (!value.HasValue)
            {
                return new Parameter(name, null, DbType.DateTime);
            }

            return new Parameter(name, value, DbType.Int32);
        }

        static Parameter P(string name, bool value)
        {
            return new Parameter(name, value, DbType.Boolean);
        }

        public void Associate(int employeeId, int roleId, int associationId)
        {
            const string insert = Prefix + ".EmployeeDatastore_EmployeeRole_Insert.txt";

            NonQuery(insert,
                new Parameter("EmployeeID", employeeId, DbType.Int32),
                new Parameter("RoleID", roleId, DbType.Int32),
                new Parameter("AssociationID", associationId, DbType.Int32));
        }

        public bool IncrementRevisionNumber(int clientId, int employeeId, int revisionNumber)
        {
            const string update = Prefix + ".EmployeeDatastore_Employee_Update.txt";

            int rows = NonQuery(update,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("EmployeeID", employeeId, DbType.Int32),
                new Parameter("RevisionNumber", revisionNumber, DbType.Int32));

            return (rows == 1);
        }
    }
}
