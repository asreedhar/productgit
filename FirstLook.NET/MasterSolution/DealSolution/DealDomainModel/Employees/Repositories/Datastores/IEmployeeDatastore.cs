﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Datastores
{
    public interface IEmployeeDatastore
    {
        IDataReader FetchRoles();

        IDataReader FetchEmployeeRoles(int employeeId);

        IDataReader Insert(int clientId, int personId, int associationId);

        IDataReader Fetch(int clientId, int employeeId);

        IDataReader Search(
            int clientId,
            bool isBuyer, bool isAppraiser, bool isSalesperson,
            string givenName, string middleName, string familyName, int? titleId,
            string occupation, string genderCode, string maritalStatusCode, DateTime? birthDate,
            PageArguments pagination);

        void Associate(int employeeId, int roleId, int associationId);

        bool IncrementRevisionNumber(int clientId, int employeeId, int revisionNumber);
    }
}
