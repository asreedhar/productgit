﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Employees.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Serializers
{
    public class EmployeeRoleSerializer : Serializer<EmployeeRole>
    {
        public override EmployeeRole Deserialize(IDataRecord record)
        {
            int employeeId = record.GetInt32(record.GetOrdinal("EmployeeID"));

            int roleId = record.GetInt32(record.GetOrdinal("RoleID"));

            int associationId = record.GetInt32(record.GetOrdinal("AssociationID"));

            return new EmployeeRole
            {
                AssociationId = associationId,
                EmployeeId = employeeId,
                RoleId = roleId
            };
        }
    }
}