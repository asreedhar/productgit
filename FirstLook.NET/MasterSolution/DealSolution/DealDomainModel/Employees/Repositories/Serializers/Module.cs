﻿using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Deal.DomainModel.Employees.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<EmployeeEntity>, EmployeeEntitySerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<EmployeeRole>, EmployeeRoleSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<Role>, RoleSerializer>(ImplementationScope.Shared);
        }
    }
}
