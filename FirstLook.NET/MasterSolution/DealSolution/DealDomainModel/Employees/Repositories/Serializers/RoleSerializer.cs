﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Employees.Model;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Serializers
{
    public class RoleSerializer : Serializer<Role>
    {
        public override Role Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal("RoleID"));

            string name = record.GetString(record.GetOrdinal("Name"));

            return new Role {Id = id, Name = name};
        }
    }
}
