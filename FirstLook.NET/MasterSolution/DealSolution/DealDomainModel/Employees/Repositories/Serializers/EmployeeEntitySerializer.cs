﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Employees.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Serializers
{
    public class EmployeeEntitySerializer : Serializer<EmployeeEntity>
    {
        public override EmployeeEntity Deserialize(IDataRecord record)
        {
            int associationId = record.GetInt32(record.GetOrdinal("AssociationID"));

            int id = record.GetInt32(record.GetOrdinal("EmployeeID"));

            int personId = record.GetInt32(record.GetOrdinal("PersonID"));

            short revisionNumber = record.GetInt16(record.GetOrdinal("RevisionNumber"));

            return new EmployeeEntity
            {
                AssociationId = associationId,
                Id = id,
                PersonId = personId,
                RevisionNumber = revisionNumber
            };
        }
    }
}
