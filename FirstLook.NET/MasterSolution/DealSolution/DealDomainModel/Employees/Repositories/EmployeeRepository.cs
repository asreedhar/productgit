﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Deal.DomainModel.Employees.Repositories.Gateways;
using FirstLook.Deal.DomainModel.Employees.Repositories.Mappers;

namespace FirstLook.Deal.DomainModel.Employees.Repositories
{
    public class EmployeeRepository : RepositoryBase, IEmployeeRepository
    {
        private readonly EmployeeMapper _mapper = new EmployeeMapper();

        private readonly RoleGateway _gateway = new RoleGateway();

        public IList<Role> Roles()
        {
            return DoInSession(() => _gateway.Fetch());
        }

        public Employee Fetch(IBroker broker, int employeeId)
        {
            return DoInSession(() => _mapper.Fetch(broker, employeeId));
        }

        public Page<Employee> Search(IBroker broker, Employee example, PageArguments pagination)
        {
            return DoInSession(() => _mapper.Fetch(broker, example, pagination));
        }

        public void Delete(IBroker broker, IPrincipal principal, int id, int revisionNumber)
        {
            DoInTransaction<Employee>(
                session => OnBeginTransaction(session, principal),
                () => _mapper.Delete(broker, principal, id, revisionNumber));
        }

        public Employee Save(IBroker broker, IPrincipal principal, Employee employee)
        {
            return DoInTransaction(
                session => OnBeginTransaction(session, principal),
                () => _mapper.Save(broker, principal, employee));
        }

        #region Miscellaneous

        protected override string DatabaseName
        {
            get { return "Deal"; }
        }

        private void OnBeginTransaction(IDataSession session, IPrincipal principal)
        {
            var audit = session.Items["Audit"] as Audit;

            if (audit == null)
            {
                audit = Resolve<IRepository>().Create(principal);

                session.Items["Audit"] = audit;
            }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // Deprecated
        }

        #endregion Miscellaneous
    }
}
