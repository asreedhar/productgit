﻿using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Employees.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IEmployeeRepository, EmployeeRepository>(ImplementationScope.Shared);
        }
    }
}
