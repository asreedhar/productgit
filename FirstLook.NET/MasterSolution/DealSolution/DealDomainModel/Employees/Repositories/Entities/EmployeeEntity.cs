﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Deal.DomainModel.Employees.Model;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Entities
{
    [Serializable]
    public class EmployeeEntity : Employee
    {
        protected internal IList<EmployeeRole> EmployeeRoles { get; set; } 

        protected internal Association Association { get; set; }

        protected internal int AssociationId { get; set; }

        protected internal int PersonId { get; set; }
    }
}
