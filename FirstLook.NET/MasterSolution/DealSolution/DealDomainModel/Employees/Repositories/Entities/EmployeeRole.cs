﻿using System;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Entities
{
    [Serializable]
    public class EmployeeRole
    {
        public int EmployeeId { get; set; }

        public int RoleId { get; set; }

        public int AssociationId { get; set; }
    }
}