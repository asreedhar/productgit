﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Deal.DomainModel.Employees.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Employees.Repositories.Entities;
using FirstLook.Deal.DomainModel.Employees.Repositories.Gateways;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Mappers
{
    class EmployeeMapper
    {
        private readonly EmployeeGateway _employees = new EmployeeGateway();

        private readonly EmployeeRoleGateway _employeeRoles = new EmployeeRoleGateway();

        public EmployeeEntity Save(IBroker broker, IPrincipal principal, Employee employee)
        {
            // shortcut the whole affair if the data is bad

            if (employee == null || !employee.IsValid)
            {
                throw new ArgumentException("Invalid Employee", "employee");
            }

            // forward declare some helpful variables

            Employee model = employee;

            EmployeeEntity entity = model as EmployeeEntity;

            // if @model is not an entity lets try and load it and merge property values

            if (entity == null)
            {
                entity = Fetch(broker, model.Id);

                if (entity != null)
                {
                    // lets merge properties from @model onto @entity

                    Merge(model, entity);

                    if (!entity.IsValid)
                    {
                        throw new ArgumentException("Invalid Employee", "employee");
                    }
                }
            }

            // is there any work to do ?

            Employee target = entity ?? model;

            IPersisted p = target;

            if (!p.IsDirty)
            {
                return entity;
            }

            // save the person ... it takes care of itself (yay) ...

            IPersonRepository people = Resolve<IPersonRepository>();

            Person person = target.Person;

            if (p.IsDeleted)
            {
                ((IPersisted)person).MarkDeleted();
            }

            person = people.Save(broker, principal, person);

            // so insert, update or delete our employee

            if (entity == null) // === better hint for VS2010 than (p.IsNew)
            {
                // ... insert ...

                entity = _employees.Insert(broker.Client, person);

                entity.Person = person;

                // assign the employee its roles ...

                foreach (Role role in employee.Roles)
                {
                    entity.EmployeeRoles.Add(_employeeRoles.Insert(entity.Id, role.Id));
                }
            }
            else
            {
                _employees.Update(broker.Client, entity);

                entity.Person = person;

                if (p.IsDeleted)
                {
                    // ... delete ...

                    foreach (EmployeeRole employeeRole in entity.EmployeeRoles)
                    {
                        _employeeRoles.Delete(employeeRole);
                    }

                    _employees.Delete(broker.Client, entity);

                    p.MarkNew();
                }
                else
                {
                    // ... update ...

                    foreach (EmployeeRole employeeRole in entity.EmployeeRoles)
                    {
                        int roleId = employeeRole.RoleId;

                        if (!entity.Roles.Any(r => r.Id == roleId))
                        {
                            _employeeRoles.Delete(employeeRole);
                        }
                    }

                    foreach (Role role in entity.Roles)
                    {
                        int roleId = role.Id;

                        if (!entity.EmployeeRoles.Any(r => r.RoleId == roleId))
                        {
                            EmployeeRole employeeRole = _employeeRoles.Insert(entity.Id, roleId);

                            entity.EmployeeRoles.Add(employeeRole);
                        }
                    }

                    p.MarkOld();
                }
            }

            return entity;
        }

        private static void Merge(Employee src, EmployeeEntity dst)
        {
            IPersisted ps = src, pd = dst;

            if (ps.IsDeleted)
            {
                pd.MarkDeleted();
            }
            else
            {
                // merge roles

                Merge(src.Roles, dst.Roles);

                // merge people

                // ... coming soon ...
            }
        }

        private static void Merge(IEnumerable<Role> src, IList<Role> dst)
        {
            foreach (Role r in src.Where(s => !dst.Any(d => d.Id == s.Id)))
            {
                dst.Add(r);
            }

            foreach (Role r in dst.Where(d => !src.Any(s => s.Id == d.Id)))
            {
                dst.Remove(r);
            }
        }

        public EmployeeEntity Delete(IBroker broker, IPrincipal principal, int employeeId, int revisionNumber)
        {
            EmployeeEntity entity = Fetch(broker, employeeId);

            if (entity.RevisionNumber != revisionNumber)
            {
                throw new ArgumentException("Concurrent modification exception", "revisionNumber");
            }

            IPersisted p = entity;

            p.MarkDeleted();

            return Save(broker, principal, entity);
        }

        public Page<Employee> Fetch(IBroker broker, Employee example, PageArguments pagination)
        {
            Page<EmployeeEntity> values = _employees.Fetch(broker, example, pagination);

            Page<Employee> employees = new Page<Employee>
            {
                Arguments = values.Arguments,
                TotalRowCount = values.TotalRowCount,
                Items = new List<Employee>()
            };

            foreach (EmployeeEntity value in values.Items)
            {
                Employee employee = Complete(broker, value);

                employees.Items.Add(employee);
            }

            return employees;
        }

        public EmployeeEntity Fetch(IBroker broker, int employeeId)
        {
            IEmployeeDatastore employees = Resolve<IEmployeeDatastore>();

            using (IDataReader reader = employees.Fetch(broker.Id, employeeId))
            {
                if (reader.Read())
                {
                    ISerializer<EmployeeEntity> se = Resolve<ISerializer<EmployeeEntity>>();

                    EmployeeEntity entity = se.Deserialize((IDataRecord)reader);

                    Complete(broker, entity);

                    return entity;
                }
            }

            return null;
        }

        protected Employee Complete(IBroker broker, EmployeeEntity entity)
        {
            // person

            IPersonRepository people = Resolve<IPersonRepository>();

            entity.Person = people.Fetch(broker, entity.PersonId);

            // roles

            entity.EmployeeRoles = _employeeRoles.Fetch(entity.Id);

            IList<Role> roles = new RoleGateway().Fetch();

            foreach (EmployeeRole employeeRole in entity.EmployeeRoles)
            {
                EmployeeRole role = employeeRole;

                if (role != null)
                {
                    entity.Roles.Add(roles.First(x => x.Id == role.RoleId));
                }
            }

            return entity;
        }

        protected T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
