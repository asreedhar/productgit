﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Employees.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Employees.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Gateways
{
    [Serializable]
    public class EmployeeRoleGateway : AssociationGateway
    {
        internal IList<EmployeeRole> Fetch(int employeeId)
        {
            ISerializer<EmployeeRole> serializer = Resolve<ISerializer<EmployeeRole>>();

            IEmployeeDatastore datastore = Resolve<IEmployeeDatastore>();

            return serializer.Deserialize(datastore.FetchEmployeeRoles(employeeId));
        }

        public EmployeeRole Insert(int employeeId, int roleId)
        {
            IEmployeeDatastore datastore = Resolve<IEmployeeDatastore>();

            Association association = InsertAssociation();

            datastore.Associate(employeeId, roleId, association.Id);

            return new EmployeeRole {AssociationId = association.Id, EmployeeId = employeeId, RoleId = roleId};
        }

        public void Delete(EmployeeRole employeeRole)
        {
            Association association = FetchAssociation(employeeRole.AssociationId);

            AuditRow row = Delete(association.AuditRow);

            InsertAssociation(association.Id, row.Id);
        }
    }
}
