﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Deal.DomainModel.Employees.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Gateways
{
    public class RoleGateway : GatewayBase
    {
        public IList<Role> Fetch()
        {
            string key = CreateCacheKey();

            IList<Role> values = Cache.Get(key) as IList<Role>;

            if (values == null)
            {
                ISerializer<Role> serializer = Resolve<ISerializer<Role>>();

                IEmployeeDatastore employees = Resolve<IEmployeeDatastore>();

                values = serializer.Deserialize(employees.FetchRoles());

                Remember(key, values);
            }

            return values;
        }
    }
}
