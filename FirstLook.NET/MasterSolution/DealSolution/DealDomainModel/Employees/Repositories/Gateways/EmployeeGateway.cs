﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Employees.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Employees.Repositories.Entities;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Employees.Repositories.Gateways
{
    public class EmployeeGateway : AssociationGateway
    {
        public EmployeeEntity Insert(IClient client, Person person)
        {
            IEmployeeDatastore datastore = Resolve<IEmployeeDatastore>();

            Association association = InsertAssociation();

            using (IDataReader reader = datastore.Insert(client.Id, person.Id, association.Id))
            {
                if (reader.Read())
                {
                    ISerializer<EmployeeEntity> serializer = Resolve<ISerializer<EmployeeEntity>>();

                    return serializer.Deserialize((IDataRecord) reader);
                }

                throw new ArgumentException("Could not create an employee from the argument person", "person");
            }
        }

        public EmployeeEntity Update(IClient client, EmployeeEntity employee)
        {
            IEmployeeDatastore datastore = Resolve<IEmployeeDatastore>();

            if (datastore.IncrementRevisionNumber(client.Id, employee.Id, employee.RevisionNumber))
            {
                employee.RevisionNumber++;

                return employee;
            }

            throw new ArgumentException("Concurrent Modification");
        }

        public EmployeeEntity Delete(IClient client, EmployeeEntity employee)
        {
            IEmployeeDatastore datastore = Resolve<IEmployeeDatastore>();

            if (datastore.IncrementRevisionNumber(client.Id, employee.Id, employee.RevisionNumber))
            {
                employee.RevisionNumber++;

                return employee;
            }

            throw new ArgumentException("Concurrent Modification");
        }

        public Page<EmployeeEntity> Fetch(IBroker broker, Employee example, PageArguments pagination)
        {
            string key = CreateCacheKey(broker.Handle, ToString(example), pagination);

            Page<EmployeeEntity> value = Cache.Get(key) as Page<EmployeeEntity>;

            if (value == null)
            {
                ISerializer<EmployeeEntity> serializer = Resolve<ISerializer<EmployeeEntity>>();

                IEmployeeDatastore datastore = Resolve<IEmployeeDatastore>();

                int? title = null;

                if (example.Person.Title != null)
                {
                    title = example.Person.Title.Id;
                }

                string gender = null, maritalStatus = null;

                if (example.Person.Gender != null)
                {
                    gender = example.Person.Gender.Id;
                }

                if (example.Person.MaritalStatus != null)
                {
                    maritalStatus = example.Person.MaritalStatus.Id;
                }

                DateTime? birthDate = null;

                if (example.Person.BirthDate != DateTime.MinValue)
                {
                    birthDate = example.Person.BirthDate;
                }

                using (IDataReader reader = datastore.Search(
                    broker.Id,
                    example.Roles.Any(x => x.Id == 1), example.Roles.Any(x => x.Id == 2), example.Roles.Any(x => x.Id == 3),
                    example.Person.GivenName, example.Person.MiddleName, example.Person.FamilyName, title,
                    example.Person.Occupation, gender, maritalStatus, birthDate,
                    pagination))
                {
                    IList<EmployeeEntity> items = serializer.Deserialize(reader);

                    int totalRowCount = 0;

                    if (reader.NextResult())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                        }
                    }

                    value = new Page<EmployeeEntity>
                    {
                        Arguments = pagination,
                        Items = items,
                        TotalRowCount = totalRowCount
                    };

                    Remember(key, value);
                }
            }

            return value;
        }

        #region Helpers

        static string ToString(Employee employee)
        {
            StringBuilder sb = new StringBuilder("[");
            // roles
            Append(sb, "Role.1", employee.Roles.Any(x => x.Id == 1));
            Append(sb, "Role.2", employee.Roles.Any(x => x.Id == 2));
            Append(sb, "Role.3", employee.Roles.Any(x => x.Id == 3));
            // employee
            Append(sb, "Id", employee.Id.ToString());
            // name
            Append(sb, "Person.GivenName", employee.Person.GivenName);
            Append(sb, "Person.MiddleName", employee.Person.MiddleName);
            Append(sb, "Person.FamilyName", employee.Person.FamilyName);
            Append(sb, "Person.Title.Id", employee.Person.Title.Id);
            // demographics
            Append(sb, "Person.BirthDate", employee.Person.BirthDate);
            Append(sb, "Person.Occupation", employee.Person.Occupation);
            Append(sb, "Person.MaritalStatus.Id", employee.Person.MaritalStatus.Id);
            Append(sb, "Person.Gender.Id", employee.Person.Gender.Id);
            return sb.Append("]").ToString();
        }

        static void Append(StringBuilder sb, string key, string value)
        {
            sb.Append(key).Append(":").Append(value).Append(";");
        }

        static void Append(StringBuilder sb, string key, int value)
        {
            sb.Append(key).Append(":").Append(value).Append(";");
        }

        static void Append(StringBuilder sb, string key, bool value)
        {
            sb.Append(key).Append(":").Append(value).Append(";");
        }

        static void Append(StringBuilder sb, string key, DateTime? value)
        {
            sb.Append(key).Append(":").Append(value).Append(";");
        }

        #endregion
    }
}
