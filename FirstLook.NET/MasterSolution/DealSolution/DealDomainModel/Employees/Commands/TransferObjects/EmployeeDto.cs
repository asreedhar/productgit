﻿using System;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects
{
    [Serializable]
    public class EmployeeDto
    {
        public int Id { get; set; }

        public List<RoleDto> Roles { get; set; }

        public PersonDto Person { get; set; }
    }
}
