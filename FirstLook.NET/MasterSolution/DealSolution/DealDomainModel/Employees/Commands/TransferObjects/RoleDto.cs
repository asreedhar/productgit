﻿using System;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects
{
    [Serializable]
    public class RoleDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
