﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DeleteEmployeeResultsDto : ResultsDto
    {
        public DeleteEmployeeArgumentsDto Arguments { get; set; }

        public bool Success { get; set; }
    }
}
