﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchEmployeeResultsDto : ResultsDto
    {
        public FetchEmployeeArgumentsDto Arguments { get; set; }

        public EmployeeDto Employee { get; set; }

        public int Id { get; set; }
    }
}
