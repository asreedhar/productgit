﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchEmployeeArgumentsDto : ArgumentsDto
    {
        public int Id { get; set; }
    }
}
