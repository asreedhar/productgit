﻿using System;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReferenceInformationResultsDto : ResultsDto
    {
        public ReferenceInformationArgumentsDto Arguments { get; set; }

        public List<RoleDto> Roles { get; set; }
    }
}
