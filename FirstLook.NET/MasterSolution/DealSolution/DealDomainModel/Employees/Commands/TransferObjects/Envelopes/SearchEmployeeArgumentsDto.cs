﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SearchEmployeeArgumentsDto : PaginatedArgumentsDto, IAuthorizationArgumentsDto
    {
        public AuthorizationDto Authorization { get; set; }

        public EmployeeDto Example { get; set; }
    }
}
