﻿using System;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReferenceInformationArgumentsDto : IAuthorizationArgumentsDto
    {
        /// <summary>
        /// Authorization arguments.
        /// </summary>
        public AuthorizationDto Authorization { get; set; }
    }
}
