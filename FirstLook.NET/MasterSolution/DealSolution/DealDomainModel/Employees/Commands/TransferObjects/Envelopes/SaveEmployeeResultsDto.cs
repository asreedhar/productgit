﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaveEmployeeResultsDto : ResultsDto
    {
        public SaveEmployeeArgumentsDto Arguments { get; set; }

        public EmployeeDto Employee { get; set; }
    }
}
