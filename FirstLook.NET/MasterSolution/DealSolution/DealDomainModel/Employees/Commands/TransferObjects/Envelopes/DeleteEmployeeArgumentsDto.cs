﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DeleteEmployeeArgumentsDto : ArgumentsDto
    {
        public int Id { get; set; }

        public int RevisionNumber { get; set; }
    }
}
