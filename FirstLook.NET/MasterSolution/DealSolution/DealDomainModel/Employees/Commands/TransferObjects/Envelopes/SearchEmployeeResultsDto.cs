﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SearchEmployeeResultsDto : PaginatedResultsDto, IAuthorizationResultsDto
    {
        public TokenStatusDto AuthorizationStatus { get; set; }

        public List<EmployeeDto> Results { get; set; }
    }
}
