﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Employees.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, Impl.CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}