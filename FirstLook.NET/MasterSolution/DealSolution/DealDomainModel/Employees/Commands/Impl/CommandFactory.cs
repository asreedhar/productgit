﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand()
        {
            return new ReferenceInformationCommand();
        }

        public ICommand<SearchEmployeeResultsDto, IIdentityContextDto<SearchEmployeeArgumentsDto>> CreateSearchEmployeeCommand()
        {
            return new SearchEmployeeCommand();
        }

             //ICommand<FetchEmployeeResultsDto, IIdentityContextDto<FetchEmployeeArgumentsDto>>
        public ICommand<FetchEmployeeResultsDto, IIdentityContextDto<FetchEmployeeArgumentsDto>> CreateFetchEmployeeCommand()
        {
            return new FetchEmployeeCommand();
        }

        public ICommand<SaveEmployeeResultsDto, IIdentityContextDto<SaveEmployeeArgumentsDto>> CreateSaveEmployeeCommand()
        {
            return new SaveEmployeeCommand();
        }

        public ICommand<DeleteEmployeeResultsDto, IIdentityContextDto<DeleteEmployeeArgumentsDto>> CreateDeleteEmployeeCommand()
        {
            return new DeleteEmployeeCommand();
        }
    }
}
