﻿using System;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Command;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Employees.Model;

namespace FirstLook.Deal.DomainModel.Employees.Commands.Impl
{
    public class FetchEmployeeCommand : ICommand<FetchEmployeeResultsDto, IIdentityContextDto<FetchEmployeeArgumentsDto>>
    {
        public FetchEmployeeResultsDto Execute(IIdentityContextDto<FetchEmployeeArgumentsDto> parameters)
        {
            IEmployeeRepository repository = RegistryFactory.GetResolver().Resolve<IEmployeeRepository>();
            FetchEmployeeArgumentsDto arguments = parameters.Arguments;

            Employee employee = repository.Fetch(null, arguments.Id);

            return new FetchEmployeeResultsDto
            {
                Arguments = arguments,
                Employee = Mapper.Map(employee)
            };
        }
    }
}
