﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.Impl
{
    public class SaveEmployeeCommand : ICommand<SaveEmployeeResultsDto, IIdentityContextDto<SaveEmployeeArgumentsDto>>
    {
        public SaveEmployeeResultsDto Execute(IIdentityContextDto<SaveEmployeeArgumentsDto> parameters)
        {
            throw new NotImplementedException();
        }
    }
}
