﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Employees.Model;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using System.Collections.Generic;
using PageMapper = FirstLook.Client.DomainModel.Common.Commands.Mapper;

namespace FirstLook.Deal.DomainModel.Employees.Commands.Impl
{
    public class SearchEmployeeCommand : ICommand<SearchEmployeeResultsDto, IIdentityContextDto<SearchEmployeeArgumentsDto>>
    {
        internal static readonly IList<string> ColumnNames = new List<string>
                                                                {
                                                                    "Name",
                                                                    "Given"
                                                                }.AsReadOnly();

        public SearchEmployeeResultsDto Execute(IIdentityContextDto<SearchEmployeeArgumentsDto> parameters)
        {
            IEmployeeRepository repository = RegistryFactory.GetResolver().Resolve<IEmployeeRepository>();
            Page<Employee> results = repository.Search(null, 
                Mapper.Map(parameters.Arguments.Example), 
                PageMapper.Map(ColumnNames, parameters.Arguments));
            return new SearchEmployeeResultsDto
            {
                Results = Mapper.Map(results),
                TotalRowCount = 2,
            };
        }
    }
}
