﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Employees.Model;
using PersonMapper = FirstLook.Deal.DomainModel.Persons.Commands.Impl.Mapper;

namespace FirstLook.Deal.DomainModel.Employees.Commands.Impl
{
    public class Mapper
    {
        public static EmployeeDto Map(Employee employee)
        {
            if (employee == null)
            {
                return null;
            }

            List<RoleDto> roles = employee.Roles.Select(Map).ToList();

            return new EmployeeDto
            {
                Id = employee.Id,
                Roles = roles,
                Person = PersonMapper.Map(employee.Person)
            };
        }

        public static RoleDto Map(Role role)
        {
            return new RoleDto
            {
                Id = role.Id,
                Name = role.Name
            };
        }

        public static Employee Map(EmployeeDto employeeDto)
        {
            if (employeeDto == null)
            {
                return null;
            }

            Employee returnEmployee = new Employee
            {
                Id = employeeDto.Id,
                Person = PersonMapper.Map(employeeDto.Person)
            };

            if (employeeDto.Roles != null)
            {
                foreach (RoleDto role in employeeDto.Roles)
                {
                    returnEmployee.Roles.Add(Map(role));
                }
            }

            return returnEmployee;
        }
        
        public static Role Map(RoleDto role)
        {
            return new Role
            {
                Id = role.Id,
                Name = role.Name
            };
        }

        public static List<EmployeeDto> Map(Page<Employee> employees)
        {
            return employees.Items.Select(Map).ToList();
        }
    }
}
