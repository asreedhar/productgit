﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands.Impl
{
    public class DeleteEmployeeCommand : ICommand<DeleteEmployeeResultsDto, IIdentityContextDto<DeleteEmployeeArgumentsDto>>
    {
        public DeleteEmployeeResultsDto Execute(IIdentityContextDto<DeleteEmployeeArgumentsDto> parameters)
        {
            throw new NotImplementedException();
        }
    }
}
