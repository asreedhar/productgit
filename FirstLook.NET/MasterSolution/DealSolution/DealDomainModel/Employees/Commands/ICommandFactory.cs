﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Employees.Commands
{
    public interface ICommandFactory
    {
        ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand();

        ICommand<SearchEmployeeResultsDto, IIdentityContextDto<SearchEmployeeArgumentsDto>> CreateSearchEmployeeCommand();

        ICommand<FetchEmployeeResultsDto, IIdentityContextDto<FetchEmployeeArgumentsDto>> CreateFetchEmployeeCommand();

        ICommand<SaveEmployeeResultsDto, IIdentityContextDto<SaveEmployeeArgumentsDto>> CreateSaveEmployeeCommand();

        ICommand<DeleteEmployeeResultsDto, IIdentityContextDto<DeleteEmployeeArgumentsDto>> CreateDeleteEmployeeCommand();
    }
}
