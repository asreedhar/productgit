﻿using System;

namespace FirstLook.Deal.DomainModel.Employees.Model
{
    /// <summary>
    /// <list>
    /// <item>Appraiser</item>
    /// <item>Buyer</item>
    /// <item>Salesperson</item>
    /// </list>
    /// </summary>
    [Serializable]
    public class Role
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }
    }
}
