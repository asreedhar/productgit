﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Deal.DomainModel.Employees.Model
{
    public interface IEmployeeRepository
    {
        #region Reference Information

        IList<Role> Roles();

        #endregion

        #region Instance Information

        Page<Employee> Search(IBroker broker, Employee example, PageArguments pagination);

        Employee Fetch(IBroker broker, int id);

        void Delete(IBroker broker, IPrincipal principal, int id, int revisionNumber);

        Employee Save(IBroker broker, IPrincipal principal, Employee employee);

        #endregion
    }
}
