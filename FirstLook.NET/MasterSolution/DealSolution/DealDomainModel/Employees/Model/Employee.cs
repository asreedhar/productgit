﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Employees.Model
{
    [Serializable]
    public class Employee : BusinessObject
    {
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public Employee()
        {
            _roles = new List<Role>();
        }

        public int RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        #region Properties

        private Person _person;

        public Person Person
        {
            get { return _person; }
            set { Let(Inspect.NameOf((Employee p) => p.Person), ref _person, value); }
        }

        private readonly List<Role> _roles;

        public IList<Role> Roles
        {
            get { return _roles; }
        }

        #endregion

        #region Validation

        private ValidationRules<Employee> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Employee>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Employee>>()));
        }

        #endregion
    }
}
