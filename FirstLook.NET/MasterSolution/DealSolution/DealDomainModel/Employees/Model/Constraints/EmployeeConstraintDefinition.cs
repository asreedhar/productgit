﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Employees.Model.Constraints
{
    public class EmployeeConstraintDefinition : ConstraintDefinition<Employee>
    {
        public EmployeeConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Employee x) => x.Person),
                new List<IConstraint<Employee>>
                    {
                        new PropertyConstraint<Employee, Person>(
                            x => x.Person,
                            new RequiredConstraint<Person>()),
                        new PropertyConstraint<Employee, Person>(
                            x => x.Person,
                            new CompositeConstraint<Person>())
                    });
        }
    }
}
