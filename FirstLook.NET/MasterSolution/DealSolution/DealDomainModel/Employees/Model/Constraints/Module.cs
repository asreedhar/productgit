﻿using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Employees.Model.Constraints
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IConstraintDefinition<Employee>, EmployeeConstraintDefinition>(ImplementationScope.Shared);
        }
    }
}
