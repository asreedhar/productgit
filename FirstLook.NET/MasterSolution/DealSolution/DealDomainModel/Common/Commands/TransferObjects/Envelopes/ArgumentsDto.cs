﻿using System;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ArgumentsDto : IAuthorizationArgumentsDto
    {
        public AuthorizationDto Authorization { get; set; }

        public Guid Broker { get; set; }
    }
}
