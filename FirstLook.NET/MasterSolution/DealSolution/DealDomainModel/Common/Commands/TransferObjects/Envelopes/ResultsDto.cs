﻿using System;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ResultsDto : IAuthorizationResultsDto
    {
        public TokenStatusDto AuthorizationStatus { get; set; }
    }
}
