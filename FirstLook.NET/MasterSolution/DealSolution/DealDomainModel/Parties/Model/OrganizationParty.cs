﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Parties.Model
{
    public class OrganizationParty : Party
    {
        public override object Clone()
        {
            throw new System.NotImplementedException();
        }

        #region Properties
        
        private Organization _organization;

        public Organization Organization
        {
            get { return _organization; }
            set { Let(Inspect.NameOf((OrganizationParty p) => p.Organization), ref _organization, value); }
        }

        #endregion

        #region Validation

        private ValidationRules<OrganizationParty> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<OrganizationParty>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<OrganizationParty>>()));
        }

        #endregion
    }
}