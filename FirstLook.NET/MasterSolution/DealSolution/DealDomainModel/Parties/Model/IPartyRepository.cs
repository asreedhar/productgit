﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Parties.Model
{
    public interface IPartyRepository
    {
        Page<PartyEntity> Search(IBroker broker, Party example, PageArguments pagination);

        Party Fetch(IBroker broker, int id);

        Party Save(IBroker broker, IPrincipal principal, Party party);
    }
}
