﻿using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Parties.Model
{
    public class PersonParty : Party
    {
        public override object Clone()
        {
            throw new System.NotImplementedException();
        }

        #region Properties

        private Person _person;

        public Person Person
        {
            get { return _person; }
            set { Let(Inspect.NameOf((PersonParty p) => p.Person), ref _person, value); }
        }

        #endregion

        #region Validation

        private ValidationRules<PersonParty> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<PersonParty>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<PersonParty>>()));
        }

        #endregion
    }
}