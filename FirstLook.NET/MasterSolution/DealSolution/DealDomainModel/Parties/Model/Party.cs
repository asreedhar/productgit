﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Parties.Model
{
    [Serializable]
    public abstract class Party : BusinessObject
    {
        public int Id { get; protected internal set; }
        public int TypeId { get; protected internal set; }
        public int ClientId { get; protected internal set; }

        #region Validation

        protected override IValidationRules GetValidationRules()
        {
            // each derived class should have its own rules
            return null;
        }

        #endregion
    }
}
