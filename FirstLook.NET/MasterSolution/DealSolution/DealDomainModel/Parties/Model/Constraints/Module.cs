﻿using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Parties.Model.Constraints
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IConstraintDefinition<Organization>, OrganizationConstraintDefinition>(ImplementationScope.Shared);

            registry.Register<IConstraintDefinition<OrganizationParty>, OrganizationPartyConstraintDefinition>(ImplementationScope.Shared);

            registry.Register<IConstraintDefinition<PersonParty>, PersonPartyConstraintDefinition>(ImplementationScope.Shared);
        }
    }
}
