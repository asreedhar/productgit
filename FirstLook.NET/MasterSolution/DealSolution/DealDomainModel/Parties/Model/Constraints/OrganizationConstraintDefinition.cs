﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Parties.Model.Constraints
{
    public class OrganizationConstraintDefinition : ConstraintDefinition<Organization>
    {
        public OrganizationConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Organization x) => x.Name),
                new List<IConstraint<Organization>>
                    {
                        new PropertyConstraint<Organization, string>(
                            x => x.Name,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Organization, string>(
                            x => x.Name,
                            new BetweenLengthConstraint(1, 100))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Organization x) => x.Contact),
                new List<IConstraint<Organization>>
                    {
                        new PropertyConstraint<Organization, Contact>(
                            x => x.Contact,
                            new CompositeConstraint<Contact>())
                    });
        }
    }
}
