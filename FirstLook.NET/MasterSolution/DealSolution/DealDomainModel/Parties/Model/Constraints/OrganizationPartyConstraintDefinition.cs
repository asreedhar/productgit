﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;

namespace FirstLook.Deal.DomainModel.Parties.Model.Constraints
{
    public class OrganizationPartyConstraintDefinition : ConstraintDefinition<OrganizationParty>
    {
        public OrganizationPartyConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((OrganizationParty x) => x.Organization),
                new List<IConstraint<OrganizationParty>>
                    {
                        new PropertyConstraint<OrganizationParty, Organization>(
                            x => x.Organization,
                            new CompositeConstraint<Organization>())
                    });
        }
    }
}
