﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Parties.Model.Constraints
{
    public class PersonPartyConstraintDefinition : ConstraintDefinition<PersonParty>
    {
        public PersonPartyConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((PersonParty x) => x.Person),
                new List<IConstraint<PersonParty>>
                    {
                        new PropertyConstraint<PersonParty, Person>(
                            x => x.Person,
                            new CompositeConstraint<Person>())
                    });
        }
    }
}
