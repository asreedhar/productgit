﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Parties.Model
{
    [Serializable]
    public class Organization : BusinessObject
    {
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        #region Identity and Version

        public int RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        #endregion

        #region Fields
        
        private string _name;

        public string Name
        {
            get { return _name; }
            set { Let(Inspect.NameOf((Organization p) => p.Name), ref _name, value); }
        }

        #endregion

        #region Contact Information

        private Contact _contact;

        public Contact Contact
        {
            get { return _contact; }
            set { Let(Inspect.NameOf((Organization p) => p.Contact), ref _contact, value); }
        }

        #endregion

        #region Validation

        private ValidationRules<Organization> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Organization>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Organization>>()));
        }

        #endregion
    }
}
