﻿using System;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Parties.Repositories.Entities
{
    [Serializable]
    public class PersonPartyEntity : PartyEntity
    {

        protected internal int PersonId { get; set; }
        protected internal Person Person { get; set; }

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        //public PersonPartyEntity() : base()
        //{

        //}

        //internal PersonPartyEntity(PersonParty party) : base(party)
        //{
        //    Person = party.Person;
        //}

    }
}
