﻿using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Deal.DomainModel.Parties.Model;

namespace FirstLook.Deal.DomainModel.Parties.Repositories.Entities
{
    [Serializable]
    public abstract class PartyEntity : Party
    {
        protected internal int AssociationId { get; set; }
        protected internal Association Association { get; set; } 
    }
}
