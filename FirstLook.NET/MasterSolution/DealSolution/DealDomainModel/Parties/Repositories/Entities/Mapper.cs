﻿using FirstLook.Deal.DomainModel.Parties.Model;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Entities
{
    public static class Mapper
    {
        public static Party Map(PartyEntity entity)
        {
            if (entity == null) return null;

            if (entity.GetType() == typeof(PersonPartyEntity))
            {
                var ppe = (PersonPartyEntity)entity;
                return new PersonParty { 
                        Id = ppe.Id, 
                        TypeId = ppe.TypeId, 
                        ClientId = ppe.ClientId, 
                        Person = ppe.Person 
                };
            }

            if (entity.GetType() == typeof(OrganizationPartyEntity))
            {
                var ope = (OrganizationPartyEntity)entity;
                return new OrganizationParty
                           {
                               Id = ope.Id, 
                               TypeId = ope.TypeId,
                               ClientId = ope.ClientId,
                               Organization = Map(ope.Organization)
                           };
            }

            return null;
        }

        public static PartyEntity Map(Party party)
        {
            if (party == null) return null;

            if (party.GetType() == typeof(PersonParty))
            {
                var pp = (PersonParty)party;

                return new PersonPartyEntity
                           {
                               Id = pp.Id,
                               TypeId = pp.TypeId,
                               ClientId = pp.ClientId,
                               Person = pp.Person,
                               PersonId = pp.Person != null ? pp.Person.Id : -1
                           };
            }

            if (party.GetType() == typeof(OrganizationParty))
            {
                var op = (OrganizationParty)party;
                return new OrganizationPartyEntity
                           {
                               Id = op.Id,
                               TypeId = op.TypeId,
                               ClientId = op.ClientId,
                               Organization = Map(op.Organization),
                               OrganizationId = op.Organization != null ? op.Organization.Id : -1
                           };
            }

            return null;
        }


        public static Organization Map(OrganizationEntity entity)
        {
            if (entity == null) return null;
            return new Organization
                       {
                           Id = entity.Id, 
                           Name = entity.Name, 
                           RevisionNumber = entity.RevisionNumber, 
                           Contact = entity.Contact
                       };
        }

        public static OrganizationEntity Map(Organization org)
        {
            if (org == null) return null;
            return new OrganizationEntity
                       {
                           Id = org.Id, 
                           Name = org.Name, 
                           RevisionNumber = org.RevisionNumber, 
                           Contact = org.Contact, 
                           ContactId = org.Contact != null ? org.Contact.Id : -1
                       };
        }
    }
}
