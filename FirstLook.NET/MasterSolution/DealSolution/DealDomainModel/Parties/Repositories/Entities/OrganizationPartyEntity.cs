﻿using System;

namespace FirstLook.Deal.DomainModel.Parties.Repositories.Entities
{
    [Serializable]
    public class OrganizationPartyEntity : PartyEntity
    {
        protected internal int OrganizationId { get; set; }

        protected internal OrganizationEntity Organization { get; set; }

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        //public OrganizationPartyEntity() : base()
        //{

        //}

        //internal OrganizationPartyEntity(OrganizationParty party) : base(party)
        //{
        //    Organization = new OrganizationEntity(party.Organization);
        //}

     

    }
}
