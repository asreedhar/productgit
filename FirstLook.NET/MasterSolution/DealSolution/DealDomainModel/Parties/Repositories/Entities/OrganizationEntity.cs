﻿using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Parties.Model;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Entities
{
    public class OrganizationEntity : Organization
    {
        protected internal int AssociationId { get; set; }
        protected internal int ContactId { get; set; }
        protected internal int ClientId { get; set; }
        protected internal Association Association { get; set; }
    }
}
