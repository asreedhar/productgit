﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Datastores
{
    public class PartyDatastore : Datastore, IPartyDatastore
    {

        public IDataReader Party_Fetch(int partyId)
        {
            const string queryName = Prefix + ".PartyDatastore_Party_Fetch_Id.txt";

            return Query(
                new[] { "Party" },
                queryName,
                new Parameter("PartyID", partyId, DbType.Int32));
        }

        public IDataReader PersonParty_Fetch(PartyEntity example, PageArguments pagination)
        {
            const string queryName = Prefix + ".PartyDatastore_Party_Fetch_PersonParty_Example.txt";

            return Query(
                new[] { "PersonParty", "TotalRowCount" },
                queryName, Map((PersonPartyEntity)example, pagination));
        }

        public IDataReader OrganizationParty_Fetch(PartyEntity example, PageArguments pagination)
        {
            const string queryName = Prefix + ".PartyDatastore_Party_Fetch_OrganizationParty_Example.txt";

            return Query(
                new[] { "OrganizationParty", "TotalRowCount" },
                queryName, Map((OrganizationPartyEntity)example, pagination));
        }
        
        public IDataReader Party_Insert(PartyEntity entity)
        {
            const string queryNameI = Prefix + ".PartyDatastore_Party_Insert.txt";
            const string queryNameF = Prefix + ".PartyDatastore_Party_Fetch_Identity.txt";

            NonQuery(queryNameI, new Parameter("PartyTypeID", entity.TypeId, DbType.Int32),
                    new Parameter("ClientID", entity.ClientId, DbType.Int32),
                    new Parameter("AssociationID", entity.Association.Id, DbType.Int32));

            return Query(new[] { "Party" }, queryNameF);
        }

        public void PersonParty_Insert(PersonPartyEntity entity)
        {
            const string queryNameI = Prefix + ".PartyDatastore_PersonParty_Insert.txt";

            NonQuery(queryNameI, new Parameter("PartyID", entity.Id, DbType.Int32),
                    new Parameter("PersonID", entity.Person.Id, DbType.Int32));
        }

        public void OrganizationParty_Insert(OrganizationPartyEntity entity)
        {
            const string queryNameI = Prefix + ".PartyDatastore_OrganizationParty_Insert.txt";

            NonQuery(queryNameI, new Parameter("PartyID", entity.Id, DbType.Int32),
                    new Parameter("OrganizationID", entity.Organization.Id, DbType.Int32));
        }
    }
}
