﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Datastores
{
    public class OrganizationDatastore : SessionDataStore, IOrganizationDatastore
    {
        #region Miscellaneous

        internal const string Prefix = "FirstLook.Deal.DomainModel.Parties.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion Miscellaneous

        public IDataReader Organization_Fetch(int orgId)
        {
            const string queryName = Prefix + ".PartyDatastore_Organization_Fetch_Id.txt";

            return Query(
                new[] { "Organization" },
                queryName,
                new Parameter("OrganizationID", orgId, DbType.Int32));
        }

        public IDataReader Organization_Insert(OrganizationEntity entity)
        {
            const string queryNameI = Prefix + ".PartyDatastore_Organization_Insert.txt";
            const string queryNameF = Prefix + ".PartyDatastore_Organization_Fetch_Identity.txt";

            NonQuery(queryNameI, new Parameter("OrgName", entity.Name, DbType.String),
                    new Parameter("OrgClientID", entity.ClientId, DbType.Int32),
                    new Parameter("OrgRevisionNumber", entity.RevisionNumber, DbType.Int32),
                    new Parameter("AssociationID", entity.Association.Id, DbType.Int32));

            return Query(new[] { "Organization" }, queryNameF);
        }

        public void OrganizationContact_Insert(OrganizationEntity entity)
        {
            const string queryNameI = Prefix + ".PartyDatastore_OrganizationContact_Insert.txt";

            NonQuery(queryNameI, new Parameter("OrganizationID", entity.Id, DbType.Int32),
                    new Parameter("OrgContactID", entity.Contact.Id, DbType.Int32));
        }

    }
}
