﻿using System.Data;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Parties.Repositories.Datastores
{
    public interface IOrganizationDatastore
    {

        #region Organization

        IDataReader Organization_Fetch(int organizationId);

        IDataReader Organization_Insert(OrganizationEntity entity);

        void OrganizationContact_Insert(OrganizationEntity entity);

        #endregion
    }
}
