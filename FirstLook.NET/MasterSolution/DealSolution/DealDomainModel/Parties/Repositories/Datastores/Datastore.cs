﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;
using FirstLook.Deal.DomainModel.Persons.Model;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Datastores
{
    /// <summary>
    /// Abstract base class for Party datastores.
    /// </summary>
    public abstract class Datastore : SessionDataStore
    {
        /// <summary>
        /// Map paginated user details into database parameters.
        /// </summary>
        /// <param name="example">Example party details.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Array of database parameters.</returns>
        protected Parameter[] Map(OrganizationPartyEntity example, PageArguments pagination)
        {
            object id = null;
            if (example.Id != 0) id = example.Id;

            var contact = example.Organization.Contact ?? new Contact();
            var addresses = contact.PostalAddresses ?? new List<PostalAddress>();
            var address = addresses.Count > 0 ? addresses[0] : new PostalAddress();
            var phones = contact.Phones ?? new List<Phone>();
            var phone = phones.Count > 0 ? phones[0] : new Phone();

            var sortExpression = !string.IsNullOrEmpty(pagination.SortExpression)
                                     ? pagination.SortExpression
                                     : "OrgName";

            return new[]
                       {
                           new Parameter("Id", id, DbType.Int32),
                           new Parameter("OrgName", example.Organization.Name, DbType.String),

                           new Parameter("City", address.City, DbType.String),
                           new Parameter("State", address.State, DbType.String),
                           new Parameter("Zip", address.Zip, DbType.String),

                           new Parameter("CountryCode", phone.CountryCode, DbType.String),
                           new Parameter("AreaCode", phone.AreaCode, DbType.String),
                           new Parameter("SubscriberNumber", phone.SubscriberNumber, DbType.String),
                           new Parameter("PhoneLineType", phone.PhoneLineType, DbType.String),
                           new Parameter("Extension", phone.Extension, DbType.String),

                           new Parameter("SortColumns", sortExpression, DbType.String),
                           new Parameter("MaximumRows", pagination.MaximumRows, DbType.Int32),
                           new Parameter("StartRowIndex", pagination.StartRowIndex, DbType.Int32)
                       };
        }

        /// <summary>
        /// Map paginated user details into database parameters.
        /// </summary>
        /// <param name="example">Example party details.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Array of database parameters.</returns>
        protected Parameter[] Map(PersonPartyEntity example, PageArguments pagination)
        {
            object id = null;
            if (example.Id != 0) id = example.Id;

            var person = example.Person ?? new Person();
            var contact = person.Contact ?? new Contact();
            var addresses = contact.PostalAddresses ?? new List<PostalAddress>();
            var address = addresses.Count > 0 ? addresses[0] : new PostalAddress();
            var phones = contact.Phones ?? new List<Phone>();
            var phone = phones.Count > 0 ? phones[0] : new Phone();
            var gender = person.Gender ?? new Gender();
            var genderName = string.IsNullOrEmpty(gender.Name) ? null : gender.Name;
            var maritalStatus = person.MaritalStatus ?? new MaritalStatus();
            var maritalStatusName = string.IsNullOrEmpty(maritalStatus.Name) ? null : maritalStatus.Name;
            var title = person.Title ?? new Title();
            var titleName = string.IsNullOrEmpty(title.Name) ? null : title.Name;

            var sortExpression = !string.IsNullOrEmpty(pagination.SortExpression)
                                     ? pagination.SortExpression
                                     : "GivenName";

            return new[]
                       {
                           new Parameter("Id", id, DbType.Int32),

                           new Parameter("Title", titleName, DbType.String),
                           new Parameter("GivenName", person.GivenName, DbType.String),
                           new Parameter("MiddleName", person.MiddleName, DbType.String),
                           new Parameter("FamilyName", person.FamilyName, DbType.String),
                           new Parameter("Gender", genderName, DbType.String),
                           new Parameter("BirthDate", person.BirthDate == DateTime.MinValue ? new DateTime(1800, 1, 1) : person.BirthDate, DbType.Date),
                           new Parameter("MaritalStatus", maritalStatusName, DbType.String),
                           new Parameter("Occupation", person.Occupation, DbType.String),

                           new Parameter("City", address.City, DbType.String),
                           new Parameter("State", address.State, DbType.String),
                           new Parameter("Zip", address.Zip, DbType.String),

                           new Parameter("CountryCode", phone.CountryCode, DbType.String),
                           new Parameter("AreaCode", phone.AreaCode, DbType.String),
                           new Parameter("SubscriberNumber", phone.SubscriberNumber, DbType.String),
                           new Parameter("PhoneLineType", phone.PhoneLineType, DbType.String),
                           new Parameter("Extension", phone.Extension, DbType.String),

                           new Parameter("SortColumns", sortExpression, DbType.String),
                           new Parameter("MaximumRows", pagination.MaximumRows, DbType.Int32),
                           new Parameter("StartRowIndex", pagination.StartRowIndex, DbType.Int32)
                       };
        }

        #region Miscellaneous

        /// <summary>
        /// Base location of sql scripts.
        /// </summary>
        internal const string Prefix = "FirstLook.Deal.DomainModel.Parties.Repositories.Resources";

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion
    }
}
