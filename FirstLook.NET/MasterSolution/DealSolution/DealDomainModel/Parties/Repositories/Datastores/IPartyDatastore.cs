﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Datastores
{
    public interface IPartyDatastore
    {
        #region Party

        IDataReader Party_Fetch(int partyId);

        IDataReader PersonParty_Fetch(PartyEntity example, PageArguments pagination);

        IDataReader OrganizationParty_Fetch(PartyEntity example, PageArguments pagination);

        IDataReader Party_Insert(PartyEntity entity);

        void PersonParty_Insert(PersonPartyEntity entity);

        void OrganizationParty_Insert(OrganizationPartyEntity entity);

        #endregion

    }
}
