﻿using FirstLook.Client.DomainModel.Clients.Repositories.Datastores;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Parties.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IPartyDatastore, PartyDatastore>(ImplementationScope.Shared);
            registry.Register<IOrganizationDatastore, OrganizationDatastore>(ImplementationScope.Shared);
            registry.Register<IAuditDatastore, AuditDatastore>(ImplementationScope.Shared);

            registry.Register<IPersonDatastore, PersonDatastore>(ImplementationScope.Shared);
        }
    }
}
