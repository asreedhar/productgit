﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Serializers;

namespace FirstLook.Deal.DomainModel.Parties.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<PersonPartyEntity>, PersonPartySerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<OrganizationPartyEntity>, OrganizationPartySerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<OrganizationEntity>, OrganizationSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<Person>, PersonSerializer>(ImplementationScope.Shared);
        }
    }
}
