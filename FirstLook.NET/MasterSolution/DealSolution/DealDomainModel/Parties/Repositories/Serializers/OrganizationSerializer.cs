﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Serializers
{
    public class OrganizationSerializer : Serializer<OrganizationEntity>
    {
        public override OrganizationEntity Deserialize(IDataRecord record)
        {
            return new OrganizationEntity
                        {
                           Id = record.GetInt32(record.GetOrdinal("OrganizationID")),
                           Name = record.GetString(record.GetOrdinal("OrgName")),
                           RevisionNumber = record.GetInt16(record.GetOrdinal("OrgRevisionNumber")),
                           ClientId = record.GetInt32(record.GetOrdinal("OrgClientID")),
                           ContactId = record.HasColumn("OrgContactID") ? (record.IsDBNull(record.GetOrdinal("OrgContactID")) ? -1 : record.GetInt32(record.GetOrdinal("OrgContactID"))) : -1,
                           AssociationId = record.GetInt32(record.GetOrdinal("AssociationID"))
                           //Association = associationRowSerializer.Deserialize(record) // use FetchAssociation instead
                        };
        }

    }
}
