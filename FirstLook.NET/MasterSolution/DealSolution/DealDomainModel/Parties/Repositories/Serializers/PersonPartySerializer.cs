﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;
using FirstLook.Deal.DomainModel.Persons.Model;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Serializers
{
    public class PersonPartySerializer : Serializer<PersonPartyEntity>
    {
        public override PersonPartyEntity Deserialize(IDataRecord record)
        {
            var resolver = RegistryFactory.GetResolver();
            var associationRowSerializer = resolver.Resolve<ISerializer<Association>>();

            return new PersonPartyEntity
            {
                Id = record.GetInt32(record.GetOrdinal("PartyID")),
                TypeId = record.GetByte(record.GetOrdinal("PartyTypeID")),
                ClientId = record.GetInt32(record.GetOrdinal("ClientID")),
                Association = associationRowSerializer.Deserialize(record),
                PersonId = record.GetInt32(record.GetOrdinal("PersonID"))
            };
        }

    }
}
