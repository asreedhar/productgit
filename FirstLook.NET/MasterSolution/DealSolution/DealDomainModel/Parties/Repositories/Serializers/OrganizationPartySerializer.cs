﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Serializers
{
    public class OrganizationPartySerializer : Serializer<OrganizationPartyEntity>
    {
        public override OrganizationPartyEntity Deserialize(IDataRecord record)
        {
            var resolver = RegistryFactory.GetResolver();
            var associationRowSerializer = resolver.Resolve<ISerializer<Association>>();
            var orgRowSerializer = resolver.Resolve<ISerializer<OrganizationEntity>>();

            return new OrganizationPartyEntity
                       {
                           Id = record.GetInt32(record.GetOrdinal("PartyID")),
                           TypeId = record.GetByte(record.GetOrdinal("PartyTypeID")),
                           ClientId = record.GetInt32(record.GetOrdinal("ClientID")),
                           OrganizationId = record.GetInt32(record.GetOrdinal("OrganizationID")),
                           AssociationId = record.GetInt32(record.GetOrdinal("AssociationID"))
                           //Association = associationRowSerializer.Deserialize(record)   // move this out to higher level
                        };
        }

    }
}
