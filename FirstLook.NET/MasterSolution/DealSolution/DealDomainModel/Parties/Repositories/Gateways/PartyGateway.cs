﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Gateways
{
    class PartyGateway : AssociationGateway
    {

        /// <summary>
        /// Query for parties that match the user-provided example.
        /// </summary>
        /// <param name="broker">User to retrieve clients for.</param>
        /// <param name="example">Example party to match results against.</param>
        /// <param name="pagination">Paging arguments.</param>
        /// <returns>Data reader.</returns>
        public Page<PartyEntity> Fetch(IBroker broker, Party example, PageArguments pagination)
        {
            var datastore = Resolve<IPartyDatastore>();
            IDataReader reader = null;
            IList<PartyEntity> items = new List<PartyEntity>();

            if (example.GetType() == typeof(PersonParty)) {
                reader = datastore.PersonParty_Fetch(Mapper.Map(example), pagination);
                var personSerializer = Resolve<ISerializer<PersonPartyEntity>>();
                var entities = personSerializer.Deserialize(reader);

                foreach(var p in entities) {
                    p.Person = new Persons.Repositories.PersonRepository().Fetch(broker, p.PersonId);
                    items.Add(p);
                }
            }
            
            if (example.GetType() == typeof(OrganizationParty))
            {
                reader = datastore.OrganizationParty_Fetch(Mapper.Map(example), pagination);
                var organizationSerializer = Resolve<ISerializer<OrganizationPartyEntity>>();
                var entities = organizationSerializer.Deserialize(reader);

                foreach (var o in entities) {
                    o.Organization = new OrganizationGateway().Fetch(broker, o.OrganizationId);
                    items.Add(o);
                }
            }
            
            int totalRowCount = 0;
            if (reader != null && reader.NextResult())
            {
                if (reader.Read())
                {
                    totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                }
            }

            return new Page<PartyEntity>
            {
                Arguments = pagination,
                Items = items,
                TotalRowCount = totalRowCount
            };
        }

        public PartyEntity Fetch(IBroker broker, int partyId) { return Fetch_Party(broker, partyId); }

        public PartyEntity Save(IBroker broker, Party party)
        {
            return party.Id > 0 ? Update_Party(broker, party) : Insert_Party(broker, party);
        }

        protected PartyEntity Fetch_Party(IBroker broker, int partyId)
        {
            var datastore = Resolve<IPartyDatastore>();
            PartyEntity entity = null;

            using (IDataReader reader = datastore.Party_Fetch(partyId))
            {
                if (reader.Read())
                {
                    var record = (IDataRecord) reader;
                    if (!record.IsDBNull(record.GetOrdinal("PersonID")))
                    {
                        var serializer = Resolve<ISerializer<PersonPartyEntity>>();
                        entity = serializer.Deserialize(record);
                        ((PersonPartyEntity)entity).Person =
                            new Persons.Repositories.Gateways.PersonGateway().Fetch(broker, record.GetInt32(record.GetOrdinal("PersonID")));
                    }
                    else if (!record.IsDBNull(record.GetOrdinal("OrganizationID")))
                    {
                        var serializer = Resolve<ISerializer<OrganizationPartyEntity>>();
                        entity = serializer.Deserialize(record);
                        ((OrganizationPartyEntity)entity).Organization = new OrganizationGateway().Fetch(broker, record.GetInt32(record.GetOrdinal("OrganizationID")));
                   
                    }
                    else
                    {
                        throw new ApplicationException("Invalid party.");
                    }

                    // currently this is done in Deserialize but should be taken out of there
                    if (entity.Association == null && entity.AssociationId > 0) {
                        entity.Association = FetchAssociation(entity.AssociationId);
                    }

                }
            }

            return entity;
        }

        protected PartyEntity Update_Party(IBroker broker, Party party)
        {
            Delete_Party(broker, party.Id);
            return Insert_Party(broker, party);
        }

        protected PartyEntity Insert_Party(IBroker broker, Party party)
        {
            PartyEntity entity = null;

            if (party.GetType() == typeof(PersonParty)) entity = Insert_PersonParty(broker, (PersonParty)party);
            if (party.GetType() == typeof(OrganizationParty)) entity = Insert_OrganizationParty(broker, (OrganizationParty)party);

            return entity;
        }

        protected PartyEntity Insert_PersonParty(IBroker broker, PersonParty party)
        {
            if (party == null) throw new ApplicationException("PersonParty is null.");

            var datastore = Resolve<IPartyDatastore>();

            Association association = InsertAssociation();
            if (association == null) throw new ApplicationException("Could not create association.");

            // Save person TODO: find out if we want to do this
            var person = new Persons.Repositories.Mappers.PersonMapper().Save(broker, party.Person);
            if (person == null) throw new ApplicationException("Could not create/update person.");

            var entity = new PersonPartyEntity
                {
                    TypeId = party.TypeId,
                    ClientId = broker.Id,
                    Person = person, 
                    PersonId = person.Id,
                    Association = association,
                    AssociationId = association.Id
                };
            
            using (IDataReader reader = datastore.Party_Insert(entity))
            {
                if (!reader.Read()) throw new DataException("Could not save party.");

                var record = (IDataRecord)reader;
                entity.Id = record.GetInt32(record.GetOrdinal("PartyID"));
            }

            // Create lookup entry
            datastore.PersonParty_Insert(entity);

            return entity;
        }

        protected PartyEntity Insert_OrganizationParty(IBroker broker, OrganizationParty party)
        {
            if (party == null) throw new ApplicationException("OrganizationParty is null.");

            var datastore = Resolve<IPartyDatastore>();

            Association association = InsertAssociation();
            if (association == null) throw new ApplicationException("Could not create association.");

            // Save organization TODO: find out if we want to do this
            var org = new OrganizationGateway().Save(broker, party.Organization);
            if (org == null) throw new ApplicationException("Could not create/update organization.");

            var entity = new OrganizationPartyEntity
            {
                TypeId = party.TypeId,
                Organization = org, 
                OrganizationId = org.Id,
                ClientId = broker.Id,
                Association = association,
                AssociationId = association.Id
            };

            using (IDataReader reader = datastore.Party_Insert(entity))
            {
                if (!reader.Read()) throw new DataException("Could not save party.");

                var record = (IDataRecord) reader;
                entity.Id = record.GetInt32(record.GetOrdinal("PartyID"));
            }

            // Create lookup entry
            datastore.OrganizationParty_Insert(entity);

            return entity;
        }

        /// <summary>
        /// Delete a party.
        /// </summary>        
        /// <param name="partyId">Party to delete.</param>
        /// <param name="broker"></param>
        /// <returns>Party that was deleted.</returns>
        internal PartyEntity Delete_Party(IBroker broker, int partyId)
        {
            var entity = Fetch_Party(broker, partyId);
            if (entity == null) throw new ArgumentException("Invalid party id.", "party");

            if (entity.Association == null && entity.AssociationId > 0) {
                entity.Association = FetchAssociation(entity.AssociationId);
            }
            if (entity.Association == null) throw new ApplicationException("Could not get association.");


            var auditRepository = Resolve<IRepository>();
            // Mark the party record as deleted.
            auditRepository.Update(entity.Association.AuditRow, false, false, true);

            return entity;
        }

    }
}
