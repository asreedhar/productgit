﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;


namespace FirstLook.Deal.DomainModel.Parties.Repositories.Gateways
{
    class OrganizationGateway : AssociationGateway
    {

        public OrganizationEntity Fetch(IBroker broker, int orgId) { return Fetch_Organization(broker, orgId); }

        public OrganizationEntity Save(IBroker broker, Organization organization)
        {
            return organization.Id > 0 ? Update_Organization(broker, organization) : Insert_Organization(broker, organization);
        }

        protected OrganizationEntity Fetch_Organization(IBroker broker, int organizationId)
        {
            var datastore = Resolve<IOrganizationDatastore>();
            OrganizationEntity entity = null;

            using (IDataReader reader = datastore.Organization_Fetch(organizationId))
            {
                if (reader.Read())
                {
                    var record = (IDataRecord) reader;
                    var serializer = Resolve<ISerializer<OrganizationEntity>>();
                    entity = serializer.Deserialize(record);

                    if (entity.ContactId > 0) {
                        entity.Contact = new Contacts.Repositories.Gateways.ContactGateway().Fetch(entity.ContactId, broker.Id);
                    }

                    if (entity.AssociationId > 0) {
                        entity.Association = FetchAssociation(entity.AssociationId);
                    }
                }
            }

            return entity;
        }

        protected OrganizationEntity Update_Organization(IBroker broker, Organization org)
        {
            Delete_Organization(broker, org.Id);
            return Insert_Organization(broker, org);
        }

        protected OrganizationEntity Insert_Organization(IBroker broker, Organization org)
        {
            var datastore = Resolve<IOrganizationDatastore>();
            
            Association association = InsertAssociation();
            if (association == null) throw new ApplicationException("Could not create association.");
            
            var entity = new OrganizationEntity
                {
                    Id = org.Id, 
                    Name = org.Name, 
                    RevisionNumber = org.RevisionNumber, 
                    Contact = org.Contact,
                    ContactId = org.Contact == null ? -1 : org.Contact.Id,
                    ClientId = broker.Id,
                    Association = association,
                    AssociationId = association.Id
                };
           
            using (IDataReader reader = datastore.Organization_Insert(entity))
            {
                if (!reader.Read()) throw new DataException("Could not save organization.");
               
                var record = (IDataRecord) reader;
                entity.Id = record.GetInt32(record.GetOrdinal("OrganizationID"));
                entity.RevisionNumber = record.HasColumn("OrgRevisionNumber") ? record.GetInt16(record.GetOrdinal("OrgRevisionNumber")) : -1;
            }

            return entity;
        }


        internal OrganizationEntity Delete_Organization(IBroker broker, int orgId)
        {
            var entity = Fetch_Organization(broker, orgId);
            if (entity == null) throw new ArgumentException("Could not get organization to delete.", "Organization");

            if (entity.Association == null && entity.AssociationId > 0) {
                entity.Association = FetchAssociation(entity.AssociationId);
            }
            if (entity.Association == null) throw new ApplicationException("Could not get association.");


            var auditRepository = Resolve<IRepository>();
           
            // Mark the party record as deleted.
            auditRepository.Update(entity.Association.AuditRow, false, false, true);

            return entity;
        }
    }
}
