﻿using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Client.DomainModel.Clients.Repositories;


namespace FirstLook.Deal.DomainModel.Parties.Repositories
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();
            registry.Register<Serializers.Module>();
            registry.Register<IRepository, AuditRepository>(ImplementationScope.Shared);
            registry.Register<IPartyRepository, PartyRepository>(ImplementationScope.Shared);
        }

        #endregion
    }
}