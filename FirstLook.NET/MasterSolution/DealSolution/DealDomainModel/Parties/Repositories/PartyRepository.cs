﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;
using FirstLook.Deal.DomainModel.Parties.Repositories.Gateways;

namespace FirstLook.Deal.DomainModel.Parties.Repositories
{
    public class PartyRepository : RepositoryBase, IPartyRepository 
    {
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        public Page<PartyEntity> Search(IBroker broker, Party example, PageArguments pagination)
        {
            return DoInSession(() => new PartyGateway().Fetch(broker, example, pagination));
        }

        public Party Fetch(IBroker broker, int id)
        {
            return DoInSession(() => Mapper.Map(new PartyGateway().Fetch(broker, id)));
        }

        public Party Save(IBroker broker, IPrincipal principal, Party party)
        {
            return DoInTransaction(session => OnBeginTransaction(session, principal), () => Mapper.Map(new PartyGateway().Save(broker, party)));
        }

        private void OnBeginTransaction(IDataSession session, IPrincipal principal)
        {
            var audit = session.Items["Audit"] as Audit;

            if (audit != null) return;
            
            audit = Resolve<IRepository>().Create(principal);
            session.Items["Audit"] = audit;
        }

        //[ObsoleteAttribute("FirstLook.Common.Core.RepositoryBase.OnBeginTransaction has been deprecated.")]
        protected override void OnBeginTransaction(IDataSession session)
        {
            //throw new NotImplementedException("FirstLook.Common.Core.RepositoryBase.OnBeginTransaction has been deprecated.");
        }
    }
}
