﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Deal.DomainModel.Parties.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Utilities;


namespace FirstLook.Deal.DomainModel.Parties.Commands.Impl
{
    public class SearchPartyCommand : ICommand<SearchPartyResultsDto, SearchPartyArgumentsDto>
    {
        internal static readonly IList<string> ColumnNames = new List<string>
                                                                {
                                                                    "Id",
                                                                    "Title", 
                                                                    "GivenName", 
                                                                    "MiddleName", 
                                                                    "FamilyName", 
                                                                    "Gender", 
                                                                    "BirthDate", 
                                                                    "MaritalStatus", 
                                                                    "Occupation", 
                                                                    "OrgName",
                                                                    "City", 
                                                                    "State", 
                                                                    "Zip", 
                                                                    "CountryCode", 
                                                                    "AreaCode", 
                                                                    "SubscriberNumber", 
                                                                    "PhoneLineType", 
                                                                    "Extension"
                                                                }.AsReadOnly();

        public SearchPartyResultsDto Execute(SearchPartyArgumentsDto parameters)
        {
            SearchPartyArgumentsDto args = parameters;
            
            // Fetch the broker
            IBroker broker = Broker.Get("bfung", "CAS", args.Broker);    //Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);     // TODO

            var repository = RegistryFactory.GetResolver().Resolve<IPartyRepository>();

            var page = repository.Search(broker, Mapper.Map(args.Example), Client.DomainModel.Common.Commands.Mapper.Map(ColumnNames, args));

            var values = new List<PartyDto>();

            (from PartyEntity party in page.Items select party).ToList().ForEach(i => values.Add(Mapper.Map(Repositories.Entities.Mapper.Map(i))));

            var results = new SearchPartyResultsDto
            {
                Results = new List<PartyDto>(values),
                TotalRowCount = page.TotalRowCount
            };

            return results;
        }

    }
}
