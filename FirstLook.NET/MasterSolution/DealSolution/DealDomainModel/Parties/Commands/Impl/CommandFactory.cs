﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Parties.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<SearchPartyResultsDto, SearchPartyArgumentsDto> CreateSearchPartyCommand()
        {
            return new SearchPartyCommand();
        }

        public ICommand<FetchPartyResultsDto, FetchPartyArgumentsDto> CreateFetchPartyCommand()
        {
            return new FetchPartyCommand();
        }

        public ICommand<SavePartyResultsDto, IIdentityContextDto<SavePartyArgumentsDto>> CreateSavePartyCommand()
        {
            return new SavePartyCommand();
        }

        //public ICommand<SavePersonPartyResultsDto, IIdentityContextDto<SavePersonPartyArgumentsDto>> CreateSavePersonPartyCommand()
        //{
        //    return new SavePersonPartyCommand();
        //}

        //public ICommand<SaveOrganizationPartyResultsDto, IIdentityContextDto<SaveOrganizationPartyArgumentsDto>> CreateSaveOrganizationPartyCommand()
        //{
        //    return new SaveOrganizationPartyCommand();
        //}
    }
}
