﻿using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Deal.DomainModel.Persons.Commands.Impl;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Parties.Commands.Impl
{
    public static class Mapper
    {
        public static PartyDto Map(Party party)
        {
            if (party == null) return null;

            if (party.GetType() == typeof(PersonParty)) return Map((PersonParty) party);
            if (party.GetType() == typeof(OrganizationParty)) return Map((OrganizationParty)party);

            return null;
        }

        public static Party Map(PartyDto party)
        {
            if (party == null) return null;

            if (party.GetType() == typeof(PersonPartyDto)) return Map((PersonPartyDto)party);
            if (party.GetType() == typeof(OrganizationPartyDto)) return Map((OrganizationPartyDto)party);

            return null;
        }

        private static PersonPartyDto Map(PersonParty pp)
        {
            return pp == null ? null : new PersonPartyDto { Id = pp.Id, TypeId = pp.TypeId, ClientId = pp.ClientId, Person = Persons.Commands.Impl.Mapper.Map(pp.Person) };
        }

        private static OrganizationPartyDto Map(OrganizationParty op)
        {
            return op == null ? null : new OrganizationPartyDto { Id = op.Id, TypeId = op.TypeId, ClientId = op.ClientId, Organization = Map(op.Organization) };
        }


        private static Party Map(OrganizationPartyDto op)
        {
            return op == null ? null : new OrganizationParty { Id = op.Id, TypeId = op.TypeId, ClientId = op.ClientId, Organization = Map(op.Organization) };
        }

        private static Party Map(PersonPartyDto pp)
        {
            return pp == null ? null : new PersonParty { Id = pp.Id, TypeId = pp.TypeId, ClientId = pp.ClientId, Person = Persons.Commands.Impl.Mapper.Map(pp.Person) };
        }



        public static OrganizationDto Map(Organization org)
        {
            return org == null ? null : new OrganizationDto { Id = org.Id, Name = org.Name, RevisionNumber = org.RevisionNumber, Contact = Contacts.Commands.Impl.Mapper.Map(org.Contact) };
        }

        public static Organization Map(OrganizationDto org)
        {
            return org == null ? null : new Organization { Id = org.Id, Name = org.Name, RevisionNumber = org.RevisionNumber, Contact = Contacts.Commands.Impl.Mapper.Map(org.Contact) };
        }

    }
}
