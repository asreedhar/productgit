﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Parties.Model;


namespace FirstLook.Deal.DomainModel.Parties.Commands.Impl
{
    public class SavePartyCommand : ICommand<SavePartyResultsDto, IIdentityContextDto<SavePartyArgumentsDto>>
    {
        public SavePartyResultsDto Execute(IIdentityContextDto<SavePartyArgumentsDto> parameters)
        {
            var repository = RegistryFactory.GetResolver().Resolve<IPartyRepository>();
            //var identity = parameters.Identity;
            var args = parameters.Arguments;

            // Fetch the broker and principal user.
            IBroker broker = Broker.Get("bfung", "CAS", args.Broker);   // Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);     // TODO
            IPrincipal principal = Principal.Get("bfung","CAS");        // Principal.Get(identity.Name, identity.AuthorityName);


            // Save the party.            
            Party party = repository.Save(broker, principal, Mapper.Map(args.Party));

            return new SavePartyResultsDto
            {
                Arguments = args,
                Party = Mapper.Map(party)
            };
        }
    }
}
