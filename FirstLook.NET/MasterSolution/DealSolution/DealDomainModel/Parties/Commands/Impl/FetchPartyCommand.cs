﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Parties.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Parties.Commands.Impl
{
    public class FetchPartyCommand : ICommand<FetchPartyResultsDto, FetchPartyArgumentsDto>
    {
        public FetchPartyResultsDto Execute(FetchPartyArgumentsDto parameters)
        {
            var args = parameters;
            //var identityName = "bfung"; // args.Identity.Name;
            //var identityAuthorityName = "CAS"; //args.Identity.AuthorityName;

            // Fetch the broker and principal user
            IBroker broker = Broker.Get("bfung", "CAS", args.Broker);   // Broker.Get(identityName, identityAuthorityName, args.Broker);     // TODO
            //var principal = Principal.Get(identityName, identityAuthorityName);

            var repository = RegistryFactory.GetResolver().Resolve<IPartyRepository>();
            var party = repository.Fetch(broker, parameters.Id);

            return new FetchPartyResultsDto
                    {
                        Arguments = parameters,
                        Party = Mapper.Map(party) 
                    };
        }
    }
}
