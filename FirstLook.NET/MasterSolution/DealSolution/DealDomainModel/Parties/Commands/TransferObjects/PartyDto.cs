﻿using System;
using System.Xml.Serialization;


namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects
{
    [Serializable, XmlInclude(typeof(OrganizationPartyDto)), XmlInclude(typeof(PersonPartyDto))]
    public abstract class PartyDto
    {
        public int Id { get; set; }

        public int TypeId { get; set; }

        public int ClientId { get; set; }

    }
}
