﻿using System;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects
{
    [Serializable]
    public class OrganizationPartyDto : PartyDto
    {
        public OrganizationDto Organization { get; set; }
    }
}
