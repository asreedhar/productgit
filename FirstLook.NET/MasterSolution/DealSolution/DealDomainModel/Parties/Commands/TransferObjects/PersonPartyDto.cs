﻿using System;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects
{
    [Serializable]
    public class PersonPartyDto : PartyDto
    {
        public PersonDto Person { get; set; }
    }
}
