﻿using System;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects
{
    [Serializable]
    public class OrganizationDto
    {
        public int RevisionNumber { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public ContactDto Contact { get; set; }
    }
}
