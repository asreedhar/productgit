﻿using System;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SearchPartyArgumentsDto : PaginatedArgumentsDto, IAuthorizationArgumentsDto
    {
        public AuthorizationDto Authorization { get; set; }

        public Guid Broker { get; set; }
        
        public PartyDto Example { get; set; }

    }
}
