﻿using System;
using System.Xml.Serialization;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SavePartyArgumentsDto : ArgumentsDto
    {
        public PartyDto Party { get; set; }
    }
}
