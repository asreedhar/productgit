﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SearchPartyResultsDto : PaginatedResultsDto, IAuthorizationResultsDto
    {
        public TokenStatusDto AuthorizationStatus { get; set; }

        public List<PartyDto> Results { get; set; }
    }
}
