﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchPartyResultsDto : ResultsDto
    {
        public FetchPartyArgumentsDto Arguments { get; set; }

        public PartyDto Party { get; set; }
    }
}
