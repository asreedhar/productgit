﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SavePartyResultsDto : ResultsDto
    {
        public SavePartyArgumentsDto Arguments { get; set; }

        public PartyDto Party { get; set; }
    }
}
