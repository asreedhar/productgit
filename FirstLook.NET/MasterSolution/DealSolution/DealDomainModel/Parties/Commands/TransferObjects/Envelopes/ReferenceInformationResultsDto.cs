﻿using System;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReferenceInformationResultsDto : ResultsDto
    {
        public ReferenceInformationArgumentsDto Arguments { get; set; }

        public List<GenderDto> Genders { get; set; }
        //public List<ContactPointTypeDto> ContactPointTypes { get; set; }
        //public List<PhoneLineTypeDto> PhoneLineTypes { get; set; }
    }
}
