﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Parties.Commands
{
    public interface ICommandFactory
    {
        ICommand<SearchPartyResultsDto, SearchPartyArgumentsDto> CreateSearchPartyCommand();

        ICommand<FetchPartyResultsDto, FetchPartyArgumentsDto> CreateFetchPartyCommand();

        ICommand<SavePartyResultsDto, IIdentityContextDto<SavePartyArgumentsDto>> CreateSavePartyCommand();

    }
}
