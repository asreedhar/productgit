﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Parties
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Repositories.Module>();

            registry.Register<Commands.Module>();

            registry.Register<Model.Constraints.Module>();
        }

        #endregion
    }
}