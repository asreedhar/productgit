﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Contacts.Module>();

            registry.Register<Persons.Module>();

            registry.Register<Parties.Module>();

            registry.Register<Employees.Module>();

            registry.Register<Deals.Module>();
        }

        #endregion
    }
}