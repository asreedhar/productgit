﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Mappers;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories
{
    public class ContactRepository : RepositoryBase, IContactRepository
    {
        private readonly ContactMapper _contactMapper = new ContactMapper();
        private readonly ContactGateway _contactGateway = new ContactGateway();
        private readonly ContactPointPurposeGateway _contactPointPurposeGateway = new ContactPointPurposeGateway();
        private readonly PhoneLineTypeGateway _phoneLineTypeGateway = new PhoneLineTypeGateway();

        public IList<ContactPointPurpose> ContactPointPurposes()
        {
            return DoInSession(() => _contactPointPurposeGateway.ContactPointPurposes());
        }

        public IList<PhoneLineType> PhoneLineTypes()
        {
            return DoInSession(() => _phoneLineTypeGateway.PhoneLineTypes());
        }

        public Contact Fetch(IBroker broker, int contactId)
        {
            return DoInSession(() => _contactMapper.Fetch(contactId, broker.Id));
        }

        public Contact Delete(IBroker broker, Contact contact)
        {
            ((IPersisted)contact).MarkDeleted();
            return Save(broker, contact);
        }

        public Contact Save(IBroker broker, Contact contact)
        {
            return DoInTransaction(() => _contactMapper.Save(contact, broker.Id));
        }

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do Nothing
        }
    }
}