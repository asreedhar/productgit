using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal class PhoneDatastore : Datastore, IPhoneDatastore
    {
        public IDataReader FetchAll(int contactId)
        {
            const string queryName = Prefix + ".PhoneDatastore_FetchAll.txt";

            return Query(new[] { "Phone" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Fetch(int contactId)
        {
            const string queryName = Prefix + ".PhoneDatastore_Fetch_Id.txt";

            return Query(new[] { "Phone" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Insert(int contactPointId, int phoneLineTypeId, string countryCode, string areaCode, 
            string centralOfficeCode, string subscriberNumber, string extension,
            int contactId, int contactPointPurposeId, bool isPrimary, int revisionNumber)
        {
            const string queryNameInsert = Prefix + ".PhoneDatastore_Insert.txt";
            const string queryNameFetchId = Prefix + ".PhoneDatastore_Fetch_Id.txt";

            NonQuery(queryNameInsert,
                    new Parameter("PhoneLineTypeID", phoneLineTypeId, DbType.Byte),
                    new Parameter("CountryCode", countryCode, DbType.String),
                    new Parameter("AreaCode", areaCode, DbType.StringFixedLength),
                    new Parameter("CentralOfficeCode", centralOfficeCode, DbType.StringFixedLength),
                    new Parameter("SubscriberNumber", subscriberNumber, DbType.StringFixedLength),
                    new Parameter("Extension", extension, DbType.StringFixedLength),
                    new Parameter("ContactID", contactId, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeId, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));

            return Query(new[] { "Phone" },
                         queryNameFetchId,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void Update(int contactPointId, int phoneLineTypeID,
            string countryCode, string areaCode, string centralOfficeCode,
            string subscriberNumber, string extension,
            int contactID, int contactPointPurposeID, bool isPrimary, int revisionNumber)
        {
            const string queryName = Prefix + ".PhoneDatastore_Update.txt";
            NonQuery(queryName,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("PhoneLineTypeID", phoneLineTypeID, DbType.Byte),
                    new Parameter("CountryCode", countryCode, DbType.String),
                    new Parameter("AreaCode", areaCode, DbType.StringFixedLength),
                    new Parameter("CentralOfficeCode", centralOfficeCode, DbType.StringFixedLength),
                    new Parameter("SubscriberNumber", subscriberNumber, DbType.StringFixedLength),
                    new Parameter("Extension", extension, DbType.StringFixedLength),
                    new Parameter("ContactID", contactID, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeID, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));
        }

        public void Delete(int contactPointId)
        {
            const string queryName = Prefix + ".PhoneDatastore_Delete.txt";
            NonQuery(queryName,
                     new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void DeleteAll(int contactId)
        {
            const string queryName = Prefix + ".PhoneDatastore_DeleteAll.txt";
            NonQuery(queryName,
                     new Parameter("ContactID", contactId, DbType.Int32));
        }
    }
}