using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    public interface IContactPointDatastore
    {
        /// <summary>
        /// Return a ContactPoint with Id = id
        /// </summary>
        /// <param name="contactId">Id of ContactPoint</param>
        /// <returns></returns>
        IDataReader Fetch(int id);

        /// <summary>
        /// Insert one entry into ContactPoint
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Insert(int contactId, int contactPointTypeId,
                           int contactPointPurposeId, bool isPrimary, 
                           int revisionNumber);
    }
}