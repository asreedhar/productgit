﻿using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal class ContactDatastore : Datastore, IContactDatastore
    {
        public IDataReader Contact_Fetch(int contactId, int clientId)
        {
            const string queryName = Prefix + ".ContactDatastore_Contact_Fetch_Id.txt";
            return Query(new[] { "Contact" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32),
                         new Parameter("ClientID", clientId, DbType.Int32));
        }

        public IDataReader Contact_Insert(int revisionNumber, int clientId)
        {
            const string queryNameInsert = Prefix + ".ContactDatastore_Contact_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".ContactDatastore_Contact_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("RevisionNumber", revisionNumber, DbType.Byte));

            return Query(new[] {"Contact"}, queryNameFetchIdentity);
        }

        public bool Contact_Update(int contactId, int clientId, int revisionNumber)
        {
            const string queryName = Prefix + ".ContactDatastore_Contact_Update.txt";

            int updatedRows = NonQuery(queryName,
                new Parameter("ContactID", contactId, DbType.Int32),
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("RevisionNumber", revisionNumber, DbType.Byte));

            return updatedRows == 1;
        }

        public IDataReader Contact_Save(int contactId, int clientId, int revisionNumber)
        {
            const string queryName = Prefix + ".ContactDatastore_Contact_Save.txt";
            return Query(new[] {"Contact"},
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32),
                         new Parameter("ClientID", clientId, DbType.Int32),
                         new Parameter("RevisionNumber", revisionNumber, DbType.Byte));
        }

        public void Contact_Delete(int contactId, int clientId)
        {
            const string queryName = Prefix + ".ContactDatastore_Contact_Delete.txt";
            NonQuery(queryName,
                     new Parameter("ContactID", contactId, DbType.Int32),
                     new Parameter("ClientID", clientId, DbType.Int32));
        }

        public IDataReader ContactPoint_Fetch(int id)
        {
            const string queryName = Prefix + ".ContactDatastore_ContactPoint_Fetch_Id.txt";
            return Query(new[] { "ContactPoint" },
                         queryName,
                         new Parameter("ContactPointID", id, DbType.Int32));
        }

        public IDataReader ContactPoint_Insert(int contactId, int contactPointTypeId, int contactPointPurposeId,
            bool isPrimary, int revisionNumber)
        {
            const string queryNameInsert = Prefix + ".ContactDatastore_ContactPoint_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".ContactDatastore_ContactPoint_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                    new Parameter("ContactPointTypeID", contactPointTypeId, DbType.Int32),
                    new Parameter("ContactID", contactId, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeId, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));

            return Query(new[] { "ContactPoint" }, queryNameFetchIdentity);
        }

        public void ContactPoint_Update(int contactPointId, int contactId, int contactPointTypeId, int contactPointPurposeId,
            bool isPrimary, int revisionNumber)
        {
            const string queryName = Prefix + ".ContactDatastore_ContactPoint_Update.txt";

            NonQuery(queryName,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("ContactPointTypeID", contactPointTypeId, DbType.Int32),
                    new Parameter("ContactID", contactId, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeId, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));
        }

        public IDataReader ContactPointPurpose_ContactPointPurposes()
        {
            const string queryName = Prefix + ".ContactDatastore_ContactPointPurpose_ContactPointPurposes.txt";
            return Query(new[] { "ContactPointPurposes" },
                         queryName);
        }

        public IDataReader Email_FetchAll(int contactId)
        {
            const string queryName = Prefix + ".ContactDatastore_Email_FetchAll.txt";
            return Query(new[] { "Email" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Email_Fetch(int id)
        {
            const string queryName = Prefix + ".ContactDatastore_Email_Fetch_Id.txt";
            return Query(new[] { "Email" },
                         queryName,
                         new Parameter("ContactPointID", id, DbType.Int32));
        }

        public IDataReader Email_Insert(int contactPointId, string value)
        {
            const string queryNameInsert = Prefix + ".ContactDatastore_Email_Insert.txt";
            const string queryNameFetchId = Prefix + ".ContactDatastore_Email_Fetch_Id.txt";

            NonQuery(queryNameInsert,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("Value", value, DbType.String));

            return Query(new[] { "Email" },
                         queryNameFetchId,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void Email_Update(int contactPointId, string value)
        {
            const string queryName = Prefix + ".ContactDatastore_Email_Update.txt";
            NonQuery(queryName,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("Value", value, DbType.String));
        }

        public void Email_Delete(int contactPointId)
        {
            const string queryName = Prefix + ".ContactDatastore_Email_Delete.txt";
            NonQuery(queryName,
                     new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void Email_DeleteAll(int contactId)
        {
            const string queryName = Prefix + ".ContactDatastore_Email_DeleteAll.txt";
            NonQuery(queryName,
                     new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Phone_FetchAll(int contactId)
        {
            const string queryName = Prefix + ".ContactDatastore_Phone_FetchAll.txt";

            return Query(new[] { "Phone" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Phone_Fetch(int contactId)
        {
            const string queryName = Prefix + ".ContactDatastore_Phone_Fetch_Id.txt";

            return Query(new[] { "Phone" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Phone_Insert(int contactPointId, int phoneLineTypeId, string countryCode, string areaCode,
            string centralOfficeCode, string subscriberNumber, string extension)
        {
            const string queryNameInsert = Prefix + ".ContactDatastore_Phone_Insert.txt";
            const string queryNameFetchId = Prefix + ".ContactDatastore_Phone_Fetch_Id.txt";

            NonQuery(queryNameInsert,
                     new Parameter("ContactPointID", contactPointId, DbType.Int32),
                     new Parameter("PhoneLineTypeID", phoneLineTypeId, DbType.Byte),
                     new Parameter("CountryCode", countryCode, DbType.String),
                     new Parameter("AreaCode", areaCode, DbType.StringFixedLength),
                     new Parameter("CentralOfficeCode", centralOfficeCode, DbType.StringFixedLength),
                     new Parameter("SubscriberNumber", subscriberNumber, DbType.StringFixedLength),
                     new Parameter("Extension", extension, DbType.StringFixedLength));

            return Query(new[] { "Phone" },
                         queryNameFetchId,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void Phone_Update(int contactPointId, int phoneLineTypeID,
            string countryCode, string areaCode, string centralOfficeCode,
            string subscriberNumber, string extension)
        {
            const string queryName = Prefix + ".ContactDatastore_Phone_Update.txt";
            NonQuery(queryName,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("PhoneLineTypeID", phoneLineTypeID, DbType.Byte),
                    new Parameter("CountryCode", countryCode, DbType.String),
                    new Parameter("AreaCode", areaCode, DbType.StringFixedLength),
                    new Parameter("CentralOfficeCode", centralOfficeCode, DbType.StringFixedLength),
                    new Parameter("SubscriberNumber", subscriberNumber, DbType.StringFixedLength),
                    new Parameter("Extension", extension, DbType.StringFixedLength));
        }

        public void Phone_Delete(int contactPointId)
        {
            const string queryName = Prefix + ".ContactDatastore_Phone_Delete.txt";
            NonQuery(queryName,
                     new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void Phone_DeleteAll(int contactId)
        {
            const string queryName = Prefix + ".ContactDatastore_Phone_DeleteAll.txt";
            NonQuery(queryName,
                     new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader PhoneLineType_PhoneLineTypes()
        {
            const string queryName = Prefix + ".ContactDatastore_PhoneLineType_PhoneLineTypes.txt";
            return Query(new[] { "PhoneLineTypes" },
                         queryName);
        }

        public IDataReader PostalAddress_FetchAll(int contactId)
        {
            const string queryName = Prefix + ".ContactDatastore_PostalAddress_FetchAll.txt";

            return Query(new[] { "PostalAddresses" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader PostalAddress_Fetch(int contactPointId)
        {
            const string queryName = Prefix + ".ContactDatastore_PostalAddress_Fetch_Id.txt";

            return Query(new[] { "PostalAddress" },
                         queryName,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public IDataReader PostalAddress_Insert(int contactPointId, string line1, string line2, string city,
                         string state, string zip, string zipPlus4)
        {
            const string queryNameInsert = Prefix + ".ContactDatastore_PostalAddress_Insert.txt";
            const string queryNameFetchId = Prefix + ".ContactDatastore_PostalAddress_Fetch_Id.txt";

            NonQuery(queryNameInsert,
                    new Parameter("Line1", line1, DbType.String),
                    new Parameter("Line2", line2, DbType.String),
                    new Parameter("City", city, DbType.String),
                    new Parameter("State", state, DbType.StringFixedLength),
                    new Parameter("Zip", zip, DbType.StringFixedLength),
                    new Parameter("ZipPlus4", zipPlus4, DbType.StringFixedLength));

            return Query(new[] { "PostalAddress" },
                         queryNameFetchId,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void PostalAddress_Update(int contactPointId, string line1,
                         string line2, string city, string state, string zip,
                         string zipPlus4)
        {
            const string queryName = Prefix + ".ContactDatastore_PostalAddress_Update.txt";
            NonQuery(queryName,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("Line1", line1, DbType.String),
                    new Parameter("Line2", line2, DbType.String),
                    new Parameter("City", city, DbType.String),
                    new Parameter("State", state, DbType.StringFixedLength),
                    new Parameter("Zip", zip, DbType.StringFixedLength),
                    new Parameter("ZipPlus4", zipPlus4, DbType.StringFixedLength));
        }

        public void PostalAddress_Delete(int contactPointId)
        {
            const string queryName = Prefix + ".ContactDatastore_PostalAddress_Delete.txt";
            NonQuery(queryName,
                     new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void PostalAddress_DeleteAll(int contactId)
        {
            const string queryName = Prefix + ".ContactDatastore_PostalAddress_DeleteAll.txt";
            NonQuery(queryName,
                     new Parameter("ContactID", contactId, DbType.Int32));
        }

        public void Save(int id)
        {
            const string queryName = Prefix + ".VehicleDatastore_Vehicle_Save_Id.txt";
            NonQuery(queryName,
                         new Parameter("ClientVehicleID", 1, DbType.Int32),
                         new Parameter("DealID", 1, DbType.Int32),
                         new Parameter("AuditRowID", 2, DbType.Int32));
        }

        public void Delete(int id)
        {
            const string queryName = Prefix + ".VehicleDatastore_Vehicle_Delete_Id.txt";
            NonQuery(queryName,
                     new Parameter("VehicleID", id, DbType.Int32));
        }
    }
}
