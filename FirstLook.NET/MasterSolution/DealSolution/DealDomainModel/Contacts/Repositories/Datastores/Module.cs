﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IContactDatastore, ContactDatastore>(ImplementationScope.Shared);
        }
    }
}
