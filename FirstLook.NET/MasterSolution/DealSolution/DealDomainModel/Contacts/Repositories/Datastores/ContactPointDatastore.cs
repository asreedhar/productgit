using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal class ContactPointDatastore : Datastore, IContactPointDatastore
    {
        public IDataReader Fetch(int id)
        {
            const string queryName = Prefix + ".ContactPointDatastore_Fetch_Id.txt";
            return Query(new[] { "ContactPoint" }, 
                         queryName,
                         new Parameter("ContactPointID", id, DbType.Int32));
        }

        public IDataReader Insert(int contactId, int contactPointTypeId, int contactPointPurposeId, 
            bool isPrimary, int revisionNumber)
        {
            const string queryNameInsert = Prefix + ".ContactPointDatastore_Insert.txt";
            const string queryNameFetchIdentity = Prefix + ".ContactPointDatastore_Fetch_Identity.txt";

            NonQuery(queryNameInsert,
                    new Parameter("ContactPointTypeID", contactPointTypeId, DbType.Int32),
                    new Parameter("ContactID", contactId, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeId, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));

            return Query(new[] { "ContactPoint" }, queryNameFetchIdentity);
        }
    }
}