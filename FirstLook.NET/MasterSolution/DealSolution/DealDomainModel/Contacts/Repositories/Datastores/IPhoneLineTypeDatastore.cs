using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal interface IPhoneLineTypeDatastore
    {
        IDataReader PhoneLineTypes();
    }
}