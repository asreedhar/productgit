using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    public interface IPostalAddressDatastore
    {
        /// <summary>
        /// Return all PostalAddresses who belong to the Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of PostalAddress</param>
        /// <returns></returns>
        IDataReader FetchAll(int contactId);

        /// <summary>
        /// Return PostalAddresses with Id = id
        /// </summary>
        /// <param name="contactId">Id of PostalAddress</param>
        /// <returns></returns>
        IDataReader Fetch(int pointId);

        /// <summary>
        /// Insert one PostalAddress
        /// </summary>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointTypeId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Insert(int contactPointId, string line1, string line2, string city, string state, string zip,
                         string zipPlus4, int contactId,
                         int contactPointPurposeId,
                         bool isPrimary, int revisionNumber);


        /// <summary>
        /// Save one PostalAddress with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of PostalAddress</param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointTypeId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        void Update(int contactPointId, string line1,
                         string line2, string city, string state, string zip,
                         string zipPlus4, int contactId,
                         int contactPointPurposeId,
                         bool isPrimary, int revisionNumber);

        /// <summary>
        /// Delete one PostalAddress with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of PostalAddress</param>
        void Delete(int contactPointId);

        /// <summary>
        /// Delete all PostalAddresses that belong to Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of PostalAddress</param>
        void DeleteAll(int contactId);
    }
}