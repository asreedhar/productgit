using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal interface IPhoneDatastore
    {
        /// <summary>
        /// Return all Phones who belong to the Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Phone</param>
        /// <returns></returns>
        IDataReader FetchAll(int contactId);

        /// <summary>
        /// Return one Phone with Id = id
        /// </summary>
        /// <param name="contactId">Id of Phone</param>
        /// <returns></returns>
        IDataReader Fetch(int id);

        /// <summary>
        /// Insert one Phone with Id = contactPointId
        /// </summary>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Insert(int contactPointId, int phoneLineTypeId,
                        string countryCode, string areaCode, string centralOfficeCode,
                        string subscriberNumber, string extension, int contactId, 
                        int contactPointPurposeId, bool isPrimary, int revisionNumber);


        /// <summary>
        /// Update one Phone with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Phone</param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointTypeId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        void Update(int contactPointId, int phoneLineTypeId,
                    string countryCode, string areaCode, string centralOfficeCode,
                    string subscriberNumber, string extension, int contactId, 
                    int contactPointPurposeId, bool isPrimary, int revisionNumber);

        /// <summary>
        /// Delete one Phone with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Phone</param>
        void Delete(int contactPointId);

        /// <summary>
        /// Delete all Phones that belong to Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Phone</param>
        void DeleteAll(int contactId);
    }
}