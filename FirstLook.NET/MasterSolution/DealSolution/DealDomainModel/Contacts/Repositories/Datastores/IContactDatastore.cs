﻿using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    public interface IContactDatastore
    {
        /// <summary>
        /// Only returns revision number for given Contact
        /// with Id = contactId
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        IDataReader Contact_Fetch(int contactId, int clientId);

        /// <summary>
        /// Create new contactId
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Contact_Insert(int revisionNumber, int clientId);

        /// <summary>
        /// Update revision number and/or client id
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="clientId"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Whether or not updated rows = 1</returns>
        bool Contact_Update(int contactId, int clientId, int revisionNumber);

        /// <summary>
        /// Deletes Contact but 
        /// dangles Phones, Emails and Postal Addresses
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        void Contact_Delete(int contactId, int clientId);

        /// <summary>
        /// Return a ContactPoint with Id = id
        /// </summary>
        /// <param name="contactId">Id of ContactPoint</param>
        /// <returns></returns>
        IDataReader ContactPoint_Fetch(int id);

        /// <summary>
        /// Insert one entry into ContactPoint
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader ContactPoint_Insert(int contactId, int contactPointTypeId,
                           int contactPointPurposeId, bool isPrimary,
                           int revisionNumber);

        /// <summary>
        /// Update one entry of ContactPoint
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        void ContactPoint_Update(int contactPointId, int contactId, int contactPointTypeId,
                           int contactPointPurposeId, bool isPrimary,
                           int revisionNumber);

        /// <summary>
        /// Reference information
        /// </summary>
        /// <returns></returns>
        IDataReader ContactPointPurpose_ContactPointPurposes();

        /// <summary>
        /// Return all Emails who belong to the Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Email</param>
        /// <returns></returns>
        IDataReader Email_FetchAll(int contactId);

        /// <summary>
        /// Return an Email with Id = id
        /// </summary>
        /// <param name="contactId">Id of Email</param>
        /// <returns></returns>
        IDataReader Email_Fetch(int id);

        /// <summary>
        /// Insert one Email
        /// </summary>
        /// <param name="contactPointId"></param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Email_Insert(int contactPointId,
                         string value);

        /// <summary>
        /// Update one Email with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Email</param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        void Email_Update(int contactPointId, string value);

        /// <summary>
        /// Delete one Email with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Email</param>
        void Email_Delete(int contactPointId);

        /// <summary>
        /// Delete all Emails that belong to Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Email</param>
        void Email_DeleteAll(int contactId);

        /// <summary>
        /// Return all Phones who belong to the Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Phone</param>
        /// <returns></returns>
        IDataReader Phone_FetchAll(int contactId);

        /// <summary>
        /// Return one Phone with Id = id
        /// </summary>
        /// <param name="contactId">Id of Phone</param>
        /// <returns></returns>
        IDataReader Phone_Fetch(int id);

        /// <summary>
        /// Insert one new Phone
        /// </summary>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Phone_Insert(int contactPointId, int phoneLineTypeId,
                                 string countryCode, string areaCode, string centralOfficeCode,
                                 string subscriberNumber, string extension);


        /// <summary>
        /// Update one Phone with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Phone</param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointTypeId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        void Phone_Update(int contactPointId, int phoneLineTypeId,
                    string countryCode, string areaCode, string centralOfficeCode,
                    string subscriberNumber, string extension);

        /// <summary>
        /// Delete one Phone with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Phone</param>
        void Phone_Delete(int contactPointId);

        /// <summary>
        /// Delete all Phones that belong to Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Phone</param>
        void Phone_DeleteAll(int contactId);

        /// <summary>
        /// Reference information
        /// </summary>
        /// <returns></returns>
        IDataReader PhoneLineType_PhoneLineTypes();


        /// <summary>
        /// Return all PostalAddresses who belong to the Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of PostalAddress</param>
        /// <returns></returns>
        IDataReader PostalAddress_FetchAll(int contactId);

        /// <summary>
        /// Return PostalAddresses with Id = id
        /// </summary>
        /// <param name="contactId">Id of PostalAddress</param>
        /// <returns></returns>
        IDataReader PostalAddress_Fetch(int contactPointId);

        /// <summary>
        /// Insert one PostalAddress
        /// </summary>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointTypeId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader PostalAddress_Insert(int contactPointId, string line1, string line2, 
            string city, string state, string zip, string zipPlus4);


        /// <summary>
        /// Save one PostalAddress with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of PostalAddress</param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointTypeId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        void PostalAddress_Update(int contactPointId, string line1,
                         string line2, string city, string state, string zip,
                         string zipPlus4);

        /// <summary>
        /// Delete one PostalAddress with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of PostalAddress</param>
        void PostalAddress_Delete(int contactPointId);

        /// <summary>
        /// Delete all PostalAddresses that belong to Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of PostalAddress</param>
        void PostalAddress_DeleteAll(int contactId);
    }
}
