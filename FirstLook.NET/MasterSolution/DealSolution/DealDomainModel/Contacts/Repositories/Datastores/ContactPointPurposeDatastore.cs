using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal class ContactPointPurposeDatastore : Datastore, IContactPointPurposeDatastore
    {
        public IDataReader ContactPointPurposes()
        {
            const string queryName = Prefix + ".ContactPointPurposeDatastore_ContactPointPurposes.txt";
            return Query(new[] { "ContactPointPurposes" },
                         queryName);
        }
    }
}