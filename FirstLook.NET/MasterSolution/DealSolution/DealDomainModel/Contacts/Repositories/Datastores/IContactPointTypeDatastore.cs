using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    public interface IContactPointTypeDatastore
    {
        IDataReader ContactPointTypes();
    }
}