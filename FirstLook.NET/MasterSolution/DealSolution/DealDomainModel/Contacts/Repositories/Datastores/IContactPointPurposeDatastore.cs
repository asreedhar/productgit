using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    public interface IContactPointPurposeDatastore
    {
        IDataReader ContactPointPurposes();
    }
}