using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal class PostalAddressDatastore : Datastore, IPostalAddressDatastore
    {
        public IDataReader FetchAll(int contactId)
        {
            const string queryName = Prefix + ".PostalAddressDatastore_FetchAll.txt";

            return Query(new[] { "PostalAddresses" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Fetch(int contactPointId)
        {
            const string queryName = Prefix + ".PostalAddressDatastore_Fetch_Id.txt";

            return Query(new[] { "PostalAddress" },
                         queryName,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public IDataReader Insert(int contactPointId, string line1, string line2, string city, 
                         string state, string zip,
                         string zipPlus4, int contactId,
                         int contactPointPurposeId,
                         bool isPrimary, int revisionNumber)
        {
            const string queryNameInsert = Prefix + ".PostalAddressDatastore_Insert.txt";
            const string queryNameFetchId = Prefix + ".PostalAddressDatastore_Fetch_Id.txt";

            NonQuery(queryNameInsert,
                    new Parameter("Line1", line1, DbType.String),
                    new Parameter("Line2", line2, DbType.String),
                    new Parameter("City", city, DbType.String),
                    new Parameter("State", state, DbType.StringFixedLength),
                    new Parameter("Zip", zip, DbType.StringFixedLength),
                    new Parameter("ZipPlus4", zipPlus4, DbType.StringFixedLength),
                    new Parameter("ContactID", contactId, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeId, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));

            return Query(new[] { "PostalAddress" },
                         queryNameFetchId,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void Update(int contactPointId, string line1,
                         string line2, string city, string state, string zip,
                         string zipPlus4, int contactId,
                         int contactPointPurposeId,
                         bool isPrimary, int revisionNumber)
        {
            const string queryName = Prefix + ".PostalAddressDatastore_Update.txt";
            NonQuery(queryName,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("Line1", line1, DbType.String),
                    new Parameter("Line2", line2, DbType.String),
                    new Parameter("City", city, DbType.String),
                    new Parameter("State", state, DbType.StringFixedLength),
                    new Parameter("Zip", zip, DbType.StringFixedLength),
                    new Parameter("ZipPlus4", zipPlus4, DbType.StringFixedLength),
                    new Parameter("ContactID", contactId, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeId, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));
        }

        public void Delete(int contactPointId)
        {
            const string queryName = Prefix + ".PostalAddressDatastore_Delete.txt";
            NonQuery(queryName,
                     new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void DeleteAll(int contactId)
        {
            const string queryName = Prefix + ".PostalAddressDatastore_DeleteAll.txt";
            NonQuery(queryName,
                     new Parameter("ContactID", contactId, DbType.Int32));
        }
    }
}