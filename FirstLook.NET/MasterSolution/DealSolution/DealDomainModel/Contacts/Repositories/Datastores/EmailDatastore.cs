using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal class EmailDatastore : Datastore, IEmailDatastore
    {
        public IDataReader FetchAll(int contactId)
        {
            const string queryName = Prefix + ".EmailDatastore_FetchAll.txt";
            return Query(new[] { "Email" },
                         queryName,
                         new Parameter("ContactID", contactId, DbType.Int32));
        }

        public IDataReader Fetch(int id)
        {
            const string queryName = Prefix + ".EmailDatastore_Fetch_Id.txt";
            return Query(new[] { "Email" },
                         queryName,
                         new Parameter("ContactPointID", id, DbType.Int32));
        }

        public IDataReader Insert(int contactPointId, string value, int contactID,
            int contactPointPurposeID, bool isPrimary, int revisionNumber)
        {
            const string queryNameInsert = Prefix + ".EmailDatastore_Insert.txt";
            const string queryNameFetchId = Prefix + ".EmailDatastore_Fetch_Id.txt";

            NonQuery(queryNameInsert,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("Value", value, DbType.String),
                    new Parameter("ContactID", contactID, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeID, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));

            return Query(new[] {"Email"},
                         queryNameFetchId,
                         new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void Update(int contactPointId, string value, int contactID,
            int contactPointPurposeID, bool isPrimary, int revisionNumber)
        {
            const string queryName = Prefix + ".EmailDatastore_Update.txt";
            NonQuery(queryName,
                    new Parameter("ContactPointID", contactPointId, DbType.Int32),
                    new Parameter("Value", value, DbType.String),
                    new Parameter("ContactID", contactID, DbType.Int32),
                    new Parameter("ContactPointPurposeID", contactPointPurposeID, DbType.Byte),
                    new Parameter("IsPrimary", isPrimary, DbType.Boolean),
                    new Parameter("RevisionNumber", revisionNumber, DbType.Byte));
        }

        public void Delete(int contactPointId)
        {
            const string queryName = Prefix + ".EmailDatastore_Delete.txt";
            NonQuery(queryName,
                     new Parameter("ContactPointID", contactPointId, DbType.Int32));
        }

        public void DeleteAll(int contactId)
        {
            const string queryName = Prefix + ".EmailDatastore_DeleteAll.txt";
            NonQuery(queryName,
                     new Parameter("ContactID", contactId, DbType.Int32));
        }
    }
}