using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    public interface IEmailDatastore
    {
        /// <summary>
        /// Return all Emails who belong to the Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Email</param>
        /// <returns></returns>
        IDataReader FetchAll(int contactId);

        /// <summary>
        /// Return an Email with Id = id
        /// </summary>
        /// <param name="contactId">Id of Email</param>
        /// <returns></returns>
        IDataReader Fetch(int id);

        /// <summary>
        /// Insert one Email
        /// </summary>
        /// <param name="contactPointId"></param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        /// <returns>Fetched @@IDENTITY</returns>
        IDataReader Insert(int contactPointId, 
                         string value, int contactId,
                         int contactPointPurposeId, bool isPrimary,
                         int revisionNumber);

        /// <summary>
        /// Update one Email with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Email</param>
        /// <param name="value"></param>
        /// <param name="contactId"></param>
        /// <param name="contactPointPurposeId"></param>
        /// <param name="isPrimary"></param>
        /// <param name="revisionNumber"></param>
        void Update(int contactPointId, string value,
                         int contactId, int contactPointPurposeId, 
                         bool isPrimary, int revisionNumber);

        /// <summary>
        /// Delete one Email with Id = contactPointId
        /// </summary>
        /// <param name="contactPointId">Id of Email</param>
        void Delete(int contactPointId);

        /// <summary>
        /// Delete all Emails that belong to Contact with Id = contactId
        /// </summary>
        /// <param name="contactId">Id of parent Contact not of Email</param>
        void DeleteAll(int contactId);
    }
}