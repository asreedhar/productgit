using System.Data;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores
{
    internal class PhoneLineTypeDatastore : Datastore, IPhoneLineTypeDatastore
    {
        public IDataReader PhoneLineTypes()
        {
            const string queryName = Prefix + ".PhoneLineTypeDatastore_PhoneLineTypes.txt";
            return Query(new[] { "PhoneLineTypes" },
                         queryName);
        }
    }
}