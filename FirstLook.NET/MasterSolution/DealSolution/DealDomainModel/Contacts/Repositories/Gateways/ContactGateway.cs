﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    internal class ContactGateway : GatewayBase
    {
        private IContactDatastore _datastore;
        private ISerializer<Contact> _serializer;

        public ContactGateway() : base()
        {
            _datastore = Resolve<IContactDatastore>();
            _serializer = Resolve<ISerializer<Contact>>();
        }

        public Contact Fetch(int contactId, int clientId)
        {
            using (IDataReader reader = _datastore.Contact_Fetch(contactId, clientId))
            {
                if (!reader.Read())
                {
                    return null;
                }
                return _serializer.Deserialize((IDataRecord) reader);
            }
        }

        public Contact Insert(Contact contact, int clientId)
        {
            using (IDataReader reader = _datastore.Contact_Insert(contact.RevisionNumber, clientId))
            {
                if (!reader.Read())
                {
                    throw new Exception("ContactGateway.Insert - Error inserting contact");
                }
                return _serializer.Deserialize((IDataRecord)reader);
            }
        }

        public bool Update(Contact contact, int clientId)
        {
            return _datastore.Contact_Update(contact.Id, clientId, contact.RevisionNumber);
        }

        public void Delete(Contact contact, int clientId)
        {
            _datastore.Contact_Delete(contact.Id, clientId);
        }

        public ContactEntity Contact_Save(int id)
        {
            IContactDatastore datastore = Resolve<IContactDatastore>();
            ContactEntity entity = null;
            
            datastore.Save(id);

            return entity;
        }

        public ContactEntity Contact_Delete(int id)
        {
            IContactDatastore datastore = Resolve<IContactDatastore>();

            datastore.Delete(id);
            return null;
        }
    }
}
