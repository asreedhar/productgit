using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    internal class ContactPointPurposeGateway : GatewayBase
    {
        private IContactDatastore _datastore;
        private ISerializer<ContactPointPurpose> _serializer;

        public ContactPointPurposeGateway() : base()
        {
            _datastore = Resolve<IContactDatastore>();
            _serializer = Resolve<ISerializer<ContactPointPurpose>>();
        }

        public IList<ContactPointPurpose> ContactPointPurposes()
        {
            using (IDataReader reader = _datastore.ContactPointPurpose_ContactPointPurposes())
            {
                return _serializer.Deserialize(reader);
            }
        }
    }
}