using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    internal class PhoneLineTypeGateway : GatewayBase
    {
        private IContactDatastore _datastore;
        private ISerializer<PhoneLineType> _serializer;

        public PhoneLineTypeGateway() : base()
        {
            _datastore = Resolve<IContactDatastore>();
            _serializer = Resolve<ISerializer<PhoneLineType>>();
        }

        public IList<PhoneLineType> PhoneLineTypes()
        {
            using (IDataReader reader = _datastore.PhoneLineType_PhoneLineTypes())
            {
                return _serializer.Deserialize(reader);
            }
        }
    }
}