﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Entities;
using System.Collections.Generic;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    class EmailsGateway : AuditRowGateway
    {
        public IList<EmailEntity> Emails_Fetch(int id)
        {
            IEmailDatastore emailsDatastore = Resolve<IEmailsDatastore>();
            IList<EmailEntity> entities = null;
            

            using (IDataReader reader = emailsDatastore.Fetch(id))
            {
                while (reader.Read())
                {
                    if (entities == null)
                    {
                        entities = new List<EmailEntity>();
                    }
                    ISerializer<EmailEntity> serializer = Resolve<ISerializer<EmailEntity>>();
                    entities.Add(serializer.Deserialize((IDataRecord) reader));
                }
            }

            return entities;
        }

        public ContactEntity Contact_Save(int id)
        {
            IContactDatastore datastore = Resolve<IContactDatastore>();
            ContactEntity entity = null;
            
            datastore.Save(id);

            return entity;
        }

        public ContactEntity Contact_Delete(int id)
        {
            IContactDatastore datastore = Resolve<IContactDatastore>();

            datastore.Delete(id);
            return null;
        }
    }
}
