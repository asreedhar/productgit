using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    internal class EmailGateway : ContactPointGateway<Email>
    {
        private IContactDatastore _datastore;

        public EmailGateway() : base(1)
        {
            _datastore = Resolve<IContactDatastore>();
        }

        protected override Func<IDataReader> DatastoreFetchAllFunction(int contactId)
        {
            return () => _datastore.Email_FetchAll(contactId);
        }

        protected override Func<IDataReader> DatastoreFetchFunction(int id)
        {
            return () => _datastore.Email_Fetch(id);
        }

        protected override Func<IDataReader> DatastoreInsertFunction(Email email, int contactPointId)
        {
            return () => _datastore.Email_Insert(contactPointId, email.Value);
        }

        protected override Action DatastoreUpdateFunction(Email email)
        {
            return () => _datastore.Email_Update(email.Id, email.Value);
        }

        protected override Action DatastoreDeleteFunction(Email email)
        {
            return () => _datastore.Email_Delete(email.Id);
        }

        protected override Action DatastoreDeleteAllFunction(int contactId)
        {
            return () => _datastore.Email_DeleteAll(contactId);
        }
    }
}