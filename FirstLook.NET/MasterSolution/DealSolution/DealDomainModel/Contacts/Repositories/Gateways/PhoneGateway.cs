using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    internal class PhoneGateway : ContactPointGateway<Phone>
    {
        private IContactDatastore _datastore;

        public PhoneGateway() : base(2)
        {
            _datastore = Resolve<IContactDatastore>();
        }

        protected override Func<IDataReader> DatastoreFetchAllFunction(int contactId)
        {
            return () => _datastore.Phone_FetchAll(contactId);
        }

        protected override Func<IDataReader> DatastoreFetchFunction(int id)
        {
            return () => _datastore.Phone_Fetch(id);
        }

        protected override Func<IDataReader> DatastoreInsertFunction(Phone phone, int contactPointId)
        {
            return () => _datastore.Phone_Insert(contactPointId, phone.PhoneLineType.Id,
                                                 phone.CountryCode, phone.AreaCode,
                                                 phone.CentralOfficeCode,
                                                 phone.SubscriberNumber,
                                                 phone.Extension);
        }

        protected override Action DatastoreUpdateFunction(Phone phone)
        {
            return () => _datastore.Phone_Update(phone.Id, phone.PhoneLineType.Id,
                                                 phone.CountryCode, phone.AreaCode,
                                                 phone.CentralOfficeCode, phone.SubscriberNumber, phone.Extension);
        }

        protected override Action DatastoreDeleteFunction(Phone phone)
        {
            return () => _datastore.Phone_Delete(phone.Id);
        }

        protected override Action DatastoreDeleteAllFunction(int contactId)
        {
            return () => _datastore.Phone_DeleteAll(contactId);
        }
    }
}