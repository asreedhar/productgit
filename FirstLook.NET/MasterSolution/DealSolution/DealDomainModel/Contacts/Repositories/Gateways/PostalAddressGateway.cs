using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    internal class PostalAddressGateway : ContactPointGateway<PostalAddress>
    {
        private IContactDatastore _datastore;

        public PostalAddressGateway() : base(3)
        {
            _datastore = Resolve<IContactDatastore>();
        }

        protected override Func<IDataReader> DatastoreFetchAllFunction(int contactId)
        {
            return () => _datastore.PostalAddress_FetchAll(contactId);
        }

        protected override Func<IDataReader> DatastoreFetchFunction(int id)
        {
            return () => _datastore.PostalAddress_Fetch(id);
        }

        protected override Func<IDataReader> DatastoreInsertFunction(PostalAddress postalAddress, 
            int contactPointId)
        {
            return () => _datastore.PostalAddress_Insert(contactPointId, postalAddress.Line1,
                                                         postalAddress.Line2,
                                                         postalAddress.City,
                                                         postalAddress.State, postalAddress.Zip,
                                                         postalAddress.ZipPlus4);

        }

        protected override Action DatastoreUpdateFunction(PostalAddress postalAddress)
        {
            return () => _datastore.PostalAddress_Update(postalAddress.Id,
                                                         postalAddress.Line1, postalAddress.Line2,
                                                         postalAddress.City, postalAddress.State, postalAddress.Zip,
                                                         postalAddress.ZipPlus4);
        }

        protected override Action DatastoreDeleteFunction(PostalAddress postalAddress)
        {
            return () => _datastore.PostalAddress_Delete(postalAddress.Id);
        }

        protected override Action DatastoreDeleteAllFunction(int contactId)
        {
            return () => _datastore.PostalAddress_DeleteAll(contactId);
        }
    }
}