using System;
using System.Collections;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Datastores;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways
{
    internal abstract class ContactPointGateway<T> : GatewayBase where T : ContactPoint
    {
        private IContactDatastore _datastore;
        private int _contactPointTypeId;
        private ISerializer<T> _childSerializer;
        private ISerializer<ContactPointEntity> _serializer;

        protected abstract Func<IDataReader> DatastoreFetchAllFunction(int contactId);
        protected abstract Func<IDataReader> DatastoreFetchFunction(int id);
        protected abstract Func<IDataReader> DatastoreInsertFunction(T t, int contactPointId);
        protected abstract Action DatastoreUpdateFunction(T t);
        protected abstract Action DatastoreDeleteFunction(T t);
        protected abstract Action DatastoreDeleteAllFunction(int contactId);

        public ContactPointGateway(int contactPointTypeId)
        {
            _datastore = Resolve<IContactDatastore>();
            _contactPointTypeId = contactPointTypeId;
            _childSerializer = Resolve<ISerializer<T>>();
            _serializer = Resolve<ISerializer<ContactPointEntity>>();
        }

        public IList<T> FetchAll(int contactId)
        {
            using (IDataReader reader = DatastoreFetchAllFunction(contactId)())
            {
                return _childSerializer.Deserialize(reader);
            }
        }

        public T Fetch(int id)
        {
            T t = null;
            using (IDataReader reader = DatastoreFetchFunction(id)())
            {
                if (!reader.Read())
                {
                    return null;
                }

                t = _childSerializer.Deserialize((IDataRecord)reader);
            }

            return t;
        }

        public T Insert(T t, int contactId)
        {
            ContactPointEntity contactPoint = null;
            using (IDataReader reader = _datastore.ContactPoint_Insert(contactId, _contactPointTypeId,
                t.ContactPointPurpose.Id, t.IsPrimary, t.RevisionNumber))
            {
                if (!reader.Read())
                {
                    throw new Exception(string.Format("{0}Gateway.Insert - Could not insert into ContactPoint", typeof(T).Name));
                }
                IDataRecord record = ((IDataRecord) reader);
                contactPoint = _serializer.Deserialize(record);
            }

            T newT = null;
            using (IDataReader reader = DatastoreInsertFunction(t, contactPoint.Id)())
            {
                if (!reader.Read())
                {
                    throw new Exception(string.Format("{0}Gateway.Insert - Error inserting {0}", typeof(T).Name));
                }
                newT = _childSerializer.Deserialize((IDataRecord)reader);
            }
            
            return newT;
        }

        public void Update(T t, int contactId)
        {
            _datastore.ContactPoint_Update(t.Id, contactId, _contactPointTypeId,
                                           t.ContactPointPurpose.Id, t.IsPrimary, t.RevisionNumber);
            DatastoreUpdateFunction(t)();
        }

        public void Delete (T t)
        {
            DatastoreDeleteFunction(t)();
        }

        public void DeleteAll(int contactId)
        {
            DatastoreDeleteAllFunction(contactId)();
        }
    }
}