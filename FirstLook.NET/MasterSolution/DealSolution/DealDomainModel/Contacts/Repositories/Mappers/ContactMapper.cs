﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Gateways;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Mappers
{
    internal class ContactMapper : GatewayBase
    {
        ContactGateway _contactGateway = new ContactGateway();
        EmailGateway _emailGateway = new EmailGateway();
        PhoneGateway _phoneGateway = new PhoneGateway();
        PostalAddressGateway _postalAddressGateway = new PostalAddressGateway();

        public Contact Fetch(int id, int clientId)
        {
            string cacheKey = GetCacheKey(id);
            Contact cachedContact = Cache.Get(cacheKey) as Contact;

            if (cachedContact != null)
            {
                Contact currentPartialContact = _contactGateway.Fetch(id, clientId);
                if (currentPartialContact != null)
                {
                    if (cachedContact.RevisionNumber == currentPartialContact.RevisionNumber)
                    {
                        return (Contact)cachedContact.Clone();
                    }
                }
                else
                {
                    RemoveCache(cachedContact);
                    return null;
                }
            }
            
            Contact contact = _contactGateway.Fetch(id, clientId);
            if (contact == null)
            {
                return null;
            }

            FetchContactPoints<Email>(contact, contact.Emails, _emailGateway);
            FetchContactPoints<Phone>(contact, contact.Phones, _phoneGateway);
            FetchContactPoints<PostalAddress>(contact, contact.PostalAddresses, _postalAddressGateway);
            ((IPersisted)contact).MarkOld();

            UpdateCache((Contact)contact.Clone());

            return contact;
        }

        public Contact Save(Contact contact, int clientId)
        {
            // savedContact starts out as the
            // contact as is in the database and
            // then will eventually be merged into
            // the destination contact and at the same pace
            // as the database will reflect the savedContact

            if (contact == null || !contact.IsValid)
            {
                throw new ArgumentException("Invalid Contact", "contact");
            }

            Contact savedContact = Fetch(contact.Id, clientId);
            Contact destinationContact = Merge(savedContact, contact);
            IPersisted persistedContact = (IPersisted)destinationContact;

            if (savedContact == null)
            {// Insert
                savedContact = _contactGateway.Insert(destinationContact, clientId);
            }
            else
            {// Update or Delete

                if (!persistedContact.IsDirty)
                {
                    return destinationContact;
                }

                if (!_contactGateway.Update(destinationContact, clientId))
                {
                    throw new ConcurrentModificationException("Concurrent modification failure.");
                }
            }

            SaveContactPoint<Email>(destinationContact, savedContact, destinationContact.Emails, 
                savedContact.Emails, _emailGateway);

            SaveContactPoint<Phone>(destinationContact, savedContact, destinationContact.Phones,
                savedContact.Phones, _phoneGateway);

            SaveContactPoint<PostalAddress>(destinationContact, savedContact, destinationContact.PostalAddresses,
                savedContact.PostalAddresses, _postalAddressGateway);

            if(persistedContact.IsDeleted)
            {
                _contactGateway.Delete(destinationContact, clientId);
                RemoveCache(savedContact);
            }
            else
            {
                UpdateCache((Contact)savedContact.Clone());
            }

            ((IPersisted)savedContact).MarkOld();
            return savedContact;
        }

        /// <summary>
        /// Merge source into destination
        /// </summary>
        /// <param name="sourceContact">Call fetch</param>
        /// <param name="inputContact">Input from Command Layer</param>
        /// <returns></returns>
        public Contact Merge(Contact sourceContact, Contact inputContact)
        {
            if (sourceContact == null)
            {
                return inputContact;
            }

            Contact destinationContact = (Contact)sourceContact.Clone();

            if (!((IPersisted)inputContact).IsDeleted)
            {
                destinationContact.Id = inputContact.Id;
                destinationContact.RevisionNumber = inputContact.RevisionNumber;
            }

            MergeContactPoints<Email>(destinationContact, inputContact, destinationContact.Emails,
                                      inputContact.Emails);

            MergeContactPoints<Phone>(destinationContact, inputContact, destinationContact.Phones,
                                      inputContact.Phones);

            MergeContactPoints<PostalAddress>(destinationContact, inputContact, destinationContact.PostalAddresses,
                                      inputContact.PostalAddresses);

            return destinationContact;
        }

        private void MergeContactPoints<T>(Contact destinationContact, Contact inputContact,
            IList<T> destinationList, IList<T> inputList) where T : ContactPoint
        {
            if (((IPersisted)inputContact).IsDeleted)
            {
                destinationList.Clear();
                foreach (T t in inputList)
                {
                    ((IPersisted)t).MarkDeleted();
                    destinationList.Add(t);
                }
                return;
            }

            IDictionary<int, T> inputContactPoints = new Dictionary<int, T>();
            foreach (T t in inputList)
            {
                inputContactPoints.Add(t.Id, t);
            }

            Dictionary<int, byte> destinationContactPoints = new Dictionary<int, byte>();
            for (int i = destinationList.Count - 1; i >= 0; i--)
            {
                T t = destinationList[i];
                destinationContactPoints.Add(t.Id, 1);
                if (inputContactPoints.ContainsKey(t.Id))
                {
                    // update
                    destinationList[i] = inputContactPoints[t.Id];
                }
                else
                {
                    // delete
                    destinationList.RemoveAt(i);
                }
            }

            foreach (T t in inputList)
            {
                if (!destinationContactPoints.ContainsKey(t.Id))
                {
                    // insert
                    destinationList.Add(t);
                }
            }
        }

        private string GetCacheKey(int uniqueId)
        {
            return string.Format("{0}_{1}", this.GetType().FullName, uniqueId);
        }

        private void UpdateCache(Contact contact)
        {
            string cacheKey = GetCacheKey(contact.Id);

            Forget(cacheKey);
            Remember(cacheKey, contact);
        }

        private void RemoveCache(Contact contact)
        {
            string cacheKey = GetCacheKey(contact.Id);
            Forget(cacheKey);
        }

        private void FetchContactPoints<T>(Contact contact, IList<T> contactList,
            ContactPointGateway<T> gateway) where T : ContactPoint
        {
            IList<T> tList = gateway.FetchAll(contact.Id);
            foreach (T t in tList)
            {
                ((IPersisted)t).MarkOld();
                contactList.Add(t);
            }
        }

        private void SaveContactPoint<T>(Contact contact, Contact savedContact, 
            IList<T> contactList, IList<T> savedList, ContactPointGateway<T> gateway) where T : ContactPoint
        {
            if (contact == null || !contact.IsValid)
            {
                throw new ArgumentException("Invalid Contact", "contact");
            }
            
            if (savedContact == null)
            {
                return;
            }

            if (((IPersisted)contact).IsDeleted)
            {
                gateway.DeleteAll(savedContact.Id);
                savedList.Clear();
                contactList.ToList().ForEach(
                    t =>
                    {
                        T copyT = (T) t.Clone();
                        ((IPersisted) copyT).MarkOld();

                        savedList.Add(copyT);
                    });

                return;
            }

            IDictionary<int, T> currentContactPoints = new Dictionary<int, T>();
            foreach (T t in contactList)
            {
                if (!t.IsValid)
                {
                    throw new ArgumentException(string.Format("Invalid {0}", t.GetType().Name), "t");
                }
                currentContactPoints.Add(t.Id, t);
            }

            Dictionary<int, byte> previousContactPoints = new Dictionary<int, byte>();
            for (int i = savedList.Count - 1; i >= 0; i--)
            {
                T t = savedList[i];
                previousContactPoints.Add(t.Id, 1);
                if (currentContactPoints.ContainsKey(t.Id))
                {
                    if (((IPersisted)t).IsDirty)
                    {
                        // update
                        gateway.Update(t, savedContact.Id);
                        ((IPersisted)t).MarkOld();
                        savedList[i] = currentContactPoints[t.Id];
                    }
                }
                else
                {
                    // delete
                    gateway.Delete(t);
                    savedList.RemoveAt(i);
                }
            }

            foreach (T t in contactList)
            {
                if (!previousContactPoints.ContainsKey(t.Id))
                {
                    // insert
                    savedList.Add(gateway.Insert(t, savedContact.Id));
                    ((IPersisted)t).MarkOld();
                }
            }
        }
    }
}
