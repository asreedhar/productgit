﻿using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();
            registry.Register<Serializers.Module>();
            registry.Register<IContactRepository, ContactRepository>(ImplementationScope.Shared);
        }

        #endregion
    }
}