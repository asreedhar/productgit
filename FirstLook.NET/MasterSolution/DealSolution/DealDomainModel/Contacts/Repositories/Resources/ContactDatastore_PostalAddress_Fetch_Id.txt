﻿SELECT
	PA.Line1, PA.Line2, PA.City, PA.State, PA.Zip, PA.ZipPlus4,
	PA.ContactPointID, CPP.ContactPointPurposeID, 
	CPP.Name AS [ContactPointPurpose_Name], 
	CP.RevisionNumber, CP.IsPrimary,
	CP.ContactPointTypeID,
FROM Contact.PostalalAddress PA 
JOIN Contact.ContactPoint CP ON PA.ContactPointID = CP.ContactPointID 
	 AND CP.ContactPointTypeID = 3
	 AND CP.ContactPointID = @ContactPointID
JOIN Contact.ContactPointPurpose CPP ON = CP.ContactPointPurposeID = CPP.ContactPointPurposeID
