﻿SELECT [ContactPointID]
      ,[ContactPointTypeID]
      ,[ContactID]
      ,[ContactPointPurposeID]
      ,[IsPrimary]
      ,[RevisionNumber]
  FROM [Contact].[ContactPoint]
WHERE ContactPointID = @@IDENTITY