﻿using System.Data;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Serializers
{
    internal class EmailSerializer : ContactPointSerializer<Email>
    {
        public override Email Deserialize(IDataRecord record)
        {
            Email returnEmail = base.Deserialize(record);
            returnEmail.Value = record.GetString(record.GetOrdinal("Value"));
            return returnEmail;
        }
    }
}
