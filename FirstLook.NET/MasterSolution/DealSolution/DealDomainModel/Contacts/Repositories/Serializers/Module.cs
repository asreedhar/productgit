﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<Contact>, ContactSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Email>, EmailSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Phone>, PhoneSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<PostalAddress>, PostalAddressSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<ContactPointPurpose>, ContactPointPurposeSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<PhoneLineType>, PhoneLineTypeSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<ContactPointEntity>, ContactPointEntitySerializer>(ImplementationScope.Shared);
        }
    }
}
