﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Serializers
{
    public class PhoneLineTypeSerializer : Serializer<PhoneLineType>
    {

        private int CastTinyInt(IDataRecord record, string columnName)
        {
            // Was having trouble casting tiny int to a byte
            // errors thrown when doing so. Sometimes int32 works
            // other times int16 works
            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(Byte))
            {
                return record.GetByte(record.GetOrdinal(columnName));
            }

            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(Int16))
            {
                return record.GetInt16(record.GetOrdinal(columnName));
            }

            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(Int32))
            {
                return record.GetInt32(record.GetOrdinal(columnName));
            }

            throw new DataException("Unable to cast tinyint");
        }

        public override PhoneLineType Deserialize(IDataRecord record)
        {
            return new PhoneLineType
            {
                Id = CastTinyInt(record, "PhoneLineTypeID"),
                Name = record.GetString(record.GetOrdinal("Name")),
            };
        }
    }
}
