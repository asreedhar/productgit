using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Contacts.Repositories.Entities;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Serializers
{
    internal class ContactPointEntitySerializer : Serializer<ContactPointEntity>
    {
        protected int CastTinyInt(IDataRecord record, string columnName)
        {
            // Was having trouble casting tiny int to a byte
            // errors thrown when doing so. Sometimes int32 works
            // other times int16 works
            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(byte))
            {
                return record.GetByte(record.GetOrdinal(columnName));
            }

            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(short))
            {
                return record.GetInt16(record.GetOrdinal(columnName));
            }

            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(int))
            {
                return record.GetInt32(record.GetOrdinal(columnName));
            }

            throw new DataException("Unable to cast tinyint");
        }

        public override ContactPointEntity Deserialize(IDataRecord record)
        {
            ISerializer<ContactPointPurpose> contactPointPurposeSerializer =
                RegistryFactory.GetResolver().Resolve<ISerializer<ContactPointPurpose>>();

            return new ContactPointEntity
            {
                ContactPointPurposeId = CastTinyInt(record, "ContactPointPurposeID"),
                RevisionNumber = CastTinyInt(record, "RevisionNumber"),
                Id = record.GetInt32(record.GetOrdinal("ContactPointID")),
                IsPrimary = record.GetBoolean(record.GetOrdinal("IsPrimary")),
            };
        }
    }
}