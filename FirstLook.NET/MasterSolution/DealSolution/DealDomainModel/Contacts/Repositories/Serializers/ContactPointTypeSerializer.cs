﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Serializers
{
    public class ContactPointTypeSerializer : Serializer<ContactPointType>
    {

        private int CastTinyInt(IDataRecord record, string columnName)
        {
            // Was having trouble casting tiny int to a byte
            // errors thrown when doing so. Sometimes int32 works
            // other times int16 works
            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(System.Byte))
            {
                return record.GetByte(record.GetOrdinal(columnName));
            }

            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(System.Int16))
            {
                return record.GetInt16(record.GetOrdinal(columnName));
            }

            if (record.GetFieldType(record.GetOrdinal(columnName)) == typeof(System.Int32))
            {
                return record.GetInt32(record.GetOrdinal(columnName));
            }

            throw new DataException("Unable to cast tinyint");
        }

        public override ContactPointType Deserialize(IDataRecord record)
        {
            return new ContactPointType
            {
                Id = CastTinyInt(record, "ContactPointTypeID"),
                Name = record.GetString(record.GetOrdinal("Name")),
            };
        }
    }
}
