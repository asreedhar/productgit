﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Serializers
{
    internal class PhoneSerializer : ContactPointSerializer<Phone>
    {
        public override Phone Deserialize(IDataRecord record)
        {
            ISerializer<PhoneLineType> phoneLineTypeSerializer =
                RegistryFactory.GetResolver().Resolve<ISerializer<PhoneLineType>>();

            Phone returnPhone = base.Deserialize(record);
            returnPhone.PhoneLineType = phoneLineTypeSerializer.Deserialize(record);
            returnPhone.AreaCode = record.GetString(record.GetOrdinal("AreaCode"));
            returnPhone.CountryCode = record.GetString(record.GetOrdinal("CountryCode"));
            returnPhone.CentralOfficeCode = record.GetString(record.GetOrdinal("CentralOfficeCode"));
            returnPhone.SubscriberNumber = record.GetString(record.GetOrdinal("SubscriberNumber"));
            returnPhone.Extension = record.GetString(record.GetOrdinal("Extension"));

            return returnPhone;
        }
    }
}
