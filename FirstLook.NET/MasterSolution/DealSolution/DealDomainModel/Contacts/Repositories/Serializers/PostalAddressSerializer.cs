﻿using System.Data;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Serializers
{
    internal class PostalAddressSerializer : ContactPointSerializer<PostalAddress>
    {
        public override PostalAddress Deserialize(IDataRecord record)
        {
            PostalAddress returnPostalAddress = base.Deserialize(record);
            returnPostalAddress.Line1 = record.GetString(record.GetOrdinal("Line1"));
            returnPostalAddress.Line2 = record.GetString(record.GetOrdinal("Line2"));
            returnPostalAddress.City = record.GetString(record.GetOrdinal("City"));
            returnPostalAddress.State = record.GetString(record.GetOrdinal("State"));
            returnPostalAddress.Zip = record.GetString(record.GetOrdinal("Zip"));
            returnPostalAddress.ZipPlus4 = record.GetString(record.GetOrdinal("ZipPlus4"));
            return returnPostalAddress;
        }
    }
}
