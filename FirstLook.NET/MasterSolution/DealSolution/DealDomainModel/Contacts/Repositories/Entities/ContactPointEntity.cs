using System;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Repositories.Entities
{
    [Serializable]
    public class ContactPointEntity
    {
        public int RevisionNumber { get; set; }
        public int Id { get; set; }
        public int ContactPointPurposeId { get; set; }
        public bool IsPrimary { get; set; }
    }
}