﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands
{
    public interface ICommandFactory
    {
        ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand();

        ICommand<FetchContactResultsDto, IIdentityContextDto<FetchContactArgumentsDto>> CreateFetchContactCommand();

        ICommand<SaveContactResultsDto, IIdentityContextDto<SaveContactArgumentsDto>> CreateSaveContactCommand();

        ICommand<DeleteContactResultsDto, IIdentityContextDto<DeleteContactArgumentsDto>> CreateDeleteContactCommand();
    }
}
