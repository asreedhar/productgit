﻿using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Commands.Impl;

namespace FirstLook.Deal.DomainModel.Contacts.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}