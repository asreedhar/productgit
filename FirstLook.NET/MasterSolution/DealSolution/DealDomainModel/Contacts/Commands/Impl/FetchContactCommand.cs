﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.Impl
{
    public class FetchContactCommand : ICommand<FetchContactResultsDto, IIdentityContextDto<FetchContactArgumentsDto>>
    {
        public FetchContactResultsDto Execute(IIdentityContextDto<FetchContactArgumentsDto> parameters)
        {
            IContactRepository repository = RegistryFactory.GetResolver().Resolve<IContactRepository>();
            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, parameters.Arguments.Broker);
            
            Contact contact = repository.Fetch(broker, parameters.Arguments.Id);

            return new FetchContactResultsDto
            {
                Arguments = parameters.Arguments,
                Contact = Mapper.Map(contact),
            };
        }
    }
}
