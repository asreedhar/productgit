﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand()
        {
            return new ReferenceInformationCommand();
        }

        public ICommand<FetchContactResultsDto, IIdentityContextDto<FetchContactArgumentsDto>> CreateFetchContactCommand()
        {
            return new FetchContactCommand();
        }

        public ICommand<SaveContactResultsDto, IIdentityContextDto<SaveContactArgumentsDto>> CreateSaveContactCommand()
        {
            return new SaveContactCommand();
        }

        public ICommand<DeleteContactResultsDto, IIdentityContextDto<DeleteContactArgumentsDto>> CreateDeleteContactCommand()
        {
            return new DeleteContactCommand();
        }
    }
}
