﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Component;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.Impl
{
    class Mapper
    {
        public static PhoneDto Map(Phone phone)
        {
            if (phone == null)
            {
                return null;
            }

            return new PhoneDto
            {
                PhoneLineType = Map(phone.PhoneLineType),
                AreaCode = phone.AreaCode,
                CentralOfficeCode = phone.CentralOfficeCode,
                ContactPointPurpose = Map(phone.ContactPointPurpose),
                CountryCode = phone.CountryCode,
                Extension = phone.Extension,
                Id = phone.Id,
                RevisionNumber = phone.RevisionNumber,
                IsPrimary = phone.IsPrimary,
                SubscriberNumber = phone.SubscriberNumber,

            };
        }

        public static Phone Map(PhoneDto phone)
        {
            if (phone == null)
            {
                return null;
            }

            return new Phone
            {
                PhoneLineType = Map(phone.PhoneLineType),
                AreaCode = phone.AreaCode,
                CentralOfficeCode = phone.CentralOfficeCode,
                ContactPointPurpose = Map(phone.ContactPointPurpose),
                CountryCode = phone.CountryCode,
                Extension = phone.Extension,
                Id = phone.Id,
                RevisionNumber = phone.RevisionNumber,
                IsPrimary = phone.IsPrimary,
                SubscriberNumber = phone.SubscriberNumber,

            };
        }

        private static PhoneLineTypeDto Map(PhoneLineType phoneLineType)
        {
            if (phoneLineType == null)
            {
                return null;
            }

            return new PhoneLineTypeDto
            {
                Id = phoneLineType.Id,
                Name = phoneLineType.Name,
            };
        }

        private static PhoneLineType Map(PhoneLineTypeDto phoneLineType)
        {
            if (phoneLineType == null)
            {
                return null;
            }

            return new PhoneLineType
            {
                Id = phoneLineType.Id,
                Name = phoneLineType.Name,
            };
        }

        public static EmailDto Map(Email email)
        {
            if (email==null)
            {
                return null;
            }

            return new EmailDto
            {
                Value = email.Value,
                Id = email.Id,
                ContactPointPurpose = Map(email.ContactPointPurpose),
                IsPrimary = email.IsPrimary,
                RevisionNumber = email.RevisionNumber,
            };
        }

        public static Email Map(EmailDto email)
        {
            if (email == null)
            {
                return null;
            }

            return new Email
            {
                Value = email.Value,
                Id = email.Id,
                ContactPointPurpose = Map(email.ContactPointPurpose),
                IsPrimary = email.IsPrimary,
                RevisionNumber = email.RevisionNumber,
            };
        }

        private static ContactPointPurposeDto Map(ContactPointPurpose contactPointPurpose)
        {
            if (contactPointPurpose==null)
            {
                return null;
            }

            return new ContactPointPurposeDto
            {
                Id = contactPointPurpose.Id,
                Name = contactPointPurpose.Name,
            };
        }

        private static ContactPointPurpose Map(ContactPointPurposeDto contactPointPurpose)
        {
            if (contactPointPurpose == null)
            {
                return null;
            }

            return new ContactPointPurpose
            {
                Id = contactPointPurpose.Id,
                Name = contactPointPurpose.Name,
            };
        }

        public static PostalAddressDto Map(PostalAddress postalAddress)
        {
            if (postalAddress == null)
            {
                return null;
            }

            return new PostalAddressDto
            {
                Line1 = postalAddress.Line1,
                Line2 = postalAddress.Line2,
                Id=postalAddress.Id,
                City = postalAddress.City,
                ContactPointPurpose=Map(postalAddress.ContactPointPurpose),
                IsPrimary=postalAddress.IsPrimary,
                RevisionNumber=postalAddress.RevisionNumber,
                State=postalAddress.State,
                Zip=postalAddress.Zip,
                ZipPlus4=postalAddress.ZipPlus4,
            };
        }

        public static PostalAddress Map(PostalAddressDto postalAddress)
        {
            if (postalAddress == null)
            {
                return null;
            }

            return new PostalAddress
            {
                Line1 = postalAddress.Line1,
                Line2 = postalAddress.Line2,
                Id = postalAddress.Id,
                City = postalAddress.City,
                ContactPointPurpose = Map(postalAddress.ContactPointPurpose),
                IsPrimary = postalAddress.IsPrimary,
                RevisionNumber = postalAddress.RevisionNumber,
                State = postalAddress.State,
                Zip = postalAddress.Zip,
                ZipPlus4 = postalAddress.ZipPlus4,
            };
        }

        public static List<EmailDto> Map(IList<Email> emails)
        {
            if (emails == null)
            {
                return null;
            }

            return emails.Select(Map).ToList();
        }

        public static IList<Email> Map(List<EmailDto> emailDtos)
        {
            if (emailDtos == null)
            {
                return null;
            }

            return emailDtos.Select(Map).ToList();
        }

        public static List<PhoneDto> Map(IList<Phone> phones)
        {
            if (phones == null)
            {
                return null;
            }

            return phones.Select(Map).ToList();
        }

        public static IList<Phone> Map(List<PhoneDto> phoneDtos)
        {
            if (phoneDtos == null)
            {
                return null;
            }

            return phoneDtos.Select(Map).ToList();
        }

        public static List<PostalAddressDto> Map(IList<PostalAddress> postalAddresses)
        {
            if (postalAddresses == null)
            {
                return null;
            }

            return postalAddresses.Select(Map).ToList();
        }

        public static IList<PostalAddress> Map(List<PostalAddressDto> postalAddressDtos)
        {
            if (postalAddressDtos == null)
            {
                return null;
            }

            return postalAddressDtos.Select(Map).ToList();
        }

        public static ContactDto Map(Contact contact)
        {
            if (contact == null)
            {
                return null;
            }

            return new ContactDto
            {
                Emails = Map(contact.Emails),
                Id = contact.Id,
                Phones = Map(contact.Phones),
                PostalAddresses = Map(contact.PostalAddresses),
                RevisionNumber = contact.RevisionNumber,
            };
        }

        public static Contact Map(ContactDto contact)
        {
            if (contact == null)
            {
                return null;
            }

            Contact dto = new Contact
            {
                Id = contact.Id,
                RevisionNumber = contact.RevisionNumber
            };

            if (contact.Emails != null)
            {
            foreach (EmailDto email in contact.Emails)
                {
                dto.Emails.Add(Map(email));
                }
            }

            if (contact.Phones != null)
            {
                foreach (PhoneDto phone in contact.Phones)
                {
                dto.Phones.Add(Map(phone));
                }
            }

            if (contact.PostalAddresses != null)
            {
                foreach (PostalAddressDto postalAddress in contact.PostalAddresses)
                {
                dto.PostalAddresses.Add(Map(postalAddress));
                }
            }

            return dto;
        }

        public static List<ContactPointPurposeDto> Map(IList<ContactPointPurpose> contactPointPurposes)
        {
            return contactPointPurposes.Select(
                contactPointPurpose => new ContactPointPurposeDto
                {
                    Id = contactPointPurpose.Id,
                    Name = contactPointPurpose.Name,
                }).ToList();
        }

        public static List<PhoneLineTypeDto> Map(IList<PhoneLineType> phoneLineTypes)
        {
            return phoneLineTypes.Select(
                phoneLineType => new PhoneLineTypeDto
                {
                    Id = phoneLineType.Id,
                    Name = phoneLineType.Name,
                }).ToList();
        }
    }
}
