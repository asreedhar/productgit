﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.Impl
{
    public class SaveContactCommand : ICommand<SaveContactResultsDto, IIdentityContextDto<SaveContactArgumentsDto>>
    {
        public SaveContactResultsDto Execute(IIdentityContextDto<SaveContactArgumentsDto> parameters)
        {
            IContactRepository repository = RegistryFactory.GetResolver().Resolve<IContactRepository>();
            IdentityDto identity = parameters.Identity;
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, parameters.Arguments.Broker);

            return new SaveContactResultsDto
            {
                Arguments = parameters.Arguments,
                Contact = Mapper.Map(repository.Save(broker, Mapper.Map(parameters.Arguments.Contact))),
            };

        }
    }
}
