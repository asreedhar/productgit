﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.Impl
{
    public class ReferenceInformationCommand : ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto>
    {
        public ReferenceInformationResultsDto Execute(ReferenceInformationArgumentsDto parameters)
        {
            IContactRepository repository = RegistryFactory.GetResolver().Resolve<IContactRepository>();

            return new ReferenceInformationResultsDto
            {
                Arguments = parameters,
                ContactPointPurposes = Mapper.Map(repository.ContactPointPurposes()),
                PhoneLineTypes = Mapper.Map(repository.PhoneLineTypes()),
            };
        }
    }
}
