﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.Impl
{
    public class DeleteContactCommand : ICommand<DeleteContactResultsDto, IIdentityContextDto<DeleteContactArgumentsDto>>
    {
        public DeleteContactResultsDto Execute(IIdentityContextDto<DeleteContactArgumentsDto> parameters)
        {
            IContactRepository repository = RegistryFactory.GetResolver().Resolve<IContactRepository>();
            bool success = false;
            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, parameters.Arguments.Broker);
            
            try
            {
                repository.Delete(broker, 
                    new Contact{ Id=parameters.Arguments.Id, RevisionNumber = parameters.Arguments.RevisionNumber});
                success = true;
            }
            catch
            {
                success = false;
            }

            return new DeleteContactResultsDto
            {
                Arguments = parameters.Arguments,
                Success = success,
            };
        }
    }
}
