﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.Impl
{
    public class ContactPointPurposesCommand : ICommand<ContactPointPurposesResultsDto, ContactPointPurposesArgumentsDto>
    {
        public ContactPointPurposesResultsDto Execute(ContactPointPurposesArgumentsDto arguments)
        {
            IContactRepository repository = RegistryFactory.GetResolver().Resolve<IContactRepository>();

            return new ContactPointPurposesResultsDto
            {
                Arguments = arguments,
                ContactPointPurposes = Mapper.Map(repository.ContactPointPurposes())
            };
        }
    }
}
