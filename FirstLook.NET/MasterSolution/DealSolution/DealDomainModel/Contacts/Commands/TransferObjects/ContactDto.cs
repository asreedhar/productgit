﻿using System;
using System.Collections.Generic;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects
{
    [Serializable]
    public class ContactDto
    {
        public int Id { get; set; }
        public int RevisionNumber { get; set; }

        public List<EmailDto> Emails { get; set; }
        public List<PhoneDto> Phones { get; set; }
        public List<PostalAddressDto> PostalAddresses { get; set; }
    }
}
