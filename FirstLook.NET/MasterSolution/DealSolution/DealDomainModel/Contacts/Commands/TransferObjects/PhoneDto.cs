﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects
{
    [Serializable]
    public class PhoneDto : ContactPointDto
    {
        public PhoneLineTypeDto PhoneLineType { get; set; }

        public string CountryCode { get; set; }

        public string AreaCode { get; set; }

        public string CentralOfficeCode { get; set; }

        public string SubscriberNumber { get; set; }

        public string Extension { get; set; }
    }
}
