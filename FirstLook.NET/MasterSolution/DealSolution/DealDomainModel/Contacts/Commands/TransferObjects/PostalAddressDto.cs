﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects
{
    [Serializable]
    public class PostalAddressDto : ContactPointDto
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string ZipPlus4 { get; set; }
    }
}
