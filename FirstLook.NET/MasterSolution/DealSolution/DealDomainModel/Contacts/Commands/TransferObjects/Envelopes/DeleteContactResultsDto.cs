﻿using System;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DeleteContactResultsDto : IAuthorizationResultsDto
    {
        public DeleteContactArgumentsDto Arguments { get; set; }

        public bool Success { get; set; }

        #region Implementation of IAuthorizationResultsDto

        /// <summary>
        /// Result of authorizatino.
        /// </summary>
        public TokenStatusDto AuthorizationStatus { get; set; }

        #endregion
    }
}
