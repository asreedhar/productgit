﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaveContactResultsDto : ResultsDto
    {
        public SaveContactArgumentsDto Arguments { get; set; }

        public ContactDto Contact { get; set; }
    }
}
