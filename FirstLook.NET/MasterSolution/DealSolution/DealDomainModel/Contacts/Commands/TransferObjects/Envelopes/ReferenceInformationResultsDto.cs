﻿using System;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReferenceInformationResultsDto : ResultsDto
    {
        public ReferenceInformationArgumentsDto Arguments { get; set; }

        public List<ContactPointPurposeDto> ContactPointPurposes { get; set; }

        public List<PhoneLineTypeDto> PhoneLineTypes { get; set; }
    }
}
