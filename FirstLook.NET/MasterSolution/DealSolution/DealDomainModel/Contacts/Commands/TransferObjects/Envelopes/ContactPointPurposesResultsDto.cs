﻿using System;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using System.Collections.Generic;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ContactPointPurposesResultsDto : IAuthorizationResultsDto
    {
        public ContactPointPurposesArgumentsDto Arguments { get; set; }
        public List<ContactPointPurposeDto> ContactPointPurposes { get; set; }

        /// <summary>
        /// Result of authorization.
        /// </summary>
        public TokenStatusDto AuthorizationStatus { get; set; }        
    }
}
