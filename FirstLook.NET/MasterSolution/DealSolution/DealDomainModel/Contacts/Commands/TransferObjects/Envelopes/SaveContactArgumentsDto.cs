﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaveContactArgumentsDto : ArgumentsDto
    {
        public ContactDto Contact { get; set; }
    }
}
