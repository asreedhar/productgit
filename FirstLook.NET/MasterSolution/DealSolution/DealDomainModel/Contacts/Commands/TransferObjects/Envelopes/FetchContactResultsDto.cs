﻿using System;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchContactResultsDto : IAuthorizationResultsDto
    {
        public FetchContactArgumentsDto Arguments { get; set; }

        public ContactDto Contact { get; set; }        

        /// <summary>
        /// Result of authorizatino.
        /// </summary>
        public TokenStatusDto AuthorizationStatus { get; set; }        
    }
}
