﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DeleteContactArgumentsDto : ArgumentsDto
    {
        public int Id { get; set; }

        public int RevisionNumber { get; set; }
    }
}
