﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects
{
    [Serializable]
    public class EmailDto : ContactPointDto
    {
        public string Value { get; set; }
    }
}
