﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects
{
    [Serializable]
    public class ContactPointDto
    {
        public int Id { get; set; }

        public int RevisionNumber { get; set; }

        public ContactPointPurposeDto ContactPointPurpose { get; set; }

        public bool IsPrimary { get; set; }
    }
}
