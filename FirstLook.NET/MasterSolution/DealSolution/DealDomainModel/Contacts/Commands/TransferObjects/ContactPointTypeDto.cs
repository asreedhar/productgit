﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects
{
    [Serializable]
    public class ContactPointTypeDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
