﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    [Serializable]
    public abstract class ContactPoint : BusinessObject
    {
        public ContactPoint() : base()
        {
        }

        public ContactPoint(ContactPoint copyContactPoint) : base(copyContactPoint)
        {
            this.RevisionNumber = copyContactPoint.RevisionNumber;
            this.Id = copyContactPoint.Id;
            this._contactPointPurpose = copyContactPoint._contactPointPurpose.Clone();
            this._isPrimary = copyContactPoint._isPrimary;
        }

        public int RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        private ContactPointPurpose _contactPointPurpose;

        private bool _isPrimary;

        public ContactPointPurpose ContactPointPurpose
        {
            get { return _contactPointPurpose; }
            set { Let(Inspect.NameOf((ContactPoint p) => p.ContactPointPurpose), ref _contactPointPurpose, value); }
        }
        
        public bool IsPrimary
        {
            get { return _isPrimary; }
            set { Let(Inspect.NameOf((ContactPoint p) => p.IsPrimary), ref _isPrimary, value); }
        }
    }
}