﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    [Serializable]
    public class Phone : ContactPoint
    {
        public Phone() : base()
        {
        }

        public Phone(Phone copyPhone) : base(copyPhone)
        {
            this._areaCode = copyPhone._areaCode;
            this._centralOfficeCode = copyPhone._centralOfficeCode;
            this._countryCode = copyPhone._countryCode;
            this._extension = copyPhone._extension;
            this._phoneLineType = copyPhone._phoneLineType.Clone();
            this._subscriberNumber = copyPhone._subscriberNumber;
        }

        public override object Clone()
        {
            return new Phone(this);
        }

        private PhoneLineType _phoneLineType;

        public PhoneLineType PhoneLineType
        {
            get { return _phoneLineType; }
            set { Let(Inspect.NameOf((Phone p) => p.PhoneLineType), ref _phoneLineType, value); }
        }

        private string _countryCode;

        public string CountryCode
        {
            get { return _countryCode; }
            set { Let(Inspect.NameOf((Phone p) => p.CountryCode), ref _countryCode, value); }
        }

        private string _areaCode;

        public string AreaCode
        {
            get { return _areaCode; }
            set { Let(Inspect.NameOf((Phone p) => p.AreaCode), ref _areaCode, value); }
        }

        private string _centralOfficeCode;

        public string CentralOfficeCode
        {
            get { return _centralOfficeCode; }
            set { Let(Inspect.NameOf((Phone p) => p.CentralOfficeCode), ref _centralOfficeCode, value); }
        }

        private string _subscriberNumber;

        public string SubscriberNumber
        {
            get { return _subscriberNumber; }
            set { Let(Inspect.NameOf((Phone p) => p.SubscriberNumber), ref _subscriberNumber, value); }
        }

        private string _extension;

        public string Extension
        {
            get { return _extension; }
            set { Let(Inspect.NameOf((Phone p) => p.Extension), ref _extension, value); }
        }

        public override string ToString()
        {
            return string.Format("+{0} ({1}) {2} {3} # {4}", CountryCode, AreaCode, CentralOfficeCode, SubscriberNumber, Extension);
        }

        #region Validation
        
        private ValidationRules<Phone> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Phone>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Phone>>()));
        }

        #endregion
    }
}