﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    [Serializable]
    public class Contact : BusinessObject
    {
        public Contact() : base()
        {
        }

        public Contact(Contact copyContact) : base(copyContact)
        {
            this._emails = copyContact._emails.Clone();
            this._phones = copyContact._phones.Clone();
            this._postalAddresses = copyContact._postalAddresses.Clone();
            this.Id = copyContact.Id;
            this.RevisionNumber = copyContact.RevisionNumber;
        }

        public override object Clone()
        {
            return new Contact(this);
        }

        public int RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        private readonly BusinessObjectList<Email> _emails = new BusinessObjectList<Email>();

        protected BusinessObjectList<Email> EmailsBase
        {
            get { return _emails; }
        }   

        public IList<Email> Emails
        {
            get { return EmailsBase; }
        }

        private readonly BusinessObjectList<Phone> _phones = new BusinessObjectList<Phone>();

        public BusinessObjectList<Phone> PhonesBase
        {
            get { return _phones; }
        }

        public IList<Phone> Phones
        {
            get { return PhonesBase; }
        }

        private readonly BusinessObjectList<PostalAddress> _postalAddresses = new BusinessObjectList<PostalAddress>();

        public BusinessObjectList<PostalAddress> PostalAddressesBase
        {
            get { return _postalAddresses; }
        }

        public IList<PostalAddress> PostalAddresses
        {
            get { return PostalAddressesBase; }
        }

        #region Validation

        protected override bool IsDirty
        {
            get
            {
                return base.IsDirty
                       || _emails.IsDirty
                       || _phones.IsDirty
                       || _postalAddresses.IsDirty;
            }
        }

        private ValidationRules<Contact> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Contact>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Contact>>()));
        }

        #endregion
    }
}