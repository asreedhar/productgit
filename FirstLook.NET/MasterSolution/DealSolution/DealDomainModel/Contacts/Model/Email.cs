﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    [Serializable]
    public class Email : ContactPoint
    {
        public Email() : base()
        {
        }

        public Email(Email copyEmail) : base(copyEmail)
        {
            this._value = copyEmail._value;
        }

        public override object Clone()
        {
            return new Email(this);
        }

        private string _value;

        public string Value
        {
            get { return _value; }
            set { Let(Inspect.NameOf((Email p) => p.Value), ref _value, value); }
        }

        #region Validation
        
        private ValidationRules<Email> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Email>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Email>>()));
        }

        #endregion
    }
}