﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    /// <summary>
    /// <list>
    /// <item>Email</item>
    /// <item>Phone</item>
    /// <item>PostalAddress</item>
    /// </list>
    /// </summary>
    [Serializable]
    public class ContactPointType
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }
    }
}