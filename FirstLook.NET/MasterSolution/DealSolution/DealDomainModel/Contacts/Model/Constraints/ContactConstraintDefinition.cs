﻿using System.Linq;
using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;

namespace FirstLook.Deal.DomainModel.Contacts.Model.Constraints
{
    public class ContactConstraintDefinition : ConstraintDefinition<Contact>
    {
        public ContactConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Contact x) => x.Emails),
                new List<IConstraint<Contact>>
                    {
                        new PropertyConstraint<Contact, IList<Email>>(
                            x => x.Emails,
                            new SinglePrimary<Email>()),
                        new PropertyConstraint<Contact, IList<Email>>(
                            x => x.Emails,
                            new CompositeCollectionConstraint<Email>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Contact x) => x.Phones),
                new List<IConstraint<Contact>>
                    {
                        new PropertyConstraint<Contact, IList<Phone>>(
                            x => x.Phones,
                            new SinglePrimary<Phone>()),
                        new PropertyConstraint<Contact, IList<Phone>>(
                            x => x.Phones,
                            new CompositeCollectionConstraint<Phone>())
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Contact x) => x.PostalAddresses),
                new List<IConstraint<Contact>>
                    {
                        new PropertyConstraint<Contact, IList<PostalAddress>>(
                            x => x.PostalAddresses,
                            new SinglePrimary<PostalAddress>()),
                        new PropertyConstraint<Contact, IList<PostalAddress>>(
                            x => x.PostalAddresses,
                            new CompositeCollectionConstraint<PostalAddress>())
                    });
        }

        /// <summary>
        /// Fail if more than one primary ContactPoint
        /// </summary>
        /// <typeparam name="T"></typeparam>
        class SinglePrimary<T> : IConstraint<IList<T>> where T : ContactPoint
        {
            private readonly string _resourceKey;

            public SinglePrimary() : this("deal.constraint.contact.primary")
            {
            }

            public SinglePrimary(string resourceKey)
            {
                _resourceKey = resourceKey;
            }

            public bool IsSatisfiedBy(IList<T> value)
            {
                if (value == null || value.Count == 0)
                {
                    return true;
                }

                if (value.Count(x => x.IsPrimary) > 1)
                {
                    return false;
                }

                return true;
            }

            public string ResourceKey
            {
                get { return _resourceKey; }
            }

            public bool StopProcessing
            {
                get { return false; }
            }
        }
    }
}
