﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;

namespace FirstLook.Deal.DomainModel.Contacts.Model.Constraints
{
    public class PhoneConstraintDefinition : ConstraintDefinition<Phone>
    {
        public PhoneConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Phone x) => x.CountryCode),
                new List<IConstraint<Phone>>
                    {
                        new PropertyConstraint<Phone, string>(
                            x => x.CountryCode,
                            RegexConstraint.GetConstraint(RegExPatterns.Integer)),
                        new PropertyConstraint<Phone, string>(
                            x => x.CountryCode,
                            new MinLengthConstraint(1)),
                        new PropertyConstraint<Phone, string>(
                            x => x.CountryCode,
                            new MaxLengthConstraint(3))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Phone x) => x.AreaCode),
                new List<IConstraint<Phone>>
                    {
                        new PropertyConstraint<Phone, string>(
                            x => x.AreaCode,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Phone, string>(
                            x => x.AreaCode,
                            RegexConstraint.GetConstraint(RegExPatterns.Integer)),
                        new PropertyConstraint<Phone, string>(
                            x => x.AreaCode,
                            new ExactLengthConstraint(3))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Phone x) => x.CentralOfficeCode),
                new List<IConstraint<Phone>>
                    {
                        new PropertyConstraint<Phone, string>(
                            x => x.CentralOfficeCode,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Phone, string>(
                            x => x.CentralOfficeCode,
                            RegexConstraint.GetConstraint(RegExPatterns.Integer)),
                        new PropertyConstraint<Phone, string>(
                            x => x.CentralOfficeCode,
                            new ExactLengthConstraint(3))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Phone x) => x.SubscriberNumber),
                new List<IConstraint<Phone>>
                    {
                        new PropertyConstraint<Phone, string>(
                            x => x.SubscriberNumber,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Phone, string>(
                            x => x.SubscriberNumber,
                            RegexConstraint.GetConstraint(RegExPatterns.Integer)),
                        new PropertyConstraint<Phone, string>(
                            x => x.SubscriberNumber,
                            new ExactLengthConstraint(4))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Phone x) => x.Extension),
                new List<IConstraint<Phone>>
                    {
                        new PropertyConstraint<Phone, string>(
                            x => x.Extension,
                            new MaxLengthConstraint(50))
                    });

            PropertyConstraints.Add(
               Inspect.NameOf((Phone x) => x.ContactPointPurpose),
               new List<IConstraint<Phone>>
               {
               });
            PropertyConstraints.Add(
                Inspect.NameOf((Phone x) => x.IsPrimary),
                new List<IConstraint<Phone>>
                {
                });

            PropertyConstraints.Add(
                Inspect.NameOf((Phone x) => x.PhoneLineType),
                new List<IConstraint<Phone>>
                {
                });
        }
    }
}