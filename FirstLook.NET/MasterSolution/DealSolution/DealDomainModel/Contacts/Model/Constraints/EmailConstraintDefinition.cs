﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;

namespace FirstLook.Deal.DomainModel.Contacts.Model.Constraints
{
    public class EmailConstraintDefinition : ConstraintDefinition<Email>
    {
        public EmailConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((Email x) => x.Value),
                new List<IConstraint<Email>>
                    {
                        new PropertyConstraint<Email, string>(
                            x => x.Value,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Email, string>(
                            x => x.Value,
                            new BetweenLengthConstraint(1, 250)),
                        new PropertyConstraint<Email, string>(
                            x => x.Value,
                            RegexConstraint.GetConstraint(RegExPatterns.Email))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Email x) => x.ContactPointPurpose),
                new List<IConstraint<Email>>
                    {
                    });
            PropertyConstraints.Add(
                Inspect.NameOf((Email x) => x.IsPrimary),
                new List<IConstraint<Email>>
                {
                });

        }
    }
}