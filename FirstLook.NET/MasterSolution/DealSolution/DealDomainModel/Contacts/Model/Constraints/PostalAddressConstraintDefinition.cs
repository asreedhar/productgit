﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;

namespace FirstLook.Deal.DomainModel.Contacts.Model.Constraints
{
    public class PostalAddressConstraintDefinition : ConstraintDefinition<PostalAddress>
    {
        public PostalAddressConstraintDefinition()
        {
            PropertyConstraints.Add(
                Inspect.NameOf((PostalAddress x) => x.Line1),
                new List<IConstraint<PostalAddress>>
                    {
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.Line1,
                            new BetweenLengthConstraint(1, 250))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((PostalAddress x) => x.Line2),
                new List<IConstraint<PostalAddress>>
                    {
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.Line2,
                            new BetweenLengthConstraint(1, 250))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((PostalAddress x) => x.City),
                new List<IConstraint<PostalAddress>>
                    {
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.City,
                            new BetweenLengthConstraint(1, 250))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((PostalAddress x) => x.State),
                new List<IConstraint<PostalAddress>>
                    {
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.State,
                            RegexConstraint.GetConstraint(RegExPatterns.StateCode))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((PostalAddress x) => x.Zip),
                new List<IConstraint<PostalAddress>>
                    {
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.State,
                            RegexConstraint.GetConstraint(RegExPatterns.Integer)),
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.Zip,
                            new ExactLengthConstraint(6))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((PostalAddress x) => x.ZipPlus4),
                new List<IConstraint<PostalAddress>>
                    {
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.State,
                            RegexConstraint.GetConstraint(RegExPatterns.Integer)),
                        new PropertyConstraint<PostalAddress, string>(
                            x => x.Zip,
                            new ExactLengthConstraint(4))
                    });

            PropertyConstraints.Add(
              Inspect.NameOf((PostalAddress x) => x.ContactPointPurpose),
              new List<IConstraint<PostalAddress>>
              {
              });
            PropertyConstraints.Add(
                Inspect.NameOf((PostalAddress x) => x.IsPrimary),
                new List<IConstraint<PostalAddress>>
                {
                });

            ObjectConstraints.Add(new OneOrMoreProperties());
        }

        class OneOrMoreProperties : IConstraint<PostalAddress>
        {
            public bool IsSatisfiedBy(PostalAddress value)
            {
                if (value == null)
                {
                    return true;
                }

                var list = new List<string> {value.Line1, value.Line2, value.City, value.State, value.Zip, value.ZipPlus4};

                if (list.All(x => string.IsNullOrEmpty(x)))
                {
                    return false;
                }

                return true;
            }

            public string ResourceKey
            {
                get { return "deal.constraint.contact.postaladdress.empty"; }
            }

            public bool StopProcessing
            {
                get { return false; }
            }
        }
    }
}