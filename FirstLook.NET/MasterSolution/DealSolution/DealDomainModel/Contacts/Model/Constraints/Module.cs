﻿using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Contacts.Model.Constraints
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IConstraintDefinition<Contact>, ContactConstraintDefinition>(ImplementationScope.Shared);

            registry.Register<IConstraintDefinition<Email>, EmailConstraintDefinition>(ImplementationScope.Shared);

            registry.Register<IConstraintDefinition<Phone>, PhoneConstraintDefinition>(ImplementationScope.Shared);

            registry.Register<IConstraintDefinition<PostalAddress>, PostalAddressConstraintDefinition>(ImplementationScope.Shared);
        }
    }
}
