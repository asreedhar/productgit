﻿using FirstLook.Client.DomainModel.Clients.Model;
using System.Collections.Generic;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    public interface IContactRepository
    {

        /// <summary>
        /// Reference information
        /// </summary>
        /// <returns></returns>
        IList<ContactPointPurpose> ContactPointPurposes();
        
        /// <summary>
        /// Reference information
        /// </summary>
        /// <returns></returns>
        IList<PhoneLineType> PhoneLineTypes();
        
        /// <summary>
        /// CRUD
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Contact Fetch(IBroker broker, int id);
        
        /// <summary>
        /// CRUD
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
    Contact Delete(IBroker broker, Contact contact);
        
        /// <summary>
        /// CRUD
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Contact Save(IBroker broker, Contact contact);
    }
}