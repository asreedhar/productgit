﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    /// <summary>
    /// <list>
    /// <item>Alternate</item>
    /// <item>Home</item>
    /// <item>General</item>
    /// <item>Work</item>
    /// </list>
    /// </summary>
    [Serializable]
    public class ContactPointPurpose
    {
        public ContactPointPurpose()
        {
        }

        public ContactPointPurpose(ContactPointPurpose copyContactPointPurpose)
        {
            this.Id = copyContactPointPurpose.Id;
            this.Name = copyContactPointPurpose.Name;
        }

        public ContactPointPurpose Clone()
        {
            return new ContactPointPurpose(this);
        }

        public int Id { get; internal set; }
        public string Name { get; internal set; }
    }
}