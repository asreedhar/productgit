﻿using System;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    /// <summary>
    /// <list>
    /// <item>Cell</item>
    /// <item>Fax</item>
    /// <item>Home</item>
    /// <item>Main</item>
    /// <item>Work</item>
    /// </list>
    /// </summary>
    [Serializable]
    public class PhoneLineType
    {
        public PhoneLineType Clone()
        {
            return new PhoneLineType {Id = this.Id, Name = this.Name};
        }

        public int Id { get; internal set; }

        public string Name { get; internal set; }
    }
}