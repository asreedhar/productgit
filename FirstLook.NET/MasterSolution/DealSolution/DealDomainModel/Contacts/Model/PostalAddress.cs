﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Contacts.Model
{
    /// <see cref="http://pe.usps.com/cpim/ftp/pubs/pub28/pub28.pdf"/>
    [Serializable]
    public class PostalAddress : ContactPoint
    {
        public PostalAddress() : base()
        {
        }

        public PostalAddress(PostalAddress copyPostalAddress) : base(copyPostalAddress)
        {
            this._city = copyPostalAddress._city;
            this._line1 = copyPostalAddress._line1;
            this._line2 = copyPostalAddress._line2;
            this._rules = copyPostalAddress._rules;
            this._state = copyPostalAddress._state;
            this._zip = copyPostalAddress._zip;
            this._zipPlus4 = copyPostalAddress._zipPlus4;
        }

        public override object Clone()
        {
            return new PostalAddress(this);
        }

        private string _line1;

        public string Line1
        {
            get { return _line1; }
            set { Let(Inspect.NameOf((PostalAddress p) => p.Line1), ref _line1, value); }
        }

        private string _line2;

        public string Line2
        {
            get { return _line2; }
            set { Let(Inspect.NameOf((PostalAddress p) => p.Line2), ref _line2, value); }
        }

        private string _city;

        public string City
        {
            get { return _city; }
            set { Let(Inspect.NameOf((PostalAddress p) => p.City), ref _city, value); }
        }

        private string _state;

        public string State
        {
            get { return _state; }
            set { Let(Inspect.NameOf((PostalAddress p) => p.State), ref _state, value); }
        }

        private string _zip;

        public string Zip
        {
            get { return _zip; }
            set { Let(Inspect.NameOf((PostalAddress p) => p.Zip), ref _zip, value); }
        }

        private string _zipPlus4;

        public string ZipPlus4
        {
            get { return _zipPlus4; }
            set { Let(Inspect.NameOf((PostalAddress p) => p.ZipPlus4), ref _zipPlus4, value); }
        }

        #region Validation

        private ValidationRules<PostalAddress> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<PostalAddress>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<PostalAddress>>()));
        }

        #endregion
    }
}