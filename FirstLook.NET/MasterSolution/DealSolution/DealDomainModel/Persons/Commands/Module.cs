﻿using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Commands.Impl;

namespace FirstLook.Deal.DomainModel.Persons.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
