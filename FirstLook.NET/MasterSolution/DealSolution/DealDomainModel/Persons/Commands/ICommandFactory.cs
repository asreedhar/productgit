﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Persons.Commands
{
    public interface ICommandFactory
    {
        ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand();

        ICommand<FetchPersonResultsDto, FetchPersonArgumentsDto> CreateFetchPersonCommand();

        ICommand<SavePersonResultsDto, IIdentityContextDto<SavePersonArgumentsDto>> CreateSavePersonCommand();

        ICommand<DeletePersonResultsDto, IIdentityContextDto<DeletePersonArgumentsDto>> CreateDeletePersonCommand();
    }
}
