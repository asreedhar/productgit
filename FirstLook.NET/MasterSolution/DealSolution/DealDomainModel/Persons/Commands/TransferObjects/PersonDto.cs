﻿using System;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects
{
    [Serializable]
    public class PersonDto
    {
        public int Id { get; set; }                          

        public short RevisionNumber { get; set; }            

        #region Name
        
        public string GivenName { get; set; }

        public string MiddleName { get; set; }

        public string FamilyName { get; set; }

        public TitleDto Title { get; set; }

        #endregion

        #region Demographics

        public DateTime? BirthDate { get; set; }

        public GenderDto Gender { get; set; }

        public MaritalStatusDto MaritalStatus { get; set; }

        public string Occupation { get; set; }

        #endregion

        public ContactDto Contact { get; set; }      
    }
}
