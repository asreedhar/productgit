﻿using System;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects
{
    [Serializable]
    public class TitleDto
    {
        public int Id { get; set; }                

        public string Name { get; set; }
    }
}
