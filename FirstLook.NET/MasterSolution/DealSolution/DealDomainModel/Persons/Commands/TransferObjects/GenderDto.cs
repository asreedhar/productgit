﻿using System;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects
{
    [Serializable]
    public class GenderDto
    {
        public string Id { get; set; }             

        public string Name { get; set; }
    }
}
