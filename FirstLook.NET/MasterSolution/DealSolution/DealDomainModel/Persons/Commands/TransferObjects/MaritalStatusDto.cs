﻿using System;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects
{
    [Serializable]
    public class MaritalStatusDto
    {
        public string Id { get; set; }             

        public string Name { get; set; }
    }
}
