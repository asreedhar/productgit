﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchPersonResultsDto : ResultsDto
    {
        public FetchPersonArgumentsDto Arguments { get; set; }

        public PersonDto Person { get; set; }
    }
}
