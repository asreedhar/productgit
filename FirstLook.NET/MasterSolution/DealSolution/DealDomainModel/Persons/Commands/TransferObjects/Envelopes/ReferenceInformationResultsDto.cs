﻿using System;
using System.Collections.Generic;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReferenceInformationResultsDto : ResultsDto
    {
        public ReferenceInformationArgumentsDto Arguments { get; set; }

        public List<TitleDto> Titles { get; set; }

        public List<GenderDto> Genders { get; set; }

        public List<MaritalStatusDto> MaritalStatuses { get; set; }
    }
}
