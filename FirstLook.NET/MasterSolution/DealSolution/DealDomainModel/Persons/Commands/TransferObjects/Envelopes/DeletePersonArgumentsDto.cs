﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DeletePersonArgumentsDto : ArgumentsDto
    {
        public int Id { get; set; }

        public short RevisionNumber { get; set; }
    }
}
