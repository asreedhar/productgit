﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SavePersonResultsDto : ResultsDto
    {
        public SavePersonArgumentsDto Arguments { get; set; }

        public PersonDto Person { get; set; }
    }
}
