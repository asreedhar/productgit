﻿using System;
using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DeletePersonResultsDto : ResultsDto
    {
        public DeletePersonArgumentsDto Arguments { get; set; }
    }
}
