﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Commands.Impl
{
    /// <summary>
    /// Grab the Title, Gender, and MaritalStatus information
    /// </summary>
    public class ReferenceInformationCommand : ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto>
    {
        /// <summary>
        /// Grab the Title, Gender, and MaritalStatus information
        /// </summary>
        /// <param name="parameters">There really aren't parms for this function</param>
        /// <returns>The list of supported titles, genders, and marital statuses</returns>
        public ReferenceInformationResultsDto Execute(ReferenceInformationArgumentsDto parameters)
        {
            //Initialize an instance of a person respository
            IPersonRepository personRepository = RegistryFactory.GetResolver().Resolve<IPersonRepository>();

            //Call on the repository to fetch reference information
            return new ReferenceInformationResultsDto
            {
                Arguments = parameters,
                Genders = (List<GenderDto>)Mapper.Map((List<Gender>)personRepository.Genders()),
                MaritalStatuses = (List<MaritalStatusDto>)Mapper.Map((List<MaritalStatus>)personRepository.MaritalStatuses()),
                Titles = (List<TitleDto>)Mapper.Map((List<Title>)personRepository.Titles())
            };
        }
    }
}
