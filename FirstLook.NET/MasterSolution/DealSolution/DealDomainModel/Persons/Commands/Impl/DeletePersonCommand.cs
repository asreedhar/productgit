﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Commands.Impl
{
    public class DeletePersonCommand : ICommand<DeletePersonResultsDto, IIdentityContextDto<DeletePersonArgumentsDto>>
    {
        /// <summary>
        /// Delete a person
        /// </summary>
        /// <param name="parameters">Contains the persondto to delete.</param>
        /// <returns></returns>
        public DeletePersonResultsDto Execute(IIdentityContextDto<DeletePersonArgumentsDto> parameters)
        {

            //Initialize an instance of a person respository
            IPersonRepository personRepository = RegistryFactory.GetResolver().Resolve<IPersonRepository>();
            IdentityDto identity = parameters.Identity;
            DeletePersonArgumentsDto arguments = parameters.Arguments;

            //TODO: Handle broker, identity, and principal correctly
            // Fetch the broker and principal
            //... comment out temporarily
            // IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            // IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);

            IBroker broker = new Client.DomainModel.Clients.Repositories.Entities.Broker();
            IPrincipal principal = new Client.DomainModel.Clients.Repositories.Entities.Principal();

            Person person = new Person
                                {
                                    Id = arguments.Id,
                                    RevisionNumber = arguments.RevisionNumber
                                };

            personRepository.Delete(broker, principal, person);

            //Call on the repository to delete a person, at this point not sure if broker is needed on this call.
            //After the delete is complete return to the caller.
            return new DeletePersonResultsDto
                        {
                           Arguments = arguments 
                        };
        }

        
    }
}
