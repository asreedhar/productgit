﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Commands.Impl
{
    /// <summary>
    /// Fetch a person based on a passed in id.
    /// </summary>
    public class FetchPersonCommand : ICommand<FetchPersonResultsDto, FetchPersonArgumentsDto>
    {
        /// <summary>
        /// Return a person based on an id.
        /// </summary>
        /// <param name="parameters">Contains the id to fetch on</param>
        /// <returns>FetchPersonResultsDto, a dto representation of a person object</returns>
        public FetchPersonResultsDto Execute(FetchPersonArgumentsDto parameters)
        {

            //Initialize an instance of a person respository
            IPersonRepository personRepository = RegistryFactory.GetResolver().Resolve<IPersonRepository>();

            //TODO: Handle broker correctly
            IBroker broker = new Client.DomainModel.Clients.Repositories.Entities.Broker();

            //Call on the repository to fetch a person, at this point not sure if broker is needed on this call.
            //After the fetch is complete it will transform the object to a dto to be returned to caller.
            return new FetchPersonResultsDto
                       {
                           Arguments = parameters, 
                           Person = Mapper.Map(personRepository.Fetch(broker, parameters.Id))
                       };
        }
    }
}
