﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Commands.Impl
{
    /// <summary>
    /// This class is reponsible for mapping business objects to DTOs for serialization back to the caller.
    /// </summary>
    class Mapper
    {
        /// <summary>
        /// Map a person to a PersonDto
        /// </summary>
        /// <param name="person">Person object to represent as a dto.</param>
        /// <returns>A PersonDto</returns>
        public static PersonDto Map(Person person)
        {
            if (person == null)
            {
                return null;
            }

            return new PersonDto
                       {
                           BirthDate = person.BirthDate,
                           Contact = Contacts.Commands.Impl.Mapper.Map(person.Contact),
                           FamilyName = person.FamilyName,
                           Gender = Map(person.Gender),
                           GivenName = person.GivenName,
                           MaritalStatus = Map(person.MaritalStatus),
                           MiddleName = person.MiddleName,
                           Occupation = person.Occupation,
                           Title = Map(person.Title),
                           Id = person.Id,
                           RevisionNumber = person.RevisionNumber
                       };

        }

        /// <summary>
        /// Map a gender to a GenderDto
        /// </summary>
        /// <param name="gender">Gender object to represent as a dto</param>
        /// <returns>A GenderDto</returns>
        public static GenderDto Map(Gender gender)
        {
            if(gender == null)
            {
                return null;
            }
            return new GenderDto {Id = gender.Id, Name = gender.Name};
        }

        /// <summary>
        /// Map a MaritalStatus to a MaritalStatusDto
        /// </summary>
        /// <param name="maritalStatus">MaritalStatus object to represent as a dto</param>
        /// <returns>A MaritalStatusDto</returns>
        public static MaritalStatusDto Map(MaritalStatus maritalStatus)
        {
            if(maritalStatus == null)
            {
                return null;
            }
            return new MaritalStatusDto {Id = maritalStatus.Id, Name = maritalStatus.Name};
        }


        /// <summary>
        /// Map a Title to a TitleDto
        /// </summary>
        /// <param name="title">Title object to represent as a dto</param>
        /// <returns>A TitleDto</returns>
        public static TitleDto Map(Title title)
        {
            if(title == null)
            {
                return null;
            }
            return new TitleDto {Id = title.Id, Name = title.Name};
        }


        /// <summary>
        /// Map a PersonDto to a Person
        /// </summary>
        /// <param name="personDto">PersonDto object to represent as a Person</param>
        /// <returns>A Person</returns>
        public static Person Map(PersonDto personDto)
        {
            if(personDto == null)
            {
                return null;
            }

            return new Person
                       {
                           BirthDate = personDto.BirthDate,
                           Contact = Contacts.Commands.Impl.Mapper.Map(personDto.Contact),
                           FamilyName = personDto.FamilyName,
                           Gender = personDto.Gender == null ? null : Map(personDto.Gender), 
                           GivenName = personDto.GivenName,
                           MaritalStatus = personDto.MaritalStatus == null ? null : Map(personDto.MaritalStatus), 
                           MiddleName = personDto.MiddleName, 
                           Occupation = personDto.Occupation,
                           Title = personDto.Title == null ? null : Map(personDto.Title),
                           RevisionNumber = personDto.RevisionNumber,
                           Id = personDto.Id
                       };
        }

        /// <summary>
        /// Map a GenderDto object to a Gender 
        /// </summary>
        /// <param name="genderDto">GenderDto object to represent as a Gender</param>
        /// <returns>A Gender</returns>
        public static Gender Map(GenderDto genderDto)
        {
            if(genderDto == null)
            {
                return null;
            }

            return new Gender {Id = genderDto.Id, Name = genderDto.Name};
        }

        /// <summary>
        /// Map a MarirtalStatusDto object to a MaritalStatus
        /// </summary>
        /// <param name="maritalStatusDto">MaritalStatusDto object to represent as a MaritalStatus</param>
        /// <returns>A MaritalStatus</returns>
        public static MaritalStatus Map(MaritalStatusDto maritalStatusDto)
        {
            if(maritalStatusDto == null)
            {
                return null;
            }
            return new MaritalStatus {Id = maritalStatusDto.Id, Name = maritalStatusDto.Name};
        }

        /// <summary>
        /// Map a TitleDto to a Title object
        /// </summary>
        /// <param name="titleDto">TitleDto objec to represent as a Title</param>
        /// <returns>A Title</returns>
        public static Title Map(TitleDto titleDto)
        {
            
            if(titleDto == null)
            {
                return null;
            }
            return new Title {Id = titleDto.Id, Name = titleDto.Name};
        }


        /// <summary>
        /// Map a list of Genders to a list of GenderDtos
        /// </summary>
        /// <param name="genders">List of genders to convert to GenderDtos</param>
        /// <returns>List of GenderDtos</returns>
        public static IList<GenderDto> Map(List<Gender> genders)
        {
            return genders.Select(Map).ToList();
        }

        /// <summary>
        /// Map a list of MaritalStatuses to a list of MaritalStatusDtos
        /// </summary>
        /// <param name="maritalStatuses">List of MaritalStatuses to convert to MaritalStatusDtos</param>
        /// <returns>List of MaritalStatusDtos</returns>
        public static IList<MaritalStatusDto> Map(List<MaritalStatus> maritalStatuses)
        {
            return maritalStatuses.Select(Map).ToList();
        }

        /// <summary>
        /// Map a list of Titles to a list of TitleDtos
        /// </summary>
        /// <param name="titles">List of Titles to convert to TitleDtos</param>
        /// <returns>List of TitleDtos</returns>
        public static IList<TitleDto> Map(List<Title> titles)
        {
            return titles.Select(Map).ToList();
        }

        /// <summary>
        /// Map a person dto into an existing person object. 
        /// </summary>
        /// <param name="p1">Person to be merged into</param>
        /// <param name="p2">Person dto to merge from.</param>
        public static void Map(Person p1, PersonDto p2)
        {
            if(p2 != null && p1 != null)
            {
                p1.BirthDate = p2.BirthDate;
                p1.Contact = Contacts.Commands.Impl.Mapper.Map(p2.Contact);
                p1.FamilyName = p2.FamilyName;
                p1.GivenName = p2.GivenName;
                p1.MiddleName = p2.MiddleName;
                p1.MaritalStatus = Map(p2.MaritalStatus);
                p1.Gender = Map(p2.Gender);
                p1.Occupation = p2.Occupation;
                p1.Title = Map(p2.Title);
            }
        }


    }
}
