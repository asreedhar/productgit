﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes;

namespace FirstLook.Deal.DomainModel.Persons.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> CreateReferenceInformationCommand()
        {
            return new ReferenceInformationCommand();
        }

        public ICommand<FetchPersonResultsDto, FetchPersonArgumentsDto> CreateFetchPersonCommand()
        {
            return new FetchPersonCommand();
        }

        public ICommand<SavePersonResultsDto, IIdentityContextDto<SavePersonArgumentsDto>> CreateSavePersonCommand()
        {
            return new SavePersonCommand();
        }

        public ICommand<DeletePersonResultsDto, IIdentityContextDto<DeletePersonArgumentsDto>> CreateDeletePersonCommand()
        {
            return new DeletePersonCommand();
        }
    }
}
