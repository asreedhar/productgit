﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Commands.Impl
{
    
    /// <summary>
    /// Save a person.  
    /// </summary>
    public class SavePersonCommand : ICommand<SavePersonResultsDto, IIdentityContextDto<SavePersonArgumentsDto>>
    {
        /// <summary>
        /// Save a person
        /// </summary>
        /// <param name="parameters">Contains the persondto to save.</param>
        /// <returns>Returns the saved person.</returns>
        public SavePersonResultsDto Execute(IIdentityContextDto<SavePersonArgumentsDto> parameters)
        {

            //Initialize an instance of a person respository
            IPersonRepository personRepository = RegistryFactory.GetResolver().Resolve<IPersonRepository>();
            IdentityDto identity = parameters.Identity;
            SavePersonArgumentsDto arguments = parameters.Arguments;

            //TODO: Handle broker and principal info correctly
            // Fetch the broker and principal
            //... comment out temporarily
            // IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            // IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);

            IBroker broker = new Client.DomainModel.Clients.Repositories.Entities.Broker();
            IPrincipal principal = new Client.DomainModel.Clients.Repositories.Entities.Principal();
            PersonDto person = parameters.Arguments.Person;

            Person entity = personRepository.Fetch(broker, parameters.Arguments.Person.Id);

            //Existing person?
            if (entity != null)
            {
                //Check revision number
                if(entity.RevisionNumber != person.RevisionNumber)
                {
                    throw new ConcurrentModificationException("Concurrent modification failure.");
                }

                //Map dto onto existing entity
                Mapper.Map(entity, person);

                if (!entity.IsValid)
                {
                    throw new ArgumentException("Invalid person", "person");
                }
            }
            else
            {
                //It's a new person object, so map it as such
                entity = Mapper.Map(person);
            }

 
            //Call on the repository to save a person, at this point not sure if broker is needed on this call.
            //After the save is complete transform the saved person into a persondto to send back to the caller.)
            return new SavePersonResultsDto
                       {
                           Arguments = arguments,
                           Person =
                               Mapper.Map(personRepository.Save(broker, principal, entity))
                       };
        }
    }
}
