﻿using System;

namespace FirstLook.Deal.DomainModel.Persons.Model
{
    [Serializable]
    public class Title
    {
        public int Id { get; internal set; }               

        public string Name { get; internal set; }
    }
}
