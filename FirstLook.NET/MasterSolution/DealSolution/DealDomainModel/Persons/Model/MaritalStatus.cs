﻿using System;

namespace FirstLook.Deal.DomainModel.Persons.Model
{
    [Serializable]
    public class MaritalStatus
    {
        public string Id { get; internal set; }            

        public string Name { get; internal set; }
    }
}
