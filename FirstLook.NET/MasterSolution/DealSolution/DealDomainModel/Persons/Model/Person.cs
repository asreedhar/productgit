﻿using System;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Persons.Model
{
    [Serializable]
    public class Person : BusinessObject
    {
        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public short RevisionNumber { get; protected internal set; }

        public int Id { get; protected internal set; }

        #region Name
        
        private string _givenName;

        public string GivenName
        {
            get { return _givenName; }
            set { Let(Inspect.NameOf((Person p) => p.GivenName), ref _givenName, value); }
        }

        private string _familyName;

        public string FamilyName
        {
            get { return _familyName; }
            set { Let(Inspect.NameOf((Person p) => p.FamilyName), ref _familyName, value); }
        }

        private string _middleName;

        public string MiddleName
        {
            get { return _middleName; }
            set { Let(Inspect.NameOf((Person p) => p.MiddleName), ref _middleName, value); }
        }

        private Title _title;

        public Title Title
        {
            get { return _title; }
            set { Let(Inspect.NameOf((Person p) => p.Title), ref _title, value); }
        }

        #endregion

        #region Demographics

        private DateTime? _birthDate;

        public DateTime? BirthDate
        {
            get { return _birthDate; }
            set { Let(Inspect.NameOf((Person p) => p.BirthDate), ref _birthDate, value); }
        }

        private Gender _gender;

        public Gender Gender
        {
            get { return _gender; }
            set { Let(Inspect.NameOf((Person p) => p.Gender), ref _gender, value); }
        }

        private MaritalStatus _maritalStatus;

        public MaritalStatus MaritalStatus
        {
            get { return _maritalStatus; }
            set { Let(Inspect.NameOf((Person p) => p.MaritalStatus), ref _maritalStatus, value); }
        }

        private string _occupation;

        public string Occupation
        {
            get { return _occupation; }
            set { Let(Inspect.NameOf((Person p) => p.Occupation), ref _occupation, value); }
        }

        #endregion

        #region Contact Information

        private Contact _contact;

        public Contact Contact
        {
            get { return _contact; }
            set { Let(Inspect.NameOf((Person p) => p.Contact), ref _contact, value); }
        }

        #endregion

        #region Validation


        private ValidationRules<Person> _rules;

        protected override IValidationRules GetValidationRules()
        {
            return _rules ?? (_rules = new ValidationRules<Person>(
                                           this,
                                           RegistryFactory.GetResolver().Resolve<IConstraintDefinition<Person>>()));
        }
        
        #endregion
        
    }
}
