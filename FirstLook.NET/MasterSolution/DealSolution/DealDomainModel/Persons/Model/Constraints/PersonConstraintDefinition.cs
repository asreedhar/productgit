﻿using System.Collections.Generic;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Validation;
using FirstLook.Common.Core.Validation.Constraints;
using FirstLook.Deal.DomainModel.Contacts.Model;

namespace FirstLook.Deal.DomainModel.Persons.Model.Constraints
{
    /// <summary>
    /// Add the required person constraints.
    /// </summary>
    public class PersonConstraintDefinition : ConstraintDefinition<Person>
    {
        /// <summary>
        /// Add the required person constaints.
        /// </summary>
        public PersonConstraintDefinition()
        {

            //TODO: All of these contraints need to be revisited.
            PropertyConstraints.Add(
                Inspect.NameOf((Person x) => x.GivenName),
                new List<IConstraint<Person>>
                    {
                        new PropertyConstraint<Person, string>(
                            x => x.GivenName,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Person, string>(
                            x => x.GivenName,
                            new BetweenLengthConstraint(1, 100))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Person x) => x.FamilyName),
                    new List<IConstraint<Person>>
                    {
                        new PropertyConstraint<Person, string>(
                            x => x.FamilyName,
                            new RequiredConstraint<string>()),
                        new PropertyConstraint<Person, string>(
                            x => x.FamilyName,
                            new BetweenLengthConstraint(1, 100))
                    });

            
            PropertyConstraints.Add(
                Inspect.NameOf((Person x) => x.MiddleName),
                    new List<IConstraint<Person>>
                    {
                        new PropertyConstraint<Person, string>(
                            x => x.MiddleName,
                            new BetweenLengthConstraint(0, 100))
                    });

            PropertyConstraints.Add(
                Inspect.NameOf((Person x) => x.Occupation),
                    new List<IConstraint<Person>>
                    {
                        new PropertyConstraint<Person, string>(
                            x => x.Occupation,
                            new BetweenLengthConstraint(0, 100))
                    });

            
            PropertyConstraints.Add(
                Inspect.NameOf((Person x) => x.BirthDate),
                new List<IConstraint<Person>>
                    {
                       //TODO: Should we have a constraint on birthdate or just late db handle it?
                    });

            PropertyConstraints.Add(
               Inspect.NameOf((Person x) => x.Gender),
               new List<IConstraint<Person>>
                    {
                        new PropertyConstraint<Person, Gender>(
                            x => x.Gender,
                            new RequiredConstraint<Gender>()),
                        new PropertyConstraint<Person, Gender>(
                            x => x.Gender,
                            new GenderConstraint())
                    });

            PropertyConstraints.Add(
             Inspect.NameOf((Person x) => x.MaritalStatus),
             new List<IConstraint<Person>>
                    {
                        new PropertyConstraint<Person, MaritalStatus>(
                            x => x.MaritalStatus,
                            new RequiredConstraint<MaritalStatus>())
                    });

            PropertyConstraints.Add(
             Inspect.NameOf((Person x) => x.Title),
             new List<IConstraint<Person>>
                    {
                        new PropertyConstraint<Person, Title>(
                            x => x.Title,
                            new RequiredConstraint<Title>())
                    });

            PropertyConstraints.Add(
             Inspect.NameOf((Person x) => x.Contact),
             new List<IConstraint<Person>>
                    {
                        //TODO: Should a contact be required for a person?
                    });

        }

        /// <summary>
        /// Gender Constraint, simply check to verify the Id on a gender object is either M or F.
        /// 
        ///TODO: THIS IS A TEST CONSTRAINT... WILL BE REMOVED.
        /// 
        /// </summary>
        class GenderConstraint : IConstraint<Gender>  
        {
            private readonly string _resourceKey;

            public GenderConstraint()
                : this("deal.constraint.persons.gender")
            {
            }

            public GenderConstraint(string resourceKey)
            {
                _resourceKey = resourceKey;
            }

            public bool IsSatisfiedBy(Gender value)
            {
                return true;
                //return false;
            }

            public string ResourceKey
            {
                get { return _resourceKey; }
            }

            public bool StopProcessing
            {
                get { return true; }
            }
        }
    }
}
