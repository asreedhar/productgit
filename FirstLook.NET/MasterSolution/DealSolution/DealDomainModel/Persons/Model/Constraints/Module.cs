﻿using FirstLook.Common.Core.Registry;
using FirstLook.Common.Core.Validation;

namespace FirstLook.Deal.DomainModel.Persons.Model.Constraints
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IConstraintDefinition<Person>, PersonConstraintDefinition>(ImplementationScope.Shared);
        }
    }
}
