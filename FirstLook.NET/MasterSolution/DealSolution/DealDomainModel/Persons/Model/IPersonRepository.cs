﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Deal.DomainModel.Persons.Model
{
    public interface IPersonRepository
    {
        #region Reference Information
        /// <summary>
        /// A list of supported titles.  Not sure on this yet.
        /// </summary>
        /// <returns>A list of supported titles</returns>
        IList<Title> Titles();

        /// <summary>
        /// A list of supported genders.  Not sure on this yet.
        /// </summary>
        /// <returns>A list of supported genders.</returns>
        IList<Gender> Genders();

        /// <summary>
        /// A list of supported marital statuses.  Not sure on this yet.
        /// </summary>
        /// <returns>A list of supported marital statuses.</returns>
        IList<MaritalStatus> MaritalStatuses();

        #endregion

        #region Instance Information

        /// <summary>
        /// Fetch a person based on id.  
        /// </summary>
        /// <param name="broker">Commenting out till I determine if it is needed.</param>
        /// <param name="id">The id to fetch a person on</param>
        /// <returns>The person fetched</returns>
        Person Fetch(IBroker broker, int id);

        /// <summary>
        /// Delete a person based on id
        /// </summary>
        /// <param name="broker"></param>
        /// <param name="principal">Access and authorization token for user making the request.</param>
        /// <param name="person">Person to remove</param>
        /// <returns>The deleted person</returns>
        Person Delete(IBroker broker, IPrincipal principal, Person person);


        /// <summary>
        /// Save a person
        /// </summary>
        /// <param name="broker"></param>
        /// <param name="principal">Access and authorization token for user making the request.</param>
        /// <param name="person">The person to add.</param>
        /// <returns>The person saved.</returns>
        Person Save(IBroker broker, IPrincipal principal, Person person);
        
        #endregion
    }
}
