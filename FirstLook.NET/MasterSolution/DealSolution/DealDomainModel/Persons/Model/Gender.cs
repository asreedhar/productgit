﻿
namespace FirstLook.Deal.DomainModel.Persons.Model
{
    public class Gender
    {

        public string Id { get; internal set; }            

        public string Name { get; internal set; }

    }
}
