﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Persons
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Model.Constraints.Module>();
            registry.Register<Commands.Module>();
            registry.Register<Repositories.Module>();
        }
    }
}
