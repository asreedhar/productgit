﻿using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Repositories
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();
            registry.Register<Serializers.Module>();
            registry.Register<IPersonRepository, PersonRepository>(ImplementationScope.Shared);
        }

        #endregion
    }
}