﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core.Component;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Persons.Repositories.Mappers;


namespace FirstLook.Deal.DomainModel.Persons.Repositories
{
    public class PersonRepository : RepositoryBase, IPersonRepository
    {
        /// <summary>
        /// Initializ a person mapper responsible for speaking with the different table gateways.
        /// </summary>
        private readonly PersonMapper _mapper = new PersonMapper();

        /// <summary>
        /// Set database name
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        /// <summary>
        /// A list of supported titles.  Not sure on this yet.
        /// </summary>
        /// <returns>A list of supported titles</returns>
        public IList<Title> Titles()
        {
            return DoInSession(() => _mapper.Titles());
        }

        /// <summary>
        /// A list of supported genders.  Not sure on this yet.
        /// </summary>
        /// <returns>A list of supported genders.</returns>
        public IList<Gender> Genders()
        {
            return DoInSession(() => _mapper.Genders());
        }


        /// <summary>
        /// A list of supported marital statuses.  Not sure on this yet.
        /// </summary>
        /// <returns>A list of supported marital statuses.</returns>
        public IList<MaritalStatus> MaritalStatuses()
        {
            return DoInSession(() => _mapper.MaritalStatuses());
        }

        /// <summary>
        /// Fetch a person based on id.  
        /// </summary>
        /// <param name="broker">Commenting out till I determine if it is needed.</param>
        /// <param name="id">The id to fetch a person on</param>
        /// <returns>The person fetched</returns>
        public Person Fetch(IBroker broker, int id)
        {
            return DoInSession(() => _mapper.Fetch(broker,id));
        }

        /// <summary>
        /// Delete a person based on id
        /// </summary>
        /// <param name="broker"></param>
        /// <param name="principal">Access and authorization token for user making the request.</param>
        /// <param name="person">Person to remove</param>

        public Person Delete(IBroker broker, IPrincipal principal, Person person)
        {
            Person deletedPerson = DoInTransaction(() => _mapper.Delete(broker, person));

            ((IPersisted)deletedPerson).MarkDeleted();  //Mark the object deleted
            return deletedPerson;
        }

        /// <summary>
        /// Save a person
        /// </summary>
        /// <param name="broker"></param>
        /// <param name="principal">Access and authorization token for user making the request.</param>
        /// <param name="person">The person to add.</param>
        /// <returns>The person saved.</returns>
        public Person Save(IBroker broker, IPrincipal principal, Person person)
        {
            Person savedPerson = DoInTransaction(() => _mapper.Save(broker, person));

            ((IPersisted)savedPerson).MarkOld();        //Mark the object as old.
            return savedPerson;
        }

        /// <summary>
        /// Need to override deprecated method or will get compile errors.
        /// </summary>
        /// <param name="session"></param>
        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do nothing.
        }

    }
}