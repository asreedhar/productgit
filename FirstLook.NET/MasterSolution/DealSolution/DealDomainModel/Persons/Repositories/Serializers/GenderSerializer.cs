﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a gender record to a gender object.
    /// </summary>
    public class GenderSerializer : Serializer<Gender>
    {
        /// <summary>
        /// Deserialize a gender record to a gender object.
        /// </summary>
        /// <param name="record">Gender record to deserialize</param>
        /// <returns>Gender object.</returns>
        public override Gender Deserialize(IDataRecord record)
        {

            return new Gender{
                                    Id = record.GetString("Gender"),
                                    Name = record.GetString("GenderName")
                                };

        }
    }
}
