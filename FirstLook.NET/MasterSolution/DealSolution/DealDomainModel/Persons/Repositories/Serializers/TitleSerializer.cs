﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a title record into a title object.
    /// </summary>
    public class TitleSerializer : Serializer<Title>
    {
        /// <summary>
        /// Deserialize a title record into a title object.
        /// </summary>
        /// <param name="record">Title record to deserialize.</param>
        /// <returns>Title object.</returns>
        public override Title Deserialize(IDataRecord record)
        {
            return new Title
                       {
                           Name = record.GetString("TitleName"),
                           Id = record.GetInt32("TitleID", 0)                                    
            };

        }
    }
}
