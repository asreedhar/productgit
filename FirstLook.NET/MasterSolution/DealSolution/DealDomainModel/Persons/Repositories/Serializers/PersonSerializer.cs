﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a complete person record into a full person object graph object.
    /// </summary>
    public class PersonSerializer : Serializer<Person>
    {
        /// <summary>
        /// Deserialize a complete person record into a full person object graph object.
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public override Person Deserialize(IDataRecord record)
        {
            //Create gender object.
            Gender gender = new Gender
                                {
                                    Id = record.GetString("GenderCode"),
                                    Name = record.GetString("GenderName")
                                };

            //Create marital status object.
            MaritalStatus maritalStatus = new MaritalStatus
                                              {
                                                  Id = record.GetString("MaritalStatusCode"),
                                                  Name = record.GetString("MaritalStatusName")
                                              };

            //Create title object.
            Title title = new Title
                              {
                                  Id = record.GetInt32("TitleID", 0),                                   
                                  Name = record.GetString("Title")
                              };

            //Return a person object complete with all the fixins aside from Contact.
            return new Person
                       {
                           BirthDate = record.GetDateTime(record.GetOrdinal("BirthDate")),
                           FamilyName = record.GetString("Family"),
                           Gender = gender,
                           MaritalStatus = maritalStatus,
                           GivenName = record.GetString("Given"),
                           MiddleName = record.GetString("Middle"),
                           Occupation = record.GetString("Occupation"),
                           Id = record.GetInt32(record.GetOrdinal("PersonID")),
                           RevisionNumber = record.GetInt16(record.GetOrdinal("RevisionNumber")),
                           Title = title
                       };

        }

        
    }
}
