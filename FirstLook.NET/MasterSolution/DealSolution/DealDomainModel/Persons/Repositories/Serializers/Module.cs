﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<Person>, PersonSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Gender>, GenderSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Title>, TitleSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<MaritalStatus>, MaritalStatusSerializer>(ImplementationScope.Shared);

        }
    }
}
