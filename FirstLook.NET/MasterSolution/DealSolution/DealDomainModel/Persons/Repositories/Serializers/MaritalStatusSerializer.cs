﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Serializers
{
    /// <summary>
    /// Deserialize a marital status record to a marital status object.
    /// </summary>
    public class MaritalStatusSerializer : Serializer<MaritalStatus>
    {
        /// <summary>
        /// Deserialize a marital status record to a marital status object.
        /// </summary>
        /// <param name="record">Marial status record to deserialize.</param>
        /// <returns>MaritalStatus Object.</returns>
        public override MaritalStatus Deserialize(IDataRecord record)
        {
            return new MaritalStatus
            {
                Id = record.GetString("MaritalStatus"),
                Name = record.GetString("MaritalStatusName")
            };

        }
    }
}
