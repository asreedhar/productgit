﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Deal.DomainModel.Persons.Model;

namespace FirstLook.Deal.DomainModel.Persons.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        public IList<Title> Titles()
        {
            throw new NotImplementedException();
        }

        public IList<Gender> Genders()
        {
            throw new NotImplementedException();
        }

        public IList<MaritalStatus> MaritalStatuses()
        {
            throw new NotImplementedException();
        }

        public Person Fetch(IBroker broker, int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(IBroker broker, IPrincipal principal, int id, int revisionNumber)
        {
            throw new NotImplementedException();
        }

        public Person Save(IBroker broker, IPrincipal principal, Person person)
        {
            throw new NotImplementedException();
        }
    }
}