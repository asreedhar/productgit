﻿using System;
using System.Data;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Datastores
{
    /// <summary>
    /// Define datastore for a person
    /// </summary>
    public interface IPersonDatastore
    {
        IDataReader Person_Fetch(int clientId, int personId);
        IDataReader Person_Save(int clientId);
        bool        Person_Delete(int clientId, int personId);

        bool        Demographics_Save(int personId, DateTime? birthDate, string occupation, string maritalStatusId, string genderId);
        bool        Demographics_Update(int personId, DateTime? birthDate, string occupation, string maritalStatusId, string genderId);
        bool        Demographics_Delete(int personId);
        IDataReader Demographics_Fetch(int personId);

        
        bool        Name_Save(int personId, int titleId, string givenName, string middleName, string familyName);
        bool        Name_Update(int personId, int titleId, string givenName, string middleName, string familyName);
        bool        Name_Delete(int personId);
        IDataReader Name_Fetch(int personId);


        bool        Contact_Save(int personId, int contactId);
        bool        Contact_Update(int personId, int contactId);
        bool        Contact_Delete(int personId);
        IDataReader Contact_Fetch(int personId);

        bool        IncrementRevisionNumber(int personId, int revisionNumber, int clientId);       

        IDataReader Titles_Fetch();
        IDataReader Genders_Fetch();
        IDataReader MaritalStatuses_Fetch();

    }
}
