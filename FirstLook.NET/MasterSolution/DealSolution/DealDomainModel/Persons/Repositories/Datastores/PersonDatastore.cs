﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Datastores
{
    internal class PersonDatastore : SessionDataStore, IPersonDatastore
    {
        #region Miscellaneous

        internal const string Prefix = "FirstLook.Deal.DomainModel.Persons.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion Miscellaneous

        /// <summary>
        /// Fetch a person based on id
        /// </summary>
        /// <param name="clientId">Client associated with the person</param>
        /// <param name="personId">The id to fetch on</param>
        /// <returns>The info required to build a person object</returns>
        public IDataReader Person_Fetch(int clientId, int personId)
        {                                             
            const string queryName = Prefix + ".PersonDatastore_Person_Fetch_Id.txt";

            return Query(new[] { "Person" },
                         queryName,
                         new Parameter("PersonID", personId, DbType.Int32),
                         new Parameter("ClientID", clientId, DbType.Int32));
        }

        /// <summary>
        /// Save a person
        /// </summary>
        /// <param name="clientId">The client id associated with the person</param>
        /// <returns>The saved person</returns>
        public IDataReader Person_Save(int clientId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Person_Insert.txt";

            const string queryNameF = Prefix + ".PersonDatastore_Person_Fetch_Identity.txt";

            NonQuery(
               queryNameI,
               new Parameter("ClientID", clientId, DbType.Int32),
               new Parameter("RevisionNumber", 1, DbType.Int32));

            return Query(
                new[] { "Person" },
                queryNameF); 
        }

        /// <summary>
        /// Update a person 
        /// </summary>
        /// <param name="personId">Id of the person to update</param>
        /// <param name="revisionNumber">Revision Number of the person to update</param>
        /// <param name="clientId">Client Id associated with person.</param>
        /// <returns>The updated person</returns>
        public bool IncrementRevisionNumber(int personId, int revisionNumber, int clientId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Person_Update.txt";
            
            int count =
               NonQuery(
               queryNameI,
               new Parameter("PersonID", personId, DbType.Int32),
               new Parameter("RevisionNumber", revisionNumber, DbType.Int32),
               new Parameter("ClientID", clientId, DbType.Int32));

            if (count != 1)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Delete a person by id
        /// </summary>
        /// <param name="clientId">The client associated with the person</param>
        /// <param name="personId">The id of the person to delete</param>
        public bool Person_Delete(int clientId, int personId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Person_Delete.txt";

            int count = NonQuery(
                   queryNameI,
                   new Parameter("PersonID", personId, DbType.Int32),
                   new Parameter("ClientID", clientId, DbType.Int32));

            if(count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Save the demographics associated with a person
        /// </summary>
        /// <param name="personId">The id of the person associated with this entry</param>
        /// <param name="birthDate">Birth date of person to be saved.</param>
        /// <param name="occupation">Occupation of person to be saved.</param>
        /// <param name="maritalStatusId">Marital Status of person to be saved</param>
        /// <param name="genderId">Gender of person to save</param>
        /// <returns>True or false depending on if record was inserted.</returns>

        public bool Demographics_Save(int personId, DateTime? birthDate, string occupation, string maritalStatusId, string genderId)
        {

            const string queryNameI = Prefix + ".PersonDatastore_Demographics_Insert.txt";

            int count =  NonQuery(
                        queryNameI,
                        new Parameter("PersonID",personId, DbType.Int32),
                        new Parameter("BirthDate", birthDate, DbType.DateTime),
                        new Parameter("Occupation", occupation, DbType.String),
                        new Parameter("MaritalStatusCode", maritalStatusId, DbType.String),
                        new Parameter("GenderCode", genderId, DbType.String));

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update person demographics info
        /// </summary>
        /// <param name="personId">Id of the person to update</param>
        /// <param name="birthDate">Birth date of person to be saved.</param>
        /// <param name="occupation">Occupation of person to be saved.</param>
        /// <param name="maritalStatusId">Marital Status of person to be saved</param>
        /// <param name="genderId">Gender of person to save</param>
        /// <returns>True or false depending on if record was updated.</returns>

        public bool Demographics_Update(int personId, DateTime? birthDate, string occupation, string maritalStatusId, string genderId)
        {

            const string queryNameI = Prefix + ".PersonDatastore_Demographics_Update.txt";

            int count = NonQuery(
                        queryNameI,
                        new Parameter("PersonID", personId, DbType.Int32),
                        new Parameter("BirthDate", birthDate, DbType.DateTime),
                        new Parameter("Occupation", occupation, DbType.String),
                        new Parameter("MaritalStatusCode", maritalStatusId, DbType.String),
                        new Parameter("GenderCode", genderId, DbType.String));

            if (count > 0)
            {
                return true;
            }

            return false;

        }

  
        /// <summary>
        /// Fetch the supported genders
        /// </summary>
        /// <returns>Rows of Genders</returns>
        public IDataReader Genders_Fetch()
        {
            const string queryNameI = Prefix + ".PersonDatastore_Genders_Fetch.txt";
            return Query(
                new[] { "Gender" },
                queryNameI);
        }


        /// <summary>
        /// Fetch the supported marital statuses
        /// </summary>
        /// <returns>Rows of Marital Statuses</returns>
        public IDataReader MaritalStatuses_Fetch()
        {
            const string queryNameI = Prefix + ".PersonDatastore_MaritalStatuses_Fetch.txt";
            return Query(
                new[] { "MaritalStatus" },
                queryNameI);
        }


        /// <summary>
        /// Save Name information associated with the person
        /// </summary>
        /// <param name="personId">Id of person being saved</param>
        /// <param name="titleId">Title of person</param>
        /// <param name="givenName">First name</param>
        /// <param name="middleName">Middle name</param>
        /// <param name="familyName">Last name</param>
        /// <returns>True or false depending on if record was inserted.</returns>
      
        public bool Name_Save(int personId, int titleId, string givenName, string middleName, string familyName)
        {

            const string queryNameI = Prefix + ".PersonDatastore_Name_Insert.txt";

            int count = NonQuery(
                        queryNameI,
                        new Parameter("PersonID", personId, DbType.Int32),
                        new Parameter("TitleID", titleId, DbType.Byte),
                        new Parameter("Given", givenName, DbType.String),
                        new Parameter("Middle", middleName, DbType.String),
                        new Parameter("Family", familyName, DbType.String));
            
            if (count > 0)
            {
                return true;
            }

            return false;

        }

        /// <summary>
        /// Update name information associated with a person.
        /// </summary>
        /// <param name="personId">Id of person to be saved</param>
        /// <param name="titleId">Title of person</param>
        /// <param name="givenName">First name</param>
        /// <param name="middleName">Middle name</param>
        /// <param name="familyName">Last name</param>
        /// <returns>True or false depending on if record was updated.</returns>
        
        public bool Name_Update(int personId, int titleId, string givenName, string middleName, string familyName)
        {

            const string queryNameI = Prefix + ".PersonDatastore_Name_Update.txt";

            int count =  NonQuery(
                        queryNameI,
                        new Parameter("PersonID", personId, DbType.Int32),
                        new Parameter("TitleID", titleId, DbType.Byte),
                        new Parameter("Given", givenName, DbType.String),
                        new Parameter("Middle", middleName, DbType.String),
                        new Parameter("Family", familyName, DbType.String));

            if (count > 0)
            {
                return true;
            }

            return false;

        }

   
        /// <summary>
        /// Fetch the supported titles.
        /// </summary>
        /// <returns>Rows of Titles</returns>
        public IDataReader Titles_Fetch()
        {
            const string queryNameI = Prefix + ".PersonDatastore_Titles_Fetch.txt";
            return Query(
                new[] { "Title" },
                queryNameI); 
        }

        /// <summary>
        /// Save Conatct reference information
        /// </summary>
        /// <param name="personId">Id of person being saved</param>
        /// <param name="contactId">ContactId of person being saved</param>
        /// <returns>True or false depending on if record was inserted.</returns>

        public bool Contact_Save(int personId, int contactId)
        {

            const string queryNameI = Prefix + ".PersonDatastore_Contact_Insert.txt";

            int count = NonQuery(
                        queryNameI,
                        new Parameter("PersonID", personId, DbType.Int32),
                        new Parameter("ContactID", contactId, DbType.Int32));
            
            if (count > 0)
            {
                return true;
            }

            return false;

        }

        /// <summary>
        /// Fetch Conatct reference information
        /// </summary>
        /// <param name="personId">Id of person related to contact info</param>
        /// <returns>ContactID.</returns>

        public IDataReader Contact_Fetch(int personId)
        {

            const string queryNameI = Prefix + ".PersonDatastore_Contact_Fetch.txt";

            return Query(new[] { "Person" },
                         queryNameI,
                         new Parameter("PersonID", personId, DbType.Int32));

        }

        /// <summary>
        /// Update Conatct reference information
        /// </summary>
        /// <param name="personId">Id of person being updated</param>
        /// <param name="contactId">ContactId of person being updated</param>
        /// <returns>True or false depending on if record was updated.</returns>

        public bool Contact_Update(int personId, int contactId)
        {

            const string queryNameI = Prefix + ".PersonDatastore_Contact_Update.txt";

            int count = NonQuery(
                        queryNameI,
                        new Parameter("PersonID", personId, DbType.Int32),
                        new Parameter("ContactID", contactId, DbType.Int32));

            if (count > 0)
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// Delete a demographic
        /// </summary>
        /// <param name="personId">The id of the person to delete</param>
        public bool Demographics_Delete(int personId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Demographics_Delete.txt";

            int count = NonQuery(
                   queryNameI,
                   new Parameter("PersonID", personId, DbType.Int32));

            if (count > 0)
            {
                return true;
            }

            return false;

        }

        /// <summary>
        /// Fetch a demographic
        /// </summary>
        /// <param name="personId">The id of the person to fetch</param>
        public IDataReader Demographics_Fetch(int personId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Demographics_Fetch.txt";

            return Query(new[] { "Person" },
                         queryNameI,
                         new Parameter("PersonID", personId, DbType.Int32));

        }

        /// <summary>
        /// Fetch a Name
        /// </summary>
        /// <param name="personId">The id of the person to fetch</param>
        public IDataReader Name_Fetch(int personId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Name_Fetch.txt";

            return Query(new[] { "Person" },
                         queryNameI,
                         new Parameter("PersonID", personId, DbType.Int32));

        }


        /// <summary>
        /// Delete a name
        /// </summary>
        /// <param name="personId">The id of the person to delete</param>
        public bool Name_Delete(int personId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Name_Delete.txt";

            int count = NonQuery(
                   queryNameI,
                   new Parameter("PersonID", personId, DbType.Int32));

            if (count > 0)
            {
                return true;
            }

            return false;

        }

        /// <summary>
        /// Delete a contact reference
        /// </summary>
        /// <param name="personId">The id of the person to delete</param>
        public bool Contact_Delete(int personId)
        {
            const string queryNameI = Prefix + ".PersonDatastore_Contact_Delete.txt";

            int count = NonQuery(
                   queryNameI,
                   new Parameter("PersonID", personId, DbType.Int32));

            if (count > 0)
            {
                return true;
            }

            return false;

        }

    }
}
