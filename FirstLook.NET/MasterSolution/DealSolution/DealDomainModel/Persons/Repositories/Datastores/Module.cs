﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IPersonDatastore, PersonDatastore>(ImplementationScope.Shared);
        }
    }
}
