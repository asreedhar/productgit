﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core.Component;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Gateways;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Mappers
{
    /// <summary>
    /// This mapper class will interface with the gateways to interact with the database. It contains the application logic
    /// to insert a fault into the database.
    /// </summary>
    public class PersonMapper
    {
        private readonly PersonGateway _personGateway = new PersonGateway();

        private readonly GenderGateway _genderGateway = new GenderGateway();

        private readonly MaritalStatusGateway _maritalStatusGateway = new MaritalStatusGateway();

        private readonly TitleGateway _titleGateway = new TitleGateway();

        private readonly DemographicsGateway _demographicsGateway = new DemographicsGateway();

        private readonly NameGateway _nameGateway = new NameGateway();

        private readonly ContactGateway _contactGateway = new ContactGateway();

        /// <summary>
        /// Fetch supported titles.
        /// </summary>
        /// <returns>List of supported titles.</returns>
        public IList<Title> Titles()
        {
            return _titleGateway.Fetch();
        }

        /// <summary>
        /// Fetch supported genders.
        /// </summary>
        /// <returns>List of supported genders.</returns>
        public IList<Gender> Genders()
        {
            return _genderGateway.Fetch();
        }

        /// <summary>
        /// Fetch supported marital statuses.
        /// </summary>
        /// <returns>List of supported marital statuses.</returns>
        public IList<MaritalStatus> MaritalStatuses()
        {
            return _maritalStatusGateway.Fetch();
        }

        /// <summary>
        /// Fetch a person by broker and personId
        /// </summary>
        /// <param name="broker">Broker</param>
        /// <param name="personId">Id of person to fetch.</param>
        /// <returns>Fetched person</returns>
        public Person Fetch(IBroker broker, int personId)
        {
            Person person = _personGateway.Fetch(broker, personId);
            
            if (person != null)
            {
                ((IPersisted)person).MarkOld();
            }

            return person;
        }

        /// <summary>
        /// Fetch a person by broker and personId, but since it is invoked from save, do not mark it as old until save transaction completes.
        /// </summary>
        /// <param name="broker">Broker</param>
        /// <param name="personId">Id of person to fetch.</param>
        /// <returns>Fetched person</returns>
        public Person FetchSave(IBroker broker, int personId)
        {
            return _personGateway.Fetch(broker, personId);
        }
   
        /// <summary>
        /// Save a person based on broker, principal, and person object to save.
        /// </summary>
        /// <param name="broker">Broker</param>
        /// <param name="person">Person object to save.</param>
        /// <returns>Saved Person.</returns>
        public Person Save(IBroker broker, Person person)
        {

            if (person == null || !person.IsValid)                  //Is this a valid object? If not then throw an argument exception
            {
                throw new ArgumentException();
            }
        
            if (!((IPersisted)person).IsDirty)                      //Has this object been modified?
            {
                return person;                                      //If not return the existing person as there is nothing to do.
            }

            if(((IPersisted)person).IsDeleted)                      //Is this person marked for deletion?
            {
                return Delete(broker, person);    
            }

            if(((IPersisted)person).IsNew)                          //Is this a new person?
            {
                person.Id = _personGateway.Save(broker);            //If so then save a new person
                _demographicsGateway.Save(person);
                _nameGateway.Save(person);
                _contactGateway.Save(broker,person);               

            }
            else
            {                                                       //Otherwise update an existing person
                _personGateway.Update(broker, person);
                _demographicsGateway.Update(person);
                _nameGateway.Update(person);
                _contactGateway.Update(broker,person);             
            }

            return FetchSave(broker, person.Id);                    //Return saved/updated person

        }


        /// <summary>
        /// Delete a person based on broker, principal, personId and the revision number.
        /// </summary>
        /// <param name="broker">Broker</param>
        /// <param name="person">Person to delete.</param>
 
        public Person Delete(IBroker broker, Person person)
        {

            Person existingPerson = Fetch(broker, person.Id);   //Check if person exists

            if (existingPerson == null)                         //Does this person even exist?
            {
                return null;                                    //No, nothing to delete
            }

            if(person.RevisionNumber != existingPerson.RevisionNumber)              //Do these revision numbers match?
            {
                throw new ConcurrentModificationException("Deleting a person.");    //No, can't delete
            }

            _personGateway.Update(broker, person);
            _demographicsGateway.Delete(person.Id);
            _nameGateway.Delete(person.Id);
            _contactGateway.Delete(existingPerson, broker);       
            _personGateway.Delete(person.Id, person.RevisionNumber, broker);

            return existingPerson;

        }

        
    }
}
