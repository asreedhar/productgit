﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Gateways
{
    /// <summary>
    /// Contact gateway
    /// </summary>
    class ContactGateway : GatewayBase
    {
        /// <summary>
        /// Save conatct reference.
        /// </summary>
        /// <param name="broker">Broker associated with person</param>
        /// <param name="person">Person object containing contact information to save</param>
        /// <returns></returns>
        public void Save(IBroker broker, Person person)
        {
            IContactRepository contactRepository = RegistryFactory.GetResolver().Resolve<IContactRepository>();

            if(person.Contact == null)
            {
                return;
            }

            Contact contact = contactRepository.Save(broker, person.Contact);     //Save contact information

            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            
            bool saveContact = datastore.Contact_Save(person.Id, contact.Id);

            if(!saveContact)
            {
                throw new ArgumentException("Error saving contact reference."); 
            }
           
        }

        /// <summary>
        /// Update contacts.
        /// </summary>
        /// <param name="broker">Broker associated with person</param>
        /// <param name="person">Person object containing contact information to update</param>
        /// <returns></returns>
        public void Update(IBroker broker, Person person)
        {
            IContactRepository contactRepository = RegistryFactory.GetResolver().Resolve<IContactRepository>();

            //If we're updating and there is no contact reference, then it may have been removed.
            if(person.Contact == null)
            {
                int contactId = Fetch(person.Id);           //Fetch the contact Id associated with the person
                if(contactId != 0)                          //If one exists then fetch the associated contact
                {
                    Contact toDelete = contactRepository.Fetch(broker, contactId);
                    if (toDelete != null)
                    {
                        contactRepository.Delete(broker, toDelete);   //Delete the object as the contact has been wiped out
                    }
                }
                Delete(person,broker);                             //Delete the contact reference
                return;                 
            }

            Contact contact = contactRepository.Save(broker, person.Contact);     //Save contact information

            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            bool updateContact = datastore.Contact_Update(person.Id, contact.Id);

            if (!updateContact)
            {
                throw new ArgumentException("Error updating contact reference.");
            }

        }

        /// <summary>
        /// Delete contacts.
        /// </summary>
        /// <param name="person">Person object containing contact information to delete</param>
        /// <param name="broker">broker</param>
        /// <returns></returns>
        public void Delete(Person person, IBroker broker)
        {
            IContactRepository contactRepository = RegistryFactory.GetResolver().Resolve<IContactRepository>();

            if(person.Contact == null)
            {
                return;                     //Nothing to delete
            }
            contactRepository.Delete(broker, person.Contact);

            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            datastore.Contact_Delete(person.Id);

        }

        /// <summary>
        /// Fetch contact.
        /// </summary>
        /// <param name="personId">Id of person to fetch contacts for</param>
        /// <returns>Contact Id</returns>
        public int Fetch(int personId)
        {
            int contactId = 0;
            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            
            using (IDataReader reader = datastore.Contact_Fetch(personId))
            {                
                while (reader.Read())
                {
                    contactId = reader.GetInt32(reader.GetOrdinal("ContactID"));
                }
            }
            return contactId;
        }

    }
}
