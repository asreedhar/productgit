﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Gateways
{
    /// <summary>
    /// Name gateway
    /// </summary>
    class NameGateway : GatewayBase
    {
        /// <summary>
        /// Save Name information.
        /// </summary>
        /// <param name="person">Person object containing name information to save</param>
        /// <returns></returns>
        public void Save(Person person)
        {

            //If there is no title, no given name, no middle name, and no family name, then just return, there is no information to save regarding this user.
            if (person.Title.Id == 0 && string.IsNullOrEmpty(person.GivenName) && string.IsNullOrEmpty(person.MiddleName) && string.IsNullOrEmpty(person.FamilyName))
            {
                return;
            } 

            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            bool saveName = datastore.Name_Save(person.Id, person.Title.Id, person.GivenName, person.MiddleName, person.FamilyName);  

            if(!saveName)
            {
                throw new ArgumentException("Error saving name."); 
            }
           
        }

        /// <summary>
        /// Update Name information.
        /// </summary>
        /// <param name="person">Person object containing name information to update</param>
        /// <returns></returns>
        public void Update(Person person)
        {
            bool updateName;
            //If there is no title, no given name, no middle name, and no family name then just remove the name record for this personId entirely
            if (person.Title.Id == 0 && string.IsNullOrEmpty(person.GivenName) && string.IsNullOrEmpty(person.MiddleName) && string.IsNullOrEmpty(person.FamilyName))
            {
                Delete(person.Id);
                return;
            } 

            IPersonDatastore datastore = Resolve<IPersonDatastore>();

            if (Fetch(person.Id) == 0)
            {
                updateName = datastore.Name_Save(person.Id, person.Title.Id, person.GivenName, person.MiddleName, person.FamilyName);  
            }
            else
            {
                updateName = datastore.Name_Update(person.Id, person.Title.Id, person.GivenName, person.MiddleName,
                                                        person.FamilyName);
            }

            if (!updateName)
            {
                throw new ArgumentException("Error updating name."); 
            }

        }

        /// <summary>
        /// Delete name.
        /// </summary>
        /// <param name="personId">Id of person name to delete.</param>
        /// <returns></returns>
        public void Delete(int personId)
        {
            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            datastore.Name_Delete(personId);            
        }


        /// <summary>
        /// Fetch Name.
        /// </summary>
        /// <param name="personId">Id of person Name to fetch.</param>
        /// <returns>Was a record fetched</returns>
        public int Fetch(int personId)
        {
            int count = 0;
            IPersonDatastore datastore = Resolve<IPersonDatastore>();

            using (IDataReader reader = datastore.Name_Fetch(personId))
            {
                while (reader.Read())
                {
                    count = reader.GetInt32(reader.GetOrdinal("Count"));
                }
            }
            return count;
        }

    }
}
