﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Gateways
{
    /// <summary>
    /// Title gateway.
    /// </summary>
    class TitleGateway : GatewayBase
    {      
        /// <summary>
        /// Fetch list of supported titles.
        /// </summary>
        /// <returns>List of supported titles.</returns>
        public IList<Title> Fetch()
        {
            string key = CreateCacheKey("titles");
            IList<Title> entity = Cache.Get(key) as List<Title>;

            if (entity == null)
            {
                IPersonDatastore datastore = Resolve<IPersonDatastore>();
                entity = new List<Title>();
                using (IDataReader reader = datastore.Titles_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<Title> serializer = Resolve<ISerializer<Title>>();
                        entity.Add(serializer.Deserialize((IDataRecord) reader));
                    }

                    Remember(key,entity);
                }
            }
            return entity;
        }

    }
}
