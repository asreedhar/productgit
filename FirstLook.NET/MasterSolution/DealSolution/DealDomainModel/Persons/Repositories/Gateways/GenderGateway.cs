﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Gateways
{
    /// <summary>
    /// Gender gateway.
    /// </summary>
    class GenderGateway : GatewayBase
    {
        /// <summary>
        /// Fetch supported genders.
        /// </summary>
        /// <returns>List of supported genders.</returns>
        public IList<Gender> Fetch()
        {
            string key = CreateCacheKey("genders");
            IList<Gender> entity = Cache.Get(key) as IList<Gender>;

            if (entity == null)
            {
                IPersonDatastore datastore = Resolve<IPersonDatastore>();
                entity = new List<Gender>();
                using (IDataReader reader = datastore.Genders_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<Gender> serializer = Resolve<ISerializer<Gender>>();
                        entity.Add(serializer.Deserialize((IDataRecord) reader));
                    }
                    Remember(key, entity);
                }

            }

            return entity;
        }

    }
}
