﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Gateways
{
    /// <summary>
    /// MaritalStatus gateway.
    /// </summary>
    class MaritalStatusGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a list of supported Marital Statuses.
        /// </summary>
        /// <returns>List of supported marital statuses.</returns>
        public IList<MaritalStatus> Fetch()
        {
            string key = CreateCacheKey("maritalstatuses");
            IList<MaritalStatus> entity = Cache.Get(key) as List<MaritalStatus>;

            if (entity == null)
            {
                IPersonDatastore datastore = Resolve<IPersonDatastore>();
                entity = new List<MaritalStatus>();
                using (IDataReader reader = datastore.MaritalStatuses_Fetch())
                {
                    while (reader.Read())
                    {
                        ISerializer<MaritalStatus> serializer = Resolve<ISerializer<MaritalStatus>>();
                        entity.Add(serializer.Deserialize((IDataRecord) reader));
                    }

                    Remember(key,entity);
                }
            }
            return entity;
        }

    }
}
