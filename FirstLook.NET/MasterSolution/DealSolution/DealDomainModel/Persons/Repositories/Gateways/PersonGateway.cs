﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Component;
using FirstLook.Common.Core.Registry;
using FirstLook.Deal.DomainModel.Contacts.Model;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Gateways
{
    /// <summary>
    /// Person gateway
    /// </summary>
    class PersonGateway : GatewayBase
    {
        /// <summary>
        /// Fetch a person based on broker and personId
        /// </summary>
        /// <param name="broker">Broker</param>
        /// <param name="personId">Id of person to fetch</param>
        /// <returns>Person</returns>
        public Person Fetch(IBroker broker, int personId)           
        {
            string key = CreateCacheKey(broker.Id,personId);        
            Person entity = Cache.Get(key) as Person;

            if (entity == null)
            {
                IPersonDatastore datastore = Resolve<IPersonDatastore>();
                IContactRepository repository = RegistryFactory.GetResolver().Resolve<IContactRepository>();

                using (IDataReader reader = datastore.Person_Fetch(broker.Id, personId))      
                {
                    if (reader.Read())
                    {
                        ISerializer<Person> serializer = Resolve<ISerializer<Person>>();
                        entity = serializer.Deserialize((IDataRecord) reader);

                        Contact contact = repository.Fetch(broker, reader.GetOrdinal("ContactID"));  

                        if (contact != null)
                        {
                            entity.Contact = contact;
                        }

                        Remember(key, entity);
                    }
                    
                }
            }

            return entity;

        }

        /// <summary>
        /// Create a person entry in the Person table and return the Id that is assocaited with the new person.
        /// </summary>
        /// <param name="broker">Broker</param>
        /// <returns>New Id of inserted person.</returns>
        public int Save(IBroker broker)
        {
            
            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            int personId;

            using (IDataReader reader = datastore.Person_Save(broker.Id))           
            {
                if (reader.Read())
                {
                    personId = reader.GetInt32(reader.GetOrdinal("PersonID"));                    
                }
                else
                {
                    throw new ArgumentException("Error saving person.");
                }
            }

            return personId;
        }

        /// <summary>
        /// Update the revision number associated with this person to lock out any concurrent updates.
        /// </summary>
        /// <param name="broker">Broker associated with person</param>
        /// <param name="person">Person to update</param>
        public void Update(IBroker broker, Person person)
        {
            string key = CreateCacheKey(broker.Id, person.Id);             //Clear it from the cache
            Forget(key);

            IPersonDatastore datastore = Resolve<IPersonDatastore>();

            bool safeUpdate = datastore.IncrementRevisionNumber(person.Id, person.RevisionNumber,broker.Id);             
            
            if(!safeUpdate)
            {
                throw new ConcurrentModificationException("Concurrent modification failure.");
            } 

        }


        /// <summary>
        /// Delete person via personId.
        /// </summary>
        /// <param name="personId">Id of person to be deleted.</param>
        /// <param name="revisionNumber">RevisionNumber of person object in question.</param>
        /// <param name="broker">Broker associated with person.</param>
        public void Delete(int personId, int revisionNumber, IBroker broker)
        {

            string key = CreateCacheKey(broker.Id, personId);             //Clear it from the cache
            Forget(key);

            IPersonDatastore datastore = Resolve<IPersonDatastore>();
           
            bool deletePerson = datastore.Person_Delete(broker.Id, personId);       

            if (!deletePerson)
            {
                throw new ArgumentException("Error deleting person.");
            }

        }
    }
}
