﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Deal.DomainModel.Persons.Model;
using FirstLook.Deal.DomainModel.Persons.Repositories.Datastores;

namespace FirstLook.Deal.DomainModel.Persons.Repositories.Gateways
{
    /// <summary>
    /// Demographics gateway
    /// </summary>
    class DemographicsGateway : GatewayBase
    {
        /// <summary>
        /// Save demographics.
        /// </summary>
        /// <param name="person">Person object containing demographics information to save</param>
        /// <returns></returns>
        public void Save(Person person)
        {

            //If the birthdate is null, no occupation is specified, no marital status, and no gender, then just return as there is no information to save for this user.
            if (person.BirthDate == null && string.IsNullOrEmpty(person.Occupation) && string.IsNullOrEmpty(person.MaritalStatus.Id) && string.IsNullOrEmpty(person.Gender.Id))
            {
                return;
            } 
            
            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            bool saveDemographic = datastore.Demographics_Save(person.Id, person.BirthDate, person.Occupation, person.MaritalStatus.Id, person.Gender.Id);

            if(!saveDemographic)
            {
                throw new ArgumentException("Error saving demographic.");
            }
           
        }

        /// <summary>
        /// Update demographics.
        /// </summary>
        /// <param name="person">Person object containing demographics information to update</param>
        /// <returns></returns>
        public void Update(Person person)
        {
            //If the birthdate is null, no occupation is specified, no marital status, and no gender, then delete the demographic information for this user.
            if (person.BirthDate == null && string.IsNullOrEmpty(person.Occupation) && string.IsNullOrEmpty(person.MaritalStatus.Id) && string.IsNullOrEmpty(person.Gender.Id))
            {
                Delete(person.Id);
                return;
            } 

            IPersonDatastore datastore = Resolve<IPersonDatastore>();

            bool updateDemographic = Fetch(person.Id) == 0 ? datastore.Demographics_Save(person.Id, person.BirthDate, person.Occupation, person.MaritalStatus.Id, person.Gender.Id) : datastore.Demographics_Update(person.Id, person.BirthDate, person.Occupation, person.MaritalStatus.Id, person.Gender.Id);

            if (!updateDemographic)
            {
                throw new ArgumentException("Error updating demographic.");
            }

        }

        /// <summary>
        /// Delete demographics.
        /// </summary>
        /// <param name="personId">Id of person demographic to delete.</param>
        /// <returns></returns>
        public void Delete(int personId)
        {
            IPersonDatastore datastore = Resolve<IPersonDatastore>();
            datastore.Demographics_Delete(personId);
        }

        /// <summary>
        /// Fetch demographics.
        /// </summary>
        /// <param name="personId">Id of person demographic to fetch.</param>
        /// <returns>Was a record fetched</returns>
        public int Fetch(int personId)
        {
            int count = 0;
            IPersonDatastore datastore = Resolve<IPersonDatastore>();

            using (IDataReader reader = datastore.Demographics_Fetch(personId))
            {
                while (reader.Read())
                {
                    count = reader.GetInt32(reader.GetOrdinal("Count"));
                }
            }
            return count;
        }

    }
}
