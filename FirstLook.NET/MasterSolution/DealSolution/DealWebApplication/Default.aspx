﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstLook.Deal.WebApplication.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contents</title>
</head>
<body>
    <form id="form1" runat="server">
    <ul>
            <li>Pages
                <ul>
                    <li>
                        <asp:HyperLink ID="ContactPage" NavigateUrl="~/Pages/Contacts/TestDesktop.aspx" runat="server"
                            Text="Contact" /></li>
                    <li>
                        <asp:HyperLink ID="DealPage" NavigateUrl="~/Pages/Deals/TestDesktop.aspx"
                            runat="server" Text="Deal" /></li>
                    <li>
                        <asp:HyperLink ID="PartyPage" NavigateUrl="~/Pages/Parties/TestDesktop.aspx" runat="server"
                            Text="Party" /></li>
                </ul>
            </li>
            <li>Services
                <ul>
                    <li>
                        <asp:HyperLink ID="ContactService" NavigateUrl="~/Services/Contacts.asmx" runat="server"
                            Text="Contact" /></li>
                    <li>
                        <asp:HyperLink ID="DealService" NavigateUrl="~/Services/Deals.asmx" runat="server"
                            Text="Deal" /></li>
                    <li>
                        <asp:HyperLink ID="PartyService" NavigateUrl="~/Services/Parties.asmx" runat="server"
                            Text="Party" /></li>
                    <li>
                        <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Services/Employees.asmx" runat="server"
                            Text="Employee" /></li>
                </ul>
            </li>
        </ul>
    </form>
</body>
</html>
