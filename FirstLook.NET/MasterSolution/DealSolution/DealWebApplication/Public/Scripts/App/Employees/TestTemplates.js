if (typeof Employee === "undefined") {
    var Employee = {};
}
(function() {
    this.raises = this.raises || [];
    this.raises.push("Employee.BrokerChange");
    this.raises.push("Employee.Load");
    this.raises.push("Employee.Delete");
    this.raises.push("Employee.Search");
    this.raises.push("Employee.Save");


    this.listensTo = this.listensTo || [];
    this.listensTo.push("Employee.SampleLoaded");
    this.listensTo.push("Employee.Loaded");
    this.listensTo.push("Employee.Initialize");
    this.listensTo.push("Employee.Searched");
    this.listensTo.push("Employee.Saved");


    var PublicEvents = {
        "Employee.Loaded": function(evt, d) {
            var template = new EmployeeTemplateBuilder(d);
            template.clean();
            template.init();
        },
        "Employee.Initialize": function(evt, d) {
            var template = new EmployeeTemplateBuilder();
            template.clean();
            template.init();
        },
        "Employee.ReferenceInformationLoaded": function(evt, d) {
            var template = new RolesTemplateBuilder(d);
        },
        "Employee.Deleted": function(evt, d) {
            if(d.Success == true){
                alert("Employee Deleted!");
            }
        },
        "Employee.Searched": function(evt, d) {
           var template = new ResultsBuilder(d);
           template.clean();
           template.build();
        }


    };
    $(Employee).bind(PublicEvents);

    var ResultsBuilder = function (d) {
        this.d = d;
        this.$dom = {
            fieldsetResults: $('#results')
        };
    };

    ResultsBuilder.prototype = {
        build:  function (){
                    var i;
                    for(i=0; i < this.d.TotalRowCount; i++){

                        var y;
                        var a = "N";
                        var b = "N";
                        var s = "N";

                        for(y=0; y < this.d.Results[i].Roles.length; y++){
                            if(this.d.Results[i].Roles[y].Name == "Appraiser"){ a = "Y"; }
                            if(this.d.Results[i].Roles[y].Name == "Buyer"){ b = "Y"; }
                            if(this.d.Results[i].Roles[y].Name == "Salesperson"){ s = "Y"; }
                        } 
                        $("#employees > tbody:last").append(
                                "<tr><td>Mr.</td><td>" + this.d.Results[i].Person.GivenName + "</td><td>" + this.d.Results[i].Person.MiddleName + "</td><td>" + this.d.Results[i].Person.FamilyName + '</td><td>' + a +
                                 "</td><td>"+ b + "</td><td>" + s + '</td><td class="action"><a href="#edit' + this.d.Results[i].Id  + "_" + this.d.Results[i].RevisionNumber + '">Edit</a></td><td class="action"><a href="#delete' + this.d.Results[i].Id  + "_" + this.d.Results[i].RevisionNumber + '">Delete</a></td><td>' + this.d.Results[i].Id + "_" + this.d.Results[i].RevisionNumber +'</tr>');                           
                                             
                    }

                   this.$dom.fieldsetResults.bind('click', $.proxy(this.modifyEmployee, this));        
            
                }, 

        modifyEmployee: function(evt){
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                c = null,
                e = null,
                v = null;
                if (n.substring(0,5) == "#edit") {
                    $(Employee).trigger("Employee.Load", n.substring(5,n.lastIndexOf('_')));
                }
                if (n.substring(0,7) == '#delete') {
                    v = {
                        Id: n.substring(7,n.lastIndexOf('_')),
                        RevisionNumber: n.substring(n.lastIndexOf('_')+1)
                    };
                    $(Employee).trigger("Employee.Delete", v);
                }
            }
        },
        
        clean: function() {
            this.$dom.fieldsetResults.unbind('click');
        }
    }


    var RolesTemplateBuilder = function(d){                 //This function is just here to make sure we are retrieving results from a referenceinformation ws call
        this.d = d;
    };

    var EmployeeTemplateBuilder = function(d) {
        this.d = d;
        this.$el = $("#EmployeeForm");
        this.$dom = {
            Broker: $('#broker'),
            fieldsetName: $('#name'), 
            fieldsetDemographics: $('#demographics'),
            fieldsetControls: $('#controls')
        };
    };

    EmployeeTemplateBuilder.prototype = {
        init: function() {
            this.build();
            
        },
        build: function() {
            if (!!this.d) {
                var i;

                $("#name_title").val(this.d.Employee.Person.Title.Id);
                $("#name_given").attr("value",(this.d.Employee.Person.GivenName));
                $("#name_middle").attr("value",(this.d.Employee.Person.MiddleName));
                $("#name_family").attr("value",(this.d.Employee.Person.FamilyName));
                $("#demo_gender").val(this.d.Employee.Person.Gender.Id);
                $("#demo_birth").attr("value",(this.d.Employee.Person.BirthDate));
                $("#demo_occupation").attr("value",(this.d.Employee.Person.Occupation));
                $("#demo_marital").val(this.d.Employee.Person.MaritalStatus.Id);       
                $("#employee_id").val(this.d.Employee.Id);       
                $("#Rev1").val(this.d.Employee.RevisionNumber);
                
                for(i = 0; i < this.d.Employee.Roles.length; i++){
                    if(this.d.Employee.Roles[i].Name == "Salesperson"){             //Need to find a way to do this dynamically
                        $("#role_s").attr('checked', true);
                    }
                    if(this.d.Employee.Roles[i].Name == "Appraiser"){
                        $("#role_a").attr('checked', true);
                    }
                    if(this.d.Employee.Roles[i].Name == "Buyer"){
                        $("#role_b").attr('checked', true);
                    }
                }
 
            }

            this.$dom.fieldsetControls.bind('click', $.proxy(this.changeEmployee, this));
        },

        clean: function() {
            this.$dom.fieldsetControls.unbind('click');
        },

        changeEmployee: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                c = null,
                e = null,
                v = null;
                if (n == '#load') {
                        e = document.getElementById('employee_id');
                        v = (e == null ? null : e.value);
                        $(Employee).trigger("Employee.Load", v);
                }
                if (n == '#delete') {
                        v = {
                                    Id: document.getElementById('employee_id').value,
                                    RevisionNumber: $("#Rev1").val()

                             };
                        $(Employee).trigger("Employee.Delete", v);                   
                }
                if (n == '#search') {
                        v = {
                                    Given: document.getElementById('name_given').value,
                                    Family: document.getElementById('name_family').value,
                                    Middle: document.getElementById('name_middle').value,
                                    BirthDate: document.getElementById('demo_birth').value,
                                    Occupation: document.getElementById('demo_occupation').value,
                                    Title: $("#name_title").val(),
                                    Gender: $("#demo_gender").val(),
                                    MaritalStatus: $("#demo_marital").val(),
                                    BrokerId: document.getElementById('broker').value
                             };
                        $(Employee).trigger("Employee.Search", v);                   
                }
                if (n == '#save') {
                        v = {
                                    Given: document.getElementById('name_given').value,
                                    Family: document.getElementById('name_family').value,
                                    Middle: document.getElementById('name_middle').value,
                                    BirthDate: document.getElementById('demo_birth').value,
                                    Occupation: document.getElementById('demo_occupation').value,
                                    Title: $("#name_title").val(),
                                    Gender: $("#demo_gender").val(),
                                    MaritalStatus: $("#demo_marital").val(),
                                    BrokerId: document.getElementById('broker').value,
                                    Id: document.getElementById('employee_id').value,
                                    RevisionNumber: document.getElementById('Rev1').value
                             };
                        $(Employee).trigger("Employee.Save", v);                   
                }
            }
       }
    };

}).apply(Employee);
