if (typeof Employee !== "function") {
    var Employee = function() {};
}

(function() {
    var State = {
        Broker: ''
    };

    /* =================== */
    /* == START: Classes = */
    /* =================== */
    /* == DTO == */
    function PersonDto () {
        this.Id = 0;
        this.RevisionNumber = 0;
        this.GivenName = '';
        this.MiddleName = '';
        this.FamilyName = '';
        this.Title = null;
        this.Gender = null;
        this.MaritalStatus = null;
        this.BirthDate = null;
        this.Occupation = '';
        this.Contact = null;
    }

    function TokenStatusDto () {
        this.status;
    }

    function AuthorizationDto () {
        this.Guid = '';
    }

    function EmployeeDto () {
        this.Id = 0;
        this.ClientId = 0;
        this.Roles = [];
        this.Person = null;
        this.RevisionNumber = 0;
    }

    function TitleDto (){
        this.Id = 0;
        this.Name = '';
    }

    function GenderDto(){
        this.Id = 0;
        this.Name = '';
    }

    function MaritalStatusDto () {
        this.Id = 0;
        this.Name = '';
    }

    function RoleDto () {
        this.Id = 0;
        this.Name = '';
    }

    /* == Envelopes == */
    function FetchEmployeeArgumentsDto ()  {
        this.Id = 0;
        this.Authorization = null;
        this.Broker = '00000000-0000-0000-0000-000000000000';
        EventBinder(this);
    }

    function FetchEmployeeResultsDto(){
        this.Arguments = null;
        this.Id = 0;
        this.Employee = null;
        this.AuthorizationStatus = null;
    }

    function ReferenceInformationArgumentsDto () {
        this.Authorization = null;
        EventBinder(this);
    }

    function ReferenceInformationResultsDto () {
        this.Arguments = null;
        this.Roles = [];
        this.AuthorizationStatus = null;
    }

    function DeleteEmployeeArgumentsDto () {
        this.Id = 0;
        this.RevisionNumber = 0;
        this.Authorization = null;
        this.Broker = '00000000-0000-0000-0000-000000000000';
        EventBinder(this);
    }

    function DeleteEmployeeResultsDto() {
        this.Arguments = null;
        this.Success = false;
        this.AuthorizationStatus = null;
    }

    function SearchEmployeeArgumentsDto () {
        this.Authorization = null;
        this.Example = null;
        this.SortColumns = [];
        this.MaximumRows = 0;
        this.StartRowIndex = 0;
        EventBinder(this);
    }

    function SearchEmployeeResultsDto () {
        this.AuthorizationStatus = null;
        this.Results = [];
        this.TotalRowCount = 0;
    }

    function SaveEmployeeArgumentsDto() {
        this.Employee = null;    
        this.Authorization = null;
        this.Broker = '00000000-0000-0000-0000-000000000000';    
        EventBinder(this);    
    }

    function SaveEmployeeResultsDto () {
        this.Arguments = null;
        this.Employee = null;
        this.AuthorizationStatus = null;
    }

    /* =================== */
    /* == END: Classes == */
    /* =================== */

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Employee).trigger("Employee.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Fetch": genericService("/Deal/Services/Employees.asmx/Fetch", FetchEmployeeResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                       "__type": "FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes.FetchEmployeeArgumentsDto",
                           "Id": this.Id,
                "Authorization": this.Authorization,            
                       "Broker": this.Broker
                }
            });
        }),
        "ReferenceInformation" : genericService("/Deal/Services/Employees.asmx/ReferenceInformation", ReferenceInformationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes.ReferenceInformationArgumentsDto",
                      "Authorization": this.Authorization
                }                
                
            });
         }),
         "Delete": genericService("/Deal/Services/Employees.asmx/Delete", DeleteEmployeeResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes.DeleteEmployeeArgumentsDto",
                        "Id": this.Id,
            "RevisionNumber": this.RevisionNumber,
             "Authorization": this.Authorization,            
                    "Broker": this.Broker
                }
            });
        }),
        "Search": genericService("/Deal/Services/Employees.asmx/Search", SearchEmployeeResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes.SearchEmployeeArgumentsDto",
             "Authorization": this.Authorization,
                   "Example": this.Example,
               "SortColumns": this.SortColumns,
               "MaximumRows": this.MaximumRows,
             "StartRowIndex": this.StartRowIndex   
                }
            });
        }),
        "Save": genericService("/Deal/Services/Employees.asmx/Save", SaveEmployeeResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes.SaveEmployeeArgumentsDto",
                  "Employee": this.Employee,
             "Authorization": this.Authorization,            
                    "Broker": this.Broker
                }
            });
        })
    };

    var Events = {
        "FetchEmployeeArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Employee).trigger("Employee.Loaded", [data]);
            }
        },
        "ReferenceInformationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Employee).trigger("Employee.ReferenceInformationLoaded", [data]);
            }
        },
         "DeleteEmployeeArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Employee).trigger("Employee.Deleted", [data]);
            }
        },
         "SearchEmployeeArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Employee).trigger("Employee.Searched", [data]);
            }
        },
          "SaveEmployeeArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Employee).trigger("Employee.Saved", [data]);
            }
        }
    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "FetchEmployeeArgumentsDto": genericMapper(FetchEmployeeArgumentsDto),
        "FetchEmployeeResultsDto": genericMapper(FetchEmployeeResultsDto, {
            "Arguments": "FetchEmployeeArgumentsDto",
            "Employee": "EmployeeDto"
        }),
        "EmployeeDto": genericMapper(EmployeeDto, {
             "Roles": "RoleDto",
             "Person": "PersonDto"
        }),
        "PersonDto": genericMapper(PersonDto, {
             "Title": "TitleDto",
             "Gender": "GenderDto",
             "MaritalStatus": "MaritalStatusDto"
        }),
        "TitleDto": genericMapper(TitleDto),
        "GenderDto" : genericMapper(GenderDto),
        "MaritalStatusDto": genericMapper(MaritalStatusDto),
        "RoleDto": genericMapper(RoleDto),
        "TokenStatusDto": genericMapper(TokenStatusDto),
        "AuthorizationDto": genericMapper(AuthorizationDto),
        "ReferenceInformationArgumentsDto": genericMapper(ReferenceInformationArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "ReferenceInformationResultsDto": genericMapper(ReferenceInformationResultsDto, {
            "Arguments": "ReferenceInformationArgumentsDto",
            "Roles": "RoleDto",
            "AuthorizationStatus": "TokenStatusDto"
        }),
        "DeleteEmployeeArgumentsDto": genericMapper(DeleteEmployeeArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "DeleteEmployeeResultsDto": genericMapper(DeleteEmployeeResultsDto, {
            "Arguments": "DeleteEmployeeArgumentsDto",
            "AuthorizationStatus": "TokenStatusDto"
        }),
        "SearchEmployeeArgumentsDto": genericMapper(SearchEmployeeArgumentsDto, {
            "Authorization": "AuthorizationDto",
            "Example": "EmployeeDto"
        }),
        "SearchEmployeeResultsDto": genericMapper(SearchEmployeeResultsDto, {
            "AuthorizationStatus": "TokenStatusDto",
            "Results": "EmployeeDto"
        }),
        "SaveEmployeeArgumentsDto": genericMapper(SaveEmployeeArgumentsDto, {
            "Authorization": "AuthorizationDto",
            "Employee": "EmployeeDto"
        }),
        "SaveEmployeeResultsDto": genericMapper(SaveEmployeeResultsDto, {
            "Arguments": "SaveEmployeeArgumentsDto",
            "AuthorizationStatus": "TokenStatusDto",
            "Employee": "EmployeeDto"
        })
    };

    var EventBinder = function(obj) {
        if (obj instanceof FetchEmployeeArgumentsDto) {
            $(obj).bind("fetchComplete", Events.FetchEmployeeArgumentsDto.fetchComplete);
        }

        if (obj instanceof ReferenceInformationArgumentsDto) {
            $(obj).bind("fetchComplete", Events.ReferenceInformationArgumentsDto.fetchComplete);
        }

        if (obj instanceof DeleteEmployeeArgumentsDto) {
            $(obj).bind("fetchComplete", Events.DeleteEmployeeArgumentsDto.fetchComplete);
        }

         if (obj instanceof SearchEmployeeArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SearchEmployeeArgumentsDto.fetchComplete);
         }

         if (obj instanceof SaveEmployeeArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SaveEmployeeArgumentsDto.fetchComplete);
        }
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(ReferenceInformationArgumentsDto, DataMapper.ReferenceInformationArgumentsDto);
        BindDataMapper(ReferenceInformationResultsDto, DataMapper.ReferenceInformationResultsDto);

        BindDataMapper(DeleteEmployeeArgumentsDto, DataMapper.DeleteEmployeeArgumentsDto);
        BindDataMapper(DeleteEmployeeResultsDto, DataMapper.DeleteEmployeeResultsDto);

        BindDataMapper(FetchEmployeeArgumentsDto, DataMapper.FetchEmployeeArgumentsDto);
        BindDataMapper(FetchEmployeeResultsDto, DataMapper.FetchEmployeeResultsDto);

        BindDataMapper(SaveEmployeeArgumentsDto, DataMapper.SaveEmployeeArgumentsDto);
        BindDataMapper(SaveEmployeeResultsDto, DataMapper.SaveEmployeeResultsDto);
        
        BindDataMapper(PersonDto, DataMapper.PersonDto);        
        BindDataMapper(TitleDto, DataMapper.TitleDto);
        BindDataMapper(GenderDto, DataMapper.GenderDto);
        BindDataMapper(MaritalStatusDto, DataMapper.MaritalStatusDto);
        BindDataMapper(RoleDto, DataMapper.RoleDto);      
        BindDataMapper(EmployeeDto, DataMapper.EmployeeDto);    

        BindDataMapper(SearchEmployeeArgumentsDto, DataMapper.SearchEmployeeArgumentsDto);
        BindDataMapper(SearchEmployeeResultsDto, DataMapper.SearchEmployeeResultsDto);

        BindDataMapper(AuthorizationDto, DataMapper.AuthorizationDto);
        BindDataMapper(TokenStatusDto, DataMapper.TokenStatusDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(FetchEmployeeArgumentsDto, Services.Fetch);
        BindFetch(ReferenceInformationArgumentsDto, Services.ReferenceInformation);
        BindFetch(DeleteEmployeeArgumentsDto, Services.Delete);
        BindFetch(SearchEmployeeArgumentsDto, Services.Search);
        BindFetch(SaveEmployeeArgumentsDto, Services.Save);
 
    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }

    this.prototype.main = function(selected) {
        if (typeof selected === "undefined") {
            selected = {};
        }
        if (selected.buid) {
            $(Employee).trigger("Employee.BrokerChange", selected.buid);
        }

        // load the reference information
        var refInfo = new ReferenceInformationArgumentsDto();
        $(refInfo).trigger("fetch");

        $(Employee).trigger("Employee.Initialize");

    };

    this.raises = this.raises || [];
    this.raises.push("Employee.Loaded");
    this.raises.push("Employee.Initialize");
    this.raises.push("Employee.ReferenceInformationLoaded");
    this.raises.push("Employee.Searched");
    this.raises.push("Employee.Saved");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Employee.SampleChange");
    this.listensTo.push("Employee.Load");
    this.listensTo.push("Employee.Delete");
    this.listensTo.push("Employee.Search");
    this.listensTo.push("Employee.Save");


    var PublicEvents = {
        "Employee.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
        },
        "Employee.Load": function(evt, value) {
            var fetchEmployee = new FetchEmployeeArgumentsDto();
            fetchEmployee.Id = value;
            $(fetchEmployee).trigger("fetch");
        },
         "Employee.Delete": function(evt, value) {
            var deleteEmployee = new DeleteEmployeeArgumentsDto();
            deleteEmployee.Id = value.Id;
            deleteEmployee.RevisionNumber = value.RevisionNumber;
            $(deleteEmployee).trigger("fetch");
        },
        "Employee.Search": function(evt, value) {
            var searchEmployee = new SearchEmployeeArgumentsDto();
            var employee = new EmployeeDto();
            var person = new PersonDto();
            var title = new TitleDto();
            var gender = new GenderDto();
            var maritalStatus = new MaritalStatusDto();
            var authDto = new AuthorizationDto();

            title.Id = value.Title;
            gender.Id = value.Gender;
            maritalStatus.Id = value.MaritalStatus;

            person.GivenName = value.Given;
            person.MiddleName = value.Middle;
            person.FamilyName = value.Family;
            person.Title = title;
            person.Gender = gender;
            person.MaritalStatus = maritalStatus;
            person.BirthDate = /*value.BirthDate*/ new Date();
            person.Occupation = value.Occupation;             
            
            searchEmployee.Authorization = authDto;
            employee.Person = person;
            searchEmployee.Example = employee;

            $(searchEmployee).trigger("fetch");
        },
        "Employee.Save": function(evt, value) {
            var saveEmployee = new SaveEmployeeArgumentsDto();
            var employee = new EmployeeDto();
            var person = new PersonDto();
            var title = new TitleDto();
            var gender = new GenderDto();
            var maritalStatus = new MaritalStatusDto();
            var authDto = new AuthorizationDto();

            title.Id = value.Title;
            gender.Id = value.Gender;
            maritalStatus.Id = value.MaritalStatus;

            person.GivenName = value.Given;
            person.MiddleName = value.Middle;
            person.FamilyName = value.Family;
            person.Title = title;
            person.Gender = gender;
            person.MaritalStatus = maritalStatus;
            person.BirthDate = /*value.BirthDate*/ new Date();
            person.Occupation = value.Occupation;             
            
            saveEmployee.Authorization = authDto;
            saveEmployee.Broker = value.BrokerId || '00000000-0000-0000-0000-000000000000';
            employee.Person = person;
            employee.Id = value.Id;
            employee.RevisionNumber = value.RevisionNumber;
            saveEmployee.Employee = employee;

            $(saveEmployee).trigger("fetch");

        }
    };
    $(Employee).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS =  */
    /* ========================== */

}).apply(Employee);

