if (typeof Party === "undefined") {
    var Party = {};
}
(function() {
    //this.raises = this.raises || [];
    //this.raises.push("Example.Event");

    //this.listensTo = this.listensTo || [];
    //this.listensTo.push("Example.Event");
    
    var State = {
        Naming: function (type_name, property_name, id) { var n = type_name + "_" + property_name; if (!!id) { n += "_" + id  } return n; },
        Name: '#name',
        Demographics: '#demographics',
        PartyID: -1,
		PartyTypeID: -1,
		PartyPersonID: -1,
		PartyPersonRevisionNumber: -1,
		ClientID: -1,
		PartyOrganizationID: -1,
		PartyOrganizationRevisionNumber: -1
    };

    function isFunction (obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    };

    var PublicEvents = {
		"Party.Initialize": function(evt, data) {
            var l = new LayoutTemplateBuilder();
            l.clean();
            l.init();
            var person = new Person();
            person.main();
            $(Person).trigger("Person.Layout", {Name: State.Name, Demographics: State.Demographics});
            $(Person).trigger("Person.Initialize");
			
			var e = new EventTemplateBuilder();
            e.clean();
            e.init();
        },
		"PersonParty.Edit": function(evt, data) {
            $(Party).trigger("Party.Fetch", data);
        },
	    "PersonParty.Reset": function(evt, data) {
            var pp = new PersonPartyTemplateBuilder(data);
			pp.reset();
        },  
		"OrgParty.Edit": function(evt, data) {
            $(Party).trigger("Party.Fetch", data);
        },
		"OrgParty.Reset": function(evt, data) {
			var op = new OrgPartyTemplateBuilder(data);
			op.reset();
        }, 		
        "Person.ReferenceInformationLoaded": function(evt, data) {
            var pp = new PersonPartyTemplateBuilder(data);
			pp.populateDropdowns(data);
        },		
		"Party.PartySearched": function(evt, data) {
            if (!!data) {
                if (data.Results != null && data.Results.length > 0) {
                    if (data.Results[0] instanceof Party.PersonPartyDto) {
						var pp = new PersonPartyTemplateBuilder(data);
						pp.clean();
						pp.build(data.Results);
                    } else if (data.Results[0] instanceof Party.OrganizationPartyDto) {
						var op = new OrgPartyTemplateBuilder(data);
						op.clean();
						op.build(data.Results);
                    } else {
						// invalid object 
					}
                }
            }
        },
		"Party.PartyLoaded": function(evt, data) {
            if (!!data) {
                if (data.Party instanceof Party.PersonPartyDto) {
					var pp = new PersonPartyTemplateBuilder(data);
					pp.populate(data.Party);
				} else if (data.Party instanceof Party.OrganizationPartyDto) {
					var op = new OrgPartyTemplateBuilder(data);
					op.populate(data.Party);
				} else {
					// invalid object 
				}
                
            }
        },
		"Party.PartySaved": function(evt, data) {
            if (!!data) {
                if (data.Party instanceof Party.PersonPartyDto) {
					// set new partyId in State
				    State.PartyID = data.Party.Id;
					State.PartyPersonID = data.Party.Person.Id;
					State.PartyPersonRevisionNumber = data.Party.Person.RevisionNumber;
				} else if (data.Party instanceof Party.OrganizationPartyDto) {
					// set new partyId in State
					State.PartyID = data.Party.Id;
					State.PartyOrganizationID = data.Party.Organization.Id;
					State.PartyOrganizationRevisionNumber = data.Party.Organization.RevisionNumber;
				} else {
					// invalid object 
				}
                
            }
        }
    };
    $(Party).bind(PublicEvents);

	
    var LayoutTemplateBuilder = function() {
        this.$dom = {
            Form: $('#subject'),
            Broker: $('#broker'),
            Menu: $('.menu'),
            Menu$Item: $('.menu a')
        };
    };

    LayoutTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.Form.bind('click', $.proxy(this.changeBroker, this));
            this.$dom.Menu.bind('click', $.proxy(this.changeMenuItem, this));
        },
        clean: function() {
            this.$dom.Form.unbind('click');
			this.$dom.Menu.unbind('click');
        },
        changeMenuItem: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "LI") {
                tgt = tgt.getElementsByTagName("A")[0];
            }
            var menu = $(tgt).closest('div.menu');
            this.$dom.Menu$Item.each(function(index) {
                var parent = $(this).parent(),
                    m = $(this).closest('div.menu');
                if (m[0] && menu[0] && m[0].id == menu[0].id) {
                    var i = this.href.lastIndexOf('#'),
                    e = this.href.substring(i);
                    if (this.href == tgt.href) {
                        parent.addClass('selected');
                        $(e).removeClass('hidden');
                    }
                    else {
                        parent.removeClass('selected');
                        $(e).addClass('hidden');
                    }
                }
            });
			
			// reset state
			State.PartyTypeID = -1;
			State.PartyID = -1;
			State.PartyPersonID = -1;
			State.PartyPersonRevisionNumber = -1;
			State.ClientID = -1;
			State.PartyOrganizationID = -1;
			
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        changeBroker: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                e = tgt.href.substring(i),
                v = document.getElementById(e.substring(1)).value,
                c = null;
                if (e == '#broker') {
                    $(Party).trigger("Party.BrokerChange", v);
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };
	
	
	 var EventTemplateBuilder = function() {
        this.$dom = {
			PersonReset: $('#person_controls a[href="#reset"]'),
			PersonSearch: $('#person_controls a[href="#search"]'),
			PersonCancel: $('#person_controls a[href="#cancel"]'),
			PersonSave: $('#person_controls a[href="#save"]'),
			OrgReset: $('#organization_controls a[href="#reset"]'),
			OrgSearch: $('#organization_controls a[href="#search"]'),
			OrgCancel: $('#organization_controls a[href="#cancel"]'),
			OrgSave: $('#organization_controls a[href="#save"]')
        };
    };

    EventTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
			this.$dom.PersonReset.bind('click', this.personReset);
			this.$dom.PersonSearch.bind('click', this.personSearch);
			this.$dom.PersonCancel.bind('click', this.personCancel);
			this.$dom.PersonSave.bind('click', this.personSave);
			
			this.$dom.OrgReset.bind('click', this.orgReset);
			this.$dom.OrgSearch.bind('click', this.orgSearch);
			this.$dom.OrgCancel.bind('click', this.orgCancel);
			this.$dom.OrgSave.bind('click', this.orgSave);
        },
        clean: function() {
			this.$dom.PersonReset.unbind('click');
			this.$dom.PersonSearch.unbind('click');
			this.$dom.PersonCancel.unbind('click');
			this.$dom.PersonSave.unbind('click');
			
			this.$dom.OrgReset.unbind('click');
			this.$dom.OrgSearch.unbind('click');
			this.$dom.OrgCancel.unbind('click');
			this.$dom.OrgSave.unbind('click');
        },
		personReset: function(evt) {
            $(Party).trigger("PersonParty.Reset");
        },
		personSearch: function(evt) {
			var person = GetPerson();
			var args = {
				Broker: '00000000-0000-0000-0000-000000000001',		// change to State.Broker in app.js
				PersonParty: { Id: State.PartyID, TypeId: State.PartyTypeID, ClientId: State.ClientID, Person: person }
			};
			$(Party).trigger("Party.Search", args);
        },
		personCancel: function(evt) {
            $(Party).trigger("Party.Cancel");
        },
		personSave: function(evt) {
			var person = GetPerson();
			State.PartyTypeID = 1;	// Party.PartyType 1 = PersonParty
			var args = {
				Broker: '00000000-0000-0000-0000-000000000001',		// change to State.Broker in app.js
				PersonParty: { Id: State.PartyID, TypeId: State.PartyTypeID, ClientId: State.ClientID, Person: person }
			};
            $(Party).trigger("Party.Save", args);
        },
		orgReset: function(evt) {
            $(Party).trigger("OrgParty.Reset");
        },
		orgSearch: function(evt) {
			var organization = GetOrganization();
			State.PartyTypeID = 2;	// Party.PartyType 2 = OrganizationParty
			var args = {
				Broker: '00000000-0000-0000-0000-000000000001',		// change to State.Broker in app.js
				OrganizationParty: { Id: State.PartyID, TypeId: State.PartyTypeID, ClientId: State.ClientID, Organization: organization }
			};
			$(Party).trigger("Party.Search", args);
        },
		orgCancel: function(evt) {
            $(Party).trigger("Party.Cancel");
        },
		orgSave: function(evt) {
			var organization = GetOrganization();
			State.PartyTypeID = 2;	// Party.PartyType 2 = OrganizationParty
			var args = {
				Broker: '00000000-0000-0000-0000-000000000001',		// change to State.Broker in app.js
				OrganizationParty: { Id: State.PartyID, TypeId: State.PartyTypeID, ClientId: State.ClientID, Organization: organization }
			};
			$(Party).trigger("Party.Save", args);
        }
    };
	
	
	var PersonPartyTemplateBuilder = function(data) {
        this.$el = $("#person_results");
		this.$dom = {
			Results: $("#person_results")
        };
		this.data = data;
    };

    PersonPartyTemplateBuilder.prototype = {
        build: function(results) {
			for (var i = 0; i < results.length; i++) {
				
				var title = " ";
				var objTitle = results[i].Person.Title;
				if(objTitle != null) { title = results[i].Person.Title.Name; }
				
				var givenName = results[i].Person.GivenName || " ";
				var middleName = results[i].Person.MiddleName || " ";
				var familyName = results[i].Person.FamilyName || " ";
				var id = results[i].Id || 0;
				
				this.$dom.Results.find("table").find("tbody").append(
					'<tr><td>' + title + 
					'</td><td>' + givenName + 
					'</td><td>' + middleName + 
					'</td><td>' + familyName + 
					'</td><td class="action"><a href="#edit" onclick="OnPersonPartyEdit(' + id + ')">Edit</a></td></tr>');    
			}
        },
		populateDropdowns: function(data) {
			var title = $('#PersonTitle');
			var gender = $('#PersonGender');
			var maritalStatus = $('#PersonMaritalStatus');
			
			title.find('option').remove();
			gender.find('option').remove();
			maritalStatus.find('option').remove();
			
			// add default-empty
			title.append($("<option></option>").attr("value", -1).text(""));
			gender.append($("<option></option>").attr("value", " ").text(""));
			maritalStatus.append($("<option></option>").attr("value", " ").text(""));
			
			for (var i = 0; i < data.Titles.length; i++) {
				title.append($("<option></option>").attr(
					"value", data.Titles[i].Id).text(data.Titles[i].Name));
			}
			for (var i = 0; i < data.Genders.length; i++) {
				gender.append($("<option></option>").attr(
					"value", data.Genders[i].Id).text(data.Genders[i].Name));
			}
			for (var i = 0; i < data.MaritalStatuses.length; i++) {
				maritalStatus.append($("<option></option>").attr(
					"value", data.MaritalStatuses[i].Id).text(data.MaritalStatuses[i].Name));
			}
		},
		populate: function(party) {
			State.PartyID = party.Id;
			State.PartyTypeID = party.TypeId;
			State.ClientID = party.ClientId;
			State.PartyPersonID = party.Person.Id;
			State.PartyPersonRevisionNumber = party.Person.RevisionNumber;
			SetGivenName(party.Person.GivenName || "");
			SetMiddleName(party.Person.MiddleName || "");
			SetFamilyName(party.Person.FamilyName || "");
			
			var milliBD = party.Person.BirthDate.replace(/\/Date\((-?\d+)\)\//, '$1');
			var birthdate = new Date(parseInt(milliBD));
			var month = birthdate.getMonth() + 1; //months are zero based
			var date = birthdate.getDate();
			var year = birthdate.getFullYear();
			var formattedDate = month + "/" + date + "/" + year;
			SetBirthDate(formattedDate);
			
			SetOccupation(party.Person.Occupation || "");
			SetTitle(party.Person.Title.Id);
			SetGender(party.Person.Gender.Id);
			SetMaritalStatus(party.Person.MaritalStatus.Id);
		},
        clean: function() {
			this.$dom.Results.find("table").find("tbody").find("tr").remove();
        },
		reset: function() {
			this.clean();
			State.PartyID = -1;
			State.PartyTypeID = -1;
			State.ClientID = -1;
			State.PartyPersonID = -1;
			State.PartyPersonRevisionNumber = -1;
			SetTitle("");
			SetGender("");
			SetMaritalStatus("");
			SetGivenName("");
			SetMiddleName("");
			SetFamilyName("");
			SetBirthDate("");
			SetOccupation("");
			$(Party).trigger("Party.Initialize");
		}
    };
	
	var OrgPartyTemplateBuilder = function(data) {
        this.$el = $("#organization_results");
		this.$dom = {
			Results: $("#organization_results")
        };
		this.data = data;
    };

    OrgPartyTemplateBuilder.prototype = {
        build: function(results) {
			for (var i = 0; i < results.length; i++) {
				var id = results[i].Id || 0;
			
				this.$dom.Results.find("table").find("tbody").append(
					'<tr><td>' + results[i].Organization.Name + '</td>' + 
					'<td class="action"><a href="#edit" onclick="OnOrgPartyEdit(' + id + ');">Edit</a></td></tr>');       
			}
        },
		populate: function(party) {
			State.PartyID = party.Id;
			State.PartyTypeID = party.TypeId;
			State.ClientID = party.ClientId;
			State.PartyOrganizationID = party.Organization.Id;
			State.PartyOrganizationRevisionNumber = party.Organization.RevisionNumber;
			SetOrgName(party.Organization.Name);
		},
        clean: function() {
			this.$dom.Results.find("table").find("tbody").find("tr").remove();
        },
		reset: function() {
			this.clean();
			State.PartyID = -1;
			State.PartyTypeID = -1;
			State.ClientID = -1;
			State.PartyOrganizationID = -1;
			SetOrgName(""); 
        }
    };
	
	function GetPerson() {
		var person = {
			Id: State.PartyPersonID,
			RevisionNumber: State.PartyPersonRevisionNumber,
			GivenName: GetGivenName(),
			MiddleName: GetMiddleName(),
			FamilyName: GetFamilyName(),
			Title: GetTitle(),
			BirthDate: GetBirthDate(),
			Gender: GetGender(),
			MaritalStatus: GetMaritalStatus(),
			Occupation: GetOccupation()
		};
		return person;
	}
	
	function GetOrganization() {
		var organization = {
			Id: State.PartyOrganizationID,
			RevisionNumber: State.PartyOrganizationRevisionNumber,
			Name: GetOrgName()
		};
		return organization;
	}

	function GetTitle() {
		var title = {
			Id: $("#PersonTitle").val(),
			Name: $("#PersonTitle :selected").text()
		};
		return title;
	}
	function SetTitle(val) {
		$("#PersonTitle").val(val)
	}


	function GetOrgName() {
		return $('#organization_name').val();
	}
	function SetOrgName(val) {
		$('#organization_name').val(val);
	}


	function GetBirthDate() {
		return $('#PersonBirthDate').val();
	}
	function SetBirthDate(val) {
		$('#PersonBirthDate').val(val);
	}


	function GetGender() {
		var gender = {
			Id: $("#PersonGender").val(),
			Name: $("#PersonGender :selected").text()
		};
		return gender;   
	}
	function SetGender(val) {
		$("#PersonGender").val(val);
	}


	function GetMaritalStatus() {
		var maritalStatus = {
			Id: $("#PersonMaritalStatus").val(),
			Name: $("#PersonMaritalStatus :selected").text()
		};
		return maritalStatus;   
	}
	function SetMaritalStatus(val) {
		$("#PersonMaritalStatus").val(val);
	}


	function GetOccupation() {
		return $('#PersonOccupation').val(); 
	}
	function SetOccupation(val) {
		$('#PersonOccupation').val(val);
	}


	function GetGivenName(){
		return $('#PersonGivenName').val(); 
	}
	function SetGivenName(val) {
		$('#PersonGivenName').val(val);
	}


	function GetMiddleName() {
		return $('#PersonMiddleName').val(); 
	}
	function SetMiddleName(val) {
		$('#PersonMiddleName').val(val);
	}


	function GetFamilyName() {
		return $('#PersonFamilyName').val(); 
	}
	function SetFamilyName(val) {
		$('#PersonFamilyName').val(val);
	}

}).apply(Party);


function OnPersonPartyEdit(id) {
	var args = { 	Id: id,
					Broker: '00000000-0000-0000-0000-000000000001'
			};
	$(Party).trigger("PersonParty.Edit", args);
}

function OnOrgPartyEdit(id) {
	var args = { 	Id: id,
					Broker: '00000000-0000-0000-0000-000000000001'
			};
	$(Party).trigger("OrgParty.Edit", args);
}



