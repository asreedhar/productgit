if (typeof Party !== "function") {
    var Party = function() {};
}

(function() {
    //this.raises = this.raises || [];
    //this.raises.push("Example.Event");

    //this.listensTo = this.listensTo || [];
    //this.listensTo.push("Example.Event");


    var EmptyGuid = '00000000-0000-0000-0000-000000000000';
    var State = {
        Broker: EmptyGuid,
		// pagination
        SortColumns: [
		{
            'ColumnName': 'GivenName',
            'Ascending': true
        }
        //,{
        //    'ColumnName': 'FamilyName',
        //    'Ascending': true
        //}
		],
        MaximumRows: 25,
        StartRowIndex: 0
    };

    /* =================== */
    /* == START: Classes = */
    /* =================== */
    
    /* == DTO == */
    
    function AuthorizationDto() {
        this.Broker = EmptyGuid;
        this.Device = EmptyGuid;
        this.DeviceType = 0;
        this.Token = EmptyGuid;
        this.ProductType = null;
    }

    function PartyDto() {
        this.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.PartyDto";	// abstract type - must be changed to PersonPartyDto/OrganizationPartyDto
        this.Id = 0;
        this.TypeId = 0;
        this.ClientId = 0;
    }

	function PersonPartyDto() {
		this.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.PersonPartyDto";
        this.Id = 0;
        this.TypeId = 0;
        this.ClientId = 0;
		this.Person = null;
    }
	PersonPartyDto.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.PersonPartyDto";
	
	function OrganizationPartyDto() {
		this.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.OrganizationPartyDto";
        this.Id = 0;
        this.TypeId = 0;
        this.ClientId = 0;
		this.Organization = null;
    }
	OrganizationPartyDto.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.OrganizationPartyDto";
	
    /* == Envelopes == */
    function FetchPartyArgumentsDto ()  {
        // parent properties
        this.Authorization = new AuthorizationDto();
        this.Broker = EmptyGuid;
        // my properties
        this.Id = 0;
        EventBinder(this);
    }

    function FetchPartyResultsDto ()  {
        // parent properties
        this.AuthorizationStatus = 0;
        this.Arguments = null;
        // my properties
        this.Party = null;
    }
	
	function SavePartyArgumentsDto ()  {
        // parent properties
        this.Authorization = new AuthorizationDto();
        this.Broker = EmptyGuid;
        // my properties
        this.Party = null;
        EventBinder(this);
    }
	
    function SavePartyResultsDto ()  {
        // parent properties
        this.AuthorizationStatus = 0;
        this.Arguments = null;
        // my properties
        this.Party = null;
    }

    function SearchPartyArgumentsDto ()  {
        // parent properties
        this.Authorization = null;
        this.Broker = null;
		this.SortColumns = '';
		this.MaximumRows = 10;
		this.StartRowIndex = 0;
        // my properties
        this.Example = null;
        EventBinder(this);
    }
	
	SearchPartyArgumentsDto.prototype = {
        fromState: function() {
			this.Broker = State.Broker;
            this.SortColumns = State.SortColumns;
            this.MaximumRows = State.MaximumRows;
            this.StartRowIndex = State.StartRowIndex;
        }
    };
	
    function SearchPartyResultsDto ()  {
        // parent properties
        this.AuthorizationStatus = 0;
        // my properties
        this.Results = [];
        this.TotalRowCount = 0;
    }

    /* =================== */
    /* == END: Classes == */
    /* =================== */
    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }
    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Party).trigger("Party.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Fetch": genericService("/Deal/Services/Parties.asmx/Fetch", FetchPartyResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes.FetchPartyArgumentsDto",
                    "Broker": this.Broker,
                    "Id": this.Id,
                    "Authorization": this.Authorization
                }
            });
        }),
        "Search": genericService("/Deal/Services/Parties.asmx/Search", SearchPartyResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes.SearchPartyArgumentsDto",
                    "Authorization": this.Authorization,
                    "Broker": this.Broker,
                    "Example": this.Example,
					"SortColumns": this.SortColumns,
                    "MaximumRows": this.MaximumRows,
                    "StartRowIndex": this.StartRowIndex
                }
            });
        }),
		"Save": genericService("/Deal/Services/Parties.asmx/Save", SavePartyResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes.SavePartyArgumentsDto",
                    "Authorization": this.Authorization,
                    "Broker": this.Broker,
                    "Party": this.Party
                }
            });
        })
    };
    var Events = {
        "FetchPartyArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Party).trigger("Party.PartyLoaded", [data]);
            }
        },
        "SearchPartyArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Party).trigger("Party.PartySearched", [data]);
            }
        },
		"SavePartyArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Party).trigger("Party.PartySaved", [data]);
            }
        }
    };
    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; // Ensure actual property
                subJson = json[prop];

                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
							var typeName = config[prop];
							if (typeName == '__type') {
								typeName = subJson[idx]['__type'];
							}
							result[prop].push(DataMapper[typeName](subJson[idx]));
                        }
                    } else {
						var typeName = config[prop];
						if (typeName == '__type') {
							typeName = subJson['__type'];
						}
					    result[prop] = DataMapper[typeName](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }
    var DataMapper = {
        // Envelopes
        "FetchPartyArgumentsDto" : genericMapper(FetchPartyArgumentsDto, {
            "Authorization" : "AuthorizationDto"
        }),
        "FetchPartyResultsDto" : genericMapper(FetchPartyResultsDto, {
            "Arguments" : "FetchPartyArgumentsDto",
            "Party" : "__type",
            "Authorization" : "AuthorizationDto"
        }),
        "SearchPartyArgumentsDto": genericMapper(SearchPartyArgumentsDto, {
            "Authorization" : "AuthorizationDto",
            "Example" : "PartyDto"
        }),
        "SearchPartyResultsDto": genericMapper(SearchPartyResultsDto, {
			"Results" : "__type"
        }),
		"SavePartyArgumentsDto": genericMapper(SavePartyArgumentsDto, {
            "Authorization" : "AuthorizationDto",
            "Party" : "PartyDto"
        }),
        "SavePartyResultsDto": genericMapper(SavePartyResultsDto, {
			"Arguments" : "SavePartyArgumentsDto",
			"Party" : "__type"
        }),
        // DTOs
        "PersonPartyDto": genericMapper(PersonPartyDto),
		"OrganizationPartyDto": genericMapper(OrganizationPartyDto),
		"FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.PersonPartyDto": genericMapper(PersonPartyDto),
		"FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.OrganizationPartyDto": genericMapper(OrganizationPartyDto),
		"PartyDto": genericMapper(PartyDto),
        "AuthorizationDto": genericMapper(AuthorizationDto)
    };

    var EventBinder = function(obj) {
        if (obj instanceof FetchPartyArgumentsDto) {
            $(obj).bind("fetchComplete", Events.FetchPartyArgumentsDto.fetchComplete);
        }
        if (obj instanceof SearchPartyArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SearchPartyArgumentsDto.fetchComplete);
        }
		if (obj instanceof SavePartyArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SavePartyArgumentsDto.fetchComplete);
        }
    };
    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }
        
        BindDataMapper(FetchPartyArgumentsDto, DataMapper.FetchPartyArgumentsDto);
        BindDataMapper(FetchPartyResultsDto, DataMapper.FetchPartyResultsDto);
        BindDataMapper(SearchPartyArgumentsDto, DataMapper.SearchPartyArgumentsDto);
        BindDataMapper(SearchPartyResultsDto, DataMapper.SearchPartyResultsDto);
		BindDataMapper(SavePartyArgumentsDto, DataMapper.SavePartyArgumentsDto);
        BindDataMapper(SavePartyResultsDto, DataMapper.SavePartyResultsDto);
    })();
    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }
        
        BindFetch(FetchPartyArgumentsDto, Services.Fetch);
        BindFetch(SearchPartyArgumentsDto, Services.Search);
		BindFetch(SavePartyArgumentsDto, Services.Save);

    })();
    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }

    this.prototype.main = function(selected) {
        if (typeof selected === "undefined") {
            selected = {};
        }
        // initialize the page
        $(Party).trigger("Party.Initialize");
        // pick the broker
        if (selected.buid) {
            $(Party).trigger("Party.BrokerChange", selected.buid);
        }
    };

    var PublicEvents = {
        "Party.BrokerChange": function(evt, newBroker) {
            if (State.Broker !== newBroker) {
                State.Broker = newBroker;
                $(Party).trigger("Party.Initialize");
            }
        },
        "Party.Fetch": function(evt, args) {
            if (State.Broker !== EmptyGuid) {
				var a = new FetchPartyArgumentsDto();
				a.Id = args.Id;
				a.Broker = State.Broker;
				
				a.Authorization = new AuthorizationDto();
				a.Authorization.Broker = a.Broker;                
				a.Authorization.Token = a.Broker;

				$(a).trigger("fetch");
			}
        },
        "Party.Search": function(evt, args) {
            if (State.Broker !== EmptyGuid) {
                var a = new SearchPartyArgumentsDto();
                a.Broker = State.Broker
                a.Authorization = new AuthorizationDto();
                a.Authorization.Broker = a.Broker;                
                a.Authorization.Token = a.Broker;
				a.MaximumRows = 10;
				a.StartRowIndex = 0;
				
				if(args.hasOwnProperty('PersonParty')) {
					a.Example = args.PersonParty;
					a.SortColumns = [
										{
											'ColumnName': 'GivenName',
											'Ascending': true
										}
									]
					a.Example.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.PersonPartyDto";
				} 
				if(args.hasOwnProperty('OrganizationParty')) {
					a.Example = args.OrganizationParty;
					a.SortColumns = [
										{
											'ColumnName': 'Name',
											'Ascending': true
										}
									]
					a.Example.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.OrganizationPartyDto";
				}

                $(a).trigger("fetch");
            }
        },
		"Party.Save": function(evt, args) {
            if (State.Broker !== EmptyGuid) {
                var a = new SavePartyArgumentsDto();
                a.Broker = State.Broker;
				
				if(args.hasOwnProperty('PersonParty')) {
					a.Party = args.PersonParty;
					a.Party.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.PersonPartyDto";
				} 
				if(args.hasOwnProperty('OrganizationParty')) {
					a.Party = args.OrganizationParty;
					a.Party.__type = "FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.OrganizationPartyDto";
				}
				
                a.Authorization = new AuthorizationDto();
                a.Authorization.Broker = a.Broker;                
                a.Authorization.Token = a.Broker;
                $(a).trigger("fetch");
            }
        }
    };
    $(Party).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
	this.PartyDto = PartyDto;
	this.PersonPartyDto = PersonPartyDto;
	this.OrganizationPartyDto = OrganizationPartyDto;
	
}).apply(Party);

