
if (typeof Contact === "undefined") {
    var Contact = {};
}

(function() {

    this.raises = this.raises || [];
    this.raises.push("Contact.BrokerChange");
    this.raises.push("Contact.Reset");
    this.raises.push("Contact.Load");
    this.raises.push("Contact.Save");
    this.raises.push("Contact.Delete");
    
    this.raises.push("Contact.Data_Email_Insert");
    this.raises.push("Contact.Data_Email_Delete");
    this.raises.push("Contact.Data_Email_Update");
    
    this.raises.push("Contact.Data_Phone_Insert");
    this.raises.push("Contact.Data_Phone_Delete");
    this.raises.push("Contact.Data_Phone_Update");

    this.raises.push("Contact.Data_PostalAddress_Insert");
    this.raises.push("Contact.Data_PostalAddress_Delete");
    this.raises.push("Contact.Data_PostalAddress_Update");
    
    this.listensTo = this.listensTo || [];
    this.listensTo.push("Contact.Layout");
    this.listensTo.push("Contact.Naming");
    this.listensTo.push("Contact.Loaded");
    this.listensTo.push("Contact.Saved");
    this.listensTo.push("Contact.Deleted");
    this.listensTo.push("Contact.Refresh");
    this.listensTo.push("Contact.ReferenceInformationLoaded");

    var State = {
        Naming: function (type_name, property_name, id) {
            var n = type_name;
            n += "[" + property_name + "]";
            if (!!id) {
                n += "[" + id + "]"
            }
            return n;
        },
        // dom elements
        Email: '#email',
        Phone: '#phone',
        PostalAddress: '#postal_address',
        // reference information
        PhoneLineTypes: [],
        ContactPointPurposes: []
    };

    function isFunction (obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    };

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function WithoutData (evt, data) {
        var l = new LayoutTemplateBuilder();
        l.clean();
        l.build();
        var c = new ControlsTemplateBuilder();
        c.clean();
        c.build();
        var e = new EmailTemplateBuilder();
        e.clean();
        e.build();
        var p = new PhoneTemplateBuilder();
        p.clean();
        p.build();
        var a = new PostalAddressTemplateBuilder();
        a.clean();
        a.build();
    }

    function WithData (evt, data) {
        // TODO: rebuild contact template with contact id
        var e = new EmailTemplateBuilder(data.Contact.Emails);
        e.clean();
        e.build();
        var p = new PhoneTemplateBuilder(data.Contact.Phones);
        p.clean();
        p.build();
        var a = new PostalAddressTemplateBuilder(data.Contact.PostalAddresses);
        a.clean();
        a.build();
    }

    var PublicEvents = {
        "Contact.Layout": function(evt, data) {
            if (!!data) {
                if (!!data.Email) {
                    State.Email = data.Email;
                }
                if (!!data.Phone) {
                    State.Phone = data.Phone;
                }
                if (!!data.PostalAddress) {
                    State.PostalAddress = data.PostalAddress;
                }
            }            
        },
        "Contact.Naming": function(evt, data) {
            if (isFunction(data)) {
                State.Naming = data;
            }
        },
        "Contact.Initialize": WithoutData,
        "Contact.Refresh": WithData,
        "Contact.Loaded": WithData,
        "Contact.Saved": WithData,
        "Contact.Deleted": WithoutData,
        "Contact.ReferenceInformationLoaded": function(evt, data) {
            // remember reference information
            State.PhoneLineTypes = data.PhoneLineTypes;
            State.ContactPointPurposes = data.ContactPointPurposes;
            // populate the page
            WithoutData(evt, data);
        }
    };

    $(Contact).bind(PublicEvents);

    var LayoutTemplateBuilder = function() {
        this.$dom = {
            Form: $('#subject'),
            Broker: $('#broker')
        };
    };

    LayoutTemplateBuilder.prototype = {
        build: function() {
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerEtc, this));
        },
        clean: function() {
            this.$dom.Form.unbind('click');
        },
        changeBrokerEtc: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    n = tgt.href.substring(i),
                    e = document.getElementById(n.substring(1)),
                    v = (e == null ? null : e.value);
                if (n == '#broker') {
                    $(Contact).trigger("Contact.BrokerChange", v);
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var ControlsTemplateBuilder = function() {
        this.$dom = {
            Form: $('#controls'),
            Contact: $('#contact_id')
        };
    };

    ControlsTemplateBuilder.prototype = {
        build: function() {
            this.$dom.Form.bind('click', $.proxy(this.operation, this));
        },
        clean: function() {
            this.$dom.Form.unbind('click');
        },
        operation: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                    n = tgt.href.substring(i),
                    e = this.$dom.Contact,
                    v = e.val();
                if (n == '#new') {
                    $(Contact).trigger("Contact.Initialize");
                }
                else if (n == '#load') {
                    $(Contact).trigger("Contact.Load", {Id: v});
                }
                else if (n == '#save') {
                    $(Contact).trigger("Contact.Save");
                }
                else if (n == '#delete') {
                    $(Contact).trigger("Contact.Delete");
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var EmailTemplateBuilder = function(data) {
        this.$el = $(State.Email);
        this.$dom = {
            Form: $('#email')
        };
        this.data = data || [];
    };

    EmailTemplateBuilder.prototype = {
        build: function() {
            var __n = State.Naming
                __t = __n('Email', 'Value'),
                __c = __n('Email', 'IsPrimary'),
                __s = __n('Email', 'ContactPointPurpose'),
                __a = __n('Email', 'Add');
            $('<table class="email" />').append(
                $('<caption />').text('Email'),
                $('<thead />').append(
                    $('<tr/>').append(
                        $('<th class="value">Email</th>'),
                        $('<th class="primary">Is <br /> Primary</th>'),
                        $('<th class="purpose">Purpose</th>'),
                        $('<th class="action"></th>')
                    )
                ),
                $('<tfoot />').append($.proxy(function(index, html) {
                    // pre-declared variables
                    var _s = $('<select />').attr({id: __s, name: __s}),
                        _x = $('<tfoot/>');
                    // bind reference data
                    _s.append($('<option/>').val('').text('Please Select'));
                    $.each(State.ContactPointPurposes, function(index, value) {
                        var opt = $('<option/>').val(value.Id).text(value.Name);
                        _s.append(opt);
                    });
                    // build dom fragment
                    $(_x).append(
                        $('<tr />').append(
                            $('<td />').append(
                                $('<input type="text" />').attr({id: __t, name: __t})
                            ),
                            $('<td />').append(
                                $('<input type="checkbox" />').attr({id: __c, name: __c})
                            ),
                            $('<td />').append(
                                _s
                            ),
                            $('<td />').append(
                                $('<a />').click($.proxy(this.add, this)).attr({href: '#' + __a}).text('Add')
                            )
                        )
                    );
                    // return dom fragment
                    return _x.children();
                }, this)),
                $('<tbody />').append($.proxy(function(index, html) {
                    var x = $('<tbody/>');
                    for (var i = 0, l = this.data.length; i < l; i++) {
                        $(x).append(this.row(this.data[i]));
                    }
                    return x.children();
                }, this))
            ).appendTo(this.$el);
        },
        clean: function() {
            this.$el.html('');
        },
        row: function(d) {
            // input names
            var _n = State.Naming,
                _t = _n('Email', 'Value', d.Id),
                _c = _n('Email', 'IsPrimary', d.Id),
                _s = _n('Email', 'ContactPointPurpose', d.Id);
            // input elements
            var t = $('<input type="text" />').attr({id: _t, name: _t, readonly: 'readonly'}),
                c = $('<input type="checkbox" />').attr({id: _c, name: _c, readonly: 'readonly'}),
                s = $('<select />').attr({id: _s, name: _s, readonly: 'readonly'});
            // row controls
            var _edt = _n('Email', 'Edit', d.Id),
                _del = _n('Email', 'Delete', d.Id),
                _sve = _n('Email', 'Save', d.Id),
                _cxl = _n('Email', 'Cancel', d.Id);
            var edt = $('<a />').attr({href: '#' + _edt}).text('Edit'),
                del = $('<a />').attr({href: '#' + _del}).text('Delete'),
                sve = $('<a />').attr({href: '#' + _sve}).text('Save').hide(),
                cxl = $('<a />').attr({href: '#' + _cxl}).text('Cancel').hide();
            // row state functions / closures
            var abc = function (evt) {
                // buttons
                edt.hide();
                del.hide();
                sve.show();
                cxl.show();
                // inputs
                t.removeAttr('readonly');
                c.removeAttr('readonly');
                s.removeAttr('readonly');
                // store
                t.data('x', t.val());
                c.data('x', c.val());
                s.data('x', s.val());
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            var cba = function (evt) {
                // buttons
                edt.show();
                del.show();
                sve.hide();
                cxl.hide();
                // inputs
                t.attr('readonly', 'readonly');
                c.attr('readonly', 'readonly');
                s.attr('readonly', 'readonly');
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            var rbk = function (evt) {
                // roll back
                t.val(t.data('x'));
                c.val(c.data('x'));
                s.val(s.data('x'));
                // clean up
                t.removeData('x');
                c.removeData('x');
                s.removeData('x');
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            // bind button behaviours
            edt.click($.proxy(abc, this));
            del.click($.proxy(abc, this)).click($.proxy(this.del, this));
            cxl.click($.proxy(cba, this)).click($.proxy(rbk, this));
            sve.click($.proxy(cba, this)).click($.proxy(this.upd, this));
            // reference information
            this.add_contact_point_purposes(s);
            // set input values
            t.val(d.Value);
            if (d.IsPrimary) {
                c.attr('checked', 'checked');
            }
            s.val(d.ContactPointPurpose.Id);
            // add row
            var r = $('<tr />').append(
                $('<td />').append(t),
                $('<td />').append(c),
                $('<td />').append(s),
                $('<td />').append(edt, '  ', del, '  ', sve, '  ', cxl)
            );
            return r;
        },
        add_contact_point_purposes: function (el) {
            el.append($('<option/>').val('').text('Please Select'));
            $.each(State.ContactPointPurposes, function(index, value) {
                var opt = $('<option/>').val(value.Id).text(value.Name);
                el.append(opt);
            });
        },
        add: function(evt) {
            // names
            var __n = State.Naming
                __t = __n('Email', 'Value'),
                __c = __n('Email', 'IsPrimary'),
                __s = __n('Email', 'ContactPointPurpose');
            // elements
            var _t = document.getElementById(__t),
                _c = document.getElementById(__c),
                _s = document.getElementById(__s);
            // values
            var t = $(_t).val(),
                c = $(_c).is(':checked'),
                s = $(_s).val(),
                u = $(_s).find('option:selected').text();
            // create the object
            var row = {
                Value: t,
                IsPrimary: c,
                ContactPointPurpose: { Id: s, Name: u }
            };
            // let people know there is a new email address ...
            $(Contact).trigger("Contact.Data_Email_Insert", [row]);
            // clear out that useless information now, sucka!
            $(_t).val('');
            $(_c).removeAttr("checked");
            $(_s).val('');
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        del: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var h = tgt.href,
                    w = h.lastIndexOf('['),
                    x = w+1,
                    y = h.lastIndexOf(']'),
                    z = h.substring(x, y);
                if (isNumber(z)) {
                    $(Contact).trigger("Contact.Data_Email_Delete", [z]);
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        upd: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var h = tgt.href,
                    w = h.lastIndexOf('['),
                    x = w+1,
                    y = h.lastIndexOf(']'),
                    z = h.substring(x, y);
                if (isNumber(z)) {
                    // names
                    var __n = State.Naming
                        __t = __n('Email', 'Value', z),
                        __c = __n('Email', 'IsPrimary', z),
                        __s = __n('Email', 'ContactPointPurpose', z);
                    // elements
                    var _t = document.getElementById(__t),
                        _c = document.getElementById(__c),
                        _s = document.getElementById(__s);
                    // values
                    var t = $(_t).val(),
                        c = $(_c).is(':checked'),
                        s = $(_s).val(),
                        u = $(_s).find('option:selected').text();
                    // create the object
                    var row = {
                        Id: z,
                        Value: t,
                        IsPrimary: c,
                        ContactPointPurpose: { Id: s, Name: u }
                    };
                    // let people know there is a new email address ...
                    $(Contact).trigger("Contact.Data_Email_Update", [row]);
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var PhoneTemplateBuilder = function(data) {
        this.$el = $(State.Phone);
        this.data = data || [];
    };

    PhoneTemplateBuilder.prototype = {
        build: function() {
            var __n = State.Naming
                __a = __n('Phone', 'CountryCode'),
                __b = __n('Phone', 'AreaCode'),
                __c = __n('Phone', 'CentralOfficeCode'),
                __d = __n('Phone', 'SubscriberNumber'),
                __e = __n('Phone', 'Extension'),
                __f = __n('Phone', 'IsPrimary'),
                __g = __n('Phone', 'ContactPointPurpose'),
                __i = __n('Phone', 'PhoneLineType'),
                __h = __n('Phone', 'Add');
            var _g = $('<select />').attr({id: __g, name: __g}),
                _i = $('<select />').attr({id: __i, name: __i});
            this.add_contact_point_purposes(_g);
            this.add_phone_line_types(_i);
            $('<table class="phone" />').append(
                $('<caption />').text('Phone'),
                $('<thead />').append(
                    $('<tr/>').append(
                        $('<th class="country">Country</th>'),
                        $('<th class="phone" colspan="3">Phone</th>'),
                        $('<th class="extension">Ext</th>'),
                        $('<th class="primary">Is <br /> Primary</th>'),
                        $('<th class="purpose">Purpose</th>'),
                        $('<th class="purpose">Type</th>'),
                        $('<th class="action"></th>')
                    )
                ),
                $('<tfoot />').append(
                    $('<tr />').append(
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __a, name: __a})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __b, name: __b})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __c, name: __c})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __d, name: __d})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __e, name: __e})
                        ),
                        $('<td />').append(
                            $('<input type="checkbox" />').attr({id: __f, name: __f})
                        ),
                        $('<td />').append(
                            _g
                        ),
                        $('<td />').append(
                            _i
                        ),
                        $('<td />').append(
                            $('<a />').click($.proxy(this.add, this)).attr({href: '#' + __h}).text('Add')
                        )
                    )
                ),
                $('<tbody />').append($.proxy(function(index, html) {
                    var x = $('<tbody/>');
                    for (var i = 0, l = this.data.length; i < l; i++) {
                        $(x).append(this.row(this.data[i]));
                    }
                    return x.children();
                }, this))
            ).appendTo(this.$el);
        },
        clean: function() {
            this.$el.html('');
        },
        row: function(item) {
            // element names
            var _n = State.Naming,
                _a = _n('Phone', 'CountryCode', item.Id),
                _b = _n('Phone', 'AreaCode', item.Id),
                _c = _n('Phone', 'CentralOfficeCode', item.Id),
                _d = _n('Phone', 'SubscriberNumber', item.Id),
                _e = _n('Phone', 'Extension', item.Id),
                _f = _n('Phone', 'IsPrimary', item.Id),
                _g = _n('Phone', 'ContactPointPurpose', item.Id),
                _i = _n('Phone', 'PhoneLineType', item.Id),
                _h = _n('Phone', 'Delete', item.Id);
            // elements
            var a = $('<input type="text" />').attr({id: _a, name: _a, readonly: 'readonly'}),
                b = $('<input type="text" />').attr({id: _b, name: _b, readonly: 'readonly'}),
                c = $('<input type="text" />').attr({id: _c, name: _c, readonly: 'readonly'}),
                d = $('<input type="text" />').attr({id: _d, name: _d, readonly: 'readonly'}),
                e = $('<input type="text" />').attr({id: _e, name: _e, readonly: 'readonly'}),
                f = $('<input type="checkbox" />').attr({id: _f, name: _f, readonly: 'readonly'}),
                g = $('<select />').attr({id: _g, name: _g, readonly: 'readonly'}),
                i = $('<select />').attr({id: _i, name: _i, readonly: 'readonly'});
            // row controls
            var _edt = _n('Phone', 'Edit', item.Id),
                _del = _n('Phone', 'Delete', item.Id),
                _sve = _n('Phone', 'Save', item.Id),
                _cxl = _n('Phone', 'Cancel', item.Id);
            var edt = $('<a />').attr({href: '#' + _edt}).text('Edit'),
                del = $('<a />').attr({href: '#' + _del}).text('Delete'),
                sve = $('<a />').attr({href: '#' + _sve}).text('Save').hide(),
                cxl = $('<a />').attr({href: '#' + _cxl}).text('Cancel').hide();
            // row state functions / closures
            var abc = function (evt) {
                // buttons
                edt.hide();
                del.hide();
                sve.show();
                cxl.show();
                // inputs
                $.each([a, b, c, d, e, f, g, i], function(index, value) { value.removeAttr('readonly'); });
                // store
                $.each([a, b, c, d, e, f, g, i], function(index, value) { value.data('x', value.val()); });
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            var cba = function (evt) {
                // buttons
                edt.show();
                del.show();
                sve.hide();
                cxl.hide();
                // inputs
                $.each([a, b, c, d, e, f, g, i], function(index, value) { value.attr('readonly', 'readonly'); });
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            var rbk = function (evt) {
                // roll back
                $.each([a, b, c, d, e, f, g, i], function(index, value) { value.val(value.data('x')); });
                // clean up
                $.each([a, b, c, d, e, f, g, i], function(index, value) { value.removeData('x'); });
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            // bind button behaviours
            edt.click($.proxy(abc, this));
            del.click($.proxy(abc, this)).click($.proxy(this.del, this));
            cxl.click($.proxy(cba, this)).click($.proxy(rbk, this));
            sve.click($.proxy(cba, this)).click($.proxy(this.upd, this));
            // reference information
            this.add_contact_point_purposes(g);
            this.add_phone_line_types(i);
            // set input values
            a.val(item.CountryCode);
            b.val(item.AreaCode);
            c.val(item.CentralOfficeCode);
            d.val(item.SubscriberNumber);
            e.val(item.Extension);
            if (item.IsPrimary) {
                f.attr('checked', 'checked');
            }
            g.val(item.ContactPointPurpose.Id);
            i.val(item.PhoneLineType.Id);
            // add row
            var r = 
                $('<tr />').append(
                    $('<td />').append(a),
                    $('<td />').append(b),
                    $('<td />').append(c),
                    $('<td />').append(d),
                    $('<td />').append(e),
                    $('<td />').append(f),
                    $('<td />').append(g),
                    $('<td />').append(i),
                    $('<td />').append(edt, ' ', del, ' ', cxl, ' ', sve)
                );
            return r;
        },
        add_contact_point_purposes: function (el) {
            el.append($('<option/>').val('').text('Please Select'));
            $.each(State.ContactPointPurposes, function(index, value) {
                var opt = $('<option/>').val(value.Id).text(value.Name);
                el.append(opt);
            });
        },
        add_phone_line_types: function (el) {
            el.append($('<option/>').val('').text('Please Select'));
            $.each(State.PhoneLineTypes, function(index, value) {
                var opt = $('<option/>').val(value.Id).text(value.Name);
                el.append(opt);
            });
        },
        add: function (evt, data) {
            // names
            var __n = State.Naming,
                __a = __n('Phone', 'CountryCode'),
                __b = __n('Phone', 'AreaCode'),
                __c = __n('Phone', 'CentralOfficeCode'),
                __d = __n('Phone', 'SubscriberNumber'),
                __e = __n('Phone', 'Extension'),
                __f = __n('Phone', 'IsPrimary'),
                __g = __n('Phone', 'ContactPointPurpose'),
                __i = __n('Phone', 'PhoneLineType');
            // elements
            var _a = document.getElementById(__a),
                _b = document.getElementById(__b),
                _c = document.getElementById(__c),
                _d = document.getElementById(__d),
                _e = document.getElementById(__e),
                _f = document.getElementById(__f),
                _g = document.getElementById(__g),
                _i = document.getElementById(__i);
            // values
            var a = $(_a).val(),
                b = $(_b).val(),
                c = $(_c).val(),
                d = $(_d).val(),
                e = $(_e).val(),
                f = $(_f).is('checked'),
                g = $(_g).val(),
                i = $(_i).val(),
               gt = $(_g).find('option:selected').text(),
               it = $(_i).find('option:selected').text();
            // create the object
            var row = {
                CountryCode: a,
                AreaCode: b,
                CentralOfficeCode: c,
                SubscriberNumber: d,
                Extension: e,
                IsPrimary: f,
                ContactPointPurpose: { Id: g, Name: gt },
                PhoneLineType: { Id: g, Name: gt }
            };
            // let people know there is a new email address ...
            $(Contact).trigger("Contact.Data_Phone_Insert", [row]);
            // clear out that useless information now, sucka!
            $(_a).val('');
            $(_b).val('');
            $(_c).val('');
            $(_d).val('');
            $(_e).val('');
            $(_f).removeAttr('checked'),
            $(_g).val('');
            $(_i).val('');
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        del: function (evt, data) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var h = tgt.href,
                    w = h.lastIndexOf('['),
                    x = w+1,
                    y = h.lastIndexOf(']'),
                    z = h.substring(x, y);
                if (isNumber(z)) {
                    $(Contact).trigger("Contact.Data_Phone_Delete", [z]);
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        upd: function (evt, data) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var h = tgt.href,
                    w = h.lastIndexOf('['),
                    x = w+1,
                    y = h.lastIndexOf(']'),
                    z = h.substring(x, y);
                if (isNumber(z)) {
                    // names
                    var __n = State.Naming,
                        __a = __n('Phone', 'CountryCode', z),
                        __b = __n('Phone', 'AreaCode', z),
                        __c = __n('Phone', 'CentralOfficeCode', z),
                        __d = __n('Phone', 'SubscriberNumber', z),
                        __e = __n('Phone', 'Extension', z),
                        __f = __n('Phone', 'IsPrimary', z),
                        __g = __n('Phone', 'ContactPointPurpose', z),
                        __i = __n('Phone', 'PhoneLineType', z);
                    // elements
                    var _a = document.getElementById(__a),
                        _b = document.getElementById(__b),
                        _c = document.getElementById(__c),
                        _d = document.getElementById(__d),
                        _e = document.getElementById(__e),
                        _f = document.getElementById(__f),
                        _g = document.getElementById(__g),
                        _i = document.getElementById(__i);
                    var a = $(_a).val(),
                        b = $(_b).val(),
                        c = $(_c).val(),
                        d = $(_d).val(),
                        e = $(_e).val(),
                        f = $(_f).is('checked'),
                        g = $(_g).val(),
                        i = $(_i).val(),
                       gt = $(_g).find('option:selected').text(),
                       it = $(_i).find('option:selected').text();
                    // create the object
                    var row = {
                        CountryCode: a,
                        AreaCode: b,
                        CentralOfficeCode: c,
                        SubscriberNumber: d,
                        Extension: e,
                        IsPrimary: f,
                        ContactPointPurpose: { Id: g, Name: gt },
                        PhoneLineType: { Id: g, Name: gt }
                    };
                    // let people know there is an updated phone record ...
                    $(Contact).trigger("Contact.Data_Phone_Update", [row]);
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var PostalAddressTemplateBuilder = function(data) {
        this.$el = $(State.PostalAddress);
        this.data = data || [];
    };

    PostalAddressTemplateBuilder.prototype = {
        build: function() {
            // names
            var __n = State.Naming,
                __a = __n('PostalAddress', 'Line1'),
                __b = __n('PostalAddress', 'Line2'),
                __c = __n('PostalAddress', 'City'),
                __d = __n('PostalAddress', 'State'),
                __e = __n('PostalAddress', 'Zip'),
                __f = __n('PostalAddress', 'ZipPlus4'),
                __g = __n('PostalAddress', 'IsPrimary'),
                __h = __n('PostalAddress', 'ContactPointPurpose'),
                __j = __n('PostalAddress', 'Add');
            // elements
            var _h = $('<select />').attr({id: __h, name: __h});
            // reference information
            this.add_contact_point_purposes(_h);
            // build dom fragment
            $('<table class="postal_address" />').append(
                $('<caption />').text('Postal Address'),
                $('<thead />').append(
                    $('<tr/>').append(
                        $('<th class="line1">Line 1</th>'),
                        $('<th class="line2">Line 2</th>'),
                        $('<th class="city">City</th>'),
                        $('<th class="state">State</th>'),
                        $('<th class="zip">Zip</th>'),
                        $('<th class="zip_plus_4">Zip+4</th>'),
                        $('<th class="primary">Is <br /> Primary</th>'),
                        $('<th class="purpose">Purpose</th>'),
                        $('<th class="action"></th>')
                    )
                ),
                $('<tfoot />').append(
                    $('<tr />').append(
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __a, name: __a})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __b, name: __b})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __c, name: __c})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __d, name: __d})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __e, name: __e})
                        ),
                        $('<td />').append(
                            $('<input type="text" />').attr({id: __f, name: __f})
                        ),
                        $('<td />').append(
                            $('<input type="checkbox" />').attr({id: __g, name: __g})
                        ),
                        $('<td />').append(
                            _h
                        ),
                        $('<td />').append(
                            $('<a />').click($.proxy(this.add, this)).attr({href: '#' + __j}).text('Add')
                        )
                    )
                ),
                $('<tbody />').append($.proxy(function(index, html) {
                    var x = $('<tbody/>');
                    for (var i = 0, l = this.data.length; i < l; i++) {
                        $(x).append(this.row(this.data[i]));
                    }
                    return x.children();
                }, this))
            ).appendTo(this.$el);
        },
        clean: function() {
            this.$el.html('');
        },
        row: function(item) {
            // element names
            var _n = State.Naming,
                _a = _n('PostalAddress', 'Line1', item.Id),
                _b = _n('PostalAddress', 'Line2', item.Id),
                _c = _n('PostalAddress', 'City', item.Id),
                _d = _n('PostalAddress', 'State', item.Id),
                _e = _n('PostalAddress', 'Zip', item.Id),
                _f = _n('PostalAddress', 'ZipPlus4', item.Id),
                _g = _n('PostalAddress', 'IsPrimary', item.Id),
                _h = _n('PostalAddress', 'ContactPointPurpose', item.Id),
                _j = _n('PostalAddress', 'Delete', item.Id);
            // elements
            var a = $('<input type="text" />').attr({id: _a, name: _a, readonly: 'readonly'}),
                b = $('<input type="text" />').attr({id: _b, name: _b, readonly: 'readonly'}),
                c = $('<input type="text" />').attr({id: _c, name: _c, readonly: 'readonly'}),
                d = $('<input type="text" />').attr({id: _d, name: _d, readonly: 'readonly'}),
                e = $('<input type="text" />').attr({id: _e, name: _e, readonly: 'readonly'}),
                f = $('<input type="text" />').attr({id: _f, name: _f, readonly: 'readonly'}),
                g = $('<input type="checkbox" />').attr({id: _g, name: _g, readonly: 'readonly'}),
                h = $('<select />').attr({id: _h, name: _h, readonly: 'readonly'});
            // row controls
            var _edt = _n('PostalAddress', 'Edit', item.Id),
                _del = _n('PostalAddress', 'Delete', item.Id),
                _sve = _n('PostalAddress', 'Save', item.Id),
                _cxl = _n('PostalAddress', 'Cancel', item.Id);
            var edt = $('<a />').attr({href: '#' + _edt}).text('Edit'),
                del = $('<a />').attr({href: '#' + _del}).text('Delete'),
                sve = $('<a />').attr({href: '#' + _sve}).text('Save').hide(),
                cxl = $('<a />').attr({href: '#' + _cxl}).text('Cancel').hide();
            // row state functions / closures
            var abc = function (evt) {
                // buttons
                edt.hide();
                del.hide();
                sve.show();
                cxl.show();
                // inputs
                $.each([a, b, c, d, e, f, g, h], function(index, value) { value.removeAttr('readonly'); });
                // store
                $.each([a, b, c, d, e, f, g, h], function(index, value) { value.data('x', value.val()); });
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            var cba = function (evt) {
                // buttons
                edt.show();
                del.show();
                sve.hide();
                cxl.hide();
                // inputs
                $.each([a, b, c, d, e, f, g, h], function(index, value) { value.attr('readonly', 'readonly'); });
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            var rbk = function (evt) {
                // roll back
                $.each([a, b, c, d, e, f, g, h], function(index, value) { value.val(value.data('x')); });
                // clean up
                $.each([a, b, c, d, e, f, g, h], function(index, value) { value.removeData('x'); });
                // kill the event
                evt.stopPropagation(); evt.preventDefault(); return false;
            };
            // bind button behaviours
            edt.click($.proxy(abc, this));
            del.click($.proxy(abc, this)).click($.proxy(this.del, this));
            cxl.click($.proxy(cba, this)).click($.proxy(rbk, this));
            sve.click($.proxy(cba, this)).click($.proxy(this.upd, this));
            // reference information
            this.add_contact_point_purposes(h);
            // set input values
            a.val(item.Line1);
            b.val(item.Line2);
            c.val(item.City);
            d.val(item.State);
            e.val(item.Zip);
            f.val(item.ZipPlus4);
            if (item.IsPrimary) {
                g.attr('checked', 'checked');
            }
            h.val(item.ContactPointPurpose.Id);
            // add row
            var r = 
                $('<tr />').append(
                    $('<td />').append(a),
                    $('<td />').append(b),
                    $('<td />').append(c),
                    $('<td />').append(d),
                    $('<td />').append(e),
                    $('<td />').append(f),
                    $('<td />').append(g),
                    $('<td />').append(h),
                    $('<td />').append(edt, ' ', del, ' ', sve, ' ', cxl)
                );
            return r;
        },
        add_contact_point_purposes: function (el) {
            el.append($('<option/>').val('').text('Please Select'));
            $.each(State.ContactPointPurposes, function(index, value) {
                var opt = $('<option/>').val(value.Id).text(value.Name);
                el.append(opt);
            });
        },
        add: function (evt, data) {
            // names
            var __n = State.Naming,
                __a = __n('PostalAddress', 'Line1'),
                __b = __n('PostalAddress', 'Line2'),
                __c = __n('PostalAddress', 'City'),
                __d = __n('PostalAddress', 'State'),
                __e = __n('PostalAddress', 'Zip'),
                __f = __n('PostalAddress', 'ZipPlus4'),
                __g = __n('PostalAddress', 'IsPrimary'),
                __h = __n('PostalAddress', 'ContactPointPurpose');
            // elements
            var _a = document.getElementById(__a),
                _b = document.getElementById(__b),
                _c = document.getElementById(__c),
                _d = document.getElementById(__d),
                _e = document.getElementById(__e),
                _f = document.getElementById(__f),
                _g = document.getElementById(__g),
                _h = document.getElementById(__h);
            // values
            var a = $(_a).val(),
                b = $(_b).val(),
                c = $(_c).val(),
                d = $(_d).val(),
                e = $(_e).val(),
                f = $(_f).val(),
                g = $(_g).is('checked'),
                h = $(_h).val(),
               ht = $(_h).find('option:selected').text();
            // create the object
            var row = {
                Line1: a,
                Line2: b,
                City: c,
                State: d,
                Zip: e,
                ZipPlus4: f,
                IsPrimary: g,
                ContactPointPurpose: { Id: h, Name: ht }
            };
            // let people know there is a new email address ...
            $(Contact).trigger("Contact.Data_PostalAddress_Insert", [row]);
            // clear out that useless information now, sucka!
            $(_a).val('');
            $(_b).val('');
            $(_c).val('');
            $(_d).val('');
            $(_e).val('');
            $(_f).val(''),
            $(_g).removeAttr('checked');
            $(_h).val('');
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        del: function (evt, data) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var h = tgt.href,
                    w = h.lastIndexOf('['),
                    x = w+1,
                    y = h.lastIndexOf(']'),
                    z = h.substring(x, y);
                if (isNumber(z)) {
                    $(Contact).trigger("Contact.Data_PostalAddress_Delete", [z]);
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        upd: function (evt, data) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var h = tgt.href,
                    w = h.lastIndexOf('['),
                    x = w+1,
                    y = h.lastIndexOf(']'),
                    z = h.substring(x, y);
                if (isNumber(z)) {
                    // names
                    var __n = State.Naming,
                        __a = __n('PostalAddress', 'Line1', z),
                        __b = __n('PostalAddress', 'Line2', z),
                        __c = __n('PostalAddress', 'City', z),
                        __d = __n('PostalAddress', 'State', z),
                        __e = __n('PostalAddress', 'Zip', z),
                        __f = __n('PostalAddress', 'ZipPlus4', z),
                        __g = __n('PostalAddress', 'IsPrimary', z),
                        __h = __n('PostalAddress', 'ContactPointPurpose', z);
                    // elements
                    var _a = document.getElementById(__a),
                        _b = document.getElementById(__b),
                        _c = document.getElementById(__c),
                        _d = document.getElementById(__d),
                        _e = document.getElementById(__e),
                        _f = document.getElementById(__f),
                        _g = document.getElementById(__g),
                        _h = document.getElementById(__h);
                    // values
                    var a = $(_a).val(),
                        b = $(_b).val(),
                        c = $(_c).val(),
                        d = $(_d).val(),
                        e = $(_e).val(),
                        f = $(_f).val(),
                        g = $(_g).is('checked'),
                        h = $(_h).val(),
                       ht = $(_h).find('option:selected').text();
                    // create the object
                    var row = {
                        Line1: a,
                        Line2: b,
                        City: c,
                        State: d,
                        Zip: e,
                        ZipPlus4: f,
                        IsPrimary: g,
                        ContactPointPurpose: { Id: h, Name: ht }
                    };
                    // let people know there is an updated postal record ...
                    $(Contact).trigger("Contact.Data_PostalAddress_Update", [row]);
                }
            }
            // kill the event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

}).apply(Contact);
