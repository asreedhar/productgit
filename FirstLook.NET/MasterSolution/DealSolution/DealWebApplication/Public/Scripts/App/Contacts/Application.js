if (typeof Contact !== "function") {
    var Contact = function() {};
}

(function() {
    
    var EmptyGuid = '00000000-0000-0000-0000-000000000000';

    var State = {
        Broker: EmptyGuid,
        Token: EmptyGuid,               // TODO: Licensing Token for Deal API
        Sequence: -1,                   // TODO: Move to instance state
        Contact: new ContactDto()       // TODO: Move to instance state
    };

    /* =================== */
    /* == START: Classes = */
    /* =================== */
    
    /* == DTO == */

    function ProductTypeDto() {
        this.Id = 0;
        this.Name = '';
    }

    function AuthorizationDto() {
        this.Token = EmptyGuid;
    }

    function ContactDto() {
        this.Id = 0;
        this.RevisionNumber = 0;
        this.Emails = [];
        this.Phones = [];
        this.PostalAddresses = [];
    }

    function ContactPointPurposeDto() {
        this.Id = 0;
        this.Name = 0;
    }

    function EmailDto() {
        // parent
        this.Id = 0;
        this.RevisionNumber = 0;
        this.ContactPointPurpose = new ContactPointPurposeDto();
        this.IsPrimary = false;
        // myself
        this.Value = '';
    }

    function PhoneDto() {
        // parent
        this.Id = 0;
        this.RevisionNumber = 0;
        this.ContactPointPurpose = new ContactPointPurposeDto();
        this.IsPrimary = false;
        // myself
        this.PhoneLineType = null;
        this.CountryCode = '';
        this.AreaCode = '';
        this.CentralOfficeCode = '';
        this.SubscriberNumber = '';
        this.Extension = null;
    }
    
    function PhoneLineTypeDto() {
        this.Id = 0;
        this.Name = 0;
    }

    function PostalAddressDto() {
        // parent
        this.Id = 0;
        this.RevisionNumber = 0;
        this.ContactPointPurpose = new ContactPointPurposeDto();
        this.IsPrimary = false;
        // myself
        this.Line1 = '';
        this.Line2 = '';
        this.City = '';
        this.State = '';
        this.Zip = '';
        this.ZipPlus4 = '';
    }

    /* == Envelopes == */

    function ReferenceInformationArgumentsDto ()  {
        // parent properties
        this.Authorization = new AuthorizationDto();
        this.Broker = EmptyGuid;
        // my properties
        this.Id = 0;
        EventBinder(this);
    }

    function ReferenceInformationResultsDto ()  {
        // parent properties
        this.AuthorizationStatus = 0;
        this.Arguments = null;
        // my properties
        this.ContactPointPurposes = null;
        this.PhoneLineTypes = null;
    }

    function FetchContactArgumentsDto ()  {
        // parent properties
        this.Authorization = new AuthorizationDto();
        this.Broker = EmptyGuid;
        // my properties
        this.Id = 0;
        EventBinder(this);
    }

    function FetchContactResultsDto ()  {
        // parent properties
        this.AuthorizationStatus = 0;
        this.Arguments = null;
        // my properties
        this.Contact = null;
    }
    
    function DeleteContactArgumentsDto ()  {
        // parent properties
        this.Authorization = new AuthorizationDto();
        this.Broker = EmptyGuid;
        // my properties
        this.Id = 0;
        this.RevisionNumber = 0;
        EventBinder(this);
    }

    function DeleteContactResultsDto ()  {
        // parent properties
        this.AuthorizationStatus = 0;
        this.Arguments = null;
        // my properties
        this.Success = false;
    }

    function SaveContactArgumentsDto ()  {
        // parent properties
        this.Authorization = new AuthorizationDto();
        this.Broker = EmptyGuid;
        // my properties
        this.Contact = null;
        EventBinder(this);
    }

    function SaveContactResultsDto ()  {
        // parent properties
        this.AuthorizationStatus = 0;
        this.Arguments = null;
        // my properties
        this.Contact = null;
    }

    /* =================== */
    /* == END: Classes == */
    /* =================== */

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Contact).trigger("Contacts.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "ReferenceInformation": genericService("/Deal/Services/Contacts.asmx/ReferenceInformation", ReferenceInformationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes.ReferenceInformationArgumentsDto",
                    "Broker": this.Broker,
                    "Authorization": this.Authorization
                }
            });
        }),
        "Fetch": genericService("/Deal/Services/Contacts.asmx/Fetch", FetchContactResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes.FetchContactArgumentsDto",
                    "Broker": this.Broker,
                    "Id": this.Id,
                    "Authorization": this.Authorization
                }
            });
        }),
        "Save": genericService("/Deal/Services/Contacts.asmx/Save", SaveContactResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes.SaveContactArgumentsDto",
                    "Broker": this.Broker,
                    "Contact": this.Contact,
                    "Authorization": this.Authorization
                }
            });
        }),
        "Delete": genericService("/Deal/Services/Contacts.asmx/Delete", DeleteContactResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes.DeleteContactArgumentsDto",
                    "Broker": this.Broker,
                    "Id": this.Id,
                    "RevisionNumber": this.RevisionNumber,
                    "Authorization": this.Authorization
                }
            });
        })
    };

    var Events = {
        "ReferenceInformationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Contact).trigger("Contact.ReferenceInformationLoaded", [data]);
            }
        },
        "FetchContactArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Contact).trigger("Contact.Loaded", [data]);
            }
        },
        "SaveContactArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Contact).trigger("Contact.Saved", [data]);
            }
        },
        "DeleteContactArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Contact).trigger("Contact.Deleted", [data]);
            }
        }
    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        // Envelopes
        "ReferenceInformationArgumentsDto": genericMapper(ReferenceInformationArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "ReferenceInformationResultsDto": genericMapper(ReferenceInformationResultsDto, {
            "Arguments": "ReferenceInformationArgumentsDto"
        }),
        "FetchContactArgumentsDto": genericMapper(FetchContactArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "FetchContactResultsDto": genericMapper(FetchContactResultsDto, {
            "Arguments": "FetchContactArgumentsDto",
            "Contact": "ContactDto",
            "Authorization": "AuthorizationDto"
        }),
        "SaveContactArgumentsDto": genericMapper(SaveContactArgumentsDto, {
            "Authorization": "AuthorizationDto",
            "Contact": "ContactDto"
        }),
        "SaveContactResultsDto": genericMapper(SaveContactResultsDto, {
            "Arguments": "SaveContactArgumentsDto",
            "Contact": "ContactDto"
        }),
        "DeleteContactArgumentsDto": genericMapper(DeleteContactArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "DeleteContactResultsDto": genericMapper(DeleteContactResultsDto, {
            "Arguments": "DeleteContactArgumentsDto"
        }),
        // DTOs
        "AuthorizationDto": genericMapper(AuthorizationDto),
        "ContactDto": genericMapper(ContactDto, {
            "Emails": "EmailDto",
            "Phones": "PhoneDto",
            "PostalAddresses": "PostalAddressDto"
        }),
        "EmailDto": genericMapper(EmailDto, {
            "ContactPointPurpose": "ContactPointPurposeDto"
        }),
        "PhoneDto": genericMapper(PhoneDto, {
            "ContactPointPurpose": "ContactPointPurposeDto",
            "PhoneLineType": "PhoneLineTypeDto"
        }),
        "PostalAddressDto": genericMapper(PostalAddressDto, {
            "ContactPointPurpose": "ContactPointPurposeDto"
        }),
        "ContactPointPurposeDto": genericMapper(ContactPointPurposeDto),
        "PhoneLineTypeDto": genericMapper(PhoneLineTypeDto)
    };

    var EventBinder = function(obj) {
        if (obj instanceof ReferenceInformationArgumentsDto) {
            $(obj).bind("fetchComplete", Events.ReferenceInformationArgumentsDto.fetchComplete);
        }
        if (obj instanceof FetchContactArgumentsDto) {
            $(obj).bind("fetchComplete", Events.FetchContactArgumentsDto.fetchComplete);
        }
        if (obj instanceof SaveContactArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SaveContactArgumentsDto.fetchComplete);
        }
        if (obj instanceof DeleteContactArgumentsDto) {
            $(obj).bind("fetchComplete", Events.DeleteContactArgumentsDto.fetchComplete);
        }
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }
        
        BindDataMapper(ReferenceInformationArgumentsDto, DataMapper.ReferenceInformationArgumentsDto);
        BindDataMapper(ReferenceInformationResultsDto, DataMapper.ReferenceInformationResultsDto);

        BindDataMapper(FetchContactArgumentsDto, DataMapper.FetchContactArgumentsDto);
        BindDataMapper(FetchContactResultsDto, DataMapper.FetchContactResultsDto);

        BindDataMapper(SaveContactArgumentsDto, DataMapper.SaveContactArgumentsDto);
        BindDataMapper(SaveContactResultsDto, DataMapper.SaveContactResultsDto);

        BindDataMapper(DeleteContactArgumentsDto, DataMapper.DeleteContactArgumentsDto);
        BindDataMapper(DeleteContactResultsDto, DataMapper.DeleteContactResultsDto);

        // naughty!

        BindDataMapper(ContactDto, DataMapper.ContactDto);
        BindDataMapper(EmailDto, DataMapper.EmailDto);
        BindDataMapper(PhoneDto, DataMapper.PhoneDto);
        BindDataMapper(PostalAddressDto, DataMapper.PostalAddressDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(ReferenceInformationArgumentsDto, Services.ReferenceInformation);

        BindFetch(FetchContactArgumentsDto, Services.Fetch);

        BindFetch(SaveContactArgumentsDto, Services.Save);

        BindFetch(DeleteContactArgumentsDto, Services.Delete);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }

    this.prototype.main = function(selected) {
        // coerce arguments
        if (typeof selected === "undefined") {
            selected = {};
        }
        // initialize the page
        $(Contact).trigger("Contact.Initialize");
        // pick the broker
        if (selected.buid) {
            $(Contact).trigger("Contact.BrokerChange", selected.buid);
        }
    };


    this.raises = this.raises || [];
    this.raises.push("Contact.Initialize");
    this.raises.push("Contact.Loaded");
    this.raises.push("Contact.Saved");
    this.raises.push("Contact.Deleted");
    this.raises.push("Contact.ReferenceInformationLoaded");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Contact.BrokerChange");
    this.listensTo.push("Contact.Reset");
    this.listensTo.push("Contact.Load");
    this.listensTo.push("Contact.Save");
    this.listensTo.push("Contact.Delete");

    // in-memory state change notification

    this.listensTo.push("Contact.Data_Email_Insert");
    this.listensTo.push("Contact.Data_Email_Delete");
    this.listensTo.push("Contact.Data_Email_Update");
    
    this.listensTo.push("Contact.Data_Phone_Insert");
    this.listensTo.push("Contact.Data_Phone_Delete");
    this.listensTo.push("Contact.Data_Phone_Update");

    this.listensTo.push("Contact.Data_PostalAddress_Insert");
    this.listensTo.push("Contact.Data_PostalAddress_Delete");
    this.listensTo.push("Contact.Data_PostalAddress_Update");

    var PublicEvents = {
        "Contact.BrokerChange": function(evt, newBroker) {
            if (State.Broker !== newBroker) {
                // keep the information
                State.Broker = newBroker;
                // request reference information
                var a = new ReferenceInformationArgumentsDto();
                a.Broker = State.Broker;
                $(a).trigger("fetch");
            }
        },
        "Contact.Reset": function(evt, args) {
            State.Contact = new ContactDto();
        },
        "Contact.Load": function(evt, args) {
            if (State.Broker !== EmptyGuid) {
                if (args.Id != 0) {
                    var a = new FetchContactArgumentsDto();
                    a.Authorization.Token = State.Token;
                    a.Broker = State.Broker
                    a.Id = args.Id;
                    $(a).trigger("fetch");
                }
            }
        },
        "Contact.Save": function(evt, args) {
            if (State.Broker !== EmptyGuid) {
                var a = new SaveContactArgumentsDto();
                a.Authorization.Token = State.Token;
                a.Broker = State.Broker
                a.Contact = State.Contact;
                $(a).trigger("fetch");
            }
        },
        "Contact.Delete": function(evt, args) {
            if (State.Broker !== EmptyGuid) {
                if (State.Contact.Id != 0) {
                    var a = new DeleteContactArgumentsDto();
                    a.Authorization.Token = State.Token;
                    a.Broker = State.Broker;
                    a.Id = State.Contact.Id;
                    a.RevisionNumber = State.Contact.RevisionNumber;
                    $(a).trigger("fetch");
                }
            }
        },
        // in memory state change notifications
        "Contact.Data_Email_Insert": function (evt, args) {
            // create a new email object
            var dto = EmailDto.fromJSON(args), contact = State.Contact;
            dto.Id = --State.Sequence;
            // add it to the list
            contact.Emails.push(dto);
            // reload the page (ugh?)
            $(Contact).trigger("Contact.Refresh", [{Contact:contact}]);
        },
        "Contact.Data_Email_Delete": function (evt, args) {
            // delete
            var contact = State.Contact,
                list = contact.Emails,
                id = args;
            for (var i = 0, l = list.length; i < l; i++) {
                var item = list[i];
                if (item.Id == id) {
                    Array.remove(list, i);
                    break;
                }
            }
            // reload the page
            $(Contact).trigger("Contact.Refresh", [{Contact:contact}]);
        },
        "Contact.Data_Email_Update": function (evt, args) {
            // update
            var contact = State.Contact,
                list = contact.Emails,
                id = args.Id;
            for (var i = 0, l = list.length; i < l; i++) {
                var item = list[i];
                if (item.Id == id) {
                    for (var attr in args) {
                        if (item.hasOwnProperty(attr)) {
                            item[attr] = args[attr];
                        }
                    }
                    break;
                }
            }
        },
        "Contact.Data_Phone_Insert": function (evt, args) {
            // create a new email object
            var dto = PhoneDto.fromJSON(args), contact = State.Contact;
            dto.Id = --State.Sequence;
            // add it to the list
            contact.Phones.push(dto);
            // reload the page
            $(Contact).trigger("Contact.Refresh", [{Contact:contact}]);
        },
        "Contact.Data_Phone_Delete": function (evt, args) {
            // delete
            var contact = State.Contact,
                list = contact.Phones,
                id = args;
            for (var i = 0, l = list.length; i < l; i++) {
                var item = list[i];
                if (item.Id == id) {
                    Array.remove(list, i);
                    break;
                }
            }
            // reload the page
            $(Contact).trigger("Contact.Refresh", [{Contact:contact}]);
        },
        "Contact.Data_Phone_Update": function (evt, args) {
            // update
            var contact = State.Contact,
                list = contact.Phones,
                id = args.Id;
            for (var i = 0, l = list.length; i < l; i++) {
                var item = list[i];
                if (item.Id == id) {
                    for (var attr in args) {
                        if (item.hasOwnProperty(attr)) {
                            item[attr] = args[attr];
                        }
                    }
                    break;
                }
            }
        },
        "Contact.Data_PostalAddress_Insert": function (evt, args) {
            // create a new email object
            var dto = PostalAddressDto.fromJSON(args), contact = State.Contact;
            dto.Id = --State.Sequence;
            // add it to the list
            contact.PostalAddresses.push(dto);
            // reload the page
            $(Contact).trigger("Contact.Refresh", [{Contact:contact}]);
        },
        "Contact.Data_PostalAddress_Delete": function (evt, args) {
            // delete
            var contact = State.Contact,
                list = contact.PostalAddresses,
                id = args;
            for (var i = 0, l = list.length; i < l; i++) {
                var item = list[i];
                if (item.Id == args) {
                    Array.remove(list, i);
                    break;
                }
            }
            // reload the page
            $(Contact).trigger("Contact.Refresh", [{Contact:contact}]);
        },
        "Contact.Data_PostalAddress_Update": function (evt, args) {
            // update
            var contact = State.Contact,
                list = contact.PostalAddresses,
                id = args.Id;
            for (var i = 0, l = list.length; i < l; i++) {
                var item = list[i];
                if (item.Id == id) {
                    for (var attr in args) {
                        if (item.hasOwnProperty(attr)) {
                            item[attr] = args[attr];
                        }
                    }
                    break;
                }
            }
        }
    };

    $(Contact).bind(PublicEvents);

    // Array Remove - By John Resig (MIT Licensed)
    Array.remove = function(array, from, to) {
      var rest = array.slice((to || from) + 1 || array.length);
      array.length = from < 0 ? array.length + from : from;
      return array.push.apply(array, rest);
    };

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    this.ContactDto = ContactDto;

}).apply(Contact);

