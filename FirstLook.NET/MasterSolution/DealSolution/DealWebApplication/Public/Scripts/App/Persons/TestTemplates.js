if (typeof Person === "undefined") {
    var Person = {};
}
(function() {
    this.raises = this.raises || [];
    this.raises.push("Person.BrokerChange");
    this.raises.push("Person.PersonLoad");
    this.raises.push("Person.Save");
    this.raises.push("Person.Delete");
    this.raises.push("Person.GetReferenceInformation");


    this.listensTo = this.listensTo || [];
    this.listensTo.push("Person.Layout");
    this.listensTo.push("Person.Naming");
    this.listensTo.push("Person.Initialize");
    this.listensTo.push("Person.PersonLoaded");
    this.listensTo.push("Person.ReferenceInformationLoaded");
    this.listensTo.push("Person.Saved");
    this.listensTo.push("Person.Deleted");

    
    var State = {
//      Naming: function (type_name, property_name, id) { var n = type_name + "[" + property_name + "]"; if (!!id) { n += "[" + id + "]"  } return n; },
        Naming: function (type_name, property_name, id) { var n = type_name + property_name; if (!!id) { n += id  } return n; },
        Name: '#name',
        Demographics: '#demographics'
    };

    function isFunction (obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    };

    var PublicEvents = {
        "Person.Layout": function(evt, data) {
            if (!!data) {
                if (!!data.Name) {
                    State.Name = data.Name;
                }
                if (!!data.Demographics) {
                    State.Demographics = data.Demographics;
                }
            }            
        },
        "Person.Naming": function(evt, data) {
            if (isFunction(data)) {
                State.Naming = data;
            }
        },
        "Person.Initialize": function(evt, data) {
            var v = {
                BrokerId: document.getElementById('broker').value
            };
            $(Person).trigger("Person.GetReferenceInformation", v);

            var e = new NameTemplateBuilder();
            e.clean();
            e.build();
            var p = new DemographicsTemplateBuilder();
            p.clean();
            p.build();
        },
        "Person.PersonLoaded": function(evt, data) {
            var e = new NameTemplateBuilder(data.Person);
            e.clean();
            e.build();
            var p = new DemographicsTemplateBuilder(data.Person);
            p.clean();
            p.build();
        },
        "Person.ReferenceInformationLoaded": function(evt, data) {
            var e = new ListTemplateBuilder(data);
            e.build();
        },
        "Person.Saved": function(evt, data) {
            var e = new NameTemplateBuilder(data.Person);
            e.clean();
            e.build();
            var p = new DemographicsTemplateBuilder(data.Person);
            p.clean();
            p.build();
        },
         "Person.Deleted": function(evt, data) {
            if(data.Success == true){
                alert("Person deleted!");
            }
            var e = new NameTemplateBuilder();
            e.clean();
            e.build();
            var p = new DemographicsTemplateBuilder();
            p.clean();
            p.build();
        }
    };
    $(Person).bind(PublicEvents);

    var ListTemplateBuilder = function(data){
        this.$el = $(State.Name);
        this.$dom = {
            Form: $('#name')
        };
        this.data = data;
    }

    ListTemplateBuilder.prototype = {
        build: function(){
        
            if(!!this.data.Titles){
                var i = 0;
                for(i = 0; i < this.data.Titles.length; i++){
                    $("#PersonTitle").append($("<option></option>").attr("value",this.data.Titles[i].Id).text(this.data.Titles[i].Name));    
                }
              
            }
            if(!!this.data.Genders){
                var i = 0;
                for(i = 0; i < this.data.Genders.length; i++){
                    $("#PersonGender").append($("<option></option>").attr("value",this.data.Genders[i].Id).text(this.data.Genders[i].Name));    
                }
              
            }        
            if(!!this.data.MaritalStatuses){
                var i = 0;
                for(i = 0; i < this.data.MaritalStatuses.length; i++){
                    $("#PersonMaritalStatus").append($("<option></option>").attr("value",this.data.MaritalStatuses[i].Id).text(this.data.MaritalStatuses[i].Name));    
                }
              
            }
        }
    };

    var NameTemplateBuilder = function(data) {
        this.$el = $(State.Name);
        this.$dom = {
           Form: $('#controls') 
        };
        this.data = data;
        this.title = null;
    };

    NameTemplateBuilder.prototype = {
        build: function() {
            var id;
            if (!!this.data) {
                id = this.data.Id;            
            }
            var _n = State.Naming
                _a = _n('Person', 'Title'),                              
                _b = _n('Person', 'GivenName'),                          
                _c = _n('Person', 'MiddleName'),                         
                _d = _n('Person', 'FamilyName');                        
            var a = this.title || $('<select />').attr({id: _a, name: _a}),
                b = $('<input type="text" />').attr({id: _b, name: _b}),
                c = $('<input type="text" />').attr({id: _c, name: _c}),
                d = $('<input type="text" />').attr({id: _d, name: _d});           

            $('<table class="name" />').append(
                $('<caption />').text('Name'),
                $('<tbody />').append(
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _a}).text('Title')
                        ),
                        $('<td />').append(a)
                    ),
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _b}).text('Given')
                        ),
                        $('<td />').append(b)
                    ),
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _c}).text('Middle')
                        ),
                        $('<td />').append(c)
                    ),
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _d}).text('Family')
                        ),
                        $('<td />').append(d)
                    )
                )
            ).appendTo(this.$el);

            if(!!this.data){
                if(this.data.Id > 0){
                    $("#PersonGivenName").val(this.data.GivenName);
                    $("#PersonMiddleName").val(this.data.MiddleName);
                    $("#PersonFamilyName").val(this.data.FamilyName);
                    $("#Rev1").val(this.data.RevisionNumber);
                    $("#PersonTitle").val(this.data.Title.Id);   
                    $("#person_id").val(this.data.Id);      
                }
            }


            this.$dom.Form.bind('click', $.proxy(this.changePerson, this));
        },
        clean: function() {
            this.title = function() {
                            if($("#PersonTitle").length > 0){
                                return $("#PersonTitle")
                            }else{
                                return null;
                            }
                         }();
            this.$el.html('');
            $("#Rev1").val('');
            this.$dom.Form.unbind('click');
        },
        
        changePerson: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                c = null,
                e = null,
                v = null;
                if (n == '#load') {
                    v = {
                        BrokerId: document.getElementById('broker').value,
                        PersonId: document.getElementById('person_id').value
                    };

                    $(Person).trigger("Person.PersonLoad", v);
                }
                if (n == '#save') {                    
                    v = {
                            Given: document.getElementById('PersonGivenName').value,
                            Family: document.getElementById('PersonFamilyName').value,
                            Middle: document.getElementById('PersonMiddleName').value,
                            BirthDate: new Date(document.getElementById('PersonBirthDate').value),
                            Occupation: document.getElementById('PersonOccupation').value,
                            Id: document.getElementById('person_id').value,
                            Title: $("#PersonTitle").val(),
                            Gender: $("#PersonGender").val(),
                            MaritalStatus: $("#PersonMaritalStatus").val(),
                            RevisionNumber: $("#Rev1").val(),
                            BrokerId: document.getElementById('broker').value
                        };
                    $(Person).trigger("Person.PersonSave", v);                    
                }
                if (n == '#new') {                    
                    v = {
                            Given: document.getElementById('PersonGivenName').value,
                            Family: document.getElementById('PersonFamilyName').value,
                            Middle: document.getElementById('PersonMiddleName').value,
                            BirthDate: new Date(),
                            Occupation: document.getElementById('PersonOccupation').value,
                            Id: 0,
                            Title: $("#PersonTitle").val(),
                            Gender: $("#PersonGender").val(),
                            MaritalStatus: $("#PersonMaritalStatus").val(),
                            RevisionNumber: 0,
                            BrokerId: document.getElementById('broker').value
                        };
                    $(Person).trigger("Person.PersonSave", v);                    
                }
                if (n == '#delete') {                    
                    e = document.getElementById('person_id');
                    v = {
                        Id: (e == null ? null : e.value),
                        RevisionNumber: $("#Rev1").val(),
                        BrokerId: document.getElementById('broker').value
                    };
                    $(Person).trigger("Person.PersonDelete", v);   
                }
                else{                  
                }
            }

            
        }
    };

    var DemographicsTemplateBuilder = function(data) {
        this.$el = $(State.Demographics);
        this.data = data;
        this.gender = null;
        this.maritalStatus = null;
    };

    DemographicsTemplateBuilder.prototype = {
        build: function() {
            var _n = State.Naming
                _a = _n('Person', 'Gender'),
                _b = _n('Person', 'BirthDate'),
                _c = _n('Person', 'MaritalStatus'),
                _d = _n('Person', 'Occupation');
            var a = this.gender || $('<select />').attr({id: _a, name: _a}),                
                b = $('<input type="text" />').attr({id: _b, name: _b}),
                c = this.maritalStatus || $('<select />').attr({id: _c, name: _c}),         
                d = $('<input type="text" />').attr({id: _d, name: _d});
         
            $('<table class="name" />').append(
                $('<caption />').text('Demographics'),
                $('<tbody />').append(
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _a}).text('Gender')
                        ),
                        $('<td />').append(a)
                    ),
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _b}).text('BirthDate')
                        ),
                        $('<td />').append(b)
                    ),
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _c}).text('Marital Status')
                        ),
                        $('<td />').append(c)
                    ),
                    $('<tr />').append(
                        $('<th />').append(
                            $('<label />').attr({'for': _d}).text('Occupation')
                        ),
                        $('<td />').append(d)
                    )
                )
            ).appendTo(this.$el);

            if(!!this.data){
                if(this.data.Id == 0){
                    alert("Load failed!");
                }
                $("#PersonGender").val(this.data.Gender.Id);    
                $("#PersonMaritalStatus").val(this.data.MaritalStatus.Id);       
                var milli = this.data.BirthDate.replace(/\/Date\((-?\d+)\)\//, '$1');
                $("#PersonBirthDate").val(new Date(parseInt(milli)));
                $("#PersonOccupation").val(this.data.Occupation);    
            }
        },
        clean: function() {
            this.gender = function() {
                            if($("#PersonGender").length > 0){
                                return $("#PersonGender")
                            }else{
                                return null;
                            }
                         }();

            this.maritalStatus = function() {
                            if($("#PersonMaritalStatus").length > 0){
                                return $("#PersonMaritalStatus")
                            }else{
                                return null;
                            }
                         }();
            this.$el.html('');
        }
    };

}).apply(Person);
