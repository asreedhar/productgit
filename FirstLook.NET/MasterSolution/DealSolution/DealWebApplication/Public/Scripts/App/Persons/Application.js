if (typeof Person !== "function") {
    var Person = function() {};
}

(function() {
    var State = {
        Broker: ''
    };

    /* =================== */
    /* == START: Classes = */
    /* =================== */
        
    /* == DTO == */

    function PersonDto(){
        this.Id = 0;
        this.RevisionNumber = 0;
        this.GivenName = '';
        this.MiddleName = '';
        this.FamilyName = '';
        this.Title = null;
        this.Gender = null;
        this.MaritalStatus = null;
        this.BirthDate = null;
        this.Occupation = '';
        this.Contact = null;
    }

    function TitleDto (){
        this.Id = 0;
        this.Name = '';
    }

    function GenderDto(){
        this.Id = 0;
        this.Name = '';
    }

    function MaritalStatusDto () {
        this.Id = 0;
        this.Name = '';
    }

    function AuthorizationDto () {
        this.Token = '00000000-0000-0000-0000-000000000000';
    }    

    function TokenStatusDto () {
        this.status;
    }


    /* == Envelopes == */

    function FetchPersonArgumentsDto ()  {
        this.Id = 0;
        this.Authorization = null;
        this.Broker = '00000000-0000-0000-0000-000000000000';
        EventBinder(this);
    }

    function FetchPersonResultsDto ()  {
        this.Arguments = null;
        this.Person = null;    
        this.AuthorizationStatus = null;
    }

    function ReferenceInformationArgumentsDto () {
        this.Authorization = null;
        this.Broker = '00000000-0000-0000-0000-000000000000';
        EventBinder(this);
    }

    function ReferenceInformationResultsDto () {
        this.Arguments = null;
        this.Titles = [];
        this.Genders = [];
        this.MaritalStatuses = [];
        this.AuthorizationStatus = null;
    }

    function SavePersonArgumentsDto () {
        this.Person = null;
        this.Authorization = null;
        this.Broker = '00000000-0000-0000-0000-000000000000';
        EventBinder(this);
    }

    function SavePersonResultsDto () {
        this.Argumetns = null;
        this.Person = null;
        this.AuthorizationStatus = null;
    }

    function DeletePersonArgumentsDto () {
        this.Id = 0;
        this.RevisionNumber = 0;
        this.Authorization = null;
        this.Broker = '00000000-0000-0000-0000-000000000000';
        EventBinder(this);
    }

    function DeletePersonResultsDto () {
        this.Arguments = null;
        this.Success = false;    
        this.AuthorizationStatus = null;
    }

    /* =================== */
    /* == END: Classes == */
    /* =================== */

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Person).trigger("Person.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Fetch": genericService("/Deal/Services/Persons.asmx/Fetch", FetchPersonResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes.FetchPersonArgumentsDto",
                        "Id": this.Id,
             "Authorization": this.Authorization,
                    "Broker": this.Broker
                }
            });
        }),
        "ReferenceInformation" : genericService("/Deal/Services/Persons.asmx/ReferenceInformation", ReferenceInformationResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes.ReferenceInformationArgumentsDto",
             "Authorization": this.Authorization,
                    "Broker": this.Broker
                }                
                
            });
         }),
         "Save": genericService("/Deal/Services/Persons.asmx/Save", SavePersonResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes.SavePersonArgumentsDto",
                    "Person": this.Person,
             "Authorization": this.Authorization,
                    "Broker": this.Broker
                }
            });
        }),
          "Delete": genericService("/Deal/Services/Persons.asmx/Delete", DeletePersonResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes.DeletePersonArgumentsDto",
                        "Id": this.Id,
            "RevisionNumber": this.RevisionNumber,
             "Authorization": this.Authorization,
                    "Broker": this.Broker
                }
            });
        })
    };

    var Events = {
        "FetchPersonArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Person).trigger("Person.PersonLoaded", [data]);
            }
        },

         "ReferenceInformationArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Person).trigger("Person.ReferenceInformationLoaded", [data]);
            }
        },

        "SavePersonArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Person).trigger("Person.Saved", [data]);
            }
        },

         "DeletePersonArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Person).trigger("Person.Deleted", [data]);
            }
        }


    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "FetchPersonArgumentsDto": genericMapper(FetchPersonArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "FetchPersonResultsDto": genericMapper(FetchPersonResultsDto, {
            "AuthorizationStatus": "TokenStatusDto",   
                      "Arguments": "FetchPersonArgumentsDto",          
                         "Person": "PersonDto"
        }),
        "PersonDto": genericMapper(PersonDto, {
             "Title": "TitleDto",
             "Gender": "GenderDto",
             "MaritalStatus": "MaritalStatusDto"
        }),
        "ReferenceInformationArgumentsDto": genericMapper(ReferenceInformationArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "ReferenceInformationResultsDto": genericMapper(ReferenceInformationResultsDto, {
            "AuthorizationStatus": "TokenStatusDto",
                      "Arguments": "ReferenceInformationArgumentsDto",
                         "Titles": "TitleDto",
                        "Genders": "GenderDto",
                "MaritalStatuses": "MaritalStatusDto"       
        }),   
        "TitleDto": genericMapper(TitleDto),
        "GenderDto" : genericMapper(GenderDto),
        "MaritalStatusDto": genericMapper(MaritalStatusDto),
        "AuthorizationDto": genericMapper(AuthorizationDto),
        "TokenStatusDto": genericMapper(TokenStatusDto),
        "SavePersonArgumentsDto": genericMapper(SavePersonArgumentsDto, {
                 "Authorization": "AuthorizationDto",
                        "Person": "PersonDto"             
        }),
        "SavePersonResultsDto" : genericMapper(SavePersonResultsDto, {
          "AuthorizationStatus": "TokenStatusDto",
                    "Agruments": "SavePersonAgrumentsDto",
                       "Person": "PersonDto"
        }),
        "DeletePersonArgumentsDto": genericMapper(DeletePersonArgumentsDto, {
            "Authorization": "AuthorizationDto"
        }),
        "DeletePersonResultsDto" : genericMapper(DeletePersonResultsDto, {
            "AuthorizationStatus": "TokenStatusDto",
                      "Agruments": "DeletePersonAgrumentsDto"
        })
    };

    var EventBinder = function(obj) {
        if (obj instanceof FetchPersonArgumentsDto) {
            $(obj).bind("fetchComplete", Events.FetchPersonArgumentsDto.fetchComplete);
        }
        if (obj instanceof ReferenceInformationArgumentsDto){
            $(obj).bind("fetchComplete", Events.ReferenceInformationArgumentsDto.fetchComplete);
        }
        if (obj instanceof SavePersonArgumentsDto){
            $(obj).bind("fetchComplete", Events.SavePersonArgumentsDto.fetchComplete);
        }
        if (obj instanceof DeletePersonArgumentsDto){
            $(obj).bind("fetchComplete", Events.DeletePersonArgumentsDto.fetchComplete);
        }

    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }
        
        BindDataMapper(FetchPersonArgumentsDto, DataMapper.FetchPersonArgumentsDto);
        BindDataMapper(FetchPersonResultsDto, DataMapper.FetchPersonResultsDto);
        BindDataMapper(PersonDto, DataMapper.PersonDto);
        
        BindDataMapper(ReferenceInformationArgumentsDto, DataMapper.ReferenceInformationArgumentsDto);
        BindDataMapper(ReferenceInformationResultsDto, DataMapper.ReferenceInformationResultsDto);
        BindDataMapper(TitleDto, DataMapper.TitleDto);
        BindDataMapper(GenderDto, DataMapper.GenderDto);
        BindDataMapper(MaritalStatusDto, DataMapper.MaritalStatusDto);

        BindDataMapper(SavePersonArgumentsDto, DataMapper.SavePersonArgumentsDto);
        BindDataMapper(SavePersonResultsDto, DataMapper.SavePersonResultsDto);

        BindDataMapper(DeletePersonArgumentsDto, DataMapper.DeletePersonArgumentsDto);
        BindDataMapper(DeletePersonResultsDto, DataMapper.DeletePersonResultsDto);

        BindDataMapper(AuthorizationDto, DataMapper.AuthorizationDto);
        BindDataMapper(TokenStatusDto, DataMapper.TokenStatusDto);

    })();

    // =======================
    // = Bind Service Fetches =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(FetchPersonArgumentsDto, Services.Fetch);
        BindFetch(ReferenceInformationArgumentsDto, Services.ReferenceInformation);
        BindFetch(SavePersonArgumentsDto, Services.Save);
        BindFetch(DeletePersonArgumentsDto, Services.Delete);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }

    this.prototype.main = function(selected) {
        if (typeof selected === "undefined") {
            selected = {};
        }
        // initialize the page
        $(Person).trigger("Person.Initialize");

        // load the reference information
        // var refInfo = new ReferenceInformationArgumentsDto();
        // $(refInfo).trigger("fetch");

        // pick the broker
        if (selected.buid) {
            $(Person).trigger("Person.BrokerChange", selected.buid);
        }
    };

    this.raises = this.raises || [];
    this.raises.push("Person.Initialize");
    this.raises.push("Person.PersonLoaded");
    this.raises.push("Person.ReferenceInformationLoaded");
    this.raises.push("Person.Saved");
    this.raises.push("Person.Deleted");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Person.PersonLoad");
    this.listensTo.push("Person.PersonSave");
    this.listensTo.push("Person.PersonDelete");
    this.listensTo.push("Person.GetReferenceInformation");


    var PublicEvents = {
        "Person.BrokerChange": function(evt, newBroker) {
            if (State.Broker !== newBroker) {
                State.Broker = newBroker;
                $(Person).trigger("Person.Initialize");
            }
        },
        "Person.PersonLoad": function(evt, value) {
            var fetchPerson = new FetchPersonArgumentsDto();
            var authDto = new AuthorizationDto();
            fetchPerson.Id = value.PersonId;
            fetchPerson.Authorization = authDto;
            fetchPerson.Broker = value.BrokerId || "00000000-0000-0000-0000-000000000000";
            $(fetchPerson).trigger("fetch");
        },
        "Person.PersonSave": function(evt, value){
            var savePerson = new SavePersonArgumentsDto();
            var authDto = new AuthorizationDto();
            var personToSave = new PersonDto();
            var title = new TitleDto();
            var gender = new GenderDto();
            var maritalStatus = new MaritalStatusDto();
            
            title.Id = value.Title;
            gender.Id = value.Gender;
            maritalStatus.Id = value.MaritalStatus;

            personToSave.Authorization = authDto;
            personToSave.Broker = value.BrokerId || "00000000-0000-0000-0000-000000000000";
            personToSave.Id = value.Id || 0;
            personToSave.RevisionNumber = value.RevisionNumber;
            personToSave.GivenName = value.Given;
            personToSave.MiddleName = value.Middle;
            personToSave.FamilyName = value.Family;
            personToSave.Title = title;
            personToSave.Gender = gender;
            personToSave.MaritalStatus = maritalStatus;
            personToSave.BirthDate = value.BirthDate;
            personToSave.Occupation = value.Occupation;

            savePerson.Person = personToSave;

            $(savePerson).trigger("fetch");

        },
         "Person.PersonDelete": function(evt, value) {
            var deletePerson = new DeletePersonArgumentsDto();
            var authDto = new AuthorizationDto();

            deletePerson.Authorization = authDto;
            deletePerson.Broker = value.BrokerId || "00000000-0000-0000-0000-000000000000";
            deletePerson.Id = value.Id;
            deletePerson.RevisionNumber = value.RevisionNumber;
            $(deletePerson).trigger("fetch");
        },

        "Person.GetReferenceInformation": function(evt, value) {
            var refInfo = new ReferenceInformationArgumentsDto();
            var authDto = new AuthorizationDto();

            refInfo.Authorization = authDto;
            refInfo.Broker = value.BrokerId || "00000000-0000-0000-0000-000000000000";

            $(refInfo).trigger("fetch");
        }
    };
    $(Person).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */

}).apply(Person);

