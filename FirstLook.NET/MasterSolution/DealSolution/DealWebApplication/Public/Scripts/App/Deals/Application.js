if (typeof Deal !== "function") {
    var Deal = function() {};
}

(function() {
    var State = {
        Broker: ''
    };

    /* =================== */
    /* == START: Classes = */
    /* =================== */

    function SampleArgumentsDto() {}

    function SampleResultsDto() {}

    /* =================== */
    /* == END: Classes == */
    /* =================== */

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {
                        $(Deal).trigger("Deal.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Sample": genericService("/Deal/Services/Partition.asmx/Method", SampleResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Deal.DomainModel.Partition.Commands.TransferObjects.Envelopes.SampleArgumentsDto",
                    "Broker": this.Broker
                }
            });
        })
    };

    var Events = {
        "SampleArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                // render the data
                $(Deal).trigger("Deal.SampleLoaded", [data]);
            }
        }
    };

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "SampleArgumentsDto": genericMapper(SampleArgumentsDto),
        "SampleResultsDto": genericMapper(SampleResultsDto, {
            "Arguments": "SampleArgumentsDto",
            "Xyz": "XyzDto"
        })
    };

    var EventBinder = function(obj) {
        if (obj instanceof SampleArgumentsDto) {
            $(obj).bind("fetchComplete", Events.SampleArgumentsDto.fetchComplete);
        }
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(SampleArgumentsDto, DataMapper.SampleArgumentsDto);
        BindDataMapper(SampleResultsDto, DataMapper.SampleResultsDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(SampleArgumentsDto, Services.Sample);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }

    this.prototype.main = function(selected) {
        if (typeof selected === "undefined") {
            selected = {};
        }
        if (selected.buid) {
            $(Deal).trigger("Deal.BrokerChange", selected.buid);
        }
        else {
            $(Deal).trigger("Deal.Main");
        }
    };

    this.raises = this.raises || [];
    this.raises.push("Deal.Main");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Deal.SampleChange");

    var PublicEvents = {
        "Deal.SampleChange": function(evt, newXyz) {
            if (State.Xyz !== newXyz) {
                State.Xyz = newXyz;
                $(new SampleArgumentsDto()).trigger("stateChange");
            }
        },
        "Deal.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
        }
    };
    $(Deal).bind(PublicEvents);

    /* ========================== */
    /* = PUBLIC STATIC METHODS = */
    /* ========================== */
    // this.FormatPriceTableTypeRow = PriceTableRowTypeDto.toString;

}).apply(Deal);

