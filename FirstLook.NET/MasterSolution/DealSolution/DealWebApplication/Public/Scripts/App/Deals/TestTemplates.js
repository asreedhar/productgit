if (typeof Deal === "undefined") {
    var Deal = {};
}
(function() {
    this.raises = this.raises || [];
    this.raises.push("Deal.BrokerChange");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Deal.Main");

    var PublicEvents = {
        "Deal.Main": function(evt) {
            var layout = new LayoutTemplateBuilder();
            layout.clean();
            layout.init();
        }
    };
    $(Deal).bind(PublicEvents);

    var LayoutTemplateBuilder = function() {
        this.$dom = {
            Form: $('#subject'),
            Broker: $('#broker'),
            Menu: $('.menu'),
            Menu$Item: $('.menu a')
        };
    };

    LayoutTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.Form.bind('click', $.proxy(this.changeBroker, this));
            this.$dom.Menu.bind('click', $.proxy(this.changeMenuItem, this));
        },
        clean: function() {
            this.$dom.Menu.unbind('change');
        },
        changeMenuItem: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "LI") {
                tgt = tgt.getElementsByTagName("A")[0];
            }
            var menu = $(tgt).closest('div.menu');
            this.$dom.Menu$Item.each(function(index) {
                var parent = $(this).parent(),
                    m = $(this).closest('div.menu');
                if (m[0] && menu[0] && m[0].id == menu[0].id) {
                    var i = this.href.lastIndexOf('#'),
                    e = this.href.substring(i);
                    if (this.href == tgt.href) {
                        parent.addClass('selected');
                        $(e).removeClass('hidden');
                    }
                    else {
                        parent.removeClass('selected');
                        $(e).addClass('hidden');
                    }
                }
            });
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        changeBroker: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                e = tgt.href.substring(i),
                v = document.getElementById(e.substring(1)).value,
                c = null;
                if (e == '#broker') {
                    $(Vehicles).trigger("Deals.BrokerChange", v);
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var XyzTemplateBuilder = function(d) {
        this.d = d;
        this.$el = $("#xyz");
        this.$dom = {
            Broker: $('#broker')
        };
    };

    XyzTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            // this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVehicleOrDate, this));
        },
        clean: function() {
            // this.$dom.Form.unbind('click');
        }
    };

}).apply(Deal);
