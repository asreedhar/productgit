﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Persons.Commands.TransferObjects.Envelopes;
using ICommandFactory = FirstLook.Deal.DomainModel.Persons.Commands.ICommandFactory;

namespace FirstLook.Deal.WebApplication.Services
{
    /// <summary>
    /// Summary description for Persons
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Persons : WebServiceBase
    {
        [WebMethod]
        public FetchPersonResultsDto Fetch(FetchPersonArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchPersonResultsDto, FetchPersonArgumentsDto> command = factory.CreateFetchPersonCommand();
            return ExecuteCommand(command, arguments);
//            return ExecuteCommand(Decorate(command), arguments);          //DR commented out to get passed licensing problem
        }

        [WebMethod]
        public SavePersonResultsDto Save(SavePersonArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SavePersonResultsDto, IIdentityContextDto<SavePersonArgumentsDto>> command = factory.CreateSavePersonCommand();

            return ExecuteCommand(command,Parameterize(arguments));

            //DR commented out to avoid licensing problems
/*            return ExecuteCommand(
                Decorate<SavePersonResultsDto, IIdentityContextDto<SavePersonArgumentsDto>, SavePersonArgumentsDto>(command),
                Parameterize(arguments));*/
        }

        [WebMethod]
        public DeletePersonResultsDto Delete(DeletePersonArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<DeletePersonResultsDto, IIdentityContextDto<DeletePersonArgumentsDto>> command = factory.CreateDeletePersonCommand();

            return ExecuteCommand(command, Parameterize(arguments));
            //DR commented out to avoid licensing problems
/*            return ExecuteCommand(
                Decorate<DeletePersonResultsDto, IIdentityContextDto<DeletePersonArgumentsDto>, DeletePersonArgumentsDto>(command),
                Parameterize(arguments));*/
        }

        [WebMethod]
        public ReferenceInformationResultsDto ReferenceInformation(ReferenceInformationArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> command = factory.CreateReferenceInformationCommand();
            return ExecuteCommand(command, arguments);
            //            return ExecuteCommand(Decorate(command), arguments);          //DR commented out to get passed licensing problem
        }
    }
}
