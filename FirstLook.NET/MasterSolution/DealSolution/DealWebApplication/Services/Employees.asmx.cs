﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Employees.Commands.TransferObjects.Envelopes;
using ICommandFactory = FirstLook.Deal.DomainModel.Employees.Commands.ICommandFactory;

namespace FirstLook.Deal.WebApplication.Services
{
    /// <summary>
    /// Summary description for Employees
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Employees : WebServiceBase
    {
        [WebMethod]
        public ReferenceInformationResultsDto ReferenceInformation(ReferenceInformationArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto> command = factory.CreateReferenceInformationCommand();

            return ExecuteCommand(Decorate(command), arguments);
        }

        [WebMethod]
        public FetchEmployeeResultsDto Fetch(FetchEmployeeArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchEmployeeResultsDto, IIdentityContextDto<FetchEmployeeArgumentsDto>> command = factory.CreateFetchEmployeeCommand();

            return ExecuteCommand(Decorate<FetchEmployeeResultsDto, IIdentityContextDto<FetchEmployeeArgumentsDto>, FetchEmployeeArgumentsDto>(command), Parameterize(arguments));
        }

        [WebMethod]
        public SaveEmployeeResultsDto Save(SaveEmployeeArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveEmployeeResultsDto, IIdentityContextDto<SaveEmployeeArgumentsDto>> command = factory.CreateSaveEmployeeCommand();

            return ExecuteCommand(
                Decorate<SaveEmployeeResultsDto, IIdentityContextDto<SaveEmployeeArgumentsDto>, SaveEmployeeArgumentsDto>(command),
                Parameterize(arguments));
        }

        [WebMethod]
        public DeleteEmployeeResultsDto Delete(DeleteEmployeeArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<DeleteEmployeeResultsDto, IIdentityContextDto<DeleteEmployeeArgumentsDto>> command = factory.CreateDeleteEmployeeCommand();

            return ExecuteCommand(
                Decorate<DeleteEmployeeResultsDto, IIdentityContextDto<DeleteEmployeeArgumentsDto>, DeleteEmployeeArgumentsDto>(command),
                Parameterize(arguments));
        }
    }
}
