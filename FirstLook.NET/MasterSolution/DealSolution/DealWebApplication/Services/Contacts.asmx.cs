﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Contacts.Commands.TransferObjects.Envelopes;
using ICommandFactory = FirstLook.Deal.DomainModel.Contacts.Commands.ICommandFactory;

namespace FirstLook.Deal.WebApplication.Services
{
    /// <summary>
    /// Summary description for Contact
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Contacts : WebServiceBase
    {

        [WebMethod]
        public ReferenceInformationResultsDto ReferenceInformation
            (ReferenceInformationArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();
            ICommand<ReferenceInformationResultsDto, ReferenceInformationArgumentsDto>
                command = factory.CreateReferenceInformationCommand();
            
            return ExecuteCommand(Decorate(command), arguments);
        }

        [WebMethod]
        public FetchContactResultsDto Fetch(FetchContactArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();
            ICommand<FetchContactResultsDto, IIdentityContextDto<FetchContactArgumentsDto>> command = factory.CreateFetchContactCommand();
            return ExecuteCommand(
                Decorate<FetchContactResultsDto, IIdentityContextDto<FetchContactArgumentsDto>, FetchContactArgumentsDto>(command), 
                Parameterize(arguments));
        }

        [WebMethod]
        public SaveContactResultsDto Save(SaveContactArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveContactResultsDto, IIdentityContextDto<SaveContactArgumentsDto>> command = factory.CreateSaveContactCommand();

            return ExecuteCommand(
                Decorate<SaveContactResultsDto, IIdentityContextDto<SaveContactArgumentsDto>, SaveContactArgumentsDto>(command),
                Parameterize(arguments));
        }

        [WebMethod]
        public DeleteContactResultsDto Delete(DeleteContactArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();
            ICommand<DeleteContactResultsDto, IIdentityContextDto<DeleteContactArgumentsDto>> command = factory.CreateDeleteContactCommand();
            return ExecuteCommand(
                Decorate<DeleteContactResultsDto, IIdentityContextDto<DeleteContactArgumentsDto>, DeleteContactArgumentsDto>(command), 
                Parameterize(arguments));
        }
    }
}
