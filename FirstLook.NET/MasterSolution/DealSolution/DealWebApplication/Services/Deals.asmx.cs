﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
//using FirstLook.Deal.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Deals.Commands.TransferObjects.Envelopes;
using ICommandFactory = FirstLook.Deal.DomainModel.Deals.Commands.ICommandFactory;

namespace FirstLook.Deal.WebApplication.Services
{
    /// <summary>
    /// Summary description for Deal
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Deals : WebServiceBase
    {
/*
        [WebMethod]
        public SaveDealResultsDto Save(SaveDealArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveDealResultsDto, IIdentityContextDto<SaveDealArgumentsDto>> command = factory.CreateSaveDealCommand();

            return ExecuteCommand(
                Decorate<SaveDealResultsDto, IIdentityContextDto<SaveDealArgumentsDto>, SaveDealArgumentsDto>(command),
                Parameterize(arguments));
        }
*/
        [WebMethod]
        public FetchVehicleResultsDto FetchVehicle(FetchVehicleArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchVehicleResultsDto, IIdentityContextDto<FetchVehicleArgumentsDto>> command = factory.CreateFetchVehicleCommand();

            return ExecuteCommand(
                Decorate<FetchVehicleResultsDto, IIdentityContextDto<FetchVehicleArgumentsDto>, FetchVehicleArgumentsDto>(command),
                Parameterize(arguments));
        }

        [WebMethod]
        public SaveVehicleResultsDto SaveVehicle(SaveVehicleArgumentsDto arguments)
        {
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveVehicleResultsDto, IIdentityContextDto<SaveVehicleArgumentsDto>> command = factory.CreateSaveVehicleCommand();

            return ExecuteCommand(
                Decorate<SaveVehicleResultsDto, IIdentityContextDto<SaveVehicleArgumentsDto>, SaveVehicleArgumentsDto>(command),
                Parameterize(arguments));
        }

    }
}