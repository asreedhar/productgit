﻿using System.Web;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using ICommandFactory = FirstLook.Client.DomainModel.Licenses.Commands.ICommandFactory;

namespace FirstLook.Deal.WebApplication.Services
{
    public abstract class WebServiceBase : WebService
    {
        protected static IdentityContextDto<T> Parameterize<T>(T arguments)
        {
            return new IdentityContextDto<T>
            {
                Identity = Identity(),
                Arguments = arguments
            };
        }

        protected static IIdentityContextDto IdentityContext()
        {
            return new IdentityContextDto { Identity = Identity() };
        }

        protected static IdentityDto Identity()
        {
            return new IdentityDto
            {
                Name = HttpContext.Current.User.Identity.Name,
                AuthorityName = HttpContext.Current.User.Identity.AuthenticationType
            };
        }

        protected static ICommand<TResult, TParameter> Decorate<TResult, TParameter>(ICommand<TResult, TParameter> command)
            where TParameter : IAuthorizationArgumentsDto
            where TResult : IAuthorizationResultsDto, new()
        {
            ICommandFactory licensing = GetResolver().Resolve<ICommandFactory>();

            return licensing.DecorateCommand(command, IdentityContext());
        }

        protected static ICommand<TResult, TParameter> Decorate<TResult, TParameter, TWrapped>(ICommand<TResult, TParameter> command)
            where TParameter : IIdentityContextDto<TWrapped>
            where TResult : IAuthorizationResultsDto, new()
            where TWrapped : IAuthorizationArgumentsDto
        {
            ICommandFactory licensing = GetResolver().Resolve<ICommandFactory>();

            return licensing.DecorateCommand<TResult, TParameter, TWrapped>(command);
        }

        protected static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = GetResolver().Resolve<ICommandExecutor>();

            ICommandResult<TResult> result = executor.Execute(command, args);

            return result.Result;
        }

        protected static IResolver GetResolver()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver;
        }
    }
}