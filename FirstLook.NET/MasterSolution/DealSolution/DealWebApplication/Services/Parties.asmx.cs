﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects;
using FirstLook.Deal.DomainModel.Parties.Commands.TransferObjects.Envelopes;
using ICommandFactory = FirstLook.Deal.DomainModel.Parties.Commands.ICommandFactory;

namespace FirstLook.Deal.WebApplication.Services
{
    /// <summary>
    /// Summary description for Party
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    [GenerateScriptType(typeof(PersonPartyDto)), GenerateScriptType(typeof(OrganizationPartyDto))]
    public class Parties : WebServiceBase
    {
        [WebMethod]
        public FetchPartyResultsDto Fetch(FetchPartyArgumentsDto arguments)
        {
            var factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchPartyResultsDto, FetchPartyArgumentsDto> command = factory.CreateFetchPartyCommand();

            return ExecuteCommand(Decorate(command), arguments);
        }

        [WebMethod]
        public SavePartyResultsDto Save(SavePartyArgumentsDto arguments)
        {
            var factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SavePartyResultsDto, IIdentityContextDto<SavePartyArgumentsDto>> command = factory.CreateSavePartyCommand();

            return ExecuteCommand(
                Decorate<SavePartyResultsDto, IIdentityContextDto<SavePartyArgumentsDto>, SavePartyArgumentsDto>(command),
                Parameterize(arguments));
        }

        [WebMethod]
        public SearchPartyResultsDto Search(SearchPartyArgumentsDto arguments)
        {
            var factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SearchPartyResultsDto, SearchPartyArgumentsDto> command = factory.CreateSearchPartyCommand();

            return ExecuteCommand(Decorate(command), arguments);
        }
    }
}