﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestDesktop.aspx.cs" Inherits="FirstLook.Deal.WebApplication.Pages.Deals.TestDesktop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Deals</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css" media="screen">
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        fieldset { overflow: auto; }
        #subject { width: 450px; }
        #name, #demographics { width: 439px; }
        table { width: 100%; }
        input { width: 95%; margin-left: 2%; }
        select { margin-left: 2%; }
        .menu ul { padding-left: 1ex; list-style-type: none; list-style-position: inside; }
        .menu li {  float: left; width: 20ex; padding: 1ex; border: solid 1px grey; margin-left: 2px; text-align: center; }
        li.selected { background-color: Lime; }
        tr.selected { background-color: Lavender; }
        .hidden { display: none; }
        fieldset { clear: both; }
        #name, #demographics { float: left; clear:none; }
        td.action { text-align: center; }
    </style>
</head>
<body>
    <form id="DealForm" runat="server">
    <div id="nav_header">
        </div>
        <div id="main_content">
            <div id="header">
                <h1>Deals</h1>
            </div>
            <div id="body">
                <fieldset id="subject">
                    <legend>Broker</legend>
                    <table border="1">
                        <tbody>
                            <tr>
                                <th><label for="broker">Broker</label></th>
                                <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                                <td><a href="#broker">Select</a></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
                <div id="main_menu" class="menu">
                    <ul>
                        <li id="menu_item_search"><a href="#search">Search</a></li>
                        <li id="menu_item_deal" class="selected"><a href="#deal">Deal</a></li>
                    </ul>
                </div>
                <fieldset id="search" class="hidden">
                    <legend>Search</legend>
                    <table border="1">
                        <caption>Criteria</caption>
                        <thead>
                            <tr>
                                <th>Motivation</th>
                                <th>Status</th>
                                <th>Inventory Decision</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><select></select></td>
                                <td><select></select></td>
                                <td><select></select></td>
                                <td class="action"><a href="#search">Search</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="1">
                        <caption>Results</caption>
                        <thead>
                            <tr>
                                <th>Year</th>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Trim</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Salesperson</th>
                                <th>Last Appraisal</th>
                                <th>Motivation</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2006</td>
                                <td>Toyota</td>
                                <td>Camry</td>
                                <td></td>
                                <td>08/01/2011</td>
                                <td>08/07/2011</td>
                                <td>Chris Bennett</td>
                                <td></td>
                                <td>Trade</td>
                                <td>Open</td>
                                <td class="action"><a href="#open">Open</a></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
                <fieldset id="deal">
                    <legend>Deal</legend>
                    <fieldset>
                        <legend>Info</legend>
                        <table border="1">
                            <tbody>
                                <tr>
                                    <th>Deal #</th>
                                    <td>-</td>
                                    <th>User</th>
                                    <td>-</td>
                                    <th><label for="deal_motivation">Motivation</label></th>
                                    <td><select id="deal_motivation" name="deal_motivation"><option>-Select-</option><option>Trade</option><option>Purchase</option><option>Service Lane</option></select></td>
                                    <td rowspan="2" class="action"><a href="#deal_save">Create</a></td>
                                </tr>
                                <tr>
                                    <th>Created</th>
                                    <td>-</td>
                                    <th>Updated</th>
                                    <td>-</td>
                                    <th><label for="deal_status">Status</label></th>
                                    <td><select id="deal_status" name="deal_status" disabled="disabled"><option>Open</option><optgroup label="Closed"><option>Won</option><option>Lost</option><option>Not Interested</option></optgroup></select></td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                    <div id="deal_menu" class="menu">
                        <ul>
                            <li id="menu_item_vehicles" class="selected"><a href="#vehicles">Vehicles</a></li>
                            <li id="menu_item_parties"><a href="#parties">Parties</a></li>
                        </ul>
                    </div>
                    <fieldset id="vehicles">
                        <legend>Vehicles</legend>
                        <div>
                            <table border="1" style="width: 60%; float: left; margin: 3px;">
                                <caption>Vehicle List</caption>
                                <thead>
                                    <tr>
                                        <th>VIN</th>
                                        <th>Transaction Type</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td><input type="text" id="vin" name="vin" maxlength="17" /></td>
                                        <td><select id="transaction_type" name="transaction_type"><option>-Select-</option><option>Buy</option><option>Sell</option></select></td>
                                        <td class="action"><a href="#deal_vehicle_add">Add</a></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr class="selected">
                                        <td>12345678901234567</td>
                                        <td>Purchase</td>
                                        <td class="action"><a href="#12345678901234567">Select</a></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="1" style="width: 38%; float: left; margin: 3px;">
                                <caption>Vehicle</caption>
                                <tbody>
                                    <tr>
                                        <th>Transaction Type</th>
                                        <td><select id="vehicle_transaction_type" name="vehicle_transaction_type"><option>-Select-</option><option>Buy</option><option>Sell</option></select></td>
                                    </tr>
                                    <tr>
                                        <th>Inventory Decision</th>
                                        <td><select id="vehicle_inventory_decision" name="vehicle_inventory_decision"><option>-Select-</option><optgroup label="Retail"><option>Retail</option></optgroup><optgroup label="Wholsale"><option>In Group</option><option>Auction / Wholesaler</option></optgroup></select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="action">
                                            <a href="#vehicle_update">Update</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <fieldset style="width: 22%; float: left;">
                                <legend>Notes</legend>
                                <table>
                                    <thead><tr><th><label for="notes_reconditioning">Reconditioning</label></th></tr></thead>
                                    <tfoot><tr><td class="action"><a href="#notes_reconditioning_update">Update</a></td></tr></tfoot>
                                    <tbody><tr><td><textarea id="notes_reconditioning" name="notes_reconditioning" rows="4"></textarea></td></tr></tbody>
                                </table>
                                <table>
                                    <thead><tr><th><label for="notes_customer_offer">Customer</label></th></tr></thead>
                                    <tfoot><tr><td class="action"><a href="#notes_customer_offer_update">Update</a></td></tr></tfoot>
                                    <tbody><tr><td><textarea id="notes_customer_offer" name="notes_customer_offer" rows="4"></textarea></td></tr></tbody>
                                </table>
                            </fieldset>
                            <fieldset style="clear: none; width: 70%; float: left;">
                                <legend>Prices</legend>
                                <div style="float: left; width: 32%; margin: 3px;">
                                    <table border="1">
                                        <caption>Reconditioning</caption>
                                        <tbody>
                                            <tr>
                                                <th>Amount</th>
                                                <td><input type="number" id="price_reconditioning" name="price_reconditioning" /></td>
                                            </tr>
                                            <tr>
                                                <th>Employee</th>
                                                <td><select id="employee_reconditioning" name="employee_reconditioning"></select></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="action"><a href="#price_reconditioning_update">Update</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="1">
                                        <caption>History</caption>
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Amount</th>
                                                <th>Employee</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>10/5</td>
                                                <td>150</td>
                                                <td>E Smith</td>
                                            </tr>
                                            <tr>
                                                <td>10/6</td>
                                                <td>400</td>
                                                <td>E Smith</td>
                                            </tr>
                                            <tr>
                                                <td>10/7</td>
                                                <td>600</td>
                                                <td>E Smith</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="float: left; width: 32%; margin: 3px;">
                                    <table border="1">
                                        <caption>Appraisal</caption>
                                        <tbody>
                                            <tr>
                                                <th>Amount</th>
                                                <td><input type="number" id="price_appraisal" name="price_appraisal" /></td>
                                            </tr>
                                            <tr>
                                                <th>Employee</th>
                                                <td><select id="Select1" name="employee_appraisal"></select></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="action"><a href="#price_appraisal_update">Update</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="1">
                                        <caption>History</caption>
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Amount</th>
                                                <th>Employee</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>10/5</td>
                                                <td>15000</td>
                                                <td>E Smith</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="float: left; width: 32%; margin: 3px;">
                                    <table border="1">
                                        <caption>Offer</caption>
                                        <tbody>
                                            <tr>
                                                <th>Amount</th>
                                                <td><input type="number" id="price_offer" name="price_offer" /></td>
                                            </tr>
                                            <tr>
                                                <th>Employee</th>
                                                <td><select id="employee_offer" name="employee_offer"></select></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="action"><a href="#price_offer_update">Update</a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="1">
                                        <caption>History</caption>
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Amount</th>
                                                <th>Employee</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>10/5</td>
                                                <td>15000</td>
                                                <td>E Smith</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </fieldset>
                    <fieldset id="parties" class="hidden">
                        <legend>Parties</legend>
                        <div>
                            <table border="1" style="width: 60%; float: left; margin: 3px;">
                                <caption>People</caption>
                                <thead>
                                    <tr>
                                        <th>First</th>
                                        <th>Middle</th>
                                        <th>Last</th>
                                        <th>Gender</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td><input type="text" /></td>
                                        <td><input type="text" /></td>
                                        <td><input type="text" /></td>
                                        <td><select><option>-Select-</option><option>Male</option><option>Female</option></select></td>
                                        <td><a href="#person_party_add">Add</a></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>Simon</td>
                                        <td>B</td>
                                        <td>Wenmouth</td>
                                        <td>M</td>
                                        <td><a href="#person_party_select">Select</a></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="1" style="width: 38%; float: left; margin: 3px;">
                                <caption>Organizations</caption>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td><input type="text" /></td>
                                        <td><a href="#organization_party_add">Add</a></td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>Manheim Illinois</td>
                                        <td><a href="#organization_party_select">Select</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div style="width: 48%; margin: 3px; float: left;">
                                <fieldset>
                                    <legend>Person</legend>
                                    <table border="1">
                                        <caption>Name</caption>
                                        <tbody>
                                            <tr>
                                                <th><label for="name_title">Title</label></th>
                                                <td><select id="name_title" name="name_title"><option>-Select-</option><option>Mr</option><option>Mrs</option></select></td>
                                            </tr>
                                            <tr>
                                                <th><label for="name_given">Given</label></th>
                                                <td><input type="text" name="name_given" id="name_given" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="name_middle">Middle</label></th>
                                                <td><input type="text" name="name_middle" id="name_middle" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="name_family">Family</label></th>
                                                <td><input type="text" name="name_family" id="name_family" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="1">
                                        <caption>Demographics</caption>
                                        <tbody>
                                            <tr>
                                                <th><label for="demo_gender">Gender</label></th>
                                                <td><select id="demo_gender" name="demo_gender"><option>-Select-</option><option>Male</option><option>Female</option></select></td>
                                            </tr>
                                            <tr>
                                                <th><label for="demo_birth">Birth</label></th>
                                                <td><input type="date" name="demo_birth" id="demo_birth" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="demo_occupation">Occupation</label></th>
                                                <td><input type="text" name="demo_occupation" id="demo_occupation" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="demo_marital">Marital</label></th>
                                                <td><select id="demo_marital" name="demo_marital"><option>-Select-</option><option>Single</option><option>Married</option></select></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    <legend>Organization</legend>
                                    <table border="1">
                                        <tbody>
                                            <tr>
                                                <th><label for="organization_name">Name</label></th>
                                                <td><input type="text" name="organization_name" id="organization_name" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                            <div style="width: 48%; margin: 3px; float: left;">
                                <fieldset>
                                    <legend>Contact</legend>
                                    <table border="1">
                                        <caption>Email</caption>
                                        <thead>
                                            <tr>
                                                <th>Email</th>
                                                <th>Is <br /> Primary</th>
                                                <th>Purpose</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="email" name="email_0_value" id="email_0_value" /></td>
                                                <td><input type="checkbox" name="email_0_primary" id="email_0_primary" /></td>
                                                <td><select name="email_0_purpose" id="email_0_purpose"><option>-Select-</option><option>Alternate</option><option>Home</option><option>General</option><option>Work</option></select></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="1">
                                        <caption>Phone</caption>
                                        <thead>
                                            <tr>
                                                <th colspan="3">Phone #</th>
                                                <th>Is <br /> Primary</th>
                                                <th>Purpose</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="number" name="phone_0_area" id="phone_0_area" maxlength="3" /></td>
                                                <td><input type="number" name="phone_0_office" id="phone_0_office" maxlength="3" /></td>
                                                <td><input type="number" name="phone_0_subscriber" id="phone_0_subscriber" maxlength="4" /></td>
                                                <td><input type="checkbox" name="phone_0_primary" id="phone_0_primary" /></td>
                                                <td><select name="phone_0_purpose" id="phone_0_purpose"><option>-Select-</option><option>Alternate</option><option>Home</option><option>General</option><option>Work</option></select></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="1">
                                        <caption>Postal Address</caption>
                                        <tbody>
                                            <tr>
                                                <th><label for="address_0_line1">Line 1</label></th>
                                                <td><input type="text" name="address_0_line1" id="address_0_line1" maxlength="250" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="address_0_line2">Line 2</label></th>
                                                <td><input type="text" name="address_0_line2" id="address_0_line2" maxlength="250" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="address_0_city">City</label></th>
                                                <td><input type="text" name="address_0_city" id="address_0_city" maxlength="250" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="address_0_state">State</label></th>
                                                <td><input type="text" name="address_0_state" id="address_0_state" maxlength="2" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="address_0_zip">Zip</label></th>
                                                <td><input type="number" name="address_0_zip" id="address_0_zip" maxlength="6" style="width:6em;" /> - <input type="number" name="address_0_zipplus4" id="address_0_zipplus4" maxlength="4" style="width:4em;" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="address_0_primary">Is Primary</label></th>
                                                <td><input type="checkbox" name="address_0_primary" id="address_0_primary" /></td>
                                            </tr>
                                            <tr>
                                                <th><label for="address_0_purpose">Purpose</label></th>
                                                <td><select name="address_0_purpose" id="address_0_purpose"><option>-Select-</option><option>Alternate</option><option>Home</option><option>General</option><option>Work</option></select></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                    </fieldset>
                </fieldset>
            </div>
        </div>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Deals/Application.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Deals/TestTemplates.js"></script>
        <script type="text/javascript" charset="utf-8">
            var _Deal = new Deal();
            _Deal.main();
            if (typeof JSON === "undefined") {
                $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
            }
            
        </script>
    </form>
</body>
</html>
