﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestDesktop.aspx.cs" Inherits="FirstLook.Deal.WebApplication.Pages.Parties.TestDesktop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Party</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css" media="screen">
        /* layout */
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        #subject, .controls, #name, #demographics, .results { float: left; width: 450px; overflow: auto; }
        #name, #person_party, #organization_party, .controls { clear: left; }
        #controls, #demographics, .results { margin-left: 20px; }
        /* tab bar */
        .menu { clear: left; }
        .menu ul { padding-left: 1ex; list-style-type: none; list-style-position: inside; }
        .menu li {  float: left; width: 20ex; padding: 1ex; border: solid 1px grey; margin-left: 2px; text-align: center; }
        .selected { background-color: Lime; }
        .hidden { display: none; }
        /* input css */
        input { width: 95%; margin-left: 2%; }
        /* table css */
        table
        {
            width: 100%;
	        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	        font-size: 12px;
	        text-align: left;
	        border-collapse: collapse;
        }
        table th
        {
	        font-size: 13px;
	        font-weight: normal;
	        padding: 8px;
	        background: #b9c9fe;
	        border-top: 4px solid #aabcfe;
	        border-bottom: 1px solid #fff;
	        color: #039;
        }
        table td
        {
	        padding: 8px;
	        background: #e8edff; 
	        border-bottom: 1px solid #fff;
	        color: #669;
	        border-top: 1px solid transparent;
        }
        table tr:hover td
        {
	        background: #d0dafd;
	        color: #339;
        }
        td.action
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="PartyForm" runat="server">
        <div id="nav_header">
        </div>
        <div id="main_content">
            <div id="header">
                <h1>Party</h1>
            </div>
            <div id="body">
                <div id="subject">
                    <table>
                        <caption>Broker</caption>
                        <thead>
                            <tr>
                                <th><label for="broker">Broker</label></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                                <td class="action"><a href="#broker">Select</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="menu">
                    <ul>
                        <li id="menu_item_person_party" class="selected"><a href="#person_party">Person Party</a></li>
                        <li id="menu_item_organization_party" ><a href="#organization_party">Organization Party</a></li>
                    </ul>
                </div>
                <fieldset id="person_party">
                    <legend>Person Party</legend>
                    <div id="name"></div>
                    <div id="demographics"></div>
                    <div id="person_controls" class="controls">
                        <table border="1">
                            <caption>Controls</caption>
                            <thead>
                                <tr>
                                    <th colspan="2">Search Mode</th>
                                    <th colspan="2">Edit Mode</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="action"><a href="#reset">Reset</a></td>
                                    <td class="action"><a href="#search">Search</a></td>
                                    <td class="action"><a href="#cancel">Cancel</a></td>
                                    <td class="action"><a href="#save">Save</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="person_results" class="results">
                        <table border="1">
                            <caption>Results</caption>
                            <thead>
                                <tr>
                                    <th colspan="4">Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Mr</td><td>Simon</td><td>B</td><td>Wenmouth</td>
                                    <td class="action"><a href="#edit">Edit</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
                <fieldset id="organization_party" class="hidden">
                    <legend>Organization Party</legend>
                    <div>
                        <table border="1">
                            <caption>Organization</caption>
                            <tbody>
                                <tr>
                                    <th><label for="organization_name">Name</label></th>
                                    <td><input type="text" name="organization_name" id="organization_name" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="organization_controls" class="controls">
                        <table border="1">
                            <caption>Controls</caption>
                            <thead>
                                <tr>
                                    <th colspan="2">Search Mode</th>
                                    <th colspan="2">Edit Mode</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="action"><a href="#reset">Reset</a></td>
                                    <td class="action"><a href="#search">Search</a></td>
                                    <td class="action"><a href="#cancel">Cancel</a></td>
                                    <td class="action"><a href="#save">Save</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="organization_results" class="results">
                        <table border="1">
                            <caption>Results</caption>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Manheim Illinois</td>
                                    <td class="action"><a href="#edit">Edit</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
        </div>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>

        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Persons/Application.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Persons/TestTemplates.js"></script>

        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Parties/Application.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Parties/TestTemplates.js"></script>

        <script type="text/javascript" charset="utf-8">
            var _Party = new Party();
            _Party.main();
            if (typeof JSON === "undefined") {
                $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
            }
        </script>
    </form>
</body>
</html>
