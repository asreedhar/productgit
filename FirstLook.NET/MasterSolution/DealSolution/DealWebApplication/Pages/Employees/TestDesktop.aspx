﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestDesktop.aspx.cs" Inherits="FirstLook.Deal.WebApplication.Pages.Employees.TestDesktop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Employees</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css" media="screen">
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        fieldset { width: 450px; overflow: auto; }
        table { width: 100%; }
        input { width: 95%; margin-left: 2%; }
        select { margin-left: 2%; }
        h2, #subject, #name, #demographics, #roles, #controls, #results { float: left; }
        h2, #name, #roles, #results { clear: left; }
        #results { width: 933px; }
        td.action { text-align: center; }
    </style>
</head>
<body>
    <form id="EmployeeForm" runat="server">
        <div id="nav_header">
        </div>
        <div id="main_content">
            <div id="header">
                <h1>Employees</h1>
            </div>
            <div id="body">
                <fieldset id="subject">
                    <legend>Broker</legend>
                    <table border="1">
                        <tbody>
                            <tr>
                                <th><label for="broker">Broker</label></th>
                                <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                                <td><a href="#broker">Select</a></td>                                
                            </tr>
                            <tr>
                                <th><label for="employee_id">Employee Id</label></th>
                                <td><input type="text" id="employee_id" name="employee_id" maxlength="36" /></td>
                            </tr>
                            <tr>
                                <th><label for="Rev1">Employee Revision Number</label></th>
                                <td><input type="text" id="Rev1" name="Rev1" maxlength="36" /></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
                <h2>Example</h2>
                <fieldset id="name">
                    <legend>Name</legend>
                    <table border="1">
                        <tbody>
                            <tr>
                                <td><label for="name_title">Title</label></td>
                                <td><select id="name_title" name="name_title"><option value='0'>-Select-</option><option value='1'>Mr.</option><option value='2'>Mrs.</option></select></td>
                            </tr>
                            <tr>
                                <td><label for="name_given">Given</label></td>
                                <td><input type="text" name="name_given" id="name_given" /></td>
                            </tr>
                            <tr>
                                <td><label for="name_middle">Middle</label></td>
                                <td><input type="text" name="name_middle" id="name_middle" /></td>
                            </tr>
                            <tr>
                                <td><label for="name_family">Family</label></td>
                                <td><input type="text" name="name_family" id="name_family" /></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
                <fieldset id="demographics">
                    <legend>Demographics</legend>
                    <table border="1">
                        <tbody>
                            <tr>
                                <td><label for="demo_gender">Gender</label></td>
                                <td><select id="demo_gender" name="demo_gender"><option value='0'>-Select-</option><option value='M'>Male</option><option value='F'>Female</option></select></td>
                            </tr>
                            <tr>
                                <td><label for="demo_birth">Birth</label></td>
                                <td><input type="date" name="demo_birth" id="demo_birth" /></td>
                            </tr>
                            <tr>
                                <td><label for="demo_occupation">Occupation</label></td>
                                <td><input type="text" name="demo_occupation" id="demo_occupation" /></td>
                            </tr>
                            <tr>
                                <td><label for="demo_marital">Marital</label></td>
                                <td><select id="demo_marital" name="demo_marital"><option value='0'>-Select-</option><option value='S'>Single</option><option value='M'>Married</option></select></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
                <fieldset id="roles">
                    <legend>Roles</legend>
                    <table border="1">
                        <thead>
                            <tr>
                                <th><label for="role_a">Appraiser</label></th>
                                <th><label for="role_b">Buyer</label></th>
                                <th><label for="role_s">Salesperson</label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th><input type="checkbox" name="role_a" id="role_a"/></th>
                                <th><input type="checkbox" name="role_b" id="role_b"/></th>
                                <th><input type="checkbox" name="role_s" id="role_s"/></th>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
                <fieldset id="controls">
                    <legend>Controls</legend>
                    <table border="1">
                        <thead>
                            <tr>
                                <th colspan="2">Search Mode</th>
                                <th colspan="2">Edit Mode</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="action"><a href="#reset">Reset</a></td>
                                <td class="action"><a href="#search">Search</a></td>
                                <td class="action"><a href="#cancel">Cancel</a></td>
                                <td class="action"><a href="#save">Save</a></td>
                            </tr>
                            <tr>
                                 <td class="action"><a href="#load">Fetch</a></td>
                                 <td></td>
                                 <td class="action"><a href="#delete">Delete</a></td>
                                 <td></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
                <h2>Employees</h2>
                <fieldset id="results">
                    <legend>Results</legend>
                    <table border="1" id="employees">
                        <thead>
                            <tr>
                                <th colspan="4">Name</th>
                                <th>Is <br /> Appraiser</th>
                                <th>Is <br /> Buyer</th>
                                <th>Is <br /> Salesperson</th>
                                <th colspan="2"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Mr</td><td>Simon</td><td>B</td><td>Wenmouth</td>
                                <td>Y</td><td>Y</td><td>Y</td>
                                <td class="action"><a href="#edit">Edit</a></td>
                                <td class="action"><a href="#delete">Delete</a></td>
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            </div>
        </div>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Employees/Application.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Employees/TestTemplates.js"></script>
        <script type="text/javascript" charset="utf-8">
            var _Employee = new Employee();
            _Employee.main();
            if (typeof JSON === "undefined") {
                $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
            }
        </script>
    </form>
</body>
</html>
