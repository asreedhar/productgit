﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestDesktop.aspx.cs" Inherits="FirstLook.Deal.WebApplication.Pages.Contacts.TestDesktop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Contact</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css" media="screen">
        /* layout */
        #main_content { width: 975px; margin-left: auto; margin-right: auto; }
        #subject, #controls { float: left; width: 450px; overflow: auto; }
        #email { clear: left; }
        #email, #phone, #postal_address { width: 920px; }
        #controls { margin-left: 20px; }
        /* input css */
        input { width: 95%; margin-left: 2%; }
        /* table css */
        table
        {
            width: 100%;
	        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	        font-size: 12px;
	        text-align: left;
	        border-collapse: collapse;
        }
        table th
        {
	        font-size: 13px;
	        font-weight: normal;
	        padding: 8px;
	        background: #b9c9fe;
	        border-top: 4px solid #aabcfe;
	        border-bottom: 1px solid #fff;
	        color: #039;
        }
        table td
        {
	        padding: 8px;
	        background: #e8edff; 
	        border-bottom: 1px solid #fff;
	        color: #669;
	        border-top: 1px solid transparent;
        }
        table tr:hover td
        {
	        background: #d0dafd;
	        color: #339;
        }
        td.action
        {
            text-align: center;
        }
        th.city
        {
            width: 8em;
        }
        th.state, th.zip, th.zip_plus_4, th.primary, th.purpose 
        {
            width: 4em;
        }
    </style>
</head>
<body>
    <form id="ContactForm" runat="server">
        <div id="nav_header">
        </div>
        <div id="main_content">
            <div id="header">
                <h1>Contact</h1>
            </div>
            <div id="body">
                <div id="subject">
                    <table>
                        <caption>Broker</caption>
                        <thead>
                            <tr>
                                <th><label for="broker">Broker</label></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                                <td class="action"><a href="#broker">Select</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="controls">
                    <table>
                        <caption>Controls</caption>
                        <thead>
                            <tr>
                                <th><label for="contact_id">ID</label></th>
                                <th colspan="4"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" id="contact_id" name="contact_id" maxlength="10" /></td>
                                <td><a href="#new">New</a></td>
                                <td><a href="#load">Load</a></td>
                                <td><a href="#save">Save</a></td>
                                <td><a href="#delete">Delete</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="email"></div>
                <div id="phone"></div>
                <div id="postal_address"></div>
            </div>
        </div>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Contacts/Application.js"></script>
        <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Contacts/TestTemplates.js"></script>
        <script type="text/javascript" charset="utf-8">

            var _contact = new Contact();

            _contact.main();
            
            if (typeof JSON === "undefined") {
                $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
            }
            
        </script>
    </form>
</body>
</html>
