using System;
using System.Linq;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using FirstLook.Common.PhotoServices;
using NUnit.Framework;

namespace PhotoService_Test
{
    [TestFixture]
    public class VehiclesPhotosResultAdapterTests
    {
        [Test]
        public void Constructor_should_throw_if_inner_is_null()
        {
            Assert.Throws<ArgumentNullException>(
                () => new VehiclesPhotosResultAdapter(null));
        }

        [Test]
        public void PhotosByVin_dictinoary_should_be_empty_if_inner_Vehicles_is_null()
        {
            var inner = new VehiclesPhotosResult();

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.PhotosByVin.Count, Is.EqualTo(0));
        }

        [Test]
        public void PhotosByVin_should_contain_vehicles_from_inner()
        {
            var inner = new VehiclesPhotosResult(
                new VehiclePhotos("A1234", "http://b", "http://c"),
                new VehiclePhotos("B1234", "http://d", "http://e"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.PhotosByVin.Keys, Is.EquivalentTo(new[] { "A1234", "B1234" }));
        }

        [Test]
        public void PhotosByVin_should_merge_duplicate_photos_for_duplicate_vins()
        {
            var inner = new VehiclesPhotosResult(
                new VehiclePhotos("A1234", "http://b", "http://c"),
                new VehiclePhotos("A1234", "http://d", "http://e"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.PhotosByVin["a1234"].PhotoUrls,
                        Is.EqualTo(new[] {"http://b", "http://c", "http://d", "http://e"}));
        }

        [Test]
        public void PhotosByVin_should_compare_vins_ignoring_case()
        {
            var inner = new VehiclesPhotosResult(
                new VehiclePhotos("A1234", "http://b", "http://c"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.PhotosByVin["a1234"].PhotoUrls.Length, Is.EqualTo(2));
        }

        [Test]
        public void PhotosByVin_should_skip_vehicles_with_empty_vin_from_inner()
        {
            var inner = new VehiclesPhotosResult(
                new VehiclePhotos("A1234", "http://a"),
                new VehiclePhotos(null, "http://b"),
                new VehiclePhotos("", "http://c"),
                new VehiclePhotos("  ", "http://d"),
                new VehiclePhotos("B1234", "http://e"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.PhotosByVin.Values.Select(v => v.PhotoUrls.First()).ToArray(),
                Is.EquivalentTo(new [] { "http://a", "http://e" }));
        }

        [Test]
        public void PhotosByVin_should_contain_photo_urls()
        {
            var inner = new VehiclesPhotosResult(
                new VehiclePhotos("A1234", "http://a", "http://b", "http://c"),
                new VehiclePhotos("B1234", "http://d"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.PhotosByVin["A1234"].PhotoUrls, Is.EquivalentTo(new[] {"http://a", "http://b", "http://c"}));
        }

        [Test]
        public void PhotosByVin_PhotoUrls_should_skip_empty_or_null_urls_from_inner()
        {
            var inner = new VehiclesPhotosResult(
                new VehiclePhotos("A1234", "http://a", "   ", "", null, "http://21"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.PhotosByVin["A1234"].PhotoUrls, Is.EquivalentTo(new[] { "http://a", "http://21" }));
        }

        [Test]
        public void Errors_should_return_inner_errors()
        {
            var inner = new VehiclesPhotosResult(new Error("NullReferenceException", "Pointer was null."));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Select(e => e.Code).ToArray(), Is.EquivalentTo(new[] { "NullReferenceException" }));
        }

        [Test]
        public void Errors_should_return_empty_array_when_inner_errors_is_null()
        {
            var inner = new VehiclesPhotosResult();

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Count, Is.EqualTo(0));
        }

        [TestCase("Foo", null, "Foo", "")]
        [TestCase(null, "Msg", "", "Msg")]
        public void Errors_should_return_only_empty_strings_for_inner_errors(string innerCode, string innerMsg, string outerCode, string outerMsg)
        {
            var inner = new VehiclesPhotosResult(new Error(innerCode, innerMsg));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Count, Is.EqualTo(1));
            Assert.That(adapter.Errors.First().Code, Is.EqualTo(outerCode));
            Assert.That(adapter.Errors.First().Message, Is.EqualTo(outerMsg));
        }

        [Test]
        public void Errors_should_skip_over_null_inner_elements()
        {
            var inner = new VehiclesPhotosResult(new Error("A", "A"), null, new Error("B", "B"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Select(e => e.Code).ToArray(), Is.EquivalentTo(new[] { "A", "B" }));
        }

        [Test]
        public void Errors_should_skip_over_elements_that_have_null_or_empty_code_and_message()
        {
            var inner = new VehiclesPhotosResult(new Error("A", "A"), new Error(null, null), new Error("", ""),
                                                 new Error("  ", "  "), new Error("B", "B"));

            var adapter = new VehiclesPhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Select(e => e.Code).ToArray(), Is.EquivalentTo(new[] { "A", "B" }));
        }
    }
}