using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.PhotoServices.WebService.V1;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using FirstLook.Common.PhotoServices;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;
using VehiclesPhotosResult = FirstLook.Common.Core.PhotoServices.WebService.V1.Model.VehiclesPhotosResult;

namespace PhotoService_Test
{
    [TestFixture]
    public class PhotoServicesTests
    {
        private const string Address = "http://localhost/photoservices/";

        private Mock<ILoggerFactory> mockLoggerFactory;
        private Mock<IChannelFactory<IPhotoWebServices>> mockChannelFactory;
        private Mock<IPhotoServicesSettings> mockSettings;
        private Mock<IPhotoWebServices> mockPhotoWebServices;
        private Mock<ILog> mockLog;
        private IPhotoServices photoServices;
        private Mock<IMember> mockMember;
        private Mock<IClientChannel> mockClientChannel;
        private Mock<ICache> mockCache;

        [SetUp]
        public void Setup()
        {
            mockLoggerFactory = new Mock<ILoggerFactory>();
            mockChannelFactory = new Mock<IChannelFactory<IPhotoWebServices>>();
            mockSettings = new Mock<IPhotoServicesSettings>();
            mockPhotoWebServices = new Mock<IPhotoWebServices>();
            mockPhotoWebServices.As<IDisposable>();
            mockClientChannel = mockPhotoWebServices.As<IClientChannel>();
            mockLog = new Mock<ILog>();
            mockMember = new Mock<IMember>();
            mockCache = new Mock<ICache>();

            mockSettings.Setup(s => s.PhotoWebServicesUrl).Returns(Address);
            mockSettings.Setup(s => s.PhotoWebServiceV2_BusinessUnit_Whitelist).Returns(String.Empty);

            mockLoggerFactory.Setup(f => f.GetLogger<PhotoServices>()).Returns(mockLog.Object);
            mockChannelFactory.Setup(f => f.CreateChannel(It.Is<EndpointAddress>(addr => addr.Uri == new Uri(Address))))
                .Returns(mockPhotoWebServices.Object);

            photoServices = new PhotoServices(mockChannelFactory.Object, mockLoggerFactory.Object,
                                              mockSettings.Object, mockCache.Object);
        }

        [TearDown]
        public void Teardown()
        {
        }

        [TestCase("rhogan", "Richard", "Hogan", "WINDY56", "V1234", "STK1234")]
        [TestCase(null, "Richard", "Hogan", "WINDY56", "V1234", "STK1234", ExpectedException=typeof(ArgumentNullException))]
        [TestCase("", "Richard", "Hogan", "WINDY56", "V1234", "STK1234", ExpectedException=typeof(ArgumentNullException))]
        [TestCase("   ", "Richard", "Hogan", "WINDY56", "V1234", "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", null, "Hogan", "WINDY56", "V1234", "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", null, "WINDY56", "V1234", "STK1234", ExpectedException=typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "   ", "V1234", "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "", "V1234", "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", null, "V1234", "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "WINDY56", "   ", "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "WINDY56", "", "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "WINDY56", null, "STK1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "WINDY56", "V1234", "   ", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "WINDY56", "V1234", "", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "WINDY56", "V1234", null, ExpectedException = typeof(ArgumentNullException))]
        public void GetPhotoManagerUrl_should_pass_values_to_inner_proxy(string username, string first, string last, string bucode, string vin, string stk)
        {
            SetupMockMember(username, first, last);

            photoServices.GetPhotoManagerUrl(mockMember.Object, bucode, vin, stk, PhotoManagerContext.IMS);

            mockPhotoWebServices.Verify(s =>
                                        s.GetPhotoManagerUrl(username, first, last, bucode, vin, stk,
                                                             PhotoManagerWebContext.IMS),
                                        Times.Once());
        }

        [Test]
        public void GetPhotoManager_should_not_accept_null_auditUser()
        {
            Assert.Throws<ArgumentNullException>(
                () =>
                photoServices.GetPhotoManagerUrl(null, "WINDY77", "V1234", "STK1234", PhotoManagerContext.MAX));
        }

        [Test]
        public void GetPhotoManager_should_not_accept_null_photoManagerContext()
        {
            SetupMockMember("rbern", "Rob", "Bern");

            Assert.Throws<ArgumentNullException>(
                () =>
                photoServices.GetPhotoManagerUrl(mockMember.Object, "WINDY83", "V1234", "STK1234", null));
        }

        [Test]
        public void GetPhotoManager_should_return_placeholder_url_if_inner_proxy_throws()
        {
            SetupMockMember("jnorris", "James", "Norris");
            mockPhotoWebServices.Setup(
                s =>
                s.GetPhotoManagerUrl("jnorris", "James", "Norris", "WINDY15", "V1234", "STK1234",
                                     PhotoManagerWebContext.IMS))
                .Throws<ApplicationException>();
            mockSettings.Setup(s => s.PhotoManagerNotAvailableUrl).Returns("http://photo-manager/placeholder");

            var url = photoServices.GetPhotoManagerUrl(mockMember.Object, "WINDY15", "V1234", "STK1234",
                                                       PhotoManagerContext.IMS);

            Assert.That(url, Is.EqualTo("http://photo-manager/placeholder"));
        }

        [Test]
        public void GetPhotoManager_should_log_error_if_inner_proxy_throws()
        {
            SetupMockMember("jnorris", "James", "Norris");
            mockPhotoWebServices.Setup(
                s =>
                s.GetPhotoManagerUrl("jnorris", "James", "Norris", "WINDY15", "V1234", "STK1234",
                                     PhotoManagerWebContext.IMS))
                .Throws<ApplicationException>();
            mockSettings.Setup(s => s.PhotoManagerNotAvailableUrl).Returns("http://photo-manager/placeholder");

            photoServices.GetPhotoManagerUrl(mockMember.Object, "WINDY15", "V1234", "STK1234",
                                                       PhotoManagerContext.IMS);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestCase("rhogan", "Richard", "Hogan", "WINDY56")]
        [TestCase(null, "Richard", "Hogan", "WINDY56", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("", "Richard", "Hogan", "WINDY56", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("   ", "Richard", "Hogan", "WINDY56", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", null, "Hogan", "WINDY56", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", null, "WINDY56", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "   ", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", "", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("rhogan", "Richard", "Hogan", null, ExpectedException = typeof(ArgumentNullException))]
        public void GetBulkUploadManagerUrl_should_pass_values_to_inner_proxy(string username, string first, string last, string bucode)
        {
            SetupMockMember(username, first, last);

            photoServices.GetBulkUploadManagerUrl(mockMember.Object, bucode, PhotoManagerContext.MAX);

            mockPhotoWebServices.Verify(s =>
                                        s.GetBulkUploadManagerUrl(username, first, last, bucode,
                                                             PhotoManagerWebContext.MAX),
                                        Times.Once());
        }

        [Test]
        public void GetBulkUploadManagerUrl_should_not_accept_null_auditUser()
        {
            Assert.Throws<ArgumentNullException>(
                () =>
                photoServices.GetBulkUploadManagerUrl(null, "WINDY77", PhotoManagerContext.MAX));
        }

        [Test]
        public void GetBulkUploadManagerUrl_should_not_accept_null_photoManagerContext()
        {
            SetupMockMember("rbern", "Rob", "Bern");

            Assert.Throws<ArgumentNullException>(
                () =>
                photoServices.GetBulkUploadManagerUrl(mockMember.Object, "WINDY83", null));
        }

        [Test]
        public void GetBulkUploadManagerUrl_should_return_placeholder_url_if_inner_proxy_throws()
        {
            SetupMockMember("jnorris", "James", "Norris");
            mockPhotoWebServices.Setup(
                s =>
                s.GetBulkUploadManagerUrl("jnorris", "James", "Norris", "WINDY15", 
                                     PhotoManagerWebContext.IMS))
                .Throws<ApplicationException>();
            mockSettings.Setup(s => s.BulkUploadManagerNotAvailableUrl).Returns("http://bulk-manager/placeholder");

            var url = photoServices.GetBulkUploadManagerUrl(mockMember.Object, "WINDY15",
                                                       PhotoManagerContext.IMS);

            Assert.That(url, Is.EqualTo("http://bulk-manager/placeholder"));
        }

        [Test]
        public void GetBulkUploadManagerUrl_should_log_error_if_inner_proxy_throws()
        {
            SetupMockMember("jnorris", "James", "Norris");
            mockPhotoWebServices.Setup(
                s =>
                s.GetBulkUploadManagerUrl("jnorris", "James", "Norris", "WINDY15",
                                     PhotoManagerWebContext.IMS))
                .Throws<ApplicationException>();

            photoServices.GetBulkUploadManagerUrl(mockMember.Object, "WINDY15", PhotoManagerContext.IMS);

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestCase("WINDY91", "V1234")]
        [TestCase("   ", "V1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("", "V1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase(null, "V1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("WINDY91", "   ", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("WINDY91", "", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("WINDY91", null, ExpectedException = typeof(ArgumentNullException))]
        public void GetMainPhotoUrl_should_pass_values_to_inner_proxy(string bucode, string vin)
        {
            photoServices.GetMainPhotoUrl(bucode, vin);

            mockPhotoWebServices.Verify(s => s.GetMainPhotoUrl(bucode, vin),
                                        Times.Once());
        }

        [Test]
        public void GetMainPhotoUrl_should_return_placeholder_url_if_inner_proxy_throws()
        {
            mockPhotoWebServices.Setup(
                s => s.GetMainPhotoUrl("WINDY15", "V1234"))
                .Throws<ApplicationException>();
            mockSettings.Setup(s => s.MainPhotoPlaceholderUrl).Returns("http://photos/main-placeholder");

            var url = photoServices.GetMainPhotoUrl("WINDY15", "V1234");

            Assert.That(url, Is.StringStarting("http://photos/main-placeholder"));
        }

        [Test]
        public void GetMainPhotoUrl_should_log_error_if_inner_proxy_throws()
        {
            mockPhotoWebServices.Setup(
                s => s.GetMainPhotoUrl("WINDY15", "V123A"))
                .Throws<ApplicationException>();

            photoServices.GetMainPhotoUrl("WINDY15", "V123A");

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestCase("   ", "V1234")]
        [TestCase("", "V1234")]
        [TestCase(null, "V1234")]
        [TestCase("WINDY91", "   ")]
        [TestCase("WINDY91", "")]
        [TestCase("WINDY91", null)]
        public void GetMainThumbnailUrl_with_incorrect_parameters_should_throw(string bucode, string vin)
        {
            Assert.Throws<ArgumentNullException>(
                () => photoServices.GetMainThumbnailUrl(bucode, vin));
        }

        [TestCase("WINDY91", "V1234", "http://foo/WINDY91/V1234/thumb.jpg")]
        public void GetMainThumbnailUrl_with_correct_parameters_should_return_url(string bucode, string vin, string expectedUrl)
        {
            mockPhotoWebServices.Setup(p => p.GetSettings())
                .Returns(new Settings {ImageHostingBaseUrl = "http://foo/", ThumbnailHeight = 25, ThumbnailWidth = 36});

            var thumbResult = photoServices.GetMainThumbnailUrl(bucode, vin);

            Assert.That(thumbResult.Url, Is.EqualTo(expectedUrl));
            Assert.That(thumbResult.Width, Is.EqualTo(36));
            Assert.That(thumbResult.Height, Is.EqualTo(25));
        }

        [Test]
        public void GetMainThumbnailUrl_with_correct_parameters_and_should_query_cache()
        {
            mockPhotoWebServices.Setup(p => p.GetSettings())
                .Returns(new Settings { ImageHostingBaseUrl = "http://foo/", ThumbnailHeight = 25, ThumbnailWidth = 36 });

            photoServices.GetMainThumbnailUrl("WINDY91", "V1234");

            mockCache.Verify(c => c.Get(It.IsAny<string>()), Times.AtLeastOnce());
        }

        [Test]
        public void GetMainThumbnailUrl_with_no_web_services_and_invalid_settings_should_throw()
        {
            mockSettings.Setup(s => s.MainThumbnailPlaceholderUrl).Returns("http://photos/thumb-place-holder");
            mockSettings.Setup(s => s.MainThumbnailWidth).Returns(17);
            mockSettings.Setup(s => s.MainThumbnailHeight).Returns(-18); // <- Invalid

            mockPhotoWebServices.Setup(s => s.GetSettings())
                .Throws<ApplicationException>();

            Assert.Throws<ArgumentException>(() => photoServices.GetMainThumbnailUrl("WINDY01", "V1234"));
        }

        [Test]
        public void GetMainThumbnailUrl_when_inner_throws_should_return_placeholder_result()
        {
            mockSettings.Setup(s => s.MainThumbnailPlaceholderUrl).Returns("http://photos/thumb-place-holder");
            mockSettings.Setup(s => s.MainThumbnailWidth).Returns(17);
            mockSettings.Setup(s => s.MainThumbnailHeight).Returns(18);

            mockPhotoWebServices.Setup(s => s.GetSettings())
                .Throws<ApplicationException>();

            var result = photoServices.GetMainThumbnailUrl("WINDY01", "V1234");

            Assert.That(result.Url, Is.EqualTo("http://photos/thumb-place-holder"));
            Assert.That(result.Width, Is.EqualTo(17));
            Assert.That(result.Height, Is.EqualTo(18));
        }

        [Test]
        public void GetMainThumbnailUrl_when_inner_throws_should_log_error()
        {
            mockSettings.Setup(s => s.MainThumbnailPlaceholderUrl).Returns("http://photos/thumb-place-holder");
            mockSettings.Setup(s => s.MainThumbnailWidth).Returns(17);
            mockSettings.Setup(s => s.MainThumbnailHeight).Returns(18);

            mockPhotoWebServices.Setup(s => s.GetSettings())
                .Throws<ApplicationException>();

            photoServices.GetMainThumbnailUrl("WINDY01", "V1234");

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void GetMainThumbnailUrl_when_inner_cache_throws_should_log_error()
        {
            mockSettings.Setup(s => s.MainThumbnailPlaceholderUrl).Returns("http://photos/thumb-place-holder");
            mockSettings.Setup(s => s.MainThumbnailWidth).Returns(17);
            mockSettings.Setup(s => s.MainThumbnailHeight).Returns(18);

            mockCache.Setup(c => c.Get(It.IsAny<string>()))
                .Throws<ApplicationException>();

            photoServices.GetMainThumbnailUrl("WINDY01", "V1234");

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void GetMainThumbnailUrl_when_inner_cache_throws_should_return_placeholder_result()
        {
            mockSettings.Setup(s => s.MainThumbnailPlaceholderUrl).Returns("http://photos/thumb-place-holder");
            mockSettings.Setup(s => s.MainThumbnailWidth).Returns(17);
            mockSettings.Setup(s => s.MainThumbnailHeight).Returns(18);

            mockCache.Setup(c => c.Get(It.IsAny<string>()))
                .Throws<ApplicationException>();

            var result = photoServices.GetMainThumbnailUrl("WINDY01", "V1234");

            Assert.That(result.Url, Is.EqualTo("http://photos/thumb-place-holder"));
            Assert.That(result.Width, Is.EqualTo(17));
            Assert.That(result.Height, Is.EqualTo(18));
        }


        [TestCase("WINDY91", "V1234")]
        [TestCase("   ", "V1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("", "V1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase(null, "V1234", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("WINDY91", "   ", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("WINDY91", "", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("WINDY91", null, ExpectedException = typeof(ArgumentNullException))]
        public void GetPhotoUrlsByVin_should_pass_values_to_inner_proxy(string bucode, string vin)
        {
            photoServices.GetPhotoUrlsByVin(bucode, vin, TimeSpan.FromSeconds(30));

            mockPhotoWebServices.Verify(s => s.GetPhotoUrlsByVin(bucode, vin, 30000));
        }

        [Test]
        public void GetPhotoUrlsByVin_should_transform_data_returned_from_proxy()
        {
            var innerData = new VehiclesPhotosResult(new VehiclePhotos("V1234", "http://a", "http://b"));
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByVin("WINDY12", "V1234", 10000))
                .Returns(innerData);

            var result = photoServices.GetPhotoUrlsByVin("WINDY12", "V1234", TimeSpan.FromSeconds(10));

            Assert.That(result.Errors.Count, Is.EqualTo(0));
            Assert.That(result.Vin, Is.EqualTo("V1234"));
            Assert.That(result.PhotoUrls, Is.EquivalentTo(new[] {"http://a", "http://b"}));
        }

        [Test]
        public void GetPhotoUrlsByVin_should_return_result_with_errors_if_inner_throws()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByVin("WINDY12", "V1234", 10000))
                .Throws<ApplicationException>();

            var result = photoServices.GetPhotoUrlsByVin("WINDY12", "V1234", TimeSpan.FromSeconds(10));

            Assert.That(result.Errors.Count, Is.GreaterThan(0));
        }

        [Test]
        public void GetPhotoUrlsByVin_should_return_empty_vin_if_inner_throws()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByVin("WINDY12", "V1234", 10000))
                .Throws<ApplicationException>();

            var result = photoServices.GetPhotoUrlsByVin("WINDY12", "V1234", TimeSpan.FromSeconds(10));

            Assert.That(result.Vin, Is.Empty);
        }

        [Test]
        public void GetPhotoUrlsByVin_should_return_empty_PhotoUrls_if_inner_throws()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByVin("WINDY12", "V1234", 10000))
                .Throws<ApplicationException>();

            var result = photoServices.GetPhotoUrlsByVin("WINDY12", "V1234", TimeSpan.FromSeconds(10));

            Assert.That(result.PhotoUrls, Is.Empty);
        }

        [Test]
        public void GetPhotoUrlsByVin_should_log_error_if_inner_throws()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByVin("WINDY12", "V1234", 10000))
                .Throws<ApplicationException>();

            photoServices.GetPhotoUrlsByVin("WINDY12", "V1234", TimeSpan.FromSeconds(10));

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void GetPhotoUrlsByVin_should_set_channel_timeout()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByVin("WINDY12", "V1234", 1234))
                .Throws<ApplicationException>();

            photoServices.GetPhotoUrlsByVin("WINDY12", "V1234", TimeSpan.FromMilliseconds(1234));

            mockClientChannel.VerifySet(ch => ch.OperationTimeout = TimeSpan.FromMilliseconds(1234));
        }

        [TestCase("WINDY91")]
        [TestCase("   ", ExpectedException = typeof(ArgumentNullException))]
        [TestCase("", ExpectedException = typeof(ArgumentNullException))]
        [TestCase(null, ExpectedException = typeof(ArgumentNullException))]
        public void GetPhotoUrlsByBusinessUnit_should_pass_values_to_inner_proxy(string bucode)
        {
            photoServices.GetPhotoUrlsByBusinessUnit(bucode, TimeSpan.FromSeconds(30));

            mockPhotoWebServices.Verify(s => s.GetPhotoUrlsByBusinessUnit(bucode, 30000));
        }

        [Test]
        public void GetPhotoUrlsByBusinessUnit_should_transform_data_returned_from_proxy()
        {
            var innerData = new VehiclesPhotosResult(
                new VehiclePhotos("V1234", "http://a", "http://b"),
                new VehiclePhotos("V1235", "http://c", "http://c"));
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByBusinessUnit("WINDY12", 10000))
                .Returns(innerData);

            var result = photoServices.GetPhotoUrlsByBusinessUnit("WINDY12", TimeSpan.FromSeconds(10));

            Assert.That(result.Errors.Count, Is.EqualTo(0));
            Assert.That(result.PhotosByVin.Count, Is.EqualTo(2));
            Assert.That(result.PhotosByVin["V1234"].Vin, Is.EqualTo("V1234"));
            Assert.That(result.PhotosByVin["V1234"].PhotoUrls, Is.EquivalentTo(new[] { "http://a", "http://b" }));
        }

        [Test]
        public void GetPhotoUrlsByBusinessUnit_should_return_result_with_errors_if_inner_throws()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByBusinessUnit("WINDY12", 10000))
                .Throws<ApplicationException>();

            var result = photoServices.GetPhotoUrlsByBusinessUnit("WINDY12", TimeSpan.FromSeconds(10));

            Assert.That(result.Errors.Count, Is.GreaterThan(0));
        }

        [Test]
        public void GetPhotoUrlsByBusinessUnit_should_return_empty_vin_if_inner_throws()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByBusinessUnit("WINDY12", 10000))
                .Throws<ApplicationException>();

            var result = photoServices.GetPhotoUrlsByBusinessUnit("WINDY12", TimeSpan.FromSeconds(10));

            Assert.That(result.PhotosByVin, Is.Empty);
        }

        [Test]
        public void GetPhotoUrlsByBusinessUnit_should_log_error_if_inner_throws()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByBusinessUnit("WINDY12", 10000))
                .Throws<ApplicationException>();

            photoServices.GetPhotoUrlsByBusinessUnit("WINDY12", TimeSpan.FromSeconds(10));

            mockLog.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [Test]
        public void GetPhotoUrlsByBusinessUnit_should_set_channel_timeout()
        {
            mockPhotoWebServices.Setup(s => s.GetPhotoUrlsByBusinessUnit("WINDY12", 1234))
                .Throws<ApplicationException>();

            photoServices.GetPhotoUrlsByBusinessUnit("WINDY12", TimeSpan.FromMilliseconds(1234));

            mockClientChannel.VerifySet(ch => ch.OperationTimeout = TimeSpan.FromMilliseconds(1234));
        }

        private void SetupMockMember(string username, string first, string last)
        {
            mockMember.Setup(m => m.UserName).Returns(username);
            mockMember.Setup(m => m.FirstName).Returns(first);
            mockMember.Setup(m => m.LastName).Returns(last);
        }
    }
}