using System;
using System.Linq;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using FirstLook.Common.PhotoServices;
using NUnit.Framework;

namespace PhotoService_Test
{
    [TestFixture]
    public class VehiclePhotosResultAdapterTests
    {
        [Test]
        public void Passing_null_inner_object_to_adapter_constructor_should_throw()
        {
            Assert.Throws<ArgumentNullException>(
                () => new VehiclePhotosResultAdapter(null));
        }

        [TestCase("V123C", "V123C")]
        [TestCase(null, "")]
        public void Vin_should_return_inner_vin(string innerVal, string expectedOuterVal)
        {
            var inner = new VehiclesPhotosResult(new VehiclePhotos(innerVal, "http://a"));

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Vin, Is.EqualTo(expectedOuterVal));
        }

        [Test]
        public void Vin_should_return_empty_string_if_inner_Vehicles_array_is_empty()
        {
            var inner = new VehiclesPhotosResult(new VehiclePhotos[0]);

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Vin, Is.Empty);
        }

        [Test]
        public void Vin_should_return_empty_string_if_inner_Vehicles_is_null()
        {
            var inner = new VehiclesPhotosResult();

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Vin, Is.Empty);
        }

        [Test]
        public void PhotoUrls_should_return_inner_photo_urls()
        {
            var inner = new VehiclesPhotosResult(new VehiclePhotos("V123C", "http://a", "http://b"));

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.PhotoUrls, Is.EquivalentTo(new[] {"http://a", "http://b"}));
        }

        [TestCase("  ")]
        [TestCase("")]
        [TestCase(null)]
        public void PhotoUrls_should_remove_null_or_empty_urls_from_inner(string innerUrl)
        {
            var inner = new VehiclesPhotosResult(new VehiclePhotos("V123D", "http://a", innerUrl, "http://c"));

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.PhotoUrls, Is.EquivalentTo(new[] {"http://a", "http://c"}));
        }

        [Test]
        public void PhotoUrls_should_return_empty_when_inner_Vehicles_array_is_empty()
        {
            var inner = new VehiclesPhotosResult(new VehiclePhotos[0]);

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.PhotoUrls.Length, Is.EqualTo(0));
        }
        
        [Test]
        public void PhotoUrls_should_return_empty_when_inner_Vehicles_array_is_null()
        {
            var inner = new VehiclesPhotosResult();

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.PhotoUrls.Length, Is.EqualTo(0));
        }

        [Test]
        public void Errors_should_return_inner_errors()
        {
            var inner = new VehiclesPhotosResult(new Error("NullReferenceException", "Pointer was null."));

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Select(e => e.Code).ToArray(), Is.EquivalentTo(new[]{"NullReferenceException"}));
        }

        [Test]
        public void Errors_should_return_empty_array_when_inner_errors_is_null()
        {
            var inner = new VehiclesPhotosResult();

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Count, Is.EqualTo(0));
        }

        [TestCase("Foo", null, "Foo", "")]
        [TestCase(null, "Msg", "", "Msg")]
        public void Errors_should_return_only_empty_strings_for_inner_errors(string innerCode, string innerMsg, string outerCode, string outerMsg)
        {
            var inner = new VehiclesPhotosResult(new Error(innerCode, innerMsg));

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Count, Is.EqualTo(1));
            Assert.That(adapter.Errors.First().Code, Is.EqualTo(outerCode));
            Assert.That(adapter.Errors.First().Message, Is.EqualTo(outerMsg));
        }

        [Test]
        public void Errors_should_skip_over_null_inner_elements()
        {
            var inner = new VehiclesPhotosResult(new Error("A", "A"), null, new Error("B", "B"));

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Select(e => e.Code).ToArray(), Is.EquivalentTo(new[] {"A", "B"}));
        }

        [Test]
        public void Errors_should_skip_over_elements_that_have_null_or_empty_code_and_message()
        {
            var inner = new VehiclesPhotosResult(new Error("A", "A"), new Error(null, null), new Error("", ""),
                                                 new Error("  ", "  "), new Error("B", "B"));

            var adapter = new VehiclePhotosResultAdapter(inner);

            Assert.That(adapter.Errors.Select(e => e.Code).ToArray(), Is.EquivalentTo(new[] {"A", "B"}));
        }
    }
}