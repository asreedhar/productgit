﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Security.Cryptography;

using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using FirstLook.Common.Core.Utilities;

namespace PhotoWebServices
{
    internal class ProxyUrlBuilder
    {
        public ISettings Settings { get; private set; }

        private const string PHOTO_MANAGER_APP_CODE = "IM";
        private const string BULK_UPLOADER_APP_CODE = "BU";

        public ProxyUrlBuilder(ISettings settings)
        {
            Settings = settings;
        }

        internal string ProxyUrl()
        {
            return Settings.ProxyBaseUrl;
        }

        internal string SecretKey()
        {
            return Settings.SecretKey;
        }

        /// <summary>
        /// Build up the photo manager url.
        /// </summary>
        /// <param name="businessUnitCode"></param>
        /// <param name="vin"></param>
        /// <param name="stockNumber"></param>
        /// <param name="userID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal string GetPhotoManagerUrl(string businessUnitCode, string vin, string stockNumber,
            string userID, string firstName, string lastName, PhotoManagerWebContext context)
        {
            if(IsInBusinessUnitWhiteList(businessUnitCode))
            {
                // Get the dictionary of k/v pairs.
                var dict = PhotoManagerDictionary(businessUnitCode, vin, stockNumber, userID, firstName, lastName, context);
                return HashFormatUrl(dict);    
            }
            
            return Settings.PhotoManager_NoAccess_Url;
        }

        /// <summary>
        /// Build up the bulk uploader url.
        /// </summary>
        /// <param name="businessUnitCode"></param>
        /// <param name="userID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal string GetBulkUploadUrl(string businessUnitCode, string userID, string firstName, string lastName, PhotoManagerWebContext context)
        {
            if(IsInBusinessUnitWhiteList(businessUnitCode))
            {
                // Get the dictionary of k/v pairs.
                var dict = BulkUploadDictionary(businessUnitCode, userID, firstName, lastName, context);
                return HashFormatUrl(dict);                
            }

            return Settings.BulkUploadManager_NoAccess_Url;
        }

        private bool IsInBusinessUnitWhiteList(string businessUnitCode)
        {
            return WhiteListHelper.IsInWhiteList(businessUnitCode, Settings.BusinessUnitCode_WhiteList ?? "");
        }

        /// <summary>
        /// Construct the query string with the hash
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        internal string HashFormatUrl(SortedDictionary<string,string> dict)
        {            

            // Convert the dictionary to a sorted string, with KV pairs separated by "&"
            string sortedKeyValuePairs = ConvertDictionaryToString(dict);

            // Append the secret key
            string stringToHash = sortedKeyValuePairs + SecretKey();

            // Calculate the MD5 Checksum of the string
            string hash = CalculateMd5Hash(stringToHash);

            // Add the "SIG" to the dictionary.
            dict.Add("SIG", hash);

            // Get the final query string (includes the SIG).
            string queryString = ConvertDictionaryToString(dict);

            // Get the proxy url.
            var proxy = ProxyUrl();

            return proxy + queryString;
        }

        /// <summary>
        /// Calcualte the MD5 hash of the input string.
        /// </summary>
        /// <param name="input"></param>
        /// <see cref="http://blogs.msdn.com/b/csharpfaq/archive/2006/10/09/how-do-i-calculate-a-md5-hash-from-a-string_3f00_.aspx"/>
        /// <returns></returns>
        public string CalculateMd5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                // Use "X2" if you want ucase alpha
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Construct a sorted dictionary of KV pairs for the photo manager URL.
        /// </summary>
        /// <param name="businessUnitCode"></param>
        /// <param name="vin"></param>
        /// <param name="stockNumber"></param>
        /// <param name="userID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal SortedDictionary<string, string> PhotoManagerDictionary(string businessUnitCode, string vin, string stockNumber,
            string userID, string firstName, string lastName, PhotoManagerWebContext context )
        {
            var keyValuePairs = new SortedDictionary<string, string>
                                                                 {
                                                                     {"APP", PHOTO_MANAGER_APP_CODE.ToUpper()},
                                                                     {"BUC", businessUnitCode.ToUpper()},
                                                                     {"VIN", vin.ToUpper().TrimEnd()},
                                                                     {"SNUM", stockNumber.ToUpper()},
                                                                     {"UID", userID.ToUpper()},
                                                                     {"FNAME", firstName.ToUpper()},
                                                                     {"LNAME", lastName.ToUpper()},
                                                                     {"CTX", ((int) context).ToString()},
                                                                     {"TIME", FormatIso8601DateTime(DateTime.Now)}
                                                                 };

            return keyValuePairs;
        }

        /// <summary>
        /// Construct a sorted dictionary of KV pairs for the bulk uploader URL.
        /// </summary>
        /// <param name="businessUnitCode"></param>
        /// <param name="userID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        internal SortedDictionary<string, string> BulkUploadDictionary(string businessUnitCode, string userID, string firstName, 
            string lastName, PhotoManagerWebContext context)
        {
            var keyValuePairs = new SortedDictionary<string, string>
                                                                 {
                                                                     {"APP", BULK_UPLOADER_APP_CODE.ToUpper()},
                                                                     {"BUC", businessUnitCode.ToUpper()},
                                                                     {"UID", userID.ToUpper()},
                                                                     {"FNAME", firstName.ToUpper()},
                                                                     {"LNAME", lastName.ToUpper()},
                                                                     {"CTX", ((int) context).ToString()},
                                                                     {"TIME", FormatIso8601DateTime(DateTime.Now)}
                                                                 };            

            return keyValuePairs;
        }


        internal string ConvertDictionaryToString( SortedDictionary<string,string> dictionary)
        {
            // build up the string
            var sb = new StringBuilder();
            var enumerator = dictionary.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var kvp = enumerator.Current;
                sb.Append(kvp.Key);
                sb.Append("=");
                sb.Append(kvp.Value);
                sb.Append("&");
            }

            // get rid of the last "&"
            int length = sb.Length;
            sb.Remove(length - 1, 1);

            return sb.ToString();
        }



        internal string FormatIso8601DateTime(DateTime dateTime)
        {
            DateTime utc = dateTime.ToUniversalTime();

            return String.Format("{0}{1}{2}T{3}{4}{5}Z",
                                 ZeroLeftPad(utc.Year, 4),
                                 ZeroLeftPad(utc.Month, 2),
                                 ZeroLeftPad(utc.Day, 2),
                                 ZeroLeftPad(utc.Hour, 2),
                                 ZeroLeftPad(utc.Minute, 2),
                                 ZeroLeftPad(utc.Second, 2));
        }

        internal string ZeroLeftPad(int obj, int totalLength)
        {
            return obj.ToString().PadLeft(totalLength, '0');
        }
    }
}