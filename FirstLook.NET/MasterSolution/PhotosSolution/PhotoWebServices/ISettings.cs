using System.Configuration;

namespace PhotoWebServices
{
    internal interface ISettings
    {
        string ProxyBaseUrl { get; }

        string SecretKey { get; }

        string ImageHostingBaseUrl { get; }

        string PhotoQueryBaseUrl { get; }

        string PhotoQueryUserName { get; }

        string PhotoQueryPassword { get; }

        string PhotoManager_NoAccess_Url { get; }

        string BulkUploadManager_NoAccess_Url { get; }

        string BusinessUnitCode_WhiteList { get; }

        int PhotoQueryTimeoutOverride { get; }
    }
}