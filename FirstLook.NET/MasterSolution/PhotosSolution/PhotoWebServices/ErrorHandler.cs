﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using FirstLook.Common.Core.Logging;

namespace PhotoWebServices
{
    public class ErrorHandler : IErrorHandler
    {
        private Type ServiceType { get; set; }
        private readonly ILog Log = LoggerFactory.GetLogger<ErrorHandler>();

        public ErrorHandler(Type serviceType)
        {
            ServiceType = serviceType;
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
        }

        public bool HandleError(Exception error)
        {
            Log.Error(
                string.Format("Unhandled exception in service type {0}", ServiceType.FullName), error);
            return false;
        }
    }
}