﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("PhotoWebServices")]
[assembly: AssemblyProduct("PhotoWebServices")]

[assembly: InternalsVisibleTo("PhotoWebServices_Tests")]
[assembly: InternalsVisibleTo("PhotoWebServices_IntegrationTests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]