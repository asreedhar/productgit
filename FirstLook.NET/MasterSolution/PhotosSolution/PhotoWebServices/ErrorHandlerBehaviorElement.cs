using System;
using System.ServiceModel.Configuration;

namespace PhotoWebServices
{
    public class ErrorHandlerBehaviorElement : BehaviorExtensionElement
    {
        protected override object CreateBehavior()
        {
            return new ErrorHandlerBehavior();
        }

        public override Type BehaviorType
        {
            get { return typeof (ErrorHandlerBehavior); }
        }
    }
}