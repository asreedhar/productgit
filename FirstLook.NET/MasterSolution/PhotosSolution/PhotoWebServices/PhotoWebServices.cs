﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using FirstLook.Common.Core.PhotoServices.WebService.V1;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using log4net;
using MAX.Caching;
using MAX.Entities.Helpers.PhotoServicesHelpers;

namespace PhotoWebServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(Namespace = Constants.PhotoWebServices_V1_Namespace)]
    internal class PhotoWebServices : IPhotoWebServices
    {
        #region Properties
        public ISettings Settings { get; private set; }
        public ProxyUrlBuilder Builder { get; private set; }
        public ICacheWrapper CacheWrapper { get; private set; }
        public ICacheKeyBuilder CacheKeyBuilder { get; private set; }

        private const int CacheMinutes = 15;
        private const int ThumbnailWidth = 225;
        private const int ThumbnailHeight = 169;

        private readonly Boolean _fakeAulTechNullReturn = Boolean.Parse(ConfigurationManager.AppSettings["FakeAULTechNullReturn"] ?? "False");

        private Helper _serviceHelper;
        public Helper ServiceHelper
        {
            get { return _serviceHelper ?? (_serviceHelper = new Helper()); }
        }

        #region Logging
        private static readonly ILog Log = LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        #endregion

        #region Methods
        #region Construction Methods
        public PhotoWebServices(ISettings settings, ProxyUrlBuilder builder, ICacheWrapper cacheWrapper, ICacheKeyBuilder cacheKeyBuilder)
        {
            Settings = settings;
            Builder = builder;
            CacheWrapper = cacheWrapper;
            CacheKeyBuilder = cacheKeyBuilder;
        }
        #endregion

        #region WebService Methods
        public string GetPhotoManagerUrl(string username, string userFirstName, string userLastName, string businessUnitCode, string vin, string stockNumber, 
                                         PhotoManagerWebContext context)
        {
            Log.InfoFormat(
                "Started GetPhotoManagerUrl(username: '{0}', userFirstName: '{1}', userLastname: '{2}' businessUnitCode: '{3}', vin: '{4}', stockNumber: '{5}', context: '{6}'",
                username, userFirstName, userLastName,
                businessUnitCode, vin, stockNumber, context);

            var url = Builder.GetPhotoManagerUrl(businessUnitCode, vin, stockNumber, username, userFirstName, userLastName, context);

            Log.InfoFormat("Completed GetPhotoManagerUrl. Returned '{0}'", url);
            return url;
        }

        public string GetBulkUploadManagerUrl(string username, string userFirstname, string userLastName, string businessUnitCode, PhotoManagerWebContext context)
        {
            Log.InfoFormat(
                "Started GetBulkUploadManagerUrl(username: '{0}', userFirstName: '{1}', userLastname: '{2}' businessUnitCode: '{3}', context: '{4}'",
                username, userFirstname, userLastName, businessUnitCode, context);

            var url = Builder.GetBulkUploadUrl(businessUnitCode, username, userFirstname, userLastName, context);

            Log.InfoFormat("Completed GetBulkUploadManagerUrl. Returned '{0}'", url);
            return url;
        }

        public VehiclesPhotosResult GetPhotoUrlsByVin(string businessUnitCode, string vin, int timeoutMilliseconds)
        {
            Log.InfoFormat("Started GetPhotoUrlsByVin(businessUnitCode: '{0}', vin: '{1}', timeoutMilliseconds: {2})", businessUnitCode, vin, timeoutMilliseconds);
            var startTime = DateTime.UtcNow;

            var url = String.Format(PhotoQueryBaseUrl() + "{0}/vinPhotos/{1}", businessUnitCode, vin);
            Log.InfoFormat("GetPhotoUrlsByVin: Endpoint: '{0}'", url);

            var results = ProcessRequest(url, businessUnitCode, vin, timeoutMilliseconds);

            Log.InfoFormat("Completed GetPhotoUrlsByVin(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }

        public VehiclesPhotosResult GetPhotoUrlsByBusinessUnit(string businessUnitCode, int timeoutMilliseconds)
        {
            Log.InfoFormat("Started GetPhotoUrlsByBusinessUnit(businessUnitCode: '{0}', timeoutMilliseconds: {1})", businessUnitCode, timeoutMilliseconds);
            var startTime = DateTime.UtcNow;

            var url = String.Format(PhotoQueryBaseUrl() + "{0}/vinPhotos", businessUnitCode);
            Log.InfoFormat("GetPhotoUrlsByBusinessUnit: Endpoint: '{0}'", url);

            var results = ProcessRequest(url, businessUnitCode, String.Empty, timeoutMilliseconds, true);

            Log.InfoFormat("Completed GetPhotoUrlsByBusinessUnit(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }

        public Dictionary<String, int> GetVinPhotoCountsForBusinessUnit(String businessUnitCode, int timeoutMilliseconds)
        {
            Log.InfoFormat("Started GetVinPhotoCountsForBusinessUnit(businessUnitCode: \"{0}\", timeoutMilliseconds: {1}", businessUnitCode, timeoutMilliseconds);
            var startTime = DateTime.UtcNow;

            var methodName = MethodBase.GetCurrentMethod().Name;
            var cacheKey = CacheKeyBuilder.CacheKey(new[] { methodName, businessUnitCode.ToString(CultureInfo.InvariantCulture) });

            // Check cache.
            var results = CacheWrapper.ReadThroughCache(cacheKey, () =>
            {
                Log.InfoFormat("Vin Photo Counts For BusinessUnit {0} not found in cache", businessUnitCode);

                // set up the URL to use to get photo URLs
                var url = String.Format(PhotoQueryBaseUrl() + "{0}/vinPhotos", businessUnitCode);
                Log.InfoFormat("Photo URL: '{0}'", url);

                // process url and get xml
                var xml = ProcessWebRequest(url, businessUnitCode, timeoutMilliseconds);

                // parse xml into a dictionary
                var photoCounts = ServiceHelper.ParseVinCounts(xml);

                if (photoCounts.Count == 0)
                {
                    throw new ApplicationException(
                        String.Format(
                            "GetVinPhotoCountsForBusinessUnit() Failed: No VINs were returned. timeElapsed: {0}",
                            DateTime.UtcNow.Subtract(startTime)));
                }

                return photoCounts;
            }, DateTime.Now.AddMinutes(CacheMinutes));

            Log.InfoFormat("Completed GetVinPhotoCountsForBusinessUnit(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }

        public string GetMainPhotoUrl(string businessUnitCode, string vin)
        {
            Log.InfoFormat("Started GetMainPhotoUrl(businessUnitCode: '{0}', vin: '{1}')",
                            businessUnitCode, vin);

            // e.g.    https://incisent.aultec.net/photos/test001/abcdefghijklmnopq/main.jpg
            var url = ImageHostingBaseUrl() + "{0}/{1}/main.jpg";
            var formattedUrl = string.Format(url, businessUnitCode, vin);

            Log.InfoFormat("Completed GetMainPhotoUrl. Returned '{0}'", formattedUrl);
            return formattedUrl;
        }

        public ThumbnailResult GetMainThumbnailUrl(string businessUnitCode, string vin)
        {
            Log.InfoFormat("Started GetMainThumbnailUrl(businessUnitCode: '{0}', vin: '{1}')",
                            businessUnitCode, vin);

            // e.g. https://incisent.aultec.net/photos/test001/abcdefghijklmnopq/thumb.jpg  
            var url = ImageHostingBaseUrl() + "{0}/{1}/thumb.jpg";
            var formattedUrl = string.Format(url, businessUnitCode, vin);
            var result = new ThumbnailResult
                                       {
                                           Width = ThumbnailWidth,
                                           Height = ThumbnailHeight,
                                           Url = formattedUrl
                                       };

            Log.InfoFormat("Completed GetMainThumbnailUrl. Returned Url: '{0}', Width: {1}, Height: {2}",
                result.Url, result.Width, result.Height);

            return result;
        }

        public string Echo(string input)
        {
            Log.InfoFormat("Started Echo(input: '{0}')", input);
            Log.InfoFormat("Completed Echo. Returned '{0}'", input);

            return input;
        }

        public Settings GetSettings()
        {
            Log.Info("Started GetSettings()");

            var settings = new Settings
                               {
                                   ImageHostingBaseUrl = Settings.ImageHostingBaseUrl,
                                   ThumbnailWidth = ThumbnailWidth,
                                   ThumbnailHeight = ThumbnailHeight
                               };

            Log.InfoFormat(
                "Completed GetSettings. Returned ImageHostingBaseUrl: '{0}', ThumbnailWidth: {1}, ThumbnailHeight: {2}",
                settings.ImageHostingBaseUrl, settings.ThumbnailWidth, settings.ThumbnailHeight);
            return settings;
        }
        #endregion

        #region Helper Methods
        internal VehiclesPhotosResult ProcessRequest(string url, string businessUnitCode, string vin, int timeoutMilliseconds, bool writeToS3 = false)
        {
            var effectiveTimeoutMilliseconds = CalcEffectiveTimeoutMilliseconds(timeoutMilliseconds);

            Log.InfoFormat(
                "Started ProcessRequest(url: '{0}', businessUnitCode: '{1}', vin: '{2}', timeoutMilliseconds: {3}), effectiveTimeoutMilliseconds: {4}",
                url, businessUnitCode, vin, timeoutMilliseconds, effectiveTimeoutMilliseconds);
            var startTime = DateTime.UtcNow;
            
            // Create the web request              
            var request = WebRequest.Create(url) as HttpWebRequest;

            if( request == null )
            {
                var message = String.Format("The url '{0}' resulted in a null HttpWebRequest. timeElapsed: {1}", url,
                    DateTime.UtcNow.Subtract(startTime));
                Log.Error(message);

                var error = new Error { Code = string.Empty, Message = message };
                //return ServiceHelper.LoadSavedVehiclesPhotosResult(_fileStoreFactory.CreateFileStore(), businessUnitCode, vin, new[] { error });
                return new VehiclesPhotosResult(new[] {error});
            }

            request.Timeout = effectiveTimeoutMilliseconds;

            // Add authentication to request
            var usernameAndPassword = Encoding.ASCII.GetBytes(PhotoQueryUserName() + ":" + PhotoQueryPassword());
            var basicAuth = "Basic " + Convert.ToBase64String(usernameAndPassword);
            request.Headers.Add(HttpRequestHeader.Authorization, basicAuth);

            HttpWebResponse response = null;
            StreamReader reader = null;
            VehiclesPhotosResult result = null;

            try
            {
#if DEBUG
                if (_fakeAulTechNullReturn)
                    throw new WebException("Forced error", WebExceptionStatus.RequestCanceled);
#endif

                // Get response  
                response = request.GetResponse() as HttpWebResponse;
                
                if( response == null )
                    throw new WebException("The response is not an HttpWebResponse.", WebExceptionStatus.UnknownError);
 
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Log.InfoFormat("200 OK received from AULTec Query API. Request url was '{0}'. timeElapsed: {1}", url,
                        DateTime.UtcNow.Subtract(startTime));

                    // Get the response stream  
                    var responseStream = response.GetResponseStream();
                    if(responseStream == null)
                        throw new WebException("Failed to get response stream.");

                    reader = new StreamReader(responseStream);

                    // Strip out any formatting characters
                    var responseText = reader.ReadToEnd();

                    // TODO: Remove this?
                    responseText = responseText.Replace("\n", "").Replace("\r", "").Replace("\t", "");

                    // Parse the xml and return
                    result = ServiceHelper.Parse(responseText);

                    //// save the results to S3
                    //if (result != null && writeToS3)
                    //    ServiceHelper.SaveVehiclesPhotosResult(_fileStoreFactory.CreateFileStore(), businessUnitCode, responseText);
                }
                else
                {
                    // All other response codes are invalid.
                    Log.ErrorFormat("Invalid response code '{0}' received from AULTec Query API.", response.StatusCode);
                    throw new WebException("Invalid reponse code.", WebExceptionStatus.ProtocolError);                    
                }
                
            }
            catch (WebException wex)
            {
                var wexResponse = wex.Response as HttpWebResponse;
                String message;

                if (wexResponse != null)
                {
                    // 404 - means "dealer not found" or "vehicle not found"
                    const string HINT = "404 received from AULTec Query API. By convention, this means dealer or vehicle not found. ";
                    var hint = wexResponse.StatusCode == HttpStatusCode.NotFound ? HINT : string.Empty;

                    message = String.Format("{2}Message is '{0}'. Request url was '{1}'. timeElapsed: {3}",
                        wexResponse.StatusDescription, url, hint, DateTime.UtcNow.Subtract(startTime));

                    var error = new Error { Code = wexResponse.StatusCode.ToString(), Message = message };
                    result = new VehiclesPhotosResult(new[] { error });
                }
                else
                {
                    message = String.Format(
                        "Null response received from AULTec Query API.  Request url was '{0}' Error '{1}'. timeElapsed: {2}",
                        url, wex.Message, DateTime.UtcNow.Subtract(startTime));

                    var error = new Error { Code = string.Empty, Message = message };
                    result = new VehiclesPhotosResult(new[] { error });
                }
                // Log it
                Log.Error(message, wex);
            }
            catch(Exception ex)
            {
                var message = string.Format("Failed in ProcessRequest(...). Request url was '{0}'. timeElapsed: {1}", url, DateTime.UtcNow.Subtract(startTime));
                Log.Error(message, ex);
                var error = new Error {Code = ex.GetType().Name, Message = message};
                result = new VehiclesPhotosResult(new[] { error });
            }
            finally
            {
                if( response != null ) response.Close();
                if( reader != null ) reader.Close();
            }

            if (result == null) result = new VehiclesPhotosResult();
            if(result.Errors == null) result.Errors = new Error[0];
            if(result.Vehicles == null) result.Vehicles = new VehiclePhotos[0];

            Log.InfoFormat(
                "Completed ProcessRequest. Results: vehicleCount: {0}, photoCount: {1}, errorCount: {2}, timeElapsed: {3}",
                result.Vehicles.Count(), result.Vehicles.Sum(v => v.PhotoUrls.Length), result.Errors.Count(),
                DateTime.UtcNow.Subtract(startTime));

            return result;
        }

        internal String ProcessWebRequest(string url, string businessUnitCode, int timeoutMilliseconds)
        {
            var effectiveTimeoutMilliseconds = CalcEffectiveTimeoutMilliseconds(timeoutMilliseconds);
            Log.InfoFormat("Started ProcessWebRequest(url: '{0}', businessUnitCode: '{1}', timeoutMilliseconds: {2}), effectiveTimeoutMilliseconds: {3}",
                url, businessUnitCode, timeoutMilliseconds, effectiveTimeoutMilliseconds);
            var startTime = DateTime.UtcNow;

            var results = String.Empty;
            try
            {
                // create the web request
                var request = WebRequest.Create(url) as HttpWebRequest;

                if (request == null)
                {
                    Log.ErrorFormat("The url '{0}' resulted in a null HttpWebRequest. timeElapsed: {1}", url,
                        DateTime.UtcNow.Subtract(startTime));

                    return string.Empty;
                }

                request.Timeout = effectiveTimeoutMilliseconds;

                // Add authentication to request
                var usernameAndPassword = Encoding.ASCII.GetBytes(PhotoQueryUserName() + ":" + PhotoQueryPassword());
                var basicAuth = "Basic " + Convert.ToBase64String(usernameAndPassword);
                request.Headers.Add(HttpRequestHeader.Authorization, basicAuth);

                // get response
                var response = request.GetResponse() as HttpWebResponse;

                if (response == null)
                    throw new WebException("The response is not an HttpWebResponse.", WebExceptionStatus.UnknownError);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Log.InfoFormat("200 OK received from AULTec Query API. Request url was '{0}'. timeElapsed: {1}", url,
                        DateTime.UtcNow.Subtract(startTime));

                    // Get the response stream  
                    var responseStream = response.GetResponseStream();
                    if (responseStream == null)
                        throw new WebException("Failed to get response stream.");

                    using (var reader = new StreamReader(responseStream))
                        results = reader.ReadToEnd();

                    // Strip out any formatting characters
                    results = results.Replace("\n", "").Replace("\r", "").Replace("\t", "");
                }
                else
                {
                    // All other response codes are invalid.
                    Log.ErrorFormat("Invalid response code '{0}' received from AULTec Query API.", response.StatusCode);
                    throw new WebException("Invalid reponse code.", WebExceptionStatus.ProtocolError);
                }
            }
            catch (WebException wex)
            {
                var wexResponse = wex.Response as HttpWebResponse;

                if (wexResponse != null)
                {
                    // 404 - means "dealer not found" or "vehicle not found"
                    const string HINT = "404 received from AULTec Query API. By convention, this means dealer or vehicle not found. ";
                    var hint = wexResponse.StatusCode == HttpStatusCode.NotFound ? HINT : string.Empty;

                    Log.Error(String.Format("{2}Message is '{0}'. Request url was '{1}'. timeElapsed: {3}",
                        wexResponse.StatusDescription, url, hint, DateTime.UtcNow.Subtract(startTime)), wex);
                }
                else
                {
                    Log.Error(String.Format(
                        "Null response received from AULTec Query API.  Request url was '{0}' Error '{1}'. timeElapsed: {2}",
                        url, wex.Message, DateTime.UtcNow.Subtract(startTime)), wex);
                }
            }
            catch (Exception ex)
            {
                Log.Error(
                    String.Format("Failed in ProcessRequest(...). Request url was '{0}'. timeElapsed: {1}", url,
                        DateTime.UtcNow.Subtract(startTime)), ex);
            }

            Log.InfoFormat("Completed ProcessWebRequest. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));

            return results;
        }

        internal int CalcEffectiveTimeoutMilliseconds(int timeoutMilliseconds)
        {
            return Settings.PhotoQueryTimeoutOverride > 0 
                ? Settings.PhotoQueryTimeoutOverride 
                : Math.Max(timeoutMilliseconds, 0);
        }

        private string ImageHostingBaseUrl()
        {
            return Settings.ImageHostingBaseUrl;
        }

        internal string PhotoQueryBaseUrl()
        {
            return Settings.PhotoQueryBaseUrl;
        }

        private string PhotoQueryUserName()
        {
            return Settings.PhotoQueryUserName;
        }

        private string PhotoQueryPassword()
        {
            return Settings.PhotoQueryPassword;
        }
        #endregion
        #endregion
    }
}