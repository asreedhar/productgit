﻿using System;
using System.Web;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Logging;
using Merchandising.Messages;
using PhotoWebServices.Properties;
using MAX.Caching;

namespace PhotoWebServices
{
    public class PhotoWebServicesApp : HttpApplication
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<PhotoWebServicesApp>();

        static PhotoWebServicesApp()
        {
            SetupLogging();
            AppDomain.CurrentDomain.UnhandledException += LogAppDomainUnhandledException;
        }

        private static void LogAppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.IsTerminating)
                Log.Fatal("Unhandled exception. Application is terminating.", e.ExceptionObject as Exception);
            else
                Log.Error("Unhandled exception.", e.ExceptionObject as Exception);
        }

        protected void Application_Start()
        {
            Log.InfoFormat("Starting Application");
            SetupIOC();
        }

        protected void Application_End()
        {
            Log.InfoFormat("Ending Application");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            Log.Error("Unhandled exception.", exception);
        }

        private static void SetupLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        private static void SetupIOC()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CoreModule());
            builder.Register(c => Settings.Default).As<ISettings>();
            builder.RegisterType<PhotoWebServices>();
            builder.RegisterType<ProxyUrlBuilder>();

            builder.RegisterType<MemcachedClientWrapper>().As<ICacheWrapper>().SingleInstance();
            builder.RegisterType<CacheKeyBuilder>().As<ICacheKeyBuilder>();

            builder.RegisterModule(new MerchandisingMessagesModule());

            var container = builder.Build();

            Autofac.Integration.Wcf.AutofacHostFactory.Container = container;
            Registry.RegisterContainer(container);
        }
    }
}