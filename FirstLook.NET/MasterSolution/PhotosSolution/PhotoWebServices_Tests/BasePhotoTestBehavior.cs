﻿using NUnit.Framework;

namespace PhotoWebServicesTests
{
    public class BasePhotoTestBehavior
    {
        protected const string BUSINESS_UNIT_CODE = "WINDYCIT05";
        protected const string USER_ID = "zbrown";
        protected const string FIRST_NAME = "zac";
        protected const string LAST_NAME = "brown";
        protected const string VIN = "WBAPK7C53BA000013";
        protected const string SNUM = "A772205";
    }
}