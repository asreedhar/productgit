﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using Moq;
using NUnit.Framework;
using PhotoWebServices;

namespace PhotoWebServicesTests
{
    [TestFixture]
    public class ProxyUrlBuilderTests : BasePhotoTestBehavior
    {
        private ProxyUrlBuilder _builder;
        private Mock<ISettings> MockSettings;
/*
        const string BUSINESS_UNIT_CODE = "WINDYCIT05";
        const string USER_ID = "zbrown";
        const string FIRST_NAME = "zac";
        const string LAST_NAME = "brown";
        const string VIN = "WBAPK7C53BA000013";
        const string SNUM = "A772205";
*/

        const PhotoManagerWebContext CONTEXT = PhotoManagerWebContext.MAX;

        [SetUp]
        public void Setup()
        {
            MockSettings = new Mock<ISettings>();

            _builder = new ProxyUrlBuilder(MockSettings.Object);
        }


        [Test]
        public void CanConstruct()
        {
            Assert.IsNotNull(_builder);
        }

        [Test]
        public void CanGetProxyBase()
        {
            MockSettings.Setup(s => s.ProxyBaseUrl).Returns("http://foo");

            Console.WriteLine(_builder.ProxyUrl());
            Assert.IsNotNullOrEmpty( _builder.ProxyUrl() );
        }

        [Test]
        public void ZeroLeftPad()
        {
            Assert.IsTrue(string.Equals("01", _builder.ZeroLeftPad(1, 2)));
            Assert.IsTrue(string.Equals("0001", _builder.ZeroLeftPad(1, 4)));            
            Assert.IsTrue(string.Equals("11", _builder.ZeroLeftPad(11, 2)));
        }

        [Test]
        public void GetIso8601DateTime()
        {
            DateTime now = DateTime.UtcNow;

            string time = _builder.FormatIso8601DateTime(now);
            Console.WriteLine(time);
            Assert.IsTrue(time.Length == 16);

            Assert.IsTrue(string.Equals(time.Substring(0, 4), _builder.ZeroLeftPad(now.Year,4)));
            Assert.IsTrue(string.Equals(time.Substring(4, 2), _builder.ZeroLeftPad(now.Month,2)));
            Assert.IsTrue(string.Equals(time.Substring(6, 2), _builder.ZeroLeftPad(now.Day,2)));

            Assert.IsTrue(string.Equals(time.Substring(8, 1), "T"));
            Assert.IsTrue(string.Equals(time.Substring(9, 2), _builder.ZeroLeftPad(now.Hour,2)));
            Assert.IsTrue(string.Equals(time.Substring(11, 2), _builder.ZeroLeftPad(now.Minute,2)));
            Assert.IsTrue(string.Equals(time.Substring(13, 2), _builder.ZeroLeftPad(now.Second,2)));
            Assert.IsTrue(string.Equals(time.Substring(15, 1), "Z"));
        }

        [Test]
        public void CalculateMd5Hash()
        {
            string hash = _builder.CalculateMd5Hash("ZAC");
            Console.WriteLine(hash);
            Assert.IsTrue(String.Equals("f4c4f397d6e848fea8cd63d50f326296", hash, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void SecretKey()
        {
            MockSettings.Setup(s => s.SecretKey).Returns("fdbc0538-8200-435f-b148-7cd8a3ec6618");

            const string KEY = "fdbc0538-8200-435f-b148-7cd8a3ec6618";
            Assert.IsTrue(string.Equals(KEY, _builder.SecretKey()));
        }

        [Test]
        public void ConvertDictionaryToString()
        {
            SortedDictionary<string,string> dictionary = new SortedDictionary<string, string>();
            dictionary.Add("A", "B");
            dictionary.Add("C", "D");
            string dict = _builder.ConvertDictionaryToString(dictionary);

            Assert.IsTrue(String.Equals("A=B&C=D", dict));

        }

        [Test]
        public void BulkUploadDictionary()
        {
            var dictionary = _builder.BulkUploadDictionary(BUSINESS_UNIT_CODE, USER_ID, FIRST_NAME, LAST_NAME, CONTEXT);

            Assert.AreEqual(BUSINESS_UNIT_CODE, dictionary["BUC"]);
            Assert.AreEqual(USER_ID.ToUpper(), dictionary["UID"]);
            Assert.AreEqual(FIRST_NAME.ToUpper(), dictionary["FNAME"]);
            Assert.AreEqual(LAST_NAME.ToUpper(), dictionary["LNAME"]);
            Assert.AreEqual(CONTEXT, Enum.Parse(typeof(PhotoManagerWebContext), dictionary["CTX"]));

            var time = dictionary["TIME"];
            Assert.AreEqual(16, time.Length);
            Assert.AreEqual('T', time[8]);
            Assert.AreEqual('Z', time[15]);            
        }

        [Test]
        public void PhotoManagerDictionary()
        {
            var dictionary = _builder.PhotoManagerDictionary(BUSINESS_UNIT_CODE, VIN,SNUM,USER_ID, FIRST_NAME, LAST_NAME, CONTEXT);

            Assert.AreEqual(BUSINESS_UNIT_CODE, dictionary["BUC"]);
            Assert.AreEqual(USER_ID.ToUpper(), dictionary["UID"]);
            Assert.AreEqual(FIRST_NAME.ToUpper(), dictionary["FNAME"]);
            Assert.AreEqual(LAST_NAME.ToUpper(), dictionary["LNAME"]);
            Assert.AreEqual(VIN.ToUpper(), dictionary["VIN"]);
            Assert.AreEqual(SNUM.ToUpper(), dictionary["SNUM"]);
            Assert.AreEqual(CONTEXT, Enum.Parse(typeof(PhotoManagerWebContext), dictionary["CTX"]));

            var time = dictionary["TIME"];
            Assert.AreEqual(16, time.Length);
            Assert.AreEqual('T', time[8]);
            Assert.AreEqual('Z', time[15]);
        }

        [Test]
        public void HashFormatUrl()
        {
            MockSettings.Setup(s => s.ProxyBaseUrl).Returns("https://incisent.aultec.net/?");
            MockSettings.Setup(s => s.SecretKey).Returns("fdbc0538-8200-435f-b148-7cd8a3ec6618");

            const string EXPECT = "https://incisent.aultec.net/?A=1&B=2&SIG=3B8E355729528046B1AD3B32331446CB";

            SortedDictionary<string,string> dict = new SortedDictionary<string, string>();
            dict.Add("A", "1");
            dict.Add("B", "2");

            var result = _builder.HashFormatUrl(dict);

            Assert.AreEqual(EXPECT, result);
        }

        [Test]
        public void GetBulkUploadUrl()
        {
            var url = _builder.GetBulkUploadUrl(BUSINESS_UNIT_CODE, USER_ID, FIRST_NAME, LAST_NAME, CONTEXT);
            Console.WriteLine(url);
        }

        [TestCase("WINDYCIT05")]
        [TestCase("  WINDYCIT05  ")]
        [TestCase(",WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        public void GetBulkUploadUrl_should_return_no_access_url_when_business_unit_code_is_not_in_white_list(string whiteList)
        {
            MockSettings.Setup(s => s.BusinessUnitCode_WhiteList).Returns(whiteList);
            MockSettings.Setup(s => s.BulkUploadManager_NoAccess_Url).Returns("http://foo");

            var url = _builder.GetBulkUploadUrl("ANOTHER_BUSINESS_UNIT_CODE", "PSTEPHENS", "Peter",
                                        "Stephens", PhotoManagerWebContext.MAX);

            Assert.That(url, Is.StringStarting("http://foo"));
        }

        [Test]
        public void GetBulkUploadUrl_should_return_no_access_url_when_white_list_is_null()
        {
            MockSettings.Setup(s => s.BusinessUnitCode_WhiteList).Returns((string)null);
            MockSettings.Setup(s => s.BulkUploadManager_NoAccess_Url).Returns("http://foo");

            var url = _builder.GetBulkUploadUrl("ANOTHER_BUSINESS_UNIT_CODE", "PSTEPHENS", "Peter",
                                        "Stephens", PhotoManagerWebContext.MAX);

            Assert.That(url, Is.StringStarting("http://foo"));
        }

        [TestCase("WINDYCIT05")]
        [TestCase("  WINDYCIT05  ")]
        [TestCase(",WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        [TestCase("ALL")]
        public void GetBulkUploadUrl_with_businessUnitCode_in_white_list_should_return_url_starting_with_proxyBaseUrl(string whiteList)
        {
            MockSettings.Setup(s => s.BusinessUnitCode_WhiteList).Returns(whiteList);
            MockSettings.Setup(s => s.ProxyBaseUrl).Returns("http://photo-manager/xyz");

            var url = _builder.GetBulkUploadUrl("WINDYCIT05", "PSTEPHENS", "Peter",
                                        "Stephens", PhotoManagerWebContext.MAX);

            Assert.That(url, Is.StringStarting("http://photo-manager/xyz"));
        }

        [Test]
        public void GetPhotoManagerUrl()
        {
            var url = _builder.GetPhotoManagerUrl(BUSINESS_UNIT_CODE, VIN, SNUM, USER_ID, FIRST_NAME, LAST_NAME, CONTEXT);
            Console.WriteLine( url );
        }

        [TestCase("WINDYCIT05")]
        [TestCase("  WINDYCIT05  ")]
        [TestCase(",WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        public void GetPhotoManagerUrl_should_return_no_access_url_when_business_unit_code_is_not_in_white_list(string whiteList)
        {
            MockSettings.Setup(s => s.BusinessUnitCode_WhiteList).Returns(whiteList);
            MockSettings.Setup(s => s.PhotoManager_NoAccess_Url).Returns("http://foo");

            var url = _builder.GetPhotoManagerUrl("ANOTHER_BUSINESS_UNIT_CODE", "V1234", "STK1234", "PSTEPHENS", "Peter",
                                        "Stephens", PhotoManagerWebContext.MAX);

            Assert.That(url, Is.StringStarting("http://foo"));
        }

        [Test]
        public void GetPhotoManager_should_return_no_access_url_when_white_list_is_null()
        {
            MockSettings.Setup(s => s.BusinessUnitCode_WhiteList).Returns((string)null);
            MockSettings.Setup(s => s.PhotoManager_NoAccess_Url).Returns("http://foo");

            var url = _builder.GetPhotoManagerUrl("ANOTHER_BUSINESS_UNIT_CODE", "V1234", "STK1234", "PSTEPHENS", "Peter",
                                        "Stephens", PhotoManagerWebContext.MAX);

            Assert.That(url, Is.StringStarting("http://foo"));
        }

        [TestCase("WINDYCIT05")]
        [TestCase("  WINDYCIT05  ")]
        [TestCase(",WINDYCIT05")]
        [TestCase("WINDYCIT01,WINDYCIT05")]
        [TestCase("ALL")]
        public void GetPhotoManagerUrl_with_businessUnitCode_in_white_list_should_return_url_starting_with_proxyBaseUrl(string whiteList)
        {
            MockSettings.Setup(s => s.BusinessUnitCode_WhiteList).Returns(whiteList);
            MockSettings.Setup(s => s.ProxyBaseUrl).Returns("http://photo-manager/xyz");

            var url = _builder.GetPhotoManagerUrl("WINDYCIT05", "V1234", "STK1234", "PSTEPHENS", "Peter",
                                        "Stephens", PhotoManagerWebContext.MAX);

            Assert.That(url, Is.StringStarting("http://photo-manager/xyz"));
        }
    }
}
