﻿using System.Management.Automation;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.PhotoServices;
using Merchandising.Messages;

namespace PhotoServices_IntegrationTest.Dependencies
{
    public static class PhotoServicesFactory
    {
        public static IPhotoServices GetPhotoServices(IPhotoServicesSettings settings, Cmdlet cmdlet)
        {
            settings = settings ?? new PhotoServicesSettings();

            var service = new PhotoServices(
                ChannelServices.GetChannelFactory(settings),
                new LoggerFactory(cmdlet),
                settings,
                new SimpleCache());
            return service;
        }
    }
}