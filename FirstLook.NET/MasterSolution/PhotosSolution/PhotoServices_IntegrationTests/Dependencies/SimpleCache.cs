using System;
using System.Runtime.Caching;
using FirstLook.Common.Core;

namespace PhotoServices_IntegrationTest.Dependencies
{
    internal class SimpleCache : ICache
    {
        private readonly MemoryCache Cache;

        public SimpleCache()
        {
            Cache = MemoryCache.Default;
        }

        public void Set(string key, object value, int? secondsToExpire)
        {
            var policy = new CacheItemPolicy();
            if(secondsToExpire.HasValue)
            {
                policy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(secondsToExpire.Value);
            }
            Cache.Set(key, value, policy);
        }

        public void Set(string key, object value)
        {
            Cache[key] = value;
        }

        public void Set(string key, object value, TimeSpan sliding)
        {
            var policy = new CacheItemPolicy {SlidingExpiration = sliding};
            Cache.Set(key, value, policy);
        }

        public object Get(string key)
        {
            return Cache.Get(key);
        }

        public object Delete(string key)
        {
            return Cache.Remove(key);
        }

        public void Clear()
        {
            Cache.Trim(100);
        }
    }
}