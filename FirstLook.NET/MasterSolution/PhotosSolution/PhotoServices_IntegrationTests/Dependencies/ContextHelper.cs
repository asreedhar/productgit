﻿using System;
using FirstLook.Common.Core.PhotoServices;

namespace PhotoServices_IntegrationTest.Dependencies
{
    public static class ContextHelper
    {
         public static PhotoManagerContext Calculate(Model.PhotoManagerContext ctx)
         {
             switch(ctx)
             {
                 case Model.PhotoManagerContext.IMS:
                     return PhotoManagerContext.IMS;
                 case Model.PhotoManagerContext.MAX:
                     return PhotoManagerContext.MAX;
                 default:
                     throw new ArgumentException("Invalid context value.", "ctx");
             }
         }
    }
}