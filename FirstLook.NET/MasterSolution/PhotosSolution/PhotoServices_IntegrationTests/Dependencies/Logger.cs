using System;
using System.Management.Automation;
using FirstLook.Common.Core.Logging;

namespace PhotoServices_IntegrationTest.Dependencies
{
    public class Logger : ILog
    {
        private Cmdlet Inner { get; set; }
        public string Name { get; set; }

        public Logger(Cmdlet inner, string name)
        {
            Inner = inner;
            Name = name;
        }

        public bool IsDebugEnabled
        {
            get { return true; }
        }

        public bool IsInfoEnabled
        {
            get { return true; }
        }

        public bool IsWarnEnabled
        {
            get { return true; }
        }

        public bool IsErrorEnabled
        {
            get { return true; }
        }

        public bool IsFatalEnabled
        {
            get { return true; }
        }

        public void Debug(object message)
        {
            Inner.WriteDebug("DEBUG: " + Name + ": " + (message ?? ""));
        }

        public void Debug(object message, Exception exception)
        {
            var msg = message ?? "";
            var stack = exception != null ? exception.StackTrace : "";
            Debug(msg + "\n" + stack);
        }

        public void DebugFormat(string format, params object[] args)
        {
            Debug(string.Format(format, args));
        }

        public void DebugFormat(string format, object arg0)
        {
            Debug(string.Format(format, arg0));
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            Debug(string.Format(format, arg0, arg1));
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            Debug(string.Format(format, arg0, arg1, arg2));
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            Debug(string.Format(provider, format, args));
        }

        public void Info(object message)
        {
            Inner.WriteVerbose("INFO:  " + Name + ": " + (message ?? ""));
        }

        public void Info(object message, Exception exception)
        {
            var msg = message ?? "";
            var stack = exception != null ? exception.StackTrace : "";
            Info(msg + "\n" + stack);
        }

        public void InfoFormat(string format, params object[] args)
        {
            Info(string.Format(format, args));
        }

        public void InfoFormat(string format, object arg0)
        {
            Info(string.Format(format, arg0));
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            Info(string.Format(format, arg0, arg1));
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            Info(string.Format(format, arg0, arg1, arg2));
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            Info(string.Format(provider, format, args));
        }

        public void Warn(object message)
        {
            Inner.WriteWarning("WARN:  " + Name + ": " + (message ?? ""));
        }

        public void Warn(object message, Exception exception)
        {
            var msg = message ?? "";
            var stack = exception != null ? exception.StackTrace : "";
            Warn(msg + "\n" + stack);
        }

        public void WarnFormat(string format, params object[] args)
        {
            Warn(string.Format(format, args));
        }

        public void WarnFormat(string format, object arg0)
        {
            Warn(string.Format(format, arg0));
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            Warn(string.Format(format, arg0, arg1));
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            Warn(string.Format(format, arg0, arg1, arg2));
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            Warn(string.Format(provider, format, args));
        }

        public void Error(object message)
        {
            LogError("ERROR: ", message, null);
        }

        private void LogError(string prefix, object message, Exception innerException)
        {
            var exc = new Exception(prefix + Name + (message ?? ""), innerException);
            var rec = new ErrorRecord(exc, "", ErrorCategory.NotSpecified, null);
            Inner.WriteError(rec);
        }

        public void Error(object message, Exception exception)
        {
            LogError("ERROR: ", message, exception);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            Error(string.Format(format, args));
        }

        public void ErrorFormat(string format, object arg0)
        {
            Error(string.Format(format, arg0));
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            Error(string.Format(format, arg0, arg1));
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            Error(string.Format(format, arg0, arg1, arg2));
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            Error(string.Format(provider, format, args));
        }

        public void Fatal(object message)
        {
            LogError("FATAL: ", message, null);
        }

        public void Fatal(object message, Exception exception)
        {
            LogError("FATAL: ", message, exception);
        }

        public void FatalFormat(string format, params object[] args)
        {
            Fatal(string.Format(format, args));
        }

        public void FatalFormat(string format, object arg0)
        {
            Fatal(string.Format(format, arg0));
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            Fatal(string.Format(format, arg0, arg1));
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            Fatal(string.Format(format, arg0, arg1, arg2));
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            Fatal(string.Format(provider, format, args));
        }
    }
}