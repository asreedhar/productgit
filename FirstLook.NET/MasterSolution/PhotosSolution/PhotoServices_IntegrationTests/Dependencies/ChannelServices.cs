﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.PhotoServices.WebService.V1;

namespace PhotoServices_IntegrationTest.Dependencies
{
    public static class ChannelServices
    {
        public static IChannelFactory<IPhotoWebServices> GetChannelFactory(IPhotoServicesSettings settings)
        {
            var factory = new ChannelFactory<IPhotoWebServices>(
                    new WebHttpBinding { MaxReceivedMessageSize = 20000000 },
                    new EndpointAddress(settings.PhotoWebServicesUrl));
            factory.Endpoint.Behaviors.Add(new WebHttpBehavior());
            return factory;
        }

        public static IPhotoWebServices GetProxy(IPhotoServicesSettings settings)
        {
            var factory = GetChannelFactory(settings);
            return factory.CreateChannel(new EndpointAddress(settings.PhotoWebServicesUrl));
        }
    }
}