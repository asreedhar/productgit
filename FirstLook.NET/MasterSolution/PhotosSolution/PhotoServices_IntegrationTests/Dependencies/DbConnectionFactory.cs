﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace PhotoServices_IntegrationTest.Dependencies
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly IDictionary ConnectionStrings;

        public DbConnectionFactory(IDictionary connectionStrings)
        {
            ConnectionStrings = connectionStrings;
        }

        public IDbConnection GetConnection(string name)
        {
            var cnStr = ConnectionStrings[name] as string;

            if(cnStr == null)
                throw new InvalidOperationException(
                    string.Format("Named connection string '{0}' was not configured.", name));

            var builder = new SqlConnectionStringBuilder(cnStr) {Pooling = true, ApplicationName = GetAppName()};

            var cn = new SqlConnection(builder.ConnectionString);
            bool success = false;
            try
            {
                cn.Open();
                success = true;
                return cn;
            }
            finally
            {
                if(!success)
                    cn.Dispose();
            }
        }

        private static string GetAppName()
        {
            return Assembly.GetExecutingAssembly().GetName().Name;
        }
    }
}