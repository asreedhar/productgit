﻿using System;
using System.Management.Automation;
using FirstLook.Common.Core.Logging;

namespace PhotoServices_IntegrationTest.Dependencies
{
    public class LoggerFactory : ILoggerFactory
    {
        private Cmdlet Inner { get; set; }

        public LoggerFactory(Cmdlet inner)
        {
            Inner = inner;
        }

        public ILog GetLogger<T>()
        {
            return GetLogger(typeof (T).FullName);
        }

        public ILog GetLogger(Type type)
        {
            return GetLogger(type.FullName);
        }

        public ILog GetLogger(string name)
        {
            return new Logger(Inner, name);
        }
    }
}