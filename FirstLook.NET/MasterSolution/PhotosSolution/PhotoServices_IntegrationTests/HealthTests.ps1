﻿param( $Configuration = "Release" )

$base = split-path $MyInvocation.MyCommand.Path

dir (join-path $base .\HealthMonitor\*.ps1) | foreach { & $_ ; write-host }