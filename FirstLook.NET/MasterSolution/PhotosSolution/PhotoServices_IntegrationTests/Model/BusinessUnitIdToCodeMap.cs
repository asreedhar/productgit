﻿namespace PhotoServices_IntegrationTest.Model
{
    public class BusinessUnitIdToCodeMap
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}