﻿using System.Security.Principal;
using FirstLook.Common.Core.Membership;

namespace PhotoServices_IntegrationTest.Model
{
    public class Member : IMember
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Member() : this(null, null, null) { }

        public Member(string username, string first, string last)
        {
            if (username == null)
            {
                var identity = WindowsIdentity.GetCurrent();
                username = identity == null ? "guest" : identity.Name ?? "guest";
            }

            UserName = FixUserName(username);
            FirstName = first ?? "John";
            LastName = last ?? "Smith";
        }

        private static string FixUserName(string name)
        {
            var i = name.IndexOf("\\");
            return i >= 0 ? name.Substring(i + 1) : name;
        }
    }
}