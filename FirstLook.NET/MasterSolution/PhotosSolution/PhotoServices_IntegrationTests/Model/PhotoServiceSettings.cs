﻿using FirstLook.Common.Core.PhotoServices;

namespace PhotoServices_IntegrationTest.Model
{
    public class PhotoServicesSettings : IPhotoServicesSettings
    {
        public static readonly string DefaultBaseResourcesUrl = "http://localhost/resources/";
        public static readonly string DefaultBasePhotoWebServicesUrl = "http://localhost/photoservices";

        public string MainThumbnailPlaceholderUrl { get; set; }
        public int MainThumbnailWidth { get; set; }
        public int MainThumbnailHeight { get; set; }
        public string MainPhotoPlaceholderUrl { get; set; }
        public string PhotoManagerNotAvailableUrl { get; set; }
        public string BulkUploadManagerNotAvailableUrl { get; set; }
        public string PhotoWebServicesUrl { get; set; }
        public string PhotoWebServicesV2Url { get; set; }
        public string PhotoWebServiceV2_BusinessUnit_Whitelist { get; set; }
        public int PhotoWebServiceV2Timeout { get; set; }

        public PhotoServicesSettings(string baseResourcesUrl, string basePhotoWebServicesUrl)
        {
            MainThumbnailPlaceholderUrl = Combine(baseResourcesUrl, "photoservices/placeholder-thumb.png");
            MainPhotoPlaceholderUrl = Combine(baseResourcesUrl, "photoservices/placeholder-main.png");
            PhotoManagerNotAvailableUrl = Combine(baseResourcesUrl, "photoservices/photo-manager-not-available.html");
            BulkUploadManagerNotAvailableUrl = Combine(baseResourcesUrl,
                                                       "photoservices/bulk-upload-manager-not-available.html");
            PhotoWebServicesUrl = Combine(basePhotoWebServicesUrl, "v1.svc/rest/");

            MainThumbnailWidth = 225;
            MainThumbnailHeight = 169;
        }

        public PhotoServicesSettings()
            : this(DefaultBaseResourcesUrl, DefaultBasePhotoWebServicesUrl)
        {
        }

        private static string Combine(string baseUrl, string relative)
        {
            return baseUrl.EndsWith("/") ? baseUrl + relative : baseUrl + "/" + relative;
        }
    }
}