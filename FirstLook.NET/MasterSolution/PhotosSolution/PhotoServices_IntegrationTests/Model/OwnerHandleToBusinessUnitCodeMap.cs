﻿using System;

namespace PhotoServices_IntegrationTest.Model
{
    public class OwnerHandleToBusinessUnitCodeMap
    {
        public Guid Handle { get; set; }
        public string Code { get; set; }
    }
}