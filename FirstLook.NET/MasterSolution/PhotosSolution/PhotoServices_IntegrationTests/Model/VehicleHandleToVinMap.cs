﻿namespace PhotoServices_IntegrationTest.Model
{
    public class VehicleHandleToVinMap
    {
        public string Handle { get; set; }
        public string Vin { get; set; }
    }
}