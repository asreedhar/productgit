﻿using System;
using System.Collections;
using System.Management.Automation;
using System.Text;
using PhotoServices_IntegrationTest.Dependencies;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsCommon.Get, "InvalidOwnerHandle")]
    public class Get_InvalidOwnerHandle : Cmdlet
    {
        private int CountProcessed { get; set; }

        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Position = 1)]
        public int Count { get; set; }

        public Get_InvalidOwnerHandle()
        {
            Count = 1;
        }

        protected override void BeginProcessing()
        {
            CountProcessed = 0;
        }

        protected override void ProcessRecord()
        {
            WriteStockInvalidItems();
            WriteInvalidItemsCheckedAgainstDatabase();

            if (CountProcessed < Count)
                WriteWarning(
                    string.Format("Tried to retrieve {0} invalid item(s), but only retrieved {1}.", Count,
                                  CountProcessed));
            if (CountProcessed <= 0)
                throw new Exception("No invalid items retrieved.");
        }

        private void WriteStockInvalidItems()
        {
            var stockItems = new[] {"Not-a-guid", ""};

            foreach (var t in stockItems)
            {
                if (CountProcessed >= Count)
                    break;
                WriteObject(t);
                CountProcessed++;
            }
        }

        private void WriteInvalidItemsCheckedAgainstDatabase()
        {
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);

            using (var cn = connectionFactory.GetConnection("IMT"))
            using (var cmd = cn.CreateCommand())
            {
                // Get more than Count to compensate for possible random collisions with real ids.
                var numberToGet = Math.Max(10, Count*2);

                var b = new StringBuilder();
                b.AppendLine("declare @handles table(handle uniqueidentifier)");
                for (var i = 0; i < numberToGet; ++i)
                    b.AppendFormat("insert into @handles values ('{0}')\n", Guid.NewGuid());

                b.AppendLine("select HA.Handle");
                b.AppendLine("  from @handles as HA");
                b.AppendLine("    left outer join Market.Pricing.Owner as OW");
                b.AppendLine("    on HA.handle = OW.Handle");
                b.AppendLine("  where OW.Handle is null");

                cmd.CommandText = b.ToString();

                using (var r = cmd.ExecuteReader())
                {
                    while (CountProcessed < Count && r.Read())
                    {
                        WriteObject(r.GetGuid(0).ToString());
                        CountProcessed++;
                    }
                }
            }
        }
    }
}