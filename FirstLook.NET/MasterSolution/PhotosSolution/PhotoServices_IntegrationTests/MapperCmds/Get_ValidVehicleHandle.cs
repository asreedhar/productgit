﻿using System;
using System.Collections;
using System.Management.Automation;
using PhotoServices_IntegrationTest.Dependencies;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsCommon.Get, "ValidVehicleHandle")]
    public class Get_ValidVehicleHandle : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Position = 1)]
        public int Count { get; set; }

        public Get_ValidVehicleHandle()
        {
            Count = 1;
        }

        protected override void ProcessRecord()
        {
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);
            
            using(var cn = connectionFactory.GetConnection("IMT"))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandText =
                   string.Format(
                       @"
select top({0}) '1' + cast(INV.InventoryID as varchar(20)) as Handle, VEH.VIN
    from IMT.dbo.Inventory as INV
        inner join IMT.dbo.tbl_Vehicle as VEH
        on INV.VehicleID = VEH.VehicleID
    where INV.InventoryID < 0
union
select top({1})
    '1' + cast(INV.InventoryID as varchar(20)) as Handle, VEH.VIN
    from IMT.dbo.Inventory as INV
        inner join IMT.dbo.tbl_Vehicle as VEH
        on INV.VehicleID = VEH.VehicleID
    where INV.InventoryID >= 0",
                       Count / 2, Count - (Count / 2));

                var countProcessed = 0;

                using (var r = cmd.ExecuteReader())
                {
                    while (countProcessed < Count && r.Read())
                    {
                        WriteObject(new VehicleHandleToVinMap { Handle = r.GetString(0), Vin = r.GetString(1) });
                        countProcessed++;
                    }
                }

                if (countProcessed < Count)
                    WriteWarning(
                        string.Format("Tried to retrieve {0} valid item(s), but only retrieved {1}.", Count,
                                      countProcessed));
                if (countProcessed <= 0)
                    throw new Exception("No valid items retrieved.");
            }
        }
    }
}