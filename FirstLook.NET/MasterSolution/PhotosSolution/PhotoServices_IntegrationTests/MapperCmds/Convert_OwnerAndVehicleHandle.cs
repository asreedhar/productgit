﻿using System.Collections;
using System.Management.Automation;
using FirstLook.Common.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsData.Convert, "OwnerAndVehiclehandle")]
    public class Convert_OwnerAndVehicleHandle : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Mandatory = true, Position = 1, ValueFromPipelineByPropertyName = true)]
        public string OwnerHandle { get; set; }

        [Parameter(Mandatory = true, Position = 2, ValueFromPipelineByPropertyName = true)]
        public string VehicleHandle { get; set; }

        protected override void ProcessRecord()
        {
            var cache = new SimpleCache();
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);
            var mapperInner = new MapperDAL(connectionFactory);
            var mapperCache = new MapperCacheDecorator(mapperInner, cache);
            var mapper = new PhotoServicesIdMap(mapperCache);

            WriteObject(mapper.MapOwnerAndVehicleHandleToBusinessUnitAndVIN(OwnerHandle, VehicleHandle));
        }
         
    }
}