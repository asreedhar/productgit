﻿using System;
using System.Collections;
using System.Management.Automation;
using PhotoServices_IntegrationTest.Dependencies;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsCommon.Get, "ValidBusinessUnitId")]
    public class Get_ValidBusinessUnitId : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Position = 1)]
        public int Count { get; set; }

        public Get_ValidBusinessUnitId()
        {
            Count = 1;
        }

        protected override void ProcessRecord()
        {
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);

            using (var cn = connectionFactory.GetConnection("IMT"))
            using (var cmd = cn.CreateCommand())
            {
                var countProcessed = 0;

                cmd.CommandText =
                    string.Format(
@"select top({0}) BusinessUnitID, BusinessUnitCode 
from dbo.BusinessUnit 
where businessunitcode >= '1' and businessunittypeid = 4 
order by businessunitcode", Count);

                using(var r = cmd.ExecuteReader())
                {
                    while(countProcessed < Count && r.Read())
                    {
                        WriteObject(new BusinessUnitIdToCodeMap {Id = r.GetInt32(0), Code = r.GetString(1)});
                        countProcessed++;
                    }
                }

                if (countProcessed < Count)
                    WriteWarning(
                        string.Format("Tried to retrieve {0} valid item(s), but only retrieved {1}.", Count,
                                      countProcessed));
                if (countProcessed <= 0)
                    throw new Exception("No valid items retrieved.");
            }
        }
    }
}