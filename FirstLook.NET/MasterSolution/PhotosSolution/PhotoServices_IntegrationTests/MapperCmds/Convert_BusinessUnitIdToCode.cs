﻿using System.Collections;
using System.Management.Automation;
using FirstLook.Common.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsData.Convert, "BusinessUnitId")]
    public class Convert_BusinessUnitId : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Mandatory = true, Position = 1, ValueFromPipeline = true)]
        public int Id { get; set; }

        protected override void ProcessRecord()
        {
            var cache = new SimpleCache();
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);
            var mapperInner = new MapperDAL(connectionFactory);
            var mapperCache = new MapperCacheDecorator(mapperInner, cache);
            var mapper = new PhotoServicesIdMap(mapperCache);

            WriteObject(mapper.MapBusinessUnitIdToBusinessUnitCode(Id));
        }
    }
}