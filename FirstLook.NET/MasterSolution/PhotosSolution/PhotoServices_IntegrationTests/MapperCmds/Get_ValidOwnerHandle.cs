﻿using System;
using System.Collections;
using System.Management.Automation;
using PhotoServices_IntegrationTest.Dependencies;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsCommon.Get, "ValidOwnerHandles")]
    public class Get_ValidOwnerHandle : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Position = 1)]
        public int Count { get; set; }

        public Get_ValidOwnerHandle()
        {
            Count = 1;
        }

        protected override void ProcessRecord()
        {
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);
            
            using(var cn = connectionFactory.GetConnection("IMT"))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandText =
                    string.Format(
@"select top({0}) OW.Handle, BU.BusinessUnitCode
    from Market.Pricing.[Owner] as OW
        inner join IMT.Dbo.BusinessUnit as BU
        on OW.OwnerEntityID = BU.BusinessUnitID
    where OW.OwnerTypeID = 1
    order by OW.OwnerName", Count);

                var countProcessed = 0;

                using(var r = cmd.ExecuteReader())
                {
                    while(countProcessed < Count && r.Read())
                    {
                        WriteObject(new OwnerHandleToBusinessUnitCodeMap {Handle = r.GetGuid(0), Code = r.GetString(1)});
                        countProcessed++;
                    }
                }

                if (countProcessed < Count)
                    WriteWarning(
                        string.Format("Tried to retrieve {0} valid item(s), but only retrieved {1}.", Count,
                                      countProcessed));
                if (countProcessed <= 0)
                    throw new Exception("No valid items retrieved.");
            }
        }
    }
}