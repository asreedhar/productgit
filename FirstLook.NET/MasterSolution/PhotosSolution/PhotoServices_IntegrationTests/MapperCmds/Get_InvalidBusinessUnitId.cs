﻿using System;
using System.Collections;
using System.Management.Automation;
using System.Text;
using PhotoServices_IntegrationTest.Dependencies;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsCommon.Get, "InvalidBusinessUnitId")]
    public class Get_InvalidBusinessUnitId : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Position = 1)]
        public int Count { get; set; }

        [Parameter(Position = 2)]
        public int? RandomSeed { get; set; }

        public Get_InvalidBusinessUnitId()
        {
            Count = 1;
        }

        protected override void ProcessRecord()
        {
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);

            var random = GetRandomizer();

            using(var cn = connectionFactory.GetConnection("IMT"))
            using(var cmd = cn.CreateCommand())
            {
                var numberToGet = Math.Max(10, Count * 2); // Get more than Count to compensate for possible random collisions with real ids.
                var countProcessed = 0;

                var b = new StringBuilder();
                b.AppendLine("declare @nums table(id int)");
                for (var i = 0; i < numberToGet; ++i)
                    b.AppendFormat("insert into @nums values ({0})\n", random.Next());

                b.AppendLine("select Nums.Id as BusinessUnitId");
                b.AppendLine("  from @nums as Nums left outer join dbo.BusinessUnit");
                b.AppendLine("  on Nums.Id = BusinessUnit.BusinessUnitID");
                b.AppendLine("  where BusinessUnit.BusinessUnitID is null");
                cmd.CommandText = b.ToString();

                using(var reader = cmd.ExecuteReader())
                {
                    while(countProcessed < Count && reader.Read())
                    {
                        WriteObject(reader.GetInt32(0));
                        countProcessed++;
                    }
                }

                if(countProcessed < Count)
                    WriteWarning(
                        string.Format("Tried to retrieve {0} invalid item(s), but only retrieved {1}.", Count,
                                      countProcessed));
                if(countProcessed <= 0)
                    throw new Exception("No invalid items retrieved.");
            }
        }

        private Random GetRandomizer()
        {
            return RandomSeed.HasValue 
                ? new Random(RandomSeed.Value) 
                : new Random();
        }
    }
}