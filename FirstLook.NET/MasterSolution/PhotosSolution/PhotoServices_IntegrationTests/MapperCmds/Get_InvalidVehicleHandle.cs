﻿using System;
using System.Collections;
using System.Data;
using System.Management.Automation;
using System.Text;
using PhotoServices_IntegrationTest.Dependencies;

namespace PhotoServices_IntegrationTest.MapperCmds
{
    [Cmdlet(VerbsCommon.Get, "InvalidVehicleHandle")]
    public class Get_InvalidVehicleHandle : Cmdlet
    {
        private int CountProcessed { get; set; }

        [Parameter(Mandatory = true, Position = 0)]
        public IDictionary ConnectionStrings { get; set; }

        [Parameter(Position = 1)]
        public int Count { get; set; }

        [Parameter(Position = 2)]
        public int? RandomSeed { get; set; }

        public Get_InvalidVehicleHandle()
        {
            Count = 1;
        }

        protected override void BeginProcessing()
        {
            CountProcessed = 0;
        }

        protected override void ProcessRecord()
        {
            WriteStockInvalidItems();
            WriteInvalidItemsCheckedAgainstDatabase();

            if (CountProcessed < Count)
                WriteWarning(
                    string.Format("Tried to retrieve {0} invalid item(s), but only retrieved {1}.", Count,
                                  CountProcessed));
            if (CountProcessed <= 0)
                throw new Exception("No invalid items retrieved.");
        }

        private void WriteInvalidItemsCheckedAgainstDatabase()
        {
            var connectionFactory = new DbConnectionFactory(ConnectionStrings);

            using (var cn = connectionFactory.GetConnection("IMT"))
            using (var cmd = cn.CreateCommand())
            {
                // Get more than Count to compensate for possible random collisions with real ids.
                int numberToGet = Math.Max(10, Count*2);
                Random randomizer = GetRandomizer();

                var b = new StringBuilder();
                b.AppendLine("declare @nums table(id int)");
                for (var i = 0; i < numberToGet; ++i)
                    b.AppendFormat("insert into @nums values ({0})\n", randomizer.Next());
                b.AppendLine("select NU.id");
                b.AppendLine("  from @nums as NU");
                b.AppendLine("    left outer join IMT.dbo.Inventory as INV");
                b.AppendLine("    on NU.id = INV.InventoryID");
                b.AppendLine("  where INV.InventoryID is null");
                cmd.CommandText = b.ToString();

                using (IDataReader r = cmd.ExecuteReader())
                {
                    while (CountProcessed < Count && r.Read())
                    {
                        WriteObject("1" + r.GetInt32(0));
                        CountProcessed++;
                    }
                }
            }
        }

        private void WriteStockInvalidItems()
        {
            var invalidItems = new[]
                    {
                        "6Anything", // Doesn't start with 1 or 4
                        "", // Too small
                        "13838838383838838383838", // Too big
                        "1abc" // Doesn't end in a number
                    };
            foreach(var item in invalidItems)
            {
                if (CountProcessed >= Count)
                    break;
                WriteObject(item);
                CountProcessed++;
            }
        }

        private Random GetRandomizer()
        {
            return RandomSeed.HasValue
                ? new Random(RandomSeed.Value)
                : new Random();
        }
    }
}