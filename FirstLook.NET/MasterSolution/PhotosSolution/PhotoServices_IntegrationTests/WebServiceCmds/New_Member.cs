﻿using System.Management.Automation;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsCommon.New, "Member")]
    public class New_Member : Cmdlet
    {
        [Parameter]
        public string UserName { get; set; }

        [Parameter]
        public string FirstName { get; set; }

        [Parameter]
        public string LastName { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(
                new Member(UserName, FirstName, LastName));
        }
    }
}