﻿using System.Management.Automation;
using FirstLook.Common.Core.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsCommon.Get, "MainThumbnailUrl")]
    public class Get_MainThumbnailUrl : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        [Alias("buc")]
        public string BusinessUnitCode { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public string VIN { get; set; }

        [Parameter]
        public IPhotoServicesSettings Settings { get; set; }

        protected override void ProcessRecord()
        {
            var service = PhotoServicesFactory.GetPhotoServices(Settings, this);

            var result = service.GetMainThumbnailUrl(BusinessUnitCode, VIN);

            WriteObject(result);
        }
    }
}