﻿using System.Management.Automation;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsCommon.Get, "PhotoManagerUrl")]
    public class Get_PhotoManagerUrl : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        [Alias("buc")]
        public string BusinessUnitCode { get; set; }

        [Parameter(Mandatory = true, Position = 1)]
        public string VIN { get; set; }

        [Parameter(Mandatory = true, Position = 2)]
        [AllowEmptyString]
        [Alias("stk")]
        public string StockNumber { get; set; }

        [Parameter]
        public Model.PhotoManagerContext Context { get; set; }

        [Parameter]
        public IMember User { get; set; }

        [Parameter]
        public IPhotoServicesSettings Settings { get; set; }

        public Get_PhotoManagerUrl()
        {
            Context = Model.PhotoManagerContext.MAX;
        }

        protected override void ProcessRecord()
        {
            var service = PhotoServicesFactory.GetPhotoServices(Settings, this);
            var context = ContextHelper.Calculate(Context);

            var url = service.GetPhotoManagerUrl(User ?? new Member(), BusinessUnitCode, VIN, StockNumber, context);

            WriteObject(url);
        }
    }
}