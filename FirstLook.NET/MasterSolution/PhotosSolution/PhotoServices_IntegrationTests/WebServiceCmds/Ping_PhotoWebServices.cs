﻿using System;
using System.Management.Automation;
using FirstLook.Common.Core.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsDiagnostic.Ping, "PhotoWebServices")]
    public class Ping_PhotoWebServices : Cmdlet
    {
        [Parameter]
        public string EchoData { get; set; }

        [Parameter]
        public IPhotoServicesSettings Settings { get; set; }

        public Ping_PhotoWebServices()
        {
            EchoData = Guid.NewGuid().ToString();
        }

        protected override void ProcessRecord()
        {
            var proxy = ChannelServices.GetProxy(Settings ?? new PhotoServicesSettings());

            var returnedData = proxy.Echo(EchoData);

            if(StringComparer.InvariantCulture.Compare(returnedData, EchoData) != 0)
                throw new Exception("Incorrect data returned from Echo operation.");
        }
    }
}