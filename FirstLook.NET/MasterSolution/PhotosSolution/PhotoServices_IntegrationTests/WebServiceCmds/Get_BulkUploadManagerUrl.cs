﻿using System.Management.Automation;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsCommon.Get, "BulkUploadManagerUrl")]
    public class Get_BulkUploadManagerUrl : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        [Alias("buc")]
        public string BusinessUnitCode { get; set; }

        [Parameter]
        public Model.PhotoManagerContext Context { get; set; }

        [Parameter]
        public IMember User { get; set; }

        [Parameter]
        public IPhotoServicesSettings Settings { get; set; }

        public Get_BulkUploadManagerUrl()
        {
            Context = Model.PhotoManagerContext.MAX;
        }

        protected override void ProcessRecord()
        {
            var service = PhotoServicesFactory.GetPhotoServices(Settings, this);
            var context = ContextHelper.Calculate(Context);

            var url = service.GetBulkUploadManagerUrl(User ?? new Member(), BusinessUnitCode, context);

            WriteObject(url);
        }
    }
}