﻿using System.Management.Automation;
using FirstLook.Common.Core.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsCommon.Get, "SettingsFromServer")]
    public class Get_SettingsFromServer : Cmdlet
    {
        [Parameter]
        public IPhotoServicesSettings Settings { get; set; }

        protected override void ProcessRecord()
        {
            var proxy = ChannelServices.GetProxy(Settings ?? new PhotoServicesSettings());

            WriteObject(proxy.GetSettings());
        }
    }
}