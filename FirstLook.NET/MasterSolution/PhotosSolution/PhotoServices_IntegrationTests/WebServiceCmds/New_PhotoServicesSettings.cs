﻿using System.Management.Automation;
using PhotoServices_IntegrationTest.Model;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsCommon.New, "PhotoServicesSettings")]
    public class New_PhotoServicesSettings : Cmdlet
    {
        [Parameter]
        public string BaseResourcesUrl { get; set; }

        [Parameter]
        public string BasePhotoWebServicesUrl { get; set; }

        protected override void ProcessRecord()
        {
            var settings = new PhotoServicesSettings(
                BaseResourcesUrl ?? PhotoServicesSettings.DefaultBaseResourcesUrl,
                BasePhotoWebServicesUrl ?? PhotoServicesSettings.DefaultBasePhotoWebServicesUrl);
            WriteObject(settings);
        }
    }
}