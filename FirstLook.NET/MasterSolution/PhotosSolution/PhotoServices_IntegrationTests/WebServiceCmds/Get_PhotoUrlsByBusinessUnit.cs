﻿using System;
using System.Management.Automation;
using FirstLook.Common.Core.PhotoServices;
using PhotoServices_IntegrationTest.Dependencies;

namespace PhotoServices_IntegrationTest.WebServiceCmds
{
    [Cmdlet(VerbsCommon.Get, "PhotoUrlsByBusinessUnit")]
    public class Get_PhotoUrlsByBusinessUnit : Cmdlet
    {
        [Parameter(Mandatory = true, Position = 0)]
        [Alias("buc")]
        public string BusinessUnitCode { get; set; }

        [Parameter]
        public int? Timeout { get; set; }

        [Parameter]
        public IPhotoServicesSettings Settings { get; set; }

        protected override void ProcessRecord()
        {
            var service = PhotoServicesFactory.GetPhotoServices(Settings, this);

            var results = service.GetPhotoUrlsByBusinessUnit(BusinessUnitCode,
                                                    TimeSpan.FromMilliseconds(Timeout.GetValueOrDefault(15000)));

            WriteObject(results);
        }
    }
}