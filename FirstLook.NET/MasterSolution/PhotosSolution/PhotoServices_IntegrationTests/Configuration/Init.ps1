﻿$baseInit = split-path $MyInvocation.MyCommand.Path

Import-Module $baseInit\..\PhotoServices_IntegrationTests.dll

if($Configuration -eq $null)
{
    $Configuration = "release"
}

. $baseInit\Config.$Configuration.ps1

add-type -typedefinition @"

// From http://couldbedone.blogspot.com/2007/08/webclient-handling-cookies.html
    
public class CookieAwareWebClient : System.Net.WebClient
{
    public readonly System.Net.CookieContainer Cookies = new System.Net.CookieContainer();

    protected override System.Net.WebRequest GetWebRequest(System.Uri address)
    {
        var request = base.GetWebRequest(address);
        if (request is System.Net.HttpWebRequest)
        {
            ((System.Net.HttpWebRequest)request).CookieContainer = Cookies;
        }
        return request;
    }
}
"@ -language CSharpVersion3


function RunTest([string]$op, [ScriptBlock] $scriptToRun)
{
    $exception = $null
    $start = [DateTimeOffset]::Now
    try
    {
        $stats = & $scriptToRun
    }
    catch
    {
        $exception = $Error[0].Exception
    }
    finally
    {
        $stop = [DateTimeOffset]::Now
    }

    $elapsed = [long](($stop - $start).TotalMilliseconds)
    if($exception -ne $null)
    {
        $msg = "failed: $($exception.Message)"
        $code = 2
    }
    else
    {
        $msg = "succeeded"
        $code = 0
    }

    if($stats -is [System.Collections.IDictionary])
    {
        $stats = " " + ($stats.GetEnumerator() | sort Value | %{ "$($_.Key)=$($_.Value)" }) -join " " 
    }

    write-host "PhotoServices $op $msg|elapsed=${elapsed}ms config=$Configuration$stats" -nonewline
    exit $code
}