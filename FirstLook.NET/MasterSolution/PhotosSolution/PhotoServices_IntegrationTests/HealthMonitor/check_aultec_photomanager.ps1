﻿param( $Configuration = "Release" )

$base = split-path $MyInvocation.MyCommand.Path
. $base\..\Configuration\Init.ps1

$businessUnitCode = "WINDYCIT05"
$vin = "V1234"
$stk = "STK1234"
$user = New-Member -UserName HealthMonitor -FirstName Nagios -LastName Alerts

RunTest "PhotoManager" { 

	# Get the photo manager url
	$url = get-PhotoManagerUrl $businessUnitCode $vin $stk -User $user -Settings $settings

	# Scrape the url and make sure
	# a) Content type is HTML
	# b) Bytes returned > 0
	
	$webClient = new-object CookieAwareWebClient
	$data = $webClient.DownloadData($url)

	$contentType = $webClient.ResponseHeaders["Content-Type"]
	if(!$contentType.StartsWith("text/html"))
	{
		throw "Invalid Content-Type for photo manager ('$contentType')."
	}

	if($data -eq $null -or $data.Length -lt 1000)
	{
		throw "Not enough data returned for photo manager."
	}

	@{ bytesReturned="$($data.Length);;;1000" }
}