﻿param( $Configuration = "Release" )

$base = split-path $MyInvocation.MyCommand.Path
. $base\..\Configuration\Init.ps1

RunTest "PING" { 
	foreach($h in $photoServicesHosts)
	{
		$a = New-PhotoServicesSettings `
			-BaseResourcesUrl $settings.BaseResourceUrl `
			-BasePhotoWebServicesUrl $h
		Ping-PhotoWebServices -settings $a
	}
	@{ hostCount = $photoServicesHosts.Length }
}