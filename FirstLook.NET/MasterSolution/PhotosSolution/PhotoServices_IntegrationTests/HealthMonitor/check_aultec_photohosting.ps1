﻿param( $Configuration = "Release" )

$base = split-path $MyInvocation.MyCommand.Path
. $base\..\Configuration\Init.ps1

$businessUnitCode = "WINDYCIT05"

RunTest "PhotoHosting" { 

	# Get at least one photo url
	$vin = $null
	$photoUrl = $null
	$data = Get-PhotoUrlsByBusinessUnit $businessUnitCode -Timeout 60000 -Settings $settings
	foreach($veh in $data.PhotosByVin.Values)
	{
		if($veh.PhotoUrls.Length -gt 0)
		{
			$vin = $veh.Vin
			$photoUrl = $veh.PhotoUrls[0]
			break
		}
	}
	if($vin -eq $null)
	{
		throw "No vehicles found for $businessUnitCode."
	}
	if($photoUrl -eq $null)
	{
		throw "No photos found for $businessUnitCode."
	}

	$mainPhotoUrl = Get-MainPhotoUrl $businessUnitCode $vin -settings $settings
	$thumbPhotoUrl = Get-MainThumbnailUrl $businessUnitCode $vin -settings $settings

	$urls = $mainPhotoUrl, $thumbPhotoUrl.Url, $photoUrl


	# Scrape the urls and make sure they return back jpgs.
	$bytes = 0
	$webClient = new-object CookieAwareWebClient
	foreach($url in $urls)
	{
		$data = $webClient.DownloadData($url)
		
		$contentType = $webClient.ResponseHeaders["Content-Type"]
		if(!$contentType.StartsWith("image/jpeg"))
		{
			throw "Invalid Content-Type for photo $url ('$contentType')."
		}

		if($data -eq $null -or $data.Length -lt 10)
		{
			throw "Not enough data returned for $url."
		}

		$bytes += $data.Length
	}

	# Return some stats
	@{ photoUrls=$urls.Length; photoBytes="$bytes;;;$($urls.Length * 10)" }
}