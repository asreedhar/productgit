﻿param( $Configuration = "Release" )

$base = split-path $MyInvocation.MyCommand.Path
. $base\..\Configuration\Init.ps1

$urlMinThreshold = 100
$vehMinThreshold = 10
$businessUnitCode = "WINDYCIT05"

RunTest "PhotoQuery" { 
	
	# Get all urls for the specified business unit
	$data = Get-PhotoUrlsByBusinessUnit $businessUnitCode -settings $settings
	
	# Check for errors
	if($data.Errors -ne $null -and $data.Errors.Count -gt 0)
	{
		$errorMsg = ($data.Errors | %{ "$($_.Code): $($_.Message)" }) -join " / "
		throw $errorMsg
	}

	# Count urls and vehicles
	$urlCount = 0
	$vehCount = 0
	$data.PhotosByVin.Values | %{ $vehCount++; $urlCount += $_.PhotoUrls.Length }
	@{ Vehicles="$vehCount;;;$vehMinThreshold"; Photos="$urlCount;;;$urlMinThreshold" }

	# Check for minimum counts
	if($urlCount -lt $urlMinThreshold)
	{
		throw "Photo count ($urlCount) fell below minimum threshold ($urlMinThreshold)"
	}
	if($vehCount -lt $vehMinThreshold)
	{
		throw "Vehicle count ($vehCount) fellow below minimum threshold ($vehMinThreshold)"
	}
}