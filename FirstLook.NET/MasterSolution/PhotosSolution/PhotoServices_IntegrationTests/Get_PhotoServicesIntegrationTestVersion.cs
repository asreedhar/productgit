﻿using System.Management.Automation;
using System.Reflection;

namespace PhotoServices_IntegrationTest
{
    [Cmdlet(VerbsCommon.Get, "PhotoServicesIntegrationTestVersion")]
    public class Get_PhotoServicesIntegrationTestVersion : Cmdlet
    {
        protected override void ProcessRecord()
        {
            WriteObject(
                Assembly.GetExecutingAssembly().GetName().Version);
        }
    }
}