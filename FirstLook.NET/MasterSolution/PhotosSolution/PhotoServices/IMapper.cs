namespace FirstLook.Common.PhotoServices
{
    internal interface IMapper
    {
        string MapBusinessUnitIdToBusinessUnitCode(int businessUnitId);
        string MapOwnerHandleToBusinessUnitCode(string ownerHandle);
        string MapVehicleHandleToVin(string vehicleHandle);
    }
}