﻿using System;
using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.Common.PhotoServices
{
    internal class MapperDAL : IMapper
    {
        private IDbConnectionFactory ConnectionFactory { get; set; }

        public MapperDAL(IDbConnectionFactory connectionFactory)
        {
            ConnectionFactory = connectionFactory;
        }

        private const string Msg_VehicleHandleFormatError = "The vehicle handle must begin with a 1 or 4 then followed by a number.";
        private const string Msg_OwnerHandleFormatError = "The owner handle must be a valid GUID value.";
        private const string Msg_FailedOwnerHandleMap = "Failed to lookup a BusinessUnit with an Owner Handle of {0}.";
        private const string Msg_FailedVehicleHandleMap = "Failed to lookup a VIN with a Vehicle Handle of {0}.";
        private const string Msg_FailedBusinessUnitIdMap = "Failed to lookup a BusinessUnit with an Id of {0}.";

        public string MapBusinessUnitIdToBusinessUnitCode(int businessUnitId)
        {
            return LookupBusinessUnitCodeByBusinessUnitId(businessUnitId);
        }

        public string MapOwnerHandleToBusinessUnitCode(string ownerHandle)
        {
            return LookupBusinessUnitCodeByOwnerHandle(ownerHandle);
        }

        public string MapVehicleHandleToVin(string vehicleHandle)
        {
            return LookupVinByVehicleHandle(vehicleHandle);
        }

        private string LookupBusinessUnitCodeByBusinessUnitId(int businessUnitId)
        {
            return ExecuteStoredProcedure(
                "photoservices.MapBusinessUnitIdToBusinessUnitCode",
                "@businessUnitId", businessUnitId, DbType.Int32,
                () => FormatException(Msg_FailedBusinessUnitIdMap, businessUnitId));
        }

        private string LookupVinByVehicleHandle(string vehicleHandle)
        {
            var inventoryId = ValidateVehicleHandleAndExtractInventoryId(vehicleHandle);

            return ExecuteStoredProcedure(
                "photoservices.MapInventoryIdToVin",
                "@inventoryID", inventoryId, DbType.Int32,
                () => FormatException(Msg_FailedVehicleHandleMap, vehicleHandle));
        }

        private string LookupBusinessUnitCodeByOwnerHandle(string ownerHandle)
        {
            Guid guid;
            if (!Guid.TryParse(ownerHandle, out guid))
                throw FormatException(Msg_OwnerHandleFormatError);

            return ExecuteStoredProcedure(
                "photoservices.MapOwnerHandleToBusinessUnitCode", 
                "@ownerHandle", guid, DbType.Guid, 
                () => FormatException(Msg_FailedOwnerHandleMap, ownerHandle));
        }

        private string ExecuteStoredProcedure(string storedProcName, 
            string storedProcParamName, object storedProcParamValue, DbType storedProcParamType, 
            Func<Exception> formatNotFoundException)
        {
            using (var cn = GetConnection())
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandTimeout = 30;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcName;

                Database.AddWithValue(cmd, storedProcParamName, storedProcParamValue, storedProcParamType);

                var val = (string)cmd.ExecuteScalar();
                if (val == null)
                    throw formatNotFoundException();

                return val;
            }
        }

        private static int ValidateVehicleHandleAndExtractInventoryId(string vehicleHandle)
        {
            if (vehicleHandle == null || vehicleHandle.Length < 2) throw FormatException(Msg_VehicleHandleFormatError);
            var first = vehicleHandle.Substring(0, 1);
            var rest = vehicleHandle.Substring(1);
            if (first != "1" && first != "4") throw FormatException(Msg_VehicleHandleFormatError);
            int inventoryId;
            if (!int.TryParse(rest, out inventoryId)) throw FormatException(Msg_VehicleHandleFormatError);
            return inventoryId;
        }

        private static Exception FormatException(string msg, params object[] parameters)
        {
            return new DataException(string.Format(msg, parameters));
        }

        private IDbConnection GetConnection()
        {
            return ConnectionFactory.GetConnection("Merchandising");
        }
    }
}