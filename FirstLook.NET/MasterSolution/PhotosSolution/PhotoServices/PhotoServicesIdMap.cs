﻿using FirstLook.Common.Core.PhotoServices;

namespace FirstLook.Common.PhotoServices
{
    internal class PhotoServicesIdMap : IPhotoServicesIdMap
    {
        private IMapper Inner { get; set; }

        public PhotoServicesIdMap(IMapper inner)
        {
            Inner = inner;
        }

        public string MapBusinessUnitIdToBusinessUnitCode(int businessUnitId)
        {
            return Inner.MapBusinessUnitIdToBusinessUnitCode(businessUnitId);
        }

        public BusinessUnitAndVIN MapOwnerAndVehicleHandleToBusinessUnitAndVIN(string ownerHandle, string vehicleHandle)
        {
            return new BusinessUnitAndVIN(
                Inner.MapOwnerHandleToBusinessUnitCode(ownerHandle),
                Inner.MapVehicleHandleToVin(vehicleHandle));
        }
    }
}
