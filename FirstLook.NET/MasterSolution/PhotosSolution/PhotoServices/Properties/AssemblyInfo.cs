﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("PhotoServices")]
[assembly: Guid("4f0fb2ae-5f8f-4850-98c1-b59f4dfe987f")]

[assembly: InternalsVisibleTo("Firstlook.Common.PhotoServices_Tests")]

[assembly: InternalsVisibleTo("PhotoServices_IntegrationTests")]