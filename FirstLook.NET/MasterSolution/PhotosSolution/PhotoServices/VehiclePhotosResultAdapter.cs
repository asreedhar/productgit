﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;

namespace FirstLook.Common.PhotoServices
{
    internal class VehiclePhotosResultAdapter : IVehiclePhotosResult
    {
        internal VehiclePhotosResultAdapter(VehiclesPhotosResult inner)
        {
            if(inner == null)
                throw new ArgumentNullException("inner");

            if(inner.Vehicles == null || inner.Vehicles.Length <= 0)
            {
                Vin = "";
                PhotoUrls = new string[0];
            }
            else
            {
                Vin = inner.Vehicles[0].Vin ?? "";
                PhotoUrls = inner.Vehicles[0].PhotoUrls.Where(url => !string.IsNullOrWhiteSpace(url)).ToArray();    
            }

            if(inner.Errors == null)
            {
                Errors = new IPhotoServicesError[0];
            }
            else
            {
                Errors =
                    (from error in inner.Errors
                     where error != null && 
                        (!string.IsNullOrWhiteSpace(error.Code) || !string.IsNullOrWhiteSpace(error.Message))
                     select new Error(error.Code ?? "", error.Message ?? ""))
                        .ToArray();
            }
        }

        public string Vin { get; private set; }

        public string[] PhotoUrls { get; private set; }

        public ICollection<IPhotoServicesError> Errors { get; private set; }
    }
}
