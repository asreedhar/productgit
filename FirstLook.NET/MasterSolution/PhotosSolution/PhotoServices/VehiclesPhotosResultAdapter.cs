﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;

namespace FirstLook.Common.PhotoServices
{
    internal class VehiclesPhotosResultAdapter : IVehiclesPhotosResult
    {
        public VehiclesPhotosResultAdapter(VehiclesPhotosResult inner)
        {
            if(inner == null)
                throw new ArgumentNullException("inner");

            PhotosByVin = new Dictionary<string, IVehiclePhotos>(StringComparer.InvariantCultureIgnoreCase);
            if(inner.Vehicles != null)
            {
                foreach(var vehicleIn in inner.Vehicles.Where(v => !string.IsNullOrWhiteSpace(v.Vin)))
                {
                    IVehiclePhotos vehicleOut;
                    PhotosByVin[vehicleIn.Vin] = PhotosByVin.TryGetValue(vehicleIn.Vin, out vehicleOut)
                        ? new VehiclePhotos(vehicleIn.Vin, CleanUrls(vehicleOut.PhotoUrls.Concat(vehicleIn.PhotoUrls))) 
                        : new VehiclePhotos(vehicleIn.Vin, CleanUrls(vehicleIn.PhotoUrls));
                }
            }

            Errors = (from error in (inner.Errors ?? Enumerable.Empty<IPhotoServicesError>())
                      where error != null &&
                            (!string.IsNullOrWhiteSpace(error.Code) || !string.IsNullOrWhiteSpace(error.Message))
                      select new Error(error.Code ?? "", error.Message ?? ""))
                .ToArray();
        }

        private static string[] CleanUrls(IEnumerable<string> photoUrls)
        {
            return (from url in photoUrls
                    where !string.IsNullOrWhiteSpace(url)
                    select url).ToArray();
        }

        public IDictionary<string, IVehiclePhotos> PhotosByVin { get; private set; }

        public ICollection<IPhotoServicesError> Errors { get; private set; }
    }
}
