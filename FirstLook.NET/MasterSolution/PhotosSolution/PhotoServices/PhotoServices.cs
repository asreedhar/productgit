using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.PhotoServices.WebService.V1;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using FirstLook.Common.Core.Utilities;
using MAX.Caching;
using Merchandising.Messages;
using MvcMiniProfiler;
using MAX.Entities.Helpers.PhotoServicesHelpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FirstLook.Common.PhotoServices
{
    internal class PhotoServices : IPhotoServices
    {
        private const int CacheTimeoutSuccess = 60;
        private const int CacheTimeoutFailure = 5;

        private IChannelFactory<IPhotoWebServices> ProxyFactory { get; set; }
        private IPhotoServicesSettings Settings { get; set; }
        private ICache Cache { get; set; }
        private ILog Log { get; set; }
        private ICacheWrapper CacheWrapper { get; set; }

        private Helper _serviceHelper;
        public Helper ServiceHelper
        {
            get { return _serviceHelper ?? (_serviceHelper = new Helper()); }
        }

        public PhotoServices(IChannelFactory<IPhotoWebServices> proxyFactory, ILoggerFactory loggerFactory,
            IPhotoServicesSettings settings, ICache cache)
        {
            ProxyFactory = proxyFactory;
            Settings = settings;
            Cache = cache;
            Log = loggerFactory.GetLogger<PhotoServices>();
        }

        public string GetPhotoManagerUrl(IMember auditUser, string businessUnitCode, string vin, string stockNumber, PhotoManagerContext context)
        {
            ValidateAuditUser(auditUser);
            ValidateBusinessUnitCode(businessUnitCode);
            ValidateVin(vin);
            ValidateStockNumber(stockNumber);
            ValidateContext(context);

            return CallProxy(
                p => p.GetPhotoManagerUrl(auditUser.UserName, auditUser.FirstName, auditUser.LastName,
                                          businessUnitCode, vin, stockNumber, context.Code),
                "GetPhotoManagerUrl",
                ex => Settings.PhotoManagerNotAvailableUrl);
        }

        public string GetBulkUploadManagerUrl(IMember auditUser, string businessUnitCode, PhotoManagerContext context)
        {
            ValidateAuditUser(auditUser);
            ValidateBusinessUnitCode(businessUnitCode);
            ValidateContext(context);

            return CallProxy(
                p =>
                p.GetBulkUploadManagerUrl(auditUser.UserName, auditUser.FirstName, auditUser.LastName, businessUnitCode,
                                          context.Code),
                "GetBulkUploadManagerUrl",
                ex => Settings.BulkUploadManagerNotAvailableUrl);
        }

        public IVehiclePhotosResult GetPhotoUrlsByVin(string businessUnitCode, string vin, TimeSpan timeout)
        {
            Log.InfoFormat("GetPhotoUrlsByVin(businessUnitCode: \"{0}\", timeout: {1}) Started.", businessUnitCode, timeout);
            var startTime = DateTime.UtcNow;
            VehiclesPhotosResult results = null;

            using (MiniProfiler.Current.Step("GetPhotoUrlsByVin"))
            {
                ValidateBusinessUnitCode(businessUnitCode);
                ValidateVin(vin);

                if (WhiteListHelper.IsInWhiteList(businessUnitCode, Settings.PhotoWebServiceV2_BusinessUnit_Whitelist))
                    results = GetPhotoUrlsByVinV2(businessUnitCode, vin, TimeSpan.FromSeconds(Settings.PhotoWebServiceV2Timeout));

                if (results == null)
                {
                    results = CallProxy(
                        p =>
                        {
                            SetChannelOperationTimeout(p, timeout);
                            return p.GetPhotoUrlsByVin(businessUnitCode, vin, (int) timeout.TotalMilliseconds);
                        },
                        "GetPhotoUrlsByVin",
                        ex => new VehiclesPhotosResult(new[] {new Error(ex.GetType().ToString(), ex.Message)}));
                }

                if (results == null)
                    results = new VehiclesPhotosResult(new[] { new Error(String.Empty, "Error retrieving vehicles") });
            }

            Log.InfoFormat("GetPhotoUrlsByVin() Finished. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return new VehiclePhotosResultAdapter(results);
        }

        public IVehiclesPhotosResult GetPhotoUrlsByBusinessUnit(string businessUnitCode, TimeSpan timeout)
        {
            Log.InfoFormat("GetPhotoUrlsByBusinessUnit(businessUnitCode: \"{0}\", timeout: {1}) Started.", businessUnitCode, timeout);
            var startTime = DateTime.UtcNow;
            VehiclesPhotosResult results = null;

            using (MiniProfiler.Current.Step("GetPhotoUrlsByBusinessUnit"))
            {
                ValidateBusinessUnitCode(businessUnitCode);

                if (WhiteListHelper.IsInWhiteList(businessUnitCode, Settings.PhotoWebServiceV2_BusinessUnit_Whitelist))
                    results = GetPhotoUrlsByBusinessUnitV2(businessUnitCode, TimeSpan.FromSeconds(Settings.PhotoWebServiceV2Timeout));

                if(results == null)
                {
                    results = CallProxy(p =>
                        {
                            SetChannelOperationTimeout(p, timeout);
                            return p.GetPhotoUrlsByBusinessUnit(businessUnitCode, (int) timeout.TotalMilliseconds);
                        },
                        "GetPhotoUrlsByBusinessUnit",
                        ex => new VehiclesPhotosResult(new[] {new Error(ex.GetType().ToString(), ex.Message)}));
                }

                if (results == null)
                    results = new VehiclesPhotosResult(new [] {new Error(String.Empty, "Error retrieving vehicles") });
            }

            Log.InfoFormat("GetPhotoUrlsByBusinessUnit() Finished. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return new VehiclesPhotosResultAdapter(results);
        }

        public IDictionary<String, int> GetVinPhotoCountsForBusinessUnit(String businessUnitCode, TimeSpan timeout)
        {
            Log.InfoFormat("GetVinPhotoCountsForBusinessUnit(businessUnitCode: \"{0}\", timeout: {1}) Started.", businessUnitCode, timeout);
            var startTime = DateTime.UtcNow;
            IDictionary<String, int> results = null;

            using (MiniProfiler.Current.Step("GetVinPhotoCountsForBusinessUnit"))
            {
                ValidateBusinessUnitCode(businessUnitCode);

                if (WhiteListHelper.IsInWhiteList(businessUnitCode, Settings.PhotoWebServiceV2_BusinessUnit_Whitelist))
                    results = GetVinPhotoCountsForBusinessUnitV2(businessUnitCode, TimeSpan.FromSeconds(Settings.PhotoWebServiceV2Timeout));

                if (results == null)
                {
                    results = CallProxy(p =>
                        {
                            SetChannelOperationTimeout(p, timeout);
                            return p.GetVinPhotoCountsForBusinessUnit(businessUnitCode, (int) timeout.TotalMilliseconds);
                        },
                        "GetVinPhotoCountsForBusinessUnit",
                        ex => new Dictionary<String, int>());
                }
            }

            Log.InfoFormat("GetVinPhotoCountsForBusinessUnit() Finished. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }

        public string GetMainPhotoUrl(string businessUnitCode, string vin)
        {
            ValidateBusinessUnitCode(businessUnitCode);
            ValidateVin(vin);

            return CallProxy(
                p => p.GetMainPhotoUrl(businessUnitCode, vin),
                "GetMainPhotoUrl",
                ex => Settings.MainPhotoPlaceholderUrl) + "?" + DateTime.Now.Ticks;

            //FogBugz ID: 16758 - Uploaded Photo not being displayed.
            //Browser in max selling was not refreshing the image because the url stayed exactly the same.
            //Adding the ticks to the end of the url causes it be different and triggers a refresh.
           
        }

        public IPhotoThumbnailResult GetMainThumbnailUrl(string businessUnitCode, string vin)
        {
            ValidateBusinessUnitCode(businessUnitCode);
            ValidateVin(vin);

            return ConstructThumbnailResult(businessUnitCode, vin);   
        }

        private IPhotoThumbnailResult ConstructThumbnailResult(string businessUnitCode, string vin)
        {
            var settings = GetPhotoWebServiceSettings();

            var url = settings.ImageHostingBaseUrl == null 
                          ? Settings.MainThumbnailPlaceholderUrl
                          : string.Format("{0}{1}/{2}/thumb.jpg", settings.ImageHostingBaseUrl, businessUnitCode, vin);
            var width = settings.ThumbnailWidth == default(int)
                            ? Settings.MainThumbnailWidth
                            : settings.ThumbnailWidth;
            var height = settings.ThumbnailWidth == default(int)
                             ? Settings.MainThumbnailHeight
                             : settings.ThumbnailHeight;

            return new Core.PhotoServices.ThumbnailResult(url, width, height);
        }

        private Settings GetPhotoWebServiceSettings()
        {
            using (MiniProfiler.Current.Step("GetPhotoWebServiceSettings"))
            {
                var cacheKey = typeof (PhotoServices).FullName + "#Settings";
                return CallProxy(p => ReadThroughCache(cacheKey, p.GetSettings),
                                 "GetPhotoWebServiceSettings",
                                 ex => ConstructEmptyPhotoWebServiceSettings());
            }
        }

        private static Settings ConstructEmptyPhotoWebServiceSettings()
        {
            return new Settings();
        }

        private T ReadThroughCache<T>(string key, Func<T> readValue) where T : class
        {
            var raw = Cache.Get(key);

            if (raw == null)
            {
                try
                {
                    raw = readValue();
                }
                catch (Exception ex)
                {
                    raw = ex;
                }

                Cache.Set(key, raw, raw is T ? CacheTimeoutSuccess : CacheTimeoutFailure);
            }

            if (raw is Exception)
                throw ((Exception)raw);
            
            return (T) raw;
        }

        private static void ValidateAuditUser(IMember auditUser)
        {
            if(auditUser == null)
                throw new ArgumentNullException("auditUser");
            if(string.IsNullOrWhiteSpace(auditUser.UserName))
                throw new ArgumentNullException("auditUser", "'auditUser.UserName' must not be null or empty.");
            if(auditUser.FirstName == null)
                throw new ArgumentNullException("auditUser", "'auditUser.Firstname' must not be null.");
            if(auditUser.LastName == null)
                throw new ArgumentNullException("auditUser", "'auditUser.LastName' must not be null.");
        }

        private static void ValidateContext(PhotoManagerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");
        }

        private static void ValidateBusinessUnitCode(string businessUnitCode)
        {
            if (string.IsNullOrWhiteSpace(businessUnitCode))
                throw new ArgumentNullException("businessUnitCode");
        }

        private static void ValidateStockNumber(string stockNumber)
        {
            if (string.IsNullOrWhiteSpace(stockNumber))
                throw new ArgumentNullException("stockNumber");
        }

        private static void ValidateVin(string vin)
        {
            if (string.IsNullOrWhiteSpace(vin))
                throw new ArgumentNullException("vin");
        }

        private T CallProxy<T>(Func<IPhotoWebServices, T> action, string actionName, Func<Exception, T> defaultOnError)
        {
            var proxy = CreateProxy();
            using((IDisposable) proxy)
            {
                try
                {
                    return action(proxy);
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Failed while calling {0}.", actionName), ex);
                    return defaultOnError(ex);
                }
            }
        }

        private IPhotoWebServices CreateProxy()
        {
            var url = Settings.PhotoWebServicesUrl;
            var channel = ProxyFactory.CreateChannel(new EndpointAddress(url));
            SetChannelOperationTimeout(channel, TimeSpan.FromSeconds(15));
            return channel;
        }

        private static void SetChannelOperationTimeout(IPhotoWebServices client, TimeSpan duration)
        {
            var channel = (IClientChannel) client;
            channel.OperationTimeout = duration;
        }

        private VehiclesPhotosResult GetPhotoUrlsByBusinessUnitV2(String businessUnitCode, TimeSpan timeout)
        {
            Log.InfoFormat("GetPhotoUrlsByBusinessUnitV2(businessUnitCode: \"{0}\", timeout: {1}) Started.", businessUnitCode, timeout);
            var startTime = DateTime.UtcNow;
            VehiclesPhotosResult results = null;

            try
            {
                var url = String.Format("{0}/photos/urls/bu_code/{1}", Settings.PhotoWebServicesV2Url, businessUnitCode);
                var request = WebRequest.Create(url) as HttpWebRequest;
                if (request == null)
                    throw new ApplicationException(String.Format("Could not create the WebRequest to URL: {0}", url));
                request.Timeout = (int)timeout.TotalMilliseconds;

                var response = request.GetResponse() as HttpWebResponse;
                if (response == null)
                    throw new ApplicationException(String.Format("Could not get a response from URL: {0}", url));

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new ApplicationException(String.Format("URL {0} responded with status code: {1}\n{2}", url, response.StatusCode, response.StatusDescription));

                var responseStream = response.GetResponseStream();
                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    using (var treader = new JsonTextReader(reader))
                    {
                        var serializer = new JsonSerializer();

                        var photosRoot = serializer.Deserialize<VINsAndPhotoUrlsRoot>(treader);
                        if(photosRoot == null)
                            throw new ApplicationException(String.Format("Error deserializing JSON to usable object"));

                        results = new VehiclesPhotosResult()
                        {
                            Vehicles = photosRoot.VINPhotos.VINPhoto.Select(photo => new VehiclePhotos()
                            {
                                Vin = photo.VIN,
                                PhotoUrls = photo.Urls.Url.ToArray()
                            }).ToArray()
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("GetPhotoUrlsByBusinessUnitV2() Failed. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime)), ex);
            }

            Log.InfoFormat("GetPhotoUrlsByBusinessUnitV2() Finished. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }

        private VehiclesPhotosResult GetPhotoUrlsByVinV2(String businessUnitCode, String vin, TimeSpan timeout)
        {
            Log.InfoFormat("GetPhotoUrlsByVinV2(businessUnitCode: \"{0}\", timeout: {1}) Started.", businessUnitCode, timeout);
            var startTime = DateTime.UtcNow;
            VehiclesPhotosResult results = null;

            try
            {
                var url = String.Format("{0}/photos/urls/bu_code/{1}/vin/{2}", Settings.PhotoWebServicesV2Url, businessUnitCode, vin);
                var request = WebRequest.Create(url) as HttpWebRequest;
                if (request == null)
                    throw new ApplicationException(String.Format("Could not create the WebRequest to URL: {0}", url));
                request.Timeout = (int)timeout.TotalMilliseconds;

                var response = request.GetResponse() as HttpWebResponse;
                if (response == null)
                    throw new ApplicationException(String.Format("Could not get a response from URL: {0}", url));

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new ApplicationException(String.Format("URL {0} responded with status code: {1}\n{2}", url, response.StatusCode, response.StatusDescription));

                var responseStream = response.GetResponseStream();
                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    using (var treader = new JsonTextReader(reader))
                    {
                        var serializer = new JsonSerializer();

                        var photosRoot = serializer.Deserialize<VINsAndPhotoUrlsRoot>(treader);
                        if (photosRoot == null)
                            throw new ApplicationException(String.Format("Error deserializing JSON to usable object"));

                        results = new VehiclesPhotosResult()
                        {
                            Vehicles = photosRoot.VINPhotos.VINPhoto.Select(photo => new VehiclePhotos()
                            {
                                Vin = photo.VIN,
                                PhotoUrls = photo.Urls.Url.ToArray()
                            }).ToArray()
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("GetPhotoUrlsByVinV2() Failed. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime)), ex);
            }

            Log.InfoFormat("GetPhotoUrlsByVinV2() Finished. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }

        private IDictionary<String, int> GetVinPhotoCountsForBusinessUnitV2(String businessUnitCode, TimeSpan timeout)
        {
            Log.InfoFormat("GetVinPhotoCountsForBusinessUnitV2(businessUnitCode: \"{0}\", timeout: {1}) Started.", businessUnitCode, timeout);
            var startTime = DateTime.UtcNow;
            Dictionary<String, int> results = null;

            try
            {
                var url = String.Format("{0}/photos/counts/bu_code/{1}", Settings.PhotoWebServicesV2Url, businessUnitCode);
                var request = WebRequest.Create(url) as HttpWebRequest;
                if (request == null)
                    throw new ApplicationException(String.Format("Could not create the WebRequest to URL: {0}", url));
                request.Timeout = (int)timeout.TotalMilliseconds;

                var response = request.GetResponse() as HttpWebResponse;
                if (response == null)
                    throw new ApplicationException(String.Format("Could not get a response from URL: {0}", url));

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new ApplicationException(String.Format("URL {0} responded with status code: {1}\n{2}", url,
                        response.StatusCode, response.StatusDescription));

                var responseStream = response.GetResponseStream();
                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    using (var treader = new JsonTextReader(reader))
                    {
                        var serializer = new JsonSerializer();

                        results = serializer.Deserialize<VINPhotoCountsRoot>(treader).Counts.ToDictionary(x => x.VIN, x => x.Count);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("GetVinPhotoCountsForBusinessUnitV2() Failed. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime)), ex);
            }

            Log.InfoFormat("GetVinPhotoCountsForBusinessUnitV2() Finished. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }
    }

    #region VIN Photo Count WebService V2 structures
    internal class VINPhotoCountsRoot
    {
        private IEnumerable<VINPhotoCount> _counts;
        public IEnumerable<VINPhotoCount> Counts {
            get { return _counts ?? (_counts = new Collection<VINPhotoCount>()); }
            set { _counts = value; }
        }
    }

    internal class VINPhotoCount
    {
        private String _vin;
        public String VIN
        {
            get { return _vin ?? (_vin = String.Empty); }
            set { _vin = value ?? String.Empty; }
        }
        public int Count { get; set; }
    }

// ReSharper disable once InconsistentNaming
    internal class VINsAndPhotoUrlsRoot
    {
        private VINsAndPhotoUrls _vinPhotos;
        public VINsAndPhotoUrls VINPhotos
        {
            get { return _vinPhotos ?? (_vinPhotos = new VINsAndPhotoUrls()); }
            set { _vinPhotos = value; }
        }
    }
    #endregion

    #region Photo URLs WebService V2 structures
    // ReSharper disable once InconsistentNaming
    internal class VINsAndPhotoUrls
    {
        private IEnumerable<VINAndPhotoUrls> _vinPhoto;
        public IEnumerable<VINAndPhotoUrls> VINPhoto
        {
            get { return _vinPhoto ?? (_vinPhoto = new Collection<VINAndPhotoUrls>()); }
            set { _vinPhoto = value; }
        }
    }

    internal class VINAndPhotoUrls
    {
        private String _vin;
        public String VIN
        {
            get { return _vin ?? (_vin = String.Empty); }
            set { _vin = value ?? String.Empty; }
        }

        private PhotoUrls _urls;
        public PhotoUrls Urls
        {
            get { return _urls ?? (_urls = new PhotoUrls()); }
            set { _urls = value; }
        }
    }

    internal class PhotoUrls
    {
        private IEnumerable<String> _url;
        public IEnumerable<String> Url
        {
            get { return _url ?? (_url = new Collection<String>()); }
            set { _url = value; }
        }
    }
    #endregion
}