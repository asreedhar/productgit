using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.PhotoServices.Properties;

namespace FirstLook.Common.PhotoServices
{
    internal class PhotoServicesSettings : IPhotoServicesSettings
    {
        public PhotoServicesSettings()
        {
            var settings = Settings.Default;
            
            MainThumbnailPlaceholderUrl = settings.MainThumbnailPlaceholderUrl;
            MainThumbnailWidth = settings.MainThumbnailWidth;
            MainThumbnailHeight = settings.MainThumbnailHeight;

            MainPhotoPlaceholderUrl = settings.MainPhotoPlaceholderUrl;

            PhotoManagerNotAvailableUrl = settings.PhotoManagerNotAvailableUrl;
            BulkUploadManagerNotAvailableUrl = settings.BulkUploadManagerNotAvailableUrl;

            PhotoWebServicesUrl = settings.PhotoWebServicesUrl;

            PhotoWebServicesV2Url = settings.PhotoWebServicesV2Url;
            PhotoWebServiceV2_BusinessUnit_Whitelist = settings.PhotoWebServiceV2_BusinessUnit_Whitelist;
            PhotoWebServiceV2Timeout = settings.PhotoWebServiceV2Timeout;
        }

        public string MainThumbnailPlaceholderUrl { get; private set; }

        public int MainThumbnailWidth { get; private set; }

        public int MainThumbnailHeight { get; private set; }

        public string MainPhotoPlaceholderUrl { get; private set; }

        public string PhotoManagerNotAvailableUrl { get; private set; }

        public string BulkUploadManagerNotAvailableUrl { get; private set; }

        public string PhotoWebServicesUrl { get; private set; }

        public string PhotoWebServicesV2Url { get; private set; }

        public string PhotoWebServiceV2_BusinessUnit_Whitelist { get; private set; }

        public int PhotoWebServiceV2Timeout { get; private set; }
    }
}