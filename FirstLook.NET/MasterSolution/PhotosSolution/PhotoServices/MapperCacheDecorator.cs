﻿using System;
using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.Common.PhotoServices
{
    internal class MapperCacheDecorator : IMapper
    {
        private IMapper Inner { get; set; }
        private ICache Cache { get; set; }

        private static readonly string KeyPrefix = typeof(MapperCacheDecorator).FullName + "#";
        private static readonly TimeSpan SlidingCacheWindow = TimeSpan.FromMinutes(30);

        public MapperCacheDecorator(IMapper inner, ICache cache)
        {
            Inner = inner;
            Cache = cache;
        }

        public string MapBusinessUnitIdToBusinessUnitCode(int businessUnitId)
        {
            return ReadThroughCache(
                KeyPrefix + "BUID#" + businessUnitId,
                () => Inner.MapBusinessUnitIdToBusinessUnitCode(businessUnitId));
        }

        public string MapOwnerHandleToBusinessUnitCode(string ownerHandle)
        {
            return ReadThroughCache(
                KeyPrefix + "OWNER#" + ownerHandle,
                () => Inner.MapOwnerHandleToBusinessUnitCode(ownerHandle));
        }

        public string MapVehicleHandleToVin(string vehicleHandle)
        {
            return ReadThroughCache(
                KeyPrefix + "VEHICLE#" + vehicleHandle,
                () => Inner.MapVehicleHandleToVin(vehicleHandle));
        }

        private string ReadThroughCache(string key, Func<string> readValue)
        {
            var raw = Cache.Get(key);

            if (raw == null)
            {
                try
                {
                    raw = readValue();
                }
                catch(Exception ex)
                {
                    // Cache exceptions too
                    raw = ex;
                }
                
                Cache.Set(key, raw, SlidingCacheWindow);
            }

            var exception = raw as Exception;
            if(exception != null)
                throw new DataException("Failed to map values.", exception);
            return (string) raw;
        }
    }
}