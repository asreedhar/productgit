using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.PhotoServices.WebService.V1;
using FirstLook.Common.PhotoServices.Properties;
using MAX.Caching;

namespace FirstLook.Common.PhotoServices
{
    public class PhotoServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c =>
                                 {
                                     var factory = new ChannelFactory<IPhotoWebServices>(
                                         new WebHttpBinding { MaxReceivedMessageSize = 20000000 }, 
                                         new EndpointAddress(Settings.Default.PhotoWebServicesUrl));
                                  
                                     factory.Endpoint.Behaviors.Add(new WebHttpBehavior());

                                     foreach (var operation in factory.Endpoint.Contract.Operations)
                                     {
                                         var behavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();
                                         if (behavior != null)
                                         {
                                             behavior.MaxItemsInObjectGraph = 250000; // Avoid de-serialization error. Must tweak value in PhotoWebServices also!
                                         }  
                                     }

                                     return factory;
                                 })
                .As<IChannelFactory<IPhotoWebServices>>()
                .SingleInstance();

            builder.RegisterType<MemcachedClientWrapper>().As<ICacheWrapper>().SingleInstance();
            builder.RegisterType<CacheKeyBuilder>().As<ICacheKeyBuilder>();
            builder.RegisterType<PhotoServices>().As<IPhotoServices>().SingleInstance();
            
            builder.RegisterType<PhotoServicesIdMap>().As<IPhotoServicesIdMap>().SingleInstance();
            builder.RegisterType<MapperDAL>().Named<IMapper>("dal").SingleInstance();
            builder.Register(c => new MapperCacheDecorator(c.ResolveNamed<IMapper>("dal"), c.Resolve<ICache>()))
                .As<IMapper>().SingleInstance();

            builder.RegisterType<PhotoServicesSettings>().As<IPhotoServicesSettings>().SingleInstance();
        }
    }
}