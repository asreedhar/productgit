﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace SafePhotosSite
{
    public class Global : System.Web.HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }

        /* PhotoQuery
         * https://incisent.aultec.net/photoquery/KRIEGERM02/vinPhotos
         * https://incisent.aultec.net/photoquery/KRIEGERM02/vinPhotos/
         * https://incisent.aultec.net/photoquery/PRIMETOY02/vinPhotos/JTMBK32V385066134
         * 
         * Photos
         *  https://incisent.aultec.net/photos/HONDAOFP02/5J6RE3H41BL042531/main.jpg
         *  https://incisent.aultec.net/photos/HONDAOFP02/5J6RE3H41BL042531/thumb.jpg 
         *   
         */
        private static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("PhotosQuery", "photoquery/{*pathInfo}", "~/Query.aspx");
            routes.MapPageRoute("Photos", "photos/{*pathInfo}", "~/Photos.aspx");
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
