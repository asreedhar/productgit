﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SafePhotosSite
{
    public partial class Photos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Context.Response.ClearContent();
            Context.Response.ClearHeaders();
            Context.Response.ContentType = "image/jpg";
            Context.Response.AddHeader("Content-disposition", "inline;filename=thumb.jpg");

            Context.Response.WriteFile("~/thumb.jpg");
            Context.Response.Flush();
            Context.Response.End();
        }
    }
}