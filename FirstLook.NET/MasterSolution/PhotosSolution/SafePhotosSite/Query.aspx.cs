﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SafePhotosSite.photoquery
{
    public partial class Query : System.Web.UI.Page
    {
        private static string _xml =
            @"<vinPhotos>
                  <vinPhoto>
                    <vin>{0}</vin>
                    <urls>
                      <url>{1}</url>
                    </urls>
                  </vinPhoto>
                </vinPhotos>";

        protected void Page_Load(object sender, EventArgs e)
        {
            string vin = "00000000000000000";

            // could pull the vin off the request if we wanted to.

            var xml = string.Format(_xml, vin, ConfigurationManager.AppSettings["ThumbUrl"]);
            Context.Response.ContentType = "text/xml";
            Context.Response.Write(xml);
        }
    }
}