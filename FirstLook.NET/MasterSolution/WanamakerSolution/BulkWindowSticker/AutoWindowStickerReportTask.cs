﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Text;
using Core.Messaging;
using Merchandising.Messages;
using Merchandising.Messages.AutoWindowSticker;

namespace BulkWindowSticker
{
    public class AutoWindowStickerReportTask : TaskRunner<AutoWindowStickerReport>
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private readonly IWindowStickerRepository _windowStickerRepository;
        private readonly IEmail _emailProvider;
        private readonly string _emailFromAddress;



        public AutoWindowStickerReportTask(IQueueFactory qfactory, IWindowStickerRepository windowStickerRepository, IEmail emailProvider)
            :base(qfactory.CreateWindowStickerReportQueue())
        {
            _windowStickerRepository = windowStickerRepository;
            _emailProvider = emailProvider;

            _emailFromAddress = ConfigurationManager.AppSettings["email_from_address"];

        }

        public override void Process(AutoWindowStickerReport message)
        {

            Log.DebugFormat("Processing AutoWindowStickerReportTask with the following arguements - email: {0} look back day(s): {1}", message.EmailList, message.LookBackDays);

            // check for empty email
            if(String.IsNullOrWhiteSpace(message.EmailList))
            {
                Log.Error("AutoWindowStickerReportTask: no email address in message");
                return;
            }

            var emailList = message.EmailList.Replace(" ", "").Split(',');

            var emailMessage = new MailMessage
                {
                    IsBodyHtml = false,
                    From = new MailAddress(_emailFromAddress, "Max Digital"),
                    Subject = "Window Sticker Report for " + DateTime.Now.ToShortDateString(),
                    SubjectEncoding = Encoding.UTF8
                };

            // create attachment
            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))    // using UTF-8 encoding by default
            {

                GetExportData(message.LookBackDays, writer);

                stream.Position = 0;     // read from the start of what was written
                emailMessage.Attachments.Add(new Attachment(stream, "windowStickerReport.csv", "text/csv"));


                foreach (string emailAddress in emailList)
                {

                    emailMessage.To.Clear();
                    emailMessage.To.Add(emailAddress);

                    emailMessage.Body = "Your window sticker report is attached in CSV format";


                    // catch email error and don't retry task message!!  just log and move on
                    try
                    {
                        //var messageId = _emailProvider.SendEmail(emailMessage);
                        var messageId = _emailProvider.SendRawEmail(emailMessage);
                        Log.InfoFormat("AutoWindowStickerReportTask sent an email to: {0} with the following message ID: {1} ", emailAddress, messageId);

                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("AutoWindowStickerReportTask email bounced for address: {0} . Exception: {1}", emailAddress, ex);
                    }

                }
            }

            Log.Debug("Processing AutoWindowStickerReportTask completed sucessfully");

        }

        private void GetExportData(int days, TextWriter writer)
        {

                // create a header row
                writer.WriteLine("PrintBatchId,BusinessUnitId,BusinessUnit,PrintFileName,CreatedOn,VehicleCount,LastEmailSent,LastAccess,LastAccessBy");

                var history = _windowStickerRepository.GetAutoWindowStickerHistoryReport(days);

                foreach (var autoWindowStickerHistory in history)
                {

                    string lineString = "";

                    lineString += autoWindowStickerHistory.PrintBatchId.ToString(CultureInfo.InvariantCulture);
                    lineString += ",";

                    lineString += autoWindowStickerHistory.BusinessUnitId.ToString(CultureInfo.InvariantCulture);
                    lineString += ",";

                    lineString += autoWindowStickerHistory.BusinessUnit.ToString(CultureInfo.InvariantCulture).Replace(",", " ");
                    lineString += ",";

                    lineString += autoWindowStickerHistory.PrintFileName.ToString(CultureInfo.InvariantCulture);
                    lineString += ",";

                    lineString += autoWindowStickerHistory.CreatedOn.ToString(CultureInfo.InvariantCulture);
                    lineString += ",";

                    lineString += autoWindowStickerHistory.VehicleCount.ToString(CultureInfo.InvariantCulture);
                    lineString += ",";

                    if (autoWindowStickerHistory.LastEmailSent != null)
                        lineString += autoWindowStickerHistory.LastEmailSent.ToString();
                    lineString += ",";

                    if (autoWindowStickerHistory.LastAccess != null)
                        lineString += autoWindowStickerHistory.LastAccess.ToString();
                    lineString += ",";

                    if (autoWindowStickerHistory.LastAccessBy != null)
                        lineString += autoWindowStickerHistory.LastAccessBy.ToString(CultureInfo.InvariantCulture).Replace(",", " ");

                    // write out the line
                    writer.WriteLine(lineString);
                }

                writer.Flush();
        }

    }
}


/*

    public class AutoWindowStickerHistory
    {
        public int PrintBatchId { get; set; }
        public int BusinessUnitId { get; set; }
        public string BusinessUnit { get; set; }
        public string PrintFileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public int VehicleCount { get; set; }
        public DateTime? LastEmailSent { get; set; }
        public DateTime? LastAccess { get; set; }
        public string LastAccessBy { get; set; }
    }



*/