﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Core.Messaging;
using Merchandising.Messages;
using Merchandising.Messages.AutoWindowSticker;

namespace BulkWindowSticker
{
    public class WindowStickerClickStatsTask : TaskRunner<ClickStatsMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly WindowStickerRepository _windowStickerRepository;
        private readonly string _bitlyAccessToken;


        public WindowStickerClickStatsTask(IQueueFactory qfactory, WindowStickerRepository wsRepo)
            : base(qfactory.CreateWindowStickClickStats())
        {
            _windowStickerRepository = wsRepo;
            _bitlyAccessToken = ConfigurationManager.AppSettings["bitly_access_token"];
        }

        public override void Process(ClickStatsMessage message)
        {

            Log.InfoFormat("Processing ClickStatsMessage - short URL:{0}", message.Link);
            
            // call bitly to get click stats
            var bitlyRepository = new BitlyRepository(_bitlyAccessToken);

            var clickResponse = bitlyRepository.LinkClicks(message.Link);

            if (clickResponse.status_code == "200")
            {
                // get rows with some data
                var clicks = clickResponse.data.link_clicks.Where(hits => hits.clicks > 0);

                foreach (var clickData in clicks)
                {
                    _windowStickerRepository.RecordUrlClicks(message.Link, clickData.dt, clickData.clicks);
                    Log.DebugFormat("Recording ClickStatsMessage - short URL:{0}, epoch timestamp: {1}, clicks {2}", message.Link, clickData.dt, clickData.clicks);
                }

            }
            else
            {
                // throw and let taskRunner requeue the message
                throw new ApplicationException(
                    String.Format("Error retrieving click data for: {0} - status code: {1} - status text {2}",
                                  message.Link, clickResponse.status_code, clickResponse.status_txt));
            }


        }

    }
}
