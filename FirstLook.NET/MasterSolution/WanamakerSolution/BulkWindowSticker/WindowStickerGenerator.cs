﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.WindowSticker;
using VehicleTemplatingWebServiceClient.VehicleTemplatingService;


namespace BulkWindowSticker
{
    public class WindowStickerGenerator
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int BusinessUnitId { get; private set; }
        public int TemplateId { get; private set; }


        private VehicleTemplatingService TemplatingService { get; set; }

        public WindowStickerGenerator(int businessUnit, int template)
        {
            BusinessUnitId = businessUnit;
            TemplateId = template;

            TemplatingService = new VehicleTemplatingService();

            Log.DebugFormat("VehicleTemplatingService.url: {0}", TemplatingService.Url);

        }

        public string GeneratateHtml(List<int> inventoryList)
        {
            string html = "";

            // prep tempate
            TemplateTO template = PrepareTemplate();

            // convert inventory
            List<VehicleData> vehicleData = PrepareInventory(inventoryList);

            // call web service
            html = TemplatingService.GenerateMultiHtml(vehicleData.ToArray(), template);

            return html;

        }

        public byte[] GeneratePDF(List<int> inventoryList)
        {
            string html = GeneratateHtml(inventoryList);

            return PdfHelper.GeneratePdf(html);

        }

        public byte[] GeneratePDF(List<int> inventoryList, string princeXmlUrl)
        {
            string html = GeneratateHtml(inventoryList);

            return PdfHelper.GeneratePdf(html, princeXmlUrl);
        }

        protected TemplateTO PrepareTemplate()
        {
            // retrieve the template from the templating service
            TemplateTO template = TemplatingService.GetTemplate(TemplateId);

            // set the template for MMS (QRCodes)
            template = ApplyJustInTimeTransformations(template);

            // set template for print dialog options (buyers guide only)?

            return template;

        }

        protected List<VehicleData> PrepareInventory(List<int> inventoryList)
        {
            // create a list of vehicle data
            List<VehicleData> vehicleList = new List<VehicleData>();

            foreach (var inventory in inventoryList)
            {

                // put together an inventory Id...this could be moved elsewhere
                var vehicleHandle = "1" + inventory;

                // get the vehicle data
                // get the owner handle from the businessUnit
                var owner = Owner.GetOwner(BusinessUnitId);
                if (owner == null)
                    throw new ApplicationException("No owner was found for the businessUnitId " + BusinessUnitId);

                var legacyVehicleAdapter = new LegacyVehicleAdapter("windowStickerGenerator");
                var vtdmVehicleData = legacyVehicleAdapter.Adapt(owner.Handle, vehicleHandle);

                // convert from normall type the type created by the webservice
                var wsVehicleData = GetWebServiceVehicleData(vtdmVehicleData);

                // add to the list
                vehicleList.Add(wsVehicleData);
            }

            return vehicleList;
        }

        private static VehicleData GetWebServiceVehicleData(VehicleTemplatingDomainModel.VehicleData vehicleDataIn)
        {
            // jsut converting from VehicleTemplatingDomainModel.VehicleData to VehicleTemplationg.VehicleData from web service call (serializing object)

            var vehicleDataOut = new VehicleData();

            vehicleDataOut.Model = vehicleDataIn.Model;
            vehicleDataOut.ModelYear = vehicleDataIn.ModelYear;
            vehicleDataOut.Make = vehicleDataIn.Make;
            vehicleDataOut.AgeInDays = vehicleDataIn.AgeInDays;
            vehicleDataOut.BodyStyle = vehicleDataIn.BodyStyle;
            vehicleDataOut.Certified = vehicleDataIn.Certified;
            vehicleDataOut.CertifiedID = vehicleDataIn.CertifiedID;
            vehicleDataOut.Class = vehicleDataIn.Class;
            vehicleDataOut.Description = vehicleDataIn.Description;
            vehicleDataOut.Drivetrain = vehicleDataIn.Drivetrain;
            vehicleDataOut.Engine = vehicleDataIn.Engine;
            vehicleDataOut.Equipment = vehicleDataIn.Equipment.ToArray();
            vehicleDataOut.Packages = vehicleDataIn.Packages.ToArray();
            vehicleDataOut.ExteriorColor = vehicleDataIn.ExteriorColor;
            vehicleDataOut.FuelType = vehicleDataIn.FuelType;
            vehicleDataOut.InteriorColor = vehicleDataIn.InteriorColor;
            vehicleDataOut.Mileage = vehicleDataIn.Mileage;
            vehicleDataOut.Price = vehicleDataIn.Price;
            vehicleDataOut.Series = vehicleDataIn.Series;
            vehicleDataOut.StockNumber = vehicleDataIn.StockNumber;
            vehicleDataOut.Transmission = vehicleDataIn.Transmission;
            vehicleDataOut.Trim = vehicleDataIn.Trim;
            vehicleDataOut.UnitCost = vehicleDataIn.UnitCost;
            vehicleDataOut.Vin = vehicleDataIn.Vin;
            vehicleDataOut.MpgCity = vehicleDataIn.MpgCity;
            vehicleDataOut.MpgHwy = vehicleDataIn.MpgHwy;
            vehicleDataOut.KBBBookValue = vehicleDataIn.KBBBookValue;
            vehicleDataOut.NADABookValue = vehicleDataIn.NADABookValue;

            return vehicleDataOut;
        }

        private TemplateTO ApplyJustInTimeTransformations(TemplateTO template)
        {
            // if business unit does not have access to QR Codes app feature, remove them from the model before sending to pdf service
            var upgradeSettings = GetUpgradeSettings.GetSettings(BusinessUnitId, false);
            if (!upgradeSettings.MAXForSmartphone)
            {
                // business unit does not have "maxForSmartphone" permission; remove all QR codes
                if (template.ContentAreas != null && template.ContentAreas.Length > 0)
                {
                    var ContentAreas_new = new List<ContentArea>(template.ContentAreas.Length);
                    for (var i = 0; i < template.ContentAreas.Length; ++i)
                    {
                        if (template.ContentAreas[i].DataPoint.Key != DynamicDataPointKeys.QRCodeURL)
                        {
                            // re-add all content areas that are not QR codes to a new collection
                            ContentAreas_new.Add(template.ContentAreas[i]);
                        }
                    }
                    template.ContentAreas = ContentAreas_new.ToArray();
                }
            }

            return template;

        }

    }

}
