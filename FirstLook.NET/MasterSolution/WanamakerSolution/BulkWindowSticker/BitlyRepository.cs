﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;


namespace BulkWindowSticker
{
    /*
     *  just a temporary repo until we are on .net 4.5 and use the bitly nuget package
     *  
     * 
     */
    
    public class BitlyRepository
    {
        private readonly string _accessToken;

        public BitlyRepository(string accessToken)
        {
            _accessToken = accessToken;
        }

        public static string AccessToken(string userName, string password)
        {

            string authorizationToken = null;

            var request = (HttpWebRequest)WebRequest.Create(@"https://api-ssl.bitly.com/oauth/access_token");

            request.Method = "POST";

            string authorizationPhrase = string.Format("{0}:{1}", userName, password);
            string encodedAuthorization = Convert.ToBase64String(Encoding.ASCII.GetBytes(authorizationPhrase));
            string credentialPhrase = string.Format("{0} {1}", "Basic", encodedAuthorization);

            request.Headers[HttpRequestHeader.Authorization] = credentialPhrase;

            WebResponse response = request.GetResponse();

            var streamReader = new StreamReader(response.GetResponseStream());

            authorizationToken = streamReader.ReadToEnd();

            return authorizationToken;

        }

        public ShortenResponse Shorten(string longUrl)
        {
            ShortenResponse response;

            using (var webClient = new WebClient())
            {

                string url = @"https://api-ssl.bitly.com/v3/shorten?access_token={0}&longUrl={1}";

                longUrl = Uri.EscapeUriString(longUrl);

                url = string.Format(url, _accessToken, longUrl);

                var jsonResponse = webClient.DownloadString(url);

                var javaScriptSerializer = new JavaScriptSerializer();

                var statusResponse = javaScriptSerializer.Deserialize<StatusResponse>(jsonResponse);

                if(statusResponse.status_code == "200")
                    response = javaScriptSerializer.Deserialize<ShortenResponse>(jsonResponse);
                else
                {
                    response = new ShortenResponse
                    {
                        status_code = statusResponse.status_code,
                        status_txt = statusResponse.status_txt,
                        data = new ShortenResponseData()
                    };
                    
                }
            }

            return response;

        }

        public ClickResponse LinkClicks(string shortUrl)
        {
            ClickResponse clickResponse;


            using (var webClient = new WebClient())
            {

                string url = @"https://api-ssl.bitly.com/v3/link/clicks?access_token={0}&link={1}&unit=hour&units=100&rollup=false&timezone=-5";

                string link = Uri.EscapeUriString(shortUrl);

                url = string.Format(url, _accessToken, link);

                var jsonResponse = webClient.DownloadString(url);

                var javaScriptSerializer = new JavaScriptSerializer();

                var statusResponse = javaScriptSerializer.Deserialize<StatusResponse>(jsonResponse);

                if (statusResponse.status_code == "200")
                    clickResponse = javaScriptSerializer.Deserialize<ClickResponse>(jsonResponse);
                else
                {
                    clickResponse = new ClickResponse
                    {
                        status_code = statusResponse.status_code,
                        status_txt = statusResponse.status_txt,
                        data = new ClickResponseClickData()
                    };

                }
            }

            return clickResponse;

        }


    }

    public class StatusResponse
    {
        public string status_code { get; set; }
        public string status_txt { get; set; }
    }

    
    public class ShortenResponse
    {
        public string status_code { get; set; }
        public string status_txt { get; set; }
        public ShortenResponseData data { get; set; }
    }

    public class ShortenResponseData
    {
        public string new_hash { get; set; }
        public string url { get; set; }
        public string hash { get; set; }
        public string global_hash { get; set; }
        public string long_url { get; set; }
    }

    public class ClickResponseLinkClickData
    {
        public int clicks { get; set; }
        public int dt { get; set; }
    }

    public class ClickResponseClickData
    {
        public List<ClickResponseLinkClickData> link_clicks { get; set; }
        public int tz_offset { get; set; }
        public string unit { get; set; }
        public int unit_reference_ts { get; set; }
        public int units { get; set; } 
    }

    public class ClickResponse
    {
        public ClickResponseClickData data { get; set; }
        public string status_code { get; set; }
        public string status_txt { get; set; }
    }


}
