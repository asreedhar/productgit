﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using Core.Messaging;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.Commands;
using Merchandising.Messages;
using Merchandising.Messages.AutoWindowSticker;

namespace BulkWindowSticker
{
    public class AutoWindowStickerTask : TaskRunner<AutoWindowStickerMessage>
    {
        private readonly IFileStoreFactory _fileStoreFactory;
        private readonly IEmail _emailProvider;
        private readonly IWindowStickerRepository _windowStickerRepository;
        private readonly string _princeUrl;
        private readonly string _s3ExternalUrl;
        private readonly string _bitlyAccessToken;
        private readonly string _emailFromAddress;


        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AutoWindowStickerTask(IQueueFactory qfactory, IFileStoreFactory fFactory, IEmail emailProv, IWindowStickerRepository wsRepo)
            : base(qfactory.CreateAutoWindowSticker())
        {
            _fileStoreFactory = fFactory;
            _emailProvider = emailProv;
            _windowStickerRepository = wsRepo;

            _princeUrl = ConfigurationManager.AppSettings["prince_xml_webservice_url"];
            _s3ExternalUrl = ConfigurationManager.AppSettings["window_stickers_external_url"];
            _bitlyAccessToken = ConfigurationManager.AppSettings["bitly_access_token"];
            _emailFromAddress = ConfigurationManager.AppSettings["email_from_address"];

        }

        public override void Process(AutoWindowStickerMessage message)
        {
            Log.DebugFormat("Processing AutoWindowStickerMessage - BusinessUnitId:{0}, Email:{1}", message.BusinessUnitId, message.Email);

            string s3Key = "";
            int printBatch = -1;

            var optionsCommand = GetUpgradeSettings.GetSettings(message.BusinessUnitId);

            if(optionsCommand.AutoWindowStickerTemplateId == null)
                throw new ApplicationException("No auto window sticker template found");

            int templateId = Convert.ToInt32(optionsCommand.AutoWindowStickerTemplateId);

            // create the generator
            var windowStickerGenerator = new WindowStickerGenerator(message.BusinessUnitId, templateId);

            // get inventory
            var inventoryList = _windowStickerRepository.GetVehiclesToAutoPrint(message.BusinessUnitId, message.MinimumDays).ToList();
            Log.DebugFormat("{0} vehicles found to auto print for businessUnitId {1}", inventoryList.Count(), message.BusinessUnitId);

            if (inventoryList.Any())
            {
                // generate the html and convert to PDF
                string html = windowStickerGenerator.GeneratateHtml(inventoryList.ToList());
                byte[] pdf;
                if (string.IsNullOrEmpty(_princeUrl))
                    pdf = PdfHelper.GeneratePdf(html);
                else
                    pdf = PdfHelper.GeneratePdf(html, _princeUrl);

                // store on s3
                IFileStorage fileStorage = _fileStoreFactory.CreateWindowSticker();

                string fileName = Guid.NewGuid() + ".pdf";
                s3Key = message.BusinessUnitId + @"\" + fileName;
                fileStorage.Write(s3Key, new MemoryStream(pdf));
                Log.DebugFormat("File {0} written to S3 for inventory list: {1}", s3Key, String.Join(",", inventoryList));

                // save print file batch
                printBatch = _windowStickerRepository.SavePrintBatch(message.BusinessUnitId, s3Key, null, "autoWindowStickerTask");

                // save batch detail
                foreach (int inventory in inventoryList)
                {
                    _windowStickerRepository.SavePrintLog(inventory, 1, templateId, true, printBatch);
                    Log.DebugFormat("PrintLog saved - InventoryId:{0}, TemplateId:{1}, PrintBatch:{2}", inventory, templateId, printBatch);
                }
            }

            // send email, separate try catch so window sticker batch is not regenerated on email failure
            // TODO: put email notification in separate message queue, retries if db or sms error without new print batch
            try
                {

                    if(!string.IsNullOrWhiteSpace(message.Email))
                    {
                        var dealerName = _windowStickerRepository.GetDealerName(message.BusinessUnitId);

                        var emailMessage = new MailMessage();
                        emailMessage.IsBodyHtml = true;

                        emailMessage.From = new MailAddress(_emailFromAddress, "Max Digital");
                        emailMessage.Subject = "Mobile Showroom Stickers for " + DateTime.Now.ToShortDateString();

                        // send to each address to track bounces
                        var emailArray = message.Email.Replace(" ", "").Split(',');
                        foreach (string emailAddress in emailArray)
                        {
                            bool bounced = false;
                            string messageId = null;
                            string shortUrl = null;

                            emailMessage.To.Clear();
                            emailMessage.To.Add(emailAddress);

                            if (printBatch > 0)
                                shortUrl = GetShortenUrl(s3Key, emailAddress); // generate short URL for tracking in email

                            emailMessage.Body = GetEmailBody(dealerName, inventoryList.Count(), shortUrl);

                            try
                            {
                                messageId = _emailProvider.SendEmail(emailMessage);
                                //emailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                                Log.InfoFormat("autoWindowStickerTask for BU: {0} sent email to: {1} ", message.BusinessUnitId, emailAddress);

                            }
                            catch (Exception ex)
                            {
                                bounced = true;
                                Log.ErrorFormat("autoWindowStickerTask email bounced for BU: {0} processed {1} address. Exception: {2}", message.BusinessUnitId, emailAddress, ex);
                            }
                            finally
                            {
                                // track the send log
                                if(printBatch > 0)  // only track it batch is created
                                    _windowStickerRepository.SavePrintBatchEmail(printBatch, emailAddress, bounced, messageId, shortUrl);
                            }
                        }
                    }
                
                // notify via log
                Log.InfoFormat("autoWindowStickerTask for BU: {0} processed {1} vehicles ", message.BusinessUnitId, inventoryList.Count());

            }
            catch (ApplicationException ex)
            {
                Log.ErrorFormat("Error processing autoWindowStickerTask for BU: {0} " + ex.Message, message.BusinessUnitId);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error processing autoWindowStickerTask for BU: {0} " + ex, message.BusinessUnitId);
            }

        }
        private string GetEmailBody(string storeName, int vehicleCount, string url)
        {
            // create link to put in email
            string link = "";
            
            link += "<a href=";
            link += url;
            link += ">";
            link += "here";
            link += "</a>";

            var stringWriter  = new StringWriter();
            stringWriter.WriteLine("<html>");
            stringWriter.WriteLine("<head>");
            stringWriter.WriteLine("<title>Window Sticker Email</title>");
            stringWriter.WriteLine("</head>");

            stringWriter.WriteLine("<body>");
            
            stringWriter.WriteLine("<p>Dear {0},</p>", storeName);

            if (vehicleCount > 0)
            {
                stringWriter.WriteLine("<p>You have {0} eligible vehicles that need a MAX Mobile Showroom sticker printed. Click ", vehicleCount.ToString(CultureInfo.InvariantCulture));
                stringWriter.WriteLine(link);
                stringWriter.WriteLine("to access.</p>");
            }
            else
            {
                stringWriter.WriteLine("<p>You have no eligible vehicles to print Mobile Showroom Stickers.</p>");
            }

            stringWriter.WriteLine("<p>If you have any questions or issues please call 1-877-378-5665.</p>");
            stringWriter.WriteLine("<p>Sincerely,</p>");
            stringWriter.WriteLine("<p>MAX Digital</p>");

            stringWriter.WriteLine("</body>");

            stringWriter.WriteLine("</html>");

            string emailBody = stringWriter.ToString();

            return emailBody;
        }
        private string GetShortenUrl(string key, string emailAddress)
        {
            string shortUrl = null;
            string longUrl = null;

            //// replace back slashes and replace with forward slashes, Amazon does not like when accessing from external url
            key = key.Replace(@"\", @"/");

            longUrl = _s3ExternalUrl;
            if (!longUrl.EndsWith(@"/"))  // extra slash causing occational Amazon access problems
                longUrl += @"/";
            longUrl += key;
            longUrl += @"?userName=" + emailAddress;

            var bitlyRepository = new BitlyRepository(_bitlyAccessToken);

            var shortenResponse = bitlyRepository.Shorten(longUrl);

            if (shortenResponse.status_code == "200")
            {
                shortUrl = shortenResponse.data.url;
            }
            else
                throw new ApplicationException("Cannot generate shortened URL: " + shortenResponse.status_txt);

            return shortUrl;

        }
    }
}
