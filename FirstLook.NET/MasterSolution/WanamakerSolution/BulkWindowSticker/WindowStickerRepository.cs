﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using FirstLook.DomainModel.Oltp;
using VehicleTemplatingDomainModel;
using TemplateTypes = VehicleTemplatingDomainModel.TemplateTypes;


namespace BulkWindowSticker
{
    public interface IWindowStickerRepository
    {
        AutoWindowSticker GetAutoWindowSticker(int businessUnitId);
        TemplateInfo[] GetWindowStickerTemplates(int businessUnitId);
        bool HasAutoWindowSticker(int businessUnitId);
        void SaveAutoWindowSticker(int businessUnitId, string email, bool emailBounced, string user, short minimumDays);
        IEnumerable<int> GetVehiclesToAutoPrint(int businessUnitId, short minimumDays);
        IEnumerable<AutoWindowStickerActive> GetDealersToAutoPrint();
        int SavePrintBatch(int businessUnitId, string printFileName, string messageId, string user);
        int SavePrintLog(int inventoryId, int templateTypeId, int @templateId, bool autoPrintTemplate, int? printBatchId);
        int SavePrintBatchEmail(int printBatchId, string email, bool emailBounced, string messageId, string shortUrl);
        IEnumerable<AutoWindowStickerHistory> GetAutoWindowStickerHistory(int businessUnitId, DateTime since);
        string GetDealerName(int businessUnitId);
        IEnumerable<string> GetClickStatsLinks(DateTime since);
        int RecordUrlClicks(string shortUrl, int epochTime, int clicks);
        int RecordPrintBatchAccess(int printBatchId, string accessBy, DateTime? accessOn);
        string GetBulkWindowStickerUrl(int printBatchId);
        IEnumerable<AutoWindowStickerHistory> GetAutoWindowStickerHistoryReport(int lookBackDays);
    }

    public class WindowStickerRepository : IWindowStickerRepository
    { 
        public AutoWindowSticker GetAutoWindowSticker(int businessUnitId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                const string query = @"
                        SELECT AutoWindowStickerId, BusinessUnitId, Email, EmailBounced, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, MinimumDays
	                        FROM settings.AutoWindowSticker
	                        WHERE BusinessUnitId = @BusinessUnitID
                    ";

                var autoWindowSticker = sqlConnection.Query<AutoWindowSticker>(query, new { BusinessUnitId = businessUnitId });
                sqlConnection.Close();

                return autoWindowSticker.FirstOrDefault();

            }
        }

        public TemplateInfo[] GetWindowStickerTemplates(int businessUnitId)
        {

            // get the owner
            var owner = Owner.GetOwner(businessUnitId);

            var template = Template.GetTemplateInfoForOwnerByTemplateType(owner.Handle, TemplateTypes.WindowSticker);

            var sorter = new TemplateInfoSorter();
            template.Sort(sorter);

            return template.ToArray();

        }

        public bool HasAutoWindowSticker(int businessUnitId)
        {
            bool hasWindowSticker = false;
            
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                const string query = @"
                            SELECT BusinessUnitid
	                            FROM settings.Merchandising
	                            WHERE maxForSmartPhone = 1
	                            AND autoWindowStickerTemplateID IS NOT NULL
	                            AND BusinessUnitid = @BusinessUnitID
                    ";

                var autoWindowSticker = sqlConnection.Query<int>(query, new { BusinessUnitId = businessUnitId });
                sqlConnection.Close();

                if (autoWindowSticker.Any())
                    hasWindowSticker = true;

                return hasWindowSticker;

            }

        }

        public void SaveAutoWindowSticker(int businessUnitId, string email, bool emailBounced, string user, short minimumDays)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                var cmd = new SqlCommand
                    {
                        Connection = sqlConnection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "settings.AutoWindowSticker#Upsert"

                    };

                cmd.Parameters.AddWithValue("@BusinessUnitId", businessUnitId);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@EmailBounced", emailBounced ? 1:  0);
                cmd.Parameters.AddWithValue("@User", user);
                cmd.Parameters.AddWithValue("@MinimumDays ", minimumDays);

                cmd.ExecuteNonQuery();

                sqlConnection.Close();

            }

        }

/*
        public void SaveAutoWindowSticker(AutoWindowSticker autoWindowSticker)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConnection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "settings.AutoWindowSticker#Upsert";

                cmd.Parameters.AddWithValue("@BusinessUnitId", autoWindowSticker.BusinessUnitId);
                cmd.Parameters.AddWithValue("@Email", autoWindowSticker.Email);
                cmd.Parameters.AddWithValue("@EmailBounced", autoWindowSticker.EmailBounce);
                cmd.Parameters.AddWithValue("@User", autoWindowSticker.UpdatedBy);

                cmd.ExecuteNonQuery();

                sqlConnection.Close();


            }
        }
*/

        private class TemplateInfoSorter : IComparer<TemplateInfo>
        {
            public int Compare(TemplateInfo x, TemplateInfo y)
            {
                return String.Compare(x.Name, y.Name, StringComparison.Ordinal);
            }
        }

        public IEnumerable<int> GetVehiclesToAutoPrint(int businessUnitId, short minimumDays)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                
                // get setting for maximum vehicles
                int maxVehicles;

                Int32.TryParse(ConfigurationManager.AppSettings["maximum_auto_window_stickers"], out maxVehicles);
                   
                sqlConnection.Open();

                string query = @"
                        SELECT Inventory.InventoryID
	                        FROM Merchandising.workflow.Inventory Inventory
	                        WHERE NOT EXISTS (SELECT *
					                        FROM IMT.WindowSticker.PrintLog PrintLog
					                        WHERE PrintLog.InventoryId = Inventory.InventoryID
					                        AND PrintLog.AutoPrintTemplate = 1
					                        )
	                        AND Inventory.BusinessUnitID = @BusinessUnitID
	                        AND Inventory.InventoryType = 2 -- used only
	                        AND Inventory.ChromeStyleId != -1 -- must have style set
	                        AND Inventory.StatusBucket != 8 -- not offine
	                        AND DATEDIFF(dd, InventoryReceivedDate, GETDATE()) >= @MinDays
                            ORDER BY inventoryReceivedDate
                    ";

                if (maxVehicles > 0)
                    query = query.Replace("SELECT ", "SELECT TOP " + maxVehicles);

                var autoWindowSticker = sqlConnection.Query<int>(query, new { BusinessUnitId = businessUnitId, MinDays = minimumDays });
                sqlConnection.Close();

                return autoWindowSticker;

            }

        }

        public IEnumerable<AutoWindowStickerActive> GetDealersToAutoPrint()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // should we look for vehicles that need printing?
                // do we send an email stating no vehilces
                const string query = @"
                            SELECT Merchandising.businessUnitID, Merchandising.autoWindowStickerTemplateId, AutoWindowSticker.Email, MinimumDays = IsNull(AutoWindowSticker.MinimumDays, 0)
                                FROM Merchandising.settings.Merchandising Merchandising
                                LEFT OUTER JOIN Merchandising.settings.AutoWindowSticker AutoWindowSticker ON Merchandising.businessUnitID = AutoWindowSticker.BusinessUnitId
                                WHERE Merchandising.maxForSmartphone = 1
                                AND Merchandising.autoWindowStickerTemplateId IS NOT NULL
                                ORDER BY Merchandising.businessUnitID
                                  ";

                var autoWindowSticker = sqlConnection.Query<AutoWindowStickerActive>(query);
                sqlConnection.Close();

                return autoWindowSticker;

            }

        }

        public int SavePrintBatch(int businessUnitId, string printFileName, string messageId, string user)
        {
            int printBatchId;

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                var cmd = new SqlCommand
                    {
                        Connection = sqlConnection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "WindowSticker.PrintBatch#Insert"
                    };

                cmd.Parameters.AddWithValue("@BusinessUnitId", businessUnitId);
                cmd.Parameters.AddWithValue("@PrintFileName", printFileName);
                cmd.Parameters.AddWithValue("@MessageID", messageId);
                cmd.Parameters.AddWithValue("@User", user);
                cmd.Parameters.Add("@PrintBatchId", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                printBatchId = Convert.ToInt32(cmd.Parameters["@PrintBatchId"].Value);

                sqlConnection.Close();


            }

            return printBatchId;

        }

        public int SavePrintLog(int inventoryId, int templateTypeId, int @templateId, bool autoPrintTemplate, int? printBatchId)
        {

            int printLogId;

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                var cmd = new SqlCommand
                    {
                        Connection = sqlConnection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "WindowSticker.LastPrintDate#Save"
                    };
                
                cmd.Parameters.AddWithValue("@InventoryId", inventoryId);
                cmd.Parameters.AddWithValue("@TemplateTypeId", templateTypeId);
                cmd.Parameters.AddWithValue("@LatestPrintDate", DateTime.Now);
                cmd.Parameters.AddWithValue("@TemplateId", templateId);
                cmd.Parameters.AddWithValue("@AutoPrintTemplate", autoPrintTemplate);
                cmd.Parameters.AddWithValue("@PrintBatchId", printBatchId);
                cmd.Parameters.Add("@PrintLogId", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                printLogId = Convert.ToInt32(cmd.Parameters["@PrintLogId"].Value);

                sqlConnection.Close();

            }

            return printLogId;
        }

        public int SavePrintBatchEmail(int printBatchId, string email, bool emailBounced, string messageId, string shortUrl)
        {
            int printBatchEmailId;

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                var cmd = new SqlCommand
                    {
                        Connection = sqlConnection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "WindowSticker.PrintBatchEmail#Insert"
                    };

                cmd.Parameters.AddWithValue("@PrintBatchId", printBatchId);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@EmailBounced", emailBounced);
                if (!string.IsNullOrWhiteSpace(messageId))
                    cmd.Parameters.AddWithValue("@MessageId", messageId);
                if (!string.IsNullOrWhiteSpace(shortUrl))
                    cmd.Parameters.AddWithValue("@ShortUrl", shortUrl);
                cmd.Parameters.Add("@PrintBatchEmailId", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                printBatchEmailId = Convert.ToInt32(cmd.Parameters["@PrintBatchEmailId"].Value);

                sqlConnection.Close();

            }

            return printBatchEmailId;

        }

        public IEnumerable<AutoWindowStickerHistory> GetAutoWindowStickerHistory(int businessUnitId, DateTime since)
        {

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT PrintBatch.PrintBatchId, PrintBatch.BusinessUnitId, PrintBatch.PrintFileName, PrintBatch.CreatedOn, batchCount.VehicleCount, LastEmailSent, LastAccess
                            FROM WindowSticker.PrintBatch PrintBatch
                            INNER JOIN (SELECT printBatchID, VehicleCount = COUNT(*)
                                            FROM WindowSticker.PrintBatchLog
                                            GROUP BY printBatchID
                                            ) batchCount ON batchCount.PrintBatchId = PrintBatch.PrintBatchId
                            LEFT OUTER JOIN (
                                        SELECT printBatchID, LastEmailSent = MAX(CreatedOn)
                                            FROM WindowSticker.PrintBatchEmail
                                            WHERE EmailBounced = 0
                                            GROUP BY printBatchID
                                        ) lastSent ON PrintBatch.PrintBatchId = lastSent.PrintBatchId
                            LEFT OUTER JOIN (
                                        SELECT printBatchID, LastAccess = MAX(AccessOn)
                                            FROM WindowSticker.PrintBatchAccessLog
                                            GROUP BY printBatchID
                                        ) lastAccessed ON PrintBatch.PrintBatchId = lastAccessed.PrintBatchId
                            WHERE BusinessUnitId = @BusinessUnitId
                            AND PrintBatch.CreatedOn > @Since
                            ORDER BY CreatedOn DESC
                                  ";

                var autoWindowStickerHistory = 
                    sqlConnection.Query<AutoWindowStickerHistory>(query, new { BusinessUnitId = businessUnitId, Since = since });
                sqlConnection.Close();

                return autoWindowStickerHistory;
            }
        }

        public IEnumerable<AutoWindowStickerHistory> GetAutoWindowStickerHistoryReport(int lookBackDays)
        {

            // find the date to send to stored proc
            lookBackDays = Math.Abs(lookBackDays);

            DateTime since = DateTime.Now.AddDays(-lookBackDays);

            string dateString = since.ToShortDateString();

            var parms = new DynamicParameters();

            parms.Add("since", dateString);
            
            // return the results
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                var autoWindowStickerHistory = sqlConnection.Query<AutoWindowStickerHistory>("WindowSticker.PrintBatchReport", parms, commandType: CommandType.StoredProcedure);

                sqlConnection.Close();

                return autoWindowStickerHistory;
            }
        }

        public string GetDealerName(int businessUnitId)
        {
            string dealerName = "Error retrieving dealer name";

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                try
                {
                    sqlConnection.Open();

                    var sqlCommand = new SqlCommand
                        {
                            Connection = sqlConnection,
                            CommandType = CommandType.StoredProcedure,
                            CommandText = "dbo.Dealer#Fetch"
                        };

                    
                    sqlCommand.Parameters.AddWithValue("@Id", businessUnitId);

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (sqlDataReader.Read())
                        dealerName = sqlDataReader["Name"].ToString();

                    sqlDataReader.Close();
                }
                finally
                {
                    if(sqlConnection.State != ConnectionState.Closed)
                        sqlConnection.Close();
                }
            }
            return dealerName;
        }

        public IEnumerable<string> GetClickStatsLinks(DateTime since)
        {

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                const string query = @"
                            SELECT ShortUrl
	                            FROM WindowSticker.PrintBatchEmail
	                            WHERE ShortUrl IS NOT NULL
	                            AND CreatedOn >= @Since
                                  ";

                var linksToCheck = sqlConnection.Query<string>(query, new { Since = since });
                sqlConnection.Close();

                return linksToCheck;
            }
        }

        public int RecordUrlClicks(string shortUrl, int epochTime, int clicks)
        {
            int printBatchAccessLogId = -1;

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                var cmd = new SqlCommand
                    {
                        Connection = sqlConnection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "WindowSticker.PrintBatchAccessLog#RecordClick"
                    };
                
                cmd.Parameters.AddWithValue("@ShortUrl", shortUrl);
                cmd.Parameters.AddWithValue("@EpochTime", epochTime);
                cmd.Parameters.AddWithValue("@Clicks", clicks);
                cmd.Parameters.Add("@PrintBatchAccessLogId", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                if (cmd.Parameters["@PrintBatchAccessLogId"].Value != Convert.DBNull) // will be null if nothing inserted
                    printBatchAccessLogId = Convert.ToInt32(cmd.Parameters["@PrintBatchAccessLogId"].Value);

                sqlConnection.Close();

            }

            return printBatchAccessLogId;

        }

        public int RecordPrintBatchAccess(int printBatchId, string accessBy, DateTime? accessOn)
        {
            int printBatchAccessLogId;

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                var cmd = new SqlCommand
                {
                    Connection = sqlConnection,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "WindowSticker.PrintBatchAccessLog#Insert"
                };

                cmd.Parameters.AddWithValue("@PrintBatchId", printBatchId);
                cmd.Parameters.AddWithValue("@AccessBy", accessBy);
                if(accessOn != null)
                    cmd.Parameters.AddWithValue("@AccessOn", accessOn);
                cmd.Parameters.Add("@PrintBatchAccessLogId", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();

                printBatchAccessLogId = Convert.ToInt32(cmd.Parameters["@PrintBatchAccessLogId"].Value);

                sqlConnection.Close();

            }

            return printBatchAccessLogId;

        }

        public string GetBulkWindowStickerUrl(int printBatchId)
        {
            string s3Url = ConfigurationManager.AppSettings["window_stickers_external_url"];
            string s3Key;


            // get the s3 key
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                const string query = @"
                            SELECT PrintFileName
	                            FROM WindowSticker.printBatch
	                            WHERE PrintBatchId = @PrintBatchId
                    ";

                var cmd = new SqlCommand
                    {
                        Connection = sqlConnection,
                        CommandType = CommandType.Text,
                        CommandText = query
                    };

                cmd.Parameters.AddWithValue("PrintBatchId", printBatchId);

                var reader = cmd.ExecuteReader();

                if(reader.Read())
                {
                    s3Key = reader["PrintFileName"].ToString();
                }
                else
                {
                    throw new ApplicationException("Error retrieving print batch");
                }

            }


            s3Key = s3Key.Replace(@"\", @"/"); // foxfire hates the wrong slash

            var url = s3Url;
            if (!url.EndsWith(@"/"))  // extra slash causing occational Amazon access problems
                url += @"/";
            url += s3Key;

            return url;


        }

    }



    public class AutoWindowSticker
    {
        public int AutoWindowStickerId { get; set; }
        public int BusinessUnitId { get; set; }
        public string Email { get; set; }
        public bool EmailBounce { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public short MinimumDays { get; set; }
    }

    public class AutoWindowStickerActive
    {
        public int BusinessUnitId { get; set; }
        public int TemplateId { get; set; }
        public string Email { get; set; }
        public short MinimumDays { get; set; }
    }

    public class AutoWindowStickerHistory
    {
        public int PrintBatchId { get; set; }
        public int BusinessUnitId { get; set; }
        public string BusinessUnit { get; set; }
        public string PrintFileName { get; set; }
        public DateTime CreatedOn { get; set; }
        public int VehicleCount { get; set; }
        public DateTime? LastEmailSent { get; set; }
        public DateTime? LastAccess { get; set; }
        public string LastAccessBy { get; set; }
    }


}
