﻿using System;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System.Linq;
using Core.Messaging;

namespace AmazonServices.SES
{
    internal class Email : IEmail
    {

        private static AmazonSimpleEmailServiceClient CreateClient()
        {
            return new AmazonSimpleEmailServiceClient(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
        }

        public string SendEmail(MailMessage message)
        {
            var client = CreateClient();

            var sendRequest = new SendEmailRequest();

            //to
            sendRequest.Destination = new Destination(message.To.Select(x => x.Address).ToList());
            sendRequest.Destination.CcAddresses = message.CC.Select(x => x.Address).ToList();
            sendRequest.Destination.BccAddresses = message.Bcc.Select(x => x.Address).ToList();

            //from
            sendRequest.Source = message.From.Address;
            sendRequest.ReplyToAddresses = message.ReplyToList.Select(x => x.Address).ToList();

            //message
            var amazonMessage = new Message(); 
            amazonMessage.Subject = new Content(message.Subject); 
            amazonMessage.Body = (message.IsBodyHtml) ? 
                new Body { Html = new Content(message.Body) } 
                : new Body { Text = new Content(message.Body)};

            sendRequest.Message = amazonMessage;

            var sendEmialResponse = client.SendEmail(sendRequest);

            // return a message id for tracking via ses
            return sendEmialResponse.MessageId;
        }

        public bool IsHardBounce(Exception e)
        {
            return (e.Message == "Address blacklisted.");
        }

        public string SendRawEmail(MailMessage message)
        {
            // create SES client
            var client = CreateClient();

            var rawMessage = new RawMessage();

            // populate raw message by convertinto mailMessage to a stream
            using (MemoryStream memoryStream = ConvertMailMessageToMemoryStream(message))
            {
                rawMessage.Data = memoryStream;
            }

            // create sendRaw, don't know why we have to set to/from address again
            var request = new SendRawEmailRequest
                {
                    RawMessage = rawMessage,
                    Destinations = message.To.Select(x => x.Address).ToList(),
                    Source = message.From.Address
                };

            // send it and return message ID if all is good
            SendRawEmailResponse response = client.SendRawEmail(request);

            return response.MessageId;

        }

        internal static MemoryStream ConvertMailMessageToMemoryStream(MailMessage message)
        {
            
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            
            var fileStream = new MemoryStream();
            
            ConstructorInfo mailWriterContructor = mailWriterType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(Stream) }, null);
            object mailWriter = mailWriterContructor.Invoke(new object[] { fileStream });


            // a lot of talk of this not working in .net 4.5 vs 4.0
            //http://stackoverflow.com/questions/9595440/getting-system-net-mail-mailmessage-as-a-memorystream-in-net-4-5-beta

            MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);

            if(sendMethod.GetParameters().Length == 2)
                sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true }, null);
            else
                sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true, true }, null);
          
            // .net 4.0 call
            //MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);
            //sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true }, null);

            // .net 4.5 call
            //MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);
            //sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true, true }, null);

            MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", BindingFlags.Instance | BindingFlags.NonPublic);
            closeMethod.Invoke(mailWriter, BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
            
            return fileStream;
        }
    }
}
