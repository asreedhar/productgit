﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.Extensions;

namespace AmazonServices
{
    /// <summary>
    /// Used to check Amazon Resource names.
    /// </summary>
    public static class GlobalNameUtility
    {
        private const int S3_GLOBAL_MAX = 63;
        private const int S3_GLOBAL_MIN = 3;
        private const string GLOBAL_SEPARATOR = "-";

        // deprecated
        private const int S3_US_STD_MAX = 80;
        private const string S3_US_STD_SEPARATOR = "_";

        public static string Name(Type t, string postfix, bool useGlobalNamingRules = false, bool autoTruncateName = true)
        {
            string separator = Separator(useGlobalNamingRules);

            // We replace the periods in class names with the separator
            var fullName = t.FullName.Replace(".", separator);

            return Name(fullName, postfix, useGlobalNamingRules, autoTruncateName);
        }


        public static string Name(string name, string postfix, char postfixSeperator, bool useGlobalNamingRules = false, bool autoTruncateName = true)
        {
            return Name(name, postfix, new string(postfixSeperator, 1), useGlobalNamingRules, autoTruncateName);
        }

        public static string Name(string name, string postfix, bool useGlobalNamingRules = false, bool autoTruncateName = true)
        {
            var separator = Separator(useGlobalNamingRules);
            return Name(name, postfix, separator, useGlobalNamingRules, autoTruncateName);
        }

        public static string Name(string name, string postfix, string postfixSeperator, bool useGlobalNamingRules = false, bool autoTruncateName = true)
        {
            name = AppendPostFix(name, postfix, postfixSeperator, useGlobalNamingRules);
            name = Truncate(name, useGlobalNamingRules, autoTruncateName);
            if (IsValidName(name, useGlobalNamingRules))
            {
                return name;
            }
            throw new ArgumentException("The bucket name '{0}' does not conform to the naming rules.", name);
        }

        // Internal methods 

        internal static string AppendPostFix(string name, string postFix, string postFixSeparator, bool useGlobalNamingRules = false)
        {
            if (string.IsNullOrWhiteSpace(postFix))
            {
                return name;
            }

            if (string.IsNullOrWhiteSpace(postFixSeparator))
            {
                postFixSeparator = Separator(useGlobalNamingRules);
            }

            return name + postFixSeparator + postFix;
        }

        internal static string Truncate(string name, bool useGlobalNamingRules = false, bool autoTruncateName = false)
        {
            if (!autoTruncateName) return name;
            if (useGlobalNamingRules) return name.Right(S3_GLOBAL_MAX);
            return name.Right(S3_US_STD_MAX);
        }

        internal static string Separator(bool useGlobalNamingRules = false)
        {
            string separator = useGlobalNamingRules ? GLOBAL_SEPARATOR : S3_US_STD_SEPARATOR;
            return separator;
        }

        internal static int MaxNameLength(bool useGlobalNamingRules = false)
        {
            return useGlobalNamingRules ? S3_GLOBAL_MAX : S3_US_STD_MAX;
        }

        internal static int MinNameLength()
        {
            return S3_GLOBAL_MIN;
        }

        internal static bool IsValidLength(string name, bool useGlobalNamingRules = false)
        {
            if (name.Length > MaxNameLength(useGlobalNamingRules)) return false;
            if (name.Length < MinNameLength()) return false;
            return true;
        }

        internal static bool IsValidName(string name, bool useGlobalNamingRules = false)
        {
            // these rules apply everywhere
            if (!IsValidLength(name, useGlobalNamingRules)) return false;

            // can't start or end with a period or a dash
            if (name.StartsWith(".")) return false;
            if (name.EndsWith(".")) return false;
            if (name.StartsWith("-")) return false;
            if (name.EndsWith("-")) return false;

            // no repeated periods, dashes, or sequences of periods and dashes
            if (name.Contains("..")) return false;
            if (name.Contains("--")) return false;
            if (name.Contains("-.")) return false;
            if (name.Contains(".-")) return false;

            // no spaces
            if (name.Contains(" ")) return false;

            // must not be formatted as an IP address
            var ip = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
            if (ip.IsMatch(name)) return false;

            // all digits are numeric is ok.
            var numeric = new Regex(@"\b[0-9]+\b");
            if (numeric.IsMatch(name)) return true;

            // stricter rules are enforced for all other regions and _should_ be used everywhere
            // to be DNS compliant. see: http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html
            if (useGlobalNamingRules)
            {
                // no upper-case allowed
                if (name.Any(char.IsUpper)) return false;

                // no underscores
                if (name.Contains("_")) return false;

                // only allowed characters are period, alphanumeric, and dash. can't repeat period or dash.
                // can't start with period or dash. must start and end with an alpha-numeric character.
                var format = new Regex(@"\b([a-z0-9]+((-)?|(\.)?))+[a-z0-9]+\b");
                return format.IsMatch(name);
            }

            return true;
        }
    }
}
