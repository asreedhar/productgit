﻿using Core.Messages;

namespace AmazonServices.SQS
{
    internal class AmazonMessage<TValue> : IMessage<TValue>
    {
        public AmazonMessage(TValue data)
        {
            Object = data;
        }

        public TValue Object { get; internal set; }
        public string ClaimCheck { get; internal set; }
        public string ReceiptHandle { get; internal set; }
    }
}
