﻿using System;
using AmazonServices.SNS;
using Core.Messages;
using Core.Messaging;

namespace AmazonServices.SQS
{
    /// <summary>
    /// Factory of Amazon Queues.
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public static class AmazonQueueFactory<TValue> where TValue : class, IMessageBase
    {
        public static IQueue<TValue> CreateAmazonQueue(string queueName, char nameDelimiter = '_', bool useGlobalNamingRules = false)
        {
            var queue = new AmazonQueue<TValue>(queueName, nameDelimiter, useGlobalNamingRules)
            {
                DeadLetterBus = BusFactory<DeadLetterMessage, DeadLetterMessage>.Create("Incisent_Core_DeadLetter"),
            };

            // We have alarms attached to "Incisent_Core_DeadLetter" so for archiving 
            // just subscribe two queues to the same bus. They can both be notified at the same time.
            var deadLetterQueue = new AmazonQueue<DeadLetterMessage>("Incisent_Core_DeadLetter");
            var deadLetterArchiveQueue = new AmazonQueue<DeadLetterMessage>("Incisent_Core_DeadLetter_Archive");

            queue.RegisterDeadLetterQueue(deadLetterQueue);
            queue.RegisterDeadLetterQueue(deadLetterArchiveQueue);

            return queue;
        }

        public static IQueue<TValue> CreateAmazonQueue(string queueName, TimeSpan visibilityTimeout, char nameDelimiter = '_', bool useGlobalNamingRules = false)
        {
            var queue = new AmazonQueue<TValue>(queueName, visibilityTimeout)
            {
                DeadLetterBus = BusFactory<DeadLetterMessage, DeadLetterMessage>.Create("Incisent_Core_DeadLetter"),
            };

            // We have alarms attached to "Incisent_Core_DeadLetter" so for archiving 
            // just subscribe two queues to the same bus. They can both be notified at the same time.
            var deadLetterQueue = new AmazonQueue<DeadLetterMessage>("Incisent_Core_DeadLetter");
            var deadLetterArchiveQueue = new AmazonQueue<DeadLetterMessage>("Incisent_Core_DeadLetter_Archive");
            
            queue.RegisterDeadLetterQueue(deadLetterQueue);
            queue.RegisterDeadLetterQueue(deadLetterArchiveQueue);

            return queue;
        }

    }
}