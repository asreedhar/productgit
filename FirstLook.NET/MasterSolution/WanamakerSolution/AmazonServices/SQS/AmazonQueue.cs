﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using AmazonServices.S3;
using Core.Messages;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;

namespace AmazonServices.SQS
{
    /// <summary>
    /// An IQueue implemntation based on amazon sqs queues.
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    internal class AmazonQueue<TValue> : IQueue<TValue> where TValue : class, IMessageBase
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly TimeSpan DefaultVisibilityTimeout = TimeSpan.FromMinutes(30);
        private const int DEAD_MESSAGE_RECEIVE_COUNT = 432; // 72hr @ 6/hr, see notes in TaskRunner.DefaultRequeueDelay
        internal const int QUEUE_MESSAGE_LIMIT = 262144; // 255 KB

        // the S3 bucket in which claim-check payloads will be stored
        private const string CLAIM_CHECK_FILE_STORE = "Incisent_SQS_ClaimCheck";

        public IBus<DeadLetterMessage, DeadLetterMessage> DeadLetterBus { get; set; }

        //Number of times a message can error without going on the deadletter queue
        public int DeadLetterErrorLimit { get; set; }

        #region Contruction and Finalization

        /// <summary>
        /// Create AmazonQueues via the AmazonQueueFactory
        /// </summary>
        internal AmazonQueue(string queueName, char nameDelimiter = '_', bool useGlobalNamingRules = false)
            : this(queueName, DefaultVisibilityTimeout, nameDelimiter, useGlobalNamingRules)
        {
        }

        internal AmazonQueue(string queueName, TimeSpan visibilityTimeout, char nameDelimiter = '_', bool useGlobalNamingRules = false)
        {
            queueName = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(queueName, nameDelimiter, useGlobalNamingRules);

            DeadLetterErrorLimit = DEAD_MESSAGE_RECEIVE_COUNT;
            Log.DebugFormat("Constructing queue named '{0}' with visibility timeout '{1}'", queueName, visibilityTimeout);

            try
            {
                Queue = SqsClient();

                try
                {
                    //see if queue exists. If it throws AmazonSQSException we can expect it doesn't exist
                    GetQueueUrlRequest urlRequest = new GetQueueUrlRequest {QueueName = queueName};
                    var urlRequestResponse = Queue.GetQueueUrl(urlRequest);
                    QueueUrl = urlRequestResponse.GetQueueUrlResult.QueueUrl;
                }
                catch (AmazonSQSException)
                {
                }

                if (QueueUrl == null) //if we couldn't get the queue from the code above we create it
                {
                    CreateQueueRequest sqsRequest = new CreateQueueRequest { QueueName = queueName};
                    sqsRequest.Attributes.Add("VisibilityTimeout", visibilityTimeout.TotalSeconds.ToString());

                    CreateQueueResponse createQueueResponse = Queue.CreateQueue(sqsRequest);
                    QueueUrl = createQueueResponse.CreateQueueResult.QueueUrl;
                }

                // Get ARN
                GetQueueAttributesRequest queueArnRequest = new GetQueueAttributesRequest { QueueUrl = QueueUrl };
                queueArnRequest.AttributeNames.Add("QueueArn");
                
                GetQueueAttributesResponse response = Queue.GetQueueAttributes(queueArnRequest);

                QueueArn = response.QueueARN;

                // set max message size            
                SetQueueAttributesRequest request = new SetQueueAttributesRequest {QueueUrl = QueueUrl };
                request.Attributes.Add("MaximumMessageSize", QUEUE_MESSAGE_LIMIT.ToString());

                Queue.SetQueueAttributes(request);
            }
            catch (Exception ex)
            {
                Log.Error("Exception raised in AmazonQueue constructor.", ex);
            }
        }

        #endregion

        internal void RegisterDeadLetterQueue(IQueue<DeadLetterMessage> queue)
        {
            DeadLetterBus.Subscribe(queue);
        }

        /// <summary>
        /// This should only be called by tests.  We currently have no use cases in which clients would delete queues.
        /// </summary>
        internal void DeleteQueue()
        {
            Log.WarnFormat("Deleting queue with url '{0}'", QueueUrl);
            Queue.DeleteQueue(new DeleteQueueRequest { QueueUrl = QueueUrl } );
        }


        private static IAmazonSQS SqsClient()
        {
            return AWSClientFactory.CreateAmazonSQSClient(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
        }

        protected IAmazonSQS Queue { get; private set; }
        protected string QueueUrl { get; private set; }


        private static IFileStorage CreateFileStore()
        {
            Log.Debug("Creating filestore to store message payload.");

            // Should all SQS messages store their claim-check payload in the same S3 bucket? 
            return new FileStore(CLAIM_CHECK_FILE_STORE);
        }

        /// <summary>
        /// Build an AmazonMessage from the supplied message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns>An AmazonMessage or null.  Callers should be prepared to deal with null.</returns>
        internal static AmazonMessage<TValue> BuildMessage(Message message)
        {
            #region Various failure scenarios
            // There's nothing useful we can do with a null message.
            if( message == null )
            {
                Log.Error("Null mesage encountered.");
                return null;
            }

            // If the message has no body, we're not going to be able to cast it to our wrapper nor will it be a valid claim check.
            if( string.IsNullOrWhiteSpace(message.Body))
            {
                Log.ErrorFormat("The message {0} has a null or empty Body.", message.MessageId);
                return null;
            }

            // Assume that the body is always a Wrapper.  The wrapper.Message is either a guid (claimcheck) or a json-encoded TValue.
            Wrapper wrapper;
            
            try
            {
                wrapper = message.Body.FromJson<Wrapper>();                
            }
            catch(Exception ex)
            {
                Log.Fatal(String.Format("Error casting message body from json to Wrapper. Message body: {0}", message.Body), ex);
                return null;
            }

            if( wrapper == null )
            {
                Log.Error("Wrapper is null");
                return null;
            }

            if( string.IsNullOrWhiteSpace(wrapper.Message) )
            {
                Log.Error("Wrapper has a null or empty message.");
                return null;
            }

            #endregion

            AmazonMessage<TValue> returnedMessage;

            try
            {
                Guid guid;
                if (Guid.TryParse(wrapper.Message, out guid)) //claim check
                {
                    Log.Debug("Guid encountered in message body. Message is interpreted as claim-check.");
                    var store = CreateFileStore();
                    var claimCheck = wrapper.Message;

                    using (var stream = store.Read(claimCheck))
                    using (var reader = new StreamReader(stream))
                    {
                        string json = reader.ReadToEnd();
                        returnedMessage = new AmazonMessage<TValue>(json.FromJson<TValue>())
                        {
                            ClaimCheck = claimCheck,
                            ReceiptHandle = message.ReceiptHandle
                        };
                    }
                }
                else
                {
                    // try to cast the message.body
                    var data = wrapper.Message.FromJson<TValue>();
                    returnedMessage = new AmazonMessage<TValue>(data)
                    {
                        ClaimCheck = null,
                        ReceiptHandle = message.ReceiptHandle
                    };

                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered in AmazonQueue.BuildMessage()", ex);
                return null;
            }

            return returnedMessage;
        }

        /// <summary>
        /// Send the message, return the message id.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="secondDelay"></param>
        /// <param name="sent"></param>
        /// <param name="sentFrom"></param>
        /// <returns>Message id</returns>
        internal string SendMessageReturnId(TValue message, int? secondDelay, DateTime? sent, string sentFrom) //secondDelay: nullabe because if set a zero value will overwrite the queue delay value if set
        {
            if( message == null )
            {
                // not much we can do with a null message
                Log.Error("SendMessageReturnId() called with a null message.");
                return string.Empty;
            }

            message.SentFrom = sentFrom;
            
            if (message.Errors.Length >= DeadLetterErrorLimit) //sent too many times. Put on deadletter. Don't send anymore
            {
                Log.ErrorFormat("Message removed from queue. ErrorCount = {0}", message.Errors.Length);
                DeadLetterMessage deadLetter = new DeadLetterMessage(message);
                DeadLetterBus.Publish(deadLetter, GetType().FullName);
                return string.Empty;
            }

            if(sent.HasValue)
                message.Sent = sent.Value;

            string jsonString = message.ToJson();

            string messageString = jsonString;
            if (messageString.Length > QUEUE_MESSAGE_LIMIT) //claim check
            {
                Log.DebugFormat("Messaage length '{0}' exceeds limit.  Message will be processed as a claim-check.", messageString.Length);

                var store = CreateFileStore();
                string guidString = Guid.NewGuid().ToString();

                Log.DebugFormat("Message claim-check key is '{0}'.", guidString);

                using (var stream = new MemoryStream())
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(messageString);    // write the json-encoded message to the stream
                    writer.Flush();
                    stream.Position = 0;
                    store.Write(guidString, stream);    // write the stream to the filestore, using the specified guid as the key/filename
                }

                messageString = guidString;
            }

            // Either way, we should now create a Wrapper.
            // TODO: I'm pretty sure we should be doing this before the claim-check test.  It's possible we are _now_ too long.
            //       I'm not going to change this today (it's code freeze) but it should be revisited in 10.0
            var wrapper = new Wrapper(messageString);

            // Convert the wrapper to JSON and send it.
            string json = wrapper.ToJson();

            SendMessageRequest sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = QueueUrl,
                MessageBody = json
            };

            if (secondDelay.HasValue) //only set if seconds has value as to not overwrite the queue setting every time.
            {
                int delay = secondDelay.Value;

                if (delay < 900)
                {
                    sendMessageRequest.DelaySeconds = secondDelay.Value;                    
                }
                else
                {
                    sendMessageRequest.DelaySeconds = 900;                                        
                }
/*
                else if (delay < MaxVisibilityTimeoutSeconds())
                {
                    // need to issue change message visibility request w/ receipt handle, but can't track errors on the message this way.
                }
*/
            }

            var sendResposne = Queue.SendMessage(sendMessageRequest);
            return sendResposne.SendMessageResult.MessageId;
        }

        internal void SendMessageReturnAsync(TValue message, int? secondDelay, DateTime? sent, string sentFrom) //secondDelay: nullabe because if set a zero value will overwrite the queue delay value if set
        {
            if (message == null)
            {
                // not much we can do with a null message
                Log.Error("SendMessageReturnAsync() called with a null message.");
                return;
            }

            message.SentFrom = sentFrom;

            if (message.Errors.Length >= DeadLetterErrorLimit) //sent too many times. Put on deadletter. Don't send anymore
            {
                Log.ErrorFormat("Message removed from queue. ErrorCount = {0}", message.Errors.Length);
                DeadLetterMessage deadLetter = new DeadLetterMessage(message);
                DeadLetterBus.PublishAync(deadLetter, GetType().FullName);
                return;
            }

            if (sent.HasValue)
                message.Sent = sent.Value;

            string jsonString = message.ToJson();

            string messageString = jsonString;
            if (messageString.Length > QUEUE_MESSAGE_LIMIT) //claim check
            {
                Log.DebugFormat("Messaage length '{0}' exceeds limit.  Message will be processed as a claim-check.", messageString.Length);

                var store = CreateFileStore();
                string guidString = Guid.NewGuid().ToString();

                Log.DebugFormat("Message claim-check key is '{0}'.", guidString);

                using (var stream = new MemoryStream())
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(messageString);    // write the json-encoded message to the stream
                    writer.Flush();
                    stream.Position = 0;
                    store.Write(guidString, stream);    // write the stream to the filestore, using the specified guid as the key/filename
                }

                messageString = guidString;
            }

            // Either way, we should now create a Wrapper.
            // TODO: I'm pretty sure we should be doing this before the claim-check test.  It's possible we are _now_ too long.
            //       I'm not going to change this today (it's code freeze) but it should be revisited in 10.0
            var wrapper = new Wrapper(messageString);

            // Convert the wrapper to JSON and send it.
            string json = wrapper.ToJson();

            SendMessageRequest sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = QueueUrl,
                MessageBody = json
            };

            if (secondDelay.HasValue) //only set if seconds has value as to not overwrite the queue setting every time.
            {
                int delay = secondDelay.Value;

                if (delay < 900)
                {
                    sendMessageRequest.DelaySeconds = secondDelay.Value;
                }
                else
                {
                    sendMessageRequest.DelaySeconds = 900;
                }
                /*
                                else if (delay < MaxVisibilityTimeoutSeconds())
                                {
                                    // need to issue change message visibility request w/ receipt handle, but can't track errors on the message this way.
                                }
                */
            }
            Queue.BeginSendMessage(sendMessageRequest,EndSendMessage,Queue);
            
        }

        private void EndSendMessage(IAsyncResult asyncResult)
        {
            var client = asyncResult.AsyncState as IAmazonSQS ??
                             Queue;
            client.EndSendMessage(asyncResult);
        }
/*
        private int MaxVisibilityTimeoutSeconds()
        {
            // max is 12 hours minus the invisibility timeout already allocated to the message
            const int ABSOLUTE_MAX = 12*60*60;
            return (int) (ABSOLUTE_MAX - DefaultVisibilityTimeout.TotalSeconds);
        }
*/

        /// <summary>
        /// Send the specified message to the dead letter queue. Record any comments as the final error entry in the message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public bool SendToDeadLetter(TValue message, string comments = "")
        {
            try
            {
                if (comments.Length > 0)
                {
                    // TODO: Track these final comments on their own field.
                    message.AddError(comments);
                }

                var deadLetter = new DeadLetterMessage(message);
                string msgId = DeadLetterBus.Publish(deadLetter, GetType().FullName);
                if (msgId.Length > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return false;
        }

        #region IQueue<TValue>

        public void SendMessage(TValue message, string sentFrom)
        {
            try
            {
                // send it but don't return the Id
                SendMessageReturnId(message, new int?(), DateTime.UtcNow, sentFrom);
            }
            catch (Exception ex)
            {
                Log.Error("Error encountered sending message", ex);
            }
        }
        public void SendMessageAsync(TValue message, string sentFrom)
        {
            try
            {
                // send it but don't return the Id
                SendMessageReturnAsync(message, new int?(), DateTime.UtcNow, sentFrom);
            }
            catch (Exception ex)
            {
                Log.Error("Error encountered sending message from SendMessageAsync", ex);
            }
        }

        public void SendMessage(TValue message, TimeSpan delay, string sentFrom)
        {
            try
            {
                // send it but don't return the Id
                SendMessageReturnId(message, (int)delay.TotalSeconds, DateTime.UtcNow, sentFrom);
            }
            catch (Exception ex)
            {
                Log.Error("Error encountered sending message", ex);
            }
        }

        /// <summary>
        /// Return the first message from the queue.
        /// </summary>
        /// <returns>The first message or null.</returns>
        public IMessage<TValue> GetMessage()
        {
            try
            {
                var messages = GetMessages(1);
                return messages.Count() > 0 ? messages.First() : null;
            }
            catch (Exception ex)
            {
                Log.Error("An error was encountered getting the message", ex);
                return null;
            }
        }

        //pump messages that aren't ready to process and find ones that are
        private IList<IMessage<TValue>> PumpMessages(int count)
        {
            var returnedMessages = new List<IMessage<TValue>>();

            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest
            {
                    QueueUrl = QueueUrl,
                    MaxNumberOfMessages = count,
                };

            receiveMessageRequest.AttributeNames.Add("ApproximateReceiveCount");
            
            ReceiveMessageResponse receiveMessageResponse = Queue.ReceiveMessage(receiveMessageRequest);
            
            while(receiveMessageResponse.Messages.Count > 0)
            {
                foreach (Message message in receiveMessageResponse.Messages)
                {
                    var returnMessage = BuildMessage(message);

                    if (returnMessage == null)
                    {
                        // We can't even delete it from the queue, as DeleteMessage() expects a non-null.  
                        // TODO: What to do here?
                        // Delete the message since we will never be able to process this.
                        Log.Fatal("BuildMessage returned a null message. We can't even delete it. You should look into this.");
                        DeleteMessage(message);
                        continue;
                    }

                    if(DateTime.UtcNow > returnMessage.Object.Sent + returnMessage.Object.Delay)
                    {
                        returnedMessages.Add(returnMessage);
                    }
                    else //Resend message. Isn't ready to processes so we recyle it.
                    {
                        SendMessageReturnId(returnMessage.Object, (int)TimeSpan.FromMinutes(15).TotalSeconds, null, returnMessage.Object.SentFrom);
                        DeleteMessage(returnMessage);
                    }   
                }

                if (returnedMessages.Count >= count)
                    break;

                //read more
                receiveMessageResponse = Queue.ReceiveMessage(receiveMessageRequest);
            }

            return returnedMessages;
        }

        /// <summary>
        /// Return the first "count" messages from the queue.
        /// </summary>
        /// <param name="count"></param>
        /// <returns>An IList of the messages.</returns>
        public IList<IMessage<TValue>> GetMessages(int count)
        {
            try
            {
                return PumpMessages(count);
            }
            catch (Exception ex)
            {
                Log.Error("Error encountered getting messages from the queue", ex);
                return new List<IMessage<TValue>>();
            }

        }

        public void DeleteMessage(Message message)
        {
            try
            {
                var deleteRequest = new DeleteMessageRequest { QueueUrl = QueueUrl, ReceiptHandle = message.ReceiptHandle };
                Queue.DeleteMessage(deleteRequest);
            }
            catch (Exception ex)
            {
                Log.Error("Error deleting message.", ex);
            }
        }

        public void DeleteMessage(IMessage<TValue> message)
        {
            try
            {
                var deleteRequest = new DeleteMessageRequest { QueueUrl = QueueUrl, ReceiptHandle = message.ReceiptHandle };
                Queue.DeleteMessage(deleteRequest);

                if (string.IsNullOrEmpty(message.ClaimCheck)) return;

                var store = CreateFileStore();
                store.Delete(message.ClaimCheck);
            }
            catch (Exception ex)
            {
                Log.Error("Error deleting message.", ex);
            }

        }

        public string QueueArn { get; set; }

        public string Url
        {
            get { return QueueUrl; }
        }

        public void DelayMessage(string receiptHandle, int seconds)
        {
            var request = new ChangeMessageVisibilityRequest
            {
                QueueUrl = QueueUrl,
                ReceiptHandle = receiptHandle,
                VisibilityTimeout = seconds
            };

            var response = Queue.ChangeMessageVisibility(request);
        }

        #endregion

        // Made internal for testing.
        internal class Wrapper
        {
            //do not set the setter to private. Causes issues with the JSON serializer for some reason. Maybe because the class is private?
            public string Message { get; set; }

            // Needed for json deserialization
            public Wrapper()
            {
                Message = String.Empty;
            }

            internal Wrapper(string message)
            {
                Message = message;
            }
        }
    }
}
