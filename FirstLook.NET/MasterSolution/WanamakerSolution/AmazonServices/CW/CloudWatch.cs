﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using FirstLook.Common.Core.Extensions;

namespace AmazonServices.CW
{
    /// <summary>
    /// Thread safe Amazon CloudWatch custom metrics publisher. Defaults to 1 second metric samples and 1 minute publish to Amazon intervals.
    /// </summary>
    public sealed class CloudWatch
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // TODO: move to config
        private static readonly TimeSpan _maxPublishRate = TimeSpan.FromMinutes(5);
        // TODO: move to config
        private static readonly TimeSpan _counterAccumulatorResolution = TimeSpan.FromMinutes(1);
        //
        private static readonly StandardUnit _metricUnit = StandardUnit.Count;


        private static volatile CloudWatch _instance;
        /// <summary>
        /// The dictionary's state is only deterministic within a locked section (1:N)
        /// </summary>
        private MetricAccumulator _metricAccumulator;
        /// <summary>
        /// Only the worker's completed event can invoke the timer (1:1)
        /// </summary>
        private readonly Timer _accumulatorTimer;

        public CloudWatchPublisher _cloudWatchPublisher;
        
        private static readonly object _syncRoot = new object();

        private CloudWatch()
        {
            if ((ConfigurationManager.AppSettings["CloudWatchMetricsActive"] ?? "true").ToLower().Equals("true"))
            {
                _metricAccumulator = new MetricAccumulator(_metricUnit);
                _cloudWatchPublisher = new CloudWatchPublisher(_maxPublishRate);
                _accumulatorTimer = new Timer(_counterAccumulatorResolution.TotalMilliseconds);

                _accumulatorTimer.Elapsed += AccumulatorTimerOnElapsed;
                _accumulatorTimer.AutoReset = true;
                _accumulatorTimer.Start();
                if (_metricAccumulator != null && _cloudWatchPublisher != null && _accumulatorTimer.Enabled)
                {
                    Log.Info("CloudWatchPublisher, metric accumulator and timer initialized and running!");
                }
                else
                {
                    Log.Info("CloudWatchPublisher, metric accumulator or timer did not initialize or start! Something unexpected is happening.");
                }
            }
            else
            {
                Log.Warn("AppSettings CloudWatchMetricsActive set to false, we aren't going to do anything ;_;");
            }

        }

        /// <summary>
        /// Publish remaining metrics on graceful shutdown
        /// </summary>
        ~CloudWatch()
        {
            lock (_syncRoot)
            {
                // This destructor can be called even when these object are not initialized
                //
                if (_cloudWatchPublisher != null && _metricAccumulator != null)
                {
                    _cloudWatchPublisher.PublishMetrics(_metricAccumulator);
                }
            }
        }

        private void AccumulatorTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            lock (_syncRoot)
            {
                _cloudWatchPublisher.PublishMetrics(_metricAccumulator);
                _metricAccumulator = new MetricAccumulator(_metricUnit);
            }
        }


        public static CloudWatch Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        _instance = new CloudWatch();
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Increment custom metric.
        /// </summary>
        /// <param name="nameSpace">Similar to SQS/SNS/S3 this is our custom metric's namespace. EX: MAX</param>
        /// <param name="metricName">Descriptive name of our metric.</param>
        /// <param name="metricValue">The amount to increase the frequency of this event by.</param>
        public void IncrementMetric(double metricValue, string metricName, string nameSpace = "MAX")
        {
            if ((ConfigurationManager.AppSettings["CloudWatchMetricsActive"] ?? "true").ToLower().Equals("true"))
            {
                metricName = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy)
                    .Apply(metricName, '-');

                if (!IsValidNamespace(nameSpace))
                    throw new ArgumentException(
                        "Provided namespace does not match Amazon CloudWatch naming constraints.");
                if (!IsValidMetricName(metricName))
                    throw new ArgumentException(
                        "Provided metric name does not match Amazon CloudWatch naming constraints.");

                lock (_syncRoot)
                {
                    _metricAccumulator.Accumulate(nameSpace, metricName, metricValue);
                }
            }
        }

        private bool IsValidNamespace(string nameSpace)
        {
            if (nameSpace == null) return false;
            if (nameSpace.StartsWith("AWS/") || nameSpace.StartsWith(":")) return false;
            if (nameSpace.Length < 1 || nameSpace.Length > 255) return false;
            return true;
        }

        private bool IsValidMetricName(string name)
        {
            if (name == null) return false;
            if (name.Length < 1 || name.Length > 255) return false;
            return true;
        }
    }
}
