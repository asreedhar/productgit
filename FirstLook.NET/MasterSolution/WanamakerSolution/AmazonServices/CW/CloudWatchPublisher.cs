﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Timers;
using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using Amazon.Runtime;
using Amazon.Runtime.Internal.Transform;
using FirstLook.Common.Core;
using Timer = System.Timers.Timer;

namespace AmazonServices.CW
{
    public sealed class CloudWatchPublisher
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int BackoffMsCap = 8000;
        private const int MaxDataPerBatch = 20; // InvalidParameterValueException: The collection MetricData must not have a size greater than 20 (not in the documentation. pls amazon)

        private static int TooMuchData = 60 * 100; // 60 accumulators per minute
        private readonly Timer _publishingTimer;
        /// <summary>
        /// Public methods may only invoke the publisher if they do via locked sections (1:N)
        /// </summary>
        private readonly BackgroundWorker _metricPublisher = new BackgroundWorker();
        private readonly IAmazonCloudWatch _cloudWatchClient;
        private readonly object _syncWork = new object();
        private Dictionary<string, List<List<MetricDatum>>> _batchedPendingData = new Dictionary<string, List<List<MetricDatum>>>();
        public CloudWatchPublisher(TimeSpan publishRate)
        {
            _cloudWatchClient = AWSClientFactory.CreateAmazonCloudWatchClient(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());

            _publishingTimer = new Timer(publishRate.TotalMilliseconds);
            _publishingTimer.Elapsed += PublishingTimerElapsed;
            _publishingTimer.AutoReset = true;
            _publishingTimer.Start();

            _metricPublisher.DoWork += DoPublish;
            _metricPublisher.WorkerSupportsCancellation = true;
        }

        public void PublishMetrics(MetricAccumulator accumulator)
        {
            Dictionary<string, List<MetricDatum>> namespacedMetrics = accumulator.ToNamespaceToDatumDictionary();
            lock (_syncWork)
            {

                foreach (var kvp in namespacedMetrics)
                {
                    var nameSpace = kvp.Key;
                    var metrics = kvp.Value;
                    if (!(_batchedPendingData.ContainsKey(nameSpace)))
                    {
                        // create namespace key in pending data dict
                        _batchedPendingData.Add(nameSpace, new List<List<MetricDatum>>());
                        // create empty data set
                        _batchedPendingData[nameSpace].Add(new List<MetricDatum>());
                    }

                    // insert into and create batches
                    while (metrics.Count > 0)
                    {
                        var lastBatchItem = _batchedPendingData[nameSpace].Last();
                        var lastBatchRemainingSpace = MaxDataPerBatch - lastBatchItem.Count;
                        var appendSet = metrics.Take(lastBatchRemainingSpace);
                        var metricDatums = appendSet as MetricDatum[] ?? appendSet.ToArray();

                        lastBatchItem.AddRange(metricDatums);
                        metrics = metrics.Except(metricDatums).ToList();
                        if (lastBatchItem.Count >= MaxDataPerBatch)
                        {
                            // If full, add an empty data set
                            _batchedPendingData[nameSpace].Add(new List<MetricDatum>());
                        }
                    }
                    ClampData(nameSpace);
                }
            }
        }

        private void DoPublish(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            Dictionary<string, List<List<MetricDatum>>> dataClone;
            lock (_syncWork)
            {
                dataClone = _batchedPendingData.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                _batchedPendingData = new Dictionary<string, List<List<MetricDatum>>>();
            }
            var nameSpaces = dataClone.Keys.ToArray();
            int backOffMs = 1000;
            foreach (var nameSpace in nameSpaces)
            {

                for (int i = 0; i < dataClone[nameSpace].Count; i++)
                {
                    // create put request from batched data
                    var request = new PutMetricDataRequest();
                    request.Namespace = nameSpace;
                    request.MetricData = dataClone[nameSpace][i];
                    PutMetricDataResponse response = null;
                    try
                    {
                        response = PutMetricData(request);
                    }
                    catch (AmazonCloudWatchException ex)
                    {
                        Log.Error("Recieved Amazon CloudWatch Exception. Aborting publishing run. Will retry later.", ex);
                        Debug.WriteLine(ex);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                    if (response == null)
                    {
                        // remove item associated with exception
                        dataClone[nameSpace].RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        switch (response.HttpStatusCode)
                        {
                            case HttpStatusCode.OK:
                                Log.Info(String.Format("Successful publish of {0} metric datapoints to namespace {1} cloudwatch. Amazon response metadata: {2}",
                                    request.MetricData.Count, request.Namespace, response.ResponseMetadata));
                                backOffMs = 1000; // reset backoff after success
                                dataClone[nameSpace].RemoveAt(i);
                                i--;
                                break;
                            case HttpStatusCode.ServiceUnavailable: // Described as "slow down" as per docs
                                Log.Warn(
                                    String.Format(
                                        "Amazon returned HTTP {0} for cloudwatch PutMetricData. This item will be retried later. Amazon response metadata: {1}",
                                        response.HttpStatusCode, response.ResponseMetadata));
                                i--;
                                if (backOffMs > BackoffMsCap)
                                {
                                    Log.Warn(
                                        String.Format(
                                            "Amazon returned HTTP {0} for cloudwatch PutMetricData. Aborting additional backoff. Items will be attempted at next publish.  Amazon response metadata: {1}",
                                            response.HttpStatusCode, response.ResponseMetadata));

                                    MergeDictOntoPendingBatchDict(dataClone);
                                    CloudWatch.Instance.IncrementMetric(1, "Cloud-Watch-Publisher-BackOff-Cap-Reached");
                                    return; // stop publishing this batch
                                }
                                Thread.Sleep(backOffMs);
                                backOffMs *= 2;
                                break;
                            // TODO: identify other response codes
                            default:
                                Log.Warn(
                                    String.Format(
                                        "Amazon returned HTTP {0} for cloudwatch PutMetricData. Removing item from publishing list due to unknown response. Amazon response metadata: {1} ",
                                        response.HttpStatusCode, response.ResponseMetadata.Metadata));
                                dataClone[nameSpace].RemoveAt(i);
                                i--;
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Thread safe. Joins provided dictionary into the pending batch data dictionary's namespaces.
        /// </summary>
        /// <param name="data">dictionary to join onto _batchedPendingData</param>
        private void MergeDictOntoPendingBatchDict(Dictionary<string, List<List<MetricDatum>>> data)
        {
            lock (_syncWork)
            {
                foreach (var item in data)
                {
                    if (_batchedPendingData.ContainsKey(item.Key))
                    {
                        _batchedPendingData[item.Key].AddRange(item.Value);
                    }
                    else
                    {
                        _batchedPendingData.Add(item.Key, item.Value);
                    }
                    ClampData(item.Key);
                }
            }
        }
        private void ClampData(string nameSpace)
        {
            var dataCount = _batchedPendingData[nameSpace].Count;

            if (dataCount > TooMuchData)
            {
                var discardCount = dataCount - TooMuchData;
                Log.Warn(String.Format("Too much data waiting to be published, discarding {0} data batches.", discardCount));
                _batchedPendingData[nameSpace].RemoveRange(0, discardCount);
            }
        }
        private void PublishingTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ((ConfigurationManager.AppSettings["CloudWatchMetricsActive"] ?? "true").ToLower().Equals("true"))
            {
                if (!_metricPublisher.IsBusy)
                {
                    _metricPublisher.RunWorkerAsync();
                }
                else
                {
                    Log.Info("CloudWatchPublisher background worker is already busy.");
                }
            }
        }

#if DEBUG
        private PutMetricDataResponse PutMetricData(PutMetricDataRequest request)
        {
            var fakeResponse = new PutMetricDataResponse();
            fakeResponse.HttpStatusCode = HttpStatusCode.OK;
            Debug.WriteLine("Amazon CloudWatch - PutMetricData FAKE request: namespace ({0}) count [{1}]", request.Namespace,
                request.MetricData.Count);
            Log.Info(string.Format("Amazon CloudWatch - PutMetricData FAKE request: namespace ({0}) count [{1}]", request.Namespace,
                request.MetricData.Count));
            return fakeResponse;
        }
#else
        // Only in Beta and Prod do we want metrics
        //
        private PutMetricDataResponse PutMetricData(PutMetricDataRequest request)
        {
            Log.Info(string.Format("Amazon CloudWatch - PutMetricData REAL request: namespace ({0}) count [{1}]", request.Namespace,
                request.MetricData.Count));
            return _cloudWatchClient.PutMetricData(request);
        }
#endif

    }
}
