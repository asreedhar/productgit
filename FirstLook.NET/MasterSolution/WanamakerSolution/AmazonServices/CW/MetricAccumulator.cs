﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;

namespace AmazonServices.CW
{
    /// <summary>
    /// Spans a single unit of time
    /// </summary>
    public sealed class MetricAccumulator
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Dictionary<string, Dictionary<string, double>> _aggregate = new Dictionary<string, Dictionary<string, double>>();
        public DateTime StartTime { get; private set; }
        public StandardUnit Unit { get; set; }
        public MetricAccumulator(StandardUnit unit)
        {
            StartTime = DateTime.UtcNow;
            Unit = unit;
        }

        public void Accumulate(string nameSpace, string metricName, double count)
        {
            if (_aggregate.ContainsKey(nameSpace))
            {
                if (_aggregate[nameSpace].ContainsKey(metricName))
                {
                    _aggregate[nameSpace][metricName] += count;
                }
                else
                {
                    _aggregate[nameSpace].Add(metricName, count);
                }
            }
            else
            {
                var metricNameDictionary = new Dictionary<string, double>();
                metricNameDictionary.Add(metricName, count);
                _aggregate.Add(nameSpace, metricNameDictionary);
            }
        }

        /// <summary>
        /// Collapse aggregate data into a dictionary analogue of PutMetricDataRequest
        /// </summary>
        /// <returns></returns>
        public Dictionary<string,List<MetricDatum>> ToNamespaceToDatumDictionary()
        {
            var dict = new Dictionary<string,List<MetricDatum>>();
            foreach (var namespaceKvp in _aggregate)
            {
                var nameSpace = namespaceKvp.Key;
                foreach (var metricDictionary in namespaceKvp.Value)
                {
                    var data = new MetricDatum();
                    var name = metricDictionary.Key;
                    data.MetricName = name;
                    data.Timestamp = StartTime;
                    data.Unit = Unit;
                    data.Value = metricDictionary.Value;

                    if (dict.ContainsKey(nameSpace))
                    {
                        dict[nameSpace].Add(data);
                    }
                    else
                    {
                        var list = new List<MetricDatum>();
                        list.Add(data);

                        dict.Add(nameSpace, list);
                    }
                }
            }
            return dict;
        }
    }
}
