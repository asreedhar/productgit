﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Messages;
using Core.Messaging;

namespace AmazonServices.SNS
{
    /// <summary>
    /// In implementation of IBus which wraps an Amazon topic.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class Bus<T, Q> : IBus<T, Q> where T : IMessageBase, Q 
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string _topicName;
        private readonly string _topicArn;

        public Bus(string topicName, string topicArn)
        {
            _topicName = topicName;
            _topicArn = topicArn;
        }

        public string Id
        {
            get { return _topicArn; }
        }

        public bool IsSubscribed(IQueue<Q> queue)
        {
            return Topic.IsSubscribed(_topicArn, queue);
        }

        public void Subscribe(IQueue<Q> queue)
        {
            Log.DebugFormat("Adding queue subscription for queue {0}", queue.QueueArn);

            // Do the subscription
            string subscription = Topic.Subscribe(_topicArn, queue);            

            Log.DebugFormat("Subscription added for queue {0}.  SubscriptionId is {1}", queue.QueueArn, subscription);
        }

        public void Unsubscribe(IQueue<Q> queue)
        {
            Log.DebugFormat("Unsubscribing queue {0}", queue.QueueArn);

            // Do the unsubscribe.
            Topic.Unsubscribe(_topicArn, queue);

            Log.DebugFormat("Queue {0} has been unsubscribed.", queue.QueueArn);
        }

        public string Publish(T message, string sentFrom)
        {
            message.SentFrom = sentFrom;
            return Topic.Publish(message, _topicArn);
        }
        public void PublishAync(T message, string sentFrom)
        {
            message.SentFrom = sentFrom;
            Topic.PublishAsync(message, _topicArn);
        }
    }
}