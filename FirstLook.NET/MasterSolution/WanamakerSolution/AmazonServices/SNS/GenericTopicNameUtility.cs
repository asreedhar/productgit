﻿using System;
using FirstLook.Common.Core.Extensions;

namespace AmazonServices.SNS
{
    /// <summary>
    /// Naming utility for generic topics.
    /// TODO: Add code to ease the transition to more conventional naming.
    /// </summary>
    public static class GenericTopicNameUtility
    {
        private const string Separator = "_";

        public static string Name(string name, string postfix = "", bool autoTruncateName = false)
        {
            // Topic names must be made up of only uppercase and lowercase ASCII letters, 
            // numbers, underscores, and hyphens, and must be between 1 and 256 characters long.
            // see: http://docs.aws.amazon.com/sns/latest/APIReference/API_CreateTopic.html 

            if (!string.IsNullOrWhiteSpace(postfix))
            {
                name = name + Separator + postfix;
            }

            // we enforce lower-case letters in our topic names for consistency.
            name = name.ToLower();

            // TODO: Regex the name to ensure no invalid characters are present.

            if (name.Length > 256)
            {
                if (!autoTruncateName)
                {
                    throw new ArgumentException("The name '{0}' is too long.", name);
                }
                name = name.Right(256);
            }

            return name;
        }

    }
}
