﻿using System;
using Amazon;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using AmazonServices.Properties;
using AmazonServices.S3;
using FirstLook.Common.Core;

namespace AmazonServices.SNS
{
    public class GenericTopic
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static IAmazonSimpleNotificationService SnsClient()
        {
            return AWSClientFactory.CreateAmazonSimpleNotificationServiceClient(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
        }

        /// <summary>
        /// Create a topic. The topic name will be lower case.
        /// </summary>
        /// <param name="name">The topic name</param>
        /// <param name="appendEnvironmentNameToTopicName">Should we append the environment name to the topic name?
        /// For example, if the environment is "Release", and your topic name is "Foo", this would create a topic named
        /// "Foo_Release". If you've already done this or don't want this behavior, leave it set to false, the default.
        /// </param>
        /// <param name="useGlobalNamingRules">This will use - instead of _ and force lowercase/DNS valid ARN creation.</param>
        /// <returns>Topic ARN</returns>
        public static string CreateTopic(string name, bool appendEnvironmentNameToTopicName = false, bool useGlobalNamingRules = false, char topicNameDelimiter = '_')
        {
            try
            {
                string topicName = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.TopicStrategy).Apply(name, topicNameDelimiter, useGlobalNamingRules, appendEnvironmentNameToTopicName);

                var r = new CreateTopicRequest() { Name = topicName };
                var response = SnsClient().CreateTopic(r);
                return response.CreateTopicResult.TopicArn;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Delete the topic.
        /// </summary>
        /// <param name="topicArn"></param>
        public static void DeleteTopic(string topicArn)
        {
            try
            {
                if (string.IsNullOrEmpty(topicArn)) return;

                DeleteTopicRequest r = new DeleteTopicRequest() { TopicArn = topicArn };
                SnsClient().DeleteTopic(r);
                Log.InfoFormat("Topic '{0}' has been deleted.", topicArn);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        /// <summary>
        /// Publish the message string to the topic arn.
        /// </summary>
        /// <param name="message">The message string. Can be whatever you want.</param>
        /// <param name="topicArn">The topic arn.</param>
        /// <returns>The messageId.</returns>
        public static string Publish(string message, string topicArn)
        {
            try
            {
                PublishRequest request = new PublishRequest()
                    {
                        Message = message,
                        TopicArn = topicArn
                    };
                
                var response = SnsClient().Publish(request);
             
                Log.DebugFormat("Message '{0}' published to topic '{1}' and resulting message id is '{2}'",
                    message, topicArn, response.PublishResult.MessageId);

                return response.PublishResult.MessageId;   
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return string.Empty;
            }
         
        }
    }    
}
