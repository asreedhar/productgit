﻿using System;
using System.Linq;
using System.Runtime.Caching;
using Amazon;
using Amazon.Auth.AccessControlPolicy;
using Amazon.Auth.AccessControlPolicy.ActionIdentifiers;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using AmazonServices.Properties;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
// using Attribute = Amazon.SQS.Model.Attribute;

namespace AmazonServices.SNS
{
    internal static class Topic
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static ObjectCache PolicyCache = new MemoryCache("SNS_Topic_Cache");

        internal static IAmazonSimpleNotificationService SnsClient()
        {
            return AWSClientFactory.CreateAmazonSimpleNotificationServiceClient(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
        }

        internal static IAmazonSQS SqsClient()
        {
            return AWSClientFactory.CreateAmazonSQSClient(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
        }

        /// <summary>
        /// Create a topic.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Topic ARN</returns>
        public static string CreateTopic(string name)
        {
            var r = new CreateTopicRequest() {Name = name};
            var response = SnsClient().CreateTopic(r);
            return response.CreateTopicResult.TopicArn;
        }




        /// <summary>
        /// Delete the topic.
        /// </summary>
        /// <param name="topicArn"></param>
        public static void DeleteTopic(string topicArn)
        {
            if (string.IsNullOrEmpty(topicArn)) return;

            DeleteTopicRequest r = new DeleteTopicRequest() {TopicArn = topicArn};
            var response = SnsClient().DeleteTopic(r);
        }

        private static Policy CreatePolicy(string topicArn, string queueArn)
        {
            // set the queue policy. See http://docs.amazonwebservices.com/sns/latest/gsg/AccessPolicyLanguage_ElementDescriptions.html
            Policy sqsPolicy =
                new Policy().WithStatements(
                        new Statement(Statement.StatementEffect.Allow)
                        .WithPrincipals(Principal.AllUsers)
                        .WithResources(new Resource(queueArn)) // Note: queue, not topic
                        .WithActionIdentifiers(SQSActionIdentifiers.SendMessage)
                        .WithConditions(
                                ConditionFactory.NewSourceArnCondition(topicArn)));

            return sqsPolicy;
        }

        private static string GetJsonQueuePolicyCacheKey(string queueUrl)
        {
            return "QueuePolicy_" + queueUrl;
        }

        private static string GetJsonQueuePolicy(IAmazonSQS sqsClient, string queueUrl)
        {
            //We want to cache the policy for a short time. If we don't and we are registering many buses to one queue there is a delay
            //from when we set the policy in Amazon to when we read it again from Amazon causing policies to get lost.

            string cacheKey = GetJsonQueuePolicyCacheKey(queueUrl);
            //check cache.
            string policyJson = PolicyCache.Get(cacheKey) as string;
            if (!String.IsNullOrEmpty(policyJson))
                return policyJson;

            var request = new GetQueueAttributesRequest() {QueueUrl = queueUrl};
            request.AttributeNames.Add("Policy");
            var response = sqsClient.GetQueueAttributes(request);
            if (response.Attributes.ContainsKey("Policy"))
            {
                policyJson = response.Policy;
            }
            
            return policyJson;
        }

        private static void SetJsonQueuePolicy(string queueUrl, string policyJson)
        {
            string cacheKey = GetJsonQueuePolicyCacheKey(queueUrl);

            if (!String.IsNullOrEmpty(policyJson))
                PolicyCache.Set(cacheKey, policyJson, DateTimeOffset.UtcNow.Add(TimeSpan.FromSeconds(60)));
        }

        private static void SetQueuePolicy(string topicArn, string queueArn, string queueUrl)
        {
            // Don't want to overwrite existing policy. If we do it causes problems when one queue is subscribing to multiple buses.
            // Instead we see if there is an existing policy and if there is we ad statements to it, otherwise create a new one.

            var sqsClient = SqsClient();

            //create Policy
            var sqsPolicy = CreatePolicy(topicArn, queueArn);

            string policyJson = GetJsonQueuePolicy(sqsClient, queueUrl);
            Policy newPolicy;
            if(!String.IsNullOrEmpty(policyJson)) //there is an existing policy set.
            {
                var existingPolicy = Policy.FromJson(policyJson);
                
                //do we already have the policy
                bool exists = existingPolicy.Statements.Any(x => x.Conditions.Any(y => y.Values.Any(z => String.Compare(z, topicArn, true) == 0)));
                if (exists) //the policy exists return.
                {
                    Log.DebugFormat("Policy for topic {0} already exists for queue {1} returning..", topicArn, queueArn);
                    return;
                }

                Log.DebugFormat("Adding policy for topic {0} to existingPolicy with existing {1} items", topicArn, existingPolicy.Statements.Count);
                existingPolicy.Statements.Add(sqsPolicy.Statements.First());
                newPolicy = existingPolicy;
            }
            else
            {
                Log.Debug("No existing policy for queue. Adding initial one.");
                newPolicy = sqsPolicy;
            }

            //add it
            var request = new SetQueueAttributesRequest {QueueUrl = queueUrl};
            request.Attributes.Add("Policy", newPolicy.ToJson());
            sqsClient.SetQueueAttributes(request);

            //cache it
            SetJsonQueuePolicy(queueUrl, newPolicy.ToJson());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topicArn"></param>
        /// <param name="queueArn"></param>
        /// <param name="protocol"></param>
        /// <returns>Subscription ARN</returns>
        public static string Subscribe(string topicArn, string queueArn, string queueUrl)
        {
            SubscribeRequest r = new SubscribeRequest()
                {
                    Endpoint=queueArn,
                    Protocol="sqs",
                    TopicArn=topicArn
                };

            var response = SnsClient().Subscribe(r);

            SetQueuePolicy(topicArn, queueArn, queueUrl);
            return response.SubscribeResult.SubscriptionArn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="topicArn"></param>
        /// <param name="queue"></param>
        /// <returns>Subscription ARN</returns>
        public static string Subscribe<T>(string topicArn, IQueue<T> queue)
        {
            var subscription = Subscribe(topicArn, queue.QueueArn, queue.Url);

            return subscription;
        }

        public static void Unsubscribe<T>(string topicArn, IQueue<T> queue)
        {
            var client = SnsClient();
            //Find the subscription
            var listRequest = new ListSubscriptionsByTopicRequest() {TopicArn = topicArn};

            var listResponse = client.ListSubscriptionsByTopic(listRequest);
            var subscriptionArn = listResponse.ListSubscriptionsByTopicResult.Subscriptions.Where(x => x.Endpoint == queue.QueueArn).Select(x => x.SubscriptionArn).FirstOrDefault();

            if (subscriptionArn != null) //if we found the subscripion
            {
                //Unsubscribe
                UnsubscribeRequest r = new UnsubscribeRequest() {SubscriptionArn = subscriptionArn};

                var response = client.Unsubscribe(r);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topicArn"></param>
        /// <param name="subscription"></param>
        /// <param name="endpoint">Queue ARN</param>
        /// <returns></returns>
        public static bool IsSubscribed(string topicArn, string subscription, string endpoint)
        {
            var r = new ListSubscriptionsByTopicRequest() {TopicArn = topicArn};

            var response = SnsClient().ListSubscriptionsByTopic(r);

            // are there more          
            // var nextToken = response.ListSubscriptionsByTopicResult.NextToken;

            var subscriptions = response.ListSubscriptionsByTopicResult.Subscriptions;

            // Find the subscription with this ARN.
            var foundBySub = subscriptions.Any(sub => sub.SubscriptionArn == subscription);
            if (foundBySub) return true;

            // Find the subscription with this endpoint
            var foundByEndpoint = subscriptions.Any(sub => sub.Endpoint == endpoint);
            return foundByEndpoint;

            // May need to look at next token.
        }

        public static bool IsSubscribed<T>(string topicArn, IQueue<T> queue)
        {
            var r = new ListSubscriptionsByTopicRequest() {TopicArn = topicArn};

            var response = SnsClient().ListSubscriptionsByTopic(r);
            var subscriptions = response.ListSubscriptionsByTopicResult.Subscriptions;

            // Find the subscription with this endpoint
            var foundByEndpoint = subscriptions.Any(sub => sub.Endpoint == queue.QueueArn);
            return foundByEndpoint;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="topicArn"></param>
        /// <returns>Message Id</returns>
        public static string Publish<T>(T message, string topicArn)
        {
            string msg = message.ToJson();

            // TODO: How big can a SNS message be? 64 KB. Can't do claim check here. No control how messages are forwarded to different targets

            PublishRequest request = new PublishRequest()
                {
                    Message = msg,
                    TopicArn = topicArn
                };
            var response = SnsClient().Publish(request);
            return response.PublishResult.MessageId;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="topicArn"></param>
        /// <returns>Message Id</returns>
        public static void PublishAsync<T>(T message, string topicArn)
        {
            string msg = message.ToJson();

            // TODO: How big can a SNS message be? 64 KB. Can't do claim check here. No control how messages are forwarded to different targets

            PublishRequest request = new PublishRequest()
            {
                Message = msg,
                TopicArn = topicArn
            };
            SnsClient().BeginPublish(request, EndPublish, SnsClient());
        }

        internal static void EndPublish(IAsyncResult asyncResult)
        {
            var client = asyncResult.AsyncState as IAmazonSimpleNotificationService ??
                             SnsClient();
            client.EndPublish(asyncResult);
        }
    }
}
