﻿using AmazonServices.Properties;
using AmazonServices.S3;
using Core.Messages;
using Core.Messaging;
using FirstLook.Common.Core;

namespace AmazonServices.SNS
{
    /// <summary>
    /// A bus factory.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class BusFactory<T, Q> where T : IMessageBase, Q 
    {
        public static IBus<T, Q> Create(string busName, char busNameDelimiter = '_', bool useGlobalNamingRules = false)
        {
            // The default naming strategy for SNS
            var name = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(busName, busNameDelimiter, useGlobalNamingRules);
            //var name = TypeUtility.Name(busName, Settings.Default.Environment);
            var topicArn = Topic.CreateTopic(name);
            return new Bus<T,Q>(name, topicArn);
        }

        internal static void Delete(IBus<T,Q> bus)
        {
            Topic.DeleteTopic(bus.Id);
        }

       
    }
}