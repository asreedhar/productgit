﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.SimpleDB.Model;
using Amazon.SimpleWorkflow.Model;
using Core.Messaging;
using FirstLook.Common.Core;
using MAX.Entities;


namespace AmazonServices.DDB
{
    public abstract class DynamoDbBase : INoSqlDb
    {
        #region Properties
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly String _tableName;

        private AmazonDynamoDBClient _dynamoDbClient;
        private AmazonDynamoDBClient DynamoDbClient
        {
            get
            {
                return _dynamoDbClient ??
                    (_dynamoDbClient = new AmazonDynamoDBClient(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion()));
            }
        }
        #endregion

        #region Methods
        protected DynamoDbBase(String tableName)
        {
            if (Log.IsDebugEnabled)
                Log.DebugFormat("DynamoDbBase() constructor called with tableName='{0}'", tableName);

            _tableName = tableName;
        }

        #region INoSqlDb Implementation
        public String ContainerName()
        {
            return _tableName;
        }

        #region Synchronous Methods
        public void Write(Dictionary<String, Object> item)
        {
            var request = new PutItemRequest {TableName = _tableName, Item = DictionaryToAttributeValues(item)};

            var response = DynamoDbClient.PutItem(request);
            // do something with the response?
        }

        public void Write(Object item)
        {
            var request = new PutItemRequest {TableName = _tableName, Item = ObjectToAttributeValues(item)};

            var response = DynamoDbClient.PutItem(request);
            // do something with the response?
        }

        public void Read(Dictionary<String, Object> keys)
        {
            ////var key = new Key
            ////{
            ////    HashKeyElement = 
            ////}
            //var request = new GetItemRequest { TableName = _tableName, Key = DictionaryToAttributeValues(keys) };

            //var response = DynamoDbClient.GetItem(request);

            //var item = response.Item;
        }

        public List<Dictionary<String, AttributeValue>> Query(Dictionary<String, Condition> conditions)
        {
            var request = new QueryRequest
            {
                TableName = _tableName,
                KeyConditions = conditions
            };

            var response = DynamoDbClient.Query(request);

            Log.DebugFormat("Query retrieved {0} items", response.Count);

            return response.Items;
        }

        /// <summary>
        /// Method to query the NoSql table with the key and range in the where clause.
        /// </summary>
        /// <param name="whereClause">A dictionary of column name and values. If the value is of type DateRange it will create a BETWEEN clause</param>
        /// <returns>A data table filled with the resulting data</returns>
        public DataTable Query(Dictionary<String, Object> whereClause)
        {
            return ResultsToDataTable(Query(WhereClauseToConditions(whereClause)));
        }

        /// <summary>
        /// Method to query the NoSql table with the key and range in the where clause.
        /// </summary>
        /// <typeparam name="T">Object type you want the results returned as</typeparam>
        /// <param name="whereClause">A dictionary of column name and values. If the value is of type DateRange it will create a BETWEEN clause</param>
        /// <returns>A list of object type T filled with the resulting data</returns>
        public IList<T> Query<T>(Dictionary<String, Object> whereClause) where T : class, new()
        {
            return ResultsToObjectList<T>(Query(WhereClauseToConditions(whereClause)));
        }
        #endregion

        #region Asynchronous Methods
        public IAsyncResult BeginWrite(Dictionary<String, Object> item, AsyncCallback callback, Object asyncState)
        {
            var request = new PutItemRequest { TableName = _tableName, Item = DictionaryToAttributeValues(item) };

            return DynamoDbClient.BeginPutItem(request, callback, asyncState);
        }

        public IAsyncResult BeginWrite(Object item, AsyncCallback callback, Object asyncState)
        {
            var request = new PutItemRequest {TableName = _tableName, Item = ObjectToAttributeValues(item)};

            return DynamoDbClient.BeginPutItem(request, callback, asyncState);
        }

        public void EndWrite(IAsyncResult asyncResult)
        {
            var response = DynamoDbClient.EndPutItem(asyncResult);
            // do something with the response?
        }
        #endregion
        #endregion

        #region Utility methods
        internal Dictionary<String, AttributeValue> DictionaryToAttributeValues(Dictionary<String, Object> item)
        {
            var attributeValues = new Dictionary<String, AttributeValue>();

            foreach (var attributeName in item.Keys)
                attributeValues[attributeName] = GetAttributeValue(item[attributeName]);

            return attributeValues;
        }

        internal Dictionary<String, AttributeValue> ObjectToAttributeValues(Object item)
        {
            var attributeValues = new Dictionary<String, AttributeValue>();

            foreach (var propInfo in item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                attributeValues[propInfo.Name] = GetAttributeValue(propInfo.GetValue(item, null));

            return attributeValues;
        }

        internal Dictionary<String, Condition> WhereClauseToConditions(Dictionary<String, Object> whereClause)
        {
            var conditions = new Dictionary<String, Condition>();

            foreach (var clause in whereClause.Keys)
            {
                var condition = new Condition();

                if(whereClause[clause].GetType() == typeof(DateRange))
                {
                    var range = whereClause[clause] as DateRange;
                    if(range == null) continue;

                    condition.ComparisonOperator = ComparisonOperator.BETWEEN;
                    condition.AttributeValueList = new List<AttributeValue>
                    {
                        GetAttributeValue(range.StartDate),
                        GetAttributeValue(range.EndDate)
                    };
                }
                else
                {
                    condition.ComparisonOperator = ComparisonOperator.EQ;
                    condition.AttributeValueList = new List<AttributeValue>
                    {
                        GetAttributeValue(whereClause[clause])
                    };
                }

                conditions.Add(clause, condition);
            }

            return conditions;
        }

        internal AttributeValue GetAttributeValue(Object value)
        {
            var attributeValue = new AttributeValue();

            switch (value.GetType().ToString())
            {
                case "System.UInt16":
                case "System.UInt32":
                case "System.UInt64":
                case "System.Int16":
                case "System.Int32":
                case "System.Int64":
                    attributeValue.N = value.ToString();
                    break;
                case "System.DateTime":
                    attributeValue.S = ((DateTime)value).ToUniversalTime()
                        .ToString("O", DateTimeFormatInfo.InvariantInfo);
                    break;
                default:
                    attributeValue.S = value.ToString();
                    break;
            }

            return attributeValue;
        }

        internal Object GetObjectValue(AttributeValue value)
        {
            Object objectValue;

            if (value.N != null)
                objectValue = Decimal.Parse(value.N.ToString(CultureInfo.InvariantCulture));
            else
                objectValue = value.S.ToString(CultureInfo.InvariantCulture);

            return objectValue;
        }

        internal void SetObjectProperty(Object theObject, String propertyName, AttributeValue attribute)
        {
            var property = theObject.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);
            if (property == null || !property.CanWrite) return;

            switch (property.PropertyType.ToString())
            {
                case "System.UInt16":
                    UInt16 uint16Value;
                    if (UInt16.TryParse(attribute.N, out uint16Value))
                        property.SetValue(theObject, uint16Value, null);
                    break;
                case "System.UInt32":
                    UInt32 uint32Value;
                    if (UInt32.TryParse(attribute.N, out uint32Value))
                        property.SetValue(theObject, uint32Value, null);
                    break;
                case "System.UInt64":
                    UInt64 uint64Value;
                    if (UInt64.TryParse(attribute.N, out uint64Value))
                        property.SetValue(theObject, uint64Value, null);
                    break;
                case "System.Int16":
                    Int16 int16Value;
                    if (Int16.TryParse(attribute.N, out int16Value))
                        property.SetValue(theObject, int16Value, null);
                    break;
                case "System.Int32":
                    Int32 int32Value;
                    if (Int32.TryParse(attribute.N, out int32Value))
                        property.SetValue(theObject, int32Value, null);
                    break;
                case "System.Int64":
                    Int64 int64Value;
                    if (Int64.TryParse(attribute.N, out int64Value))
                        property.SetValue(theObject, int64Value, null);
                    break;
                case "System.DateTime":
                    DateTime dateValue;
                    if (DateTime.TryParse(attribute.S, out dateValue))
                        property.SetValue(theObject, dateValue, null);
                    break;
                case "System.String":
                    property.SetValue(theObject, attribute.S, null);
                    break;

                    // MAX specific types
                case "MAX.Entities.WorkflowType":
                    MAX.Entities.WorkflowType enumValue;
                    if (Enum.TryParse(attribute.S, out enumValue))
                        property.SetValue(theObject, enumValue, null);
                    break;
            }
        }

        internal DataTable ResultsToDataTable(List<Dictionary<String, AttributeValue>> resultList)
        {
            var dataTable = new DataTable(_tableName);
            if (resultList.Count <= 0) return dataTable;

            var firstResult = resultList.ElementAt(0);
            foreach (var column in firstResult.Keys.Select(key => new DataColumn(key)
            {
                ReadOnly = true,
                DataType = GetObjectValue(firstResult[key]).GetType()
            }))
            {
                dataTable.Columns.Add(column);
            }

            foreach (var result in resultList)
            {
                var row = dataTable.NewRow();
                foreach (var key in result.Keys)
                    row[key] = GetObjectValue(result[key]);

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }

        internal IList<T> ResultsToObjectList<T>(List<Dictionary<String, AttributeValue>> resultList) where T : class, new()
        {
            var objectList = new List<T>();
            if (resultList.Count <= 0) return objectList;

            foreach (var result in resultList)
            {
                var newObject = new T();
                foreach (var key in result.Keys)
                    SetObjectProperty(newObject, key, result[key]);

                objectList.Add(newObject);
            }

            return objectList;
        }
        #endregion
        #endregion
    }
}
