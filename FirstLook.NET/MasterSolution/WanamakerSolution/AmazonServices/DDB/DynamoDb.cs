﻿using System;

namespace AmazonServices.DDB
{
    public class DynamoDb : DynamoDbBase
    {
        public DynamoDb(String tableName, char tableNameDelimiter = '_', bool useGlobalNamingRules = true)
            : base(NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(tableName, tableNameDelimiter, useGlobalNamingRules))
        {
        }
    }
}
