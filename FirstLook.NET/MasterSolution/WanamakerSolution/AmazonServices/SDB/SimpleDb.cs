﻿using System;
using AmazonServices.Properties;

namespace AmazonServices.SDB
{
    public sealed class SimpleDb : SimpleDbBase
    {
        public SimpleDb(String tableName, char tableNameDelimiter = '_', bool useGlobalNamingRules = false, bool forShowroom = false)
            : base(NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(tableName, tableNameDelimiter, useGlobalNamingRules, forShowroom))
        {
        }

        public SimpleDb(String tableName, String awsAccessKey, String awsSecretAccessKey, char tableNameDelimiter = '_', bool useGlobalNamingRules = false, bool forShowroom = false)
            : base(NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(tableName, tableNameDelimiter, useGlobalNamingRules, forShowroom: forShowroom), awsAccessKey, awsSecretAccessKey)
        {
        }
    }

    public sealed class GoogleAnalyticsReportDb : SimpleDbBase
    {
        public GoogleAnalyticsReportDb()
            : base(
                NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy)
                    .Apply("ga_reports_ds_report_data", '_', false, forShowroom: true),
                Settings.Default.MaxGoogleAnalyticsAccessKey, Settings.Default.MaxGoogleAnalyticsSecretAccessKey)
        {
        }
    }
}
