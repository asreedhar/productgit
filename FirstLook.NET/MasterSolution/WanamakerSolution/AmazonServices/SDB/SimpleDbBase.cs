﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.Runtime;
using Amazon.SimpleDB;
using Amazon.SimpleDB.Model;
using AmazonServices.Properties;
using Core.Messaging;

namespace AmazonServices.SDB
{
    public abstract class SimpleDbBase : ISimpleDb
    {
        #region Properties
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public readonly String DomainName;
        private readonly String _awsAccessKey;
        private readonly String _awsSecretAccessKey;

        private AmazonSimpleDBClient _simpleDbClient;
        internal AmazonSimpleDBClient SimpleDbClient
        {
            get
            {
                return _simpleDbClient ??
                    (_simpleDbClient = new AmazonSimpleDBClient(AmazonHelper.GetCredentials(_awsAccessKey, _awsSecretAccessKey), AmazonHelper.GetRegion()));
            }
        }
        #endregion

        #region Methods
        protected SimpleDbBase(String domainName)
        {
            if (Log.IsDebugEnabled)
                Log.DebugFormat("SimpleDbBase() constructor called with tableName='{0}'", domainName);

            DomainName = domainName;
            _awsAccessKey = Settings.Default.AwsAccessKey;
            _awsSecretAccessKey = Settings.Default.AwsSecretAccessKey;
        }

        protected SimpleDbBase(String domainName, String awsAccessKey, String awsSecretAccessKey)
            : this(domainName)
        {
            _awsAccessKey = awsAccessKey;
            _awsSecretAccessKey = awsSecretAccessKey;
        }

        #region ISimpleDb implementation
        String ISimpleDb.ContainerName()
        {
            return DomainName;
        }

        #region Synchronous Methods
        public List<Dictionary<string, string>> Select(Dictionary<String, String> whereClause)
        {
            return Select(new List<string> {"*"}, whereClause);
        }

        public List<Dictionary<string, string>> Select(List<String> selectList, Dictionary<String, String> whereClause)
        {
            var attributes = "*";

            if (selectList.Count > 0)
                attributes = String.Join(",", selectList);

            var query = String.Format("SELECT {0} FROM {1}", attributes, DomainName);

            for (var x = 0; x < whereClause.Keys.Count; x++)
            {
                var key = whereClause.Keys.ElementAt(x);
                query += (x == 0 ? " WHERE" : " AND");
                query += String.Format(" {0} = \"{1}\"", key, whereClause[key]);
            }

            return Select(query);
        }

        public List<Dictionary<string, string>> Select(String query)
        {
            try
            {
                var selectRequest = new SelectRequest()
                {
                    SelectExpression = query
                };

                var selectResult = SimpleDbClient.Select(selectRequest);

                var resultList =
                    selectResult.Items.Select(item =>
                    {
                        var attributes = item.Attributes.ToDictionary(x => x.Name, x => x.Value);
                        if(query.ToLower().Contains("itemname") || query.Contains("*"))
                            attributes["itemName"] = item.Name; // add the AWS primary key

                        return attributes;
                    }).ToList();

                return resultList;
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }

        public void Write(String primaryKeyValue, Dictionary<String, String> updateList)
        {
            try
            {
                var request = new PutAttributesRequest
                {
                    DomainName = DomainName,
                    Attributes = DictionaryToReplaceableAttributes(updateList),
                    ItemName = primaryKeyValue
                };

                var response = SimpleDbClient.PutAttributes(request);
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }

        public int BatchDelete(Dictionary<String, String> whereClause)
        {
            try
            {
                var deleteList = Select(new List<String> { "itemName()" }, whereClause);
                var batchDeleteRequest = new BatchDeleteAttributesRequest();

                foreach (var deletableItem in deleteList.Where(item => item.ContainsKey("itemName")).Select(item => new DeletableItem {Name = item["itemName"]}))
                    batchDeleteRequest.Items.Add(deletableItem);

                var batchDeleteResponse = SimpleDbClient.BatchDeleteAttributes(batchDeleteRequest);
                return batchDeleteRequest.Items.Count;
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }

        public HttpStatusCode Delete(String itemName)
        {
            try
            {
                var deleteRequest = new DeleteAttributesRequest
                {
                    DomainName = DomainName,
                    ItemName = itemName
                };

                var deleteResponse = SimpleDbClient.DeleteAttributes(deleteRequest);
                return deleteResponse.HttpStatusCode;
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }
        #endregion

        #region Asynchronous Methods
        public IAsyncResult BeginSelect(Dictionary<String, String> whereClause, AsyncCallback callback, Object asyncState)
        {
            return BeginSelect(new List<string> { "*" }, whereClause, callback, asyncState);
        }

        public IAsyncResult BeginSelect(List<String> selectList, Dictionary<String, String> whereClause, AsyncCallback callback, Object asyncState)
        {
            var attributes = "*";

            if (selectList.Count > 0)
                attributes = String.Join(",", selectList);

            var query = String.Format("SELECT {0} FROM {1}", attributes, DomainName);

            for (var x = 0; x < whereClause.Keys.Count; x++)
            {
                var key = whereClause.Keys.ElementAt(x);
                query += (x == 0 ? " WHERE" : " AND");
                query += String.Format(" {0} = \"{1}\"", key, whereClause[key]);
            }

            return BeginSelect(query, callback, asyncState);
        }

        public IAsyncResult BeginSelect(String query, AsyncCallback callback, Object asyncState)
        {
            try
            {
                var selectRequest = new SelectRequest()
                {
                    SelectExpression = query
                };
                asyncState = query;

                return SimpleDbClient.BeginSelect(selectRequest, callback, asyncState);
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }

        public List<Dictionary<string, string>> EndSelect(IAsyncResult asyncResult)
        {
            var selectResult = SimpleDbClient.EndSelect(asyncResult);
            var originalQuery = asyncResult.AsyncState.ToString();

            var resultList =
                selectResult.Items.Select(item =>
                {
                    var attributes = item.Attributes.ToDictionary(x => x.Name, x => x.Value);
                    if(originalQuery.ToLower().Contains("itemname") || originalQuery.Contains("*"))
                        attributes["itemName"] = item.Name; // add the AWS primary key

                    return attributes;
                }).ToList();

            return resultList;
        }

        public IAsyncResult BeginBatchDelete(Dictionary<String, String> whereClause, AsyncCallback callback, Object asyncState)
        {
            try
            {
                var deleteList = Select(new List<String> { "itemName()" }, whereClause);
                var batchDeleteRequest = new BatchDeleteAttributesRequest();

                foreach (var deletableItem in deleteList.Where(item => item.ContainsKey("itemName")).Select(item => new DeletableItem { Name = item["itemName"] }))
                    batchDeleteRequest.Items.Add(deletableItem);

                return SimpleDbClient.BeginBatchDeleteAttributes(batchDeleteRequest, callback, asyncState);
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }

        public HttpStatusCode EndBatchDelete(IAsyncResult asyncResult)
        {
            try
            {
                var batchDeleteResponse = SimpleDbClient.EndBatchDeleteAttributes(asyncResult);
                return batchDeleteResponse.HttpStatusCode;
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }

        public IAsyncResult BeginDelete(String itemName, AsyncCallback callback, Object asyncState)
        {
            try
            {
                var deleteRequest = new DeleteAttributesRequest
                {
                    DomainName = DomainName,
                    ItemName = itemName
                };

                return SimpleDbClient.BeginDeleteAttributes(deleteRequest, callback, asyncState);
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                throw;
            }
        }

        public HttpStatusCode EndDelete(IAsyncResult asyncResult)
        {
            try
            {
                var deleteResponse = SimpleDbClient.EndDeleteAttributes(asyncResult);
                return deleteResponse.HttpStatusCode;
            }
            catch (AmazonSimpleDBException sdbEx)
            {
                Log.DebugFormat("Amazon query returned error:\n{0}", sdbEx);
                return HttpStatusCode.ExpectationFailed;
            }
        }
        #endregion
        #endregion

        internal List<ReplaceableAttribute> DictionaryToReplaceableAttributes(Dictionary<String, String> item)
        {
            return item.Keys.Select(attributerName => new ReplaceableAttribute
            {
                Name = attributerName, Value = item[attributerName], Replace = true
            }).ToList();
        }

        #endregion
    }
}