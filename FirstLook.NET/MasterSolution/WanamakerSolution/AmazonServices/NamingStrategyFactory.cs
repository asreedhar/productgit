﻿using AmazonServices.Properties;
using AmazonServices.SNS;
using FirstLook.Common.Core;

namespace AmazonServices
{
    public enum NamingStrategyType
    {
        TopicStrategy,
        GlobalStrategy
    }

    internal static class NamingStrategyFactory
    {
        internal static INamingStrategy GetNamingStrategy(NamingStrategyType strategyType)
        {
            switch (strategyType)
            {
                case NamingStrategyType.TopicStrategy:
                    return new TopicNamingStrategy();
                default:
                    return new GenericNamingStrategy();
            }
        }
    }

    internal interface INamingStrategy
    {
        string Apply(string bucketName, char bucketNameDelimiter, bool useGlobalNamingRules = false, bool appendEnvironmentPostfix = true, bool forShowroom = false);
    }

    /// <summary>
    /// Topics (SNS) have their own original naming strategy.
    /// </summary>
    internal class TopicNamingStrategy : INamingStrategy
    {
        public string Apply(string topicName, char bucketNameDelimiter, bool useGlobalNamingRules = false, bool appendEnvironmentPostfix = true, bool forShowroom = false)
        {
            var environment = Settings.Default.Environment;
            if (forShowroom) // showroom only has beta or prod suffixes
                environment = environment.ToLower() == "release" ? "prod" : "beta";

            if (useGlobalNamingRules)
            {
                // New naming strategy.
                // TODO: In 18.1, we should use this exclusively, since it passes useGlobalNamingRules to the BucketNameUtility. 
                // That utility should also cover the legacy naming conventions, but this needs further testing.
                // We can vary the "useGlobalNamingRules" to get the old behavior.
                return GlobalNameUtility.Name(topicName.ToLower(), environment.ToLower(),
                                              useGlobalNamingRules: true, autoTruncateName: false);
            }

            // Old naming strategy.
            return GenericTopicNameUtility.Name(topicName, environment);
        }
    }


    /// <summary>
    /// Used to migrate code over to the new global naming rules.  Picks a strategy and uses it to form the bucket name.
    /// If you use global naming rules, it picks the new naming strategy. Otherwise it uses the old naming strategy.
    /// </summary>
    internal class GenericNamingStrategy : INamingStrategy
    {
        public string Apply(string bucketName, char bucketNameDelimiter, bool useGlobalNamingRules = false, bool appendEnvironmentPostfix = true, bool forShowroom = false)
        {
            var environment = Settings.Default.Environment;
            if (forShowroom) // showroom only has beta or prod suffixes
                environment = environment.ToLower() == "release" ? "prod" : "beta";

            if (useGlobalNamingRules)
            {
                // New naming strategy.
                // TODO: In 18.1, we should use this exclusively, since it passes useGlobalNamingRules to the BucketNameUtility. 
                // That utility should also cover the legacy naming conventions, but this needs further testing.
                // We can vary the "useGlobalNamingRules" to get the old behavior.
                return GlobalNameUtility.Name(bucketName.ToLower(), environment.ToLower(),
                                              useGlobalNamingRules: true, autoTruncateName: false);
            }

            // Old naming strategy.
            return TypeUtility.Name(bucketName, environment, bucketNameDelimiter);
        }
    }
}