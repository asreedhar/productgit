﻿using System;
using System.Collections.Generic;
using AmazonServices.S3;
using AmazonServices.SES;
using AmazonServices.SNS;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Core.Messaging;

namespace AmazonServices
{
    public class AmazonServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Email>().As<IEmail>();
            //builder.RegisterGeneric(typeof (BusFactory<>)).As(typeof (IBusFactory<>));
            
            // This would be great if I could just use it in a constructor --TH
            builder.Register<Func<NamingStrategyType, INamingStrategy>>(c => NamingStrategyFactory.GetNamingStrategy);
            
            
            base.Load(builder);
        }
    }
}
