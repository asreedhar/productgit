﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon;
using Amazon.S3.Model;
using Amazon.Runtime;
using Amazon.S3;
using AmazonServices.Properties;

namespace AmazonServices
{
    public static class AmazonHelper
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string GeneratePreSignedURL(string bucketName, string fileName)
        {
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest()
            { 
                BucketName = bucketName,
                Key = fileName,
                Expires = DateTime.Now.AddYears(1)
            };
            
            var client = new AmazonS3Client(GetCredentials());
            string url = String.Empty;

            try
            {
                url = client.GetPreSignedURL(request);
            }
            catch (Exception e)
            {
                Log.Error("Error Generating presigned url:" + e.Message);
            }
            return url;
        }

        internal static AWSCredentials GetCredentials()
        {
            return new BasicAWSCredentials(Settings.Default.AwsAccessKey, Settings.Default.AwsSecretAccessKey);
        }

        internal static AWSCredentials GetCredentials(String awsAccessKey, String awsSecretAccessKey)
        {
            return new BasicAWSCredentials(awsAccessKey, awsSecretAccessKey);
        }

        /// <summary>
        /// Required for AWS API v2.0
        /// </summary>
        /// <returns></returns>
        internal static RegionEndpoint GetRegion()
        {
            return RegionEndpoint.USEast1;
        }
    }
}
