﻿using System.Collections.Specialized;
using AmazonServices.Properties;
using FirstLook.Common.Core;

namespace AmazonServices.S3
{
    public class ImageStore : StorageBase
    {
        public ImageStore(string bucketName, char bucketNameDelimiter = '-') :
            base(TypeUtility.Name(bucketName, Settings.Default.Environment, bucketNameDelimiter),
                 "image/jpeg", true, HttpHeaders)
        {
        }

        private static readonly NameValueCollection HttpHeaders = CreateHttpHeaders();

        private static NameValueCollection CreateHttpHeaders()
        {
            return new NameValueCollection {{"Cache-Control", "public,max-age=2592000"}};
        }
    }
}
