﻿using System;
using System.Collections.Specialized;

namespace AmazonServices.S3
{
    public class FileStore : StorageBase
    {
        /// <summary>
        /// Create a FileStore with the specified name and delimiter. If you don't use global naming rules,
        /// then your bucket name will not be DNS compliant and will only work in us-east. If you use
        /// global naming rules, then your bucket will be DNS compliant and your code can work in any region.
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="bucketNameDelimiter"></param>
        /// <param name="useGlobalNamingRules"></param>
        public FileStore(string bucketName, char bucketNameDelimiter = '_', bool useGlobalNamingRules = false)
            : base(NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(bucketName, bucketNameDelimiter, useGlobalNamingRules), "text/plain")
        {
        }

        /// <summary>
        /// Create a FileStore with the specified name and delimiter. If you don't use global naming rules,
        /// then your bucket name will not be DNS compliant and will only work in us-east. If you use
        /// global naming rules, then your bucket will be DNS compliant and your code can work in any region.
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="isAsyncCall"></param>
        /// <param name="bucketNameDelimiter"></param>
        /// <param name="useGlobalNamingRules"></param>
        public FileStore(string bucketName, Boolean isAsyncCall, char bucketNameDelimiter = '_', bool useGlobalNamingRules = false)
            : base(NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(bucketName, bucketNameDelimiter, useGlobalNamingRules), "text/plain", false, null, isAsyncCall)
        {
        }
        /// <summary>
        /// Create a FileStore with the specified name and delimiter. If you don't use global naming rules,
        /// then your bucket name will not be DNS compliant and will only work in us-east. If you use
        /// global naming rules, then your bucket will be DNS compliant and your code can work in any region.
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="headers"></param>
        /// <param name="contentType"></param>
        /// <param name="bucketNameDelimiter"></param>
        /// <param name="useGlobalNamingRules"></param>
        public FileStore(string bucketName, NameValueCollection headers, string contentType = "text/plain", 
                         char bucketNameDelimiter = '_', bool useGlobalNamingRules = false)
            : base(
                NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy).Apply(bucketName, bucketNameDelimiter, useGlobalNamingRules)
                , contentType, false, headers)
        {
        }
    }


    
}
