﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Runtime.Remoting.Messaging;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using AmazonServices.Properties;
using Core.Messaging;
using System.Linq;

namespace AmazonServices.S3
{
    public abstract class StorageBase : IFileStorage
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected StorageBase(string bucketName, string contentType = "text/plain", bool isPublicRead = false, NameValueCollection headers = null, bool isAsync=false)
        {
            if(Log.IsDebugEnabled)
                Log.DebugFormat("StorageBase() constructor called with bucketName='{0}' contentType='{1}' isPublicRead='{2}'",
                    bucketName, contentType, isPublicRead);

            BucketName = bucketName;
            ContentType = contentType;
            IsPublicRead = isPublicRead;
            Headers = headers;

            try
            {
                //check to see if we have bucket
                var client = new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
                var listRequest = new ListBucketsRequest();
                if (!isAsync)
                {
                    var response = client.ListBuckets(listRequest);
                    var bucket = response.Buckets.FirstOrDefault(x => x.BucketName == bucketName);

                    if (bucket == null) 
                    {
                        // Create bucket specify the bucket region on creation, so we can specify bucket region on read thus avoiding redirects.
                        var createBucketRequest = new PutBucketRequest() {BucketName = bucketName};
                        client.PutBucket(createBucketRequest);
                    }
                }
                else
                {
                    //check to see if we have bucket
                   
                    IAsyncResult asyncResult = client.BeginListBuckets(listRequest, CallbackListBucket, client); 
                }
               
            }
            catch (Exception ex)
            {
                Log.Error("Error creating S3 bucket.", ex);
            }
        }
    
        public string BucketName { get; private set; }
        public string ContentType { get; private set; }
        public bool IsPublicRead { get; private set; }
        public NameValueCollection Headers { get; private set; }

        public void Write(string fileName, Stream stream, out string bucketName , out string keyName, string contentType = null )
        {
            keyName = string.Empty;
            bucketName = string.Empty;

            try
            {
                var key = SanitizeKey(fileName);

                if (contentType == null)
                    contentType = ContentType;  // set the 'default' content type if one was not passed in

                var client = new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
                var request = new PutObjectRequest()
                    {
                        BucketName = BucketName,
                        ContentType = contentType,
                        // The GenerateMD5 property has been removed from PutObjectRequest and UploadPartRequest 
                        // because this is now automatically computed as the object is being written to Amazon S3 
                        // and compared against the MD5 returned in the response from Amazon S3.
                        // GenerateChecksum = true,
                        StorageClass = S3StorageClass.Standard,
                        Key = key
                    };
                
                request.InputStream = stream;

                if (IsPublicRead)
                    request.CannedACL = S3CannedACL.PublicRead;

                if (Headers != null)
                    foreach (var headerKey in Headers.AllKeys)
                        request.Headers[headerKey] = Headers[headerKey];

                client.PutObject(request);
                
                keyName = key;
                bucketName = BucketName;
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered writing stream", ex);
            }            
        }

        public void Write(string fileName, Stream stream)
        {
            Write(fileName, stream, null);
        }

        public void Write(string fileName, Stream stream, string contentType)
        {
            string bucketName, keyName;
            Write(fileName, stream, out bucketName, out keyName, contentType);
        }

        public Stream Exists(string fileName, out bool exists)
        {
            return Exists(fileName, out exists, null);
        }

        public Stream Exists(string fileName, out bool exists, DateTime? modifiedSince)
        {
            /*
             * "Exists" in this context really means "exists and is modified since the datetime passed in".  
             * When we get a 304 (Not Modified) from Amazon, the object obviously exists, but it hasn't been modified since the time we passed in.
             * Perhaps we should rename this public method.  I would do it now, but that is more change than I want to introduce during a code-freeze.
             * dh, 2015-04-17
             */

            Stream stream = null;
            exists = true;
            try
            {
                stream = Read(fileName, modifiedSince);
            }
            catch (AmazonS3Exception e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound 
                    || e.StatusCode == HttpStatusCode.NotModified)
                    exists = false;
                else
                {
                    Log.Error("Exception encountered reading stream", e);
                    throw;
                }
            }

            return stream;
        }

        public Stream Read(string fileName)
        {
            return Read(fileName, null);
        }

        public Stream Read(string fileName, DateTime? modifiedSince)
        {
            try
            {
                using (AmazonS3Client client = new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion()))
                {
                    var request = new GetObjectRequest()
                        {
                            BucketName = BucketName,
                            Key = SanitizeKey(fileName)
                        };

                    if (modifiedSince.HasValue)
                        request.ModifiedSinceDate = modifiedSince.Value;

                    using (var response = client.GetObject(request))
                    {
                        using (var str = response.ResponseStream)
                        {
                            var copy = new MemoryStream();
                            str.CopyTo(copy);
                            copy.Seek(0, SeekOrigin.Begin);
                            return copy;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error encountered reading fileName '{0}'", fileName), ex);
                throw;
            }
            
        }

        public void Delete(string fileName)
        {
            try
            {
                var client = new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());

                var request = new DeleteObjectRequest()
                    {
                        BucketName=BucketName,
                        Key=SanitizeKey(fileName)
                    };

                client.DeleteObject(request);
                
            }
            catch (Exception ex)
            {
                Log.Error("Error deleting from storage.", ex);
            }

        }

        /// <summary>
        /// For all those times you don't want to chew up bandwidth just to see if a file exists...
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExistsNoStream(string fileName)
        {
            bool exists = true;
            try
            {
                using (var client = new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion()))
                {
                    var request = new GetObjectMetadataRequest();
                    request.BucketName = BucketName;
                    request.Key = SanitizeKey(fileName);

                    client.GetObjectMetadata(request);
                }
            }
            catch (AmazonS3Exception e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                    exists = false;
                else
                {
                    Log.Error("Exception encountered reading stream", e);
                    throw;
                }
            }
            return exists;
        }

        public string SanitizeKey(string keyname)
        {
            return keyname.Replace('\\', '/');
        }


        public string ContainerName()
        {
            return BucketName;
        }

        public Stream ReadIfModified(string fileName, DateTime modifiedSince, out bool exists)
        {
            Stream stream = null;
            stream = Exists(fileName, out exists, modifiedSince);
            exists = exists && stream.Length > 0;
            return stream;
        }

        #region Asynchronous methods
        public IAsyncResult BeginWrite(string fileName, Stream stream, AsyncCallback callback)
        {
            Log.InfoFormat("Starting BeginWrite(fileName: '{0}', stream.Length: {1})", fileName, stream.Length);
            var startTime = DateTime.UtcNow;

            IAsyncResult result = null;
            try
            {
                var key = SanitizeKey(fileName);

                var client = new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
                var request = new PutObjectRequest
                {
                    BucketName = BucketName,
                    ContentType = ContentType,
                    StorageClass = S3StorageClass.Standard,
                    Key = key,
                    InputStream = stream
                };

                if (IsPublicRead)
                    request.CannedACL = S3CannedACL.PublicRead;

                if (Headers != null)
                    foreach (var headerKey in Headers.AllKeys)
                        request.Headers[headerKey] = Headers[headerKey];

                result = client.BeginPutObject(request, callback, client);
            }
            catch (AmazonS3Exception s3Ex)
            {
                Log.ErrorFormat("Amazon S3 BeginWrite returned error:\n{0}", s3Ex);
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered in BeginWrite:\n", ex);
            }

            Log.InfoFormat("Completed BeginWrite(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return result;
        }

        public void LogEndWrite(IAsyncResult asyncResult)
        {
            Log.Info("Starting LogEndWrite()");
            var startTime = DateTime.UtcNow;

            try
            {
                var client = asyncResult.AsyncState as AmazonS3Client ??
                             new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
                client.EndPutObject(asyncResult);
            }
            catch (AmazonS3Exception s3Ex)
            {
                Log.ErrorFormat("Amazon S3 EndWrite returned error:\n{0}", s3Ex);
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered in EndWrite:\n", ex);
            }

            Log.InfoFormat("Completed LogEndWrite(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
        }

        public HttpStatusCode EndWrite(IAsyncResult asyncResult)
        {
            Log.Info("Starting EndWrite()");
            var startTime = DateTime.UtcNow;

            var statusCode = HttpStatusCode.ExpectationFailed;
            try
            {
                var client = asyncResult.AsyncState as AmazonS3Client ??
                             new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
                var response = client.EndPutObject(asyncResult);

                statusCode = response.HttpStatusCode;
            }
            catch (AmazonS3Exception s3Ex)
            {
                Log.ErrorFormat("Amazon S3 EndWrite returned error:\n{0}", s3Ex);
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered in EndWrite:\n", ex);
            }

            Log.InfoFormat("Completed EndWrite(). Returned {0}. timeElapsed: {1}", statusCode, DateTime.UtcNow.Subtract(startTime));
            return statusCode;
        }

        public void LogEndPutBucket(IAsyncResult asyncResult)
        {
            Log.Info("Starting LogEndPutBucket()");
            var startTime = DateTime.UtcNow;

            try
            {
                var client = asyncResult.AsyncState as AmazonS3Client ??
                             new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
                client.EndPutBucket(asyncResult);
            }
            catch (AmazonS3Exception s3Ex)
            {
                Log.ErrorFormat("Amazon S3 LogEndPutBucket returned error:\n{0}", s3Ex);
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered in LogEndPutBucket:\n", ex);
            }

            Log.InfoFormat("Completed LogEndPutBucket(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
        }
        public void CallbackListBucket(IAsyncResult asyncResult)
        {
            Log.InfoFormat("Starting CallbackListBucket()");
            var startTime = DateTime.UtcNow;

            try
            {
                var client = asyncResult.AsyncState as AmazonS3Client ??
                             new AmazonS3Client(AmazonHelper.GetCredentials(), AmazonHelper.GetRegion());
                var response = client.EndListBuckets(asyncResult);
                var bucket = response.Buckets.FirstOrDefault(x => x.BucketName == BucketName);
                if (bucket == null)
                {
                    // Create bucket specify the bucket region on creation, so we can specify bucket region on read thus avoiding redirects.
                    var createBucketRequest = new PutBucketRequest() { BucketName = BucketName };
                    client.BeginPutBucket(createBucketRequest, LogEndPutBucket, client);

                }
            }
            catch (AmazonS3Exception s3Ex)
            {
                Log.ErrorFormat("Amazon S3 CallbackListBucket returned error:\n{0}", s3Ex);
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered in CallbackListBucket:\n", ex);
            }

            Log.InfoFormat("Completed CallbackListBucket(). timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
        }
        #endregion
    }
}
