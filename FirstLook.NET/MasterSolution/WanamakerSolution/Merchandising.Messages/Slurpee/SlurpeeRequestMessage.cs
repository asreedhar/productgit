﻿namespace Merchandising.Messages.Slurpee
{
    public class SlurpeeRequestMessage
    {
        public int bu_id { get; set; }
        public int site_id { get; set; }
        public string vin { get; set; }
        public string user_name { get; set; }
        public string pass_word { get; set; }
        public string correlation_id { get; set; }
        public string creds_proceed_webhook { get; set; }
        public string creds_pass_webhook { get; set; }
        public string creds_fail_webhook { get; set; }
        public string dealer_code { get; set; }

    }
}


