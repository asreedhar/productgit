﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Core.Messages;


namespace Merchandising.Messages.Slurpee
{

    
    public class SlurpeeResultMessage : MessageBase
    {
        public SlurpeeMeta meta { get; set; }
        public SlurpeeVehicle Vehicle { get; set; }
        public SlurpeeError error { get; set; }
    }
    public class SlurpeeVehicle
    {
        public string vin { get; set; }
        public string oem { get; set; }
        public string year { get; set; }
        public string model { get; set; }
        public string trim { get; set; }
        public string site { get; set; }
        public string msrp { get; set; }
        public SlurpeeOption engine { get; set; }
        public SlurpeeOption transmission { get; set; }
        public string drive { get; set; }
        public List<SlurpeeOption> options { get; set; }
        public SlurpeeOption ext_color { get; set; }
        public SlurpeeOption int_color { get; set; }

    }

    public class SlurpeeOption
    {
        public string code { get; set; }
        public string description { get; set; }
    }

    public class SlurpeeMeta
    {
        public int bu_id { get; set; }
        public int site_id { get; set; }
        public string vin { get; set; }
    }

    public class SlurpeeError
    {
        public int err_code { get; set; }
        public string err_details { get; set; }
        public string ext_err_details { get; set; }
    }

}
