﻿using Core.Messages;
using Core.Messaging;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.AutoOffline;
using Merchandising.Messages.AutoWindowSticker;
using Merchandising.Messages.GroupLevel;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.Slurpee;
using Merchandising.Messages.VehicleTransfer;
using Merchandising.Messages.WebLoader;
using Merchandising.Messages.GoogleAnalytics;

namespace Merchandising.Messages
{
    public interface IQueueFactory
    {
        /********Auto Approve**********/
        IQueue<InventoryMessage> CreateInventoryChanged();
        IQueue<AutoApproveInventoryMessage> CreateAutoApproveInventory();
        IQueue<AutoApproveMessage> CreateAutoApprove();
        
        /******Approval Queues**********/
        IQueue<ApproveMessage> CreateApprove(); 
        IQueue<CreatePdfMessage> CreatePdf();
        
        /******** Emails **************/

        /******** Auto Load **********/
        IQueue<FetchAuditMessage> CreateAutoLoadAudit();
        IQueue<AutoLoadResultMessage> CreateAutoLoadResult();
        IQueue<AutoLoadInventoryMessage> CreateAutoLoadInventory();
        IQueue<NewInventoryMessage> CreateAutoLoadNewInventory();
        IQueue<VinDecodedMessage> CreateVinDecodedQueue();
        IQueue<FordDirectAutoLoadMessage> CreateFordDirectAutoLoadMessageQueue();
        IQueue<SlurpeeResultMessage> CreateSlurpeeResultQueue();
        IQueue<HondaEconfigBuildRequestMessage> CreateHondaEconfigRequestQueue();

        /******** General Merchandising ********/
        IQueue<NewInventoryMessage> CreateMerchandisingNewInventory();
        IQueue<AutoWindowStickerMessage> CreateAutoWindowSticker();
        IQueue<ClickStatsMessage> CreateWindowStickClickStats();
        IQueue<AutoWindowStickerReport> CreateWindowStickerReportQueue();
            
        /******** Auto Offline ********/
        IQueue<ProcessAutoOfflineMessage> CreateProcessAutoOffline();


        /*******Group Level***********/
        IQueue<GroupLevelAggregationMessage> CreateGroupLevelAggregation();

        /*******WebLoader*********/
        IQueue<PhotoUploadMessage> CreateWebLoaderPhotoUpload();
        IQueue<PhotoBatchUploadMessage> CreateWebLoaderAbsolutePosition();
        IQueue<DirectPhotoUploadMessage> CreateWebLoaderDirectPhotoUpload(); 
        
        IQueue<PhotoTransferMessage> CreatePhotoTransferQueue();

        IQueue<DeadLetterMessage> CreateDeadLetterArchive();

        // aseshadri 8/6: Moved to IDiscountCampaignQueueFactory
        /**************Campaign Discounts -- New batch car pricing feature-- *****************/
        //IQueue<DiscountCampaignExpirationMessage> CreateDiscountCampaignExpirationQueue();
        //IQueue<ApplyInventoryDiscountCampaignMessage> CreateApplyInventoryDiscountCampaignQueue();
        //IQueue<BatchPriceChangeMessage> CreateBatchPriceChangeQueue();
        //IQueue<StyleSetMessage> CreateCampaignStyleSet();
       

        /******* Google Analytics *********/
        IQueue<GoogleAnalyticsGenerateMessage> CreateGoogleAnalyticsQueue();
            
        // Inactive Inventory Activity task control queue
        IQueue<InactiveInventoryTaskStartMessage> CreateInactiveInventoryTaskControlQueue();
    }
}
