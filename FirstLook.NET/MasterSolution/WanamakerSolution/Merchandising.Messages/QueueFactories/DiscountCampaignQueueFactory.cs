using AmazonServices.SQS;
using Core.Messages;
using Core.Messaging;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.InventoryDocuments;

namespace Merchandising.Messages.QueueFactories
{
    public class DiscountCampaignQueueFactory : IDiscountCampaignQueueFactory
    {
        public IQueue<DiscountCampaignExpirationMessage> CreateDiscountCampaignExpirationQueue()
        {
            var queue = AmazonQueueFactory<DiscountCampaignExpirationMessage>.CreateAmazonQueue("Incisent_Merchandising_DiscountCampaign_Expiration");
            queue.DeadLetterErrorLimit = QueueFactory.CriticalRetryLimit;
            return queue;
        }

        public IQueue<BatchPriceChangeMessage> CreateBatchPriceChangeQueue()
        {
            var queue = AmazonQueueFactory<BatchPriceChangeMessage>.CreateAmazonQueue("Incisent_Merchandising_BatchPriceChange");
            queue.DeadLetterErrorLimit = QueueFactory.CriticalRetryLimit;
            return queue;
        }

        public IQueue<ApplyInventoryDiscountCampaignMessage> CreateApplyInventoryDiscountCampaignQueue()
        {
            var queue = AmazonQueueFactory<ApplyInventoryDiscountCampaignMessage>.CreateAmazonQueue("Incisent_Merchandising_InventoryDiscountCampaign");
            queue.DeadLetterErrorLimit = QueueFactory.CriticalRetryLimit;
            return queue;
        }

        public IQueue<StyleSetMessage> CreateCampaignStyleSet()
        {
            var queue = AmazonQueueFactory<StyleSetMessage>.CreateAmazonQueue("Incisent_Merchandising_CampaignStyleSet");
            queue.DeadLetterErrorLimit = QueueFactory.CriticalRetryLimit;
            return queue;
        }

        public IQueue<DealerCommand> CreateReprocessDealerInventoryForDiscountsQueue()
        {
            var queue = AmazonQueueFactory<DealerCommand>.CreateAmazonQueue("Incisent_Merchandising_ReprocessDealerInventoryForDiscounts");
            queue.DeadLetterErrorLimit = QueueFactory.CriticalRetryLimit;
            return queue;
        }
    }
}