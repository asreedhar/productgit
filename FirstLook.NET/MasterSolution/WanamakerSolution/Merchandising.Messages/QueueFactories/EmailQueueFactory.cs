using AmazonServices.SQS;
using Core.Messaging;
using MAX.Entities.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.BucketAlerts;

namespace Merchandising.Messages.QueueFactories
{
    internal class EmailQueueFactory : IEmailQueueFactory
    {
        public IQueue<DailyEmailMessage> CreateEmail()
        {
            return AmazonQueueFactory<DailyEmailMessage>.CreateAmazonQueue("Incisent_Merchandising_DailyEmail");
        }

        public IQueue<BucketAlertMessage> CreateBucketAlert()
        {
            return AmazonQueueFactory<BucketAlertMessage>.CreateAmazonQueue("Incisent_Merchandising_BucketAlerts");
        }

        public IQueue<BucketValueMessage> CreateBucketValueQueue()
        {
            return AmazonQueueFactory<BucketValueMessage>.CreateAmazonQueue("Incisent_Merchandising_BucketValues");
        }
    }
}