﻿using AmazonServices.SQS;
using Core.Messaging;
using Merchandising.Messages.InventoryDocuments;

namespace Merchandising.Messages.QueueFactories
{
    internal class DocumentPublishingQueueFactory : IDocumentPublishingQueueFactory
    {
        public IQueue<MaxPhotoPublishMessage> MaxPublishPhotosQueue()
        {
            return AmazonQueueFactory<MaxPhotoPublishMessage>.CreateAmazonQueue("max-publish-photos", useGlobalNamingRules: true, nameDelimiter: '-');
        }

        public IQueue<DealerCommand> MaxPublishDealerPhotoDocsQueue()
        {
            return AmazonQueueFactory<DealerCommand>.CreateAmazonQueue("max-publish-dealer-photos", useGlobalNamingRules: true, nameDelimiter: '-');
        }

        public IQueue<MaxDocumentPublishMessage> MaxPublishMarketingDocumentsQueue()
        {
            return AmazonQueueFactory<MaxDocumentPublishMessage>.CreateAmazonQueue("max-publish-marketing-documents", useGlobalNamingRules: true, nameDelimiter: '-');
        }

        public IQueue<MaxDocumentPublishMessage> MaxPublishEquipmentDocumentsQueue()
        {
            return AmazonQueueFactory<MaxDocumentPublishMessage>.CreateAmazonQueue("max-publish-equipment-documents", useGlobalNamingRules: true, nameDelimiter: '-');
        }

        public IQueue<MaxDocumentPublishMessage> MaxPublishOptionDocumentsQueue()
        {
            return AmazonQueueFactory<MaxDocumentPublishMessage>.CreateAmazonQueue("max-publish-option-documents", useGlobalNamingRules: true, nameDelimiter: '-');
        }

        public IQueue<MaxDocumentPublishMessage> MaxPublishReferenceDocumentsQueue()
        {
            return AmazonQueueFactory<MaxDocumentPublishMessage>.CreateAmazonQueue("max-publish-reference-documents", useGlobalNamingRules: true, nameDelimiter: '-');
        }

        public IQueue<MaxDocumentPublishMessage> MaxPublishAdDescriptionDocumentsQueue()
        {
            return AmazonQueueFactory<MaxDocumentPublishMessage>.CreateAmazonQueue("max-publish-adDescription-documents", useGlobalNamingRules: true, nameDelimiter: '-');
        }

        public IQueue<MaxDocumentPublishMessage> MaxPublishCarFaxReportUrlsQueue()
        {
            return AmazonQueueFactory<MaxDocumentPublishMessage>.CreateAmazonQueue("max-publish-carfax-report-urls", useGlobalNamingRules: true, nameDelimiter: '-');
        }


        public IQueue<MaxDocumentPublishMessage> MaxPublishMarketingHtmlDocumentsQueue()
        {
            return AmazonQueueFactory<MaxDocumentPublishMessage>.CreateAmazonQueue("max-publish-marketing-html-documents", useGlobalNamingRules: true, nameDelimiter: '-');
        }
    }
}
