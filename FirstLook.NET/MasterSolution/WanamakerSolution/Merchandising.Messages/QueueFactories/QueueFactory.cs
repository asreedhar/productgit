﻿using System;
using AmazonServices.SQS;
using Core.Messages;
using Core.Messaging;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.AutoOffline;
using Merchandising.Messages.AutoWindowSticker;
using Merchandising.Messages.GoogleAnalytics;
using Merchandising.Messages.GroupLevel;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.Slurpee;
using Merchandising.Messages.VehicleTransfer;
using Merchandising.Messages.WebLoader;

namespace Merchandising.Messages.QueueFactories
{
    /// <summary>
    /// TODO: Good news, queue factories are on sale. Seriously though, break this thing up.
    /// It's causing all sorts of down-stream cruft.  ZB 5/2014
    /// </summary>
    internal class QueueFactory : IQueueFactory
    {
        public const int CriticalRetryLimit = 144; // 24 hrs @ 6/hr

        public IQueue<AutoApproveMessage> CreateAutoApprove()
        {
            var queue = AmazonQueueFactory<AutoApproveMessage>.CreateAmazonQueue("Incisent_Merchandising_AutoApprove");
            queue.DeadLetterErrorLimit = CriticalRetryLimit;
            return queue;
        }

        public IQueue<ApproveMessage> CreateApprove()
        {
            var queue = AmazonQueueFactory<ApproveMessage>.CreateAmazonQueue("Incisent_Merchandising_Approve");
            queue.DeadLetterErrorLimit = CriticalRetryLimit;
            return queue;
        }
        
        public IQueue<CreatePdfMessage> CreatePdf()
        {
            return AmazonQueueFactory<CreatePdfMessage>.CreateAmazonQueue("Incisent_Merchandising_CreatePdf");
        }

        public IQueue<InventoryMessage> CreateInventoryChanged()
        {
            return AmazonQueueFactory<InventoryMessage>.CreateAmazonQueue("Incisent_Merchandising_InventoryChanged");
        }

        public IQueue<AutoApproveInventoryMessage> CreateAutoApproveInventory()
        {
            var queue = AmazonQueueFactory<AutoApproveInventoryMessage>.CreateAmazonQueue("Incisent_Merchandising_AutoApproveInventory");
            queue.DeadLetterErrorLimit = CriticalRetryLimit;
            return queue;
        }


        /******* Group Level********/

        public IQueue<GroupLevelAggregationMessage> CreateGroupLevelAggregation()
        {
            return AmazonQueueFactory<GroupLevelAggregationMessage>.CreateAmazonQueue("Incisent_Merchandising_GroupLevelAggregation", TimeSpan.FromHours(4));
        }


        /*******WebLoader*********/

        public IQueue<PhotoUploadMessage> CreateWebLoaderPhotoUpload()
        {
            var queue = AmazonQueueFactory<PhotoUploadMessage>.CreateAmazonQueue("Incisent_Merchandising_WebLoader_PhotoUpload");
            queue.DeadLetterErrorLimit = CriticalRetryLimit;
            return queue;
        }

        public IQueue<PhotoBatchUploadMessage> CreateWebLoaderAbsolutePosition()
        {
            var queue = AmazonQueueFactory<PhotoBatchUploadMessage>.CreateAmazonQueue("Incisent_Merchandising_WebLoader_PhotoAbsolutePosition");
            queue.DeadLetterErrorLimit = CriticalRetryLimit;
            return queue;
        }

        public IQueue<DirectPhotoUploadMessage> CreateWebLoaderDirectPhotoUpload()
        {
            var queue =
                AmazonQueueFactory<DirectPhotoUploadMessage>.CreateAmazonQueue(
                    "Incisent_Merchandising_WebLoader_DirectPhotoUpload");
            queue.DeadLetterErrorLimit = CriticalRetryLimit;
            return queue;
        }

        /***********AutoLoad *************/

        public IQueue<FetchAuditMessage> CreateAutoLoadAudit()
        {
            return AmazonQueueFactory<FetchAuditMessage>.CreateAmazonQueue("Incisent_AutoLoad_Audit");
        }

        public IQueue<AutoLoadResultMessage> CreateAutoLoadResult()
        {
            return AmazonQueueFactory<AutoLoadResultMessage>.CreateAmazonQueue("Incisent_AutoLoad_Result");
        }

        public IQueue<AutoLoadInventoryMessage> CreateAutoLoadInventory()
        {
            return AmazonQueueFactory<AutoLoadInventoryMessage>.CreateAmazonQueue("Incisent_AutoLoad_AutoLoadInventory");
        }

        public IQueue<NewInventoryMessage> CreateAutoLoadNewInventory()
        {
            return AmazonQueueFactory<NewInventoryMessage>.CreateAmazonQueue("Incisent_AutoLoad_NewInventory");
        }
        
        public IQueue<VinDecodedMessage> CreateVinDecodedQueue()
        {
            return AmazonQueueFactory<VinDecodedMessage>.CreateAmazonQueue("Incisent_AutoLoad_VinDecoded");            
        }

        public IQueue<FordDirectAutoLoadMessage> CreateFordDirectAutoLoadMessageQueue()
        {
            return AmazonQueueFactory<FordDirectAutoLoadMessage>.CreateAmazonQueue("Incisent_AutoLoad_FordDirectAutoLoad");                        
        }

        public IQueue<SlurpeeResultMessage> CreateSlurpeeResultQueue()
        {
            return AmazonQueueFactory<SlurpeeResultMessage>.CreateAmazonQueue("max-slurpee-build-data", '-', true);
        }

        public IQueue<HondaEconfigBuildRequestMessage> CreateHondaEconfigRequestQueue()
        {
            return AmazonQueueFactory<HondaEconfigBuildRequestMessage>.CreateAmazonQueue("max-build-request-honda", '-', true);
        }

        /******** General Merchandising ********/
        public IQueue<NewInventoryMessage> CreateMerchandisingNewInventory()
        {
            return AmazonQueueFactory<NewInventoryMessage>.CreateAmazonQueue("Incisient_Merchandising_NewInventory");
        }

        public IQueue<ProcessAutoOfflineMessage> CreateProcessAutoOffline()
        {
            return AmazonQueueFactory<ProcessAutoOfflineMessage>.CreateAmazonQueue("Incisent_Merchandising_AutoOffline");
        }
        
        public IQueue<PhotoTransferMessage> CreatePhotoTransferQueue()
        {
            return AmazonQueueFactory<PhotoTransferMessage>.CreateAmazonQueue("Incisent_Photos_Transfer");
        }

        public IQueue<AutoWindowStickerMessage> CreateAutoWindowSticker()
        {
            return AmazonQueueFactory<AutoWindowStickerMessage>.CreateAmazonQueue("Incisent_Merchandising_AutoWindowSticker");
        }
        public IQueue<ClickStatsMessage> CreateWindowStickClickStats()
        {
            return AmazonQueueFactory<ClickStatsMessage>.CreateAmazonQueue("Incisent_Merchandising_WindowStickerClickStats");
        }

        public IQueue<AutoWindowStickerReport> CreateWindowStickerReportQueue()
        {
            return AmazonQueueFactory<AutoWindowStickerReport>.CreateAmazonQueue("Incisent_Merchandising_AutoWindowStickerReport");
        }



        /***
         * DeadLetter Queue Factory
         ***/
        public IQueue<DeadLetterMessage> CreateDeadLetter()
        {
            return AmazonQueueFactory<DeadLetterMessage>.CreateAmazonQueue("Incisent_Core_DeadLetter");
        }
        public IQueue<DeadLetterMessage> CreateDeadLetterArchive()
        {
            return AmazonQueueFactory<DeadLetterMessage>.CreateAmazonQueue("Incisent_Core_DeadLetter_Archive");
        }


        /******* Google Analytics *********/
        public IQueue<GoogleAnalyticsGenerateMessage> CreateGoogleAnalyticsQueue()
        {
            return AmazonQueueFactory<GoogleAnalyticsGenerateMessage>.CreateAmazonQueue("Incisent_Merchandising_GoogleAnalytics");
        }

        // Document publishing queues - TODO: Move these to a different queue factory.  They don't all have to be lumped into a monolith.
        public IQueue<InactiveInventoryTaskStartMessage> CreateInactiveInventoryTaskControlQueue()
        {
            return AmazonQueueFactory<InactiveInventoryTaskStartMessage>.CreateAmazonQueue("max-inactive-inventory-task-control", 
            useGlobalNamingRules: true, nameDelimiter: '-', visibilityTimeout: TimeSpan.FromHours(1) );
        }
    }
}
