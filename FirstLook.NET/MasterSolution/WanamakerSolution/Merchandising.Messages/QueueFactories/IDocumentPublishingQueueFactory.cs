﻿using Core.Messaging;
using Merchandising.Messages.InventoryDocuments;

namespace Merchandising.Messages.QueueFactories
{
    public interface IDocumentPublishingQueueFactory
    {
        IQueue<MaxPhotoPublishMessage> MaxPublishPhotosQueue();
        IQueue<DealerCommand> MaxPublishDealerPhotoDocsQueue();

        // MAX Document Publishing Queues
        IQueue<MaxDocumentPublishMessage> MaxPublishMarketingDocumentsQueue();
        IQueue<MaxDocumentPublishMessage> MaxPublishEquipmentDocumentsQueue();
        IQueue<MaxDocumentPublishMessage> MaxPublishOptionDocumentsQueue();
        IQueue<MaxDocumentPublishMessage> MaxPublishReferenceDocumentsQueue();
        IQueue<MaxDocumentPublishMessage> MaxPublishAdDescriptionDocumentsQueue();
        IQueue<MaxDocumentPublishMessage> MaxPublishCarFaxReportUrlsQueue();
        IQueue<MaxDocumentPublishMessage> MaxPublishMarketingHtmlDocumentsQueue();
    }
}