﻿using Core.Messages;
using Core.Messaging;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.InventoryDocuments;

namespace Merchandising.Messages.QueueFactories
{
    public interface IDiscountCampaignQueueFactory
    {
        IQueue<DiscountCampaignExpirationMessage> CreateDiscountCampaignExpirationQueue();
        IQueue<BatchPriceChangeMessage> CreateBatchPriceChangeQueue();
        IQueue<ApplyInventoryDiscountCampaignMessage> CreateApplyInventoryDiscountCampaignQueue();
        IQueue<StyleSetMessage> CreateCampaignStyleSet();
        IQueue<DealerCommand> CreateReprocessDealerInventoryForDiscountsQueue();
    }
}
