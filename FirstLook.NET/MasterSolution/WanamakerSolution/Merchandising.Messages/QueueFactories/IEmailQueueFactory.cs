﻿using Core.Messaging;
using MAX.Entities.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.BucketAlerts;

namespace Merchandising.Messages.QueueFactories
{
    public interface IEmailQueueFactory
    {
        IQueue<DailyEmailMessage> CreateEmail();
        IQueue<BucketAlertMessage> CreateBucketAlert();
        IQueue<BucketValueMessage> CreateBucketValueQueue();
    }
}