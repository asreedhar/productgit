﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messaging;

namespace Merchandising.Messages
{
    public interface INoSqlDbFactory
    {
        INoSqlDb GetBucketValueDb();
    }
}
