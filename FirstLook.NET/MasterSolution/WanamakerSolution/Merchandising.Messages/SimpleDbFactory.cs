﻿using AmazonServices.SDB;
using Core.Messaging;

namespace Merchandising.Messages
{
    public class SimpleDbFactory : ISimpleDbFactory
    {
        public ISimpleDb GetAutoLoadAuditABTestDb()
        {
            return new SimpleDb("Incisent_Merchandising_AutoLoadAudit_ABTest");
        }

        public ISimpleDb GetDealerRaterDb()
        {
            // override the access key and secret access key
            return new SimpleDb("dealer_rater_id_map", "AKIAITEDBHOMPK43RTSA", "Fdne0XCXqaIq8Wilxjem05g3lKmu7K+AxdEJOJAH", forShowroom: true);
        }

        public ISimpleDb GetGoogleAnalyticsReportDb()
        {
            return new GoogleAnalyticsReportDb();
        }
    }
}
