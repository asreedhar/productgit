﻿namespace Merchandising.Messages
{
    //These values are persisted in the database [Merchandising].[builder].[AutoloadStatusTypes]
    //so please do not change the numbers.
    public enum Status
    {
        UnknownError = -100,
        InvalidCredentials = -99,
        NoVehicleFound = -98,
        NetworkError = -97,
        InsufficientPermissions = -96,

        // after build request has been downloaded ... decoding errors?
        CannotDecodeStyle = -89,
        CannotDecodeExtColor = -88,

        // utility errors
        Delayed = -9,
        ProlongedDelayed = -8,
        
        Pending = 0,

        Successful = 100,
        
    }
}
