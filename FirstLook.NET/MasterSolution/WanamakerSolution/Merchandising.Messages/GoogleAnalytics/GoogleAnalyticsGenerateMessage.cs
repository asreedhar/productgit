﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;
using FirstLook.Common.Core;

namespace Merchandising.Messages.GoogleAnalytics
{
    public class GoogleAnalyticsGenerateMessage : MessageBase
    {
        public int BusinessUnitId { get; set; }
        public string ProfileType { get; set; }
        public DateRange DateRange { get; set; }
        public bool Force { get; set; }

        public GoogleAnalyticsGenerateMessage() { }
        
        public GoogleAnalyticsGenerateMessage(int businessUnitId, string profileType, DateRange dateRange) : 
            this(businessUnitId, profileType, dateRange, false)
        { }

        public GoogleAnalyticsGenerateMessage(int businessUnitId, string profileType, DateRange dateRange, bool force)
        {
            BusinessUnitId = businessUnitId;
            ProfileType = profileType;
            DateRange = dateRange;
            Force = force;
        }
    }
}
