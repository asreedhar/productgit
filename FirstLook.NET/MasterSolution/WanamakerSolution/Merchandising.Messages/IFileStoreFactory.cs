﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messaging;

namespace Merchandising.Messages
{
    public interface IFileStoreFactory
    {
        IFileStorage CreateFileStore();
        IFileStorage CreateFileStore(Boolean isAsync);
        IFileStorage CreateFileStore(System.Collections.Specialized.NameValueCollection headers);
        IFileStorage CreatePdfFileStore(System.Collections.Specialized.NameValueCollection headers);
        IFileStorage CreateAutoLoadAudit();
        IFileStorage WebLoaderApiPhotos();
        IFileStorage DeadLetterArchive(System.Collections.Specialized.NameValueCollection headers = null);
        IFileStorage MaxDocStorage(System.Collections.Specialized.NameValueCollection headers = null, string contentType = "text/plain");
        IFileStorage CreateWindowSticker();
        IFileStorage CreateBionicReportStorage();
	    IFileStorage CreateBionicImageStorage();
        IFileStorage CreateBionicTrackingStorage();
    }
}
