﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class FetchAuditMessageStored : MessageBase
    {
        public FetchAuditMessageStored(string source, string xml)
        {
            Source = source;
            Xml = xml;
        }

        public string Source { get; set; }
        public string Xml { get; set; }
    }
}
