﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class FordOpenAutoloadMessage : MessageBase
    {
        public int BusinessUnitId { get; set; }
        public string Vin { get; set; }
    }
}