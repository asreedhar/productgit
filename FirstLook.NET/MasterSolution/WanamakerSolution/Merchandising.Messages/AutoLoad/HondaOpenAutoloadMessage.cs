﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    class HondaOpenAutoloadMessage : MessageBase
    {
        public int BusinessUnitId { get; set; }
        public string Vin { get; set; }
        public string Make { get; set; }
    }
}
