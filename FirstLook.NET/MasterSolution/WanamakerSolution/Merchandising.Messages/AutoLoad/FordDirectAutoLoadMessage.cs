﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class FordDirectAutoLoadMessage : MessageBase
    {
        public int BusinessUnitId { get; set; }
        public string DealerPaCode { get; set; }

        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
    }
}