﻿namespace Merchandising.Messages.AutoLoad
{
    public class BuildRequestEnvelope
    {
        public string DataSource { get; set; }
        public string Aquired { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
    }
}
