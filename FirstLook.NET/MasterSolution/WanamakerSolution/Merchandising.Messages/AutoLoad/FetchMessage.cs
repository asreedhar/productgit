﻿using System;
using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class FetchMessage : MessageBase, IActiveInventoryMessage
    {
        //Needed for serialization
        //A class should either have a default constructor, one constructor with arguments or a constructor marked with the JsonConstructor attribute
        public FetchMessage()
        {
        }

        public FetchMessage(int businessUnitId, int inventoryId, Agent agent, string userName, string password, string vin, int inventoryType)
            : this(businessUnitId, inventoryId, agent, userName, password, vin, inventoryType, String.Empty)
        {
        }

        public FetchMessage(int businessUnitId, int inventoryId, Agent agent, string userName, string password, string vin, int inventoryType, string dealerCode)
        {
            BusinessUnitId = businessUnitId;
            InventoryId = inventoryId;
            Agent = agent;
            UserName = userName;
            Password = password;
            Vin = vin;
            InventoryType = inventoryType;
            DealerCode = dealerCode;
        }
       
        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public Agent Agent { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Vin { get; set; }
        public int InventoryType { get; set; }
        public string DealerCode { get; set; }
    }
}
