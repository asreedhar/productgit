﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;
using Core.Messaging;

namespace Merchandising.Messages.AutoLoad
{
    public class FetchAuditMessage : MessageBase
    {
        public FetchAuditMessage(string vin, string xml)
        {
            Vin = vin;
            Xml = xml;
        }

        public string Vin { get; set; }
        public string Xml { get; set; }
    }
}
