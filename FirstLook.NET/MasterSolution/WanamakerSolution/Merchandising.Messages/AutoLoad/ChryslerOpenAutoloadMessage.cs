﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class ChryslerOpenAutoloadMessage : MessageBase
    {
        public int BusinessUnitId { get; set; }
        public string Vin { get; set; }
    }
}