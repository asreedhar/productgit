﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class AutoLoadInventoryMessage : MessageBase
    {
        public AutoLoadInventoryMessage(int businessUnitId, AutoLoadInventoryType type)
        {
            BusinessUnitId = businessUnitId;
            Type = type;
        }

         //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
         //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute 
        public int BusinessUnitId { get; set; }
        public AutoLoadInventoryType Type { get; set; }
        public Agent Agent { get; set; }
    }
}
