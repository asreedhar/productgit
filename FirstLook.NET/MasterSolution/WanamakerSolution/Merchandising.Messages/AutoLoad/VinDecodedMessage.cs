﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    /// <summary>
    /// A VinDecodedMessage represents data which has been determined by some external process.
    /// All properties have already been calculated and set.  All that needs to happen now
    /// is for the system to save the data and fire any downstream events.
    /// 
    /// The main idea here is to decouple how autoload data is determined (the processes which send these messages)
    /// from the act of saving/applying it to the system.
    /// </summary>
    public class VinDecodedMessage : MessageBase
    {
        public string Vin { get; set; }

        public int CountryCode { get; set; }
        public int ChromeStyleId { get; set; }
        public string[] OptionCodes { get; set; }

        public string ExteriorManufacturerColorCode { get; set; }
        public string ExteriorManufacturerColorDesc { get; set; }
        
        public string InteriorManufacturerColorCode { get; set; }
        public string InteriorManufacturerColorDesc { get; set; }

        /// <summary>
        /// The date/time at which this data was determined.
        /// </summary>
        public string TimeStamp { get; set; }

        /// <summary>
        /// The process name which determined this data and sent this message
        /// </summary>
        public string SourceProcessName { get; set; }

        /// <summary>
        /// A URL which points to any data document which was used to support/infer/deduce the data points in this message.
        /// </summary>
        public string SourceDataUrl { get; set; }

    }
}
