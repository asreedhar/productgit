﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class AutoLoadResultMessage : MessageBase, IActiveInventoryMessage
    {
        public AutoLoadResultMessage(int businessUnitId, int inventoryId, string result, Status status, Agent agent, int inventoryType)
        {
            BusinessUnitId = businessUnitId;
            InventoryId = inventoryId;
            XmlResult = result;
            Status = status;
            Agent = agent;
            InventoryType = inventoryType;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string XmlResult { get; set; }
        public Status Status { get; set; }
        public Agent Agent { get; set; }
        public int InventoryType { get; set; }
    }
}
