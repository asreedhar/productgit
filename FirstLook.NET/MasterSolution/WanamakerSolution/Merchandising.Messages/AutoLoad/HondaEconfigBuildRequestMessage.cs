﻿using Core.Messages;

namespace Merchandising.Messages.AutoLoad
{
    public class HondaEconfigBuildRequestMessage : MessageBase, IActiveInventoryMessage
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string Vin { get; set; }
        public string Make { get; set; }
    }
}
