
namespace Merchandising.Messages.VehicleBuild
{
    public class OptionPackage
    {
        private string oemCode;
        private string providerDescription;
        public OptionPackage(string oemCode)
        {
            this.oemCode = oemCode;
            providerDescription = string.Empty;
        }
        public OptionPackage(string oemCode, string providerDescription)
        {
            this.oemCode = oemCode;
            this.providerDescription = providerDescription;
        }

        public string ProviderDescription
        {
            get { return providerDescription; }
            set { providerDescription = value; }
        }

        public string OemCode
        {
            get { return oemCode; }
            set { oemCode = value; }
        }
    }
}