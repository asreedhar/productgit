﻿using System.Collections.Generic;

namespace Merchandising.Messages.VehicleBuild
{
    public class OptionFilter
    {
        public decimal? MinumumMsrp  { get; set; }
        public List<string> IncludePhrases { get; set; }
        public List<string> ExcludePhrases { get; set; }

        public OptionFilter()
        {
            MinumumMsrp = null;  
            IncludePhrases = new List<string>();
            ExcludePhrases = new List<string>();
        }

    }
}
