
namespace Merchandising.Messages.VehicleBuild
{
    public class VehicleBuildResult
    {
        public Status Status { get; private set; }
        public VehicleBuildData BuildData { get; private set; }

        public VehicleBuildResult(Status status, VehicleBuildData buildData)
        {
            Status = status;
            BuildData = buildData;
        }
    }
}