using System.Collections.Generic;

namespace Merchandising.Messages.VehicleBuild
{
    public class VehicleBuildData
    {
        public VehicleBuildData()
        {
            UnfilteredOptionPackages = new List<OptionPackage>();
            OptionPackages = new List<OptionPackage>();
        }

        public VehicleBuildData(List<OptionPackage> optionPackages, List<OptionPackage> unfilteredOptionPackages, int? styleId, string trim, 
            string oemModelCode, string exteriorColorCode, string exteriorColor, string interiorColorCode, string interiorColor, decimal estimatedMSRP)
        {
            OptionPackages = optionPackages;
            UnfilteredOptionPackages = unfilteredOptionPackages;
            StyleId = styleId;
            Trim = trim;
            OemModelCode = oemModelCode;
            ExteriorColorCode = exteriorColorCode;
            ExteriorColor = exteriorColor;
            InteriorColorCode = interiorColorCode;
            InteriorColor = interiorColor;
            EstimatedMSRP = estimatedMSRP;
        }

        public List<OptionPackage> UnfilteredOptionPackages { get; set; }
        public List<OptionPackage> OptionPackages { get; set; }
        public int? StyleId { get; set; }
        public string Trim { get; set; }
        public string OemModelCode { get; set; }
        public string ExteriorColorCode { get; set; }
        public string ExteriorColor { get; set; }
        public string InteriorColorCode { get; set; }
        public string InteriorColor { get; set; }
        public decimal EstimatedMSRP { get; set; }
    }
}