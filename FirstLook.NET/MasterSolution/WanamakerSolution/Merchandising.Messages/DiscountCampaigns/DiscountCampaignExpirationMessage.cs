﻿using Core.Messages;

namespace Merchandising.Messages.DiscountCampaigns
{
    public class DiscountCampaignExpirationMessage : MessageBase
    {
        public int BusinessUnitId { get; set; }
        public int CampaignId { get; set; }
    }
}