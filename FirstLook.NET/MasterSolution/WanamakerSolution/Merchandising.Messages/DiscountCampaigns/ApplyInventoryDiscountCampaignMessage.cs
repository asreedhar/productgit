﻿using Core.Messages;

namespace Merchandising.Messages.DiscountCampaigns
{
    public class ApplyInventoryDiscountCampaignMessage : MessageBase, IActiveInventoryMessage
    {
        public ApplyInventoryDiscountCampaignMessage()
        {
        }

        public ApplyInventoryDiscountCampaignMessage(int businessUnitId, int inventoryId, string activatedBy) : this(businessUnitId, inventoryId, activatedBy, false)
        {
        }

        public ApplyInventoryDiscountCampaignMessage(int businessUnitId, int inventoryId, string activatedBy, bool force)
        {
            BusinessUnitId = businessUnitId;
            InventoryId = inventoryId;
            ActivatedBy = activatedBy;
            Force = force;
        }


        public string ActivatedBy { get; set; }
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public bool Force { get; set; }
    }
}