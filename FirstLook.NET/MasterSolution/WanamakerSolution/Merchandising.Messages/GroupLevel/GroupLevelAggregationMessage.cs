﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;

namespace Merchandising.Messages.GroupLevel
{
    public class GroupLevelAggregationMessage : MessageBase
    {
        public GroupLevelAggregationMessage(int groupId)
        {
            GroupId = groupId;
        }

        public int GroupId { get; set; }
    }
}
