﻿using System;
using Core.Messages;

namespace Merchandising.Messages.BionicReport
{
    public class BionicReportMessage : MessageBase
    {
        public Int32 BusinessUnitId { get; set; }
        public String EmailAddresses { get; set; }

        public BionicReportMessage(Int32 businessUnitId, String emailAddresses)
        {
            BusinessUnitId = businessUnitId;
            EmailAddresses = emailAddresses;
        }
    }
}
