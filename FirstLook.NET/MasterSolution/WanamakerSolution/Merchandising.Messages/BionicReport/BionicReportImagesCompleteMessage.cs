﻿using System;
using System.Collections.Generic;
using MAX.Entities.Messages;
using MAX.Entities.Reports.Bionic;

namespace Merchandising.Messages.BionicReport
{
    public class BionicReportImagesCompleteMessage : BionicReportImageCreationMessage
    {
        public Dictionary<ReportImageType, string> ImageNames { get; set; }
        public BionicReportImagesCompleteMessage(int businessUnitid, String emailAddresses, String trackingSnip, String dataFileKey, Dictionary<ReportImageType, string> imageNames)
            : base(businessUnitid, emailAddresses, trackingSnip, dataFileKey)
        {
            ImageNames = imageNames;
        }
    }
}