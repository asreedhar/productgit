﻿﻿using Core.Messages;

namespace Merchandising.Messages.InventoryDocuments
{
    public class DealerDocumentPublishCommand : MessageBase
    {
        public DealerDocumentPublishCommand()
        {
        }

        public DealerDocumentPublishCommand(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }

        public int BusinessUnitId { get; set; }
    }
}