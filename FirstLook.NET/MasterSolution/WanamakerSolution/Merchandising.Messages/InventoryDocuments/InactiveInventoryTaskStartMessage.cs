﻿using System;
using System.Collections.Generic;
using Core.Messages;

namespace Merchandising.Messages.InventoryDocuments
{
    public class InactiveInventoryTaskStartMessage : MessageBase
    {
        public IEnumerable<int> BusinessUnitIds { get; set; } 
        public DateTime? IgnoreEverythingEarlierThan { get; set; }

        public InactiveInventoryTaskStartMessage()
        {
            BusinessUnitIds = new List<int>();
            IgnoreEverythingEarlierThan = null;
        }

        public InactiveInventoryTaskStartMessage(IEnumerable<int> businessUnitIds, DateTime? ignoreEverythingEarlierThan)
        {
            BusinessUnitIds = businessUnitIds;
            IgnoreEverythingEarlierThan = ignoreEverythingEarlierThan;
        }
    }
}
