﻿using Core.Messages;

namespace Merchandising.Messages.InventoryDocuments
{
    public class DocumentPublishCommand : MessageBase, IDocumentPublishCommand
    {
        public DocumentPublishCommand()
        {
        }

        public DocumentPublishCommand(int businessUnitId, string vin)
        {
            BusinessUnitId = businessUnitId;
            Vin = vin;
        }

        public int BusinessUnitId { get; set; }
        public string Vin { get; set; }
    }
}