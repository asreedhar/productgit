﻿using Core.Messages;

namespace Merchandising.Messages.InventoryDocuments
{
    public class MaxDocumentPublishMessage : DocumentPublishCommand, IActiveAndNotOfflineInventoryMessage
    {
        public MaxDocumentPublishMessage()
        {
        }

        public MaxDocumentPublishMessage(int businessUnitId, int inventoryId, string vin) : base (businessUnitId, vin)
        {
            InventoryId = inventoryId;
        }

        public int InventoryId { get; set; }
    }
}
