﻿using Core.Messages;

namespace Merchandising.Messages.InventoryDocuments
{
    public class MaxMarketingInventoryXmlMessage : MessageBase
    {
        public string Xml { get; set; }
    }
}
