﻿using Core.Messages;

namespace Merchandising.Messages.InventoryDocuments
{
    public class InventoryActivityMessage : MessageBase, IInventoryMessage
    {
        public InventoryActivityMessage(){}
        public InventoryActivityMessage(int businessUnitId, string ownerHandle, int inventoryId, string vin, string vehicleHandle,
            bool active, string inventoryReceivedDateUtc, string inventoryDeletedDateUtc)
        {
            BusinessUnitId = businessUnitId;
            OwnerHandle = ownerHandle;
            InventoryId = inventoryId;
            Vin = vin;
            VehicleHandle = vehicleHandle;
            Active = active;
            InventoryReceivedDateUtc = inventoryReceivedDateUtc;
            InventoryDeletedDateUtc = inventoryDeletedDateUtc;
        }

        public int BusinessUnitId { get; set; }
        public string OwnerHandle { get; set; }

        public int InventoryId { get; set; }
        public string Vin { get; set; }
        public string VehicleHandle { get; set; }

        public bool Active { get; set; }
        public string InventoryReceivedDateUtc { get; set; }
        public string InventoryDeletedDateUtc { get; set; }
    }
}
