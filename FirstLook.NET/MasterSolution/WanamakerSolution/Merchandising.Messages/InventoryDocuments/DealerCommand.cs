﻿﻿using Core.Messages;

namespace Merchandising.Messages.InventoryDocuments
{
    public class DealerCommand : MessageBase
    {
        public DealerCommand()
        {
        }

        public DealerCommand(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }

        public int BusinessUnitId { get; set; }
    }
}