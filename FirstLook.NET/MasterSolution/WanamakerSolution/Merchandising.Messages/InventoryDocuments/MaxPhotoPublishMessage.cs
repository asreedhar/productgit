﻿namespace Merchandising.Messages.InventoryDocuments
{
    public class MaxPhotoPublishMessage : DocumentPublishCommand
    {
        public string PhotoUrlsXml { get; set; }

        public MaxPhotoPublishMessage(int businessUnitId, string vin, string photoUrlsXml) : base(businessUnitId, vin)
        {
            PhotoUrlsXml = photoUrlsXml;
        }
    }
}
