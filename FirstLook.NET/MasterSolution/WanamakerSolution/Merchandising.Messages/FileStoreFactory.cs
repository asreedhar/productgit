﻿using System;
using AmazonServices.S3;
using Core.Messaging;
using System.Collections.Specialized;

namespace Merchandising.Messages
{
    public class FileStoreFactory : IFileStoreFactory
    {
        public IFileStorage CreateFileStore()
        {
            return new FileStore("Incisent_Merchandising_FileStore");
        }
        public IFileStorage CreateFileStore(Boolean isAsyncCall)
        {
            return new FileStore("Incisent_Merchandising_FileStore", isAsyncCall);
        }

        public IFileStorage CreateFileStore(NameValueCollection headers)
        {
            return new FileStore("Incisent_Merchandising_FileStore", headers);
        }

        public IFileStorage CreatePdfFileStore(NameValueCollection headers)
        {
            return new FileStore("Incisent_Merchandising_FileStore_Inventory", headers, "application/pdf");
        }

        public IFileStorage CreateAutoLoadAudit()
        {
            // since the encoding of the xml is UTF-8, use 'application/xml' instead of 'text/xml', 'text/xml' assumes ascII
            return new FileStore("Incisent_Merchandising_AutoLoadAudit", null, "application/xml");
        }

        public IFileStorage WebLoaderApiPhotos()
        {
            return new ImageStore("Incisent_Merchandising_WebLoader_Photos");
        }

        public IFileStorage DeadLetterArchive( NameValueCollection headers = null )
        {
            return new FileStore("Incisent_Core_DeadLetter_Archive", headers);
        }

        public IFileStorage MaxDocStorage(NameValueCollection headers = null, string contentType = "text/plain")
        {
            return new FileStore("max-docs", headers, contentType, '-', true);
        }
        public IFileStorage CreateWindowSticker()
        {
            return new FileStore("WindowSticker", null, "application/pdf", '-', true);
        }

        public IFileStorage CreateBionicReportStorage()
        {
			return new FileStore("Incisent_Merchandising_BionicReportData");
        }

	    public IFileStorage CreateBionicImageStorage()
	    {
			return new FileStore("Incisent_Merchandising_BionicReportData", null, "image/png");
	    }

        public IFileStorage CreateBionicTrackingStorage()
        {
            return new FileStore("Incisent_Merchandising_BionicReportData");
        }

        
    }
}
