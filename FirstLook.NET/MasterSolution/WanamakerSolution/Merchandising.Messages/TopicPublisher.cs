﻿using System;
using AmazonServices.SNS;

namespace Merchandising.Messages
{
    public class TopicPublisher
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Publish the specified message to the topic with the specified name.
        /// Note that the "_environment" will be appended to the end of the topic name.
        /// For example, if the topicName is "foo" and the environemnt is "bar", the topic 
        /// will be foo_bar
        /// </summary>
        /// <param name="message">The message payload</param>
        /// <param name="topicName">The topic name.</param>
        /// <returns>The message id. Returns an empty string if there was an error.</returns>
        public string Publish(string message, string topicName)
        {
            try
            {
                string arn = GenericTopic.CreateTopic(topicName, true);
                return GenericTopic.Publish(message, arn);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return string.Empty;
            }
        }
    }
}
