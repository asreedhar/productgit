﻿using AmazonServices.DDB;
using Core.Messaging;

namespace Merchandising.Messages
{
    public class NoSqlDbFactory : INoSqlDbFactory
    {
        public INoSqlDb GetBucketValueDb()
        {
            return new DynamoDb("max-merchandising-daily-bucket-values");
        }
    }
}
