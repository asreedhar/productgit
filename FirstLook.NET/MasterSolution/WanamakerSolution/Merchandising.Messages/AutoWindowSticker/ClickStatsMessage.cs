﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;

namespace Merchandising.Messages.AutoWindowSticker
{
    public class ClickStatsMessage : MessageBase
    {
        public string Link { get; set; }

        public ClickStatsMessage(string link)
        {
            Link = link;
        }

    }
}
