﻿using Core.Messages;

namespace Merchandising.Messages.AutoWindowSticker
{
    public class AutoWindowStickerMessage : MessageBase
    {
        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public string Email { get; set; }
        public short MinimumDays { get; set; }

        public AutoWindowStickerMessage(int businessUnitId, string email, short minimumDays)
        {
            BusinessUnitId = businessUnitId;
            Email = email;
            MinimumDays = minimumDays;
        }
    }
}
