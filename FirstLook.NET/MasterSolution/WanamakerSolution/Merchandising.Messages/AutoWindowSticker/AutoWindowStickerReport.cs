﻿using Core.Messages;

namespace Merchandising.Messages.AutoWindowSticker
{

    public class AutoWindowStickerReport : MessageBase
    {
        public string EmailList { get; set; }
        public int LookBackDays { get; set; }


        public AutoWindowStickerReport(string emailList, int lookBackDays)
        {
            EmailList = emailList;
            LookBackDays = lookBackDays;
        }
    }
}
