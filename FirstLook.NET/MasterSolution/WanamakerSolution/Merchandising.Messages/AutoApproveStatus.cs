﻿namespace Merchandising.Messages
{
    public enum AutoApproveStatus 
    {
        Off = 0,
        OnForUsed = 1,
        OnForNew = 2,
        OnForAll = 3
    }
}