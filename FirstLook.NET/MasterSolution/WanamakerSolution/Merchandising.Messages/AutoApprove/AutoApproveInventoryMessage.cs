﻿using Core.Messages;
using MAX.Entities;

namespace Merchandising.Messages.AutoApprove
{
    /*
    * Used to auto generate advertisments for a subset of inventory for a particular dealer. This
    * is based on the WorkFlowType parameter. Usually called when AutoApprove is turned on for a dealer.
    */
    public class AutoApproveInventoryMessage : MessageBase
    {
        public AutoApproveInventoryMessage(int businessUnitId, AutoApproveStatus status, WorkflowType workflow)
        {
            BusinessUnitId = businessUnitId;
            Status = status;
            Workflow = workflow;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public AutoApproveStatus Status { get; set; }
        public WorkflowType Workflow { get; set; }
    }
}
