﻿using Core.Messages;

namespace Merchandising.Messages.AutoApprove
{
    /*
    * Creates a pdf for a car
    */
    public class CreatePdfMessage : MessageBase, IActiveInventoryMessage
    {
        public CreatePdfMessage(int businessUnitId, int inventoryId, string userName)
        {
            BusinessUnitId = businessUnitId;
            InventoryId = inventoryId;
            UserName = userName;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string UserName { get; set; }
    }
}
