﻿using Core.Messages;
using Core.Messaging;

namespace Merchandising.Messages.AutoApprove
{
    /*
    * Emails generated ads for a dealer.
    */
    public class DailyEmailMessage : MessageBase
    {
        public DailyEmailMessage(int businessUnitId, string email)
        {
            BusinessUnitId = businessUnitId;
            Email = email;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public string Email { get; set; }
    }
}
