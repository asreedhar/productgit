﻿using System.Collections.Generic;
using Core.Messages;

namespace Merchandising.Messages.AutoApprove
{
    public class BatchPriceChangeMessage : MessageBase
    {
        public int BusinessUnitId { get; set; }
        public IEnumerable<int> InventoryIds { get; set; }
        public string UserName { get; set; }

        public BatchPriceChangeMessage(int businessUnitId, IEnumerable<int> inventoryIds, string userName )
        {
            BusinessUnitId = businessUnitId;
            InventoryIds = inventoryIds;
            UserName = userName;
        }
    }
}
