﻿using Core.Messages;

namespace Merchandising.Messages.AutoApprove
{
    /*
    * Used to release an advertisment. This means to copy the ad to certain tables
    */
    public class ApproveMessage : MessageBase, IActiveInventoryMessage
    {
        public ApproveMessage(int businessUnitId, int inventoryId, string userName)
        {
            BusinessUnitId = businessUnitId;
            InventoryId = inventoryId;
            UserName = userName;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId{ get; set; }
        public int InventoryId { get; set; }
        public string UserName { get; set; }
    }
}
