﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;

namespace Merchandising.Messages.VehicleTransfer
{
    /// <summary>
    /// Message to handle transferring of photos of vehicle when transferred from one dealership to another (related) dealership
    /// </summary>
    public class PhotoTransferMessage : MessageBase
    {
        public int SrcInventoryId { get; set; }
        public int DstInventoryId { get; set; }
        
        public string Vin { get; set; }
        
        public int SrcBusinessUnitId { get; set; }
        public string SrcBusinessUnitCode { get; set; }
        
        public int DstBusinessUnitId { get; set; }
        public string DstBusinessUnitCode { get; set; }
        
        public PhotoTransferMessage(string vin, int srcBusinessUnitId, string srcBusinessUnitCode, int srcInventoryId, int dstBusinessUnitId, string dstBusinessUnitCode, int dstInventoryId)
        {
            Vin = vin;

            SrcBusinessUnitId = srcBusinessUnitId;
            SrcBusinessUnitCode = srcBusinessUnitCode;
            SrcInventoryId = srcInventoryId;

            DstBusinessUnitId = dstBusinessUnitId;
            DstBusinessUnitCode = dstBusinessUnitCode;
            DstInventoryId = dstInventoryId;
        }
        
        public override string ToString()
        {
            return String.Format(
                "PhotoTransferMessage vin={0} srcBusinessUnitId={1} srcBusinessCode={2} srcInventoryId={3} dstBusinessUnitId={4} dstBusinessCode={5} dstInventoryId={6}",
                Vin,
                SrcBusinessUnitId, SrcBusinessUnitCode, SrcInventoryId, 
                DstBusinessUnitId, DstBusinessUnitCode, DstInventoryId
                                                                        );
        }
    }
}
