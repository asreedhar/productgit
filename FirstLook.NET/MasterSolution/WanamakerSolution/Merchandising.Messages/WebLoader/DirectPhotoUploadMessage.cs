﻿using System;
using Core.Messages;

namespace Merchandising.Messages.WebLoader
{
    public class DirectPhotoUploadMessage : MessageBase
    {
        public DirectPhotoUploadMessage(string dealerCode, int vehicleId, Guid guid, string timestamp, 
            int position, string userName, Guid batchId, int batchCount, bool @lock)
        {
            Dealercode = dealerCode;
            VehicleId = vehicleId;
            Position = position;
            Timestamp = timestamp;
            Guid = guid;
            UserName = userName;
            Batch = batchId;
            BatchCount = batchCount;
            Lock = @lock;
        }

        public string Dealercode { get; set; }
        public int VehicleId { get; set; }
        public Guid Guid { get; set; }
        public string Timestamp { get; set; }
        public int Position { get; set; }
        public string UserName { get; set; }
        public Guid Batch { get; set; }
        public int BatchCount { get; set; }
        public bool Lock { get; set; }
    }
}
