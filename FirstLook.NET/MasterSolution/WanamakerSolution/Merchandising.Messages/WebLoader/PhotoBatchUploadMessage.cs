﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;

namespace Merchandising.Messages.WebLoader
{
    public class PhotoBatchUploadMessage : MessageBase
    {
        
        public PhotoBatchUploadMessage(Guid batch, PhotoUploadMessage[] photos)
        {
            Photos = photos;
            Batch = batch;
        }

        public Guid Batch { get; set; }
        public PhotoUploadMessage[] Photos { get; private set; }
    }
}
