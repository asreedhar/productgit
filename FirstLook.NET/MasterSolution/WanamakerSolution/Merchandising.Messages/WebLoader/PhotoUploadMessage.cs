﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;

namespace Merchandising.Messages.WebLoader
{
    public class PhotoUploadMessage :  MessageBase
    {
        public PhotoUploadMessage(string dealerCode, bool @lock, string vin, string stockNumber, int inventoryId, int position, string t, Guid guid, string s3FileName, string user, string firstName, string lastName)
        {
            Dealercode = dealerCode;
            Lock = @lock;

            InventoryId = inventoryId;
            Position = position;
            Vin = vin;
            StockNumber = stockNumber;
            T = t;
            Guid = guid;
            S3FileName = s3FileName;
            User = user;
            FirstName = firstName;
            LastName = lastName;
        }

        public string Dealercode { get; set; }
        public bool Lock { get; set; }
        public string Vin { get; set; }
        public string StockNumber { get; set; }
        public int InventoryId { get; set; }
        public int Position { get; set; }
        public string T { get; set; }
        public Guid Guid { get; set; }
        public string S3FileName { get; set; }
        public string User { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
