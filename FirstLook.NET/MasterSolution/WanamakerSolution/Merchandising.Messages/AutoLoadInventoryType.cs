﻿namespace Merchandising.Messages
{
    public enum AutoLoadInventoryType
    {
        NoStatus,
        Errors,
        AgentErrors, //queue all errors across business units for a particular agent
        SlurpeeRequeue,
        OpenRequeue
    }
}
