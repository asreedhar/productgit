﻿using Core.Messaging;

namespace Merchandising.Messages
{
    public interface ISimpleDbFactory
    {
        ISimpleDb GetAutoLoadAuditABTestDb();
        ISimpleDb GetDealerRaterDb();
        ISimpleDb GetGoogleAnalyticsReportDb();
    }
}
