﻿namespace Merchandising.Messages
{
    //Don't rename the names. Names used by Amazon S3.
    public enum Agent
    {
        Undefined = 0,
        DealerSpeed = 1,            //BMW
        GmGlobalConnect = 2,        //GM
        DealerConnect = 3,          //Chrysler
        VolkswagenHub = 4,          //Volkswagen
        MazdaDcs = 5,               //Mazda
        FordConnect = 6,            //Ford (old)
        ToyotaTechinfo = 7,         //Toyota (old)
        AccessAudi = 8,             //Audi
        HyundaiTechInfo = 9,        //Hyundai
        NatDealerDaily = 10,        // National Toyota/Lexus/Scion
        SetDealerDaily = 11,         // Southest Toyota
        NissanNorthAmerica = 12,    // Nissan
        HondaEconfig = 13
        

    }
}
