﻿using Core.Messages;
using System;

namespace Merchandising.Messages.TimeToMarket
{
    public class TimeToMarketGenerationMessage : MessageBase
    {
        public TimeToMarketGenerationMessage(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }

        public int BusinessUnitId { get; set; }
    }
}
