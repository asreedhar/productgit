﻿using Core.Messages;

namespace Merchandising.Messages.BucketAlerts
{
    /*
    * Emails bucket alerts
     * 
     * FB: 29468 -- added attributes for weekly send
    */
    public class BucketAlertMessage : MessageBase
    {
        private bool _sendWeekly;

        public BucketAlertMessage(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
            SendDaily = false;
            _sendWeekly = false;
            WriteToDb = true;
        }

        //Leave setters public. There is weird behavior with Json.NET serializer if left private in that
        //certain properties fail to serialize. Don't want to use Json.NET JsonProperty attribute
        public int BusinessUnitId { get; set; }
        public bool SendDaily { get; set; }
        public bool SendWeekly
        {
            get { return _sendWeekly; }
            set
            {
                _sendWeekly = value;

                if (_sendWeekly)
                    WriteToDb = false;
            }
        }
        public bool WriteToDb { get; set; }
    }
}
