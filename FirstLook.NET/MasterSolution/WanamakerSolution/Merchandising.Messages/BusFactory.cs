﻿using Core.Messages;
using Core.Messaging;
using Merchandising.Messages.InventoryDocuments;

namespace Merchandising.Messages
{
    public static class BusFactory
    {
        public static IBus<T, Q> CreatePriceChange<T, Q>() where T : PriceChangeMessage, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("Incisent_Core_PriceChange");
        }

        public static IBus<T, Q> CreateNewInventory<T, Q>() where T : NewInventoryMessage, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("Incisent_Core_NewInventory");
        }

        public static IBus<T, Q> CreateNewBookout<T, Q>() where T : NewBookoutMessage, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("Incisent_Core_NewBookout");
        }

        public static IBus<T, Q> CreateStyleSet<T, Q>() where T : StyleSetMessage, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("Incisent_Core_StyleSet");
        }

        public static IBus<T, Q> CreateDeadLetterBus<T, Q>() where T : DeadLetterMessage, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("Incisent_Core_DeadLetter");
        }

        public static IBus<T, Q> CreatePublishDocumentsBus<T, Q>() where T : DocumentPublishCommand, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("max-publish-documents", '-', true);
        }

        public static IBus<T, Q> CreateInventoryActivityBus<T, Q>() where T : InventoryActivityMessage, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("max-inventory-activity", '-', true);
        }

        public static IBus<T, Q> CreatePublishPhotoDocumentsBus<T, Q>() where T : MaxPhotoPublishMessage, Q
        {
            return AmazonServices.SNS.BusFactory<T, Q>.Create("max-publish-photo-documents", '-', true);
        }

    }
}
