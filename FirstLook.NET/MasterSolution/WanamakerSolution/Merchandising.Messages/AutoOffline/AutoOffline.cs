﻿using Core.Messages;

namespace Merchandising.Messages.AutoOffline
{
    public class ProcessAutoOfflineMessage : MessageBase
    {
        public ProcessAutoOfflineMessage(int? businessUnitId = null)
        {
            BusinessUnitId = businessUnitId;
        }

        public int? BusinessUnitId { get; set; }
    }
}