﻿using Autofac;
using Merchandising.Messages.QueueFactories;

namespace Merchandising.Messages
{
    public class MerchandisingMessagesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QueueFactory>().As<IQueueFactory>();
            builder.RegisterType<EmailQueueFactory>().As<IEmailQueueFactory>();
            builder.RegisterType<DocumentPublishingQueueFactory>().As<IDocumentPublishingQueueFactory>();
            builder.RegisterType<DiscountCampaignQueueFactory>().As<IDiscountCampaignQueueFactory>();

            builder.RegisterType<FileStoreFactory>().As<IFileStoreFactory>();

            base.Load(builder);
        }
    }
}
