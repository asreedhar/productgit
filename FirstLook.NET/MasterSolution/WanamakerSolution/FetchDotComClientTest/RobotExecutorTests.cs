﻿using System.IO;
using System.Reflection;
using System.Xml;
using NUnit.Framework;
using FetchDotComClient;

namespace FetchDotComClientTest
{
    [TestFixture]
    public class RobotExecutorTests
    {

        [Test]
        public void CanProcessXml_ActualVehicleXml()
        {
            // first test actual xml
            var xmlDocument = new XmlDocument();
            using ( var stream = GetStreamFromEmbeddedResource( "fetch_xsd_v6.xml" ) )
            {
                xmlDocument.Load(stream);
                var vehicleBuildData = FetchDotComAgent.GetVehicleBuildDataFromFetchXml(xmlDocument);
                Assert.IsNotNull(stream);
                Assert.IsNotNull(vehicleBuildData);
            }
        }

        [Test]
        public void CanProcessXml_VehicleXml_NoOptions()
        {
            // test a modified version - no options
            var xmlDocument = new XmlDocument();
            using ( var stream = GetStreamFromEmbeddedResource( "fetch_xsd_v6_EmptyOptions.xml" ) )
            {
                xmlDocument.Load(stream);
                var vehicleBuildData = FetchDotComAgent.GetVehicleBuildDataFromFetchXml(xmlDocument);
                Assert.IsNotNull(vehicleBuildData);
            }
        }

        [Test]
        public void CanProcessXml_VehicleXml_Error101()
        {
            // now test a modified version - error 101
            // this time our output should be null
            var xmlDocument = new XmlDocument();
            using ( var stream = GetStreamFromEmbeddedResource( "fetch_xsd_v6_Error101.xml" ) )
            {
                xmlDocument.Load(stream);
                var vehicleBuildData = FetchDotComAgent.GetVehicleBuildDataFromFetchXml(xmlDocument);
                Assert.IsNull(vehicleBuildData.VehicleBuildData);
            }
        }

        [Test]
        public void CanProcessXml_VehicleXml_Error102()
        {
            // Test for new error status 102: Insufficient Privileges.
            var xmlDocument = new XmlDocument();
            using (var stream = GetStreamFromEmbeddedResource("fetch_xsd_v6_Error102.xml"))
            {
                xmlDocument.Load(stream);
                var vehicleBuildData = FetchDotComAgent.GetVehicleBuildDataFromFetchXml(xmlDocument);
                Assert.IsNull(vehicleBuildData.VehicleBuildData);
            }
        }

        [Test]
        public void CanProcessXml_VehicleXml_OnlyRequiredVehicleElements()
        {
            // now test a modified version - only required vehicle elements
            var xmlDocument = new XmlDocument();

            using ( var stream = GetStreamFromEmbeddedResource( "fetch_xsd_v6_OnlyRequiredVehicleElements.xml" ) )
            {
                xmlDocument.Load(stream);
                var vehicleBuildData = FetchDotComAgent.GetVehicleBuildDataFromFetchXml(xmlDocument);
                Assert.IsNotNull(vehicleBuildData);
            }
        }

        private static Stream GetStreamFromEmbeddedResource(string resourceName)
        {
            const string schemaName = @"FetchDotComClientTest.SampleXml.";
            var fullResourceName = schemaName + resourceName;
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(fullResourceName);
        }
    }
}
