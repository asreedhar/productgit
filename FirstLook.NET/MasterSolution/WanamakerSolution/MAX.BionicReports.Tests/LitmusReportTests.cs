﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using Core.Messaging;
using MAX.BionicReports.Entities;
using MAX.BionicReports.Utility;
using Moq;
using NUnit.Framework;


namespace MAX.BionicReports.Tests
{
    [TestFixture]
    public class LitmusReportTests
    {

        private Mock<IFileStorage> _mockCreationFileStore;
        private Mock<IFileStorage> _mockTemplateFileStore;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _mockCreationFileStore = new Mock<IFileStorage>();
            _mockCreationFileStore.Setup(x => x.Read(It.IsAny<string>())).Returns(new MemoryStream());
            _mockTemplateFileStore = new Mock<IFileStorage>();
            _mockTemplateFileStore.Setup(x => x.Read(It.IsAny<string>())).Returns(ReadEmailTemplateFile());
        }

        [Test]
        public void TestXmlDeserialization()
        {
            var xmlStream = LitmusReportFileStream();

            var reports = LitmusCurrentMonthCampaign.DeserializeXmlStreamToObject<LitmusReports>(xmlStream); ;

            Assert.NotNull(reports);
            Assert.True(reports.Entries.Length == 3);
        }

        [Test]
        public void TestCreateReportDeserialization()
        {
            var xmlStream = LitmusCreateReportFileStream();
            var report = LitmusCurrentMonthCampaign.DeserializeXmlStreamToObject<LitmusReport>(xmlStream);

            Assert.NotNull(report);
            Assert.True("API Test".Equals(report.Name));
        }

        [Test]
        public void TestDeserializeEmptyReports()
        {
            var xmlStream =
                new MemoryStream(
                    Encoding.UTF8.GetBytes(
                        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><reports type=\"array\"></reports>"));
            xmlStream.Position = 0;

            var reports = LitmusCurrentMonthCampaign.DeserializeXmlStreamToObject<LitmusReports>(xmlStream);

            Assert.NotNull(reports);
            Assert.True(reports.Entries == null);
        }

        [Test]
        public void TestEmptyReponseDeserialization()
        {
            var xmlStream = new MemoryStream(Encoding.UTF8.GetBytes(""));
            xmlStream.Position = 0;
            LitmusReports reports = null;
            try
            {
                reports = LitmusCurrentMonthCampaign.DeserializeXmlStreamToObject<LitmusReports>(xmlStream);
            }
            catch (InvalidOperationException xmlEx)
            {
                xmlStream.Position = 0;
                Debug.WriteLine(String.Format("Response from litmus' report listing API is returning invalid XML. ({0}) : {1}", xmlEx, new StreamReader(xmlStream).ReadToEnd()));
            }

            Assert.IsNull(reports, "Should not set reports if there is an error in the XML.");
        }

        [Test]
        public void TestValidXMLEmptyReponseDeserialization()
        {
            var xmlStream = new MemoryStream(Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"UTF-8\"?><somethingelse></somethingelse>"));
            xmlStream.Position = 0;
            LitmusReports reports = null;
            try
            {
                reports = LitmusCurrentMonthCampaign.DeserializeXmlStreamToObject<LitmusReports>(xmlStream);
            }
            catch (InvalidOperationException xmlEx)
            {
                xmlStream.Position = 0;
                Debug.WriteLine(String.Format("Response from litmus' report listing API is returning unexpected response. ({0}) : {1}", xmlEx, new StreamReader(xmlStream).ReadToEnd()));
            }

            Assert.IsNull(reports, "Should not set reports if there is an error in the XML.");
        }

        private Stream LitmusReportFileStream()
        {
            return GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.LitmusReports.xml");
        }

        private Stream LitmusCreateReportFileStream()
        {
            return GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.LitmusCreateReportReponse.xml");
        }

        private Stream ReadEmailTemplateFile()
        {
            return GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.mockEmailTemplate.html");
        }
    }
}
