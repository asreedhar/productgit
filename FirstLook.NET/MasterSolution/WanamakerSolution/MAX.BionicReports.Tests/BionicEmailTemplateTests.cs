﻿using System;
using System.IO;
using Core.Messaging;
using MAX.BionicReports.Services;
using MAX.BionicReports.Utility;
using Moq;
using NUnit.Framework;

namespace MAX.BionicReports.Tests
{
    [TestFixture]
    class BionicEmailTemplateTests
    {
        private Mock<IFileStorage> _mockFileStore;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _mockFileStore = new Mock<IFileStorage>();
            _mockFileStore.Setup(x => x.Read(It.IsAny<string>())).Returns(ReadEmailTemplateFile());
        }

        private Stream ReadEmailTemplateFile()
        {
            return GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.mockEmailTemplate.html");
        }

    }
}
