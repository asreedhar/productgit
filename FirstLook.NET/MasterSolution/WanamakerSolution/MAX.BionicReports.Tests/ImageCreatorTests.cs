﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;
using FirstLook.Common.Core.Extensions;
using MAX.BionicReports.Images;
using MAX.Entities;
using MAX.Entities.Reports.Bionic;
using NUnit.Framework;

namespace MAX.BionicReports.Tests
{
	[TestFixture]
	public class ImageCreatorTests
	{
		private const string NoPhotosChartPng =    "..\\NoPhotosChart.png";
		private const string NoPriceChartPng =     "..\\NoPriceChart.png";
		private const string NotOnlineChartPng =   "..\\NotOnlineChart.png";
		private const string TtmCompleteChartPng = "..\\TtmCompleteChart.png";
		private const string TtmPhotosChartPng =   "..\\TtmPhotosChart.png";

		#region bucketcharts

		[Test]
		public void NoPhotosChart()
		{
			var data = GetReportData();
			if (data is EmptyBionicReportData)
				throw new ApplicationException("Data file not found");

			var imgCreator = new ImageCreator();
		    var seriesData =
		        CreateFiveDayBucketValueCollection(data.BucketValues.Where(bv => bv.Bucket == WorkflowType.NoPhotos));
			using (var npstream = imgCreator.CreateBucketChartImage(seriesData, VehicleType.Used))
			{
				using(var fs = new FileStream(NoPhotosChartPng, FileMode.Create))
					npstream.WriteTo(fs);
			}

			Assert.IsTrue(File.Exists(NoPhotosChartPng));
		}

		[Test]
		public void NoPriceChart()
		{
			var data = GetReportData();
			if (data is EmptyBionicReportData)
				throw new ApplicationException("Data file not found");

			var imgCreator = new ImageCreator();
		    var seriesData =
		        CreateFiveDayBucketValueCollection(data.BucketValues.Where(bv => bv.Bucket == WorkflowType.NoPrice));
			using (var npstream = imgCreator.CreateBucketChartImage(seriesData, VehicleType.Used))
			{
				using (var fs = new FileStream(NoPriceChartPng, FileMode.Create))
					npstream.WriteTo(fs);
			}

			Assert.IsTrue(File.Exists(NoPriceChartPng));
		}

		[Test]
		public void NotOnlineChart()
		{
			var data = GetReportData();
			if (data is EmptyBionicReportData)
				throw new ApplicationException("Data file not found");

			var imgCreator = new ImageCreator();
		    var seriesData =
		        CreateFiveDayBucketValueCollection(data.BucketValues.Where(bv => bv.Bucket == WorkflowType.NoPrice));
			using (var npstream = imgCreator.CreateBucketChartImage(seriesData, VehicleType.Used))
			{
				using (var fs = new FileStream(NotOnlineChartPng, FileMode.Create))
					npstream.WriteTo(fs);
			}

			Assert.IsTrue(File.Exists(NotOnlineChartPng));
		}

		#endregion bucketcharts


		#region ttmcharts

		[Test]
		public void TimeToMarketCompleteChart()
		{
			var data = GetTimeToMarketReportData();
			if (data is EmptyBionicReportData)
				throw new ApplicationException("Data file not found");

			var chartLines = new List<Tuple<ChartDashStyle, double, string>>
						{
							new Tuple<ChartDashStyle, double, string>(ChartDashStyle.Solid, 4, "AVG"),
							new Tuple<ChartDashStyle, double, string>(ChartDashStyle.Dash, 3, "GOAL")
						};

			var imgCreator = new ImageCreator();
		    var seriesData = CreateFiveDayTtmValueCollection(data
		        .TimeToMarketReport
		        .Days
		        .Select(day => new KeyValuePair<DateTime, int>(day.Date, day.AvgElapsedDaysAdComplete)));
            using (var npstream = imgCreator.CreateTtmChartImage(seriesData, ReportImageType.TimeToMarketComplete, chartLines))
			{
				using (var fs = new FileStream(TtmCompleteChartPng, FileMode.Create))
					npstream.WriteTo(fs);
			}

			Assert.IsTrue(File.Exists(TtmCompleteChartPng));
		}

		[Test]
		public void TimeToMarketPhotosChart()
		{
			var data = GetTimeToMarketReportData();
			if (data is EmptyBionicReportData)
				throw new ApplicationException("Data file not found");

			var imgCreator = new ImageCreator();
		    var seriesData = CreateFiveDayTtmValueCollection(data
		        .TimeToMarketReport
		        .Days
		        .Select(day => new KeyValuePair<DateTime, int>(day.Date, day.AvgElapsedDaysPhoto)));
			using (var npstream = imgCreator.CreateTtmChartImage(seriesData, ReportImageType.TimeToMarketFirstPhoto, null))
			{
				using (var fs = new FileStream(TtmPhotosChartPng, FileMode.Create))
					npstream.WriteTo(fs);
			}

			Assert.IsTrue(File.Exists(TtmPhotosChartPng));
		}

		#endregion bucketcharts


		#region privates
		private BionicReportData GetReportData()
		{
			using (var stream = GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.AllStarChevy.json"))
			{
				if (stream == null)
				{
					return new EmptyBionicReportData();
				}
				using (TextReader textReader = new StreamReader(stream))
				{
					var jsonString = textReader.ReadToEnd();
					return jsonString.FromJson<BionicReportData>();
				}
			}
		}

		private BionicReportData GetTimeToMarketReportData()
		{
			using (var stream = GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.AllStarChevy.json"))
			{
				if (stream == null)
				{
					return new EmptyBionicReportData();
				}
				using (TextReader textReader = new StreamReader(stream))
				{
					var jsonString = textReader.ReadToEnd();
					return jsonString.FromJson<BionicReportData>();
				}
			}
		}
		#endregion privates

		[TestFixtureSetUp]
		public void SetUpFixture()
		{
			var photosFiles = new[] { NoPhotosChartPng, NoPriceChartPng, NotOnlineChartPng, TtmPhotosChartPng, TtmCompleteChartPng };
			foreach (var s in photosFiles)
			{
				File.Delete(s);
			}
		}

		[TestFixtureTearDown]
		public void TearDownFixture()
		{
			var photosFiles = new[] {NoPhotosChartPng, NoPriceChartPng, NotOnlineChartPng, TtmPhotosChartPng, TtmCompleteChartPng};
			foreach (var s in photosFiles )
			{
				//File.Delete(s);
			}
		}


		internal IEnumerable<KeyValuePair<DateTime, int?>> CreateFiveDayTtmValueCollection(IEnumerable<KeyValuePair<DateTime, int>> enumerableDates)
		{
			var valuesByWeek = new List<KeyValuePair<DateTime, int?>>();
			var keyValuePairs = enumerableDates as KeyValuePair<DateTime, int>[] ?? enumerableDates.ToArray();
			if (!keyValuePairs.Any())
				return valuesByWeek;

            keyValuePairs = keyValuePairs.OrderBy(kvp => kvp.Key).ToArray();

			var kvpDates = keyValuePairs.First();
			var daysInMonth = DateTime.DaysInMonth(kvpDates.Key.Year, kvpDates.Key.Month);
			var sections = daysInMonth / 5;
			var extraDaysInLastSection = daysInMonth % 5;

			for (var i = 0; i < sections; i++)
			{
				var startDay = i * 5;
				var endDay = startDay + 5;

				if (i == sections - 1)
				{
					endDay += extraDaysInLastSection;
				}

				var kvpValSectionEnum = keyValuePairs.Where(kv => kv.Key.Day > startDay && kv.Key.Day <= endDay);
				var valSectionEnum = kvpValSectionEnum as KeyValuePair<DateTime, int>[] ?? kvpValSectionEnum.ToArray();
                
                valuesByWeek.Add(!valSectionEnum.Any()
                ? new KeyValuePair<DateTime, int?>(new DateTime(kvpDates.Key.Year, kvpDates.Key.Month, endDay), null)
                : new KeyValuePair<DateTime, int?>(new DateTime(kvpDates.Key.Year, kvpDates.Key.Month, endDay), (int)valSectionEnum.Average(kvp => kvp.Value)));
                
			}
			return valuesByWeek;
		}

		internal IEnumerable<BucketValue> CreateFiveDayBucketValueCollection(IEnumerable<BucketValue> bucketValuesEnum)
		{
			var valuesByWeek = new List<BucketValue>();

			var bucketValues = bucketValuesEnum.ToArray();
			bucketValues.OrderBy(bv => bv.Date);
			var bvFirst = bucketValues.First();
			if (bvFirst == null)
				return valuesByWeek;

			var daysInMonth = DateTime.DaysInMonth(bvFirst.Date.Year, bvFirst.Date.Month);
			var sections = daysInMonth / 5;
			var extraDaysInLastSection = daysInMonth % 5;

			for (var i = 0; i < sections; i++)
			{
				var startDay = i * 5;
				var endDay = startDay + 5;

				if (i == sections - 1)
				{
					endDay += extraDaysInLastSection;
				}

				var bucketValueSectionEnum = bucketValues.Where(bv => bv.Date.Day > startDay && bv.Date.Day <= endDay);
				var valueSectionEnum = bucketValueSectionEnum as BucketValue[] ?? bucketValueSectionEnum.ToArray();

				if (!valueSectionEnum.Any())
					continue;

				var singleBucketVal = valueSectionEnum.Last().Clone();
				singleBucketVal.Date = new DateTime(singleBucketVal.Date.Year, singleBucketVal.Date.Month, endDay);
				singleBucketVal.NewCount = (int)valueSectionEnum.Average(bv => bv.NewCount);
				singleBucketVal.UsedCount = (int)valueSectionEnum.Average(bv => bv.UsedCount);

				valuesByWeek.Add(singleBucketVal);
			}

			return valuesByWeek;
		}
	}
}