﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Core.Messaging;
using MAX.BionicReports.Utility;
using Moq;
using NUnit.Framework;

namespace MAX.BionicReports.Tests
{
    [TestFixture]
    class GroupBionicEmailTemplateTests
    {

        private Mock<IFileStorage> _mockFileStore;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _mockFileStore = new Mock<IFileStorage>();
            _mockFileStore.Setup(x => x.Read(It.IsAny<string>())).Returns(ReadEmailTemplateFile());
        }

        [Test]
        public void TemplateTransformerCreatesMultipleBlocks()
        {
            var reader = new StreamReader(ReadEmailTemplateFile());
            var html = reader.ReadToEnd();
            //var result = EmailTemplateTransform.ApplyGroupBionic(html, "<!--{emailAddress}-->");

        }

        private Stream ReadEmailTemplateFile()
        {
            return GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.mockGroupEmailTemplate.html");
        }
    }
}
