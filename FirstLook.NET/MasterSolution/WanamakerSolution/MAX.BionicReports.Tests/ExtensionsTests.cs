﻿using System;
using System.Collections.Generic;
using System.Linq;
using MAX.BionicReports.Utility;
using NUnit.Framework;

namespace MAX.BionicReports.Tests
{
    [TestFixture]
    public class ExtensionsTests
    {
        [Test]
        public void TestLerpExExtension()
        {
            var data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            var lerpex = data.InterpolateAndExtend();
            Assert.AreEqual(lerpex.Count, 1, "The output list size should be the same as the input");
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            // ReSharper is confused
            Assert.AreEqual(lerpex.First().Value, null, "The original list should be returned if the original list only contained null values");

            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(3), 1));
            lerpex = data.InterpolateAndExtend();
            Assert.AreEqual(lerpex.First().Value, 1, "Null values on the start of a data set should be replaced with their following neighbor's value via extending");

            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(4), null));
            lerpex = data.InterpolateAndExtend();
            Assert.AreEqual(lerpex.Last().Value, 1, "Null values on the end of a data set should be replaced with their preceeding neighbor's value via extending");

            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(5), null));
            lerpex = data.InterpolateAndExtend();
            Assert.AreEqual(lerpex.Last().Value, 1, "Extending should cover multiple null values at the end of the data set.");

            data.Insert(0, new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            lerpex = data.InterpolateAndExtend();
            Assert.AreEqual(lerpex.First().Value, 1, "Extending should cover multiple null values at the start of the data set.");

            // 4 null null 1 null null
            data.Insert(0, new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 4));
            lerpex = data.InterpolateAndExtend();
            Assert.AreEqual(lerpex.First().Value, 4, "Should not be interpolated or extended and should be it's original value.");

            Assert.AreEqual(lerpex[1].Value, 3, "Should be properly interpolated.");
            Assert.AreEqual(lerpex[2].Value, 2, "Should be properly interpolated.");
        }

        [Test]
        public void TestOrphansExtension()
        {
            var data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            Assert.True(data.Orphans().Count == 0, "There should be no null orphans");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            Assert.True(data.Orphans().Count == 1, "There should be lone non-null orphans");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            Assert.True(data.Orphans().Count == 1, "There should be left bounded orphans followed by null points");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            Assert.True(data.Orphans().Count == 1, "There should be right bounded orphans preceded by null points");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            Assert.True(data.Orphans().Count == 0, "There should be no orphans when only bounded");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), 1));
            Assert.True(data.Orphans().Count == 0, "There should be no orphans when there are no nulls and more than one point");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            Assert.True(data.Orphans().Count == 1, "There should be orphans bounded by only null points");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), 1));
            Assert.True(data.Orphans().Count == 2, "There should be two orphans if bounded and separated by a null");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(3), null));
            Assert.True(data.Orphans().Count == 0, "There should not be orphans which are next to their twin siblings");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(3), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(4), null));
            Assert.True(data.Orphans().Count == 2, "There should be two orphans if unbounded but surrounded by nulls");


        }

        [Test]
        public void TestContiguousSegmentExtension()
        {
            var data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            var segments = data.ContiguousSegments();
            Assert.True(segments.Count == 0, "Null lone point should produce no segments");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 0, "A lone non null data point should produce no segments");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 0, "Null data should produce no segments");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 1, "Two or more data points absent of nulls should produce one segment");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 0, "There should be no segments when an orphaned point is bounded by nulls");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 0, "There should be no segments on a left bounded orphaned point followed by null data");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 0, "There should be no segments on a right bounded orphaned point preceeded by null data");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 1, "There should only be one segment with a null trailing value");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 0, "There should be zero segments with only orphaned points");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 1, "There should only be one segment with null preceeding values");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(3), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(4), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 2, "There should be two segments when divided by a null value");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(3), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 1, "There should only be one segment when a left bounded orphan point followed by a null is followed by a segment");

            data = new List<KeyValuePair<DateTime, int?>>();
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(0), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(1), 1));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(2), null));
            data.Add(new KeyValuePair<DateTime, int?>(DateTime.FromBinary(3), 1));
            segments = data.ContiguousSegments();
            Assert.True(segments.Count == 1, "There should only be one segment when a right bounded orphan point preceeded by a null is preceeded by a segment");
        }
    }
}
