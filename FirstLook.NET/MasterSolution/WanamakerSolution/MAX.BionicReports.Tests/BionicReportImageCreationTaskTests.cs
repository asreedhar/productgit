﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using MAX.BionicReports.Images;
using MAX.BionicReports.Queues;
using MAX.BionicReports.Tasks;
using MAX.BionicReports.Topics;
using MAX.Entities;
using MAX.Entities.Messages;
using MAX.Entities.Reports.Bionic;
using Merchandising.Messages.BionicReport;
using Moq;
using NUnit.Framework;

namespace MAX.BionicReports.Tests
{
	[TestFixture]
	public class BionicReportImageCreationTaskTests
	{
		private Mock<IBionicReportQueueFactory> _mockQueueFactory;
		private Mock<IBionicReportsBusFactory> _mockBusFactory;
		private TestableBionicReportImageCreationTask _reportTask;
		private Mock<IBus<BionicReportImagesCompleteMessage, BionicReportImagesCompleteMessage>> _mockBus;
		private Mock<IFileStorage> _mockFileStore;
		private Mock<IImageCreator> _mockImageCreator;

		[TestFixtureSetUp]
		public void SetupFixture()
		{
			_mockQueueFactory = new Mock<IBionicReportQueueFactory>();
			_mockBusFactory = new Mock<IBionicReportsBusFactory>();

			_mockBus = new Mock<IBus<BionicReportImagesCompleteMessage, BionicReportImagesCompleteMessage>>();
			_mockBusFactory.Setup(x => 
				x.CreateBionicImagesCompleteBus<BionicReportImagesCompleteMessage, BionicReportImagesCompleteMessage>())
				.Returns(_mockBus.Object);

			_mockFileStore = new Mock<IFileStorage>();
			_mockFileStore.Setup(x => x.Read(It.IsAny<string>())).Returns(ReadBucketJsonFile());

			_mockImageCreator = new Mock<IImageCreator>();

			_reportTask = new TestableBionicReportImageCreationTask(_mockQueueFactory.Object, _mockBusFactory.Object, _mockFileStore.Object, new ImageCreator());
		}

		private Stream ReadBucketJsonFile()
		{
			return GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.HendrickMini.json");
		}

		private Stream ReadTimeToMarketJsonFile()
		{
			return GetType().Assembly.GetManifestResourceStream("MAX.BionicReports.Tests.TestData.Hendrick_BMW_June_2014_TTM.json");
		}

		[Test]
		public void Process_Sends_CompleteMessage()
		{
			BionicReportData reportData;
			using (var stream = ReadBucketJsonFile())
			{
				if (stream == null)
					return;

				reportData = ReadReportData(stream);
			}

			var tstMsg = new BionicReportImageCreationMessage(reportData.BusinessUnitId, "test@test.com", "filestring", "MockTrackingCode");
			_reportTask.Process(tstMsg);
			_mockBus.Verify(x => x.Publish(It.IsAny<BionicReportImagesCompleteMessage>(), It.IsAny<string>()), Times.Once());
		}

		[Test]
		public void CreateFiveDayBucketValueCollection_Returns_Summed_Dates()
		{
			using (var stream = ReadBucketJsonFile())
			{
				if (stream == null)
					return;

				var reportData = ReadReportData(stream);
				var results = _reportTask.CreateFiveDayBucketValueCollection(reportData.BucketValues.Where(bv => bv.Bucket == WorkflowType.NoPhotos));
			}
		}

		[Test]
		public void CreateFiveDayTtmValueCollection_Returns_Summed_Dates()
		{
			using (var stream = ReadBucketJsonFile())
			{
				if (stream == null)
					return;

				var reportData = ReadReportData(stream);
				var results = _reportTask.CreateFiveDayTtmValueCollection(reportData
							.TimeToMarketReport
							.Days
							.Select(day => new KeyValuePair<DateTime, int>(day.Date, day.AvgElapsedDaysAdComplete)));
			}
		}

		private BionicReportData ReadReportData(Stream dataStream)
		{
			using (TextReader textReader = new StreamReader(dataStream))
			{
				var jsonString = textReader.ReadToEnd();
				return jsonString.FromJson<BionicReportData>();
			}
		}
	}

	class TestableBionicReportImageCreationTask : BionicReportImageCreationTask
	{
		internal TestableBionicReportImageCreationTask(IBionicReportQueueFactory bionicReportQueueFactory, IBionicReportsBusFactory bionicReportsBusFactory, IFileStorage fileStorage, IImageCreator imageCreator)
			: base(bionicReportQueueFactory, bionicReportsBusFactory, fileStorage, imageCreator)
		{
		}

		internal override string FileName(int businessUnitId, ReportImageType imageType)
		{
			return "testfilename";
		}
	}
}
