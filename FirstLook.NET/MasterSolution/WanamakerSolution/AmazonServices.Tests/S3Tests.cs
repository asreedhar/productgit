﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using AmazonServices.S3;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class S3Tests
    {
        [Test]
        public void TestReadWrite()
        {
            var store = new FileStore("S3UnitTest");

            var fileName = "TestS3-123" + ".txt";

            var payload = string.Format("This is an S3 test. Performed on {0}",
                                       DateTimeOffset.Now.ToString("G"));
            var bytes = Encoding.ASCII.GetBytes(payload);
            var stream = new MemoryStream(bytes);

            store.Write(fileName, stream);

            Thread.Sleep(5000);

            using (var returnStream = store.Read(fileName))
            using (var reader = new StreamReader(returnStream, Encoding.ASCII))
            {
                var returnedPayload = reader.ReadToEnd();

                Assert.That(returnedPayload, Is.EqualTo(payload));
            }

            store.Delete(fileName);
            bool exists;
            store.Exists(fileName, out exists);

            Assert.IsFalse(exists);

        }

        [Test]
        public void TestAsyncWrite()
        {
            const string fileName = "TestS3-123" + ".txt";

            var store = new FileStore("S3UnitTest");
            var payload = string.Format("This is an S3 test. Performed on {0}",
                                       DateTimeOffset.Now.ToString("G"));
            var bytes = Encoding.ASCII.GetBytes(payload);
            var stream = new MemoryStream(bytes);

            var asyncResult = store.BeginWrite(fileName, stream, null);

            while (!asyncResult.IsCompleted)
                Thread.Sleep(1000);

            var statusCode = store.EndWrite(asyncResult);
            Assert.AreEqual(statusCode, HttpStatusCode.OK);

            using (var returnStream = store.Read(fileName))
            using (var reader = new StreamReader(returnStream, Encoding.ASCII))
            {
                var returnedPayload = reader.ReadToEnd();

                Assert.That(returnedPayload, Is.EqualTo(payload));
            }

            store.Delete(fileName);
            bool exists;
            store.Exists(fileName, out exists);

            Assert.IsFalse(exists);

        }

        [Test]
        public void Test_Write_on_ImageStore_bucket_should_allow_anonymous_access()
        {
            var store = new ImageStore("S3UnitTest");

            const string fileName = "TestS3-Img.txt";

            var payload = string.Format("This is another test. Performed on {0:G}", DateTimeOffset.Now);

            var bytes = Encoding.ASCII.GetBytes(payload);
            var stream = new MemoryStream(bytes);

            store.Write(fileName, stream);

            try
            {
                var publicUrl = "http://s3.amazonaws.com/" + store.BucketName + "/" + fileName;

                var stop = DateTimeOffset.UtcNow.AddSeconds(10);
                Exception ex = null;
                for (;;)
                {
                    if (DateTimeOffset.UtcNow > stop)
                        Assert.Fail("Failed to retrieve correct file within the time alloted." +
                                    (ex != null ? " Last failure: " + ex.Message : ""));

                    try
                    {
                        var result = new WebClient().DownloadString(publicUrl);
                        if (result == payload)
                            return; // Success
                    }
                    catch (Exception e)
                    {
                        ex = e;
                    }

                    Thread.Sleep(100);
                }
            }
            finally
            {
                store.Delete(fileName);
            }
        }

        [Test]
        public void Test_Write_on_FileStore_bucket_should_not_allow_anonymous_access()
        {
            var store = new FileStore("S3UnitTest");

            const string fileName = "TestS3-File.txt";

            var payload = string.Format("This is another test. Performed on {0:G}", DateTimeOffset.Now);

            var bytes = Encoding.ASCII.GetBytes(payload);
            var stream = new MemoryStream(bytes);

            store.Write(fileName, stream);

            try
            {
                var publicUrl = "http://s3.amazonaws.com/" + store.BucketName + "/" + fileName;

                var stop = DateTimeOffset.UtcNow.AddSeconds(10);
                Exception ex = null;
                for (; ; )
                {
                    if (DateTimeOffset.UtcNow > stop)
                        Assert.Fail("Failed to retrieve correct file within the time alloted." +
                                    (ex != null ? " Last failure: " + ex.Message : ""));

                    try
                    {
                        new WebClient().DownloadString(publicUrl);
                        Assert.Fail("Should not have succeeded in anonymously reading a protected bucket object.");
                    }
                    catch (Exception e)
                    {
                        ex = e;
                        var webException = e as WebException;
                        if(webException != null)
                        {
                            var response = (HttpWebResponse) webException.Response;
                            if (response.StatusCode == HttpStatusCode.Forbidden)
                                return; // Successfully determined that resource access was denied.
                        }
                    }

                    Thread.Sleep(100);
                }
            }
            finally
            {
                store.Delete(fileName);
            }
        }

        [Test]
        public void Write_on_ImageStore_should_create_file_with_CacheControl_Public_and_MaxAge()
        {
            var store = new ImageStore("S3UnitTest");

            const string fileName = "TestS3-Img2.txt";

            var payload = string.Format("This is another test. Performed on {0:G}", DateTimeOffset.Now);

            var bytes = Encoding.ASCII.GetBytes(payload);
            var stream = new MemoryStream(bytes);

            store.Write(fileName, stream);

            try
            {
                var publicUrl = "http://s3.amazonaws.com/" + store.BucketName + "/" + fileName;

                var stop = DateTimeOffset.UtcNow.AddSeconds(10);
                Exception ex = null;
                for (; ; )
                {
                    if (DateTimeOffset.UtcNow > stop)
                        throw new Exception("Failed to retrieve correct cache-control header within the time alloted.", ex);
                    ex = null;
                    try
                    {
                        var webClient = new WebClient();
                        var result = webClient.DownloadString(publicUrl);
                        if (result == payload && 
                            Regex.IsMatch(webClient.ResponseHeaders[HttpResponseHeader.CacheControl] ?? "",
                                "public\\s*,\\s*max-age=\\d+", RegexOptions.IgnoreCase))
                            return; // Success
                    }
                    catch (Exception e)
                    {
                        ex = e;
                    }

                    Thread.Sleep(100);
                }
            }
            finally
            {
                store.Delete(fileName);
            }
        }

        [Test]
        public void TestExists()
        {
            var store = new FileStore("S3UnitTest");
            bool exists;
            store.Exists("test/nothing", out exists);

            Assert.IsFalse(exists);

        }

        [Test]
        public void TestExistsNoStream()
        {
            var store = new FileStore("S3UnitTest");
            bool exists = store.ExistsNoStream("test/nothing");

            Assert.IsFalse(exists);
        }

        [Test]
        public void Test_Exists_Modified_Since()
        {
            var testFileName = "test/existsSince";
            var testStartTime = DateTime.Now;
            var store = new FileStore("S3UnitTest");
            var payload = string.Format("This is a test for the exists/modified_since method. Performed on {0:G}", DateTimeOffset.Now);

            try
            {
                bool exists = false;
                store.Exists(testFileName, out exists, testStartTime);

                Assert.That(exists, Is.False);

                var bytes = Encoding.ASCII.GetBytes(payload);
                var stream = new MemoryStream(bytes);
                
                store.Write(testFileName, stream);

                // Allow some time to pass before checking for the file.
                Thread.Sleep(1000);

                // Amazon's ModifiedSince only checks the Date, not the time.
                exists = false;
                store.Exists(testFileName, out exists, DateTime.Now);
                Assert.That(exists, Is.True);

            }
            finally
            {
                store.Delete(testFileName);
            }
        }
    }
}
