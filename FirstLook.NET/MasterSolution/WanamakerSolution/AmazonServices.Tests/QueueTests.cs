﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Amazon.SQS.Model;
using AmazonServices.SNS;
using AmazonServices.SQS;
using Core.Messages;
using Core.Messaging;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class QueueTests
    {
        private AmazonQueue<TestMessage> _queue;

        [TestFixtureSetUp]
        public void Setup()
        {
            // Create the queue
            Console.WriteLine("Creating Queue.");
            _queue = new AmazonQueue<TestMessage>("Tests_Queue")
            {
                DeadLetterBus = BusFactory<DeadLetterMessage, DeadLetterMessage>.Create("Tests_Bus_DeadLetter")
            };   

            //remove messages
            IMessage < TestMessage > message;
            while( (message = _queue.GetMessage()) != null)
            {
                _queue.DeleteMessage(message);
                Thread.Sleep(1000);
            }

        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            // Wait 61 seconds
            Console.WriteLine("Waiting 61 seconds.");
            Thread.Sleep(61 * 1000);
        }

        [Test]
        public void TestDeadLetter()
        {
            var message = new TestMessage(3.1416.ToString());
	        var errors = new List<String>();
			
			// Since we updated the dead letter error limit, use that new limit for this test.
	        while (errors.Count < 72)
	        {
		        errors.Add(string.Format("Error {0}", errors.Count));
	        }
	        message.Errors = errors.ToArray();

            _queue.SendMessage(message, "");
            Thread.Sleep(20000);
            var returnedMessage = _queue.GetMessage();

            Assert.IsTrue(returnedMessage == null);
        }

        [Test]
        public void TestErrorsOneUnderDeadLetterSent()
        {
            var random = new Random().NextDouble().ToString();
            var message = new TestMessage(random);
            message.Errors = new string[]
            {
                "error 1",
                "error 2",
                "error 3",
                "error 4",
            };

            _queue.SendMessage(message, GetType().FullName);
            Thread.Sleep(20000);
            var returnedMessage = _queue.GetMessage();

            Assert.IsTrue(returnedMessage != null);
            Assert.IsTrue(returnedMessage.Object.Errors.Length == 4);
            Assert.IsTrue(returnedMessage.Object.TestValue == random);

            _queue.DeleteMessage(returnedMessage);
        }

        [Test]
        public void MultipleRead()
        {
            var random = new Random().NextDouble().ToString();
            var message = new TestMessage(random);

            _queue.SendMessage(message, GetType().FullName);
            _queue.SendMessage(message, GetType().FullName);
            _queue.SendMessage(message, GetType().FullName);
            Thread.Sleep(20000);

            var messages =_queue.GetMessages(3);
            Assert.IsTrue(messages.Count() == 3);

            foreach(var deleteMessage in messages)
                _queue.DeleteMessage(deleteMessage);
        }

        [Test]
        public void TestInternalDelay()
        {
            var random = new Random().NextDouble().ToString();
            var message = new TestMessage(random);
            message.Delay = TimeSpan.FromDays(1);
            _queue.SendMessage(message, GetType().FullName);

            message.Delay = TimeSpan.Zero;
            _queue.SendMessage(message, GetType().FullName);

            Thread.Sleep(20000);
            var returnedMessage = _queue.GetMessage();

            Assert.IsTrue(returnedMessage != null, "Message expected");
            Assert.IsTrue(returnedMessage.Object.Delay == TimeSpan.Zero);
            Assert.IsTrue(returnedMessage.ClaimCheck == null);
            Assert.IsTrue(returnedMessage.Object.TestValue == random);
            Assert.IsTrue(returnedMessage.Object.Errors.Length == 0);
            _queue.DeleteMessage(returnedMessage);

            returnedMessage = _queue.GetMessage();
            Assert.IsTrue(returnedMessage == null);
        }

        [Test]
        public void RoundTripSmallMessage()
        {
            var random = new Random().NextDouble().ToString();
            var message = new TestMessage(random);

            _queue.SendMessage(message, GetType().FullName);
            Thread.Sleep(20000);
            var returnedMessage = _queue.GetMessage();

            Assert.IsTrue(returnedMessage != null, "Message expected");
            Assert.IsTrue(returnedMessage.ClaimCheck == null);
            Assert.IsTrue(returnedMessage.Object.TestValue == random);
            Assert.IsTrue(returnedMessage.Object.Errors.Length == 0);
           
            _queue.DeleteMessage(returnedMessage);
        }

        [Test]
        public void TestDelayedMessage()
        {
            var random = new Random().NextDouble().ToString();
            var message = new TestMessage(random);

            _queue.SendMessage(message, TimeSpan.FromMilliseconds(40500), "");
            Thread.Sleep(20000);
            var returnedMessage = _queue.GetMessage();

            Assert.IsTrue(returnedMessage == null);
            Thread.Sleep(30000);

            returnedMessage = _queue.GetMessage();
            Assert.IsTrue(returnedMessage != null, "Message expected");
            Assert.IsTrue(returnedMessage.ClaimCheck == null);
            Assert.IsTrue(returnedMessage.Object.TestValue == random);
            Assert.IsTrue(returnedMessage.Object.Errors.Length == 0);

            _queue.DeleteMessage(returnedMessage);
        }

        [Test]
        public void TestBigMessageClaimCheck()
        {
            StringBuilder builder = new StringBuilder();

            for (int x = 0; x < AmazonQueue<TestMessage>.QUEUE_MESSAGE_LIMIT + 1; x++)
                builder.Append("t");

            string payLoad = builder.ToString();
            var message = new TestMessage(payLoad);

            _queue.SendMessage(message, GetType().FullName);
            Thread.Sleep(20000);
            var returnedMessage = _queue.GetMessage();

            Assert.IsTrue(returnedMessage != null, "Message expected");
            Assert.IsTrue(returnedMessage.Object.TestValue == payLoad);
            Assert.IsTrue(!string.IsNullOrEmpty(returnedMessage.ClaimCheck));

            _queue.DeleteMessage(returnedMessage);
        }

        [Test]
        public void BuildMessagePassedNullReturnsNull()
        {
            var result = AmazonQueue<TestMessage>.BuildMessage(null);
            Assert.IsNull(result);
        }

        [Test]
        public void BuildMessageReturnsNullWhenPassedMessageWithNullBody()
        {
            var m = new Message {Body = null};
            var result = AmazonQueue<TestMessage>.BuildMessage(m);
            Assert.IsNull(result);
        }

        [Test]
        public void BuildMessageReturnsNullWhenPassedMessageWithEmptyStringBody()
        {
            var m = new Message {Body = string.Empty};
            var result = AmazonQueue<TestMessage>.BuildMessage(m);
            Assert.IsNull(result);
            
        }

        [Test]
        public void BuildMessageReturnsNullWhenPassedMessageWithoutJsonEncodedWrapperInBody()
        {
            var m = new Message { Body = "abc" };
            var result = AmazonQueue<TestMessage>.BuildMessage(m);
            Assert.IsNull(result);            
        }

        [Test]
        public void BuildMessageReturnsNullWhenPassedMessageWithInvalidClaimCheckToken()
        {
            var m = new Message
                        {
                            Body = new AmazonQueue<TestMessage>.Wrapper(Guid.NewGuid().ToString()).ToString()
                        };
            var result = AmazonQueue<TestMessage>.BuildMessage(m);
            Assert.IsNull(result);                        
        }

        [Test]
        public void SendMessageReturnIdReturnsEmptyStringWhenPassedNull()
        {
            var id = _queue.SendMessageReturnId(null, null, null, null);
            Assert.IsEmpty(id);
        }



        private class TestMessage : MessageBase
        {
            public TestMessage(string testValue)
            {
                TestValue = testValue;
            }

            public string TestValue { get; private set; }
        }
    }
}
