﻿using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class NamingStrategyFactoryTests
    {
        [Test]
        public void FactoryGetsNamingStrategy_Global()
        {
            var strategy = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy);
            Assert.That(strategy.GetType().IsAssignableFrom(typeof(GenericNamingStrategy)));
        }

        [Test]
        public void FactoryGetsNamingStrategy_Topic()
        {
            var strategy = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.TopicStrategy);
            Assert.That(strategy.GetType().IsAssignableFrom(typeof(TopicNamingStrategy)));
        }
    }
}
