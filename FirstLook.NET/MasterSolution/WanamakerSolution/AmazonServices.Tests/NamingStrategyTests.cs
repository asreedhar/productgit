﻿using System;
using AmazonServices.Properties;
using AmazonServices.S3;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class NamingStrategyTests
    {
        [Test]
        public void Global_Naming_Strategy()
        {
            const char INVALID_DELIM = '?';

            var namingStrategy = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.GlobalStrategy);

            string name = Guid.NewGuid().ToString();
            var old_name = namingStrategy.Apply(name, INVALID_DELIM);
            Console.WriteLine("Old name: {0}", old_name);

            var new_name = namingStrategy.Apply(name, INVALID_DELIM, true);
            Console.WriteLine("New name: {0}", new_name);

            Assert.AreEqual( name + "?" + Settings.Default.Environment , old_name);
            Assert.AreEqual( name + "-" + Settings.Default.Environment.ToLower(), new_name);
        }

        [Test]
        public void Topic_Naming_Strategy()
        {
            const char INVALID_DELIM = '_';

            var namingStrategy = NamingStrategyFactory.GetNamingStrategy(NamingStrategyType.TopicStrategy);

            string name = Guid.NewGuid().ToString();
            var old_name = namingStrategy.Apply(name, INVALID_DELIM);
            Console.WriteLine("Old name: {0}", old_name);

            var new_name = namingStrategy.Apply(name, INVALID_DELIM, true);
            Console.WriteLine("New name: {0}", new_name);

            // The generic topic name utility also enforces lowercase topic names
            Assert.AreEqual(name + "_" + Settings.Default.Environment.ToLower(), old_name);
            Assert.AreEqual(name + "-" + Settings.Default.Environment.ToLower(), new_name);
        }
    }
}
