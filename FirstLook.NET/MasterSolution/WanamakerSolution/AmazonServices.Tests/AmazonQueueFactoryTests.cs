﻿using System;
using System.Linq;
using AmazonServices.SQS;
using Core.Messaging;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    /// <summary>
    /// You must wait 60 seconds between successive runs of these tests.  AmazonQueue cannot be deleted/re-created again within 60 seconds.
    /// </summary>
    [TestFixture]
    public class AmazonQueueFactoryTests
    {
        private const string QUEUE_NAME = "AmazonQueueFactoryTests";
        private IQueue<AqfTest> _queue;

        [TestFixtureSetUp]
        public void Setup()
        {
            // create a queue
            _queue = AmazonQueueFactory<AqfTest>.CreateAmazonQueue(QUEUE_NAME);            
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            // Permissions have changed, no longer can delete queues or bussessesseseess
            //var q = _queue as AmazonQueue<AqfTest>;
            //q.DeleteQueue();
        }


        [Test]
        public void AfterCreateQueueIsNotNull()
        {
            Assert.IsNotNull(_queue);            
        }

        [Test]
        public void AfterCreateQueueHasArn()
        {
            Console.WriteLine(_queue.QueueArn);
            Assert.IsNotNullOrEmpty(_queue.QueueArn);
        }

        [Test]
        public void AfterCreateQueueHasUrl()
        {
            Console.WriteLine(_queue.Url);
            Assert.IsNotNullOrEmpty(_queue.Url);            
        }

        [Test]
        public void AfterCreateQueueHasNoMessages()
        {
            // no messages
            var messages = _queue.GetMessages(10);
            Assert.IsNotNull(messages);
            Assert.AreEqual(0, messages.Count());
        }

        [Test]
        public void AfterCreateDeadLetterQueueIsAvailable()
        {
            AmazonQueue<AqfTest> q = _queue as AmazonQueue<AqfTest>;
            Assert.IsNotNull(q);
            Assert.IsNotNull(q.DeadLetterBus);
        }


    }

    public class AqfTest : Entity 
    {
    }
}
