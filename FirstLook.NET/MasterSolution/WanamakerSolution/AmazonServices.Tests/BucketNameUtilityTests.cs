﻿using System;
using AmazonServices.S3;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class BucketNameUtilityTests
    {
        private const int GLOBAL_MAX = 63;
        private const int US_MAX = 80;
        private const int GLOBAL_MIN = 3;

        [Test]
        public void Max_Name_Length()
        {
            Assert.AreEqual(GLOBAL_MAX, GlobalNameUtility.MaxNameLength(true));
            Assert.AreEqual(US_MAX, GlobalNameUtility.MaxNameLength(false));
        }

        [Test]
        public void IsValidLength()
        {
            // test the max
            string max_new = new string('a', GLOBAL_MAX);
            string max_old = new string('a', US_MAX);
            Assert.IsTrue(GlobalNameUtility.IsValidLength(max_new, true));
            Assert.IsTrue(GlobalNameUtility.IsValidLength(max_old));

            // these are too long
            string long_new = new string('a', GLOBAL_MAX + 1);
            string long_old = new string('a', US_MAX + 1);
            Assert.IsFalse(GlobalNameUtility.IsValidLength(long_new, true));
            Assert.IsFalse(GlobalNameUtility.IsValidLength(long_old));

            // too short
            string short_string = new string('a', GLOBAL_MIN - 1);
            Assert.IsFalse(GlobalNameUtility.IsValidLength(short_string));
            Assert.IsFalse(GlobalNameUtility.IsValidLength(short_string,true));
        }

        [Test]
        public void IsValidName_ReturnsTrue()
        {
            var valid_names = new[]
            {
                "900",
                "90-hello",
                "this-is-fine-90",
                "a-b",
                "a.b",
                "a.b.c",
                "a-b-c",
                "a.b-c",
                "a-b.c",
                "a-b.c-d.e-f"
            };

            foreach (string test in valid_names)
            {
                Console.WriteLine(test);
                Assert.IsTrue(GlobalNameUtility.IsValidName(test, true));
            }

        }

        [Test]
        public void IsValidName_ReturnsFalse()
        {
            foreach (bool b in new[] { true, false })
            {
                // it's never ok to start or end with a period or dash
                Assert.IsFalse(GlobalNameUtility.IsValidName(".a", b));
                Assert.IsFalse(GlobalNameUtility.IsValidName("a.", b));
                Assert.IsFalse(GlobalNameUtility.IsValidName("-a", b));
                Assert.IsFalse(GlobalNameUtility.IsValidName("a-", b));

                // never ok to have repeated periods, dashes, or sequences of periods and dashes
                Assert.IsFalse(GlobalNameUtility.IsValidName("a..a", b));
                Assert.IsFalse(GlobalNameUtility.IsValidName("a--a", b));
                Assert.IsFalse(GlobalNameUtility.IsValidName("a.-a", b));
                Assert.IsFalse(GlobalNameUtility.IsValidName("a-.a", b));

                // must not be formatted as an IP address
                Assert.IsFalse(GlobalNameUtility.IsValidName("1.2.3.4", b));                
            }

            // these stricter rules only apply to global naming

            // no upper case
            Assert.IsFalse(GlobalNameUtility.IsValidName("abC", true));

            // only allowed characters are period, alphanumeric, and dash
            foreach (string s in new[]
            {
                "<", ">", "?", "/", "~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "=",
                "{", "[", "}", "]", "\\", "|", ";", ":", "'", "\""
            })
            {
                string test = "a" + s + "b";
                Console.WriteLine(test);
                Assert.IsFalse(GlobalNameUtility.IsValidName(test, true));
            }

            var invalid_names = new string[]
            {
                "b_a_d",
                "a_bad",
                "-bad",
                ".bad",
                "bad.",
                "a..bad",
                "a--bad",
                "a bad",
                "a-.bad",
                "a.-bad",
                "a--bad"
            };

            foreach (var invalid in invalid_names)
            {
                Console.WriteLine(invalid);
                Assert.IsFalse(GlobalNameUtility.IsValidName(invalid, true));
            }
        }

    }
}
