﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AmazonServices.SNS;
using AmazonServices.SQS;
using Core.Messages;
using Core.Messaging;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    public class BusTests
    {
        private IBus<BusTestMessage, BusTestMessage> _bus;
        private IList<AmazonQueue<BusTestMessage>> _queues;

        private AmazonQueue<BusTestMessage> _nonSubscribedQueue;

        private Random _random;

        [TestFixtureSetUp]
        public void Setup()
        {
            _bus = BusFactory<BusTestMessage, BusTestMessage>.Create("Tests_BusTestMessage");

            _queues = new List<AmazonQueue<BusTestMessage>>();
            for (int x = 0; x < 3; x++)
            {
                var queue = new AmazonQueue<BusTestMessage>("Tests_BusTestMessage_" + x);
                queue.DeadLetterBus = BusFactory<DeadLetterMessage, DeadLetterMessage>.Create("Tests_DeadLetter");
                _queues.Add(queue);
            }

            _nonSubscribedQueue = new AmazonQueue<BusTestMessage>("Tests_BusTestMessage");
            _nonSubscribedQueue.DeadLetterBus = BusFactory<DeadLetterMessage, DeadLetterMessage>.Create("Tests_DeadLetter");

            _random = new Random(DateTime.Now.Millisecond);
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            // Clean up
/*
            _queue.DeleteQueue(); // need to notify the bus?
            _factory.Delete(_bus);
*/
        }


        [Test]
        public void SubscriberReceivesMessage()
        {
            // Add a subscriber

            foreach (var queue in _queues)
            {
                //test subscribing twice
                _bus.Subscribe(queue);
                _bus.Subscribe(queue);
            }

            // publish a message and verify receipt.
            var m = new BusTestMessage {Id = _random.Next(), Name = "GetSubscribersReturnsSubscriber"};

            _bus.Publish(m, GetType().FullName);

            // wait a few seconds
            Thread.Sleep(20000);

            foreach (var queue in _queues)
            {
                // Look in the top 10 messages for ours
                var topTen = queue.GetMessages(10);

                bool received = false;
                IMessage<BusTestMessage> receivedMessage = null;

                foreach (var message in topTen)
                {
                    if (message.Object.Id == m.Id && message.Object.Name == m.Name)
                    {
                        received = true;
                        receivedMessage = message;
                        break;
                    }
                }

                Assert.IsTrue(received);

                // Delete the message if it was found
                if (receivedMessage != null)
                {
                    queue.DeleteMessage(receivedMessage);
                }
            }
        }

        [Test]
        public void GetSubscribersSubscribeUnsubscribe()
        {
            // Add a subscriber queue
            _bus.Subscribe(_queues.First());
            Assert.IsTrue(_bus.IsSubscribed(_queues.First()));
         
            // Remove a subscriber queue
            _bus.Unsubscribe(_queues.First());
            Assert.IsFalse(_bus.IsSubscribed(_queues.First()));
        }

        [Test]
        public void UnSubscribeNonSubscrided()
        {
            _bus.Unsubscribe(_nonSubscribedQueue);
        }



    }

    public class BusTestMessage : Entity {}
}