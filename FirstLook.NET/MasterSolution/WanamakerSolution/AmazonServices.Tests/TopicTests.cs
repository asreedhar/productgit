﻿using System;
using AmazonServices.SNS;
using AmazonServices.SQS;
using Core.Messaging;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    public class TopicTests
    {
        private string _topic = "TopicTest";
        private string _topicArn = string.Empty;
        private string _subscriptionArn = string.Empty;

        private IQueue<TopicTest> _queue;

        [TestFixtureSetUp]
        public void Setup()
        {
            // Create topic and subscribe via our return queue
            _topicArn = Topic.CreateTopic(_topic);

            // We will receive messages here
            _queue = AmazonQueueFactory<TopicTest>.CreateAmazonQueue("Tests_TopicTest");

            // Get rid of any existing stuff
            /*
                        Bus.Unsubscribe(_subscriptionArn);
                        Bus.DeleteTopic(_topicArn);
            */
        }

        [TestFixtureTearDown]
        public void Unsubscribe()
        {
            // unsubscribe the queue
            //            Bus.Unsubscribe(_subscriptionArn);

            // remove the topic
            //            Bus.DeleteTopic(_topicArn);

            // delete the queue

        }

        [Test]
        public void CreateTopic()
        {            
            Console.WriteLine(_topicArn);
            Assert.IsNotNullOrEmpty(_topicArn);

            // create again
            var topic2 = Topic.CreateTopic(_topic);
            Console.WriteLine(topic2);
            Assert.IsNotNullOrEmpty(topic2);

            Assert.AreEqual(_topicArn, topic2);
        }

        [Test]
        public void RegisterSubscribeWithQueueEndpoint()
        {
            // Remove subscriptions.
            Topic.Unsubscribe(_topicArn, _queue);

            // verify subscription is now false
            bool subscribed = Topic.IsSubscribed(_topicArn, string.Empty, _queue.QueueArn);
            Assert.IsFalse(subscribed);

            subscribed = Topic.IsSubscribed(_topicArn, _queue);
            Assert.IsFalse(subscribed);


            // Subscribe
            _subscriptionArn = Topic.Subscribe(_topicArn, _queue);
            Console.WriteLine(_subscriptionArn);
            Assert.IsNotNullOrEmpty(_subscriptionArn);

            // verify subscription is now true
            subscribed = Topic.IsSubscribed(_topicArn, string.Empty, _queue.QueueArn);
            Assert.IsTrue(subscribed);

            subscribed = Topic.IsSubscribed(_topicArn, _queue);
            Assert.IsTrue(subscribed);


            // Also verify by subscription
            subscribed = Topic.IsSubscribed(_topicArn, _subscriptionArn, string.Empty);
            Assert.IsTrue(subscribed);
        }


        [Test]
        public void Publish()
        {
            // Subscribe
            _subscriptionArn = Topic.Subscribe(_topicArn, _queue);

            // Publish a message
            var f = new TopicTest {Id = 1, Name = "Zac"};
            var messageId = Topic.Publish(f, _topicArn);
            Console.WriteLine(messageId);
            Assert.IsNotNullOrEmpty(messageId);

        }
    }

    public class TopicTest : Entity  {}
}