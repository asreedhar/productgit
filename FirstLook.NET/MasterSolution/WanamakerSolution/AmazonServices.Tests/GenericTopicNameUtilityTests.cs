﻿using AmazonServices.SNS;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class GenericTopicNameUtilityTests
    {
        [Test]
        public void Name_To_Lower()
        {
            var name = "MixedCase";
            name = GenericTopicNameUtility.Name(name);
            Assert.AreEqual("mixedcase", name);
        }

        [Test]
        public void Name_Includes_Postfix()
        {
            var name = "Topic";
            name = GenericTopicNameUtility.Name(name, postfix: "Alpha");
            Assert.AreEqual("topic_alpha", name);
        }

    }
}
