﻿using Core.Messaging;
using Core.Messages;

namespace AmazonServices.IntegrationTests
{
    public class Entity : MessageBase 
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
