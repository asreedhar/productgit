﻿using System;
using System.Threading;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using AmazonServices.Properties;
using AmazonServices.SNS;

using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class GenericTopicTests
    {
/*
        [Test]
        public void Can_Create_And_Delete_Topic()
        {
            var bucketName = Guid.NewGuid().ToString();
            var arn = GenericTopic.CreateTopic(bucketName);
            Console.WriteLine("ARN: {0}", arn);
            Assert.IsNotEmpty(arn);

            Thread.Sleep(10000);
            Assert.IsTrue( Exists(arn) );

            // need elevated permissions to delete the topic.
            GenericTopic.DeleteTopic(arn);
            Thread.Sleep(30000);

            Assert.IsFalse(Exists(arn));
        }
*/

        private bool Exists(string arn)
        {
            var snsClient = new AmazonSimpleNotificationServiceClient(Settings.Default.AwsAccessKey,
                Settings.Default.AwsSecretAccessKey);
            
            var listTopicsResponse = snsClient.ListTopics(new ListTopicsRequest());

            return listTopicsResponse.ListTopicsResult.Topics.Exists(t => t.TopicArn == arn);            
        }

        [Test]
        public void Can_Send_Message()
        {
            var topicName = "generic_topic_tests_can_send_message";
            var arn = GenericTopic.CreateTopic(topicName);
            Console.WriteLine("ARN: {0}", arn);

            string message = "Test message from Can_Send_Message test";
            string messageId = GenericTopic.Publish(message, arn);

            Console.WriteLine("Message id: {0}", messageId);
            Assert.IsNotEmpty(messageId);
        }

    }
}
