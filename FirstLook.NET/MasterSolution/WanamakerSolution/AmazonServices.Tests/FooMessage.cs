﻿using Core.Messaging;
using Core.Messages;

namespace AmazonServices.IntegrationTests
{
    public class FooMessage : MessageBase 
    {
        public string Bar { get; set; }
    }
}