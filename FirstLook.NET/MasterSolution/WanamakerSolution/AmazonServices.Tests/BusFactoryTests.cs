﻿using System;
using AmazonServices.SNS;
using Core.Messaging;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    public class BusFactoryTests
    {
        private IBus<BusFactoryTestMessage, Entity> _bus;

        private const string BUS_NAME = "Tests_BusFactoryTest";

        [TestFixtureSetUp]
        public void Setup()
        {
            _bus =  BusFactory<BusFactoryTestMessage, Entity>.Create(BUS_NAME);
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            // Permissions have changed, no longer can delete queues or bussessesseseess
            //BusFactory<BusFactoryTestMessage, Entity>.Delete(_bus);
        }

        [Test]
        public void AfterCreateBusIsNotNull()
        {
            Assert.IsNotNull(_bus);
        }

        [Test]
        public void AfterCreateBusHasId()
        {
            Assert.IsNotNullOrEmpty(_bus.Id);
        }

        [Test]
        [Ignore] // TODO: Write this test before we start using SNS
        public void Publish()
        {
            throw new NotImplementedException("Write a test");
        }

        [Test]
        [Ignore] // TODO: Write this test before we start using SNS
        public void Subscribe()
        {
            throw new NotImplementedException("Write a test");
        }

        [Test]
        [Ignore] // TODO: Write this test before we start using SNS
        public void Unsubscibe()
        {
            throw new NotImplementedException("Write a test");
        }

    }

    public class BusFactoryTestMessage : Entity {}
}
