﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using AmazingCloudSearch.MaxInterface;
using AmazingCloudSearch.MaxInterface.Documents;
using NUnit.Framework;

namespace AmazonServices.IntegrationTests
{
    [TestFixture]
    public class CloudSearchTests
    {
        private DealerSearchDocument _testDocument;
        private List<DealerSearchDocument> _dealerSearchDocuments;

        [SetUp]
        public void Setup()
        {
            _dealerSearchDocuments = new List<DealerSearchDocument>
            {
                new DealerSearchDocument
                {
                    Address = "Test address",
                    BusinessUnitCode = "TESTUNIT01",
                    BusinessUnitId = 111111,
                    BusinessUnitName = "Test Business Unit 3",
                    City = "Test City",
                    Description = "Dealer tag line goes here",
                    Email = "thuber@maxdigital.com",
                    GroupId = 222222,
                    OwnerHandle = "TestOwnerHandle2",
                    Phone = "512-123-1234",
                    State = "TX",
                    Url = "www.google.com",
                    LogoUrl = "https://www.google.com/images/srpr/logo11w.png",
                    ZipCode = "54321",
                    HasMaxForWebsite2Upgrade = false,
                    HasMobileShowroomUpgrade = true,
                    HasShowroomUpgrade = true
                },
                new DealerSearchDocument
                {
                    Address = "Test address",
                    BusinessUnitCode = "TESTUNIT02",
                    BusinessUnitId = 111111,
                    BusinessUnitName = "Test Business Unit",
                    City = "Test City",
                    Description = "Dealer tag line goes here",
                    Email = "thuber@maxdigital.com",
                    GroupId = 222222,
                    OwnerHandle = "TestOwnerHandle",
                    Phone = "512-123-1234",
                    State = "TX",
                    Url = "www.google.com",
                    LogoUrl = "https://www.google.com/images/srpr/logo11w.png",
                    ZipCode = "54321",
                    HasMaxForWebsite2Upgrade = false,
                    HasMobileShowroomUpgrade = true,
                    HasShowroomUpgrade = true
                },
                new DealerSearchDocument
                {
                    Address = "Test address",
                    BusinessUnitCode = "TESTUNIT03",
                    BusinessUnitId = 111111,
                    BusinessUnitName = "Test Business Unit 3",
                    City = "Test City",
                    Description = "Dealer tag line goes here",
                    Email = "thuber@maxdigital.com",
                    GroupId = 222222,
                    OwnerHandle = "TestOwnerHandle3",
                    Phone = "512-123-1234",
                    State = "TX",
                    Url = "www.google.com",
                    LogoUrl = "https://www.google.com/images/srpr/logo11w.png",
                    ZipCode = "54321",
                    HasMaxForWebsite2Upgrade = false,
                    HasMobileShowroomUpgrade = true,
                    HasShowroomUpgrade = true
                }
            };

            _testDocument = _dealerSearchDocuments.First();
        }

        [Test]
        public void CanUploadToCloudSearch()
        {
            var csDomain = CloudSearchFactory<DealerSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["dealer_cloud_search_url"]);
            var addresult = csDomain.Add(_testDocument);
            Assert.That(addresult.IsError == false);
            Assert.That(addresult.errors.Count == 0);
            Assert.That(addresult.adds == 1);
        }

        [Test]
        public void CanUpdateCloudSearch()
        {
            // First insert a record in the dev test domain
            var csDomain = CloudSearchFactory<DealerSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["dealer_cloud_search_url"]);
            var addresult = csDomain.Add(_testDocument);
            Assert.That(addresult.IsError == false);

            // Make sure it's right
            //var bQuery = new BooleanQuery();
            //var stringQuery = new StringBooleanCondition("ownerhandle", _testDocument.OwnerHandle);
            //bQuery.Conditions.Add(stringQuery);
            //var searchQuery = new SearchQuery<DealerSearchDocument> {BooleanQuery = bQuery};
            //var searchResults = csDomain.Search(searchQuery);
            //Assert.That(searchResults.IsError == false);
            //Assert.That(searchResults.hits.found == 1);
            //Assert.That(searchResults.hits.hit[0].data.BusinessUnitCode == _testDocument.BusinessUnitCode);


            // Update the data
            var upDoc = _testDocument;
            Assert.That(upDoc != null);
            upDoc.BusinessUnitCode = "TESTUPDA01";

            var updateResult = csDomain.Update(upDoc);
            Assert.That(updateResult.IsError == false);

            //// Make sure that is right
            //searchQuery = new SearchQuery<DealerSearchDocument> { BooleanQuery = bQuery };
            //searchResults = csDomain.Search(searchQuery);
            //Assert.That(searchResults.IsError == false);
            //Assert.That(searchResults.hits.found == 1);
            //Assert.That(searchResults.hits.hit[0].data.BusinessUnitCode == upDoc.BusinessUnitCode);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoCloudSearchIdThrowsArgumentNull()
        {
            var csDomain = CloudSearchFactory<DealerSearchDocument>.CreateCloudSearchConnection();
            csDomain.Add(_testDocument);
        }

        [Test]
        [Ignore("Nothing to assert here, just wanted to verify that Add will do an update if the document exists already.")]
        public void AddTheSameDocumentPerformsUpdate()
        {
            var csDomain = CloudSearchFactory<DealerSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["dealer_cloud_search_url"]);
            csDomain.Add(_testDocument);
            Thread.Sleep(5000);
            _testDocument.BusinessUnitCode = "TESTUPDA01";
            csDomain.Add(_testDocument);
            Thread.Sleep(5000);
            _testDocument.BusinessUnitCode = "TESTUPDA02";
            csDomain.Add(_testDocument);
            Thread.Sleep(5000);
        }

        [Test]
        public void AddAcceptsAList()
        {
            var csDomain = CloudSearchFactory<DealerSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["dealer_cloud_search_url"]);
            var addresult = csDomain.Add(_dealerSearchDocuments);
            Assert.That(addresult.IsError == false);
            Assert.That(addresult.errors.Count == 0);
            Assert.That(addresult.adds == 3);
        }

        [TearDown]
        public void TearDown()
        {
            // Make sure there is enough time for the document to actually be there before trying to delete it
            Thread.Sleep(2000);
            var csDomain = CloudSearchFactory<DealerSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["dealer_cloud_search_url"]);
            csDomain.Delete(_dealerSearchDocuments);
        }
    }
}