﻿using System;
using System.Collections.Generic;
using System.Threading;
using MAX.Threading;
using System.ServiceProcess;


namespace MAX.TaskFramework
{

    public abstract class TaskServiceBase : ServiceBase
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly List<Thread> _threads;

        private readonly ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private readonly ManualResetEvent _blockWhenNoMessages = new ManualResetEvent(false);

        protected TaskServiceBase()
        {
            _threads = new List<Thread>();
        }

        protected void StartBackgroundThread(IRunWithCancellation runner, string name)
        {
            // Threads will be blocked for 15 seconds between iteratations of their run loop.
            Thread t = NamedBackgroundThread.Start(name, 
                () => runner.Run(() => _shutdownEvent.WaitOne(0), () => _blockWhenNoMessages.WaitOne(15000)));

            _threads.Add(t);
        }

        protected void StopThreads()
        {
            _shutdownEvent.Set();       // signal that it's time to exit.
            _blockWhenNoMessages.Set(); // will wake blocked threads immediately.

            // Give each thread 30 seconds to stop.
            TimeSpan totalTimeout = TimeSpan.FromSeconds(30 * _threads.Count);

            foreach (Thread thread in _threads)
            {
                DateTime start = DateTime.Now;

                Log.InfoFormat("Attempting to stop thread '{0}'.", thread.Name);

                if (!thread.Join(totalTimeout))
                {
                    thread.Abort();
                    Log.ErrorFormat("Thread '{0}' aborted after {1} seconds.", thread.Name, totalTimeout.TotalSeconds);
                }
                else
                {
                    Log.InfoFormat("Thread '{0}' stopped cleanly.", thread.Name);
                }
                totalTimeout -= (DateTime.Now - start);
            }
            Log.Info("All threads stopped.");
        }

        protected static Version AppVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}
