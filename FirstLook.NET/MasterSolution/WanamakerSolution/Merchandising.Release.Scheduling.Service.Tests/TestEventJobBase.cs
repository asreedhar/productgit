﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using Merchandising.Release.Scheduling.Service.Jobs;
using Core.Messaging;

namespace Merchandising.Release.Scheduling.Service.Tests
{
    [TestFixture]
    public class TestEventJobBase
    {


        [Test]
        public void TestDriver()
        {

            // test database higher but within max delta
            Assert.AreEqual(TestEventDiff(10050, "10000", "100"), 10000, "Should have returned S3 value!");

            // test database higher but outside max delta
            Assert.AreEqual(TestEventDiff(10150, "10000", "100"), 10150, "Should have returned database value!");

            // test database higher without max delta
            Assert.AreEqual(TestEventDiff(10150, "10000", null), 10000, "Should have returned S3 value!");

            // test database higher without max delta
            Assert.AreEqual(TestEventDiff(10050, "10000", null), 10000, "Should have returned S3 value!");

            // test s3 higher without max delta
            Assert.AreEqual(TestEventDiff(10000, "10050", null), 10000, "Should have returned database value!");

            // test s3 higher with max delta
            Assert.AreEqual(TestEventDiff(10000, "10050", "100"), 10000, "Should have returned database value!");
        }


        public int TestEventDiff(int iDatabaseValue, string sAmazonS3value, string sMaxBusEventDelta)
        {
            
            // mock config setting
            var appRespository =  new Mock<IAppSettingsRepository>();
            appRespository.Setup(x => x.Fetch(It.IsAny<string>())).Returns(sMaxBusEventDelta);
            
            // mock the s3 "last value"
            var fileStorage = new Mock<IFileStorage>();
            bool exists = true;
            fileStorage.Setup(x => x.Exists(It.IsAny<string>(), out exists)).Returns((string file, bool test) =>
            {
                var stream = new MemoryStream();
                var writer = new StreamWriter(stream);

                string value = sAmazonS3value;
                writer.WriteLine(value);
                writer.Flush();
                stream.Position = 0;
                return stream;
            });

            // mock the database value return
            var queueFactoryMock = new Mock<EventJobBase<int>>("file", appRespository.Object);
            
            queueFactoryMock.CallBase = true;
            queueFactoryMock.Setup(x => x.GetMaxEvent()).Returns(iDatabaseValue);

            int returnValue =  queueFactoryMock.Object.GetLastEventId(fileStorage.Object);

            return returnValue;
            
        }
    }
}
