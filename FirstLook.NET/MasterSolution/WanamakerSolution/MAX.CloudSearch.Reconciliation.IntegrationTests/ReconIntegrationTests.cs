﻿

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using MAX.CloudSearch.Reconciliation.CloudSearch;
using MAX.CloudSearch.Reconciliation.Models;
using NUnit.Framework;

namespace MAX.CloudSearch.Reconciliation.IntegrationTests
{
    [TestFixture]
    public class ReconIntegrationTests
    {

        private enum VinOptions
        {
            All,
            LowerCase
        }

        readonly VehicleCloudSearchDataRepository _repository = new VehicleCloudSearchDataRepository();

        private IEnumerable<VehicleSearchData> GetVinsAtDealer(string ownerHandle, int businessUnitId, VinOptions vinOptions)
        {
            var csVehicles = new VehicleCloudSearchDataRepository().GetCloudSearchVehicles(new DealerInfo
            {
                BusinessUnitId = businessUnitId,
                OwnerHandle = new Guid(ownerHandle)
            }, 10000);

            return vinOptions == VinOptions.LowerCase ? 
                csVehicles.Where(vehicleSearchData => vehicleSearchData.Vin != vehicleSearchData.Vin.ToUpperInvariant()) 
                : csVehicles;
        }


        [Test]
        public void GetVinsForDealer()
        {
            //var ownerHandle = "2B346BB8-E463-E111-BD37-0022198DC5E2";
            //var businessUnitId = 105700;


            var ownerHandle = "4FB3C338-B5C3-DC11-9377-0014221831B0";
            var businessUnitId = 102303;

            var x = GetVinsAtDealer(ownerHandle, businessUnitId, VinOptions.All).ToList();
            var y = GetVinsAtDealer(ownerHandle, businessUnitId, VinOptions.LowerCase).ToList();

            Debug.WriteLine(string.Format("Lowercase Vins ({0}):", y.Count));
            foreach (var vehicleSearchData in y)
            {
                Debug.WriteLine(string.Format("LC Vehicle:{0}", vehicleSearchData.Vin));
            }

            Debug.WriteLine("");

            Debug.WriteLine(string.Format("All Vins ({0}):", x.Count));
            foreach (var vehicleSearchData in x)
            {
                Debug.WriteLine(string.Format("Vehicle:{0}", vehicleSearchData.Vin));
            }
        }




        [Test]
        public void DeleteOneLowerCaseVin()
        {

            var lowercasevins = GetVinsAtDealer("2B346BB8-E463-E111-BD37-0022198DC5E2", 105700, VinOptions.LowerCase).ToList();

            Debug.WriteLine(string.Format("LowerCaseVin Count: {0}", lowercasevins.Count));

            if (lowercasevins.Any())
            {
                Debug.WriteLine("Attempting to delete the first document with an upper case vin");

                var thisSearchData = lowercasevins[0];
                var originalVin = thisSearchData.Vin;
                var newVin = originalVin.ToUpperInvariant();

                Debug.WriteLine("Changing vin from {0} to {1}", originalVin, newVin);

                thisSearchData.Vin = newVin;

                Debug.WriteLine("Attempting delete...");
                var deletes1 = new VehicleCloudSearchDataRepository().DeleteCloudSearchVehicles(new List<VehicleSearchData> { thisSearchData });
                Debug.WriteLine(string.Format("deletes1: {0}", deletes1));

                Thread.Sleep(5000);

                var lcvinsAfterFirstTry = GetVinsAtDealer("2B346BB8-E463-E111-BD37-0022198DC5E2", 105700, VinOptions.LowerCase).ToList();

                Debug.WriteLine(string.Format("LowerCaseVin (1) Count: {0}", lcvinsAfterFirstTry.Count()));

                Debug.WriteLine(string.Format("Changing vin from {0} back to {1}", thisSearchData.Vin, originalVin ));

                thisSearchData.Vin = originalVin;

                Debug.WriteLine("Attempting delete again...");
                var deletes2 = new VehicleCloudSearchDataRepository().DeleteCloudSearchVehicles(new List<VehicleSearchData> { thisSearchData });
                Debug.WriteLine(string.Format("deletes2: {0}", deletes2));
                Thread.Sleep(5000);

                var lcvinsAfterSecondTry = GetVinsAtDealer("2B346BB8-E463-E111-BD37-0022198DC5E2", 105700, VinOptions.LowerCase).ToList();

                Debug.WriteLine(string.Format("LowerCaseVin (2) Count: {0}", lcvinsAfterSecondTry.Count()));

            }

        }


    }
}
