﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Autofac;
using Elasticsearch.Net;
using Nest;
using NUnit.Framework;

namespace MAX.Market.IntegrationTest
{
    [TestFixture]
    public class MarketIntegrationTests
    {

        private IContainer _container;

        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new MaxMarketModule());

            _container = builder.Build();
        }


        [TestCase(365933)]
        [TestCase(374441)]
        [TestCase(373972)]
        public void GetVehicleAttributes(int chromeStyleId)
        {

            var client = _container.Resolve<IMappingClient>();

            var x = client.GetVehicleMarketInfo(chromeStyleId);
            Assert.IsNotNull(x);
            Display(x.VehicleMarketAttributeList);
        }




        [Test]
        public void CreateNewIndexBeforeLoadingData()
        {

            var cluster = "http://5e99e43600f0c80122425c4bbe4a1149-us-east-1.foundcluster.com:9200"; // beta
            var indexName = "20150521a";


            var hostUri = new Uri(cluster);

            var connectionSettings = new ConnectionSettings(hostUri, indexName);
            connectionSettings.SetBasicAuthentication("readwrite", "Hgo74p5HB=6FcwSwZnAB");
            connectionSettings.ExposeRawResponse();

            var settings = new IndexSettings();

            var client = new ElasticClient(connectionSettings);

            Debug.WriteLine("Creating index...");

            var indexResponse = client.CreateIndex(c => c
                .Index(indexName)
                .InitializeUsing(settings)
            );

            Debug.WriteLine(string.Format("Acknowledged: {0}", indexResponse.Acknowledged));
            Debug.WriteLine(string.Format("Connection Status: {0}", indexResponse.ConnectionStatus));
            if (indexResponse.ServerError != null)
                DisplayServerError(indexResponse.ServerError);

            Debug.WriteLine("Mapping...");

            var client2 = new ElasticClient(connectionSettings);

            var mapResult = client2.Map<VehicleInfo>(c => c.MapFromAttributes().IgnoreConflicts());

            Debug.WriteLine(string.Format("Acknowledged: {0}", mapResult.Acknowledged));
            Debug.WriteLine(string.Format("Connection Status: {0}", mapResult.ConnectionStatus));

            if (mapResult.ServerError != null)
                DisplayServerError(mapResult.ServerError);



        }

        private static void DisplayServerError(ElasticsearchServerError error)
        {
            Debug.WriteLine("");
            Debug.WriteLine("Server Error:");
            Debug.WriteLine(string.Format("ServerError: {0}", error));
            Debug.WriteLine(string.Format("ServerError.Error {0}", error.Error));
            Debug.WriteLine(string.Format("ServerError.ExceptionType {0}", error.ExceptionType));
            Debug.WriteLine(string.Format("ServerError.Status {0}", error.Status));
            Debug.WriteLine("End of Server Error");
            Debug.WriteLine("");
        }

        private static void Display(IEnumerable<VehicleMarketAttributes> attributesByTrim)
        {
            foreach (var trimAttributes in attributesByTrim)
            {
                Debug.WriteLine(string.Empty);

                Debug.WriteLine(string.Format("TRIM: {0}", trimAttributes.Trim));
                Debug.WriteLine("MATCHES TARGET: {0}", trimAttributes.MatchesTargetVehicleTrim);

                foreach (var e in trimAttributes.Engines)
                {
                    Debug.WriteLine(string.Format("engines: {0}", e));
                }

                foreach (var t in trimAttributes.Transmissions)
                {
                    Debug.WriteLine(string.Format("transmission: {0}", t));
                }

                foreach (var f in trimAttributes.FuelTypes)
                {
                    Debug.WriteLine(string.Format("fuel type: {0}", f));
                }

                foreach (var d in trimAttributes.DriveTrains)
                {
                    Debug.WriteLine(string.Format("drive train: {0}", d));
                }

            }

        }
    }





}
