﻿using System;
using Core.Messages;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.GroupLevel;
using Merchandising.Release.Tasks.Service.Tasks;
using Moq;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;

namespace Merchandising.Release.Tasks.Service.Tests
{
    [TestFixture]
    public class GroupLevelDashboardAggregationTaskTests
    {
        [Test]
        public void TestDeadletterCacheSetToFailed()
        {
            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<GroupLevelAggregationMessage>>();
            var groupLevelCacheMock = new Mock<IGroupLevelDataCache>();
            var dashboardMock = new Mock<IDashboardRepository>();
            var sitePerformanceMock = new Mock<ISitePerformanceRepository>();
            var groupGoogleMock = new Mock<IGoogleAnalyticsResultCache>();
            var groupTtmMock = new Mock<ITimeToMarketRepository>();
            var dealerRepository = new Mock<IDashboardBusinessUnitRepository>();

            queueFactoryMock.Setup(x => x.CreateGroupLevelAggregation()).Returns(queueMock.Object);
            queueMock.SetupGet(x => x.DeadLetterErrorLimit).Returns(5);
            groupLevelCacheMock.Setup(x => x.GetCachedStatus(34)).Returns(new CachedStatus(State.Empty, DateTime.MinValue, 0));

            DashboardManager manager = new DashboardManager(dashboardMock.Object, sitePerformanceMock.Object, null, null, null);
            GroupDashboardManager groupManager = new GroupDashboardManager(groupLevelCacheMock.Object, manager, sitePerformanceMock.Object, groupGoogleMock.Object, groupTtmMock.Object, dealerRepository.Object);

            var task = new Tasks.GroupLevelDashboardAggregation(queueFactoryMock.Object, groupManager);

            var groupAggMessage = new GroupLevelAggregationMessage(34);
            groupAggMessage.AddError("error");
            groupAggMessage.AddError("error");
            groupAggMessage.AddError("error");
            groupAggMessage.AddError("error");

            task.Process(groupAggMessage);

            groupLevelCacheMock.Verify(x => x.SetCachedState(34, State.Processing), Times.Once());
            groupLevelCacheMock.Verify(x => x.SetCachedState(34, State.Failed), Times.Once());
        }

        [Test]
        public void TestWhenPendingNothingIsProcessed()
        {
            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<GroupLevelAggregationMessage>>();
            var groupLevelCacheMock = new Mock<IGroupLevelDataCache>();
            var dashboardMock = new Mock<IDashboardRepository>();
            var sitePerformanceMock = new Mock<ISitePerformanceRepository>();
            var groupGoogleMock = new Mock<IGoogleAnalyticsResultCache>();
            var groupTtmMock = new Mock<ITimeToMarketRepository>();
            var dealerRepository = new Mock<IDashboardBusinessUnitRepository>();

            queueFactoryMock.Setup(x => x.CreateGroupLevelAggregation()).Returns(queueMock.Object);
            queueMock.SetupGet(x => x.DeadLetterErrorLimit).Returns(5);
            groupLevelCacheMock.Setup(x => x.GetCachedStatus(34)).Returns(new CachedStatus(State.Processing, DateTime.Now.ToUniversalTime(), 0));

            DashboardManager manager = new DashboardManager(dashboardMock.Object, sitePerformanceMock.Object, null, null, null);
            GroupDashboardManager groupManager = new GroupDashboardManager(groupLevelCacheMock.Object, manager, sitePerformanceMock.Object, groupGoogleMock.Object, groupTtmMock.Object, dealerRepository.Object);
            var task = new Tasks.GroupLevelDashboardAggregation(queueFactoryMock.Object, groupManager);
            task.Process(new GroupLevelAggregationMessage(34));

            groupLevelCacheMock.Verify(x => x.SetCachedState(34, State.Processing), Times.Never());
            groupLevelCacheMock.Verify(x => x.SetCachedState(34, State.Failed), Times.Never());
        }
    }
}
