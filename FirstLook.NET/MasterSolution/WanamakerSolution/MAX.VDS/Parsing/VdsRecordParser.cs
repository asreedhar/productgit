using System.Collections.Generic;

namespace MAX.VDS.Parsing
{
    public interface IVdsRecordParser
    {
        IEnumerable<string> ParseOptionCodes(string make, IEnumerable<string> optionCodes);
        IEnumerable<string> ParseOptionCodes(string make, string optionCodes);

    }

    public class VdsRecordParser : IVdsRecordParser
    {
        public IEnumerable<string> ParseOptionCodes(string make, string optionCodes)
        {
            return ParseOptionCodes(make, optionCodes.Split(','));
        }

        public IEnumerable<string> ParseOptionCodes(string make, IEnumerable<string> optionCodes)
        {
            if (make != null && make.ToUpper().Trim() == "BMW")
            {
                return ParseOptionCodesBMW(optionCodes);
            }

            return optionCodes;
        }

        private IEnumerable<string> ParseOptionCodesBMW(IEnumerable<string> optionCodes)
        {
            var fixedOptionCodes = new List<string>();
            foreach (var code in optionCodes)
            {
                if (code.Length == 4 && code.StartsWith("0"))
                {
                    fixedOptionCodes.Add(code.Substring(1, 3));
                }
                else
                {
                    fixedOptionCodes.Add(code);
                }
            }
            return fixedOptionCodes;
        }

    }
}