﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;
using MAX.VDS.DataAccess;
using MAX.VDS.Parsing;

namespace MAX.VDS
{
    /// <summary>
    /// Act as a facade to the VDS system.  
    /// </summary>
    public class VdsDataFacade
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IVdsRepository _vdsRepo;

        private const int CountryCode = 1;

        public VdsDataFacade()
        {
            _vdsRepo = new VdsRepository();
        }

        public VdsDataFacade(IVdsRepository vdsRepository)
        {
            _vdsRepo = vdsRepository;
        }

        /// <summary>
        /// Return data extracted from VDS.
        /// </summary>
        /// <param name="vin"></param>
        /// <returns></returns>
        public IVdsData GetChromeData(string vin)
        {
            Log.InfoFormat("Evaluating vin {0}", vin);
            
            // Get a parser.
            IVdsRecordParser parser = new VdsRecordParser();

            // Get vehicle records
            var vehicles = _vdsRepo.GetVehicle(vin).ToList();
            var vehicleMasters = _vdsRepo.GetVehicleMaster(vin).ToList();

            var vehicle = vehicles.FirstOrDefault() ?? new VehicleRecord();

            // Get and parse all the options from the vehicle record and all the master records.
            var options = new List<string>();
            options.AddRange(GetOptionCodes(new[] {vehicle}));
            options.AddRange(GetOptionCodes(vehicleMasters));
            var dedupedOptions = options.Where(o => !string.IsNullOrWhiteSpace(o)).Distinct();
            var parsedOptions = parser.ParseOptionCodes(vehicle.Make, dedupedOptions).ToList();
            Log.InfoFormat("Parsed option codes are '{0}' for vin {1}", string.Join(",", parsedOptions), vin);

            // get the chrome style id
            int? chromeStyleId = GetChromeStyleId(vehicle, vehicleMasters, vin);
            if (!chromeStyleId.HasValue)
            {
                // time to stop. log it.
                // TODO: If the chrome style isn't known, we could still return the options and the other
                //       info we know for downstream decoding.
                Log.WarnFormat("No chrome style id could be determined for vin {0}.", vin);
                return new VdsData();
            }
            Log.InfoFormat("Chrome style id {0} found for vin {1}.", chromeStyleId.Value, vin);

            // Return data.
            var vdsData = new VdsData
            {
                ChromeStyleId = chromeStyleId.Value,
                CountryCode = CountryCode,
                OptionCodes = parsedOptions,
                InteriorColorCode = vehicle.InteriorColorCode,
                InteriorColorDescription = vehicle.InteriorColorDescription,
                ExteriorColorCode = vehicle.ExteriorColorCode,
                ExteriorColorDescription = vehicle.ExteriorColorDescription
            };

            return vdsData;
        }

        private IEnumerable<string> GetOptionCodes(IEnumerable<IVehicleRecord> records)
        {
            var specified = new List<string>();
            foreach (var veh in records)
            {
                if (veh.OptionCodes != null)
                {
                    var options = veh.OptionCodes.Split(',');
                    specified.AddRange(options);                    
                }

                // Also look at the engine and transmission codes, which are sometimes option codes.
                if (veh.EngineCode != null)
                {
                    specified.Add(veh.EngineCode);
                }
                if (veh.TransmissionCode != null)
                {
                    specified.Add(veh.TransmissionCode);
                }
            }

            return specified;
        }

        private int? GetChromeStyleId(IVehicleRecord vehicleRecord, IEnumerable<IVehicleMasterRecord> vehicleMasters, string vin)
        {
            // what chrome style id was reported on the vehicle record?
            if (vehicleRecord.ChromeStyleId.HasValue)
            {
                Log.InfoFormat("Chrome style id {0} found for vin {1} in VDS.Vehicle", vehicleRecord.ChromeStyleId, vin);
                return vehicleRecord.ChromeStyleId.Value;
            }

            // try to get the chrome style id from the vehicle master records.
            var masterChromeStyleIds = vehicleMasters.Where(m => m.ChromeStyleId.HasValue).Select(m => m.ChromeStyleId).Distinct().ToList();
            if (masterChromeStyleIds.Count() == 1)
            {
                Log.InfoFormat("Chrome style id {0} found for vin {1} in VDS.VehicleMaster", masterChromeStyleIds.First(), vin);
                return masterChromeStyleIds.First();
            }
            if (masterChromeStyleIds.Count > 1)
            {
                Log.InfoFormat("Multiple chrome style ids are specified for vin {0} in VDS.VehicleMaster", vin);                                
            }
            else if (masterChromeStyleIds.Count == 0)
            {
                Log.InfoFormat("No chrome style id is set for vin {0} in VDS.VehicleMaster", vin);                
            }
            return null;
        }
    }

    public interface IVdsData
    {
        int? CountryCode { get; set; }
        int? ChromeStyleId { get; set; }
        IEnumerable<string> OptionCodes { get; set; }
        string InteriorColorCode { get; set; }
        string InteriorColorDescription { get; set; }
        string ExteriorColorCode { get; set; }
        string ExteriorColorDescription { get; set; }
    }

    internal class VdsData : IVdsData
    {
        public int? CountryCode { get; set; }
        public int? ChromeStyleId { get; set; }
        public IEnumerable<string> OptionCodes { get; set; }
        public string InteriorColorCode { get; set; }
        public string InteriorColorDescription { get; set; }
        public string ExteriorColorCode { get; set; }
        public string ExteriorColorDescription { get; set; }
    }
}
