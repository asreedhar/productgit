﻿using System;

namespace MAX.VDS.DataAccess
{
    public interface IVehicleRecord
    {
        string VIN { get; set; }
        string Make { get; set; }
        string Model { get; set; }
        int? ModelYear { get; set; }
        string Trim { get; set; }
        string Engine { get; set; }
        string Transmission { get; set; }
        string ModelCode { get; set; }
        string EngineCode { get; set; }
        string TransmissionCode { get; set; }
        string OptionCodes { get; set; }
        int? ChromeStyleId { get; set; }
        DateTime LoadDate { get; set; }
        string InteriorColorCode { get; set; }
        string InteriorColorDescription { get; set; }
        string ExteriorColorCode { get; set; }
        string ExteriorColorDescription { get; set; }
    }

    class VehicleRecord : IVehicleRecord
    {
        public VehicleRecord()
        {
            ChromeStyleId = null;
            Engine = string.Empty;
            EngineCode = string.Empty;
            LoadDate = DateTime.UtcNow;
            Make = string.Empty;
            Model = string.Empty;
            ModelCode = string.Empty;
            ModelYear = null;
            OptionCodes = string.Empty;
            Transmission = string.Empty;
            TransmissionCode = string.Empty;
            Trim = string.Empty;
            VIN = string.Empty;
            InteriorColorCode = string.Empty;
            InteriorColorDescription = string.Empty;
            ExteriorColorCode = string.Empty;
            ExteriorColorDescription = string.Empty;
        }

        public VehicleRecord(IVehicleRecord record)
        {
            ChromeStyleId = record.ChromeStyleId;
            Engine = record.Engine;
            EngineCode = record.EngineCode;
            LoadDate = record.LoadDate;
            Make = record.Make;
            Model = record.Model;
            ModelCode = record.ModelCode;
            ModelYear = record.ModelYear;
            OptionCodes = record.OptionCodes;
            Transmission = record.Transmission;
            TransmissionCode = record.TransmissionCode;
            Trim = record.Trim;
            VIN = record.VIN;
            InteriorColorCode = record.InteriorColorCode;
            InteriorColorDescription = record.InteriorColorDescription;
            ExteriorColorCode = record.ExteriorColorCode;
            ExteriorColorDescription = record.ExteriorColorDescription;
        }

        public string VIN { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int? ModelYear { get; set; }
        public string Trim { get; set; }
        public string Engine { get; set; }
        public string Transmission { get; set; }
        public string ModelCode { get; set; }
        public string EngineCode { get; set; }
        public string TransmissionCode { get; set; }
        public string OptionCodes { get; set; }
        public int? ChromeStyleId { get; set; }
        public DateTime LoadDate { get; set; }
        public string InteriorColorCode { get; set; }
        public string InteriorColorDescription { get; set; }
        public string ExteriorColorCode { get; set; }
        public string ExteriorColorDescription { get; set; }

    }
}