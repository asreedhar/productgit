﻿namespace MAX.VDS.DataAccess
{
    public interface IVehicleMasterRecord : IVehicleRecord
    {
        int SourceId { get; set; }
        int AuditId { get; set; }
        int? LoadId { get; set; }
    }

    class VehicleMasterRecord : VehicleRecord, IVehicleMasterRecord
    {
        internal VehicleMasterRecord() : base()
        {
            
        }

        internal VehicleMasterRecord( IVehicleMasterRecord record)
            : base(record)
        {
            SourceId = record.SourceId;
            AuditId = record.AuditId;
            LoadId = record.LoadId;
        }

        public int SourceId { get; set; }
        public int AuditId { get; set; }
        public int? LoadId { get; set; }
    }
}