﻿/*
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace MAX.VDS.DataAccess
{
    internal interface IChromeRepository
    {
        IEnumerable<IOption> GetOptions(int countryCode, int styleId);
    }

    internal class ChromeRepository : IChromeRepository
    {
        public IEnumerable<IOption> GetOptions(int countryCode, int styleId)
        {
            string query = @"Select OptionCode, OptionDesc
                             From VehicleCatalog.Chrome.Options
                             Where CountryCode = @countryCode and StyleId = @styleId";

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Chrome"].ConnectionString))
            {
                sqlConnection.Open();
                var vdsRecords = sqlConnection.Query<Option>(query, new { CountryCode = countryCode, StyleId = styleId });
                sqlConnection.Close();
                return vdsRecords;
            }

        }
    }

    internal interface IOption
    {
        string OptionCode { get; set; }
        string OptionDesc { get; set; }
    }

    internal class Option : IOption
    {
        public string OptionCode { get; set; }
        public string OptionDesc { get; set; }
    }
}
*/
