﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace MAX.VDS.DataAccess
{
    public interface IVdsRepository
    {
        IEnumerable<IVehicleRecord> GetVehicle(string vin);
        IEnumerable<IVehicleMasterRecord> GetVehicleMaster(string vin);
    }

    internal class VdsRepository : IVdsRepository
    {
        private const string QueryTemplate = "SELECT * FROM VehicleCatalog.VDS.{0} WHERE Vin = @vin";

        public IEnumerable<IVehicleRecord> GetVehicle(string vin)
        {
            string query = string.Format(QueryTemplate, "Vehicle");
            return RunQuery<VehicleRecord>(query, vin);
        }

        public IEnumerable<IVehicleMasterRecord> GetVehicleMaster(string vin)
        {
            string query = string.Format(QueryTemplate, "VehicleMaster");
            return RunQuery<VehicleMasterRecord>(query, vin);
        }

        private IEnumerable<T> RunQuery<T>(string query, string vin)
        {
            int cmdTimeout;
            bool cmdTimeoutSetting = Int32.TryParse(ConfigurationManager.AppSettings["CommandTimeoutSeconds"], out cmdTimeout);

            if (!cmdTimeoutSetting)
            {
                cmdTimeout = 100;
            }

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Chrome"].ConnectionString))
            {
                sqlConnection.Open();
                var vdsRecords = sqlConnection.Query<T>(query,
                    new { vin = new DbString { Value = vin, IsFixedLength = true, Length = 17, IsAnsi = true } }, commandTimeout: cmdTimeout );
                sqlConnection.Close();
                return vdsRecords;
            }
        }
    }
}
