﻿namespace MAX.VDS.DataAccess
{
    internal enum VehicleSources
    {
        AutodataDatamax = 1,
        Dms = 2,
        Ads7 = 3,
        HondaNa = 4,
        ManufacturerBuildData = 5,
        HomeNetListingVehicle = 6
    }
}
