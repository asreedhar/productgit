using System.Collections.Generic;

namespace MAX.VDS
{

    // Needs to be public for JSON.Net to serialize/deserialize it correctly
    public class VdsAutoLoadConfigItem
    {
        // Can contain "*" for all makes
        // Can contain make names - e.g. "bmw"
        public string[] Makes { get; set; }

        // Treating the store ids as strings to allow "*"
        // Will normally contain business unit ids.
        public string[] Stores { get; set; }
    }

    public class VdsAutoLoadConfigParser
    {
        public string EncodeVdsConfigItems(IEnumerable<VdsAutoLoadConfigItem> items)
        {
            var encodedItems = new List<string>();
            foreach (var item in items)
            {
                var makes = item.Makes;
                var stores = item.Stores;

                var makesEncoded = string.Join(",", makes);
                var storesEncoded = string.Join(",", stores);

                var itemEncoded = makesEncoded + ";" + storesEncoded;
                encodedItems.Add(itemEncoded);
            }
            return string.Join("|", encodedItems);
            // make1,make2;store1,store2|make3,make4;store3
        }

        public IEnumerable<VdsAutoLoadConfigItem> ParseVdsConfigItemsText(string configText)
        {
            var parsedConfigItems = new List<VdsAutoLoadConfigItem>();
            // format: makes;stores|makes;stores;
            //         within makes and stores, comma delimited values
            // example: make1,make2;store1,store2,store3|*;store4|make3,make8;store27
            if (string.IsNullOrWhiteSpace(configText))
            {
                return parsedConfigItems;
            }
            var items = configText.Split('|');
            foreach (var item in items)
            {
                var makesAndStores = item.Split(';');
                var makes = makesAndStores[0];
                var stores = makesAndStores[1];

                var vdsConfigItem = new VdsAutoLoadConfigItem {Makes = makes.Split(','), Stores = stores.Split(',')};
                parsedConfigItems.Add( vdsConfigItem);
            }

            return parsedConfigItems;
        }
    }
}