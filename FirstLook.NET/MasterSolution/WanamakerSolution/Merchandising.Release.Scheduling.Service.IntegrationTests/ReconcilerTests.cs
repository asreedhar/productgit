﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Autofac;
using Dapper;
using Merchandising.Release.Scheduling.Service.CloudSearchReconciler;
using Merchandising.Release.Scheduling.Service.CloudSearchReconciler.CloudSearch;
using Merchandising.Release.Scheduling.Service.CloudSearchReconciler.Models;
using NUnit.Framework;
using ServiceStack.Text;

namespace Merchandising.Release.Scheduling.Service.IntegrationTests
{

    
    [TestFixture]
    public class ReconcilerTests
    {
        public class MissingReportRecord
        {
            public MissingReportRecord(Dealer dealer, Inventory inventory)
            {
                BusinessUnitId = dealer.BusinessUnitId;
                OwnerHandle = dealer.OwnerHandle;
                MaxForSmartphone = dealer.MaxForSmartphone;
                MaxForWebsite20 = dealer.MaxForWebsite20;
                MaxDigitalShowroom = dealer.MaxDigitalShowroom;
                VehiclesInShowroom = dealer.VehiclesInShowroom;

                InventoryId = inventory.InventoryId;
                Vin = inventory.Vin;
                StockNumber = inventory.StockNumber;
                InventoryType = inventory.InventoryType;
                InventoryReceivedDate = inventory.InventoryReceivedDate;
            }

            public int BusinessUnitId { get; set; }
            public Guid OwnerHandle { get; set; }
            public bool MaxForSmartphone { get; set; }
            public bool MaxForWebsite20 { get; set; }
            public bool MaxDigitalShowroom { get; set; }
            public Byte VehiclesInShowroom { get; set; }

            public int InventoryId { get; set; }
            public string Vin { get; set; }
            public string StockNumber { get; set; }
            public int InventoryType { get; set; }
            public DateTime InventoryReceivedDate { get; set; }
        }

        public class Dealer
        {
            public int BusinessUnitId { get; set; }
            public Guid OwnerHandle { get; set; }
            public bool MaxForSmartphone { get; set; }
            public bool MaxForWebsite20 { get; set; }
            public bool MaxDigitalShowroom { get; set; }
            public Byte VehiclesInShowroom { get; set; }

            public override string ToString()
            {
                return
                    string.Format(
                        "BusinessUnitId: {0}, OwnerHandle: {1}, MaxForSmartphone:{2}, MaxForWebsite20:{3}, MaxDigitalShowroom:{4}, VehiclesInShowroom: {5}",
                        BusinessUnitId, OwnerHandle, MaxForSmartphone, MaxForWebsite20, MaxDigitalShowroom, VehiclesInShowroom);
            }

        }


        public class Inventory
        {
            public int InventoryId { get; set; }
            public string Vin { get; set; }
            public string StockNumber { get; set; }
            public int InventoryType { get; set; }
            public DateTime InventoryReceivedDate { get; set; }

            public override string ToString()
            {
                return
                    string.Format(
                        "InventoryId: {0},  Vin: {1}, StockNumber: {2}, InventoryType: {3},  InventoryReceivedDate: {4}",
                        InventoryId, Vin, StockNumber, InventoryType, InventoryReceivedDate);
            }
        }

        private IContainer _container;


        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new CloudSearchReconciliationModule());
            _container = builder.Build();
        }



        [Test]
        public void GetAllCloudSearchVinsForDealer()
        {
            var oh = "08FE3EA2-FB93-E311-92F8-848F69DDF3CC";

            var repository = _container.Resolve<IVehicleCloudSearchDataRepository>();
            var csvehicles = repository.GetCloudSearchVehicles(new DealerInfo { OwnerHandle = new Guid(oh) }).ToList();

            Debug.WriteLine(string.Format("{1} CS vehicles for owner handle {0}:", oh, csvehicles.Count()));
            foreach (var vehicleSearchData in csvehicles)
            {
                Debug.WriteLine(string.Format("VIN: {0}", vehicleSearchData.Vin));
            }
        }

        [Test]
        public void GetAllActiveVinsForDealer()
        {
            var repository = _container.Resolve<IVehicleCloudSearchDataRepository>();
            var buid = 106754;
            var activeVehicles = repository.GetActiveVehicleData(buid).ToList();

            Debug.WriteLine(string.Format("{1} active vehicles for buid {0}:", buid, activeVehicles.Count()));
            foreach (var vehicleInfo in activeVehicles)
            {
                Debug.WriteLine(string.Format("VIN: {0}", vehicleInfo.Vin));
            }
        }


        [Test]
        public void TestGetDealers()
        {
            var dealers = GetDealers();

            foreach (var dealer in dealers)
            {
                Debug.WriteLine(dealer.ToString());
            }
        }


        [Test]
        public void TestGetInventoryMissingInCloudsearchForDealers()
        {
            var dealers = GetDealers();

            var missingReport = (
                from dealer in dealers 
                let missing = Missing(dealer) 
                    from inventory 
                    in missing 
                select new MissingReportRecord(dealer, inventory)).ToList();

            Debug.WriteLine("Total Missing Count: {0}", missingReport.Count);

            var output = missingReport.ToCsv();

            //Debug.WriteLine(output);


            var filename = string.Format("MissingReport-{0:yyyy-MM-dd_hh-mm-ss-tt}.csv", DateTime.Now);

            using (var file = File.CreateText(filename))
            {
                file.Write(output);
                file.Close();
            }

            Debug.WriteLine(string.Format("Created outputfile: {0}", Path.GetFullPath(filename)));

        }

        [Test]
        public void GetVinsActiveButNotInCloudSearch()
        {
            var dealer =
                new Dealer { OwnerHandle = new Guid("60032F47-0CEE-DD11-A9E5-0022198DB286"), BusinessUnitId = 101661 };


            var missing = Missing(dealer).ToList();

            Debug.WriteLine(string.Format("{1} missing vehicles for buid {0}:", dealer.BusinessUnitId, missing.Count()));
            Debug.WriteLine(string.Format("Time:{0}", DateTime.Now));
            
            foreach (var invItem in missing)
            {
                Debug.WriteLine(invItem);
            }
        }


        private IEnumerable<Inventory> Missing(Dealer dealer)
        {
            var repository = _container.Resolve<IVehicleCloudSearchDataRepository>();
            var activeInventory = GetActiveInventory(dealer.BusinessUnitId);
            var csvehicles = repository.GetCloudSearchVehicles(new DealerInfo{BusinessUnitId = dealer.BusinessUnitId, OwnerHandle = dealer.OwnerHandle});

            // select all the inventory that are not in the cloudsearch list
            var missing = ( 
                from invItem
                    in activeInventory
                where csvehicles.All(x => x.Vin != invItem.Vin)
                select invItem)
                .ToList();

            return missing;
        }

        private static IEnumerable<Dealer> GetDealers()
        {

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                const string query = @"
                SELECT msm.BusinessUnitId, o.Handle AS OwnerHandle, msm.maxForSmartphone MaxForSmartphone, msm.MAXForWebsite20 MaxForWebsite20, msm.MaxDigitalShowroom, msm.vehiclesInShowroom VehiclesInShowroom
                FROM Merchandising.settings.Merchandising msm
                JOIN Market.Pricing.Owner o ON o.OwnerEntityID = msm.businessUnitID AND o.OwnerTypeID = 1
                WHERE 
                ( 
	                maxForSmartPhone = 1 
	                OR MAXForWebsite20 = 1
	                OR MaxDigitalShowroom = 1 
                ) 
                OR 
                (	
	                vehiclesInShowroom IN (0,1)
                )

                    ";

                var dealers = sqlConnection.Query<Dealer>(query).ToList();
                sqlConnection.Close();
                return dealers;

            } 
        }

        public IEnumerable<Inventory> GetActiveInventory(int businessUnitId)
        {
            const string sql =
            @"
            SELECT ia.InventoryId, ia.Vin, ia.StockNumber, ia.InventoryType, ia.InventoryReceivedDate
            FROM FLDW.dbo.InventoryActive ia
            WHERE ia.BusinessUnitId = @BusinessUnitId
            AND NOT EXISTS (
	            SELECT *
	            FROM Merchandising.builder.OptionsConfiguration oc
	            WHERE oc.businessUnitID = ia.BusinessUnitID
	            AND oc.inventoryId = ia.InventoryID
	            AND oc.doNotPostFlag = 1
            )
            ";

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FLDW"].ConnectionString))
            {
                conn.Open();
                return conn.Query<Inventory>(sql, new { BusinessUnitId = businessUnitId });
            }
        }

    }
}
