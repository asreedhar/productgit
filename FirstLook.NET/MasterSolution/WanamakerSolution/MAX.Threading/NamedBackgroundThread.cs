﻿using System;
using System.Threading;

namespace MAX.Threading
{
    public static class NamedBackgroundThread
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Thread Start(string name, Action threadBody)
        {
            var thread = new Thread(() => threadBody())
            {
                IsBackground = true,
                Name = name
            };
            thread.Start();
            Log.InfoFormat("Thread '{0}' started.", thread.Name);
            return thread;
        }
    }
}
