﻿using System;

namespace MAX.Threading
{
    public interface IRunWithCancellation
    {
        void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages);
    }
}
