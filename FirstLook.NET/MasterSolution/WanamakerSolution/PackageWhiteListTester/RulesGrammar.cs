﻿using Irony.Parsing;

namespace PackageWhiteListTester
{
    [Language("Rules", "1.0", "White list rules")]
    public class RulesGrammar : Grammar
    {
        public RulesGrammar()
            : base(false)
        {
            var lineComment = new CommentTerminal("Line Comment", "#", "\r", "\n");
            NonGrammarTerminals.Add(lineComment);

            var stringLiteral = CreateStringLiteral();
            var identifier = new IdentifierTerminal("Identifier");

            var When = ToTerm("WHEN");
            var Accept = ToTerm("ACCEPT");
            var Reject = ToTerm("REJECT");
            var Begin = ToTerm("BEGIN");
            var End = ToTerm("END");
            var Then = ToTerm("THEN");
            var Not = ToTerm("NOT");
            var And = ToTerm("AND");
            var Or = ToTerm("OR");
            var Equals = ToTerm("EQUALS");
            var Contains = ToTerm("CONTAINS");
            var Matches = ToTerm("MATCHES");

            var stmtList = new NonTerminal("stmtList");
            var stmt = new NonTerminal("stmt");
            var whenStmt = new NonTerminal("whenStmt");
            var acceptStmt = new NonTerminal("acceptStmt");
            var rejectStmt = new NonTerminal("rejectStmt");
            var block = new NonTerminal("block");
            var expression = new NonTerminal("expression");
            var binExpr = new NonTerminal("binExpr");
            var binOp = new NonTerminal("binOp");
            var unaExpr = new NonTerminal("unaExpr");
            var unaOp = new NonTerminal("unaOp");

            // Grammar
            Root = stmtList;
            stmtList.Rule = MakeStarRule(stmtList, stmt);
            stmt.Rule = whenStmt | acceptStmt | rejectStmt | block;

            whenStmt.Rule = When + expression + Then + stmt;
            acceptStmt.Rule = Accept;
            rejectStmt.Rule = Reject;
            block.Rule = Begin + stmtList + End;

            expression.Rule = unaExpr | binExpr | "(" + expression + ")" | identifier | stringLiteral;
            unaExpr.Rule = unaOp + expression;
            unaOp.Rule = Not;
            binExpr.Rule = expression + binOp + expression;
            binOp.Rule = And | Or | Matches | Contains | Equals;

            RegisterOperators(1, Or);
            RegisterOperators(2, And);
            RegisterOperators(3, Not);
            RegisterOperators(4, Matches, Contains, Equals);

            MarkPunctuation("(", ")");
            MarkPunctuation(Begin, End);
            RegisterBracePair("(", ")");
            Begin.SetFlag(TermFlags.IsOpenBrace);
            Begin.IsPairFor = End;
            End.SetFlag(TermFlags.IsCloseBrace);
            End.IsPairFor = Begin;
            
            MarkTransient(stmt, expression, binOp, unaOp, block);
        }
        
        private static StringLiteral CreateStringLiteral()
        {
            var literal = new StringLiteral("String");
            literal.AddStartEnd("\"", StringOptions.AllowsDoubledQuote | StringOptions.NoEscapes);
            literal.AddStartEnd("\'", StringOptions.AllowsDoubledQuote | StringOptions.NoEscapes);
            return literal;
        }
    }
}