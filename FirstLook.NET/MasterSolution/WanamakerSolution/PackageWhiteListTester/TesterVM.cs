﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Irony.Parsing;

namespace PackageWhiteListTester
{
    public class TesterVM : INotifyPropertyChanged
    {
        #region MVVM Infrastructure
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        private class CommandHandler : ICommand
        {
            private readonly Action<object> action;

            public CommandHandler(Action handler)
            {
                action = o => handler();
            }

            public void Execute(object parameter)
            {
                action(parameter);
            }

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;
        }
        #endregion

        private string rules;
        public string Rules
        {
            get { return rules; }
            set
            {
                if (Equals(rules, value)) return;
                rules = value;
                OnPropertyChanged("Rules");
            }
        }
       
        public ICommand TestRules
        {
            get { return new CommandHandler(OnTestRules); }
        }
        private void OnTestRules()
        {
            var grammar = new RulesGrammar();
            var parser = new Parser(grammar);
            var rootNode = parser.Parse(Rules);

            if(rootNode.HasErrors())
            {
                MessageBox.Show("Error: " +
                                string.Join("; ", rootNode.ParserMessages.Select(m => m.ToString())));
                return;
            }


        }
    }
}