﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.BionicReports.Entities;
using MAX.Entities.Reports.Bionic;

namespace MAX.BionicReports.Utility
{
    public static class EmailTemplateTransform
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly Regex _tagRegex = new Regex(@"\{\w*\}");

        public static string ApplyGroupBionic(string inlinedEmailHtml, string trackingSnip, GroupBionicGroupData dtoV1)
        {
            string result = inlinedEmailHtml;
            // Tracking snip //
            ReplaceTag(ref result, "{trackingBlock}", trackingSnip);

            // Tracking //
            ReplaceTag(ref result, "{emailAddress}", dtoV1.EmailAddressTo); // this must come after the tracking snip is in place,
                                                                        // otherwise we won't replace the email addr placeholders

            // Strings //
            ReplaceTag(ref result, "{monthYear}", dtoV1.ReportDateRange.StartDate.ToString("MMMM") + " " + dtoV1.ReportDateRange.StartDate.Year);
            ReplaceTag(ref result, "{groupName}", dtoV1.BusinessUnit.ToUpper());
            ReplaceTag(ref result, "{clickToSeeInMaxUrl}", dtoV1.BusinessUnitHome);
            ReplaceTag(ref result, "{modifySettingsUrl}", dtoV1.SettingsHome);

            const string startTag = "<!--{DealershipRepeatStart}-->";
            const string endTag = "<!--{DealershipRepeatEnd}-->";

            var originalRepeaterBlock = CopyBlock(result, startTag, endTag);

            var trimmedBlock = String.Copy(originalRepeaterBlock);
            RemoveTag(ref trimmedBlock, startTag);
            RemoveTag(ref trimmedBlock, endTag);

            var builtDealershipBlocks = "";
            Int32 count = 0;

            foreach (var dealership in dtoV1.Dealers)
            {
                count++;
                var dealershipBlock = String.Copy(trimmedBlock);

                
                // dealership block replacements:
                //
                ReplaceTag(ref dealershipBlock, "{count}", count.ToString());
                ReplaceTag(ref dealershipBlock, "{dealership}", dealership.BusinessUnit);
                ReplaceTag(ref dealershipBlock, "{barPercent}",
                    Math.Round(dealership.AverageDaysToCompleteAd /
                               (dtoV1.MaxAverageDaysToCompleteAd < 1 ? 1 : dtoV1.MaxAverageDaysToCompleteAd) * 100)
                        .ToString("N0")); // Math.Round (only supports 0-100)


                // all of the following can only reliably contain 1-2 characters,
                // make sure to round decimals:
                //
                // Completed Ad Online
                //
                ReplaceTag(ref dealershipBlock, "{completeAdAvg}", dealership.AverageDaysToCompleteAd.ToString("N0"));
                ReplaceTag(ref dealershipBlock, "{completeAdMin}", dealership.MinDaysComplete.ToString("N0"));
                ReplaceTag(ref dealershipBlock, "{completeAdMax}", dealership.MaxDaysComplete.ToString("N0"));

                // Initial Photo Online
                //
                ReplaceTag(ref dealershipBlock, "{initialPhotoAvg}", dealership.AverageDaysToPhotoOnline.ToString("N0"));
                ReplaceTag(ref dealershipBlock, "{initialPhotoMin}", dealership.MinDaysPhoto.ToString("N0"));
                ReplaceTag(ref dealershipBlock, "{initialPhotoMax}", dealership.MaxDaysPhoto.ToString("N0"));

                builtDealershipBlocks += dealershipBlock;
            }

            result = result.Replace(originalRepeaterBlock, builtDealershipBlocks);

            CheckForUnpopulatedTags(result);
            return result;
        }

        public static string ApplyBionic(string inlinedEmailHtml, string trackingSnip, BionicReportEmail dtoV1)
        {
            string result = inlinedEmailHtml;

            // Tracking snip //
            ReplaceTag(ref result, "{trackingBlock}", trackingSnip);

            // Tracking //
            ReplaceTag(ref result, "{emailAddress}", dtoV1.EmailAddressTo); // this must come after the tracking snip is in place,
                                                                            // otherwise we won't replace the email addr placeholders

            // Strings //
            ReplaceTag(ref result, "{monthYear}", dtoV1.ReportDateRange.StartDate.ToString("MMMM") + " " + dtoV1.ReportDateRange.StartDate.Year);
            ReplaceTag(ref result, "{customer}", dtoV1.DealerName);
            ReplaceTag(ref result, "{name}", dtoV1.DMSName);
            ReplaceTag(ref result, "{title}", "Digital Marketing Specialist");

            ReplaceTag(ref result, "{onlineDays}", dtoV1.AverageDaysToCompleteAd.ToString("N0"));
            ReplaceTag(ref result, "{onlineTargetDays}", dtoV1.TimeToMarketGoalDays.ToString("N0"));
            ReplaceTag(ref result, "{caption}", String.Empty);

            ReplaceTag(ref result, "{ttmDaysWithPhoto}", dtoV1.AverageDaysToPhotoOnline.ToString("N0"));
            ReplaceTag(ref result, "{ttmDaysWithPhotoPercent}", "3.7"); // Hard coded per Chad Sosna

            ReplaceTag(ref result, "{noPhotoCount}", dtoV1.AverageNoPhotos.ToString("N0"));
            ReplaceTag(ref result, "{noPhotoPercent}", (Math.Round(dtoV1.AverageNoPhotos/dtoV1.AverageAllInventory, 2)*100).ToString("N0"));

            ReplaceTag(ref result, "{noPriceVehicleCount}", dtoV1.AverageNoPrice.ToString("N0"));
            ReplaceTag(ref result, "{noPriceVehiclePercent}", (Math.Round(dtoV1.AverageNoPrice/dtoV1.AverageAllInventory, 2)*100).ToString("N0"));

            ReplaceTag(ref result, "{notOnlineVehicleCount}", dtoV1.AverageNotOnline.ToString("N0"));
            ReplaceTag(ref result, "{notOnlineVehiclePercent}", (Math.Round(dtoV1.AverageNotOnline/dtoV1.AverageAllInventory, 2)*100).ToString("N0"));

            // Images //
            ReplaceTag(ref result, "{onlineDaysChartUrl}", dtoV1.ReportImages[ReportImageType.TimeToMarketComplete]);
            ReplaceTag(ref result, "{ttmDaysWithPhotoChartUrl}", dtoV1.ReportImages[ReportImageType.TimeToMarketFirstPhoto]);
            ReplaceTag(ref result, "{noPhotoChartUrl}", dtoV1.ReportImages[ReportImageType.VehiclesWithNoPhotos]);
            ReplaceTag(ref result, "{noPriceVehicleChartUrl}", dtoV1.ReportImages[ReportImageType.VehiclesWithNoPrice]);
            ReplaceTag(ref result, "{notOnlineVehicleChartUrl}", dtoV1.ReportImages[ReportImageType.VehiclesNotOnline]);

            // Links //
            ReplaceTag(ref result, "{clickToSeeInMaxUrl}", dtoV1.BusinessUnitHome);
            ReplaceTag(ref result, "{modifySettingsUrl}", dtoV1.SettingsHome);

            CheckForUnpopulatedTags(result);
            return result;
        }

        public static void CheckForUnpopulatedTags(string html)
        {
            var remainingTags = new List<string>();
            foreach (Match match in _tagRegex.Matches(html))
            {
                if (!remainingTags.Contains(match.Value))
                {
                    remainingTags.Add(match.Value);
                }
            }
            if (remainingTags.Count > 0)
            {

                throw new Exception("Not all tags have been replaced: (" + String.Join(", ", remainingTags) + ")");
            }
        }

        public static void ReplaceTag(ref string html, string tag, string value)
        {
            if (html.Contains(tag)) {
                html = html.Replace(tag, value);
            } else {
                throw new Exception(String.Format("No instances of tag ({0}) found in provided email html.",tag));
            }
        }

        public static void RemoveTag(ref string html, string tag)
        {
            ReplaceTag(ref html, tag, String.Empty);
        }

        /// <summary>
        /// Copies a block of html including start and end indicators.
        /// </summary>
        /// <param name="html">html to have block copied from</param>
        /// <param name="startBlockTag">block start marker</param>
        /// <param name="endBlockTag">block end marker</param>
        /// <returns>block of html including start and end markers</returns>
        public static string CopyBlock(string html, string startBlockTag, string endBlockTag)
        {
            var blockStart = html.IndexOf(startBlockTag, StringComparison.InvariantCulture);
            if (blockStart == -1)
            {
                var issue = String.Format("CopyBlock was unable to find the startBlockTag '{0}'.", startBlockTag);
                Log.Error(issue);
                throw new Exception(issue);
            }
            Log.Trace(String.Format("CopyBlock found startBlockTag '{0}'", startBlockTag));

            var blockEnd = html.IndexOf(endBlockTag, StringComparison.InvariantCulture);
            if (blockEnd == -1)
            {
                var issue = String.Format("CopyBlock was unable to find the endBlockTag '{0}'.", endBlockTag);
                Log.Error(issue);
                throw new Exception(issue);
            }
            Log.Trace(String.Format("CopyBlock found endBlockTag '{0}'", endBlockTag));

            var block = html.Substring(blockStart, blockEnd - blockStart + endBlockTag.Length);
            if (String.IsNullOrWhiteSpace(block))
            {
                var issue = "CopyBlock block was null or whitespace.";
                Log.Error(issue);
                throw new Exception(issue);
            }
            Log.Trace("Found CopyBlock, removing tags from block.");
            return block;
        }

        public static void ReplaceBlock(ref string html, string startBlockTag, string endBlockTag, string newBlock)
        {
            var originalBlock = CopyBlock(html, startBlockTag, endBlockTag);
            html = html.Replace(originalBlock, newBlock);
        }
    }
}
