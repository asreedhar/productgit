﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Serialization;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.BionicReports.Entities;

namespace MAX.BionicReports.Utility
{
    /// <summary>
    /// Utility for getting email tracking campaigns from litmus or a fileStore and saving any new ones to the fileStore
    /// </summary>
    public class LitmusCurrentMonthCampaign
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly static Uri _litmusApiReportsUri = new Uri("https://maxdigital2.litmus.com/reports.xml");

        /// <summary>
        /// Enum for how we will name the tracking campaign
        /// </summary>
        public enum Type
        {
            Bionic,
            GroupBionic
        }

        // litmus basic auth uname/pass
        private string _username;
        private string _password;

        // Name of litmus tracking campaign
        private string _trackingName;

        // Path to fileStore load/save object
        private string _trackingFileWithPath;

        private Type _type;
        private IFileStorage _fileStore;

        /// <summary>
        /// Was the tracking code successfully created and persisted?
        /// </summary>
        public bool Success
        {
            get { return (!String.IsNullOrWhiteSpace(TrackingCode)); }
            set
            {
                if (!value)
                {
                    TrackingCode = null;
                }
            }
        }

        /// <summary>
        /// Null or created/loaded tracking code.
        /// </summary>
        public string TrackingCode { get; private set; }

        /// <summary>
        /// Service abstraction layer class to manage persisting, creating and retrieving email tracking codes.
        /// Instantiation will block for a duration while making calls to fileStore and litmus' API.
        /// </summary>
        /// <param name="type">Type of tracking code to generate</param>
        /// <param name="fileStore">Persistence layer for created campaign tracking codes</param>
        /// <param name="litmusUsername">Litmus.com username (full email address)</param>
        /// <param name="litmusPassword">Litmus.com password</param>
        public LitmusCurrentMonthCampaign(Type type, IFileStorage fileStore, string litmusUsername, string litmusPassword)
        {
            _username = litmusUsername;
            _password = litmusPassword;
            _fileStore = fileStore;
            _type = type;

            var now = DateTime.UtcNow;
            //                                2015_01_Bionic
            _trackingName = String.Format("{0}_{1}_{2}", now.Year, now.Month, _type);
            _trackingFileWithPath = String.Format("Tracking/{0}.xml", _trackingName);


            Log.Trace(string.Format("See if tracking snip '{0}' already exists in fileStore.", _trackingFileWithPath));
            TrackingCode = TryGetCodeFromFileStore();

            if (TrackingCode == null)
            {
                Log.Trace("Unable to find tracking snip in the fileStore. Getting/making new tracking snip with litmus.");
                TrackingCode = GetOrCreateMonthCode();

                if (TrackingCode != null)
                {
                    // Save it to fileStore
                    //
                    try
                    {
                        SaveNewLitmusCampaign(TrackingCode);
                    }
                    catch (Exception ex)
                    {
                        Log.Fatal("Unable to save created tracking code to fileStore.");
                        Success = false;
                    }
                }
            }
        }

        #region fileStore methods
        /// <summary>
        /// Tries to get this month's tracking code from the fileStore
        /// </summary>
        /// <returns></returns>
        private string TryGetCodeFromFileStore()
        {

            string trackingSource = null;            
            try
            {
                bool trackingSnipExists;
                var trackingFileStream = _fileStore.Exists(_trackingFileWithPath, out trackingSnipExists);
                if (!trackingSnipExists)
                {
                    return trackingSource;
                }

                var trackingFileStreamReader = new StreamReader(trackingFileStream);
                trackingSource = trackingFileStreamReader.ReadToEnd();

                if (string.IsNullOrWhiteSpace(trackingSource))
                {
                    throw new Exception("Email Tracking Code generator caught an exception while trying to find an existing tracking code from the fileStore.");
                }
                Log.Trace(string.Format("Found matching tracking snip in fileStore. Contents are: {0}", trackingSource));
                
            }
            catch (Exception ex)
            {
                // Amazon's S3 library doesn't throw an explicit exception type when unable to read the file
                //
                Log.Info("Unable to read tracking snip from fileStore.", ex);
            }

            return trackingSource;
        }

        /// <summary>
        /// Save _trackingSource to S3
        /// </summary>
        private void SaveNewLitmusCampaign(string trackingSource)
        {
            if (String.IsNullOrWhiteSpace(trackingSource)) throw new ArgumentException("Tracking source code must not be null.");

            try
            {
                // Store tracking campaign to fileStore
                //
                Log.Trace("Storing tracking campaign code to filestore.");
                var trackingMemoryStream = new MemoryStream();
                var trackingStreamWriter = new StreamWriter(trackingMemoryStream);
                trackingStreamWriter.Write(trackingSource);
                trackingStreamWriter.Flush();
                trackingMemoryStream.Position = 0;

                _fileStore.Write(_trackingFileWithPath, trackingMemoryStream);
            }
            catch (Exception ex)
            {
                Log.Error("Email Tracking Code generator unable to save litmus campaign code to fileStore.", ex);
                throw;
            }
        }
        #endregion

        #region litmus abstraction layer

        //
        // TODO: turn this into a litmus client
        //


        /// <summary>
        /// Attempts to get or create a current month tracking code with litmus
        /// </summary>
        /// <param name="fileStore">Persistant storage for the tracking code to be referenced</param>
        /// <param name="type">Type of tracking code to be created</param>
        /// <returns>Tracking code or null if ApplicationException was encountered. Tracking codes have a string identifier of "[UNIQUE]" where the email recipient email address should be replaced.</returns>
        private string GetOrCreateMonthCode()
        {
            string trackingCode = null;

            if (String.IsNullOrWhiteSpace(_username) || String.IsNullOrWhiteSpace(_password))
            {
                throw new ApplicationException("Litmus username or password not provided in config, aborting due to lack of persisted tracking code and inability to validate with litmus.");
            }

            // Looks like our fileStore didn't work out so well...
            // Time to make a new code
            //
            try
            {
                // See if litmus already has a campaign created for this month (In case we were unable to record a created litmus campaign to the fileStore for some reason)
                //
                trackingCode = FindExistingLitmusMonthCampaign();

                if (trackingCode == null)
                {
                    // create new campaign
                    //
                    trackingCode = CreateLitmusMonthCampaign();
                }
                
            }
            catch (Exception ex)
            {
                // Looks like we need to try again later, everything is failing
                //
                Log.Fatal("Email Tracking Code generator had issues getting, creating or saving new tracking code.", ex);
            }

            return trackingCode;
        }

        /// <summary>
        /// Lets try to get an existing campaign from litmus for this month
        /// </summary>
        /// <returns>The tracking code if found, null if not</returns>
        private string FindExistingLitmusMonthCampaign()
        {
            string trackingSource = null;

            // Try to grab the campaign from litmus if it already exists
            //
            Log.Trace("Getting most recent tracking campaigns from litmus.");
            var recentCampaigns = ListRecentLitmusCampaigns();

            if (recentCampaigns != null && recentCampaigns.Entries != null)
            {
                var existingCampaign = recentCampaigns.Entries.FirstOrDefault(campaign => _trackingName.Equals(campaign.Name));
                if (existingCampaign != null)
                {
                    Log.Trace(String.Format("Found existing campaign from litmus: {0}", existingCampaign.TrackingCode));
                    trackingSource = existingCampaign.TrackingCode;
                }
                else
                {
                    Log.Trace(String.Format("No matching campaign by name '{0}' found in recent litmus campaign report xml.", _trackingName));
                }
            }
            return trackingSource;
        }


        /// <summary>
        /// Gets the most recent existing 100 reports/campaigns from litmus
        /// </summary>
        /// <returns>null or LitmusReports object</returns>
        private LitmusReports ListRecentLitmusCampaigns()
        {
            LitmusReports reports = null;

            var webRequest = WebRequest.Create(_litmusApiReportsUri);
            webRequest.Credentials = new NetworkCredential(_username, _password);
            Log.Trace("Requesting report history from litmus.");
            using (var webResponse = webRequest.GetResponse())
            {
                using (var responseStream = webResponse.GetResponseStream())
                {
                    var reportsXml = new StreamReader(responseStream).ReadToEnd();
                    Log.Trace(String.Format("Reading data from litmus. Response Length: {0}", reportsXml.Length));

                    var reportsStream = new MemoryStream(Encoding.UTF8.GetBytes(reportsXml));
                    reportsStream.Position = 0;

                    
                    try
                    {
                        reports = DeserializeXmlStreamToObject<LitmusReports>(reportsStream);
                    }
                    catch (InvalidOperationException xmlEx)
                    {
                        reportsStream.Position = 0;
                        Log.Error(String.Format("Response from litmus' report listing API is returning unexpected response. ({0}) : {1}", xmlEx, new StreamReader(reportsStream).ReadToEnd()));
                    }

                    Log.Trace(String.Format("Deserialized reports summary. Found {0} entries.",
                        reports.Entries.Length));
                }
            }

            return reports;
        }

        /// <summary>
        /// Lets try to create a new litmus tracking campaign for this month
        /// </summary>
        /// <returns>The tracking code if found, null if not</returns>
        private string CreateLitmusMonthCampaign()
        {
            string trackingSource = null;

            var webRequest = WebRequest.Create(_litmusApiReportsUri);
            webRequest.Method = "POST"; // Where is that POST enum located again?
            webRequest.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(_username + ":" + _password));

            var postData = Encoding.UTF8.GetBytes(String.Format("<report><name>{0}</name></report>", _trackingName));
            webRequest.ContentLength = postData.Length;
            webRequest.ContentType = "application/xml";

            Log.Trace("POSTing new campaign to litmus.");
            using (var requestStream = webRequest.GetRequestStream())
            {
                requestStream.Write(postData, 0, postData.Length);
            }
            using (var webResponse = webRequest.GetResponse())
            {
                using (var responseStream = webResponse.GetResponseStream())
                {
                    var reportXml = new StreamReader(responseStream).ReadToEnd();
                    Log.Trace(String.Format("Reading data from litmus. Response Length: {0}", reportXml.Length));

                    var reportStream = new MemoryStream(Encoding.UTF8.GetBytes(reportXml));
                    reportStream.Position = 0;
                    LitmusReport report = null;
                    try
                    {
                        report = DeserializeXmlStreamToObject<LitmusReport>(reportStream);
                    }
                    catch (InvalidOperationException xmlEx)
                    {
                        reportStream.Position = 0;
                        Log.Error(
                            String.Format(
                                "Response from litmus' report creation API is returning unexpected response. ({0}) : {1}",
                                xmlEx, new StreamReader(reportStream).ReadToEnd()));
                    }
                    if (report != null)
                    {
                        Log.Trace(String.Format("Deserialized report. New code name {0} : {1}", report.Name, report.TrackingCode));
                        trackingSource = report.TrackingCode;
                    }
                }
            }
            return trackingSource;
        }

        public static T DeserializeXmlStreamToObject<T>(Stream xmlStream)
        {
            var deserializer = new XmlSerializer(typeof(T));

            var o = (T)deserializer.Deserialize(xmlStream);

            if (o == null)
                Log.Info("XmlStream deserializer created a null object.");
            return o;
        }
        #endregion
    }
}
