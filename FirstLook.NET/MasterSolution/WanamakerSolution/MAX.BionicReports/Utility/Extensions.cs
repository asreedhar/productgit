﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MAX.BionicReports.Utility
{
    public static class Extensions
    {
        private enum Bound { Left, Right, Not };

        /// <summary>
        /// Gets a list of orphans with non-null values. Orphans are bounded by null value points and the start/end of the list ordered by the Keys.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IList<KeyValuePair<DateTime, int?>> Orphans(this IList<KeyValuePair<DateTime, int?>> list)
        {
            var pointsWithValue = list.Where(p => p.Value != null).ToList();
            if (pointsWithValue.Count == 0 || pointsWithValue.Count == 1)
                return pointsWithValue;

            var orphans = new List<KeyValuePair<DateTime, int?>>();

            var rightBound = list.Count-1; // value should always be 1 to N
            foreach (var point in pointsWithValue) // 2 or more points
            {
                var index = list.IndexOf(point);
                switch (index.Bounded(rightBound))
                {
                    case Bound.Left: // always index = 0
                        if (list[index + 1].Value == null)
                            orphans.Add(point);
                        break;
                    case Bound.Right: // always index > 0
                        if (list[index - 1].Value == null)
                            orphans.Add(point);
                        break;
                    case Bound.Not: // always index > 0 && index < N-1
                        if (list[index - 1].Value == null && list[index + 1].Value == null)
                            orphans.Add(point);
                        break;
                }

            }
            return orphans;
        }


        public static IList<KeyValuePair<DateTime, float?>> InterpolateAndExtend(this IList<KeyValuePair<DateTime, int?>> list)
        {
            list = list.OrderBy(item => item.Key).ToList();
            
            var leftmostWithValue = list.FirstOrDefault(i => i.Value != null);
            var leftmostWithValueIndex = list.IndexOf(leftmostWithValue);
            var rightmostWithValue = list.LastOrDefault(i => i.Value != null);
            var rightmostWithValueIndex = list.IndexOf(rightmostWithValue);

            var interpolatedList = new List<KeyValuePair<DateTime, float?>>();

            if (leftmostWithValue.IsDefault() || rightmostWithValue.IsDefault())
            {
                return list.Select(x => new KeyValuePair<DateTime, float?>(x.Key, x.Value)).ToList(); // no values ;_;
            }


            // interpolate
            //
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Value == null)
                {
                    if (i < leftmostWithValueIndex) // left extend
                    {
                        interpolatedList.Add(new KeyValuePair<DateTime, float?>(list[i].Key, leftmostWithValue.Value));
                    }
                    else if (leftmostWithValueIndex < i && i < rightmostWithValueIndex) // inner points / interpolation
                    {
                        var lastWithValue = list.LastOrDefault(p => p.Value != null && list.IndexOf(p) < i);
                        var nextWithValue = list.FirstOrDefault(p => p.Value != null && list.IndexOf(p) > i);
                        if (lastWithValue.IsDefault() || nextWithValue.IsDefault())
                        {
                            
                        }
                        var span = list.IndexOf(nextWithValue) - list.IndexOf(lastWithValue);
                        if (span > 0)
                        {
                            var leftVal = (float) lastWithValue.Value;
                            var rightVal = (float) nextWithValue.Value;
                            var interpolated = Lerp(leftVal, rightVal, (float)(i - list.IndexOf(lastWithValue)) / span);
                            interpolatedList.Add(new KeyValuePair<DateTime, float?>(list[i].Key, interpolated));
                        }
                    }
                    else if (i > rightmostWithValueIndex) // right extend
                    {
                        interpolatedList.Add(new KeyValuePair<DateTime, float?>(list[i].Key, rightmostWithValue.Value));
                    }
                }
                else
                {
                    interpolatedList.Add(new KeyValuePair<DateTime, float?>(list[i].Key, list[i].Value));
                }
            }
            
            return interpolatedList;
        }

        

        public static bool IsDefault<T>(this T value) where T : struct
        {
            bool isDefault = value.Equals(default(T));

            return isDefault;
        }

        /// <summary>
        /// Get a list of segments of 2 or more non-null value points in a row
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<List<KeyValuePair<DateTime, int?>>> ContiguousSegments(
            this IList<KeyValuePair<DateTime, int?>> list)
        {
            var contiguousSetArray = new List<
                List<KeyValuePair<DateTime, int?>>
                >();
            for (int outer = 0; outer < list.Count; outer++)
            {
                if (list[outer].Value != null && outer < list.Count - 1) // non-null point AND at not the last point in the list
                {
                    var segment = new List<KeyValuePair<DateTime, int?>>();
                    segment.Add(list[outer]);

                    for (int inner = outer+1; inner < list.Count; inner++)
                    {
                        if (list[inner].Value == null)
                            break;
                        
                        segment.Add(list[inner]);
                        
                    }
                    if (segment.Count > 1) // 2 or more points
                        contiguousSetArray.Add(segment);
                }
            }

            return contiguousSetArray;
        }

        /// <summary>
        /// Get bounds enum
        /// </summary>
        /// <param name="index">0 based array index</param>
        /// <param name="rightBound">last possible index value for the array</param>
        /// <param name="leftBound">0</param>
        /// <returns>Bound enum value</returns>
        private static Bound Bounded(this int index, int rightBound, int leftBound = 0)
        {
            if (leftBound == index)
                return Bound.Left;
            if (rightBound == index)
                return Bound.Right;
            return Bound.Not;
        }

        /// <summary>
        /// In mathematics, linear interpolation is a method of curve fitting using linear polynomials.
        /// </summary>
        private static float Lerp(float v0, float v1, float t)
        {
            return v0 + t * (v1 - v0);
        }
    }
}
