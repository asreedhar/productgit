﻿using Autofac;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using MAX.BionicReports.Images;
using MAX.BionicReports.Queues;
using MAX.BionicReports.ThreadStarter;
using MAX.BionicReports.Topics;

namespace MAX.BionicReports
{
	public class BionicReportsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<BionicReportQueueFactory>().As<IBionicReportQueueFactory>();
			builder.RegisterType<BionicThreadStarter>().As<IBionicThreadStarter>();
			builder.RegisterType<BionicReportsBusFactory>().As<IBionicReportsBusFactory>();
		    builder.RegisterType<MaxDealersRepository>().As<IMaxDealersRepository>();
			builder.RegisterType<ImageCreator>().As<IImageCreator>();

            base.Load(builder);
		}
	}
}