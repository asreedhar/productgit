using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;
using MAX.Entities;
using MAX.Entities.Reports.Bionic;

namespace MAX.BionicReports.Images
{
	public interface IImageCreator
	{
		MemoryStream CreateTtmChartImage(IEnumerable<KeyValuePair<DateTime, int?>> list, ReportImageType reportImageType, List<Tuple<ChartDashStyle, double, string>> stripLinesCollection);
		MemoryStream CreateBucketChartImage(IEnumerable<BucketValue> bucketValueCollection, VehicleType vehicleType);
	}
}