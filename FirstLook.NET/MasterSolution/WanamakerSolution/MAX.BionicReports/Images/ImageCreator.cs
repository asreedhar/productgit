﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using FirstLook.Merchandising.DomainModel.Annotations;
using MAX.BionicReports.Utility;
using MAX.Entities;
using MAX.Entities.Reports.Bionic;

namespace MAX.BionicReports.Images
{
	internal class ImageCreator : IImageCreator
	{
		private readonly Size _smallChartSize = new Size(280, 180);
		private readonly Size _largeChartSize = new Size(580, 180);

		//private readonly Color _blue = Color.FromArgb(24, 66, 147); // Dark blue
		//private readonly Color _blue = Color.FromArgb(49, 91, 175); // Medium blue
		private readonly Color _blue = Color.FromArgb(0, 151, 214); // light blue
	    private readonly Color _lightBlue = Color.FromArgb(12, 168, 233);
		private readonly Color _orange = Color.FromArgb(249, 152, 34);
        private readonly Color _gray = Color.FromArgb(102, 102, 102);
        private readonly Color _lightGray = Color.FromArgb(204, 204, 204);


		public MemoryStream CreateTtmChartImage([NotNull] IEnumerable<KeyValuePair<DateTime, int?>> list, ReportImageType reportImageType,
			List<Tuple<ChartDashStyle, double, string>> stripLinesCollection)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			
			Size chartSize = GetChartSize(reportImageType);

			var chart = GetBaseChart("ttmChart", chartSize);

			AddPointsToChart(list, reportImageType, chart);

			AddLabelsToChart(stripLinesCollection, chart);

			return GetChartStream(chart);
		}

		private void AddLabelsToChart(List<Tuple<ChartDashStyle, double, string>> stripLinesCollection, Chart chart)
		{
			foreach (var chartArea in chart.ChartAreas)
			{
				chartArea.AxisX.IsMarginVisible = false;
				chartArea.AxisX.IsStartedFromZero = true;
				chartArea.AxisX.LabelStyle = new LabelStyle
				{
					Font = new Font("Verdana", 7F, FontStyle.Regular, GraphicsUnit.Point, 0),
					ForeColor = Color.FromArgb(255, _gray),
					Format = "M/d"
				};

				//chartArea.AxisY.Interval = reportImageType == ReportImageType.TimeToMarketComplete ? 5 : 2;
				chartArea.AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;
				chartArea.AxisY2.IntervalAutoMode = IntervalAutoMode.VariableCount;

				chartArea.AxisY.LabelStyle = new LabelStyle
				{
                    ForeColor = Color.FromArgb(255, _gray)
				};

				chartArea.AxisX.IsStartedFromZero = false;
				if (stripLinesCollection == null || !stripLinesCollection.Any())
				{
					continue;
				}

				chartArea.AxisY2.LabelStyle = new LabelStyle
				{
					Font = new Font("Verdana", 9F, FontStyle.Regular, GraphicsUnit.Point, 0),
                    ForeColor = Color.FromArgb(255, _gray),
					TruncatedLabels = false
				};

				chartArea.AxisY2.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont | LabelAutoFitStyles.StaggeredLabels;

				// Add Average Line
				var avgStripLine = stripLinesCollection.First(sl => sl.Item3.ToUpperInvariant() == "AVG");

				AddStripLine(chartArea, avgStripLine);

				if (!(Math.Abs(stripLinesCollection.Max(l => l.Item2) - stripLinesCollection.Min(l => l.Item2)) > 1))
				{
					continue;
				}

				// Add Goal line
				var stripLine = stripLinesCollection.First(sl => sl.Item3.ToUpperInvariant() == "GOAL");
				AddStripLine(chartArea, stripLine);
			}
		}

		private void AddStripLine(ChartArea chartArea, Tuple<ChartDashStyle, double, string> avgStripLine)
		{
			chartArea.AxisY.StripLines.Add(new StripLine
			{
				// Set the position of the line by the average of the series
				IntervalOffset = avgStripLine.Item2,
				StripWidth = 0.0,
				BorderColor = _lightGray,
				BorderDashStyle = avgStripLine.Item1,
				BorderWidth = 2

			});

			chartArea.AxisY2.Enabled = AxisEnabled.True;
			chartArea.AxisY2.IsLabelAutoFit = true;
			chartArea.AxisY2.CustomLabels.Add(new CustomLabel
			{
				Text = avgStripLine.Item2 + "\n" + avgStripLine.Item3,
				FromPosition = avgStripLine.Item2 - 1,
				ToPosition = avgStripLine.Item2 + 1
			});
		}

		private void AddPointsToChart(IEnumerable<KeyValuePair<DateTime, int?>> list, ReportImageType reportImageType, Chart chart)
		{
			var chartColor = GetChartColor(reportImageType);
			var keyValuePairs = list as IList<KeyValuePair<DateTime, int?>> ?? list.ToList();

            // create orphaned points data series
            var hasData = new Series
            {
                Name = "ttm_hasData_series",
                Color = chartColor,
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Point,
                MarkerSize = 9,
                MarkerStyle = MarkerStyle.Circle
            };
            chart.Series.Add(hasData);

			// create new data series
			var series = new Series
			{
				Name = "ttm_series",
				Color = chartColor,
				IsVisibleInLegend = false,
				IsXValueIndexed = true,
				ChartType = SeriesChartType.Line,
				BorderWidth = 5
			};
			chart.Series.Add(series);
            
		    var noData = keyValuePairs.Where(kvp => kvp.Value == null);
		    noData = noData as IList<KeyValuePair<DateTime, int?>> ?? noData.ToList();
            // apply data
            
		    foreach (var weekAvg in keyValuePairs.InterpolateAndExtend())
		    {
                var index = series.Points.AddXY(weekAvg.Key, weekAvg.Value);
                series.Points[index].Color = chartColor;
		        if (noData.Any(nd => nd.Key == weekAvg.Key))
		        {
		            index = hasData.Points.AddXY(weekAvg.Key, weekAvg.Value);
		            hasData.Points[index].Color = Color.FromArgb(0, chartColor);
		        } 
                else 
                {
		            index = hasData.Points.AddXY(weekAvg.Key, weekAvg.Value);
                    hasData.Points[index].Color = chartColor;
		        }
		    }
        }


		public MemoryStream CreateBucketChartImage(IEnumerable<BucketValue> bucketValueCollection, VehicleType vehicleType)
		{
			var bucketValues = bucketValueCollection as BucketValue[] ?? bucketValueCollection.ToArray();
			if (bucketValues.Length == 0)
				return null;

			var first = bucketValues.First();

			var chart = GetBaseChart(string.Format("{0}", first.BucketDescription), _smallChartSize);
		    foreach (var chartArea in chart.ChartAreas)
		    {
		        chartArea.AxisX.LabelStyle = new LabelStyle
		        {
		            Font = new Font("Verdana", 7F, FontStyle.Regular, GraphicsUnit.Point, 0),
                    Format = "M/d",
                    ForeColor = _gray
		        };
                chartArea.AxisY.LabelStyle = new LabelStyle
                {
                    ForeColor = _gray
                };
		    }
		    // create new data series
			
			var series = new Series
			{
				Name = string.Format("bucket_{0}_series", first.Bucket),
				Color = _lightBlue,
				IsVisibleInLegend = false,
				IsXValueIndexed = true,
				ChartType = SeriesChartType.Column,
                BackGradientStyle = GradientStyle.LeftRight,
                BackSecondaryColor = _blue,
                BorderColor = _lightGray,
                BorderWidth = 1
			};
            
			chart.Series.Add(series);
		    
			// add points to series
			foreach (var point in bucketValues)
			{
			    series.Points.AddXY(point.Date, point.GetCount(vehicleType));
			}

            if (bucketValues.Max(bv => bv.GetCount(vehicleType)) < 5)
			{
				foreach (var chartArea in  chart.ChartAreas)
				{
					chartArea.AxisY.Interval = 1;
				}
			}

			return GetChartStream(chart);
		}

		#region privates

		private Size GetChartSize(ReportImageType reportImageType)
		{
			return reportImageType == ReportImageType.TimeToMarketComplete ? _largeChartSize : _smallChartSize;
		}

		private Color GetChartColor(ReportImageType reportImageType)
		{
			return reportImageType == ReportImageType.TimeToMarketComplete ? _orange : _blue;
		}

		private MemoryStream GetChartStream(Chart chart)
		{
			// draw chart
			chart.Invalidate();

			// save chart to stream
			var imageStream = new MemoryStream();
			chart.SaveImage(imageStream, ChartImageFormat.Png);
			return imageStream;
		}

		private Chart GetBaseChart(string name, Size size)
		{
			// init and style chartArea
			var chartArea = new ChartArea {Name = name};
			chartArea.AxisY.MajorGrid.Enabled = false;
			chartArea.AxisY.MinorGrid.Enabled = false;

            chartArea.AxisY.LineColor = _lightGray;
            chartArea.AxisY2.LineColor = _lightGray;
            chartArea.AxisY.MajorTickMark.LineColor = _gray;
            chartArea.AxisY2.MajorTickMark.LineColor = _gray;

			chartArea.AxisY2.MajorGrid.Enabled = false;
			chartArea.AxisY2.MinorGrid.Enabled = false;

			chartArea.AxisX.MajorGrid.Enabled = false;
			chartArea.AxisX.MinorGrid.Enabled = false;

            chartArea.AxisX.LineColor = _lightGray;
            chartArea.AxisX2.LineColor = _lightGray;
		    chartArea.AxisX.MajorTickMark.LineColor = _gray;
            chartArea.AxisX2.MajorTickMark.LineColor = _gray;

			chartArea.AxisX2.MajorGrid.Enabled = false;
			chartArea.AxisX2.MinorGrid.Enabled = false;

            chartArea.AxisX.LabelStyle.Format = "M/d";

			chartArea.AxisY.MajorTickMark.Enabled = false;
			chartArea.AxisY2.MajorTickMark.Enabled = false;

			chartArea.AxisY.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont | LabelAutoFitStyles.StaggeredLabels;
			chartArea.AxisX.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont;
			chartArea.AxisY2.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont | LabelAutoFitStyles.StaggeredLabels;
		    

			// init chart, add legend and chartArea
			var chart = new Chart();
			chart.ChartAreas.Add(chartArea);

			chart.BorderDashStyle = ChartDashStyle.DashDot;
			chart.BorderlineDashStyle = ChartDashStyle.Dot;
			chart.Dock = DockStyle.Fill;
			chart.Location = new Point(10, 10);
		    chart.Size = size;		    

			return chart;
		}

        

		#endregion privates
	}
}