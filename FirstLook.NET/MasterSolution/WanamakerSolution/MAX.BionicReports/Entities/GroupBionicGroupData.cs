﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core;
using MAX.Entities.Helpers.DashboardHelpers;
using System.Configuration;

namespace MAX.BionicReports.Entities
{
    [Serializable]
    public class GroupBionicGroupData
    {
        #region Properties
        public int BusinessUnitId { get; set; }
        public String BusinessUnit { get; set; }
        public DateRange ReportDateRange { get; set; }
        public List<GroupBionicDealerData> Dealers { get; private set; }
        public String EmailAddressTo { get; set; }
        public String EmailAddressFrom { get; set; }
        public int UserBusinessUnitId { get; set; }

        public int DealerCount
        {
            get
            {
                return Dealers.Count;
            }
        }
        public int VehicleCount
        {
            get
            {
                return Dealers.Sum(d => d.VehicleCount);
            }
        }
        public String BusinessUnitHome
        {
            get
            {
                return String.Format("{0}{1}", ConfigurationManager.AppSettings["merchandising_host_path"],
                    UserBusinessUnitId > 0
                        ? Helper.RewriteDealerSpecificURL(UserBusinessUnitId, "GroupDashboard.aspx")
                        : "GroupDashboard.aspx"); // if the user business unit id <= 0 then go through the login page
            }
        }
        public String SettingsHome
        {
            get
            {
                return String.Format("{0}{1}", ConfigurationManager.AppSettings["merchandising_host_path"],
                    UserBusinessUnitId > 0
                        ? Helper.RewriteDealerSpecificURL(UserBusinessUnitId, "CustomizeMAX/DealerPreferences.aspx")
                        : "CustomizeMAX/DealerPreferences.aspx"); // if the user business unit id <= 0 then go through the login page
            }
        }
        public double MaxAverageDaysToPhotoOnline
        {
            get
            {
                return Dealers.Max(dl => dl.AverageDaysToPhotoOnline);
            }
        }
        public double MaxAverageDaysToCompleteAd
        {
            get
            {
                return Dealers.Max(dl => dl.AverageDaysToCompleteAd);
            }
        }
        #endregion

        #region Methods
        public GroupBionicGroupData()
        {
            BusinessUnitId = 0;
            BusinessUnit = String.Empty;
            Dealers = new List<GroupBionicDealerData>();
            EmailAddressTo = String.Empty;
            EmailAddressFrom = String.Empty;
            UserBusinessUnitId = 0;
        }

        public GroupBionicGroupData(int businessUnitId) : this()
        {
            BusinessUnitId = businessUnitId;
        }

        #endregion
    }

    [Serializable]
    public class GroupBionicDealerData
    {
        #region Properties
        public int BusinessUnitId { get; set; }
        public String BusinessUnit { get; set; }
        public DateRange DateRange { get; set; }
        public int VehicleCount { get; set; }
        public int DaysCount { get; set; }
        public int MinDaysPhoto { get; set; }
        public int MaxDaysPhoto { get; set; }
        public int MinDaysComplete { get; set; }
        public int MaxDaysComplete { get; set; }
        public double AverageDaysToPhotoOnline { get; set; }
        public double AverageDaysToCompleteAd { get; set; }

        public String BusinessUnitHome
        {
            get
            {
                return String.Format("{0}{1}", ConfigurationManager.AppSettings["merchandising_host_path"], Helper.RewriteDealerSpecificURL(BusinessUnitId, "Default.aspx?ref=GLD"));
            }
        }
        #endregion

        #region Methods
        public GroupBionicDealerData()
        {
            BusinessUnitId = 0;
            BusinessUnit = String.Empty;
            VehicleCount = 0;
            DaysCount = 0;
            MinDaysPhoto = 0;
            MaxDaysPhoto = 0;
            MinDaysComplete = 0;
            MaxDaysComplete = 0;
            AverageDaysToPhotoOnline = 0L;
            AverageDaysToCompleteAd = 0L;
        }

        public GroupBionicDealerData(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }
        #endregion
    }
}
