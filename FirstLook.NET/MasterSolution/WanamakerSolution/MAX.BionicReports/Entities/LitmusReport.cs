﻿using System;
using System.Xml.Serialization;

namespace MAX.BionicReports.Entities
{
    [XmlRoot("report")]
    public class LitmusReport
    {
        [XmlIgnore]
        private string _trackingCode;

        [XmlElement("bug_html")]
        public string TrackingCode {
            get
            {
                var code = _trackingCode.Trim();
                code = code.Replace("[UNIQUE]", "{emailAddress}"); // convert replace tag to what EmailTemplateTransform.cs expects
                code = code.Replace("<style>", "<style ref=\"ignore\">"); // prevents inlining of style
                return code;
            } 
            set { _trackingCode = value; } }

        [XmlElement("created_at")]
        public DateTime CreationTime { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }
    }
}
