﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using MAX.Entities.Reports.Bionic;

namespace MAX.BionicReports.Entities
{
    public class BionicReportEmail
    {
        public String EmailAddressTo;

        public DateRange ReportDateRange;
        public String DealerName;
        public String DMSName;
        public String DMSEmail;
        public String BusinessUnitHome;
        public String SettingsHome;
        public Int32 TimeToMarketGoalDays;
        public Double AverageDaysToCompleteAd;
        public Int32 MinDaysComplete;
        public Double AverageDaysToPhotoOnline;
        public Int32 MinDaysPhoto;
        public Double AverageAllInventory;
        public Double AverageNoPhotos;
        public Double AverageNoPrice;
        public Double AverageNotOnline;
        public Dictionary<ReportImageType, String> ReportImages;
    }
}
