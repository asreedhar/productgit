﻿using System.Xml.Serialization;

namespace MAX.BionicReports.Entities
{
    [XmlRoot("reports")]
    public class LitmusReports
    {
        /// <summary>
        /// It is possible for the reports object to have NO Entries.
        /// </summary>
        [XmlElement("report")]
        public LitmusReport[] Entries { get; set; }
    }
}
