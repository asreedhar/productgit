﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using Amazon.SimpleEmail.Model;
using AmazonServices.CW;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Release;
using MAX.BionicReports.Entities;
using MAX.BionicReports.Queues;
using MAX.BionicReports.Services;
using MAX.BionicReports.Utility;
using MAX.Entities;
using MAX.Entities.Messages;
using Merchandising.Messages;

namespace MAX.BionicReports.Tasks
{
    internal class GroupBionicReportCreationTask : TaskRunner<GroupBionicReportCreationMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const String ReportTemplate = "groupttm-email-template-v2-inlined.html";

        private readonly IEmail _emailProvider;

        private readonly IFileStorage _fileStorage;

        internal GroupBionicReportCreationTask(
            IBionicReportQueueFactory bionicReportQueueFactory,
            IFileStoreFactory fileStoreFactory,
            IEmail emailProvider)
            : base(bionicReportQueueFactory.CreateGroupBionicReportCreationQueue())
        {
            _emailProvider = emailProvider;

            _fileStorage = fileStoreFactory.CreateBionicReportStorage();
        }

        public override void Process(GroupBionicReportCreationMessage message)
        {
            log4net.LogicalThreadContext.Properties["threadId"] = string.Format("GroupId: {0}", message.GroupId);
            Log.Info("GroupBionicReportCreationTask Processing - Begin");

            var groupBusinessUnit = BusinessUnitFinder.Instance().Find(message.GroupId);
            var reportData = ReadGroupReportData(message.DataFileKey);
            var inlinedTemplate = EmailTemplateService.Instance.GetTemplate(ReportTemplate, _fileStorage);
            var whiteList = EmailJobBase.GetBionicWhiteList();
            var emailAddresses = message.EmailAddresses.Split(',');

            var emailList = emailAddresses.Select(emailAddress =>
            {
                var dealerId = 0;
                var member = MemberFinder.Instance().FindByEmailAddress(emailAddress);
                if (member != null)
                {
                    var component = SoftwareSystemComponentFinder.Instance().FindByToken("DEALER_SYSTEM_COMPONENT");
                    var componentState =
                        SoftwareSystemComponentStateFinder.Instance()
                            .FindByAuthorizedMemberAndSoftwareSystemComponent(member, component);
                    if (componentState != null)
                    {
                        var dealer = componentState.Dealer.GetValue();
                        if (dealer != null)
                            dealerId = dealer.Id;
                    }
                }

                // transform the data into a DTO
                var groupData = new GroupBionicGroupData(groupBusinessUnit.Id)
                {
                    BusinessUnit = groupBusinessUnit.Name,
                    ReportDateRange = reportData.ReportDateRange,
                    EmailAddressTo = emailAddress,
                    EmailAddressFrom = "DigitalMarketingSpecialist@maxdigital.com",
                    UserBusinessUnitId = dealerId
                };

                groupData.Dealers.AddRange(
                    reportData.TimeToMarketGroupReport.BusinessUnits.Select(
                        bu => new GroupBionicDealerData(bu.BusinessUnitId)
                        {
                            BusinessUnit = bu.BusinessUnit,
                            DateRange = bu.DateRange,
                            VehicleCount = bu.VehicleCount,
                            DaysCount = bu.DaysCount,
                            MinDaysPhoto = bu.MinDaysPhoto < 0 ? 0 : bu.MinDaysPhoto,
                            MaxDaysPhoto = bu.MaxDaysPhoto < 0 ? 0 : bu.MaxDaysPhoto,
                            MinDaysComplete = bu.MinDaysComplete < 0 ? 0 : bu.MinDaysComplete,
                            MaxDaysComplete = bu.MaxDaysComplete < 0 ? 0 : bu.MaxDaysComplete,
                            AverageDaysToPhotoOnline = bu.AverageDaysToPhotoOnline,
                            AverageDaysToCompleteAd = bu.AverageDaysToCompleteAd
                        }));
                groupData.Dealers.Sort((x, y) => x.AverageDaysToCompleteAd.CompareTo(y.AverageDaysToCompleteAd));

                return groupData;
            });

            foreach (var email in emailList)
            {
                var success = false;

                if (!EmailJobBase.CanSend(whiteList, email.EmailAddressTo))
                {
                    Log.InfoFormat("Email address {0} not in whitelist, skipping.", email.EmailAddressTo);
                    continue;
                }

                try
                {
                    if (String.IsNullOrWhiteSpace(email.EmailAddressTo))
                        throw new ArgumentNullException(); // trap for when the email address is empty

                    var emailMessage = new MailMessage
                    {
                        From = new MailAddress(email.EmailAddressFrom),
                        IsBodyHtml = true,
                        Subject = "Group Digital Marketing Analysis " + email.BusinessUnit,
                        Body = EmailTemplateTransform.ApplyGroupBionic(inlinedTemplate, message.TrackingSnip, email)
                    };
                    emailMessage.To.Add(new MailAddress(email.EmailAddressTo));

                    _emailProvider.SendEmail(emailMessage);

                    Log.InfoFormat("Email sent to {0} for BusinessUnitId {1}", email.EmailAddressTo, email.BusinessUnitId);

                    success = true;
                }
                catch (MessageRejectedException messageRejectedException)
                {
                    Log.Error("GroupBionicReportCreationTask: AWS SES Message Rejected Exception (sent from: DigitalMarketingSpecialist@maxdigital.com", messageRejectedException);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    Log.Warn("GroupBionicReportCreationTask: Argument Null Exception", argumentNullException);
                }
                catch (ArgumentException argumentException)
                {
                    Log.Warn("GroupBionicReportCreationTask: Argument Exception", argumentException);
                }
                catch (FormatException formatException)
                {
                    Log.Warn("GroupBionicReportCreationTask: Format Exception", formatException);
                }
                catch (ApplicationException applicationException)
                {
                    Log.Warn("GroupBionicReportCreationTask: Application Exception", applicationException);
                }
                catch (KeyNotFoundException keyNotFoundException)
                {
                    Log.Error("GroupBionicReportCreationTask: Key Not Found Exception", keyNotFoundException);
                }
                catch (Exception ex)
                {
                    if (_emailProvider.IsHardBounce(ex))
                    {
                        // eat the message because one of the email addresses has been bounced by Amazon
                        Log.Warn("GroupBionicReportCreationTask: AWS Hardbounced the email");
                    }
                    else
                    {
                        Log.Error("GroupBionicReportCreationTask encountered an error");
                        throw;
                    }
                }
                finally
                {
                    if (!success)
                    {
                        CloudWatch.Instance.IncrementMetric(1, "Bionic-SentEmail-Failed");
                    }
                }
            }

            Log.Info("GroupBionicReportCreationTask Processing - End");
        }

        private GroupBionicReportData ReadGroupReportData(string dataFileKey)
        {
            using (var stream = _fileStorage.Read(dataFileKey))
            {
                if (stream == null)
                {
                    return new EmptyGroupBionicReportData();
                }
                using (TextReader textReader = new StreamReader(stream))
                {
                    var jsonString = textReader.ReadToEnd();
                    return jsonString.FromJson<GroupBionicReportData>();
                }
            }
        }
    }
}
