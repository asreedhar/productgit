﻿using System;
using System.IO;
using System.Net.Mail;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.BionicReports.Queues;
using MAX.Entities;
using MAX.Entities.Messages;
using Merchandising.Messages;

namespace MAX.BionicReports.Tasks
{
    internal class GroupBionicReportDataCollectionTask : TaskRunner<GroupBionicReportDataCollectionMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IBionicReportQueueFactory _bionicReportQueueFactory;
        private readonly ITimeToMarketRepository _timeToMarketRepository;
        private readonly IFileStoreFactory _fileStoreFactory;
        private readonly IEmail _emailProvider;

        internal GroupBionicReportDataCollectionTask(IBionicReportQueueFactory bionicReportQueueFactory, ITimeToMarketRepository timeToMarketRepository, IFileStoreFactory fileStoreFactory, IEmail emailProvider)
            : base(bionicReportQueueFactory.CreateGroupBionicReportDataCollectionQueue())
        {
            _bionicReportQueueFactory = bionicReportQueueFactory;
            _timeToMarketRepository = timeToMarketRepository;
            _fileStoreFactory = fileStoreFactory;
            _emailProvider = emailProvider;
        }

        public override void Process(GroupBionicReportDataCollectionMessage message)
        {
            log4net.LogicalThreadContext.Properties["threadId"] = string.Format("GroupId: {0}", message.GroupId);
            Log.Info("GroupBionicReportDataCollectionTask Processing - Begin");

            try
            {
                var end = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);
                var start = end.AddMonths(-1);

                var reportData = new GroupBionicReportData(message.GroupId)
                {
                    EmailAddresses = message.EmailAddresses,
                    ReportDateRange = new DateRange(start, end),
                    VehicleType = VehicleType.Used
                };

                // Get report data for the email
                Log.Trace(String.Format("Fetching the Group Report for GroupID: {0}", message.GroupId));

                reportData.TimeToMarketGroupReport = _timeToMarketRepository.FetchGroupReport(message.GroupId,
                    reportData.VehicleType, reportData.ReportDateRange);

                // save to Json to S3
                var fileStore = _fileStoreFactory.CreateBionicReportStorage();
                var fileName = FileName(message.GroupId);

                using (var stream = new MemoryStream())
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(reportData.ToJson());
                    writer.Flush();
                    stream.Position = 0;
                    fileStore.Write(fileName, stream);
                }

                // send message into the queue
                var queue = _bionicReportQueueFactory.CreateGroupBionicReportCreationQueue();
                var nextMessage = new GroupBionicReportCreationMessage(message.GroupId, message.EmailAddresses, message.TrackingSnip, fileName);

                queue.SendMessage(nextMessage, GetType().FullName);
            }
            catch (ArgumentNullException argumentNullException)
            {
                Log.Warn("GroupBionicReportDataCollectionTask: Argument Null Exception", argumentNullException);
            }
            catch (ArgumentException argumentException)
            {
                Log.Warn("GroupBionicReportDataCollectionTask: Argument Exception", argumentException);
            }
            catch (FormatException formatException)
            {
                Log.Warn("GroupBionicReportDataCollectionTask: Format Exception", formatException);
            }
            catch (ApplicationException applicationException)
            {
                Log.Warn("GroupBionicReportDataCollectionTask: Application Exception", applicationException);
            }
            catch (Exception ex)
            {
                if (_emailProvider.IsHardBounce(ex))
                {
                    // eat the message because one of the email addresses has been bounced by Amazon
                    Log.Warn("GroupBionicReportDataCollectionTask: AWS Hardbounced the email");
                }
                else
                {
                    Log.Error("GroupBionicReportDataCollectionTask encountered an error");
                    throw;
                }
            }
            finally
            {
                Log.Info("GroupBionicReportDataCollectionTask Processing - End");
            }
        }

        internal string FileName(int groupId)
        {
            //var owner = Owner.GetOwner(groupId); // can't use owner handle because group dealer records don't have an owner entry
            var timestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            // GroupReport/86D21295-1417-4375-BFAF-0D738462FA35_100004_20140805000000
            return String.Format("GroupReport/{0}_{1}_{2}", Guid.NewGuid(), groupId, timestamp);
        }
    }
}
