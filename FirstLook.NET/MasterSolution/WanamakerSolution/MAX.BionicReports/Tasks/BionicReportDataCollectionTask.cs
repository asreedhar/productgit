﻿using System;
using System.IO;
using System.Net.Mail;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.BionicReports.Queues;
using MAX.Entities;
using MAX.Entities.Messages;
using Merchandising.Messages;

namespace MAX.BionicReports.Tasks
{
    internal class BionicReportDataCollectionTask : TaskRunner<BionicReportDataCollectionMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IBionicReportQueueFactory _bionicReportQueueFactory;
        private readonly IDashboardRepository _dashboardRepository;
        private readonly ITimeToMarketRepository _timeToMarketRepository;
        private readonly IFileStoreFactory _fileStoreFactory;
        private readonly IEmail _emailProvider;

        internal BionicReportDataCollectionTask(IBionicReportQueueFactory bionicReportQueueFactory, IDashboardRepository dashboardRepository, ITimeToMarketRepository timeToMarketRepository, IFileStoreFactory fileStoreFactory, IEmail emailProvider)
            : base(bionicReportQueueFactory.CreateBionicReportDataCollectionQueue())
        {
            _bionicReportQueueFactory = bionicReportQueueFactory;
            _dashboardRepository = dashboardRepository;
            _timeToMarketRepository = timeToMarketRepository;
            _fileStoreFactory = fileStoreFactory;
            _emailProvider = emailProvider;
        }

        public override void Process(BionicReportDataCollectionMessage message)
        {
            log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}", message.BusinessUnitId);
            Log.Info("BionicReportDataCollectionTask Processing - Begin");

            try
            {
                var end = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);
                var start = end.AddMonths(-1);
                
                var reportData = new BionicReportData(message.BusinessUnitId)
                {
                    EmailAddresses = message.EmailAddresses,
                    ReportDateRange = new DateRange(start, end),
                    VehicleType = VehicleType.Used
                };

                // 1. Get settings DMS, goals, etc...
                var miscSettings = GetMiscSettings.GetSettings(message.BusinessUnitId);
                reportData.DMSName = miscSettings.DMSName;
                reportData.DMSEmail = miscSettings.DMSEmail;

                var reportSettings = GetReportSettings.GetSettings(message.BusinessUnitId);
                reportData.TimeToMarketGoalDays = reportSettings.TimeToMarketGoalDays;

                // 2. Get Bucket Alerts data for each chart
                reportData.BucketValues = _dashboardRepository.FetchBucketValuesReport(message.BusinessUnitId,
                    reportData.ReportDateRange);

                // 3. Get TTM data for each chart
                reportData.TimeToMarketReport = _timeToMarketRepository.FetchReport(message.BusinessUnitId,
                    reportData.VehicleType,
                    reportData.ReportDateRange);

                if (reportData.TimeToMarketReport.VehicleCount <= 0) // no TTM data found
                {
                    var businessUnit = BusinessUnitFinder.Instance().Find(message.BusinessUnitId);

                    var emailMessage = new MailMessage
                    {
                        From = new MailAddress("helpdesk@maxdigital.com"),
                        IsBodyHtml = false,
                        Subject = "Digital Marketing Analysis for " + businessUnit.ShortName + " failed",
                        Body = "There was no Time to Market data found for " + businessUnit.ShortName
                    };
                    emailMessage.To.Add(new MailAddress(reportData.DMSEmail));
                    _emailProvider.SendEmail(emailMessage);

                    throw new ApplicationException(String.Format("Time to market data not found for {0}",
                        businessUnit.ShortName));
                }

                // 4. save to Json to S3
                var fileStore = _fileStoreFactory.CreateBionicReportStorage();
                var fileName = FileName(message.BusinessUnitId);

                using (var stream = new MemoryStream())
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(reportData.ToJson());
                    writer.Flush();
                    stream.Position = 0;
                    fileStore.Write(fileName, stream);
                }

                // 5. send message into the queue
                var queue = _bionicReportQueueFactory.CreateBionicReportImageCreationQueue();
                var nextMessage = new BionicReportImageCreationMessage(message.BusinessUnitId, message.EmailAddresses, message.TrackingSnip,
                    fileName);

                queue.SendMessage(nextMessage, GetType().FullName);
            }
            catch (ArgumentNullException argumentNullException)
            {
                Log.Warn("Argument Null Exception", (Exception) argumentNullException);
            }
            catch (ArgumentException argumentException)
            {
                Log.Warn("Argument Exception", (Exception) argumentException);
            }
            catch (FormatException formatException)
            {
                Log.Warn("Format Exception", (Exception) formatException);
            }
            catch (ApplicationException applicationException)
            {
                Log.Warn("Application Exception", (Exception)applicationException);
            }
            catch (Exception ex)
            {
                if (_emailProvider.IsHardBounce(ex))
                {
                    // eat the message because one of the email addresses has been bounced by Amazon
                    Log.Warn("AWS Hardbounced the email");
                }
                else
                {
                    Log.Error("BionicReportDataCollectionTask encountered an error");
                    throw;
                }
            }
            finally
            {
                Log.Info("BionicReportDataCollectionTask Processing - End");
            }
        }

        internal string FileName(int businessUnitId)
        {
            var owner = Owner.GetOwner(businessUnitId);
            var timestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            // 86D21295-1417-4375-BFAF-0D738462FA35_20140805000000
            return String.Format("{0}_{1}", owner.Handle, timestamp);
        }
    }
}
