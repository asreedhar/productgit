﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms.DataVisualization.Charting;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using log4net;
using MAX.BionicReports.Images;
using MAX.BionicReports.Queues;
using MAX.BionicReports.Topics;
using MAX.Entities;
using MAX.Entities.Messages;
using MAX.Entities.Reports.Bionic;
using Merchandising.Messages.BionicReport;

namespace MAX.BionicReports.Tasks
{
	public class BionicReportImageCreationTask : TaskRunner<BionicReportImageCreationMessage>
	{
		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging

		private readonly IFileStorage _fileStorage;
		private readonly IImageCreator _imageCreator;
		private readonly IBus<BionicReportImagesCompleteMessage, BionicReportImagesCompleteMessage> _processingCompleteNotificationBus;

		//								Ownerhandle/yearmonth_ImageType.png
		private const string FileNameFormat = "Images/{0}/{1}_{2}.png";

		internal BionicReportImageCreationTask(
			IBionicReportQueueFactory bionicReportQueueFactory,
			IBionicReportsBusFactory bionicReportsBusFactory, 
			IFileStorage fileStorage,
			IImageCreator imageCreator
		) : base(bionicReportQueueFactory.CreateBionicReportImageCreationQueue())
		{
			_fileStorage = fileStorage;
			_imageCreator = imageCreator;
			_processingCompleteNotificationBus = bionicReportsBusFactory.CreateBionicImagesCompleteBus<BionicReportImagesCompleteMessage, BionicReportImagesCompleteMessage>();
		}

		public override void Process(BionicReportImageCreationMessage message)
		{
			if (message.BusinessUnitId <= 0)
			{
				Log.Warn("Message received with no BusinessUnitId. Exiting.");
				return;
			}
			if (string.IsNullOrWhiteSpace(message.DataFileKey))
			{
				Log.WarnFormat("Message received for BusinessUnitId {0} with no DataFileKey. Exiting.", message.BusinessUnitId);
				return;
			}

			Log.Info("Creating Bionic Images.");
			var reportData = ReadReportData(message.DataFileKey);

			var createdImages = EnumHelper.GetValues<ReportImageType>().Where(imageType => TryCreateImage(imageType, reportData))
				.ToDictionary(imageType => imageType, imageType => FileName(message.BusinessUnitId, imageType));

			if (createdImages.Count == 5)
			{
				var imagesCompleteMessage = new BionicReportImagesCompleteMessage(
					message.BusinessUnitId,
                    message.EmailAddresses,
                    message.TrackingSnip,
					message.DataFileKey,
					createdImages
					);

				_processingCompleteNotificationBus.Publish(imagesCompleteMessage, GetType().FullName);
				Log.Info("Bionic Images Complete.");
			}
			else
			{
				Log.WarnFormat("Bionic Images InComplete. Only {0} images created", createdImages.Count);
			}
		}

		private bool TryCreateImage(ReportImageType reportImageType, BionicReportData reportData)
		{
			try
			{
				MemoryStream imgStream = null;
				switch (reportImageType)
				{
					case ReportImageType.VehiclesWithNoPhotos:
						imgStream = _imageCreator.CreateBucketChartImage(CreateFiveDayBucketValueCollection(reportData
							.BucketValues
                            .Where(bv => bv.Bucket == WorkflowType.NoPhotos)), reportData.VehicleType);
						break;

					case ReportImageType.VehiclesWithNoPrice:
						imgStream = _imageCreator.CreateBucketChartImage(CreateFiveDayBucketValueCollection(reportData
							.BucketValues
                            .Where(bv => bv.Bucket == WorkflowType.NoPrice)), reportData.VehicleType);
						break;

					case ReportImageType.VehiclesNotOnline:
						imgStream = _imageCreator.CreateBucketChartImage(CreateFiveDayBucketValueCollection(reportData.BucketValues
                            .Where(bv => bv.Bucket == WorkflowType.NotOnline)), reportData.VehicleType);
						break;

					case ReportImageType.TimeToMarketComplete:
						var chartLines = new List<Tuple<ChartDashStyle, double, string>>
						{
							new Tuple<ChartDashStyle, double, string>(ChartDashStyle.Solid, Math.Round(reportData.TimeToMarketReport.AverageDaysToCompleteAd, 0, MidpointRounding.AwayFromZero), "AVG"),
							new Tuple<ChartDashStyle, double, string>(ChartDashStyle.Dash, reportData.TimeToMarketGoalDays, "GOAL")
						};

						imgStream = _imageCreator.CreateTtmChartImage(CreateFiveDayTtmValueCollection(reportData
							.TimeToMarketReport
							.Days
							.Select(day => new KeyValuePair<DateTime, int>(day.Date, day.AvgElapsedDaysAdComplete))), reportImageType, chartLines);
						break;

					case ReportImageType.TimeToMarketFirstPhoto:
						imgStream = _imageCreator.CreateTtmChartImage(CreateFiveDayTtmValueCollection(reportData
							.TimeToMarketReport
							.Days
							.Select(day => new KeyValuePair<DateTime, int>(day.Date, day.AvgElapsedDaysPhoto))), reportImageType, null);
						break;
				}

				if (imgStream != null)
				{
					var fn = FileName(reportData.BusinessUnitId, reportImageType);
					_fileStorage.Write(fn, imgStream);
					imgStream.Dispose();

					Log.DebugFormat("Wrote image to filestore: {0}", fn);

					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
				Log.WarnFormat("Error thrown trying to create image type {0}. {1}", reportImageType, ex.StackTrace);
				return false;
			}
		}

		internal IEnumerable<KeyValuePair<DateTime, int?>> CreateFiveDayTtmValueCollection(IEnumerable<KeyValuePair<DateTime, int>> enumerableDates)
		{

			var valuesByWeek = new List<KeyValuePair<DateTime, int?>>();
			var keyValuePairs = enumerableDates as KeyValuePair<DateTime, int>[] ?? enumerableDates.ToArray();
			if (!keyValuePairs.Any())
				return valuesByWeek;

            keyValuePairs = keyValuePairs.OrderBy(kvp => kvp.Key).ToArray();

			var kvpDates = keyValuePairs.First();
			var daysInMonth = DateTime.DaysInMonth(kvpDates.Key.Year, kvpDates.Key.Month);
			var sections = daysInMonth/5;
			var extraDaysInLastSection = daysInMonth%5;

			for (var i = 0; i < sections; i++)
			{
				var startDay = i*5;
				var endDay = startDay + 5;

				if (i == sections - 1)
				{
					endDay += extraDaysInLastSection;
				}

				var kvpValSectionEnum = keyValuePairs.Where(kv => kv.Key.Day > startDay && kv.Key.Day <= endDay);
				var valSectionEnum = kvpValSectionEnum as KeyValuePair<DateTime, int>[] ?? kvpValSectionEnum.ToArray();
				valuesByWeek.Add(!valSectionEnum.Any()
					? new KeyValuePair<DateTime, int?>(new DateTime(kvpDates.Key.Year, kvpDates.Key.Month, endDay), null)
					: new KeyValuePair<DateTime, int?>(new DateTime(kvpDates.Key.Year, kvpDates.Key.Month, endDay), (int) valSectionEnum.Average(kvp => kvp.Value)));
			}
			return valuesByWeek;
		}

		internal IEnumerable<BucketValue> CreateFiveDayBucketValueCollection(IEnumerable<BucketValue> bucketValuesEnum)
		{
			var valuesByWeek = new List<BucketValue>();

			var bucketValues = bucketValuesEnum.ToArray();
			bucketValues.OrderBy(bv => bv.Date);
			var bvFirst = bucketValues.First();
			if (bvFirst == null)
				return valuesByWeek;
			
			var daysInMonth = DateTime.DaysInMonth(bvFirst.Date.Year, bvFirst.Date.Month);
			var sections = daysInMonth/5;
			var extraDaysInLastSection = daysInMonth%5;

			for (var i = 0; i < sections; i++)
			{
				var startDay = i*5;
				var endDay = startDay + 5;

				if (i == sections - 1)
				{
					endDay += extraDaysInLastSection;
				}

				var bucketValueSectionEnum = bucketValues.Where(bv => bv.Date.Day > startDay && bv.Date.Day <= endDay);
				var valueSectionEnum = bucketValueSectionEnum as BucketValue[] ?? bucketValueSectionEnum.ToArray();
				
				if(!valueSectionEnum.Any())
					continue;

				var singleBucketVal = valueSectionEnum.Last().Clone();
				singleBucketVal.Date = new DateTime(singleBucketVal.Date.Year, singleBucketVal.Date.Month, endDay);
				singleBucketVal.NewCount = (int)valueSectionEnum.Average(bv => bv.NewCount);
				singleBucketVal.UsedCount = (int)valueSectionEnum.Average(bv => bv.UsedCount);

				valuesByWeek.Add(singleBucketVal);
			}

			return valuesByWeek;
		}

		private BionicReportData ReadReportData(string dataFileKey)
		{
			using (var stream = _fileStorage.Read(dataFileKey))
			{
				if (stream == null)
				{
					return new EmptyBionicReportData();
				}
				using (TextReader textReader = new StreamReader(stream))
				{
					var jsonString = textReader.ReadToEnd();
					return jsonString.FromJson<BionicReportData>();
				}
			}
		}

		internal virtual string FileName(int businessUnitId, ReportImageType imageType)
		{
			var owner = Owner.GetOwner(businessUnitId);
			//string ownerhandle = 
			var timestamp = DateTime.UtcNow.ToString("yyyyMMdd");

			// 86D21295-1417-4375-BFAF-0D738462FA35/201408_
			return String.Format(FileNameFormat, owner.Handle, timestamp, imageType);
		}
	}

	internal static class ObjectCloner
	{
		/// <summary>
		/// Clones an object by using the
		/// <see cref="BinaryFormatter" />.
		/// </summary>
		/// <param name="obj">The object to clone.</param>
		/// <remarks>
		/// The object to be cloned must be serializable.
		/// </remarks>
		public static T Clone<T>(T obj)
		{
			using (var buffer = new MemoryStream())
			{
				var formatter = new BinaryFormatter();
				formatter.Serialize(buffer, obj);
				buffer.Position = 0;
				return (T) formatter.Deserialize(buffer);
			}
		}
	}
}
