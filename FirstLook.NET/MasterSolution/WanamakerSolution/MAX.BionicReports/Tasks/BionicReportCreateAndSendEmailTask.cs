﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using Amazon.SimpleEmail.Model;
using AmazonServices.CW;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Release;
using MAX.BionicReports.Entities;
using MAX.BionicReports.Queues;
using MAX.BionicReports.Services;
using MAX.BionicReports.Topics;
using MAX.BionicReports.Utility;
using MAX.Entities;
using Merchandising.Messages.BionicReport;

namespace MAX.BionicReports.Tasks
{
    internal class BionicReportCreateAndSendEmailTask : TaskRunner<BionicReportImagesCompleteMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const String ReportTemplate = "bionic-email-template-v2-inlined.html";

        private readonly IFileStorage _fileStorage;
        private readonly IEmail _emailProvider;

        internal BionicReportCreateAndSendEmailTask(
			IBionicReportQueueFactory bionicReportQueueFactory,
			IFileStorage fileStorage,
            IBionicReportsBusFactory bionicBusFactory,
            IEmail emailProvider
		) : base(bionicReportQueueFactory.CreateBionicReportImagesCompletedQueue())
        {
            _fileStorage = fileStorage;
            _emailProvider = emailProvider;

            var completedBus = bionicBusFactory.CreateBionicImagesCompleteBus<BionicReportImagesCompleteMessage, BionicReportImagesCompleteMessage>();
            completedBus.Subscribe(ReadQueue);
        }

        public override void Process(BionicReportImagesCompleteMessage message)
        {
            log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}", message.BusinessUnitId);
            Log.Info("BionicReportCreateAndSendEmailTask Processing - Begin");

            var businessUnit = BusinessUnitFinder.Instance().Find(message.BusinessUnitId);
            var reportData = ReadReportData(message.DataFileKey);
            var emailAddresses = message.EmailAddresses.Split(',');//reportData.EmailAddresses.Split(','); // use the message emails
            var inlinedTemplate = EmailTemplateService.Instance.GetTemplate(ReportTemplate, _fileStorage);

            var reportImages = message.ImageNames.Select(k => new
            {
                k.Key,
                Value = String.Format("https://s3.amazonaws.com/{0}/{1}", _fileStorage.ContainerName(), k.Value)
            }).ToDictionary(k => k.Key, k => k.Value);

            var settingsEntry = new RedirectEntry(businessUnit.Id, "/merchandising/CustomizeMAX/DealerPreferences.aspx");
            var emailList = emailAddresses.Select(t => new BionicReportEmail
            {
                EmailAddressTo = t.Trim(),
                DealerName = businessUnit.Name,
                DMSName = reportData.DMSName,
                DMSEmail = reportData.DMSEmail,
                BusinessUnitHome = String.Format("{0}{1}", ConfigurationManager.AppSettings["merchandising_host_path"], reportData.TimeToMarketReport.BusinessUnitHome),
                SettingsHome = String.Format("{0}Workflow/Link.aspx?link={1}", ConfigurationManager.AppSettings["merchandising_host_path"], settingsEntry.ToToken()),
                ReportDateRange = reportData.ReportDateRange,
                TimeToMarketGoalDays = reportData.TimeToMarketGoalDays,
                AverageDaysToCompleteAd = reportData.TimeToMarketReport.AverageDaysToCompleteAd,
                MinDaysComplete = reportData.TimeToMarketReport.MinDaysComplete,
                MinDaysPhoto = reportData.TimeToMarketReport.MinDaysPhoto,
                AverageDaysToPhotoOnline = reportData.TimeToMarketReport.AverageDaysToPhotoOnline,
                ReportImages = reportImages,
                AverageAllInventory =
                    Math.Round(
                        (Double)
                            reportData.BucketValues.Where(v => v.Bucket == WorkflowType.AllInventory)
                                .Sum(v => v.GetCount(reportData.VehicleType))/
                        reportData.BucketValues.Count(y => y.Bucket == WorkflowType.AllInventory), 0),
                AverageNoPhotos =
                    Math.Round(
                        (Double)
                            reportData.BucketValues.Where(v => v.Bucket == WorkflowType.NoPhotos)
                                .Sum(v => v.GetCount(reportData.VehicleType)) /
                        reportData.BucketValues.Count(v => v.Bucket == WorkflowType.NoPhotos), 0),
                AverageNoPrice =
                    Math.Round(
                        (Double)
                            reportData.BucketValues.Where(v => v.Bucket == WorkflowType.NoPrice)
                                .Sum(v => v.GetCount(reportData.VehicleType)) /
                        reportData.BucketValues.Count(v => v.Bucket == WorkflowType.NoPrice), 0),
                AverageNotOnline =
                    Math.Round(
                        (Double)
                            reportData.BucketValues.Where(v => v.Bucket == WorkflowType.NotOnline)
                                .Sum(v => v.GetCount(reportData.VehicleType)) /
                        reportData.BucketValues.Count(v => v.Bucket == WorkflowType.NotOnline), 0)
            });

            var whiteList = EmailJobBase.GetBionicWhiteList();

            foreach (var email in emailList)
            {
                if (!EmailJobBase.CanSend(whiteList, email.EmailAddressTo))
                {
                    Log.Info(string.Format("Email address {0} not in whitelist, skipping.", email.EmailAddressTo));
                    continue;
                }

                var success = false;
                try
                {
                    if (String.IsNullOrWhiteSpace(email.EmailAddressTo))
                        throw new ArgumentNullException(); // trap for when the email address is empty
                    if (String.IsNullOrWhiteSpace(email.DMSEmail))
                        throw new ArgumentNullException(); // trap for empty DMS Email address

                    var emailMessage = new MailMessage
                    {
                        From = new MailAddress(email.DMSEmail),
                        IsBodyHtml = true,
                        Subject = "Digital Marketing Analysis " + businessUnit.ShortName,
                        Body = EmailTemplateTransform.ApplyBionic(inlinedTemplate, message.TrackingSnip, email)
                    };
                    emailMessage.To.Add(new MailAddress(email.EmailAddressTo));
                    emailMessage.Bcc.Add(new MailAddress(email.DMSEmail));

                    _emailProvider.SendEmail(emailMessage);

                    Log.InfoFormat("Email sent to {0}, bcc {1} for BusinessUnitId {2}", email.EmailAddressTo, email.DMSEmail, message.BusinessUnitId);
                    success = true;
                }
                catch (MessageRejectedException messageRejectedException)
                {
                    Log.Error(String.Format("AWS SES Message Rejected Exception (sent from: {0})", email.DMSEmail), (Exception)messageRejectedException);
                }
                catch (ArgumentNullException argumentNullException)
                {
                    Log.Warn("Argument Null Exception", (Exception)argumentNullException);
                }
                catch (ArgumentException argumentException)
                {
                    Log.Warn("Argument Exception", (Exception)argumentException);
                }
                catch (FormatException formatException)
                {
                    Log.Warn("Format Exception", (Exception)formatException);
                }
                catch (KeyNotFoundException keyNotFoundException)
                {
                    Log.Warn("Key Not Found Exception", (Exception)keyNotFoundException);
                }
                catch (Exception e)
                {
                    if (_emailProvider.IsHardBounce(e))
                    {
                        Log.Warn("AWS Hardbounced the email");
                    }
                    else
                    {
                        Log.Error("BucketAlertTask encountered an error in sending email", e);

                        // resend only for the email address that failed
                        ReadQueue.SendMessage(
                            new BionicReportImagesCompleteMessage(message.BusinessUnitId, email.EmailAddressTo, message.TrackingSnip,
                                message.DataFileKey, message.ImageNames), GetType().FullName);
                    }
                }
                finally
                {
                    if (!success)
                    {
                        CloudWatch.Instance.IncrementMetric(1, "Bionic-SentEmail-Failed");
                    }
                }
            }

            Log.Info("BionicReportCreateAndSendEmailTask Processing - End");
        }

        private BionicReportData ReadReportData(string dataFileKey)
        {
            using (var stream = _fileStorage.Read(dataFileKey))
            {
                if (stream == null)
                {
                    return new EmptyBionicReportData();
                }
                using (TextReader textReader = new StreamReader(stream))
                {
                    var jsonString = textReader.ReadToEnd();
                    return jsonString.FromJson<BionicReportData>();
                }
            }
        }
    }
}
