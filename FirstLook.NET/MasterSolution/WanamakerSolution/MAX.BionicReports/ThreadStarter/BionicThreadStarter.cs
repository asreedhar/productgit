using System;
using System.IO;
using System.Reflection;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using log4net;
using MAX.BionicReports.Images;
using MAX.BionicReports.Queues;
using MAX.BionicReports.Tasks;
using MAX.BionicReports.Topics;
using MAX.Threading;
using Merchandising.Messages;

namespace MAX.BionicReports.ThreadStarter
{
    internal class BionicThreadStarter : IBionicThreadStarter
    {
        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Logging

        private readonly object _sync = new object();
        private readonly IBionicReportQueueFactory _bionicReportQueueFactory;
		private readonly IBionicReportsBusFactory _bionicReportsBusFactory;
		private readonly IDashboardRepository _dashboardRepository;
        private readonly ITimeToMarketRepository _timeToMarketRepository;
        private readonly IFileStoreFactory _fileStoreFactory;
	    private readonly IImageCreator _imageCreator;
        private readonly IEmail _emailProvider;

	    public BionicThreadStarter(IBionicReportQueueFactory bionicReportQueueFactory, IBionicReportsBusFactory bionicReportsBusFactory, 
			IDashboardRepository dashboardRepository, ITimeToMarketRepository timeToMarketRepository,
            IFileStoreFactory fileStoreFactory, IImageCreator imageCreator, IEmail emailProvider)
        {
            _bionicReportQueueFactory = bionicReportQueueFactory;
			_bionicReportsBusFactory = bionicReportsBusFactory;
			_dashboardRepository = dashboardRepository;
            _timeToMarketRepository = timeToMarketRepository;
            _fileStoreFactory = fileStoreFactory;
			_imageCreator = imageCreator;
	        _emailProvider = emailProvider;
        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            Log.Info("Merchandising threads starting.");

            lock (_sync)
            {
                NamedBackgroundThread.Start("Worker - BionicReportDataCollectionTask",
                    () => new BionicReportDataCollectionTask(_bionicReportQueueFactory, _dashboardRepository, _timeToMarketRepository, _fileStoreFactory, _emailProvider).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - BionicReportImageCreationTask",
                    () => new BionicReportImageCreationTask(_bionicReportQueueFactory, _bionicReportsBusFactory, _fileStoreFactory.CreateBionicReportStorage(), _imageCreator).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - BionicReportCreateAndSendEmailTask",
                    () => new BionicReportCreateAndSendEmailTask(_bionicReportQueueFactory, _fileStoreFactory.CreateBionicImageStorage(), _bionicReportsBusFactory, _emailProvider).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - GroupBionicReportDataCollectionTask",
                    () => new GroupBionicReportDataCollectionTask(_bionicReportQueueFactory, _timeToMarketRepository, _fileStoreFactory, _emailProvider).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - GroupBionicReportCreationTask",
                    () => new GroupBionicReportCreationTask(_bionicReportQueueFactory, _fileStoreFactory, _emailProvider).Run(shouldCancel, blockWhileNoMessages));
            }
        }
    }
}