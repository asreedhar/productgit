﻿using MAX.Threading;

namespace MAX.BionicReports.ThreadStarter
{
    public interface IBionicThreadStarter:IRunWithCancellation
    {
    }
}