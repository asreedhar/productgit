﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Core.Messaging;

namespace MAX.BionicReports.Services
{
    public sealed class EmailTemplateService
    {
        private static volatile EmailTemplateService _instance;
        private static readonly object _syncRoot = new object();

        private static readonly Dictionary<string, string> _EmailTemplates = new Dictionary<string, string>();
        private static readonly object _syncDictionary = new object();

        private EmailTemplateService() { }

        public static EmailTemplateService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        _instance = new EmailTemplateService();
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Gets email template from S3 store or uploads template artifact to S3 if it doesn't exist.
        /// This allows us to modify the template without deploying new code.
        /// This does connect to Amazon S3 the first time, so it can block for a while.
        /// The original template is loaded out of the emailTemplates folder
        /// </summary>
        /// <param name="templateFileName">The email template to be inlined</param>
        /// <param name="fileStore">FileStore that contains the emailTemplates folder and templateFileName file</param>
        /// <returns>The inlined email template, nullable</returns>
        public string GetTemplate(string templateFileName, IFileStorage fileStore)
        {
            CheckArgs(templateFileName, fileStore);

            string template;
            lock (_syncDictionary)
            {
                var lookupKey = GetLookupHash(templateFileName,fileStore);
                _EmailTemplates.TryGetValue(lookupKey, out template);
                if (template == null)
                {
                    var fileName = String.Format("EmailTemplates/{0}", templateFileName);

                    Stream fileStream;
                    if (!fileStore.ExistsNoStream(fileName))
                    {
                        // get the file from the resource stream and then upload it to S3
                        fileStream = Assembly.GetExecutingAssembly()
                            .GetManifestResourceStream(String.Format("MAX.BionicReports.Templates.{0}", templateFileName));

                        if (fileStream == null)
                            throw new FileNotFoundException("Email template was not found!");

                        var uploadStream = new MemoryStream();
                        fileStream.CopyTo(uploadStream);
                        fileStore.Write(fileName, uploadStream);
                        fileStream.Position = 0;
                    }
                    else // get the file from S3
                        fileStream = fileStore.Read(fileName);

                    if (fileStream == null)
                        throw new FileNotFoundException("Email template was not found!");

                    var streamReader = new StreamReader(fileStream); // attempts to detect encoding
                    var source = streamReader.ReadToEnd();

                    template = source;

                    _EmailTemplates.Add(lookupKey, template);
                }
            }

            return template;
        }

        public string ReloadTemplate(string templateFileName, IFileStorage fileStore)
        {
            CheckArgs(templateFileName,fileStore);

            var lookupKey = GetLookupHash(templateFileName, fileStore);
            lock (_syncDictionary)
            {
                _EmailTemplates.Remove(lookupKey);
            }
            return GetTemplate(templateFileName, fileStore);
        }

        public void UnloadTemplate(string templateFileName, IFileStorage fileStore)
        {
            CheckArgs(templateFileName, fileStore);

            var lookupKey = GetLookupHash(templateFileName, fileStore);
            lock (_syncDictionary)
            {
                _EmailTemplates.Remove(lookupKey);
            }
        }

        private static string GetLookupHash(string templateFileName, IFileStorage fileStore)
        {
            return fileStore.ContainerName() + templateFileName;
        }

        private static void CheckArgs(string templateFileName, IFileStorage fileStore)
        {
            if (String.IsNullOrEmpty(templateFileName))
            {
                throw new ArgumentNullException("templateFileName");
            }
            if (fileStore == null)
            {
                throw new ArgumentNullException("fileStore");
            }
        }
    }
}
