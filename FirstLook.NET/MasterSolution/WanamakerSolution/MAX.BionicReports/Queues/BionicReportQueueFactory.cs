using AmazonServices.SQS;
using Core.Messaging;
using MAX.Entities.Messages;
using Merchandising.Messages.BionicReport;

namespace MAX.BionicReports.Queues
{
    internal class BionicReportQueueFactory : IBionicReportQueueFactory
    {
        public IQueue<BionicReportDataCollectionMessage> CreateBionicReportDataCollectionQueue()
        {
            return AmazonQueueFactory<BionicReportDataCollectionMessage>.CreateAmazonQueue("Incisent_Merchandising_BionicReportDataCollection");
        }

        public IQueue<BionicReportImageCreationMessage> CreateBionicReportImageCreationQueue()
        {
            return AmazonQueueFactory<BionicReportImageCreationMessage>.CreateAmazonQueue("Incisent_Merchandising_BionicReportImageCreation");
        }

        public IQueue<BionicReportImagesCompleteMessage> CreateBionicReportImagesCompletedQueue()
        {
            return AmazonQueueFactory<BionicReportImagesCompleteMessage>.CreateAmazonQueue("Incisent_Merchandising_BionicReportImagesCreated");
        }

        public IQueue<GroupBionicReportDataCollectionMessage> CreateGroupBionicReportDataCollectionQueue()
        {
            return AmazonQueueFactory<GroupBionicReportDataCollectionMessage>.CreateAmazonQueue("Incisent_Merchandising_GroupBionicReportDataCollection");
        }

        public IQueue<GroupBionicReportCreationMessage> CreateGroupBionicReportCreationQueue()
        {
            return AmazonQueueFactory<GroupBionicReportCreationMessage>.CreateAmazonQueue("Incisent_Merchandising_GroupBionicReportCreation");
        }
    }
}