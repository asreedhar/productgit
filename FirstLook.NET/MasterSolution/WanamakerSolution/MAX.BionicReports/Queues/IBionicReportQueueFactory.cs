using Core.Messaging;
using MAX.Entities.Messages;
using Merchandising.Messages.BionicReport;

namespace MAX.BionicReports.Queues
{
    public interface IBionicReportQueueFactory
    {
        IQueue<BionicReportDataCollectionMessage> CreateBionicReportDataCollectionQueue();
        IQueue<BionicReportImageCreationMessage> CreateBionicReportImageCreationQueue();
        IQueue<BionicReportImagesCompleteMessage> CreateBionicReportImagesCompletedQueue();

        IQueue<GroupBionicReportDataCollectionMessage> CreateGroupBionicReportDataCollectionQueue();
        IQueue<GroupBionicReportCreationMessage> CreateGroupBionicReportCreationQueue();
    }
}