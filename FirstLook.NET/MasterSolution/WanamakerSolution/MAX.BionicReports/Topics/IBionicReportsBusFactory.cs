﻿using Core.Messaging;
using Merchandising.Messages.BionicReport;

namespace MAX.BionicReports.Topics
{
	public interface IBionicReportsBusFactory
	{
		IBus<T, TQ> CreateBionicImagesCompleteBus<T, TQ>() where T : BionicReportImagesCompleteMessage, TQ;
	}
}