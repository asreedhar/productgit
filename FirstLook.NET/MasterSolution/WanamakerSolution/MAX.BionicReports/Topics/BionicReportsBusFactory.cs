﻿using Core.Messaging;
using Merchandising.Messages.BionicReport;

namespace MAX.BionicReports.Topics
{
	internal class BionicReportsBusFactory : IBionicReportsBusFactory
	{
		public IBus<T, TQ> CreateBionicImagesCompleteBus<T, TQ>() where T : BionicReportImagesCompleteMessage, TQ
		{
			return AmazonServices.SNS.BusFactory<T, TQ>.Create("max-bionic-reports-images-complete", '-', true);
		} 
	}
}