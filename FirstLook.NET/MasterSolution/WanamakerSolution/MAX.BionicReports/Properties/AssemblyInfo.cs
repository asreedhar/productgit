﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MAX.BionicReports")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("MAX.BionicReports")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3a10374f-ecb0-46a7-af28-28f24364a316")]

[assembly: InternalsVisibleTo("MAX.BionicReports.Tests")]
