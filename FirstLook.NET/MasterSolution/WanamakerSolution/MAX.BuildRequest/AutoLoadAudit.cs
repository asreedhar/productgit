﻿using System.IO;
using System.Text;
using Core.Messaging;

namespace MAX.BuildRequest
{
    /// <summary>
    /// moved from FetchDotCom library, tureen 2/2/2015
    /// </summary>
    public static class AutoLoadAudit
    {
        public static void Audit(IFileStorage storage, string vin, string xml)
        {
            var contentType = "text/plain";

            // determine the content type
            if(xml.Length > 0)
            {
                if (xml.StartsWith("<"))
                    contentType = "application/xml";
                else if (xml.StartsWith("{"))
                    contentType = "application/json";

            }

            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;
                storage.Write(vin, stream, contentType);
            }
        }

        public static bool Exists(IFileStorage storage, string vin)
        {
            return storage.ExistsNoStream(vin);
        }

        public static string Fetch(IFileStorage storage, string vin)
        {
            string xmlString;

            using (var stream = storage.Read(vin))
            using (var streamReader = new StreamReader(stream, Encoding.UTF8))
            {
                xmlString = streamReader.ReadToEnd();
            }

            return xmlString;
        }

    }
}
