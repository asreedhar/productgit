﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAX.BuildRequest
{
    public class BuildRequestModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BuildRequestRepository>().As<IBuildRequestRepository>();
        }
    }
}
