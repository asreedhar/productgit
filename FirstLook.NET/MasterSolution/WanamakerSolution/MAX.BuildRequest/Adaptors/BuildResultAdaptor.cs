﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Merchandising.Messages.Slurpee;
using MAX.BuildRequest.DataObjects;

namespace MAX.BuildRequest.Adaptors
{
    public class BuildResultAdaptor
    {

        public virtual string Translate(SlurpeeResultMessage slurpeeResultMessage)
        {

            // transfer to the dto we use to serialize to XML
            var vehicle = new ExportVehicle
                {
                    VIN = slurpeeResultMessage.Vehicle.vin,
                    Oem = slurpeeResultMessage.Vehicle.oem,
                    OemSite = slurpeeResultMessage.Vehicle.site,
                    OemModelCode = TranslastModel(slurpeeResultMessage.Vehicle.model),
                    Trim = TranslastTrim(slurpeeResultMessage.Vehicle.trim),
                    Msrp = TranslastMsrp(slurpeeResultMessage.Vehicle.msrp),
                    Options = TranslateOptions(slurpeeResultMessage.Vehicle.options),
                    Transmission = TranslateOption(slurpeeResultMessage.Vehicle.transmission),
                    InteriorColor = TranslateOption(slurpeeResultMessage.Vehicle.int_color),
                    ExteriorColor = TranslateOption(slurpeeResultMessage.Vehicle.ext_color),
                    Engine = TranslateOption(slurpeeResultMessage.Vehicle.engine)
                };

            // put the newly transformed vehicle into the structure to be serialized
            var xmlExportObject = new ExportAgentExecution
                {
                    ExecutionDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                    PlanId = "slurpee",
                    ExecutionPlanId = "?",
                    OemListings = new ExportOemListing
                        {
                            Transactions = new ExportTransaction()
                        }
                };

            xmlExportObject.OemListings.Transactions.Vehciles = new List<ExportVehicle>
                {
                    vehicle
                };


            // 
            string result;

            // create XML serializier ofr export class
            var xmlSerializer = new XmlSerializer(typeof (ExportAgentExecution));

            // setting for xmlWrite
            var xmlWriterSettings = new XmlWriterSettings
                {
                    Encoding = Encoding.UTF8,
                    Indent = true,
                    NewLineHandling = NewLineHandling.Entitize
                
                };

            // create writer with special UTF-8, must be UTF8 with the millions of file already in audit bucket
            using (StringWriter textWriter = new StringWriterWithEncoding(Encoding.UTF8))
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, xmlWriterSettings))
                {
                    // create an empty namespace for the serializer
                    var xmlNameSpace = new XmlSerializerNamespaces();
                    xmlNameSpace.Add("", "");

                    xmlSerializer.Serialize(xmlWriter, xmlExportObject, xmlNameSpace);
                }
                result = textWriter.ToString();
            }

            return result;

        }
        public virtual bool MessageIsValid(SlurpeeResultMessage slurpeeResultMessage)
        {
            bool validMessage = true;

            try
            {
                // no VIN most likely a bad scrap
                if (slurpeeResultMessage.Vehicle.vin == null)
                    validMessage = false;

                if (slurpeeResultMessage.Vehicle.model == null && slurpeeResultMessage.Vehicle.options == null)
                    validMessage = false;


            }
            catch
            {
                validMessage = false;
            }

            return validMessage;
        }
        protected virtual string TranslastMsrp(string msrp)
        {
            string returnValue = null;

            if(msrp != null)
                returnValue = msrp.Replace("$", "").Replace(",", "");

            // make sure it is a good decimal
            decimal msrpTest;

            if (!decimal.TryParse(msrp, out msrpTest))
                returnValue = null;

            return returnValue;

        }
        protected virtual string TranslastTrim(string trim)
        {
            return trim;
        }
        protected virtual string TranslastModel(string model)
        {
            return model;
        }

        protected virtual List<ExportOption> TranslateOptions(List<SlurpeeOption> scrapedOptions)
        {

            var options = new List<ExportOption>();

            if (scrapedOptions != null)
            {
                // resharper got a fancy ... just calling TransferOption for each scrapedOption
                options.AddRange(scrapedOptions.Select(TranslateOption));
            }

            return options;

        }
        protected virtual ExportOption TranslateOption(SlurpeeOption scrapedOption)
        {

            var option = new ExportOption();

            if (scrapedOption != null)
            {
                option.Code = scrapedOption.code;
                option.Description = scrapedOption.description;
            }

            return option;

        }

        /// <summary>
        /// This class was "borrowed" from the MerchandisingDomainModel project in order to break the dependecy on that project.  
        /// If we fix this class, the other class might need fixing too, or even better,  we could just consolidate the classes 
        /// into some common low-lying project that both projects can reference.
        /// dhillis - 2014-10-07
        /// </summary>
        public class StringWriterWithEncoding : System.IO.StringWriter
        {
            private readonly Encoding _encoding;

            public StringWriterWithEncoding(Encoding encoding)
            {
                _encoding = encoding;
            }

            public override Encoding Encoding
            {
                get { return _encoding ?? base.Encoding; }
            }
        }
    }
}
