using System;
using System.Collections.Generic;
using Core.Messaging;
using MAX.BuildRequest.Adaptors;
using MAX.BuildRequest.DataObjects;
using MAX.BuildRequest.VehicleDataProviders;
using MAX.ExternalCredentials;
using Merchandising.Messages;
using Merchandising.Messages.Slurpee;

namespace MAX.BuildRequest
{
    public interface IBuildRequestRepository
    {
        int LogBuildRequest(int businessUnit, string vin, Agent site, Status status, int? credentialBusinessUnit, string memberLogin);
        BuildResultAdaptor GetBuildResultAdaptor(int agent);
        List<InventoryIdType> GetInventoryIdType(int businessUnitId, string vin);
        void SetAutoloadStart(int businessUnitId, int inventoryId, string memberLogin);
        void SetAutoloadEnd(int businessUnitId, int inventoryId, Status status, string memberLogin);
        void SetAutoloadEnd(int businessUnitId, string vin, Status status, string memberLogin);
        string SendSlurpeeRequest(SlurpeeRequestMessage slurpeeRequest);
	    Dictionary<int, bool> AsynchronousSiteSupportSettings(int businessUnitId);
        string GetSlurpeeCallbackUrl(string baseUrl, string controller, Guid hash, byte[] version);
        Agent DetermineAgent(string fileConent);
        bool LoadDataExists(IFileStorage storage, string vin);
        bool IsValidBuildRequest(string xmlString);
        IVehicleDataProvider GetVehicleDataProvider(Agent site);
        string SendSlurpeeRequest(int businessUnitId, string vinList, EncryptedSiteCredential encryptedSiteCredential);

    }
}