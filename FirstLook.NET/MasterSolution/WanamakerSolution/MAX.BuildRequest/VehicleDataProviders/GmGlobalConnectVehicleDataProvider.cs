﻿using System;
using Merchandising.Messages;
using Merchandising.Messages.VehicleBuild;

namespace MAX.BuildRequest.VehicleDataProviders
{
    public class GmGlobalConnectVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public GmGlobalConnectVehicleDataProvider()
        {
            ManufacturerName = "General Motors";
            Site = Agent.GmGlobalConnect;
            SiteDescription = "General Motors";
        }

        /// <summary>
        /// Additional test on GM data
        /// 1. - if no options, the build data is not valid
        /// </summary>
        /// <param name="vehicleBuildData"></param>
        /// <returns></returns>
        public override bool IsValid(VehicleBuildData vehicleBuildData)
        {
            bool isValid = base.IsValid(vehicleBuildData);

            // check for the crazy model code, "ZZUSV"?
            // this is a message from GM that the vehicle is not found ... and will not be found ... ever
            if (vehicleBuildData.OemModelCode.StartsWith("ZZUSV", StringComparison.CurrentCultureIgnoreCase))
            {
                isValid = true;
            }
            else
            {
                // any options?
                if (vehicleBuildData.UnfilteredOptionPackages.Count < 1)
                {
                    isValid = false;
                    Log.Debug("Build data did not pass validation because it contains no options");
                }

                // test for empty model code
                if (string.IsNullOrWhiteSpace(vehicleBuildData.OemModelCode))
                {
                    isValid = false;
                    Log.Debug("Build data did not pass validation because the model code is blank");
                }
                
            }

            return isValid;
        }
    }

}