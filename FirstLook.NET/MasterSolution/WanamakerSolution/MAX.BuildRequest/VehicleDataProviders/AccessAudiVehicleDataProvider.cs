﻿using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    public class AccessAudiVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public AccessAudiVehicleDataProvider()
        {
            ManufacturerName = "Audi";
            Site = Agent.AccessAudi;
            SiteDescription = "Audi";

        }
    }
}
