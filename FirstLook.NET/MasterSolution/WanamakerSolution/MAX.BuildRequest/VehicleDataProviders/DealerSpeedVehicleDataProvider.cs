﻿using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    internal class DealerSpeedVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public DealerSpeedVehicleDataProvider()
        {
            ManufacturerName = "BMW";
            Site = Agent.DealerSpeed;
            SiteDescription = "BMW/MINI";
        }
    }

}