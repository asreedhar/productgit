﻿using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    public class HyundaiTechInfoVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public HyundaiTechInfoVehicleDataProvider()
        {
            ManufacturerName = "Hyundai";
            Site = Agent.HyundaiTechInfo;
            SiteDescription = "Hyundai";
        }
    }
}
