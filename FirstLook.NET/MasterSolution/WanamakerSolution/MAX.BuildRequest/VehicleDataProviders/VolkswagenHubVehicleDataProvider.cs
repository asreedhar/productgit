﻿using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    internal class VolkswagenHubDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public VolkswagenHubDataProvider()
        {
            ManufacturerName = "Volkswagen";
            Site = Agent.VolkswagenHub;
            SiteDescription = "Volkswagen";
        }
    }

}