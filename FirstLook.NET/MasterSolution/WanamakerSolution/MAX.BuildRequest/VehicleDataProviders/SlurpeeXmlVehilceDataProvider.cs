﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using FetchDotComClient;
using Merchandising.Messages;
using Merchandising.Messages.VehicleBuild;
using VehicleBuildData = Merchandising.Messages.VehicleBuild.VehicleBuildData;
using OptionPackage = Merchandising.Messages.VehicleBuild.OptionPackage;
using FetchVehicleResponse = FetchDotComClient.VehicleBuild.VehicleResponse;
using FetchVehicleBuildData = FetchDotComClient.VehicleBuild.VehicleBuildData;



namespace MAX.BuildRequest.VehicleDataProviders
{
    public class SlurpeeXmlVehilceDataProvider : VehicleDataProvider
    {

        public override VehicleBuildData GetVehicleBuildData(string fileContent)
        {

            // totally fetch based old school.  
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(fileContent);

            FetchVehicleResponse response = FetchDotComAgent.GetVehicleBuildDataFromFetchXml(xmlDocument);

            var result = ProcessResult(response);  // the result is the max's response NOT fetchDotCom's

            if (result.Status == Status.Successful)
            {
                return result.BuildData;
            }

            return null;
        }

        public VehicleBuildResult ProcessResult(FetchVehicleResponse response)
        {

            // FB: 30552 - moved credential updates to fetch process

            // Deal with the results from the robot
            switch (response.ResponseStatus)
            {
                case Status.Successful:
                    // Good deal - save credentials and adapt the fetch data
                    //if (SiteCredential != null) // only save with good credentials, might be from audit file
                    //    SiteCredential.Save();
                    var buildData = GetVehicleBuildDataFromFetchBuildData(response.VehicleBuildData);
                    return new VehicleBuildResult(Status.Successful, buildData);

                case Status.InsufficientPermissions:
                    // Insufficient Permission to retrieve vehicle data
                    //SetLockout(true);
                    return new VehicleBuildResult(Status.InsufficientPermissions, null);

                case Status.InvalidCredentials:
                    // Bad Credentials - lockout this account
                    //SetLockout(true);
                    return new VehicleBuildResult(Status.InvalidCredentials, null);

                case Status.NoVehicleFound:
                    // No Vehicle Found, but the credentials worked so save credentials
                    //if (SiteCredential != null) // only save with good credentials, might be from audit file
                    //    SiteCredential.Save();
                    return new VehicleBuildResult(Status.NoVehicleFound, null);

                case Status.UnknownError: // Unaccounted error
                    return new VehicleBuildResult(Status.UnknownError, null);

                default:
                    throw new ApplicationException("Unhandled error processing robot.");
            }
        }

        /// <summary>
        /// stole this from the old vehicleDataProvider
        /// basically converts from FetchDotCom vehicleBuild to Max vehicleBuild
        /// 2/28/2015 - removed white list functionality and moved to vehicleAutoLoader so options can be 
        ///             matched properly (might need to do this for colors too)
        /// </summary>
        /// <param name="fetchBuildData"></param>
        /// <returns></returns>
        protected VehicleBuildData GetVehicleBuildDataFromFetchBuildData(FetchVehicleBuildData fetchBuildData)
        {
            //if interior color name is empty we should use the description.
            string interiorColor = string.Empty;
            if (fetchBuildData.InteriorColor != null)
            {
                interiorColor = String.IsNullOrEmpty(fetchBuildData.InteriorColor.Name)
                                           ? fetchBuildData.InteriorColor.Description
                                           : fetchBuildData.InteriorColor.Name;
            }

            var vehicleBuildData = new VehicleBuildData
            {
                OptionPackages = new List< OptionPackage>(),
                UnfilteredOptionPackages = new List<OptionPackage>(),
                InteriorColor = fetchBuildData.InteriorColor == null ? null : interiorColor,
                InteriorColorCode = fetchBuildData.InteriorColor == null ? null : fetchBuildData.InteriorColor.OemCode,
                ExteriorColor = fetchBuildData.ExteriorColor == null ? null : fetchBuildData.ExteriorColor.Description,
                ExteriorColorCode = fetchBuildData.ExteriorColor == null ? null : fetchBuildData.ExteriorColor.OemCode,
                EstimatedMSRP = fetchBuildData.EstimatedMSRP.GetValueOrDefault(),
                OemModelCode = fetchBuildData.OemModelCode
            };


            // The wite list instructs us to ignore certain options for the purposes of ad generation.
            //var whiteList = new HashSet<string>(GetOptionsWhiteList(), StringComparer.InvariantCultureIgnoreCase);

            // We can't do this until we know the style.
            var colorOptionCodeKindList = new HashSet<FirstLook.Common.Core.Tuple<string, int>>();
            foreach (var colorOptionCode in GetColorOptionCodeList(fetchBuildData.Vin))
            {
                colorOptionCodeKindList.Add(new FirstLook.Common.Core.Tuple<string, int>(colorOptionCode.First.ToLower(), colorOptionCode.Second));
            }

            // convert option packages
            foreach (var optionPackage in fetchBuildData.OptionPackages)
            {
                // The Unfiltered list of packages should contain everything we got from fetch.
                vehicleBuildData.UnfilteredOptionPackages.Add(new OptionPackage(optionPackage.OemCode,
                                                                                optionPackage.Description));


                if (optionPackage.OemCode == null)
                    Log.Debug("option package had null property: OemCode");
                if (optionPackage.Description == null)
                    Log.Debug("option package had null property: Description");
                //if (!whiteList.Contains(optionPackage.OemCode))
                //{
                //    // this doesn't seem very interesting.
                //    Log.DebugFormat("White list doesn't contain option '{0}'", optionPackage.OemCode);
                //}

                Regex extColorMatchRegex = new Regex("^[0-9]{2}U$", RegexOptions.IgnoreCase);
                Regex intColorMatchRegex = new Regex("^[0-9]{2}I$", RegexOptions.IgnoreCase);

                string oemCode = optionPackage.OemCode.ToLower() ?? "";

                if (
                    // Only set the color for the first oemCode found.
                    string.IsNullOrEmpty(vehicleBuildData.ExteriorColorCode)
                    && colorOptionCodeKindList != null && colorOptionCodeKindList.Count > 0 && colorOptionCodeKindList.Any(
                        c => (
                            c.First == oemCode
                            || (extColorMatchRegex.IsMatch(oemCode)
                                && colorOptionCodeKindList.Any(c2 => c2.First == oemCode.Substring(0, 2)))
                            )
                            && c.Second == 0
                        )
                    )
                {
                    vehicleBuildData.ExteriorColorCode = optionPackage.OemCode;
                    vehicleBuildData.ExteriorColor = optionPackage.Description;
                }
                else if (
                    // Only set the color for the first oemCode found.
                    string.IsNullOrEmpty(vehicleBuildData.InteriorColorCode)
                    && colorOptionCodeKindList != null && colorOptionCodeKindList.Count > 0 && colorOptionCodeKindList.Any(
                        c => (
                            c.First == oemCode
                            || (intColorMatchRegex.IsMatch(oemCode)
                                && colorOptionCodeKindList.Any(c2 => c2.First == oemCode.Substring(0, 2)))
                            )
                            && c.Second == 1
                        )
                    )
                {
                    vehicleBuildData.InteriorColorCode = optionPackage.OemCode;
                    vehicleBuildData.InteriorColor = optionPackage.Description;
                }
                //else if (whiteList == null || whiteList.Count == 0 || whiteList.Contains(optionPackage.OemCode))
                //{
                //    // add the options that are in our whitelist to the OptionPackages 
                //    vehicleBuildData.OptionPackages.Add(new OptionPackage(optionPackage.OemCode, optionPackage.Description));
                //}
                // It would be very conveient to simply OR the above with any options that have an MSRP > 0, but we can't do that yet
                // since we don't know the styleId.
            }

            return vehicleBuildData;
        }





    }
}
