﻿using System.Collections.Generic;
using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    public class DealerDailyVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public DealerDailyVehicleDataProvider()
        {
            ManufacturerName = "Toyota";
            Site = Agent.NatDealerDaily;
            SiteDescription = "Lexus/Toyota National";
        }
    }
}
