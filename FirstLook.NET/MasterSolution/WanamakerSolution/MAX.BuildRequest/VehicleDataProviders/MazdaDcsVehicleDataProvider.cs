﻿using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    internal class MazdaDcsVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public MazdaDcsVehicleDataProvider()
        {
            ManufacturerName = "Mazda";
            Site = Agent.MazdaDcs;
            SiteDescription = "Mazda";
        }
    }

}