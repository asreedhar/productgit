﻿using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    public class NissanVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public NissanVehicleDataProvider()
        {
            ManufacturerName = "Nissan";
            Site = Agent.NissanNorthAmerica;
            SiteDescription = "Nissan/Infiniti";

        }
    }
}
