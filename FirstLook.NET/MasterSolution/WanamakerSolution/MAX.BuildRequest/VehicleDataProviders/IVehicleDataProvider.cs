using System.Collections.Generic;
using Merchandising.Messages;
using Merchandising.Messages.VehicleBuild;
using VehicleBuildData = Merchandising.Messages.VehicleBuild.VehicleBuildData;

namespace MAX.BuildRequest.VehicleDataProviders
{
    public interface IVehicleDataProvider
    {
        VehicleBuildData GetVehicleBuildData(string vin, int inventoryType, string processName);
        //VehicleBuildResult ProcessResult(VehicleResponse response);
        //bool RequiresLogin { get; }
        //EncryptedSiteCredential SiteCredential { get; set; }
        Agent Site { get; }
        //bool HasUsableCredential{ get;}
        //bool HasCredentials { get; }
        string SiteDescription { get; }
		string ManufacturerName { get; }
        VehicleBuildData GetVehicleBuildData(string fileContent);
        bool IsValid(string fileConent);
        bool IsValid(VehicleBuildData vehicleBuildData);
        List<string> GetOptionsWhiteList(int manufacturer, int division);
        OptionFilter GetOptionFilters(int manufacturer);

    }
    
}