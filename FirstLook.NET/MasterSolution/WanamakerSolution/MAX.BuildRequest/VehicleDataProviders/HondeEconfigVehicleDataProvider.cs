﻿using System.Linq;
using System.Web.Script.Serialization;
using MAX.BuildRequest.DataObjects;
using Merchandising.Messages;
using Merchandising.Messages.VehicleBuild;

namespace MAX.BuildRequest.VehicleDataProviders 
{
    public class HondeEconfigVehicleDataProvider : VehicleDataProvider
    {

        public HondeEconfigVehicleDataProvider()
        {
            ManufacturerName = "Honda Motors";
            Site = Agent.HondaEconfig;
            SiteDescription = "Honda/Acura Econfig";
        }

        public override VehicleBuildData GetVehicleBuildData(string fileContent)
        {
            // holds converted data
            var buildData = new VehicleBuildData();

            // load this sucker up
            var javaScriptSerializer = new JavaScriptSerializer();

            var siteConfig = javaScriptSerializer.Deserialize<HondaEconfig>(fileContent);

            if (siteConfig != null && siteConfig.vins != null)
            {

                // model code
                buildData.OemModelCode = siteConfig.vins.vin.model.id;

                // exterior color
                var exteriorColor = (from color in siteConfig.vins.vin.model.colors.color
                                     where color.color_type == "E"
                                     select (color)).ToList();

                if (exteriorColor.Count() == 1)
                {
                    buildData.ExteriorColorCode = exteriorColor.First().color_cd;
                    buildData.ExteriorColor = exteriorColor.First().name;
                }

                // interior color
                var interiorColor = (from color in siteConfig.vins.vin.model.colors.color
                                     where color.color_type == "I"
                                     select (color)).ToList();

                if (interiorColor.Count() == 1)
                {
                    buildData.InteriorColorCode = interiorColor.First().color_cd;
                    buildData.InteriorColor = interiorColor.First().name;
                }

                // trim
                buildData.Trim = siteConfig.vins.vin.model.trim_name;

                // options -- sadly no options for honad/acura
            }

            return buildData;
        }

        public override bool IsValid(VehicleBuildData vehicleBuildData)
        {
            
            // check base first
            var valid = base.IsValid(vehicleBuildData);

            if(valid)
            {

                // make sure we have at least a model code
                if (string.IsNullOrWhiteSpace(vehicleBuildData.OemModelCode))
                    valid = false;
            }

            return valid;
        }


    }
}
