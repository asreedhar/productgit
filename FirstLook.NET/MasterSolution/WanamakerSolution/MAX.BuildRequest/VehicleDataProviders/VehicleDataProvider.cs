﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using FirstLook.Common.Core.Data;
using log4net;
using Merchandising.Messages;
using Merchandising.Messages.VehicleBuild;

namespace MAX.BuildRequest.VehicleDataProviders
{
    public abstract class VehicleDataProvider : IVehicleDataProvider
    {

        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(typeof(VehicleDataProvider).FullName);

        #endregion

        public Agent Site { get; set; }

        public string ManufacturerName { get; internal set; }

        public string SiteDescription { get; internal set; }

        public List<string> OptionWhiteList { get; set; }

        public List<FirstLook.Common.Core.Tuple<string, int>> ColorOptionCodeList { get; set; }
        
        
        public virtual VehicleBuildData GetVehicleBuildData(string vin, int inventoryType, string processName)
        {
            throw new NotImplementedException("Old FetchDotCom call");

            return null;
        }


        /// <summary>
        /// this should return vehicle build data from the content of a build request
        /// </summary>
        /// <param name="fileContent">content from file as a string</param>
        /// <returns></returns>
        public virtual VehicleBuildData GetVehicleBuildData(string fileContent)
        {

            throw new NotImplementedException("Should process a file sent as a string to vehicleBuildData");

            return null;
        }

        public virtual bool IsValid(string fileConent)
        {
            return IsValid(GetVehicleBuildData(fileConent));
        }

        public virtual bool IsValid(VehicleBuildData vehicleBuildData)
        {
            // not really much of a test.  made mostly for descendants
            if (vehicleBuildData == null)
                return false;

            return true;
        }


        /// <summary>
        /// Return the options "white list" for this manufacturer.
        /// added the return of optionWhiteList to make this a little more unit testable
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerable<string> GetOptionsWhiteList()
        {

            var list = new List<string>();

            if (OptionWhiteList != null)
                list = OptionWhiteList;
            else
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
                {
                    conn.Open();

                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[dbo].[ManufacturerVehicleOptionCodeWhiteList#Fetch]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.AddParameter("@Manufacturer", DbType.String, ManufacturerName);
                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string code = reader.GetString(0).ToLower();
                                list.Add(code);
                            }
                        }
                    }
                }

            }

            return list;
        }

        /// <summary>
        /// Get the list of white listed options for a specific manufacture and division
        /// </summary>
        /// <param name="manufacturer">manufacturer id from chrome</param>
        /// <param name="division">division id from chrome</param>
        /// <returns></returns>
        public virtual List<string> GetOptionsWhiteList(int manufacturer, int division)
        {
            
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                            SELECT OptionCode 
                                FROM [dbo].[ManufacturerVehicleOptionCodeWhiteList] 
                                WHERE VehicleManufacturerID = @ManufacturerID 
                                AND VehicleMakeID = @MakeID
                                ORDER BY OptionCode 
                            ";

                var list = sqlConnection.Query<String>(query, new {@ManufacturerID = manufacturer, @MakeID = division}).ToList();
                sqlConnection.Close();

                return list;
            }

        }

        public virtual OptionFilter GetOptionFilters(int manufacturer)
        {

            var optionFilter = new OptionFilter();

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                using(var cmd = new SqlCommand())
                {
                    cmd.Connection = sqlConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[settings].[OptionFilters#Fetch]";
                    cmd.Parameters.AddWithValue("@ManufactureId", manufacturer);

                    using(var dataReader = cmd.ExecuteReader())
                    {
                        // minimumMSRP
                        if (dataReader.Read())
                        {
                            optionFilter.MinumumMsrp = Convert.ToDecimal(dataReader["MinimumMsrp"]);
                        }

                        // includePhrases
                        dataReader.NextResult();
                        while (dataReader.Read())
                        {
                            optionFilter.IncludePhrases.Add(dataReader["IncludePhrase"].ToString());
                        }

                        // excludePhrases
                        dataReader.NextResult();
                        while (dataReader.Read())
                        {
                            optionFilter.ExcludePhrases.Add(dataReader["ExcludePhrase"].ToString());
                        }
                    }
                }

                sqlConnection.Close();

                return optionFilter;
            }
            
        }


        /// <summary>
        /// added the return of ColorOptionCodeList to make this a little more unit testable
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerable<FirstLook.Common.Core.Tuple<string, int>> GetColorOptionCodeList(string vin)
        {
            var list = new List<FirstLook.Common.Core.Tuple<string, int>>();

            if (ColorOptionCodeList != null)
                list = ColorOptionCodeList;
            else
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
                {
                    conn.Open();

                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "chrome.ColorOptionCode#LookupByVin";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.AddParameter("@vin", DbType.String, vin);

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string code = ((string)reader["OptionCode"]).ToLower();// ..GetString( 0 ).ToLower();
                                var kind = ((int)reader["OptionKind"]);//.GetInt32( 1 );
                                list.Add(new FirstLook.Common.Core.Tuple<string, int>(code, kind));
                            }
                        }
                    }
                }
            }

            return list;
        }

    }
}
