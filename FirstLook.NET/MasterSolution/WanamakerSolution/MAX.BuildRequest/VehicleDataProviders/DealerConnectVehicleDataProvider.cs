﻿using Merchandising.Messages;

namespace MAX.BuildRequest.VehicleDataProviders
{
    internal class DealerConnectVehicleDataProvider : SlurpeeXmlVehilceDataProvider
    {
        public DealerConnectVehicleDataProvider()
        {
            ManufacturerName = "Chrysler";
            Site = Agent.DealerConnect;
            SiteDescription = "Chrysler";
        }
    }

}