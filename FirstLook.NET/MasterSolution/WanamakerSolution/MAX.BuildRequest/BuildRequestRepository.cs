﻿using System;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using AmazonServices.SNS;
using Core.Messaging;
using Dapper;
using MAX.BuildRequest.Adaptors;
using MAX.BuildRequest.DataObjects;
using MAX.BuildRequest.VehicleDataProviders;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.Slurpee;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using MAX.ExternalCredentials;


namespace MAX.BuildRequest
{
    public class BuildRequestRepository : IBuildRequestRepository
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public BuildResultAdaptor GetBuildResultAdaptor(int agent)
        {
            return new BuildResultAdaptor(); // all the same right now!!!
        }

        public int LogBuildRequest(int businessUnit, string vin, Agent site, Status status, int? credentialBusinessUnit, string memberLogin)
        {

            var buildRequestLog = new BuildRequestLog
            {
                BusinessUnitId = businessUnit,
                VIN = vin,
                SiteId = site,
                CredentialBusinessUnitId = credentialBusinessUnit,
                StatusTypeId = status,
                RequestedBy = memberLogin
            };

            return buildRequestLog.Save();

        }

        public List<InventoryIdType> GetInventoryIdType(int businessUnitId, string vin)
        {

            // get the inv and type to relate back to MAX
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT TOP 1 InventoryID, InventoryType
	                        FROM workflow.Inventory
	                        WHERE businessUnitID = @BusinessUnitId
	                        AND vin = @vin
                    ";

                var vehicle = sqlConnection.Query<InventoryIdType>(query, new { BusinessUnitId = businessUnitId, Vin = new DbString { Value = vin, IsAnsi = true, Length = 17 } });
                sqlConnection.Close();

                return vehicle.ToList();
            }

        }

        public void SetAutoloadStart(int businessUnitId, int inventoryId, string memberLogin)
        {

            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#Save";

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("BusinessUnitId", businessUnitId);
                    cmd.Parameters.AddWithValue("InventoryId", inventoryId);
                    cmd.Parameters.AddWithValue("StatusTypeId", Status.Pending);
                    //cmd.Parameters.AddWithValue("startTime", null);
                    //cmd.Parameters.AddWithValue("endTime", null);
                    cmd.Parameters.AddWithValue("MemberLogin", memberLogin);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void SetAutoloadEnd(int businessUnitId, int inventoryId, Status status, string memberLogin)
        {


            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "builder.AutoloadStatus#SetEnd";

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("BusinessUnitId", businessUnitId);
                    cmd.Parameters.AddWithValue("InventoryId", inventoryId);
                    cmd.Parameters.AddWithValue("StatusTypeId", status);
                    cmd.Parameters.AddWithValue("MemberLogin", memberLogin);

                    cmd.ExecuteNonQuery();
                }
            }


        }

        public void SetAutoloadEnd(int businessUnitId, string vin, Status status, string memberLogin)
        {

            // get all vehicles at this businessUnit
            var vehicles = GetInventoryIdType(businessUnitId, vin);

            foreach (var vehicle in vehicles)
            {
                try
                {
                    // set them all.  don't worry about errors
                    SetAutoloadEnd(businessUnitId, vehicle.InventoryId, status, memberLogin);    
                }
                catch(Exception ex)
                {
                    Log.Error(string.Format("Error setting AutoloadEnd for VIN:{0}", vin), ex); 
                }
            }

        }

        public string SendSlurpeeRequest(SlurpeeRequestMessage slurpeeRequest)
        {
            var json = new JavaScriptSerializer().Serialize(slurpeeRequest);

            var topicArn = GenericTopic.CreateTopic("slurpee-manufacturer-request", true, true, '-');
            string messageId = GenericTopic.Publish(json, topicArn);

            return messageId;
        }
        public string SendSlurpeeRequest(int businessUnitId, string vin, EncryptedSiteCredential encryptedSiteCredential)
        {

            string messageId = null;

            // slurpee credential call back URL
            var baseUrl = ConfigurationManager.AppSettings["credentialServiceBaseUrl"];

            if (string.IsNullOrWhiteSpace(baseUrl))
            {
                Log.Error("AutoLoadNewInventoryTask: cannot call slurpee request because the 'credentialServiceBaseUrl' is not set in the app.config");
            }
            else
            {

                messageId = SendSlurpeeRequest(new SlurpeeRequestMessage
                                    {
                                        bu_id = businessUnitId,
                                        site_id = (int)encryptedSiteCredential.SiteId,
                                        vin = vin,
                                        pass_word = encryptedSiteCredential.Password,
                                        user_name = encryptedSiteCredential.UserName,
                                        correlation_id = Guid.NewGuid().ToString(),
                                        dealer_code = encryptedSiteCredential.DealerCode,
                                        creds_fail_webhook = GetSlurpeeCallbackUrl(baseUrl, "fail", encryptedSiteCredential.CredentialHash, encryptedSiteCredential.CredentialVersion),
                                        creds_pass_webhook = GetSlurpeeCallbackUrl(baseUrl, "pass", encryptedSiteCredential.CredentialHash, encryptedSiteCredential.CredentialVersion),
                                        creds_proceed_webhook = GetSlurpeeCallbackUrl(baseUrl, "status", encryptedSiteCredential.CredentialHash, encryptedSiteCredential.CredentialVersion)
                                    });

            }

            return messageId;



        }


        public Dictionary<int, bool> AsynchronousSiteSupportSettings(int businessUnitId)
	    {
			// Setup default values for all Agents.
			var settingsDict = Enum.GetValues(typeof (Agent))
				.Cast<int>()
				.ToDictionary(agent => agent, agent => false);

			//const string sql = @"settings.SiteCredentialsBuildRequestType#Fetch";
	        const string sql = @"SELECT DISTINCT Dealer_SiteCredentials.siteId, Dealer_SiteCredentials.buildRequestType
	                                FROM workflow.Inventory Inventory
	                                INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON Inventory.Make = Divisions.DivisionName 
		                                AND Divisions.CountryCode = 1
	                                INNER JOIN merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings ON Divisions.ManufacturerID = VehicleAutoLoadSettings.ManufacturerID
	                                INNER JOIN settings.Dealer_SiteCredentials Dealer_SiteCredentials ON VehicleAutoLoadSettings.siteId = Dealer_SiteCredentials.siteId 
		                                AND Inventory.BusinessUnitID = Dealer_SiteCredentials.businessUnitId
	                                INNER JOIN settings.Merchandising Merchandising ON Inventory.BusinessUnitID = merchandising.businessUnitID
	                                WHERE Inventory.BusinessUnitID = @BusinessUnitId
	                                AND buildRequestType  = 2 -- slurpee
	                                AND Dealer_SiteCredentials.siteId IN (1,2) -- GM, BMW";
		    using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
		    {
				sqlConnection.Open();

				//var results = sqlConnection.Query <SettingsQueryResults>(sql, new { BusinessUnitId = businessUnitId }, commandType: CommandType.StoredProcedure).ToArray();
				var results = sqlConnection.Query<SettingsQueryResults>(sql, new { BusinessUnitId = businessUnitId }, commandType: CommandType.Text).ToArray();
			    if (results.Any())
			    {
				    foreach (var res in results)
				    {
					    if (settingsDict.ContainsKey(res.SiteId))
					    {
						    settingsDict[res.SiteId] = res.BuildRequestType == 2; // 2 = is Slurpee, supports async autoload
					    }
					    else
					    {
							settingsDict.Add(res.SiteId, res.BuildRequestType == 2); // 2 = is Slurpee, supports async autoload
					    }
				    }
			    }
		    }

		    return settingsDict;
	    }
        
		// ReSharper disable ClassNeverInstantiated.Local
	    private class SettingsQueryResults
		// ReSharper restore ClassNeverInstantiated.Local
		{
			// ReSharper disable FieldCanBeMadeReadOnly.Local
			public int SiteId;
			public int BuildRequestType;
			// ReSharper restore FieldCanBeMadeReadOnly.Local
		    protected SettingsQueryResults(int siteId, byte buildRequestType)
		    {
			    SiteId = siteId;
			    BuildRequestType = buildRequestType;
		    }
		}

        public string GetSlurpeeCallbackUrl(string baseUrl, string controller, Guid hash, byte[] version)
        {
            // check base url for the last backslash
            baseUrl = baseUrl.Trim();
            if (!baseUrl.EndsWith(@"\"))
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);

            string sUrl = @"{0}/{1}/{2}/{3}";

            string credentialHash = HttpUtility.UrlEncode(hash.ToString());
            string credentialVersion = HttpUtility.UrlEncode(BitConverter.ToUInt64(version, 0).ToString(CultureInfo.InvariantCulture));

            sUrl = string.Format(sUrl, baseUrl, controller, credentialHash, credentialVersion);

            return sUrl;

        }

        public bool LoadDataExists(IFileStorage storage, string vin)
        {
            bool goodDataExists = false;

            if (AutoLoadAudit.Exists(storage, vin))
            {
                string xmlString = AutoLoadAudit.Fetch(storage, vin);
                if (!string.IsNullOrWhiteSpace(xmlString))
                {
                    goodDataExists = IsValidBuildRequest(xmlString);
                }
            }

            return goodDataExists;
        }

        public bool IsValidBuildRequest(string xmlString)
        {

            bool isValid;

            try
            {
                // determine Agent from the content of the file
                var agent = DetermineAgent(xmlString);

                // create vehicleDataProvider based on the agent (site)
                var vehicleDataProvider = GetVehicleDataProvider(agent);

                // with the appropriate vehicleDataProvider
                isValid = vehicleDataProvider.IsValid(xmlString);

            }
            catch (Exception ex)
            {
                Log.Error("Error extracting build data from file", ex);
                isValid = false;
            }
            
            return isValid;
        }

        public IVehicleDataProvider GetVehicleDataProvider(Agent site)
        {
            switch (site)
            {
                case Agent.DealerSpeed:
                    return new DealerSpeedVehicleDataProvider { Site = site };
                case Agent.GmGlobalConnect:
                    return new GmGlobalConnectVehicleDataProvider { Site = site };
                case Agent.DealerConnect:
                    return new DealerConnectVehicleDataProvider { Site = site };
                case Agent.VolkswagenHub:
                    return new VolkswagenHubDataProvider { Site = site };
                case Agent.MazdaDcs:
                    return new MazdaDcsVehicleDataProvider { Site = site };
                case Agent.AccessAudi:
                    return new AccessAudiVehicleDataProvider { Site = site };
                case Agent.HyundaiTechInfo:
                    return new HyundaiTechInfoVehicleDataProvider { Site = site };
                case Agent.NatDealerDaily:
                    return new DealerDailyVehicleDataProvider { Site = site };
                case Agent.SetDealerDaily:
                    return new DealerDailyVehicleDataProvider { Site = site };
                case Agent.HondaEconfig:
                    return new HondeEconfigVehicleDataProvider { Site = site };
                case Agent.NissanNorthAmerica:
                    return new NissanVehicleDataProvider { Site = site };

                default:
                    throw new ApplicationException(string.Format("No provider available for agent:{0}", site));
            }

        }

        /// <summary>
        /// Determines the type of Agent was used get the build request
        /// this totally assumes it is an XML in Connotate format!!!!!!, need to change soon
        /// </summary>
        /// <param name="fileConent">the content of the file</param>
        /// <returns></returns>
        public Agent DetermineAgent(string fileConent)
        {
            var agent = Agent.Undefined;

            fileConent = fileConent.Trim();

            if(fileConent.Length > 1)
            {
                if(fileConent.StartsWith("<"))
                {
                    // load file to a doc
                    XContainer xDocument = XDocument.Parse(fileConent);

                    // find OEM_Site
                    var siteElement = (from site in xDocument.Descendants("OEM_Site")
                                       select site).ToList();

                    if (siteElement.Count != 0)
                    {
                        // big ole case statement
                        var oemSite = siteElement.First().Value.ToLower();

                        switch (oemSite)
                        {
                            case "dealerspeed":
                            case "dealerspeed.net":
                                agent = Agent.DealerSpeed; // BMW
                                break;

                            case "gm-vis":
                            case "autopartners.net":
                                agent = Agent.GmGlobalConnect; //GM
                                break;

                            case "dealerconnect":
                                agent = Agent.DealerConnect; // Chrysler
                                break;

                            case "vwhub.com":
                                agent = Agent.VolkswagenHub; // VW
                                break;

                            case "portal.mazdausa.com":
                                agent = Agent.MazdaDcs; // Mazda
                                break;

                            case "accessaudi.com":
                                agent = Agent.AccessAudi;  // audi
                                break;

                            case "webdcs.hyundaidealer.com":
                                agent = Agent.HyundaiTechInfo;  // hyundai
                                break;

                            case "dealerdaily.toyota.com":
                            case "dealerdaily.lexus.com":
                                agent = Agent.NatDealerDaily;  // toyota national
                                break;

                            case "pilota.setdealerdaily.com":
                                agent = Agent.SetDealerDaily;
                                break;

                            case "nnanet.com":
                                agent = Agent.NissanNorthAmerica;
                                break;

                            default:
                                agent = Agent.Undefined;
                                Log.WarnFormat("Cannot determine agent for OEM_SITE: {0}", oemSite);
                                break;

                            // fordvehicleremaketing
                            // fmcdealer
                        }
                    }
                    else
                    {
                        Log.Warn("No OEM_Site segment found in XML");
                    }

                }
                else if(fileConent.StartsWith("{"))
                {
                    // find data source within the JSON document
                    var dataSource = FindDataSource(fileConent);

                    if (!String.IsNullOrWhiteSpace(dataSource))
                    {
                        switch (dataSource.Trim().ToLower())
                        {
                            case "hondaeconfig":
                                agent = Agent.HondaEconfig;
                                break;
                            default:
                                agent = Agent.Undefined;
                                Log.WarnFormat("Cannot determine agent for DataSource: {0}", dataSource);
                                break;
                        }

                    }
                    else
                    {
                        Log.WarnFormat("Cannot find envelope in JSON: {0}", fileConent);
                    }

                }
                else
                    Log.WarnFormat("Cannot determine agent for : {0}", fileConent);

            }


            // if things are going bad check OEM for default?

            return agent;

        }

        private string FindDataSource(string json)
        {

            string dataSource = null;

            try
            {
                // try to deserialize
                var javaScriptSerializer = new JavaScriptSerializer();

                var jsonDictionary = javaScriptSerializer.Deserialize<Dictionary<string, object>>(json);

                if (jsonDictionary.ContainsKey("Envelope"))
                {
                    var envelope = jsonDictionary["Envelope"];
                    if (envelope is IDictionary<string, object>)
                    {
                        var envDict = (IDictionary<string, object>)envelope;
                        if (envDict.ContainsKey("DataSource"))
                        {
                            var dataSourceObject = envDict["DataSource"];
                            dataSource = dataSourceObject.ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error determining data source in JSON response: {0} Error:{1}", json, ex);
                
            }

            return dataSource;
        }

    }

    
}
