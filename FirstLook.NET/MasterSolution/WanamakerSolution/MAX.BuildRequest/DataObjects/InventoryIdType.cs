﻿namespace MAX.BuildRequest.DataObjects
{
    public class InventoryIdType
    {
        public int InventoryId { get; set; }
        public byte InventoryType { get; set; }
    }
}
