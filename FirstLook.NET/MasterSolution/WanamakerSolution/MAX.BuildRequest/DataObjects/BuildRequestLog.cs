﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Merchandising.Messages;
using log4net;

namespace MAX.BuildRequest.DataObjects
{

    public class BuildRequestLog
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public int BusinessUnitId { get; set; }
        public string VIN { get; set; }
        public Agent SiteId { get; set; }
        public Status StatusTypeId { get; set; }
        public int? CredentialBusinessUnitId { get; set; }
        public string RequestedBy { get; set; }

        public int Save()
        {
            int logId = -1;

            try
            {
                using (
                    var sqlConnection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
                {
                    sqlConnection.Open();

                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandText = "builder.BuildRequestLog#Insert";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@businessUnitId", BusinessUnitId);
                        cmd.Parameters.AddWithValue("@vin", VIN);
                        cmd.Parameters.AddWithValue("@statusTypeId", (int)StatusTypeId);
                        cmd.Parameters.AddWithValue("@siteId", SiteId);
                        cmd.Parameters.AddWithValue("@credentialBusinessUnit", CredentialBusinessUnitId);
                        cmd.Parameters.AddWithValue("@requestedBy", RequestedBy);
                        cmd.Parameters.Add("@buildRequestLogId", SqlDbType.Int).Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        logId = Convert.ToInt32(cmd.Parameters["@buildRequestLogId"].Value);
                    }


                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return logId;
        }

    }

}
