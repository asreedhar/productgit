﻿using System.Collections.Generic;
using Merchandising.Messages.AutoLoad;

namespace MAX.BuildRequest.DataObjects
{

    public class HondaEconfig
    {
        public BuildRequestEnvelope Envelope { get; set; }
        public HondaEconfigVins vins { get; set; }
    }

    public class HondaEconfigVins
    {
        public HondaEconfigVin vin { get; set; }
    }

    public class HondaEconfigVin
    {
        public HondaEconfigModel model { get; set; }
        public string number { get; set; }
    }

    public class HondaEconfigModel
    {
        public HondaEconfigColors colors { get; set; }
        public string division_cd { get; set; }
        public string id { get; set; }
        public string make { get; set; }
        public string model_group_name { get; set; }
        public string model_name { get; set; }
        public string model_year { get; set; }
        public string trans_name { get; set; }
        public string trim_name { get; set; }
    }

    public class HondaEconfigColors
    {
        public List<HondaEconfigColor> color { get; set; }
    }

    public class HondaEconfigColor
    {
        public string color_cd { get; set; }
        public string color_type { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public HondaEconfigAssets assets { get; set; }
        public string mfg_color_cd { get; set; }
    }

    public class HondaEconfigAssets
    {
        public List<HondaEconfigAsset> asset { get; set; }
    }

    public class HondaEconfigAsset
    {
        public string id { get; set; }
        public string path { get; set; }
        public string type_cd { get; set; }
        public string view_cd { get; set; }
    }









}
