﻿namespace MAX.BuildRequest.DataObjects
{
    public class BusinessUnitSite
    {
        public int BusinessUnitId { get; set; }
        public int SiteId { get; set; }
    }
}
