﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MAX.BuildRequest.DataObjects
{

    [XmlRoot(ElementName = "AgentExecution", Namespace = "", IsNullable = false)]
    public class ExportAgentExecution
    {
        [XmlElement(ElementName = "OEM_Listings")]
        public ExportOemListing OemListings { get; set; }
        [XmlAttribute(AttributeName = "date")]
        public string ExecutionDate { get; set; }
        [XmlAttribute(AttributeName = "planId")]
        public string PlanId { get; set; }
        [XmlAttribute(AttributeName = "planExecutionId")]
        public string ExecutionPlanId { get; set; }
    }

    public class ExportOemListing
    {
        [XmlElement("Transaction")]
        public ExportTransaction Transactions { get; set; }
    }


    public class ExportTransaction
    {
        [XmlArray("Vehicles")]
        [XmlArrayItem("Vehicle")]
        public List<ExportVehicle> Vehciles { get; set; }
    }

    public class ExportVehicle
    {
        public string VIN { get; set; }
        
        [XmlElement(ElementName = "Vehicle_Identifier")]
        public string VehicleIdentifier { get; set; }

        [XmlElement(ElementName = "OEM")]
        public string Oem { get; set; }

        [XmlElement(ElementName = "OEM_Site")]
        public string OemSite { get; set; }

        [XmlElement(ElementName = "OEM_Model_Code")]
        public string OemModelCode { get; set; }
        
        [XmlElement(ElementName = "MSRP", IsNullable = false)]
        public string Msrp { get; set; }
        public string Trim { get; set; }
        
        [XmlArray("Options")]
        [XmlArrayItem("Option")]
        public List<ExportOption> Options { get; set; }

        public ExportOption Engine { get; set; }
        public ExportOption Transmission { get; set; }
        public ExportOption InteriorColor { get; set; }
        public ExportOption ExteriorColor { get; set; }

    }

    public class ExportOption
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}


/*
[XmlElement(ElementName = "x")] -- set element name
 * 
  [XmlArray("x")] -- set array name
  [XmlArrayItem("MemberName")] -- set the name of the inner array

*/