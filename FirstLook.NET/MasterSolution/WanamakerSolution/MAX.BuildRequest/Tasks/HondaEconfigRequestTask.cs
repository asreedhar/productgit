﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web.Script.Serialization;
using Core.Messaging;
using MAX.BuildRequest.DataObjects;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace MAX.BuildRequest.Tasks 
{
    public class HondaEconfigRequestTask : TaskRunner<HondaEconfigBuildRequestMessage>
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string DeclaringTypeName = MethodBase.GetCurrentMethod().DeclaringType.Name;
        
        private readonly IQueueFactory _queueFactory;
        private readonly IBuildRequestRepository _buildRequestResultRepository;
        private readonly IQueue<FetchAuditMessage> _auditQueue;
        private readonly IQueue<AutoLoadResultMessage> _autoLoadResultQueue;

        public HondaEconfigRequestTask(IQueueFactory qfactory, IBuildRequestRepository buildRequestResultRepository) 
            :base(qfactory.CreateHondaEconfigRequestQueue())
        {
            _queueFactory = qfactory;
            _buildRequestResultRepository = buildRequestResultRepository;
            _auditQueue = _queueFactory.CreateAutoLoadAudit();
            _autoLoadResultQueue = _queueFactory.CreateAutoLoadResult();

        }

        public override void Process(HondaEconfigBuildRequestMessage message)
        {

            Log.InfoFormat("Starting Honda eConfig processing for VIN: {0}", message.Vin);

            // slow things down a little bit ....
            int delay;

            if (!int.TryParse(ConfigurationManager.AppSettings["HondaRequestDelay"], out delay))
                delay = 10000;

            Thread.Sleep(delay);

            
            Status autoLoadStatus;

            var buildRequestEnvelope = new BuildRequestEnvelope{DataSource = "HondaEconfig", Aquired = DateTime.Now.ToString(CultureInfo.InvariantCulture)};
            string jsonResponse = null;

            try
            {

                // make request
                using (var webClient = new WebClient())
                {

                    string url;
                        
                    if(message.Make.ToLower().Trim() == "acura")
                    {
                        
                        if(ConfigurationManager.AppSettings["AcuraEconfigUrl"] != null)
                            url = ConfigurationManager.AppSettings["AcuraEconfigUrl"];
                        else
                            url = @"http://pat.acura.com/eConfigNet/GetModelByVIN.ashx/GetModelByVin.ashx?vin={0}&appid=www.maxadsystems.com";
                    }
                    else
                    {
                        if (ConfigurationManager.AppSettings["HondaEconfigUrl"] != null)
                            url = ConfigurationManager.AppSettings["HondaEconfigUrl"];
                        else
                            url =
                                @"http://pat.automobiles.honda.com/eConfigNet/GetModelByVin.ashx?vin={0}&appid=www.maxadsystems.com";

                    }

                    url = string.Format(url, message.Vin);
                    jsonResponse = webClient.DownloadString(url);

                    // add envelope!!!
                    jsonResponse = AddEnvelope(jsonResponse, buildRequestEnvelope);

                }

                // do some kind of texts
                if(_buildRequestResultRepository.IsValidBuildRequest(jsonResponse))
                {

                    autoLoadStatus = Status.Successful;

                    // send it to autoload result
                    var autoloadResult = new AutoLoadResultMessage(message.BusinessUnitId, message.InventoryId, jsonResponse, autoLoadStatus, Agent.HondaEconfig, 0);
                    
                    _autoLoadResultQueue.SendMessage(autoloadResult, DeclaringTypeName);

                    // send to autoload audit
                    _auditQueue.SendMessage(new FetchAuditMessage(message.Vin, jsonResponse), DeclaringTypeName);

                }
                else
                {
                    autoLoadStatus = Status.NoVehicleFound;
                    buildRequestEnvelope.ErrorText = "Vehicle Not Found";
                }

            }
            catch (WebException ex)
            {
                Log.ErrorFormat("a web error occurred while processing Honda Econfig web services for VIN: {0}", message.Vin);
                autoLoadStatus = Status.NetworkError;
                buildRequestEnvelope.ErrorText = ex.Message;


            }
            catch (Exception ex)
            {
                Log.ErrorFormat("an error occurred while processing Honda Econfig web services for VIN: {0}", message.Vin);
                autoLoadStatus = Status.UnknownError;
                buildRequestEnvelope.ErrorText = ex.Message;

            }

            // set build request log
            try
            {
                // not really mission critial, don't retry if it fails
                _buildRequestResultRepository.LogBuildRequest(message.BusinessUnitId, message.Vin, Agent.HondaEconfig, autoLoadStatus, message.BusinessUnitId, DeclaringTypeName);

                // set autoload end if not passing on to autoload result. autoload result will set 100 if sucessful
                if (autoLoadStatus != Status.Successful)
                {
                    _buildRequestResultRepository.SetAutoloadEnd(message.BusinessUnitId, message.Vin, autoLoadStatus, DeclaringTypeName);

                    // send audit
                    _auditQueue.SendMessage(new FetchAuditMessage(message.Vin, AddEnvelope(jsonResponse, buildRequestEnvelope)), DeclaringTypeName);

                }

            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Unable log slurpee result data for VIN: {0}", message.Vin), ex);
            }

        }

        private string AddEnvelope(string jsonResponse, BuildRequestEnvelope buildRequestEnvelope)
        {
            if(String.IsNullOrWhiteSpace(jsonResponse))
            {
                jsonResponse = "{}"; // keep the serializer happy if now result
            }

            // add envelope!!!
            var javaScriptSerializer = new JavaScriptSerializer();

            var config = javaScriptSerializer.Deserialize<HondaEconfig>(jsonResponse);

            config.Envelope = buildRequestEnvelope;
            config.Envelope.Aquired = DateTime.Now.ToString(CultureInfo.InvariantCulture);

            return javaScriptSerializer.Serialize(config);
        }

    }
}
