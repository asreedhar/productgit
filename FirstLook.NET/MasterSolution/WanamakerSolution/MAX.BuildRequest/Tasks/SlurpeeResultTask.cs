﻿using System;
using System.Reflection;
using Core.Messaging;
using MAX.BuildRequest.DataObjects;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.Slurpee;

namespace MAX.BuildRequest.Tasks
{
    public class SlurpeeResultTask : TaskRunner<SlurpeeResultMessage>
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string DeclaringTypeName = MethodBase.GetCurrentMethod().DeclaringType.Name;
        
        private readonly IQueueFactory _queueFactory;
        private readonly IBuildRequestRepository _buildRequestResultRepository;
        private readonly IQueue<FetchAuditMessage> _auditQueue;
        private readonly IQueue<AutoLoadResultMessage> _autoLoadResultQueue;

        public SlurpeeResultTask(IQueueFactory qfactory, IBuildRequestRepository buildRequestResultRepository) 
            :base(qfactory.CreateSlurpeeResultQueue())
        {
            _queueFactory = qfactory;
            _buildRequestResultRepository = buildRequestResultRepository;
            _auditQueue = _queueFactory.CreateAutoLoadAudit();
            _autoLoadResultQueue = _queueFactory.CreateAutoLoadResult();

        }

        public override void Process(SlurpeeResultMessage message)
        {

            var autoloadStatus = Status.NoVehicleFound;


            // try to adapt data to autoload audit format, send to autoload result, and log request
            try
            {

                // find the correct adaptor for the manufacturer
                var adapter = _buildRequestResultRepository.GetBuildResultAdaptor(message.meta.site_id);

                // set the status
                autoloadStatus = (Status)message.error.err_code;

                if (adapter.MessageIsValid(message)) // test before getting in a big huff ...
                {
                    
                    // adapt the results to the 'Connotate' format
                    string xmlResponse = adapter.Translate(message);
                    
                    // send to autoLoadAudit and autoLoadResult
                    if (xmlResponse != null)
                    {

                        _auditQueue.SendMessage(new FetchAuditMessage(message.meta.vin, xmlResponse), DeclaringTypeName);
                        Log.DebugFormat("Slurpee build data sucessfully translated for VIN: {0}", message.meta.vin);

                        
                        // load autoload result for this data
                        var vehicles = _buildRequestResultRepository.GetInventoryIdType(message.meta.bu_id,
                                                                                        message.meta.vin);


                        if(vehicles.Count == 0)
                            Log.DebugFormat("Cannot apply slurpee data for vehicle BU: {0} and VIN: {1} because no active vehicle", message.meta.bu_id, message.meta.vin);
                        else
                        {
                            foreach (InventoryIdType vehicle in vehicles)
                            {

                                // this is definately the wrong place for this, should be set be slurpie scheduler .... when it is done .. its done FB: 32807
                                //_buildRequestResultRepository.SetAutoloadStart(message.meta.bu_id, vehicle.InventoryId, DeclaringTypeName);

                                // load autoload result to load build data on vehicle
                                _autoLoadResultQueue.SendMessage(new AutoLoadResultMessage(message.meta.bu_id, vehicle.InventoryId, xmlResponse, Status.Successful, (Agent)message.meta.site_id, vehicle.InventoryType), GetType().FullName);
                                Log.DebugFormat("AutoLoad result message sent from Slurpee result task for BU: {0} and Inv: {1}", message.meta.bu_id, vehicle.InventoryId);
                                
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                // trap ex and log, it in not goign to get any better
                Log.Error(String.Format("Unable to translate Slurpee build data for VIN: {0}", message.meta.vin), ex);
                autoloadStatus = Status.UnknownError;
            }

            // set build request log
            try
            {
                // not really mission critial, don't retry if it fails
                _buildRequestResultRepository.LogBuildRequest(message.meta.bu_id, message.meta.vin, (Agent)message.meta.site_id, autoloadStatus, message.meta.bu_id,DeclaringTypeName);

                // set autoload end if not passing on to autoload result. autoload result will set 100 if sucessful
                if(autoloadStatus != Status.Successful)
                    _buildRequestResultRepository.SetAutoloadEnd(message.meta.bu_id, message.meta.vin, autoloadStatus, DeclaringTypeName);


            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Unable log slurpee result data for VIN: {0}", message.meta.vin), ex);
            }


        }

    }
}
