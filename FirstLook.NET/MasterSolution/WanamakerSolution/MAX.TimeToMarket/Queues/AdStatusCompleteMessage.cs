﻿using System;
using Core.Messages;

namespace MAX.TimeToMarket.Queues
{
	public class AdStatusCompleteMessage : MessageBase
	{
		public AdStatusCompleteMessage() {}

		public AdStatusCompleteMessage(string jobName, string @event, DateTime eventDateTime)
		{
			JobName = jobName;
			Event = @event;
			EventDateTime = eventDateTime;
		}

		public string JobName { get; set; }
		public string Event { get; set; }
		public DateTime EventDateTime { get; set; }
	}
}