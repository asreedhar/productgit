﻿using System;
using AmazonServices.SQS;
using Core.Messaging;
using Merchandising.Messages.TimeToMarket;

namespace MAX.TimeToMarket.Queues
{
	internal class TimeToMarketQueueFactory : ITimeToMarketQueueFactory
	{
		private const int CriticalRetryLimit = 144; // 24 hrs @ 6/hr

		public IQueue<AdStatusCompleteMessage> CreateAdStatusCompleteQueue()
		{
			var queue = AmazonQueueFactory<AdStatusCompleteMessage>.CreateAmazonQueue("Incisent_Merchandising_AdStatusComplete");
			queue.DeadLetterErrorLimit = CriticalRetryLimit;
			return queue;
		}

		public IQueue<TimeToMarketGenerationMessage> CreateTimeToMarketV2Queue()
		{
			var queue = AmazonQueueFactory<TimeToMarketGenerationMessage>.CreateAmazonQueue("Incisent_Merchandising_TimeToMarketV2");
			queue.DeadLetterErrorLimit = CriticalRetryLimit;
			return queue;
		}

		public IQueue<TimeToMarketGenerationMessage> CreateTimeToMarketQueue()
		{
			var queue = AmazonQueueFactory<TimeToMarketGenerationMessage>.CreateAmazonQueue("Incisent_Merchandising_TimeToMarket");
			queue.DeadLetterErrorLimit = CriticalRetryLimit;
			return queue;
		}
	}
}