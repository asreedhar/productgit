﻿using Core.Messaging;
using Merchandising.Messages.TimeToMarket;

namespace MAX.TimeToMarket.Queues
{
	public interface ITimeToMarketQueueFactory
	{
		IQueue<AdStatusCompleteMessage> CreateAdStatusCompleteQueue();
		IQueue<TimeToMarketGenerationMessage> CreateTimeToMarketV2Queue();

		IQueue<TimeToMarketGenerationMessage> CreateTimeToMarketQueue();
	}
}