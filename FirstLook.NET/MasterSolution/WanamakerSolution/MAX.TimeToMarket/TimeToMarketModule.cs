﻿using Autofac;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.TimeToMarket.Queues;
using MAX.TimeToMarket.Repositories;
using MAX.TimeToMarket.ThreadStarter;

namespace MAX.TimeToMarket
{
	public class TimeToMarketModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<TimeToMarketManager>().As<ITimeToMarketManager>();
			builder.RegisterType<TimeToMarketRepositoryFactory>().As<ITimeToMarketRepositoryFactory>();
			builder.RegisterType<TimeToMarketQueueFactory>().As<ITimeToMarketQueueFactory>();
			builder.RegisterType<TimeToMarketThreadStarter>().As<ITimeToMarketTaskThreadStarter>();
			
			// For the TTMV2 task, I want to use this specific implementation
			builder.RegisterType<AdStatusV2Repository>().As<IAdStatusRepository>().AsSelf();
			// So make sure we resolve everything correctly
			builder.Register(ctx => new TimeToMarketV2Repository(
				ctx.Resolve<ITimeToMarketRepository>(),
				ctx.Resolve<AdStatusV2Repository>(), 
				ctx.Resolve<IDashboardBusinessUnitRepository>()));
			base.Load(builder);
		}
	}
}