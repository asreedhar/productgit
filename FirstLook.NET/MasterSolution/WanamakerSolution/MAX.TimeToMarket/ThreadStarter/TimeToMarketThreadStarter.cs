﻿using System;
using System.Configuration;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Tasks;
using MAX.Threading;
using MAX.TimeToMarket.Queues;
using MAX.TimeToMarket.Repositories;
using MAX.TimeToMarket.Tasks;

namespace MAX.TimeToMarket.ThreadStarter
{
	public class TimeToMarketThreadStarter: ITimeToMarketTaskThreadStarter
	{
		private readonly ITimeToMarketQueueFactory _timeToMarketQueueFactory;
		private readonly ITimeToMarketRepositoryFactory _timeToMarketRepositoryFactory;
		private readonly ITimeToMarketManager _timeToMarketManager;
		private readonly IReportStartDates _reportStartDates;

		public TimeToMarketThreadStarter(ITimeToMarketQueueFactory timeToMarketQueueFactory, ITimeToMarketRepositoryFactory timeToMarketRepositoryFactory,
			ITimeToMarketManager timeToMarketManager, IReportStartDates reportStartDates)
		{
			_timeToMarketQueueFactory = timeToMarketQueueFactory;
			_timeToMarketRepositoryFactory = timeToMarketRepositoryFactory;
			_timeToMarketManager = timeToMarketManager;
			_reportStartDates = reportStartDates;
		}

		public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
		{
			// AdStatus Task - waits for AdStatusComplete signal and then kicks off TTM v2
			NamedBackgroundThread.Start("Worker - AdStatusCompleteTask", () =>
				new AdStatusCompleteTask(_timeToMarketQueueFactory, _timeToMarketRepositoryFactory.GetRepository(TimeToMarketRepositoryVersion.Version2))
				.Run(shouldCancel, blockWhileNoMessages));

			// TTM V2 task
			NamedBackgroundThread.Start("Worker - TimeToMarketTaskV2", () => 
				new TimeToMarketTaskV2(_timeToMarketQueueFactory, _timeToMarketManager)
				.Run(shouldCancel, blockWhileNoMessages));
		}
	}
}