﻿namespace MAX.TimeToMarket
{
	public interface ITimeToMarketManager
	{
		void FillAdStatusDates(int businessUnitId);
		void FillMaxDates(int businessUnitId);
	}
}