﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using Dapper;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using log4net;
using MAX.Entities;
using MAX.Entities.Enumerations;

namespace MAX.TimeToMarket.Repositories
{
	public class TimeToMarketV2Repository : TimeToMarketRepositoryDecorator
	{
		private readonly IAdStatusRepository _adStatusRepository;
		private readonly IDashboardBusinessUnitRepository _dashboardBusinessUnitRepository;

		#region Logging
		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		#endregion Logging

		protected GidProviders GidProviders;
	    protected ReportDataSources ReportDataSources; 
		private int _desiredPhotoCount;
		private int _recentlyActiveDays;
		private IEnumerable<IAdStatusResult> _gidResults;

		public TimeToMarketV2Repository(
			ITimeToMarketRepository internalTimeToMarketRepository, 
			IAdStatusRepository adStatusRepository, 
			IDashboardBusinessUnitRepository businessUnitRepository)
			: base(internalTimeToMarketRepository)
		{
			_adStatusRepository = adStatusRepository;
			_dashboardBusinessUnitRepository = businessUnitRepository;
		}

		public override void Initialize(int businessUnitId, GidProviders gidProviders)
		{
			GetBusinessUnitSettings(businessUnitId);
			int recentlyActiveDays;
			if (!int.TryParse(ConfigurationManager.AppSettings["TimeToMarketRecentlyActiveDays"], out recentlyActiveDays))
			{
				recentlyActiveDays = 45;
			}
			
			_recentlyActiveDays = recentlyActiveDays;
		}

		public override IEnumerable<RowItem> GetVinData(int businessUnitId, DateTime reportStartDate)
		{
			var vins = new List<RowItem>();
			Log.Debug("Getting VINs to update");
			using (var con = GetMerchandisingConnection())
			{
				if(con.State != ConnectionState.Open)
					con.Open();

				using (var cmd = con.CreateCommand())
				{
					cmd.CommandText = "timeToMarket.TimeToMarketV2#GetRecentlyActiveVehicles";
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.AddParameter("@BusinessUnitId", DbType.Int32, businessUnitId);
					cmd.AddParameter("@EarliestInventoryReceivedDate", DbType.DateTime, reportStartDate);
					cmd.AddParameter("@RecentlyActiveDays", DbType.Int32, _recentlyActiveDays);

					using (var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							vins.Add(new RowItem(reader));
						}
					}
				}
			}

			Log.InfoFormat("TimeToMarket updatable vin count {0}", vins.Count);
			return vins;
		}

		public override IEnumerable<IAdStatusResult> GetGidDataForVins(IEnumerable<RowItem> @where)
		{
			var rowItems = @where as RowItem[] ?? @where.ToArray();
			
			if (!rowItems.Any())
				return new List<AdStatusResultV2>();

			var adStatusArgs = GetAdStatusTuples(rowItems);

			try
			{
				_gidResults = _adStatusRepository.GetAdStatusResults(adStatusArgs).ToArray();

				Log.DebugFormat("MaxAnalytics AdStatus results for {0} VINs", _gidResults.Count());
			}
			catch (WebException wex)
			{
				Log.ErrorFormat("Web Exception calling analytics service. Status {0}", wex.Status);
				throw; // go ahead and let the error handling re-queue this message
			}
			return _gidResults;
		}

		internal virtual AdStatusV2Args GetAdStatusTuples(IEnumerable<RowItem> @where)
		{
			var rowItems = @where as RowItem[] ?? @where.ToArray();

			var businessunitId = rowItems.First().BusinessUnitId;
			var bu = _dashboardBusinessUnitRepository.GetBusinessUnit(businessunitId);

			const string sql = @"SELECT Vehicle.VIN AS Vin, InventoryReceivedDate AS StartDate, 
								CASE WHEN DeleteDt IS NULL THEN GETDATE() ELSE DeleteDt END AS EndDate 
								FROM dbo.Inventory JOIN dbo.Vehicle ON Vehicle.VehicleId = Inventory.VehicleId
								WHERE Inventory.BusinessUnitId = @businessUnitId AND Vehicle.VIN IN ({0}) 
								AND (	
									Inventory.InventoryActive = 1 OR 
									Inventory.DeleteDt > DATEADD(dd, (0 - @recentlyActiveDays), CAST(GETDATE() AS DATE))
								)";

			List<AdStatusVehicleTuples> tuples;
			var query = String.Format(sql, rowItems.Select(x => "'" + x.VIN + "'").ToDelimitedString(","));
			using (var connection = GetImtConnection())
			{
				tuples = connection.Query<AdStatusVehicleTuples>(query,
					new
					{
						businessunitId,
						recentlyActiveDays = _recentlyActiveDays
					}).ToList();
			}

			var adStatusArgs = new AdStatusV2Args
			{
				Store = bu.Code,
				MinPhotoCount = _desiredPhotoCount,
				InventoryTuples = tuples
			};

			Log.DebugFormat("Call MaxAnalytics AdStatus for {0} VINs", rowItems.Length);
			return adStatusArgs;
		}

		public override void FillPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
		{
			Log.DebugFormat("AdStatusResults - PhotosFirstDate");
			var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
			if (!rowItems.Any())
				return;

			NewUpdateGidData(
				rowItems,
				vehicleAds => vehicleAds.Cast<MaxAnalyticsDates>().Where(ad => ad.FirstPhotoDate.HasValue).Select(ad => (DateTime) ad.FirstPhotoDate),
				"FirstPhotoDate",
				(vehicleThatShouldBeUpdated, allDates) =>
				{
					var sortedDates = allDates.ToArray();
					Array.Sort(sortedDates);
					vehicleThatShouldBeUpdated.PhotosFirstDate = sortedDates.First();
				},
				vehicleThatShouldBeUpdated => vehicleThatShouldBeUpdated.PhotosFirstDate = null
			);
		}

		public override void FillMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
		{
			Log.DebugFormat("AdStatusResults - MinimumPhotoFirstDate");
			var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
			if (!rowItems.Any())
				return;

			NewUpdateGidData(
				rowItems,
				vehicleAds => vehicleAds.Cast<MaxAnalyticsDates>().Where(ad => ad.FirstRequiredPhotoCountDate.HasValue).Select(ad => (DateTime)ad.FirstRequiredPhotoCountDate),
				"MinimumPhotoFirstDate",
				(vehicleThatShouldBeUpdated, allDates) =>
				{
					var sortedDates = allDates.ToArray();
					Array.Sort(sortedDates);
					vehicleThatShouldBeUpdated.MinimumPhotoFirstDate = sortedDates.First();
				},
				vehicleThatShouldBeUpdated => vehicleThatShouldBeUpdated.MinimumPhotoFirstDate = null
			);
		}

		public override void FillPriceFirstDate(IEnumerable<RowItem> updatableVins)
		{
			Log.DebugFormat("AdStatusResults - PriceFirstDate");
			var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
			if (!rowItems.Any())
				return;

			NewUpdateGidData(
				rowItems,
				vehicleAds => vehicleAds.Cast<MaxAnalyticsDates>().Where(ad => ad.FirstNonZeroPriceDate.HasValue).Select(ad => (DateTime)ad.FirstNonZeroPriceDate),
				"PriceFirstDate",
				(vehicleThatShouldBeUpdated, allDates) =>
				{
					var sortedDates = allDates.ToArray();
					Array.Sort(sortedDates);
					vehicleThatShouldBeUpdated.PriceFirstDate = sortedDates.First();
				},
				vehicleThatShouldBeUpdated => vehicleThatShouldBeUpdated.PriceFirstDate = null
			);
		}

		public override void FillDescriptionFirstDate(IEnumerable<RowItem> updatableVins)
		{
			Log.DebugFormat("AdStatusResults - DescriptionFirstDate");
			var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
			if (!rowItems.Any())
				return;

			NewUpdateGidData(
				rowItems,
				vehicleAds => vehicleAds.Cast<MaxAnalyticsDates>().Where(ad => ad.FirstAdDate.HasValue).Select(ad => (DateTime)ad.FirstAdDate),
				"DescriptionFirstDate",
				(vehicleThatShouldBeUpdated, allDates) =>
				{
					var sortedDates = allDates.ToArray();
					Array.Sort(sortedDates);
					vehicleThatShouldBeUpdated.DescriptionFirstDate = sortedDates.First();
				},
				vehicleThatShouldBeUpdated => vehicleThatShouldBeUpdated.DescriptionFirstDate = null
			);
		}

		public override void FillGidFirstDate(IEnumerable<RowItem> updatableVins)
		{
            Log.DebugFormat("AdStatusResults - GidFirstDate");
	        foreach (var updateVin in updatableVins)
	        {
		        var dates = new[] {updateVin.DescriptionFirstDate, updateVin.MinimumPhotoFirstDate, updateVin.PhotosFirstDate, updateVin.PriceFirstDate};
		        if (dates.All(d => d.HasValue == false))
		        {
			        updateVin.GidFirstDate = null;
		        }
		        else
		        {
			        var usableDates = dates.Where(d => d.HasValue).ToArray();
					Array.Sort(usableDates);
			        var dateTime = usableDates.First();
			        if (dateTime != null)
			        {
				        updateVin.GidFirstDate = dateTime;
			        }
		        }
			}
		}

		public override void FillCompleteAdFirstDate(IEnumerable<RowItem> updatableVins)
		{
			var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
			if (rowItems.Any())
			{
				Log.DebugFormat("CompleteAdFirstDate");

				// For complete ad first get the correct AdCompleteFlags setting for this businessUnitId
				// then check the updatable vins for records that do not have an adcomplete date
				// check those vins for fields matching the adcompleteflags setting
				// set adcomplete if the times match the current setting.
				var buCompleteFlags = GetAdCompleteFlags(rowItems.First().BusinessUnitId);
				Log.DebugFormat("buCompleteFlags = {0}", buCompleteFlags);

				// Set complete date
				foreach (var setDate in rowItems.AsParallel().ToList().FindAll(completeVins => (
					completeVins.AdApprovalFirstDate.HasValue &&
					(!buCompleteFlags.HasFlag(AdCompleteFlags.MinimumPhotos) || completeVins.MinimumPhotoFirstDate.HasValue) &&
					(!buCompleteFlags.HasFlag(AdCompleteFlags.Description) || completeVins.DescriptionFirstDate.HasValue) &&
					(!buCompleteFlags.HasFlag(AdCompleteFlags.Price) || completeVins.PriceFirstDate.HasValue))
					))
				{
					var latestDate = new List<DateTime?>
					{
						setDate.PhotosFirstDate,
						setDate.MinimumPhotoFirstDate,
						setDate.PriceFirstDate,
						setDate.DescriptionFirstDate
					}
						.Where(dt => dt.HasValue)
						.OrderByDescending(dt => dt.Value)
						.First();

					if (latestDate != null && latestDate.Value != DateTime.MinValue)
						setDate.CompleteAdFirstDate = latestDate.Value;
				}


				foreach (var setDate in rowItems.AsParallel().ToList().FindAll(incompleteVins => (
					// have not published a MAX ad.
					!(incompleteVins.AdApprovalFirstDate.HasValue) ||
					
					// has the minimum photos flag, but does not have minimum photos date
					(buCompleteFlags.HasFlag(AdCompleteFlags.MinimumPhotos) && !(incompleteVins.MinimumPhotoFirstDate.HasValue)) ||
					
					// has the description flag, but does not have a description date
					(buCompleteFlags.HasFlag(AdCompleteFlags.Description) && !(incompleteVins.DescriptionFirstDate.HasValue)) ||
					
					// has the price flag, but does not have a price date
					(buCompleteFlags.HasFlag(AdCompleteFlags.Price) && !(incompleteVins.PriceFirstDate.HasValue)))
					))
				{
					setDate.CompleteAdFirstDate = null;
				}
			}
		}

		#region virtuals for testing

		internal virtual AdCompleteFlags GetAdCompleteFlags(int businessUnitId)
		{
			var reportSettings = new GetReportSettings(businessUnitId);
			AbstractCommand.DoRun(reportSettings);
			return (AdCompleteFlags)reportSettings.AdCompleteFlags;
		}

		protected virtual SqlConnection GetMerchandisingConnection()
		{
			var conn= new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString);
			conn.Open();
			return conn;
		}

		protected virtual SqlConnection GetImtConnection()
		{
			var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString);
			conn.Open();
			return conn;
		}
		
		protected virtual void GetBusinessUnitSettings(int businessUnitId)
		{
			GidProviders = new GidProviders
			{
				{VehicleType.New, GidProvider.Unknown},
				{VehicleType.Used, GidProvider.Unknown},
			};

            ReportDataSources = new ReportDataSources()
            {
				{VehicleType.New, ReportDataSource.None},
				{VehicleType.Used, ReportDataSource.None},
			};

			using (var conn = GetMerchandisingConnection())
			{
				var rows = conn.Query("SELECT GIDProviderNew, GIDProviderUsed, desiredPhotoCount, ReportDataSourceUsed, ReportDataSourceNew FROM settings.Merchandising WHERE businessUnitId = @businessUnitId", new {businessUnitId}).ToArray();
				if(rows.Any())
				{
					GidProviders[VehicleType.New] = (GidProvider)rows[0].GIDProviderNew;
					GidProviders[VehicleType.Used] = (GidProvider)rows[0].GIDProviderUsed;
					_desiredPhotoCount = rows[0].desiredPhotoCount;
                    ReportDataSources[VehicleType.New] = (ReportDataSource)rows[0].ReportDataSourceNew;
                    ReportDataSources[VehicleType.Used] = (ReportDataSource)rows[0].ReportDataSourceUsed;
				}
			}
			Log.InfoFormat("BusinessUnitSettings: gidProviders: {0}, desiredPhotoCount: {1}, ReportDataSources: {2}", GidProviders.ToJson(), _desiredPhotoCount, ReportDataSources.ToJson());
		}

		#endregion virtuals for testing

		internal IEnumerable<IMaxAnalyticsAd> GetGidDataForVehicle(string vin, DateRange activeDates, GidProvider gidProvider, ReportDataSource reportDataSource)
		{
			if (reportDataSource == ReportDataSource.None)
				return null;

		    // Take all the AdStatusResults for the correct provider which also match the vin and fall within the correct daterange
		    var providerAds = _gidResults
		        .Where(providerResult =>
		            providerResult.DataSource == reportDataSource &&
		            ((AdStatusResultV2) providerResult).Ads.Any(
		                ad => String.Equals(ad.VIN, vin, StringComparison.InvariantCultureIgnoreCase))
		        );

		    // select many to project and flatten the results into one collection
		    IEnumerable<IMaxAnalyticsAd> vinAds = providerAds.SelectMany(pa => ((AdStatusResultV2) pa).Ads.Where(
		        ad =>
		        {
		            var adDates =
		                new[]
		                {ad.FirstAdDate, ad.FirstNonZeroPriceDate, ad.FirstPhotoDate, ad.FirstRequiredPhotoCountDate}
		                    .Where(dt => dt.HasValue);
		            return (String.Equals(ad.VIN, vin, StringComparison.InvariantCultureIgnoreCase) &&
		                    adDates.All(dt => dt != null && activeDates.Contains((DateTime) dt)));
		        })
		        );
		    
		    return vinAds;
		}


		private void NewUpdateGidData(
			IEnumerable<RowItem> updatableVins,
			Func<IEnumerable<IMaxAnalyticsAd>, IEnumerable<DateTime>> allDatesFunc,
			string loggingFieldName,
			Action<RowItem, IEnumerable<DateTime>> foundValueUpdateFunction,
			Action<RowItem> notFoundValueUpdateFunction)
		{
			Log.InfoFormat("UpdateGidData -> Setting from AdStatusResults - {0}", loggingFieldName);
			foreach (var vehicleThatShouldBeUpdated in updatableVins)
			{
				// Wrap the whole thing in a try catch for each vehicle. So one bad apple won't spoil the whole bunch
				try
				{
					// I Nedd log4net to support Trace level Log.DebugFormat("{0} before {1}", loggingFieldName, vehicleThatShouldBeUpdated.ToJson());
					var lastDateForVehicle = vehicleThatShouldBeUpdated.DeleteDate.HasValue
						? vehicleThatShouldBeUpdated.DeleteDate.Value
						: DateTime.UtcNow;

					// A vin may appear multiple times, but one with a deletedt < ird is weird.
					if (lastDateForVehicle.Date < vehicleThatShouldBeUpdated.InventoryReceivedDate.Date)
					{
						Log.WarnFormat("DeleteDate is less than Received date. InventoryID: {0}, IRD: {1}, DeleteDate: {2}",
							vehicleThatShouldBeUpdated.InventoryId,
							vehicleThatShouldBeUpdated.InventoryReceivedDate,
							vehicleThatShouldBeUpdated.DeleteDate);
						continue;
					}
					var vehicleAdsEnum = GetGidDataForVehicle(
						vehicleThatShouldBeUpdated.VIN,
						new DateRange(vehicleThatShouldBeUpdated.InventoryReceivedDate.Date, lastDateForVehicle.Date),
						GidProviders[vehicleThatShouldBeUpdated.InventoryType], ReportDataSources[vehicleThatShouldBeUpdated.InventoryType]);

					// I Nedd log4net to support Trace level Log.DebugFormat("ad data from adstatus: {0}", vehicleAds.ToJson());
					var vehicleAds = new IMaxAnalyticsAd[] {};

					if (vehicleAdsEnum != null)
						vehicleAds = vehicleAdsEnum.ToArray();

					var allDates = allDatesFunc(vehicleAds);
					if (allDates != null && allDates.Any())
					{
						var dateTimes = allDates as DateTime[] ?? allDates.ToArray();
						Log.DebugFormat("Earliest {0} found for vin {1} is {2}", loggingFieldName, vehicleThatShouldBeUpdated.VIN, dateTimes.First());
						foundValueUpdateFunction(vehicleThatShouldBeUpdated, dateTimes);
					}
					else
					{
						Log.DebugFormat("No {0} found for vin: {1} setting to NULL", loggingFieldName, vehicleThatShouldBeUpdated.VIN);
						notFoundValueUpdateFunction(vehicleThatShouldBeUpdated);
					}

					// I Nedd log4net to support Trace level Log.DebugFormat("{0} after {1}", loggingFieldName, vehicleThatShouldBeUpdated.ToJson());
				}
				catch (Exception ex)
				{
					Log.ErrorFormat("Exception on a specific vehicle. Msg: {0}, StackTrace: {1}", ex.Message, ex.StackTrace);
				}
			}
		}
	}
}