﻿using System;
using System.Collections.Generic;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;

namespace MAX.TimeToMarket.Repositories
{
	public interface INewTimeToMarketRepository
    {
        IEnumerable<RowItem> GetVinData(int businessUnitId, DateTime reportStartDate);
        void UpdateVins(IEnumerable<RowItem> vinsToUpdate);
        int[] GetMaxBusinessUnits(Upgrade upgrade);
		IEnumerable<AdStatusResult> GetGidDataForVins(IEnumerable<RowItem> @where); 

        void FillCompleteAdFirstDate(IEnumerable<RowItem> updatableVins);
        void FillAdApprovalFirstDate(IEnumerable<RowItem> updatableVins);

        void FillPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillPriceFirstDate(IEnumerable<RowItem> updatableVins);
        void FillDescriptionFirstDate(IEnumerable<RowItem> updatableVins);
        void FillGidFirstDate(IEnumerable<RowItem> updatableVins);

        void FillMAXPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillMAXMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillMAXPriceFirstDate(IEnumerable<RowItem> updatableVins);
        void FillMAXDescriptionFirstDate(IEnumerable<RowItem> updatableVins);

        void Initialize(int businessUnitId, GidProviders gidProviders);
    }
}
