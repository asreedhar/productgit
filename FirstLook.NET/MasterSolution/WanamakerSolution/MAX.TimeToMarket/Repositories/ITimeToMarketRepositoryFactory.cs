using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;

namespace MAX.TimeToMarket.Repositories
{
	public enum TimeToMarketRepositoryVersion
	{
		Version1,
		Version2
	}
	public interface ITimeToMarketRepositoryFactory
	{
		ITimeToMarketRepository GetRepository(TimeToMarketRepositoryVersion version);
	}
}