﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using MAX.Entities;
using MAX.Entities.Reports.TimeToMarket;

namespace MAX.TimeToMarket.Repositories
{
	public class TimeToMarketRepositoryDecorator : ITimeToMarketRepository
	{
		private readonly ITimeToMarketRepository _timeToMarketRepository;

		public TimeToMarketRepositoryDecorator(ITimeToMarketRepository timeToMarketRepository)
		{
			_timeToMarketRepository = timeToMarketRepository;
		}

		public virtual IEnumerable<RowItem> GetVinData(int businessUnitId, DateTime reportStartDate)
		{
			return _timeToMarketRepository.GetVinData(businessUnitId, reportStartDate);
		}

		public virtual void UpdateVins(IEnumerable<RowItem> vinsToUpdate)
		{
			_timeToMarketRepository.UpdateVins(vinsToUpdate);
		}

		public virtual int[] GetMaxBusinessUnits(Upgrade upgrade)
		{
			return _timeToMarketRepository.GetMaxBusinessUnits(upgrade);
		}

		public virtual void FillCompleteAdFirstDate(IEnumerable<RowItem> updatableVins)
		{
			_timeToMarketRepository.FillCompleteAdFirstDate(updatableVins);
		}

		public virtual void FillAdApprovalFirstDate(IEnumerable<RowItem> updatableVins)
		{
			_timeToMarketRepository.FillAdApprovalFirstDate(updatableVins);
		}

		public virtual void FillPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
		{
			_timeToMarketRepository.FillPhotosFirstDate(updatableVins, reportStartDate);
		}

		public virtual void FillMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
		{
			_timeToMarketRepository.FillMinimumPhotosFirstDate(updatableVins, reportStartDate);
		}

		public virtual void FillPriceFirstDate(IEnumerable<RowItem> updatableVins)
		{
			_timeToMarketRepository.FillPriceFirstDate(updatableVins);
		}

		public virtual void FillDescriptionFirstDate(IEnumerable<RowItem> updatableVins)
		{
			_timeToMarketRepository.FillDescriptionFirstDate(updatableVins);
		}

		public virtual void FillGidFirstDate(IEnumerable<RowItem> updatableVins)
		{
			_timeToMarketRepository.FillGidFirstDate(updatableVins);
		}

		public virtual void FillMAXPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
		{
			_timeToMarketRepository.FillMAXPhotosFirstDate(updatableVins, reportStartDate);
		}

		public virtual void FillMAXMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
		{
			_timeToMarketRepository.FillMAXMinimumPhotosFirstDate(updatableVins, reportStartDate);
		}

		public virtual void FillMAXPriceFirstDate(IEnumerable<RowItem> updatableVins)
		{
			_timeToMarketRepository.FillMAXPriceFirstDate(updatableVins);
		}

		public virtual void FillMAXDescriptionFirstDate(IEnumerable<RowItem> updatableVins)
		{
			_timeToMarketRepository.FillMAXDescriptionFirstDate(updatableVins);
		}

		public virtual void Initialize(int businessUnitId, GidProviders gidProviders)
		{
			_timeToMarketRepository.Initialize(businessUnitId, gidProviders);
		}

		public virtual BusinessUnitReport FetchReport(int businessUnitId, VehicleType vehicleType, DateRange dateRange)
		{
			return _timeToMarketRepository.FetchReport(businessUnitId, vehicleType, dateRange);
		}

		public virtual GroupReport FetchGroupReport(int groupId, VehicleType vehicleType, DateRange dateRange)
		{
			return _timeToMarketRepository.FetchGroupReport(groupId, vehicleType, dateRange);
		}

		public virtual IEnumerable<IAdStatusResult> GetGidDataForVins(IEnumerable<RowItem> @where)
		{
			return _timeToMarketRepository.GetGidDataForVins(@where);
		}
	}
}