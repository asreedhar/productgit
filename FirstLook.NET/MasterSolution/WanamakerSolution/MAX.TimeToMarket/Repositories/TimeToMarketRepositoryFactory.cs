﻿using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;

namespace MAX.TimeToMarket.Repositories
{
	public class TimeToMarketRepositoryFactory : ITimeToMarketRepositoryFactory
	{
		private readonly IAdStatusRepository _adStatusRepository;
		private readonly IInventoryDataRepository _inventoryDataRepository;
		private readonly IDashboardBusinessUnitRepository _businessUnitRepository;

		public TimeToMarketRepositoryFactory(IAdStatusRepository adStatusRepository, IInventoryDataRepository inventoryDataRepository,
			IDashboardBusinessUnitRepository businessUnitRepository)
		{
			_adStatusRepository = adStatusRepository;
			_inventoryDataRepository = inventoryDataRepository;
			_businessUnitRepository = businessUnitRepository;
		}

		public ITimeToMarketRepository GetRepository(TimeToMarketRepositoryVersion version)
		{
			var timeToMarketRepository = GetTimeToMarketRepository();
			return version == TimeToMarketRepositoryVersion.Version1 ? 
				new TimeToMarketRepositoryDecorator(timeToMarketRepository) : 
				new TimeToMarketV2Repository(timeToMarketRepository, _adStatusRepository, _businessUnitRepository);
		}

		private ITimeToMarketRepository GetTimeToMarketRepository()
		{
			return new TimeToMarketRepository(_adStatusRepository, _inventoryDataRepository, _businessUnitRepository);
		}
	}
}