﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using MAX.Entities;
using MAX.TimeToMarket.Queues;
using Merchandising.Messages.TimeToMarket;

namespace MAX.TimeToMarket.Tasks
{
    public class TimeToMarketGenerationTask : TaskRunner<TimeToMarketGenerationMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ITimeToMarketRepository _repository;
        private readonly IReportStartDates _reportDates;

        public TimeToMarketGenerationTask(ITimeToMarketQueueFactory queueFactory, ITimeToMarketRepository repository, IReportStartDates reportDates)
            : base(queueFactory.CreateTimeToMarketQueue())
        {
            _repository = repository;
            _reportDates = reportDates;
        }

        public override void Process(TimeToMarketGenerationMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}",message.BusinessUnitId);

            Log.DebugFormat("Processing TimeToMarketGenerationMessage: BusinessUnitId: {0}",
                            message.BusinessUnitId);
	        var settings = GetMiscSettings.GetSettings(message.BusinessUnitId);
	        var providers = new GidProviders
	        {
		        {VehicleType.New, settings.NewGidProvider},
		        {VehicleType.Used, settings.UsedGidProvider}
	        };

            _repository.Initialize(message.BusinessUnitId, providers);

            if (HasTimeToMarketEnabled(message.BusinessUnitId))
            {
                try
                {
                    _reportDates.Initialize(message.BusinessUnitId, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));


                    Log.DebugFormat("Report start dates for BUID {0}: photo: {1}, complete {2}", message.BusinessUnitId, _reportDates.FirstPhotoStartDate, _reportDates.CompleteStartDate);

                    /***************************************************************************
                     * 
                     * Get NewInGID Vehicles
                     *  Update db accordingly
                     *  
                     ***************************************************************************/
                    IEnumerable<RowItem> updatableVins = _repository.GetVinData(message.BusinessUnitId, _reportDates.FirstPhotoStartDate);

                    var rowItems = updatableVins as IList<RowItem> ?? updatableVins.ToList();

                    if (!rowItems.Any())
                        return;

                    _repository.GetGidDataForVins(
                        rowItems.Where(
                            ttmri =>
                                ttmri.InventoryReceivedDate > _reportDates.FirstPhotoStartDate &&
                                (ttmri.DescriptionFirstDate.HasValue == false || ttmri.MinimumPhotoFirstDate.HasValue == false || ttmri.PhotosFirstDate.HasValue == false ||
                                 ttmri.PriceFirstDate.HasValue == false)));


                    Log.DebugFormat("Set MAXPhotos using reportStartDate: {0} for {1} InventoryIds", _reportDates.FirstPhotoStartDate, rowItems.Count());

                    SetMAXPhotosDates(rowItems, _reportDates.FirstPhotoStartDate);

                    /************************************************************
                     * 
                     * All of the following fill methods were created
                     * after the initial release date. For that reason, they will
                     * use a filtered reports date.
                     * 
                     ************************************************************/

                    Log.DebugFormat("Use CompleteReportStartDate: {0} for {1} InventoryIds", _reportDates.CompleteStartDate, rowItems.Count);

                    SetMAXMinimumPhotosFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate), _reportDates.CompleteStartDate);
                    SetMAXPriceDates(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate));
                    SetMAXDescriptionDates(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate));
                    SetAdApprovalFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate));


                    SetPhotosFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate), _reportDates.FirstPhotoStartDate);
                    SetMinimumPhotosFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate), _reportDates.FirstPhotoStartDate);
                    SetPriceFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate));
                    SetDescriptionFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate));
                    SetGidFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate));

                    SetCompleteAdFirstDate(rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportDates.CompleteStartDate));


                    Log.DebugFormat("Finished filling the update table.");


                    // Save the full list.
                    SaveUpdates(rowItems);


                    Log.DebugFormat("Finished Processing TimeToMarketGenerationMessage: BusinessUnitId: {0}", message.BusinessUnitId);
                }
                catch (Exception ex)
                {
                    Log.Error("Failed while Getting report data.", ex);
                    throw;
                }
            }
        }



        private void SetMAXPriceDates(IEnumerable<RowItem> updatableVins)
        {
            _repository.FillMAXPriceFirstDate(updatableVins);
        }

        private void SaveUpdates(IEnumerable<RowItem> updatableVins)
        {
            _repository.UpdateVins(updatableVins);
        }

        private void SetMAXPhotosDates(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            _repository.FillMAXPhotosFirstDate(updatableVins, reportStartDate);
        }

        private void SetMAXMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            _repository.FillMAXMinimumPhotosFirstDate(updatableVins, reportStartDate);
        }

        private void SetMAXDescriptionDates(IEnumerable<RowItem> updatableVins)
        {
            _repository.FillMAXDescriptionFirstDate(updatableVins);
        }

        private void SetAdApprovalFirstDate(IEnumerable<RowItem> updatableVins)
        {
            _repository.FillAdApprovalFirstDate(updatableVins);
        }

        private void SetCompleteAdFirstDate(IEnumerable<RowItem> updatableVins)
        {
            _repository.FillCompleteAdFirstDate(updatableVins);
        }

        #region GID Data
        private void SetMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            _repository.FillMinimumPhotosFirstDate(updatableVins, reportStartDate);
        }
        private void SetPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            _repository.FillPhotosFirstDate(updatableVins, reportStartDate);
        }
        private void SetPriceFirstDate(IEnumerable<RowItem> updatableVins)
        {
            _repository.FillPriceFirstDate(updatableVins);
        }
        private void SetDescriptionFirstDate(IEnumerable<RowItem> updatableVins)
        {
            _repository.FillDescriptionFirstDate(updatableVins);
        }

        private void SetGidFirstDate(IEnumerable<RowItem> updatableVins)
        {
            _repository.FillGidFirstDate(updatableVins);
        }
        #endregion GID Data

        /// <summary>
        /// Check the business unit for reportability
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        // ReSharper disable once UnusedParameter.Local
        private bool HasTimeToMarketEnabled(int p)
        {
            // We only queue up messages for business units that have the max30 upgrade, do we need to check again?
            // I don't think so, but useful for testing!
            //return p.Equals(100147);
            return true;
        }
    }
}
