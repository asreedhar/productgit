﻿using System;
using System.Reflection;
using Core.Messaging;
using MAX.TimeToMarket.Queues;
using Merchandising.Messages.TimeToMarket;

namespace MAX.TimeToMarket.Tasks
{
	public class TimeToMarketTaskV2 : TaskRunner<TimeToMarketGenerationMessage>
	{
		#region Logging
		protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		#endregion Logging

		private readonly ITimeToMarketManager _manager;

		public TimeToMarketTaskV2(ITimeToMarketQueueFactory queueFactory, ITimeToMarketManager manager)
            : base(queueFactory.CreateTimeToMarketV2Queue())
        {
			_manager = manager;
        }

		public override void Process(TimeToMarketGenerationMessage message)
		{
			Log.InfoFormat("Processing TimeToMarketGenerationMessage: BusinessUnitId: {0}",
				message.BusinessUnitId);

			try
			{
				Log.Info("Calling FillMaxDates");
				_manager.FillMaxDates(message.BusinessUnitId);
				
				Log.Info("Calling FillAdStatusDates");
				_manager.FillAdStatusDates(message.BusinessUnitId);
			}
			catch (Exception ex)
			{
				Log.Error("Exception filling ad status dates", ex);
				throw;
			}
		}
	}
}