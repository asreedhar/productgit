﻿using System.Linq;
using System.Reflection;
using Core.Messaging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using log4net;
using MAX.TimeToMarket.Queues;
using Merchandising.Messages.TimeToMarket;

namespace MAX.TimeToMarket.Tasks
{
	public class AdStatusCompleteTask : TaskRunner<AdStatusCompleteMessage>
	{
		private readonly ITimeToMarketRepository _ttmRepository;
		private readonly IQueue<TimeToMarketGenerationMessage> _writeQueue;

		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging


		private const string Adstatus = "AdStatus";
		private const string Completion = "Completion";

		public AdStatusCompleteTask(ITimeToMarketQueueFactory queueFactory, ITimeToMarketRepository ttmRepository)
			: base(queueFactory.CreateAdStatusCompleteQueue())
		{
			_ttmRepository = ttmRepository;
			_writeQueue = queueFactory.CreateTimeToMarketV2Queue();
		}

		public override void Process(AdStatusCompleteMessage message)
		{
			Log.Info("Received AdStatusCompleteMessage");
			if (message.JobName == Adstatus && message.Event == Completion)
			{
				//Get all max business units
				var activeBusinessUnits = _ttmRepository.GetMaxBusinessUnits(Upgrade.Merchandising);
				Log.InfoFormat("Sending TimeToMarketGenerationMessages for {0} active business units", activeBusinessUnits.Length);
				// send TimeToMarketGenerationMessage for each.
				foreach (var activeBusinessUnit in activeBusinessUnits)
				{
					Log.DebugFormat("Sending TimeToMarketGenerationMessage for BusinessUnitId: {0}", activeBusinessUnit);
					_writeQueue.SendMessage(new TimeToMarketGenerationMessage(activeBusinessUnit), GetType().FullName);
				}
			}
			Log.Info("AdStatusCompleteTask complete");
		}
	}
}