﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using log4net;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.TimeToMarket.Repositories;

namespace MAX.TimeToMarket
{
	public class TimeToMarketManager : ITimeToMarketManager
	{
		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging

		private readonly ITimeToMarketRepository _ttmRepository;
		private readonly IReportStartDates _reportStartDates;

		public TimeToMarketManager(ITimeToMarketRepositoryFactory ttmRepositoryFactory, IReportStartDates reportStartDates)
		{
			_ttmRepository = ttmRepositoryFactory.GetRepository(TimeToMarketRepositoryVersion.Version2);
			_reportStartDates = reportStartDates;
		}

		public void FillAdStatusDates(int businessUnitId)
		{
			Log.Info("Filling Ad Status Dates");
			_reportStartDates.Initialize(businessUnitId, DateTime.Now);
			_ttmRepository.Initialize(businessUnitId, null);
		    IEnumerable<RowItem> updatableVins = _ttmRepository.GetVinData(businessUnitId, _reportStartDates.FirstPhotoStartDate);

			var rowItems = updatableVins as IList<RowItem> ?? updatableVins.ToList();
			Log.InfoFormat("Found {0} items that could be updated.", rowItems.Count);
			/*** // I Nedd log4net to support Trace level 
			 * var str = rowItems.Select(uv => new
			{
				uv.VIN, 
				PhotosFirstDate = ToDateStringForDebug(uv.PhotosFirstDate),
				MinimumPhotosFirstDate = ToDateStringForDebug(uv.MinimumPhotoFirstDate),
				PriceFirstDate = ToDateStringForDebug(uv.PriceFirstDate),
				DescriptionFirstDate = ToDateStringForDebug(uv.DescriptionFirstDate),
				GidFirstDate = ToDateStringForDebug(uv.GidFirstDate),
				MAXDescriptionFirstDate = ToDateStringForDebug(uv.MAXDescriptionFirstDate),
				MAXMinimumPhotoFirstDate = ToDateStringForDebug(uv.MAXMinimumPhotoFirstDate),
				MAXPhotoFirstDate = ToDateStringForDebug(uv.MAXPhotoFirstDate),
				MAXPriceFirstDate = ToDateStringForDebug(uv.MAXPriceFirstDate),
				AdApprovalFirstDate = ToDateStringForDebug(uv.AdApprovalFirstDate)
			}).ToJson();

			Log.DebugFormat("RAW Starting Data: {0}", str);
			 * ***/

			if (!rowItems.Any())
			{
				Log.Info("No items found -- FIN!");
				return;
			}
			Log.DebugFormat("Update InventoryIds: {0}", rowItems.Select(ri => ri.InventoryId).ToDelimitedString(","));

            var firstReportDate = HasCarsApi(businessUnitId) ? Max(_reportStartDates.CompleteStartDate, _reportStartDates.CarsApiStartDate) : _reportStartDates.CompleteStartDate;
            var firstPhotoStartDate = HasCarsApi(businessUnitId) ? Max(_reportStartDates.FirstPhotoStartDate, _reportStartDates.CarsApiStartDate) : _reportStartDates.FirstPhotoStartDate;
            var firstPhotoRows = rowItems.Where(ttmri => ttmri.InventoryReceivedDate > firstPhotoStartDate);
            var allDataRows = rowItems.Where(ttmri => ttmri.InventoryReceivedDate > firstReportDate);

			if (_ttmRepository.GetGidDataForVins(firstPhotoRows).Any())
			{
				var dataRows = allDataRows as RowItem[] ?? allDataRows.ToArray();

				var startdate = new DateTime(2014, 03, 31);

				Log.Debug("FillDescriptionFirstDate");
				_ttmRepository.FillDescriptionFirstDate(dataRows.Where(r => r.DescriptionFirstDate.HasValue == false || r.DescriptionFirstDate > startdate));

				Log.Debug("FillPhotosFirstDate");
				_ttmRepository.FillPhotosFirstDate(dataRows.Where(r => r.PhotosFirstDate.HasValue == false || r.PhotosFirstDate > startdate), _reportStartDates.FirstPhotoStartDate);
				
				Log.Debug("FillMinimumPhotosFirstDate");
				_ttmRepository.FillMinimumPhotosFirstDate(dataRows.Where(r => r.MinimumPhotoFirstDate.HasValue == false || r.MinimumPhotoFirstDate > startdate), _reportStartDates.CompleteStartDate);
				
				Log.Debug("FillPriceFirstDate");
				_ttmRepository.FillPriceFirstDate(dataRows.Where(r => r.PriceFirstDate.HasValue == false || r.PriceFirstDate > startdate));

				Log.Debug("FillCompleteAdFirstDate");
				_ttmRepository.FillCompleteAdFirstDate(dataRows.Where(r => r.CompleteAdFirstDate.HasValue == false || r.CompleteAdFirstDate > startdate));
				
				Log.Debug("FillGidFirstDate");
				_ttmRepository.FillGidFirstDate(dataRows);

				Log.Info("Updating TTM VIN data after all processing");
				/*** 
				 * I Nedd log4net to support Trace level 
				var upstr = rowItems.Select(uv => new
				{
					uv.VIN,
					PhotosFirstDate = ToDateStringForDebug(uv.PhotosFirstDate),
					MinimumPhotosFirstDate = ToDateStringForDebug(uv.MinimumPhotoFirstDate),
					PriceFirstDate = ToDateStringForDebug(uv.PriceFirstDate),
					DescriptionFirstDate = ToDateStringForDebug(uv.DescriptionFirstDate),
					GidFirstDate = ToDateStringForDebug(uv.GidFirstDate),
					MAXDescriptionFirstDate = ToDateStringForDebug(uv.MAXDescriptionFirstDate),
					MAXMinimumPhotoFirstDate = ToDateStringForDebug(uv.MAXMinimumPhotoFirstDate),
					MAXPhotoFirstDate = ToDateStringForDebug(uv.MAXPhotoFirstDate),
					MAXPriceFirstDate = ToDateStringForDebug(uv.MAXPriceFirstDate),
					AdApprovalFirstDate = ToDateStringForDebug(uv.AdApprovalFirstDate)
				}).ToJson();

				Log.DebugFormat("Updating Data: {0}", upstr);
				***/

				_ttmRepository.UpdateVins(rowItems);
			}
			else
			{
				Log.InfoFormat("No data returned from AdStatus2 for BusinessUnit: {0}", businessUnitId);
			}
		}

		private static string ToDateStringForDebug(DateTime? uv)
		{
			if (uv != null)
			{
				return ((DateTime)uv).ToString("d");
			}
			return "[unset]";
		}

		public void FillMaxDates(int businessUnitId)
		{
			Log.Info("Filling TimeToMAX Dates");
			_reportStartDates.Initialize(businessUnitId, DateTime.Now.Date);
			_ttmRepository.Initialize(businessUnitId, null);
			IEnumerable<RowItem> updatableVins = _ttmRepository.GetVinData(businessUnitId, _reportStartDates.FirstPhotoStartDate);

			var rowItems = updatableVins as IList<RowItem> ?? updatableVins.ToList();

			Log.InfoFormat("Found {0} items that could be updated.", rowItems.Count);

			if (!rowItems.Any())
			{
				Log.Info("No items found -- FIN!");
				return;
			}
			Log.DebugFormat("Update InventoryIds: {0}", rowItems.Select(ri => ri.InventoryId).ToDelimitedString(","));

			var firstPhotoRows = rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportStartDates.FirstPhotoStartDate);
			var allDataRows = rowItems.Where(ttmri => ttmri.InventoryReceivedDate > _reportStartDates.CompleteStartDate);


			Log.InfoFormat("Report start dates for BUID {0}: photo: {1}, complete {2}", businessUnitId, _reportStartDates.FirstPhotoStartDate, _reportStartDates.CompleteStartDate);

			Log.InfoFormat("Set MAXPhotos using reportStartDate: {0} for {1} InventoryIds", _reportStartDates.FirstPhotoStartDate, rowItems.Count());
			_ttmRepository.FillMAXPhotosFirstDate(firstPhotoRows, _reportStartDates.FirstPhotoStartDate);

			/************************************************************
			 * 
			 * All of the following fill methods were created
			 * after the initial release date. For that reason, they will
			 * use a filtered reports date.
			 * 
			 ************************************************************/

			Log.DebugFormat("Use CompleteReportStartDate: {0} for {1} InventoryIds", _reportStartDates.CompleteStartDate, rowItems.Count);

			var dataRows = allDataRows as RowItem[] ?? allDataRows.ToArray();
			
			Log.Debug("FillMAXMinimumPhotosFirstDate");
			_ttmRepository.FillMAXMinimumPhotosFirstDate(dataRows, _reportStartDates.CompleteStartDate);
			
			Log.Debug("FillMAXPriceFirstDate");
			_ttmRepository.FillMAXPriceFirstDate(dataRows);
			
			Log.Debug("FillMAXDescriptionFirstDate");
			_ttmRepository.FillMAXDescriptionFirstDate(dataRows);
			
			Log.Debug("FillAdApprovalFirstDate");
			_ttmRepository.FillAdApprovalFirstDate(dataRows);

			Log.Info("Updating TTMAX VIN data");
			_ttmRepository.UpdateVins(rowItems);
		}

	    private bool HasCarsApi(int businessUnitId)
	    {
	        return GetMiscSettings.GetSettings(businessUnitId).ReportDataSourceNew == ReportDataSource.CarsApi ||
	               GetMiscSettings.GetSettings(businessUnitId).ReportDataSourceUsed == ReportDataSource.CarsApi;
	    }

	    private DateTime Max(DateTime a, DateTime b)
	    {
            return a > b ? a : b;
	    }
	}

}