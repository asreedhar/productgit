﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using AmazingCloudSearch.MaxInterface;
using AmazingCloudSearch.Query;
using AmazingCloudSearch.Query.Boolean;
using Dapper;
using MAX.CloudSearch.Reconciliation.Models;

namespace MAX.CloudSearch.Reconciliation.CloudSearch
{
	public interface IVehicleCloudSearchDataRepository
	{
		IEnumerable<DealerInfo> GetActiveDealers();
		IEnumerable<VehicleSearchData> GetActiveInventoryForDealer(DealerInfo dealerInfo);
		IEnumerable<VehicleSearchData> GetCloudSearchVehicles(DealerInfo dealerInfo);
		int DeleteCloudSearchVehicles(IEnumerable<VehicleSearchData> deleteVehicles);
	}

	public class VehicleCloudSearchDataRepository : IVehicleCloudSearchDataRepository
	{
		public IEnumerable<DealerInfo> GetActiveDealers()
		{
			const string sql = @"dbo.GetCloudSearchEnabledBusinessUnits";

			using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
			{
				conn.Open();
				return conn.Query<DealerInfo>(sql, commandType: CommandType.StoredProcedure);
			}
		}

		public IEnumerable<VehicleSearchData> GetActiveInventoryForDealer(DealerInfo dealerInfo)
		{
			const string sql = @"SELECT @OwnerHandle AS OwnerHandle, VIN AS Vin 
								FROM IMT.dbo.Inventory 
								JOIN IMT.dbo.Vehicle ON Vehicle.VehicleId = Inventory.VehicleId
								LEFT JOIN Merchandising.builder.OptionsConfiguration ON OptionsConfiguration.businessUnitID = Inventory.BusinessUnitID AND OptionsConfiguration.inventoryId = Inventory.InventoryID
								WHERE Inventory.BusinessUnitId = @BusinessUnitId AND (InventoryActive = 1 OR 
								Inventory.DeleteDt > @DeleteDate)
								AND ISNULL(doNotPostFlag, 0) = 0";

			var deleteDateString = ConfigurationManager.AppSettings["RecentlyActiveDays"];
			DateTime deleteDate;
			if(string.IsNullOrWhiteSpace(deleteDateString))
				deleteDate = DateTime.Now.AddDays(-45).Date;
			else
				deleteDate = DateTime.Now.AddDays(0 - int.Parse(deleteDateString)).Date;

			using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
			{
				conn.Open();
				return conn.Query<VehicleSearchData>(
					sql,
					new { dealerInfo.BusinessUnitId, dealerInfo.OwnerHandle, DeleteDate = deleteDate }, commandTimeout: 45);
			}
		}

        // Not adding this method to the interface because it is just used in some integration tests
        public IEnumerable<VehicleSearchData> GetCloudSearchVehicles(DealerInfo dealerInfo, int size)
        {
            var csDomain = CloudSearchFactory<VehicleSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["vehicle_cloud_search_url"]);
            var bQuery = new BooleanQuery();
            var ownerCondition = new StringBooleanCondition("ownerhandle", dealerInfo.OwnerHandle.ToString().ToUpperInvariant());
            bQuery.Conditions.Add(ownerCondition);

            var searchQuery = new SearchQuery<VehicleSearchDocument> { BooleanQuery = bQuery, Size = size };
            var searchResults = csDomain.Search(searchQuery);
            return searchResults.hits.hit.Select(hit => new VehicleSearchData { OwnerHandle = Guid.Parse(hit.data.OwnerHandle), Vin = hit.data.Vin });

        }

		public IEnumerable<VehicleSearchData> GetCloudSearchVehicles(DealerInfo dealerInfo)
		{
		    return GetCloudSearchVehicles(dealerInfo, 1000);
		}

		public int DeleteCloudSearchVehicles(IEnumerable<VehicleSearchData> deleteVehicles)
		{

			var deleteDocuments =
				deleteVehicles.Select(dv => new VehicleSearchDocument { OwnerHandle = dv.OwnerHandle.ToString().ToUpperInvariant(), Vin = dv.Vin }).ToList();

			var csDomain = CloudSearchFactory<VehicleSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["vehicle_cloud_search_url"]);

			return csDomain.Delete(deleteDocuments).deletes;
		}
	}
}