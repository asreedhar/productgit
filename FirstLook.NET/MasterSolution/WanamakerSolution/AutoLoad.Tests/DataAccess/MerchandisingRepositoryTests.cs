﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoad.FordNew.DataAccess;
using NUnit.Framework;

namespace DataAccess.Tests
{
    [TestFixture]
    public class MerchandisingRepositoryTests
    {
        [Test]
        public void InventoryWithUnknownTrim()
        {
            MerchandisingRepository mr = new MerchandisingRepository();
            var inventory = mr.InventoryWithUnknownOptionsConfiguration(106311);

            foreach (Inventory inv in inventory)
            {
                ObjectDumper.Write(inv);
            }

            Assert.AreEqual(9, inventory.Count());
        }

        [Test]
        public void InventoryWithNoOptionsConfiguration()
        {
            MerchandisingRepository mr = new MerchandisingRepository();
            var inventory = mr.InventoryWithNoOptionsConfiguration(106311);
            foreach (var inventory1 in inventory)
            {
                ObjectDumper.Write(inventory1);
            }
            Assert.AreEqual(23, inventory.Count());
        }
    }
}
