﻿using AutoLoad.FordNew.DataAccess;
using NUnit.Framework;

namespace AutoLoad.Tests.DataAccess
{
    [TestFixture]
    public class InventoryRepositoryTests
    {
        [Test]
        public void GetAthensFordActiveInventory()
        {
            InventoryRepository ir = new InventoryRepository();
            var inventory = ir.ActiveInventory(106311);

            foreach(var inv in inventory)
            {
                ObjectDumper.Write(inv);
            }
        }

    }
}
