﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using AutoLoad.FordNew.DataAccess;
using AutoLoad.Services;
using MAX.Entities.Enumerations;
using NUnit.Framework;
using Moq;

namespace AutoLoad.Tests.DataAccess
{
    class FordDirectApiTests
    {
        /*
         * Internal dev testing use:
        [Test]
        public void IntGetYearMakeModels()
        {
            var thisYear = DateTime.UtcNow.Year;
            var fordService = new FordWebService(new List<Make> { Make.Ford, Make.Lincoln },
                new List<Int32> { thisYear }); // only test current year, normally we run +- 1 year as well
            var yearMakeModels = fordService.YearMakeModels;
            Assert.True(yearMakeModels.Count > 0, "Ford should have vehicles this year.");

            var aYmm = yearMakeModels.ElementAt(0);
            var newYmm = new YearMakeModel {Year = aYmm.Year, Make = aYmm.Make, Model = aYmm.Model};
            Assert.True(yearMakeModels.Contains(newYmm), "Comparator should detect dupes.");
        }*/

        [Test]
        public void UnitGetYearMakeModels()
        {
            var mockRequest = new Mock<WebRequest>();

            var mockResponse = new Mock<WebResponse>();
            
            var stream = new MemoryStream(Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                                                 "<Response status=\"OK\" ttl=\"14400\">" +
                                                                 "<ModelVersion make=\"Lincoln\" year=\"2014\" model=\"MKS\" config=\"02/10/2015 02:53\" pricing=\"Dec 22, 2014 10:00 PM\"><AppVersions></AppVersions><Attributes></Attributes></ModelVersion>" +
                                                                 "<ModelVersion make=\"Lincoln\" year=\"2014\" model=\"MKS\" config=\"02/10/2015 02:53\" pricing=\"Dec 22, 2014 10:00 PM\"><AppVersions></AppVersions><Attributes></Attributes></ModelVersion>" +
                                                                 "<ModelVersion make=\"Lincoln\" year=\"2014\" model=\"MKZ\" config=\"04/03/2014 11:11\" pricing=\"Dec 23, 2014 10:00 AM\"><AppVersions></AppVersions><Attributes></Attributes></ModelVersion><ModelVersion make=\"Lincoln\" year=\"2014\" model=\"MKT\" config=\"07/16/2014 04:28\" pricing=\"Dec 23, 2014 10:00 PM\"><AppVersions></AppVersions><Attributes></Attributes></ModelVersion><ModelVersion make=\"Lincoln\" year=\"2014\" model=\"Navigator\" config=\"07/19/2013 08:46\" pricing=\"Dec 22, 2014 10:00 AM\"><AppVersions></AppVersions><Attributes></Attributes></ModelVersion><ModelVersion make=\"Lincoln\" year=\"2014\" model=\"MKX\" config=\"02/25/2014 00:24\" pricing=\"Jan 6, 2015 10:00 PM\"><AppVersions></AppVersions><Attributes></Attributes></ModelVersion></Response>"));
            // Added dupe MKS above ^
            mockResponse.Setup(r => r.GetResponseStream()).Returns(stream);

            mockRequest.Setup(r => r.GetResponse()).Returns(mockResponse.Object);

            var thisYear = DateTime.UtcNow.Year;
            var fordService = new FordWebService(new List<Make> { Make.Ford, Make.Lincoln },
                new List<Int32> { thisYear }, mockRequest.Object);
            var yearMakeModels = fordService.YearMakeModels;
            Assert.True(yearMakeModels.Count > 0, "Ford should have vehicles this year.");

            Assert.True(yearMakeModels.Count == 5, "Should not have duplicate MKS model.");

            var aYmm = yearMakeModels.ElementAt(0);
            var newYmm = new YearMakeModel { Year = aYmm.Year, Make = aYmm.Make, Model = aYmm.Model };
            Assert.True(yearMakeModels.Contains(newYmm), "Comparator should detect dupes.");
        }
    }
}
