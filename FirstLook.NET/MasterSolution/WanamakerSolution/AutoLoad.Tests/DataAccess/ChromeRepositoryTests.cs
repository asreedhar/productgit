﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoad.FordNew.DataAccess;
using NUnit.Framework;

namespace DataAccess.Tests
{
    [TestFixture]
    public class ChromeRepositoryTests
    {
        [Test]
        public void GetStyles()
        {
            var cr = new ChromeRepository();
            var styles = cr.GetStyles("1FMJU1H52DEF23443");

            foreach(var style in styles)
            {
                ObjectDumper.Write(style);
            }

            Assert.AreEqual(2, styles.Select(s => s.StyleId).Distinct().Count());
            foreach(var style in styles)
            {
                Assert.AreEqual(3, style.WheelBases.Count);
                Assert.AreEqual(3, style.WheelBases.Count);
            }
        }

        [Test]
        public void GetOptions()
        {
            var cr = new ChromeRepository();
            var styles = cr.GetStyles("1FMJU1K51DEF11759");

            var options = cr.GetOptions(styles.Select(s => s.StyleId).Distinct());

            foreach(var option in options)
            {
                ObjectDumper.Write(option);
            }

            Assert.AreEqual(48, options.Count());
        }

        [Test]
        public void GetColors()
        {
            var cr = new ChromeRepository();
            var styles = cr.GetStyles("1FMJU1K51DEF11759");

            var colors = cr.GetColors(styles.Select(s => s.StyleId).Distinct());

            foreach (var color in colors)
            {
                ObjectDumper.Write(color);
            }

            Assert.AreEqual(14, colors.Count());
        }

    }

}
