﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoLoad.FordNew.DataAccess;
using AutoLoad.FordNew.Domain;
using NUnit.Framework;

namespace AutoLoad.Tests.Domain
{
    [TestFixture]
    public class OptionComparerTests
    {
        private OptionCodeComparer _optionCodeComparer;
        private List<ChromeOption> _chromeOptionList;

        [SetUp]
        public void Setup()
        {

            _optionCodeComparer = new OptionCodeComparer();

            _chromeOptionList = new List<ChromeOption>
                {
                    new ChromeOption {OptionCode = "test"},
                    new ChromeOption {OptionCode = "41H-1"},
                    new ChromeOption {OptionCode = "41H-2"},
                    new ChromeOption {OptionCode = "HR"},
                    new ChromeOption {OptionCode = "HR-R"},
                    new ChromeOption {OptionCode = "HR-F"},
                    new ChromeOption {OptionCode = "200A"},
                    new ChromeOption {OptionCode = "201A"},
                    new ChromeOption {OptionCode = "202A-0"},
                    new ChromeOption {OptionCode = "202B-0"},
                    new ChromeOption {OptionCode = "153-0"},
                    new ChromeOption {OptionCode = "998-2"},
                    new ChromeOption {OptionCode = "998-1"},
                    new ChromeOption {OptionCode = ""}
                };
        }


        [TestCase("test", "test")]
        [TestCase("202", "202-1")]
        [TestCase("202-R", "202-R")]
        [TestCase("101", "101-R")]
        [TestCase("101", "101-0-R")]
        [TestCase("101-0-R", "101")]
        [TestCase("202A", "202A-0")]
        [TestCase("-PAINT", "-PAINT")]
        [TestCase("-RET202A", "-RET202A")]
        [TestCase("998-1", "998-2")]
        public void CompareOptionCodesShouldMatch(string x, string y)
        {
            var comparer = new OptionCodeComparer();

            Assert.IsTrue(comparer.Equals(x, y), string.Format("Option code '{0}' and '{1}' should match!", x, y));

        }

        [TestCase("201", "202-1-F")]
        [TestCase("102-1", "202-1")]
        [TestCase("AB", "ABC")]
        [TestCase("", "")]
        [TestCase("-RET202A", "RET202A")]
        public void CompareOptionCodesShouldNotMatch(string x, string y)
        {
            var comparer = new OptionCodeComparer();

            Assert.IsFalse(comparer.Equals(x, y), string.Format("Option code '{0}' and '{1}' not should match!", x, y));
        }

        [TestCase("202A")]
        [TestCase("998")]
        [TestCase("998-1")]
        public void CompareOptionCodesContains(string optionCode)
        {

            var resultList = from optList in _chromeOptionList
                             where _optionCodeComparer.Equals(optList.OptionCode, optionCode)
                             select optList;


            Assert.GreaterOrEqual(resultList.Count(), 1,
                                  string.Format("Option Code: {0} was not found in the option list", optionCode));

        }

        [TestCase("203A")]
        public void CompareOptionCodesNotContains(string optionCode)
        {

            var resultList = from optList in _chromeOptionList
                       where _optionCodeComparer.Equals(optList.OptionCode, optionCode)
                       select optList;


            Assert.AreEqual(resultList.Count(), 0, string.Format("Option Code: {0} was found in the option list", optionCode));

        }

        [Test]
        public void OptionComparerDistinctTest()
        {

            var distinctList = _chromeOptionList.Distinct(new ChromeOptionComparer()).ToList();

            Assert.AreEqual(distinctList.Count(), 10, "Distinct Chrome options test failed to eliminate duplicates");

        }

    }
}
