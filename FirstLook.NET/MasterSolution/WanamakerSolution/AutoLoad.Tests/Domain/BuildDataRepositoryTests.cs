﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using AutoLoad.FordNew.DataAccess;
using AutoLoad.FordNew.Domain;
using DataAccess;
using Merchandising.Messages;
using NUnit.Framework;

namespace AutoLoad.Tests.Domain
{
    [TestFixture]
    public class BuildDataRepositoryTests
    {
        private YearMakeModel _ymm;
        private VehicleDetails _vd;
        private DealerCode _dc;

        private IFileStoreFactory _fileStoreFactory;
        private BuildDataRepository _buildDataRepository;

/*
        _vd = new VehicleDetails();
        _vd.DealerPaCode = "paCode";
        _vd.Document = new XDocument();
        _vd.Make = "make";
        _vd.Model = "model";
        _vd.Url = "http://slashdot.org";
        _vd.Year = 2013;
*/
        [SetUp]
        public void Setup()
        {
            _fileStoreFactory = new FileStoreFactory();
            _buildDataRepository = new BuildDataRepository(_fileStoreFactory);            
        }

        [Test]
        public void FileName()
        {
            _ymm = new YearMakeModel(2013,"make","model");
            _dc = new DealerCode(1,"2");

            string fileName = _buildDataRepository.FileName(_ymm, _dc);

            string expected = "FordDirect/2013-make-model_1-2";

            // FileName appends a timestamp to the end, so just make sure the rest matches.
            Assert.AreEqual(expected, fileName.Substring(0,30));
        }

        [Test]
        public void FileDoesNotExist()
        {
            _ymm = new YearMakeModel(2013, "make", "model");
            _dc = new DealerCode(1, "3");

            var file = _buildDataRepository.FindDocument(_ymm, _dc);

            Assert.IsNull(file);
        }

        [Ignore]
        [Test]
        public void WriteFile()
        {
            _ymm = new YearMakeModel(2013,"make","model");
            _dc = new DealerCode(1,"2");
            XDocument doc = 
                new XDocument( new XElement("test") );

            Console.WriteLine(doc.ToString());

            VehicleDetails vd = new VehicleDetails();
            vd.Document = doc;

            _buildDataRepository.SaveDocument(vd, _ymm, _dc);

            // wait a few seconds
            Thread.Sleep(5000);

            var found = _buildDataRepository.FindDocument(_ymm, _dc);
            Assert.IsNotNull(found);

            Assert.AreEqual(doc.ToString(), found.ToString());

        }
    }
}
