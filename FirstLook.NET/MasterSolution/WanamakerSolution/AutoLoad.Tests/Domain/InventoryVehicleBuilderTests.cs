﻿using System;
using System.Linq;
using System.Xml.Linq;
using AutoLoad.FordNew.Domain;
using NUnit.Framework;

namespace AutoLoad.Tests.Domain
{
    [TestFixture]
    public class InventoryVehicleBuilderTests
    {
        [Test]
        public void VehicleBuilds()
        {
            XDocument doc =
                XDocument.Load("..\\..\\Domain\\ExampleXml\\VehicleDetails-F-150.xml");

            var fd = new InventoryVehicleBuilder();

            var inventory = fd.VehicleBuilds(doc);

            Assert.IsNotNull(inventory);

            var FB79841 = inventory.First(i => i.StockNumber == "FB79841");
            Assert.IsNotNull(FB79841);

            Assert.AreEqual("Ford",FB79841.Make);
            Assert.AreEqual("09775", FB79841.DealerPaCode);
            Assert.AreEqual("F-150",FB79841.Model);
            Assert.IsNotNull(FB79841.Options);
            Assert.IsTrue(FB79841.Options.Any() );

            Console.Write("Options: ");
            foreach(var option in FB79841.Options)
            {
                Console.Write(option + ",");
            }

            Console.WriteLine();
            Console.Write("Parts: ");
            foreach (var part in FB79841.Parts)
            {
                Console.Write(part + ",");
            }


            Assert.AreEqual("SuperCrew 4X2 - 301A", FB79841.Product);
            Assert.AreEqual("FB79841", FB79841.StockNumber);
            Assert.AreEqual("1FTEW1CM1DFB79841", FB79841.Vin);
            Assert.AreEqual("145",FB79841.WheelBase);
            Assert.AreEqual("http://www.inventory.ford.com/services/inventory/WindowSticker.pdf?vin=1FTEW1CM1DFB79841", FB79841.WindowStickerUrl);
            Assert.AreEqual("2013",FB79841.Year);
        }

        
    }
}
