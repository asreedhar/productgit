﻿using System.Collections.Generic;
using System.Linq;
using AmazonServices;
using AutoLoad.FordNew.DataAccess;
using AutoLoad.FordNew.Domain;
using Autofac;
using FirstLook.Common.Core.IOC;
using Merchandising.Messages;
using MerchandisingRegistry;
using NUnit.Framework;

namespace AutoLoad.Tests.Domain
{
    [TestFixture]
    public class ChromeStyleMatcherTests
    {
        private ChromeStyleMatcher _matcher;
        private InventoryVehicleBuild _build;
        private List<ChromeStyle> _styles;
        private List<ChromeOption> _options;
        private List<ChromeColor> _colors;

        ChromeStyle _foundStyle;
        IEnumerable<ChromeOption> _foundOptions;
        ChromeColor _foundColor;

        public IQueueFactory QueueFactory { get; set; }
        public IFileStoreFactory FileStoreFactory { get; set; }
        private IContainer _container;

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new MerchandisingDomainModule());
            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterModule(new AmazonServicesModule());
            //RegisterAssemblyTypes(builder);

            _container = builder.Build();
            Registry.RegisterContainer(_container);

            Registry.BuildUp(this);
        }

        [SetUp]
        public void TestSetUp()
        {
            _matcher = new ChromeStyleMatcher();
//            _message = new FordDirectAutoLoadMessage();

//            _inventory = new Inventory();
            _build = new InventoryVehicleBuild();
            _styles = new List<ChromeStyle>();
            _options = new List<ChromeOption>();
            _colors = new List<ChromeColor>();

            _foundStyle = null;
            _foundOptions = null;
            _foundColor = null;
        }

        [Test]
        public void MatchNull()
        {
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
        }

        [Test]
        public void MatchSingleStyle()
        {
            // Match on StyleCode "A"
            _build.ModelCode = "A";

            // Setup a single style - it will match since there is only once choice.
            _styles.Add(new ChromeStyle { StyleCode = "B", StyleId = 1 });
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }

        [Test]
        public void MatchStyleCodeFails()
        {
            // Match on StyleCode "A"
            _build.ModelCode = "A";

            // Setup two styles, neither of which match on the build's style code.
            _styles.Add(new ChromeStyle { StyleCode = "B", StyleId = 1 });
            _styles.Add(new ChromeStyle { StyleCode = "C", StyleId = 2 });
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);
            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            _styles.Clear();
        }

        [Test]
        public void MatchStyleCodePasses()
        {
            // Match on StyleCode "A"
            _build.ModelCode = "A";

            // try again with a match 
            _styles.Add(new ChromeStyle { StyleCode = "A", StyleId = 1 });
            _styles.Add(new ChromeStyle { StyleCode = "B", StyleId = 2 });
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();

            // try again with a case mis-match 
            _styles.Add(new ChromeStyle { StyleCode = "a", StyleId = 1 });
            _styles.Add(new ChromeStyle { StyleCode = "B", StyleId = 2 });
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }

        [Test]
        public void MatchStyleCodeEmptyFails()
        {
            // Match on StyleCode "A"
            _build.ModelCode = "";

            // We need two styles, since it will pick the first if only supplied with one.
            _styles.Add(new ChromeStyle { StyleCode = "", StyleId = 1 });
            _styles.Add(new ChromeStyle { StyleCode = "b", StyleId = 2 });
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);

            _styles.Clear();
        }

        [Test]
        public void MatchFullStyleCode()
        {
            _build.ModelCode = "A";

            // try again with a match 
            _styles.Add(new ChromeStyle { FullStyleCode = "A", StyleId = 1 });
            _styles.Add(new ChromeStyle { FullStyleCode = "B", StyleId = 2 });
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();

            // try again with a case mis-match 
            _styles.Add(new ChromeStyle { FullStyleCode = "a", StyleId = 1 });
            _styles.Add(new ChromeStyle { FullStyleCode = "B", StyleId = 2 });
            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }

/*
        [Test]
        public void MatchWheelBase()
        {
            _build.WheelBase = "144";
            _styles.Add(new ChromeStyle { WheelBase = new decimal(144.00) });
            _styles.Add(new ChromeStyle { WheelBase = new decimal(156.00) });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }

*/
        [Test]
        public void MatchTrim()
        {
            _build.Model = "XLT";
            _styles.Add(new ChromeStyle { Trim = "XLT", StyleId = 1 });
            _styles.Add(new ChromeStyle { Trim = "Limited", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }

        [Test]
        public void MatchAutomaticTransmissionFails()
        {
            _build.AutomaticTransmission = true;
            _styles.Add(new ChromeStyle { AutoTrans = "N", StyleId = 1});
            _styles.Add(new ChromeStyle { AutoTrans = "N", StyleId = 2});

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);

            _styles.Clear();
        }

        [Test]
        public void MatchAutomaticTransmissionStandard()
        {
            _build.AutomaticTransmission = true;
            _styles.Add(new ChromeStyle { AutoTrans = "S", StyleId = 1 }); // standard
            _styles.Add(new ChromeStyle { AutoTrans = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }

        [Test]
        public void MatchAutomaticTransmissionOptional()
        {
            _build.AutomaticTransmission = true;
            _styles.Add(new ChromeStyle { AutoTrans = "O", StyleId = 1 }); // standard
            _styles.Add(new ChromeStyle { AutoTrans = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }

        [Test]
        public void MatchDriveTrainFWDFails()
        {
            _build.DriveTrain = "fwd";
            _styles.Add(new ChromeStyle { FrontWd = "N", StyleId = 1 });
            _styles.Add(new ChromeStyle { FrontWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);

            _styles.Clear();

        }

        [Test]
        public void MatchDriveTrainFWDStandard()
        {
            _build.DriveTrain = "fwd";
            _styles.Add(new ChromeStyle { FrontWd = "S", StyleId = 1 });
            _styles.Add(new ChromeStyle { FrontWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();
        }


        [Test]
        public void MatchDriveTrainRWDFails()
        {
            _build.DriveTrain = "rwd";
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 1 });
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);

            _styles.Clear();
        }

        [Test]
        public void MatchDriveTrainRWDPasses()
        {
            _build.DriveTrain = "rwd";
            _styles.Add(new ChromeStyle { RearWd = "S", StyleId = 1 });
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();

            _styles.Add(new ChromeStyle { RearWd = "O", StyleId = 1 });
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);
        }

        [Test]
        public void MatchDriveTrain4x2Fails()
        {
            _build.DriveTrain = "4x2";
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 1});
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 2});

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);

            _styles.Clear();
        }

        [Test]
        public void MatchDriveTrain4x2Passes()
        {
            _build.DriveTrain = "4x2";
            _styles.Add(new ChromeStyle { RearWd = "S", StyleId = 1 });
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();

            _styles.Add(new ChromeStyle { RearWd = "O", StyleId = 1 });
            _styles.Add(new ChromeStyle { RearWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);
        }

        [Test]
        public void MatchDriveTrain4x4Fails()
        {
            _build.DriveTrain = "4x4";
            _styles.Add(new ChromeStyle { FourWd = "N", StyleId = 1});
            _styles.Add(new ChromeStyle { FourWd = "N", StyleId = 2});

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);

            _styles.Clear();
        }

        [Test]
        public void MatchDriveTrain4x4Passes()
        {
            _build.DriveTrain = "4x4";
            _styles.Add(new ChromeStyle { FourWd = "S", StyleId = 1 });
            _styles.Add(new ChromeStyle { FourWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);

            _styles.Clear();

            _styles.Add(new ChromeStyle { FourWd = "O", StyleId = 1 });
            _styles.Add(new ChromeStyle { FourWd = "N", StyleId = 2 });

            _matcher.Match(_build, _styles, _options, _colors, out _foundStyle, out _foundOptions, out _foundColor);

            Assert.IsNotNull(_foundStyle);
            Assert.IsEmpty(_foundOptions);
            Assert.IsNull(_foundColor);
            Assert.AreSame(_styles.First(), _foundStyle);
        }

    }
}
