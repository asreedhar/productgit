﻿using AmazonServices;
using AutoLoad.FordNew.Domain;
using Autofac;
using FirstLook.Common.Core.IOC;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using MerchandisingRegistry;
using NUnit.Framework;

namespace AutoLoad.Tests.Domain
{
    [TestFixture]
    public class FordDirectAutoLoadMessageProcessorTests
    {
        public IQueueFactory QueueFactory { get; set; }
        public IFileStoreFactory FileStoreFactory { get; set; }
        private IContainer _container;

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new MerchandisingDomainModule());
            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterModule(new AmazonServicesModule());
            //RegisterAssemblyTypes(builder);

            _container = builder.Build();
            Registry.RegisterContainer(_container);

            Registry.BuildUp(this);
        }

        [Test]
        public void RunProcessor()
        {
            var message = new FordDirectAutoLoadMessage();
            message.DealerPaCode = "";

            var task = new FordDirectAutoLoadMessageProcessor(QueueFactory, FileStoreFactory);
            task.Process(message);
        }

       

    }
}
