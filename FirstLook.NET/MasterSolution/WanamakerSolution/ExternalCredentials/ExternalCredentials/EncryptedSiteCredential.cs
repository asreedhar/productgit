using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using FirstLook.Common.Core.Data;
using Merchandising.Messages;

namespace MAX.ExternalCredentials
{
    public interface IEncryptedSiteCredential
    {
        int BusinessUnitId { get; }
        bool IsLockedOut { get; }
        string DealerCode { get; }
        Agent SiteId { get; }
        string UserName { get; set; }
        string Password { get; set; }
        byte BuildRequestType { get; set; }
        Guid CredentialHash { get; }
        Byte[] CredentialVersion { get; set; }

        //void Lockout(int businessUnitId, Agent site);
        int Save(bool unlock);
        int Save();
        NetworkCredential GetCredential(Uri uri, string authType);
        NetworkCredential GetCredential(string host, int port, string authenticationType);
    }

    public class EncryptedSiteCredential : NetworkCredential, IEncryptedSiteCredential
    {

        private const string Key = "test1234";
        public int BusinessUnitId { get; private set; }
        public bool IsLockedOut { get; private set; }
        public string DealerCode { get; private set; }
		public Agent SiteId { get; private set; }
        public byte BuildRequestType { get; set; } // not updatable at this point, used to find correct build request method
        public Guid CredentialHash { get; private set; }
        public Byte[] CredentialVersion { get; set; }



        public EncryptedSiteCredential()
        {
        }

        public EncryptedSiteCredential(int businessUnitId, Agent siteId, string dealerCode, string username, string password, bool isLockedOut)
            : base(username, password)
        {
            IsLockedOut = isLockedOut;
            BusinessUnitId = businessUnitId;
            DealerCode = dealerCode;
			SiteId = siteId;
        }
        public EncryptedSiteCredential(int businessUnitId, Agent siteId, string dealerCode, string username, string password, bool isLockedOut, byte buildRequestType, Guid credentialHash, Byte[] credentialVersion)
            : base(username, password)
        {
            IsLockedOut = isLockedOut;
            BusinessUnitId = businessUnitId;
            DealerCode = dealerCode;
            SiteId = siteId;
            BuildRequestType = buildRequestType;
            CredentialHash = credentialHash;
            CredentialVersion = credentialVersion;

        }


        public static IEnumerable<EncryptedSiteCredential> Fetch(int businessUnitId, bool allowLockedOutCredentials)
        {
            using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                using (var cmd = con.CreateCommand())
                {
                    var credentials = new List<EncryptedSiteCredential>();

                    cmd.CommandText =
                        @"	SELECT * 
                        FROM settings.Dealer_SiteCredentials
                        WHERE businessUnitId = @BusinessUnitId 
                        AND (@allowLockedOutCredentials = 1 OR isLockedOut = 0)"
                        ;
                    cmd.CommandType = CommandType.Text;
                    Database.AddWithValue(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddWithValue(cmd, "allowLockedOutCredentials", allowLockedOutCredentials, DbType.Boolean);

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var credential = GetCredentialFromReader(reader);
                            credentials.Add(credential);
                        }
                        return credentials;
                    }
                }
            }
        }

        private static EncryptedSiteCredential GetCredentialFromReader(IDataReader reader)
        {
            int businessUnitId = reader.GetInt32(reader.GetOrdinal("businessUnitId"));
            string username = reader.GetString(reader.GetOrdinal("username"));
            string password = EncryptionHelper.Decrypt(reader.GetString(reader.GetOrdinal("encryptedPassword")), Key);
            var siteId = (Agent) reader.GetInt32(reader.GetOrdinal("siteId"));
			string dealercode = reader.GetString(reader.GetOrdinal("dealerCode"));
            var lockedOut = reader.GetBoolean(reader.GetOrdinal("isLockedOut"));
            byte buildRequestType = reader.GetByte(reader.GetOrdinal("buildRequestType"));
            Guid credentialHash = reader.GetGuid(reader.GetOrdinal("credentialHash"));
            var credentialVersion = new byte[8];  // sqlServer rowversion is an 8 byte array (can be converted to a uLong if necessary)

            reader.GetBytes(reader.GetOrdinal("credentialVersion"), 0, credentialVersion, 0, 8);

            var credential = new EncryptedSiteCredential(businessUnitId, siteId, dealercode, username, password, lockedOut, buildRequestType, credentialHash, credentialVersion);

            return credential;
        }

        public static EncryptedSiteCredential Fetch(int businessUnitId, Agent site, bool allowLockedOutCredentials)
        {
            using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.DealerSiteCredentials#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue( cmd, "businessUnitId", businessUnitId, DbType.Int32 );
                    Database.AddWithValue( cmd, "siteId", (int)site, DbType.Int32 );
                    Database.AddWithValue(cmd, "allowLockedOutCredentials", allowLockedOutCredentials, DbType.Boolean);

                    if( con.State == ConnectionState.Closed )
                    {
                        con.Open();
                    }

                    using (var reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        return reader.Read() ? GetCredentialFromReader(reader) : null;
                    }
                }
            }            
        }

        public static EncryptedSiteCredential Fetch(Guid credentialHash)
        {
            using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                using (var cmd = con.CreateCommand())
                {
                    con.Open();

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"
                        SELECT *
	                        FROM settings.Dealer_SiteCredentials
	                        WHERE credentialHash = @credentialHash
                        ";

                    cmd.Parameters.AddWithValue("credentialHash", credentialHash);

                    using (var reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        return reader.Read() ? GetCredentialFromReader(reader) : null;
                    }
                }
            }
        }

        public static void Delete(int businessUnitId, Agent site, Byte[] credentialVersion)
        {
            using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                // switched to SQLCommand to support timestamp FB: 31286
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.DealerSiteCredentials#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("siteId", (int) site);
                    cmd.Parameters.AddWithValue("businessUnitId", businessUnitId);
                    cmd.Parameters.AddWithValue("credentialVersion", credentialVersion);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    
                }
            }
        }

        //public static void SetLockoutStatus(int businessUnitId, Agent site, bool isLockedOut)
        //{
        //    using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
        //    {
        //        using (IDbCommand cmd = con.CreateCommand())
        //        {
        //            cmd.CommandText = "Merchandising.DealerSiteCredentials#SetLockoutStatus";
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            Database.AddWithValue(cmd, "siteId", (int)site, DbType.Int32);
        //            Database.AddWithValue(cmd, "businessUnitId", businessUnitId, DbType.Int32);
        //            Database.AddWithValue(cmd, "isLockedOut", isLockedOut, DbType.Boolean);
        //            con.Open();
        //            cmd.ExecuteNonQuery();

        //        }
        //    }
        //}
        //public void Lockout(int businessUnitId, Agent site) { SetLockoutStatus(businessUnitId, site, true); }

        public int SetLockAndSave(bool lockCredentials)
        {
            IsLockedOut = lockCredentials;

            using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                con.Open();

                // switched to SQLCommand to support timestamp FB: 31286
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.DealerSiteCredentials#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("siteId", SiteId);
                    cmd.Parameters.AddWithValue("businessUnitId", BusinessUnitId);
                    cmd.Parameters.AddWithValue("username", UserName);
                    cmd.Parameters.AddWithValue("encryptedPassword", EncryptionHelper.Encrypt(Password, Key));
                    cmd.Parameters.AddWithValue("dealerCode", DealerCode);

                    cmd.Parameters.AddWithValue("LockOut", IsLockedOut);
                    cmd.Parameters.AddWithValue("credentialVersion", CredentialVersion);

                    cmd.Parameters.Add("rowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    return Convert.ToInt32(cmd.Parameters["rowsUpdated"].Value);
                    
                }

            }

        }

        public static bool DoesDealerHaveAnyValidCredentials(int businessUnitId)
        {
            using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.GetDealerSiteCredentialsCount";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "businessUnitId", businessUnitId, DbType.Int32);

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }

                    Object o = cmd.ExecuteScalar();
                    if (o == null) return false;
                    int count;
                    var success = int.TryParse(o.ToString(), out count);
                    return success && count > 0;
                }
            }
        }

        public int Save(bool unlock)
        {
            using (var con = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                // switched to SQLCommand to support timestamp FB: 31286
                using (var cmd = con.CreateCommand())
                {
                    
                    cmd.CommandText = "Merchandising.DealerSiteCredentials#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("siteId", SiteId);
                    cmd.Parameters.AddWithValue("businessUnitId", BusinessUnitId);
                    cmd.Parameters.AddWithValue("username", UserName);
                    cmd.Parameters.AddWithValue("encryptedPassword", EncryptionHelper.Encrypt(Password, Key));
                    cmd.Parameters.AddWithValue("dealerCode", DealerCode);
                    if (CredentialVersion != null)
                        cmd.Parameters.AddWithValue("credentialVersion", CredentialVersion);
                    cmd.Parameters.Add("rowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output;
                    
                    if (unlock)
                    {
                        Database.AddWithValue(cmd, "LockOut", 0, DbType.Boolean);
                    }

                    con.Open();
                    cmd.ExecuteNonQuery();

                    return Convert.ToInt32(cmd.Parameters["rowsUpdated"].Value);

                }
            }
        }
        public int Save() 
        { 
            return Save(true); 
        }
    }

}

