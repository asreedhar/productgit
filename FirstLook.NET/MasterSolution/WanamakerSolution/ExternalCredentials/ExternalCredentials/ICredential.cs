using System;

namespace MAX.ExternalCredentials
{
    public interface ICredential
    {
        CredentialGroup Group { get; }
        int TypeId { get; }
        string Description { get; }
        string UserName { get; set; }
        string Password { get; set; }
        string ExternalID { get; set; }
        bool IsValid { get; }
        bool IsInsufficientPermissions { get; }
        Byte[] CredentialVersion { get; set; }
    }
}