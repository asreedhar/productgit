﻿using Autofac;


namespace MAX.ExternalCredentials
{
    public class ExternalCredentialsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CredentialRepository>().As<ICredentialRepository>();
            builder.RegisterType<CredentialedSiteRepository>().As<ICredentialedSiteRepository>();
            //builder.RegisterType<EncryptedSiteCredential>().As<IEncryptedSiteCredential>();
            base.Load(builder);
        }

    }
}
