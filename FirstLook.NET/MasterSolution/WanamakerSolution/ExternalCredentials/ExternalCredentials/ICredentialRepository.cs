using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages;

namespace MAX.ExternalCredentials
{
    public interface ICredentialRepository
    {
        List<ICredential> GetCredentials(int businessUnitId);
        void SaveCredential(int businessUnitId, ICredential credential);
        EncryptedSiteCredential Fetch(int businessUnitId, Agent site, bool allowLockedOutCredentials = false);
        EncryptedSiteCredential GetSiteCredentials(InventoryData inv, bool allowLockedOutCredentials = false);

    }
}