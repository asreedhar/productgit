using System;
using MAX.ExternalCredentials;
using Merchandising.Messages;

namespace MAX.ExternalCredentials
{
    public class OEMSiteCredential : AbstractSiteCredential
    {

        private readonly Agent _agent;

        public OEMSiteCredential(Agent agent, string userName, string password, bool isValid, Byte[] credentialVersion)
            : base(userName, password, isValid, false)
        {
            CredentialVersion = credentialVersion;
            _agent = agent;
        }

        public OEMSiteCredential(Agent agent, string userName, string password, bool isValid, Byte[] credentialVersion, bool isInsufficientPermissions)
            : base(userName, password, isValid, isInsufficientPermissions)
        {
            CredentialVersion = credentialVersion;
            _agent = agent;
        }

        public override CredentialGroup Group
        {
            get { return CredentialGroup.OEM; }
        }

        public override int TypeId
        {
            get { return (int) _agent; }
        }

        public override string Description
        {
            get { return _agent.ToString(); }
        }
    }
}