﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Dapper;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace MAX.ExternalCredentials
{
    public class CredentialRepository : ICredentialRepository
    {

        public List<ICredential> GetCredentials(int businessUnitId)
        {
            var credentials = new List<ICredential>();
            credentials.AddRange(GetOEMSiteCredentials(businessUnitId));
            credentials.AddRange(GetListingSiteCredentials(businessUnitId));
            return credentials;
        }

        public EncryptedSiteCredential Fetch(int businessUnitId, Agent site, bool allowLockedOutCredentials = false)
        {
            return EncryptedSiteCredential.Fetch(businessUnitId, site, allowLockedOutCredentials);
        }

        public void SaveCredential(int businessUnitId, ICredential credential)
        {
            if (credential.Group == CredentialGroup.OEM)
            {
                SaveCredential(businessUnitId, (OEMSiteCredential) credential);
                return;
            }

            if (credential.Group == CredentialGroup.ListingSite)
            {
                SaveCredential(businessUnitId, (ListingSiteCredential) credential);
                return;
            }

            throw new NotSupportedException(string.Format("Save is not supported for this implementation of ICredential: {0}", credential.GetType()));
        }

        /// <summary>
        /// get the appropriate build request site credentials for a particular vehicle
        /// 
        /// left some 'room' in here for shared credentials
        /// 
        /// there can be more that one set of credentials for a vehicle make (toyota).  added priority on vehicleAutoloadSettings
        /// </summary>
        /// <param name="inv">inventory item</param>
        /// <param name="allowLockedOutCredentials">only return unlocked credentials </param>
        /// <returns></returns>
        public EncryptedSiteCredential GetSiteCredentials(InventoryData inv, bool allowLockedOutCredentials = false)
        {
            EncryptedSiteCredential creds = null;
            List<SiteCredential> siteCredentialList;

            // get creds available for this build request
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT dealer_siteCredentials.siteID, dealer_siteCredentials.BusinessUnitId, isLockedOut, priority
                            FROM settings.dealer_siteCredentials
                            INNER JOIN settings.CredentialedSites ON dealer_siteCredentials.siteID = CredentialedSites.siteID
                            INNER JOIN merchandising.VehicleAutoLoadSettings ON  VehicleAutoLoadSettings.siteID = CredentialedSites.siteID
                            INNER JOIN VehicleCatalog.Chrome.Divisions ON VehicleAutoLoadSettings.ManufacturerID = Divisions.ManufacturerID	
                                                                                    AND Divisions.CountryCode = 1
                            WHERE Divisions.DivisionName = @Make
                            AND dealer_siteCredentials.businessUnitID = @BusinessUnitId
                            AND credentialedSites.canRequestData = 1
                            ORDER BY priority

                    ";

                siteCredentialList = sqlConnection.Query<SiteCredential>(query, new { BusinessUnitId = inv.BusinessUnitID, inv.Make }).ToList();
                sqlConnection.Close();

            }

            if (!allowLockedOutCredentials)
            {
                siteCredentialList = (from siteCreds in siteCredentialList
                                      where siteCreds.IsLockedOut == false
                                      orderby siteCreds.priority
                                      select siteCreds
                                      ).ToList();
            }


            // find out if we have any
            if (siteCredentialList.Count > 0)
            {
                creds = EncryptedSiteCredential.Fetch(siteCredentialList.First().BusinessUnitId, (Agent)siteCredentialList.First().SiteId, allowLockedOutCredentials);
            }

            return creds;

        }

        private class SiteCredential
        {
            public int SiteId { get; set; }
            public int BusinessUnitId { get; set; }
            public bool IsLockedOut { get; set; }
            public byte priority { get; set; }
        }



        #region static methods

        public static ICredential GetListingSiteCredential(int businessUnitId, EdtDestinations destination)
        {
            ListingSiteCredential credential = null;

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                connection.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Release.ListingSitePreferences#Fetch";
                    command.Parameters.AddWithValue("BusinessUnitId", businessUnitId);
                    command.Parameters.AddWithValue("DestinationId", (int)destination);
                    command.Parameters.AddWithValue("OnlyActiveReleaseSites", false);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var destinationId = (EdtDestinations)reader.GetInt32(reader.GetOrdinal("destinationId"));
                            var username = reader.GetString(reader.GetOrdinal("username"));
                            var password = reader.GetString(reader.GetOrdinal("password"));
                            var hasFailedLogin = reader.GetBoolean(reader.GetOrdinal("hasFailedLogin"));
                            var ListingSiteDealerId = reader.IsDBNull(reader.GetOrdinal("ListingSiteDealerId"))? null : reader.GetString(reader.GetOrdinal("ListingSiteDealerId"));

                            credential = new ListingSiteCredential(destinationId, username, password, !hasFailedLogin);
                            credential.ExternalID = ListingSiteDealerId;
                        }
                    }
                }
            }

            return credential;
        }


        public static IEnumerable<ICredential> GetListingSiteCredentials(int businessUnitId)
        {
            var credentials = new List<ListingSiteCredential>();

            using (var connection = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                connection.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Release.ListingSitePreferences#Fetch";
                    command.Parameters.AddWithValue("BusinessUnitId", businessUnitId);
                    command.Parameters.AddWithValue("OnlyActiveReleaseSites", false);
                    using (var reader = command.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var destinationId = (EdtDestinations) reader.GetInt32(reader.GetOrdinal("destinationId"));
                            var username = reader.GetString(reader.GetOrdinal("username"));
                            var password = reader.GetString(reader.GetOrdinal("password"));
                            var hasFailedLogin = reader.GetBoolean(reader.GetOrdinal("hasFailedLogin"));
                            var ListingSiteDealerId = reader.IsDBNull(reader.GetOrdinal("ListingSiteDealerId")) ? null : reader.GetString(reader.GetOrdinal("ListingSiteDealerId"));

                            var credential = new ListingSiteCredential(destinationId, username, password, !hasFailedLogin);
                            credential.ExternalID = ListingSiteDealerId;

                            credentials.Add( credential );
                        }
                    }
                }
            }

            return credentials;
        }

        internal static void SaveCredential(int businessUnitId, OEMSiteCredential credential )
        {
            var esCredential = EncryptedSiteCredential.Fetch(businessUnitId, (Agent) credential.TypeId, true);

            if (esCredential == null) throw new ApplicationException("The credential was not found to update.  Inserts are not currently supported.");
            
            esCredential.UserName = credential.UserName;
            esCredential.Password = credential.Password;
            esCredential.CredentialVersion = credential.CredentialVersion;
            
            var rowsUpdated = esCredential.Save();
            

            //var fileStore = Registry.Resolve<IFileStoreFactory>().CreateFileStore();
            var messageSender = Registry.Resolve<IAdMessageSender>();

            // FB: 30552 - autoload credential lockout
            //FetchAgentCredentials.UnLock(fileStore, businessUnitId, (Agent)credential.TypeId);

            var settings = GetMiscSettings.GetSettings(businessUnitId);
            if (rowsUpdated > 0 && settings.HasSettings && settings.BatchAutoload)
            {
                messageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(businessUnitId, AutoLoadInventoryType.Errors), MethodBase.GetCurrentMethod().DeclaringType.FullName);
                messageSender.SendBatchAutoLoadInventory(new AutoLoadInventoryMessage(businessUnitId, AutoLoadInventoryType.NoStatus), MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }


        }

        internal static void SaveCredential(int businessUnitId, ListingSiteCredential credential)
        {

            using (var connection = new SqlConnection((ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString)))
            {
                connection.Open();

                using (var command = new SqlCommand())
                {

                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Release.ListingSitePreferences#CredentialUpdate";
                    command.Parameters.AddWithValue("BusinessUnitId", businessUnitId);
                    command.Parameters.AddWithValue("DestinationId", (EdtDestinations)credential.TypeId);
                    command.Parameters.AddWithValue("Username", credential.UserName);
                    command.Parameters.AddWithValue("Password", credential.Password);

                    command.ExecuteNonQuery();
                    
                }
            }
        }

        private static IEnumerable<ICredential> GetOEMSiteCredentials(int businessUnitId)
        {
            //var fileStore = Registry.Resolve<IFileStoreFactory>().CreateFileStore();
            Status outReason = Status.Pending;
            //return EncryptedSiteCredential.Fetch(businessUnitId, true).Select(credential => new OEMSiteCredential(credential.SiteId, credential.UserName, credential.Password, !FetchAgentCredentials.Islocked(fileStore, businessUnitId, credential.SiteId, out outReason) && !credential.IsLockedOut, outReason.Equals(Status.InsufficientPermissions))).Cast<ICredential>().ToList();
            return EncryptedSiteCredential.Fetch(businessUnitId, true).Select(credential => new OEMSiteCredential(credential.SiteId, credential.UserName, credential.Password, !credential.IsLockedOut, credential.CredentialVersion, outReason.Equals(Status.InsufficientPermissions))).Cast<ICredential>().ToList();
        }
        #endregion
    }

}
