using System;

namespace MAX.ExternalCredentials
{
    public abstract class AbstractSiteCredential : ICredential
    {
        protected AbstractSiteCredential(string userName, string password, bool isValid, bool isInsufficientPerms)
        {
            UserName = userName;
            Password = password;
            IsValid = isValid;
            IsInsufficientPermissions = isInsufficientPerms;
        }

        public abstract CredentialGroup Group { get; }
        public abstract int TypeId { get; }

        public abstract string Description { get;  }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ExternalID { get; set; }
        public bool IsValid { get; protected set; }
        public bool IsInsufficientPermissions { get; protected set; }
        public Byte[] CredentialVersion { get; set; }
    }
}