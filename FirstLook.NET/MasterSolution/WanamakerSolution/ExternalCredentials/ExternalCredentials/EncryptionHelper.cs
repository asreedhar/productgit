using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MAX.ExternalCredentials
{
    public class EncryptionHelper
    {
        //encrypt data using an encoding key
        public static string Encrypt(string strText, string strEncrKey)
        {
            byte[] byKey = Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();

            return Convert.ToBase64String(ms.ToArray());
        }

        //The function used to decrypt the text
        public static string Decrypt(string strText, string sDecrKey)
        {
            byte[] byKey = Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            byte[] inputByteArray = Convert.FromBase64String(strText);

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();

            return Encoding.UTF8.GetString(ms.ToArray());

        }
    }
}
