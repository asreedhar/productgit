namespace MAX.ExternalCredentials
{
    public enum CredentialGroup
    {
        Undefined = -1,
        OEM = 1,
        ListingSite = 2
    }
}