﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace MAX.ExternalCredentials
{
    public interface ICredentialedSiteRepository
    {
        CredentialSite Fetch(int siteId);
        List<CredentialSiteManufacturerName> FetchAutoLoadSettingSites();
        List<CredentialSite> FetchBuildRequestSites(InventoryData inventoryData);
    }

    public class CredentialedSiteRepository : ICredentialedSiteRepository
    {
        public CredentialSite Fetch(int siteId)
        {
            // get the inv and type to relate back to MAX
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                    SELECT siteId, siteName, DealerCodeRequired, batchProcessOnly, maxRequestsPerCall, maxCallsPerBatch, dealerCredentialsRequired, buildRequestTypeId
	                    FROM settings.credentialedSites
	                    WHERE siteId = @SiteId
                    ";

                var sites = sqlConnection.Query<CredentialSite>(query, new { SiteId = siteId });
                sqlConnection.Close();

                return sites.FirstOrDefault();
            }
        }

        public List<CredentialSiteManufacturerName> FetchAutoLoadSettingSites()
        {
            
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                            SELECT VehicleAutoLoadSettings.siteId, ManufacturerName = credentialedSites.siteName
                                FROM merchandising.merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings
                                INNER JOIN merchandising.settings.credentialedSites credentialedSites ON VehicleAutoLoadSettings.siteId = credentialedSites.siteId
                                    																	AND credentialedSites.canRequestData = 1
                                    																	AND credentialedSites.DealerCredentialsRequired = 1
                                INNER JOIN VehicleCatalog.Chrome.Manufacturers Manufacturers ON VehicleAutoLoadSettings.ManufacturerID = Manufacturers.ManufacturerID
												                                                AND Manufacturers.CountryCode = 1
                            UNION (SELECT 6, 'Ford')
                                ORDER BY ManufacturerName
                            ";

                var sites = sqlConnection.Query<CredentialSiteManufacturerName>(query);
                sqlConnection.Close();

                return sites.ToList();
            }
        }

        public List<CredentialSite> FetchBuildRequestSites(InventoryData inventoryData)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                                    SELECT credentialedSites.siteId, siteName, DealerCodeRequired, batchProcessOnly, maxRequestsPerCall, maxCallsPerBatch, dealerCredentialsRequired, buildRequestTypeId
                                        FROM settings.credentialedSites
                                        INNER JOIN merchandising.merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings ON credentialedSites.siteId = VehicleAutoLoadSettings.siteId
                                        INNER JOIN VehicleCatalog.Chrome.Divisions ON VehicleAutoLoadSettings.ManufacturerID = Divisions.ManufacturerID 
												                                    AND Divisions.CountryCode = 1
	                                    WHERE DivisionName = @Make
	                                    AND credentialedSites.canRequestData = 1
	                                    ORDER BY VehicleAutoLoadSettings.priority
                            ";

                var sites = sqlConnection.Query<CredentialSite>(query, new { inventoryData.Make });
                sqlConnection.Close();

                return sites.ToList();
            }

        }

    }


    public class CredentialSiteManufacturerName
    {
        public int SiteId { get; set; }
        public string ManufacturerName { get; set; }
    }

    public class CredentialSite
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public byte DealerCodeRequired { get; set; }
        public byte BatchProcessOnly { get; set; }
        public int MaxRequestsPerCall { get; set; }
        public int? MaxCallsPerBatch { get; set; }
        public bool DealerCredentialsRequired { get; set; }
        public byte? BuildRequestTypeId { get; set; }
    }
}
