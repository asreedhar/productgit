using FirstLook.Merchandising.DomainModel.Postings;

namespace MAX.ExternalCredentials
{
    public class ListingSiteCredential : AbstractSiteCredential
    {
        private readonly EdtDestinations _destination;

        public ListingSiteCredential(EdtDestinations destination, string userName, string password, bool isValid) 
            : base(userName, password, isValid, false)
        {
            _destination = destination;
        }

        public override CredentialGroup Group
        {
            get { return CredentialGroup.ListingSite; }
        }

        public override int TypeId
        {
            get { return (int) _destination; }
        }

        public override string Description
        {
            get
            {
                switch (_destination)
                {
                        case EdtDestinations.AutoTrader:
                        return "AutoTrader.com";

                        case EdtDestinations.CarsDotCom:
                        return "Cars.com";

                    default:
                        return _destination.ToString();
                }
            }
        }
    }
}