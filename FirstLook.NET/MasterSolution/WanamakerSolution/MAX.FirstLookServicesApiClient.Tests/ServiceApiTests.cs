﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using NUnit.Framework;

namespace MAX.FirstLookServicesApiClient.Tests
{
    [TestFixture]
    public class ServiceApiTests
    {
        private int _goodBusinessUnitId;

        private const int BadBusinessUnitId = 1;
        private const string GoodUserName = "QAShiner";
        private const string BadUserName = "DoublePlusUnGood";
        private const string GoodPassword = "N@d@123";
        private const string BadPassword = "ThereIsNoFInWay";

        [SetUp]
        public void SetUp_Tests()
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                conn.Open();
                const string query = @"SELECT TOP 1 dp.BusinessUnitID
                    FROM imt.dbo.DealerPreference dp
                    JOIN imt.dbo.BusinessUnit bu ON bu.BusinessUnitID = dp.BusinessUnitID
                    JOIN imt.dbo.ThirdParties tp1 ON tp1.ThirdPartyID = dp.GuideBookId
                    JOIN imt.dbo.ThirdParties tp2 ON tp2.ThirdPartyID = dp.GuideBook2Id
                    JOIN imt.dbo.MemberAccess ON MemberAccess.BusinessUnitId = bu.BusinessUnitId

                    WHERE (tp1.Name = 'NADA' OR tp2.Name = 'NADA')
                    AND BusinessUnit NOT LIKE '%test%'
                    AND bu.Active = 1
                    AND imt.dbo.MemberAccess.MemberID = (
						SELECT MemberID FROM imt.dbo.Member WHERE [Login] = @LoginId
                    )
                    ORDER BY bu.BusinessUnitId DESC";

                var ret = conn.Query(query, new {LoginId = GoodUserName});
                _goodBusinessUnitId = ret.First().BusinessUnitID;
                Assert.IsNotNull(_goodBusinessUnitId);
                Assert.IsTrue(_goodBusinessUnitId > 0);
            }
        }

        /// <summary>
        /// Test the return values still work.
        /// </summary>
        [Test]
        public void GetDealerBooks_ValidDealer_ReturnsNonEmptyList()
        {
            var client = new FirstLookBooksClient();
            var result = client.GetDealerBooks(_goodBusinessUnitId, GoodUserName, GoodPassword);
            Assert.IsNotInstanceOf<EmptyBooksList>(result);
            Assert.IsInstanceOf<BooksList>(result);
            Assert.IsNotEmpty(result.Books);
        }

        [Test]
        public void GetDealerBooks_InvalidDealer_ReturnsEmptyList()
        {
            var client = new FirstLookBooksClient();
            var result = client.GetDealerBooks(BadBusinessUnitId, GoodUserName, GoodPassword);
            Assert.IsInstanceOf<EmptyBooksList>(result);
        }

        [Test]
        public void GetDealerBooks_BadCredentials_ReturnsEmptyList()
        {
            var client = new FirstLookBooksClient();
            var result = client.GetDealerBooks(_goodBusinessUnitId, BadUserName, BadPassword);
            Assert.IsInstanceOf<EmptyBooksList>(result);
        }


        [Test]
        public void GetVehicleInfo1()
        {
            var client = new FirstLookVehicleInfoClient();
            var result = client.GetVehicleInfo(101590, 39542250);
            Assert.IsNotNull(result);
        }
    }
}
