﻿This is 0.1 version of FetchAutoLoadTest tool, it basically requests and gethers the Color and Option Codes of Fetch responses from BMW
GM, VW, Mazda, Chrysler, store the codes in a file called 'FetchTestActualData', then comparing with FetchTestBaseLineData file, 
The test result will be in a file called, 'FetchTestResult', if there is a differece, the test will Fail, otherwise, it will Pass!

There are few known issues, like the user credentail in the URL may become invalid after a period of time, it doesn't tell the cause of
failures, it needs to be a scheduled job and email the failure alert once it fails. 


How to run: 

1. Copy the FetchTestBaseLineData.txt file (in the solution) to c:\. 
2. Run the tool. 
3. After the DOS window goes away, Check the test result in c:\FetchTestResult.txt, may need to refresh the window, if you don't see the FetchTestResult file. 
