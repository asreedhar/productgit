﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace FetchAutoloadTest
{

    class GetFetchResponse
    {
        static void Main(string[] args)
        {
            string[] URLString = {
                                     "https://incisent.fetch.com/agent/execute?_item=incisent/feedgrouppoc/feeds/dealerspeed/f_dealerspeed&inputUsername=james.bingham&inputPassword=325bmw330&inputVin=WMWZC3C59BWH98344"
                                     ,
                                     "https://incisent.fetch.com/agent/execute?_item=incisent/feedgrouppoc/feeds/gmglobalconnect/f_gmglobalconnect&inputUsername=dmcadoo&inputPassword=alicia19&inputVin=1G4HC5EM8BU102107"
                                     ,
                                     "https://incisent.fetch.com/agent/execute?_item=incisent/feedgrouppoc/feeds/volkswagenhub/f_volkswagenhub&inputUsername=rleicht1&inputPassword=faulknervw2&inputVin=WVGFK9BP2CD000108"
                                     ,
                                     "https://incisent.fetch.com/agent/execute?_item=incisent/feedgrouppoc/feeds/mazdadcs/f_mazdadcs&inputUsername=hfaulkn3&inputPassword=faulkner04&inputVin=1YVHZ8EH7C5M04212"
                                     ,
                                     "https://incisent.fetch.com/agent/execute?_item=incisent/feedgrouppoc/feeds/chryslerdealerconnect/f_chryslerdealerconnect&inputOdometer=0&inputUsername=S19630k&inputPassword=Cart0014&inputVin=2C3KA63H67H651623"
                                 };

            string codeFile = "C:\\FetchTestActualData.txt";
            System.IO.StreamWriter objWriter;
            objWriter = new System.IO.StreamWriter(codeFile);
            int count = 1;
            foreach (string s in URLString)
            {
                XDocument doc = XDocument.Load(s);
                if (count == 1)
                {
                    objWriter.WriteLine("********* DealerSpeed *********" + " " + s);
                }
                if (count == 2)
                {
                    objWriter.WriteLine("********* GM Global *********" + " " + s);
                }
                if (count == 3)
                {
                    objWriter.WriteLine("********* VW HUB  **********" + " " + s);
                }
                if (count == 4)
                {
                    objWriter.WriteLine("********* Mazda  **********" + " " + s);
                }
                if (count == 5)
                {
                    objWriter.WriteLine("********* Chrysler  ***********" + " " + s);
                }


                var enteriorColorNodeQuery = from exteriorColorNode in doc.Descendants("ExteriorColor")
                                             let exteriorColor =
                                                 new
                                                     {
                                                         Code = exteriorColorNode.Element("Code").Value,
                                                         //Description = exteriorColorNode.Element("Description").Value
                                                     }
                                             select exteriorColor;

                foreach (var exteriorColor in enteriorColorNodeQuery)
                {
                    objWriter.WriteLine(exteriorColor.Code + "\r\n");
                    Console.WriteLine("code: {0} ", exteriorColor.Code);
                }

                var interiorColorNodeQuery = from interiorColorNode in doc.Descendants("InteriorColor")
                                             let interiorColor =
                                                 new
                                                     {
                                                         Code = interiorColorNode.Element("Code").Value,
                                                         //Description = interiorColorNode.Element("Description").Value
                                                     }
                                             select interiorColor;

                foreach (var interiorColor in interiorColorNodeQuery)
                {
                    objWriter.WriteLine(interiorColor.Code + "\r\n");
                    Console.WriteLine("code: {0} ", interiorColor.Code);
                }


                var optionNodeQuery = from optionNode in doc.Descendants("Option")
                                      let option =
                                          new //anonymous object
                                              {
                                                  Code = optionNode.Element("Code").Value,
                                                  //Description = optionNode.Element("Description").Value
                                              }
                                      //orderby option.Code descending
                                      select option;

                foreach (var option in optionNodeQuery)
                {
                    objWriter.WriteLine(option.Code + "\r\n");
                    Console.WriteLine("code: {0} ", option.Code);
                }
                count++;
            }
            objWriter.Close();



            string resultFile = "C:\\FetchTestResult.txt";
            System.IO.StreamWriter objWriter2;
            objWriter2 = new System.IO.StreamWriter(resultFile);

            if (File.ReadLines("C:\\FetchTestActualData.txt").SequenceEqual(File.ReadLines("C:\\FetchTestBaseLineData.txt")))
            {                                                                               
                objWriter2.WriteLine(" $$$$$$$$$  Passed, Fetch return correct Code $$$$$$$");
            }
            else
            {
                objWriter2.WriteLine("!!!!!!    Failed, Please check C:\\FetchTestActualData.txt file for detail      !!!!!!");
            }

            objWriter2.Close();
        }
    }

}
