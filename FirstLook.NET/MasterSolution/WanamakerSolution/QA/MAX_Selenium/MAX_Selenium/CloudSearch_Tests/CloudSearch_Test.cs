﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using NUnit.Framework;

namespace MAX_Selenium
{
    class CloudSearch_Test : Test
    {

        [Test]
        [Description("Verifies HasMaxForWebsite1 Upgrade is the same in both CloudSearch and the database")]
        public void Verify_Dealer_HasMaxForWebsite1_Enabled_C20799()
        {
            string BusinessUnitCode = "WINDYCIT05"; // Windy City BMW

            CloudSearch_Page cloudSearch = new CloudSearch_Page(driver);
            var csQuery = new Uri(cloudSearch.Dealer_Search_Base_Query + BusinessUnitCode);
            var jsontext = cloudSearch.GetCloudSearchData(csQuery);
            var dealer = JsonConvert.DeserializeObject<dynamic>(jsontext);
            int HasMaxForWebsite1_cloudSearch = dealer.hits.hit[0].fields.mobileshowroom_enabled;
            
            var db = DataFinder.ExecQuery(DataFinder.Merchandising,
                  @"
                    SELECT HasMaxForWebsite1
                    FROM Merchandising.settings.DealerProfilePublishedSnapshot
                    WHERE BusinessUnitCode = '" + BusinessUnitCode + "' ")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { HasMaxForWebsite1 = (Boolean)dr["HasMaxForWebsite1"] })
                .First();

            int HasMaxForWebsite1 = Convert.ToInt16(db.HasMaxForWebsite1);
            Assert.IsTrue(HasMaxForWebsite1 == HasMaxForWebsite1_cloudSearch, "The HasMaxForWebsite1 upgrade value in cloudSearch is {0} and in the database it is {1}", HasMaxForWebsite1_cloudSearch, HasMaxForWebsite1);
        }

        //   FB 31145 - number of dealers in CS differs from the db table CS gets its data from, causing these tests to fail. ----------------------
         
        
        [Test] // This test will fail until FB 31145 is fixed.
        [Description("Confirms CloudSearch is updating reasonably by comparing CS counts with database - HasMaxForWebsite2")]
        public void Total_Num_Of_Dealers_With_HasMaxForWebsite2_C19268()
        {
            CloudSearch_Page cloudSearch = new CloudSearch_Page(driver);
            var csQueryStub = new UriBuilder(cloudSearch.Dealer_Search_Structured_Query_stub);
            Uri csQuery;
            csQueryStub.Query = "q.parser=structured" + "&" + "q=maxforwebsite2_enabled:'1'"; //how MSDN handles "&"; cannot append string directly
            csQuery = csQueryStub.Uri;
            var jsontext = cloudSearch.GetCloudSearchData(csQuery);
            var dealerSearch = JsonConvert.DeserializeObject<dynamic>(jsontext);
            int csHits = dealerSearch.hits.found;

            // This table will never have inactive dealers.
            var db = DataFinder.ExecQuery(DataFinder.Merchandising,
                  @"
                    SELECT COUNT(HasMaxForWebsite2) as HasMaxForWebsite2
                    FROM Merchandising.settings.DealerProfilePublishedSnapshot
                    WHERE HasMaxForWebsite2 = 1")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { HasMaxForWebsite2 = (int)dr["HasMaxForWebsite2"] })
                .First();

            // CloudSearch is only updated 4 times a day and could temporarily be out-of-synch with the db when test runs, so allow results to vary +-4.   
            Assert.IsTrue((db.HasMaxForWebsite2 - 4) <= csHits && csHits <= (db.HasMaxForWebsite2 + 4),
                "The total number of dealers with HasMaxForWebsite2 enabled in cloudSearch is {0} and in the database it is {1}", csHits, db.HasMaxForWebsite2);
        }


        [Test] // This test will fail until FB 31145 is fixed.
        [Description("Confirms CloudSearch is updating reasonably by comparing CS counts with database - HasMobileShowroom")]
        public void Total_Num_Of_Dealers_With_HasMobileShowroom_C19267()
        {
            CloudSearch_Page cloudSearch = new CloudSearch_Page(driver);
            var csQueryStub = new UriBuilder(cloudSearch.Dealer_Search_Structured_Query_stub);
            Uri csQuery;
            csQueryStub.Query = "q.parser=structured" + "&" + "q=mobileshowroom_enabled:'1'"; //how MSDN handles "&"; cannot append string directly
            csQuery = csQueryStub.Uri;
            var jsontext = cloudSearch.GetCloudSearchData(csQuery);
            var dealerSearch = JsonConvert.DeserializeObject<dynamic>(jsontext);
            int csHits = dealerSearch.hits.found;

            var db = DataFinder.ExecQuery(DataFinder.Merchandising,
                  @"
                    SELECT COUNT(HasMobileShowroom) as HasMobileShowroom
                    FROM Merchandising.settings.DealerProfilePublishedSnapshot
                    WHERE HasMobileShowroom = 1")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { HasMobileShowroom = (int)dr["HasMobileShowroom"] })
                .First();

            // CloudSearch is only updated 4 times a day and could temporarily be out-of-synch with the db when test runs, so allow results to vary +-4. 
            Assert.IsTrue((db.HasMobileShowroom - 4) <= csHits && csHits <= (db.HasMobileShowroom + 4),
                "The total number of dealers with HasMobileShowroom enabled in cloudSearch is {0} and in the database it is {1}", csHits, db.HasMobileShowroom);
        }


        [Test] // This test will fail until FB 31145 is fixed.
        [Description("Confirms CloudSearch is updating reasonably by comparing CS counts with database - HasMaxForWebsite1(Website PDF) upgrade ")]
        public void Total_Num_Of_Dealers_With_HasMaxForWebsite1_C19269()
        {
            CloudSearch_Page cloudSearch = new CloudSearch_Page(driver);
            var csQueryStub = new UriBuilder(cloudSearch.Dealer_Search_Structured_Query_stub);
            Uri csQuery;
            csQueryStub.Query = "q.parser=structured" + "&" + "q=maxforwebsite1_enabled:'1'"; //how MSDN handles "&"; cannot append string directly
            csQuery = csQueryStub.Uri;
            var jsontext = cloudSearch.GetCloudSearchData(csQuery);
            var dealerSearch = JsonConvert.DeserializeObject<dynamic>(jsontext);
            int csHits = dealerSearch.hits.found;

            var db = DataFinder.ExecQuery(DataFinder.Merchandising,
                  @"
                    SELECT COUNT(HasMaxForWebsite1) as HasMaxForWebsite1
                    FROM Merchandising.settings.DealerProfilePublishedSnapshot
                    WHERE HasMaxForWebsite1 = 1")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { HasMaxForWebsite1 = (int)dr["HasMaxForWebsite1"] })
                .First();

            // CloudSearch is only updated 4 times a day and could temporarily be out-of-synch with the db when test runs, so allow results to vary +-4. 
            Assert.IsTrue((db.HasMaxForWebsite1 - 4) <= csHits && csHits <= (db.HasMaxForWebsite1 + 4),
                "The total number of dealers with HasMaxForWebsite1 enabled in cloudSearch is {0} and in the database it is {1}", csHits, db.HasMaxForWebsite1);
        }

        [Test]
        [Description("Verifies most dealer info attributes in the beta-search document")]
        public void verify_Index_fields_In_BetaSearch_Domain_CloudSearch_C19266()
        {
            //query finds the newest customer added in MAX to test with
            string query =
                @"
                SELECT TOP 1 DP.*
                FROM Merchandising.settings.DealerProfilePublishedSnapshot DP
                JOIN Merchandising.settings.Merchandising M on DP.BusinessUnitID = M.BusinessUnitID
                WHERE HasMaxForWebsite1 = 1
                AND HasMaxForWebsite2 = 1
                AND HasMobileShowroom = 1
                ORDER BY year(insertdate) desc, month(insertdate) desc, day(insertdate) desc";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            string BusinessUnitCode = Convert.ToString(dr.ItemArray[2]);
            string dealerName = Convert.ToString(dr.ItemArray[1]);

            CloudSearch_Page cloudSearch = new CloudSearch_Page(driver);
            var csQuery = new Uri(cloudSearch.Dealer_Search_Base_Query + BusinessUnitCode);
            var jsontext = cloudSearch.GetCloudSearchData(csQuery);
            var dealer = JsonConvert.DeserializeObject<dynamic>(jsontext);
            
            int BusinessUnitId_cs = dealer.hits.hit[0].fields.businessunitid;
            int BusinessUnitId_db = Convert.ToInt32(dr.ItemArray[0]);
            Assert.IsTrue(BusinessUnitId_cs == BusinessUnitId_db, "The BusinessUnitId in cloudSearch is {0} and in the database it is {1} for dealer {2}", BusinessUnitId_cs, BusinessUnitId_db, dealerName);

            string ownerhandle_cs = dealer.hits.hit[0].fields.ownerhandle;
            string ownerhandle_db = dr.ItemArray[3].ToString().ToUpper();
            Assert.IsTrue(ownerhandle_cs == ownerhandle_db, "The ownerhandle in cloudSearch is {0} and in the database it is {1} for dealer {2}", ownerhandle_cs, ownerhandle_db, dealerName);

            int groupid_cs = dealer.hits.hit[0].fields.groupid;
            int groupid_db = Convert.ToInt32(dr.ItemArray[4]);
            Assert.IsTrue(groupid_cs == groupid_db, "The groupid in cloudSearch is {0} and in the database it is {1} for dealer {2}", groupid_cs, groupid_db, dealerName);

            //remaining attributes may be empty in database and will not be displayed at all in CloudSearch
            //The If statement checks for this, if it is empty, it skips that attribute

            string address_db = dr.ItemArray[5].ToString();
            if (address_db.Length > 0)
            {
                string address_cs = dealer.hits.hit[0].fields.address;
                Assert.IsTrue(address_cs == address_db, "The address in cloudSearch is {0} and in the database it is {1} for dealer {2}", address_cs, address_db, dealerName);
            }

            string city_db = dr.ItemArray[6].ToString();
            if (city_db.Length > 0)
            {
                string city_cs = dealer.hits.hit[0].fields.city;
                Assert.IsTrue(city_cs == city_db, "The city in cloudSearch is {0} and in the database it is {1} for dealer {2}", city_cs, city_db, dealerName);
            }

            string description_db = dr.ItemArray[7].ToString();
            if (description_db.Length > 0)
            {
                string description_cs = dealer.hits.hit[0].fields.description;
                Assert.IsTrue(description_cs == description_db, "The description in cloudSearch is {0} and in the database it is {1} for dealer {2}", description_cs, description_db, dealerName);
            }
            
            string email_db = dr.ItemArray[8].ToString();
            if(email_db.Length > 0)
            {
                string email_cs = dealer.hits.hit[0].fields.email;
                Assert.IsTrue(email_cs == email_db, "The email in cloudSearch is {0} and in the database it is {1} for dealer {2}", email_cs, email_db, dealerName);
            }
               
            string phone_db = dr.ItemArray[9].ToString();
            if(phone_db.Length > 0)
            {
                string phone_cs = dealer.hits.hit[0].fields.phone;
                Assert.IsTrue(phone_cs == phone_db, "The phone number in cloudSearch is {0} and in the database it is {1} for dealer {2}", phone_cs, phone_db, dealerName);
            }
            
            string state_db = dr.ItemArray[11].ToString();
            if(state_db.Length > 0)
            {
                string state_cs = dealer.hits.hit[0].fields.state;
                Assert.IsTrue(state_cs == state_db, "The state in cloudSearch is {0} and in the database it is {1} for dealer {2}", state_cs, state_db, dealerName);
            }

            string zip_db = dr.ItemArray[12].ToString();
            if(zip_db.Length > 0)
            {
                string zip_cs = dealer.hits.hit[0].fields.zip;
                Assert.IsTrue(zip_cs == zip_db, "The zip code in cloudSearch is {0} and in the database it is {1} for dealer {2}", zip_cs, zip_db, dealerName);
            }
            
        }

        [Test]
        [Description("Verifies the logoUrl is correct and works properly")]
        public void Verify_LogoUrl_C19270()
        {
            //query finds the LogoUrl of the latest customer added in MAX to test with
            string query =
                @"
                SELECT Top 1 LogoUrl
                FROM Merchandising.settings.DealerProfile DP
                JOIN Merchandising.settings.Merchandising M on DP.BusinessUnitID = M.BusinessUnitID
                WHERE LogoUrl <> ''
                ORDER BY year(insertdate) desc, month(insertdate) desc, day(insertdate) desc";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            string base_url = "https://www.firstlook.biz/digitalimages/";
            string LogoUrl = base_url + dr.ItemArray[0];
            driver.Navigate().GoToUrl(LogoUrl);

            //checking if the page loads by finding the img tag in the correct place in the HTML hierarchy, not so much if the url is the same. 
            IWebElement LogoUrlFromImgTag = driver.FindElement(By.XPath("/html/body/img"));
            String LogoUrlFromImgTag_Text = LogoUrlFromImgTag.GetAttribute("src");
            
            Assert.IsTrue(LogoUrl == LogoUrlFromImgTag_Text);

        }

        [Test]
        [Description("Compares CloudSearch data to the data in vehicle details section in MfW")]
        public void VehicleDetails_CloudSearch_And_MaxForWebsite_C20798()
        {
            const string BusinessUnitCode = "WINDYCIT01"; //Windy City Chevrolet

            CloudSearch_Page cloudSearch = new CloudSearch_Page(driver);
            var csQuery = new Uri(cloudSearch.Dealer_Search_Base_Query + BusinessUnitCode);
            var jsontext = cloudSearch.GetCloudSearchData(csQuery);

            //Find an ownerhandle in dealer-search given BusinessUnitCode
            var dealerSearch = JsonConvert.DeserializeObject<dynamic>(jsontext);
            string ownerhandle = dealerSearch.hits.hit[0].fields.ownerhandle;
            ownerhandle = ownerhandle.ToUpper();

            //use this ownderhandle to find a list of vehicles in vehicle-search and grab a vin
            //this search needs to exclude non-zero data where tested: mileage != 0 or -1, and others if necessary
            var csQuery2 =  new Uri(cloudSearch.Vehicle_Search_Base_Lucene_Query + "ownerhandle:%22" + ownerhandle + "%22+and+-(mileage:%220%22+OR+mileage:%22-1%22)&size=10");
            var jsontext2 = cloudSearch.GetCloudSearchData(csQuery2);
            var vehicleSearch = JsonConvert.DeserializeObject<dynamic>(jsontext2);
            string vin = vehicleSearch.hits.hit[0].fields.vin;

            //use this vin to get only vehicle's data in vehicle-search
            var csQuery3 =  new Uri(cloudSearch.Vehicle_Search_Base_Structured_Query + "vin:%27" + vin + "%27");
            var jsontext3 = cloudSearch.GetCloudSearchData(csQuery3);
            var vehicle = JsonConvert.DeserializeObject<dynamic>(jsontext3);
            NavigateToMaxForWebsite(vin); //instantiates
            Thread.Sleep(2000);

            //color
            string Color_cs = vehicle.hits.hit[0].fields.color;
            if (Color_cs != null)
                Color_cs = Color_cs.ToLower();
            
            string Color_mfw = maxForWebsite_Page.Color.Text.ToLower();
            if(Color_mfw != "Uknown" && Color_cs != null)
                Assert.IsTrue((Color_cs == Color_mfw), "Color from CloudSearch is {0} and from MFW vehicle details is {1} for {2} at dealer {3}", Color_cs, Color_mfw, vin, BusinessUnitCode);

            //year make model
            string YearModel_cs = vehicle.hits.hit[0].fields.yearmodel;
            YearModel_cs = YearModel_cs.ToLower();
            
            string YearModel_mfw = maxForWebsite_Page.YearModel.Text.ToLower();
            Assert.IsTrue((YearModel_cs == YearModel_mfw),"Year/model from CloudSearch is {0} and from MFW vehicle details is {1} for {2} at dealer {3}", YearModel_cs, YearModel_mfw, vin, BusinessUnitCode);

            //mileage
            string Mileage = vehicle.hits.hit[0].fields.mileage;
            int Mileage_cs = Convert.ToInt32(Mileage);
            
            int Mileage_mfw = Utility.ExtractDigitsFromString(maxForWebsite_Page.mileage.Text);
            Assert.IsTrue((Mileage_cs == Mileage_mfw), "Mileage from CloudSearch is {0} and from MFW vehicle details is {1} for {2} at dealer {3}", Mileage_cs, Mileage_mfw, vin, BusinessUnitCode);

        }

        [Test] // This test may fail until FB 34034 is fixed. 8/5/2015 TM
        [Description("Confirms 10 vehicles with the most recent delete date in the db are not in CS(except within 2 days of delete date) - FB 31962")]
        public void confirmVehicleIsRemovedFromCS_C21932()
        {
            CloudSearch_Page cloudSearch = new CloudSearch_Page(driver);
            
            //This query can take up to 35 sec on a clean buffer & cache
            var query =
                @"
                SELECT TOP 10 Vin, IDI.BusinessUnitID
                FROM IMT.dbo.Vehicle IDV
                JOIN IMT.dbo.Inventory IDI ON IDV.VehicleID = IDI.VehicleID
                WHERE 1=1
                AND IDI.InventoryActive = 0
                AND IDI.DeleteDt BETWEEN DATEADD(dd, -20, GETDATE()) AND DATEADD(dd, -3, GETDATE())
                AND EXISTS (select * from IMT.dbo.BusinessUnit idb where Active = 1 and idb.BusinessUnitID = IDI.BusinessUnitID)
                AND EXISTS (select * from Merchandising.settings.Merchandising msm where MAXForWebsite20 = 1 and MaxDigitalShowroom = 1
	            and msm.businessUnitID = IDI.BusinessUnitID)";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            var csQueryStub = new UriBuilder(cloudSearch.Vehicle_Search_Structured_Query_stub);
            Uri csQuery;

            for (int i = 0; i < 10; i++)
            { 
                var vin = dataset.Rows[i].ItemArray[0];
                var businessUnitId = dataset.Rows[i].ItemArray[1];
                csQueryStub.Query = "q.parser=structured" + "&" + "q=vin:%27" + vin + "%27"; //how MSDN handles "&"; cannot append string directly
                csQuery = csQueryStub.Uri;
                var jsontext = cloudSearch.GetCloudSearchData(csQuery);
                var vehicleSearch = JsonConvert.DeserializeObject<dynamic>(jsontext);
                string found = vehicleSearch.hits.found;

                Assert.That((found == "0"), "Failed for VIN {0}, BUID: {1}", vin, businessUnitId);  
                Thread.Sleep(250);
            }
        }
       
    }
}
