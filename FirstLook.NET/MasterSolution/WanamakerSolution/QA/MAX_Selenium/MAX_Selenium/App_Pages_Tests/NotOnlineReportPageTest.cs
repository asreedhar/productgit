﻿using System;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    class NotOnlineReportPageTest:Test
    {
        [SetUp]
        public void SetUp()
        {
            //InitializePageAndLogin_InventoryPage();
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Kenny Ross Chevrolet Cadillac Somerset");  //Faulkner BMW
            notOnlineReportPage = inventoryPage.GoTo_NotOnlineReportPage();
        }

        [Test]
        public void NOR_ChangeDealerTest_C26876()
        {
            notOnlineReportPage.SelectDealer("All Star Chevrolet");
            Assert.That(notOnlineReportPage.SelectDealerDDL.Text.Contains("All Star Chevrolet"));
        }

        [Test]
        public void ChangeStatusFilterTest_C26890()
        {
            notOnlineReportPage.SelectStatusFilter(StatusFilterType.Not_Online);
            Assert.That(notOnlineReportPage.StatusDDL.Text.Contains("Not Online"));
        }

        [Test]
        public void ChangeNewOrUsedFilterTest_C26889()
        {
            notOnlineReportPage.SelectNewOrUsedFilter(NewOrUsedFilterType.New_Used);
            Assert.That(notOnlineReportPage.NewOrUsedDDL.Text.Contains("New & Used"));
        }

        [Test]
        [Description("Selects a min age in settings and verfies the vehicles in the NOR is equal or greater to this value")]  
        public void MinAgeFilter_C8407()
        {
            IWebElement valueSelectedInDDL_Report = driver.FindElement((By.XPath("//*[@id=\"ctl00_BodyPlaceHolder_ageInDays\"]/option[@selected='selected']")));
            String valueSelectedInDDL_Report_Text = valueSelectedInDDL_Report.Text;

            driver.Navigate().GoToUrl(BaseURL + "/merchandising/CustomizeMAX/DealerPreferences.aspx");
            driver.FindElement(By.Id("ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_9")).Click();
            Thread.Sleep(2000);
            IWebElement valueSelectedInDDL_ReportTab =
                driver.FindElement(By.XPath("//*[@id=\"ageInDays\"]/option[@selected='selected']"));
            String valueSelectedInDDL_ReportTab_Text = valueSelectedInDDL_ReportTab.Text;
            Thread.Sleep(5000);

            Assert.That(valueSelectedInDDL_Report_Text, Is.EqualTo(valueSelectedInDDL_ReportTab_Text),
                        "Not Online Report is  {0} but Report tab settings is: {1}", valueSelectedInDDL_Report_Text, valueSelectedInDDL_ReportTab_Text);
            int minDays = 0;

            if(valueSelectedInDDL_ReportTab_Text != "14")  //to select a different min days from what is already saved in the app
            {
                new SelectElement(driver.FindElement(By.Id("ageInDays"))).SelectByIndex(14);
                minDays = 14;
            }
            else
            {
                new SelectElement(driver.FindElement(By.Id("ageInDays"))).SelectByIndex(13);
                minDays = 13;
            }

            driver.FindElement(By.Id("SaveButton")).Click();
            Thread.Sleep(4000);
            notOnlineReportPage = inventoryPage.GoTo_NotOnlineReportPage();
            Thread.Sleep(5000);

            driver.FindElement(By.XPath(".//div[contains(@id,'_1_18iT0')]"));
            driver.FindElement(By.XPath(".//table/tbody/tr/td[2]/a/img")).Click();
            Thread.Sleep(8000);
            driver.FindElement(By.XPath(".//td[contains(@id,'ReportCell')]"));
            int ageFromNotOnlineReport = Utility.ExtractDigitsFromString(driver.FindElement(By.XPath(".//table/tbody/tr/td/table/tbody/tr[3]/td[2]/table/tbody/tr[3]/td[2]/div")).Text);
            Assert.IsTrue(ageFromNotOnlineReport >= minDays);
           
        }

    }
}
