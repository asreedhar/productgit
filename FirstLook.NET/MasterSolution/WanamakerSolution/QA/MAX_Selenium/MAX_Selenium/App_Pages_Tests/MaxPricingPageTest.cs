﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Data;
//using MbUnit.Framework;
using System.Text.RegularExpressions;
using System.Threading;
//using MAX_Selenium.App_Pages;
using Microsoft.SqlServer.Server;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    internal class MaxPricingPageTest : Test
    {

        /*[Test] // This test will not work; no dealer context (no dealer selected).
        public void GotoMaxPricingPage()
        {
            driver.Navigate().GoToUrl("https://betamax.firstlook.biz/merchandising/pricinganalysis/pingone");
            Thread.Sleep(5000);
            driver.FindElement(By.XPath(".//*[@id='username']")).SendKeys("snaik");
            driver.FindElement(By.XPath(".//*[@id='password']")).SendKeys("N@d@123");
            driver.FindElement(By.XPath(".//*[@id='loginformtext']/form/input[5]")).Click();            
            Thread.Sleep(5000);
            Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);

        }
         * */


        [Ignore("Under development")]
        //[TestCase(Firefox, true)]
        [TestCase(InternetExplorer, true)]
        //[TestCase(Chrome, true)]
        public void DefineCompetitiveDefaultValues(string browser, bool overRide)
        {
            var dealer = FindProfitMaxDealer();
            InitializeDriver(browser, overRide);
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer);
            approvalPage = inventoryPage.ClickOnFirstVehicle();
            maxPricingPage = approvalPage.GoTo_ProfitMax_Page();
            // Wait for mask to clear
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until((d) => driver.FindElement(By.Id("divDataLoader")).GetCssValue("display") == "none");

            // TODO Determine how to deal with concept of "defaults", since configurable and dynamically saved in S3.

           // Thread.Sleep(5000);
            //Console.Write(maxPricingPage.ListingDefaultValue.Text);
            Assert.AreEqual("Active",maxPricingPage.ListingDefaultValue.Text);
            Assert.AreEqual("100", maxPricingPage.DistanceDefaultValue.Text);
            Assert.AreEqual("All", maxPricingPage.TrimDefaultValue.Text);
            Assert.AreEqual("All", maxPricingPage.TransmissionDefaultValue.Text);
            Assert.AreEqual("All", maxPricingPage.DriveTrainDefaultValue.Text);
            Assert.AreEqual("All", maxPricingPage.EngineDefaultValue.Text);
            Assert.AreEqual("All", maxPricingPage.FuelDefaultValue.Text);    
        }
        /*

         [NUnit.Framework.Test]
         public void DistanceFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);
             Console.Write(maxPricingPage.DistanceDefaultValue.Text);//current Default value
             maxPricingPage.DistanceLabel.Click();
             Console.Write(maxPricingPage.DistanceValue1.Text);//First Value
             String distanceValue = maxPricingPage.DistanceValue1.Text;
             maxPricingPage.DistanceRadio1.Click();
             maxPricingPage.DistanceUpdate.Click();

             Console.Write(maxPricingPage.DistanceDefaultValue.Text);//Distance value -After selecting Distance 
             Assert.AreEqual(distanceValue, maxPricingPage.DistanceDefaultValue.Text);
             
         }



          [NUnit.Framework.Test]
         public void ListingFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);
             Console.Write(maxPricingPage.ListingDefaultValue.Text);//current Default value
             maxPricingPage.ListingLabel.Click();
            // Console.Write(maxPricingPage.ListingActiveText.Text);//Active
            // Console.Write(maxPricingPage.ListingRecentText.Text);//Recent
             String listingRecent = maxPricingPage.ListingRecentText.Text;
             maxPricingPage.ListingRecent.Click();
             maxPricingPage.ListingUpdate.Click();
             Console.Write(maxPricingPage.ListingDefaultValue.Text);//Listing value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual("Recent", maxPricingPage.ListingDefaultValue.Text);   
         }



         [NUnit.Framework.Test]
         public void CertifiedFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);
          
             Console.WriteLine(maxPricingPage.CertifiedDefaultValue.Text);//current Default value
             Console.WriteLine(maxPricingPage.CertifiedDefaultValue1.Text);//current Default value
             maxPricingPage.CertifiedLabel.Click();
             Console.WriteLine(maxPricingPage.CertifiedText.Text);//Certified
             Console.WriteLine(maxPricingPage.NotCertifiedText.Text);//Not Certified
             String certifiedRecent = maxPricingPage.CertifiedText.Text;
             maxPricingPage.NotCertifiedCheck.Click();
             maxPricingPage.CertifiedCheck.Click();
             maxPricingPage.CertifiedUpdate.Click();
             Console.WriteLine(maxPricingPage.CertifiedDefaultValue.Text);//certified value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual(certifiedRecent, maxPricingPage.CertifiedDefaultValue.Text);
         }


        /*
         [NUnit.Framework.Test]
         public void TrimFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Console.WriteLine(maxPricingPage.TrimDefaultValue.Text);//current Default value
             maxPricingPage.TrimLabel.Click();
             maxPricingPage.TrimValue2.Click();
             maxPricingPage.TrimValue1.Click();
             maxPricingPage.TrimUpdate.Click();
             Console.WriteLine(maxPricingPage.TrimDefaultValue.Text);//Trim value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreNotEqual("All",maxPricingPage.CertifiedDefaultValue.Text);
         }

         [NUnit.Framework.Test]
         public void TransmissionFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Console.WriteLine(maxPricingPage.TransmissionDefaultValue.Text);//current Default value
             maxPricingPage.TransmissionLabel.Click();
             maxPricingPage.TransmissionValue1.Click();
             maxPricingPage.TransmissionValue2.Click();
             maxPricingPage.TransmissionUpdate.Click();
             Console.WriteLine(maxPricingPage.TransmissionDefaultValue.Text);//Transmission value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreNotEqual("All", maxPricingPage.TransmissionDefaultValue.Text);
         }

        
         [NUnit.Framework.Test]
         public void MarketListingUi()
         {
             //Select Listing value
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);
             maxPricingPage.ListingLabel.Click();
             String listingRecent = maxPricingPage.ListingRecentText.Text;
             maxPricingPage.ListingRecent.Click();
             maxPricingPage.ListingUpdate.Click();
             Console.Write(maxPricingPage.ListingDefaultValue.Text);//Listing value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual("Recent", maxPricingPage.ListingDefaultValue.Text);
             
             //Check Market Listing
             Assert.IsTrue(maxPricingPage.ViewAllLink.Displayed);
             //Assert.IsTrue(maxPricingPage.MarketListingLink.Displayed)
             maxPricingPage.ViewData.Click();
             Thread.Sleep(5000);
             Assert.IsTrue(maxPricingPage.ExportLink.Displayed);
             Assert.IsTrue(maxPricingPage.AutotraderLink.Displayed);
             Assert.IsTrue(maxPricingPage.CarsLink.Displayed);
             Assert.AreEqual("Age",maxPricingPage.MarketListAge.Text.Trim());
             Assert.AreEqual("Seller", maxPricingPage.MarketListSeller.Text.Trim());
             Assert.AreEqual("Vehicle Description", maxPricingPage.MarketListVehicleDesc.Text.Trim());
             Assert.AreEqual("Certified", maxPricingPage.MarketListCertified.Text.Trim());
             Assert.AreEqual("Color", maxPricingPage.MarketListColor.Text.Trim());
             Assert.AreEqual("Mileage", maxPricingPage.MarketListMileage.Text.Trim());
             Assert.AreEqual("Internet Price", maxPricingPage.MarketListInternetPrice.Text.Trim());
             Assert.AreEqual("% of Market Average", maxPricingPage.MarketListMktAvg.Text.Trim());
             Assert.AreEqual("Distance", maxPricingPage.MarketListDistance.Text.Trim());
             Assert.AreEqual("Vin", maxPricingPage.MarketListVin.Text.Trim());

         }

        [NUnit.Framework.Test]
         public void VhrTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             Thread.Sleep(5000);
            // Assert.AreEqual("VEHICLE HISTORY REPORT", maxPricingPage.VhrLabel.Text);
             Assert.AreEqual("1-Owner", maxPricingPage.VhrOwner.Text);
             Assert.AreEqual("Buy Back Guarantee", maxPricingPage.VhrBuyBackGuarantee.Text);
             Assert.AreEqual("No Total Loss Reported", maxPricingPage.VhrTotalLoss.Text);
             Assert.AreEqual("No Frame Damage Reported", maxPricingPage.VhrDamage.Text);
             Assert.AreEqual("No Airbag Deployment Reported", maxPricingPage.VhrAirbag.Text);
             Assert.AreEqual("No Odometer Rollback Reported", maxPricingPage.VhrOdometer.Text);
             Assert.AreEqual("No Manufacturer Recalls Reported", maxPricingPage.VhrManufacturer.Text);
             Assert.AreEqual("No Accident/Damage Reported", maxPricingPage.VhrAccident.Text);

             Assert.AreEqual("View Carfax Report", maxPricingPage.VhrCarfaxReportLink.Text);
             Assert.IsTrue(maxPricingPage.VhrCarfaxReportLink.Displayed);
             maxPricingPage.VhrCarfaxReportLink.Click();
            //To-do Navigate to carafx link and verify the make model desc in new window URl. It should  have same make model desc as that in vehicle Header

         }


         [NUnit.Framework.Test]
         public void TrimFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Console.WriteLine(maxPricingPage.TrimDefaultValue.Text);//current Default value
             maxPricingPage.TrimLabel.Click();
            // maxPricingPage.TrimValue1.Click();
             maxPricingPage.TrimValue2.Click();
             maxPricingPage.TrimUpdate.Click();
             string error = maxPricingPage.TrimErrorMessage.Text;
             Console.WriteLine(error);
             Assert.AreEqual("You must select at least one trim.", error);
             maxPricingPage.TrimValue3.Click();
             maxPricingPage.TrimUpdate.Click();
             Console.WriteLine(maxPricingPage.TrimDefaultValue.Text);//Trim value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual("Trim3", maxPricingPage.TrimDefaultValue.Text);
         }

         [NUnit.Framework.Test]
         public void TransmissionFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Console.WriteLine(maxPricingPage.TransmissionDefaultValue.Text);//current Default value
             maxPricingPage.TransmissionLabel.Click();
             maxPricingPage.TransmissionValue2.Click();
             maxPricingPage.TransmissionUpdate.Click();
             string error = maxPricingPage.TransmissionErrorMessage.Text;
             Console.WriteLine(error);
             Assert.AreEqual("You must make at least one selection.", error);
             maxPricingPage.TransmissionValue1.Click();
             maxPricingPage.TransmissionUpdate.Click();
             Console.WriteLine(maxPricingPage.TransmissionDefaultValue.Text);//Transmission value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual("ttt", maxPricingPage.TransmissionDefaultValue.Text);
         }

         [NUnit.Framework.Test]
         public void DriveTrainFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Console.WriteLine(maxPricingPage.DriveTrainDefaultValue.Text);//current Default value
             maxPricingPage.DriveTrainLabel.Click();
             maxPricingPage.DriveTrainValue1.Click();
             maxPricingPage.DriveTrainUpdate.Click();
             string error = maxPricingPage.DriveTrainErrorMessage.Text;
             Console.WriteLine(error);
             Assert.AreEqual("You must make at least one selection.", error);
             maxPricingPage.DriveTrainValue2.Click();
             maxPricingPage.DriveTrainUpdate.Click();
             Console.WriteLine(maxPricingPage.DriveTrainDefaultValue.Text);//Transmission value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual("hhhh", maxPricingPage.DriveTrainDefaultValue.Text);
         }

         [NUnit.Framework.Test]
         public void EngineFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Console.WriteLine(maxPricingPage.EngineDefaultValue.Text);//current Default value
             maxPricingPage.EngineLabel.Click();
             maxPricingPage.EngineValue1.Click();
             maxPricingPage.EngineUpdate.Click();
             string error = maxPricingPage.EngineErrorMessage.Text;
             Console.WriteLine(error);
             Assert.AreEqual("You must make at least one selection.", error);
             maxPricingPage.EngineValue2.Click();
             maxPricingPage.EngineUpdate.Click();
             Console.WriteLine(maxPricingPage.EngineDefaultValue.Text);//Transmission value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual("kkk", maxPricingPage.EngineDefaultValue.Text);
         }

         [NUnit.Framework.Test]
         public void FuelFilterTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Console.WriteLine(maxPricingPage.FuelDefaultValue.Text);//current Default value
             maxPricingPage.FuelLabel.Click();
             maxPricingPage.FuelValue1.Click();
             maxPricingPage.FuelUpdate.Click();
             string error = maxPricingPage.FuelErrorMessage.Text;
             Console.WriteLine(error);
             Assert.AreEqual("You must make at least one selection.", error);
             maxPricingPage.FuelValue2.Click();
             maxPricingPage.FuelUpdate.Click();
             Console.WriteLine(maxPricingPage.FuelDefaultValue.Text);//Transmission value -After selecting Listing
             Thread.Sleep(3000);
             Assert.AreEqual("5", maxPricingPage.FuelDefaultValue.Text);
         }

         [NUnit.Framework.Test]
         public void VehicleHeaderTest()
         {
             InitializePageAndLogin_MaxPricingPage();
             Thread.Sleep(6000);
             Assert.AreEqual("PingOne | MAX : Online Inventory. Perfected.", driver.Title);
             maxPricingPage.SelectDropdown();
             // Thread.Sleep(5000);

             Boolean makeModelYear = maxPricingPage.VehicleMakeModelYearValue.Displayed;
             Console.WriteLine(maxPricingPage.VehicleMakeModelYearValue.Text);
             Console.WriteLine(makeModelYear);
             Assert.True(makeModelYear);
             Assert.AreEqual("2005 BMW 3 SERIES", maxPricingPage.VehicleMakeModelYearValue.Text);
             Boolean days = maxPricingPage.VehicleDaysValue.Displayed;
             Console.WriteLine(maxPricingPage.VehicleDaysValue.Text);
             Console.WriteLine(days);
             Assert.True(days);
             Boolean color = maxPricingPage.VehicleColorValue.Displayed;
             Console.WriteLine(maxPricingPage.VehicleColorValue.Text);
             Console.WriteLine(color);
             Assert.True(color);
             Boolean mileage = maxPricingPage.VehicleMileageValue.Displayed;
             Console.WriteLine(maxPricingPage.VehicleMileageValue.Text);
             Console.WriteLine(mileage);
             Assert.True(mileage);
             Boolean unitCost = maxPricingPage.VehicleUnitCostValue.Displayed;
             Console.WriteLine(maxPricingPage.VehicleUnitCostValue.Text);
             Console.WriteLine(unitCost);
             Assert.True(unitCost);
             Boolean certified = maxPricingPage.VehicleCertifiedValue.Displayed;
             Console.WriteLine(maxPricingPage.VehicleCertifiedValue.Text);
             Console.WriteLine(certified);
             Assert.True(certified);
         }


        */

        public string FindProfitMaxDealer()
        {
            const String query = @"SELECT top 10 BusinessUnit
                            FROM IMT.dbo.BusinessUnit IDB
                            JOIN IMT.dbo.DealerPreference_Pricing IDP ON IDB.BusinessUnitID = IDP.BusinessUnitID
                            WHERE IsNewPing = 1";

            DataTable dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            var dealerName = dr.ItemArray[0].ToString().Trim();
            return dealerName;
        }


    }
}
