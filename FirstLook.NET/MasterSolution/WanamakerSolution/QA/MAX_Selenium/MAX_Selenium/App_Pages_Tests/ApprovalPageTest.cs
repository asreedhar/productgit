﻿using System;
using System.Data;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

// ReSharper disable UnusedMember.Local

namespace MAX_Selenium
{
    [TestFixture]
    class ApprovalPageTest : Test
    {
        private static object[] GMtestVehicleVins_New_Used = {
                                                               new [] { NewGMVehicleVIN },
                                                               new [] { UsedGMVehicleVIN }
                                                           };

        private static readonly object[] GMtestVehicleVins_Used = {
                                                               new [] { UsedGMVehicleVIN }
                                                           };

        private static readonly object[] GMtestVehicleVins_New = {
                                                               new [] { NewGMVehicleVIN }
                                                           };

        private static object[] BMW328VehicleVins_Used = {
                                                             new[] {UsedBMW328VehicleVIN}
                                                         };

        private static readonly object[] BMWVehicleVIN_WindyCityBMW_Used = {
                                                                     new[] {UsedBMWVehicleVIN_WindyCityBMW}
                                                                 };

        private const String char_251_String =
            "stuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz " +
            "abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxy";

        private const String char_250_String =
            "stuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz " +
            "abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwx";

        private const String char_149_String =
            "stuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz " +
            "abcdefghijklmnopqrstuvwxyz abcde";


        // Ping III title can change through FL-Admin
        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Reprice_link_for_Used_C26880(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.RepriceLink.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until((d) => driver.WindowHandles.Count > 1);
            //Thread.Sleep(5000);
            driver.SwitchTo().Window(driver.WindowHandles[1]);
            //Utility.SaveScreenShot("validateRepriceLink", driver);
            StringAssert.IsMatch("(Ping III)*(Market Pricing)", driver.Title);            
        }
 
        [Test]
        [Category("SmokeApproval")]
        public void Reprice_Function_UsedCar_C1224()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.InventoryType_DDL.SelectByText("Used Vehicles Only");
            approvalPage = inventoryPage.ClickOnFirstVehicle();
            var vehiclePrice = Utility.ExtractDigitsFromString(approvalPage.InternetPriceValue.Text);
            if (vehiclePrice == 0)
            { vehiclePrice = vehiclePrice + 1000; }
            else if (vehiclePrice > 50000)
            {
                vehiclePrice = 200;
            }
            else { vehiclePrice = vehiclePrice + 200; }
            var inputValue = Convert.ToString(vehiclePrice);

            approvalPage.RepriceThroughPingPage(inputValue);
            var modifiedInternetPrice = Utility.ExtractDigitsFromString(approvalPage.InternetPriceValue.Text);
            //Utility.SaveScreenShot("Test_Reprice_Function_UsedCar_C1224", driver);
            Assert.AreEqual(vehiclePrice, modifiedInternetPrice);
        }       
 
        /*
        //Test requires AutoApprove disabled to render warning text. Few customers (about 40) are in this state.
        //Test has very long run time and limited value. Commenting out for this reason. -T. Muir 6/26/2015
        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Warning_Message_After_Price_Change_Reprice_C1213(string vin)
        {
            //InitializePageAndLogin_InventoryPage();
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_autoapprove_page = inventoryPage.GoTo_MAXSettings_AutoApprove_Tab();
            settings_maxsettings_autoapprove_page.Disable_AutoApprove();
            settings_maxsettings_autoapprove_page.Inventory_Link.Click();
            inventoryPage.WaitForAjax();
            approvalPage = inventoryPage.Goto_ApprovalPageForFoundVehicle(vin);
            var vehiclePrice = Utility.ExtractDigitsFromString(approvalPage.InternetPriceValue.Text);
            if (vehiclePrice <= 200) { vehiclePrice = vehiclePrice + 1000; }
            String inputValue = Convert.ToString(vehiclePrice);
            approvalPage.RepriceThroughPingPage(inputValue);
            approvalPage.Approve_AD();
            String inputValue2 = Convert.ToString((Convert.ToInt32(inputValue) + 500));
            approvalPage.RepriceThroughPingPage(inputValue2);
            Assert.That(driver.FindElement(By.CssSelector("span.message")).Text.Contains("WARNING: Ad copy created with outdated price"));
            //Re-enable AutoApprove
            approvalPage.Settings.Click();
            approvalPage.MAX_Settings.Click();
            approvalPage.AutoApprove_Tab.Click();
            settings_maxsettings_autoapprove_page.Enable_AutoApprove();

        }
         */

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Highlight_Preview_Part_Of_Ad_C1744(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            var bg = approvalPage.PreviewSection.GetCssValue("background-image").ToLower();
            var bc = approvalPage.PreviewSection.GetCssValue("border-top-color");
            var comparisonString1 = "Images/sprite_Gradients3.png";
            comparisonString1 = comparisonString1.ToLower();
            Assert.That(bg.Contains(comparisonString1));
            Assert.That(bc.Contains("rgba(201, 201, 201, 1)"));
        }

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Key_Information_Text_ShowUp_In_AD_C1745(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);           
            approvalPage.Open_Key_Information_Header();

            Boolean temp = approvalPage.KeyInformationNonSmoker.Selected;
            if (temp == false)
            {
                approvalPage.KeyInformationNonSmoker.Click();
            }
            approvalPage.RegenerateAdButton.Click();
            Assert.IsTrue(approvalPage.WholeAddSection.Text.Contains("Non-Smoker vehicle"));
        }

        //assumes new vehicle pricing feature is enabled. 
        [TestCaseSource("GMtestVehicleVins_New")] // not used anymore
        [Category("SmokeApproval")]
        public void Reprice_Function_NewCar_C6360(string vin)
        {
            InitializePageAndLogin_InventoryPage();          
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.SelectVehicles(InventoryType.New);
            approvalPage = inventoryPage.ClickOnFirstVehicle();
            var temp = approvalPage.NewCarListingPrice.GetAttribute("value");          
            var originalValue = Utility.ExtractDigitsFromString(temp);
            var newValue = Convert.ToString(originalValue + 1000);
            approvalPage.NewCarListingPrice.Clear();
            approvalPage.NewCarListingPrice.SendKeys(newValue);
            approvalPage.NewCarRepriceButton.Click();
            var temp2 = approvalPage.NewCarListingPrice.GetAttribute("value");
            Assert.AreEqual(newValue, temp2);
            approvalPage.Approve_AD();
            driver.Navigate().Refresh();
            if (BrowserType == "Firefox") //handle the unique Firefox POST alert
            {
                if (isAlertPresent())
                {
                    IAlert alert = driver.SwitchTo().Alert();
                    alert.Accept();
                }
                
            }
            WaitForAjax();
            Thread.Sleep(1000);
            var temp3 = approvalPage.NewCarListingPrice.GetAttribute("value");
            temp3 = temp3.Replace(",", "");           
            Assert.AreEqual(newValue, temp3);
        }

        //John disabled this test in Sept 2012. See FB 20723 for details. 
        //Requires AutoApprove to be disabled. Only about 40 customers in that state now - 6/26/2015.
        //Test is long-running and limited value. Keeping active due to unresolved bug FB 20723.
        [TestCaseSource("GMtestVehicleVins_New")]
        [Ignore]
        public void Reprice_NewCar_REGEN_AD_Button_RED_C6106(string vin)
        {
            //InitializePageAndLogin_InventoryPage();
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_autoapprove_page = inventoryPage.GoTo_MAXSettings_AutoApprove_Tab();
            settings_maxsettings_autoapprove_page.Disable_AutoApprove();
            settings_maxsettings_autoapprove_page.Inventory_Link.Click();
            inventoryPage.WaitForAjax();
            inventoryPage.InventoryType_DDL.SelectByText("New Vehicles Only");
            inventoryPage.ClickOnFirstVehicle();
            approvalPage = new ApprovalPage(driver);
            approvalPage.RegenerateAdButton.Click();
            var temp = approvalPage.NewCarListingPrice.GetAttribute("value");
            var originalValue = Utility.ExtractDigitsFromString(temp);
            var newValue = Convert.ToString(originalValue + 1000);
            approvalPage.NewCarListingPrice.Clear();
            approvalPage.NewCarListingPrice.SendKeys(newValue);
            approvalPage.NewCarRepriceButton.Click();
            Assert.That(approvalPage.RegenerateAdButton.HasClass("red"), Is.True);
            //Re-enable AutoApprove
            approvalPage.Settings.Click();
            approvalPage.MAX_Settings.Click();
            approvalPage.AutoApprove_Tab.Click();
            settings_maxsettings_autoapprove_page.Enable_AutoApprove();

         }

        [TestCaseSource("GMtestVehicleVins_Used")]    
        public void Add_Package_Through_View_Available_Packages_Showup_In_AdPreview_C1226_1(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.Open_No_Packages_Header();     
            approvalPage.ViewAvailablePackages.Click();
            approvalPage.PackagesDialogFirst_Checkbox.Click();
            approvalPage.PackagesDialogAdd_Button.Click();
            // get the newly added package title name on the Option Packages popup window
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions
                  .ElementIsVisible(By.CssSelector("div.ui-dialog a.ui-dialog-titlebar-close.ui-corner-all")));

            var packageName = (driver.FindElement(By.Id("UserTitle")).GetAttribute("value")).Trim();  

            wait.Until(ExpectedConditions
                 .ElementIsVisible(By.CssSelector("div.ui-dialog a.ui-dialog-titlebar-close.ui-corner-all")));
            approvalPage.PackagesDialogClose_Button.Click();
            approvalPage.Approve_AD();
            Assert.IsTrue(approvalPage.ADpreviewSection.Text.Contains(packageName));
            //delete the added package
            approvalPage.Open_No_Packages_Header();                
            driver.FindElement(By.Id("BtnDelete")).Click();
            wait.Until(ExpectedConditions
                  .ElementIsVisible(By.CssSelector("div#AdPanel input#RewriteAdButton.red")));
            approvalPage.Approve_AD();
        }

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Removed_Packages_DoNotAppear_In_AdPreview_C1226_2(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            //add a package
            approvalPage.Open_No_Packages_Header();
            approvalPage.ViewAvailablePackages.Click();
            driver.FindElement(By.Id("ChkAdd")).Click();
            driver.FindElement(By.Id("BtnAdd")).Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("UserTitle")));
            var packageName = (driver.FindElement(By.Id("UserTitle")).GetAttribute("value")).Trim(); // get the newly added package title name on the Option Packages popup window
            //wait for Options dialog to load
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.ui-dialog tr:last-child")));
            approvalPage.WaitForAjax();
            driver.FindElement(By.CssSelector("div.ui-dialog a.ui-dialog-titlebar-close")).SendKeys(Keys.Return); // close the Option Packages pop up window
            approvalPage.RegenerateAdButton.Click();
            //delete the package
            approvalPage.Open_No_Packages_Header();
            driver.FindElement(By.Id("BtnDelete")).Click();
            approvalPage.RegenerateAdButton.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
            Assert.IsFalse(approvalPage.ADpreviewSection.Text.Contains(packageName));        
        }
        
        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Add_Section_Then_Delete_It_C1232_C1231(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.AddSection("webdriver section test");
            var newlyAddedModule = approvalPage.FindSection("webdriver section test");
            Assert.IsNotNull(newlyAddedModule);            
            //clean up/delete the added section
            approvalPage.DeleteSection(newlyAddedModule);
        }

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void validate_Last_Ad_Published_for_Used_C1234 (string vin)
        {
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.LastAdPublishedLink.Click();
            //Utility.saveScreenShot("validateLastAdPublished", driver);          
            Assert.IsTrue(driver.FindElement(By.CssSelector("span#ui-id-1.ui-dialog-title")).Text.Contains("Previous Ad"));
        }

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void validate_Certified_ID_8_Digits_GM_Used_C6903(string vin)
        {
            const string certifiedFragment = "Certified Pre-Owned Vehicle (CPOV)";
            const string eightDigitString = "12345678";
            var carData = new VehicleData.BasicVehicleData(vin);
            var certifiedString = "" + carData.Make + " " + certifiedFragment + "";
            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.Certified_Programs_DDL = new SelectElement(approvalPage.certified_Programs_ddl_bud);

            if (approvalPage.IsPriceLessThan200())
                approvalPage.RepriceThroughPingPage("5000");
            
            if (approvalPage.Certified_Programs_DDL.SelectedOption.Text != certifiedString) 
            {
                approvalPage.Certified_Programs_DDL.SelectByText(certifiedString);
            }
            approvalPage.CertifiedIdTextField.Clear();
            approvalPage.CertifiedIdTextField.SendKeys(eightDigitString);
            approvalPage.Approve_AD();
            driver.Navigate().Refresh();
            if (BrowserType == "Firefox") //handle the unique Firefox POST alert
            {
                if (isAlertPresent())
                {
                    IAlert alert = driver.SwitchTo().Alert();
                    alert.Accept();
                }

            }
            var carData2 = new VehicleData.BasicVehicleData(vin); // trigger updated DB call
            Assert.AreEqual(eightDigitString, approvalPage.CertifiedIdTextField.GetAttribute("value"));
            Assert.True(carData2.CertifiedId == eightDigitString);
        }

        [Test]
        //[ExpectedException(typeof(NoSuchElementException))] // sometimes needed sometimes not..??
        public void No_Certified_DDL_For_New_Vehicles_C20737() //repurposed from C6904
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.SelectVehicles(InventoryType.New);
            approvalPage = inventoryPage.ClickOnFirstVehicle();
            Assert.Throws<NoSuchElementException>(approvalPage.certified_Programs_ddl_bud.Click);
        }
     
        [TestCaseSource("GMtestVehicleVins_Used")]
        //[ExpectedException(typeof(NoSuchElementException))] // This hosed us again, unsure if/when/how needed
        public void validate_Preview_Setting_C5199_250_char(string vin)
        {
            const string preview_250_Warning =
                "Your preview has exceeded the character limit (250) set in your preferences. Please correct this before clicking 'Done.'";
            InitializePageAndLogin_InventoryPage();
            inventoryPage.Set_Preview_Length(driver, "preview250"); // takes "preview250"; "preview150"; "previewCustom" (for previewCustom, length is 300)
            inventoryPage.GoBackTo_ApprovalPage(vin, BaseURL, InventoryPageURL);
            ApprovalPage approvalPage = new ApprovalPage(driver);
            // Each module blurb box can only hold 249 characters. When maximum test string length is 250, we need to add 2 characters to next module for a minimum 251 chars.
            approvalPage.Input_Long_Text_Into_Preview_Edit_Mode(driver, char_251_String);
            driver.FindElement(By.Id("Module0Blurb1Textbox")).SendKeys("xy"); 
            Assert.AreEqual(preview_250_Warning, approvalPage.PreviewLengthWarning.Text);
            approvalPage.PreviewCancelButton_0.Click();
            approvalPage.Input_Long_Text_Into_Preview_Edit_Mode(driver, char_250_String);
            Assert.Throws<NoSuchElementException>(approvalPage.PreviewLengthWarning.Click);
        }

        [TestCaseSource("GMtestVehicleVins_Used")] //Failing until FB 31025 is fixed.
        [Ignore]  // remove ignore attribute when 31025 is fixed
        public void validate_Preview_Setting_C5199_150char(string vin)
        {
            const string preview_150_Warning =
                "Your preview has exceeded the character limit (150) set in your preferences. Please correct this before clicking 'Done.'";
            InitializePageAndLogin_InventoryPage();
            inventoryPage.Set_Preview_Length(driver, "preview150"); // takes "preview250"; "preview150"; "previewCustom" (for previewCustom, length is 300)
            inventoryPage.GoBackTo_ApprovalPage(vin, BaseURL, InventoryPageURL);
            ApprovalPage approvalPage = new ApprovalPage(driver);
            approvalPage.Input_Long_Text_Into_Preview_Edit_Mode(driver, char_251_String);
            Assert.AreEqual(preview_150_Warning, approvalPage.PreviewLengthWarning.Text);
            approvalPage.PreviewCancelButton_0.Click();
            approvalPage.Input_Long_Text_Into_Preview_Edit_Mode(driver, char_149_String);
            Assert.Throws<NoSuchElementException>(approvalPage.PreviewLengthWarning.Click);
        }

        [TestCaseSource("BMWVehicleVIN_WindyCityBMW_Used")]
        public void Non_Certified_Vehicle_C2376(string vin)
        {
            InitializePageAndLogin_Inventory_Direct("qashiner", "N@d@123", "Windy City BMW");
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.Certified_Programs_DDL = new SelectElement(approvalPage.certified_Programs_ddl_bud);

            if (approvalPage.IsPriceLessThan200())
                approvalPage.RepriceThroughPingPage("5000");

            if (approvalPage.Certified_Programs_DDL.SelectedOption.Text != "Not Certified") // If already not certified, just check the DB flag
            {
                approvalPage.Certified_Programs_DDL.SelectByText("Not Certified");
                approvalPage.Approve_AD();
                driver.Navigate().Refresh();
                Assert.True(approvalPage.Certified_Programs_DDL.SelectedOption.Text == "Not Certified");
            }

            var carData = new VehicleData.BasicVehicleData(vin); // trigger updated DB call
            Assert.True(carData.Certified == 0);
        }

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Regen_Approve_CertificationID_Intact_C2375(string vin)
        {
            const string certifiedFragment = "Certified Pre-Owned Vehicle (CPOV)";
            const string eightDigitString = "12123232";
            var carData = new VehicleData.BasicVehicleData(vin);
            var previewCertText = carData.Make + " Certified";
            var certifiedString = "" + carData.Make + " " + certifiedFragment + "";

            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.Certified_Programs_DDL = new SelectElement(approvalPage.certified_Programs_ddl_bud);

            if (approvalPage.IsPriceLessThan200())
                approvalPage.RepriceThroughPingPage("5000");

            if (approvalPage.Certified_Programs_DDL.SelectedOption.Text != certifiedString)
            {
                approvalPage.Certified_Programs_DDL.SelectByText(certifiedString);
                approvalPage.CertifiedIdTextField.Clear();
                approvalPage.CertifiedIdTextField.SendKeys(eightDigitString);
                approvalPage.Approve_AD();
                //driver.Navigate().Refresh();
            }
            else if (approvalPage.CertifiedIdTextField.GetAttribute("value") == "")
            {
                approvalPage.CertifiedIdTextField.SendKeys(eightDigitString);
                approvalPage.Approve_AD();
            }

            var initialCertId = approvalPage.CertifiedIdTextField.GetAttribute("value");
            approvalPage.RegenerateAdButton.Click();
            Assert.That(approvalPage.PreviewSection.Text.Contains(previewCertText));
            Assert.True(approvalPage.Certified_Programs_DDL.SelectedOption.Text == certifiedString);
            approvalPage.ApprovalButton.Click();            // execute each step menually; don't use Approve_AD method
            approvalPage.ClickApprovalSuccessfulNo_Button();
            Thread.Sleep(1000);
            var currentCertId = approvalPage.CertifiedIdTextField.GetAttribute("value");
            Assert.True(currentCertId == initialCertId);

        }

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Modify_Certification_ID_C2378(string vin)
        {
            const string certifiedFragment = "Certified Pre-Owned Vehicle (CPOV)";
            const string eightDigitString = "12123232";
            const string sevenDigitString = "7654321";
            var carData = new VehicleData.BasicVehicleData(vin);
            var certifiedString = "" + carData.Make + " " + certifiedFragment + "";

            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.Certified_Programs_DDL = new SelectElement(approvalPage.certified_Programs_ddl_bud);

            if (approvalPage.IsPriceLessThan200())
                approvalPage.RepriceThroughPingPage("5000");

            if ((approvalPage.Certified_Programs_DDL.SelectedOption.Text != certifiedString) ||
                (approvalPage.CertifiedIdTextField.GetAttribute("value") == sevenDigitString))
            {
                approvalPage.Certified_Programs_DDL.SelectByText(certifiedString);
                approvalPage.CertifiedIdTextField.Clear();
                approvalPage.CertifiedIdTextField.SendKeys(eightDigitString);
                approvalPage.Approve_AD();
                var carData3 = new VehicleData.BasicVehicleData(vin); // trigger DB query for new data
                Assert.True(carData3.CertifiedId == eightDigitString); // confirm it got changed before proceeding
                //driver.Navigate().Refresh();
            }

            approvalPage.CertifiedIdTextField.Clear();
            approvalPage.CertifiedIdTextField.SendKeys(sevenDigitString);
            approvalPage.Approve_AD();
            approvalPage.EquipmentPageTab.Click();
            approvalPage.ApprovalPageTab.Click();
            var uiCertId = approvalPage.CertifiedIdTextField.GetAttribute("value");
            var carData2 = new VehicleData.BasicVehicleData(vin); // trigger DB query for new data
            Assert.True(uiCertId == sevenDigitString);
            Assert.True(carData2.CertifiedId == sevenDigitString);

        }

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Alphanumeric_Chars_Max_15_Chars_C2379(string vin)
        {
            const string certifiedFragment = "Certified Pre-Owned Vehicle (CPOV)";
            const string textString = "abcdefghijklmnop";
            const string warningText =
                "CertifiedId is not a valid format. A Certified Id should be a number 6 to 8 digits long.";
            var carData = new VehicleData.BasicVehicleData(vin);
            var certifiedString = "" + carData.Make + " " + certifiedFragment + "";

            InitializePageAndLogin_InventoryPage();
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.Certified_Programs_DDL = new SelectElement(approvalPage.certified_Programs_ddl_bud);

            if (approvalPage.Certified_Programs_DDL.SelectedOption.Text != certifiedString)
                approvalPage.Certified_Programs_DDL.SelectByText(certifiedString);

            approvalPage.CertifiedIdTextField.Clear();
            approvalPage.CertifiedIdTextField.SendKeys(textString);
            approvalPage.RegenerateAdButton.Click();
            Assert.AreEqual(warningText, approvalPage.CloseAlertAndGetItsText());

        }

        [Test]
        public void Certified_DropDown_Different_Manufactures_C2381()
        {
            // list needs to be representative, not exhaustive. Include special cases like BMW, MINI and Mercedes
            var makers = new[] { "BMW", "Chevrolet", "Chrysler", "Dodge", "Ford", 
                "Honda", "Mercedes-Benz", "MINI", "Nissan", "Toyota", "Volkswagen" };

            const string basicCertifiedFragment = " Certified Pre-Owned Vehicle (CPOV)";
            const string bmwFragment = " Certified Pre-Owned";
            const string MINIFragment = " NEXT CERTIFIED PRE-OWNED";
            string certifiedString;
            var located = false;
            var totalMakers = makers.Length;
            var failedToLocate = 0; //track fail-to-find makes
            var thisVin = "";
            var invType = 0;

            // This Test case also takes care of C2380 -- Manufacturer's Name
            InitializePageAndLogin_InventoryPage();

            foreach (var make in makers)
            {

                inventoryPage.InventoryTab.Click();

                switch (make)
                {
                    case "BMW":
                        certifiedString = make + bmwFragment;
                        break;
                    case "MINI":
                        certifiedString = make + MINIFragment;
                        break;
                    case "Mercedes-Benz":
                        certifiedString = "Mercedes" + basicCertifiedFragment;
                        break;
                    default:
                        certifiedString = make + basicCertifiedFragment;
                        break;
                }

                // if the manufacturer is not there then just go to the next one
                try
                {
                    approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(make);
                    approvalPage.Certified_Programs_DDL = new SelectElement(approvalPage.certified_Programs_ddl_bud);
                    located = true;
                    thisVin = approvalPage.VIN.Text;
                    invType = new VehicleData.BasicVehicleData(thisVin).InventoryType;

                }
                catch (NoSuchElementException)
                {
                    located = false;                  
                }

                if (!located || (invType != 2)) // exclude new cars
                {
                    failedToLocate = failedToLocate++;
                    continue; //skip current iteration
                }
                if (approvalPage.Certified_Programs_DDL.SelectedOption.Text != certifiedString) // if matching it is an automatic pass
                {
                    approvalPage.Certified_Programs_DDL.SelectByText(certifiedString); // test should blow up if DDL not found
                    Assert.That(approvalPage.RegenerateAdButton.HasClass("red"), Is.True); //only appears if DDL changes
                }

            }
            if (failedToLocate == totalMakers)
                throw new Exception("Zero cars found from makers list");
            Assert.True(true); // if nothing blows up, this entire test passed. This is needed b/c Red button asserts may get bypassed if all DDLs are currently set

         }

        [Test]
        [Ignore]    //We have a new Publishing app that behaves differently. Currently if PDF is not found it redirects to "coming soon" page.
                    //Test needs rework
                    //this test assumes PDFs are generated instantly upon Approval and could easily yield false positives.
        public void WebsitePDF_NoUpgrade_C2385()
        {
            //Find dealer where MAX Merchandising(Selling & Email) upgrade is NOT active
            string query =
                @"select * from FLDW.dbo.BusinessUnit BU
                  where exists (select 1 
                  from IMT.dbo.DealerUpgrade DU 
                  where BU.BusinessUnitID = DU.BusinessUnitID 
                  and DU.DealerUpgradeCD = 24 
                  and Active = 1) -- Has Max Ad
                  and not exists(
                        select 1
                        from IMT.dbo.DealerUpgrade DU
                        where BU.BusinessUnitID = DU.BusinessUnitID
                        and DU.DealerUpgradeCD = 23
                        and Active = 1) -- Does not have MAX Merchandising(Selling & Email)";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            string dealer_name = dr.ItemArray[2].ToString().Trim();
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.SelectVehicles(InventoryType.Used);
            approvalPage = inventoryPage.ClickOnFirstVehicle();

            if (approvalPage.IsPriceLessThan200())
                approvalPage.RepriceThroughPingPage("5000");

            approvalPage.Approve_AD();
            driver.Navigate().Refresh();
            Thread.Sleep(4000); // Give PDF time to generate if it's going to
            string vin_num = approvalPage.VIN.Text;
            string url = string.Format("http://betapub.firstlook.biz/go?vin={0}&doctype=1", vin_num);
            driver.Navigate().GoToUrl(url);
            Assert.That(driver.Url.Equals("http://betapub.firstlook.biz/Pages/PdfNotFound.htm"));
        }

        [Test] // Test explicitely looks for car that has no Trim
        public void Vehicle_Without_TrimData_In_Dropdown_OlderThan1989_C4991()
        {
            //the following query cannot run faster than 60s on a DB with clean buffer and cache. Daniel optimized.
            string query =
                @"select top(5) V.Vin, V.VehicleYear, BU.BusinessUnit
                    from IMT.dbo.Vehicle as V
                        INNER JOIN FLDW.dbo.InventoryActive as IA
                        on V.VehicleID = IA.VehicleID
                        INNER JOIN IMT.dbo.BusinessUnit as BU
                        on IA.BusinessUnitID = BU.BusinessUnitID
                        INNER JOIN Merchandising.builder.OptionsConfiguration as oc 
                        ON oc.InventoryID = IA.InventoryID and oc.businessUnitID = IA.BusinessUnitID
                    where oc.chromeStyleID = -1
                        and exists (select * from IMT.dbo.DealerUpgrade du where du.Active = 1 and du.DealerUpgradeCD = 24 and du.BusinessUnitId = IA.BusinessUnitID)
                        and exists (select * from IMT.dbo.tbl_Vehicle v1 where VehicleYear < 1989 and v1.VehicleId = V.VehicleID )";
            var dataTable = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            WaitForDatabase(dataTable);

            if(dataTable.Rows.Count > 0)
            {
                DataRow dr = dataTable.Rows[0];
                string dealer_name = dr.ItemArray[2].ToString().Trim();
                string vin = dr.ItemArray[0].ToString().Trim();
                InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
                approvalPage = inventoryPage.Goto_ApprovalPageNoTrimVehicle(vin);
            }
            Assert.AreEqual("Please select trim above to continue.", driver.FindElement(By.CssSelector("div.mask > p")).Text);
        }

        

        //private static object[] _ModelLevelFrameData = {
        //                                                       new [] { NewMazdaVehicleVIN, "qa103307", "abcd1234" } // login to Faulkner Mazda
        //                                                   };
        // need to add the logic to turn on the "Model Specific Ads" in Miscellaneous Setting page.
        // run setting for the framework defaulting (Vehicle Framework Defaults), Need to set them before the real test begining to run. 
        /*
        [TestCaseSource("_ModelLevelFrameData")]
        public void Test_Multiple_Parameters(string vin, string username, string password)
        {
            InitializePageAndLogin_InventoryPage(username, password);
            approvalPage = inventoryPage.gotoApprovalPageForFoundVehicleByVin(vin);
            var HYMM = approvalPage.HeaderYearMakeModelLabel.Text;
            var TSDD = approvalPage.TrimStyleDropDown.Text;
            var PAdL = approvalPage.PreviousAdLink.Text;
            var FWDD = approvalPage.FrameWorkDropDown.Text;

            Assert.That(FWDD.Contains(HYMM)); // need to come up a string that conbine the common thing in both HYMM and TSDD
        }*/

        /*
        private static object[] TestCaseData = {
                                                               new [] { newGMVehicleVIN, "qa101621", "N@d@123" }
                                                           };
        [TestCaseSource("TestCaseData")]
        public void Test_Multiple_Parameters(string vin, string username, string password)
        {
            InitializePageAndLogin_InventoryPage(username, password);

            Assert.That(vin, Is.EqualTo(newGMVehicleVIN));
        }
        */


        //[TestCase(Firefox, true)]
        //[TestCase(InternetExplorer, true)]
        [TestCase(Chrome, true)]
        [Category("SmokeApproval")]
        public void Approval_Term_Conditions_PriceInBetween_0_200_C3155(string browser, bool overRide)
        {
            InitializeDriver(browser, overRide);
            InitializePageAndLogin_InventoryPage();
            inventoryPage.InventoryType_DDL.SelectByText("Used Vehicles Only");
            approvalPage = inventoryPage.ClickOnFirstVehicle();
            var vehiclePrice = Utility.ExtractDigitsFromString(approvalPage.InternetPriceValue.Text);
            if (vehiclePrice == 0)
            {
                vehiclePrice = vehiclePrice + 1;
            }
            else if (vehiclePrice == 199)
            {
                vehiclePrice = 199 - 100;
            }
            else { vehiclePrice = 199; }
            String inputValue = Convert.ToString(vehiclePrice);

            approvalPage.RepriceThroughPingPage(inputValue);
            Utility.SaveScreenShot("Approval_Term_Conditions_InBetween_Zero_Two_Hundred_C3155", driver);

            Assert.That(
                approvalPage.ApprovalAgreement.Text.Contains("You must reprice this vehicle before approving the advertisement."));
        }

        [Test]
        public void TrimNotSet_PageElements_Disabled_C1201()
        {   
            //the following query locates dealers who have used no-trim cars in inventory
            string query =
                @"SELECT DISTINCT top (5) BusinessUnit 
				FROM Merchandising.workflow.Inventory i
                JOIN IMT.dbo.BusinessUnit bu on bu.BusinessUnitID = i.BusinessUnitID
                WHERE (i.InventoryType = 2 AND i.ChromeStyleId = -1)
                AND EXISTS (select * from IMT.dbo.DealerUpgrade du 
                where du.Active = 1 
                and du.DealerUpgradeCD = 24
                and bu.Active = 1 
                and du.BusinessUnitId = bu.BusinessUnitID)";
            var dataTable = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            WaitForDatabase(dataTable);

            //only execute test if a no-Trim car can be found
            if (dataTable.Rows.Count > 0)
            {
                DataRow dr = dataTable.Rows[0];
                string dealer_name = dr.ItemArray[0].ToString().Trim();
                string vin = dr.ItemArray[0].ToString().Trim();
                InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
                inventoryPage.WaitForAjax();
                inventoryPage.NoTrim_Sub_Tab.Click();
                inventoryPage.FirstVehicleLink.Click();
                var approvalPage = new ApprovalPage(driver); //cannot use ClickOnFirstVehicle() method
                Assert.True(driver.FindElement(By.CssSelector("form#form1 div.mask")).Enabled);
                Assert.False(approvalPage.RegenerateAdButton.Enabled);
                Assert.False(approvalPage.ApprovalButton.Enabled);

            }
          }
       }   
    }

