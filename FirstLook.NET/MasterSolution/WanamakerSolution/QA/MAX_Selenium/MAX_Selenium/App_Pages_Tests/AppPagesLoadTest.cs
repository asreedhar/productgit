﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{

    [TestFixture]
    class AppPagesLoadTest : Test
    {

        [Test]
        [Category("SmokePagesAndLinks")]
        [Description("Quick check for release night to verify in one test each tab loads correctly")]
        public void Verify_All_MaxSettings_Tabs_Load_C20256()
        {
            string dealer = "Windy City BMW";
            InitializePageAndLogin_MAX_Settings_Page("QABlueMoon", "N@d@123", dealer);   
            Thread.Sleep(4000);

            settings_maxsettings_page.Specials_Tab.Click();  //this will make the first tab not active and become "tab_button_0", needed for the loop to work

            for(int i = 0; i < 13; i++)
            {
                string tabLocation = ".//input[@class='Tab_Button_" + i +"']";
                driver.FindElement(By.XPath(tabLocation)).Click();
                Thread.Sleep(2000);

                string tabLocation_ActiveWindow = ".//input[@class='Tab_Button_" + i + " ui-tab-active-input']";
                Assert.IsTrue((driver.FindElement(By.XPath(tabLocation_ActiveWindow)).Displayed), "Failed at dealer {0}", dealer);
            }

        }

        //[TestCase(InternetExplorer, true)]
        [TestCase(Chrome, true)]
        [Category("SmokePagesAndLinks")]
        [Description("Make sure DPA page loads as expected")]
        public void VerifyDPAPageLoad_C27706(string browser, bool overRide)
        {
            InitializeDriver(browser, overRide);
            const string dealer = "Windy City BMW";
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.GoTo_DigitalPerfAnalysisPage();
            Assert.True(driver.Title == "Digital Performance Analysis | MAX : Intelligent Online Advertising Systems");

        }

        [Test]  // NOTE: This test will fail if there are zero No Trim (0) cars under "Need Review"
                // This is b/c the No Description subfilter will not render when No Trim = 0.
                // Test needs to be re-written to account for this. In the meantime, choose another dealer and run manually.
        [Category("SmokePagesAndLinks")]
        [Description("quick check for release night to verify in one test each filter loads correctly")]
        public void Verify_All_Filters_Load_InventoryPage_C20243()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Chevrolet");

            Thread.Sleep(5000);

            for (int i = 1; i < 5; i++) //to cycle through each tab
            {
                IWebElement tab = driver.FindElement(By.XPath("//*[@id=\"MainFilters\"]/ul/li[" + i + "]/a"));
                string tabname = tab.Text;
                tab.Click();
                Thread.Sleep(3000);

                int k; //number of filters differ per tab
                if (i == 1 || i == 4)
                    k = 4;
                else if (i == 2)
                    k = 3;
                else
                    k = 6;


                for (int j = 1; j <= k; j++) //to cycle through each filter per tab
                {
                    IWebElement filter = driver.FindElement(By.XPath("//*[@id=\"MainFilters\"]/ul/li[" + i + "]/div/ul/li[" + j + "]/a"));
                    string filterName = filter.Text;
                    filter.Click();
                    Thread.Sleep(2000);
                    String pattern1 = "\\(";
                    string[] filterNameFormatted = Regex.Split(filterName, pattern1);

                    string currentFilter = driver.FindElement(By.XPath("//*[@id=\"CurrentFilter\"]/span[2]")).Text;
                    String pattern2 = ">";
                    string[] currentFilterFormatted = Regex.Split(currentFilter, pattern2);

                    Assert.That(filterNameFormatted[0], Is.EqualTo(currentFilterFormatted[0]),
                      "The {0}filter on {1}tab did not load properly", filterNameFormatted[0], tabname);

                }

            }

        }


        [Test]
        [Category("SmokePagesAndLinks")]
        [Description("quick check for release night to verify in one test each tab on the dashboard loads correctly")]
        public void Verify_All_Tabs_Load_Dashboard_C20242()
        {
            const string dealer = "Windy City BMW"; //no data is required from services

            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            //check/enable all settings needed to show all possible tabs
            admin_Miscellaneous_Page = dashBoardPage.GoTo_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.Enable_TTM_DPA_OCO();
            admin_Upgrades_Page = admin_Miscellaneous_Page.GoTo_Admin_Upgrades_Page();
            admin_Upgrades_Page.Check_MfW_MDS_DSWR();
            dashBoardPage = admin_Miscellaneous_Page.GoTo_DashBoardPage();

            IWebElement[] TabName_WebElement = {
                                                   dashBoardPage.Time_to_Market_tab,
                                                   dashBoardPage.Online_Classified_Performance_tab,
                                                   dashBoardPage.CostPerConsumerImpressions_ClassifiedPerformance_subtab,
                                                   dashBoardPage.CostPerDirectLead_ClassifiedPerformance_subtab,
                                                   dashBoardPage.Classified_Trends_tab,
                                                   dashBoardPage.ConsumerImpressions_ClassifiedTrends_subtab,
                                                   dashBoardPage.CostPerConsumerImpressions_ClassifiedTrends_subtab,
                                                   dashBoardPage.DirectLeads_ClassifiedTrends_subtab,
                                                   dashBoardPage.CostPerDirectLeads_ClassifiedTrends_subtab,
                                                   dashBoardPage.Website_Traffic_tab,
                                                   dashBoardPage.TrafficSources_WebsiteTraffic_subtab,
                                                   dashBoardPage.TrafficTrends_WebsiteTraffic_subtab,
                                                   dashBoardPage.MobileTraffic_WebsiteTraffic_subtab,
                                                   dashBoardPage.Showroom_and_Website_tab,
                                                   dashBoardPage.Website_20_Traffic_ShowroomWebsite_subtab,
                                                   dashBoardPage.Website_20_Leads_ShowroomWebsite_subtab
                                               };
            string[] TabName_Id = {
                                      "tab_ttm",
                                      "tab_perf",
                                      "tab_impression",
                                      "tab_lead",
                                      "tab_trends",
                                      "tab_vdp_trend",
                                      "tab_costper_vdp_trend",
                                      "tab_lead_trend",
                                      "tab_costper_lead_trends",
                                      "tab_ga",
                                      "tab_sources",
                                      "tab_ga_trends",
                                      "tab_mobile",
                                      "tab_show",
                                      "tab_wst",
                                      "tab_wsl"
                                  };

            var elementCount = TabName_WebElement.Count();

            for (int i = 0; i < elementCount; i++)
            {
                TabName_WebElement[i].Click();
                Thread.Sleep(1000);
                Assert.IsTrue((Convert.ToBoolean(driver.FindElement(By.Id(TabName_Id[i])).GetAttribute("aria-expanded"))), "Failed tab: {0}, Dealer: {1}", TabName_Id[i], dealer);
            }
        }

        [Test] //This test will fail if "No Trim" = 0 b/c the No Description filter won't render. Needs updating.
        // Test assumes all "Display on Dashboard" checkboxes are selected in Settings --> Alerts tab.
        [Category("SmokePagesAndLinks")]
        [Description("quick check for release night to verify in one test each link on the dashboard goes to the right filter")]
        public void Test_Link_Navigation_Dashboard_C20246()
        {

            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Windy City Chevrolet");
            WaitForAjax();
            //Thread.Sleep(4000); Probably not needed.

            string[,] dashboardLinks = new string[12, 2];

            dashboardLinks[0, 0] = dashBoardPage.NoPriceLink.GetAttribute("href");
            dashboardLinks[0, 1] = dashBoardPage.NoPriceLink.Text;

            dashboardLinks[1, 0] = dashBoardPage.NoDescriptionLink.GetAttribute("href");
            dashboardLinks[1, 1] = dashBoardPage.NoDescriptionLink.Text;

            dashboardLinks[2, 0] = dashBoardPage.NoPhotosLink.GetAttribute("href");
            dashboardLinks[2, 1] = dashBoardPage.NoPhotosLink.Text;

            dashboardLinks[3, 0] = dashBoardPage.NoPackagesLink.GetAttribute("href");
            dashboardLinks[3, 1] = dashBoardPage.NoPackagesLink.Text;

            dashboardLinks[4, 0] = dashBoardPage.NoBookValueLink.GetAttribute("href");
            dashboardLinks[4, 1] = dashBoardPage.NoBookValueLink.Text;

            dashboardLinks[5, 0] = dashBoardPage.NoCarFaxLink.GetAttribute("href");
            dashboardLinks[5, 1] = dashBoardPage.NoCarFaxLink.Text;

            dashboardLinks[6, 0] = dashBoardPage.LowPhotosLink.GetAttribute("href");
            dashboardLinks[6, 1] = dashBoardPage.LowPhotosLink.Text;

            dashboardLinks[7, 0] = dashBoardPage.EquipmentReviewLink.GetAttribute("href");
            dashboardLinks[7, 1] = dashBoardPage.EquipmentReviewLink.Text;

            dashboardLinks[8, 0] = dashBoardPage.NeedsRepricingLink.GetAttribute("href");
            dashboardLinks[8, 1] = dashBoardPage.NeedsRepricingLink.Text;

            dashboardLinks[9, 0] = dashBoardPage.NoTrimLink.GetAttribute("href");
            dashboardLinks[9, 1] = dashBoardPage.NoTrimLink.Text;

            dashboardLinks[10, 0] = dashBoardPage.NotOnlineLink.GetAttribute("href");
            dashboardLinks[10, 1] = dashBoardPage.NotOnlineLink.Text;

            dashboardLinks[11, 0] = dashBoardPage.LowOnLineActivityLink.GetAttribute("href");
            dashboardLinks[11, 1] = dashBoardPage.LowOnLineActivityLink.Text;

            for (int i = 0; i < 12; i++)
            {
                driver.Navigate().GoToUrl(dashboardLinks[i, 0]);
                String pattern = "\\(";
                string[] name = Regex.Split(dashboardLinks[i, 1], pattern);
                Thread.Sleep(1000);

                if (i < 10)
                    Assert.IsTrue(driver.FindElement(By.XPath(".//*[text()=\'" + name[0] + "\']")).Displayed);

                if (i == 10)
                    Assert.IsTrue(driver.FindElement(By.XPath(".//*[text()=\'Not Online Report']")).Displayed);

                if (i == 11)
                    Assert.IsTrue(driver.FindElement(By.XPath(".//*[text()='Low Activity Report']")).Displayed);
            }

        }

        [Test]
        [Category("SmokePagesAndLinks")]
        [Description("Quick check for release night to verify in one test each tab loads correctly in Admin Home")]
        public void Verify_All_Admin_Tabs_Load_C20257()
        {
            string dealer = "Windy City BMW";
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            WaitForAjax();

            dashBoardPage.GoTo_Admin_Misc_Tab();

            for (int i = 0; i < 6; i++)
            {
                if(i != 3)
                {
                    string tabLocation = ".//input[@class='Tab_Button_" + i + "']";
                    driver.FindElement(By.XPath(tabLocation)).Click();
                    WaitForAjax();

                    string tabLocation_ActiveWindow = ".//input[@class='Tab_Button_" + i + " ui-tab-active-input']";
                    Assert.IsTrue((driver.FindElement(By.XPath(tabLocation_ActiveWindow)).Displayed), "Failed at dealer {0}", dealer);
                }
            }
        }
        
        [Test]
        [Category("SmokePagesAndLinks")]
        [Description("Quick check for release night to verify in one test each page found in the Reports ddl loads correctly")]
        public void Test_Reports_Pages_Load_C20258()
        {
            const string dealer = "Windy City BMW";
            const int pages = 5;

            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            //Thread.Sleep(4000);

            string[] ReportsMenuUrlArray = GetUrlsFromMenu(pages, "ReportsMenu");  

            string[] reportPageTitle = {
                                      "Low Activity Report | MAX : Online Inventory. Perfected.",
                                      "Not Online Report | MAX : Online Inventory. Perfected.",
                                      "Performance Summary Report | MAX : Online Inventory. Perfected.",
                                      "Dashboard | MAX : Intelligent Online Advertising Systems",
                                      "Group Dashboard | MAX : Intelligent Online Advertising Systems"
                                  };

            for (int i = 0; i < pages; i++)
            {
                driver.Navigate().GoToUrl(ReportsMenuUrlArray[i]);
                Thread.Sleep(2000);
                Assert.That((driver.Title == reportPageTitle[i]), "Link failed: {0}, Dealer: {1}", ReportsMenuUrlArray[i], dealer);
            }

        }

        public string[] GetUrlsFromMenu(int pages, string menuName)  //num of links in menu, name of menu.  Returns links as an array
       {
           string[] reportPageUrl = new string[pages];

           for (int j = 1; j <= pages; j++)
           {
               reportPageUrl[j - 1] =
                   driver.FindElement(By.XPath("//*[@id=\""+ menuName + "\"]/ul/li[" + j + "]/a")).GetAttribute("href");
           }

           return reportPageUrl;

       }

        [Test]
        [Category("SmokePagesAndLinks")]
        [Description("Quick check for release night to verify in one test each page found in the Admin ddl loads correctly")]
        public void Test_Admin_Menu_Pages_Load_C20259()
        {
            string dealer = "Windy City BMW";
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.WaitForAjax();

            int pages = 5;

            string[] AdminMenuUrlArray = GetUrlsFromMenu(pages, "AdminMenu");

            string[] reportPageTitle = {
                                      "Admin | MAX : Online Inventory. Perfected.",
                                      "AutoLoadStatus",
                                      "Add Snippet | MAX Administration",
                                      "Template Manager | MAX : Online Inventory. Perfected.",
                                      "MAX Administration"
                                  };

            for (int i = 0; i < pages; i++)
            {
                driver.Navigate().GoToUrl(AdminMenuUrlArray[i]);
                WaitForAjax();
                Assert.That((driver.Title == reportPageTitle[i]), "Link failed: {0}, Dealer: {1}", AdminMenuUrlArray[i], dealer);             
                
            }
        }

        [Test] 
        [Category("SmokePagesAndLinks")]
        [Description("Quick check for release night to verify in one test each workflow page loads")]
        public void Workflow_Pages_Load_C20260()
        {
            string dealer = "Windy City BMW";
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer);

            inventoryPage.SelectVehicles(InventoryType.Used);
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.FirstTrimmedVehicleLink.Click();

            string[,] Workflow_Page_Id_Title = {
                                      {"LoadEquipmentLink","Load Equipment | MAX : Online Inventory. Perfected."},
                                      {"LoadPackagesLink","Load Packages | MAX : Online Inventory. Perfected."},
                                      {"LoadPhotosLink","Photo Manager | MAX : Online Inventory. Perfected."},
                                      {"PricingPageLink", "Pricing | MAX : Online Inventory. Perfected."},
                                      {"ApprovalLink","Ad Approval | MAX : Online Inventory. Perfected."},
                                      {"MAXSellingLink","MAX Merchandising"}
                                  };


            //{"PricingPageLink","PingOne | MAX : Online Inventory. Perfected."},
            var navMenuCount = Workflow_Page_Id_Title.Length / 2;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));

            for(int i = 0; i < navMenuCount; i++)
            {
                driver.FindElement(By.Id(Workflow_Page_Id_Title[i,0])).Click();
                wait.Until((d) => driver.Title.Length > 0);
                inventoryPage.WaitForAjax(); //wait for document complete
                // We think we need to use this in production.
                //if (i == 5)
                //{
                //    Thread.Sleep(5000);
                //    page.HandleJDPowerInterrupt();
                //}
                Assert.That((driver.Title == Workflow_Page_Id_Title[i, 1]), "Failed page: {0}, dealer: {1} ", Workflow_Page_Id_Title[i,0], dealer);
            }

        }

        [Test]
        [Category("SmokePagesAndLinks")]
        [Description("Quick check for release night to verify in one test Dealer Setup Wixard pages load")]
        public void Dealer_Setup_Wizard_C20261()
        {
            string dealer = "Windy City BMW";
            
            InitializePageAndLogin_Inventory_Direct(Default_user, Default_password, dealer);

            driver.Navigate().GoToUrl(BaseURL + "/merchandising/CustomizeMAX/DealerSetup.aspx");

            string[,] dealer_setup = {
                                         {"1. Data In/Out Setup", "lotProviderDDL"},
                                         {"2. Condition Descriptors", "NewExcellentConditionDescription"},
                                         {"3. Safety, Warranty, Fuel Economy", "crashCount"},
                                         {"4. Preferred Advertising Book", "PreferredBookCategory"},
                                         {"5. Key Information", "InfoAdDisplay"},
                                         {"6. Template Default", "defaultTemplateDDL"}
                                     };

            for(int j = 0; j < 6; j++)
            {
                driver.FindElement(By.XPath("//*[text()='" + dealer_setup[j,0] + "']")).Click();
                Thread.Sleep(3000);
                Assert.That((driver.FindElement(By.Id(dealer_setup[j, 1])).Displayed), "Failed for set: {0} at dealer {1}", dealer_setup[j, 0], dealer);
            }
        }

    }
}
