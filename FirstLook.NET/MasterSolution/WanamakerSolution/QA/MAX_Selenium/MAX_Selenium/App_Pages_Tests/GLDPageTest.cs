﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    class GLDPageTest : Test
    {

        [Test]
        public void GroupLevelDashboard_VerifyPage_C26885()
        {
            String dealer = GLDPage.RandomDealerChooser_GLD();
            InitializePageAndLogin_GLD_Direct("QABluemoon", "N@d@123", dealer);
            Thread.Sleep(2000);
            Assert.AreEqual("Group Dashboard | MAX : Intelligent Online Advertising Systems", driver.Title);
        }

        [Test]
        public void DaysToOnlineWithPhotos_GroupAvg_C17128()
        {
            InitializePageAndLogin_GLD_Direct("QABluemoon", "N@d@123", "Kenny Ross Chevy Buick Nissan");
            if (GldPage.DaysToOnlineWithPhotos_TTM_subtab.FindElement(By.XPath("./parent::li")).GetAttribute("aria-selected") != "true")
                GldPage.DaysToOnlineWithPhotos_TTM_subtab.Click();
            if (GldPage.GLD_InventoryType_DDL.SelectedOption.Text != "Used")
                GldPage.GLD_InventoryType_DDL.SelectByText("Used");           
            GldPage.WaitForAjax(20.0M);
            GldPage.GLD_FromDate_DDL.SelectByIndex(2);
            GldPage.GLD_ToDate_DDL.SelectByIndex(2);
            GldPage.WaitForAjax(20.0M);
            GldPage.WaitForDashboardServices();

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var arrLength = js.ExecuteScript(@"var arr = (dashboard.charts.TimeToMarket_DaysToOnlineWithPhotos.data().series[0].data); return arr.length;");
            int arrLengthInt = Convert.ToInt16(arrLength);
            double sum = 0;

            for (int i = 0; i < arrLengthInt; i++)
            {
                var avg = js.ExecuteScript(@"var arr = (dashboard.charts.TimeToMarket_DaysToOnlineWithPhotos.data().series[0].data[" + i + "].y); return arr;");
                double avgDouble = Convert.ToDouble(avg);
                sum = sum + avgDouble;
            }

            double groupAvg = sum / arrLengthInt;  //potential problem is the order of operations in the app could be different, rounding each dealer avg first and then finding the sum and group avg.
            String groupAvgFormatted = String.Format("{0:0.0}", groupAvg);
            //String groupAvgFormatted = groupAvg.ToString("###.#");

            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
           // wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='tab_ttm_g_owp']//*[local-name() ='svg']" +
           //                                                         "//*[local-name()='g' and @class='highcharts-extensions']//*[local-name()='text'][1]")));
            
            String groupAvgFromChart = GldPage.DaysToOnlineWithPhotos_GroupAvg.Text;
            String pattern = " ";  //this section removes "?" from the string
            string[] substrings = Regex.Split(groupAvgFromChart, pattern);
            String groupAvgFromChartFormatted = Convert.ToString(substrings[0]);

            Assert.That(groupAvgFromChartFormatted, Is.EqualTo(groupAvgFormatted),
                        "The Days To Online With Photos number on the graph is {0} but the calculated avg based on the full report is  {1} ", groupAvgFromChartFormatted, groupAvgFormatted);
        }

        [Test]
        public void DaysToCompleteAdOnline_GroupAvg_C17129()
        {
            InitializePageAndLogin_GLD_Direct("QABluemoon", "N@d@123", "Kenny Ross Chevy Buick Nissan");
            GldPage.DaysToCompleteAdOnline_TTM_subtab.Click();
            GldPage.GLD_FromDate_DDL.SelectByIndex(2);
            GldPage.GLD_ToDate_DDL.SelectByIndex(2);
            GldPage.WaitForAjax(20);
            GldPage.WaitForDashboardServices();

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var arrLength = js.ExecuteScript(@"var arr = (dashboard.charts.TimeToMarket_DaysToCompleteAdOnline.data().series[0].data); return arr.length;");
            int arrLengthInt = Convert.ToInt16(arrLength);
            double sum = 0;

            for (int i = 0; i < arrLengthInt; i++)
            {
                var avg = js.ExecuteScript(@"var arr = (dashboard.charts.TimeToMarket_DaysToCompleteAdOnline.data().series[0].data[" + i + "].y); return arr;");
                double avgdouble = Convert.ToDouble(avg);
                sum = sum + avgdouble;
            }

            double groupAvg = sum / arrLengthInt;  //potential problem is the order of operations in the app could be different, rounding each dealer avg first and then finding the sum and group avg.
            String groupAvgFormatted = String.Format("{0:0.0}", groupAvg);
            //String groupAvgFormatted = groupAvg.ToString("###.#");
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            //wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='tab_ttm_g_cao']//*[local-name() ='svg']" +
            //                                            "//*[local-name()='g' and @class='highcharts-extensions']//*[local-name()='text'][1]")));

            String groupAvgFromChart = GldPage.DaysToCompleteAdOnline_GroupAvg.Text;

            String pattern = " ";  //this section removes "?" from the string
            string[] substrings = Regex.Split(groupAvgFromChart, pattern);
            String groupAvgFromChartFormatted = Convert.ToString(substrings[0]);

            Assert.That(groupAvgFromChartFormatted, Is.EqualTo(groupAvgFormatted),
                        "The Days To Complete Ad Online number on the graph is {0} but the calculated avg based on the full report is  {1} ", groupAvgFromChartFormatted, groupAvgFormatted);
        }

        [Test]
        [Description("Calculates the group avg Impressions for AT and Cars from raw data and compares it to the chart.")]
        public void avgConsumerImpressions_singleMonth_C15005()
        {
            const string dealer = "Hendrick BMW";
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            InitializePageAndLogin_GLD_Direct("QABluemoon", "N@d@123", dealer);
            GldPage.Online_Classified_Performance_Tab.Click();
            GldPage.WaitForAjax(20.0M);
            GldPage.WaitForDashboardServices(180.0M); //Hendrick Group can take a long time

            String[] vendors = {"AutoTrader.com", "Cars.com"};

            foreach (var vendor in vendors)
            {
                var avgImpressionsGld = GldPage.Get_AvgConsumerImpressions(vendor); //returns Int32
                var arrayLength = js.ExecuteScript(@"return dashboard.reports.OnlineClassifiedPerformance.data()['" + vendor + "'].rows.length");
                var impressionsSum = js.ExecuteScript(@"var arr = dashboard.reports.OnlineClassifiedPerformance.data()
                                                        ['" + vendor + "'].rows; var sum = 0; for( var i =0; i < arr.length; ++i ){ sum += arr[i][2]; } return sum;");
                var len = Convert.ToDecimal(arrayLength);
                var sum = Convert.ToDecimal(impressionsSum);
                var avgImpressionsCalculatedSeed = sum / len;
                var avgImpressionsCalculated =
                    (int) Math.Round(avgImpressionsCalculatedSeed, 0, MidpointRounding.AwayFromZero); //cast as int for final comparison

                Assert.True(avgImpressionsGld == avgImpressionsCalculated, "Calculated average impressions {0} for {1}, did not match Dashboard {2}", avgImpressionsCalculated, vendor, avgImpressionsGld);

            }

        }


        [Test]
        [Description("Calculates the Avg Cost-Per Impressions for AutoTrader.com and Cars.com, and compares it to the values in the Dashboard.")]
        public void avgCostPerImpression_singleMonth_ATCars_C14655()
        {
            // Hendrick is the only group who currently supplies up-to-date Autotrader data, via direct feed.
            const string dealer = "Hendrick BMW";

            InitializePageAndLogin_GLD_Direct("QABluemoon", "N@d@123", dealer);  
            GldPage.GLD_FromDate_DDL.SelectByIndex(10);
            GldPage.GLD_ToDate_DDL.SelectByIndex(10);
            GldPage.WaitForAjax();
            GldPage.Online_Classified_Performance_Tab.Click();
            GldPage.WaitForAjax();
            GldPage.WaitForDashboardServices(180); //Hendrick GLD can take long time to load
            var atMonthlyCost = GldPage.GetAverageMonthlyCost("AutoTrader.com");
            var atImpressions = GldPage.Get_AvgConsumerImpressions("AutoTrader.com");
            var atCostPerSeed = (decimal) atMonthlyCost / atImpressions;
            var atGldCostPerSeed = GldPage.Get_Avg_CostPerImpression("AutoTrader.com");
            var atCalcAvgCostPerImpression = Math.Round(atCostPerSeed, 2, MidpointRounding.AwayFromZero);
            var atGldAvgCostPerImpression = Math.Round(atGldCostPerSeed, 2, MidpointRounding.AwayFromZero);

            var carsMonthlyCost = GldPage.GetAverageMonthlyCost("Cars.com");
            var carsImpressions = GldPage.Get_AvgConsumerImpressions("Cars.com");
            var carsCostPerSeed = (decimal) carsMonthlyCost / carsImpressions;
            var carsGldCostPerSeed = GldPage.Get_Avg_CostPerImpression("Cars.com");
            var carsCalcAvgCostPerImpression = Math.Round(carsCostPerSeed, 2, MidpointRounding.AwayFromZero);
            var carsGldAvgCostPerImpression = Math.Round(carsGldCostPerSeed, 2, MidpointRounding.AwayFromZero);

            Assert.True(atCalcAvgCostPerImpression == atGldAvgCostPerImpression, "Calculated AutoTrader average: {0}, not equal to GLD rendered value: {1}", atCalcAvgCostPerImpression, atGldAvgCostPerImpression);
            Assert.True(carsCalcAvgCostPerImpression == carsGldAvgCostPerImpression, "Calculated Cars.com average: {0}, not equal to GLD rendered value: {1}", carsCalcAvgCostPerImpression, carsGldAvgCostPerImpression);


        }

    }
}
