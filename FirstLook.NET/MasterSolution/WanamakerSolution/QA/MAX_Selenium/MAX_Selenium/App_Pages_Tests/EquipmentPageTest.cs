﻿using System;
using System.Data;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    internal class EquipmentPageTest : Test
    {

        private static object[] GMtestVehicleVins_Used = {
                                                               new [] { UsedGMVehicleVIN }
                                                           };

        [TestCaseSource("GMtestVehicleVins_Used")]
        public void Changes_To_Completed_Workflow_Checkboxes_C6105(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            string query =
               string.Format("SELECT Vin, postingStatus FROM merchandising.workflow.Inventory WHERE vin ='{0}'", vin);
            //Vin = '3G5DB03L96S987966'
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            equipmentPage = approvalPage.Get_Equipment_Page();
            
            equipmentPage.UnCheck_Equipment_Complete_CheckBox();
            var result1 = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr1 = result1.Rows[0];
            string ps1 = dr1.ItemArray[1].ToString();          

            equipmentPage.Check_Equipment_Complete_CheckBox();
            var result2 = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr2 = result1.Rows[0];
            string ps2 = dr2.ItemArray[1].ToString();
            Assert.AreEqual(ps1, ps2);
        }

       
        [TestCaseSource("GMtestVehicleVins_Used")]
        [Ignore]//no printer driver installed in build box,need to find out how to fix this.
        public void Print_Window_Sticker_C2360(string vin)
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.SelectVehicles(InventoryType.Used);
            approvalPage = inventoryPage.ClickOnFirstVehicle();
            equipmentPage = approvalPage.Get_Equipment_Page();
            equipmentPage.Print_Window_Sticker_Link.Click();
            equipmentPage.Check_PopUp_Print_Window_Sticker_Checkbox();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            equipmentPage.PopUp_Print_Button.Click();
            Thread.Sleep(4000);
            driver.SwitchTo().Window(driver.WindowHandles[1]);
            Assert.That(driver.Url.Contains("PrintPdf.aspx"), "The vin: {0} did not create a PrintPdf window properly", vin);
            if (driver.Title.Contains("No Pdf was found to process."))
            {
                throw new Exception("Wanamaker.WebApp unhandled exception encountered");
            }
        }
    }
}
