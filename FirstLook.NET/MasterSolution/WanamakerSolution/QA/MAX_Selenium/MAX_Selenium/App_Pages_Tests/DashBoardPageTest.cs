﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    internal class DashBoardPageTest : Test
    {

        [Test]
        [Description("Verifies the Dashboard Not Online link redirects to the correct report page.")]
        public void Not_Online_Link_DashBoard_C5831()
        {
            String dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.NotOnlineLink.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("Not Online Report | MAX : Online Inventory. Perfected."));
            //Thread.Sleep(2000);
            Assert.AreEqual("Not Online Report | MAX : Online Inventory. Perfected.", driver.Title, "Failed for dealer: {0} ", dealer);
        }

        [Test]
        [Description("Verify the link on DashBoard lands on the correct page and Name and Number match as well")]
        public void Low_Online_Activity_DashBoard_C6090()
        {
            String dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            var lowOnlineActivityCount = 0;
            var larTotalCars = 0;

            //don't waste time with settings if link in present
            try
            {
                lowOnlineActivityCount = Utility.ExtractDigitsFromString(dashBoardPage.LowOnLineActivityLink.Text);

            }
            catch (NoSuchElementException)
            {
                settings_maxsettings_alerts_page = dashBoardPage.GoTo_MAXSettings_Alerts_Tab();
                settings_maxsettings_alerts_page.Check_Low_Online_Activity_Checkbox();
                settings_maxsettings_alerts_page.GoTo_DashBoardPage();
                lowOnlineActivityCount = Utility.ExtractDigitsFromString(dashBoardPage.LowOnLineActivityLink.Text);

            }
            dashBoardPage.LowOnLineActivityLink.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Low Activity Report | MAX : Online Inventory. Perfected."));
            lowActivityReportPage = new LowActivityReportPage(driver);
            lowActivityReportPage.WaitForAjax(20.0M);
            larTotalCars = lowActivityReportPage.CountCars_LowActivityReport();

            Assert.AreEqual("Low Activity Report | MAX : Online Inventory. Perfected.", driver.Title, "Failed for dealer: {0} ", dealer);
            Assert.True(lowOnlineActivityCount == larTotalCars, "Dashboard " +lowOnlineActivityCount + " and Report " + larTotalCars + " counts didn't match.");
        }

        [Test]
        [Description("verify the text and value on DashBoard matches the Low Online Activity Filter")]
        public void Low_Online_Activity_C3454()
        {
            InitializePageAndLogin_Dashboard_URL();
            dashBoardPage.InventoryType_DDL.SelectByValue("Used");
            var LowOnlineActivityLink_Text_DashBoard = dashBoardPage.LowOnLineActivityLink.Text;
            dashBoardPage.NoPriceLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            inventoryPage.Merchandising_Tab.Click();
            var LowOnlineActivityTab_Text_InventoryPage = inventoryPage.LowActivity_Sub_Tab.Text;
            Assert.AreEqual(LowOnlineActivityLink_Text_DashBoard, LowOnlineActivityTab_Text_InventoryPage);
        }

        [Test]
        [Description("verify the link on DashBoard lands the correct page and Name and Number match as well")]
        public void No_Book_Value_DashBoard_C11139()
        {
            InitializePageAndLogin_Dashboard_URL();
            dashBoardPage.InventoryType_DDL.SelectByValue("Used"); //TestRail case is for Used Vehicles only
            var NoBookValueLink_Text_DashBoard = dashBoardPage.NoBookValueLink.Text;
            dashBoardPage.NoBookValueLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var NoBookValueTab_Text_InventoryPage = inventoryPage.NoBookValue_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Book Value"), Is.True);
            Assert.AreEqual(NoBookValueLink_Text_DashBoard, NoBookValueTab_Text_InventoryPage);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the link on DashBoard lands the correct page and Name and Number match as well")]
        public void No_Carfax_DashBoard_C11138()
        {
            InitializePageAndLogin_Dashboard_URL();
            dashBoardPage.InventoryType_DDL.SelectByValue("Used"); //TestRail case is for Used Vehicles only
            Thread.Sleep(2000);
            var NoCarFaxLink_Text_DashBoard = dashBoardPage.NoCarFaxLink.Text;
            dashBoardPage.NoCarFaxLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var NoCarFaxTab_Text_InventoryPage = inventoryPage.NoCarFax_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Carfax"), Is.True);
            //Has to use ToLower() since the Text/Name of the link is not CamelBack on DashBoard, Defect #22996
            Assert.AreEqual(NoCarFaxLink_Text_DashBoard.ToLower(), NoCarFaxTab_Text_InventoryPage.ToLower());
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the link on DashBoard lands the correct page and Name and Number match as well")]
        public void Big_MAX_Button_Link_DashBoard_C26879()
        {
            String dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            Thread.Sleep(4000);
            dashBoardPage.BigMaxButton.Click();
            Thread.Sleep(4000);
            Assert.AreEqual("Current Inventory Summary | MAX : Online Inventory. Perfected.", driver.Title, "Failed for dealer:  {0} ", dealer);
        }

        /* Test duplicates 20246
        [Test]
        public void Link_To_Not_Online_Report_DashBoard()
        {
            String dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.NotOnlineReportLink.Click();
            Thread.Sleep(2000);
            Assert.AreEqual("Not Online Report | MAX : Online Inventory. Perfected.", driver.Title, "Failed for dealer:  {0} ", dealer);
        }*/

        [Test]
        [Category("SmokeDashBoard")]
        public void Graph_Link_To_Not_Online_Report_DashBoard_C5829()
        {
            String dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.NotOnlineGraph.Click();
            Thread.Sleep(2000);
            Assert.AreEqual("Not Online Report | MAX : Online Inventory. Perfected.", driver.Title, "Failed for dealer:  {0} ", dealer);
        }

        /*[Test]
        public void Link_To_Low_Activity_Report_DashBoard()
        {
            InitializePageAndLogin_DashBoardPage();
            Wait_For_Dashboard_Loading();
            dashBoardPage.LowActivityReportLink.Click();
            Thread.Sleep(2000);
            Assert.AreEqual("Low Activity Report | MAX : Online Inventory. Perfected.", driver.Title);
        }*/

        /*[Test]
        public void Graph_Link_To_Low_Activity_Report_DashBoard_C5828()
        {
            InitializePageAndLogin_DashBoardPage();
            Wait_For_Dashboard_Loading();
            dashBoardPage.LowOnlineActivityGraph.Click();
            Thread.Sleep(2000);
            Assert.AreEqual("Low Activity Report | MAX : Online Inventory. Perfected.", driver.Title);
        }*/

        [Test]
        public void Link_To_Performance_Summary_Report_DashBoard_C5830()
        {
            String dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.GoTo_Admin_Misc_Tab();
            dashBoardPage.Enable_OnlineClassifiedOverview();
            //dashBoardPage.Reports_Tab.Click();
            //dashBoardPage.Executive_Dashboard_InDDL.Click();
            inventoryPage = new InventoryPage(driver);
            inventoryPage.HomeLink.Click();
            Thread.Sleep(2000);
            dashBoardPage.Reports_Tab.Click();
            IWebElement perfSummReportLink = driver.FindElement(By.LinkText("Performance Summary"));
            perfSummReportLink.Click();
            Thread.Sleep(2000);
            Assert.AreEqual("Performance Summary Report | MAX : Online Inventory. Perfected.", driver.Title, "Failed for dealer:  {0} ", dealer);
        }
        
        [Test]
        [ExpectedException(typeof(NoSuchElementException))]
        [Description("Verify disabling OCO will also disable the Performance Summary Report link in the Reports menu")]
        public void Link_To_Performance_Summary_Report_DashBoard_Disabled_15739()
        {
            String dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            admin_Miscellaneous_Page = dashBoardPage.GoTo_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.DisableOnlineClassifiedOverview();
            admin_Miscellaneous_Page.BackToMaxAdLink.Click();
            dashBoardPage.Reports_Tab.Click();
            Assert.Throws<NoSuchElementException>(driver.FindElement(By.LinkText("Performance Summary")).Click, "Performance Summary Report link detected, for {0}", dealer);
        }

        [Test]
        public void Link_To_Vehicles_Marked_Offline_DashBoard_C26884()
        {
            InitializePageAndLogin_Dashboard_URL();
            dashBoardPage.VehiclesMarkedOffline.Click();
            Thread.Sleep(2000);
            InventoryPage inventoryPage = new InventoryPage(driver);
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("Offline"), Is.True);
        }

        [Test]
        [Description("verify the right side graph(No Photos or No Price) count matches the count in Online Alerts")]
        public void Merchandising_Alert_Gauge_Count_C7322()
        {
            InitializePageAndLogin_Dashboard_URL();
            var graphCount = Utility.ExtractDigitsFromString(dashBoardPage.Merchandising_Alert_Gauge_Count.Text);
            String FilterOnGraph = dashBoardPage.Merchandising_Alert_Gauge.Text;

            var linkCount = 0;
            if(FilterOnGraph == "NO PHOTOS")
            {
                linkCount = Utility.ExtractDigitsFromString(dashBoardPage.NoPhotosLink.Text);
            }

            else if(FilterOnGraph == "NO PRICE")
            {
                linkCount = Utility.ExtractDigitsFromString(dashBoardPage.NoPriceLink.Text);
            }

            Assert.That(graphCount, Is.EqualTo(linkCount),
             "The Merchandising Alert Graph is displaying the {0} filter with a count of {1}, but the Online Alert count is {2}", FilterOnGraph, graphCount, linkCount);
        }

        [Test]
        [Description("verify the link on DashBoard lands the correct page and Name and Number match as well")]
        public void No_Price_Link_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            //var number = Utility.GetDigitsFromElement(dashBoardPage.NoPriceLink);
            var NoPriceLink_Text_DashBoard = dashBoardPage.NoPriceLink.Text;
            dashBoardPage.NoPriceLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var NoPriceTab_Text_InventoryPage = inventoryPage.NoPrice_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Price"), Is.True);
            Assert.That(NoPriceTab_Text_InventoryPage, Is.EqualTo(NoPriceLink_Text_DashBoard),
                        "The No Price Label {0} does not match the Dashboard link {1}.", NoPriceTab_Text_InventoryPage,
                        NoPriceLink_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the link on DashBoard lands the correct page and Name and Number match as well")]
        public void No_Description_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            dashBoardPage.InventoryType_DDL.SelectByValue("Both");  //selects Used&New vehicles
            //var number = Utility.GetDigitsFromElement(dashBoardPage.NoPriceLink);
            Thread.Sleep(4000);
            var NoDescriptionLink_Text_DashBoard = dashBoardPage.NoDescriptionLink.Text;
            dashBoardPage.NoDescriptionLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var NoDescriptionTab_Text_InventoryPage = inventoryPage.NoDescription_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Description"), Is.True);
            Assert.That(NoDescriptionTab_Text_InventoryPage, Is.EqualTo(NoDescriptionLink_Text_DashBoard),
                        "The No Description Label {0} does not match the Dashboard link {1}.",
                        NoDescriptionTab_Text_InventoryPage, NoDescriptionLink_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector(); 
        }

        [Test]
        [Description("verify the link on DashBoard lands the correct page and Name and Number match as well")]
        public void No_Photos_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            //var number = Utility.GetDigitsFromElement(dashBoardPage.NoPriceLink);
            var NoPhotosLink_Text_DashBoard = dashBoardPage.NoPhotosLink.Text;
            dashBoardPage.NoPhotosLink.Click();
            Thread.Sleep(4000);
            inventoryPage = new InventoryPage(driver);
            var NoPhotosTab_Text_InventoryPage = inventoryPage.NoPhotos_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Photos"), Is.True);
            Assert.That(NoPhotosTab_Text_InventoryPage, Is.EqualTo(NoPhotosLink_Text_DashBoard),
                        "The No Photos Label {0} does not match the Dashboard link {1}.", NoPhotosTab_Text_InventoryPage,
                        NoPhotosLink_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description(
            "verify the Equipment Review link on DashBoard lands on the Equipment Review Sub Tab, and Name and Number match as well"
            )]
        public void Equipment_Review_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            Thread.Sleep(2000);
            var equipment_review_text_dashBoard = dashBoardPage.EquipmentReviewLink.Text;
            dashBoardPage.EquipmentReviewLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var equipment_review_label = inventoryPage.EquipmentReview_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("Equipment Review"), Is.True);
            Assert.That(equipment_review_label, Is.EqualTo(equipment_review_text_dashBoard),
                        "The Equipment Review Label {0} does not match the Dashboard link {1}.", equipment_review_label,
                        equipment_review_text_dashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description(
            "verify the No Trim link on DashBoard lands on the No Trim Sub Tab, and Name and Number match as well")]
        public void No_Trim_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            var noTrim_Text_DashBoard = dashBoardPage.NoTrimLink.Text;
            dashBoardPage.NoTrimLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var no_trim_label = inventoryPage.NoTrim_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Trim"), Is.True);
            Assert.That(no_trim_label, Is.EqualTo(noTrim_Text_DashBoard),
                        "The No Trim Label {0} does not match the Dashboard link {1}.", no_trim_label,
                        noTrim_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the Needs Re-pricing link on DashBoard lands on the Needs Re-pricing Sub Tab, and Name and Number match as well")]
        public void Needs_Repricing_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            var needs_repricing_Text_DashBoard = dashBoardPage.NeedsRepricingLink.Text;
            dashBoardPage.NeedsRepricingLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var needs_repricing_label = inventoryPage.NeedsRePricing_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("Needs Re-pricing"), Is.True);
            Assert.That(needs_repricing_label, Is.EqualTo(needs_repricing_Text_DashBoard),
                        "The Needs Repricing Label {0} does not match the Dashboard link {1}.", needs_repricing_label,
                        needs_repricing_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the No Packages link on DashBoard lands on the No Packages Sub Tab, and Name and Number match as well")]
        public void No_Packages_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            var NoPackagesLink_Text_DashBoard = dashBoardPage.NoPackagesLink.Text;
            dashBoardPage.NoPackagesLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var NoPackageTab_Text_InventoryPage = inventoryPage.NoPackages_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Packages"), Is.True);
            Assert.That(NoPackageTab_Text_InventoryPage, Is.EqualTo(NoPackagesLink_Text_DashBoard),
                        "The No Packages Label {0} does not match the Dashboard link {1}.",
                        NoPackageTab_Text_InventoryPage, NoPackagesLink_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the Low Photos link on DashBoard on lands the Low Photos Sub Tab, and Name and Number match as well")]
        public void Low_Photos_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            var LowPhotos_Text_DashBoard = dashBoardPage.LowPhotosLink.Text;
            dashBoardPage.LowPhotosLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var LowPhotosTab_Text_InventoryPage = inventoryPage.LowPhotos_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("Low Photos"), Is.True);
            Assert.That(LowPhotosTab_Text_InventoryPage, Is.EqualTo(LowPhotos_Text_DashBoard),
                        "The Low Photos Label {0} does not match the Dashboard link {1}.",
                        LowPhotosTab_Text_InventoryPage, LowPhotos_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the No Book Value link on DashBoard lands on the No Book Value Sub Tab, and Name and Number match as well")]
        public void No_Book_Value_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            var NoBookValueLink_Text_DashBoard = dashBoardPage.NoBookValueLink.Text;
            dashBoardPage.NoBookValueLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var NoBookValueTab_Text_InventoryPage = inventoryPage.NoBookValue_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Book Value"), Is.True);
            Assert.That(NoBookValueTab_Text_InventoryPage, Is.EqualTo(NoBookValueLink_Text_DashBoard),
                        "The No Book Value Label {0} does not match the Dashboard link {1}.",
                        NoBookValueTab_Text_InventoryPage, NoBookValueLink_Text_DashBoard);
            Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
        }

        [Test]
        [Description("verify the No Carfax link on DashBoard lands the No Carfax Sub Tab, and Name and Number match as well")]
        public void No_Carfax_DashBoard_C6090()
        {
            InitializePageAndLogin_Dashboard_URL();
            var NoCarFaxLink_Text_DashBoard = dashBoardPage.NoCarFaxLink.Text;
            var CarFaxNum = Utility.GetDigitsFromElement(dashBoardPage.NoCarFaxLink);
            dashBoardPage.NoCarFaxLink.Click();
            Thread.Sleep(2000);
            inventoryPage = new InventoryPage(driver);
            var NoCarFaxTab_Text_InventoryPage = inventoryPage.NoCarFax_Sub_Tab.Text;
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Carfax"), Is.True);
            //Has to use ToLower() since the Text/Name of the link is not CamelBack on DashBoard, Defect #22996
            Assert.That(NoCarFaxTab_Text_InventoryPage.ToLower(), Is.EqualTo(NoCarFaxLink_Text_DashBoard.ToLower()),
                        "The No Carfax Label {0} does not match the Dashboard link {1}.", NoCarFaxTab_Text_InventoryPage,
                        NoCarFaxLink_Text_DashBoard);
            if (CarFaxNum > 0)
            {
                Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector();
            }
        }

        //In the current filter on the Inventory page, this will grab the total count and compare it to the workflow total count
        public void Compare_Total_Vehicles_On_InventoryPage_And_WorkFlowSelector()
        {
            if (inventoryPage.Is_TotalItemCount_Present())
            {
                int TotalCount_From_InventoryPage = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
                driver.FindElement(By.XPath("//*[@id=\"BtnInventoryItem\"]")).Click();
                Thread.Sleep(12000);
                int TotalCount_From_ApprovalPage =
                    ApprovalPage.GetTotalCountFromWokflowWebElement(driver.FindElement(By.Id("lblCurrentItem")));
                Assert.That(TotalCount_From_InventoryPage, Is.EqualTo(TotalCount_From_ApprovalPage),
                            "This filter on the Inventory page has {0} vehciles, does not match the Approval page with {1} vehicles.",
                            TotalCount_From_InventoryPage, TotalCount_From_ApprovalPage);
            }
        }

        [Test]
        public void Test_Alerts_Settings_Check_All_Checkbox_Method_C26891()
        {
            InitializePageAndLogin_MAX_Settings_Page("qashiner", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_alerts_page = settings_maxsettings_page.Get_Alerts_Page();
            settings_maxsettings_alerts_page.Check_All_CheckBox_Alerts_SettingPage();
            Assert.That(settings_maxsettings_alerts_page.Is_Not_Online_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_No_Price_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_No_Photos_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_No_Description_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_No_Trim_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_Need_RePricing_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_Need_Equipment_Review_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_Low_Online_Activity_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_Low_Photos_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_No_Packages_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_No_BooksValue_Checkbox_Checked() == true);
            Assert.That(settings_maxsettings_alerts_page.Is_No_Carfax_Checkbox_Checked() == true);
        }


        [Test] // This test may fail depending on Highcharts dynamically adding <tspan> elements or not.. Needs rework.
        public void Calculate_Cost_Per_VDP_C10090()
        {
            InitializePageAndLogin_Dashboard_URL();


            dashBoardPage.setFromMonthByIndexInDDL(12);
            dashBoardPage.setToMonthByIndexInDDL(10);
            dashBoardPage.Online_Classified_Performance_tab.Click();

            Wait_For_Dashboard_Loading();

            String AT = "AutoTrader.com";
            String CARS = "Cars.com";

            decimal AT_Monthly_Cost = dashBoardPage.Get_Monthly_Cost(AT);
            decimal CARS_Monthly_Cost = dashBoardPage.Get_Monthly_Cost(CARS);

            decimal AT_Impressions = dashBoardPage.Get_Num_Of_Impressions(AT);
            decimal CARS_Impressions = dashBoardPage.Get_Num_Of_Impressions(CARS);

            decimal Graph_AT_Cost_Per_Impression = dashBoardPage.Get_Avg_Cost_Per_Impression(AT);
            decimal Graph_CARS_Cost_Per_Impression = dashBoardPage.Get_Avg_Cost_Per_Impression(CARS);

            decimal temp1 = AT_Monthly_Cost / AT_Impressions;
            decimal AT_Ratio = Math.Round(temp1, 2);

            decimal temp2 = CARS_Monthly_Cost / CARS_Impressions;
            decimal CARS_Ratio = Math.Round(temp2, 2);

            Assert.That(Graph_AT_Cost_Per_Impression, Is.EqualTo(AT_Ratio),
                        "The Cost Per Impression calculation on the Dashboard for AT, {0}, does not match the manual calculation of {1}.", Graph_AT_Cost_Per_Impression, AT_Ratio);
            Assert.That(Graph_CARS_Cost_Per_Impression, Is.EqualTo(CARS_Ratio),
                        "The Cost Per Impression calculation on the Dashboard for CARS, {0}, does not match the manual calculation of {1}.", Graph_CARS_Cost_Per_Impression, CARS_Ratio);
        }

        /*
[Test]
public  void Compare_Detail_Page_Views_InReport_To_VDP_Impression_InDashboard_C9797()
{
    Login_Windy_City_BMW_AsAdmin_Then_Goto_DashBoard_Page();

    dashBoardPage.Set_From_Month_ToJanuary();
    dashBoardPage.Set_To_Month_ToMay();
    Wait_For_Dashboard_Loading();
    //string To_Month = dashBoardPage.Get_Default_To_Month();
            
    //Get the VDP on the center graph of Online Performance Analytics on DashBoard
    int AT_VDP_DashBoard = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("div#highcharts-4.highcharts-container> svg > g.highcharts-data-labels > g:nth-child(1) > text > tspan")).Text);
    int CARS_VDP_DashBoard = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("div#highcharts-4.highcharts-container> svg > g.highcharts-data-labels > g:nth-child(2) > text > tspan")).Text);

    //set the months in Performance Report DropDown to be between Jan to Apr 
    Set_PerformanceReport_Months_SameAS_Dashboard();
            
    //wait for the page gets loaded
    //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(8));
    Thread.Sleep(5000);

    int AT_DPV_Performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(4) > td:nth-child(4) > div")).Text);
    int CARS_DPV_performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(3) > td:nth-child(4) > div")).Text);

    Assert.That(Math.Abs(AT_VDP_DashBoard*4 - AT_DPV_Performance_Report) <= 1);
    Assert.That(Math.Abs(CARS_VDP_DashBoard*4 - CARS_DPV_performance_Report) <= 1);
}

[Test]
public void Compare_Conversion_Rate_InReport_To_Conversion_Rate_InDashboard_C9799()
{
    Login_Windy_City_BMW_AsAdmin_Then_Goto_DashBoard_Page();

    dashBoardPage.Set_From_Month_ToJanuary();
    dashBoardPage.Set_To_Month_ToMay();
    Wait_For_Dashboard_Loading();

    dashBoardPage.Toggle_On_DashBoard.Click();
           
    //string Current_Month = dashBoardPage.GetCurrentMonth_InMonthDDL();
            
    //Get the Conversion Rate on OnLine Classified Overview page on DashBoard
    int AT_Con_Rate_DashBoard =
        Utility.ExtractDigitsFromString(
            driver.FindElement(By.XPath(".//*[@id='chart_stats']/div[4]/div[2]/span[1]")).Text);
    int CARS_Con_Rate_DashBoard =
        Utility.ExtractDigitsFromString(
            driver.FindElement(By.XPath(".//*[@id='chart_stats']/div[4]/div[2]/span[2]")).Text);

    //set the months in Performance Report DropDown to be between Jan to Apr 
    Set_PerformanceReport_Months_SameAS_Dashboard();

    //wait for the page gets loaded
    //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(8));
    Thread.Sleep(5000);

    int AT_Con_Rate_Performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(4) > td:nth-child(10) > div")).Text);
    int CARS_Con_Rate_performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(3) > td:nth-child(10) > div")).Text);

    Assert.That(AT_Con_Rate_DashBoard.Equals(AT_Con_Rate_Performance_Report));
    Assert.That(CARS_Con_Rate_DashBoard.Equals(CARS_Con_Rate_performance_Report));
}

[Test]
public void Compare_Click_Thru_Rate_InReport_To_CTR_InDashboard_C9798()
{
    Login_Windy_City_BMW_AsAdmin_Then_Goto_DashBoard_Page();

    dashBoardPage.Set_From_Month_ToJanuary();
    dashBoardPage.Set_To_Month_ToMay();
    Wait_For_Dashboard_Loading();

    dashBoardPage.Toggle_On_DashBoard.Click();
            
    //string Current_Month = dashBoardPage.GetCurrentMonth_InMonthDDL();
            
    //Get the Click Through Rate on OnLine Classified Overview page on DashBoard
    int AT_CTR_DashBoard =
        Utility.ExtractDigitsFromString(
            driver.FindElement(By.XPath(".//*[@id='chart_stats']/div[4]/div[1]/span[1]")).Text);
    int CARS_CTR_DashBoard =
        Utility.ExtractDigitsFromString(
            driver.FindElement(By.XPath(".//*[@id='chart_stats']/div[4]/div[1]/span[2]")).Text);

    //set the months in Performance Report DropDown to be between Jan to Apr 
    Set_PerformanceReport_Months_SameAS_Dashboard();

    //wait for the page gets loaded
    //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(8));
    Thread.Sleep(5000);

    int AT_CTR_Performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(4) > td:nth-child(9) > div")).Text);
    int CARS_CTR_performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(3) > td:nth-child(9) > div")).Text);

    Assert.That(AT_CTR_DashBoard.Equals(AT_CTR_Performance_Report));
    Assert.That(CARS_CTR_DashBoard.Equals(CARS_CTR_performance_Report));
}

[Test]
public void Compare_Total_Actions_InReport_To_Actions_InDashboard_C9796()
{
    Login_Windy_City_BMW_AsAdmin_Then_Goto_DashBoard_Page();

    dashBoardPage.Set_From_Month_ToJanuary();
    dashBoardPage.Set_To_Month_ToMay();
    Wait_For_Dashboard_Loading();
    //string To_Month = dashBoardPage.Get_Default_To_Month();

    //Get the VDP on the center graph of Online Performance Analytics on DashBoard
    int AT_AVG_TOTAL_LEADS_DashBoard = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("div#highcharts-8.highcharts-container> svg > g.highcharts-data-labels > g:nth-child(1) > text > tspan")).Text);
    int CARS_AVG_TOTAL_LEADS_DashBoard = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("div#highcharts-8.highcharts-container> svg > g.highcharts-data-labels > g:nth-child(2) > text > tspan")).Text);

    //set the months in Performance Report DropDown to be between Jan to Apr 
    Set_PerformanceReport_Months_SameAS_Dashboard();

    //wait for the page gets loaded
    //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(8));
    Thread.Sleep(5000);

    int AT_Total_Actions_Performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(4) > td:nth-child(5) > div")).Text);
    int CARS_Total_Actions_performance_Report = Utility.ExtractDigitsFromString(driver.FindElement(By.CssSelector("#ReportDiv table table table table table[cols] > tbody > tr:nth-child(3) > td:nth-child(5) > div")).Text);

    Assert.That(Math.Abs(AT_AVG_TOTAL_LEADS_DashBoard * 4 - AT_Total_Actions_Performance_Report) <= 1);
    Assert.That(Math.Abs(CARS_AVG_TOTAL_LEADS_DashBoard * 4 - CARS_Total_Actions_performance_Report) <= 1);
}
 */

        public void Login_Windy_City_BMW_AsAdmin_Then_Goto_DashBoard_Page()
        {
            LoginAsAdmin_InventoryPage("QABlueMoon", "N@d@123", "Windy City BMW");
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.Check_Online_Performance_Analytics();
            admin_Miscellaneous_Page.Save_Button.Click();
            inventoryPage = admin_Home_Page.GoBackTo_Inventory_Page();
            dashBoardPage = inventoryPage.GoTo_DashBoardPage();
        }

        public void Set_PerformanceReport_Months_SameAS_Dashboard()
        {
            //Got to the Performance Report Page
            performanceSummaryReportPage = dashBoardPage.Goto_Performance_Report();

            //Set the month in Report using the month from DashBoard
            //Month ToMonth = dashBoardPage.Convert_Month_From_DigitString_To_Name(To_Month);

            //Dashboard is set to FROM January in test code
            performanceSummaryReportPage.SetFromMonth(Month.January);
            performanceSummaryReportPage.SetToMonth(Month.April);
            //Set the same month as Dashboard
            //performanceSummaryReportPage.SetToMonth(ToMonth);

            performanceSummaryReportPage.UpdateButton.Click();
        }

        public void Wait_For_Dashboard_Loading()
        {
            Thread.Sleep(5000);
        }

        [Test]
        [Description("verify Max Settings selects No Price and the Dasboard displays it on the right-side graph")]
        public void Merchandising_Alert_Gauge_Displays_No_Price_C15376()
        {
            InitializePageAndLogin_MAX_Settings_Page("qashiner", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_reports_page = settings_maxsettings_page.Get_Reports_Page();
            settings_maxsettings_reports_page.Select_No_Price_Radio_Button();
            Assert.That(settings_maxsettings_reports_page.Is_No_Price_Radio_Button_Selected() == true);
            
            settings_maxsettings_reports_page.Reports_Tap_Tab.Click();  
            settings_maxsettings_reports_page.ExecutiveDashboardLink.Click();
            DashBoardPage MerchandisingAlertGauge = new DashBoardPage(driver);
            var MerchandisingAlertGaugeText = MerchandisingAlertGauge.Merchandising_Alert_Gauge.Text;
            Assert.That("NO PRICE", Is.EqualTo(MerchandisingAlertGaugeText),
             "The Merchandising Alert Graph on the Dashboard should display No Price, but instead is displaying: {0}", MerchandisingAlertGaugeText);
        }

        [Test]
        [Description("verify Max Settings selects No Photos and the Dasboard displays it on the right-side graph")]
        public void Merchandising_Alert_Gauge_Displays_No_Photos_C15375()
        {
            InitializePageAndLogin_MAX_Settings_Page("qashiner", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_reports_page = settings_maxsettings_page.Get_Reports_Page();
            settings_maxsettings_reports_page.Select_No_Photo_Radio_Button();
            Assert.That(settings_maxsettings_reports_page.Is_No_Photo_Radio_Button_Selected() == true);

            settings_maxsettings_reports_page.Reports_Tap_Tab.Click();
            settings_maxsettings_reports_page.ExecutiveDashboardLink.Click();
            DashBoardPage MerchandisingAlertGauge = new DashBoardPage(driver);
            var MerchandisingAlertGaugeText = MerchandisingAlertGauge.Merchandising_Alert_Gauge.Text;
            Assert.That("NO PHOTOS", Is.EqualTo(MerchandisingAlertGaugeText),
             "The Merchandising Alert Graph on the Dashboard should display No Photos, but instead is displaying: {0}", MerchandisingAlertGaugeText);    
        }

        [Test] 
        public void Compare_Monthly_Cost_DB_And_Dashboard_For_Cars_C14657()
        {
            InitializePageAndLogin_Dashboard_URL();

            String date_dashboard = "Dec 2014";  //date format: Mmm yyyy
            String date_db = "2014-12-01";       //date format: yyyy-mm-dd
            int bu = 105080;
            String vendor = "Cars.com";                      

            dashBoardPage = new DashBoardPage(driver);
            dashBoardPage.Set_From_Month(date_dashboard);
            dashBoardPage.Set_To_Month(date_dashboard);
            dashBoardPage.Online_Classified_Performance_tab.Click();

            int db_monthly_cost = dashBoardPage.Get_Monthly_Cost_From_DB(date_db, bu, vendor);

            int dashboard_monthly_cost = dashBoardPage.Get_Monthly_Cost(vendor);

            Assert.That(dashboard_monthly_cost, Is.EqualTo(db_monthly_cost),
             "The monthly cost for {0} at {1} on the Dashboard is:  {2}, and the monthly cost in the database is:  {3}", vendor, bu, dashboard_monthly_cost, db_monthly_cost);
        }

        [Test]
        public void Compare_Monthly_Cost_DB_And_Dashboard_For_AutoTrader_C14656()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Hendrick BMW");

            String date_dashboard = "May 2015";  //date format: Mmm yyyy
            String date_db = "2015-05-01";       //date format: yyyy-mm-dd
            const int bu = 100148;
            String vendor = "AutoTrader.com";

            dashBoardPage = new DashBoardPage(driver);
            dashBoardPage.Set_From_Month(date_dashboard);
            dashBoardPage.Set_To_Month(date_dashboard);
            dashBoardPage.Online_Classified_Performance_tab.Click();
            
            int db_monthly_cost = 0;
            db_monthly_cost = dashBoardPage.Get_Monthly_Cost_From_DB(date_db, bu, vendor);
            
            int dashboard_monthly_cost = 0;
            dashboard_monthly_cost = dashBoardPage.Get_Monthly_Cost(vendor);
            
            Assert.That(dashboard_monthly_cost, Is.EqualTo(db_monthly_cost),
             "The monthly cost for {0} at {1} on the Dashboard is:  {2}, and the monthly cost in the database is:  {3}", vendor, bu, dashboard_monthly_cost, db_monthly_cost);
        }

        [Test] // This test may fail depending on Highcharts dynamically adding <tspan> elements or not.. Needs rework.
        public void Compare_OnlineClassifiedPerformance_And_ClassifiedTrends_Impressions_C17126()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Hendrick BMW");

            String date = "May 2015";
            dashBoardPage.Set_From_Month(date);
            dashBoardPage.Set_To_Month(date);
            dashBoardPage.Online_Classified_Performance_tab.Click();
            Wait_For_Dashboard_Loading();
            String AT = "AutoTrader.com";
            String CARS = "Cars.com";
            int AT_Impressions_OCP = dashBoardPage.Get_Num_Of_Impressions(AT);
            int CARS_Impressions_OCP = dashBoardPage.Get_Num_Of_Impressions(CARS);

            dashBoardPage.Classified_Trends_tab.Click();
            dashBoardPage.vdp_trend_details.Click();

            int AT_Impressions_TrendsReport = dashBoardPage.Get_Impressions_From_Trends_Report(AT);
            int CARS_Impressions_TrendsReport = dashBoardPage.Get_Impressions_From_Trends_Report(CARS);

            Assert.That(AT_Impressions_OCP, Is.EqualTo(AT_Impressions_TrendsReport),
             "The number of Impressions on the Dashboard for {0} on the OCP tab is {1} and in the Trends Report is {2}", AT, AT_Impressions_OCP, AT_Impressions_TrendsReport);
            Assert.That(CARS_Impressions_OCP, Is.EqualTo(CARS_Impressions_TrendsReport),
             "The number of Impressions on the Dashboard for {0} on the OCP tab is {1} and in the Trends Report is {2}", CARS, CARS_Impressions_OCP, CARS_Impressions_TrendsReport);
        }

        /* //This test can probably be eliminated. Ensuring View-Details-link-works is duplicated in C14644
        [Test]
        public void TimeToMarket_View_Details_Link_C14643()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            dashBoardPage.CloseNaggingWindow();
            dashBoardPage.Select_Inventory_Type("Used");
            Thread.Sleep(3000);
            dashBoardPage.setFromMonthByIndexInDDL(7);
            dashBoardPage.setToMonthByIndexInDDL(7);
            //assumes TTM is configured in Admin/Miscellaneous tab
            dashBoardPage.Time_to_Market_tab.Click();
            dashBoardPage.TTM_ViewDetails_Link.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            //IWebElement selectElement = wait.Until(ExpectedConditions
                 // .ElementIsVisible(By.CssSelector("div.ui-dialog span.ui-dialog-title")));
            
            var popupTitle = driver.FindElement(By.Id("ui-id-14")).Text;
            StringAssert.IsMatch("Time To Market By Vehicle", popupTitle);
        }
         */

        [Test]
        [Description("Makes sure TTM details-report links to the correct car by comparing stock numbers")]
        public void TimeToMarket_Vehicle_Link_C14644()
        {
            String stockNumber;
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Hendrick BMW");
            admin_Miscellaneous_Page = inventoryPage.GoTo_Admin_Miscellaneous_Page();
            Enable_TimeToMarket();
            inventoryPage = admin_Miscellaneous_Page.GoBackTo_Inventory_Page();
            dashBoardPage = inventoryPage.GoTo_DashBoardPage();        
            dashBoardPage.InventoryType_DDL.SelectByValue("Used");
            Thread.Sleep(3000);
            dashBoardPage.setFromMonthByIndexInDDL(1);
            dashBoardPage.setToMonthByIndexInDDL(1);
            //assumes TTM is configured in Admin/Miscellaneous tab
            dashBoardPage.Time_to_Market_tab.Click();
            dashBoardPage.TTM_ViewDetails_Link.Click();
            IWebElement TR = dashBoardPage.TTM_First_Clickable_Car_Link.FindElement(By.XPath("./ancestor::tr"));
            IWebElement stockNumberElement = TR.FindElement(By.XPath("td[7]"));
            stockNumber = stockNumberElement.Text;
            dashBoardPage.TTM_First_Clickable_Car_Link.SendKeys(Keys.Return);
            approvalPage = new ApprovalPage(driver);
            String approvalPageStockNumber = driver.FindElement(By.CssSelector("div.inventoryData span#stockNumber")).Text;
            StringAssert.IsMatch(stockNumber, approvalPageStockNumber);
        }

        [Test]
        [Description("Calculates the average of the Days To Online With Photos column in the TTM report and compares it to the TTM graph")]
        public void AverageDaysToOnlineWithPhoto_C16118()
        {
            InitializePageAndLogin_Dashboard_URL();
            dashBoardPage.InventoryType_DDL.SelectByValue("Used");
            Thread.Sleep(2000);
            dashBoardPage.setFromMonthByIndexInDDL(7);
            dashBoardPage.setToMonthByIndexInDDL(7);
            dashBoardPage.WaitForDashboardServices();
            String daysAvgGraph = dashBoardPage.AvgDaysToOnlineWithPhoto_Graph.Text;
            dashBoardPage.TTM_ViewDetails_Link.Click();
            Thread.Sleep(3000);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            //js finds the sum of the Days To Online With Photos column and divides that by the total number of rows(array length)
            var daysAvg = js.ExecuteScript(@" var arr = dashboard.data.reports.TimeToMarket().rows; var sum = 0; for( var i =0; i < arr.length; ++i ){ sum += arr[i][3]; } return sum/arr.length;");
            double daysAvgDouble = (double)daysAvg;
            String daysAvgDoubleFormatted = daysAvgDouble.ToString("###.#");

            Assert.That(daysAvgGraph, Is.EqualTo(daysAvgDoubleFormatted),
             "The Days To Online With Photos number on the graph is {0} but the calculated avg based on the report is  {1} ", daysAvgGraph, daysAvgDoubleFormatted);

        }

        [Test] //TODO Currently only looks at GLD chart. Add Report data check as well
        [Description("Compares the Days To Online With Photos average on the TTM report between the Dealer and Group Dashboards")]
        public void AverageDaysToOnlineWithPhoto_DashboardAndGLD_C16119()
        {
            string dealer = "Voss Toyota";  //Long dealer names get shortened on GLD Highcharts. Select a short or medium length one for successful location.
            
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.InventoryType_DDL.SelectByValue("Used");
            dashBoardPage.WaitForAjax();
            dashBoardPage.FromDate_DDL.SelectByIndex(7);
            dashBoardPage.ToDate_DDL.SelectByIndex(7);
            dashBoardPage.WaitForAjax();
            dashBoardPage.WaitForDashboardServices();
            
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var daysAvgGraph = js.ExecuteScript(@"return dashboard.services.ttm.response().AverageDaysToPhotoOnline;");
            double daysAvgGraphDouble = Convert.ToDouble(daysAvgGraph);
            String daysAvgGraphFormatted = daysAvgGraphDouble.ToString("###.#");

            GldPage = dashBoardPage.GoTo_GLDPage();
            GldPage.GLD_InventoryType_DDL.SelectByText("Used");
            GldPage.WaitForAjax();

            //find the column index in the chart for your desired dealer
            var columnCount =
                driver.FindElements(
                    By.XPath(
                        "//div[@id='tab_ttm_g_owp']//*[local-name()='tspan' and text()='"+ dealer +"']" +
                        "/parent::*[local-name()='text']/preceding-sibling::*[local-name()='text']")).Count + 1;
            //plug index into data locator to match dealer data
            var avgDaysGLD = driver.FindElement(By.CssSelector("div#chart_ttm_g_owp.chart-data g[class='highcharts-data-labels highcharts-tracker'] " +
                                                               "g:nth-child(" + columnCount + ") tspan")).Text;

            Assert.True(daysAvgGraphFormatted == avgDaysGLD, "Dealer Dashboard services returned {0}, but Group Dashboard displayed {1} ", daysAvgGraphFormatted, avgDaysGLD);

        }
        
        
        [Test] //This test is super optimistic and doesn't even check to make sure TTM is enabled.
        [Description("Calculates the average of the Days To Complete Ad Online column(excludes no data rows) in the TTM report and compares it to the TTM graph")]
        public void AverageDaysToCompleteAdOnline_C16619()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Performance Chrysler");
            dashBoardPage.InventoryType_DDL.SelectByValue("Used");
            dashBoardPage.WaitForAjax();
            dashBoardPage.FromDate_DDL.SelectByIndex(7);
            dashBoardPage.ToDate_DDL.SelectByIndex(7);
            dashBoardPage.WaitForAjax();
            dashBoardPage.WaitForDashboardServices();

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var daysAvgGraph = js.ExecuteScript(@"return dashboard.services.ttm.response().AverageDaysToCompleteAd;");
            double daysAvgGraphDouble = Convert.ToDouble(daysAvgGraph);
            String daysAvgGraphFormatted = daysAvgGraphDouble.ToString("###.#");


            dashBoardPage.TTM_ViewDetails_Link.Click();
            Thread.Sleep(4000);

            var arrLength = js.ExecuteScript(@"var arr = _(dashboard.data.reports.TimeToMarket().rows).map( function( i ){ return i[4]; }); return arr.length;");
            int arrLengthInt = Convert.ToInt16(arrLength);
            
            double sum = 0.0;
            int count = 0;
            double countRowsWithData = 0.0;

            for(int i = 1; i <= arrLengthInt; i++)
            {
                var days = js.ExecuteScript(@"return dashboard.data.reports.TimeToMarket().rows[" + count + "][4];");
                int daysInt = Convert.ToInt32(days);
                if(daysInt >= 0)//excludes no data values
                {
                    sum = sum + daysInt;
                    countRowsWithData++;
                }
                count++;
            }
            double avg = sum/countRowsWithData;
            String avgString = avg.ToString("###.#");

            Assert.That(daysAvgGraphFormatted, Is.EqualTo(avgString),
             "The Days To Complete Ad Online number on the graph is {0} but the calculated avg based on the report is  {1} ", daysAvgGraphFormatted, avgString);
        }


        [Test]
        [Description("Compares the Days To Complete Ad Online average on the TTM report between the dashboard and group dashboard")]  
        public void AverageDaysToCompleteAdOnline_DashboardAndGLD_C16620()
        {
            const string dealer = "Voss Toyota";  //Long dealer names get shortened on GLD, select a short or medium length one for this test

            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            dashBoardPage.InventoryType_DDL.SelectByValue("Used");
            Thread.Sleep(3000);
            dashBoardPage.setFromMonthByIndexInDDL(7);
            dashBoardPage.setToMonthByIndexInDDL(7);
            dashBoardPage.WaitForDashboardServices();

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var daysAvgGraph = js.ExecuteScript(@"return dashboard.services.ttm.response().AverageDaysToCompleteAd;");
            decimal daysAvgGraphdecimal = Convert.ToDecimal(daysAvgGraph);
            daysAvgGraphdecimal = Math.Round(daysAvgGraphdecimal, 1, MidpointRounding.AwayFromZero);
            GldPage = dashBoardPage.GoTo_GLDPage();
            driver.FindElement(By.XPath("//*[@id=\"InventoryTypeDDL\"]/option[2]")).Click(); //Option value: 1 = Used & New, 2 =  Used, 3 = New
            Thread.Sleep(4000);
            GldPage.DaysToCompleteAdOnline_TTM_subtab.Click();
            GldPage.ViewFullReport_Context_WebsitePerformance.Click();
            IWebElement dealerRow = driver.FindElement(By.XPath(".//div[@id = 'timetomarket_report']//tbody//tr[td[div[a[text() = '" + dealer + "']]]]"));
            const String avgDaysCAOLColumn = "count(//div[@id = 'timetomarket_report']//thead//tr/th[text() = 'Avg Days to Complete Ad Online']/preceding-sibling::th) + 1";
            String avgDaysToCAOLString = dealerRow.FindElement(By.XPath("td[" + avgDaysCAOLColumn + "]/div/div[1]")).Text;
            var avgDaysGLD = Convert.ToDecimal(avgDaysToCAOLString);
            Assert.True(daysAvgGraphdecimal == avgDaysGLD, "Values did not match");

        }

    
        [Test]
        [Description("Calculates the Days To Online With Photo number and compares it the values in the column")]
        public void daysToOnlineWithPhotoDateDiffCalculation_C16621()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Hendrick MINI");
            admin_Miscellaneous_Page = dashBoardPage.GoTo_Admin_Miscellaneous_Page();
            Enable_TimeToMarket();
            inventoryPage = admin_Miscellaneous_Page.GoBackTo_Inventory_Page();
            dashBoardPage = inventoryPage.GoTo_DashBoardPage(); 
            dashBoardPage.InventoryType_DDL.SelectByValue("Used");
            Thread.Sleep(3000);
            dashBoardPage.setFromMonthByIndexInDDL(1);
            dashBoardPage.setToMonthByIndexInDDL(1);
            Thread.Sleep(4000);
            dashBoardPage.TTM_ViewDetails_Link.Click();
            Thread.Sleep(4000);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var arrLength = js.ExecuteScript(@"var arr = _(dashboard.data.reports.TimeToMarket().rows).map( function( i ){ return i[2]; }); return arr.length;");
            int arrLengthInt = Convert.ToInt16(arrLength);

            for (int i = 1; i < arrLengthInt; i++)
            {
                var daysDiffCalculation = js.ExecuteScript(@"var d1 = dashboard.data.reports.TimeToMarket().rows[" + i + "][1].getTime(); var d2 = dashboard.data.reports.TimeToMarket().rows[" + i + "][2].getTime();   return d2-d1;");
                decimal daysDiffCalculation1 = Convert.ToDecimal(daysDiffCalculation);
                decimal daysDiffCalculationdec = daysDiffCalculation1 / (24.0m * 3600.0m * 1000.0m);
                daysDiffCalculationdec = Math.Round(daysDiffCalculationdec, MidpointRounding.AwayFromZero);

                var daysToOnline = js.ExecuteScript(@"return dashboard.data.reports.TimeToMarket().rows[" + i + "][3];");  
                decimal daysToOnlinedec = Convert.ToDecimal(daysToOnline);

                Assert.That(daysToOnlinedec, Is.EqualTo(daysDiffCalculationdec),
                        "The Days To Complete Ad Online number on the graph is {0} but the calculated avg based on the report is {1} ", daysToOnlinedec, daysDiffCalculationdec);
            }        
        }

        [Test]
        [Description("Compares the Days To Online With Photo number in the report to the database(new TTM columns, not MAX columns")]
        public void daysToOnlineWithPhotoDateDiffCalculationVsDatabase_C16622()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Hendrick MINI");  //If dealer is changed, also change bu variable
            admin_Miscellaneous_Page = dashBoardPage.GoTo_Admin_Miscellaneous_Page();
            Enable_TimeToMarket();
            inventoryPage = admin_Miscellaneous_Page.GoBackTo_Inventory_Page();
            dashBoardPage = inventoryPage.GoTo_DashBoardPage();
            dashBoardPage.setFromMonthByIndexInDDL(1);
            dashBoardPage.setToMonthByIndexInDDL(1);
            Thread.Sleep(4000);
            dashBoardPage.TTM_ViewDetails_Link.Click();
            Thread.Sleep(4000);

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var arrLength = js.ExecuteScript(@"var arr = _(dashboard.data.reports.TimeToMarket().rows).map( function( i ){ return i[2]; }); return arr.length;");
            int arrLengthInt = Convert.ToInt16(arrLength);

            int count = 0;
            int bu = 106361;

            for (int i = 0; i < arrLengthInt; i++)
            {
                var daysToOnline = js.ExecuteScript(@"return dashboard.data.reports.TimeToMarket().rows[" + count + "][3];");
                var stockNumber = js.ExecuteScript(@"return dashboard.data.reports.TimeToMarket().rows[" + count + "][5];");
                int daysToOnlineInt = Convert.ToInt16(daysToOnline);

                //find duplicate stock numbers
                String numOfDuplicateStockNumber =
                @"SELECT COUNT(i.StockNumber)
                FROM Merchandising.dashboard.TimeToMarketTracking ttmt
                JOIN IMT.dbo.Inventory i ON ttmt.BusinessUnitId = i.BusinessUnitID AND ttmt.InventoryId = i.InventoryID
                JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitId = ttmt.BusinessUnitId
                WHERE ttmt.BusinessUnitId = " + bu + " AND i.StockNumber = '" + stockNumber + "'";

                var dataset1 = DataFinder.ExecQuery(DataFinder.Merchandising, numOfDuplicateStockNumber);
                DataRow dr1 = dataset1.Rows[0];
                int numOfStockNumber = Convert.ToInt16(dr1.ItemArray[0].ToString());

                //Only check stock numbers that appear once in the TTM UI
                if (numOfStockNumber == 1)
                {
                    String query =
                    @"SELECT top 1 DATEDIFF(day, i.InventoryReceivedDate, PhotosFirstDate) as daysdiff
                    FROM Merchandising.dashboard.TimeToMarketTracking ttmt
                    JOIN IMT.dbo.Inventory i ON ttmt.BusinessUnitId = i.BusinessUnitID AND ttmt.InventoryId = i.InventoryID
                    JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitId = ttmt.BusinessUnitId
                    WHERE ttmt.BusinessUnitId = " + bu + " AND i.StockNumber = '" + stockNumber + "'";

                    var dataset2 = DataFinder.ExecQuery(DataFinder.Merchandising, query);
                    DataRow dr2 = dataset2.Rows[0];
                    int daysToOnlineDB = Convert.ToInt16(dr2.ItemArray[0].ToString());

                    //In the UI when photos have a date before inventory received date, the difference is set to zero and not a negative number.  
                    if (daysToOnlineDB < 0)
                        daysToOnlineDB = 0;

                    Assert.That(daysToOnlineInt, Is.EqualTo(daysToOnlineDB),
                            "The Days To Online with Photo number in the report is {0} but the database is {1} for stocknumber {2}", daysToOnlineInt, daysToOnlineDB, stockNumber);
                }

                count++;
            }

        }
           
        [Test]
        [Description("The Max Tile link on the Firstlook Ruby page should take user to the Dashboard, and default to Used type")]
        public void Firstlook_MAX_Tile_Link_To_Dashboard_C7320()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            dashBoardPage.InventoryType_DDL.SelectByValue("New");
            dashBoardPage.WaitForAjax();
            dashBoardPage.GoTo_Firstlook_Home_Page();
            homePage.Max_Tile_Link.Click();
            var selectedOption = dashBoardPage.InventoryType_DDL.SelectedOption.GetAttribute("value");
            StringAssert.IsMatch("Used", selectedOption);            
        }

        //[TestCase(Firefox, true)]
        [TestCase(InternetExplorer, false)] // Set override to false. IE is failing on new build server.
        //[TestCase(Chrome, true)]
        public void TestEmailPhoneChatCheckboxes_C14645(string browser, bool overRide)
        {
            InitializeDriver(browser, overRide);
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Kenny Ross Chevy Buick Nissan");

            if (BrowserType == "IE")
                dashBoardPage.Classified_Trends_tab.SendKeys(Keys.Enter); // IE is flakey
            else
            {
                dashBoardPage.Classified_Trends_tab.Click();
            }
            if (BrowserType == "IE")
                dashBoardPage.DirectLeads_ClassifiedTrends_subtab.SendKeys(Keys.Enter); // IE is flakey
            else
            {
                dashBoardPage.DirectLeads_ClassifiedTrends_subtab.Click();
            }
           
            dashBoardPage.WaitForAjax();
            dashBoardPage.setFromMonthByIndexInDDL(8);
            Wait_For_Dashboard_Loading();
            dashBoardPage.WaitForAjax();
            dashBoardPage.setToMonthByIndexInDDL(2);
            Wait_For_Dashboard_Loading();
            dashBoardPage.WaitForAjax();
            var checkboxes = driver.FindElements(By.XPath("//*[@id=\"trends_lead_choices\"]/div[1]/label/input"));

            foreach (var checkbox in checkboxes)
            {
                dashBoardPage.verifyCheckboxFuntionality(checkbox);
            }

        }

        /* // The two below tests have been combined into one test above using foreach in C14645 above TODO Delete 
        [Test]
        public void Phone_Checkbox_C14646()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Kenny Ross Chevy Buick Nissan");
            dashBoardPage.Classified_Trends_tab.Click();
            dashBoardPage.DirectLeads_ClassifiedTrends_subtab.Click();
            dashBoardPage.setFromMonthByIndexInDDL(8);
            dashBoardPage.setToMonthByIndexInDDL(2);
            Thread.Sleep(4000);

            IWebElement checkbox = driver.FindElement(By.XPath("//*[@id=\"trends_lead_choices\"]/div[1]/label[2]/input"));
            dashBoardPage.verifyCheckboxFuntionality(checkbox);
        }

        [Test]
        public void Chat_Checkbox_C14647()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Kenny Ross Chevy Buick Nissan");
            dashBoardPage.Classified_Trends_tab.Click();
            dashBoardPage.DirectLeads_ClassifiedTrends_subtab.Click();
            dashBoardPage.setFromMonthByIndexInDDL(8);
            dashBoardPage.setToMonthByIndexInDDL(2);
            Thread.Sleep(4000);

            IWebElement checkbox = driver.FindElement(By.XPath("//*[@id=\"trends_lead_choices\"]/div[1]/label[3]/input"));
            dashBoardPage.verifyCheckboxFuntionality(checkbox);
        }

         */

        [Test]
        [Description("Verifies marketing message appears on three tabs when DPA is disabled on the Admin page")]
        public void DigitalPerformanceAnalyticsSetting_C11875()
        {
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            admin_Miscellaneous_Page = dashBoardPage.GoTo_Admin_Miscellaneous_Page(); 
            admin_Miscellaneous_Page.DisableDigitalPerformanceAnalytics(); 

            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Dashboard.aspx");
            
            dashBoardPage.Online_Classified_Performance_tab.Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("tab_perf"));
            IWebElement messageOCP =
                driver.FindElement(By.XPath(".//img[@src='/merchandising/Themes/MaxMvc/Images/Dashboard/marketing_message_dpa_ocp.png']"));
            Assert.IsTrue(messageOCP.Displayed);
            
            dashBoardPage.Classified_Trends_tab.Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("tab_trends"));
            IWebElement messageCT =
                driver.FindElement(By.XPath(".//img[@src='/merchandising/Themes/MaxMvc/Images/Dashboard/marketing_message_dpa_ct.png']"));
            Assert.IsTrue(messageCT.Displayed);
            
            dashBoardPage.Website_Traffic_tab.Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("tab_ga"));
            IWebElement messageWT =
                driver.FindElement(By.XPath(".//img[@src='/merchandising/Themes/MaxMvc/Images/Dashboard/marketing_message_dpa_wt.png']"));
            Assert.IsTrue(messageWT.Displayed);
            
        }


        [Test] //TODO Test should also check DB and make sure UI is rendering/calculating-with that value
        [Description("Compare AutoTrader & Cars.com Monthly Cost between GLD and Dashboard for one dealer")]
        public void MonthlyCost_Dashboard_and_GLD_AT_C18677()
        {
            //Hendrick is only group that supplies current AutoTrader data via direct feed
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", "Hendrick BMW"); 
            GldPage = dashBoardPage.GoTo_GLDPage();
            GldPage.GLD_FromDate_DDL.SelectByIndex(8);
            GldPage.GLD_ToDate_DDL.SelectByIndex(8);
            GldPage.WaitForAjax(60.0M); //GLD for Hendrick can be super slow
            GldPage.Online_Classified_Performance_Tab.Click(); //triggers full report to open
            GldPage.WaitForAjax(60.0M);
            GldPage.WaitForDashboardServices(360.0M);
            GldPage.ClassifiedPerformanceReport_AutoTrader_link.Click();
            GldPage.WaitForAjax(20.0M);

            //sort Autotrader by cost high-->low to exclude zero-value dealerships
            GldPage.OnlineClassPerf_MonthlyCost_Header.Click();
            GldPage.WaitForAjax(30.0M);

            //get the name of the first dealer
            var firstDealerName = driver.FindElement(By.XPath(".//span[@style ='']//tbody/tr[1]/td[2]/div/a")).Text;

            IWebElement monthlyATCostReport = driver.FindElement(
                    By.CssSelector("span[style=''] tbody > tr:nth-child(1) td:nth-child(3) div[class='rowEntry']"));

            var firstDealer_AT_MonthlyCostText = monthlyATCostReport.Text;
            var firstDealerMonthly_AT_CostInt = Utility.ExtractDigitsFromString(firstDealer_AT_MonthlyCostText);

            GldPage.ClassifiedPerformanceReport_Cars_link.Click();
            GldPage.WaitForAjax(20);

            //find the row in Cars for the same Autotrader dealership
            IWebElement row =
                driver.FindElement(
                    By.XPath(".//span[@style ='']//tbody//tr[td[div[a[text() = '" + firstDealerName + "']]]]"));

            var monthlyCarsCostReport = row.FindElement(By.CssSelector("td:nth-child(3) div[class='rowEntry']"));
            var firstDealer_Cars_MonthlyCostText = monthlyCarsCostReport.Text;
            var firstDealerMonthly_Cars_CostInt = Utility.ExtractDigitsFromString(firstDealer_Cars_MonthlyCostText);

            //navigate to the dealership
            IWebElement foundDealerLink = row.FindElement(By.XPath("./td[2]/div/a"));
            foundDealerLink.Click();
            dashBoardPage.WaitForAjax(20.0M);
            dashBoardPage.Online_Classified_Performance_tab.Click();
            dashBoardPage.WaitForAjax();
            var AT_MonthlyCostDashboard = dashBoardPage.Get_Monthly_Cost("AutoTrader.com");
            var Cars_MonthlyCostDashboard = dashBoardPage.Get_Monthly_Cost("Cars.com");

            Assert.True(firstDealerMonthly_AT_CostInt == AT_MonthlyCostDashboard, "AutoTrader monthly cost for {0} on GLD report was {1}, but was {2} on Dashboard."
                , firstDealerName, firstDealerMonthly_AT_CostInt, AT_MonthlyCostDashboard);
            Assert.True(firstDealerMonthly_Cars_CostInt == Cars_MonthlyCostDashboard, "Cars monthly cost for {0} on GLD report was {1}, but was {2} on Dashboard."
                , firstDealerName, firstDealerMonthly_Cars_CostInt, Cars_MonthlyCostDashboard);

        }

        [Ignore] //These two tests have a spotty record and need improvement
        [Test]
        public void ClickThruRate_Dashboard_vs_PerformanceReport_C9798()  // Test requires dealer to have both Cars and AT data for the current month
        {
            string dealer = "Performance BMW";
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            Thread.Sleep(4000);

            try
            {
                dashBoardPage.Toggle_On_DashBoard.Click();
                
            }
            catch (Exception)
            {
                dashBoardPage.GoTo_Admin_Misc_Tab();
                dashBoardPage.Enable_OnlineClassifiedOverview();
                driver.Navigate().GoToUrl(BaseURL + "/merchandising/Dashboard.aspx");
                Thread.Sleep(4000);
                dashBoardPage.Toggle_On_DashBoard.Click();
            }

            int index = 12;  // month in date ddl
            dashBoardPage.setFromMonthByIndexInDDL(index);
            dashBoardPage.setToMonthByIndexInDDL(index);
            Thread.Sleep(4000);
            string fromMonthYear = dashBoardPage.getFromMonthYear(index); //read month/year to convert it later for perf report format
            string toMonthYear = dashBoardPage.getToMonthYear(index);

            string cars_CTR = dashBoardPage.OnlineClassifiedOverview_CTR_Cars.Text;
            string autoTrader_CTR = dashBoardPage.OnlineClassifiedOverview_CTR_AutoTrader.Text;

            if (cars_CTR.Length == 3)
                cars_CTR = cars_CTR + "0";
            if (autoTrader_CTR.Length == 3)
                autoTrader_CTR = autoTrader_CTR + "0";

            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Reports/PerformanceSummaryReport.aspx");

            performanceSummaryReportPage = new PerformanceSummaryReportPage(driver);

            // with dashboard's date strings, this will pick the same date and year
            performanceSummaryReportPage.pickFromMonthYear_FromDashboardFormat(fromMonthYear);  
            performanceSummaryReportPage.pickToMonthYear_FromDashboardFormat(toMonthYear);
            performanceSummaryReportPage.UpdateButton.Click();
            
            Thread.Sleep(6000);
            Assert.That((driver.FindElement(By.XPath(".//div[text()= \"" + cars_CTR + "%\"]")).Displayed), 
                "Cars.com Click Thru Rate was different between the Dashboard and Performance Report at dealer: {0}", dealer);

            Thread.Sleep(6000);
            Assert.That((driver.FindElement(By.XPath(".//div[text()= \"" + autoTrader_CTR + "%\"]")).Displayed), 
                "AutoTrader.com Click Thru Rate was different between the Dashboard and Performance Report at dealer: {0}", dealer);
        }

        [Ignore] //These two tests have a spotty record and need improvement
        [Test]
        public void ConversionRate_Dashboard_vs_PerformanceReport_C9799()  // Test requires dealer to have both Cars and AT data for the current month
        {
            string dealer = "Performance BMW";
            InitializePageAndLogin_Dashboard_Direct("QABlueMoon", "N@d@123", dealer);
            Thread.Sleep(4000);

            try
            {
                dashBoardPage.Toggle_On_DashBoard.Click();
            }
            catch (Exception)
            {
                dashBoardPage.GoTo_Admin_Misc_Tab();
                dashBoardPage.Enable_OnlineClassifiedOverview();
                driver.Navigate().GoToUrl(BaseURL + "/merchandising/Dashboard.aspx");
                Thread.Sleep(5000);
                dashBoardPage.Toggle_On_DashBoard.Click();
            }

            int index = 12;  // month in date ddl
            dashBoardPage.setFromMonthByIndexInDDL(index);
            dashBoardPage.setToMonthByIndexInDDL(index);
            Thread.Sleep(4000);
            string fromMonthYear = dashBoardPage.getFromMonthYear(index); //read month/year to convert it later for perf report format
            string toMonthYear = dashBoardPage.getToMonthYear(index);

            string cars_CR = dashBoardPage.OnlineClassifiedOverview_ConversionRate_Cars.Text;
            string autoTrader_CR = dashBoardPage.OnlineClassifiedOverview_ConversionRate_AutoTrader.Text;
           
            if (cars_CR.Length == 3)
                cars_CR = cars_CR + "0";
            if (autoTrader_CR.Length == 3)
                autoTrader_CR = autoTrader_CR + "0";

            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Reports/PerformanceSummaryReport.aspx");

            performanceSummaryReportPage = new PerformanceSummaryReportPage(driver);

            // with dashboard's date strings, this will pick the same date and year
            performanceSummaryReportPage.pickFromMonthYear_FromDashboardFormat(fromMonthYear);
            performanceSummaryReportPage.pickToMonthYear_FromDashboardFormat(toMonthYear);
            performanceSummaryReportPage.UpdateButton.Click();

            Thread.Sleep(6000);
            Assert.That((driver.FindElement(By.XPath(".//div[text()= \"" + cars_CR + "%\"]")).Displayed),
                "Cars.com Click Thru Rate was different between the Dashboard and Performance Report at dealer: {0}", dealer);

            Thread.Sleep(6000);
            Assert.That((driver.FindElement(By.XPath(".//div[text()= \"" + autoTrader_CR + "%\"]")).Displayed),
                "AutoTrader.com Click Thru Rate was different between the Dashboard and Performance Report at dealer: {0}", dealer);
        }

    }
}