﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;

namespace MAX_Selenium
{
    [TestFixture]
    public class LoginPageTest : Test
    {

        [Test]
        [Description("Enters invalid credentials and asserts that a correct error message is displayed.")]
        public void SubmitForm_InvalidCredentials_C26897()
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            loginPage.FailLoginAs("nobody", "wrong");
            Assert.True(loginPage.GetMessage().Contains("Invalid UserName"));
        }

        [Test]
        [Description("Enters an User name and empty Password and asserts that a correct error message is displayed.")]
        public void SubmitFormEmptyPassword()
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            loginPage.FailLoginAs("nobody", "");
            Assert.True(loginPage.GetMessage().Contains("Password is a required field."));
        }

        [Test]
        [Description("Enters valid credentials and asserts that the user is taken to the home page.")]
        public void SubmitForm_ValidCredentials_C26894()
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs(Single_Dealer_Default_user, Single_Dealer_Default_password);                
            //PageFactory.InitElements(driver, homePage);
            //Console.WriteLine("message:" + homePage.getTitle());
            Assert.AreEqual("FirstLook | Home", homePage.GetTitle());
        }

        [Test]
        [Description("MaxStandAloneLogin with valid credentials and asserts that the user is taken to the Dashboard page.")]
        public void LoginMaxStandAlonePage_GoToDashboard_C26893()
        {
            driver.Navigate().GoToUrl(MaxStandAloneURL);
            standAloneLoginPage  = new StandAloneLoginPage(driver);
            dashBoardPage = standAloneLoginPage.LoginAs(Single_Dealer_Default_user, Single_Dealer_Default_password);
            Assert.AreEqual("Dashboard | MAX : Intelligent Online Advertising Systems", driver.Title);
        }
  
        [Test]
        [Description("MaxStandAlone Login into the Inventory page, then logout back to login page.")]
        public void Login_Logout_MaxStandAlonePage_C26886()
        {
            driver.Navigate().GoToUrl(MaxStandAloneURL);
            standAloneLoginPage = new StandAloneLoginPage(driver);
            //driver.Navigate().GoToUrl(MaxStandAloneURL);
            dashBoardPage = standAloneLoginPage.LoginAs(Single_Dealer_Default_user, Single_Dealer_Default_password);
            Thread.Sleep(5000);
            dashBoardPage.LogOut.Click();
            Thread.Sleep(5000);
            Assert.AreEqual("MAX : Intelligent Online Advertising Systems", driver.Title);
        }

        [Test]
        [Description("MaxStandAloneLogin with invalid credentials and asserts that a correct error message is displayed.")]
        public void Login_MaxStandAlonePageInvalidCredentials_C26896()
        {
            driver.Navigate().GoToUrl(MaxStandAloneURL);
            standAloneLoginPage = new StandAloneLoginPage(driver);
            standAloneLoginPage.FailLoginAs("nobody", "wrong");
            Assert.True(standAloneLoginPage.GetMessage().Contains("Invalid User name or Password."));
        }

        [Test]
        [Description("MaxStandAloneLogin with Empty Password and asserts that a correct error message is displayed.")]
        public void Login_MaxStandAlonePage_EmptyPassword_C26895()
        {
            driver.Navigate().GoToUrl(MaxStandAloneURL);
            standAloneLoginPage = new StandAloneLoginPage(driver);
            standAloneLoginPage.FailLoginAs("nobody", "");
            Assert.True(standAloneLoginPage.GetMessage().Contains("Password is required."));
        }

     }

 }

