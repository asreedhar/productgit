﻿using System;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    internal class HomePageTest : Test
    {


        //[TestCase("1G1YW3DW6C5107778", "testGMNew1", "5", "78215")]
        [TestCase("3GNFC16018G103005", "testGMOld1", "35000", "34997")]
        [Ignore("This case needs to be revisited. It can only run 1x per release")]
        public void Create_New_In_Transit_Inventory_HomePage_C2351(string vin, string SN, string mile, string P)
        {
            InitializePageAndLogin_HomePage();  
            homePage.Create_New_InTransit_Inventory(vin, SN, mile, P);
        }

        /* Not the appropriate venue for performance tests
        [Test]
        [Description("Testing loading InventoryPage speed from Firstlook home page, it will fail if it takes more than 5 sec")]
        public void Load_MAX_Inventory_Page_From_FirstLook_Home_C3856()
        {
            InitializePageAndLogin_HomePage();            
            homePage.GotoInventoryPage();
            Stopwatch stopWatch = Stopwatch.StartNew();

            //wait for the AutoLoad result pop up window to show
            IWebElement totalItemCountElement = ExplicitWaitById("TotalItemCount", 10);
            if (totalItemCountElement == null)
                Assert.Fail("TotalItemCount element couldn't be found.");

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            Assert.That(ts.Seconds <= 5);
        }
        */
    }
}
