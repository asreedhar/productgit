﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Ionic.Zip;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System.Web;

namespace MAX_Selenium
{
    [TestFixture]
    public abstract class Test
    {
        private bool acceptNextAlert = true;
        protected string browserProcessName;
        protected string serverProcess;
        protected IWebDriver driver;
        protected const String Firefox = "Firefox";
        protected const String InternetExplorer = "InternetExplorer";
        protected const String Chrome = "Chrome";
        private static readonly long SETTLE_TIME = 1500 * TimeSpan.TicksPerMillisecond;
        protected Page page;
        protected MS_MaxForWebsite_Page maxForWebsite_Page;
        protected LoginPage loginPage;
        protected StandAloneLoginPage standAloneLoginPage;
        protected HomePage homePage;
        protected InventoryPage inventoryPage;
        protected ApprovalPage approvalPage;
        protected Merchandising_Page merchandisingPage;
        protected DashBoardPage dashBoardPage;
        protected GLDPage GldPage;
        protected EquipmentPage equipmentPage;
        protected PricingPage pricingPage;
        protected Admin_Home_Page admin_Home_Page;
        protected Admin_Miscellaneous_Page admin_Miscellaneous_Page;
        protected Admin_Upgrades_Page admin_Upgrades_Page;
        protected Admin_Autoload_Settings_Page admin_autoload_settings_page;
        protected LowActivityReportPage lowActivityReportPage;
        protected NotOnlineReportPage notOnlineReportPage;
        protected PerformanceSummaryReportPage performanceSummaryReportPage;
        protected Settings_MAXsettings_Page settings_maxsettings_page;
        protected Settings_MAXsettings_Reports_Page settings_maxsettings_reports_page;
        protected Settings_MAXsettings_Alerts_Page settings_maxsettings_alerts_page;
        protected Settings_MAXsettings_AutoLoad_Page settings_maxsettings_autoload_page;
        protected Settings_MAXsettings_AutoApprove_Page settings_maxsettings_autoapprove_page;
        protected Settings_MAXsettings_Certified_Page settings_maxsettings_certified_page;
        protected Settings_AdvancedSettings_Page settings_advancedSettings_page;
        protected Settings_AdvancedSettings_DataSourceSettings_Page settings_advancedSettings_dataSourceSettings_page;
        protected DigitalPerfAnalysis_Page digitalPerfAnalysis_page;
        protected Setup_Wizard_Page setup_wizard_page;
        protected Settings_MAXsettings_WindowSticker_Page settings_maxsettings_windowsticker_page;
        protected CloudSearch_Page cloudsearch_page;
        protected MaxDigitalShowroom_Page maxdigitalshowroom_page;
        protected Firstlook_Certified_Program_Page firstlook_certified_program_page;
        protected MaxPricingPage maxPricingPage;  //QA_quinnox
        public static String BaseURL = ConfigurationManager.AppSettings["MaxWebsiteBaseUrl"];
        public static String FirstlookBaseURL = ConfigurationManager.AppSettings["FirstlookBaseUrl"];
        public static String InventoryPageURL = ConfigurationManager.AppSettings["InventoryPageUrl"];
        public static String MaxStandAloneURL = ConfigurationManager.AppSettings["MaxStandAloneUrl"];
        public static String MaxForWebsiteURL = ConfigurationManager.AppSettings["MaxForWebsiteUrl"];
        public static String ScreenShotFilePath= ConfigurationManager.AppSettings["ScreenShotFilePath"];
        public static String Web_Service_Environment = ConfigurationManager.AppSettings["Web_Service_Environment"];
        public static String MaxDigitalShowroomURL = ConfigurationManager.AppSettings["MaxDigitalShowroomUrl"];
        public static String CloudSearchURL = ConfigurationManager.AppSettings["CloudSearchUrl"];

        public static bool ScreenShotsEnabled = StringComparer.InvariantCultureIgnoreCase.Equals(
            ConfigurationManager.AppSettings["ScreenShotsEnabled"], "true");

        public static String InTransitURL_101621 = "/support/Pages/Inventory/InTransitInventory.aspx?bu=101621";
        public static String InventoryHomeURL = "/merchandising/Workflow/Inventory.aspx";
        public static String DashboardURL = "/merchandising/Dashboard.aspx";
        public static String GLD_URL = "/merchandising/GroupDashboard.aspx";
        public static String RubyHomeURL = "/home";
        public static String WhatDriver = ConfigurationManager.AppSettings["WhatDriver"];
        public static Boolean UseSetup = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSetupAttribute"]);
        public static String Default_user = ConfigurationManager.AppSettings["User"];
        public static String Default_password = ConfigurationManager.AppSettings["Password"];
        public static String Single_Dealer_Default_user = ConfigurationManager.AppSettings["SingleDealerUser"];
        public static String Single_Dealer_Default_password = ConfigurationManager.AppSettings["SingleDealerPassword"];
        public static String DealerSpeed_user = ConfigurationManager.AppSettings["DealerSpeedUser"];
        public static String DealerSpeed_password = ConfigurationManager.AppSettings["DealerSpeedPwd"];
        public static String Phony_user = ConfigurationManager.AppSettings["PhonyUser"];
        public static String Phony_password = ConfigurationManager.AppSettings["PhonyPassword"];
        public static String GMGlobalConnect_user = ConfigurationManager.AppSettings["GMGlobalConnectUser"];
        public static String GMGlobalConnect_password = ConfigurationManager.AppSettings["GMGlobalConnectPwd"];
        public static String CurrentEnvironment = ConfigurationManager.AppSettings["Environment"];

        //if the ...VIN doesn't have specific dealer appended to it (like _WindyCityBMW), it is default to Windy City Pontiac Buick GMC 
        public static String NewGMVehicleVIN = ConfigurationManager.AppSettings["NewGMVehicleVIN"];
        public static String NewGMVehicleStock = ConfigurationManager.AppSettings["NewGMVehicleStock"];
        public static String UsedGMVehicleVIN = ConfigurationManager.AppSettings["UsedGMVehicleVIN"];
        public static String NewMazdaVehicleVIN = ConfigurationManager.AppSettings["NewMazdaVehicleVIN"];
        public static String UsedBMW328VehicleVIN = ConfigurationManager.AppSettings["UsedBMW328VehicleVIN"];
        public static String UsedBMWVehicleVIN_WindyCityBMW =
            ConfigurationManager.AppSettings["UsedBMWVehicleVIN_WindyCityBMW"];
        public static String UsedLandRoverVehicleVIN = ConfigurationManager.AppSettings["UsedLandRoverVehicleVIN"];
        public static String NewBMW740LiVehicleVIN_WindyCityBMW =
            ConfigurationManager.AppSettings["NewBMW740LiVehicleVIN_WindyCityBMW"];
        public static String MaxForWebsiteVIN = ConfigurationManager.AppSettings["MaxForWebsiteVIN"];

        public static String MaxDigitalShowroomS3BucketName = ConfigurationManager.AppSettings["MaxDigitalShowroomS3BucketName"];
        public static String MaxDigitalShowroomAwsAccessKey = ConfigurationManager.AppSettings["MaxDigitalShowroomAwsAccessKey"];
        public static String MaxDigitalShowroomAwsSecretAccessKey = ConfigurationManager.AppSettings["MaxDigitalShowroomAwsSecretAccessKey"];
        public static String MaxDigitalShowroomVehicleJsonPrefix = ConfigurationManager.AppSettings["MaxDigitalShowroomVehicleJsonPrefix"];

        //QA_quinnox
        public static String MaxPricingUrl = ConfigurationManager.AppSettings["MaxPricingUrl"];

        //properties
        public static string BrowserType { get; private set; }

        public IWebDriver CurrentDriver
        {
            get { return driver; }
        }

        public LoginPage CurrentLoginPage
        {
            get { return loginPage; }
        }

        public StandAloneLoginPage CurrentStandAloneLoginPage
        {
            get { return standAloneLoginPage; }
        }

        public HomePage CurrentHomePage
        {
            get { return homePage; }
        }

        public InventoryPage CurrentInventoryPage
        {
            get { return inventoryPage; }
        }

        public LowActivityReportPage CurrentLowActivityReportPage
        {
            get { return lowActivityReportPage; }
        }

        public ApprovalPage CurrentApprovalPage
        {
            get { return approvalPage; }
        }

        public DashBoardPage CurrentDashBoardPage
        {
            get { return dashBoardPage; }
        }

        //QA_Quinnox
        public MaxPricingPage CurrentMaxPricingPage
        {
            get { return maxPricingPage; }
        }


        private AmazonS3Client _maxDigitalShowroomS3Client;
        protected AmazonS3Client MaxDigitalShowroomS3Client
        {
            get
            {
                return _maxDigitalShowroomS3Client ??
                       (_maxDigitalShowroomS3Client =
                           new AmazonS3Client(MaxDigitalShowroomAwsAccessKey, MaxDigitalShowroomAwsSecretAccessKey,
                               RegionEndpoint.USEast1));
            }
        }

        private List<S3Object> _maxDigitalShowroomS3VehicleJsonObjects;
        protected List<S3Object> MaxDigitalShowroomS3VehicleJsonObjects
        {
            get
            {
                if (_maxDigitalShowroomS3VehicleJsonObjects != null && _maxDigitalShowroomS3VehicleJsonObjects.Count > 0)
                    return _maxDigitalShowroomS3VehicleJsonObjects;

                _maxDigitalShowroomS3VehicleJsonObjects = new List<S3Object>();

                var request = new ListObjectsRequest
                {
                    BucketName = MaxDigitalShowroomS3BucketName,
                    Prefix = MaxDigitalShowroomVehicleJsonPrefix
                };

                do
                {
                    var response = MaxDigitalShowroomS3Client.ListObjects(request);

                    _maxDigitalShowroomS3VehicleJsonObjects.AddRange(
                        from s3Obj in response.S3Objects
                        where
                            s3Obj.Key.ToLower().Contains(".json") && // only take json extension files
                            !s3Obj.Key.ToLower().Contains("test") // filter out test items
                        select s3Obj);

                    // If response is truncated, set the marker to get the next set of keys.
                    if (response.IsTruncated)
                        request.Marker = response.NextMarker;
                    else
                        request = null;
                } while (request != null);

                return _maxDigitalShowroomS3VehicleJsonObjects;
            }
        }

        protected List<S3Object> MaxDigitalShowroomsS3VehicleJsonObjectsFromLast24Hours
        {
            get
            {
                return MaxDigitalShowRoomS3VehicleJsonObjectsDaysFromToday(1);
            }
        }

        protected List<String> MaxDigitalShowroomS3VinList
        {
            get
            {
                return MaxDigitalShowroomS3ObjectsToVinList(MaxDigitalShowroomS3VehicleJsonObjects);
            }
        }

        protected List<String> MaxDigitalShowroomS3VinListFromLast24Hours
        {
            get
            {
                return MaxDigitalShowroomS3ObjectsToVinList(MaxDigitalShowroomsS3VehicleJsonObjectsFromLast24Hours);
            }
        }

        protected List<S3Object> MaxDigitalShowRoomS3VehicleJsonObjectsDaysFromToday(int daysFromToday)
        {
            var lastModified = DateTime.UtcNow.Subtract(new TimeSpan(daysFromToday, 0, 0, 0));
            return MaxDigitalShowroomS3VehicleJsonObjects.Where(s3Obj => s3Obj.LastModified >= lastModified).ToList();
        }

        protected List<String> MaxDigitalShowroomS3ObjectsToVinList(List<S3Object> objList)
        {
            return
                objList.Select(
                    x =>
                        x.Key.Substring(MaxDigitalShowroomVehicleJsonPrefix.Length + 1,
                            x.Key.Length - MaxDigitalShowroomVehicleJsonPrefix.Length - 6)).ToList();
        }



        [SetUp]
        [Description("Set up each test case.")]
        public void SetUpBase()
        {
            if (UseSetup == false) return;
            if (StringComparer.InvariantCultureIgnoreCase.Compare(WhatDriver, "IE") == 0)
            {
                CheckForIEDriverServer();
                browserProcessName = "iexplore";
                serverProcess = "iedriverserver";
                var options = new InternetExplorerOptions();
                options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                options.IgnoreZoomLevel = true;
                options.EnableNativeEvents = true; //clicks may fail if this is disabled
                driver = new InternetExplorerDriver(options);
                driver.Manage().Window.Maximize();
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
                driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(60));
                BrowserType = "IE";
            }
            else if (StringComparer.InvariantCultureIgnoreCase.Compare(WhatDriver, "FireFoxDriver") == 0)
            {
                browserProcessName = "firefox";
                var profile = new FirefoxProfile { EnableNativeEvents = true }; //Selenium recommends enabling, but reportedly causes problems
                driver = new FirefoxDriver(profile);
                driver.Manage().Window.Maximize();
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
                BrowserType = "Firefox";
            }
            else if ((StringComparer.InvariantCultureIgnoreCase.Compare(WhatDriver, "Chrome") == 0))
            {
                CheckForChromeDriver();
                browserProcessName = "chrome";
                serverProcess = "chromedriver";
                var chromeOptions = new ChromeOptions();
                driver = new ChromeDriver(chromeOptions);
                driver.Manage().Window.Maximize();
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
                BrowserType = "Chrome";
            }

        }

        //This checks local machine for IEDriverServer.exe in .../Exports/UserInterfaceTests, if missing copies a zip ver, and extracted ver to that dir. 
        private void CheckForIEDriverServer()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var pathToServerDriver = Path.Combine(path, "IEDriverServer.exe");

            if(!File.Exists(pathToServerDriver))
            {
                var targetPath = Path.Combine(path, "IEServerDriver.zip");

                var request = new WebClient();
                request.DownloadFile("http://selenium-release.storage.googleapis.com/2.45/IEDriverServer_Win32_2.45.0.zip", targetPath);

                var zip = new ZipFile(targetPath);
                zip.ExtractSelectedEntries("IEDriverServer.exe", "", path);
            }
        }

        //This checks local machine for chromedriver.exe in .../Exports/UserInterfaceTests, if missing copies a zip ver, and extracted ver to that dir.
        private void CheckForChromeDriver()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var pathToServerDriver = Path.Combine(path, "chromedriver.exe");

            if (!File.Exists(pathToServerDriver))
            {
                var targetPath = Path.Combine(path, "chromedriver_win32.zip");

                var request = new WebClient();
                request.DownloadFile("http://chromedriver.storage.googleapis.com/2.15/chromedriver_win32.zip", targetPath);

                var zip = new ZipFile(targetPath);
                zip.ExtractSelectedEntries("chromedriver.exe", "", path);
            }
        }

        [TearDown]
        [Description("Clean up each test case.")]
        public void TearDown()
        {
            TryQuitWebDriver();
            var pollUntil = DateTimeOffset.UtcNow.AddMilliseconds(2000);
            while(HasLiveBrowserProcess() && DateTimeOffset.UtcNow < pollUntil)
            {
                Thread.Sleep(50);
            }

            pollUntil = DateTimeOffset.UtcNow.AddMilliseconds(8000);
            while (HasLiveBrowserProcess() && DateTimeOffset.UtcNow < pollUntil)
            {
                TryKillBrowserProcess();   
                Thread.Sleep(50);
            }
            //needed to clean up orphaned IEDriverServer instances
            pollUntil = DateTimeOffset.UtcNow.AddMilliseconds(8000);
            while (HasLiveDriverServerProcess() && DateTimeOffset.UtcNow < pollUntil)
            {
                TryKillServerProcess();
                Thread.Sleep(50);
            }
            if (HasLiveBrowserProcess())
            {
                throw new Exception("Failed to properly shut down the browser.");
            }
        }

        public void InitializeDriver (string browser, bool overRide)
        {
            if (UseSetup && (overRide == false)) return;

            switch (browser)
            {
                case "InternetExplorer":
                    if (StringComparer.InvariantCultureIgnoreCase.Compare(WhatDriver, "IE") == 0
                        && UseSetup) break;
                    try
                    {
                        driver.Dispose(); //release any previously instantiated driver objects
                    }
                    catch (Exception)
                    {}
                    CheckForIEDriverServer();
                    browserProcessName = "iexplore";
                    serverProcess = "iedriverserver";
                    var ieOptions = new InternetExplorerOptions();
                    ieOptions.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                    ieOptions.IgnoreZoomLevel = true;
                    ieOptions.EnableNativeEvents = true; //clicks may fail if this is disabled
                    driver = new InternetExplorerDriver(ieOptions);
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
                    driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(60));
                    BrowserType = "IE";
                    break;

                case "Firefox":
                    if (StringComparer.InvariantCultureIgnoreCase.Compare(WhatDriver, "FireFoxDriver") == 0
                        && UseSetup) break;
                    try
                    {
                        driver.Dispose(); //release any previously instantiated driver objects
                    }
                    catch (Exception)
                    {}
                    browserProcessName = "firefox";
                    serverProcess = "firefox";
                    var ffProfile = new FirefoxProfile { EnableNativeEvents = true }; //Selenium recommends enabling, but reportedly causes problems
                    driver = new FirefoxDriver(ffProfile);
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
                    BrowserType = "Firefox";
                    break;

                case "Chrome":
                    if (StringComparer.InvariantCultureIgnoreCase.Compare(WhatDriver, "Chrome") == 0
                        && UseSetup) break;
                    try
                    {
                        driver.Dispose(); //release any previously instantiated driver objects
                    }
                    catch (Exception)
                    {}
                    CheckForChromeDriver();
                    browserProcessName = "chrome";
                    serverProcess = "chromedriver";
                    var chromeOptions = new ChromeOptions();
                    driver = new ChromeDriver(chromeOptions);
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
                    BrowserType = "Chrome";
                    break;

            }
         
    }




        private void TryKillBrowserProcess()
        {
            //var viewProcesses = Process.GetCurrentProcess(); //debug
            var processes = Process.GetProcessesByName(browserProcessName);
            foreach(var process in processes)
            {
                try
                {
                    process.Kill();
                    
                }
                catch
                {
                }
            }
        }

        private void TryKillServerProcess()
        {
            var theProcess = Process.GetProcessesByName(serverProcess);
            foreach (var process in theProcess)
            {
                try
                {
                    process.Kill();
                }
                catch
                {
                }
            }
        }

        private bool HasLiveBrowserProcess()
        {
            var processes = Process.GetProcessesByName(browserProcessName);
            return processes.Length > 0;
        }

        private bool HasLiveDriverServerProcess()
        {
            var theProcess = Process.GetProcessesByName(serverProcess);
            return theProcess.Length > 0;
        }

        private void TryQuitWebDriver()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

        protected void InitializePageAndLogin_HomePage(string user = null, string password = null)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs(user ?? Single_Dealer_Default_user, password ?? Single_Dealer_Default_password);
        }

        //QA_quinnox
        protected void InitializePageAndLogin_MaxPricingPage(string user = null, string password = null)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs(user ?? Single_Dealer_Default_user, password ?? Single_Dealer_Default_password);
            maxPricingPage = homePage.GoToMaxPricingpage(MaxPricingUrl);
        }

        protected void LoginAsAdmin_HomePage(string user, string password, string dealer)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs_Admin(user, password, dealer);
        }

        protected void LoginAsAdmin_InventoryPage(string user, string password, string dealer)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs_Admin(user, password, dealer);
            inventoryPage = homePage.GoToInventoryDirectly(InventoryHomeURL);
            //inventoryPage = homePage.GotoInventoryPage();
        }

        protected void InitializePageAndLogin_InventoryPage(string user = null, string password = null)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs(user ?? Single_Dealer_Default_user, password ?? Single_Dealer_Default_password);
            inventoryPage = homePage.GoToInventoryDirectly(InventoryHomeURL);
        }

        protected void InitializePageAndLogin_DashBoardPage()
        {
            InitializePageAndLogin_InventoryPage();
            dashBoardPage = inventoryPage.GoTo_DashBoardPage();
        }

        protected void InitializePageAndLogin_Dashboard_Direct(string user, string password, string dealer)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs_Admin(user, password, dealer);
            dashBoardPage = homePage.GoToDashboardDirectly(DashboardURL);
            String currentDealer = dashBoardPage.Dealer_Chooser_DDL.Text;
            StringAssert.IsMatch(currentDealer, dealer, "Navigation landed improperly; expected {0} but got {1}", dealer, currentDealer);
        }

        protected void InitializePageAndLogin_Inventory_Direct(string user, string password, string dealer)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs_Admin(user, password, dealer);
            inventoryPage = homePage.GoToInventoryDirectly(InventoryHomeURL);
            String currentDealer = inventoryPage.Dealer_Chooser_DDL.Text;
            StringAssert.IsMatch(currentDealer, dealer, "Navigation landed improperly; expected {0} but got {1}", dealer, currentDealer);
        }

        protected void NavigateToMaxForWebsite(string vin)
        {
            driver.Navigate().GoToUrl(MaxForWebsiteURL + vin);
            WaitForAjax();
            maxForWebsite_Page = new MS_MaxForWebsite_Page(driver);
        }

        protected void NavigateToMaxForWeb_GoodDeal(string vin)
        {
            driver.Navigate().GoToUrl(MaxDigitalShowroomURL + "web/#!/vehicle/" + vin);
            WaitForAjax();
            maxForWebsite_Page = new MS_MaxForWebsite_Page(driver);
        }

        protected void NavigateToMaxDigitalShowroom()
        {
            driver.Navigate().GoToUrl(MaxDigitalShowroomURL);
            maxdigitalshowroom_page = new MaxDigitalShowroom_Page(driver);
        }

        protected void InitializePageAndLogin_MAX_Settings_Page(string user, string password, string dealer)
        {
            LoginAsAdmin_InventoryPage(user, password, dealer);
            //LoginAsAdmin_InventoryPage("qashiner", "N@d@123", "Windy City Pontiac Buick GMC");         
            settings_maxsettings_page = inventoryPage.GoTo_MAXsettings_Page();
        }

        protected void InitializePageAndLogin_Dashboard_URL(string user = null, string password = null)
        {
            LoginAsAdmin_HomePage("QABluemoon", "N@d@123", "Kenny Ross Chevy Buick Nissan");
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Dashboard.aspx");
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Dashboard | MAX : Intelligent Online Advertising Systems"));

            dashBoardPage = new DashBoardPage(driver);
            dashBoardPage.CloseNaggingWindow();
        }

        protected void InitializePageAndLogin_GLD_Direct(string user, string password, string dealer)
        {
            driver.Navigate().GoToUrl(BaseURL);
            loginPage = new LoginPage(driver);
            homePage = loginPage.LoginAs_Admin(user, password, dealer);
            GldPage = homePage.GoToGLDDirectly(GLD_URL);
        }

        protected void Enter_DealerSpeed_Credentials(string username, string password)
        {
            settings_maxsettings_autoload_page = settings_maxsettings_autoload_page.GoTo_MAXSettings_AutoLoad_Tab();
            settings_maxsettings_autoload_page.Set_Supported_Make("BMW/MINI");
            settings_maxsettings_autoload_page.SupportedMakes_Edit_Button.Click();
            settings_maxsettings_autoload_page.Username_Field.Clear();
            settings_maxsettings_autoload_page.Password_Field.Clear();
            settings_maxsettings_autoload_page.Username_Field.SendKeys(username);
            settings_maxsettings_autoload_page.Password_Field.SendKeys(password);
            settings_maxsettings_autoload_page.Save_Button.Click();
        }

        protected void Enter_GMGlobalConnect_Credentials(string username, string password)
        {
            settings_maxsettings_autoload_page = settings_maxsettings_autoload_page.GoTo_MAXSettings_AutoLoad_Tab();
            settings_maxsettings_autoload_page.Set_Supported_Make("General Motors - GM Global Connect");
            settings_maxsettings_autoload_page.SupportedMakes_Edit_Button.Click();
            Boolean credentialWarningPresent =
                IsWebElementDisplayed(settings_maxsettings_autoload_page.AutoLoadCredential_Warning_Panel);

            if (!credentialWarningPresent) return;
            settings_maxsettings_autoload_page.Username_Field.Clear();
            settings_maxsettings_autoload_page.Password_Field.Clear();
            settings_maxsettings_autoload_page.Username_Field.SendKeys(username);
            settings_maxsettings_autoload_page.Password_Field.SendKeys(password);
            settings_maxsettings_autoload_page.Save_Button.Click();
        }

        protected void Enable_TimeToMarket()
        {
            if (admin_Miscellaneous_Page.ShowTimeToMarket_CheckBox.Selected) return;
            admin_Miscellaneous_Page.ShowTimeToMarket_CheckBox.Click();
            admin_Miscellaneous_Page.Save_Button.Click();
            admin_Miscellaneous_Page.WaitForPost();

        }

        protected IWebElement ExplicitWaitByPartialLinkText(string text, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement linkElement = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.PartialLinkText(text));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return linkElement;
        }

        protected IWebElement ExplicitWaitByName(string name, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement elementName = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.Name(name));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return elementName;
        }

        protected IWebElement ExplicitWaitById(string id, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement ID = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.Id(id));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return ID;
        }

        protected IWebElement ExplicitWaitByCssSelector(string css, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement cssSelector = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.CssSelector(css));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return cssSelector;
        }

        protected void WaitForElementToDisappear(IWebElement element)
        {
            var pollUntil = DateTimeOffset.UtcNow.AddMilliseconds(8000);
            while (Element_Displayed(element) && DateTimeOffset.UtcNow < pollUntil)
            {
                Thread.Sleep(50);
            }
        }

        private bool Element_Displayed(IWebElement element)
        {
            if (element.Displayed)
                return true;
            return false;
        }

        protected bool Assert_If_Item_In_DDL(IWebElement dropdown, string item)
        {
            SelectElement select = new SelectElement(dropdown);
            IList<IWebElement> options = select.Options;
            bool match = false;
            foreach(IWebElement web in options)
            {
                if (web.Text.Equals(item))
                {
                    match = true;
                    break;
                }
            }
            return match;
        }

        public bool IsElementPresent(By locatorKey)
        {
            try
            {
                driver.FindElement(locatorKey);
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
        }

        public bool IsElementDisplayed(By locatorKey)
        {
            try
            {
                return driver.FindElement(locatorKey).Displayed;
            }

            catch (NoSuchElementException e)
            {
                return false;
            }
        }

        public bool IsWebElementDisplayed(IWebElement locatorKey)
        {
            try
            {
                return locatorKey.Displayed;  //driver.FindElement(locatorKey).Displayed;
            }

            catch (NoSuchElementException e)
            {
                return false;
            }
        }


        public void Wait_For()
        {
            Thread.Sleep(5000);
        }

        public Boolean isAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }   // try 
            catch (NoAlertPresentException)
            {
                return false;
            }   // catch 
        }   // isAlertPresent()

        public string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alert.Text;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        public static void WaitForDatabase(DataTable dataTable, decimal wait_timeout_seconds = 90.0M)
        {
            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;

            while (wait_timeout_ticks >= 0 && (DateTime.Now.Ticks - started) < wait_timeout_ticks)
            {
                if (dataTable == null || dataTable.Rows == null ||
                    dataTable.Rows.Count == 0 || dataTable.Rows[0] == null)
                {
                    Thread.Sleep(1000);
                }
                else return;
            }
            throw new TimeoutException("timeout " + wait_timeout_seconds + " exceeded while waiting for Database query to finish");
        }

        public void ScrollToPageBottom()
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var jQueryLoaded = "";
            TimeSpan maxDuration = TimeSpan.FromSeconds(120);
            Stopwatch sw = Stopwatch.StartNew();
            Int64 maxScrollHeight = 0;
            Int64 currentScrollHeight = 1;


            while((sw.Elapsed < maxDuration) && (maxScrollHeight != currentScrollHeight))
            {

                jQueryLoaded = (String)js.ExecuteScript(@"return typeof $");
                if (jQueryLoaded == "function")
                {
                    Thread.Sleep(100);
                    maxScrollHeight = (Int64)js.ExecuteScript(@"return document.body.scrollHeight;");
                    js.ExecuteScript(@"window.scrollTo(0, (document.body.scrollHeight || document.documentElement.scrollHeight) + 4000);");
                    Thread.Sleep(500);
                    js.ExecuteScript(@"window.scrollTo(0, (document.body.scrollHeight || document.documentElement.scrollHeight) + 4000);");
                    WaitForAjax();
                    currentScrollHeight = (Int64)js.ExecuteScript(@"return document.body.scrollHeight;");

                }
            }

        }

        public void WaitForAjax(decimal wait_timeout_seconds = 10.0M)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;
            long last_zero_transition_ts = -1;
            long last_active = 1;
            Int64 num_active = 0;
            string documentState = "";
            string jQueryLoaded;

            while (wait_timeout_ticks >= 0 && (DateTime.Now.Ticks - started) < wait_timeout_ticks)
            {
                Thread.Sleep(50);
                jQueryLoaded = (String)js.ExecuteScript(@"return typeof $");
                if (jQueryLoaded == "function")
                {
                    num_active = (Int64)js.ExecuteScript(@"return jQuery.active;");
                    documentState = (String)js.ExecuteScript(@"return document.readyState;");
                }

                if (num_active == 0 && last_active != 0)
                {
                    last_zero_transition_ts = DateTime.Now.Ticks;
                }

                if ((DateTime.Now.Ticks - last_zero_transition_ts) > SETTLE_TIME) //no activity for SETTLE_TIME seconds
                {
                    if (documentState == "complete")
                    {
                        Thread.Sleep(500);
                        return; // done
                    }

                }
                last_active = num_active;

            }
            throw new TimeoutException("Timeout " + wait_timeout_seconds + " exceeded while waiting for Ajax calls to finish");
        }



        public string FindCurrentEnvironment()
        {
            return CurrentEnvironment;
        }

    }
}
