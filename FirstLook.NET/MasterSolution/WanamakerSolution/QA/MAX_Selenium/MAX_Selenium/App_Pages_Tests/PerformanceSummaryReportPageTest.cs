﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;


namespace MAX_Selenium
{
    [TestFixture]
    class PerformanceSummaryReportPageTest : Test
    {
        [SetUp]
        public void SetUp()
        {
            //Hendrick is the only group that has up-to-date Autotrader data from special direct feed.
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Hendrick BMW");
            admin_Miscellaneous_Page = inventoryPage.GoTo_Admin_Miscellaneous_Page();
            //Performance Report is disabled by default as of 18.0, Online Classified Overview checkbox in admin settings enables it
            admin_Miscellaneous_Page.MiscellaneousTab.Click();
            admin_Miscellaneous_Page.Check_Digital_Performance_Analytics();
            admin_Miscellaneous_Page.Check_Show_Online_Classified_Overview();
            admin_Miscellaneous_Page.BackToMaxAdLink.Click();
            performanceSummaryReportPage = inventoryPage.GoTo_PerformanceReportPage();
        }

        [Test]
        public void ChangeDealerTest_C26881()
        {
            performanceSummaryReportPage.SelectDealer("All Star Chevrolet");
            var dealerElement = new SelectElement(performanceSummaryReportPage.SelectDealerDDL);
            var currentSelected = dealerElement.SelectedOption.Text;
            Assert.True(currentSelected == "All Star Chevrolet", "Selected dealership did not change");
        }

        [Test]
        public void SetFromMonthTest_C26883()
        {
            performanceSummaryReportPage.SetFromMonth(Month.March);
            var monthElement = new SelectElement(performanceSummaryReportPage.FromMonthDDL);
            var currentSelected = monthElement.SelectedOption.Text;
            Assert.True(currentSelected == "March", "Selected month did not change");
        }

        [Test]
        public void ChangeSiteTest_C26878()
        {
            //no need to enable if using Select Dealer DDL to change dealership
            performanceSummaryReportPage.SelectDealer("Hendrick Chevrolet - Cary");
            // Confirm there's more than 1 possible selection
            var siteElement = new SelectElement(performanceSummaryReportPage.SelectSiteDDL);
            performanceSummaryReportPage.WaitForAjax(); 
            var ddlOptions = siteElement.Options.Count;
            Assert.True(ddlOptions > 1, "Not enough site options to properly test");
            performanceSummaryReportPage.SelectSiteFilter(SelectSiteFilterType.Cars);
            //wait for the Cars.com DDL item to re-render
            performanceSummaryReportPage.WaitForAjax();           
            var currentSelected = siteElement.SelectedOption.Text;
            Assert.True(currentSelected == "Cars.com", "Selected site did not change");
        }

        [Test]
        public void ChangeNewOrUsedFilterTest_C26882()
        {
            performanceSummaryReportPage.SelectNewOrUsedFilter(NewOrUsedFilterType_P.New);
            //wait for the New/Used DDL item to re-render
            performanceSummaryReportPage.WaitForAjax();
            var newUsedElement = new SelectElement(performanceSummaryReportPage.NewOrUsedDDL);
            var currentSelected = newUsedElement.SelectedOption.Text;
            Assert.True(currentSelected == "New", "Selected inventory type did not change");
        }

        [Ignore] //test is currently useless and doesn't test original problem in C6100
        [Test]
        [Description("Make sure there's no duplicate sites in DDL")]
        public void SelectSiteDropDownMenu_PerformanceReport_C6100()
        {
            performanceSummaryReportPage.DurationInitialSetting();
            performanceSummaryReportPage.WaitForAjax();
            //wait for the New/Used DDL item to re-render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.Id("ctl00_BodyPlaceHolder_sites"));
            });
            //Thread.Sleep(1000); commented out to test if really necessary
            StringAssert.IsMatch("(Overall)*(Cars.com)*(AutoTrader.com)", performanceSummaryReportPage.SelectSiteDDL.Text);
        }


        [Test]
        public void DateRangeValidation_C5832()
        {
            performanceSummaryReportPage.SetToMonth(Month.March);
            performanceSummaryReportPage.SetToYear("2013");
            performanceSummaryReportPage.UpdateButton.Click();
            Assert.That(performanceSummaryReportPage.DateRangeError.Text.Equals("To Date Must Be Earlier Than From Date"));   
        }


        [Test]
        [Description("Does what title says for Autotrader only")]
        public void CompareDetailPageViewBetweenReport_Dashboard_C9795()
        {
            performanceSummaryReportPage.SetFromMonth(Month.May);
            performanceSummaryReportPage.SetFromYear("2015");
            performanceSummaryReportPage.SetToMonth(Month.May);
            performanceSummaryReportPage.SetToYear("2015");
            performanceSummaryReportPage.UpdateButton.Click();
            performanceSummaryReportPage.WaitForAjax(20.0M);
            Thread.Sleep(1000);
            Assert.That(performanceSummaryReportPage.SelectSiteDDL.Text.Contains("AutoTrader.com"));
            IWebElement autoTraderRow = driver.FindElement(By.XPath(".//div[@id = 'ReportDiv']//tr[td[div[text() = 'AutoTrader.com']]]"));
            const String detailPageViewsColumn = "count(//div[@id = 'ReportDiv']//td[text() = 'Detail Page_Views']/ancestor::td) - 1";
            String detailPageViewsString = autoTraderRow.FindElement(By.XPath("td[" + detailPageViewsColumn + "]")).Text;
            var detailPageViews = Convert.ToInt32(detailPageViewsString);
            dashBoardPage = performanceSummaryReportPage.GoTo_DashBoardPage();
            dashBoardPage.Set_From_Month("May 2015");
            dashBoardPage.Set_To_Month("May 2015");
            dashBoardPage.WaitForAjax();
            dashBoardPage.Online_Classified_Performance_tab.Click();
            Thread.Sleep(2000);
            //TODO Need wait element in the below method
            var AT_Impressions = dashBoardPage.Get_Num_Of_Impressions("AutoTrader.com"); 
            Assert.True(detailPageViews == AT_Impressions, "Values did not match");          
        }


    }
}
