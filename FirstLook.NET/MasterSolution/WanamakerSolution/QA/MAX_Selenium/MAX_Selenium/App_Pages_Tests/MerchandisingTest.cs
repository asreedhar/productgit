﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace MAX_Selenium
{
    [TestFixture]
    class Merchandising_Page_Test : Test
    {
        [Test]
        [Description("If Website PDF is disabled, Merchandising should default to Sales Packet, and MAX Info Displayed DDL shouldn't work")]
        public void No_Uprade_WebsitePDF_C19155()
        {
            //the following query locates dealers who have MAX & Merchandising, but not Website PDF
            string query =
                @"SELECT DISTINCT top (5) BusinessUnit
                    FROM IMT.dbo.BusinessUnit bu
                    WHERE bu.Active = 1
                    AND EXISTS (select * from IMT.dbo.DealerUpgrade du where du.Active = 1 
                    and du.DealerUpgradeCD = 23 -- Merchandising enabled
                    and du.BusinessUnitId = bu.BusinessUnitID)
                    AND EXISTS (select * from IMT.dbo.DealerUpgrade du where du.Active = 1 
                    and du.DealerUpgradeCD = 24 -- MAX Ad enabled
                    and du.BusinessUnitId = bu.BusinessUnitID)         
                    AND NOT EXISTS ( select * from IMT.dbo.DealerUpgrade du 
                    where du.DealerUpgradeCD = 25 -- Website PDF disabled
                    and du.BusinessUnitId = bu.BusinessUnitID) ";
            var dataTable = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            WaitForDatabase(dataTable);

            if (dataTable.Rows.Count > 0)
            {
                DataRow dr = dataTable.Rows[0];
                string dealer_name = dr.ItemArray[0].ToString().Trim();
                InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
                inventoryPage.SelectVehicles(InventoryType.Used);
                approvalPage = inventoryPage.ClickOnFirstVehicle();
                merchandisingPage = approvalPage.GoTo_Merchandising_Page();
                //look for mask
                Assert.True(merchandisingPage.MerchandisingMask.Enabled);
                merchandisingPage.WebsitePDF_Widget_OK_Button.Click();
                //make sure correct panel is displayed by default
                Assert.True(merchandisingPage.Sales_Packet_Panel.Displayed);
                Assert.False(merchandisingPage.MAXInfo_Displayed_Panel.Displayed);
                //attempt to change DDL
                new SelectElement(merchandisingPage.Preview_Sheet_DDL).SelectByText("MAX Info Displayed");

            }
            Assert.True(merchandisingPage.MerchandisingMask.Enabled);
        }

        [Test]
        [ExpectedException(typeof(NoSuchElementException))]
        [Description("If Merchandising is disabled,The two tabs: Ping III & Max Merchandising shouldn't appear")]
        public void No_Upgrade_WebsitePDF_C19156()
        {
            //the following query locates dealers who have MAX but not Merchandising, and have AutoApprove enabled
            string query =
                @"SELECT DISTINCT top (5) BusinessUnit
                    FROM IMT.dbo.BusinessUnit bu
                    WHERE bu.Active = 1
                    AND EXISTS (select * from IMT.dbo.DealerUpgrade du where du.Active = 1 
                    and du.DealerUpgradeCD = 19 -- Ping III enabled
                    and du.BusinessUnitId = bu.BusinessUnitID)
                    AND EXISTS (select * from IMT.dbo.DealerUpgrade du where du.Active = 1 
                    and du.DealerUpgradeCD = 24 -- MAX Ad enabled
                    and du.BusinessUnitId = bu.BusinessUnitID)         
                    AND NOT EXISTS ( select * from IMT.dbo.DealerUpgrade du 
                    where du.DealerUpgradeCD = 23 -- Merchandising disabled
                    and du.BusinessUnitId = bu.BusinessUnitID)
                    AND EXISTS (select * from Merchandising.settings.AutoApprove sa
                    where sa.[Status] = 3 -- AutoApprove enabled
                    and sa.businessUnitID = bu.BusinessUnitID)";
            var dataTable = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            WaitForDatabase(dataTable);

            if (dataTable.Rows.Count > 0)
            {
                DataRow dr = dataTable.Rows[0];
                string dealer_name = dr.ItemArray[0].ToString().Trim();
                InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
                inventoryPage.SelectVehicles(InventoryType.Used);
                approvalPage = inventoryPage.ClickOnFirstVehicle();
                pricingPage = approvalPage.GoTo_Pricing_Page();
                driver.SwitchTo().Frame(0);

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                IWebElement selectElement = wait.Until(ExpectedConditions
                      .ElementIsVisible(By.CssSelector("html.js div.page-footer em")));

                Assert.Throws<NoSuchElementException>(driver.FindElement(By.CssSelector("html.js li#IAATab")).Click);
                Assert.Throws<NoSuchElementException>(driver.FindElement(By.CssSelector("body div#WorkflowHeader a#MAXSellingLink")).Click);

            }
            Assert.True(driver.FindElement(By.CssSelector("html.js h1#page_branding")).Displayed);
        }

    }
}
