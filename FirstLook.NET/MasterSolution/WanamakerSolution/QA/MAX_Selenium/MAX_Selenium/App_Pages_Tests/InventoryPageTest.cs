﻿using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [NUnit.Framework.TestFixture]
    internal class InventoryPageTest : Test
    {

        private static object[] BMW328VehicleVins_Used = {
                                                             new[] {UsedBMW328VehicleVIN}
                                                         };

        private static object[] BMWVehicleVIN_WindyCityBMW_Used = {
                                                                     new[] {UsedBMWVehicleVIN_WindyCityBMW}
                                                                 };

        private static object[] BMW740VehicleVIN_WindyCityBMW_New = {
                                                                        new[] {NewBMW740LiVehicleVIN_WindyCityBMW}
                                                                    };

        [Test]
        public void SwitchFromShowAllToUsePaging_C1212()
        {
            var dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Inventory_Direct("QABluemoon", "N@d@123", dealer);
            if (Test.BrowserType == "IE")
                inventoryPage.AllInventoryLink.SendKeys(Keys.Enter); //IE is flakey on this link.
            else
                inventoryPage.AllInventoryLink.Click();
            WaitForAjax();
            //wait for Show All link to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ShowAllItems")));
            inventoryPage.Click_ShowAll();
            Assert.AreEqual("Use Paging", inventoryPage.UsePaging.Text, "Failed for dealer:  {0} ", dealer);
        }

        [NUnit.Framework.Test]
        public void SwitchFromUsePagingToShowAll_C1211()
        {
            var dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Inventory_Direct("QABluemoon", "N@d@123", dealer);
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.Click_ShowAll();
            inventoryPage.Click_UsePaging();
            Assert.AreEqual("Show All", inventoryPage.ShowAll.Text, "Failed for dealer:  {0} ", dealer);
        }


        //need to add go to offline bucket logic
        [NUnit.Framework.Test]
        public void Verify_Bulk_Actions_Has_Mark_As_Online_In_Offline_Bucket_C26887()
        {
            var dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Inventory_Direct("QABluemoon", "N@d@123", dealer);
            // login from the standalone page, need to click button that StandAloneLoginPage class!!!!
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.BulkActionCheckBox.Click();
            new SelectElement(inventoryPage.BulkActionDropDown).SelectByText("Mark As Offline");
            inventoryPage.OffLineLink.Click();
            inventoryPage.BulkActionCheckBox.Click();
            new SelectElement(inventoryPage.BulkActionDropDown);
            Assert.AreEqual("Mark As Online", driver.FindElement(By.CssSelector("option[value=\"online\"]")).Text, "Failed for dealer:  {0} ", dealer);
            //Assert.That(inventoryPage.getMessage(), Is.StringContaining("Mark as Online"));
        }


        [NUnit.Framework.Test]
        [Description("This test confirms the alternate high-level/low-level filter hierarchy for dealers with AutoApprove disabled.")]
        [Category("SmokeInventory")] //This test configures settings in Prod. Do not apply to real dealer 
        public void Test_Link_NeedApproval_NoDescription_C26877()
        {
            InitializePageAndLogin_Inventory_Direct("QABluemoon", "N@d@123", "Windy City BMW");
            settings_maxsettings_autoapprove_page = inventoryPage.GoTo_MAXSettings_AutoApprove_Tab();
            settings_maxsettings_autoapprove_page.Disable_AutoApprove();
            inventoryPage = settings_maxsettings_autoapprove_page.GoTo_Inventory_Page();
            inventoryPage.NeedApprovalTab.Click();
            inventoryPage.NoDescription_Sub_Tab.Click();
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("No Description"), "Failed for dealer:  {0} ", "Windy City BMW");
        }


        [Test]
        public void Test_Now_Showing_Label_C3858()
        {

            var dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            const String AutoApprove_ON_Default_Tab = "Ad Review Needed ";
            const String AutoApprove_OFF_Default_Tab = "No Description ";

            //Test AutoApprove OFF
            InitializePageAndLogin_Inventory_Direct("QABluemoon", "N@d@123", dealer);
            settings_maxsettings_autoapprove_page = inventoryPage.GoTo_MAXSettings_AutoApprove_Tab();
            settings_maxsettings_autoapprove_page.Disable_AutoApprove();
            inventoryPage = settings_maxsettings_autoapprove_page.GoTo_Inventory_Page();
            inventoryPage.ApprovedLink.Click();
            IWebElement body = driver.FindElement(By.TagName("body"));
            body.SendKeys(Keys.Control + 't');      
            driver.Navigate().GoToUrl(BaseURL + "/" + InventoryPageURL);
            string capturedCurrentTab = inventoryPage.CurrentFilterSection.Text;
            Assert.That(capturedCurrentTab.Contains(AutoApprove_OFF_Default_Tab), "Failed for dealer:  {0}, expected {1}, but got {2} ", dealer, AutoApprove_OFF_Default_Tab, capturedCurrentTab);

            //Test AutoApprove ON
            settings_maxsettings_autoapprove_page = inventoryPage.GoTo_MAXSettings_AutoApprove_Tab();
            settings_maxsettings_autoapprove_page.Enable_AutoApprove();
            inventoryPage = settings_maxsettings_autoapprove_page.GoTo_Inventory_Page();
            inventoryPage.ApprovedLink.Click();
            body = driver.FindElement(By.TagName("body"));
            body.SendKeys(Keys.Control + 't');
            driver.Navigate().GoToUrl(BaseURL + "/" + InventoryPageURL);
            capturedCurrentTab = inventoryPage.CurrentFilterSection.Text;
            Assert.That(capturedCurrentTab.Contains(AutoApprove_ON_Default_Tab), "Failed for dealer:  {0}, expected {1}, but got {2} ", dealer, AutoApprove_ON_Default_Tab, capturedCurrentTab);

        }


        [Test] // This test will fail if the No Packages count is 0. Needs improvement.
        // need to add logic to check the setting to ensure the AutoApproval is off, since the tab names will be different. 
        public void Test_Link_LowAdQuality_No_Packages_C26888()
        {
            var dealer = DashBoardPage.RandomDealerChooser_Dashboard();
            InitializePageAndLogin_Inventory_Direct("QABluemoon", "N@d@123", dealer);
            inventoryPage.SelectVehicles(InventoryType.NewOrUsed);
            inventoryPage.Merchandising_Tab.Click();
            inventoryPage.NoPackages_Sub_Tab.Click();
            Assert.That(inventoryPage.CurrentFilterSection.Text.Contains("Merchandising Alerts > No Packages"), "Failed for dealer:  {0} ", dealer);

            // This is for further consideration so that we can assert that the int in the filter count matches the total item count 
            // and eventually should match the Filter Description (e.g. Low Ad Quality > All > 0-66+ Days (72))
            int NoPackageCount = Utility.ExtractDigitsFromString(inventoryPage.NoPackages_Sub_Tab.Text);
            int totalItemCount = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            Assert.AreEqual(NoPackageCount, totalItemCount);
        }


        [Test] // Verify that the Main Filter is not displayed after clicking All Inventory link
        public void MainFilterIsNotVisibleAllInventory_C3456()
        {
            InitializePageAndLogin_InventoryPage();
            if (Test.BrowserType == "IE")
                inventoryPage.AllInventoryLink.SendKeys(Keys.Enter); //IE is flakey on this link
            else
            inventoryPage.AllInventoryLink.Click();

            Thread.Sleep(500);
            Assert.That(inventoryPage.FilterMenu.Displayed, Is.False,
                        "The Main Filter should not be displayed after clicking All Inventory link");
        }

        [Test] // Verify that the Main Filter is not displayed after clicking Offline link
        public void MainFilterIsNotVisibleOffline_C8408()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.NeedPricing_Tab.Click();
            inventoryPage.OffLineLink.Click();
            Assert.That(inventoryPage.FilterMenu.Displayed, Is.False,
                        "The Main Filter should not be displayed after clicking Offline link");
        }

        [Test] // Verify that the Main Filter is not displayed after clicking Approved link
        public void MainFilterIsNotVisibleApproved_C8409()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.Merchandising_Tab.Click();
            inventoryPage.NoPackages_Sub_Tab.Click();
            inventoryPage.ApprovedLink.Click();
            Assert.That(inventoryPage.FilterMenu.Displayed, Is.False,
                        "The Main Filter should not be displayed after clicking Approved link");
        }

        [Test] // Verify that the Main Filter is displayed after clicking Need Pricing tab
        public void MainFilterIsVisibleNoPrice_C8410()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.AllInventoryLink.Click();
            inventoryPage.NeedPricing_Tab.Click();
            inventoryPage.NoPrice_Sub_Tab.Click();
            Assert.That(inventoryPage.NoPrice_Sub_Tab.Displayed, Is.True,
                        "The No Price Sub Tab should be displayed after clicking the link inside the Filter Menu");
        }

        [NUnit.Framework.Test]
        // Verify that All Inventory Link is selected and that the Total Item count matches the All Inventory count and that the currentFilter says All Inventory
        public void All_Inventory_Displayed_C3457()
        {
            InitializePageAndLogin_InventoryPage();
            int AllInventoryCount = Utility.ExtractDigitsFromString(inventoryPage.AllInventoryText.Text);
            inventoryPage.AllInventoryLink.Click();

            int TotalItemCount = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            Assert.That(AllInventoryCount, Is.EqualTo(TotalItemCount),
                        "The All Inventory Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        AllInventoryCount, TotalItemCount);
            Assert.That(inventoryPage.CurrentFilterSection.Text, Is.StringContaining("All Inventory >"),
                        "The current filter does not contain All Inventory >");
        }

        [NUnit.Framework.Test] // Verify that pagination should not change even when changing filter options
        public void PageControlShowAll_C3459()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.NewOrUsed);

            int ApprovedNum = Utility.GetDigitsFromElement(inventoryPage.ApprovedText);
            int OfflineNum = Utility.GetDigitsFromElement(inventoryPage.OfflineText);

            int NeedApprovalNum = 0;
            int NeedReviewNum = 0;

            //If Auto-Approve is turned off, the Need Approval Tab shows up in place of the Need Review, this CssSelector finds the first tab text name only
            if (driver.FindElement(By.CssSelector("div#FiltersDiv ul.clearfix li")).Text.Contains("Need Review"))
            {
                NeedReviewNum =
                        Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsAdReviewAll span.count")).Text);
            }
            else
            {
                NeedApprovalNum =
                        Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsApprovalAll span.count")).Text);
            }

            int NeedPricingNum = Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsPricingAll span.count")).Text);
            // int NeedMerchAlertsNum = Utility.ExtractDigitsFromString(inventoryPage.Merchandising_Tab.Text);  //This is broken in FB case 29267
            int NeedPhotoNum = Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsPhotosEquipmentAll span.count")).Text);
            
            if (ApprovedNum > 20)
            {
                inventoryPage.ApprovedLink.Click();
            }
            else if (OfflineNum > 20)
            {
                inventoryPage.OffLineLink.Click();
            }
            else if (NeedApprovalNum > 20)
            {
                inventoryPage.NeedApprovalTab.Click();
            }
            else if (NeedReviewNum > 20)
            {
                inventoryPage.NeedApprovalTab.Click();
            }
            else if (NeedPricingNum > 20)
            {
                inventoryPage.NeedPricing_Tab.Click();
            }
            //else if (NeedMerchAlertsNum >= 20)   //This is broken in FB case 29267
            //{
            //    inventoryPage.Merchandising_Tab.Click();
            //    inventoryPage.NoPackages_Sub_Tab.Click();
            //}
            else if (NeedPhotoNum > 20)
            {
                inventoryPage.NeedPhotosEquipment_Tab.Click();
            }
            else
            {
                return;
            }
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement selectElement = wait.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Show All")));
            inventoryPage.Click_ShowAll(); //Click the Show All paging at the bottom of the page

            inventoryPage.AllInventoryLink.Click();
            
            Thread.Sleep(5000);

            int LastItemCount = Utility.ExtractDigitsFromString(inventoryPage.NumberOfLastItem.Text);
            int TotalItemCount = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            int AllInventoryCount = Utility.ExtractDigitsFromString(inventoryPage.AllInventoryText.Text);

            // Verify that Last Item Count = Total Item Count = All Inventory Count
            Assert.That(LastItemCount, Is.EqualTo(TotalItemCount),
                        "The Last Item Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        LastItemCount, TotalItemCount);
            Assert.That(AllInventoryCount, Is.EqualTo(LastItemCount),
                        "The All Inventory Count {0} is not equal to the Last Item Count {1} at the bottom of the page.",
                        AllInventoryCount, LastItemCount);
            Assert.That(inventoryPage.UsePaging.Displayed, Is.True,
                        "The Use Paging link should be displayed at the bottom of the page");
        }
          
        [NUnit.Framework.Test] // Verify that pagination should not change even when changing inventory Type
        public void PageControlShowAll_C8411()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.Merchandising_Tab.Click();
            inventoryPage.SelectVehicles(InventoryType.New);

            Thread.Sleep(5000);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            IWebElement selectElement = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("li.NeedsPricingAll span.count")));

            int NeedApprovalNum = 0;
            int NeedReviewNum = 0;

            //If Auto-Approve is turned off, the Need Approval Tab shows up in place of the Need Review, this CssSelector finds the first tab text name only
            if (driver.FindElement(By.CssSelector("div#FiltersDiv ul.clearfix li")).Text.Contains("Need Review"))
            {
                NeedReviewNum =
                        Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsAdReviewAll span.count")).Text);
            }
            else
            {
                NeedApprovalNum =
                        Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsApprovalAll span.count")).Text);
            }

            int NeedPricingNum = Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsPricingAll span.count")).Text);
            // int NeedMerchAlertsNum = Utility.ExtractDigitsFromString(inventoryPage.Merchandising_Tab.Text);  //This is broken in FB case 29267
            int NeedPhotoNum = Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsPhotosEquipmentAll span.count")).Text);

            if (NeedApprovalNum > 20)
            {
                inventoryPage.NeedApprovalTab.Click();
                inventoryPage.All_Sub_Tab.Click();
            }
            else if (NeedReviewNum > 20)
            {
                inventoryPage.NeedReviewTab.Click();
                inventoryPage.All_Sub_Tab.Click();
            }
            else if (NeedPricingNum > 20)
            {
                inventoryPage.NeedPricing_Tab.Click();
                inventoryPage.NeedsRePricing_Sub_Tab.Click();
            }
            //else if (NeedMerchAlertsNum >= 20)   //This is broken in FB case 29267
            //{
            //    inventoryPage.Merchandising_Tab.Click();
            //    inventoryPage.NoPackages_Sub_Tab.Click();
            //}
            else if (NeedPhotoNum > 20)
            {
                inventoryPage.NeedPhotosEquipment_Tab.Click();
                inventoryPage.NoPhotos_Sub_Tab.Click();
            }
            else
            {
                return;
            }

            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement selectElement2 = wait2.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Show All")));
            inventoryPage.Click_ShowAll(); //Click the Show All paging at the bottom of the page
            
            //Change inventory Type after clicking Show All
            inventoryPage.SelectVehicles(InventoryType.Used);

            Thread.Sleep(5000);

            //Need to wait for this to load so that Elements are still valid
            WebDriverWait wait3 = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement selectElement3 = wait3.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Use Paging")));

            int LastItemCount = Utility.ExtractDigitsFromString(inventoryPage.NumberOfLastItem.Text);
            int TotalItemCount = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            int CurrentFilterCount = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);

            // Verify that Last Item Count = Total Item Count = Current Filter Count
            Assert.That(LastItemCount, Is.EqualTo(TotalItemCount),
                        "The Last Item Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        LastItemCount, TotalItemCount);
            Assert.That(CurrentFilterCount, Is.EqualTo(LastItemCount),
                        "The Tab Count {0} is not equal to the Last Item Count {1} at the bottom of the page.",
                        CurrentFilterCount, LastItemCount);
            Assert.That(inventoryPage.UsePaging.Displayed, Is.True,
                        "The Use Paging link should be displayed at the bottom of the page");
        }
         
        [NUnit.Framework.Test] // Verify that pagination should not change even when changing sub tab filter options
        public void PageControlShowAll_C8412()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.Used);


            int NeedApprovalNum = 0;
            int NeedReviewNum = 0;

            //If Auto-Approve is turned off, the Need Approval Tab shows up in place of the Need Review, this CssSelector finds the first tab text name only
            if (driver.FindElement(By.CssSelector("div#FiltersDiv ul.clearfix li")).Text.Contains("Need Review"))
            {
                NeedReviewNum =
                        Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsAdReviewAll span.count")).Text);
            }
            else
            {
                NeedApprovalNum =
                        Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsApprovalAll span.count")).Text);
            }

            int NeedPricingNum = Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsPricingAll span.count")).Text);
            int NeedMerchAlertsNum = Convert.ToInt32(driver.FindElement(By.CssSelector("li.LowAdQualityAll span.count")).Text);
            int NeedPhotoNum = Convert.ToInt32(driver.FindElement(By.CssSelector("li.NeedsPhotosEquipmentAll span.count")).Text);

            if (NeedApprovalNum > 20)
            {
                //inventoryPage.NoDescription_Sub_Tab.Click();
                inventoryPage.All_Sub_Tab.Click();
            }
            else if (NeedReviewNum > 20)
            {
                inventoryPage.All_Sub_Tab.Click();
            }
            else if (NeedPricingNum > 20)
            {
                inventoryPage.NeedPricing_Tab.Click();
                inventoryPage.NeedsRePricing_Sub_Tab.Click();
            }
            else if (NeedMerchAlertsNum > 20)
            {
                inventoryPage.Merchandising_Tab.Click();
                inventoryPage.NoPackages_Sub_Tab.Click();
            }
            else if (NeedPhotoNum > 20)
            {
                inventoryPage.NeedPhotosEquipment_Tab.Click();
                inventoryPage.NoPhotos_Sub_Tab.Click();
            }
            else
            {
                return;
            }

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement selectElement = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ShowAllItems")));
            
            inventoryPage.Click_ShowAll(); //Click the Show All paging at the bottom of the page
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            inventoryPage.Merchandising_Tab.Click();
            //change sub tabs to make sure it does not change pagination
            inventoryPage.NoPackages_Sub_Tab.Click();

            int LastItemCount = Utility.ExtractDigitsFromString(inventoryPage.NumberOfLastItem.Text);
            int TotalItemCount = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            int NoPriceSubTabCount = Utility.ExtractDigitsFromString(inventoryPage.NoPackages_Sub_Tab.Text);

            // Verify that Last Item Count = Total Item Count = No Price Sub Tab Count
            Assert.That(LastItemCount, Is.EqualTo(TotalItemCount),
                        "The Last Item Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        LastItemCount, TotalItemCount);
            Assert.That(NoPriceSubTabCount, Is.EqualTo(LastItemCount),
                        "The Sub Tab Count {0} is not equal to the Last Item Count {1} at the bottom of the page.",
                        NoPriceSubTabCount, LastItemCount);
            Assert.That(inventoryPage.UsePaging.Displayed, Is.True,
                        "The Use Paging link should be displayed at the bottom of the page");
        }
         
        
        [NUnit.Framework.Test] // Verify that pagination should not change even when changing filter options
        public void PageControlUsePaging_C8413()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.Used);
            inventoryPage.NeedPhotosEquipment_Tab.Click();
            inventoryPage.Click_ShowAll();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement selectElement = wait.Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText("Equipment Review")));

            inventoryPage.EquipmentReview_Sub_Tab.Click();
            
            inventoryPage.Click_UsePaging();

            //inventoryPage.NeedApprovalTab.Click();
            inventoryPage.Merchandising_Tab.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));

            int LastItemCount = Utility.ExtractDigitsFromString(inventoryPage.NumberOfLastItem.Text);
            int TotalItemCount = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            int PageItemCount = 20;

            // Verify that The NumberOfLastItemCount = 20 unless the TotalItemCount < 20, then NumberOfItemLastCount = TotalItemCount
            if (TotalItemCount <= PageItemCount)
            {
                Assert.That(LastItemCount, Is.EqualTo(TotalItemCount),
                            "The Last Item Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                            LastItemCount, TotalItemCount);
            }
            else
            {
                Assert.That(LastItemCount, Is.EqualTo(PageItemCount),
                            "The Last Item Count {0} is not equal to the Page Item Count {1} at the bottom of the page.",
                            LastItemCount, PageItemCount);

                // failed with error "Could not find element by: By.Id: ShowAllItems", because there were only 17 cars on the page.
                Assert.That(inventoryPage.ShowAll.Displayed, Is.True,
                        "The Show All link should be displayed at the bottom of the page");
            }
            
        } 
         
        //This belongs somewhere else; in its own class
        [Test]
        public void Test_Database_Connection_C26898()
        {
            var inventory = DataFinder.ExecQuery(DataFinder.Merchandising,
                                                 @"
                    select top 1 InventoryID, BusinessUnitID
                    from FLDW.dbo.InventoryActive I
                    where InventoryActive = 1
                        and exists (select 1
                                    from IMT.dbo.DealerUpgrade DU
                                    where DU.BusinessUnitID = I.BusinessUnitID
                                        and DU.DealerUpgradeCD = 24
                                        and DU.Active = 1
                                        and DU.EffectiveDateActive = 1)
                    ")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new {InventoryID = (int) dr["InventoryID"], BusinessUnitID = (int) dr["BusinessUnitID"]})
                .First();

            Assert.That(inventory.InventoryID, Is.Not.EqualTo(0));
            Assert.That(inventory.BusinessUnitID, Is.Not.EqualTo(0));
        }

        [NUnit.Framework.Test]
        public void Test_Pricing_Workflow_C1214()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(0);
            inventoryPage.NeedPricing_Tab.Click();
            inventoryPage.NeedsRePricing_Sub_Tab.Click();
            try
            {
                if (inventoryPage.FirstTrimmedVehicleLink.Displayed)
                {
                    inventoryPage.FirstTrimmedVehicleLink.Click();
                }

            }
            catch (NoSuchElementException e)
            {
                //test will fail.     
               Assert.Fail(e.ToString());
            }
            Assert.AreEqual("Pricing | MAX : Online Inventory. Perfected.", driver.Title);
        }

        [NUnit.Framework.Test]
        [Description("Verify the values in the label, filter and page total match the database for Offline Link")]
        public void Link_To_Get_To_Offline_Ads_C1198()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.NewOrUsed);
            inventoryPage.OffLineLink.Click();
            int offline_lable_number = Utility.ExtractDigitsFromString(inventoryPage.OfflineLabel.Text);
            int offline_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            int total_item_count = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            string query =
                "select count(*) from Merchandising.workflow.inventory where businessunitid = 101621 and statusbucket = 8";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int DB_query_count = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());
            Assert.That(offline_lable_number, Is.EqualTo(offline_bucket_count));
            Assert.That(offline_bucket_count, Is.EqualTo(total_item_count));
            Assert.That(total_item_count, Is.EqualTo(DB_query_count));
        }


        [NUnit.Framework.Test]
        [Description("Verify the values in the label, filter and page total match the database for All Inventory Link")]
        public void NewCarFilter_C3453()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.New);
            inventoryPage.AllInventoryLink.Click();
            int all_inventory_label_number = Utility.ExtractDigitsFromString(inventoryPage.AllInventoryText.Text);
            int all_inventory_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            int total_item_count = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            string query =
                "select count(*) from Merchandising.workflow.inventory where businessunitid = 101621 and InventoryType = 1";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int DB_query_count = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());
            Assert.That(all_inventory_label_number, Is.EqualTo(all_inventory_bucket_count),
                        "The All Inventory Label Count {0} is not equal to the All Inventory Bucket Count {1}.",
                        all_inventory_label_number, all_inventory_bucket_count);
            Assert.That(all_inventory_bucket_count, Is.EqualTo(total_item_count),
                        "All Inventory Bucket Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        all_inventory_bucket_count, total_item_count);
            Assert.That(total_item_count, Is.EqualTo(DB_query_count),
                        "Total Item Count {0} is not equal to the DB Query Count {1}.", total_item_count, DB_query_count);
        }

        [NUnit.Framework.Test]
        [Description("Verify the values in the label, filter and page total match the database for No Trim filter")]
        public void NewandUsedCarFilterNoTrim_C3455()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.NewOrUsed);
            inventoryPage.NoTrim_Sub_Tab.Click();
            int no_trim_label_number = Utility.ExtractDigitsFromString(inventoryPage.NoTrim_Sub_Tab.Text);
            int filter_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            int total_item_count = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            string query =
                @"select count(*) from Merchandising.workflow.Inventory INV 
                    inner join IMT.dbo.BusinessUnit BU on INV.BusinessUnitID = BU.BusinessUnitId 
                    where BU.BusinessUnitID = 101621 and BU.BusinessUnitTypeID = 4
                        and ((StatusBucket <> 8 
                            AND ISNULL(ChromeStyleID,-1) = -1))";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int DB_query_count = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());
            Assert.That(no_trim_label_number, Is.EqualTo(filter_bucket_count),
                        "The No Trim Label Count {0} is not equal to the Filter Bucket Count {1}.", no_trim_label_number,
                        filter_bucket_count);
            Assert.That(filter_bucket_count, Is.EqualTo(total_item_count),
                        "Filter Bucket Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        filter_bucket_count, total_item_count);
            Assert.That(total_item_count, Is.EqualTo(DB_query_count),
                        "Total Item Count {0} is not equal to the DB Query Count {1}.", total_item_count, DB_query_count);
        }



        [NUnit.Framework.Test]
        [Description("Verify the values in the label, filter match for New Car No Book Value = 0")]
        public void NewCarFilterNoBookValue_C11037()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.New);
            inventoryPage.Merchandising_Tab.Click();
            //wait for No Book Value sub-tab to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.PartialLinkText("No Book Value"));
            });
            inventoryPage.NoBookValue_Sub_Tab.Click();
            int no_book_value_label_number = Utility.ExtractDigitsFromString(inventoryPage.NoBookValue_Sub_Tab.Text);
            int filter_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            Assert.That(no_book_value_label_number, Is.EqualTo(filter_bucket_count),
                        "The No Book Value Count {0} is not equal to the Filter Bucket Count {1}.",
                        no_book_value_label_number, filter_bucket_count);
            Assert.That(filter_bucket_count, Is.EqualTo(0), "Filter Bucket Count {0} is not equal to 0.",
                        filter_bucket_count);
        }

        [NUnit.Framework.Test]
        [Description(
            "Verify the values in the label, filter and page total  match the db for used car No Book Value Filter")]
        public void UsedCarFilterNoBookValue_C11139()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.NewOrUsed);
                //there should be no new cars with no book out values.
            inventoryPage.Merchandising_Tab.Click();
            //wait for No Book Value link to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.PartialLinkText("No Book Value"));
            });
            inventoryPage.NoBookValue_Sub_Tab.Click();
            int no_book_value_label_number = Utility.ExtractDigitsFromString(inventoryPage.NoBookValue_Sub_Tab.Text);
            int filter_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            int total_item_count = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            string query =
                @"select count(*)
                    from Merchandising.workflow.Inventory INV
                    inner join IMT.dbo.BusinessUnit BU on INV.BusinessUnitID = BU.BusinessUnitId
                    where BU.BusinessUnitID = 101621 and BU.BusinessUnitTypeID = 4
                        and (StatusBucket <> 8 and InventoryType = 2 and HasCurrentBookValue = 0)";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int DB_query_count = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());
            Assert.That(no_book_value_label_number, Is.EqualTo(filter_bucket_count),
                        "The No Book Value Count {0} is not equal to the Filter Bucket Count {1}.",
                        no_book_value_label_number, filter_bucket_count);
            Assert.That(filter_bucket_count, Is.EqualTo(total_item_count),
                        "Filter Bucket Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        filter_bucket_count, total_item_count);
            Assert.That(total_item_count, Is.EqualTo(DB_query_count),
                        "Total Item Count {0} is not equal to the DB Query Count {1}.", total_item_count, DB_query_count);
        }

        [NUnit.Framework.Test]
        [Description("Verify the values in the label, filter match for New Car Carfax Report = 0")]
        public void NewCarFilterNoCarfaxValue_C11038()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.New);
            //wait for Merchandising tab to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.PartialLinkText("Merchandising "));
            });
            inventoryPage.Merchandising_Tab.Click();
            //wait for No CarFax sub-tab to render
            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement2 = wait2.Until<IWebElement>((d) =>
                {
                    return d.FindElement(By.PartialLinkText("No Carfax"));
                });

            inventoryPage.NoCarFax_Sub_Tab.Click();
            int no_carfax_label_number = Utility.ExtractDigitsFromString(inventoryPage.NoCarFax_Sub_Tab.Text);
            int filter_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            Assert.That(no_carfax_label_number, Is.EqualTo(filter_bucket_count),
                        "The No Book Value Count {0} is not equal to the Filter Bucket Count {1}.",
                        no_carfax_label_number, filter_bucket_count);
            Assert.That(filter_bucket_count, Is.EqualTo(0), "Filter Bucket Count {0} is not equal to 0.",
                        filter_bucket_count);
        }

        [NUnit.Framework.Test]
        [Description(
            "Verify the values in the label, filter and page total  match the db for used Car No Carfax Filter ")]
        public void UsedCarFilterNoCarfaxValue_C11138()
        {
            InitializePageAndLogin_InventoryPage();   
     
            inventoryPage.SelectVehicles(InventoryType.NewOrUsed); //there should be no new cars with carfax reports.
            inventoryPage.Merchandising_Tab.Click();
            inventoryPage.NoCarFax_Sub_Tab.Click();
            
            int no_carfax_label_number = Utility.ExtractDigitsFromString(inventoryPage.NoCarFax_Sub_Tab.Text);
            int filter_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            int total_item_count = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            string query =
                @"select count(*)
                    from Merchandising.workflow.Inventory INV
                    inner join IMT.dbo.BusinessUnit BU on INV.BusinessUnitID = BU.BusinessUnitId
                    where BU.BusinessUnitID = 101621 and BU.BusinessUnitTypeID = 4
                        and (StatusBucket <> 8 and InventoryType = 2 and HasCarfax = 0)";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int DB_query_count = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());
            Assert.That(no_carfax_label_number, Is.EqualTo(filter_bucket_count),
                        "The No Carfax Value Count {0} is not equal to the Filter Bucket Count {1}.",
                        no_carfax_label_number, filter_bucket_count);
            Assert.That(filter_bucket_count, Is.EqualTo(total_item_count),
                        "Filter Bucket Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        filter_bucket_count, total_item_count);
            Assert.That(total_item_count, Is.EqualTo(DB_query_count),
                        "Total Item Count {0} is not equal to the DB Query Count {1}.", total_item_count, DB_query_count);
            //sert.That(total_item_count.Equals(DB_query_count) || (total_item_count-1).Equals(DB_query_count));
            //this case is ghosted, the "total_item_count" is alway 1 higher than the value in DB and manual steps couldn't repo in Firefox.             
        }
            

        [NUnit.Framework.Test]
        [Description(
            "Verify the values in the label, filter and page total match the database for Equipment Review filter")]
        public void NewCarFilterReviewEquipment_C11137()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.New);
            //wait for Need Photos Equipment link to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.PartialLinkText("Need Photos/Equipment"));
            });
            inventoryPage.NeedPhotosEquipment_Tab.Click();
            inventoryPage.EquipmentReview_Sub_Tab.Click();
            int equipment_review_label_number =
                Utility.ExtractDigitsFromString(inventoryPage.EquipmentReview_Sub_Tab.Text);
            int filter_bucket_count = Utility.ExtractDigitsFromString(inventoryPage.Count.Text);
            int total_item_count = Utility.ExtractDigitsFromString(inventoryPage.TotalItemCount.Text);
            string query =
                @"select count(*)
                    from Merchandising.workflow.Inventory INV
                    inner join IMT.dbo.BusinessUnit BU on INV.BusinessUnitID = BU.BusinessUnitId
                    where BU.BusinessUnitID = 101621 and BU.BusinessUnitTypeID = 4
                        and InventoryType = 1
                        and (StatusBucket <> 8 and (exteriorStatus < 2 or interiorStatus < 2))";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int DB_query_count = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());
            Assert.That(equipment_review_label_number, Is.EqualTo(filter_bucket_count),
                        "The Equipment Review Label Count {0} is not equal to the Filter Bucket Count {1}.",
                        equipment_review_label_number, filter_bucket_count);
            Assert.That(filter_bucket_count, Is.EqualTo(total_item_count),
                        "Filter Bucket Count {0} is not equal to the Total Item Count {1} at the bottom of the page.",
                        filter_bucket_count, total_item_count);
            Assert.That(total_item_count, Is.EqualTo(DB_query_count),
                        "Total Item Count {0} is not equal to the DB Query Count {1}.", total_item_count, DB_query_count);
        }

        [NUnit.Framework.Test]
        [Description(
            "Verify the All Inventory values in the label match the Sum of Approved, Offline and No Description filter")
        ]
        public void AllInventorySum_C3452()
        {
            InitializePageAndLogin_InventoryPage();
            inventoryPage.SelectVehicles(InventoryType.NewOrUsed);

            //wait for All Inventory element to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.XPath(".//*[@id='AuxFilters']/ul/li[1]"));  

            });
            //and Ajax to finish
            inventoryPage.WaitForAjax();
            Thread.Sleep(500);
            int allInventoryCount = Utility.ExtractDigitsFromString(inventoryPage.AllInventoryText.Text);
            int approvedCount = Utility.ExtractDigitsFromString(inventoryPage.ApprovedText.Text);
            int offlineCount = Utility.ExtractDigitsFromString(inventoryPage.OfflineText.Text);
            int noDescriptionCount = Utility.ExtractDigitsFromString(inventoryPage.NoDescription_Sub_Tab.Text);
            int sumCount = noDescriptionCount + offlineCount + approvedCount;

            string query =
                @"select count(*)
                    from Merchandising.workflow.Inventory INV
                    inner join IMT.dbo.BusinessUnit BU on INV.BusinessUnitID = BU.BusinessUnitId
                    where BU.BusinessUnitID = 101621 and BU.BusinessUnitTypeID = 4";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int DB_query_count = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());

            Assert.That(allInventoryCount, Is.EqualTo(sumCount),
                        "The All Inventory Count {0} is not equal to the Sum of Approved, Offline and No Description Count {1}.",
                        allInventoryCount, sumCount);
            Assert.That(allInventoryCount, Is.EqualTo(DB_query_count),
                        "All Inventory Count {0} is not equal to the DB Query Count {1}.", allInventoryCount,
                        DB_query_count);
        }

        /*
        [TestCaseSource("BMW328VehicleVins_Used")] //Test soon to be obsolete with new AutoLoad. Autoload button will go away with new AutoLoad
        [Ignore]  //  FB 31268 - Remove manual autoload button
        public void Autoload_BMW_From_Max_Inventory_Page_C1746(string vin)
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            inventoryPage.CloseNaggingWindow();
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            admin_autoload_settings_page = admin_Home_Page.Get_Admin_Autoload_Setting_Page();
            admin_autoload_settings_page.Check_BMW_GM_Chrysler_Ford_Mazda_VW();
            admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.UnCheck_Batch_Autoload();
            inventoryPage = admin_Home_Page.GoBackTo_Inventory_Page();
            approvalPage = inventoryPage.Goto_ApprovalPageForFoundVehicle(vin);
            approvalPage.Open_No_Packages_Header();
            approvalPage.Remove_Packages_In_MicroWorkFlow();
            approvalPage.Open_Colors_Header();
            approvalPage.Reset_Colors_In_MicroWorkFlow();
            approvalPage.RegenerateAdButton.Click();
            approvalPage.GoTo_InventoryPage();
            inventoryPage.SearchVehicle(vin);
            inventoryPage.AutoLoad_Button.Click();

            //wait for the AutoLoad result pop up window to show
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            IWebElement selectElement = wait.Until(ExpectedConditions
                  .ElementIsVisible(By.CssSelector("div.ui-dialog input#closeReport")));

            IWebElement PopUp_Window_Label = driver.FindElement(By.Id("ReportLabel"));
            Assert.That(PopUp_Window_Label.Text.Contains("Setting exterior color"));
            Assert.That(PopUp_Window_Label.Text.Contains("Setting interior color"));
            Assert.That(PopUp_Window_Label.Text.Contains("OEM Vehicle packages"));
            Assert.That(PopUp_Window_Label.Text.Contains("AutoLoad: Complete"));

            inventoryPage.AutoLoad_Complete_Popup_Close_Button.Click();
            approvalPage = inventoryPage.Goto_ApprovalPageForFoundVehicle(vin);
            approvalPage.Open_No_Packages_Header();

            Assert.That(driver.FindElement(By.CssSelector("a#BtnDelete.deleteLink")).Displayed);

            approvalPage.Open_Colors_Header();


            Assert.That(
                driver.FindElement(By.Id("ExteriorColorDropDown")).Selected.Equals("Choose a color...").Equals(false));
        }
         */

        [Test] // Autoload button will go away with new AutoLoad
        [Ignore]  //  FB 31268 - Remove manual autoload button
        public void Multiple_Requests_AutoLoad_C3787()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            admin_autoload_settings_page = admin_Home_Page.Get_Admin_Autoload_Setting_Page();
            admin_autoload_settings_page.Check_BMW_GM_Chrysler_Ford_Mazda_VW();
            admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.UnCheck_Batch_Autoload();
            inventoryPage = admin_Home_Page.GoBackTo_Inventory_Page();
            settings_maxsettings_autoload_page = inventoryPage.GoTo_MAXSettings_AutoLoad_Tab(); 
            Enter_GMGlobalConnect_Credentials(GMGlobalConnect_user, GMGlobalConnect_password);
            inventoryPage = settings_maxsettings_autoload_page.GoTo_Inventory_Page();

            inventoryPage.SearchVehicle("GMC");
            inventoryPage.AutoLoad_Button.Click();

            //if "Collection Complete - Close Window" button showing in Pop-up window, pass, otherwise if it doesn't show, fail it, use try, catch 

            //wait for the AutoLoad result pop up window to show
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            IWebElement AutoLoadPopUpWindow_CloseButton = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.Id("closeReport"));
                }
                catch (NoSuchElementException e)
			    {
			        Assert.Fail(e.ToString());
			        return null;
			    }
            });

            try
            {
                Assert.That(AutoLoadPopUpWindow_CloseButton.GetAttribute("value") ==
                            "Collection Complete - Close Report");
                AutoLoadPopUpWindow_CloseButton.Click();
            }
            catch (NoSuchElementException e)
			{
			    Assert.Fail(e.ToString());
            }

            inventoryPage.SearchVehicle("BMW");
            inventoryPage.AutoLoad_Button.Click();
            //wait for the AutoLoad result pop up window to show
            WebDriverWait wait_2 = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            IWebElement AutoLoadPopUpWindow_CloseButton_2 = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.Id("closeReport"));
                }
                catch (NoSuchElementException e)
			    {
				    Assert.Fail(e.ToString());
			        return null;
			    }
            });

            try
            {
                Assert.That(AutoLoadPopUpWindow_CloseButton_2.GetAttribute("value") ==
                            "Collection Complete - Close Report");
                AutoLoadPopUpWindow_CloseButton_2.Click();
            }
            catch (NoSuchElementException e)
            {
                Assert.Fail(e.ToString());
            }
        }


        [Test]
        public void Options_StandardEquipment_DataFromLotProvider_C1239()
        {
            //Find the car that has Data From Lot Provider
            string query =
                @"select top(1) BU.BusinessUnit, AI.StockNumber, V.VIN
                from Lot.Listing.Vehicle as LLV
                inner join IMT.dbo.Vehicle as V
                    on LLV.VIN = V.VIN
                inner join FLDW.dbo.InventoryActive as AI
                on AI.VehicleID = V.VehicleID
                inner join IMT.dbo.BusinessUnit as BU
                on AI.BusinessUnitID = BU.BusinessUnitID
                inner join Merchandising.settings.merchandising as MSM
                on BU.BusinessUnitID = MSM.BusinessUnitID and LLV.ProviderID = MSM.LotProviderID
                ";
            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            string dealer_name = (dr.ItemArray[0].ToString()).Trim();
            string vin = dr.ItemArray[2].ToString();
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
            approvalPage = inventoryPage.Goto_ApprovalPageTrimmedVehicle(vin);
            approvalPage.Open_Options_Standard_Equipment_Header();
            Thread.Sleep(4000);
            Assert.That(approvalPage.Lot_Provider_Data_Label.Text.Contains("No Lot Provider Equipment Info Found") == false);
            //The below assert is better, but will test them both to verify
            Assert.IsFalse(approvalPage.Lot_Provider_Data_Label.Text.Contains("No Lot Provider Equipment Info Found"),
                "The VIN: {1} for the Business Unit {0} says that No Lot Provider Equipment Info was found.",dealer_name,vin);
        }

        [Test]
        public  void Move_Vehicles_FromTo_Offline_bucket_Bulk_Actions_C3458()
        {
            InitializePageAndLogin_InventoryPage();
            //empty offline bucket
            while (Utility.ExtractDigitsFromString(inventoryPage.OfflineText.Text) != 0)
            {
                inventoryPage.OffLineLink.Click();
                if (Utility.ExtractDigitsFromString(inventoryPage.OfflineText.Text) == 0)
                {
                    break;
                }
                inventoryPage.BulkActionCheckBox.Click();
                new SelectElement(inventoryPage.BulkActionDropDown).SelectByText("Mark As Online");
                inventoryPage.WaitForAjax();
            }

            inventoryPage.AllInventoryLink.Click();
            var First_Car_StockNumber_AllInventoryBucket =
                (driver.FindElement(
                    By.CssSelector("table#InventoryTable > tbody > tr > td.alignLeft ul.vehicleSubInfo a"))).Text;
            inventoryPage.BulkActionCheckBox.Click();
            new SelectElement(inventoryPage.BulkActionDropDown).SelectByText("Mark As Offline");
            inventoryPage.OffLineLink.Click();
            Assert.That(Utility.ExtractDigitsFromString(inventoryPage.OfflineText.Text).Equals(20));
            var First_Car_StockNumber_OfflineBucket =
                (driver.FindElement(
                    By.CssSelector("table#InventoryTable > tbody > tr > td.alignLeft ul.vehicleSubInfo a"))).Text;
            Assert.That(First_Car_StockNumber_AllInventoryBucket.Equals(First_Car_StockNumber_OfflineBucket),
                        "AllInventoryBucket{0}, OfflineBucket{1}", First_Car_StockNumber_AllInventoryBucket,
                        First_Car_StockNumber_OfflineBucket);
        }

        [TestCaseSource("BMWVehicleVIN_WindyCityBMW_Used")]
        public void Data_Source_Setting_C2354(string vin)
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            inventoryPage.SearchVehicle(vin);
            var photoCount = Int32.Parse(inventoryPage.Photo_Count.Text);
            var photoCount_background_color = inventoryPage.Photo_Count.GetCssValue("background-color");
            //"rgba(191, 0, 0, 1)" is red backgroundcolor; "rgba(42, 116, 168, 1)" is the blue background color
            settings_advancedSettings_page = inventoryPage.GoTo_Settings_AdvancedSettings_Page();
            settings_advancedSettings_dataSourceSettings_page = settings_advancedSettings_page.Get_DataSouceSettings_Page();

            if (photoCount_background_color == "rgba(191, 0, 0, 1)")
            {
                Assert.That(Int32.Parse(settings_advancedSettings_dataSourceSettings_page.Current_Photo_Count_Element.Text) > photoCount);
            }
            else if (photoCount_background_color == "rgba(42, 116, 168, 1)")
            {
                Assert.That(
                    Int32.Parse(settings_advancedSettings_dataSourceSettings_page.Current_Photo_Count_Element.Text) <=
                    photoCount);
            }

            settings_advancedSettings_dataSourceSettings_page.Edit_Button.Click();

            var temp = photoCount + 1;
            settings_advancedSettings_dataSourceSettings_page.Set_Photo_Count_Settings(temp.ToString());
            inventoryPage = settings_advancedSettings_page.Get_Inventory_Page();
            inventoryPage.SearchVehicle(vin);
            Assert.That(inventoryPage.Photo_Count.GetCssValue("background-color").Equals("rgba(191, 0, 0, 1)"));
        }

        [Test]
        public void HomeDotaspx_To_InventoryDotaspz_Redirect_C6093()
        {
            InitializePageAndLogin_InventoryPage();
            
            string Base_ENV = null;

            if (BaseURL == "https://betamax.firstlook.biz")
                Base_ENV = "betamax.firstlook.biz";
            else if (BaseURL == "https://max.firstlook.biz")
                Base_ENV = "max.firstlook.biz";
            else if (BaseURL == "https://max-b.int.firstlook.biz")
                Base_ENV = "max-b.int.firstlook.biz";

            string url = string.Format(" https://{0}/merchandising/Workflow/Home.aspx", Base_ENV);
            driver.Navigate().GoToUrl(url);
            //verifiy the new URL is below, being Redirected
            string NewUrl = string.Format("https://{0}/merchandising/Workflow/Inventory.aspx", Base_ENV);
            Assert.That(driver.Url.Equals(NewUrl));
        }


        [Test]
        public void Create_NewCarPricing_Campaign_C15377()
        {
            String rebateSign;
            String discountSign;
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            inventoryPage.CloseNaggingWindow();
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.Check_Batch_NewCar_Pricing();
            inventoryPage = admin_Miscellaneous_Page.GoBackTo_Inventory_Page();
            inventoryPage.Open_ActiveCampaigns_Widget();
            inventoryPage.Clear_Automation_Campaigns("QABlueMoon"); //by user
            inventoryPage.Open_NewCarPricing_Widget();
            String yearMakeModel = inventoryPage.NCP_SelectandRecord_YearMakeModel();
            String typeOfDiscount =  inventoryPage.SetandRecord_NCP_Price_Baseline("Invoice");
            String rawmathSign = inventoryPage.SetandRecord_NCP_Price_Button("subtract");
            String rebate =  inventoryPage.SetandRecord_NCP_Rebate(0);
            String discount =  inventoryPage.SetandRecord_NCP_Discount(500);
            String expireDate = inventoryPage.SetandRecord_NCP_Date_From_Today(2); //parameter = additional days
            String startDate = DateTime.Now.Date.ToString("d");
            inventoryPage.NCP_Apply_Button.Click();
            driver.SwitchTo().Alert().Accept();
            inventoryPage.Wait_For_NCP_Modal_To_Close();
            inventoryPage.Open_ActiveCampaigns_Widget();
            String resultingCampaign = inventoryPage.Get_NCP_Campaign_Data("QABlueMoon");
            inventoryPage.Clear_Automation_Campaigns("QABlueMoon");
            if (rebate == "0")
            {
                rebateSign = "$";
            }
            else
            {
                rebateSign = rawmathSign;
            }
            if (discount == "0")
            {
                discountSign = "$";
            }
            else
            {
                discountSign = rawmathSign;
            }
            String setCampaign = (yearMakeModel + " " + startDate + " " + expireDate + " " + typeOfDiscount + " " + rebateSign + rebate + " " + discountSign + discount);
            StringAssert.Contains(setCampaign, resultingCampaign);
        }

        [Test]
        public void NewCarPricing_Buttons_NonAppearance_C14651()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            inventoryPage.CloseNaggingWindow();
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.UnCheck_Batch_NewCar_Pricing();
            inventoryPage = admin_Miscellaneous_Page.GoBackTo_Inventory_Page();
            inventoryPage.CloseNaggingWindow();
            Assert.Throws<NoSuchElementException>(inventoryPage.NewCarPricing_Button.Click);
            Assert.Throws<NoSuchElementException>(inventoryPage.ViewActiveDiscounts_Link.Click);
            //re-enable Batch NCP
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.Check_Batch_NewCar_Pricing();
        }

        [Test]
        public void NewCarPricing_Buttons_Appearance_C14651_2()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            inventoryPage.CloseNaggingWindow();
            //don't waste time with settings, if buttons are already there.
            try
            {
                Assert.True(inventoryPage.NewCarPricing_Button.Displayed);
                Assert.True(inventoryPage.ViewActiveDiscounts_Link.Displayed);
            }
            catch (AssertionException)
            {

                admin_Miscellaneous_Page = inventoryPage.GoTo_Admin_Miscellaneous_Page();
                //admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
                admin_Miscellaneous_Page.Check_Enable_Batch_New_Car_Pricing();
                //admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
                //admin_Miscellaneous_Page.Check_Batch_NewCar_Pricing();
                inventoryPage = admin_Miscellaneous_Page.GoBackTo_Inventory_Page();
                inventoryPage.CloseNaggingWindow();
                Assert.True(inventoryPage.NewCarPricing_Button.Displayed);
                Assert.True(inventoryPage.ViewActiveDiscounts_Link.Displayed);
            }

        }

        [Test]
        [Description("Total new vehicles in NCP matches Inventory Page")]
        public void NewCarPricing_Total_Vehicles_Match_Inventory_Page_C16109()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            inventoryPage.Open_NewCarPricing_Widget();

            int totalVehicleCount_NCP = totalVehiclesFromNCP();
            
            //inventoryPage.InventoryTypeDDL.Click();
            //driver.FindElement(By.XPath("//*[@id=\"InventoryTypeDDL\"]/option[3]")).Click();
            inventoryPage.SelectVehicles(InventoryType.New);
            
            Thread.Sleep(5000);
            int totalVehicleCount_InventoryPage = Utility.ExtractDigitsFromString(inventoryPage.AllInventoryText.Text);
            Assert.That(totalVehicleCount_NCP, Is.EqualTo(totalVehicleCount_InventoryPage),
             "The total vehicle count for new vehicles on the Inventory Page is displaying {0} and NCP is displaying {1}", totalVehicleCount_InventoryPage, totalVehicleCount_NCP);    
        }

        public int totalVehiclesFromNCP()
        {
            int NCP_Total_Vehicles_Count = 0;
            int position = 1;

            while (true)
            {
                try
                {
                    driver.FindElement(By.XPath("//*[@id='yearSelectBox']/option[" + position + "]")).Click();
                    Thread.Sleep(10000);
                    int totalVehiclesSelectedFromNCP = Utility.ExtractDigitsFromString(inventoryPage.NCP_Total_Vehicles_Selected.Text);

                    NCP_Total_Vehicles_Count = NCP_Total_Vehicles_Count + totalVehiclesSelectedFromNCP;
                    driver.FindElement(By.XPath("//*[@id='yearSelectBox']/option[" + position + "]")).Click();
                    Thread.Sleep(5000);
                    position++;
                }
                catch (Exception)
                {
                    driver.FindElement(By.CssSelector("div#apply-buttons button")).Click();
                    return NCP_Total_Vehicles_Count;
                }
            }



        }

        [Test]
        [Description("Checks active NCP campaign in the DB, compares the Special Price with Internet Price on the Inventory Page")]
        public void NCP_NewPrice_in_DB_matches_InventoryPage_C16110()
        {

            string dealerName = "Kenny Ross Chevy Buick Nissan";
            var dealerBUID = 105103;

            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealerName);

            //the following query selects a car that is in an active NCP campaign for the indicated dealer
            var inventory = DataFinder.ExecQuery(DataFinder.Merchandising,
                                                 @"
                                                SELECT TOP 1 StockNumber, ListPrice, SpecialPrice
                                                FROM  Merchandising.workflow.Inventory MWI
                                                WHERE BusinessUnitID = " + dealerBUID + " " +
                                                 "AND EXISTS (select * from Merchandising.merchandising.DiscountPricingCampaignInventory " +
                                                 "dpci where Active = 1 and dpci.InventoryId = MWI.InventoryID)")                                             
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { stockNumber = (string)dr["StockNumber"], listPrice = (decimal)dr["ListPrice"], specialPrice = (decimal)dr["SpecialPrice"] })
                .First();

            inventoryPage.SearchField.SendKeys(inventory.stockNumber);
            inventoryPage.SearchButton.Click();

            int listPriceDB = (int)Math.Round(inventory.listPrice);
            int specialPriceDB = (int)Math.Round(inventory.specialPrice);
            int internetPriceInventoryPage = Utility.ExtractDigitsFromString(driver.FindElement(By.Id("ListPriceLink1")).Text);

            Assert.True(internetPriceInventoryPage == specialPriceDB, "Inventory Page and Special Price in database do not match");
            Assert.False(internetPriceInventoryPage == listPriceDB, "Inventory Page appears to be using List Price instead of Special Price");

        }
       
        [Test] 
        public void Offline_Cars_Require_ReApproval_C6104()
        {
            
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            //disable AutoApprove
            settings_maxsettings_autoapprove_page = inventoryPage.GoTo_MAXSettings_AutoApprove_Tab();
            settings_maxsettings_autoapprove_page.Disable_AutoApprove();
            settings_maxsettings_autoapprove_page.Inventory_Link.Click();
            inventoryPage.ApprovedLink.Click();
            inventoryPage.BulkAction_DDL = new SelectElement(inventoryPage.bulkAction_DDL_bud); //must instantiate here
            string stockNumber = inventoryPage.Get_FirstListed_Inventory_StockNo();
            inventoryPage.FirstCarBulkActionCheckBox.Click();            
            inventoryPage.BulkAction_DDL.SelectByText("Mark As Offline");
            inventoryPage.CloseNaggingWindow();
            inventoryPage.OffLineLink.Click();
            inventoryPage.Click_ShowAll_IfPresent();
            IWebElement targetBulkActionCheckbox = inventoryPage.Locate_BulkAction_Checkbox_By_StockNo(stockNumber);
            targetBulkActionCheckbox.Click();
            inventoryPage.BulkAction_DDL.SelectByText("Mark As Online");
            inventoryPage.CloseNaggingWindow();
            inventoryPage.InventoryTab.Click();
            inventoryPage.NoDescription_Sub_Tab.Click();
            inventoryPage.Click_ShowAll_IfPresent();
            IWebElement targetVehicleLink = inventoryPage.Locate_VehicleLink_By_StockNo(stockNumber);
            targetVehicleLink.Click();
            approvalPage = new ApprovalPage(driver);
            Assert.Throws<NoSuchElementException>(approvalPage.DateAdPublished.Click);
            //re-enable AutoApprove
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/CustomizeMAX/DealerPreferences.aspx");
            settings_maxsettings_autoapprove_page.AutoApprove_Tab.Click();
            settings_maxsettings_autoapprove_page.Enable_AutoApprove();

        }
        

        [Test]
        [Description(
            "When MMS is enabled and a template saved, a vehicle that has aleady printed the window sticker will have an icon that appears")]
        public void verifyMobileShowroomStickerIconAppears_C17970()
        {

            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");

            var inventory = DataFinder.ExecQuery(DataFinder.Merchandising,
                                                 @"
                    SELECT TOP 1 Inventory.StockNumber
                    FROM Merchandising.workflow.Inventory Inventory
                    WHERE Inventory.BusinessUnitID = 105239
                    AND Inventory.InventoryType = 2
                    AND Inventory.ChromeStyleId != -1
                    AND Inventory.StatusBucket != 8
                    AND Inventory.AutoWindowStickerPrinted = 1
                    ORDER BY Inventory.InventoryID, Inventory.StockNumber
                    ")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { stocknumber = (string)dr["stocknumber"] })
                .First();

            inventoryPage.SearchField.SendKeys(inventory.stocknumber);
            Thread.Sleep(4000);

            inventoryPage.SearchButton.Click();
            Thread.Sleep(4000);

            IWebElement windowStickerIcon = driver.FindElement(By.XPath("//*[@id=\"lnkMmsSticker\"]/img"));

            Assert.IsTrue(windowStickerIcon.Displayed);
        }

        [Test]
        [Description("When Max For SmartPhone is enabled, printing the mobile window sticker will cause a flag to be set in the database")]
        public void verifyMobileWindowStickerPrintingSetsflag_C18233()
        {
            //query returns a dealer who still has unprinted window stickers
            string query =
               @"
                SELECT TOP 1 COUNT(Inventory.InventoryID) as NumOfVehicles, Inventory.BusinessUnitID, businessUnit.BusinessUnit
                FROM Merchandising.workflow.Inventory Inventory
                INNER JOIN imt.dbo.businessUnit businessUnit ON businessUnit.businessUnitID = Inventory.BusinessUnitID
                WHERE NOT EXISTS (SELECT *
                FROM IMT.WindowSticker.PrintLog PrintLog 
                WHERE PrintLog.InventoryId = Inventory.InventoryID
                AND PrintLog.AutoPrintTemplate = 1
                )
                AND Inventory.BusinessUnitID IN 
                (
                SELECT businessUnit.BusinessUnitID
                FROM merchandising.settings.Merchandising Merchandising
                INNER JOIN imt.dbo.businessUnit businessUnit ON Merchandising.businessUnitID = BusinessUnit.BusinessUnitID
                WHERE maxForSmartPhone = 1 -- MMS enabled
                )
                AND Inventory.InventoryType = 2 -- used only
                AND Inventory.ChromeStyleId != -1 -- must have style set
                AND Inventory.StatusBucket != 8  -- Offline cars not included
                GROUP BY Inventory.BusinessUnitID, businessUnit.BusinessUnit
                ORDER BY businessUnit.BusinessUnit";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dealer = dataset.Rows[0];

            int BusinessUnitID = Convert.ToInt32(dealer.ItemArray[1]);
            string dealerName = Convert.ToString(dealer.ItemArray[2]);

            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealerName);

            Thread.Sleep(5000);

            //Query finds stocknumber of a vehicle that has not been printed
            var NonPrintedInventory = DataFinder.ExecQuery(DataFinder.Merchandising,
                                                           @"
                    SELECT TOP 1 Inventory.StockNumber
                    FROM Merchandising.workflow.Inventory Inventory
                    WHERE Inventory.InventoryType = 2
                    AND Inventory.InventoryType = 2
                    AND Inventory.ChromeStyleId != -1
                    AND Inventory.StatusBucket != 8
                    AND Inventory.AutoWindowStickerPrinted = 0
                    AND Inventory.BusinessUnitID = " + BusinessUnitID + " ORDER BY Inventory.InventoryID, Inventory.StockNumber")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { stocknumber = (string)dr["stocknumber"] })
                .First();

            string NonPrintedInventory_stocknumber = Convert.ToString(NonPrintedInventory.stocknumber);

            inventoryPage.SearchField.SendKeys(NonPrintedInventory.stocknumber);
            inventoryPage.SearchButton.Click();
            Thread.Sleep(5000);
            driver.FindElement(By.Id("BtnInventoryItem")).Click();
            Thread.Sleep(6000);
            driver.FindElement(By.Id("LoadEquipmentLink")).Click();
            Thread.Sleep(5000);

            EquipmentPage equipmentpage = new EquipmentPage(driver);
            equipmentpage.Print_Window_Sticker_Link.Click();
            Thread.Sleep(5000);

            //Must use template that is saved for Max for SmartPhone settings in Admin > Home > Upgrades tab 
            var maxMobileWindowSticker = DataFinder.ExecQuery(DataFinder.Merchandising,
                                                              @"
                    SELECT Name
                    FROM merchandising.settings.Merchandising M
                    INNER JOIN imt.dbo.businessUnit BU ON M.businessUnitID = BU.BusinessUnitID
                    INNER JOIN IMT.WindowSticker.Template T ON M.autoWindowStickerTemplateId = T.TemplateID
                    WHERE maxForSmartPhone = 1
                    AND BU.BusinessUnitID = " + BusinessUnitID + "")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { TemplateName = (string)dr["Name"] })
                .First();

            var windowSticker = new SelectElement(equipmentpage.Window_Sticker_Template_DDL);
            windowSticker.SelectByText(maxMobileWindowSticker.TemplateName);
            Thread.Sleep(6000);
            equipmentpage.PopUp_Print_Window_Sticker_Checkbox.Click();
            Thread.Sleep(3000);
            equipmentpage.PopUp_Print_Button.Click();

            //vehicles that have been printed with the same template as on the Upgrades tab will appear in this query
            var PrintedInventory = DataFinder.ExecQuery(DataFinder.Merchandising,
                  @"
                    SELECT AutowindowStickerPrinted
                    FROM Merchandising.workflow.Inventory
                    WHERE InventoryType = 2
                    AND ChromeStyleId != -1
                    AND StatusBucket != 8
                    AND AutoWindowStickerPrinted = 1
                    AND StockNumber = '" + NonPrintedInventory_stocknumber + "' AND BusinessUnitID = " + BusinessUnitID + "")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { AutowindowStickerPrinted = (Boolean)dr["AutowindowStickerPrinted"] })
                .First();

            Assert.IsTrue((PrintedInventory.AutowindowStickerPrinted), "The AutowindowStickerPrinted flag was not set for Stocknumber {0} at dealer {1}",NonPrintedInventory_stocknumber, dealerName);
        }

        [Test]
        [ExpectedException(typeof(NoSuchElementException))]
        public void No_Uprade_No_Merchandising_Link_C1200()
        {
            //the following query locates dealers who have MAX, but not Merchandising 
            string query =
                @"SELECT top 10 --du.BusinessUnitID, 
                bu.BusinessUnit
                FROM IMT.dbo.DealerUpgrade du
                JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = du.BusinessUnitID
                WHERE du.DealerUpgradeCD = 24 AND du.Active = 1 AND bu.Active = 1
                EXCEPT
                SELECT top 10 --du.BusinessUnitID, 
                bu.BusinessUnit
                FROM IMT.dbo.DealerUpgrade du
                JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = du.BusinessUnitID
                WHERE du.DealerUpgradeCD = 23 AND du.Active = 1 AND bu.Active = 1";
            var dataTable = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            WaitForDatabase(dataTable);

            if (dataTable.Rows.Count > 0)
            {
                DataRow dr = dataTable.Rows[0];
                string dealer_name = dr.ItemArray[0].ToString().Trim();
                InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
                inventoryPage.SelectVehicles(InventoryType.Used);
                approvalPage = inventoryPage.ClickOnFirstVehicle();

            }
            Assert.Throws<NoSuchElementException>(driver.FindElement(By.CssSelector("body div#WorkflowHeader a#MAXSellingLink")).Click);
        }

        
    }
}