﻿using System;
using System.Net;
using System.IO;
using System.Web;
using System.Configuration;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Interactions;

namespace MAX_Selenium
{
    public abstract class Page
    {
        //data objects
        public static String BaseURL = ConfigurationManager.AppSettings["MaxWebsiteBaseUrl"];
        private static readonly long SETTLE_TIME = 1500 * TimeSpan.TicksPerMillisecond;
        protected IWebDriver driver;

        //element finders - these must be in common and visible in all inherting pages or tests will fail

        [FindsBy(How = How.LinkText, Using = "Inventory")]
        public IWebElement Inventory_Link;

        [FindsBy(How = How.LinkText, Using = "Reports")]
        public IWebElement Reports;

        [FindsBy(How = How.PartialLinkText, Using = "Low Activity")]
        public IWebElement LowActivityReportLink;

        [FindsBy(How = How.LinkText, Using = "Settings")]
        public IWebElement Settings;

        [FindsBy(How = How.LinkText, Using = "Admin")]
        public IWebElement Admin;

        [FindsBy(How = How.XPath, Using = ".//*[@id='AdminMenu']/ul/li[1]/a")]
        public IWebElement Admin_Home;

        [FindsBy(How = How.XPath, Using = ".//form//div[@id='Tabset']//input[@value='Upgrades']")]
        public IWebElement Admin_Upgrades_Tab;

        [FindsBy(How = How.LinkText, Using = "MAX Settings")]
        public IWebElement MAX_Settings;

        [FindsBy(How = How.LinkText, Using = "Digital Performance Analysis")]
        public IWebElement DigPerfAnalysis_Link;

        [FindsBy(How = How.LinkText, Using = "FirstLook")]
        public IWebElement FirstLook_Tab;

        [FindsBy(How = How.CssSelector, Using = "div#Header a.dealerSelector")] 
        public IWebElement Dealer_Chooser_DDL;

        [FindsBy(How = How.CssSelector, Using = "#preferencesTabs input[value=Auto-Approve]")]
        public IWebElement AutoApprove_Tab;

        [FindsBy(How = How.Id, Using = "PricingPageLink")]
        public IWebElement PricingPageTab;


        // constructor
        protected Page(IWebDriver driver)
        {
            this.driver = driver;
        }

        protected Page(){}

        public String GetMessage()
        {
            return driver.PageSource;
        }

        public String GetPageSource()
        {
            return driver.PageSource;
        }

        public String GetTitle()
        {
            return driver.Title;
        }

        public String GetCurrentURL()
        {
            return driver.Url;
        }

        public void Wait_For()
        {
            Thread.Sleep(5000);
        }


        //utilities

        public bool IsElementPresent(By locatorKey)
        {
            try
            {
                driver.FindElement(locatorKey);
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
        }

        protected IWebElement ExplicitWaitByPartialLinkText(string text, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement linkElement = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.PartialLinkText(text));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return linkElement;
        }

        protected IWebElement ExplicitWaitByName(string name, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement elementName = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.Name(name));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return elementName;
        }

        protected IWebElement ExplicitWaitById(string id, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement ID = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.Id(id));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return ID;
        }

        protected IWebElement ExplicitWaitByCssSelector(string css, int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            IWebElement cssSelector = wait.Until<IWebElement>((d) =>
            {
                try
                {
                    return d.FindElement(By.CssSelector(css));
                }
                catch (NoSuchElementException e)
                {
                    return null;
                }
            });

            return cssSelector;
        }

        public void CloseNaggingWindow()
        {
            if (isNaggingPresent())
            {
                //dismiss the Nagging dialog
                //driver.FindElement(By.CssSelector("div.ui-dialog a.dismiss")).SendKeys(Keys.Return);
                driver.FindElement(By.CssSelector("div.ui-dialog a.dismiss")).Click();
            }
        }

        public void HandleJDPowerInterrupt()
        {
            if (isJDPowerPresent())
            {
                //handle the JD Power agreement interrupt
                driver.FindElement(By.Id("AcceptanceCheckbox")).Click();
                driver.FindElement(By.Id("SubmitLicenseButton")).Click();
            }
        }

        public void HandleIAAInterrupt()
        {
            if (isIAAPresent())
            {
                //handle the Internet Advertising Accelerator chooser interrupt
                driver.FindElement(By.CssSelector("div.ui-dialog a.ui-dialog-titlebar-close")).Click();
            }
        }

        public Boolean isNaggingPresent()
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3)); //speed up detection
            try
            {
                if (driver.FindElement(By.Id("external_credentials_modal")).Displayed)
                {
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5)); //return to normal
                    return true;
                }

            } 
            catch (NotFoundException)
            {
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5)); //return to normal
                return false;
            }
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5)); //return to normal
            return false;
        }

        public Boolean isJDPowerPresent()
        {
            try
            {
                if (driver.Title == "J.D. Power")
                {
                    return true;
                }

            } 
            catch (NotFoundException)
            {
                return false;
            }
            return false;
        }

        public Boolean isIAAPresent()
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(2)); //speed up detection
            try
            {
                if (driver.FindElement(By.CssSelector("div.ui-dialog div#ChoicePanel.ui-dialog-content")).Displayed)
                {
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5)); //return to normal
                    return true;
                }

            }   // try 
            catch (NotFoundException)
            {
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5)); //return to normal
                return false;
            }   // catch 
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
            return false;
        }

        
        public void WaitForAjax(decimal wait_timeout_seconds = 10.0M)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            long wait_timeout_ticks = (long) (wait_timeout_seconds*TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;
            long last_zero_transition_ts = -1;
            long last_active = 1; 
            Int64 num_active = 0;
            string documentState = "";
            string jQueryLoaded;

            while (wait_timeout_ticks >= 0 && (DateTime.Now.Ticks - started) < wait_timeout_ticks)
            {
                Thread.Sleep(50);
                jQueryLoaded = (String) js.ExecuteScript(@"return typeof $");
                if (jQueryLoaded == "function")
                {
                    num_active = (Int64)js.ExecuteScript(@"return jQuery.active;");
                    documentState = (String)js.ExecuteScript(@"return document.readyState;");
                }

                if (num_active == 0 && last_active != 0) 
                {
                    last_zero_transition_ts = DateTime.Now.Ticks;           
                }

                if ((DateTime.Now.Ticks - last_zero_transition_ts) > SETTLE_TIME) //no activity for SETTLE_TIME seconds
                {
                    if(documentState == "complete")
                    {
                        Thread.Sleep(500);
                        return; // done
                    }

                } 
                last_active = num_active;

            }
            throw new TimeoutException("Timeout " + wait_timeout_seconds + " exceeded while waiting for Ajax calls to finish");
        }


        //waits for document date to change before continuing.
        public void WaitForPost(decimal wait_timeout_seconds = 10.0M)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;

            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            DateTime startTime = DateTime.Now;
            long started = DateTime.Now.Ticks;

            while (wait_timeout_ticks >= 0 && (DateTime.Now.Ticks - started) < wait_timeout_ticks)
            {
                Thread.Sleep(500);
                
                var lastModifiedDate = Convert.ToDateTime(js.ExecuteScript("return document.lastModified"));
                
                if (DateTime.Compare(lastModifiedDate, startTime) > 0 )
                {
                    GC.Collect();
                    return; //done              
                }
            }
            throw new TimeoutException("timeout " + wait_timeout_seconds + " exceeded while waiting for Post");
        }

        //This method waits until the Dashboard is ready for all queries.
        public void WaitForDashboardServices(decimal wait_timeout_seconds = 30.0M)
        {
            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;

            WaitForDashboardInitialized();

            Int64 count_services_loading = -1;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            while (wait_timeout_ticks <= 0 || (DateTime.Now.Ticks - started) < wait_timeout_ticks)
            {
                count_services_loading = (Int64)js.ExecuteScript(@"return dashboard.CountServicesLoading()");
                if (count_services_loading == 0)
                    return; // done

                Thread.Sleep(100);
            }
            throw new Exception("timeout " + wait_timeout_seconds + " exceeded while waiting for all dashboard services to finish loading; "
                + (count_services_loading != -1 ? count_services_loading.ToString() : "(unknown)") + " remaining");
        }

        public void WaitForDashboardInitialized(decimal wait_timeout_seconds = 10.0M)
        {
            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            while (wait_timeout_ticks <= 0 || (DateTime.Now.Ticks - started) < wait_timeout_ticks)
            {
                var is_dashboard_initialized = (Boolean)js.ExecuteScript(@"return (typeof dashboard != 'undefined' && dashboard != null && (dashboard.constructor == MaxDashboardExecutive || dashboard.constructor == MaxDashboardGroup))");
                if (is_dashboard_initialized)
                    return; // done

                Thread.Sleep(100);
            }
            throw new Exception("timeout " + wait_timeout_seconds + " exceeded while waiting for dashboard to initialize");

        }


        public DateTime GetLastModifiedDate()
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            return Convert.ToDateTime(js.ExecuteScript("return document.lastModified"));
        }
        
        public void WaitForLastModifiedDateUpdate(DateTime original)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var current = original;

            while (original == current)
            {
               current = GetLastModifiedDate();
               Thread.Sleep(25) ; 
            }

            return;
        }

        //returns a date in US-en M/D/YYYY format
        public String Todays_Date_Plus_Days(Int32 days)
        {
            DateTime dateString = DateTime.Now;
            return dateString.AddDays(days).ToShortDateString();
        }

        //navigators
        public Admin_Miscellaneous_Page GoTo_Admin_Miscellaneous_Page()
        {          
            //Admin.Click();
            //Thread.Sleep(1000);
            //Admin_Home.Click();
            //Thread.Sleep(1000);
            //WaitForPost(30);
            //Admin_MiscellaneousSettings_Tab.Click();
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Admin/Admin.aspx");
            WaitForAjax();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Admin | MAX : Online Inventory. Perfected."));

            return new Admin_Miscellaneous_Page(driver);
        }

        public Admin_Upgrades_Page GoTo_Admin_Upgrades_Page()
        {
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Admin/Admin.aspx");
            WaitForAjax();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Admin | MAX : Online Inventory. Perfected."));

            Admin_Upgrades_Tab.Click();
            //Admin_Upgrades_Tab.SendKeys(Keys.Enter);
            WaitForAjax();
            return new Admin_Upgrades_Page(driver);
        }


        public LowActivityReportPage GoTo_LowActivityReportPage()
        {
            Reports.Click();
            LowActivityReportLink.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Low Activity Report | MAX : Online Inventory. Perfected."));

            return new LowActivityReportPage(driver);
        }

        public Settings_MAXsettings_AutoLoad_Page GoTo_MAXSettings_AutoLoad_Tab()
        {
            Settings.Click();
            MAX_Settings.Click();
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            IWebElement autoLoadTab = driver.FindElement(By.CssSelector("#preferencesTabs input[value=AutoLoad]"));
            autoLoadTab.Click();
            return new Settings_MAXsettings_AutoLoad_Page(driver);
        }

        public Settings_MAXsettings_AutoApprove_Page GoTo_MAXSettings_AutoApprove_Tab()
        {
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/CustomizeMAX/DealerPreferences.aspx");
            WaitForAjax();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            //IWebElement autoApproveTab = driver.FindElement(By.CssSelector("#preferencesTabs input[value=Auto-Approve]"));
            //autoApproveTab.Click();
            AutoApprove_Tab.Click();


            return new Settings_MAXsettings_AutoApprove_Page(driver);
        }

        public Settings_MAXsettings_Alerts_Page GoTo_MAXSettings_Alerts_Tab()
        {
            Settings.Click();
            MAX_Settings.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            IWebElement alertsTab = driver.FindElement(By.CssSelector("#preferencesTabs input[value=Alerts]"));
            alertsTab.Click();
            return new Settings_MAXsettings_Alerts_Page(driver);
        }

        public Settings_MAXsettings_Certified_Page GoTo_MAXSettings_Certified_Tab()
        {
            Settings.Click();
            MAX_Settings.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            IWebElement certifiedTab = driver.FindElement(By.CssSelector("#preferencesTabs input[value=Certified]"));
            certifiedTab.Click();
            return new Settings_MAXsettings_Certified_Page(driver);
        }

        public InventoryPage GoTo_Inventory_Page()
        {
            Inventory_Link.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("Current Inventory Summary | MAX : Online Inventory. Perfected."));

            return new InventoryPage(driver);
        }

        public Merchandising_Page GoTo_Merchandising_Page()
        {

            IWebElement maxMerchandisingTab = driver.FindElement(By.CssSelector("body div#WorkflowHeader a#MAXSellingLink"));
            maxMerchandisingTab.Click();
            HandleJDPowerInterrupt();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.TitleContains("MAX Merchandising"));

            return new Merchandising_Page(driver);
        }

        public PricingPage GoTo_Pricing_Page()
        {

            IWebElement pricingPageLink = driver.FindElement(By.CssSelector("div#Body a#PricingPageLink.needsPricing"));
            pricingPageLink.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.TitleContains("Pricing | MAX : Online Inventory. Perfected."));

            return new PricingPage(driver);
        }

        public MaxPricingPage GoTo_ProfitMax_Page()
        {

            PricingPageTab.Click();
            WaitForAjax();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.TitleContains("PingOne | MAX : Online Inventory. Perfected."));
            return new MaxPricingPage(driver);

        }


        public HomePage GoTo_Firstlook_Home_Page()
        {
            FirstLook_Tab.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.TitleContains("FirstLook | Home"));

            return new HomePage(driver);

        }

        public PerformanceSummaryReportPage GoTo_PerformanceReportPage()
        {
            Reports.Click();
            CloseNaggingWindow();

            //wait for Performance Summary Report link to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div#Header li#ReportsMenu.menuHead a[href='../Reports/PerformanceSummaryReport.aspx']")));

            IWebElement perfSummReportLink = driver.FindElement(By.LinkText("Performance Summary"));
            perfSummReportLink.Click();

            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait2.Until(ExpectedConditions.TitleContains("Performance Summary Report | MAX : Online Inventory. Perfected."));

            return new PerformanceSummaryReportPage(driver);
        }

        public DigitalPerfAnalysis_Page GoTo_DigitalPerfAnalysisPage()
        {
            Settings.Click();
            CloseNaggingWindow();

            //wait for DPA link to render
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText("Digital Performance Analysis")));

            DigPerfAnalysis_Link.Click();
            return new DigitalPerfAnalysis_Page(driver);
        }


        public DashBoardPage GoTo_DashBoardPage()
        {
            //Home_Tab.Click();
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Dashboard.aspx");
            WaitForAjax();
            CloseNaggingWindow();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.TitleContains("Dashboard | MAX : Intelligent Online Advertising Systems"));

            return new DashBoardPage(driver);
        }

        public GLDPage GoTo_GLDPage()
        {
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/GroupDashboard.aspx");
            WaitForAjax();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Group Dashboard | MAX : Intelligent Online Advertising Systems")); 

            return new GLDPage(driver);
        }

        //promoted from ApprovalPage class to make available to all pages
        public void Set_Preview_Length(IWebDriver dr, String length)
        {
            dr.Navigate().GoToUrl(Test.BaseURL + "/merchandising/CustomizeMAX/DealerSetup.aspx");
            dr.FindElement(By.LinkText("6. Template Default")).Click();

            if ((length == "preview150") || (length == "preview250"))
            {
                dr.FindElement(By.Id(length)).Click();
                dr.FindElement(By.Id("FinishButton")).Click();
            }
            else if (length == "previewCustom")
            {
                dr.FindElement(By.Id("previewCustom")).Click();
                dr.FindElement(By.Id("CustomPreviewLen")).Clear();
                dr.FindElement(By.Id("CustomPreviewLen")).SendKeys("300");
                dr.FindElement(By.Id("FinishButton")).Click();
            }
        }

    }
}
