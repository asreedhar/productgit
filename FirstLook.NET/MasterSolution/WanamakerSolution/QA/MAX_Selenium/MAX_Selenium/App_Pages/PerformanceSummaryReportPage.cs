﻿using System;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium
{
    public class PerformanceSummaryReportPage : ReportPage
    {
        [FindsBy(How = How.Id, Using = "HeaderSearchText")]
        public IWebElement SearchField;

        [FindsBy(How = How.Id, Using = "HeaderSearchButton")]
        public IWebElement SearchButton;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_UpdateButton")]
        public IWebElement UpdateButton;

        [FindsBy(How = How.CssSelector, Using = "div#Content div#DealerSelector select#ctl00_BodyPlaceHolder_dealers")]
        public IWebElement SelectDealerDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_sites")]
        public IWebElement SelectSiteDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_inventoryTypes")]
        public IWebElement NewOrUsedDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_FromMonth")]
        public IWebElement FromMonthDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_FromYear")]
        public IWebElement FromYearDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_ToMonth")]
        public IWebElement ToMonthDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_ToYear")]
        public IWebElement ToYearDDL;

        [FindsBy(How = How.Id, Using = "DateRangeError")]
        public IWebElement DateRangeError;

        public PerformanceSummaryReportPage(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Performance Summary Report | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Performance Summary Report | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Performance Summary Report Page");
            }

            PageFactory.InitElements(driver, this);
        }

        public void SelectDealer(string dealerName)  // for example "Faulkner BMW"
        {
            var ddl = new SelectElement(SelectDealerDDL);
            if (ddl.SelectedOption.Text != dealerName) //this search is slow because the dealer list is huge.
                                                        // need a strategy better than Selenium's to find the selected element
            {
                ddl.SelectByText(dealerName);  
            }
        }

        public void SelectSiteFilter(SelectSiteFilterType option)
        {
            SelectItemInDDL(SelectSiteDDL,option);

        }

        public void SelectNewOrUsedFilter(NewOrUsedFilterType_P option)
        {
            var ddl = new SelectElement(NewOrUsedDDL);
            if (ddl.SelectedOption.Text != option.ToString())
            {
                ddl.SelectByText(option.ToString());
            }
        }

        public void SetFromMonth(Month month)
        {
            var ddl = new SelectElement(FromMonthDDL);

            if (ddl.SelectedOption.Text != month.ToString())
            {
                ddl.SelectByText(month.ToString());
            }
        }

        public void SetFromYear(string year)
        {
            var ddl = new SelectElement(FromYearDDL);

            if (ddl.SelectedOption.Text != year)
            {
                ddl.SelectByText(year);
            }
        }

        public void SetToMonth(Month month)
        {
            var ddl = new SelectElement(ToMonthDDL);

            if (ddl.SelectedOption.Text != month.ToString())
            {
                ddl.SelectByText(month.ToString());
            }
        }

        public void SetToYear(string year)
        {
            var ddl = new SelectElement(ToYearDDL);

            if (year != null && ddl.SelectedOption.Text != year)
            {
                ddl.SelectByText(year);
            }
        }
         /* Replaced with parameterized version below
        public void DurationInitialSetting()
        {
            SetFromMonth(Month.January);
            SetFromYear("2013");
            SetToMonth(Month.December);
            UpdateButton.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }
        */
        public void setFromMonthByIndexInDDL(int monthIndex)  // *Index* (not value) 0 in the ddl is January, 11 is December
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_FromMonth"))).SelectByIndex(monthIndex);
        }

        public void setFromYearByIndexInDDL(int yearIndex)  // Index 0 in the ddl is the most recent year, 4 the oldest
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_FromYear"))).SelectByIndex(yearIndex);
        }

        public void setToYearByIndexInDDL(int yearIndex)  // Index 0 in the ddl is the most recent year, 4 the oldest
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_ToYear"))).SelectByIndex(yearIndex);
        }
        public void setToMonthByIndexInDDL(int monthIndex)  // *Index* (not value) 0 in the ddl is January, 11 is December
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_ToMonth"))).SelectByIndex(monthIndex);
        }

        public void DurationInitialSetting()
        {
            setFromMonthByIndexInDDL(0);
            setFromYearByIndexInDDL(1);
            setToMonthByIndexInDDL(11);
            //setToYearByIndexInDDL(2); //don't change; leave at default
            UpdateButton.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }


        public void pickFromMonthYear_FromDashboardFormat(string date)
        {
           
            String pattern = " ";
            string[] fromMonthYear = Regex.Split(date, pattern);

            string fromMonth = fromMonthYear[0];
            string fromYear = fromMonthYear[1];


            switch (fromMonth)
            {
                case "Jan":
                    setFromMonthByIndex(0);
                    break;
                    
                case "Feb":
                    setFromMonthByIndex(1);
                    break;

                case "Mar":
                    setFromMonthByIndex(2);
                    break;

                case "Apr":
                    setFromMonthByIndex(3);
                    break;

                case "May":
                    setFromMonthByIndex(4);
                    break;

                case "Jun":
                    setFromMonthByIndex(5);
                    break;

                case "Jul":
                    setFromMonthByIndex(6);
                    break;

                case "Aug":
                    setFromMonthByIndex(7);
                    break;

                case "Sep":
                    setFromMonthByIndex(8);
                    break;

                case "Oct":
                    setFromMonthByIndex(9);
                    break;

                case "Nov":
                    setFromMonthByIndex(10);
                    break;

                case "Dec":
                    setFromMonthByIndex(11);
                    break;
            }

            switch (fromYear)
            {
                case "2015":
                    setFromYearByIndex(0);
                    break;

                case "2014":
                    setFromYearByIndex(1);
                    break;
            }

        }

        public void pickToMonthYear_FromDashboardFormat(string date)
        {

            String pattern = " ";
            string[] fromMonthYear = Regex.Split(date, pattern);

            string toMonth = fromMonthYear[0];
            string toYear = fromMonthYear[1];


            switch (toMonth)
            {
                case "Jan":
                    setToMonthByIndex(0);
                    break;

                case "Feb":
                    setToMonthByIndex(1);
                    break;

                case "Mar":
                    setToMonthByIndex(2);
                    break;

                case "Apr":
                    setToMonthByIndex(3);
                    break;

                case "May":
                    setToMonthByIndex(4);
                    break;

                case "Jun":
                    setToMonthByIndex(5);
                    break;

                case "Jul":
                    setToMonthByIndex(6);
                    break;

                case "Aug":
                    setToMonthByIndex(7);
                    break;

                case "Sep":
                    setToMonthByIndex(8);
                    break;

                case "Oct":
                    setToMonthByIndex(9);
                    break;

                case "Nov":
                    setToMonthByIndex(10);
                    break;

                case "Dec":
                    setToMonthByIndex(11);
                    break;
            }

            switch (toYear)
            {
                case "2015":
                    setToYearByIndex(0);
                    break;

                case "2014":
                    setToYearByIndex(1);
                    break;
            }

        }

        public void setFromMonthByIndex(int index)  // Index 1 in the ddl is the most recent month, 13 is the oldest
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_FromMonth"))).SelectByIndex(index);
        }

        public void setToMonthByIndex(int index) 
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_ToMonth"))).SelectByIndex(index);
        }

        public void setFromYearByIndex(int index)  // Index 1 in the ddl is the most recent year, 2 is the oldest
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_FromYear"))).SelectByIndex(index);
        }

        public void setToYearByIndex(int index)  
        {
            new SelectElement(driver.FindElement(By.Id("ctl00_BodyPlaceHolder_ToYear"))).SelectByIndex(index);
        }
    }
}
