﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;


namespace MAX_Selenium
{
    public class Merchandising_Page : Page
    {
        //element finders
        [FindsBy(How = How.CssSelector, Using = "body div.ui-widget-overlay")]
        public IWebElement MerchandisingMask;

        [FindsBy(How = How.CssSelector, Using = "body div.ui-dialog div.ui-dialog-buttonpane button.ui-state-default")]
        public IWebElement WebsitePDF_Widget_OK_Button;

        [FindsBy(How = How.CssSelector, Using = "body div#Container div#SellingSheetFields")]
        public IWebElement Sales_Packet_Panel;

        [FindsBy(How = How.CssSelector, Using = "body div#Container div#WebsitePdfFields")]
        public IWebElement MAXInfo_Displayed_Panel;

        [FindsBy(How = How.CssSelector, Using = "body div#Container select#PreviewNavigationForm_PreviewSheetDDL.previewSheetDDL")]
        public IWebElement Preview_Sheet_DDL;

        //constructor
        public Merchandising_Page(IWebDriver driver) : base(driver)
        {  

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.TitleContains("MAX Merchandising"));

            if (driver.Title != "MAX Merchandising") 
            {
                throw new StaleElementReferenceException("This is not the MAX Merchandising page.");
            }
            PageFactory.InitElements(driver, this);
	    }
  


    }
}
