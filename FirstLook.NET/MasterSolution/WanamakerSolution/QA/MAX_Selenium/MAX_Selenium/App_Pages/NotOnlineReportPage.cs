﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium
{
    public class NotOnlineReportPage : ReportPage
    {
        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_dealers")]
        public IWebElement SelectDealerDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_status")]
        public IWebElement StatusDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_inventoryTypes")]
        public IWebElement NewOrUsedDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_ageInDays")]
        public IWebElement MinAgeDDL;

        [FindsBy(How = How.Id, Using = "HeaderSearchText")] 
        public IWebElement SearchField;

        [FindsBy(How = How.Id, Using = "HeaderSearchButton")]
        public IWebElement SearchButton;

        public NotOnlineReportPage(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Not Online Report | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Not Online Report | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Not Online Report Page");
            }

            PageFactory.InitElements(driver, this);
        }

        public void SelectStatusFilter(StatusFilterType option)
        {
            SelectItemInDDL(StatusDDL, option);
        }
      
        public void SelectNewOrUsedFilter(NewOrUsedFilterType option)
        {
            SelectItemInDDL(NewOrUsedDDL, option);
        }

        public void SelectDealer(string dealerName)  // for example "Faulkner BMW"
        {
            var ddl = new SelectElement(SelectDealerDDL);
            if (ddl.SelectedOption.GetAttribute("value") != dealerName) 
            {
                ddl.SelectByText(dealerName);  
            }
        }
    }
}
