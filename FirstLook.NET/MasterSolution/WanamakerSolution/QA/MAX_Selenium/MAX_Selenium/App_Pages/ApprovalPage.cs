﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class ApprovalPage :Page
    {
        private bool acceptNextAlert = true;

        //element finders
        [FindsBy(How = How.LinkText, Using = "Home")] 
        public IWebElement HomeTab; // Behavior of this button changed in 15.0; now goes to Dashboard instead of Inventory Page

        [FindsBy(How = How.LinkText, Using = "Inventory")] 
        public IWebElement InventoryTab;

        [FindsBy(How = How.Id, Using = "PricingPageLink")]
        public IWebElement PricingPageTab;

        [FindsBy(How = How.Id, Using = "ApprovalLink")]
        public IWebElement ApprovalPageTab;

        //you shouldn't have to access the workflow menus from Approval page only
        //[FindsBy(How = How.CssSelector, Using = "body div#WorkflowHeader.clearfix a#MAXSellingLink")]
        //public IWebElement MaxMerchandisingTab;

        [FindsBy(How = How.Id, Using = "BtnAutoLoad")] 
        public IWebElement AutoLoadButton;

        [FindsBy(How = How.Id, Using = "RewriteAdButton")]
        public IWebElement RegenerateAdButton;

        [FindsBy(How = How.Id, Using = "approveButton")]
        public IWebElement ApprovalButton;

        [FindsBy(How = How.CssSelector, Using = "div#ApprovalModalPanel a#btnApproveSuccessfulNo")]
        public IWebElement ApprovalSuccessfulNo_Button;

        [FindsBy(How = How.Id, Using = "wfsInventoryItem_ApprovalLink")]
        public IWebElement ApprovalTab;

        [FindsBy(How = How.Id, Using = "LoadEquipmentLink")] 
        public IWebElement EquipmentPageTab;

        [FindsBy(How = How.Id, Using = "LoadPackagesLink")]
        public IWebElement OptionPackagesTab;

        [FindsBy(How = How.Id, Using = "priceLnk")]
        public IWebElement RepriceLink;

        [FindsBy(How = How.Id, Using = "headerListPrice")]
        public IWebElement InternetPriceValue;

        [FindsBy(How = How.Id, Using = "NewCarListPrice")]
        public IWebElement NewCarListingPrice;

        [FindsBy(How = How.Id, Using = "NewCarRepriceButton")]
        public IWebElement NewCarRepriceButton;

        [FindsBy(How = How.XPath, Using = "//span[@onclick=\"_gaq.push(['_trackEvent', 'Quick Packages', 'Click', 'View Available Packages']);\"]")]
        public IWebElement ViewAvailablePackages;

        [FindsBy(How = How.Id, Using = "LblTitle")]
        public IWebElement PackageLableName;

        [FindsBy(How = How.Id, Using = "Module0")]
        public IWebElement ADpreviewSection;

        [FindsBy(How = How.Id, Using = "EditModule0CancelButton")]
        public IWebElement PreviewCancelButton_0;

        [FindsBy(How = How.Id, Using = "AddModuleLink")]
        public IWebElement AddSectionLink;

        [FindsBy(How = How.XPath, Using = ".//*[@id='Advertisement']")]
        public IWebElement WholeAddSection;

        [FindsBy(How = How.Id, Using = "AdditionalInfoItemsList_1")]
        public IWebElement KeyInformationNonSmoker;

        [FindsBy(How = How.Id, Using = "ColorsHeader")] 
        public IWebElement Colors;

        [FindsBy(How = How.Id, Using = "ExteriorColorSelectionLabel")] 
        public IWebElement ExteriorColor;

        [FindsBy(How = How.Id, Using = "ExteriorColorDropDown")] 
        public IWebElement ExteriorColorDDL;

        [FindsBy(How = How.Id, Using = "InteriorColorDropDown")] 
        public IWebElement InteriorColorDDL;

        [FindsBy(How = How.CssSelector, Using = "#ApprovalAgreement")]
        public IWebElement ApprovalAgreement;

        [FindsBy(How = How.XPath, Using = ".//*[@id='LastAdWindowLink']")]
        public IWebElement LastAdPublishedLink;

        [FindsBy(How = How.Id, Using = "DateAdPublished")] 
        public IWebElement DateAdPublished;

        [FindsBy(How = How.Id, Using = "KeyInformationHeader")]
        public IWebElement KeyInformationHeader;

        [FindsBy(How = How.Id, Using = "QuickPackagesHeader")]
        public IWebElement PackagesHeader;

        [FindsBy(How = How.Id, Using = "warning")]
        public IWebElement NewCarPricingWarningMessage;

        [FindsBy(How = How.Id, Using = "stockNumber")]
        public IWebElement EStockCardLink;

        [FindsBy(How = How.Id, Using = "TrimStyleDropDown")]
        public IWebElement TrimStyleDropDown;

        [FindsBy(How = How.Id, Using = "headerYearMakeModel")]
        public IWebElement HeaderYearMakeModelLabel;

        [FindsBy(How = How.Id, Using = "VehicleSummary")]
        public IWebElement VehicleSummary;

        [FindsBy(How = How.Id, Using = "chkCertified")] //TODO obsolete needs to be removed
        public IWebElement CertifiedCheckBox;

        [FindsBy(How = How.Id, Using = "txtCertifiedID")]
        public IWebElement CertifiedIdTextField;

        [FindsBy(How = How.XPath, Using = ".//*[@id='CertifiedCheckboxItem']/label")] 
        public IWebElement CertifiedLabel;

        [FindsBy(How = How.Id, Using = "FrameworkSelectorDDL")]
        public IWebElement FrameWorkDropDown;

        [FindsBy(How = How.CssSelector, Using = "#AdTemplate .module.adPreview")] 
        public IWebElement PreviewSection;

        [FindsBy(How = How.CssSelector, Using = "#PreviewLengthWarning > p")] 
        public IWebElement PreviewLengthWarning;    

        [FindsBy(How = How.Id, Using = "lblConditionReport")] 
        public IWebElement OptionsAndStandardEquipment;

        [FindsBy(How = How.CssSelector, Using = "#mapped>h3")] 
        public IWebElement Data_From_Lot_Provider_Header;

        [FindsBy(How = How.CssSelector, Using = "#ListingOpts>tbody>tr>td>label")] 
        public IWebElement Lot_Provider_Data_Label;

        [FindsBy(How = How.Id, Using = "vin")] 
        public IWebElement VIN;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog td.first input#ChkAdd")]
        public IWebElement PackagesDialogFirst_Checkbox;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog input#BtnAdd")]
        public IWebElement PackagesDialogAdd_Button;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog a.ui-dialog-titlebar-close.ui-corner-all")]
        public IWebElement PackagesDialogClose_Button;

        [FindsBy(How = How.XPath, Using = ".//*[@id='mapped']/h3")]
        public IWebElement Lot_Provider;

        //drop down lists
        [FindsBy(How = How.Id, Using = "ddlCertifiedPrograms")]
        public IWebElement certified_Programs_ddl_bud;
        public SelectElement Certified_Programs_DDL; //cannot instantiate in the Constructor, b/c element isn't there for New cars.


        public ApprovalPage(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Ad Approval | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Ad Approval | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Approval Page");
            }

            PageFactory.InitElements(driver, this);
        }

        public ApprovalPage GetApprovalPage()
        {
            ApprovalTab.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Ad Approval | MAX : Online Inventory. Perfected.")); 

            return new ApprovalPage(driver);
        }

        public EquipmentPage Get_Equipment_Page()
        {
            EquipmentPageTab.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Load Equipment | MAX : Online Inventory. Perfected.")); 

            return new EquipmentPage(driver);
        }

        public void Open_No_Packages_Header()
        {
            if (ViewAvailablePackages.Displayed == false)
            {
                PackagesHeader.Click();
            }
        }

        public void Open_Key_Information_Header()
        {
            if (KeyInformationNonSmoker.Displayed == false)
            {
                KeyInformationHeader.Click();
            }
        }

        public void Open_Colors_Header()
        {
            if (ExteriorColor.Displayed == false)
            {
                Colors.Click();
            }
        }

        public void Open_Options_Standard_Equipment_Header()
        {
            if (Data_From_Lot_Provider_Header.Displayed == false)
            {
                OptionsAndStandardEquipment.Click();
                Assert.IsTrue(Lot_Provider.Text.Contains("Data From Lot Provider"));
            }
        }

        public bool ModuleContainsText(ISearchContext moduleElement, string textToMatch)
        {
            return moduleElement.
                FindElements(By.CssSelector(".moduleContent .header"))
                .First()
                .Text == textToMatch;
        }

        public void RepriceThroughPingPage(String newPrice)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            RepriceLink.Click();
            wait.Until((d) => driver.WindowHandles.Count > 1);
            Thread.Sleep(1000);  
            driver.SwitchTo().Window(driver.WindowHandles[1]);
            wait.Until(
                ExpectedConditions.ElementIsVisible(
                    By.CssSelector("div#container.cf fieldset#calculator.pricing_calculator input#Text2")));

            Thread.Sleep(1000);
            driver.FindElement(By.XPath(".//*[@id='Text2']")).Clear();
            driver.FindElement(By.XPath(".//*[@id='Text2']")).SendKeys(newPrice);
            driver.FindElement(By.XPath(".//fieldset[@id='calculator']//button[@name='save_price']")).Click();
            WaitForAjax(15.0M);
           // driver.SwitchTo().Window(driver.WindowHandles[1]).Close();
            driver.Close();
            Thread.Sleep(1000);          
            driver.SwitchTo().Window(driver.WindowHandles[0]);
           driver.Navigate().Refresh();
            WaitForAjax(20.0M);

         }

        public void AddSection(String sectionHeaderName)
        {
            AddSectionLink.Click();
            driver.FindElement(By.Id("AddModuleHeaderText")).Clear();
            driver.FindElement(By.Id("AddModuleHeaderText")).SendKeys(sectionHeaderName);
            driver.FindElement(By.Id("AddModuleParagraphText")).Clear();
            driver.FindElement(By.Id("AddModuleParagraphText")).SendKeys("webdriver rocks");
            driver.FindElement(By.XPath("html/body/div[3]/div[11]/div/button[1]")).Click();
            RegenerateAdButton.Click();

            //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));

            if (IsPriceLessThan200())
                RepriceThroughPingPage("5000");   
            Approve_AD();
        }

        public bool IsPriceLessThan200()
        {
            var temp = Utility.ExtractDigitsFromString(InternetPriceValue.Text);
            if (temp < 200)
                return true;
            return false;
        }

        public void Approve_AD()
        {
            //RegenerateAdButton.SendKeys(Keys.Return);
            RegenerateAdButton.Click();
            try //don't panic if page loaded before Wait began
            {
                WaitForAjax();
            }
            catch (TimeoutException)
            {               
            }

            Thread.Sleep(500);
            WaitForAjax();
            //Thread.Sleep(2000);
            ApprovalButton.Click();
            WebDriverWait waitx = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            waitx.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div#ApprovalModalPanel a#btnApproveSuccessfulNo")));
            ApprovalSuccessfulNo_Button.Click();
            Thread.Sleep(500);
        }

        public void ClickApprovalSuccessfulNo_Button()
        {

            driver.FindElement(By.Id("btnApproveSuccessfulNo")).Click();
        }

        public IWebElement FindSection(String sectionHeader)
        {
            return driver.FindElements(By.CssSelector(".module"))
                        .Single(moduleElement =>
                          ModuleContainsText(moduleElement, sectionHeader)); // this is Linq
        }

        public void DeleteSection(IWebElement module)
        {
            var deleteButton = module.FindElement(By.CssSelector(".moduleLinks .deleteModuleLink")); //delete button is by context (not distinct) so OK to have here
            Make_Edit_Delete_Button_Visible();
            deleteButton.Click();
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
            Approve_AD();
        } 

        public void Make_Edit_Delete_Button_Visible()
        {
            const string script = @"
            function AddStyles(styleText) {
                var head = document.getElementsByTagName('head')[0],
                    style = document.createElement('style'),
                    rules = document.createTextNode(styleText);

                style.type = 'text/css';
                if(style.styleSheet)
                    style.styleSheet.cssText = rules.nodeValue;
                else style.appendChild(rules);
                head.appendChild(style);
            }
            AddStyles('#AdTemplate .moduleLinks { visibility: visible; }');
            ";
            var js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(script);
        }

        public void Input_Long_Text_Into_Preview_Edit_Mode(IWebDriver dr, String st)
        {
            int i = 0;

            Make_Edit_Delete_Button_Visible();
            dr.FindElement(By.Id("Module0EditLink")).Click();

            try
            {
                while (dr.FindElement(By.XPath("//*[@id=\"Module0Blurb" + i + "Textbox\"]")).Displayed)
                    {
                        dr.FindElement(By.XPath("//*[@id=\"Module0Blurb" + i + "Textbox\"]")).Clear();
                        i++;
                    }
            }
            catch (NoSuchElementException)
            {    
            }

            dr.FindElement(By.XPath("//*[@id=\"Module0Blurb0Textbox\"]")).SendKeys(st);  

            //need to have the logic to determine how many Module0Blurbs out there!!!!!!!!!!!!!
            //Make_Edit_Delete_Button_Visible();
            //dr.FindElement(By.Id("Module0EditLink")).Click();
            //if (dr.FindElement(By.Id("Module0Blurb0Textbox")).Displayed)
            //    dr.FindElement(By.Id("Module0Blurb0Textbox")).Clear();
            //if (dr.FindElement(By.Id("Module0Blurb1Textbox")).Displayed)
            //    dr.FindElement(By.Id("Module0Blurb1Textbox")).Clear();
            //if (dr.FindElement(By.Id("Module0Blurb2Textbox")).Displayed)
            //    dr.FindElement(By.Id("Module0Blurb2Textbox")).Clear();
            //if (dr.FindElement(By.Id("Module0Blurb3Textbox")).Displayed)
            //    dr.FindElement(By.Id("Module0Blurb3Textbox")).Clear();

            
        }

        public void GoTo_InventoryPage()
        {
            InventoryTab.Click();
            CloseNaggingWindow();
        }

        public void Remove_Packages_In_MicroWorkFlow()
        {
            for (int i = 1; i < 10; i++)
            {

                IWebElement temp_element = null;
                try
                {
                    temp_element =
                        driver.FindElement(
                            By.CssSelector(String.Format("#pnlPackages > ul > li:nth-child({0}) #BtnDelete", i)));
                }
                catch (NoSuchElementException e)
                {
                    break;
                }
                if (temp_element.Displayed)
                {
                    temp_element.Click();
                    Thread.Sleep(3000);
                    //WaitForElementToDisappear(temp_element);
                }
                else break;
            }
        }

        public void Reset_Colors_In_MicroWorkFlow()
        {
            new SelectElement(driver.FindElement(By.Id("ExteriorColorDropDown"))).SelectByText("Choose a color...");
            new SelectElement(driver.FindElement(By.Id("InteriorColorDropDown"))).SelectByText("Choose a color...");
        }

        public bool Is_Certified_CheckBox_Checked()
        {
            if (CertifiedCheckBox.Selected)
                return true;
            return false;
        }

        public void Check_Certified_CheckBox()
        {
            if (CertifiedCheckBox.Selected == false)            
                CertifiedCheckBox.Click();            
        }

        public void UnCheck_Certified_CheckBox()
        {
            if (CertifiedCheckBox.Selected)
                CertifiedCheckBox.Click();
        }

        public void Modify_Certified_ID(string NewId)
        {
            CertifiedIdTextField.Clear();
            CertifiedIdTextField.SendKeys(NewId);
            //CertifiedIdTextField.Submit();
        }

        public string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                
                
                if (acceptNextAlert)
                {
                    string alertText = alert.Text;
                    alert.Accept();
                    return alertText;
                }
                else
                {
                    alert.Dismiss();
                }
                //return alert.Text;              
            }
            finally
            {
                acceptNextAlert = true;
            }
            return null;
        }

        public static int GetTotalCountFromWokflowWebElement(IWebElement element)
        {
            String input = element.Text;
            String pattern = " ";
            string[] substrings = Regex.Split(input, pattern);
            int totalCount = Convert.ToInt32(substrings[2]);
            return totalCount;
        }

        
    }
 }
