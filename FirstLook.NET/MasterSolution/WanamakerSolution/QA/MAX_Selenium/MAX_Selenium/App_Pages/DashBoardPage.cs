﻿using System;
using System.Data;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium
{
    public class DashBoardPage : Page
    {
        [FindsBy(How = How.CssSelector, Using = "#MaxAdLink")]
        public IWebElement BigMaxButton;

        [FindsBy(How = How.PartialLinkText, Using = "Not Online (")]
        public IWebElement NotOnlineLink;

        [FindsBy(How = How.PartialLinkText, Using = "No Price")]
        public IWebElement NoPriceLink;

        [FindsBy(How = How.PartialLinkText, Using = "No Description")]
        public IWebElement NoDescriptionLink;

        [FindsBy(How = How.PartialLinkText, Using = "No Photos")]
        public IWebElement NoPhotosLink;

        [FindsBy(How = How.PartialLinkText, Using = "No Trim")]
        public IWebElement NoTrimLink;

        [FindsBy(How = How.PartialLinkText, Using = "Needs Re-pricing")]
        public IWebElement NeedsRepricingLink;

        [FindsBy(How = How.PartialLinkText, Using = "Equipment Review")]
        public IWebElement EquipmentReviewLink;

        [FindsBy(How = How.PartialLinkText, Using = "Low Online Activity")]
        public IWebElement LowOnLineActivityLink;

        [FindsBy(How = How.PartialLinkText, Using = "No Packages")]
        public IWebElement NoPackagesLink;

        [FindsBy(How = How.PartialLinkText, Using = "No Book Value")]
        public IWebElement NoBookValueLink;

        [FindsBy(How = How.PartialLinkText, Using = "No Carfax")]
        public IWebElement NoCarFaxLink;

        [FindsBy(How = How.PartialLinkText, Using = "Low Photos")]
        public IWebElement LowPhotosLink;

        [FindsBy(How = How.PartialLinkText, Using = "Not Online")] 
        public IWebElement NotOnlineReportLink;

        [FindsBy(How = How.PartialLinkText, Using = "Vehicles Marked Offline")] 
        public IWebElement VehiclesMarkedOffline;

        [FindsBy(How = How.CssSelector, Using = "#NotOnline > span.foreground > span.label")] 
        public IWebElement NotOnlineGraph;

        [FindsBy(How = How.Id, Using = "startDate")] 
        public IWebElement FromMonth_DDL;

        [FindsBy(How = How.Id, Using = "endDate")]
        public IWebElement ToMonth_DDL;

        [FindsBy(How = How.XPath, Using = ".//*[@id='WebsitePerformance']/div[1]/div[1]/span")] 
        public IWebElement Toggle_On_DashBoard;

        [FindsBy(How = How.LinkText, Using = "Inventory")]
        public IWebElement Inventory_Tab;

        [FindsBy(How = How.LinkText, Using = "Reports")]
        public IWebElement Reports_Tab;

        [FindsBy(How = How.LinkText, Using = "Performance Summary")]
        public IWebElement Performance_Summary_Reports_InDDL;

        [FindsBy(How = How.LinkText, Using = "MAX Dashboard")]
        public IWebElement Executive_Dashboard_InDDL;

        [FindsBy(How = How.LinkText, Using = ("Log Out"))] 
        public IWebElement LogOut;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_ttm']")]
        public IWebElement Time_to_Market_tab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_perf']")]
        public IWebElement Online_Classified_Performance_tab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_impression']")]
        public IWebElement CostPerConsumerImpressions_ClassifiedPerformance_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_lead']")]
        public IWebElement CostPerDirectLead_ClassifiedPerformance_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_trends']")]
        public IWebElement Classified_Trends_tab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_vdp_trend']")]
        public IWebElement ConsumerImpressions_ClassifiedTrends_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_costper_vdp_trend']")]
        public IWebElement CostPerConsumerImpressions_ClassifiedTrends_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_lead_trend']")]
        public IWebElement DirectLeads_ClassifiedTrends_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_costper_lead_trends']")]
        public IWebElement CostPerDirectLeads_ClassifiedTrends_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_ga']")]
        public IWebElement Website_Traffic_tab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_sources']")]
        public IWebElement TrafficSources_WebsiteTraffic_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_ga_trends']")]
        public IWebElement TrafficTrends_WebsiteTraffic_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_mobile']")]
        public IWebElement MobileTraffic_WebsiteTraffic_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_show']")]
        public IWebElement Showroom_and_Website_tab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_dspvt']")]
        public IWebElement Digital_S_Pageviews_Traffic_ShowroomWebsite_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_wst']")]
        public IWebElement Website_20_Traffic_ShowroomWebsite_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges div#WebsitePerformance a[href='#tab_wsl']")]
        public IWebElement Website_20_Leads_ShowroomWebsite_subtab;

        [FindsBy(How = How.CssSelector, Using = "#MerchandisingAlertGauge > span.foreground > span.label > span")]
        public IWebElement Merchandising_Alert_Gauge;

        [FindsBy(How = How.CssSelector, Using = "#MerchandisingAlertGauge > span.foreground > span.count > span")]
        public IWebElement Merchandising_Alert_Gauge_Count;

        [FindsBy(How = How.CssSelector, Using = "#MerchandisingAlertGauge > span.foreground > span.percentage > span")]
        public IWebElement Merchandising_Alert_Gauge_Percentage;

        [FindsBy(How = How.CssSelector, Using = "div#WebsitePerformance.chart_container a#vdp_trend_details")]
        public IWebElement vdp_trend_details;

        [FindsBy(How = How.CssSelector, Using = "div#Gauges a#ttm_details")]
        public IWebElement TTM_ViewDetails_Link;

        [FindsBy(How = How.XPath, Using = "(//a[contains(@href,'merchandising/Workflow/RedirectToInventoryItem.aspx')])[1]")]
        public IWebElement TTM_First_Clickable_Car_Link;

        [FindsBy(How = How.CssSelector, Using = "#chart_ttm > div > div + div > div > div > div:nth-child(1) > span > span")]
        public IWebElement AvgDaysToOnlineWithPhoto_Graph;

        [FindsBy(How = How.CssSelector, Using = "#chart_ttm > div > div + div > div > div > div:nth-child(2) > span > span")]
        public IWebElement AvgDaysToCompleteAdOnline_Graph;

        [FindsBy(How = How.XPath, Using = ".//*[@id='mCSB_2']/div[1]/span[1]/table/tbody/tr[2]/td[7]")]
        public IWebElement TTM_Stock;

        [FindsBy(How = How.XPath, Using = ".//*[@id=\"chart_stats\"]/div[2]/div[1]/span/span[2]")]
        public IWebElement OnlineClassifiedOverview_CTR_Cars;

        [FindsBy(How = How.XPath, Using = ".//*[@id=\"chart_stats\"]/div[2]/div[1]/span/span[4]")]
        public IWebElement OnlineClassifiedOverview_CTR_AutoTrader;

        [FindsBy(How = How.XPath, Using = ".//*[@id=\"chart_stats\"]/div[2]/div[2]/span/span[2]")]
        public IWebElement OnlineClassifiedOverview_ConversionRate_Cars;

        [FindsBy(How = How.XPath, Using = ".//*[@id=\"chart_stats\"]/div[2]/div[2]/span/span[4]")]
        public IWebElement OnlineClassifiedOverview_ConversionRate_AutoTrader;


        //drop down lists. Must be instantiated in the constructor.
        [FindsBy(How = How.CssSelector, Using = "div#InventoryType select#InventoryTypeDDL")]
        protected IWebElement inventoryType_DDL_bud;
        public SelectElement InventoryType_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.chart-subheader div select#startDate")]
        protected IWebElement fromDate_DDL_bud;
        public SelectElement FromDate_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.chart-subheader div select#endDate")]
        protected IWebElement toDate_DDL_bud;
        public SelectElement ToDate_DDL;

        //constructor
        public DashBoardPage (IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Dashboard | MAX : Intelligent Online Advertising Systems")); 

            if (driver.Title != "Dashboard | MAX : Intelligent Online Advertising Systems")
            {
                throw new Exception("This is not the Dashboard Page");
            } 

            PageFactory.InitElements(driver, this);
            CloseNaggingWindow();
            InventoryType_DDL = new SelectElement(inventoryType_DDL_bud);
            FromDate_DDL = new SelectElement(fromDate_DDL_bud);
            ToDate_DDL = new SelectElement(toDate_DDL_bud);

        }

        public PerformanceSummaryReportPage Goto_Performance_Report()
        {
            Reports_Tab.Click();
            Performance_Summary_Reports_InDDL.Click();
            return new PerformanceSummaryReportPage(driver);
        }

        public void Set_From_Month_ToJanuary()
        {
            Set_From_Month("Jan 2013");
        }

        public void Set_To_Month_ToMay()
        {
            Set_To_Month("May 2013");
        }

        public string Get_Default_To_Month()
        {
            var ddl = new SelectElement(ToMonth_DDL);
            return ddl.SelectedOption.GetAttribute("value");
        }

        public void Set_From_Month(string month)
        {
            var ddl = new SelectElement(FromMonth_DDL);
            if (ddl.SelectedOption.GetAttribute("value") != month)
            {
                ddl.SelectByText(month);
            }
        }

        public void Set_To_Month(string month)
        {
            var ddl = new SelectElement(ToMonth_DDL);
            if (ddl.SelectedOption.GetAttribute("value") != month)
            {
                ddl.SelectByText(month);
            }
        }

       
        public Month Convert_Month_From_DigitString_To_Name(string mon)
        {
            string Current_Month ="";

            if (mon == "1/2013")
            {
                Current_Month = "January";
            }
            else if (mon == "2/2013")
            {
                Current_Month = "February";
            }
            else if (mon == "3/2013")
            {
                Current_Month = "March";
            }
            else if (mon == "4/2013")
            {
                Current_Month = "April";
            }
            else if (mon == "5/2013")
            {
                Current_Month = "May";
            }
            else if (mon == "6/2013")
            {
                Current_Month = "June";
            }
            else if (mon == "7/2013")
            {
                Current_Month = "July";
            }
            else if (mon == "8/2013")
            {
                Current_Month = "August";
            }
            else if (mon == "9/2013")
            {
                Current_Month = "September";
            }
            else if (mon == "10/2013")
            {
                Current_Month = "October";
            }
            else if (mon == "11/2013")
            {
                Current_Month = "November";
            }
            else if (mon == "12/2013")
            {
                Current_Month = "December";
            }

            return (Month)Enum.Parse(typeof(Month), Current_Month);
        }


        public int Get_Monthly_Cost(String vendor) //vendor is "AutoTrader.com" or "Cars.com"
        {
            var costText = driver.FindElement(By.XPath("//div[@id='tab_impression']//div[@class='chart-footer vendor-budgets']" +
                                                       "//li[span[text()='" + vendor + "']]//span[2]")).Text;

            var costInt = Utility.ExtractDigitsFromString(costText);
            return costInt;

        }

        public int Get_Num_Of_Impressions(String vendor)  //this method will return the number of impressions as an int
        {
            int position = 1;
            int impressions = 0;
            IWebElement VendorElementIsPresent = driver.FindElement(By.CssSelector("#chart_cv_chart .highcharts-axis-labels text:nth-child(1)"));

            while (VendorElementIsPresent.Displayed)
            {
                var FindVendor = driver.FindElement(By.CssSelector("#chart_cv_chart .highcharts-axis-labels text:nth-child(" + position + ")"));
                var VendorIsPresent = FindVendor.Text;

                if (VendorIsPresent.Equals(vendor))
                {
                    impressions = Utility.GetDigitsFromElement(driver.FindElement(By.CssSelector("#chart_cv_chart .highcharts-data-labels > g:nth-child(" + position + ") > text > tspan")));
                    break;
                }
                position++;

            }
            return impressions;
        }

        public decimal Get_Avg_Cost_Per_Impression(String vendor)
        {
            int position = 1;
            decimal VDPperImpression = 0;
            IWebElement VendorElementIsPresent = driver.FindElement(By.CssSelector("#chart_cpv_chart .highcharts-axis-labels > text:nth-child(1)"));

            while (VendorElementIsPresent.Displayed)
            {
                IWebElement FindVendor = driver.FindElement(By.CssSelector("#chart_cpv_chart .highcharts-axis-labels > text:nth-child(" + position + ") "));
                String VendorIsPresent = FindVendor.Text;

                if (VendorIsPresent.Equals(vendor))
                {
                    VDPperImpression = (Utility.GetDigitsFromElement(driver.FindElement(By.CssSelector("#chart_cpv_chart .highcharts-stack-labels > text:nth-child(" + position + ")")))) / 100m;
                    break;
                }
                position++;

            }
            return VDPperImpression;
        }

        public void GoTo_Admin_Misc_Tab()
        {
            Admin.Click();
            Admin_Home.Click();
            WaitForAjax();
        }

        //following two methods should be in misc settings page class.
        public void Enable_OnlineClassifiedOverview()
        {
            Admin_Miscellaneous_Page showOnlineClassifiedOverview = new Admin_Miscellaneous_Page(driver);
            showOnlineClassifiedOverview.Check_Show_Online_Classified_Overview();
            showOnlineClassifiedOverview.BackToMaxAdLink.Click();
        }

        public void Disable_OnlineClassifiedOverview()
        {
            Admin_Miscellaneous_Page showOnlineClassifiedOverview = new Admin_Miscellaneous_Page(driver);
            showOnlineClassifiedOverview.Uncheck_Show_Online_Classified_Overview();
            showOnlineClassifiedOverview.BackToMaxAdLink.Click();
        }

        public int Get_Monthly_Cost_From_DB(String date, int bu, String vendor)        
        {
            //date format: yyyy-mm-dd
            //Vendor is "Cars.com" or "AutoTrader.com"

            int destinationid = 0;
            if(vendor == "AutoTrader.com")
            {
                destinationid = 2;
            }
            else if(vendor == "Cars.com")
            {
                destinationid = 1;
            }

            String query =
                @"SELECT MAX(budget)
                FROM [Merchandising].[settings].[SiteBudget] sb
                FULL JOIN Merchandising.settings.EdtDestinations ed ON sb.destinationid = ed.destinationid
                FULL JOIN imt.dbo.BusinessUnit bu ON sb.businessunitid = bu.businessunitid
                WHERE sb.businessunitid = " + bu + " AND monthapplicable <= '" + date + "' AND sb.destinationid = " + destinationid + " ";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            int monthly_cost = Utility.ExtractDigitsFromString(dr.ItemArray[0].ToString());

            return monthly_cost;
        }

        public int Get_Impressions_From_Trends_Report(String vendor)
        {
            String vendor_vdp = vendor + " VDP";
            String site1 = (driver.FindElement(By.CssSelector("div#vdp_trend_report th.header + th"))).Text;
            String site2 = (driver.FindElement(By.CssSelector("div#vdp_trend_report th.header + th + th"))).Text;
            
            if(vendor_vdp == site1)
            {
                int impressions = Utility.ExtractDigitsFromString((driver.FindElement(By.XPath("//*[@id=\"mCSB_1\"]/div[1]/span[1]/table/tbody/tr/td[3]/div/div"))).Text);
                return impressions;
            }
            else if (vendor_vdp == site2)
            {
                int impressions = Utility.ExtractDigitsFromString((driver.FindElement(By.XPath("//*[@id=\"mCSB_1\"]/div[1]/span[1]/table/tbody/tr/td[4]/div/div"))).Text);
                return impressions;
            }
            else
            {
                return 0;
            }
        }

        public void setFromMonthByIndexInDDL(int monthIndex)  // Index 1 in the ddl is the most recent month, 13 the oldest
        {
            new SelectElement(driver.FindElement(By.Id("startDate"))).SelectByIndex(monthIndex);
        }

        public void setToMonthByIndexInDDL(int monthIndex)  // Index 1 in the ddl is the most recent month, 13 the oldest
        {
            new SelectElement(driver.FindElement(By.Id("endDate"))).SelectByIndex(monthIndex);
        }

        public void verifyCheckboxFuntionality(IWebElement checkbox)
        {
            if (checkbox.Selected)
            {
                checkbox.Click();
                Assert.IsFalse(checkbox.Selected);
            }
            else
            {
                checkbox.Click();
                Assert.IsTrue(checkbox.Selected);
            }
        }

        public static String RandomDealerChooser_Dashboard()
        {
            String[] dealerNames = { "Kenny Ross Ford Adamsburg", 
                                     //"Wetzel Chevy", Low Online Activity not checked in Alerts.; causes tests to fail if used.
                                     "Performance Ford",
                                     "Voss Hyundai",
                                     "All Star Chevrolet",
                                     "BMW of Murrieta",
                                     "Boch Honda",
                                     "Rick Hendrick City Chevrolet"};
            //All these stores have TTM and DPA enabled, impression data

            Random ran = new Random();
            return dealerNames[ran.Next(dealerNames.Length)];
        }

        public string getFromMonthYear(int index)
        {
            index++;
            string date = driver.FindElement(By.XPath("//*[@id=\"startDate\"]/option[" + index + "]")).Text;

            return date;
        }

        public string getToMonthYear(int index)
        {
            index++;
            string date = driver.FindElement(By.XPath("//*[@id=\"endDate\"]/option[" + index + "]")).Text;

            return date;
        }


    }
}
