﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium
{
    public class MaxPricingPage : Page
    {
        public static String MaxPricingPage_Title = "PingOne | MAX : Online Inventory. Perfected.";

        //finders
        //[FindsBy(How = How.CssSelector, Using = "#selectbox")]
        //public IWebElement HeaderTrimDropdown;


        //1stdropdown xpath
        [FindsBy(How = How.XPath, Using = ".//*[@id='selectbox']//option[2]")]
        public IWebElement HeaderTrimDropdownValue;

        //Definite Competive Filters and thier Default values locators
        [FindsBy(How = How.CssSelector, Using = "#lblListing")]
        public IWebElement ListingLabel;

        [FindsBy(How = How.CssSelector, Using = "#listSpan")]
        public IWebElement ListingDefaultValue;

        [FindsBy(How = How.CssSelector, Using = "#lblDistance")]
        public IWebElement DistanceLabel;

        [FindsBy(How = How.CssSelector, Using = "#distanceSpan")]
        public IWebElement DistanceDefaultValue;

        [FindsBy(How = How.CssSelector, Using = "#lblCertified")]
        public IWebElement CertifiedLabel;

        [FindsBy(How = How.XPath, Using = ".//*[@id='certifiedSpan']/ul/li[1]")]
        public IWebElement CertifiedDefaultValue;

        [FindsBy(How = How.XPath, Using = ".//*[@id='certifiedSpan']/ul/li[2]")]
        public IWebElement CertifiedDefaultValue1;

        [FindsBy(How = How.CssSelector, Using = "#lblTrim")]
        public IWebElement TrimLabel;

        [FindsBy(How = How.CssSelector, Using = "#trimSpan")]
        public IWebElement TrimDefaultValue;

        [FindsBy(How = How.CssSelector, Using = "#lblTransmission")]
        public IWebElement TransmissionLabel;

        [FindsBy(How = How.CssSelector, Using = "#transmissionSpan")]
        public IWebElement TransmissionDefaultValue;


        [FindsBy(How = How.CssSelector, Using = "#lblDriveTrain")]
        public IWebElement DriveTrainLabel;

        [FindsBy(How = How.CssSelector, Using = "#driveTrainSpan")]
        public IWebElement DriveTrainDefaultValue;

        [FindsBy(How = How.CssSelector, Using = "#lblEngine")]
        public IWebElement EngineLabel;

        [FindsBy(How = How.CssSelector, Using = "#engineSpan")]
        public IWebElement EngineDefaultValue;

        [FindsBy(How = How.CssSelector, Using = "#lblFuel")]
        public IWebElement FuelLabel;

        [FindsBy(How = How.CssSelector, Using = "#fuelSpan")]
        public IWebElement FuelDefaultValue;


        //DistancePopOver values
        [FindsBy(How = How.XPath, Using = ".//*[@id='distance']/ul/li[1]")]
        public IWebElement DistanceValue1;

        [FindsBy(How = How.XPath, Using = ".//*[@id='250']")]
        public IWebElement DistanceRadio1;

         [FindsBy(How = How.XPath, Using = ".//*[@id='distance']/input")]
        public IWebElement DistanceUpdate;

        [FindsBy(How = How.CssSelector, Using = "#distanceClose")]
        public IWebElement DistanceClose;

        [FindsBy(How = How.XPath, Using = ".//*[@id='500']")]
        public IWebElement DistanceValue2;

        [FindsBy(How = How.XPath, Using = ".//*[@id='650']")]
        public IWebElement DistanceValue3;

        //Listing Popover values
        [FindsBy(How = How.XPath, Using = ".//*[@id='Recent']")]
        public IWebElement ListingRecent;

        [FindsBy(How = How.XPath, Using = ".//*[@id='list']/ul/li[1]")]
        public IWebElement ListingRecentText;

        [FindsBy(How = How.XPath, Using = ".//*[@id='list']/ul/li[2]")]
        public IWebElement ListingActiveText;

        [FindsBy(How = How.XPath, Using = ".//*[@id='Active']")]
        public IWebElement ListingActive;

        [FindsBy(How = How.XPath, Using = ".//*[@id='list']/input")]
        public IWebElement ListingUpdate;

        //Cetified Popover values
        [FindsBy(How = How.XPath, Using = ".//*[@id='Certified']")]
        public IWebElement CertifiedCheck;

        [FindsBy(How = How.XPath, Using = ".//*[@id='certified']/ul/li[1]")]
        public IWebElement CertifiedText;
        
        [FindsBy(How = How.XPath, Using = ".//*[@id='NotCertified']")]
        public IWebElement NotCertifiedCheck;

        [FindsBy(How = How.XPath, Using = ".//*[@id='certified']/ul/li[2]")]
        public IWebElement NotCertifiedText;

        [FindsBy(How = How.XPath, Using = ".//*[@id='certified']/input")]
        public IWebElement CertifiedUpdate;
       
        //Trim Popover values
        [FindsBy(How = How.XPath, Using = ".//*[@id='Trim1']")]
        public IWebElement TrimValue1;

        [FindsBy(How = How.XPath, Using = ".//*[@id='Trim2']")]
        public IWebElement TrimValue2;

        [FindsBy(How = How.XPath, Using = ".//*[@id='trim']/input")]
        public IWebElement TrimUpdate;

        [FindsBy(How = How.XPath, Using = ".//*[@id='Trim3']")]
        public IWebElement TrimValue3;

        [FindsBy(How = How.XPath, Using = ".//*[@id='trim']/span[2]")]
        public IWebElement TrimErrorMessage;
        
        //Transmission Popover values
        [FindsBy(How = How.XPath, Using = ".//*[@id='ttt']")]
        public IWebElement TransmissionValue1;

        [FindsBy(How = How.XPath, Using = "//*[@id='dtdd']")]
        public IWebElement TransmissionValue2;

        [FindsBy(How = How.XPath, Using = ".//*[@id='transmission']/input")]
        public IWebElement TransmissionUpdate;


        [FindsBy(How = How.XPath, Using = ".//*[@id='transmission']/span[2]")]
        public IWebElement TransmissionErrorMessage;


        //Market Listing 

        [FindsBy(How = How.LinkText, Using = "View All")]
        public IWebElement ViewAllLink;

         [FindsBy(How = How.XPath, Using = ".//*[@id='pingPage']/div[3]/div/div[1]/div/div[1]")]
        public IWebElement ViewData;
       

        [FindsBy(How = How.PartialLinkText, Using = "Market Listings")]
        public IWebElement MarketListingLink;

        [FindsBy(How = How.LinkText, Using = "Export")]
        public IWebElement ExportLink;

        [FindsBy(How = How.PartialLinkText, Using = "AutoTrader.com")]
        public IWebElement AutotraderLink;

        [FindsBy(How = How.PartialLinkText, Using = "Cars.com")]
        public IWebElement CarsLink;

        [FindsBy(How = How.XPath, Using = ".//*[@id='showdollar0listings']")]
        public IWebElement ShowZeroPricedVehicles;

        [FindsBy(How = How.XPath, Using = "//table[@class='ui-jqgrid-htable']/thead/tr/th[2]")]
        public IWebElement MarketListAge;

        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_seller']")]
        public IWebElement MarketListSeller;

        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_vehicleDescription']")]
        public IWebElement MarketListVehicleDesc;

        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_isCertified']")]
        public IWebElement MarketListCertified;

        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_color']")]
        public IWebElement MarketListColor;

        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_mileage']")]
        public IWebElement MarketListMileage;


        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_internetPrice']")]
        public IWebElement MarketListInternetPrice;


        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_pMarketAverage']")]
        public IWebElement MarketListMktAvg;

        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_distance']")]
        public IWebElement MarketListDistance;

        [FindsBy(How = How.XPath, Using = ".//*[@id='jqgh_jqGrid_vin']")]
        public IWebElement MarketListVin;

        //VHR
        [FindsBy(How = How.XPath, Using = "//div[@class='vehicle-history-report']/div[1]/label")]
        public IWebElement VhrLabel;

        [FindsBy(How = How.XPath, Using = "//li[@id='ownerCarfax']/label")]
        public IWebElement VhrOwner;

        [FindsBy(How = How.XPath, Using = "//li[@id='buyBackCarfax']/label")]
        public IWebElement VhrBuyBackGuarantee;

        [FindsBy(How = How.XPath, Using = "//li[@id='totalLossCarfax']/label")]
        public IWebElement VhrTotalLoss;

        [FindsBy(How = How.XPath, Using = "//li[@id='frameDamageCarfax']/label")]
        public IWebElement VhrDamage;

        [FindsBy(How = How.XPath, Using = "//li[@id='airbagCarfax']/label")]
        public IWebElement VhrAirbag;

        [FindsBy(How = How.XPath, Using = "//li[@id='odometerCarfax']/label")]
        public IWebElement VhrOdometer;

        [FindsBy(How = How.XPath, Using = "//li[@id='accidentsCarfax']/label")]
        public IWebElement VhrAccident;

        [FindsBy(How = How.XPath, Using = "//li[@id='manufacturerCarfax']/label")]
        public IWebElement VhrManufacturer;

        [FindsBy(How = How.LinkText, Using = "View Carfax Report")]
        public IWebElement VhrCarfaxReportLink;

        //Advait

        //Drive Train Popover values
        [FindsBy(How = How.XPath, Using = ".//*[@id='hhhk']")]
        public IWebElement DriveTrainValue1;

        [FindsBy(How = How.XPath, Using = ".//*[@id='hhhh']")]
        public IWebElement DriveTrainValue2;

        [FindsBy(How = How.XPath, Using = ".//*[@id='driveTrain']/input")]
        public IWebElement DriveTrainUpdate;

        [FindsBy(How = How.XPath, Using = ".//*[@id='driveTrain']/span[2]")]
        public IWebElement DriveTrainErrorMessage;

        //Engine Popover values
        [FindsBy(How = How.XPath, Using = ".//*[@id='rrr']")]
        public IWebElement EngineValue1;

        [FindsBy(How = How.XPath, Using = ".//*[@id='kkk']")]
        public IWebElement EngineValue2;

        [FindsBy(How = How.XPath, Using = ".//*[@id='engine']/input")]
        public IWebElement EngineUpdate;

        [FindsBy(How = How.XPath, Using = ".//*[@id='engine']/span[2]")]
        public IWebElement EngineErrorMessage;

        //Fuel Popover values
        [FindsBy(How = How.XPath, Using = ".//*[@id='10']")]
        public IWebElement FuelValue1;

        [FindsBy(How = How.XPath, Using = ".//*[@id='5']")]
        public IWebElement FuelValue2;

        [FindsBy(How = How.XPath, Using = ".//*[@id='fuel']/input")]
        public IWebElement FuelUpdate;

        [FindsBy(How = How.XPath, Using = ".//*[@id='fuel']/span[2]")]
        public IWebElement FuelErrorMessage;

        //Header Section
        [FindsBy(How = How.XPath, Using = ".//*[@id='pingPage']/div[1]/div/div[1]/span[1]/label[1]")]
        public IWebElement VehicleMakeModelYearValue;

        [FindsBy(How = How.XPath, Using = ".//*[@id='pingPage']/div[1]/div/div[2]/span[1]/label")]
        public IWebElement VehicleDaysValue;

        [FindsBy(How = How.XPath, Using = ".//*[@id='pingPage']/div[1]/div/div[2]/span[2]/label")]
        public IWebElement VehicleColorValue;

        [FindsBy(How = How.XPath, Using = ".//*[@id='pingPage']/div[1]/div/div[2]/span[3]/label")]
        public IWebElement VehicleMileageValue;

        [FindsBy(How = How.XPath, Using = ".//*[@id='pingPage']/div[1]/div/div[2]/span[4]/label")]
        public IWebElement VehicleUnitCostValue;

        [FindsBy(How = How.XPath, Using = ".//*[@id='pingPage']/div[1]/div/div[2]/span[5]/label")]
        public IWebElement VehicleCertifiedValue;


        /*
        [FindsBy(How = How.Id, Using = "BulkActionsCkb")]
        public IWebElement BulkActionCheckBox;

        
        [FindsBy(How = How.ClassName, Using = "mastheadLogo")]
        public IWebElement MAX_Logo;



        [FindsBy(How = How.LinkText, Using = "Home")]
        public IWebElement Home_Tab;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"AuxFilters\"]/ul/li[1]")]
        public IWebElement AllInventoryText;
         */

        //constructor
        public MaxPricingPage(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(100));
            wait.Until(ExpectedConditions.TitleContains("PingOne | MAX : Online Inventory. Perfected."));

            if (driver.Title != "PingOne | MAX : Online Inventory. Perfected.")
            {
                throw new Exception("This is not the Max Pricing Page");
            }
            PageFactory.InitElements(driver, this);
        }

        public void SelectDropdown()
        {
            HeaderTrimDropdownValue.Click();
        }

    }
}
