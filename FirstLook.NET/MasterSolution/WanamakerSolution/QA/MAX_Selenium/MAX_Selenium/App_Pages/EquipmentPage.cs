﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class EquipmentPage : Page
    {
        [FindsBy(How = How.Id, Using = "StepCompletedCkb")]
        public IWebElement EquipmentComplete_CheckBox;

        [FindsBy(How = How.Id, Using = "vin")] 
        public IWebElement VIN;

        [FindsBy(How = How.LinkText, Using = "Print Window Sticker")] 
        public IWebElement Print_Window_Sticker_Link;

        [FindsBy(How = How.Id, Using = "PrintWindowStickerCheckbox")] 
        public IWebElement PopUp_Print_Window_Sticker_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$invDataViewer$PrintWindowStickerControl$PrintButton")] 
        public IWebElement PopUp_Print_Button;

        [FindsBy(How = How.Id, Using = "ApprovalMenuItem")] 
        public IWebElement Approval_Tab;

        [FindsBy(How = How.Id, Using = "WindowStickerTemplateList")]
        public IWebElement Window_Sticker_Template_DDL;
      
        //constructor
        public EquipmentPage (IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Load Equipment | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Load Equipment | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Equipment Page");
            }
            PageFactory.InitElements(driver, this);
        }

        public Boolean Is_Equipment_Complete_CheckBox_Checked()
        {
            if (EquipmentComplete_CheckBox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Equipment_Complete_CheckBox()
        {
            if (Is_Equipment_Complete_CheckBox_Checked() == false)
                EquipmentComplete_CheckBox.Click();
        }

        public void UnCheck_Equipment_Complete_CheckBox()
        {
            if (Is_Equipment_Complete_CheckBox_Checked())
                EquipmentComplete_CheckBox.Click();
        }

        public bool Is_PopUp_Print_Window_Sticker_CheckBox_Checked()
        {
            if (PopUp_Print_Window_Sticker_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_PopUp_Print_Window_Sticker_Checkbox()
        {
            if (Is_PopUp_Print_Window_Sticker_CheckBox_Checked() == false)
                PopUp_Print_Window_Sticker_Checkbox.Click();
        }
    }
}
