﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;


namespace MAX_Selenium
{
    public class LoginPage : Page
    {  	  
        //element finders
        [FindsBy(How = How.Id, Using = "username")]
        IWebElement UserName;

        [FindsBy(How = How.Id, Using = "password")]
        IWebElement Passwd;
    
        [FindsBy(How = How.Name, Using = "submit")]
        IWebElement Submit;

        [FindsBy(How = How.Id, Using = "loginFormText")]
        IWebElement LoginFormText;

        [FindsBy(How = How.Id, Using = "userNameField")]
        IWebElement UserNameField;
    
        //constructor
        public LoginPage(IWebDriver driver) //cannot inherit page elements from: base(driver)
	    {
	        this.driver = driver;
            //Cookie cookie = new Cookie("promotion", "true");
            //driver.Manage().Cookies.AddCookie(cookie);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("FirstLook - Inventory Management and Pre-Owned Automotive - Pat Ryan Jr"));

            if (driver.Title != "FirstLook - Inventory Management and Pre-Owned Automotive - Pat Ryan Jr") 
            {
                throw new StaleElementReferenceException("This is not the Login Page");
            }
            PageFactory.InitElements(driver, this);
	    }  
	  
	    public HomePage LoginAs(String username, String password) 
        {  
	    // This is the only place in the test code that "knows" how to enter these details 
            ExecuteLogin(username, password);
            // Return a new page object representing the destination.
	        return new HomePage(driver);  
	    }

        public HomePage LoginAs_Admin(String username, String password, string dealer)
        {
            ExecuteLogin(username, password);
            switch (Test.BrowserType)
            {
                case "IE":
                    driver.FindElement(By.LinkText(dealer)).SendKeys(Keys.Return); //replaces above Click() below
                    break;
                default:
                    driver.FindElement(By.LinkText(dealer)).Click(); //Doesn't work for IE10
                    break;
            }

            return new HomePage(driver);
        }

        public void FailLoginAs(String username, String password)
        {
            ExecuteLogin(username, password);
        }
	  
        public void ExecuteLogin(String username, String password)
        {
            UserName.Clear();
            UserName.SendKeys(username);
            Passwd.Clear();
            Passwd.SendKeys(password);
            Submit.Submit();            
        }
  
	}  
}
