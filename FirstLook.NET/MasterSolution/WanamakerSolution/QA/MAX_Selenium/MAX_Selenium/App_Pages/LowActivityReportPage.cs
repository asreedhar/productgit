﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAX_Selenium
{
    public class LowActivityReportPage : ReportPage
    {
        public SelectElement ClickThis;

        //element finders
        [FindsBy(How = How.Id, Using = "HeaderSearchText")] 
        public IWebElement SearchField;

        [FindsBy(How = How.Id, Using = "HeaderSearchButton")] 
        public IWebElement SearchButton;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_dealers")]
        public IWebElement SelectDealerDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_inventoryTypes")] 
        public IWebElement NewOrUsedDDL;

        [FindsBy(How = How.Id, Using = "ctl00_BodyPlaceHolder_ageInDays")]
        public IWebElement MinAgeDDL;
        
        [FindsBy(How = How.CssSelector, Using = "#ctl00_BodyPlaceHolder_activityFilter")] 
        public IWebElement ActivityFilterDDL;

        [FindsBy(How = How.CssSelector, Using = "div#ReportDiv tr span#ctl00_BodyPlaceHolder_viewer_ctl05_ctl00_TotalPages")]
        public IWebElement TotalPages;

        [FindsBy(How = How.CssSelector, Using = "div#ReportDiv tr input#ctl00_BodyPlaceHolder_viewer_ctl05_ctl00_Next_ctl00_ctl00")]
        public IWebElement NextPage_Button;


        public LowActivityReportPage(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Low Activity Report | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Low Activity Report | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Low Activity Report Page");
            }

            PageFactory.InitElements(driver, this);
        }

        public void SelectDealer(string dealerName)  // for example "Faulkner BMW"
        {
            var ddl = new SelectElement(SelectDealerDDL);
            if (ddl.SelectedOption.GetAttribute("value") != dealerName)
            {
                ddl.SelectByText(dealerName);
            }
        }

        public void SelectActivityFilter(ActivityFilterType option)
        {
            SelectItemInDDL(ActivityFilterDDL, option);                        
        }

        public void SelectNewOrUsedFilter(NewOrUsedFilterType option)
        {
            SelectItemInDDL(NewOrUsedDDL, option);
        }

        public void SelectMinAge(string days)
        {
            var ddl = new SelectElement(MinAgeDDL);
            if (ddl.SelectedOption.GetAttribute("value") != days)
            {
                ddl.SelectByText(days);
            }
        }

        public void SearchByStockNum(string stockNum)
        {
            SearchField.Clear();
            SearchField.SendKeys(stockNum);
            SearchButton.Click();
        }

        public int CountCars_LowActivityReport()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div#ReportDiv tr span#ctl00_BodyPlaceHolder_viewer_ctl05_ctl00_TotalPages")));
            Thread.Sleep(1000);
            var pages = Convert.ToInt32(TotalPages.Text);
            int totalVehicles = 0;
            var pageCarCount = 0;

            for (var i = 1; i <= pages; i++)
            {
                //identify all <tr> elements below this particular tbody where <tr> height='0' uniquely delineating the table
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(".//tr[@height='0']/parent::tbody/tr")));
                Thread.Sleep(2000);
                pageCarCount = driver.FindElements(By.XPath(".//tr[@height='0']/parent::tbody/tr")).Count();
                totalVehicles = totalVehicles + pageCarCount - 2; //exclude header rows           
                if (i == pages) break;
                NextPage_Button.Click();
                WaitForAjax();
            }
            return totalVehicles;
        }

        public IWebElement Locate_Element_ByLinkText(String text)
        {
            IWebElement mylink = driver.FindElement(By.PartialLinkText(text));
            return mylink;
        }

        public Boolean IsLinkTextPresent(String linkTextName)
        {
            for (int second = 0;; second++)
            {
                if (second >= 60)
                {
                    return false;                    
                }
                try
                {
                    if (driver.FindElement(By.LinkText(linkTextName)).Displayed) return true;
                }
                catch (Exception)
                {
                }
                Thread.Sleep(1000);
            }
        }
    }
}