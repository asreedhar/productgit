﻿using System;
using System.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium
{
    public class GLDPage : Page
    {
        [FindsBy(How = How.Id, Using = "div#WebsitePerformance ul#chart_select.ui-tabs-nav a[href='#tab_ttm']")]
        public IWebElement Time_to_Market_Tab;

        [FindsBy(How = How.CssSelector, Using = "div#WebsitePerformance ul#chart_select.ui-tabs-nav a[href='#tab_perf']")]
        public IWebElement Online_Classified_Performance_Tab;

        [FindsBy(How = How.XPath, Using = ".//span[@style ='']//table//th[text() = 'Monthly Cost']")]
        public IWebElement OnlineClassPerf_MonthlyCost_Header;

        [FindsBy(How = How.CssSelector, Using = "div#WebsitePerformance ul#chart_select.ui-tabs-nav a[href='#tab_ga']")]
        public IWebElement Website_Traffic_Tab;

        [FindsBy(How = How.CssSelector, Using = "div#WebsitePerformance div#tab_ttm a[href='#tab_ttm_g_owp']")]
        public IWebElement DaysToOnlineWithPhotos_TTM_subtab;

        [FindsBy(How = How.CssSelector, Using = "div#WebsitePerformance div#tab_ttm a[href='#tab_ttm_g_cao']")]
        public IWebElement DaysToCompleteAdOnline_TTM_subtab;

        [FindsBy(How = How.XPath, Using = "//div[@id='tab_ttm_g_owp']//*[local-name() ='svg']//*[local-name()='g' and @class='highcharts-extensions']//*[local-name()='text'][1]")]
        public IWebElement DaysToOnlineWithPhotos_GroupAvg;

        [FindsBy(How = How.XPath, Using = "//div[@id='tab_ttm_g_cao']//*[local-name() ='svg']//*[local-name()='g' and @class='highcharts-extensions']//*[local-name()='text'][1]")]
        public IWebElement DaysToCompleteAdOnline_GroupAvg;

        [FindsBy(How = How.XPath, Using = ".//*[@id=\"perf_report\"]//li[text() ='Cars.com']")]
        public IWebElement ClassifiedPerformanceReport_Cars_link;

        [FindsBy(How = How.XPath, Using = "//div[@id='perf_report']//li[text() ='AutoTrader.com']")]
        public IWebElement ClassifiedPerformanceReport_AutoTrader_link;

        [FindsBy(How = How.CssSelector, Using = "a#view-WebsitePerformance.report-link")]
        public IWebElement ViewFullReport_Context_WebsitePerformance;

        //drop down lists. Must be instantiated in the constructor.
        [FindsBy(How = How.CssSelector, Using = "div.groupDash div#InventoryType select#InventoryTypeDDL")]
        protected IWebElement gld_inventoryType_DDL_bud;
        public SelectElement GLD_InventoryType_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.groupDash select#startDate")]
        protected IWebElement gld_fromDate_DDL_bud;
        public SelectElement GLD_FromDate_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.groupDash select#endDate")]
        protected IWebElement gld_toDate_DDL_bud;
        public SelectElement GLD_ToDate_DDL;

        //constructor
        public GLDPage(IWebDriver driver) //cannot inherit page elements from: base(driver)
        {
            this.driver = driver;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Group Dashboard | MAX : Intelligent Online Advertising Systems")); 

            if (driver.Title != "Group Dashboard | MAX : Intelligent Online Advertising Systems")
            {
                throw new Exception("This is not the GLD Page");
            } 

            PageFactory.InitElements(driver, this);
            GLD_InventoryType_DDL = new SelectElement(gld_inventoryType_DDL_bud);
            GLD_FromDate_DDL = new SelectElement(gld_fromDate_DDL_bud);
            GLD_ToDate_DDL = new SelectElement(gld_toDate_DDL_bud);
            CloseNaggingWindow();

        }



        public void setFromMonthByIndexInDDL(int monthIndex)  // Index 1 in the ddl is the most recent month, 13 the oldest
        {
            new SelectElement(driver.FindElement(By.Id("startDate"))).SelectByIndex(monthIndex);
        }

        public void setToMonthByIndexInDDL(int monthIndex)  // Index 1 in the ddl is the most recent month, 13 the oldest
        {
            new SelectElement(driver.FindElement(By.Id("endDate"))).SelectByIndex(monthIndex);
        }

        // TODO Need to figure out how to handle dynamically appearing/disappearing tspans
        public int Get_AvgConsumerImpressions(String vendor)  //valid vendors are: "AutoTrader.com", "Cars.com", "Dealer" (Dealer Websites)
        {

            var barIndex =
                driver.FindElements(
                    By.XPath(
                        "//div[@id='chart_cv_chart']" +
                        "//*[local-name() ='tspan' and text()= '" + vendor + "']/parent::*[local-name()='text']/preceding-sibling::*[local-name()='text']")).Count + 1;
            var avgImpressions = driver.FindElement(By.CssSelector("div#chart_cv_chart g[class='highcharts-data-labels highcharts-tracker'] " +
                                                                   "g:nth-child(" + barIndex + ") tspan")).Text;
            return Utility.ExtractDigitsFromString(avgImpressions);

        }

        public decimal Get_Avg_CostPerImpression(String vendor)  //Valid vendors are: AutoTrader.com, Cars.com
        {

            var barIndex =
                driver.FindElements(
                    By.XPath("//div[@id='chart_cpv_chart']" +
                             "//*[local-name() ='tspan' and text()= '" + vendor + "']/parent::*[local-name()='text']/preceding-sibling::*[local-name()='text']")).Count + 1;
            var avgCPV = driver.FindElement(By.CssSelector("div#chart_cpv_chart g[class='highcharts-stack-labels'] " +
                                                                   "text:nth-child(" + barIndex + ") tspan")).Text;

            avgCPV = avgCPV.Trim('$');
            return Convert.ToDecimal(avgCPV);

        }

        public int GetAverageMonthlyCost(String vendor) //vendor is "AutoTrader.com" or "Cars.com"
        {

            var costText = driver.FindElement(By.XPath("//div[@id='tab_impression']//div[@class='chart-footer vendor-budgets']" +
                                           "//li[span[text()='" + vendor + "']]//span[2]")).Text;

            return Utility.ExtractDigitsFromString(costText);

        }


        public static String RandomDealerChooser_GLD()
        {
            String[] dealerNames = { "Kenny Ross Ford Adamsburg", 
                                     "Windy City BMW", 
                                     "Performance Ford",
                                     "Voss Hyundai",
                                     "All Star Chevrolet",
                                     "Wetzel Chevy",
                                     "Don Beyer Volkswagen"};

            Random ran = new Random();
            return dealerNames[ran.Next(dealerNames.Length)];
        }
        
    }
}
