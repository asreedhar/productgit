﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;

namespace MAX_Selenium
{
    public class StandAloneLoginPage : Page
    {
        [FindsBy(How = How.Id, Using = "username")]
        IWebElement UserName;

        [FindsBy(How = How.Id, Using = "password")]
        IWebElement Passwd;

        [FindsBy(How = How.Name, Using = "submit")]
        IWebElement Submit;

        public StandAloneLoginPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("MAX : Intelligent Online Advertising Systems"));

            if (driver.Title != "MAX : Intelligent Online Advertising Systems")
            {
                throw new StaleElementReferenceException("This is not the Stand Alone Login Page");
            }
            PageFactory.InitElements(driver, this);
        }

        public DashBoardPage LoginAs(String username, String password)
        {
            // This is the only place in the test code that "knows" how to enter these details 
            executeLogin(username, password);
            // Return a new page object representing the destination.
            return new DashBoardPage(driver);
        }

        public void FailLoginAs(String username, String password)
        {
            executeLogin(username, password);
        }

        public void executeLogin(String username, String password)
        {
            UserName.Clear();
            UserName.SendKeys(username);
            Passwd.Clear();
            Passwd.SendKeys(password);
            Submit.Submit();
        }
    }
}
