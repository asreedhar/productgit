﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium
{
    public abstract class ReportPage : Page
    {
        protected ReportPage(IWebDriver driver) : base(driver)
        {
        }

        public void SelectItemInDDL(IWebElement element, Enum type)
        {
            var ddl = new SelectElement(element);
            string temp;
            switch (type.ToString())
            {
                case "Cars":
                    temp = "Cars.com";
                    break;
                case "AutoTrader":
                    temp = "Auto Trader.com";
                    break;
                case "Not_Online":
                    temp = "Not Online";
                    break;
                case "New_Used":
                    temp = "New & Used";
                    break;
                case "New_Only":
                    temp = "New Only";
                    break;
                case "Used_Only":
                    temp = "Used Only";
                    break;
                case "Low_Activity":
                    temp = "Low Activity";
                    break;
                case "High_Activity":
                    temp = "High Activity";
                    break;
                default:
                    temp = type.ToString();
                    break;
            }
            if (ddl.SelectedOption.GetAttribute("value") != temp)
            {
                ddl.SelectByText(temp);
            }
        }

    }
}
