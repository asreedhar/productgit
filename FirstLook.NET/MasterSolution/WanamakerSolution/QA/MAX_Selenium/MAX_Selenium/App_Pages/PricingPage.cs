﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;


// under development; do not use
namespace MAX_Selenium
{
    
    public class PricingPage : Page
    {
        //finders
        //[FindsBy(How = How.CssSelector, Using = "html.js li#CVATab")]
        //public IWebElement MaxMerchandising_Tab;

        //only a clickable tab if Merchandising enabled, otherwise just a label
        //[FindsBy(How = How.CssSelector, Using = "html.js li#IAATab")]
       // public IWebElement Ping_III_Tab;

        //present in the absence of Merchandising upgrade
        //[FindsBy(How = How.CssSelector, Using = "html.js h1#page_branding")]
       // public IWebElement Ping_III_Label;

        public PricingPage(IWebDriver driver) : base(driver)
        {  

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.TitleContains("Pricing | MAX : Online Inventory. Perfected."));

            if (driver.Title != "Pricing | MAX : Online Inventory. Perfected.") 
            {
                throw new StaleElementReferenceException("This is not the Pricing page.");
            }
            PageFactory.InitElements(driver, this);
	    }  
    }
}
