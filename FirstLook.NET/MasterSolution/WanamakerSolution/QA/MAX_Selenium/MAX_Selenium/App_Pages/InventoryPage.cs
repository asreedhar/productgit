﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium
{
    public class InventoryPage : Page
    {

        //data objects
        public static String NewCarPricing_Title = "New Car Pricing (BETA)";

        //finders
        [FindsBy(How = How.Id, Using = "BulkActionsCkb")]
        public IWebElement BulkActionCheckBox;

        [FindsBy(How = How.CssSelector, Using = "tbody div.field input")]
        public IWebElement FirstCarBulkActionCheckBox;

        [FindsBy(How = How.Id, Using = "BulkActionsSelect")]
        public IWebElement BulkActionDropDown;

        [FindsBy(How = How.ClassName, Using = "mastheadLogo")] 
        public IWebElement MAX_Logo;

        [FindsBy(How = How.LinkText, Using = "Not Online")]
        public IWebElement NotOnlineReportLink;

        [FindsBy(How = How.Id, Using = "lnkDashboard")]
        public IWebElement ExecutiveDashboardLink;

        [FindsBy(How = How.LinkText, Using = "Inventory")]
        public IWebElement InventoryTab;

        [FindsBy(How = How.LinkText, Using = "Advanced Settings")] 
        public IWebElement Advanced_Settings;

        [FindsBy(How = How.LinkText, Using = "Setup Wizard")] 
        public IWebElement Setup_Wizard;
        
        [FindsBy(How = How.Id, Using = "EquipmentMenuItem")]
        public IWebElement EquipmentTab;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"AuxFilters\"]/ul/li[1]")]
        public IWebElement AllInventoryText;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"AuxFilters\"]/ul/li[2]")]
        public IWebElement ApprovedText;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"AuxFilters\"]/ul/li[3]")]
        public IWebElement OfflineText;

        [FindsBy(How = How.LinkText, Using = "All Inventory")]
        public IWebElement AllInventoryLink;

        [FindsBy(How = How.LinkText, Using = "Approved")]
        public IWebElement ApprovedLink;

        [FindsBy(How = How.LinkText, Using = "Offline")]
        public IWebElement OffLineLink;

        [FindsBy(How = How.CssSelector, Using = "li.selected")] 
        public IWebElement OfflineLabel;

        [FindsBy(How = How.CssSelector, Using = "div#FiltersDiv li.NeedsAdReviewAll a")]
        public IWebElement NeedReviewTab;

        //requires AutoApprove disabled for this text to appear
        [FindsBy(How = How.CssSelector, Using = "div#FiltersDiv li.NeedsApprovalAll a")]
        public IWebElement NeedApprovalTab;

        [FindsBy(How = How.PartialLinkText, Using = "No Description")]
        public IWebElement NoDescription_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "No Trim")]
        public IWebElement NoTrim_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Outdated Price in Ad")]
        public IWebElement OutdatedPrice_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Need Pricing")]
        public IWebElement NeedPricing_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "No Price")]
        public IWebElement NoPrice_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Needs Re-pricing ")]
        public IWebElement NeedsRePricing_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Merchandising Alerts ")]
        public IWebElement Merchandising_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "No Packages")]
        public IWebElement NoPackages_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "No Book Value")]
        public IWebElement NoBookValue_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Need Photos/Equipment")]
        public IWebElement NeedPhotosEquipment_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Equipment Review")]
        public IWebElement EquipmentReview_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "No Photos")] //"No/Low Photos" separated in 12.1
        public IWebElement NoPhotos_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Low Photos")] //"No/Low Photos" separated in 12.1
        public IWebElement LowPhotos_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "No Carfax")]
        public IWebElement NoCarFax_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Low Online Activity")]
        public IWebElement LowActivity_Sub_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "All")]  //This will work for all sub  tabs named All
        public IWebElement All_Sub_Tab;

        [FindsBy(How = How.Id, Using = "CurrentFilter")]
        public IWebElement CurrentFilterSection;

        [FindsBy(How = How.Id, Using = "HeaderSearchText")]
        public IWebElement SearchField;

        [FindsBy(How = How.Id, Using = "HeaderSearchButton")]
        public IWebElement SearchButton;

        [FindsBy(How = How.Id, Using = "BtnInventoryItem")] 
        public IWebElement FirstVehicleLink;

        [FindsBy(How = How.XPath, Using = "(.//a[@id='BtnInventoryItem' and not(following-sibling::div)])[1]")]
        public IWebElement FirstTrimmedVehicleLink;

        [FindsBy(How = How.Id, Using = "NumberOfLastItem")]
        public IWebElement NumberOfLastItem;

        [FindsBy(How = How.Id, Using = "TotalItemCount")]
        public IWebElement TotalItemCount;

        [FindsBy(How = How.Id, Using = "Count")]
        public IWebElement Count;

        [FindsBy(How = How.Id, Using = "lnkBtnAge")]
        public IWebElement Age;
        
        [FindsBy(How = How.XPath, Using = ".//*[@id='UsePaging']")]
        //[FindsBy(How = How.Id, Using = "UsePaging")]
        public IWebElement UsePaging;

        [FindsBy(How = How.XPath, Using = ".//*[@id='ShowAllItems']")] 
        //[FindsBy(How = How.Id, Using = "ShowAllItems")] 
        public IWebElement ShowAll;

        [FindsBy(How = How.CssSelector, Using = "div.filterMenu")]
        public IWebElement FilterMenu;

        [FindsBy(How = How.XPath, Using = ".//*[@id='CurrentFilter']")]
        public IWebElement CurrentFilter;

        [FindsBy(How = How.LinkText, Using = "Log Out")] 
        public IWebElement LogOut;

        [FindsBy(How = How.Id, Using = "InventoryTypeDDL")]
        public IWebElement InventoryTypeDDL;

        [FindsBy(How = How.CssSelector, Using = "div#Body.clearfix a#BtnAutoLoad")] 
        public IWebElement AutoLoad_Button;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog input#CancelAutoBuild.button")]
        public IWebElement AutoLoad_CredNeeded_Popup_Cancel_Button;

        [FindsBy(How = How.CssSelector, Using = "#InventoryTable td div.photos .count")] 
        public IWebElement Photo_Count;

        [FindsBy(How = How.Id, Using = "incentives")]
        public IWebElement NewCarPricing_Button;

        [FindsBy(How = How.Id, Using = "viewCampaigns")]
        public IWebElement ViewActiveDiscounts_Link;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog select#yearSelectBox > option")] 
        public IWebElement NewCarPricing_Year;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog select#makeSelectBox > option")]
        public IWebElement NewCarPricing_Make;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog select#modelSelectBox > option")]
        public IWebElement NewCarPricing_Model;

        [FindsBy(How = How.Id, Using = "priceBaseLine")]
        public IWebElement DiscountType_DDL;

        [FindsBy(How = How.Id, Using = "price-buttons")]
        public IWebElement Price_Buttons;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog input#rebate-price")] 
        public IWebElement NCP_Manufacturer_Rebate;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog input#discount-price")]
        public IWebElement NCP_Dealer_Discount;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog input#newCar-Expiration")]
        public IWebElement NCP_Expiration_Date;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog button")]
        public IWebElement NCP_Cancel_Button;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog button#apply-prices")]
        public IWebElement NCP_Apply_Button;

        [FindsBy(How = How.XPath, Using = "//div[@role='dialog']/div[span[@id='ui-id-2']]//a[@role='button']")]
        public IWebElement NCP_Campaign_CloseIcon;

        [FindsBy(How = How.CssSelector, Using = "div.ui-dialog div#activeCampaignsDialog.dialog label > select")]
        public IWebElement Show_Entries_DDL;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"new-car-vehicles\"]")]
        public IWebElement NCP_Total_Vehicles_Selected;

        [FindsBy(How = How.LinkText, Using = "Home")]
        public IWebElement HomeLink;

        //[FindsBy(How = How.Id, Using = "closeReport")]
        //public IWebElement AutoLoad_Complete_Popup_Close_Button;

        //Drop Down Lists
        [FindsBy(How = How.CssSelector, Using = "div#BulkActions.field select#BulkActionsSelect")]
        public IWebElement bulkAction_DDL_bud; //must be public to instantiate in test
        public SelectElement BulkAction_DDL; //cannot always instantiate on pageload. Element won't render if no cars listed.

        [FindsBy(How = How.Id, Using = "InventoryTypeDDL")] 
        protected IWebElement inventoryType_DDL_bud;
        public SelectElement InventoryType_DDL;


        //constructor
        public InventoryPage(IWebDriver driver) : base(driver)
        {
            //this.driver = driver;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Current Inventory Summary | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Current Inventory Summary | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Inventory Page");
            } 

            PageFactory.InitElements(driver, this);
            CloseNaggingWindow();
            InventoryType_DDL = new SelectElement(inventoryType_DDL_bud);
        }

        public ApprovalPage Goto_ApprovalPageTrimmedVehicle(string searchKey)
        {
            SearchVehicle(searchKey);           
            WaitForAjax();
            FirstTrimmedVehicleLink.Click();
           
            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait2.Until(ExpectedConditions.TitleContains("Ad Approval | MAX : Online Inventory. Perfected."));

            CloseNaggingWindow();
            HandleIAAInterrupt();
            return new ApprovalPage(driver);
        }

        public ApprovalPage Goto_ApprovalPageNoTrimVehicle(string searchKey)
        {
            SearchVehicle(searchKey);
            WaitForAjax();
            FirstVehicleLink.Click();

            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait2.Until(ExpectedConditions.TitleContains("Ad Approval | MAX : Online Inventory. Perfected."));

            CloseNaggingWindow();
            HandleIAAInterrupt();
            return new ApprovalPage(driver);
        }



        public Admin_Home_Page GoTo_Admin_Home_Page()
        {
            
            driver.Navigate().GoToUrl(BaseURL + "/merchandising/Admin/Admin.aspx");
            
            Thread.Sleep(5000);
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.TitleContains("Admin | MAX : Online Inventory. Perfected."));
            
            return new Admin_Home_Page(driver);
        }

        public void GoBackTo_ApprovalPage(string vin, string baseUrl, string inventoryPageUrl)
        {
            driver.Navigate().GoToUrl(baseUrl +"/"+ inventoryPageUrl);
            SearchVehicle(vin);
            FirstTrimmedVehicleLink.Click();
        }

        public void SearchVehicle(string searchKey)
        {
            SearchField.Clear();
            SearchField.SendKeys(searchKey);
            SearchButton.Click();
            CloseNaggingWindow();
        }

        public void SelectVehicles(InventoryType type)
        {
            var valueToSelect = (int)type;

            var ddl = new SelectElement(InventoryTypeDDL);
            
            if (ddl.SelectedOption.GetAttribute("value") != valueToSelect.ToString())
            {
                ddl.SelectByValue(valueToSelect.ToString());
            }
        }

        public ApprovalPage ClickOnFirstVehicle()
        {
            //ApprovedLink.Click(); //avoid No Trim cars
            FirstTrimmedVehicleLink.Click();
            HandleIAAInterrupt(); // needed until FB 30621 is resolved
            CloseNaggingWindow();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Ad Approval | MAX : Online Inventory. Perfected."));

            return new ApprovalPage(driver);
        }

        public NotOnlineReportPage GoTo_NotOnlineReportPage()
        {
            Reports.Click();
            //wait for Not Online Report link to render
            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait2.Until<IWebElement>((d) =>
            {
                return d.FindElement(By.LinkText("Not Online"));
            });
            NotOnlineReportLink.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Not Online Report | MAX : Online Inventory. Perfected."));

            return new NotOnlineReportPage(driver);
        }

        public Settings_MAXsettings_Page GoTo_MAXsettings_Page()
        {
            Settings.Click();
            MAX_Settings.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            return new Settings_MAXsettings_Page(driver);
        }

        public Settings_AdvancedSettings_Page GoTo_Settings_AdvancedSettings_Page()
        {
            Settings.Click();
            Advanced_Settings.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Advanced Dealer Preferences | MAX : Online Inventory. Perfected."));

            return new Settings_AdvancedSettings_Page(driver);
        }

        public Setup_Wizard_Page GoTo_Setup_Wizard_Page()
        {
            Settings.Click();
            Setup_Wizard.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Dealer Setup Wizard | MAX : Online Inventory. Perfected."));

            return new Setup_Wizard_Page(driver);
        }

        public void Click_ShowAll()
        {
            ShowAll.Click();
            IWebElement usePageingElement = ExplicitWaitById("UsePaging", 30);
            if (usePageingElement == null)
                Assert.Fail("UsePaging element couldn't be found.");         

        }

        public void Click_ShowAll_IfPresent()
        {
            try
            {
                ShowAll.Click();
                WaitForAjax();
            }
            catch (Exception)
            {
                return;
            }
        }

        public void Click_UsePaging()
        {
            UsePaging.Click();
            IWebElement showAllItemsElement = ExplicitWaitById("ShowAllItems", 30);
            if (showAllItemsElement == null)
                Assert.Fail("ShowAllItems element couldn't be found.");
        }

        public void Open_NewCarPricing_Widget()
        {
            NewCarPricing_Button.Click();
            string popupTitle = driver.FindElement(By.CssSelector("div.ui-dialog span#ui-id-1.ui-dialog-title")).Text;
            StringAssert.Contains(NewCarPricing_Title, popupTitle);
        }

        public void Open_ActiveCampaigns_Widget()
        {
            ViewActiveDiscounts_Link.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.ui-dialog div#activeCampaignsDialog.dialog")));

            bool activeCampaignModalOpen = driver.FindElement(By.CssSelector("div.ui-dialog div#activeCampaignsDialog.dialog")).Displayed;
            Assert.IsTrue(activeCampaignModalOpen);

            WebDriverWait wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait2.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div.ui-dialog div#activeCampaignsDialog.dialog label > select")));

            var seDDL = new SelectElement(Show_Entries_DDL);
            seDDL.SelectByText("100");
        }

        public String NCP_SelectandRecord_YearMakeModel()
        {
            NewCarPricing_Year.Click();
            string year = NewCarPricing_Year.Text;
            NewCarPricing_Make.Click();
            string make = NewCarPricing_Make.Text;
            NewCarPricing_Model.Click();
            string model = NewCarPricing_Model.Text;
            return (year + " - " + make + " - " + model);
        }

        //input text is "Invoice" or "MSRP"
        public string SetandRecord_NCP_Price_Baseline(string discountType)
        {
            var ddl = new SelectElement(DiscountType_DDL);
            ddl.SelectByText(discountType);
            return (discountType);
        }

        //input text is "add" or "subtract" - lower case
        public string SetandRecord_NCP_Price_Button(string priceDelta)
        {
            priceDelta = priceDelta.ToLower();
            driver.FindElement(By.CssSelector("div.ui-dialog div#price-buttons > input[value = '" + priceDelta + "']")).Click();
            if (priceDelta == "subtract")
            {
                return ("$-");
            }
            if (priceDelta == "add")
            {
                return ("$");
            }
            throw new Exception("unknown math type");
        }

        public string SetandRecord_NCP_Rebate (Int32 rebate)
        {
           // NCP_Manufacturer_Rebate.Clear(); //cannot use for Invoice campaigns until rebate is unmasked (feature request)
            string rebateString = rebate.ToString();
            // NCP_Manufacturer_Rebate.SendKeys(rebateString); //cannot use for Invoice campaigns until rebate is unmasked (feature request)
            return (rebateString);
        }

        public string SetandRecord_NCP_Discount(Int32 discount)
        {
            NCP_Dealer_Discount.Clear();
            string discountString = discount.ToString();
            NCP_Dealer_Discount.SendKeys(discountString);
            return (discountString);
        }

        //baseline is today's date, plus or minus any additional days
        public string SetandRecord_NCP_Date_From_Today (Int32 additionalDays)
        {
            string setDate = Todays_Date_Plus_Days(additionalDays);
            Thread.Sleep(1000); //give calendar a chance to load
            //NCP_Expiration_Date.Clear(); //this may wipe out the calendar temporarily and may not be necessary
            NCP_Expiration_Date.SendKeys(setDate);
            //Thread.Sleep(2000); //this may be in the wrong place
            return (setDate);
        }

        public string Get_FirstListed_Inventory_StockNo()
        {
            IWebElement parentTD = FirstTrimmedVehicleLink.FindElement(By.XPath("./ancestor::td"));
            string stockNum = parentTD.FindElement(By.CssSelector("ul a")).Text;
            return (stockNum);
        }

        public IWebElement Locate_BulkAction_Checkbox_By_StockNo(string stockNum)
        {
            IWebElement stockNoLink = driver.FindElement(By.LinkText(stockNum));
            IWebElement myBulkActionCheckbox = stockNoLink.FindElement(By.XPath("./ancestor::tr//div/input"));
            return (myBulkActionCheckbox);
        }

        public IWebElement Locate_VehicleLink_By_StockNo(string stockNum)
        {
            IWebElement stockNoLink = driver.FindElement(By.LinkText(stockNum));
            IWebElement myVehicleLink = stockNoLink.FindElement(By.XPath("./ancestor::td//a"));
            return (myVehicleLink);
        }

        public Boolean Is_TotalItemCount_Present()  //Checks if current selected filter has at least one vehicle
        {
            try
            {
                driver.FindElement(By.Id("TotalItemCount")); 
                return true; 
            } catch (NoSuchElementException e) 
            { 
                return false; 
            }
       }

        public void Wait_For_NCP_Modal_To_Close()
        {
            TimeSpan maxDuration = TimeSpan.FromSeconds(15);
            Stopwatch sw = Stopwatch.StartNew();
            bool modalClosed = false;

            while (sw.Elapsed < maxDuration && !modalClosed)
            {
                Thread.Sleep(100);
                if (!driver.FindElement(By.CssSelector("div.ui-dialog div#new-car-pricing.dialog")).Displayed)
                {
                    modalClosed = true;
                    return;
                }
            }

            throw new Exception("timeout " + maxDuration + " exceeded while waiting for window to close");
        }

        public void Wait_For_AutoLoad_Cred_Modal_To_Close()
        {
            TimeSpan maxDuration = TimeSpan.FromSeconds(15);
            Stopwatch sw = Stopwatch.StartNew();
            bool modalClosed = false;

            while (sw.Elapsed < maxDuration && !modalClosed)
            {
                Thread.Sleep(100);
                if (!driver.FindElement(By.CssSelector("div.ui-dialog span#ui-id-1.ui-dialog-title")).Displayed)
                {
                    modalClosed = true;
                    return;
                }
            }

            throw new Exception("timeout " + maxDuration + " exceeded while waiting for window to close");
        }


        //Active Campaigns window must be open first - a check is made to ensure this.
        public void Clear_Automation_Campaigns(string userName)
        {
            bool activeCampaignModalOpen = driver.FindElement(By.CssSelector("div.ui-dialog div#activeCampaignsDialog.dialog")).Displayed;
            Assert.IsTrue(activeCampaignModalOpen);
            while (true)
            {
                try
                {
                    IWebElement cancelButton = driver.FindElement(By.XPath("//div[@id='campaigns']//tr[td[contains(.,'" + userName + "')]]/td[button]/button"));
                    Thread.Sleep(500);
                    cancelButton.Click();
                    Thread.Sleep(1000);
                }
                catch (Exception)
                {
                    NCP_Campaign_CloseIcon.Click();
                    return;
                }
            }

        }

        //Active Campaigns window must be open first - a check is made to ensure this.
        public string Get_NCP_Campaign_Data (string userName)
        {
            bool activeCampaignModalOpen = driver.FindElement(By.CssSelector("div.ui-dialog div#activeCampaignsDialog.dialog")).Displayed;
            Assert.IsTrue(activeCampaignModalOpen);
            String returnData = driver.FindElement(By.XPath("//div[@id='campaigns']//tr[td[contains(.,'" + userName + "')]]")).Text;
            return(returnData);

        }

        public static String FindNewestDealer()
        {

            var NewDealers = DataFinder.ExecQuery(DataFinder.Merchandising,
                                                 @"
                        SELECT TOP 1 BusinessUnit
                        FROM merchandising.settings.Merchandising M
                        JOIN imt.dbo.BusinessUnit BU on M.BusinessUnitID = BU.BusinessUnitID
                        WHERE showDashboard = 1
                        AND MaxVersion = 3
                        AND BusinessUnit NOT LIKE ('QA%')  -- will not use test dealers
                        ORDER BY year(insertdate) desc, month(insertdate) desc
                    ")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { BusinessUnit = (String)dr["BusinessUnit"]})
                .First();

            return NewDealers.BusinessUnit;
        }

    }
}
