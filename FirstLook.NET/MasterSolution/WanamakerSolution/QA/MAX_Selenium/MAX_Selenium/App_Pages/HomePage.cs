﻿
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;

namespace MAX_Selenium
{
    public class HomePage :Page
    {
        [FindsBy(How = How.Id, Using = "max_branding")]
        public IWebElement MAX_AD_Button_20;

        [FindsBy(How = How.LinkText, Using = "Digital Marketing System")] 
        public IWebElement MAX_AD_Button_30;

        [FindsBy(How = How.Id, Using = "version")] 
        public IWebElement MAX_AD_Version;

        [FindsBy(How = How.CssSelector, Using = "div.bordered-box-inner div#maxdoe-dashboard.maxdoe-left-block")]
        public IWebElement MAX_Dashboard_Link;

        [FindsBy(How = How.XPath, Using = ".//*[@id='global_menu_bar']/li[2]/span")] 
        public IWebElement Tools_Menu;

        [FindsBy(How = How.LinkText, Using = "Create New Inventory")]
        public IWebElement Create_New_Inventory;

        [FindsBy(How = How.CssSelector, Using = "div.bordered-box-inner div#maxdoe-right-header")]
        public IWebElement Online_Inventory_Status_Link;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"maxdoe-header\"]/a")]
        public IWebElement Max_Tile_Link;
        
        //constructor
        public HomePage(IWebDriver driver) //cannot inherit page elements from: base(driver)
        {
            this.driver = driver;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            IWebElement selectElement = wait.Until(ExpectedConditions
                  .ElementIsVisible(By.CssSelector("div#container ul#global_menu_bar.global_menu_bar")));
            //Thread.Sleep(2000);

            if ((driver.Title != "FirstLook | Home") & (driver.Title != "MAX: Home")) // login with test user "qa101621"/"N@d@123" to directly go to home page            
            {
                throw new StaleElementReferenceException("This is not the Firstlook Home Page");
            }
         
            PageFactory.InitElements(driver, this);

        }

        /* Needs to be moved, if needed at all
         public InventoryPage GotoInventoryPage()
            {
                if (driver.Title.Equals("FirstLook | Home"))
		            {
		                MAX_AD_Button_30.Click();
                        CloseNaggingWindow();
                        Inventory_Link.Click();
		            }

		         else if (driver.Title.Equals("MAX: Home"))
		            {
		                MAX_AD_Button_20.Click();
		                CloseNaggingWindow();
		                Inventory_Link.Click();
                        Thread.Sleep(2000);
                        Inventory_Link.Click();
		            }
		         else
		            {
		                throw new Exception("Firstlook Home page not detected.");
		            }
                return new InventoryPage(driver);
            }
         */

        public DashBoardPage GoToDashboardDirectly(string url)
        {
            driver.Navigate().GoToUrl(BaseURL + url);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Dashboard | MAX : Intelligent Online Advertising Systems"));
            CloseNaggingWindow();
            return new DashBoardPage(driver);
        }

        public GLDPage GoToGLDDirectly(string url)
        {
            driver.Navigate().GoToUrl(BaseURL + url);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Group Dashboard | MAX : Intelligent Online Advertising Systems"));

            return new GLDPage(driver);
        }

        public InventoryPage GoToInventoryDirectly(string url)
        {
            driver.Navigate().GoToUrl(BaseURL + url);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Current Inventory Summary | MAX : Online Inventory. Perfected."));

            CloseNaggingWindow();
            return new InventoryPage(driver);

        }

        //QA_quinnox
        public MaxPricingPage GoToMaxPricingpage(string url)
        {
            driver.Navigate().GoToUrl(BaseURL + url);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("PingOne | MAX : Online Inventory. Perfected."));

            return new MaxPricingPage(driver);
        }


        public void Make_Tools_Menu_Visible ()
        {
            EnsureJQuery();
            const string script = @"jQuery('#global_menu_bar_container li:nth-child(2) ul').css('display', 'block')";
            var js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(script);
        }

        public void EnsureJQuery()
        {
            TryLoadJQuery();
            WaitForJQuery();
        }

        public void TryLoadJQuery()
        {
            var js = (IJavaScriptExecutor)driver;
            // script.src should not be http: but in the context of current page hierarchy and security, thus //
            js.ExecuteScript(
                @"
                if(typeof(window['jQuery']) === 'undefined' && document.getElementById('jQueryLoader') === null) {
	                var script = document.createElement('script');
	                script.id = 'jQueryLoader';
	                script.src = '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
	                document.body.appendChild(script);
                }
                ");
        }

        public void WaitForJQuery()
        {
            var timeout = DateTimeOffset.UtcNow.AddMilliseconds(5000);
            while(!HasJQuery())
            {
                if(DateTimeOffset.UtcNow > timeout)
                    throw new InvalidOperationException("Failed to load jQuery.");
                Thread.Sleep(50);
            }
        }

        public bool HasJQuery()
        {
            try
            {
                var js = (IJavaScriptExecutor)driver;
                return (bool)js.ExecuteScript(
                    @"
                var hasjQuery = typeof(window['jQuery']) !== 'undefined';
                var hadToLoadjQuery = document.getElementById('jQueryLoader') !== null;
                if(hasjQuery && hadToLoadjQuery)
                {
	                jQuery.noConflict();
                }
                return hasjQuery;
                ");
            }
            catch(Exception ex)
            {
                return false;
            }
        }


        public void Create_New_InTransit_Inventory(string vin, string stockNumber, string mileage, string price)
        {
            //var elem = jQuery('#global_menu_bar li').filter(function() { return jQuery('span', this).text() === 'TOOLS'; })[0]
            //new Actions(driver).MoveToElement(Tools_Menu).Build().Perform();
            Make_Tools_Menu_Visible();
            Create_New_Inventory.Click();
            driver.FindElement(By.Id("InTransitInventoryFormView_VinTextBox")).Clear();
            driver.FindElement(By.Id("InTransitInventoryFormView_VinTextBox")).SendKeys(vin);
            driver.FindElement(By.Id("InTransitInventoryFormView_StockNumberTextBox")).Clear();
            driver.FindElement(By.Id("InTransitInventoryFormView_StockNumberTextBox")).SendKeys(stockNumber);
            driver.FindElement(By.Id("InTransitInventoryFormView_MileageTextBox")).Clear();
            driver.FindElement(By.Id("InTransitInventoryFormView_MileageTextBox")).SendKeys(mileage);
            driver.FindElement(By.Id("InTransitInventoryFormView_UnitCostTextBox")).Clear();
            driver.FindElement(By.Id("InTransitInventoryFormView_UnitCostTextBox")).SendKeys(price);
            driver.FindElement(By.Id("InTransitInventoryFormView_DealTypeRadioButtons_0")).Click();
            driver.FindElement(By.Id("InTransitInventoryFormView_InsertButton")).Click();
        }

    }
}
