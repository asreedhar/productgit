﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Compilation;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.SQS.Model;
using NUnit.Framework;

// ReSharper disable once CheckNamespace
namespace MAX_Selenium
{
    [Ignore]
    [TestFixture]
    class Setup_ABTestData : Test
    {
        // used for all tests
        private const String StatusTypeId = "100";
        private const String SiteId = "4";
        private const String CountryCode = "1";
        private const String ModelAboveYear = "1999";
        private const String Manufacturer = "Volkswagen";
        private const int NumberOfVinsToProcess = 14;

        // used for all tests, can change if need different test set per week
        private const int NumOfWeeksBackBegin = -10;
        private const int NumOfWeeksBackEnd = -8;

        // used for LoadVehiclesDirectlyToSlurpee
        private const String UserName = "chicagobears";
        private const String PassWord = "Chris2112";
        /*
        [Test]
        [Ignore] //under development
        public void LoadVehiclesThroughInTransit()
        {
            try
            {
                var streamWriter = new StreamWriter(String.Format("VINs_{0}.txt", Suffix));

                LoginAsAdmin_HomePage("QABlueMoon", "N@d@123", "Windy City Chevrolet");

                var index = 0;
                foreach (DataRow dataRow in _vinData.Rows)
                {
                    var vin = dataRow["vin"].ToString();
                    var stockNumber = String.Format("ABAL_Test_{0}", index);
                    var mileage = dataRow["MileageReceived"].ToString();
                    var price = dataRow["LotPrice"].ToString();

                    // retrieve XML from Prod S3 and put in Beta bucket/folder
                    CopyS3Object(SourceBucket, vin, TargetBucket, String.Format("{0}/{1}", ProdBackupPrefix, vin));

                    // backup existing XML in Beta root into backup folder
                    if (CopyS3Object(TargetBucket, vin, TargetBucket, String.Format("{0}/{1}", BetaBackupPrefix, vin)))
                        RemoveS3Object(TargetBucket, vin);

                    try
                    {
                        // Load VINs into dealer
                        homePage.Create_New_InTransit_Inventory(vin, stockNumber, mileage, price);
                        Thread.Sleep(1000);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    // Save VIN info to the file and to simple DB for AB Testing after autoload completes
                    streamWriter.WriteLine(vin);

                    index++;
                }

                var vinFile = ((FileStream)(streamWriter.BaseStream)).Name;
                streamWriter.Close();

                Console.WriteLine("Backed up VIN files in Beta to {0}/{1}", TargetBucket, BetaBackupPrefix);
                Console.WriteLine("Copied VIN files from Production to {0}/{1}", TargetBucket, ProdBackupPrefix);
                Console.WriteLine("Completed {0} items! Wrote VINs to {1}", index, vinFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw; // fail the test if we have uncaught exceptions
            }
        }
        
        [Test]
        [Ignore] //under development
        public void LoadVehiclesDirectlyToSlurpee()
        {
            try
            {
                var dataQuery = "SELECT TOP 1 businessUnitId, credentialHash, credentialVersion\n";
                dataQuery += "FROM Merchandising.settings.dealer_siteCredentials\n";
                dataQuery += "WHERE isLockedOut = 0\n";
                dataQuery += "    AND siteID = " + SiteId + "\n";
                dataQuery += "    AND UserName = '" + UserName + "'\n";

                var dataTable = DataFinder.ExecQuery("Merchandising", dataQuery);

                if (dataTable == null || dataTable.Rows == null || dataTable.Rows.Count <= 0)
                    throw new Exception(String.Format("Could not find the user {0} in Dealer Site Credentials!", UserName));

                var businessUnitId = dataTable.Rows[0]["businessUnitId"].ToString();
                var credentialHash = HttpUtility.UrlEncode(dataTable.Rows[0]["credentialHash"].ToString());
                var credentialVersion =
                    HttpUtility.UrlEncode(
                        BitConverter.ToUInt64(((byte[])dataTable.Rows[0]["credentialVersion"]), 0)
                            .ToString(CultureInfo.InvariantCulture));

                // temporarily use httpbin.org service so beta db can be rebuilt - 2014-11-05
                // var businessUnitId = "111111";

                var streamWriter = new StreamWriter(String.Format("VINs_{0}.txt", Suffix));

                var index = 0;
                foreach (DataRow dataRow in _vinData.Rows)
                {
                    var vin = dataRow["vin"].ToString();
                    var correlationId = Guid.NewGuid();

                    // retrieve XML from Prod S3 and put in Beta bucket/folder
                    CopyS3Object(SourceBucket, vin, TargetBucket, String.Format("{0}/{1}", ProdBackupPrefix, vin));

                    // backup existing XML in Beta root into backup folder
                    if (CopyS3Object(TargetBucket, vin, TargetBucket, String.Format("{0}/{1}", BetaBackupPrefix, vin)))
                        RemoveS3Object(TargetBucket, vin);

                    // Publish the message on the topic
                    var msg = "{\n";
                    msg += "    \"bu_id\":" + businessUnitId + ",\n";
                    msg += "    \"site_id\":" + SiteId + ",\n";
                    msg += "    \"vin\":\"" + vin + "\",\n";
                    msg += "    \"user_name\":\"" + UserName + "\",\n";
                    msg += "    \"pass_word\":\"" + PassWord + "\",\n";
                    msg += "    \"correlation_id\":\"" + correlationId + "\",\n";
                    msg += "    \"creds_proceed_webhook\":\"https://betamax.firstlook.biz/maxservices/OEMCredentialService/api/status/" + credentialHash + "/" + credentialVersion + "\",\n";
                    msg += "    \"creds_pass_webhook\":\"https://betamax.firstlook.biz/maxservices/OEMCredentialService/api/pass/" + credentialHash + "/" + credentialVersion + "\",\n";
                    msg += "    \"creds_fail_webhook\":\"https://betamax.firstlook.biz/maxservices/OEMCredentialService/api/fail/" + credentialHash + "/" + credentialVersion + "\"\n";

                    // temporarily use httpbin.org service so beta db can be rebuilt - 2014-11-05
                    // msg += "    \"creds_proceed_webhook\":\"http://httpbin.org/status/304\",\n";
                    // msg += "    \"creds_pass_webhook\":\"http://httpbin.org/status/200\",\n";
                    // msg += "    \"creds_fail_webhook\":\"http://httpbin.org/status/200\"\n";
                    msg += "}";

                    if (!PublishSnsMessage(TargetTopicArn, msg))
                        Console.WriteLine("Message for VIN {0} could not be published", vin);

                    // Save VIN info to the file and to simple DB for AB Testing after autoload completes
                    streamWriter.WriteLine(vin);

                    index++;
                }

                var vinFile = ((FileStream)(streamWriter.BaseStream)).Name;
                streamWriter.Close();

                Console.WriteLine("Backed up VIN files in Beta to {0}/{1}", TargetBucket, BetaBackupPrefix);
                Console.WriteLine("Copied VIN files from Production to {0}/{1}", TargetBucket, ProdBackupPrefix);
                Console.WriteLine("Completed {0} items! Wrote VINs to {1}", index, vinFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw; // fail the test if we have uncaught exceptions
            }
        }*/

        #region Utility methods
        #region Test set up
        private const String SourceBucket = "Incisent_Merchandising_AutoLoadAudit_Release";
        private const String TargetBucket = "Incisent_Merchandising_AutoLoadAudit_Beta";
        private const String TargetTopicArn = "arn:aws:sns:us-east-1:669197963852:slurpee-manufacturer-request-beta";
        private const String AwsAccessKey = "AKIAJZHH6VT37TTZHYBA";
        private const String AwsSecretAccessKey = "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";

        private DataTable _vinData;
        private IAmazonS3 _s3Client;
        private IAmazonSimpleNotificationService _snsClient;
        private String _suffix;

        public IAmazonS3 S3Client
        {
            get
            {
                if (_s3Client == null)
                    _s3Client = new AmazonS3Client(AwsAccessKey, AwsSecretAccessKey, RegionEndpoint.USEast1);

                return _s3Client;
            }
        }
        public IAmazonSimpleNotificationService SnsClient
        {
            get
            {
                if(_snsClient == null)
                    _snsClient = new AmazonSimpleNotificationServiceClient(AwsAccessKey, AwsSecretAccessKey, RegionEndpoint.USEast1);

                return _snsClient;
            }
        }
        public String Suffix
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_suffix))
                    _suffix = DateTime.UtcNow.ToString("s").Replace(":", "_");

                return _suffix;
            }
        }
        public String BetaBackupPrefix
        {
            get { return String.Format("Back_{0}", Suffix); }
        }
        public String ProdBackupPrefix
        {
            get { return String.Format("ABAL_Test_{0}", Suffix); }
        }

        [TestFixtureSetUp]
        public void GetVinData()
        {
            var dataQuery = "SELECT TOP " + NumberOfVinsToProcess + " RIGHT(v.vin,7),\n";
            dataQuery += "    bu.BusinessUnit,\n";
            dataQuery += "    i.InventoryID,\n";
            dataQuery += "    i.VehicleID,\n";
            //dataQuery += "    als.statusTypeId,\n";
            //dataQuery += "    alst.statusType,\n";
            //dataQuery += "    alst.statusTypeDescription,\n";
            //dataQuery += "    als.lastUpdatedOn,\n";
            dataQuery += "    dsc.siteId,\n";
            //dataQuery += "    oc.chromeStyleID,\n";
            //dataQuery += "    v.ModelCode,\n";
            dataQuery += "    v.Vin,\n";
            //dataQuery += "    v.VehicleTrim,\n";
            //dataQuery += "    m.ModelYear,\n";
            //dataQuery += "    m.ModelName,\n";
            //dataQuery += "    m.DivisionID,\n";
            //dataQuery += "    s.CFModelName,\n";
            //dataQuery += "    s.CFStyleName,\n";
            //dataQuery += "    s.StyleName,\n";
            //dataQuery += "    s.StyleNameWOTrim,\n";
            //dataQuery += "    s.Trim,\n";
            //dataQuery += "    d.DivisionName,\n";
            dataQuery += "    i.InventoryType,\n";
            dataQuery += "    i.MileageReceived,\n";
            dataQuery += "    i.LotPrice\n";
            dataQuery += "FROM imt.dbo.Inventory i\n";
            dataQuery += "JOIN Merchandising.builder.AutoloadStatus als ON als.inventoryID = i.InventoryID\n";
            dataQuery += "    AND als.businessUnitID = i.BusinessUnitID\n";
            dataQuery += "    AND als.statusTypeId  = " + StatusTypeId + "\n";
            dataQuery += "    AND als.lastUpdatedOn BETWEEN DATEADD(week, " + NumOfWeeksBackBegin + " , GETDATE()) AND DATEADD(week, " + NumOfWeeksBackEnd + " , GETDATE())\n";
            dataQuery += "JOIN Merchandising.builder.AutoloadStatusTypes alst ON alst.statusTypeId = als.statusTypeId\n";
            dataQuery += "JOIN Merchandising.settings.Dealer_SiteCredentials dsc ON dsc.businessUnitId = i.BusinessUnitID\n";
            dataQuery += "    AND dsc.siteId = " + SiteId + "\n";
            dataQuery += "    AND dsc.isLockedOut = 0\n";
            dataQuery += "JOIN Merchandising.builder.OptionsConfiguration oc ON oc.inventoryId = i.InventoryID\n";
            dataQuery += "    AND oc.businessUnitID = i.BusinessUnitID\n";
            dataQuery += "JOIN fldw.dbo.Vehicle v ON v.BusinessUnitID = i.BusinessUnitID\n";
            dataQuery += "    AND v.VehicleID = i.VehicleID\n";
            dataQuery += "JOIN VehicleCatalog.Chrome.Styles s ON s.StyleID = oc.chromeStyleID\n";
            dataQuery += "JOIN VehicleCatalog.Chrome.Models m ON m.ModelID = s.ModelID\n";
            dataQuery += "    AND m.CountryCode = " + CountryCode + "\n";
            dataQuery += "    AND m.ModelYear > " + ModelAboveYear + "\n";
            dataQuery += "JOIN VehicleCatalog.Chrome.Divisions d ON d.CountryCode = m.CountryCode\n";
            dataQuery += "    AND d.DivisionID = m.DivisionID\n";
            dataQuery += "JOIN VehicleCatalog.Chrome.Manufacturers man ON man.ManufacturerID = d.ManufacturerID\n";
            dataQuery += "    AND man.CountryCode = d.CountryCode\n";
            dataQuery += "    AND man.ManufacturerName = '" + Manufacturer + "'\n";
            dataQuery += "JOIN imt.dbo.BusinessUnit bu ON bu.BusinessUnitID = i.BusinessUnitID\n";
            dataQuery += "    AND bu.BusinessUnit not like '%windy%'\n";
            dataQuery += "WHERE NOT EXISTS (\n";
            dataQuery += "    SELECT i2.inventoryID\n";
            dataQuery += "    FROM IMT.dbo.Inventory i2\n";
            dataQuery += "    WHERE i2.vehicleID = i.VehicleID\n";
            dataQuery += "        AND i2.BusinessUnitID IN (101590, 101620, 101621, 105239))";
            dataQuery += "ORDER BY als.lastUpdatedOn DESC\n";

            _vinData = DataFinder.ExecQuery("Prod_Merchandising", dataQuery);
        }
        #endregion

        internal Boolean CopyS3Object(String sourceBucket, String sourceKey, String targetBucket, String targetKey)
        {
            var copyResponses = new List<CopyPartResponse>();

            var initRequest = new InitiateMultipartUploadRequest()
            {
                BucketName = targetBucket,
                Key = targetKey
            };

            var initResponse = S3Client.InitiateMultipartUpload(initRequest);
            var uploadId = initResponse.UploadId;

            try
            {
                var metadataRequest = new GetObjectMetadataRequest()
                {
                    BucketName = sourceBucket,
                    Key = sourceKey
                };

                var metadataResponse = S3Client.GetObjectMetadata(metadataRequest);
                var objectSize = metadataResponse.ContentLength; // in bytes

                var partSize = 5L*(long) Math.Pow(2, 20); // 5 MB
                var bytePos = 0L;

                for (var partIndex = 1; bytePos < objectSize; partIndex++)
                {
                    var copyPartRequest = new CopyPartRequest()
                    {
                        DestinationBucket = targetBucket,
                        DestinationKey = targetKey,
                        SourceBucket = sourceBucket,
                        SourceKey = sourceKey,
                        UploadId = uploadId,
                        FirstByte = bytePos,
                        LastByte = bytePos + partSize - 1 >= objectSize ? objectSize - 1 : bytePos + partSize - 1,
                        PartNumber = partIndex
                    };
                    copyResponses.Add(S3Client.CopyPart(copyPartRequest));

                    bytePos += partSize;
                }

                var completeRequest = new CompleteMultipartUploadRequest()
                {
                    BucketName = targetBucket,
                    Key = targetKey,
                    UploadId = initResponse.UploadId
                };

                completeRequest.AddPartETags(copyResponses);
                var completeUploadResponse = S3Client.CompleteMultipartUpload(completeRequest);
            }
            catch (AmazonS3Exception s3ex)
            {
                if (s3ex.ErrorCode != "NotFound")
                    Console.WriteLine(s3ex.Message);

                return false;
            }
            return true;
        }

        internal Boolean RemoveS3Object(String targetBucket, String targetKey)
        {
            try
            {
                var deleteObjectRequest = new DeleteObjectRequest()
                {
                    BucketName = targetBucket,
                    Key = targetKey
                };
                S3Client.DeleteObject(deleteObjectRequest);
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        internal Boolean PublishSnsMessage(String topicArn, String message)
        {
            try
            {
                var publishRequest = new PublishRequest()
                {
                    Message = message,
                    TopicArn = topicArn
                };
                SnsClient.Publish(publishRequest);
                return true;
            }
            catch (AmazonSimpleNotificationServiceException snsEx)
            {
                Console.WriteLine(snsEx.Message);
                return false;
            }
        }
        #endregion


        [Ignore] //This should not be run nightly; it is a utility. Maybe s/b just a method, not Test.
        [Test]
        public void GetListOfVins()
        {
            try
            {
                //var itemList = new List<S3Object>();

                //var request = new ListObjectsRequest
                //{
                //    BucketName = MaxDigitalShowroomS3BucketName,
                //    Prefix = "data/vehicle-json/"
                //};

                //do
                //{
                //    var response = MaxDigitalShowroomS3Client.ListObjects(request);

                //    itemList.AddRange(response.S3Objects);

                    // If response is truncated, set the marker to get the next set of keys.
                //    if (response.IsTruncated)
                //        request.Marker = response.NextMarker;
                //    else
                //        request = null;
                //} while (request != null);

                //Console.WriteLine(itemList.Count);

                //itemList.Sort((x, y) => y.LastModified.CompareTo(x.LastModified));
                //Console.WriteLine(itemList.Count);
                //foreach (var obj in MaxDigitalShowroomS3VehicleJsonObjects)
                //{
                //    var key = obj.Key;
                //    var vin = obj.Key.Substring(MaxDigitalShowroomVehicleJsonPrefix.Length + 1, obj.Key.Length - MaxDigitalShowroomVehicleJsonPrefix.Length - 6);
                //    Console.WriteLine("{0} => {1}", key, vin);
                //}
                foreach (var str in MaxDigitalShowroomS3VinListFromLast24Hours)
                    Console.WriteLine(str);

                Console.WriteLine("");
                Console.WriteLine("Object count = {0}", MaxDigitalShowroomS3VehicleJsonObjects.Count);
                Console.WriteLine("Objects from last 24 hours count = {0}", MaxDigitalShowroomsS3VehicleJsonObjectsFromLast24Hours.Count);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
