﻿using System;
using System.IO;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public static class Utility
    {        

        // This will Take the screen shot of the webpage and will save it at particular location
        public static void SaveScreenShot(String screenshotFirstName, IWebDriver driver)
        {
            if (!Test.ScreenShotsEnabled)
                return;

            //var folderLocation = Environment.CurrentDirectory.Replace("Out","\\ScreenShot\\");
            var folderLocation = Test.ScreenShotFilePath;
            if (!Directory.Exists(folderLocation))
            {
                Directory.CreateDirectory(folderLocation);
            }
            Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            var filename = new StringBuilder(folderLocation);
            filename.Append(screenshotFirstName);
            filename.Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
            filename.Append(".png");
            screenshot.SaveAsFile(filename.ToString(), System.Drawing.Imaging.ImageFormat.Png);
        } 

        public static int ExtractDigitsFromString(String expr)
        {
            return Convert.ToInt32(string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]")));
        }


        public static int GetDigitsFromElement(IWebElement element)
        {
            var text = element.Text;
            return Utility.ExtractDigitsFromString(text);
        }

        public static bool HasClass(this IWebElement ctrl, string cls)
        {
            return (" " + ctrl.GetAttribute("class") + " ").Contains(" " + cls + " ");
        }

    }
}
