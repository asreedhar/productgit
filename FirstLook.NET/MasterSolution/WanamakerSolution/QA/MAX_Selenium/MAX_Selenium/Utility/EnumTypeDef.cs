﻿namespace MAX_Selenium
{
    public enum InventoryType
    {
        NewOrUsed = 0,
        New = 1,
        Used = 2
    }

    public enum ActivityFilterType
    {
        Low_Activity,
        High_Activity,
        ALL
    }

    public enum OEM_Type
    {
        BMW,
        Buick,
        Cadillac,
        Chevrolet, 
        GMC,
        Honda, 
        Chrysler,
        Ford
    }

    public enum StatusFilterType
    {
        Not_Online, // need to replace the "_" with " " when using it. "var st = (StatusFilterType)Enum.Parse(typeof(StatusFilterType), str.Replace(' ', '_'));"
        Online,
        All
    }

    public enum NewOrUsedFilterType
    {
        New_Used,
        Used_Only,
        New_Only
    }

    public enum SelectSiteFilterType
    {
        Overall,
        Cars,  // Cars.com
        AutoTrader  // Auto Trader.com
    }

    public enum NewOrUsedFilterType_P
    {
        Both,
        New,
        Used
    }

    public enum Month
    {
        January,
        February,
        March,
        April,
        May,
        June,
        July,
        August, 
        September,
        October,
        November,
        December
    }
}