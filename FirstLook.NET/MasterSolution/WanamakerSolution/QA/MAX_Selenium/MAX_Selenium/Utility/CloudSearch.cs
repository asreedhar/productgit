﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Text;

namespace MAX_Selenium
{
    /// <summary>
    /// Provides means to connect to Amazon CloudSearch and return data.
    /// 
    /// </summary>
    public static class CloudSearch
    {

        public static string Vehicle_Search_Query_stub = Test.CloudSearchURL + "/vehicle-search";
        public static string Dealer_Search_Query_stub = Test.CloudSearchURL + "/dealer-search";

        // constructor
        static CloudSearch()
        {
        }

        //Returns JSON string from the CloudSearch API, given the query string as a Uri object
        public static string GetCloudSearchData(Uri apiQuery)
        {
            var request = WebRequest.Create(apiQuery);
            var response = request.GetResponse();
            var dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);
            var jsonString = reader.ReadToEnd();
            return jsonString;

        }




    }
}
