﻿using System;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    class Setup_TestData : Test
    {
        private static object[] All_Test_Vehicles_WindyCityPontiacBuickGMC  = {
                                                                            
                               //commented out the new cars, since In-Transit-Inventory can only create used carss.
                                                               //new [] {"1GKFK66U76J989391", "Test_GM_NEW", "5", "34997"},
                                                               new [] {"3G5DB03L96S987966", "Test_GM_USED", "35000", "4997"},
                                                               //new [] {"JM1DE1LZ0C0137668", "Test_Mazda_NEW", "5", "18000"},
                                                               new [] {"WBADW3C54DE824615", "Test_BMW328_USED", "35000", "24997"},
                                                               new [] {"SALSH2E47AA222888", "Test_LandRover_USED", "35000", "34997"},                                                               
                                                               new [] {"WVGZE77L97D052257", "Test_VW_Used", "45375", "25000"}  
                                                           };
        private static object[] All_Test_Vehicles_WindyCityBMW = {
                                                               new [] {"5UXFE43568L031112", "Test_BMW_X5_USED", "30000", "34997"},
                                                               //new [] {"WBAYF4C54DDE23558", "Test_BMW_740Li_NEW", "30000", "84997"},
                                                               new [] {"WBA3A5C56CF000016", "Test_BMW328I_USED", "30000", "34997"}
                                                           };

        [Ignore] //This should only be run 1x per milestone
        [TestCaseSource("All_Test_Vehicles_WindyCityPontiacBuickGMC")]
        public void Create_Test_Vehicles_Through_In_Transit_WindyCityPontiacBuickGMC(string vin, string SN, string mile, string P)
        {
            InitializePageAndLogin_HomePage();
            homePage.Create_New_InTransit_Inventory(vin, SN, mile, P);
        }

        [Ignore] //Should only be run 1x per milestone
        [TestCaseSource("All_Test_Vehicles_WindyCityBMW")]
        public void Create_Test_Vehicles_Through_In_Transit_WindyCityBMW(string vin, string SN, string mile, string P)
        {
            LoginAsAdmin_HomePage("qashiner", "N@d@123", "Windy City BMW");
            homePage.Create_New_InTransit_Inventory(vin, SN, mile, P);
        }

        [Ignore] //Should only be run when necessary, such as after data restores.
        [Test]
        [Description("This simple Update writes text into all snapshot dealer phone numbers, " +
                     "thereby unsynchronizing view/snapshot and forcing an update to the snapshot and dealer CloudSearch.")]
        public void UnsynchronizeDealerProfileSnapshot()
        {
            var query = @"UPDATE Merchandising.settings.DealerProfilePublishedSnapshot SET Phone = 'forceupdate'";
            DataFinder.ExecQuery(DataFinder.Merchandising, query);

        }

    }
}
