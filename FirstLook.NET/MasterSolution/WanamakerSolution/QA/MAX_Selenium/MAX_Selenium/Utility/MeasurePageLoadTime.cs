﻿/*
using log4net;
using log4net.Config;
using System;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    internal class MeasurePageLoadTime : Test
    {
        // Define a static logger variable so that it references the
        // Logger instance named "MeasurePageLoadTime".
        private static readonly ILog log = LogManager.GetLogger(typeof(MeasurePageLoadTime));

        [Test]
        [Description("Measure the time it takes to load pages")]
        public void Load_MAX_Inventory_Page_From_FirstLook_Home_C3856()
        {
            InitializePageAndLogin_HomePage();
            homePage.GotoInventoryPage();
            Stopwatch stopWatch = Stopwatch.StartNew();

            // Wait for the page to load, timeout after 10 seconds
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until((d) => { return d.Title.ToLower().StartsWith("Inventory"); });

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            
            //log the time 
        }

    }
}
*/