﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace MAX_Selenium
{
    internal class PageSearchDataPoint : IComparable
    {
        static Catalysoft.SimilarityTool similarity_tool = new Catalysoft.SimilarityTool();

        public IWebElement page_element;
        public string page_search;

        public string element_text;
        public double similarity;

        public PageSearchDataPoint( IWebElement page_element, string page_search ) {
            this.page_element = page_element;
            this.element_text = page_element != null? page_element.Text:null;
            this.page_search = page_search;
            this.similarity = 0.0d;
            if( this.element_text != null )
                this.similarity = similarity_tool.CompareStrings( this.element_text, this.page_search );
            else if( page_search == null || page_search == "" )
                this.similarity = 1.0d;
        }

        // descending order
        public int CompareTo( object obj )
        {
            return Math.Sign( (obj as PageSearchDataPoint).similarity - similarity );
        }
    }
}
