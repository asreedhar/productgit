﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace MAX_Selenium
{
    public static class DataFinder
    {
        public static readonly string Merchandising = "Merchandising";
        public static readonly string Chrome = "Chrome";
        public static readonly string IMT = "IMT";

        public static DataTable ExecQuery(string database, string query)
        {
            var provider = GetDbFactory(database);

            using (var cn = GetOpenConnection(database))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandText = query;

                var adapter = provider.CreateDataAdapter();
                if (adapter == null)
                    throw new InvalidOperationException("Failed to create adapter.");

                adapter.SelectCommand = (DbCommand) cmd;

                var table = new DataTable();
                adapter.Fill(table);

                return table;
            }
        }

        public static DbProviderFactory GetDbFactory(string database)
        {
            var connectionStringConfig = ConfigurationManager.ConnectionStrings[database];
            return DbProviderFactories.GetFactory(connectionStringConfig.ProviderName);
        }

        public static IDbConnection GetOpenConnection(string database)
        {
            var connectionStringConfig = ConfigurationManager.ConnectionStrings[database];
            var dbFactory = DbProviderFactories.GetFactory(connectionStringConfig.ProviderName);

            var cn = dbFactory.CreateConnection();
            if (cn == null)
                throw new InvalidOperationException("Failed to create connection.");

            var success = false;
            try
            {
                cn.ConnectionString = connectionStringConfig.ConnectionString;
                cn.Open();
                success = true;
                return cn;
            }
            finally
            {
                if (!success)
                    cn.Dispose();
            }
        }
    }
}