﻿using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    /// <summary>
    /// Provides a convenient method for manipulating selections of menu items in an HTML popover element.
    /// 
    /// </summary>
    public class SelectPopoverElement : IWrapsElement
    {
        private readonly IWebElement element;

        /// <summary>
        /// Gets the <see cref="T:OpenQA.Selenium.IWebElement"/> wrapped by this object.
        /// 
        /// </summary>
        public IWebElement WrappedElement
        {
            get { return this.element; }
        }

        /// <summary>
        /// Gets a value indicating whether the parent element supports multiple selections.
        /// 
        /// </summary>
        public bool IsMultiple { get; private set; }

        /// <summary>
        /// Gets the list of options for the popover element.
        /// 
        /// </summary>
        public IList<IWebElement> Options
        {
            get { return (IList<IWebElement>)this.element.FindElements(By.XPath(".//div[contains(@class, 'menu-item')]")); }
        }

        /// <summary>
        /// Gets the selected item within the popover menu element.
        /// 
        /// </summary>
        /// 
        /// <remarks>
        /// If more than one item is selected this may not work
        /// </remarks>
        /// <exception cref="T:OpenQA.Selenium.NoSuchElementException">Thrown if no option is selected.</exception>
        public IWebElement SelectedOption
        {
            get
            {
                try
                {
                    IWebElement webElement = this.element.FindElement(By.XPath(".//div"));
                    return webElement;
                }
                catch (Exception)
                {

                    throw new NoSuchElementException("No option is selected");
                }
            }
        }

            /// <summary>
    /// Initializes a new instance of the SelectPopoverElement class.
    /// 
    /// </summary>
    /// <param name="element">The element to be wrapped</param><exception cref="T:System.ArgumentNullException">Thrown when the <see cref="T:OpenQA.Selenium.IWebElement"/> object is <see langword="null"/></exception><exception cref="T:OpenQA.Selenium.Support.UI.UnexpectedTagNameException">Thrown when the element wrapped is not a &lt;select&gt; element.</exception>
    public SelectPopoverElement(IWebElement element)
    {
      if (element == null)
        throw new ArgumentNullException("element", "element cannot be null");
      if (string.IsNullOrEmpty(element.TagName) || string.Compare(element.TagName, "div", StringComparison.OrdinalIgnoreCase) != 0)
        throw new UnexpectedTagNameException("div", element.TagName);
      this.element = element;
      //element.Click();
      string attribute = element.GetAttribute("multiple");
      this.IsMultiple = attribute != null && attribute.ToLowerInvariant() != "false";
    }

    /// <summary>
    /// Select all options by the text displayed.
    /// 
    /// </summary>
    /// <param name="text">The text of the option to be selected. If an exact match is not found,
    ///             this method will perform a substring match.</param>
    /// <remarks>
    /// When given "Bar" this method would select an option like:
    /// 
    /// <para>
    /// &lt;option value="foo"&gt;Bar&lt;/option&gt;
    /// 
    /// </para>
    /// 
    /// </remarks>
    /// <exception cref="T:OpenQA.Selenium.NoSuchElementException">Thrown if there is no element with the given text present.</exception>
    public void SelectByText(string text)
    {

        if (text == null)
            throw new ArgumentNullException("text", "text must not be null");
        IList<IWebElement> list = (IList<IWebElement>)this.element.FindElements(By.XPath(".//span[normalize-space(.) = " + SelectPopoverElement.EscapeQuotes(text) + "]"));
        bool flag = false;
        foreach (IWebElement option in (IEnumerable<IWebElement>)list)
        {
            var clickableElement = element.FindElement(By.CssSelector("div.menu-label"));
            clickableElement.Click(); //expose the DDL popover list
            SelectPopoverElement.SetSelected(option);
            if (!this.IsMultiple)
                return;
            flag = true;
        }
        if (list.Count == 0 && text.Contains(" "))
        {
            string substringWithoutSpace = SelectPopoverElement.GetLongestSubstringWithoutSpace(text);
            foreach (IWebElement option in !string.IsNullOrEmpty(substringWithoutSpace) ? (IEnumerable<IWebElement>)this.element.FindElements(By.XPath(".//span[contains(., " + SelectPopoverElement.EscapeQuotes(substringWithoutSpace) + ")]")) : (IEnumerable<IWebElement>)this.element.FindElements(By.TagName("span")))
            {
                if (text == option.Text)
                {
                    element.Click(); //expose the DDL popover list
                    SelectPopoverElement.SetSelected(option);
                    if (!this.IsMultiple)
                        return;
                    flag = true;
                }
            }
        }
        if (!flag)
            throw new NoSuchElementException("Cannot locate element with text: " + text);
    }


    /// <summary>
    /// Select the option by the index, as determined by an iterator
    /// 
    /// </summary>
    /// <param name="index">The value of the index attribute of the option to be selected.</param><exception cref="T:OpenQA.Selenium.NoSuchElementException">Thrown when no element exists with the proposed index.</exception>
    public void SelectByIndex(int index)
    {
        int counter = 0;

        bool flag = false;
        foreach (IWebElement option in (IEnumerable<IWebElement>)this.Options)
        {
            
            if (counter == index)
            {
                var clickableElement = element.FindElement(By.CssSelector("div.menu-label"));
                clickableElement.Click(); //expose the DDL popover list
                SelectPopoverElement.SetSelected(option);
                if (!this.IsMultiple)
                    return;
                flag = true;
            }
            counter += 1;

        }
        if (!flag)
            throw new NoSuchElementException("Cannot locate option with index: " + (object)index);
    }


    private static string EscapeQuotes(string toEscape)
    {
        if (toEscape.IndexOf("\"", StringComparison.OrdinalIgnoreCase) > -1 && toEscape.IndexOf("'", StringComparison.OrdinalIgnoreCase) > -1)
        {
            bool flag = false;
            if (toEscape.LastIndexOf("\"", StringComparison.OrdinalIgnoreCase) == toEscape.Length - 1)
                flag = true;
            List<string> list = new List<string>((IEnumerable<string>)toEscape.Split(new char[1]
        {
          '"'
        }));
            if (flag && string.IsNullOrEmpty(list[list.Count - 1]))
                list.RemoveAt(list.Count - 1);
            StringBuilder stringBuilder = new StringBuilder("concat(");
            for (int index = 0; index < list.Count; ++index)
            {
                stringBuilder.Append("\"").Append(list[index]).Append("\"");
                if (index == list.Count - 1)
                {
                    if (flag)
                        stringBuilder.Append(", '\"')");
                    else
                        stringBuilder.Append(")");
                }
                else
                    stringBuilder.Append(", '\"', ");
            }
            return ((object)stringBuilder).ToString();
        }
        else if (toEscape.IndexOf("\"", StringComparison.OrdinalIgnoreCase) > -1)
            return string.Format((IFormatProvider)CultureInfo.InvariantCulture, "'{0}'", new object[1]
        {
          (object) toEscape
        });
        else
            return string.Format((IFormatProvider)CultureInfo.InvariantCulture, "\"{0}\"", new object[1]
        {
          (object) toEscape
        });
    }

    private static string GetLongestSubstringWithoutSpace(string s)
    {
        string str1 = string.Empty;
        string str2 = s;
        char[] chArray = new char[1]
      {
        ' '
      };
        foreach (string str3 in str2.Split(chArray))
        {
            if (str3.Length > str1.Length)
                str1 = str3;
        }
        return str1;
    }

    private static void SetSelected(IWebElement option)
    {
        if (option.Selected)
            return;
        option.Click();
    }


    }

}
