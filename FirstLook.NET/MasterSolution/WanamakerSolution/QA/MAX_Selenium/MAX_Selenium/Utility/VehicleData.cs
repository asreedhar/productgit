﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAX_Selenium
{
    public class VehicleData
    {
        public int Year { get; private set; }
        public string Make { get; private set; }
        public string Model { get; private set; }
        public string Trim { get; private set; }
        public byte Certified { get; private set; }
        public string CertifiedId { get; private set; }
        public byte InventoryType { get; private set; }
        
        public class BasicVehicleData: VehicleData
        {
            // constructors
            public BasicVehicleData(string vin)
            {
                var query = String.Format(@"SELECT VehicleYear, Make, Model, trim, Certified, CertifiedID, InventoryType
                                        FROM Merchandising.workflow.Inventory
                                        WHERE Vin = '{0}'", vin);

                var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
                DataRow dr = dataset.Rows[0];

                FillBasicDataProperties(dr);

            }

            public BasicVehicleData(int inventoryId)
            {
                var query = String.Format(@"SELECT VehicleYear, Make, Model, trim, Certified, CertifiedID, InventoryType
                                        FROM Merchandising.workflow.Inventory
                                        WHERE inventoryId = {0}", inventoryId);

                var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
                DataRow dr = dataset.Rows[0];

                FillBasicDataProperties(dr);

            }
        }

        // methods
        private void FillBasicDataProperties(DataRow dRow)
        {
            Year = (int)dRow.ItemArray[0]; //unbox
            Make = Convert.ToString(dRow.ItemArray[1]);
            Model = Convert.ToString(dRow.ItemArray[2]);
            Trim = Convert.ToString(dRow.ItemArray[3]);
            Certified = (byte)(dRow.ItemArray[4]); //unbox tinyint
            CertifiedId = Convert.ToString(dRow.ItemArray[5]);
            InventoryType = (byte)(dRow.ItemArray[6]);
        }





    }
}
