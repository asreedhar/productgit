﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace MAX_Selenium
{
    /// <summary>
    /// Provides dealer data through queries to various backing stores.
    /// 
    /// </summary>
    public class DealerData
    {
        public int BusinessUnitId { get; private set; }
        //public int CSBusinessUnitId { get; private set; }
        public string BusinessUnitName { get; private set; }
        public string OwnerHandle { get; private set; }
        public int GroupId { get; private set; }
        public bool PricingVersion { get; private set; } // 0 = Old Ping, 1 = New Ping
        public int NewCarsEnabled { get; private set; }
        public int UsedCarsEnabled { get; private set; }
        public int Website20Enabled { get; private set; }
        public int Website10Enabled { get; private set; }
        public int ShowroomEnabled { get; private set; }
        public int MobileShowroomEnabled { get; private set; }


        public class BasicDealerData: DealerData
        {
            // constructors
            public BasicDealerData(string dealerName)
            {
                var query = String.Format(@"SELECT MSD.BusinessUnitID, BusinessUnitName, OwnerHandle, GroupId, IsNewPing
                                        FROM Merchandising.settings.DealerProfile MSD
                                        JOIN IMT.dbo.DealerPreference_Pricing DPP ON MSD.BusinessUnitID = DPP.BusinessUnitID
                                        WHERE BusinessUnitName LIKE '{0}'", dealerName);

                var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
                DataRow dr = dataset.Rows[0];

                FillBasicDataProperties(dr);

            }

            public BasicDealerData(int buId)
            {
                var query = String.Format(@"SELECT MSD.BusinessUnitID, BusinessUnitName, OwnerHandle, GroupId, IsNewPing
                                        FROM Merchandising.settings.DealerProfile MSD
                                        JOIN IMT.dbo.DealerPreference_Pricing DPP ON MSD.BusinessUnitID = DPP.BusinessUnitID
                                        WHERE MSD.BusinessUnitID = {0}", buId);

                var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
                DataRow dr = dataset.Rows[0];

                FillBasicDataProperties(dr);

            }

        }

        public class BasicCloudSearchDealerData: DealerData
        {

            public BasicCloudSearchDealerData(string ownerHandle)
            {
                var csQueryStub = new UriBuilder(CloudSearch.Dealer_Search_Query_stub);
                csQueryStub.Query = "q.parser=lucene" + "&" + "q=ownerhandle:%22" + ownerHandle + "%22"; //how MSDN handles "&"; cannot append string directly
                var csQuery = csQueryStub.Uri;
                var jsontext = CloudSearch.GetCloudSearchData(csQuery);
                var vehicleSearch = JsonConvert.DeserializeObject<dynamic>(jsontext);

                FillBasicCSProperties(vehicleSearch);

            }

        }


        // methods
        private void FillBasicDataProperties(DataRow dRow)
        {
            BusinessUnitId = (int)dRow.ItemArray[0]; //unbox
            BusinessUnitName = Convert.ToString(dRow.ItemArray[1]);
            OwnerHandle = Convert.ToString(dRow.ItemArray[2]).ToUpper();
            GroupId = (int)dRow.ItemArray[3];
            PricingVersion = (bool)(dRow.ItemArray[4]); //unbox bit

        }

        private void FillBasicCSProperties(dynamic jsonText)
        {
            BusinessUnitId = (int)jsonText.hits.hit[0].fields.businessunitid;
            NewCarsEnabled = (int)jsonText.hits.hit[0].fields.new_vehicles_enabled;
            UsedCarsEnabled = (int)jsonText.hits.hit[0].fields.used_vehicles_enabled;
            Website20Enabled = (int) jsonText.hits.hit[0].fields.maxforwebsite2_enabled;
            Website10Enabled = (int)jsonText.hits.hit[0].fields.maxforwebsite1_enabled;
            Website10Enabled = (int)jsonText.hits.hit[0].fields.maxforwebsite1_enabled;
            ShowroomEnabled = (int)jsonText.hits.hit[0].fields.showroom_enabled;
            MobileShowroomEnabled = (int) jsonText.hits.hit[0].fields.mobileshowroom_enabled;

        }

    }
}
