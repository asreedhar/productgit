---------------------------------------------------------------------------------------
--	To run this script, user needs the write permissions to DBASTAT, IMT, FLDW database
--  Also, it is better to change the dates in these vehicles to current month to avoid 
--  the Age Bucket issue that blocks the Ping II pricing page being working.
---------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--	To run this script, user needs the write permissions to DBASTAT, IMT, FLDW database
----------------------------------------------------------------------------------------
USE DBASTAT
------------------------------------------------------------------------
--	Disable any straggler extracts (shouldn't be any)
------------------------------------------------------------------------

UPDATE	DH
SET	ProcessResult = 99
FROM	dbo.Dataload_History DH 
WHERE	ProcessResult = 3 
	AND FileType IN (1,2)

------------------------------------------------------------------------
--	Find a processed load ID to use as a model inventory extract
------------------------------------------------------------------------

DECLARE @ModelLoadID INT

SELECT	@ModelLoadID = MAX(Load_ID)
FROM	dbo.Dataload_History DH
WHERE	FileType IN (1)
	AND ProcessResult = 5

------------------------------------------------------------------------
--	From the resultset, pick a test dealer.  Let's pick AIRPORTC01.
------------------------------------------------------------------------

/*
SELECT	*
FROM	DBASTAT.dbo.Dataload_Raw_History DRH
	INNER JOIN DBASTAT.dbo.Dataload_History DH ON DRH.LOAD_ID = DH.Load_ID
	INNER JOIN DBASTAT.dbo.Datasources DS ON DH.DatasourceID = DS.DatasourceID
WHERE	DRH.LOAD_ID = @ModelLoadID

*/
------------------------------------------------------------------------
--	Register a new virtual inventory load.  Please note that each
--	load must have a unique file name in order to be processed
------------------------------------------------------------------------


DECLARE @TestLoadID INT

INSERT
INTO	dbo.Dataload_History (SourceType, FileName, FileType, DealerStatus, EntryDT, ProcessResult, DatafeedCode)
SELECT	1, 'WINDYCIT04-TEST-02', 1, 4, GETDATE(), 3, 'DMi'

SET @TestLoadID = SCOPE_IDENTITY()

------------------------------------------------------------------------
--	Duplicate the model extract, replacing the model load ID w/the
--	test.  We only want the rows previously marked as Add or Update
--	as they were from the original file.
--
--	Yes, there are far more columns than you would need.  Feel free
--	to cull the ones you don't care about.
------------------------------------------------------------------------

-- WINDYCIT04 = Windy City Pontiac Buick GMC = 101621

INSERT INTO	dbo.Dataload_Raw_History	
	(
	DEALER_ID, DEALER_STATUS, EXCEPTION_CODE, DELTA_FLAG, SALE_STATUS, SALES_REF_NUM, DEAL_NUM_FI, DEAL_DT, VIN, STOCK_NUM, VEH_YEAR, MAKE, MODEL, MODEL_PKG, BODY_TYPE, DOOR_CT, 
	VEH_TYPE, VEH_CLASS, BASE_COLOR, EXT_COLOR, INT_COLOR, INT_DESC, ENGINE_DESC, CYLINDER_CT, FUEL_CODE, TRANS_DESC, DRIVE_TRAIN_DESC, MILEAGE, USED_SELLING_PRICE, DISPOSITION, 
	CERTIFIED, ACQUISITION_PRICE, PHOTO_FILE_NAME, PHOTO_AVAIL_FLAG, RECON_COST, PACK_AMOUNT, UNIT_COST, SALE_PRICE, FRONT_END_GROSS, BACKEND_GROSS, AFTERMARKET_GROSS, TOTAL_GROSS, 
	ENTRY_DT, MODIFIED_DT, VEH_STATUS, VEH_STATUS_CODE, VEH_LOCATION, VEH_SOURCE, VEH_SOURCE_DTL, SALESPERSON_ID, SALESPERSON_LASTNAME, SALESPERSON_FIRSTNAME, SALES_COMMISSION, 
	OTHER_COMMISSIONS, PWR_WINDOWS, PWR_LOCKS, VEH_PROMOTION, LEASE_FLAG, AIR_BAGS, AIR_CONDITIONING, SALE_DESC, CRUISE, REF_DT, RECEIVED_DT, ALARM_SYSTEM, AGE_IN_DAYS, 
	CUSTOMER_TYPE, CUSTOMER_ADDRESS1, CUSTOMER_ADDRESS2, CUSTOMER_CITY, CUSTOMER_STATE, CUSTOMER_ZIP, CUSTOMER_ZIP_EXT, SUN_ROOF, TILT_STEERING, PWR_STEERING, CONVERTIBLE, 
	PWR_SEATS, AUDIO_AM_FM, AUDIO_CASS, AUDIO_CD, BRAKES_DISC, BRAKES_FR_DISC_REAR_DRUM, BRAKES_ABS, TIRE_ALL_SEASON, TIRE_ALL_TERR, TIRE_OFF_ROAD, TIRE_PERF, TIRE_HWY, 
	TIRE_TOURING, WHEEL_ALLOY, WHEEL_ALUM, WHEEL_CHROME, WHEEL_SPECIAL, FILLER1, FILLER2, FILLER3, FILLER4, FILLER5, FILLER6, FILLER7, FILLER8, FILLER9, FILLER10, FILLER11, 
	FILLER12, FILLER13, FILLER14, FILLER15, FILLER16, FILLER17, FILLER18, FILLER19, FILLER20, 
	--AUDIT_ID, 
	LOAD_ID, ORIGINAL_EXT_COLOR, ORIGINAL_INT_COLOR, ACQUISITION_SOURCE, WHSL_BUYER, ACCOUNTING_DT, REVERSAL_DT, MODEL_CODE, BRANCH_NUMBER, DEALERSHIP_IDENTIFIER, LOT_PRICE, MSRP, 
	EXT_COLOR_CD, INT_COLOR_CD, STORE_NUMBER, USED_GROUP, MEMO, BEST_PRICE, CODED_COST, INVOICE_AMOUNT, INVENTORY_GL_AMOUNT, SALES_COST, DMV_AMOUNT, FLOORPLAN_AMOUNT
	)
-- new car 2011 Cadillac Escalade
--SELECT 'WINDYCIT04',4,0,'A',NULL,NULL,NULL,NULL,'1GYS4CEF4BR146017',5936,2011,'CADILLAC','ESCALADE','PREMIUM','4DR UTILITY',4,'U',NULL,'RED','INFRARED',NULL,NULL,'6.2L V8',8,'F','AUTO 6SPD','AWD',5,0,NULL,'N',0,NULL,NULL,NULL,NULL,69934.35,NULL,NULL,NULL,NULL,NULL,'03/14/2012 12:50:49PM','03/12/2012 12:15',NULL,'S',NULL,'P',NULL,NULL,NULL,NULL,NULL,NULL,'Y','Y',NULL,NULL,'Y','Y',NULL,'Y','03/12/2012 12:02:38AM','3/7/2012','Y',314,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,LOAD_ID = @TestLoadID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
--UNION ALL
-- used car 2010 Chevrolet Cobalt
--SELECT 'WINDYCIT04',4,0,'A',NULL,NULL,NULL,NULL,'1G1AD5F5XA7198404',39392,2010,'CHEVROLET','COBALT','LT','4DR',4,'C',NULL,'RED','RED',NULL,NULL,'2.2L I4',4,'G',NULL,'FWD',34340,0,NULL,'N',10933,NULL,NULL,NULL,NULL,11272.34,NULL,NULL,NULL,NULL,NULL,'03/07/2012 12:02:21AM','3/23/2012 16:45',NULL,'S',NULL,'P',NULL,NULL,NULL,NULL,NULL,NULL,'Y','Y',NULL,NULL,'Y','Y',NULL,'N','03/23/2012 12:02:25AM','3/6/2012','Y',6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N','Y','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'U',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,LOAD_ID = @TestLoadID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL

-- new car 2011 Cadillac Escalade
SELECT 'WINDYCIT04',4,0,'A',NULL,NULL,NULL,NULL,'1GYS4CEF4BR146017',5936,2011,'CADILLAC','ESCALADE','PREMIUM','4DR UTILITY',4,'U',NULL,'RED','INFRARED',NULL,NULL,'6.2L V8',8,'F','AUTO 6SPD','AWD',5,70000,NULL,'N',0,NULL,NULL,NULL,NULL,69934.35,70000,NULL,NULL,NULL,NULL,'03/27/2012 12:50:49PM','03/27/2012 12:15',NULL,'S',NULL,'P',NULL,NULL,NULL,NULL,NULL,NULL,'Y','Y',NULL,NULL,'Y','Y',NULL,'Y','03/27/2012 12:02:38AM','3/27/2012','Y',314,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y','Y','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,LOAD_ID = @TestLoadID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
UNION ALL
-- used car 2010 Chevrolet Cobalt
SELECT 'WINDYCIT04',4,0,'A',NULL,NULL,NULL,NULL,'1G1AD5F5XA7198404',39392,2010,'CHEVROLET','COBALT','LT','4DR',4,'C',NULL,'RED','RED',NULL,NULL,'2.2L I4',4,'G',NULL,'FWD',34340,10000,NULL,'N',10933,NULL,NULL,NULL,NULL,11272.34,10000,NULL,NULL,NULL,NULL,'03/27/2012 12:02:21AM','3/27/2012 16:45',NULL,'S',NULL,'P',NULL,NULL,NULL,NULL,NULL,NULL,'Y','Y',NULL,NULL,'Y','Y',NULL,'N','03/27/2012 12:02:25AM','3/27/2012','Y',6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N','Y','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'U',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,LOAD_ID = @TestLoadID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL


------------------------------------------------------------------------
--	Change the data for load @TestLoadID at will.
--	If you want to add new inventory, just duplicate an existing row
--	and minimally change the VIN and Stock Number.  Make sure 
--	the DELTA_FLAG is set to 'A'
------------------------------------------------------------------------	


------------------------------------------------------------------------	
--	Once the data has been modified, load the interface table for 
--	the test load.  An interface table is another name for the
--	staging table that all data for the load process must "past 
--	through" in order to be loaded
------------------------------------------------------------------------	

EXEC DMS.Interface#Load @LoadID = @TestLoadID -- int


------------------------------------------------------------------------	
--	Verify that the load is now in state #4 (ProcessResult) -- ready
--	to load
------------------------------------------------------------------------


SELECT	*
FROM	dbo.Dataload_History DH
WHERE	LOAD_ID = @TestLoadID


------------------------------------------------------------------------	
--	Process the interface table, effecting the load to the IMT base
--	tables Inventory and Vehicle
------------------------------------------------------------------------

EXEC DMS.Interface#Process @RunPostLoad = 0 -- bit


------------------------------------------------------------------------	
--	Check the load status w/above SELECT.  Should be in state #5.
--	Check the results of the load process w/the following:
------------------------------------------------------------------------

SELECT	InventoryStatus		= AIS.AuditStatusDESC, 
	SalesStatus		= SS.AuditStatusDESC, 
	FatalException		= QA.dbo.GetExceptionDescription2(ER.ExceptionRuleID), 
	*
FROM	DBASTAT.dbo.Dataload_Raw_History DRH
	INNER JOIN DBASTAT.dbo.Dataload_History DH ON DRH.LOAD_ID = DH.Load_ID
	INNER JOIN DBASTAT.dbo.Datasources DS ON DH.DatasourceID = DS.DatasourceID
	LEFT JOIN (	DBASTAT.dbo.Dataload_Audit_Exceptions AX
			INNER JOIN QA.dbo.ExceptionRules ER ON AX.ExceptionRuleID = ER.ExceptionRuleID
								AND er.SeverityCD = 1
			) ON AX.Audit_ID = DRH.AUDIT_ID
	LEFT JOIN (	DBASTAT.dbo.Dataload_Audit_Inventory AI
			INNER JOIN IMT.dbo.lu_AuditStatus AIS ON AI.STATUS = AIS.AuditStatusCD
			)  ON DRH.AUDIT_ID = AI.AUDIT_ID
	LEFT JOIN (	DBASTAT.dbo.Dataload_Audit_Sales _AS 
			INNER JOIN IMT.dbo.lu_AuditStatus SS ON [_AS].STATUS = SS.AuditStatusCD
			) ON DRH.AUDIT_ID = _AS.AUDIT_ID

WHERE	DRH.LOAD_ID = @TestLoadID


------------------------------------------------------------------------
--	If the rows processed, the data will be in IMT.  Run your fave
--	IMT query to check it out.
------------------------------------------------------------------------
--SELECT BU.BusinessUnitCode, *
--FROM   IMT.dbo.Inventory I
--       INNER JOIN IMT.dbo.Vehicle V ON I.VehicleID = V.VehicleID
--       INNER JOIN IMT.dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID
--WHERE  V.VIN = '1G1AD5F5XA7198404'  -- replace with the real VIN you want to check

------------------------------------------------------------------------
-- Once the Vehicles are in IMT
-- Run the following script to fetch/load the data to FLDW from IMT 
------------------------------------------------------------------------
USE FLDW
EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 2
EXEC LoadWarehouseTables @TableTypeID = 1, @TableID = 4

------------------------------------------------------------------------
-- To check the load history for a giving user
------------------------------------------------------------------------

--SELECT * FROM DBASTAT.dbo.Log_Journal LJ 
--WHERE user_name = 'FIRSTLOOK\jsu'
--ORDER BY log_id desc
