﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Admin_Miscellaneous_Page : Admin_Home_Page
    {
        //element finders
        [FindsBy(How = How.Id, Using = "Max30Upgrade")] 
        public IWebElement Max30Upgrade_Checkbox;

        [FindsBy(How = How.Id, Using = "OEMBrandingEnabled")] 
        public IWebElement MaxForOEM_Checkbox;

        [FindsBy(How = How.Id, Using = "BatchAutoloadEnabled")] 
        public IWebElement Batch_Autoload_Checkbox;

        [FindsBy(How = How.Id, Using = "MaxForOEMList")] 
        public IWebElement MaxForOEM_DropDown;

        [FindsBy(How = How.Id, Using = "DashboardEnabled")]
        public IWebElement DashBoard_Checkbox;

        [FindsBy(How = How.Id, Using = "ModelSpecificAdsEnabled")]
        public IWebElement Model_SpecificAds_Checkbox;

        [FindsBy(How = How.Id, Using = "AnalyticsSuiteCheckBox")]
        public IWebElement Digital_Performance_Analytics_Checkbox;

        [FindsBy(How = How.Id, Using = "DealerSegmentList_1")]
        public IWebElement Import_RadioButton;

        [FindsBy(How = How.Id, Using = "DealerSegmentList_2")]
        public IWebElement Highline_RadioButton;

        [FindsBy(How = How.Id, Using = "GroupLevelDashboardEnabled")]
        public IWebElement Group_Level_Dashboard_Checkbox;

        [FindsBy(How = How.Id, Using = "chkOptimalFormat")] 
        public IWebElement SendOptimaFormat_Checkbox;

        [FindsBy(How = How.Id, Using = "QueueAutoLoadButton")] 
        public IWebElement Requeue_Button;

        [FindsBy(How = How.Id, Using = "chkNewCars")]
        public IWebElement Enable_NewCar_Pricing_Checkbox;

        [FindsBy(How = How.Id, Using = "batchPriceNewCars")]
        public IWebElement Enable_Batch_NewCar_Pricing_Checkbox;

        [FindsBy(How = How.Id, Using = "AutoOffline_WholesalePlanTrigger")]
        public IWebElement AutoOffLine_CheckBox;

        [FindsBy(How = How.Id, Using = "CtrGraph")]
        public IWebElement CtrGraph_CheckBox;

        [FindsBy(How = How.Id, Using = "PhotoTransfersEnabled")]
        public IWebElement PhotoTransfers_CheckBox;

        [FindsBy(How = How.Id, Using = "MaxAnalyticsOnlineCheckbox")]
        public IWebElement MaxAnalyticsOnline_CheckBox;

        [FindsBy(How = How.Id, Using = "MaxAnalyticsPerformanceSummaryCheckbox")]
        public IWebElement MaxAnalyticsPerformanceSummary_CheckBox;

        [FindsBy(How = How.Id, Using = "SaveButton")]
        public IWebElement Save_Button;

        [FindsBy(How = How.Id, Using = "chkShowOlineClassifiedOverview")]
        public IWebElement ShowOnlineClassifiedOverview_CheckBox;

        [FindsBy(How = How.Id, Using = "chkShowTimeToMarket")]
        public IWebElement ShowTimeToMarket_CheckBox;

        [FindsBy(How = How.PartialLinkText, Using = "Back to MAX Ad")]
        public IWebElement BackToMaxAdLink;

        [FindsBy(How = How.Id, Using = "ctl00$ContentPlaceHolder1$Tabset$Tab_Button_4")]
        public IWebElement MiscellaneousTab;

        [FindsBy(How = How.Id, Using = "DealerSegmentList_0")]
        public IWebElement Domestic_DigitalPerformanceAnalytics_RadioButton;

        [FindsBy(How = How.Id, Using = "DealerSegmentList_1")]
        public IWebElement Import_DigitalPerformanceAnalytics_RadioButton;

        [FindsBy(How = How.Id, Using = "DealerSegmentList_2")]
        public IWebElement Highline_DigitalPerformanceAnalytics_RadioButton;

        [FindsBy(How = How.CssSelector, Using = "div.indent span#customValidatorBatchAutoload div.error_desc p")]
        public IWebElement Batch_Autoload_Error_Message;

        [FindsBy(How = How.CssSelector, Using = "div.indent span#showTimeToMarketValidator div.error_desc p")]
        public IWebElement Show_TTM_Error_Message;

        //drop down lists; must be instantiated by the constructor
        [FindsBy(How = How.CssSelector, Using = "div#Tabset div.fieldLabelPair select#GIDProviderUsed")] 
        protected IWebElement gid_ProviderUsed_ddl_bud;
        public SelectElement GID_ProviderUsed_DDL; 

        [FindsBy(How = How.CssSelector, Using = "div#Tabset div.fieldLabelPair select#GIDProviderNew")]
        protected IWebElement gid_ProviderNew_ddl_bud;
        public SelectElement GID_ProviderNew_DDL;

        [FindsBy(How = How.CssSelector, Using = "div#Tabset div.fieldLabelPair select#ReportDataSourceUsed")]
        protected IWebElement report_DataSourceUsed_ddl_bud;
        public SelectElement Report_DataSource_Used_DDL;

        [FindsBy(How = How.CssSelector, Using = "div#Tabset div.fieldLabelPair select#ReportDataSourceNew")]
        protected IWebElement report_DataSourceNew_ddl_bud;
        public SelectElement Report_DataSource_New_DDL;



        public Admin_Miscellaneous_Page(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Admin | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Admin | MAX : Online Inventory. Perfected.")
            {
                throw new Exception("This is not the Admin Page");
            }

            PageFactory.InitElements(driver, this);
            //construct page Drop Down Lists
            GID_ProviderUsed_DDL = new SelectElement(gid_ProviderUsed_ddl_bud);
            GID_ProviderNew_DDL = new SelectElement(gid_ProviderNew_ddl_bud);
            Report_DataSource_Used_DDL = new SelectElement(report_DataSourceUsed_ddl_bud);
            Report_DataSource_New_DDL = new SelectElement(report_DataSourceNew_ddl_bud);

        }

        public Boolean Is_Max30Upgrade_Checked()
        {
            if (Max30Upgrade_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public Boolean Is_MaxForOEM_Checked()
        {
            if (MaxForOEM_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public Boolean Is_Show_Online_Classified_Overview_Checked()
        {
            if (ShowOnlineClassifiedOverview_CheckBox.Selected)
            {
                return true;
            }
            return false;
        }

        public Boolean Is_Batch_Autoload_Checked()
        {
            if (Batch_Autoload_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public Boolean Is_Online_Performance_Analytics_Checked()
        {
            if (Digital_Performance_Analytics_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public Boolean Is_Enable_Batch_NewCar_Pricing_Checked()
        {
            if (Enable_Batch_NewCar_Pricing_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public Boolean Is_Show_TimeToMarket_Checked()
        {
            if (ShowTimeToMarket_CheckBox.Selected)
            {
                return true;
            }
            return false;
        }


        public void Check_Batch_Autoload_Checked()
        {
            if (!Is_Batch_Autoload_Checked())
            {
                Batch_Autoload_Checkbox.Click();
                Save_Button.Click();
            }

        }

        public void UnCheck_Batch_Autoload()
        {
            if (Is_Batch_Autoload_Checked())
            {
                Batch_Autoload_Checkbox.Click();
                Save_Button.Click();
                WaitForPost();
            }
        }

        public void Check_Max30Upgrade()
        {
            if (!Is_Max30Upgrade_Checked())
                Max30Upgrade_Checkbox.Click();            
        }

        public void Check_MaxForOEM()
        {
            if(!Is_MaxForOEM_Checked())
                MaxForOEM_Checkbox.Click();
        }

        public void Check_Online_Performance_Analytics()
        {
            if (!Is_Online_Performance_Analytics_Checked())
                Digital_Performance_Analytics_Checkbox.Click();
        }

        public void UnCheck_Online_Performance_Analytics()
        {
            if (Is_Online_Performance_Analytics_Checked())
                Digital_Performance_Analytics_Checkbox.Click();
        }

        public void Check_Digital_Performance_Analytics()
        {
            if (!Digital_Performance_Analytics_Checkbox.Selected)
            {
                Digital_Performance_Analytics_Checkbox.Click();
                Domestic_DigitalPerformanceAnalytics_RadioButton.Click();
                Save_Button.Click();
            }              
        }

        //TODO Add TTM checkbox error checker
        public void Check_Show_Online_Classified_Overview() //This method does not make sure GID Provider or Report Data Source is set.
        {
            if (!Is_Show_Online_Classified_Overview_Checked())
            {
                ShowOnlineClassifiedOverview_CheckBox.Click();
                // TODO put GID & Report Data Source checker here.
                Save_Button.Click();
                try
                {
                    if (Batch_Autoload_Error_Message.Displayed) //bad creds
                    {
                        Batch_Autoload_Checkbox.Click(); //uncheck Batch Autoload
                        ShowOnlineClassifiedOverview_CheckBox.Click();
                        Save_Button.Click();
                    }
                }
                catch (NoSuchElementException)
                {
                    return;
                }
                Thread.Sleep(4000);
            }
            
        }

        //TODO Add TTM checkbox error checker
        public void Check_Enable_Batch_New_Car_Pricing() //Enable New Car Pricing gets checked automatically as part of this
        {
            if (!Enable_Batch_NewCar_Pricing_Checkbox.Selected)
            {
                Enable_Batch_NewCar_Pricing_Checkbox.Click();
                Save_Button.Click();
                try
                {
                    if (Batch_Autoload_Error_Message.Displayed) //bad creds
                    {
                        Batch_Autoload_Checkbox.Click(); //uncheck Batch Autoload
                        Enable_Batch_NewCar_Pricing_Checkbox.Click();
                        Save_Button.Click();
                        WaitForPost(15);
                    }
                }
                catch (Exception e)
                {
                    if (e is TimeoutException || e is NoSuchElementException)
                    {
                        return;
                    }
                    throw;
                }
            }

        }

        public void Uncheck_Show_Online_Classified_Overview()
        {
            if (Is_Show_Online_Classified_Overview_Checked())
            {
                ShowOnlineClassifiedOverview_CheckBox.Click();
                Save_Button.Click();
            }

        }

        public void Check_Batch_NewCar_Pricing()
        {
            if (!Is_Enable_Batch_NewCar_Pricing_Checked())
            {
                Enable_Batch_NewCar_Pricing_Checkbox.Click();
                Save_Button.Click();
            }

        }

        public void UnCheck_Batch_NewCar_Pricing()
        {
            if (Is_Enable_Batch_NewCar_Pricing_Checked())
            {
                Enable_Batch_NewCar_Pricing_Checkbox.Click();
                Save_Button.Click();
            }
        }

        public void UnCheck_Show_TimeToMarket()
        {
            if (Is_Show_TimeToMarket_Checked())
            {
                ShowTimeToMarket_CheckBox.Click();
                var original = GetLastModifiedDate();
                Save_Button.Click();
                WaitForLastModifiedDateUpdate(original);
                //WaitForPost();
            }
        }

        public void Select_OEM_Name(OEM_Type option)
        {
            var ddl = new SelectElement(MaxForOEM_DropDown);

            if (ddl.SelectedOption.GetAttribute("value") != option.ToString())
            {
                ddl.SelectByText(option.ToString());
                Thread.Sleep(1000); // make sure the DDL sets on the page before moving on
            }
        }

        public void Select_Save_OEM(OEM_Type option)
        {
            Check_Max30Upgrade();
            Check_MaxForOEM();
            Select_OEM_Name(option);
            UnCheck_Online_Performance_Analytics();
            //default checked TTM checkbox prevents save if no GID selected
            UnCheck_Show_TimeToMarket();
            var original = GetLastModifiedDate(); //
            Save_Button.Click();
            //WaitForPost(20.0M);
            WaitForLastModifiedDateUpdate(original);
        }

        public void setUsedGIDProviderByIndexInDDL(int provider)  // Index: 0 = Select one, 1 = None, 2 = Aultec, 3 = eBiz
        {
            new SelectElement(driver.FindElement(By.Id("GIDProviderUsed"))).SelectByIndex(provider);
        }

        public void setNewGIDProviderByIndexInDDL(int provider)  // Index: 0 = Select one, 1 = None, 2 = Aultec, 3 = eBiz
        {
            new SelectElement(driver.FindElement(By.Id("GIDProviderNew"))).SelectByIndex(provider);
        }

        public void DisableOnlineClassifiedOverview()
        {
            if (!ShowOnlineClassifiedOverview_CheckBox.Selected) return;
            Save_Button.Click(); //will trigger error messages if any
            WaitForAjax();
            try
            {
                if (Batch_Autoload_Error_Message.Displayed)
                    Batch_Autoload_Checkbox.Click(); //uncheck
                else if(Show_TTM_Error_Message.Displayed)
                    ShowTimeToMarket_CheckBox.Click(); //uncheck
            }
            catch (NoSuchElementException)
            {
            }           
            ShowOnlineClassifiedOverview_CheckBox.Click(); //uncheck
            Save_Button.Click();
            WaitForAjax(20.0M);
        }

        public void DisableDigitalPerformanceAnalytics()
        {
            if (!Digital_Performance_Analytics_Checkbox.Selected) return;
            Save_Button.Click(); //will trigger error messages if any
            WaitForAjax();
            try
            {
                if (Batch_Autoload_Error_Message.Displayed)
                    Batch_Autoload_Checkbox.Click(); //uncheck
                else if (Show_TTM_Error_Message.Displayed)
                    ShowTimeToMarket_CheckBox.Click(); //uncheck
            }
            catch (NoSuchElementException)
            {
            }
            Digital_Performance_Analytics_Checkbox.Click(); //uncheck
            Save_Button.Click();
            WaitForAjax(20.0M);
        }

        public void Enable_TTM_DPA_OCO()
        {
            Save_Button.Click(); //will trigger error messages if any
            WaitForAjax();
            try
            {
                if (Batch_Autoload_Error_Message.Displayed)
                {
                    Batch_Autoload_Checkbox.Click(); //uncheck
                    Check_TTM_DPA_OCO();
                }
                else
                {
                    Check_TTM_DPA_OCO();
                }
            }
            catch (NoSuchElementException)
            {           
                Check_TTM_DPA_OCO();
            }
            Save_Button.Click();
            WaitForAjax(20.0M);
        }

        //This is a simple selector and does not look for error messages. That is handled by Enable_TTM_DPA_OCO()
        public void Check_TTM_DPA_OCO()
        {
            if (!ShowTimeToMarket_CheckBox.Selected)
            {
                ShowTimeToMarket_CheckBox.Click();
                WaitForAjax(5.0M);
                Report_DataSource_Used_DDL.SelectByText("Aultec");
                Report_DataSource_New_DDL.SelectByText("Aultec");
                //TODO DMS fields? May not be necessary.
            }
            if (!Digital_Performance_Analytics_Checkbox.Selected)
            {
                Digital_Performance_Analytics_Checkbox.Click();
                WaitForAjax(5.0M);
                Domestic_DigitalPerformanceAnalytics_RadioButton.Click();
            }
            if (!ShowOnlineClassifiedOverview_CheckBox.Selected)
            {
                ShowOnlineClassifiedOverview_CheckBox.Click();
                WaitForAjax(5.0M);
                GID_ProviderUsed_DDL.SelectByText("Aultec");
                GID_ProviderNew_DDL.SelectByText("Aultec");
            }
        }

    }
}
