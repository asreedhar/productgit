﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Admin_Autoload_Settings_Page : Admin_Home_Page
    {

        [FindsBy(How = How.Name, Using = "ctl00$ContentPlaceHolder1$manufacturerGridView$ctl06$ManufacturerAutoLoadCheckBox")] 
        public IWebElement BMW_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$ContentPlaceHolder1$manufacturerGridView$ctl07$ManufacturerAutoLoadCheckBox")]
        public IWebElement GeneralMotors_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$ContentPlaceHolder1$manufacturerGridView$ctl08$ManufacturerAutoLoadCheckBox")]
        public IWebElement Chrysler_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$ContentPlaceHolder1$manufacturerGridView$ctl09$ManufacturerAutoLoadCheckBox")]
        public IWebElement Ford_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$ContentPlaceHolder1$manufacturerGridView$ctl17$ManufacturerAutoLoadCheckBox")]
        public IWebElement Mazda_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$ContentPlaceHolder1$manufacturerGridView$ctl24$ManufacturerAutoLoadCheckBox")]
        public IWebElement Volkswagen_Checkbox;

        [FindsBy(How = How.Id, Using = "SaveAutoLoadSettingsButton")] public IWebElement Save_Auto_Load_Settings;

        public Admin_Autoload_Settings_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Admin | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Admin | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Admin Page");
            }

            PageFactory.InitElements(driver, this);
        }


        public Boolean Is_BMW_Checkbox_Checked()
        {
            if (BMW_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_BMW_Checkbox()
        {
            if (!Is_BMW_Checkbox_Checked())
                BMW_Checkbox.Click();
        }


        public Boolean Is_GeneralMotors_Checkbox_Checked()
        {
            if (GeneralMotors_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_GeneralMotors_Checkbox()
        {
            if (!Is_GeneralMotors_Checkbox_Checked())
                GeneralMotors_Checkbox.Click();
        }

        public Boolean Is_Chrysler_Checkbox_Checked()
        {
            if (Chrysler_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Chrysler_Checkbox()
        {
            if (!Is_Chrysler_Checkbox_Checked())
                Chrysler_Checkbox.Click();
        }

        public Boolean Is_Ford_Checkbox_Checked()
        {
            if (Ford_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Ford_Checkbox()
        {
            if (!Is_Ford_Checkbox_Checked())
                Ford_Checkbox.Click();
        }

        public Boolean Is_Mazda_Checkbox_Checked()
        {
            if (Mazda_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Mazda_Checkbox()
        {
            if (!Is_Mazda_Checkbox_Checked())
                Mazda_Checkbox.Click();
        }

        public Boolean Is_Volkswagen_Checkbox_Checked()
        {
            if (Volkswagen_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Volkswagen_Checkbox()
        {
            if (!Is_Volkswagen_Checkbox_Checked())
                Volkswagen_Checkbox.Click();
        }

        public void Check_BMW_GM_Chrysler_Ford_Mazda_VW()
        {
            Check_BMW_Checkbox();
            Check_GeneralMotors_Checkbox();
            Check_Chrysler_Checkbox();
            //Check_Ford_Checkbox(); //Checkbox enables Fetch agent. We don't use Fetch for Ford Autoload
            Check_Mazda_Checkbox();
            Check_Volkswagen_Checkbox();
        }

        public Boolean Is_Checkbox_Checked(IWebElement checkbox)
        {
            if (checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_A_CheckBox(IWebElement checkbox)
        {
            if (!Is_Checkbox_Checked(checkbox))
                checkbox.Click();
        }
    }
}
