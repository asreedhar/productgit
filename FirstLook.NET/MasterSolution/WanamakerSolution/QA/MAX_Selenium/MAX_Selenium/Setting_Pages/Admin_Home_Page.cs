﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Admin_Home_Page : Page
    {
        [FindsBy(How = How.Id, Using = "ctl00$ContentPlaceHolder1$Tabset$Tab_Button_3")] 
        public IWebElement Autoload_Settings_Button;

        [FindsBy(How = How.Id, Using = "ctl00$ContentPlaceHolder1$Tabset$Tab_Button_4")] 
        public IWebElement Miscellaneous_Settings_Tab;

        [FindsBy(How = How.PartialLinkText, Using = "Back to MAX Ad")]
        public IWebElement Back_To_MaxAD_Button;

        [FindsBy(How = How.CssSelector, Using = "div.adminContent li.ui-tab input[value='Upgrades']")] 
        public IWebElement Upgrades_Tab;

        [FindsBy(How = How.Id, Using = "chkMaxForWebsite20")]
        public IWebElement Max_For_Website20_Checkbox_on_Upgrades_Tab;

        [FindsBy(How = How.Id, Using = "SaveButton")] 
        public IWebElement Save_Button_Upgrades_Tab;

        //constructor
        public Admin_Home_Page(IWebDriver driver) //cannot inherit page elements from: base(driver)
        {
            this.driver = driver;
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Admin | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Admin | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Admin Page");
            }
            
            // This line exposes the hidden AutoLoad Tab (not AutoLoad button)
            Make_AutoLoad_Tab_Visible();
            PageFactory.InitElements(driver, this);
        }

        public Admin_Miscellaneous_Page Get_Admin_Miscellaneous_Page()
        {
            Miscellaneous_Settings_Tab.Click();

            IWebElement temp = ExplicitWaitById("Max30Upgrade", 20);
            if (temp == null)
                Assert.Fail("Max30Upgrade button couldn't found. ");
 
            return new Admin_Miscellaneous_Page(driver);
        }

        public Admin_Autoload_Settings_Page Get_Admin_Autoload_Setting_Page()
        {
            Autoload_Settings_Button.Click();

            IWebElement temp = ExplicitWaitById("ManufacturerAutoLoadCheckBox", 20);
            if (temp == null)
                Assert.Fail("Manufacturer AutoLoad Check Box couldn't be found.");
 
            return new Admin_Autoload_Settings_Page(driver);
        }

        //renamed to differentiate from Page.cs method of the same name
        public InventoryPage GoBackTo_Inventory_Page()
        {
            Back_To_MaxAD_Button.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TitleContains("Current Inventory Summary | MAX : Online Inventory. Perfected.")); 

            return new InventoryPage(driver);
        }


        public void Make_AutoLoad_Tab_Visible()
        {
            const string script = @"
                  $('.explicitly_hidden').show(); 
            ";
            var js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(script);
        }

    }
}
