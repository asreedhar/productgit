﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Admin_Upgrades_Page : Admin_Home_Page
    {

        //element finders
        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content div#settings2.fieldLabelPair input#chkMaxForWebsite20")]
        public IWebElement Max_ForWebsite_20_CheckBox;

        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content input#chkMaxDigitalShowroom")]
        public IWebElement Max_DigitalShowrm_CheckBox;

        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content input#chkEnableShowroomReports")]
        public IWebElement DigitalShowrm_WS20_Reports_CheckBox;

        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content input#SaveButton")]
        public IWebElement Upgrades_Save_Button;

        [FindsBy(How = How.Id, Using = "chkMaxForSmartphone")]
        public IWebElement MaxForSmartPhone_Checkbox;

        [FindsBy(How = How.Id, Using = "SaveButton")]
        public IWebElement Save_Button;

        //drop down lists
        [FindsBy(How = How.Id, Using = "ddlTemplate")]
        public IWebElement autoBatchTemplate_DDL_bud;
        public SelectElement AutoBatchTemplate_DDL;




        //constructor
        public Admin_Upgrades_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Admin | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Admin | MAX : Online Inventory. Perfected.")
            {
                throw new Exception("This is not the Admin Upgrades Page");
            }

            PageFactory.InitElements(driver, this);
            AutoBatchTemplate_DDL = new SelectElement(autoBatchTemplate_DDL_bud);

        }


        //This is a simple selector and does not look for error messages.
        public void Check_MfW_MDS_DSWR()
        {
            if (!Max_ForWebsite_20_CheckBox.Selected)
            {
                Max_ForWebsite_20_CheckBox.Click();
                WaitForAjax(5.0M);
            }
            if (!Max_DigitalShowrm_CheckBox.Selected)
            {
                Max_DigitalShowrm_CheckBox.Click();
                WaitForAjax(5.0M);
            }
            if (!DigitalShowrm_WS20_Reports_CheckBox.Selected)
            {
                DigitalShowrm_WS20_Reports_CheckBox.Click();
                WaitForAjax(5.0M);
            }
            Upgrades_Save_Button.Click();
            WaitForAjax();
        }

    }

}
