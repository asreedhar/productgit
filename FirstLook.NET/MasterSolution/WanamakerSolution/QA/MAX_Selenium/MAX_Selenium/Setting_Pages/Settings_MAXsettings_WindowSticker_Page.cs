﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_MAXsettings_WindowSticker_Page : Settings_MAXsettings_Page
    {


        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_12")]
        public IWebElement Window_Sticker_Tab;



        public Settings_MAXsettings_WindowSticker_Page(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Dealer Preferences | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the MAX Setting Window Sticker Page");
            }

            PageFactory.InitElements(driver, this);
        }
     

    }
}

