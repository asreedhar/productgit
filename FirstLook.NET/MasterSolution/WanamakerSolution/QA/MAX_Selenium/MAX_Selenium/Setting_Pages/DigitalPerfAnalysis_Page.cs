﻿using System;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class DigitalPerfAnalysis_Page : Page
    {
        
        //element locators
        //...
        
        
        public DigitalPerfAnalysis_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Digital Performance Analysis | MAX : Intelligent Online Advertising Systems"));

            if (driver.Title != "Digital Performance Analysis | MAX : Intelligent Online Advertising Systems")
            {
                throw new Exception("This is not the Settings/Digital Performance Analysis page.");
            }

            PageFactory.InitElements(driver, this);
        }



    }
}
