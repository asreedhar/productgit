﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_AdvancedSettings_DataSourceSettings_Page : Settings_AdvancedSettings_Page
    {
        [FindsBy(How = How.CssSelector, Using = "#DataSourceDetailsView > tbody > tr:nth-child(1) > td:nth-child(2)")] 
        public IWebElement Current_Photo_Count_Element;

        [FindsBy(How = How.LinkText, Using = "Edit")]
        public IWebElement Edit_Button;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$DataSourceDetailsView$ctl01")]
        public IWebElement Photo_Count_Field;

        [FindsBy(How = How.LinkText, Using = "Update")]
        public IWebElement Update_Button;

        [FindsBy(How = How.LinkText, Using = "Cancel")]
        public IWebElement Cancel_Button;

        public Settings_AdvancedSettings_DataSourceSettings_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Advanced Dealer Preferences | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Advanced Dealer Preferences | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Advanced Settings - Data Source Settings page.");
            }

            PageFactory.InitElements(driver, this);
        }

        public int Get_Current_Photo_Count_Setting()
        {
            return Utility.ExtractDigitsFromString(Current_Photo_Count_Element.Text);
        }

        public void Set_Photo_Count_Settings(string num)
        {
            Edit_Button.Click();
            Photo_Count_Field.Clear();
            Photo_Count_Field.SendKeys(num);
            Update_Button.Click();
        }
    }
}
