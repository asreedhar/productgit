﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_MAXsettings_Reports_Page : Settings_MAXsettings_Page
    {

        [FindsBy(How = How.Id, Using = "MerchandisingGraphDefault_0")]
        public IWebElement No_Photo_Radio_Button;

        [FindsBy(How = How.Id, Using = "MerchandisingGraphDefault_1")]
        public IWebElement No_Price_Radio_Button;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$ReportSettings$SaveButton")]
        public IWebElement Save_Button;

        public Settings_MAXsettings_Reports_Page(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Dealer Preferences | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the MAX Setting Reports Page");
            }

            PageFactory.InitElements(driver, this);
        }

        public Boolean Is_No_Photo_Radio_Button_Selected()
        {
            if (No_Photo_Radio_Button.Selected)
            {
                return true;
            }
            return false;
        }

        public void Select_No_Photo_Radio_Button()
        {
            if (!Is_No_Photo_Radio_Button_Selected())
            {
                No_Photo_Radio_Button.Click();
                Save_Button.Click();
                Thread.Sleep(4000);
            }
        }

        public Boolean Is_No_Price_Radio_Button_Selected()
        {
            if (No_Price_Radio_Button.Selected)
            {
                return true;
            }
            return false;
        }

        public void Select_No_Price_Radio_Button()
        {
            if (!Is_No_Price_Radio_Button_Selected())
            {
                No_Price_Radio_Button.Click();
                Save_Button.Click();
                Thread.Sleep(4000);
            }

        }




    }
}
