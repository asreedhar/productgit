﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_MAXsettings_AutoLoad_Page : Settings_MAXsettings_Page
    {
        //finders
        [FindsBy(How = How.Id, Using = "AutomaterMakesDDL")]
        public IWebElement SupportedMakes_DDL;

        [FindsBy(How = How.Id, Using = "EditAutomaterMakeCredentials")]
        public IWebElement SupportedMakes_Edit_Button;

        [FindsBy(How = How.Id, Using = "CredentialLockedOutPanel")]
        public IWebElement AutoLoadCredential_Warning_Panel;

        [FindsBy(How = How.Id, Using = "usernameTB")]
        public IWebElement Username_Field;

        [FindsBy(How = How.Id, Using = "passwordTB")]
        public IWebElement Password_Field;

        [FindsBy(How = How.Id, Using = "ClearAutomaterCredentials")]
        public IWebElement Clear_Button;

        [FindsBy(How = How.Id, Using = "CreateAutomaterCredentials")]
        public IWebElement Save_Button;


        public Settings_MAXsettings_AutoLoad_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            if (!(driver.FindElement(By.CssSelector("div.ui-tab-content > h2")).Text.Equals("Automate Loading Of Vehicle Equipment")))
            {
                throw new StaleElementReferenceException("This is not the MAX Settings --> AutoLoad Tab page");
            }

            PageFactory.InitElements(driver, this);
        }

        public void Set_Supported_Make(string make)
        {
            var ddl = new SelectElement(SupportedMakes_DDL);
            ddl.SelectByText(make);
        }

    }
}