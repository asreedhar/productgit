﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_MAXsettings_AutoApprove_Page : Settings_MAXsettings_Page
    {
        //finders
        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content input#btn_PreApproveSettings.button")]
        public IWebElement Save_Button;

        [FindsBy(How = How.CssSelector, Using = "div.formButtons input[value*='I ACCEPT']")]
        public IWebElement Accept_Button;

        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content input#preapprove_FirstName")]
        public IWebElement FirstName_Field;

        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content input#preapprove_LastName")]
        public IWebElement LastName_Field;

        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content input#preapprove_Email")]
        public IWebElement Email_Field;

        [FindsBy(How = How.CssSelector, Using = "#preferencesTabs input[value=Auto-Approve]")]
        public IWebElement AutoApprove_Tab;

        //Drop Down Lists
        [FindsBy(How = How.CssSelector, Using = "div.ui-tab-content select#preapprove_Status")]
        protected IWebElement autoApprove_DDL_bud;
        public SelectElement AutoApprove_DDL;

    
        public Settings_MAXsettings_AutoApprove_Page(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            if (!(driver.FindElement(By.CssSelector("div.ui-tab-content > h2")).Text.Equals("Auto-Approve")))
            {
                throw new StaleElementReferenceException("This is not the MAX Settings --> AutoApprove Tab page");
            }

            PageFactory.InitElements(driver, this);
            AutoApprove_DDL = new SelectElement(autoApprove_DDL_bud);
        }
    
        //methods
        public void Disable_AutoApprove ()
        {

            if (AutoApprove_DDL.SelectedOption.Text == "Off") return;
            AutoApprove_DDL.SelectByText("Off");
            Save_Button.Click();
            WaitForAjax();

        }

        public void Enable_AutoApprove()
        {
            if (AutoApprove_DDL.SelectedOption.Text == "On (New & Used)") return;

            // check for the "I ACCEPT" version of the button
            if (I_ACCEPT_Button_Tester())
            {

                FirstName_Field.Clear();
                FirstName_Field.SendKeys("Max");
                LastName_Field.Clear();
                LastName_Field.SendKeys("QA");
                Email_Field.Clear();
                Email_Field.SendKeys("maxqa@phonyemail.com");

            }
            AutoApprove_DDL.SelectByText("On (New & Used)");
            Save_Button.Click();
            WaitForAjax();

        }

        //supporting methods
        public bool I_ACCEPT_Button_Tester()
        {
            //if button is not found, Webdriver will throw exception
            try
            {
                return Accept_Button.Enabled;
            }
            catch (Exception)
            {          
                return false;
            }
        }


    }
}
