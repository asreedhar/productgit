﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_MAXsettings_Alerts_Page : Settings_MAXsettings_Page
    {

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl00$chkActive")]
        public IWebElement Not_Online_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl01$chkActive")]
        public IWebElement No_Price_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl02$chkActive")]
        public IWebElement No_Photos_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl03$chkActive")] 
        public IWebElement No_Description_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl04$chkActive")]
        public IWebElement No_Trim_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl05$chkActive")]
        public IWebElement Need_RePricing_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl06$chkActive")]
        public IWebElement Need_Equipment_Review_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl07$chkActive")]
        public IWebElement Low_Online_Activity_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl08$chkActive")]
        public IWebElement Low_Photos_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl09$chkActive")]
        public IWebElement No_Packages_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl10$chkActive")]
        public IWebElement No_BooksValue_Checkbox;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$rptBuckets$ctl11$chkActive")]
        public IWebElement No_Carfax_Checkbox;

        [FindsBy(How = How.Id, Using = "NewUsedDropDown")] 
        public IWebElement NewAndUsed_DDL;

        [FindsBy(How = How.Name, Using = "ctl00$BodyPlaceHolder$BucketAlertsSettings$SaveButton")] 
        public IWebElement Save_Button;

        public Settings_MAXsettings_Alerts_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Dealer Preferences | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the MAX Setting Alerts Page");
            }

            PageFactory.InitElements(driver, this);
        }


        public Boolean Is_Not_Online_Checkbox_Checked()
        {
            if (Not_Online_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }
        
        public void Check_Not_Online_Checkbox()
        {
            if (!Is_Not_Online_Checkbox_Checked())
                Not_Online_Checkbox.Click();
        }
        
        public Boolean Is_No_Price_Checkbox_Checked()
        {
            if (No_Price_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_No_Price_Checkbox()
        {
            if(!Is_No_Price_Checkbox_Checked())
                No_Price_Checkbox.Click();
        }
             
        public Boolean Is_No_Photos_Checkbox_Checked()
        {
            if (No_Photos_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_No_Photos_Checkbox()
        {
            if (!Is_No_Photos_Checkbox_Checked())
                No_Photos_Checkbox.Click();
        }

        public Boolean Is_No_Description_Checkbox_Checked()
        {
            if (No_Description_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_No_Description_Checkbox()
        {
            if (!Is_No_Description_Checkbox_Checked())
                No_Description_Checkbox.Click();
        }

        public Boolean Is_No_Trim_Checkbox_Checked()
        {
            if (No_Trim_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_No_Trim_Checkbox()
        {
            if (!Is_No_Trim_Checkbox_Checked())
                No_Trim_Checkbox.Click();
        }

        public Boolean Is_Need_RePricing_Checkbox_Checked()
        {
            if (Need_RePricing_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Need_RePricing_Checkbox()
        {
            if (!Is_Need_RePricing_Checkbox_Checked())
                Need_RePricing_Checkbox.Click();
        }

        public Boolean Is_Need_Equipment_Review_Checkbox_Checked()
        {
            if (Need_Equipment_Review_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Need_Equipment_Review_Checkbox()
        {
            if (!Is_Need_Equipment_Review_Checkbox_Checked())
                Need_Equipment_Review_Checkbox.Click();
        }

        public Boolean Is_Low_Online_Activity_Checkbox_Checked()
        {
            if (Low_Online_Activity_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Low_Online_Activity_Checkbox()
        {
            if (Low_Online_Activity_Checkbox.Selected) return;
                Low_Online_Activity_Checkbox.Click();
                Save_Button.Click();

        }

        public Boolean Is_Low_Photos_Checkbox_Checked()
        {
            if (Low_Photos_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_Low_Photos_Checkbox()
        {
            if (!Is_Low_Photos_Checkbox_Checked())
                Low_Photos_Checkbox.Click();
        }

        public Boolean Is_No_Packages_Checkbox_Checked()
        {
            if (No_Packages_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_No_Packages_Checkbox()
        {
            if (!Is_No_Packages_Checkbox_Checked())
                No_Packages_Checkbox.Click();
        }

        public Boolean Is_No_BooksValue_Checkbox_Checked()
        {
            if (No_BooksValue_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_No_BooksValue_Checkbox()
        {
            if (!Is_No_BooksValue_Checkbox_Checked())
                No_BooksValue_Checkbox.Click();
        }

        public Boolean Is_No_Carfax_Checkbox_Checked()
        {
            if (No_Carfax_Checkbox.Selected)
            {
                return true;
            }
            return false;
        }

        public void Check_No_Carfax_Checkbox()
        {
            if (!Is_No_Carfax_Checkbox_Checked())
                No_Carfax_Checkbox.Click();
        }

        public void Check_All_CheckBox_Alerts_SettingPage()
        {
            Check_Not_Online_Checkbox();
            Check_No_Price_Checkbox();
            Check_No_Photos_Checkbox();
            Check_No_Description_Checkbox();
            Check_No_Trim_Checkbox();
            Check_Need_RePricing_Checkbox();
            Check_Need_Equipment_Review_Checkbox();
            Check_Low_Online_Activity_Checkbox();
            Check_Low_Photos_Checkbox();
            Check_No_Packages_Checkbox();
            Check_No_BooksValue_Checkbox();
            Check_No_Carfax_Checkbox();
            Save_Button.Click();
            Thread.Sleep(20);
        }
    }
}