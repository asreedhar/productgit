﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Setup_Wizard_Page : Page
    {
        [FindsBy(How = How.LinkText, Using = "1. Data In/Out Setup")]
        public IWebElement Data_In_Out_Setup_Button;

        [FindsBy(How = How.LinkText, Using = "2. Condition Descriptors")]
        public IWebElement Condition_Descriptors_Button;

        [FindsBy(How = How.Id, Using = "DestDDL")]
        public IWebElement Destination_DDL;

        [FindsBy(How = How.Id, Using = "AddSourceCredentialDisclaimer")]
        public IWebElement Source_Credential_Disclaimer;

        [FindsBy(How = How.LinkText, Using = "Home")]
        public IWebElement Home_Top_Tab; // The behaver of this button has been changed in 15.0, it goes to Dashboard now instead of Inventory Page

        [FindsBy(How = How.LinkText, Using = "Reports")]
        public IWebElement Reports_Tap_Tab;

        [FindsBy(How = How.LinkText, Using = "Executive Dashboard")]
        public IWebElement ExecutiveDashboardLink;

        [FindsBy(How = How.LinkText, Using = "FirstLook")]
        public IWebElement FirstLook_Top_Tab;

        [FindsBy(How = How.Id, Using = "startCollections")]
        public IWebElement StartCollections;

        public Setup_Wizard_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Setup Wizard | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Dealer Setup Wizard | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Setup Wizard page.");
            }

            PageFactory.InitElements(driver, this);
        }

        public void Select_Source_Name(string destination)  
        {
            var ddl = new SelectElement(Destination_DDL);
            if (ddl.SelectedOption.GetAttribute("value") != destination)
            {
                ddl.SelectByText(destination);
            }
        }


        public InventoryPage Get_Inventory_Page()
        {
            Home_Top_Tab.Click();
            return new InventoryPage(driver);
        }

        public HomePage Get_FirstLook_Home_Page()
        {
            FirstLook_Top_Tab.Click();
            return new HomePage(driver);
        }

    }
}
