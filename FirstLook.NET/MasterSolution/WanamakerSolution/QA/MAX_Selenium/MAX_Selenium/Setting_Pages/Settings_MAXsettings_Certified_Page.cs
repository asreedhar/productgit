﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_MAXsettings_Certified_Page : Settings_MAXsettings_Page
    {

        [FindsBy(How = How.Id, Using = "CertificationDDL")]
        public IWebElement Configure_Maker_DDL;

        [FindsBy(How = How.Id, Using = "ManufacturerProgramsLink")]
        public IWebElement Manufacturer_Link;

        [FindsBy(How = How.Id, Using = "DealerProgramsLink")]
        public IWebElement Dealer_Link;

        public Settings_MAXsettings_Certified_Page(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected."));

            if (!(driver.FindElement(By.CssSelector("div.ui-tab-content > h2")).Text.Equals("Certified Pre-Owned Vehicle Settings")))
            {
                throw new StaleElementReferenceException("This is not the MAX Settings --> Certified Tab page");
            }

            PageFactory.InitElements(driver, this);
        }

    }
}