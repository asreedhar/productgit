﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;

namespace MAX_Selenium
{
    public class Settings_MAXsettings_Page : Page
    {

        [FindsBy(How = How.CssSelector, Using = "#preferencesTabs input[value=Reports]")]
        public IWebElement Reports_Tab;

        [FindsBy(How = How.LinkText, Using = "Home")]
        public IWebElement Home_Top_Tab;  // The behaver of this button has been changed in 15.0, it goes to Dashboard now instead of Inventory Page

        [FindsBy(How = How.LinkText, Using = "Reports")]
        public IWebElement Reports_Tap_Tab;

        [FindsBy(How = How.Id, Using = "lnkDashboard")]
        public IWebElement ExecutiveDashboardLink;

        [FindsBy(How = How.LinkText, Using = "FirstLook")]
        public IWebElement FirstLook_Top_Tab;

        [FindsBy(How = How.CssSelector, Using = "#preferencesTabs input[value=AutoLoad]")]
        public IWebElement AutoLoad_Tab;

        [FindsBy(How = How.CssSelector, Using = "#preferencesTabs input[value=Alerts]")]
        public IWebElement Alerts_Tab;

        [FindsBy(How = How.CssSelector, Using = "#preferencesTabs input[value=Certified]")]
        public IWebElement Certified_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_0")]
        public IWebElement Tagline_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_1")]
        public IWebElement Specials_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_2")]
        public IWebElement VehicleProfiles_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_3")]
        public IWebElement EquipmentPriority_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_5")]
        public IWebElement Campaigns_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_6")]
        public IWebElement Disclaimer_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_11")]
        public IWebElement Analytics_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$preferencesTabs$Tab_Button_12")]
        public IWebElement WindowSticker_Tab;

        public Settings_MAXsettings_Page(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Dealer Preferences | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Dealer Preferences | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Max Settings page.");
            }

            PageFactory.InitElements(driver, this);
        }

     
        public Settings_MAXsettings_Alerts_Page Get_Alerts_Page()
        {
            Alerts_Tab.Click();
            return new Settings_MAXsettings_Alerts_Page(driver);
        }

        public Settings_MAXsettings_AutoLoad_Page Get_AutoLoad_Page()
        {
            AutoLoad_Tab.Click();
            return new Settings_MAXsettings_AutoLoad_Page(driver);
        }

        public  Settings_MAXsettings_Certified_Page Get_Certified_Page()
        {
            Certified_Tab.Click();
            return new Settings_MAXsettings_Certified_Page(driver);
        }

        public Settings_MAXsettings_Reports_Page Get_Reports_Page()
        {
            Reports_Tab.Click();
            return new Settings_MAXsettings_Reports_Page(driver);
        }

        public InventoryPage Get_Inventory_Page()
        {
            Inventory_Link.Click();
            CloseNaggingWindow();
            return new InventoryPage(driver);
        }

        public HomePage Get_FirstLook_Home_Page()
        {
            FirstLook_Top_Tab.Click();
            return new HomePage(driver);
        }
        
    }
}
