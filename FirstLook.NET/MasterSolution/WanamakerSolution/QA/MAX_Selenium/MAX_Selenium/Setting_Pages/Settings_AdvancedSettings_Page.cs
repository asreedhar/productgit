﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class Settings_AdvancedSettings_Page : Page
    {
        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$advancedTabSet$Tab_Button_3")]
        public IWebElement Data_Source_Settings_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$advancedTabSet$Tab_Button_2")]
        public IWebElement Alerting_Tab;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$advancedTabSet$Tab_Button_1")]
        public IWebElement Ad_Setings;

        [FindsBy(How = How.Id, Using = "ctl00$BodyPlaceHolder$advancedTabSet$Tab_Button_0")]
        public IWebElement Equipment_Groups_Tab;

        [FindsBy(How = How.LinkText, Using = "Home")]
        public IWebElement Home_Top_Tab;  // The behaver of this button has been changed in 15.0, it goes to Dashboard now instead of Inventory Page

        [FindsBy(How = How.LinkText, Using = "Inventory")]
        public IWebElement Inventory_Top_Tab;

        [FindsBy(How = How.LinkText, Using = "Reports")]
        public IWebElement Reports_Tap_Tab;

        [FindsBy(How = How.LinkText, Using = "Executive Dashboard")]
        public IWebElement ExecutiveDashboardLink;

        [FindsBy(How = How.LinkText, Using = "FirstLook")]
        public IWebElement FirstLook_Top_Tab;

        public Settings_AdvancedSettings_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Advanced Dealer Preferences | MAX : Online Inventory. Perfected.")); 

            if (driver.Title != "Advanced Dealer Preferences | MAX : Online Inventory. Perfected.")
            {
                throw new StaleElementReferenceException("This is not the Advanced Settings page.");
            }

            PageFactory.InitElements(driver, this);
        }


        public Settings_AdvancedSettings_DataSourceSettings_Page Get_DataSouceSettings_Page()
        {
            Data_Source_Settings_Tab.Click();
            return new Settings_AdvancedSettings_DataSourceSettings_Page(driver);
        }

        public InventoryPage Get_Inventory_Page()
        {
            Inventory_Top_Tab.Click();
            return new InventoryPage(driver);
        }

        public HomePage Get_FirstLook_Home_Page()
        {
            FirstLook_Top_Tab.Click();
            return new HomePage(driver);
        }

    }
}
