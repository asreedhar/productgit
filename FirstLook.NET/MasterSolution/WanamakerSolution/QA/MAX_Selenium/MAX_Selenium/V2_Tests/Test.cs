﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using Ionic.Zip;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium_V2
{
    [TestFixture]
    public abstract class Test
    {
        public string processName;
        public IWebDriver driver;
        public IJavaScriptExecutor js;

        public static String BaseURL = ConfigurationManager.AppSettings["MaxWebsiteBaseUrl"];
        public static String MaxStandAloneURL = ConfigurationManager.AppSettings["MaxStandAloneUrl"];
        public static String InventoryPageURL = ConfigurationManager.AppSettings["InventoryPageUrl"];
        public static String ExecutiveDashboardPageURL = ConfigurationManager.AppSettings["ExecutiveDashboardPageUrl"];
        public static String GroupDashboardPageURL = ConfigurationManager.AppSettings["GroupDashboardPageUrl"];
        public static String ScreenShotFilePath= ConfigurationManager.AppSettings["ScreenShotFilePath"];
        public static String Web_Service_Environment = ConfigurationManager.AppSettings["Web_Service_Environment"];
        public static String WhatDriver = ConfigurationManager.AppSettings["WhatDriver"];
        public static String Default_user = ConfigurationManager.AppSettings["User"];
        public static String Default_password = ConfigurationManager.AppSettings["Password"];
        public static String Single_Dealer_Default_user = ConfigurationManager.AppSettings["SingleDealerUser"];
        public static String Single_Dealer_Default_password = ConfigurationManager.AppSettings["SingleDealerPassword"];
        public static bool ScreenShotsEnabled = "true".Equals( ConfigurationManager.AppSettings["ScreenShotsEnabled"], StringComparison.InvariantCultureIgnoreCase );


        [SetUp]
        [Description("Set up each test case.")]
        public void SetUpBase()
        {
            // instantiate web driver
            if( "IE".Equals( WhatDriver, StringComparison.InvariantCultureIgnoreCase ))
            {
                CheckForIEDriverServer();
                processName = "iexplore";
                driver = new InternetExplorerDriver();
                driver.Manage().Window.Maximize();
            }
            else if( "FireFoxDriver".Equals( WhatDriver, StringComparison.InvariantCultureIgnoreCase ))
            {
                processName = "firefox";
                var profile = new FirefoxProfile { EnableNativeEvents = false };
                driver = new FirefoxDriver(profile);
                driver.Manage().Window.Maximize();
            }
            // javascript executor
            js = driver as IJavaScriptExecutor;
            
            // publish
            Page.SetDriver( driver );
        }

        public void Perform_Login( LoginPage page, string username = null, string password = null )
        {
            // skip promotions
            Cookie cookie = new Cookie( "promotion", "true" );
            driver.Manage().Cookies.AddCookie( cookie );

            page.UserName.Clear();
            page.UserName.SendKeys(username ?? Single_Dealer_Default_user);
            page.Passwd.Clear();
            page.Passwd.SendKeys(password ?? Single_Dealer_Default_password);
            page.Submit.Submit();
        }

        private void CheckForIEDriverServer()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var pathToServerDriver = Path.Combine(path, "IEDriverServer.exe");

            if(!File.Exists(pathToServerDriver))
            {
                var targetPath = Path.Combine(path, "IEServerDriver.zip");

                var request = new System.Net.WebClient();
                request.DownloadFile("http://selenium.googlecode.com/files/IEDriverServer_Win32_2.37.0.zip", targetPath);

                var zip = new ZipFile(targetPath);
                zip.ExtractSelectedEntries("IEDriverServer.exe", "", path);
            }
        }

        [TearDown]
        [Description("Clean up each test case.")]
        public void TearDown()
        {
            TryQuitWebDriver();
            var pollUntil = DateTimeOffset.UtcNow.AddMilliseconds(2000);
            while(HasLiveBrowserProcess() && DateTimeOffset.UtcNow < pollUntil)
            {
                Thread.Sleep(50);
            }

            pollUntil = DateTimeOffset.UtcNow.AddMilliseconds(8000);
            while (HasLiveBrowserProcess() && DateTimeOffset.UtcNow < pollUntil)
            {
                TryKillBrowserProcess();
                Thread.Sleep(50);
            }

            if(HasLiveBrowserProcess())
            {
                throw new Exception("Failed to properly shut down the browser.");
            }
        }

        private bool HasLiveBrowserProcess()
        {
            var processes = Process.GetProcessesByName(processName);
            return processes.Length > 0;
        }

        private void TryKillBrowserProcess()
        {
            var processes = Process.GetProcessesByName(processName);
            foreach(var process in processes)
            {
                try
                {
                    process.Kill();
                }
                catch
                {
                    // Ignore errors if unable to close the browser
                }
            }
        }

        private void TryQuitWebDriver()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

    }
}
