﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace MAX_Selenium_V2
{
    [TestFixture]
    public class DashboardPageTest : Test
    {
        // Important Documentation regarding Selenium's IJavaScriptExecutor.ExecuteScript Method
        /*
           The ExecuteScript(String, Object[])method executes JavaScript in the context of the currently selected frame or window.
           This means that "document" will refer to the current document. If the script has a return value, then the following steps will be taken:
               For an HTML element, an IWebElement is returned
               For a number, an Int64 is returned
               For a boolean, a Boolean is returned
               For all other cases a String is returned
               For an array, we attempt to return a List<T> where T is derived from the rules above as applied to the first element. Nested lists are not supported.
               If the value is null or there is no return value, null is returned.
             
           Arguments must be a number (which will be converted to a Int64), a Boolean, a String or a IWebElement. 
           An exception will be thrown if the arguments do not meet these criteria.
           The arguments will be made available to the JavaScript via the "arguments" magic variable, as if the function were called via "Function.apply"
        */

        /*  This dealer doesn't have dashboard data and you can't change dealers with this old version 2 framework.
         *  I'm not spending time fixing this version, since we focus on the other one.  
        [Test]
        public void ExecutiveDashboard_ChartData_Overview_NonZero()
        {
            var page = GoTo_ExecutiveDashboard() as DashboardPage;
            SetDateFrom_12MonthsAgo( page );
            WaitFor_SomeServicesLoading();
            WaitFor_AllServicesLoaded();

            string json = (string) js.ExecuteScript(@"return ko.toJSON( dashboard.data.charts.Overview_AvgSearches() )");
            dynamic data = JObject.Parse( json );
            {
                "series": [{
                    "data": [{
                        "name": "AutoTrader.com",
                        "y": 273154
                    },{
                        "name": "Cars.com",
                        "y": 181648
                    }],
                    "name": "Series 1"
                }]
            }
            
            Assert.IsTrue( data.series[0].data.Count > 0 && data.series[0].data[0].y > 0 );
        } */

        /*
        [Test]
        public void proof_of_concept_exercise()
        {
            GoTo_ExecutiveDashboard();
            WaitFor_DashboardInitialized();

            var bu_id = js.ExecuteScript(@"return dashboard.dynamic_config.businessUnitId()");
            Assert.AreEqual( 101621, bu_id );
        }*/

        [Test]
        public void ExecutiveDashboard_ServiceResponses_Trends_NoErrors_20sec_C26892()
        {
            GoTo_ExecutiveDashboard();
            WaitFor_AllServicesLoaded();
            
            var check = (Boolean) js.ExecuteScript(@"return (dashboard.services.trends.response() != null)");
            Assert.IsTrue( check );
        }

        public Page GoTo_ExecutiveDashboard()
        {
            return __GoTo_SomeDashboard( ExecutiveDashboardPageURL, new DashboardPage() );
        }

        public Page GoTo_GroupLevelDashboard()
        {
            return __GoTo_SomeDashboard( GroupDashboardPageURL, new GroupDashboardPage() );
        }

        private Page __GoTo_SomeDashboard( string URL, Page expectedPage )
        {
            driver.Navigate().GoToUrl( BaseURL );
            Page page = Page.WaitFor( new LoginPage() );
            
            if( !( page is LoginPage ))
                throw new Exception("expected LoginPage");
            Perform_Login( page as LoginPage );
            page = Page.WaitFor( new LoginPage_InvalidCredential(), new DealerSelectorPage(), new HomePage(), new PromotionsPage() );
            
            if( page is LoginPage_InvalidCredential )
                throw new Exception("invalid credentials");

            driver.Navigate().GoToUrl( BaseURL+"/"+URL );
            page = Page.WaitFor( expectedPage );
            return page;
        }

        public void WaitFor_DashboardInitialized( decimal wait_timeout_seconds = 10.0M )
        {
            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;
            while( wait_timeout_ticks <= 0 || (DateTime.Now.Ticks - started) < wait_timeout_ticks )
            {
                var is_dashboard_initialized = (Boolean) js.ExecuteScript(@"return (typeof dashboard != 'undefined' && dashboard != null && (dashboard.constructor == MaxDashboardExecutive || dashboard.constructor == MaxDashboardGroup))");
                if( is_dashboard_initialized )
                    return; // done
                
                Thread.Sleep( 100 );
            }
            throw new Exception( "timeout "+wait_timeout_seconds+" exceeded while waiting for dashboard to initialize");

        }

        public void WaitFor_AllServicesLoaded( decimal wait_timeout_seconds = 30.0M )
        {
            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;

            WaitFor_DashboardInitialized();
            
            Int64 count_services_loading = -1;
            while( wait_timeout_ticks <= 0 || (DateTime.Now.Ticks - started) < wait_timeout_ticks )
            {
                count_services_loading = (Int64) js.ExecuteScript(@"return dashboard.CountServicesLoading()");
                if( count_services_loading == 0 )
                    return; // done
                
                Thread.Sleep( 100 );
            }
            throw new Exception( "timeout "+wait_timeout_seconds+" exceeded while waiting for all dashboard services to finish loading; "
                +(count_services_loading != -1 ? count_services_loading.ToString() : "(unknown)")+" remaining");
        }

        public void WaitFor_SomeServicesLoading( decimal wait_timeout_seconds = 30.0M )
        {
            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;

            WaitFor_DashboardInitialized();
            
            Int64 count_services_loading = -1;
            while( wait_timeout_ticks <= 0 || (DateTime.Now.Ticks - started) < wait_timeout_ticks )
            {
                count_services_loading = (Int64) js.ExecuteScript(@"return dashboard.CountServicesLoading()");
                if( count_services_loading > 0 )
                    return; // done
                
                Thread.Sleep( 100 );
            }
            throw new Exception( "timeout "+wait_timeout_seconds+" exceeded while waiting for at least one dashboard service to start loading" );
        }

        public void SetDateFrom_12MonthsAgo( DashboardPage page )
        {
            var date_string = String.Format( "{0:MM/yyyy}", DateTime.Today.AddYears( -1 ));
            var select = new SelectElement( page.FromDate );
            select.SelectByValue( date_string );
        }
    }
}