﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;


namespace MAX_Selenium
{
    public class Firstlook_Certified_Program_Page : Page
    {
        //constants
        public const String MANUFACTURER_CPO_PAGE_TITLE = "Manufacturer Certified Program Benefit Programs";
        public const String DEALER_CPO_PAGE_TITLE = "Dealer Certified Program Benefit Programs";

        //element finders
        [FindsBy(How = How.CssSelector, Using = "div.participation > h2")]
        public IWebElement CPO_Form_Name;
        
        public Firstlook_Certified_Program_Page(IWebDriver driver) : base(driver)
        {

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TitleContains("Certified Program Benefit Programs"));

            PageFactory.InitElements(driver, this);

            if (!(GetTitle() == MANUFACTURER_CPO_PAGE_TITLE || GetTitle() == DEALER_CPO_PAGE_TITLE))
            {
                throw new Exception("This is not a Firstlook Certified Program page");
            }

        }


    }
}
