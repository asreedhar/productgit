﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using Newtonsoft.Json;

namespace MAX_Selenium
{
    public class CloudSearch_Page : Page
    {

        public string Dealer_Search_Base_Query = Test.CloudSearchURL + "/dealer-search?q="; //add BusinessUnitCode      
        public string Dealer_Search_Base_Structured_Query = Test.CloudSearchURL + "/dealer-search?q.parser=structured&q=";
        public string Vehicle_Search_Base_Structured_Query = Test.CloudSearchURL + "/vehicle-search?q.parser=structured&q=";
        public string Vehicle_Search_Base_Lucene_Query = Test.CloudSearchURL + "/vehicle-search?q.parser=lucene&q=";
        public string Vehicle_Search_Structured_Query_stub = Test.CloudSearchURL + "/vehicle-search";
        public string Dealer_Search_Structured_Query_stub = Test.CloudSearchURL + "/dealer-search";


        public CloudSearch_Page(IWebDriver driver) : base(driver)
        {
        }

        //Returns JSON string from the CloudSearch API, given the query string as a Uri object
        public string GetCloudSearchData(Uri apiQuery)
        {
            var request = WebRequest.Create(apiQuery);
            var response = request.GetResponse();
            var dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);
            var jsonString = reader.ReadToEnd();
            return jsonString;

        }

        public DataRow GetDealerInfoFromDB(string BusinessUnitCode)  //this table is where CloudSearch gets its dealer info from
        {
            var query =
                @"
                SELECT *
                FROM Merchandising.settings.DealerProfilePublishedSnapshot
                WHERE BusinessUnitCode = '" + BusinessUnitCode + "' ";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            return dr;
        }

    }
}
