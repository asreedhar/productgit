﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using NUnit.Framework;
using OpenQA.Selenium;

namespace MAX_Selenium.Rest_API_Tests
{
    [TestFixture]
    class Photo_Rest_API_Test : Test
    {
        [Test]
        public void Ping_Photo_WebService_C3849()
        {
            InitializePageAndLogin_HomePage();
            //string WSN = Web_Service_Environment;
            string URL =
                String.Format("http://{0}.firstlook.biz/photoservices/v1.svc/rest/Echo?input=Hello World", Web_Service_Environment);
            driver.Navigate().GoToUrl(URL);
            Assert.That(driver.PageSource.Contains("Hello World"));
        }

        [Test]
        public void GetBulkUploadManagerUrl_C3850()
        {
            InitializePageAndLogin_HomePage();
            string URL = String.Format("http://{0}.firstlook.biz/photoservices/v1.svc/rest/GetBulkUploadManagerUrl?username=jsu&firstname=John&lastname=Su&businessUnitCode=WINDYCIT05&context=2", Web_Service_Environment);
            driver.Navigate().GoToUrl(URL);
            string pageSource = driver.PageSource;
            XmlDocument domContent = new XmlDocument();
            domContent.LoadXml(pageSource);
            
            var temp = domContent.SelectSingleNode("//*[local-name() = 'string']"); // "//string didn't work, possible namespace problem. 
           
            if (temp != null)
            {
                string BulkUpLoadManangerUrl = temp.InnerText;
                driver.Navigate().GoToUrl(BulkUpLoadManangerUrl);
                Assert.That(driver.FindElement(By.Id("assignButton")).Displayed);
            } 
            else 
                Assert.Fail("Failed to get the Bulk Up Load Manager URL");                      
        }

        [Test]
        public void GetMainPhotoUrl_C3851()
        {
            InitializePageAndLogin_HomePage();
            string URL =
                String.Format(
                    "http://{0}.firstlook.biz/photoservices/v1.svc/rest/GetMainPhotoUrl?businessUnitCode=WINDYCIT05&vin={1}",
                    Web_Service_Environment, UsedGMVehicleVIN);
            driver.Navigate().GoToUrl(URL);
            string pageSource = driver.PageSource;
            XmlDocument domContent = new XmlDocument();
            domContent.LoadXml(pageSource);
            var temp = domContent.SelectSingleNode("//*[local-name() = 'string']"); // "//string didn't work, possible namespace problem. 
            if (temp != null)
            {
                string mainPhotoUrl = temp.InnerText;
                driver.Navigate().GoToUrl(mainPhotoUrl);
                Assert.That(driver.PageSource.Contains("640 × 480 pixels")); // the url stored in mainPhotoUrl now redirects to S3, HTML no longer says "mainPhotoUrl", but does give pixel size
            }
            else
                Assert.Fail("Failed to get the Main Photo URL");  
        }

        [Test]
        public void GetMainThumbNailUrl_C3852()
        {
            InitializePageAndLogin_HomePage();
            string URL =
                String.Format(
                    "http://{0}.firstlook.biz/photoservices/v1.svc/rest/GetMainThumbnailUrl?businessUnitCode=WINDYCIT05&vin={1}",
                    Web_Service_Environment, UsedGMVehicleVIN);
            driver.Navigate().GoToUrl(URL);
            string pageSource = driver.PageSource;
            XmlDocument domContent = new XmlDocument();
            domContent.LoadXml(pageSource);
            var temp = domContent.SelectSingleNode("//*[local-name() = 'Url']");  
            if (temp != null)
            {
                string mainThumUrl = temp.InnerText;
                driver.Navigate().GoToUrl(mainThumUrl);
                Assert.That(driver.PageSource.Contains("225 × 169 pixels")); // the url stored in mainThumUrl now redirects to S3, HTML no longer says "mainThumUrl", but does give pixel size
            }
            else
                Assert.Fail("Failed to get the Main Thumb Nail Photo URL");  
        }

        [Test]
        public void GetPhotoManagerUrl_C3853()
        {
            InitializePageAndLogin_HomePage();
            string URL =
                String.Format(
                    "http://{0}.firstlook.biz/photoservices/v1.svc/rest/GetPhotoManagerUrl?username=JSU&firstname=John&lastname=Su&businessUnitCode=WINDYCIT05&vin=2G1WB5EK3A1148671&stockNumber=31390&context=2",
                    Web_Service_Environment);
            driver.Navigate().GoToUrl(URL);
            string pageSource = driver.PageSource;
            XmlDocument domContent = new XmlDocument();
            domContent.LoadXml(pageSource);
            var temp = domContent.SelectSingleNode("//*[local-name() = 'string']");
            if (temp != null)
            {
                string photoManagerUrl = temp.InnerText;
                driver.Navigate().GoToUrl(photoManagerUrl);
                Assert.That(driver.FindElement(By.LinkText("Save")).Displayed);
            }
            else
                Assert.Fail("Failed to get the Photo Manager URL");  
        }
    }
}
