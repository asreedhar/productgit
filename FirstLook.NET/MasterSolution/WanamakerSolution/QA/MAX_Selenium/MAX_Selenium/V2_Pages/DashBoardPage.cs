﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium_V2
{
    public class DashboardPage : Page
    {
        [FindsBy(How = How.XPath, Using = "/html/head/title[contains(.,'Dashboard | MAX : Intelligent Online Advertising Systems')]")]
        public IWebElement PageTitle;

        [FindsBy(How = How.CssSelector, Using = "#MaxAdLink")]
        public IWebElement BigMaxButton;

        [FindsBy(How = How.CssSelector, Using = "select#startDate")]
        public IWebElement FromDate;
        [FindsBy(How = How.CssSelector, Using = "select#endDate")]
        public IWebElement ToDate;
    }
}
