﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium_V2
{
    public class GroupDashboardPage : Page
    {
        [FindsBy(How = How.XPath, Using = "/html/head/title[contains(.,'Group Dashboard | MAX : Intelligent Online Advertising Systems')]")]
        public IWebElement PageTitle;

        [FindsBy(How = How.CssSelector, Using = ".refresh > .progress")]
        public IWebElement RefreshProgress;

    }
}
