﻿
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;

namespace MAX_Selenium_V2
{
    public class HomePage : Page
    {
        [FindsBy(How = How.XPath, Using = "/html/head/title[contains(.,'FirstLook | Home')]")]
        public IWebElement PageTitle;

        [FindsBy(How = How.LinkText, Using = "Digital Marketing System")]
        public IWebElement MAX_AD_Button_30;

        [FindsBy(How = How.XPath, Using = ".//*[@id='global_menu_bar']/li[2]/span")]
        public IWebElement Tools_Menu;

    }
}
