﻿using System;
using System.Reflection;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;

namespace MAX_Selenium_V2
{
    public class Page
    {
        public static IWebDriver driver;
        public const decimal default_timeout_seconds = 30.0M;

        public static void SetDriver( IWebDriver driver )
        {
            Page.driver = driver;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////////

        // explicitly passing timeout less than or equal to zero means timeout is unused
        // otherwise the default is used
        public static Page WaitFor( decimal wait_timeout_seconds, params Page[] pages )
        {
            if( driver == null )
                return null; // must inform this class of the driver being used before waiting for pages
            if( pages.Length == 0 )
                return null;

            long wait_timeout_ticks = (long)(wait_timeout_seconds * TimeSpan.TicksPerSecond);
            long started = DateTime.Now.Ticks;
            while( wait_timeout_ticks <= 0 || (DateTime.Now.Ticks - started) < wait_timeout_ticks )
            {
                foreach( Page p in pages )
                {
                    p.Clear();
                    try
                    {
                        PageFactory.InitElements( driver, p );
                        if( p.IsFullyInstantiated() )
                        {
                            return p;
                        }
                    }
                    catch( NoSuchElementException e )
                    {
                        // this is actually pretty normal
                    }
                }
                Thread.Sleep( 0 );
            }
            
            var message = "WebDriver timeout while waiting for any one of the following pages:";
            message += "\n  "+String.Join(", ", pages.Select( p => p.GetType().Name ));
            foreach( var p in pages )
                message += "\n    "+p.GetType().Name+": "+String.Join(", ", p.ListUninstantiatedElements())+" (unmatched)";
            throw new Exception( message );                
        }

        // wait for one of the provided page objects to decide it is a match
        public static Page WaitFor( params Page[] pages )
        {
            return WaitFor( default_timeout_seconds, pages );
        }

        // alternate parameter format overload: list of types
        public static Page WaitFor( decimal wait_timeout_seconds, params Type[] page_types )
        {
            Page[] pages = (from page_type in page_types
                           select ConstructDefault( page_type ) as Page).ToArray();
            return WaitFor( wait_timeout_seconds, pages );
        }

        public static Page WaitFor( params Type[] page_types )
        {
            return WaitFor( default_timeout_seconds, page_types );
        }
        
        // alternate parameter format overload: list of types as generic
        public static Page WaitFor<T1>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page );
        }
        public static Page WaitFor<T1,T2>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page  where T2:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page, ConstructDefault( typeof(T2) ) as Page );
        }
        public static Page WaitFor<T1,T2,T3>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page  where T2:Page  where T3:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page, ConstructDefault( typeof(T2) ) as Page, ConstructDefault( typeof(T3) ) as Page );
        }
        public static Page WaitFor<T1,T2,T3,T4>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page  where T2:Page  where T3:Page  where T4:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page, ConstructDefault( typeof(T2) ) as Page, ConstructDefault( typeof(T3) ) as Page, ConstructDefault( typeof(T4) ) as Page );
        }
        public static Page WaitFor<T1,T2,T3,T4,T5>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page  where T2:Page  where T3:Page  where T4:Page  where T5:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page, ConstructDefault( typeof(T2) ) as Page, ConstructDefault( typeof(T3) ) as Page, ConstructDefault( typeof(T4) ) as Page, ConstructDefault( typeof(T5) ) as Page );
        }
        public static Page WaitFor<T1,T2,T3,T4,T5,T6>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page  where T2:Page  where T3:Page  where T4:Page  where T5:Page  where T6:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page, ConstructDefault( typeof(T2) ) as Page, ConstructDefault( typeof(T3) ) as Page, ConstructDefault( typeof(T4) ) as Page, ConstructDefault( typeof(T5) ) as Page, ConstructDefault( typeof(T6) ) as Page );
        }
        public static Page WaitFor<T1,T2,T3,T4,T5,T6,T7>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page  where T2:Page  where T3:Page  where T4:Page  where T5:Page  where T6:Page  where T7:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page, ConstructDefault( typeof(T2) ) as Page, ConstructDefault( typeof(T3) ) as Page, ConstructDefault( typeof(T4) ) as Page, ConstructDefault( typeof(T5) ) as Page, ConstructDefault( typeof(T6) ) as Page, ConstructDefault( typeof(T7) ) as Page );
        }
        public static Page WaitFor<T1,T2,T3,T4,T5,T6,T7,T8>( decimal wait_timeout_seconds = default_timeout_seconds )  where T1:Page  where T2:Page  where T3:Page  where T4:Page  where T5:Page  where T6:Page  where T7:Page  where T8:Page
        {
            return WaitFor( wait_timeout_seconds,  ConstructDefault( typeof(T1) ) as Page, ConstructDefault( typeof(T2) ) as Page, ConstructDefault( typeof(T3) ) as Page, ConstructDefault( typeof(T4) ) as Page, ConstructDefault( typeof(T5) ) as Page, ConstructDefault( typeof(T6) ) as Page, ConstructDefault( typeof(T7) ) as Page, ConstructDefault( typeof(T8) ) as Page );
        }

        // call default constructor for type
        private static object ConstructDefault( Type T )
        {
            var constructor = T.GetConstructor(System.Type.EmptyTypes);
            if( constructor == null )
                return null;
            return constructor.Invoke(new Object[0]);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        // removes all previously-instantiated references to declared elements
        public void Clear()
        {
            foreach( var m in GetType().GetMembers() )
            {
                if( m.IsDefined( typeof(FindsByAttribute), true ))
                {
                    ((FieldInfo)m).SetValue( this, null );
                }
            }
        }

        // a.k.a.: "All Elements Matched"
        //   with selenium, if a web element is instantiated, it exists in the current web drivers' DOM and has been matched based on provided search criteria
        public bool IsFullyInstantiated()
        {
            var fields = GetType().GetFields();
            //Debug.Print( String.Format( "{0} ({1})", GetType().Name, fields.Length ));
            foreach( var field in fields )
            {
                //Debug.Print( String.Format( "  {0}:{1} {2}", field.Name, field.FieldType.Name, String.Join(",",field.FieldType.GetInterfaces().Select(x=>x.Name)) ));
                if( typeof( IWebElement ).IsAssignableFrom( field.FieldType ))
                {
                    try
                    {
                        IWebElement element = field.GetValue(this) as IWebElement;
                        if( element == null )
                            return false;
                        // required because IWebElements populated by Selenium Framework are "lazy" proxies
                        var tag_name = element.TagName;
                    }
                    catch( NoSuchElementException e )
                    {
                        return false;
                    }
                    catch( StaleElementReferenceException e )
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        // a.k.a.: "What Didn't Match?"
        //   for composing more thorough error messages, will provide a list of unmatched elements
        public IEnumerable<string> ListUninstantiatedElements()
        {
            var fields = GetType().GetFields();
            var list = new List<string>( fields.Count() );
            foreach( var field in fields )
            {
                if( typeof( IWebElement ).IsAssignableFrom( field.FieldType ))
                {
                    try
                    {
                        IWebElement element = field.GetValue(this) as IWebElement;
                        if( element == null )
                        {
                            list.Add( field.Name );
                        }
                    }
                    catch( NoSuchElementException e )
                    {
                    }
                    catch( StaleElementReferenceException e )
                    {
                    }
                }
            }
            return list;
        }
    }
}
