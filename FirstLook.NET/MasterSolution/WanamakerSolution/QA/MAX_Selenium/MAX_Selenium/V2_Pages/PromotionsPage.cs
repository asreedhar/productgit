﻿
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;

namespace MAX_Selenium_V2
{
    public class PromotionsPage : Page
    {
        [FindsBy(How = How.XPath, Using = "/html/head/title[contains(.,'Promotion Summary')]")]
        public IWebElement PageTitle;

        [FindsBy(How = How.CssSelector, Using = "img.promotion_summary_image")] 
        public IWebElement PromotionImage;

        [FindsBy(How = How.CssSelector, Using = "input.button[src*='no_thanks']")]
        public IWebElement NoThanks_Button;

        [FindsBy(How = How.CssSelector, Using = "input.button[src*='yes_i_want_to_learn_more']")]
        public IWebElement YesIWantToLearnMore_Button;

        [FindsBy(How = How.CssSelector, Using = "input#neverCheckbox")]
        public IWebElement DontShowAgain_Checkbox;
    }
}
