﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace MAX_Selenium_V2
{
    public class DealerSelectorPage : Page
    {
        [FindsBy(How = How.XPath, Using = "/html/head/title[contains(.,'MAX: Login')]")]
        public IWebElement PageTitle;

        [FindsBy(How = How.XPath, Using = "//ul[./li/a[contains(@href,'/login/select_business_unit/')]]")]
        public IWebElement DealerList;

    }
}
