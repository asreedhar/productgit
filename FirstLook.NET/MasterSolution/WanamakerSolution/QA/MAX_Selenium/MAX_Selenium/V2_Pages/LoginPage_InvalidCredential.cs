﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;

namespace MAX_Selenium_V2
{
    public class LoginPage_InvalidCredential : LoginPage
    {
        [FindsBy(How = How.XPath, Using = "//div[@id='loginformtext' and contains(.,'Invalid UserName or Password.')]")]
        public IWebElement LoginFormText_Error;
	}  
}
