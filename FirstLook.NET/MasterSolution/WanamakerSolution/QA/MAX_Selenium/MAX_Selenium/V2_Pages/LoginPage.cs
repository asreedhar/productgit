﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;

namespace MAX_Selenium_V2
{
    public class LoginPage : Page
    {
        [FindsBy(How = How.XPath, Using = "/html/head/title[contains(.,'FirstLook - Inventory Management and Pre-Owned Automotive - Pat Ryan Jr')]")]
        public IWebElement PageTitle;

        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement UserName;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement Passwd;
    
        [FindsBy(How = How.Name, Using = "submit")]
        public IWebElement Submit;
  
	}  
}
