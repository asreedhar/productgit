﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class MaxDigitalShowroom_Page : Page
    {

        [FindsBy(How = How.Id, Using = "dealer-name")]
        public IWebElement DealerSearchPage_DealerSearch;

        [FindsBy(How = How.CssSelector, Using = "div.container a.different-dealership")] 
        public IWebElement ChooseDealership_Link;

        [FindsBy(How = How.CssSelector, Using = "#dealer-search-form > div > h1")]
        public IWebElement DealerSearchForm_PleaseEnter_Text;

        [FindsBy(How = How.ClassName, Using = "ui-menu-item")]          //remove this locator after defect is fixed
        public IWebElement DealerSearchPage_FirstItemInSearchResults;   //defect - type in dealer, must also click on suggested one from text box results before submitting

        [FindsBy(How = How.Id, Using = "dealer-search-submit")]
        public IWebElement DealerSearchPage_SubmitButton;

        [FindsBy(How = How.Id, Using = "stockvin")]
        public IWebElement VehicleSearchPage_VinStockNumSearch;

        [FindsBy(How = How.Id, Using = "vehicle-search-submit")]
        public IWebElement VehicleSearchPage_SearchButton;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"search-header-container\"]/div/header/div[2]")]
        public IWebElement VehicleSearchPage_DealerLogo;

        [FindsBy(How = How.Id, Using = "make")]
        public IWebElement VehicleSearchPage_MakeDDL;


        // Dropdown Lists
        [FindsBy(How = How.CssSelector, Using = "div.filters select#years-max.form-control")]
        public IWebElement desktop_YearsNewFilter_bud;
        public SelectElement Desktop_YearsNewFilter_DDL; //cannot instantiate in constructor; requires navigation first

        // Popover Drop Down Lists
        [FindsBy(How = How.CssSelector, Using = "popupmenu#newused-select div.select-menu")]
        public IWebElement vehicleSearch_NewUsedDDL_bud;
        public SelectPopoverElement VehicleSearch_NewUsed_DDL;

        [FindsBy(How = How.CssSelector, Using = "makemenu div.select-menu")]
        public IWebElement vehicleSearch_MakeDDL_bud;
        public SelectPopoverElement VehicleSearch_Make_DDL;

        [FindsBy(How = How.CssSelector, Using = "modelmenu div.select-menu")]
        public IWebElement vehicleSearch_ModelDDL_bud;
        public SelectPopoverElement VehicleSearch_Model_DDL;

        [FindsBy(How = How.CssSelector, Using = "makemenu.make-menu div.select-menu")]
        public IWebElement searchResultsPage_MakeDDL_bud;
        public SelectPopoverElement Desktop_SearchResults_Make_DDL;

        [FindsBy(How = How.CssSelector, Using = "modelmenu.model-menu div.select-menu")]
        public IWebElement searchResultsPage_ModelDDL_bud;
        public SelectPopoverElement Desktop_SearchResults_Model_DDL;

        [FindsBy(How = How.CssSelector, Using = "popupmenu#years-max div.select-menu")]
        public IWebElement desktop_SearchResultsPage_YearsMax_bud;
        public SelectPopoverElement Desktop_SearchResults_Years_DDL;


        // Mobile view locators
        [FindsBy(How = How.XPath, Using = "//div[@class='details small']/div[@class='price ng-scope ng-binding']")]
        public IWebElement Mobile_SearchResultsPage_Price;

        [FindsBy(How = How.XPath, Using = "//div[@class='item mileage ng-scope ng-binding']")]
        public IWebElement Mobile_SearchResultsPage_Mileage;

        [FindsBy(How = How.XPath, Using = "//div[@class='item transmission ng-binding']")]
        public IWebElement Mobile_SearchResultsPage_Trim;

        [FindsBy(How = How.XPath, Using = "//div[@class='item color ng-binding']")]
        public IWebElement Mobile_SearchResultsPage_Color;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"search-results\"]/div[3]/div/div[2]/div[2]")]
        public IWebElement Mobile_SearchResultsPage_VehiclePhoto;

        [FindsBy(How = How.XPath, Using = "//div[@class='year-make-model small ng-binding']")]
        public IWebElement Mobile_SearchResultsPage_YearModel;

        [FindsBy(How = How.XPath, Using = "//div[@class='make-model ng-binding']")]
        public IWebElement Mobile_SearchResultsPage_SmallMenuTitle_MakeModel;

        [FindsBy(How = How.Id, Using = "vehicle-name")] 
        public IWebElement Mobile_VehicleDescriptionPage_VehicleName;

        [FindsBy(How = How.XPath, Using = "//a[@href='#see-more']")]
        public IWebElement Mobile_VehicleDescriptionPage_SeeMoreButton;


        // Desktop view locators
        [FindsBy(How = How.XPath, Using = ".//div[@class='results-section ng-scope'][1]//div[starts-with(@class,'price')]")]
        public IWebElement Desktop_SearchResultsPage_FirstPrice;

        [FindsBy(How = How.XPath, Using = ".//div[@class='results-section ng-scope'][1]//td[starts-with(@class,'mileage')]/div")]
        public IWebElement Desktop_SearchResultsPage_FirstMileage;

        [FindsBy(How = How.XPath, Using = ".//div[@class='results-section ng-scope'][1]//td[starts-with(@class,'color')]")]
        public IWebElement Desktop_SearchResultsPage_FirstColor;

        [FindsBy(How = How.CssSelector, Using = "div.container div.ad-group-stocknumber")]
        public IWebElement Desktop_SearchResultsPage_StockNumber;

        [FindsBy(How = How.Id, Using = "years-min")]
        public IWebElement Desktop_SearchResultsPage_YearsMin;

        [FindsBy(How = How.Id, Using = "years-range")]
        public IWebElement Desktop_SearchResultsPage_YearsRange;

        [FindsBy(How = How.Id, Using = "mileage-min")]
        public IWebElement Desktop_SearchResultsPage_MileageMin;

        [FindsBy(How = How.Id, Using = "mileage-max")]
        public IWebElement Desktop_SearchResultsPage_MileageMax;

        [FindsBy(How = How.Id, Using = "mileage-range")]
        public IWebElement Desktop_SearchResultsPage_MileageRange;

        [FindsBy(How = How.Id, Using = "price-min")]
        public IWebElement Desktop_SearchResultsPage_PriceMin;

        [FindsBy(How = How.Id, Using = "price-max")]
        public IWebElement Desktop_SearchResultsPage_PriceMax;

        [FindsBy(How = How.Id, Using = "price-range")]
        public IWebElement Desktop_SearchResultsPage_PriceRange;

        [FindsBy(How = How.Id, Using = "trim")]
        public IWebElement Desktop_SearchResultsPage_Trim;

        [FindsBy(How = How.Id, Using = "extColor")]
        public IWebElement Desktop_SearchResultsPage_Color;

        [FindsBy(How = How.Id, Using = "filter-packages")]
        public IWebElement Desktop_SearchResultsPage_Packages;

        [FindsBy(How = How.CssSelector, Using = "#search-results > div.filters.form-inline > div.vehicles-available > span")]
        public IWebElement Desktop_SearchResultsPage_AvailalbleVehiclesCount;

        [FindsBy(How = How.Id, Using = "vin")]
        public IWebElement Desktop_SearchResultsPage_VinStockNumSearch;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"search-form\"]/div/form/input[2]")]
        public IWebElement Desktop_SearchResultsPage_SearchButton;


        //Max Mobile Showroom locators
        [FindsBy(How = How.XPath, Using = "//*[@id=\"overview\"]/div[1]/a[2]/span/span")]
        public IWebElement MMS_SaveButton;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"overview-menu-item-highlights\"]/div/div/a")]
        public IWebElement MMS_HighlightsButton;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"vehicle-details\"]/p[1]")]
        public IWebElement MMS_Mileage;

        [FindsBy(How = How.CssSelector, Using = "div#vehicle-details p.interior-color")]
        public IWebElement MMS_InteriorColor;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"vehicle-details\"]/p[3]")]
        public IWebElement MMS_Transmission;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"vehicle-details\"]/p[4]")]
        public IWebElement MMS_Engine;
        
        public MaxDigitalShowroom_Page(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("Digital Showroom"));

            if (driver.Title != "Digital Showroom")
            {
                throw new StaleElementReferenceException("This is not the Digital Showroom page.");
            }
            PageFactory.InitElements(driver, this);
        }

        public void GoToMobileView()
        {
            driver.Manage().Window.Size = new Size(750, 1000);
        }

        public void GoToDesktopView()
        {
            driver.Manage().Window.Size = new Size(1200, 1000);
        }

        public void SearchForDealer(String dealer)
        {
            try
            {
                ChooseDealership_Link.SendKeys(Keys.Enter);
                WaitForAjax();
            }
            catch (NoSuchElementException)
            {}
            DealerSearchPage_DealerSearch.SendKeys(dealer);
            DealerSearchPage_FirstItemInSearchResults.Click();
            DealerSearchPage_SubmitButton.Click();
            WaitForAjax();
        }

        //Pass in a BU of a store with MMS enabled and this returns a used vin with some vehicle info. Defaults to Used cars.
        public DataRow GetVinWithShowroomEnabled(int bu)  
        {
            var query =
                @"
                   SELECT TOP 10
                   i.vin,i.StockNumber,i.ListPrice,i.VehicleYear,i.Make,i.Model,
                   i.Trim,i.MileageReceived,i.BaseColor,i.StockNumber,
                    Transmission.UserFriendlyName AS Transmission,
                    Engine.UserFriendlyName AS Engine
                    FROM Merchandising.workflow.Inventory i	
					JOIN Merchandising.postings.VehicleAdScheduling vas on vas.businessUnitID = i.BusinessUnitID and vas.inventoryId = i.InventoryID
					JOIN Merchandising.builder.NewEquipmentMapping n ON vas.inventoryId = n.inventoryID
					JOIN VehicleCatalog.Chrome.Categories Transmission ON Transmission.CategoryID = n.TransmissionTypeCategoryID
					JOIN VehicleCatalog.Chrome.Categories Engine ON Engine.CategoryID = n.EngineTypeCategoryID
                    WHERE i.InventoryType = 2
                    AND i.StatusBucket != 8 --exclude not-online
                    --AND i.Make NOT IN (UPPER(i.Make) COLLATE SQL_Latin1_General_CP1_CS_AS) --using this will exclude BMWs and can result in zero results
                    AND i.Model NOT IN (UPPER(i.Model) COLLATE SQL_Latin1_General_CP1_CS_AS) --exclude ALL CAPS results
                    AND i.MileageReceived > 0
                    --AND vas.actualReleaseTime <= DATEADD(hh, -5, GETDATE()) -- temporary disable for 7/7 DSR release
                    AND i.BusinessUnitID = " + bu + " " +
                    "ORDER BY vas.actualReleaseTime ASC";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            return dr;
        }

        //Overload - Accepts businessUnitId and "New" or "Used", Returns 1 vehicle's info
        public DataRow GetVinWithShowroomEnabled(int bu, string newUsed)
        {
            int invType = 0;
            newUsed = newUsed.ToLower();
            if (newUsed == "new")
            {
                invType = 1;
            }
            else if (newUsed == "used")
            {
                invType = 2;
            }

            var query =
                string.Format(@"
                   SELECT TOP 10
                   i.vin,i.StockNumber,i.ListPrice,i.VehicleYear,i.Make,i.Model,
                   i.Trim,i.MileageReceived,i.BaseColor,i.StockNumber,
                    Transmission.UserFriendlyName AS Transmission,
                    Engine.UserFriendlyName AS Engine
                    FROM Merchandising.workflow.Inventory i	
					JOIN Merchandising.postings.VehicleAdScheduling vas on vas.businessUnitID = i.BusinessUnitID and vas.inventoryId = i.InventoryID
					JOIN Merchandising.builder.NewEquipmentMapping n ON vas.inventoryId = n.inventoryID
					JOIN VehicleCatalog.Chrome.Categories Transmission ON Transmission.CategoryID = n.TransmissionTypeCategoryID
					JOIN VehicleCatalog.Chrome.Categories Engine ON Engine.CategoryID = n.EngineTypeCategoryID
                    WHERE i.InventoryType = {0}
                    AND i.StatusBucket != 8 --exclude not-online
                    AND i.Make NOT IN (UPPER(i.Make) COLLATE SQL_Latin1_General_CP1_CS_AS) --exclude ALL CAPS results
                    AND i.Model NOT IN (UPPER(i.Model) COLLATE SQL_Latin1_General_CP1_CS_AS)
                    AND i.MileageReceived > 0
                    AND vas.actualReleaseTime <= DATEADD(hh, -5, GETDATE())
                    AND i.BusinessUnitID = {1} ORDER BY vas.actualReleaseTime ASC", invType, bu);

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            return dr;
        }



        public void selectInventoryTypeByTextInDDL(string type)
        {
            try
            {
                new SelectElement(driver.FindElement(By.Id("newused-select"))).SelectByText(type);
            }
            catch (NoSuchElementException)
            {
            }
        }

        public void selectMakeByTextInDDL(string make)  
        {
            new SelectElement(driver.FindElement(By.Id("make"))).SelectByText(make);
        }

        public void selectModelByTextInDDL(string model)
        {
            new SelectElement(driver.FindElement(By.Id("model"))).SelectByText(model);
        }

        public DataRow GetCertifiedVinWithShowroomEnabled(int bu, int certified)  
        {
            var query =
                @"
                    SELECT TOP 1 MWI.vin,MWI.StockNumber,MWI.ListPrice,MWI.VehicleYear,MWI.Make,MWI.Model,MWI.Trim,MWI.MileageReceived,MWI.BaseColor,MWI.StockNumber,IDV.OriginalMake,IDV.OriginalModel,
                        IDV.VehicleEngine, IDV.VehicleTransmission, IDV.BaseColor
                    FROM merchandising.workflow.Inventory MWI
                        JOIN IMT.dbo.Vehicle IDV ON IDV.vin = MWI.vin
                        JOIN Merchandising.postings.VehicleAdScheduling vas on vas.businessUnitID = MWI.BusinessUnitID and vas.inventoryId = MWI.InventoryID
                    WHERE mwi.InventoryType = 2
                    AND MWI.Certified = " + certified + " AND MWI.BusinessUnitID = " + bu + 
                    "ORDER BY vas.actualReleaseTime DESC";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            return dr;
        }
    }
}
