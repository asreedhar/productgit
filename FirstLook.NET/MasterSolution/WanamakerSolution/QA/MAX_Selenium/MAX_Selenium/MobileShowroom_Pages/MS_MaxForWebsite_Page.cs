﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    public class MS_MaxForWebsite_Page : Page
    {
        //finders
        [FindsBy(How = How.CssSelector, Using = "header span[class='nav-item trade-in ng-scope']")]
        public IWebElement ValueTradeIn_Link;

        [FindsBy(How = How.XPath, Using = ".//span[@class='vehicle-color detail-item-value ng-scope ng-binding']")]
        public IWebElement Color;

        [FindsBy(How = How.XPath, Using = ".//span[@class='detail-item-value ng-binding']")]
        public IWebElement mileage;

        [FindsBy(How = How.XPath, Using = ".//h1[@class='vehicle-name ng-scope ng-binding']")]
        public IWebElement YearModel;

        [FindsBy(How = How.XPath, Using = ".//span[@class='price ng-binding']")]
        public IWebElement Your_Price;

        [FindsBy(How = How.XPath, Using = ".//div[@class='row vehicle-section']//div[@ng-if = 'shouldShowPricingArea(vehicle)']")]
        public IWebElement Price_Gauge_Div;

        [FindsBy(How = How.CssSelector, Using = "div.tradein-nada-lightbox div.tradein-error")]
        public IWebElement TradeInError;


        // drop down lists
        [FindsBy(How = How.CssSelector, Using = "div.tradein-nada-lightbox select.select-year")]
        public IWebElement nadalightbox_YearDDL_bud;
        public SelectElement NADALightbox_Year_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.tradein-nada-lightbox select.select-make")]
        public IWebElement nadalightbox_MakeDDL_bud;
        public SelectElement NADALightbox_Make_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.tradein-nada-lightbox select.select-model")]
        public IWebElement nadalightbox_ModelDDL_bud;
        public SelectElement NADALightbox_Model_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.tradein-nada-lightbox select.select-trim")]
        public IWebElement nadalightbox_TrimDDL_bud;
        public SelectElement NADALightbox_Trim_DDL;

        [FindsBy(How = How.CssSelector, Using = "div.tradein-nada-lightbox select.select-mileage")]
        public IWebElement nadalightbox_MileageDDL_bud;
        public SelectElement NADALightbox_Mileage_DDL;

        
        public MS_MaxForWebsite_Page(IWebDriver driver) : base(driver)
        {  

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            wait.Until(ExpectedConditions.TitleContains("MAX for Website 2.0"));

            if (driver.Title != "MAX for Website 2.0") 
            {
                throw new StaleElementReferenceException("This is not the MAX For Website page.");
            }
            PageFactory.InitElements(driver, this);

	    }  

    }
}
