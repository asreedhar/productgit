﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    
    [TestFixture]
    class MaxDigitalShowroom_Test : Test
    {


        [TestCase(Firefox, true)]
        //[TestCase(InternetExplorer, true)]
        [TestCase(Chrome, true)]
        [Category("SmokeDigitalShowroom")]
        [Description("This looks at the 'Please enter dealership name' text and verifies it changes to the correct size for mobile and desktop")]
        public void Window_Resize_Adjusts_UI_For_Mobile_And_Desktop_C20262(string browser, bool overRide)
        {
            InitializeDriver(browser, overRide);
            NavigateToMaxDigitalShowroom();
            try
            {
                maxdigitalshowroom_page.ChooseDealership_Link.SendKeys(Keys.Enter);
            }
            catch (NoSuchElementException)
            { }

            //resizes window for mobile
            driver.Manage().Window.Size = new Size(800, 800);
            Thread.Sleep(1000);
            var fontSizeMobile = maxdigitalshowroom_page.DealerSearchForm_PleaseEnter_Text.GetCssValue("font-size");
            Assert.That(fontSizeMobile == "35px");

            //resizes window for desktop
            driver.Manage().Window.Size = new Size(1000, 800);
            Thread.Sleep(1000);
            string fontSizeDesktop = maxdigitalshowroom_page.DealerSearchForm_PleaseEnter_Text.GetCssValue("font-size");
            Assert.That(fontSizeDesktop == "52px");

        }

        [Test]
        [Description("Change to mobile view and search for a vin, and verify its data on the search results page")]
        public void Search_Vin_And_Verify_Vehicle_Data_In_Mobile_View_C20299()
        {
            string dealer = "Windy City Chevrolet";
            int bu = 101590;

            var dbDealerData = new DealerData.BasicDealerData(bu);
            var ownerHandle = dbDealerData.OwnerHandle;
            var csDealerData = new DealerData.BasicCloudSearchDealerData(ownerHandle);
            var hasNew = csDealerData.NewCarsEnabled;
            
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(bu);
            string vin = Convert.ToString(dr.ItemArray[0]);

            if (hasNew == 1) // If New is enabled, the New/Used DDL will appear
            {
                maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_NewUsedDDL_bud);
                maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("Used");

            }

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            
            int Price_DB = Convert.ToInt32(dr.ItemArray[2]);
            if(Price_DB != 0)  //Price does not get displayed in the UI if it is zero, no assert is necessary 
            {
                //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
               // wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(".//div[@class='price ng-scope ng-binding']")));

                int Price_UI = Utility.ExtractDigitsFromString(maxdigitalshowroom_page.Mobile_SearchResultsPage_Price.Text);
                Assert.IsTrue(Price_DB == Price_UI, "Price in the database is {0}, Price in the UI is {1} for vin {2} at {3} ", Price_DB, Price_UI, vin, dealer); 
            }
            
            int Mileage_DB = Convert.ToInt32(dr.ItemArray[7]);
            int Mileage_UI = Utility.ExtractDigitsFromString(maxdigitalshowroom_page.Mobile_SearchResultsPage_Mileage.Text);
            Assert.IsTrue(Mileage_DB == Mileage_UI, "Mileage in the database is {0}, Mileage in the UI is {1} for vin {2} at {3} ", Mileage_DB, Mileage_UI, vin, dealer);

            string Trim_DB = Convert.ToString(dr.ItemArray[6]).ToLower();
            if(Trim_DB != "")
            {
                string Trim_UI = maxdigitalshowroom_page.Mobile_SearchResultsPage_Trim.Text.ToLower();
                Assert.IsTrue(Trim_DB == Trim_UI, "Trim in the database is {0}, Trim in the UI is {1} for vin {2} at {3} ", Trim_DB, Trim_UI, vin, dealer);
            }
            
            string Year_DB = Convert.ToString(dr.ItemArray[3]);
            string Model_DB = (Convert.ToString(dr.ItemArray[5])).ToLower();
            string YearModel_DB = Year_DB + " " + Model_DB;
            string YearModel_UI = maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Text.ToLower();
            Assert.IsTrue(YearModel_DB == YearModel_UI, "Year/Model in the database is {0}, Year/Model in the UI is {1} for vin {2} at {3} ", YearModel_DB, YearModel_UI, vin, dealer);

            string Color_DB = Convert.ToString(dr.ItemArray[8]).ToLower();
            if(Color_DB != "")
            {
                string Color_UI = maxdigitalshowroom_page.Mobile_SearchResultsPage_Color.Text.ToLower();
                Assert.IsTrue(Color_DB == Color_UI, "Color in the database is {0}, Color in the UI is {1} for vin {2} at {3} ", Color_DB, Color_UI, vin, dealer);
            }
        }

        [TestCase(Firefox, true)]
        //[TestCase(InternetExplorer, true)]
        [TestCase(Chrome, true)]
        [Category("SmokeDigitalShowroom")]
        [Description("Change to desktop view and search for a vin, and verify its data on the search results page")]
        public void Search_Vin_And_Verify_Vehicle_Data_In_Desktop_View_C20738(string browser, bool overRide)
        {
            string dealer = "Windy City Chevrolet";
            int bu = 101590;

            InitializeDriver(browser, overRide);
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToDesktopView();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(bu);
            
            string vin = Convert.ToString(dr.ItemArray[0]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();

            int Price_DB = Convert.ToInt32(dr.ItemArray[2]);
            if(Price_DB != 0)  //Price does not get displayed in the UI if it is zero, no assert is necessary 
            {
                int Price_UI = Utility.ExtractDigitsFromString(maxdigitalshowroom_page.Desktop_SearchResultsPage_FirstPrice.Text);
                Assert.IsTrue(Price_DB == Price_UI, "Price in the database is {0}, and in the UI is {1} for vin {2} at {3} ", Price_DB, Price_UI, vin, dealer); 
            }
            

            int Mileage_DB = Convert.ToInt32(dr.ItemArray[7]);
            int Mileage_UI = Utility.ExtractDigitsFromString(maxdigitalshowroom_page.Desktop_SearchResultsPage_FirstMileage.Text);
            Assert.IsTrue(Mileage_DB == Mileage_UI, "Mileage in the database is {0}, and in the UI is {1} for vin {2} at {3} ", Mileage_DB, Mileage_UI, vin, dealer);

            string Color_DB = Convert.ToString(dr.ItemArray[8]).ToLower();
            if (Color_DB != "")
            {
                string Color_UI = maxdigitalshowroom_page.Desktop_SearchResultsPage_FirstColor.Text.ToLower();
                Assert.IsTrue(Color_DB == Color_UI, "Color in the database is {0}, and in the UI is {1} for vin {2} at {3} ", Color_DB, Color_UI, vin, dealer);
            }

            string StockNumber_DB = Convert.ToString(dr.ItemArray[9]);
            StockNumber_DB = "Stock #: " + StockNumber_DB;
            string StockNumber_UI = maxdigitalshowroom_page.Desktop_SearchResultsPage_StockNumber.Text;
            Assert.IsTrue(StockNumber_DB == StockNumber_UI, "Stock number in the database is {0}, and in the UI is {1} for vin {2} at {3} ", StockNumber_DB, StockNumber_UI, vin);
        }

        /*

        [Ignore("Don't know root cause of blank screen, don't know how to detect yet")]
        //[TestCase(Firefox, true)]
        //[TestCase(InternetExplorer, true)]
        [TestCase(Chrome, true)]    // This problem affects Chrome
        [Category("SmokeDigitalShowroom")]
        [Description("In mobile view make sure page renders after clicking 'Back' from VDP")]
        public void ConfirmPageRerendersOnBackClick_C207xx(string browser, bool overRide)
        {
            string dealer = "Windy City Chevrolet";
            int bu = 101590;

            InitializeDriver(browser, overRide);
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer(dealer);
            WaitForAjax();

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(bu);

            string vin = Convert.ToString(dr.ItemArray[0]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();
            Thread.Sleep(500);
            maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Click();
            maxdigitalshowroom_page.WaitForAjax();
            Thread.Sleep(500);
            maxdigitalshowroom_page.Mobile_VehicleDetails_Equipment_Link.Click();
            maxdigitalshowroom_page.WaitForAjax();
            Thread.Sleep(500);
            maxdigitalshowroom_page.Mobile_VDPDrillDown_BackButton.Click();
            maxdigitalshowroom_page.WaitForAjax();

            //Assert something

            Thread.Sleep(5000);




        }

        */


        [Test]
        [Category("SmokeDigitalShowroom")]
        [Description("Verifies the logo url is correct")]
        public void DealerLogo_VehicleSearchPage_C20734()
        {
            string dealer = "Leith BMW";
            string businessunitcode = "LEITHBMW01";
            
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            CloudSearch_Page cloudSearchPage = new CloudSearch_Page(driver);
            DataRow dr = cloudSearchPage.GetDealerInfoFromDB(businessunitcode);

            string LogoUrl_DB = Convert.ToString(dr.ItemArray[17]);
            string LogoUrl_DB_Formatted = FirstlookBaseURL + "/digitalimages/" + LogoUrl_DB;

            Thread.Sleep(2000);
            
            string LogoUrl_UI = maxdigitalshowroom_page.VehicleSearchPage_DealerLogo.GetAttribute("style");
            Thread.Sleep(2000);

            String pattern = "\"";
            string[] temp = Regex.Split(LogoUrl_UI, pattern);
            string LogoUrl_UI_Formatted = temp[1];

            Assert.That(LogoUrl_DB_Formatted == LogoUrl_UI_Formatted, "Logo Url in the database is {0}, and in the UI it is {1} at {2} ", LogoUrl_DB_Formatted, LogoUrl_UI_Formatted, dealer);
        }


        [Test]
        [Category("SmokeDigitalShowroom")]
        public void DealerEmail_C20735()
        {
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer("Windy City Chevrolet");

            DataRow dr1 = maxdigitalshowroom_page.GetVinWithShowroomEnabled(101590); //Windy City Chevrolet

            string vin = Convert.ToString(dr1.ItemArray[0]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();
            maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Click();
            maxdigitalshowroom_page.WaitForAjax();
            Thread.Sleep(500);
            maxdigitalshowroom_page.MMS_SaveButton.Click();

            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var dealeremail = js.ExecuteScript(@"var email = dealerEmail; return email;");
            String email_UI = Convert.ToString(dealeremail);

            CloudSearch_Page cloudSearchPage = new CloudSearch_Page(driver);
            DataRow dr2 = cloudSearchPage.GetDealerInfoFromDB("WINDYCIT01");
            string email_DB = Convert.ToString(dr2.ItemArray[8]);

            Assert.That(email_UI == email_DB, "The dealer email in showroom is {0} and in the db it is {1}", email_UI, email_DB);
        }

        [Test] //TODO Correct false positives from poor logic and empty ValueHighlights_MMS objects - fail-to-capture text
        [Category("SmokeDigitalShowroom")] //This test may fail until FB 33431 is fixed.
        [Description("Compares the Value Highlights text(ad preview) in MMS to Cloud Search")]
        public void VehicleHighlightsText_C20736()
        {
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer("Windy City Chevrolet");

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(101590);
            string vin = Convert.ToString(dr.ItemArray[0]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Click();
            maxdigitalshowroom_page.WaitForAjax();
            maxdigitalshowroom_page.MMS_HighlightsButton.Click();

            string ValueHighlights = driver.FindElement(By.XPath("//*[@id=\"advertisement\"]/p")).Text;
            String pattern = " more";
            string[] temp = Regex.Split(ValueHighlights, pattern);
            string ValueHighlights_MMS = temp[0];
            
            CloudSearch_Page cloudSearchPage = new CloudSearch_Page(driver);

            var csQuery = new Uri(cloudSearchPage.Vehicle_Search_Base_Structured_Query + "vin:%27" + vin + "%27");
            var jsonText = cloudSearchPage.GetCloudSearchData(csQuery);
            var vehicle = JsonConvert.DeserializeObject<dynamic>(jsonText);
            string ValueHighlights_CS = vehicle.hits.hit[0].fields.ad_preview;
            
            Assert.IsTrue((ValueHighlights_CS).Contains(ValueHighlights_MMS), "Value Highlights text did not match for vin: {0}.", vin); //if MMS = "" this assert will pass
        }

        [Test]
        [Category("SmokeDigitalShowroom")]
        [Description("In desktop view, this test verifies the filters for searching used vehicles are displayed.  These are not in the mobile view")]
        //TODO We need a negative version of this test
        public void VehicleSearchFiltersUsed_C20739()
        {
            const string dealer = "Windy City Chevrolet";
            const int businessUnitId = 101590;

            var dbDealerData = new DealerData.BasicDealerData(businessUnitId);
            var ownerHandle = dbDealerData.OwnerHandle;
            var csDealerData = new DealerData.BasicCloudSearchDealerData(ownerHandle);
            var hasNew = csDealerData.NewCarsEnabled;
            
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToDesktopView();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(businessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);

            if (hasNew == 1) // If New is enabled, the New/Used DDL will appear
            {
                maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_NewUsedDDL_bud);
                maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("Used");

            }
            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();

            IWebElement[] FilterName_WebElement = {
                                                   //maxdigitalshowroom_page.Desktop_SearchResultsPage_YearsMin,
                                                   //maxdigitalshowroom_page.Desktop_SearchResultsPage_YearsMax,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_YearsRange,
                                                   //maxdigitalshowroom_page.Desktop_SearchResultsPage_MileageMin,
                                                  // maxdigitalshowroom_page.Desktop_SearchResultsPage_MileageMax,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_MileageRange,
                                                   //maxdigitalshowroom_page.Desktop_SearchResultsPage_PriceMin,
                                                   //maxdigitalshowroom_page.Desktop_SearchResultsPage_PriceMax,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_PriceRange,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_Trim,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_Color,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_Packages
                                               };

            var filterLength = FilterName_WebElement.Length;
            for (int i = 0; i < filterLength; i++)
            {
                FilterName_WebElement[i].Click();
                Assert.IsTrue(FilterName_WebElement[i].Displayed, "Missing filter is {0} at dealer {1}", FilterName_WebElement[i], dealer);
                Thread.Sleep(1000);
            }

        }

        [Test]
        [Category("SmokeDigitalShowroom")]
        [Description("In desktop view, this test verifies the filters for searching new vehicles are displayed. Not in the mobile view")]
        //TODO We need a negative version of this test
        public void VehicleSearchFiltersNew_C27707()
        {
            string dealer = "Leith BMW";
            int BusinessUnitId = 106161;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToDesktopView();
            maxdigitalshowroom_page.SearchForDealer(dealer);
            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_NewUsedDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("New");


            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId, "New");

            string vin = Convert.ToString(dr.ItemArray[0]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();

            IWebElement[] FilterName_WebElement = {
                                                   maxdigitalshowroom_page.desktop_SearchResultsPage_YearsMax_bud,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_PriceRange,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_Trim,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_Color,
                                                   maxdigitalshowroom_page.Desktop_SearchResultsPage_Packages
                                               };

            var filterLength = FilterName_WebElement.Length;
            for (int i = 0; i < filterLength; i++)
            {
                FilterName_WebElement[i].Click();
                Assert.IsTrue(FilterName_WebElement[i].Displayed, "Missing filter is {0} at dealer {1}", FilterName_WebElement[i], dealer);
                Thread.Sleep(1000);
            }

        }


        [Test] //This test may fail until FB 33462 is fixed.
        [Category("SmokeDigitalShowroom")]
        [Description("Confirms the available vehicles count updates when doing a search")]
        public void VehicleSearchAvailableCount_C20740()
        {
            string dealer = "Windy City BMW";
            int BusinessUnitId = 105239;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);

            var dbDealerData = new DealerData.BasicDealerData(BusinessUnitId);
            var ownerHandle = dbDealerData.OwnerHandle;
            var csDealerData = new DealerData.BasicCloudSearchDealerData(ownerHandle);
            var hasNew = csDealerData.NewCarsEnabled;


            if (hasNew == 1) // If New is enabled, the New/Used DDL will appear
            {

                maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_NewUsedDDL_bud);
                maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("Used");

            }

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            Thread.Sleep(3000);
            int availableCount1;
            availableCount1 = Convert.ToInt16(maxdigitalshowroom_page.Desktop_SearchResultsPage_AvailalbleVehiclesCount.Text);

            //searching for one vin should only return 1 available vehicle
            Assert.That(availableCount1 == 1, "Number of vehicles available when searching with vin {0} is {1}", vin, availableCount1);

            maxdigitalshowroom_page.Desktop_SearchResultsPage_VinStockNumSearch.Clear();
            //Thread.Sleep(3000);

            maxdigitalshowroom_page.Desktop_SearchResults_Make_DDL = new SelectPopoverElement(maxdigitalshowroom_page.searchResultsPage_MakeDDL_bud);
            maxdigitalshowroom_page.Desktop_SearchResults_Make_DDL.SelectByText("BMW");

            maxdigitalshowroom_page.Desktop_SearchResults_Model_DDL = new SelectPopoverElement(maxdigitalshowroom_page.searchResultsPage_ModelDDL_bud);
            maxdigitalshowroom_page.Desktop_SearchResults_Model_DDL.SelectByText("3 Series");
            maxdigitalshowroom_page.Desktop_SearchResultsPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();

            int availableCount2;
            availableCount2 = Convert.ToInt16(maxdigitalshowroom_page.Desktop_SearchResultsPage_AvailalbleVehiclesCount.Text);

            //searching for a BMW 3 Series at a BMW dealer should return more than 1 vehicle and verify the available count is updating
            Assert.That(availableCount2 >= 1);
        }

        [Test] 
        [Category("SmokeDigitalShowroom")]
        [Ignore] // Test needs updating to accomodate new ranged Filters
        [Description("The min and max year ddl should not be lesser or greater than the vehicle year found in the results")]
        public void YearsFilter_SearchResultsPage_C20774()
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);
            int year = Convert.ToInt16(dr.ItemArray[3]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            Thread.Sleep(2000);

            int yearMin =
                Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"years-min\"]/option[2]")));

            int yearMax =
                Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"years-max\"]/option[2]")));

            Assert.That(year == yearMin && year == yearMax, "Failed for vin {0}", vin);
        }


        [Ignore] // FB 32449 - There is a defect with the filters causing this one to fail.  
        [Test]
        [Description("The min and max mileage ddl upper and lower limit should be within 10,000 and a multiple of 10,000")]
        public void MileageFilter_SearchResultsPage_C20775()
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);
            int mileage = Convert.ToInt32(dr.ItemArray[7]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            Thread.Sleep(2000);

            int temp = mileage / 10000;
            int mileageLow = temp * 10000;
            int mileageHigh = mileageLow + 10000;

            bool multipleOfTenThousand = false;
            if(mileage % 10000 == 0)
            {
                mileageLow = mileage;
                multipleOfTenThousand = true;
            }

            int mileageMin_low = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"mileage-min\"]/option[2]")));
            int mileageMax_low = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"mileage-max\"]/option[2]")));

            Assert.That((mileageMin_low == mileageLow) && (mileageMax_low == mileageLow), "Failed for vin {0} at {1} with mileage of {2}", vin, dealer, mileage);

            if (!multipleOfTenThousand) // when mileage is an exact multiple of ten thousand, the ddl only has one value. If true, skip this assert for the second value.
            {
                int mileageMin_high = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"mileage-min\"]/option[3]")));
                int mileageMax_high = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"mileage-max\"]/option[3]")));

                Assert.That((mileageMin_high == mileageHigh) && (mileageMax_high == mileageHigh), "Failed for vin {0} at {1} with mileage of {2}", vin, dealer, mileage);
            }
        }


        [Ignore] // FB 32449 - There is a defect with the filters causing this one to fail. 
        [Test]
        [Description("The min and max price ddl upper and lower limit should be within 5,000 and a multiple of 5,000")]
        public void PriceFilter_SearchhResultsPage_C20776()
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;
            
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);
            int price = Convert.ToInt32(dr.ItemArray[2]);
            
            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            Thread.Sleep(2000);
            
            int priceLow = 0;
            int priceHigh = 0;
            int temp1 = price / 10000;
            double temp2 = (double)price / 10000;
            double diff = (temp2 - temp1);
            bool multipleOfFiveThousand = false;
            
            if (diff >= 0.0 && diff < .5)
            {
                priceLow = temp1 * 10000;
                priceHigh = priceLow + 5000; 
            }
            if(diff >= .5 && diff < 1.0)
            {
                priceLow = (temp1 * 10000) + 5000;
                priceHigh = priceLow + 5000;
            }
            if(price % 5000 == 0)
            {
                priceLow = price;
                multipleOfFiveThousand = true;
            }
            
            int priceMin_low = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"price-min\"]/option[2]")));
            int priceMax_low = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"price-max\"]/option[2]")));

            Assert.That((priceMin_low == priceLow) && (priceMax_low == priceLow), "Failed for vin {0} at {1} with a price of {2}", vin, dealer, price);

            if (!multipleOfFiveThousand) // when mileage is an exact multiple of five thousand, the ddl only has one value. If true, skip this assert for the second value.
            {
                int priceMin_high = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"price-min\"]/option[3]")));
                int priceMax_high = Utility.GetDigitsFromElement(driver.FindElement(By.XPath("//*[@id=\"price-max\"]/option[3]")));

                Assert.That((priceMin_high == priceHigh) && (priceMax_high == priceHigh), "Failed for vin {0} at {1} with a price of {2}", vin, dealer, price);
            }
        }

        [Test] //Needs updating, currently failing due to drop-down weakness.
        [Description("The trim in the ddl should match what is found in the search results")]
        public void TrimFilter_SearchhResultsPage_C20781()
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);
            string trim = Convert.ToString(dr.ItemArray[6]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            Thread.Sleep(2000);

            string trim_ddl = driver.FindElement(By.XPath("//*[@id=\"trim\"]/option[2]")).Text;

            Assert.That((trim == trim_ddl), "Trim of the vehicle is {0} and the trim in the ddl is {1} for vin {2} at {3}", trim, trim_ddl, vin, dealer);
        }

        [Test]
        [Category("SmokeDigitalShowroom")]
        [Description("Searching by make and model of a vin found in inventory should return at least one vehicle")]
        public void SearchByMakeModel_DesktopView_C20782()
        {
            string dealer = "Windy City BMW";
            int BusinessUnitId = 105239;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            var query =
                @"
                    SELECT TOP 1 i.vin,i.StockNumber,i.ListPrice,i.VehicleYear,i.Make,i.Model,i.Trim,i.MileageReceived,i.BaseColor,i.StockNumber,v.OriginalMake,v.OriginalModel
                    from  merchandising.workflow.Inventory i	
						JOIN Merchandising.postings.VehicleAdScheduling vas on vas.businessUnitID = i.BusinessUnitID and vas.inventoryId = i.InventoryID
						JOIN IMT.dbo.Vehicle v on v.VIN = i.Vin
                    WHERE i.InventoryType = 2
                    AND v.OriginalModel = '3 Series'
                    AND i.BusinessUnitID = " + BusinessUnitId + " ORDER BY vas.actualReleaseTime DESC";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            string vin = Convert.ToString(dr.ItemArray[0]);
            string make = Convert.ToString(dr.ItemArray[4]);
            string model = Convert.ToString(dr.ItemArray[11]);   // Original Model may be upper case

            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            model = myTI.ToTitleCase(model.ToLower());                    //convert if necessary to how it appears in Digital Showroom

            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_NewUsedDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("Used");
            Thread.Sleep(500);
            maxdigitalshowroom_page.VehicleSearch_Make_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_MakeDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_Make_DDL.SelectByText(make);
            Thread.Sleep(500);
            maxdigitalshowroom_page.VehicleSearch_Model_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_ModelDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_Model_DDL.SelectByText(model);
            Thread.Sleep(500);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();
            Thread.Sleep(500);
            int count = Utility.ExtractDigitsFromString(maxdigitalshowroom_page.Desktop_SearchResultsPage_AvailalbleVehiclesCount.Text);

            Assert.That((count > 0), "This vehicle, {0}, and any other BMW 3 Series vehicles, were not found at {1}", vin, dealer);
        }

        [Test]
        [Category("SmokeDigitalShowroom")]
        [Description("Verifies the make and model menu title on the search results page is the same as what was searched for")]
        public void MakeModelMenuTitleResultsPage_Mobile_View_C20808()
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            var query =
                @"
                    SELECT TOP 1 MWI.vin,MWI.StockNumber,MWI.ListPrice,MWI.VehicleYear,MWI.Make,MWI.Model,IDV.OriginalMake,IDV.OriginalModel
                    FROM IMT.dbo.Inventory IDI
                    JOIN merchandising.workflow.Inventory MWI ON MWI.InventoryID = IDI.InventoryID
                    JOIN IMT.dbo.Vehicle IDV ON IDV.vin = MWI.vin
                    WHERE MWI.InventoryType = 2
                    --AND IDV.OriginalMake = 'Chevrolet' --Why only look for Chevy?
                    -- exclude UPPER CASE results if possible
                    AND MWI.Make NOT IN (UPPER(MWI.Make) COLLATE SQL_Latin1_General_CP1_CS_AS)
                    AND MWI.Model NOT IN (UPPER(MWI.Model) COLLATE SQL_Latin1_General_CP1_CS_AS)
                    AND IDI.BusinessUnitID = " + BusinessUnitId + " ORDER BY IDI.InsertDate ASC";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];

            string vin = Convert.ToString(dr.ItemArray[0]);
            string make = Convert.ToString(dr.ItemArray[4]);
            maxdigitalshowroom_page.VehicleSearch_Make_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_MakeDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_Make_DDL.SelectByText(make);
            Thread.Sleep(500);
            maxdigitalshowroom_page.VehicleSearch_Model_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_ModelDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_Model_DDL.SelectByIndex(1);   // Grab the first model available in the DDL
            var model = maxdigitalshowroom_page.VehicleSearch_Model_DDL.SelectedOption.Text;
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();
            Thread.Sleep(500);
            string makeModel_db = make + " " + model;
            string makeModel_UI = driver.FindElement(By.CssSelector("#small-menu > div")).Text;

            Assert.That((makeModel_UI == makeModel_db), "Failed for vin {0} at {1} ", vin, dealer);

        }

        //[TestCase(Firefox, true)]
        //[TestCase(InternetExplorer, true)]
        [TestCase(Chrome, true)]
        [Description("The ddl defaults to All Makes/Models and the search button should always be enabled to search all inventory, even w/o a vin or make selected")]
        public void SearchButton_EnablesDisables_C21259(string browser, bool overRide)
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;
            InitializeDriver(browser, overRide);

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);

            for(int i = 0; i < 2; i++)  //run test in both mobile and desktop view
            {

                if (i == 0)
                    driver.Manage().Window.Size = new Size(1000, 800);
                else if (i == 1)
                    driver.Manage().Window.Size = new Size(800, 800);

                maxdigitalshowroom_page.VehicleSearch_Make_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_MakeDDL_bud);
                var currentDDLMake = maxdigitalshowroom_page.VehicleSearch_Make_DDL.SelectedOption.Text;

                maxdigitalshowroom_page.VehicleSearch_Model_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_ModelDDL_bud);
                var currentDDLModel = maxdigitalshowroom_page.VehicleSearch_Model_DDL.SelectedOption.Text;

                Assert.True(currentDDLMake == "All Makes");
                Assert.True(currentDDLModel == "All Models");

                String colorEnabled = maxdigitalshowroom_page.VehicleSearchPage_SearchButton.GetCssValue("background-color");
                Assert.That((colorEnabled == "rgba(1, 183, 14, 1)"), "The search button should always be enabled now at {0}", dealer);  //This is a new change, should always be enabled
                maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.Clear();
                maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
                Assert.That((colorEnabled == "rgba(1, 183, 14, 1)"), "The search button is not enabled after typing in a vin at {0}", dealer);
            }
        }

        [Test]
        [Description("Verifies the setion with color, mileage, engine and transmission data on a vehicle's profile page in MMS")]
        public void VehicleDetails_MMS_C21260()
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;
            
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            DataRow dr = maxdigitalshowroom_page.GetVinWithShowroomEnabled(BusinessUnitId);
            string vin = Convert.ToString(dr.ItemArray[0]);

            maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Click();
            Thread.Sleep(3000);

            string color_UI = maxdigitalshowroom_page.MMS_Mileage.Text.ToLower(); // color and mileage are in the same webelement
            string color_DB = Convert.ToString(dr.ItemArray[8]).ToLower();
            Assert.That((color_UI.Contains(color_DB)), "Test failed because of the color of vin {0} at {1}", vin, dealer);

            string mileage = maxdigitalshowroom_page.MMS_Mileage.Text;
            int mileage_UI = Utility.ExtractDigitsFromString(mileage);
            int mileage_DB = Convert.ToInt32(dr.ItemArray[7]);
            Assert.That((mileage_UI == mileage_DB), "Test failed because of the mileage of vin {0} at {1}", vin, dealer);

            string transmission_UI = maxdigitalshowroom_page.MMS_Transmission.Text;
            string transmission_DB = Convert.ToString(dr.ItemArray[10]);
            transmission_DB = transmission_DB + " Transmission";
            Assert.That((transmission_UI == transmission_DB), "Test failed because of the transmission of vin {0} at {1}", vin, dealer);

            string engine_UI = maxdigitalshowroom_page.MMS_Engine.Text;
            string engine_DB = Convert.ToString(dr.ItemArray[11]);
            Assert.That((engine_DB.Contains(engine_UI)), "Test failed because of the engine of vin {0} at {1}", vin, dealer);
        }

        [Test] //This test will fail until FB 32610 is fixed.
        [Category("SmokeDigitalShowroom")]
        [Description("Tests finds a certified and a non certified vehicle, and verifies CPOV info is displayed when appropriate")]
        public void CertifiedInfoDisplayedOnlyForCertifiedVehicles_MMS_C21262()
        {
            string dealer = "Windy City Chevrolet";
            int BusinessUnitId = 101590;

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer(dealer);

            for(int i = 0; i < 2; i++)  // loops around twice, once for a certified vehicle and once for a non certified vehicle
            {
                DataRow dr = maxdigitalshowroom_page.GetCertifiedVinWithShowroomEnabled(BusinessUnitId, i);
                string vin = Convert.ToString(dr.ItemArray[0]);
                
                maxdigitalshowroom_page.VehicleSearchPage_VinStockNumSearch.SendKeys(vin);
                maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
                WaitForAjax();
                maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Click();
                WaitForAjax();
                maxdigitalshowroom_page.MMS_HighlightsButton.Click();
                WaitForAjax();

                bool IsCertifiedIdPresent = false;
                if(i == 0)  // is not certified
                {
                    try
                    {
                        IsCertifiedIdPresent =  Convert.ToBoolean(driver.FindElement(By.XPath("//*[@id=\"certified\"]")).Displayed);
                        Assert.IsFalse(IsCertifiedIdPresent);
                    }
                    catch(Exception)
                    {
                        Assert.IsFalse(IsCertifiedIdPresent);
                    }
                }

                if (i == 1) // is certified
                {
                    try
                    {
                        IsCertifiedIdPresent = Convert.ToBoolean(driver.FindElement(By.XPath("//*[@id=\"certified\"]")).Displayed);
                        Assert.IsTrue(IsCertifiedIdPresent);
                    }
                    catch (Exception)
                    {
                        Assert.IsTrue(IsCertifiedIdPresent);
                    }
                }
                driver.Navigate().GoToUrl(MaxDigitalShowroomURL);
            }
        }


        //[TestCase(Firefox, true)]
        [TestCase(InternetExplorer, false)] // Set override false. IE is failing on new build server for some reason
        //[TestCase(Chrome, true)]
        [Description("Confirms searched-for Make & Model appears throughout application pages")]
        public void SeeMoreVehiclesButton_C22415(string browser, bool overRide)
        {
            const string dealer = "Windy City BMW";
            const string make = "BMW";

            InitializeDriver(browser, overRide);

            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.GoToMobileView();
            maxdigitalshowroom_page.SearchForDealer(dealer);
            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_NewUsedDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("Used");
            maxdigitalshowroom_page.VehicleSearch_Make_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_MakeDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_Make_DDL.SelectByText(make);
            maxdigitalshowroom_page.VehicleSearch_Model_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_ModelDDL_bud);
            var modelCount = maxdigitalshowroom_page.VehicleSearch_Model_DDL.Options.Count;

            for (var i = 1;  i < modelCount; i++)
            {

                driver.Navigate().GoToUrl(MaxDigitalShowroomURL + "web/#!/show/dealer");
                maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("Used");
                maxdigitalshowroom_page.VehicleSearch_Make_DDL.SelectByText(make);
                maxdigitalshowroom_page.VehicleSearch_Model_DDL.SelectByIndex(i);
                var model = maxdigitalshowroom_page.VehicleSearch_Model_DDL.SelectedOption.Text;
                maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
                WaitForAjax();
                var makeModel = maxdigitalshowroom_page.Mobile_SearchResultsPage_SmallMenuTitle_MakeModel.Text;

                Assert.IsTrue(makeModel.Contains(make) && makeModel.Contains(model));
                //Thread.Sleep(1000);

                var firstVehicleInResults = maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Text;
                Assert.IsTrue(firstVehicleInResults.Contains(model));

                maxdigitalshowroom_page.Mobile_SearchResultsPage_YearModel.Click();
                WaitForAjax();
                Thread.Sleep(500);
                var vehicleMakeModel = maxdigitalshowroom_page.Mobile_VehicleDescriptionPage_VehicleName.Text;
                Assert.IsTrue(vehicleMakeModel.Contains(make) && vehicleMakeModel.Contains(model));

                maxdigitalshowroom_page.Mobile_VehicleDescriptionPage_SeeMoreButton.Click();
                WaitForAjax();

                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var numOfVehicles = js.ExecuteScript(@" var arr = document.getElementsByClassName('makemodel ui-li-desc'); return arr.length;");
                int numOfVehiclesInt = Convert.ToInt16(numOfVehicles);

                for (int j = 0; j < numOfVehiclesInt; j++)
                {
                    var temp = js.ExecuteScript(@" var num = document.getElementsByClassName('makemodel ui-li-desc'); return num[" + j + "];");
                    var eachMakeModel = ((IWebElement)temp).Text;
                    Assert.IsTrue((eachMakeModel.Contains(make) && eachMakeModel.Contains(model)), "This vehicle is not a {0} {1} at {2}", make, model, dealer);
                }

            }

        }

        //[TestCase(Firefox, true)] //this test may fail until FB 33610 is fixed
        [TestCase(InternetExplorer, false)] // Set override false. IE is failing on new build server for some reason
        //[TestCase(Chrome, true)]
        [Description("Confirms that only the model year cars filtered-for appear in the results - New cars")]
        public void ConfirmYearsFilterNew_C26899(string browser, bool overRide)
        {

            const string dealer = "Leith BMW";

            InitializeDriver(browser, overRide);
            NavigateToMaxDigitalShowroom();
            maxdigitalshowroom_page.SearchForDealer(dealer);
            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL = new SelectPopoverElement(maxdigitalshowroom_page.vehicleSearch_NewUsedDDL_bud);
            maxdigitalshowroom_page.VehicleSearch_NewUsed_DDL.SelectByText("New");
            maxdigitalshowroom_page.VehicleSearchPage_SearchButton.Click();
            maxdigitalshowroom_page.WaitForAjax();
            Thread.Sleep(100);

            // Determine the maximum year available in the "New" DDL
            maxdigitalshowroom_page.desktop_SearchResultsPage_YearsMax_bud.Click(); // make DDL elements visible
            Thread.Sleep(750);
            var ddlElements = maxdigitalshowroom_page.desktop_SearchResultsPage_YearsMax_bud.FindElements(By.XPath(".//div[@class='menu-body']//div"));
            var maxYearInt = 1;
            var currentYearInt = 0;

            foreach (var ddlElement in ddlElements)
            {             
                try
                {
                    currentYearInt = Convert.ToInt32(ddlElement.Text); // Any non-numeric text will break Convert.
                }
                catch (FormatException)
                {                   
                    continue;
                }               
                if (currentYearInt > maxYearInt)
                    maxYearInt = currentYearInt;                
            }

            maxYearInt = maxYearInt -1; // Previous Year often provides bigger sample

            var maxYear = maxYearInt.ToString();
            maxdigitalshowroom_page.desktop_SearchResultsPage_YearsMax_bud.Click(); // close DDL

            // Select the max year
            maxdigitalshowroom_page.Desktop_SearchResults_Years_DDL = new SelectPopoverElement(maxdigitalshowroom_page.desktop_SearchResultsPage_YearsMax_bud);
            maxdigitalshowroom_page.Desktop_SearchResults_Years_DDL.SelectByText(maxYear);

            ScrollToPageBottom(); // Expose all available sections

            var sectionHeaderElements = driver.FindElements(By.CssSelector("div.results-section-header"));
            foreach (var sectionHeaderElement in sectionHeaderElements)
            {
                var testString = sectionHeaderElement.Text.Remove(4);
                Assert.True(testString == maxYear);

            }

            //Interesting LINQ statement substitute for above logic:
            //var maxYearInt = (from ddlElement in ddlElements let text = ddlElement.Text select Convert.ToInt32(ddlElement.Text)).Concat(new[] { 0 }).Max();


        }

    }

}
