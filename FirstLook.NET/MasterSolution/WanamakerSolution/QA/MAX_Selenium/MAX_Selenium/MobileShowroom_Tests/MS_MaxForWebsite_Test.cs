﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    class MS_MaxForWebsite_Test : Test
    {

        private static readonly string NADA_ERROR = "We are unable to value this model.";

        //source objects must be instantiated before use in any TestCaseSource()
        private static object[] MaxForWebsiteVin = { new[] {MaxForWebsiteVIN} };

        [TestCaseSource("MaxForWebsiteVin")]
        [Category("SmokeMfW")]
        public void MaxForWebsite_VinNotFound_C18671(string vin)
        {
            NavigateToMaxForWebsite(vin + "1");
            //Look for the vehicle image on the page
            Assert.True(driver.FindElement(By.CssSelector("body h1.not-found")).Text == "Vehicle Not Found");
        }

        [Test]
        [Category("SmokeMfW")]
        [Description("Make sure the Max for Website price matches List Price from database.")]
        public void Compare_MfW_Price_To_DB_C21257()
        {
            //The query selects the latest used released vehicles and *should not be modified* to exclude the very latest. It helps measure whether docs are getting published at all.
            //This query can take up to 11s on a clean buffer and cache.
            String query = @"SELECT top 5 Vin, i.ListPrice
                    FROM Merchandising.workflow.Inventory i
                    JOIN Merchandising.postings.VehicleAdScheduling vas on vas.businessUnitID = i.BusinessUnitID and vas.inventoryId = i.InventoryID
                    WHERE i.InventoryType = 2 --used
                    AND i.ListPrice > 5000 --avoid 0 ListPrice cases
                    AND vas.actualReleaseTime < DATEADD(dd, 10, GETDATE())
                    AND EXISTS (select * from IMT.dbo.DealerUpgrade du where du.DealerUpgradeCD = 23 and du.BusinessUnitID = i.BusinessUnitID) --Merchandising enabled
                    AND EXISTS (select * from Merchandising.settings.Merchandising m where m.MAXForWebsite20 = 1 and m.businessUnitID = i.BusinessUnitID) -- MfW 2.0 enabled
                    ORDER BY vas.actualReleaseTime DESC"
                ;

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            int rows = dataset.Rows.Count;
            for (int row = 0; row < rows; row ++)
            {
                DataRow datarow = dataset.Rows[row];
                var rawDbPrice = (decimal)datarow.ItemArray[1]; //unbox decimal object
                var dbPrice = Decimal.ToInt32(rawDbPrice);
                string vin = datarow.ItemArray[0].ToString().Trim();
                NavigateToMaxForWebsite(vin);
                maxForWebsite_Page.WaitForAjax();

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(".//span[@class='price ng-binding']")));

                var mfwPrice = Utility.ExtractDigitsFromString(maxForWebsite_Page.Your_Price.Text);
                Assert.That(dbPrice.Equals(mfwPrice), "The DB Price {0} does not equal the Max for Website Price {1} for VIN: {2}", dbPrice, mfwPrice, vin );
                //Thread.Sleep(2000)
            }
        }

        [Test] //Test will allow some "Vehicle not found" results, but will fail if all are.
        [Category("SmokeMfW")]
        [Description("Make sure no price gauge renders if ListPrice = 0")]
        public void No_Price_Gauge_C21258()
        {
            String query = @"SELECT top 3 Vin, i.ListPrice
                    FROM Merchandising.workflow.Inventory i
                    JOIN Merchandising.postings.VehicleAdScheduling vas on vas.businessUnitID = i.BusinessUnitID and vas.inventoryId = i.InventoryID
                    WHERE i.InventoryType = 2 --used
                    AND i.ListPrice = 0
                    AND EXISTS (select * from IMT.dbo.DealerUpgrade du where du.DealerUpgradeCD = 23 and du.BusinessUnitID = i.BusinessUnitID) --Merchandising enabled
                    AND EXISTS (select * from Merchandising.settings.Merchandising m where m.MAXForWebsite20 = 1 and m.businessUnitID = i.BusinessUnitID) -- MfW 2.0 enabled
                    ORDER BY vas.actualReleaseTime DESC -- recently released cars should have updated json files in the vehicle folder
                    ";

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            int rows = dataset.Rows.Count;
            Boolean carFound = false;
            for (int row = 0; row < rows; row++)
            {
                try
                {                 
                    DataRow datarow = dataset.Rows[row];
                    string vin = datarow.ItemArray[0].ToString().Trim();
                    NavigateToMaxForWebsite(vin);
                    maxForWebsite_Page.WaitForAjax();
                    if (!carFound) // don't test or set if at least 1 car found
                    {
                        carFound = !(driver.FindElement(By.CssSelector("body h1.not-found")).Text == "Vehicle Not Found");
                    }
                }
                catch (NoSuchElementException)
                {
                    //continue;
                }
                Assert.Throws<NoSuchElementException>(maxForWebsite_Page.Price_Gauge_Div.Click);
            }
            if (!carFound)
            {
                throw new Exception("No WS 2.0 documents found from query.");
            }
        }


        //[TestCase(Firefox, true)] 
        [TestCase(InternetExplorer, false)]  // This test will fail until FB 33993 is fixed. 8/1/2015 TM
        // Set override false. IE is failing on new build server for some reason
        //[TestCase(Chrome, true)]
        [Description("Exercises the NADA Trade-in tool for common vehicles to make sure they return data from the FL service")]
        public void ConfirmNADAWorking_C29525(string browser, bool overRide)
        {
            const string dealer = "Hendrick BMW"; // Hendrick BMW should have Book Valuations enabled - Admin/Upgrades

            var vehicleList = new List<String[]> {
                new string[] {"2013", "Chevrolet", "Silverado 1500", "Extended Cab LT 2WD", "35,000"},
                new string[] {"2012", "BMW", "328i", "Coupe 2D 328i", "40,000"},            
                new string[] {"2012", "Honda","Civic", "Sedan 4D LX", "40,000" },
                new string[] {"2014", "Ford","Focus","Sedan 4D SE I4 ", "30,000" },
                new string[] {"2014", "Chevrolet", "Cruze", "Sedan 4D LT I4 Turbo", "30,000" },
                new string[] {"2013", "Ford", "Fusion", "Sedan 4D SE I4", "35,000" },
                new string[] {"2013", "Chevrolet", "Equinox", "Utility 4D LT 2WD I4", "35,000" },
                new string[] {"2014", "Chevrolet", "Impala", "Sedan 4D LT V6", "30,000" },
                new string[] {"2012", "Toyota", "Camry", "Sedan 4D SE", "40,000" },
                new string[] {"2013", "Hyundai", "Elantra", "Sedan 4D GLS I4", "35,000" },
            };

            InitializeDriver(browser, overRide);

            // The following query gets a used VIN belonging to the test-configured dealer
            var query = String.Format(@"SELECT top 3 Vin
                                        FROM Merchandising.workflow.Inventory i
                                        JOIN IMT.dbo.BusinessUnit b ON i.BusinessUnitID = b.BusinessUnitID
                                        WHERE i.InventoryType = 2 --used
                                        AND BusinessUnit ='{0}'", dealer);

            var dataset = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            DataRow dr = dataset.Rows[0];
            string vin = dr.ItemArray[0].ToString().Trim();
            NavigateToMaxForWeb_GoodDeal(vin);
            maxForWebsite_Page.WaitForAjax();
            maxForWebsite_Page.ValueTradeIn_Link.Click();
            maxForWebsite_Page.WaitForAjax();
            // Confirm the lightbox is open
            var lightboxOpen = driver.FindElement(By.CssSelector("div[class='tradein-nada-lightbox open']")).Enabled;
            Assert.True(lightboxOpen);
            //instantiate lightbox DDLs. These can be moved to constructor if used often enough.
            maxForWebsite_Page.NADALightbox_Year_DDL = new SelectElement(maxForWebsite_Page.nadalightbox_YearDDL_bud);
            maxForWebsite_Page.NADALightbox_Make_DDL = new SelectElement(maxForWebsite_Page.nadalightbox_MakeDDL_bud);
            maxForWebsite_Page.NADALightbox_Model_DDL = new SelectElement(maxForWebsite_Page.nadalightbox_ModelDDL_bud);
            maxForWebsite_Page.NADALightbox_Trim_DDL = new SelectElement(maxForWebsite_Page.nadalightbox_TrimDDL_bud);
            maxForWebsite_Page.NADALightbox_Mileage_DDL = new SelectElement(maxForWebsite_Page.nadalightbox_MileageDDL_bud);
           // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
            var errorText = maxForWebsite_Page.TradeInError.Text;

            foreach (var s in vehicleList)
            {
                var year = s[0].Trim();
                var make = s[1].Trim();
                var model = s[2].Trim();
                var trim = s[3].Trim();
                var mileage = s[4].Trim();

                maxForWebsite_Page.NADALightbox_Year_DDL.SelectByText(year);
                maxForWebsite_Page.NADALightbox_Make_DDL.SelectByText(make);
                WaitForAjax();

                errorText = maxForWebsite_Page.TradeInError.Text;
                Assert.False(errorText.Contains(NADA_ERROR), "Got 'Unable to find' message."); // Check for error message at every entry step hereafter
                maxForWebsite_Page.NADALightbox_Model_DDL.SelectByText(model);
                WaitForAjax();
                Thread.Sleep(1000);
                errorText = maxForWebsite_Page.TradeInError.Text;
                Assert.False(errorText.Contains(NADA_ERROR), "Got 'Unable to find' message.");
                maxForWebsite_Page.NADALightbox_Trim_DDL.SelectByText(trim);
                WaitForAjax();
                Thread.Sleep(1000);
                errorText = maxForWebsite_Page.TradeInError.Text;
                Assert.False(errorText.Contains(NADA_ERROR), "Got 'Unable to find' message.");
                maxForWebsite_Page.NADALightbox_Mileage_DDL.SelectByText(mileage);
                WaitForAjax();
                Thread.Sleep(1000);
                errorText = maxForWebsite_Page.TradeInError.Text;
                Assert.False(errorText.Contains(NADA_ERROR), "Got 'Unable to find' message.");

            }

        }

    }
}
