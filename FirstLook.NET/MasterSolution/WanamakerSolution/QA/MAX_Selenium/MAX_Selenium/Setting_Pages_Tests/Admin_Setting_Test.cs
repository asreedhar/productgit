﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

// ReSharper disable UnusedMember.Local

namespace MAX_Selenium
{
    [TestFixture]
    internal class Admin_Setting_Test : Test
    {
        /* this is not currently passed to any test
        private static object[] LandRoverVehicleVIN_Used = {
                                                                     new[] {UsedLandRoverVehicleVIN}
                                                                 };

         */

        [Test]
        [Description("going through all OEM_TYPE (like BMW, Buick, Honda, etc), verifing the MAX logo on Inventory Page")]
        public void MAX_LOGO_For_OEM_Branding_C5202()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");

            foreach (OEM_Type value in Enum.GetValues(typeof (OEM_Type)))
            {
                admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
                admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
                admin_Miscellaneous_Page.Select_Save_OEM(value);
                inventoryPage = admin_Home_Page.GoBackTo_Inventory_Page();
                var bg = inventoryPage.MAX_Logo.GetCssValue("background-image").ToLower(); // IE captures css as lowercase
                string imageString = string.Format("images/logo_max_{0}.png", value).ToLower();
                Assert.That(bg.Contains(imageString));
            }
        }

        [Test]
        public void Send_Optimal_Format_Field_C1741()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            //admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            Assert.AreEqual("Send Optimal Format", driver.FindElement(By.CssSelector("#settings > div:nth-child(5) > label")).Text);
        }

        /*   FB 31268 - Remove manual autoload buttons
        [Test]
        public void Autoload_Settings_C6087()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            admin_autoload_settings_page = admin_Home_Page.Get_Admin_Autoload_Setting_Page();
            admin_autoload_settings_page.Check_BMW_GM_Chrysler_Ford_Mazda_VW();
            admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
            admin_Miscellaneous_Page.UnCheck_Batch_Autoload();
            inventoryPage = admin_Home_Page.GoBackTo_Inventory_Page();
            inventoryPage.SearchVehicle("Chevrolet");
            Assert.AreEqual("Auto Load", driver.FindElement(By.CssSelector("li.autoLoad")).Text);
            inventoryPage.SearchVehicle("BMW");
            Assert.AreEqual("Auto Load", driver.FindElement(By.CssSelector("li.autoLoad")).Text);
            inventoryPage.SearchVehicle("Mazda");
            Assert.AreEqual("", driver.FindElement(By.CssSelector("li.autoLoad")).Text);
            inventoryPage.SearchVehicle("Volkswagen");
            Assert.AreEqual("", driver.FindElement(By.CssSelector("li.autoLoad")).Text);
            inventoryPage.SearchVehicle("Chrysler");
            Assert.AreEqual("", driver.FindElement(By.CssSelector("li.autoLoad")).Text);
        }*/

        [Test]
        [Ignore] //Needs revision to sync with new CPO functionality
        public void LandRover_CPO_C2356()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_page = inventoryPage.GoTo_MAXsettings_Page();
            settings_maxsettings_certified_page = settings_maxsettings_page.Get_Certified_Page();
            Assert.That(settings_maxsettings_certified_page.Configure_Maker_DDL.Text.Contains("LandRover"));
            Assert.That(Assert_If_Item_In_DDL(settings_maxsettings_certified_page.Configure_Maker_DDL, "LandRover") == true);
        }

        [Test]
        public void Autotrader_CarDotCom_Legal_Credential_Disclaimer_C5204()
        {
            InitializePageAndLogin_InventoryPage();
            setup_wizard_page = inventoryPage.GoTo_Setup_Wizard_Page();
            setup_wizard_page.Data_In_Out_Setup_Button.Click();

            if (IsWebElementDisplayed(setup_wizard_page.StartCollections))
            {
                setup_wizard_page.StartCollections.Click();
            }

            setup_wizard_page.Select_Source_Name("AutoTrader.com");
            Assert.That(setup_wizard_page.Source_Credential_Disclaimer.Text.Equals("I am an authorized AutoTrader.com user with a valid username and password. I would like to automate the retrieval of my dealership’s online performance data daily from AutoTrader.com for online inventory performance analysis."));
            setup_wizard_page.Select_Source_Name("Cars.com");
            Assert.That(setup_wizard_page.Source_Credential_Disclaimer.Text.Equals("I am an authorized Cars.com user with a valid username and password. I would like to automate the retrieval of my dealership’s online performance data daily from Cars.com for online inventory performance analysis."));
        }

        [Test]
        [Description("Verifies Max For Website 2.0 checkbox setting with the database")]
        public void MaxForWebsite20_EnablesDisables_C18235()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            int bu = 105239;

            InventoryPage inventorypage = new InventoryPage(driver);
            inventorypage.GoTo_Admin_Home_Page();
            Thread.Sleep(4000);
            Admin_Home_Page admin_home_page = new Admin_Home_Page(driver);
            admin_home_page.Upgrades_Tab.Click();


            if (admin_home_page.Max_For_Website20_Checkbox_on_Upgrades_Tab.Selected)
            {
                Assert.IsTrue(MaxForWebsite20_DB(bu));

                admin_home_page.Max_For_Website20_Checkbox_on_Upgrades_Tab.Click();
                admin_home_page.Save_Button_Upgrades_Tab.Click();
                Thread.Sleep(4000);

                Assert.IsFalse(MaxForWebsite20_DB(bu));

            }

            else
            {
                Assert.IsFalse(MaxForWebsite20_DB(bu));

                admin_home_page.Max_For_Website20_Checkbox_on_Upgrades_Tab.Click();
                admin_home_page.Save_Button_Upgrades_Tab.Click();
                Thread.Sleep(4000);

                Assert.IsTrue(MaxForWebsite20_DB(bu));
            }

        }

        public bool MaxForWebsite20_DB(int bu)
        {

            var AdminSettings = DataFinder.ExecQuery(DataFinder.Merchandising,
                  @"
                    SELECT MAXForWebsite20
                    FROM Merchandising.Settings.Merchandising
                    WHERE BusinessUnitID = " + bu + " ")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { MAXForWebsite20 = (Boolean)dr["MAXForWebsite20"] })
                .First();

            return AdminSettings.MAXForWebsite20;
        }


        //[TestCase(Firefox, true)]
        [TestCase(InternetExplorer, false)] // Set override false. IE is failing on new build server for some reason
        //[TestCase(Chrome, true)]
        [Description("Verifies Max For SmartPhone checkbox setting with the database")]
        public void MaxForSmartPhone_EnablesDisables_C18236(string browser, bool overRide)
        {
            int bu = 105239;
            InitializeDriver(browser, overRide);

            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");
            admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
            inventoryPage.WaitForAjax();
            admin_Home_Page.Upgrades_Tab.Click();
            WaitForAjax();
            admin_Upgrades_Page = new Admin_Upgrades_Page(driver);  


            if (admin_Upgrades_Page.MaxForSmartPhone_Checkbox.Selected)
            {
                Assert.IsTrue(MaxForSmartPhone_DB(bu));
                admin_Upgrades_Page.MaxForSmartPhone_Checkbox.Click(); // deselect
                admin_Upgrades_Page.AutoBatchTemplate_DDL.SelectByIndex(0);
                admin_Upgrades_Page.Save_Button.Click();
                admin_Upgrades_Page.WaitForAjax();
                Assert.IsFalse(MaxForSmartPhone_DB(bu));

                //before exiting test, leaves a template selected for other tests.
                admin_Upgrades_Page.MaxForSmartPhone_Checkbox.Click();
                admin_Upgrades_Page.AutoBatchTemplate_DDL.SelectByIndex(2);
                admin_Upgrades_Page.Save_Button.Click();
                admin_Upgrades_Page.WaitForAjax();

            }

            else
            {
                Assert.IsFalse(MaxForSmartPhone_DB(bu));
                admin_Upgrades_Page.MaxForSmartPhone_Checkbox.Click();
                admin_Upgrades_Page.AutoBatchTemplate_DDL.SelectByIndex(1);
                admin_Upgrades_Page.Save_Button.Click();
                admin_Upgrades_Page.WaitForAjax();
                Assert.IsTrue(MaxForSmartPhone_DB(bu));
            }

        }

        public bool MaxForSmartPhone_DB(int bu)
        {

            var AdminSettings = DataFinder.ExecQuery(DataFinder.Merchandising,
                  @"
                    SELECT MAXForSmartPhone
                    FROM Merchandising.Settings.Merchandising
                    WHERE BusinessUnitID = " + bu + " ")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { MAXForSmartPhone = (Boolean)dr["MAXForSmartPhone"] })
                .First();

            return AdminSettings.MAXForSmartPhone;
        }

        [Test]
        [Description("This test makes sure the Batch Autoload checkbox is enabled (not greyed out) when a user enters new credentials")]
        public void Missing_BatchAutoload_Creds_C6088()
        {
            //the following query locates dealers who have MAX, but no AutoLoad credentials.
            string query =
                @"SELECT DISTINCT top 5 bu.BusinessUnit
                FROM IMT.dbo.DealerUpgrade du
                JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = du.BusinessUnitID
                JOIN Merchandising.settings.merchandising sm on sm.BusinessUnitID = bu.BusinessUnitID
                --JOIN Merchandising.settings.AutoApprove sa on sa.BusinessUnitID = bu.BusinessUnitID --optional
                WHERE du.DealerUpgradeCD = 24 AND du.Active = 1 AND bu.Active = 1 AND sm.batchAutoload = 0
               -- is this necessary?  AND (bu.BusinessUnit LIKE '%BMW%' OR bu.BusinessUnit LIKE '%Chevrolet%' or bu.BusinessUnit LIKE '%Mazda%')
                AND bu.businessUnitID NOT IN (Select businessunitid from Merchandising.settings.Dealer_SiteCredentials)";
            var dataTable = DataFinder.ExecQuery(DataFinder.Merchandising, query);
            WaitForDatabase(dataTable);

            if (dataTable.Rows.Count > 0)
            {
                DataRow dr = dataTable.Rows[0];
                string dealer_name = dr.ItemArray[0].ToString().Trim();
                InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", dealer_name);
                admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
                admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();
                Assert.False(admin_Miscellaneous_Page.Batch_Autoload_Checkbox.Enabled);
                inventoryPage = admin_Miscellaneous_Page.GoBackTo_Inventory_Page();
                settings_maxsettings_autoload_page = inventoryPage.GoTo_MAXSettings_AutoLoad_Tab();      
                Enter_DealerSpeed_Credentials(Phony_user, Phony_password);  //always sets BMW creds to enable Batch checkbox.
                admin_Home_Page = inventoryPage.GoTo_Admin_Home_Page();
                admin_Miscellaneous_Page = admin_Home_Page.Get_Admin_Miscellaneous_Page();

            }
            Assert.True(admin_Miscellaneous_Page.Batch_Autoload_Checkbox.Enabled);
        }

        


    }
}