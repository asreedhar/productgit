﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MAX_Selenium.Setting_Pages;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MAX_Selenium
{
    [TestFixture]
    class Settings_MAXsettings_Certified_PageTest: Test
    {
        [Test]
        [Description("Ensure Manufacturer Programs Management link redirects user to FL Manufacturer Certified Program page")]
        public void Link_To_Manufacturer_CPO_Setting_C18245()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_certified_page = inventoryPage.GoTo_MAXSettings_Certified_Tab();
            settings_maxsettings_certified_page.Manufacturer_Link.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            try
            {
                wait.Until((d) => driver.WindowHandles.Count > 1);
            }
            catch (WebDriverTimeoutException)
            {
                throw new Exception("No new window detected");
            }

            driver.SwitchTo().Window(driver.WindowHandles[1]);
            Firstlook_Certified_Program_Page firstlook_certified_program_page = new Firstlook_Certified_Program_Page(driver);
            Assert.AreEqual(firstlook_certified_program_page.CPO_Form_Name.Text, "Manufacturer Certified Program Participation");
        }

        [Test]
        [Description("Ensure Dealer Programs Management link redirects user to FL Dealer/Group Certified Program page")]
        public void Link_To_Dealer_CPO_Setting_C18254()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City Pontiac Buick GMC");
            settings_maxsettings_certified_page = inventoryPage.GoTo_MAXSettings_Certified_Tab();
            settings_maxsettings_certified_page.Dealer_Link.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            try
            {
                wait.Until((d) => driver.WindowHandles.Count > 1);
            }
            catch (WebDriverTimeoutException)
            {
                throw new Exception("No new window detected");
            }
            
            driver.SwitchTo().Window(driver.WindowHandles[1]);
            Firstlook_Certified_Program_Page firstlook_certified_program_page = new Firstlook_Certified_Program_Page(driver);
            Assert.AreEqual(firstlook_certified_program_page.CPO_Form_Name.Text, "Dealer/Group Certified Programs");
        }
    }
}
