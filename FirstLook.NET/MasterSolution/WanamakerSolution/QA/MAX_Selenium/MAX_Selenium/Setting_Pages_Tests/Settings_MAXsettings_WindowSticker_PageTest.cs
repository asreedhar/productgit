﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace MAX_Selenium
{
    [TestFixture]
    internal class Settings_MAXsettings_WindowSticker_PageTest : Test
    {

        [Test]
        [Description("Test finds the most recent window sticker PDF batch file in the database and in Max Settings, and verifies they are equal")]
        public void PDF_batch_file_Link_C18266()
        {
            InitializePageAndLogin_Inventory_Direct("QABlueMoon", "N@d@123", "Windy City BMW");  //store must have previous PDF batches created
            
            InventoryPage inventorypage = new InventoryPage(driver);
            inventorypage.GoTo_MAXsettings_Page();
            Settings_MAXsettings_Page maxSettings = new Settings_MAXsettings_Page(driver);
            maxSettings.WindowSticker_Tab.Click();

            Thread.Sleep(3000);
            
            var PDF = DataFinder.ExecQuery(DataFinder.Merchandising,
                                                           @"
                    SELECT PrintBatchId
                    FROM IMT.windowsticker.PrintBatch
                    WHERE PrintBatchId = (SELECT MAX(PrintBatchId) 
					FROM IMT.windowsticker.PrintBatch
				    WHERE businessunitid = 105239) 
                    ")
                .Rows
                .Cast<DataRow>()
                .Select(dr => new { MostRecentPDF_File_DB = (int)dr["PrintBatchId"] })
                .First();

            string printPdfBatchId_text = driver.FindElement(By.Id("HyperLink1")).GetAttribute("href");
            
            int MostRecentPDF_File_UI = Utility.ExtractDigitsFromString(printPdfBatchId_text);
            
            Assert.That(PDF.MostRecentPDF_File_DB, Is.EqualTo(MostRecentPDF_File_UI),
                        "most recent PDF in the database is {0} and in Max Settings it is {1}", PDF.MostRecentPDF_File_DB, MostRecentPDF_File_UI);

        }

    }
}
