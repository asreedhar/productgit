param(
	[string] $environment = "Prod"
)

if((get-module TestHarness) -ne $null)
{
	remove-module TestHarness
}

$basePath = split-path $MyInvocation.MyCommand.Path
import-module (join-path $basePath TestHarness.psm1)

try
{
	Initialize-Testharness $basePath $environment

	Start-Tests
}
catch
{
	throw $_
}