Add-Type -Language CSharpVersion3 @'
	public class UrlTestResult
	{
		public bool Passed { get; set; }
		public string TestName { get; set; }
		public string Message { get; set; }
		public string Url { get; set; }
		
		public UrlTestResult(string testName, string url, bool passed, string msg)
		{
			TestName = testName;
			Url = url;
			Passed = passed;
			Message = msg;
		}
		
		public override string ToString()
		{
			return string.Format("{0}: Url='{1}' Test='{2}' Msg='{3}'", 
				Passed ? "Passed" : "Failed", 
				Url, TestName, Message);
		}
	}
	
	public class TestDef
	{
		public string Name { get; set; }
		public string Url { get; set; }
		public object Test { get; set; }
		
		public TestDef(string name, string url, object test)
		{
			Name = name;
			Url = url;
			Test = test;
		}
	}
'@

function Initialize-TestHarness($basePath, $environment)
{
	Reset-TestHarness
	
	$global:path = $basePath
	Invoke-EnvScript $environment
	Read-Tests
}

function Reset-TestHarness
{
	$global:settings = @{ }
	$global:path = ""
	$global:tests = new-object System.Collections.ArrayList
}

function Invoke-EnvScript([string] $environment)
{
	$pathToEnv = join-path $path "Environments\$environment.ps1"
	& $pathToEnv
}

function Read-Tests
{
	$testPaths = get-childitem (join-path $path Tests\*.ps1) | sort Name
	
	foreach($testPath in $testPaths)
	{
		& $testPath
	}
}

function Set-BaseUrls([string] $class, [string[]] $urls)
{
	Set-TestSetting ($class + "Urls") $urls
}

function Get-BaseUrls([string] $class)
{
	Get-TestSetting ($class + "Urls")
}

function Set-TestSetting([string] $key, $value)
{
	$global:settings.$key = $value
}

function Get-TestSetting([string] $key)
{
	$global:settings.$key
}

function Add-Test([string] $name, [string] $url, [scriptblock] $test)
{
	$def = new-object TestDef $name, $url, $test
	$global:tests.Add($def) | out-null
}

function Start-Tests
{
	foreach($test in $global:tests)
	{
		$testDef = $test
		try
		{
			& $testDef.Test
		}
		catch
		{
			new-object UrlTestResult $testDef.Name, $testDef.Url, $false, "Test Failed: $_"
		}
	}
}

function Test-ResponseHeader($url,  $header, [scriptblock] $testHeader, $errorMessage)
{
		# [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
		# $web = new-object System.Net.WebClient
		# $data = $web.DownloadData($url)
        
        # $web = new-object -com "WinHttp.WinHttpRequest.5.1"
        # $web.SetTimeouts( 1*60*1000, 1*60*1000, 1*60*1000, 1*60*1000 )
        # $web.Open( "GET", $url, $false )
        # $web.Send()
        # $data = $web.ResponseText

		# $headerVal = $web.ResponseHeaders[$header]
		# $passed = & $testHeader $headerVal
		# $msg = if($passed) { "" } else { $errorMessage }
		# New-Object UrlTestResult $testDef.Name, $url, $passed, $msg

        $web = new-object -com "Microsoft.XMLHTTP"
        $web.Open( "GET", $url, $false )
        $web.Send()
        $data = $web.ResponseText
        
		$passed = $web.status -lt 400
		$msg = if( $passed ) { "" } else { $errorMessage }
		New-Object UrlTestResult $testDef.Name, $url, $passed, $msg
}

$global:settings = @{ }
$global:path = ""
$global:tests = new-object System.Collections.ArrayList