$urls = "/app_themes/max60/images/sprite_ApprovalPage.png",
	"/App_Themes/MAX60/Home.debug.css",
	"/Public/Scripts/Workflow/Home.Combined.js",
	"/Public/Media/WindowStickers/AllStar.css"

$tests = @{
	"Empty-Etag" =
	{
		Test-ResponseHeader $url 'ETag' `
			{ param($val) $val -match '^\"\"$' } `
			"ETag must be enclosed in double quotes and be empty."
	};
	"Cache-Control-Max-Age" =
	{
		Test-ResponseHeader $url 'Cache-Control' `
			{ param($val) $val -match '^max-age=(\d+)$' -and ([int]$matches[1]) -ge 3600 } `
			"Cache-Control must be set to 'max-age={number}' and {number} must be greater than 3600."
	};
	"Last-Modified-Exists" =
	{
		Test-ResponseHeader $url 'Last-Modified' `
			{ param($val) $val -match '.+' } `
			"Last-Modified header must be set."
	};
	"Date-Exists" =
	{
		Test-ResponseHeader $url 'Date' `
			{ param($val) $val -match '.+' } `
			"Date header must be set."
	};
}

$baseUrls = Get-BaseUrls merchandising

foreach($test in $tests.GetEnumerator() | Sort Key)
{
	foreach($baseUrl in $baseUrls)
	{
		foreach($urlPart in $urls)
		{
			$url = $baseUrl + $urlPart
			Add-Test $test.Key $url $test.Value.GetNewClosure()
		}
	}
}