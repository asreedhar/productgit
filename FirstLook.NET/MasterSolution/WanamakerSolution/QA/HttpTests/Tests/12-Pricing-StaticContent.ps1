$urls = "/Public/Scripts/Pages/IPA.js",
	"/App_Themes/Leopard/Pricing.css",
	"/App_Themes/Leopard/Images/IPA/inventory_facts_bg.gif"

$tests = @{
	"Empty-Etag" =
	{
		Test-ResponseHeader $url 'ETag' `
			{ param($val) $val -match '^\"\"$' } `
			"ETag must be enclosed in double quotes and be empty."
	};
	"Cache-Control-Max-Age" =
	{
		Test-ResponseHeader $url 'Cache-Control' `
			{ param($val) $val -match '^max-age=(\d+)$' -and ([int]$matches[1]) -ge 3600 } `
			"Cache-Control must be set to 'max-age={number}' and {number} must be greater than 3600."
	};
	"Last-Modified-Exists" =
	{
		Test-ResponseHeader $url 'Last-Modified' `
			{ param($val) $val -match '.+' } `
			"Last-Modified header must be set."
	};
	"Date-Exists" =
	{
		Test-ResponseHeader $url 'Date' `
			{ param($val) $val -match '.+' } `
			"Date header must be set."
	};
}

$baseUrls = Get-BaseUrls pricing

foreach($test in $tests.GetEnumerator() | Sort Key)
{
	foreach($baseUrl in $baseUrls)
	{
		foreach($urlPart in $urls)
		{
			$url = $baseUrl + $urlPart
			Add-Test $test.Key $url $test.Value.GetNewClosure()
		}
	}
}