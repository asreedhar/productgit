$tests = @{
	"Prince-Health" =
	{
		$web = new-object System.Net.WebClient
		$data = $web.DownloadString($url)
		$doc = [xml] $data
		
		$passed = $doc.Definitions -ne $null
		$msg = if($passed) { "" } else { "Failed to find expected elements on PrinceXmlWebService WSDL." }
		New-Object UrlTestResult $test.Key, $url, $passed, $msg
	};
}
	
$baseUrls = Get-BaseUrls prince

foreach($test in $tests.GetEnumerator() | Sort Key)
{
	foreach($baseUrl in $baseUrls)
	{
		$url = $baseUrl + "?wsdl"
		Add-Test $test.Key $url $test.Value.GetNewClosure()
	}
}
