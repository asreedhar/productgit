Set-BaseUrls merchandising `
    http://2k12beta-web01.firstlook.biz/merchandising,
    http://2k12beta-web02.firstlook.biz/merchandising,
	https://betamax.firstlook.biz/merchandising

Set-BaseUrls pricing `
    http://2k12beta-web01.firstlook.biz/pricing,
    http://2k12beta-web02.firstlook.biz/pricing,
	https://betamax.firstlook.biz/pricing

Set-BaseUrls resources `
    http://2k12beta-web01.firstlook.biz/resources,
    http://2k12beta-web02.firstlook.biz/resources,
	https://beta.firstlook.biz/resources

Set-BaseUrls prince `
	http://betaweb01/firstlook-queue-ws/services/PrinceXmlWebService,
	http://betaweb02/firstlook-queue-ws/services/PrinceXmlWebService,
	https://betamax.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService