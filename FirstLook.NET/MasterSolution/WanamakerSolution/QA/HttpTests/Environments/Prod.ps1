Set-BaseUrls merchandising `
	http://2K3EWEB-WANA01X.firstlook.biz/merchandising,  #Going away in 23.0
	http://2K3EWEB-WANA02X.firstlook.biz/merchandising,  #Going away in 23.0
    http://2k12prod-web01.firstlook.biz/merchandising,
    http://2k12prod-web02.firstlook.biz/merchandising,
	http://2k12prod-web03.firstlook.biz/merchandising,
	https://max.firstlook.biz/merchandising
	
Set-BaseUrls pricing `
    http://2k12prod-web01.firstlook.biz/pricing,
    http://2k12prod-web02.firstlook.biz/pricing,
	http://2k12prod-web03.firstlook.biz/pricing,
	https://max.firstlook.biz/pricing

Set-BaseUrls resources `
    http://2k12prod-web01.firstlook.biz/resources,
    http://2k12prod-web02.firstlook.biz/resources,
	http://2k12prod-web03.firstlook.biz/resources,
	https://max.firstlook.biz/resources

Set-BaseUrls prince `
	http://prodweb07.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService,
	http://prodweb08.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService,
	https://max.firstlook.biz/firstlook-queue-ws/services/PrinceXmlWebService
	