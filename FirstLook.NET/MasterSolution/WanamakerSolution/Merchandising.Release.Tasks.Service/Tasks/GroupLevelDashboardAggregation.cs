﻿using System;
using System.Linq;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using MAX.Entities;
using Merchandising.Messages;
using Merchandising.Messages.GroupLevel;

namespace Merchandising.Release.Tasks.Service.Tasks
{
    internal class GroupLevelDashboardAggregation : TaskRunner<GroupLevelAggregationMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly VehicleType[]  Types = new [] { VehicleType.Both, VehicleType.New, VehicleType.Used };
        private static readonly GoogleAnalyticsType[] GoogleAnalyticsTypes = EnumHelper.GetValues<GoogleAnalyticsType>().ToArray();

        private IGroupDashboardManager _dashboardManager;

        public GroupLevelDashboardAggregation(IQueueFactory queueFactory, IGroupDashboardManager dashboardManager)
            : base(queueFactory.CreateGroupLevelAggregation())
        {
            _dashboardManager = dashboardManager;

            ReadQueue.DeadLetterErrorLimit = 3;
        }

        protected override AbstractDelayCalculator CreateDelayCalculator()
        {
            //we don't want to try every 10 minutes here because this message can be sent from UI and 
            //we want to error out quicker then expotential backoff.
            return new ConstantDelayCalculator(TimeSpan.FromMinutes(5));
        }

        private void GenerateGroupDashboardData(int groupId, Action<float> progress)
        {
            //for progress counting
            int iterationCount = 0;

            foreach (var type in Types)
            {
                _dashboardManager.GenerateDashboardData(groupId, type,
                                                        (percent) =>
                                                        progress((percent/Types.Length) +
                                                                 ((1/(float) Types.Length)*iterationCount)));
                
                iterationCount++;
            }
        }

        private void GenerateSitePerformanceData(int groupId, Action<float> progress)
        {
            const int monthsInPast = 13;
            var currentDate = DateTime.Now;

            //for progress counting
            int iterationCount = 0;
            int iterationTotal = Types.Length * monthsInPast;
            float iterationsComplete = 1 / (float)(iterationTotal);

            foreach (var type in Types)
            {
                var minDate = currentDate.AddMonths(-(monthsInPast - 1));

                while (minDate <= currentDate)
                { 
                    int month = minDate.Month;
                    int year = minDate.Year;

                    _dashboardManager.GenerateSitePerformance(groupId, type, month, year,
                                                              (percent) =>
                                                              progress((percent/iterationTotal) +
                                                                       (iterationsComplete*iterationCount)));

                    minDate = minDate.AddMonths(1);
                    iterationCount++;
                }
            }
        }


        private void GenerateGoogleAnalyticsData(int groupId, Action<float> progress)
        {
            const int monthsInPast = 13;
            var currentDate = DateTime.Now;

            //for progress counting
            int iterationCount = 0;
            int iterationTotal = GoogleAnalyticsTypes.Length * monthsInPast;

            float iterationsComplete = 1 / (float)(iterationTotal);

            foreach (var type in GoogleAnalyticsTypes)
            {
                var minDate = currentDate.AddMonths(-(monthsInPast - 1));

                while (minDate <= currentDate)
                {
                    int month = minDate.Month;
                    int year = minDate.Year;
                    
                    _dashboardManager.GenerateAllGoogleAnalyticsData(groupId, type, DateRange.MonthOfDate(new DateTime(year, month, 1)),
                        (percent) =>
                            progress((percent / iterationTotal) +
                                    (iterationsComplete * iterationCount)));

                    minDate = minDate.AddMonths(1);
                    iterationCount++;
                }
            }
        }

        public override void Process(GroupLevelAggregationMessage message)
        {
            var status = _dashboardManager.FetchCachedStatus(message.GroupId);
            if (status.State != State.Processing) //this is a problem if the code
            {
                try
                {
                    _dashboardManager.SaveCachedState(message.GroupId, State.Processing);

                    //Catch 1 error before the message goes on the dead letter
                    if (message.Errors.Length >= ReadQueue.DeadLetterErrorLimit - 1)
                    {
                        _dashboardManager.SaveCachedState(message.GroupId, State.Failed);

                        //requeue one more time so it gets put on the deadletter
                        RequeueWithError(message, message.Errors.Last());
                        return;
                    }

                    int updateCount = 0;

                    
                    GenerateGroupDashboardData(message.GroupId,
                        (percent) =>
                        {
                            if ((updateCount % 5) == 0) //don't update progress too often to s3, kills perf 
                                _dashboardManager.SaveCachedState(message.GroupId, percent / 3);

                            updateCount++;
                        });

                    GenerateSitePerformanceData(message.GroupId,
                        (percent) =>
                        {
                            if ((updateCount % 10) == 0) //don't update progress too often to s3, kills perf 
                                _dashboardManager.SaveCachedState(message.GroupId, (percent / 3) + .33f);

                            updateCount++;
                        });

                    GenerateGoogleAnalyticsData(message.GroupId,
                        (percent) =>
                        {
                            if ((updateCount % 5) == 0) //don't update progress too often to s3, kills perf 
                                _dashboardManager.SaveCachedState(message.GroupId, (percent / 3) + .66f);

                            updateCount++;
                        });

                    _dashboardManager.SaveCachedState(message.GroupId, State.Cached);
                }
                catch (Exception e)
                {
                    //We want to swallow all errors because we need to change the state to failed.
                    _dashboardManager.SaveCachedState(message.GroupId, State.Failed);
                    throw;
                }
            }
        }
    }
}
