﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using AmazonServices;
using Autofac;
using AutoLoad;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.IOC;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.LotIntegration.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.BionicReports;
using MAX.BionicReports.ThreadStarter;
using MAX.BuildRequest;
using MAX.Caching;
using MAX.ExternalCredentials;
using log4net;
using FirstLook.DomainModel.Oltp;
using MAX.Threading;
using MAX.TimeToMarket;
using MAX.TimeToMarket.ThreadStarter;
using MerchandisingRegistry;
using FirstLook.Common.PhotoServices;
using Merchandising.Messages;
using ICache = FirstLook.Common.Core.ICache;

namespace Merchandising.Release.Tasks.Service
{
    static class Program
    {
        private static IContainer _container;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            SetupAutofac();
            SetupLogging();

            //This can be called in "console mode" from the debugger without having to run a service by passing -console
            if (args.Any(s => StringComparer.InvariantCultureIgnoreCase.Compare(s, "-console") == 0))
                ExecuteConsoleMode();
            else
                ExecuteServiceMode();
        }

        private static void ExecuteServiceMode()
        {
            //Here we create the Service through the container to resolve its dependencies
            var service = _container.Resolve<Service>();
            // Set the current directory to the app directory, so relative paths work for services.
            // http://stackoverflow.com/a/2714363
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            ServiceBase.Run(service);
        }

        //Used primarly for testing in console mode. Pass -console as an argument in Visual Studio project properties
        private static void ExecuteConsoleMode()
        {
            var threads = new List<Thread>();

            var shutdownEvent = new ManualResetEvent(false);
            var blockWhenNoMessages = new ManualResetEvent(false);

            var merchThreadStarter = _container.Resolve<IMerchandisingTaskThreadStarter>();
            var merchThread = NamedBackgroundThread.Start("Merchandising threads", () =>
                merchThreadStarter.Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));
            threads.Add(merchThread);

            var alThreadStarter = _container.Resolve<IAutoLoadThreadStarter>();
            var alThread = NamedBackgroundThread.Start("Autoload threads", () =>
                alThreadStarter.Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));
            threads.Add(alThread);

            var dashboardThreadStarter = _container.Resolve<IDashboardThreadStarter>();
            var dashThread = NamedBackgroundThread.Start("Dashboard thread", () =>
                dashboardThreadStarter.Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));
            threads.Add(dashThread);

            var fordThreadStarter = _container.Resolve<IFordDirectAutoLoadThreadStarter>();
            var fordThread = NamedBackgroundThread.Start("Ford direct autoload thread", () =>
                fordThreadStarter.Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));
            threads.Add(fordThread);

            var ttmThread = NamedBackgroundThread.Start("TimeToMarket threads", () =>
                _container.Resolve<ITimeToMarketTaskThreadStarter>().Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));
            threads.Add(ttmThread);

            var bionicThread = NamedBackgroundThread.Start("BionicReport threads", () =>
                _container.Resolve<IBionicThreadStarter>().Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));
            threads.Add(bionicThread);

            Console.WriteLine("Press enter to quit.");
            Console.ReadLine();

            // signal cancellation and then wait for threads
            shutdownEvent.Set();
            blockWhenNoMessages.Set();

            foreach(var thread in threads)
            {
                if (!thread.Join(30000))
                {
                    Log.ErrorFormat("Error joining thread '{0}'", thread.Name );
                    thread.Abort();
                }                
            }
        }

        internal static void SetupLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        internal static void SetupAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());

            builder.RegisterModule(new MerchandisingDomainModule());
            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterModule(new WebAppModule());
            builder.RegisterModule(new AmazonServicesModule());
            builder.RegisterModule(new LotIntegrationServiceModule());
            builder.RegisterModule(new ExternalCredentialsModule());
			builder.RegisterModule(new BionicReportsModule());

            builder.RegisterModule(new AutoLoadModule());
			builder.RegisterModule(new TimeToMarketModule());

            builder.RegisterModule(new BuildRequestModule());

            builder.RegisterType<DotnetMemoryCacheWrapper>().As<ICache>();

            builder.RegisterType<Service>().AsSelf();
            builder.RegisterType<DashboardThreadStarter>().As<IDashboardThreadStarter>();

	        builder.RegisterType<MemcachedClientWrapper>().As<ICacheWrapper>();
	        builder.RegisterType<CacheKeyBuilder>().As<ICacheKeyBuilder>();

            

			// Cache results. Use the regular InventoryDataRepo
	        builder.Register(ctx => new NewCarPricingRepository(
				ctx.Resolve<ICacheKeyBuilder>(),
				ctx.Resolve<ICacheWrapper>()
			));

            _container = builder.Build();
            Registry.RegisterContainer(_container);
        }
    }
}
