﻿using AutoLoad;
using FirstLook.LotIntegration.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel.Tasks;
using MAX.BionicReports.ThreadStarter;
using MAX.TaskFramework;
using MAX.TimeToMarket.ThreadStarter;

namespace Merchandising.Release.Tasks.Service
{
    public partial class Service : TaskServiceBase
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IMerchandisingTaskThreadStarter _merchandisingTaskThreadStarter;
        private readonly IAutoLoadThreadStarter _autoLoadThreadStarter;
        private readonly IDashboardThreadStarter _dashboardThreadStarter;
        private readonly IFordDirectAutoLoadThreadStarter _fordDirectAutoLoadThreadStarter;
	    private readonly ITimeToMarketTaskThreadStarter _timeToMarketTaskThreadStarter;
	    private readonly IBionicThreadStarter _bionicThreadStarter;

	    /// <summary>
	    /// The kitchen sink of services.
	    /// </summary>
	    /// <param name="merchandisingTaskThreadStarter">starts the threads that read the AutoApprove queues</param>
	    /// <param name="autoLoadRunner">starts the threads that read the autoload queues</param>
	    /// <param name="dashboardThreadStarter">starts threads related to dashboard calculations</param>
	    /// <param name="fordDirectAutoLoadThreadStarter">starts the threads that execute internal autoloads</param>
	    /// <param name="timeToMarketTaskThreadStarter">starts the threads for time to market tasks</param>
	    /// <param name="bionicThreadStarter"></param>
	    public Service(IMerchandisingTaskThreadStarter merchandisingTaskThreadStarter, IAutoLoadThreadStarter autoLoadRunner,
             IDashboardThreadStarter dashboardThreadStarter, 
            IFordDirectAutoLoadThreadStarter fordDirectAutoLoadThreadStarter, ITimeToMarketTaskThreadStarter timeToMarketTaskThreadStarter, IBionicThreadStarter bionicThreadStarter)
        {
            InitializeComponent();

            _merchandisingTaskThreadStarter = merchandisingTaskThreadStarter;
            _autoLoadThreadStarter = autoLoadRunner;
            _dashboardThreadStarter = dashboardThreadStarter;
            _fordDirectAutoLoadThreadStarter = fordDirectAutoLoadThreadStarter;
	        _timeToMarketTaskThreadStarter = timeToMarketTaskThreadStarter;
		    _bionicThreadStarter = bionicThreadStarter;
        }

        protected override void OnStart(string[] args)
        {
            Log.InfoFormat("OnStart() for Release.Tasks.Service version {0} has been called.", AppVersion());

            StartBackgroundThread(_merchandisingTaskThreadStarter, "Merchandising Tasks Thread Tree" );
            StartBackgroundThread(_autoLoadThreadStarter, "AutoLoad Tasks Thread Tree" );
            StartBackgroundThread(_dashboardThreadStarter, "Dashboard Tasks Thread Tree");
            StartBackgroundThread(_fordDirectAutoLoadThreadStarter, "Ford Direct AutoLoad Tasks Thread Tree");
			StartBackgroundThread(_timeToMarketTaskThreadStarter, "Time to Market Tasks Thread Tree");
			StartBackgroundThread(_bionicThreadStarter, "BionicReports Thread Tree");

            base.OnStart(args);
        }

        protected override void OnStop()
        {
            Log.InfoFormat("OnStop() for {0} version {1} has been called.", typeof(Service), AppVersion());

            StopThreads();
            Log.Info("Exiting");
            base.OnStop();
        }
    }
}
