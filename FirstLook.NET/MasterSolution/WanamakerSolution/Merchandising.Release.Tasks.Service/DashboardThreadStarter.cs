﻿using System;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using MAX.Threading;
using Merchandising.Messages;
using GroupLevelDashboardAggregation = Merchandising.Release.Tasks.Service.Tasks.GroupLevelDashboardAggregation;

namespace Merchandising.Release.Tasks.Service
{
    /// <summary>
    /// A marker interface.
    /// </summary>
    public interface IDashboardThreadStarter : IRunWithCancellation
    {}

    /// <summary>
    /// Starts threads related to the dashboard.
    /// </summary>
    internal class DashboardThreadStarter : IDashboardThreadStarter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IQueueFactory _queueFactory;
        private readonly IGroupDashboardManager _groupDashboardManager;

        public DashboardThreadStarter(IQueueFactory queueFactory, IGroupDashboardManager groupDashboardManager)
        {
            _queueFactory = queueFactory;
            _groupDashboardManager = groupDashboardManager;
        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {            
            Log.Info("Dashboard threads starting.");

            NamedBackgroundThread.Start("Worker - GroupLevelDashboardAggregation", 
                () => new GroupLevelDashboardAggregation(_queueFactory, _groupDashboardManager).Run(shouldCancel, blockWhileNoMessages));
        }
    }
}
