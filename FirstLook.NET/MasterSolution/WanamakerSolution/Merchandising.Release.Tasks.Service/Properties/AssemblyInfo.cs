﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MerchandisingReleaseTasks")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("MerchandisingReleaseTasks")]

[assembly: InternalsVisibleTo("Merchandising.Release.Tasks.Service.IntegrationTests")]
[assembly: InternalsVisibleTo("Merchandising.Release.Tasks.Service.Tests")]