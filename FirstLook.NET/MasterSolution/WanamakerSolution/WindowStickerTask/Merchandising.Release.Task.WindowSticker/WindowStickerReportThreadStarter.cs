﻿using System;
using BulkWindowSticker;
using Core.Messaging;
using MAX.Threading;
using Merchandising.Messages;
using log4net;

namespace Merchandising.Release.Task.WindowSticker
{

    public interface IWindowStickerReportThreadStarter : IRunWithCancellation
    { }


    public class WindowStickerReportThreadStarter : IWindowStickerReportThreadStarter
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static IQueueFactory _queueFactory;
        private static IEmail _emailProvider;
        private static IWindowStickerRepository _windowStickerRepository;

        public WindowStickerReportThreadStarter(IQueueFactory queueFactory, IEmail emailProvider, IWindowStickerRepository windowStickerRepository)
        {
            _queueFactory = queueFactory;
            _emailProvider = emailProvider;
            _windowStickerRepository = windowStickerRepository;

        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            Log.Debug("WindowStickerReportThreadStarter.Run() called");

            var autoWindowStickerReportTask = new AutoWindowStickerReportTask(_queueFactory, _windowStickerRepository, _emailProvider);

            autoWindowStickerReportTask.Run(shouldCancel, blockWhileNoMessages);

            Log.Debug("WindowStickerReportThreadStarter.Run() exiting");
        }

    }
}
