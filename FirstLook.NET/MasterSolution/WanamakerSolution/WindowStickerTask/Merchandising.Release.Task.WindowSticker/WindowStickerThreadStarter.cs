﻿using System;
using BulkWindowSticker;
using Core.Messaging;
using log4net;
using MAX.Threading;
using Merchandising.Messages;

namespace Merchandising.Release.Task.WindowSticker
{
    /// <summary>
    /// A marker interface.
    /// </summary>
    public interface IWindowStickerThreadStarter : IRunWithCancellation
    { }

    internal class WindowStickerThreadStarter : IWindowStickerThreadStarter
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static IQueueFactory _queueFactory;
        private static IFileStoreFactory _fileStoreFactory;
        private static IEmail _emailProvider;
        private static IWindowStickerRepository _windowStickerRepository;

        public WindowStickerThreadStarter(IQueueFactory queueFactory, IFileStoreFactory fileStoreFactory, IEmail emailProvider, IWindowStickerRepository windowStickerRepository)
        {
            _queueFactory = queueFactory;
            _fileStoreFactory = fileStoreFactory;
            _emailProvider = emailProvider;
            _windowStickerRepository = windowStickerRepository;
        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            Log.Debug("WindowStickerThreadStarter.Run() called");

            var autoWindowStickerTask = new AutoWindowStickerTask(_queueFactory, _fileStoreFactory,
                _emailProvider, _windowStickerRepository);

            autoWindowStickerTask.Run(shouldCancel, blockWhileNoMessages);

            Log.Debug("WindowStickerThreadStarter.Run() exiting");
        }
    }
}
