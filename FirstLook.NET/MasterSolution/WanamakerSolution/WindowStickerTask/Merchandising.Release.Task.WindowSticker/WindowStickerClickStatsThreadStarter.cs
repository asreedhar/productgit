﻿using System;
using BulkWindowSticker;
using FirstLook.Common.Core.IOC;
using MAX.Threading;
using Merchandising.Messages;
using log4net;

namespace Merchandising.Release.Task.WindowSticker
{
    /// <summary>
    /// A marker interface.
    /// </summary>
    public interface IWindowStickerClickStatsThreadStarter : IRunWithCancellation
    {}

    internal class WindowStickerClickStatsThreadStarter : IWindowStickerClickStatsThreadStarter
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static IQueueFactory _queueFactory;
        private static WindowStickerRepository _windowStickerRepository;

        public WindowStickerClickStatsThreadStarter(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
            _windowStickerRepository = new WindowStickerRepository();
        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            Log.DebugFormat("DoWork() called {0} in ", GetType().Name);

            if (_queueFactory == null)
            {
                _queueFactory = Registry.Resolve<IQueueFactory>();                
            }

            var windowStickerClickStatsTask = new WindowStickerClickStatsTask(_queueFactory, _windowStickerRepository);

            windowStickerClickStatsTask.Run(shouldCancel, blockWhileNoMessages);

            Log.DebugFormat("DoWork() completed {0} in ", GetType().Name);
        }
    }
}
