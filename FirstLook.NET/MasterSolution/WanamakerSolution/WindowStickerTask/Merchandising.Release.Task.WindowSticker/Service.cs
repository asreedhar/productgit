﻿using log4net;
using MAX.TaskFramework;

namespace Merchandising.Release.Task.WindowSticker
{
    partial class Service : TaskServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IWindowStickerThreadStarter _windowStickerThreadStarter;
        private readonly IWindowStickerClickStatsThreadStarter _windowStickerClickStatsThreadStarter;
        private readonly IWindowStickerReportThreadStarter _windowStickerReportThreadStarter;

        public Service(IWindowStickerThreadStarter windowStickerThreadStarter, 
            IWindowStickerClickStatsThreadStarter windowStickerClickStatsThreadStarter,
            IWindowStickerReportThreadStarter windowStickerReportThreadStarter)
        {
            InitializeComponent();

            _windowStickerThreadStarter = windowStickerThreadStarter;
            _windowStickerClickStatsThreadStarter = windowStickerClickStatsThreadStarter;
            _windowStickerReportThreadStarter = windowStickerReportThreadStarter;
        }

        protected override void OnStart(string[] args)
        {
            Log.InfoFormat("OnStart() for {0} version {1} has been called.", typeof(Service), AppVersion());

            StartBackgroundThread(_windowStickerThreadStarter, "Window Sticker Worker Thread");
            StartBackgroundThread(_windowStickerClickStatsThreadStarter, "Window Sticker Click Stats");
            StartBackgroundThread(_windowStickerReportThreadStarter, "Window Sticker Report Worker Thead");

            base.OnStart(args);            
        }

        protected override void OnStop()
        {
            Log.InfoFormat("OnStop() for {0} version {1} has been called.", typeof(Service), AppVersion());

            StopThreads();
            Log.Info("Exiting");
            base.OnStop();
        }
    }
}
