﻿using System.ServiceProcess;
using System.Threading;
using AmazonServices;
using Autofac;
using BulkWindowSticker;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using System;
using System.Linq;
using FirstLook.DomainModel.Oltp;
using MAX.Threading;
using Merchandising.Messages;
using MerchandisingRegistry;
using log4net;

namespace Merchandising.Release.Task.WindowSticker
{
    static class Program
    {
        private static IContainer _container;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            SetupAutofac();
            SetupLogging();

            //This can be called in "console mode" from the debugger without having to run a service by passing -console
            if (args.Any(s => StringComparer.InvariantCultureIgnoreCase.Compare(s, "-console") == 0))
                ExecuteConsoleMode();
            else
                ExecuteServiceMode();
        }
        private static void ExecuteServiceMode()
        {
            // Here we create the Service through the container to resolve its dependencies
            var service = _container.Resolve<Service>();
            ServiceBase.Run(service);
        }

        //Used primarly for testing in console mode. Pass -console as an argument in Visual Studio project properties
        private static void ExecuteConsoleMode()
        {
            var shutdownEvent = new ManualResetEvent(false);
            var blockWhenNoMessages = new ManualResetEvent(false);

            var windowStickerWorker = _container.Resolve<IWindowStickerThreadStarter>();
            Thread wsThread = NamedBackgroundThread.Start("Window Sticker Worker Thread",
                () => windowStickerWorker.Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));

            var clickStatWorker = _container.Resolve<IWindowStickerClickStatsThreadStarter>();
            Thread statsThread = NamedBackgroundThread.Start("Window Sticker Click Stats Worker Thread",
                () => clickStatWorker.Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));

            var windowStickerReportWorker = _container.Resolve<IWindowStickerReportThreadStarter>();
            Thread wsReportThread = NamedBackgroundThread.Start("Window Sticker report Worker Thread",
                () => windowStickerReportWorker.Run(() => shutdownEvent.WaitOne(0), () => blockWhenNoMessages.WaitOne(15000)));

            
            Console.WriteLine("Press enter to quit.");
            Console.ReadLine();

            // signal cancellation and then wait for thread for up to 10 seconds
            shutdownEvent.Set();
            blockWhenNoMessages.Set();

            if (!wsThread.Join(30000))
            { 
                Log.Error("Error joining worker thread.");
                wsThread.Abort();
            }

            if (!statsThread.Join(10000))
			{
				Log.Error("Error joining clickStats thread.");
                statsThread.Abort();
			}

            if (!wsReportThread.Join(10000))
            {
                Log.Error("Error joining window sticker report thread.");
                wsReportThread.Abort();
            }	


        }
        private static void SetupLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        private static void SetupAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());

            builder.RegisterModule(new MerchandisingDomainModule());
            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterModule(new AmazonServicesModule());

            builder.RegisterType<WindowStickerRepository>().As<IWindowStickerRepository>();


            // Register our thread starters so we can resolve their dependencies.
            builder.RegisterType<WindowStickerThreadStarter>().As<IWindowStickerThreadStarter>();
            builder.RegisterType<WindowStickerClickStatsThreadStarter>().As<IWindowStickerClickStatsThreadStarter>();
            builder.RegisterType<WindowStickerReportThreadStarter>().As<IWindowStickerReportThreadStarter>();


            // Register the service so we can resolve its dependencies
            builder.RegisterType<Service>().AsSelf();

            _container = builder.Build();
            Registry.RegisterContainer(_container);
        }

    }
}
