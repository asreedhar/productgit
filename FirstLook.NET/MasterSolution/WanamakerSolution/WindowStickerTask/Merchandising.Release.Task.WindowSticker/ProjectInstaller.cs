﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;


namespace Merchandising.Release.Task.WindowSticker
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
        protected override void OnCommitted(IDictionary savedState)
        {
            // start the service after installation
            new ServiceController(serviceInstaller1.ServiceName).Start();
            
            base.OnCommitted(savedState);
        }

    }
}
