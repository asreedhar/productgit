﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OEMCredentialService.Filters;
using MAX.ExternalCredentials;

namespace OEMCredentialService.Controllers
{
    [SlurpeeBasicAuthenticationFilter]
    public class FailController : ApiController
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);


        // POST api/fail
        public HttpResponseMessage Post(string hash, string version)
        {

            try
            {
                Guid credentialHash;

                if (Guid.TryParse(hash, out credentialHash))
                {
                    var creds = EncryptedSiteCredential.Fetch(credentialHash);

                    if(creds == null)
                        throw new ApplicationException(String.Format("Cannot find a credential with a hash of: {0}", hash));

                    creds.CredentialVersion = BitConverter.GetBytes(Convert.ToUInt64(version));

                    if (creds.SetLockAndSave(true) < 1) // lock this sucker out!!
                        throw new ApplicationException(string.Format("Credential with hash: {0} was already locked out", hash));

                }
                else
                {
                    throw new ApplicationException(String.Format("Invalid hash value: {0} passed to credential fail", hash));
                }

            }
            catch (Exception ex)
            {
                // send a 202 back to the calling application
                Log.DebugFormat("OEM Credential Service failed called with wrong version. Hash: {0} Version: {1}", hash, version);
                return Request.CreateErrorResponse(HttpStatusCode.Accepted, ex.Message);

            }

            Log.InfoFormat("OEM Credential Service fail called and sucessfully locked credentials. Hash: {0} Version: {1}", hash, version);
            return Request.CreateResponse(HttpStatusCode.OK);

        }
    }
}
