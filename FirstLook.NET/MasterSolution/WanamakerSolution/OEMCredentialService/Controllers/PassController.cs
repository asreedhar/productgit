﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using OEMCredentialService.Filters;

namespace OEMCredentialService.Controllers
{
    [SlurpeeBasicAuthenticationFilter]
    public class PassController : ApiController
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        // POST api/pass
        public HttpResponseMessage Post(string hash, string version)
        {

            // nothing to do right now!!!
            
            Log.DebugFormat("OEM Credential Service Pass called. Hash: {0} Version: {1}", hash, version);

            return Request.CreateResponse(HttpStatusCode.OK);


        }
    }
}
