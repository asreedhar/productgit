﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using FirstLook.Internal;

namespace OEMCredentialService.Controllers
{
    public class VersionController : ApiController
    {
        // GET api/version
        public HttpResponseMessage Get()
        {
            //return Request.CreateErrorResponse(HttpStatusCode.OK, "Version 0.0.0.0");

            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(
                    typeof(FirstLookAssemblyInfo).Assembly.GetName().Version.ToString(),
                    //"Version 0.0.0.0",
                    Encoding.UTF8,
                    "text/plain")
            };

        }

    }
}
