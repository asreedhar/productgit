﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OEMCredentialService.Filters;
using MAX.ExternalCredentials;

namespace OEMCredentialService.Controllers
{
    [SlurpeeBasicAuthenticationFilter]
    public class StatusController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        // GET api/status/5
        public HttpResponseMessage Get(string hash, string version)
        {

            try
            {
                Guid credentialHash;

                if (Guid.TryParse(hash, out credentialHash))
                {
                    var creds = EncryptedSiteCredential.Fetch(credentialHash);

                    if (creds == null)
                        throw new ApplicationException(String.Format("Cannot find a credential with a hash of: {0}",hash));

                    var testVersion = BitConverter.GetBytes(Convert.ToUInt64(version));
                    
                    //if(creds.CredentialVersion != BitConverter.GetBytes(Convert.ToUInt64(version)))
                    if (!creds.CredentialVersion.SequenceEqual(testVersion))
                        throw new ApplicationException("This version of your credential is out of date");

                }
                else
                {
                    throw new ApplicationException(String.Format("Invalid hash value: {0} passed to credential status", hash));
                }

            }
            catch (Exception ex)
            {

                Log.InfoFormat("OEM Credential Service Status call failed. Hash: {0} Version: {1}", hash, version);
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.Message);
            }

            Log.DebugFormat("OEM Credential Service Status call passed. Hash: {0} Version: {1}", hash, version);
            return Request.CreateResponse(HttpStatusCode.NotModified);

        }
    }
}
