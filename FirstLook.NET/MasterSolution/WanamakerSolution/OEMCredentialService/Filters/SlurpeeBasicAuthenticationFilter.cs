﻿using System.Configuration;

namespace OEMCredentialService.Filters
{
    public class SlurpeeBasicAuthenticationFilter: BasicAuthenticationFilter
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

        public SlurpeeBasicAuthenticationFilter()
        {
        }

        public SlurpeeBasicAuthenticationFilter(bool active): base(active)
        {
        }

        protected override bool OnAuthorizeUser(string username, string password, System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            bool authorized = false;

            string configUserName = ConfigurationManager.AppSettings["SlurpeeUserName"];
            string configPassword = ConfigurationManager.AppSettings["SlurpeePassword"];

            if(string.IsNullOrWhiteSpace(configUserName))
                configUserName = "";
            
            if(string.IsNullOrWhiteSpace(configPassword))
                configPassword = "";

            if(configUserName == username && configPassword == password)
            {
                authorized = true;
                Log.DebugFormat("UserName: {0} passed basic authentication", username);
            }
            else
            {
                Log.InfoFormat("UserName: {0} failed basic authentication", username);
            }


            return authorized;

        }

    }
}