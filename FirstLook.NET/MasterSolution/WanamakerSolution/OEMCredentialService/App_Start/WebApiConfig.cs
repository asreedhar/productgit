﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace OEMCredentialService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // slurpee credential routes
            config.Routes.MapHttpRoute(
                name: "SlurpeeWebHookApi",
                routeTemplate: "api/{controller}/{hash}/{version}");

            
            //config.Routes.MapHttpRoute(
            //        name: "HelloApi",
            //        routeTemplate: "api/hello/{id}/{site}",
            //        defaults: new {
            //                controller = "hello"
            //                }
            //        );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
