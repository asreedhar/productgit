﻿using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.Pricing.DomainModel;

namespace MerchandisingRegistry
{
    public class WebAppModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HealthMonitoringWrapper>().As<ILogger>();
        }
    }
}
