﻿using Autofac;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.ModelBuilder;
using FirstLook.Pricing.DomainModel;

namespace MerchandisingRegistry
{
    public class MerchandisingDomainModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            new PricingDomainRegister().Register(builder);
            new MerchandisingDomainRegistry().Register(builder);
            new MerchandisingModelBuilderRegistry().Register(builder);
        }
    }
}
