﻿using Autofac;
using NUnit.Framework;

namespace MAX.Market.Test
{
    [TestFixture]
    public class MarketTest
    {
        private IContainer _container;

        [TestFixtureSetUp]
        public void SetUp()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<MockMappingClient>().AsImplementedInterfaces();

            _container = builder.Build();
        }


        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void Test1(int chromestyleid)
        {
            var client = _container.Resolve<IMappingClient>();
            var marketInfo = client.GetVehicleMarketInfo(1);
            Assert.IsNotNull(marketInfo);
            Assert.IsNotNull(marketInfo);
            
            if (marketInfo.VehicleMarketAttributeList.Count <= 0) return; // nothing left to assert

            Assert.IsNotNull(marketInfo.VehicleMarketAttributeList[0].Engines);
            Assert.IsNotNull(marketInfo.VehicleMarketAttributeList[0].Transmissions);
            Assert.IsNotNull(marketInfo.VehicleMarketAttributeList[0].DriveTrains);
            Assert.IsNotNull(marketInfo.VehicleMarketAttributeList[0].FuelTypes);
        }



    }
}
