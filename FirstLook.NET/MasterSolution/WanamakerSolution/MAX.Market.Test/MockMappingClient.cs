using System.Collections.Generic;

namespace MAX.Market.Test
{
    public class MockMappingClient : IMappingClient
    {
        public VehicleMappingResponse GetVehicleMarketInfo(int chromeStyleId)
        {
            var group = chromeStyleId%4;

            var atts = new List<VehicleMarketAttributes>();

            if (group == 0)
            {
                atts.Add(
                    new VehicleMarketAttributes
                    {
                        Trim = "LX",
                        Engines = new[] {"5.0 V8", "6.2 V8"},
                        Transmissions = new[] {"6 Speed Automatic", "5 Speed Manual"},
                        FuelTypes = new[] {"Gasoline"},
                        DriveTrains = new[] {"Rear Wheel Drive"}
                    });
                atts.Add(
                    new VehicleMarketAttributes
                    {
                        Trim = "S",
                        Engines = new[] {"5.0 V8", "6.2 V8", "7.0 V8"},
                        Transmissions = new[] {"6 Speed Automatic", "5 Speed Manual", "7 Speed Automatic"},
                        FuelTypes = new[] {"Gasoline"},
                        DriveTrains = new[] {"Rear Wheel Drive", "All Wheel Drive"}
                    });
            }
            
            if (group == 1)
            {
                atts.Add(
                    new VehicleMarketAttributes
                    {
                        Trim = "RS",
                        Engines = new[] {"5.7L V8"},
                        Transmissions = new[] {"Manual"},
                        FuelTypes = new[] {"Gasoline"},
                        DriveTrains = new[] {"Rear Wheel Drive"}
                    });
            }

            if (group == 2)
            {
                atts.Add(
                    new VehicleMarketAttributes
                    {
                        Trim = "Basic",
                        Engines = new[] {"2.1 4 Cylinder", "3.6 V6"},
                        Transmissions = new[] {"Automatic"},
                        FuelTypes = new[] {"Gasoline", "Natural Gas"},
                        DriveTrains = new[] {"Front Wheel Drive"}
                    });
            }

            // group == 3, nothing added
            return new VehicleMappingResponse
            {
                Year = 1999,    // does not matter for test
                Make = "Toyota", // does not matter for test
                Model = "Camry", // does not matter for test
                VehicleMarketAttributeList = atts
            };
        }
    }
}