﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Moq;
using NUnit.Framework;
using MAX.Entities.Helpers.PhotoServicesHelpers;

namespace MAX.Entities.Tests.Helpers.PhotoServicesHelpers
{
    public class BasePhotoTestBehavior
    {
        protected const string BUSINESS_UNIT_CODE = "WINDYCIT05";
        protected const string USER_ID = "zbrown";
        protected const string FIRST_NAME = "zac";
        protected const string LAST_NAME = "brown";
        protected const string VIN = "WBAPK7C53BA000013";
        protected const string SNUM = "A772205";
    }

    [TestFixture]
    public class HelperTest : BasePhotoTestBehavior
    {
        private Helper _serviceHelper;
        public Helper ServiceHelper
        {
            get { return _serviceHelper ?? (_serviceHelper = new Helper()); }
        }

        [Test]
        public void ParseNone()
        {
            var doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("vinPhotos"));

            var result = ServiceHelper.Parse(doc.ToString(SaveOptions.DisableFormatting));

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(0, result.Errors.Length);
            Assert.IsNotNull(result.Vehicles);
        }

        [Test]
        public void ParseOne()
        {
            const string URL1 = "https://a.com/a.jpg";
            const string URL2 = "https://b.com/b.jpg";

            var doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("vinPhotos",
                             new XElement("vinPhoto",
                                          new XElement("vin", VIN),
                                          new XElement("urls",
                                                       new XElement("url", URL1),
                                                       new XElement("url", URL2))
                                         )
                            )
                );

            var result = ServiceHelper.Parse(doc.ToString(SaveOptions.DisableFormatting));

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(0, result.Errors.Length);
            Assert.AreEqual(1, result.Vehicles.Length);
            Assert.AreEqual(VIN, result.Vehicles[0].Vin);
            Assert.AreEqual(2, result.Vehicles[0].PhotoUrls.Length);
            Assert.AreEqual(URL1, result.Vehicles[0].PhotoUrls[0]);
            Assert.AreEqual(URL2, result.Vehicles[0].PhotoUrls[1]);
        }

        [Test]
        public void ParseTwo()
        {
            const string VIN1 = "abcdefghijlkmnopq";
            const string URL1_A = "https://1.com/1a.jpg";
            const string URL1_B = "https://1.com/1b.jpg";

            const string VIN2 = "bbcdefghijlkmnopq";
            const string URL2_A = "https://2.com/2a.jpg";
            const string URL2_B = "https://2.com/2b.jpg";


            var doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("vinPhotos",
                             new XElement("vinPhoto",
                                          new XElement("vin", VIN1),
                                          new XElement("urls",
                                                       new XElement("url", URL1_A),
                                                       new XElement("url", URL1_B))
                                         ),
                             new XElement("vinPhoto",
                                          new XElement("vin", VIN2),
                                          new XElement("urls",
                                                       new XElement("url", URL2_A),
                                                       new XElement("url", URL2_B))
                                         )

                            )
                );

            var result = ServiceHelper.Parse(doc.ToString(SaveOptions.DisableFormatting));

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(0, result.Errors.Length);
            Assert.AreEqual(2, result.Vehicles.Length);
            Assert.AreEqual(VIN1, result.Vehicles[0].Vin);

            Assert.AreEqual(2, result.Vehicles[0].PhotoUrls.Length);
            Assert.AreEqual(URL1_A, result.Vehicles[0].PhotoUrls[0]);
            Assert.AreEqual(URL1_B, result.Vehicles[0].PhotoUrls[1]);

            Assert.AreEqual(2, result.Vehicles[1].PhotoUrls.Length);
            Assert.AreEqual(URL2_A, result.Vehicles[1].PhotoUrls[0]);
            Assert.AreEqual(URL2_B, result.Vehicles[1].PhotoUrls[1]);
        }
    }
}
