﻿using System.Collections.Generic;

namespace MAX.Market
{
    public class VehicleMarketAttributes : IVehicleMarketAttributes
    {
        internal VehicleMarketAttributes()
        {
            Engines = new List<string>();
            Transmissions = new List<string>();
            DriveTrains = new List<string>();
            FuelTypes = new List<string>();
        }

        public string Trim { get; internal set; }
        public bool MatchesTargetVehicleTrim { get; set; }
        public IList<string> Engines { get; internal set; }
        public IList<string> Transmissions { get; internal set; }
        public IList<string> DriveTrains { get; internal set; }
        public IList<string> FuelTypes { get; internal set; }
    }
}