﻿using Nest;

namespace MAX.Market
{
    public class VehicleInfoData
    {
        [ElasticProperty(Name = "modelYear")]
        public string ModelYear { get; set; }

        [ElasticProperty(Name = "make", Index = FieldIndexOption.NotAnalyzed)]
        public string Make { get; set; }

        [ElasticProperty(Name = "model", Index = FieldIndexOption.NotAnalyzed)]
        public string Model { get; set; }

        // The aggregated fields need to be "not analyzed" to keep them from being tokenized (we want them whole)
        [ElasticProperty(Name = "trim", Index = FieldIndexOption.NotAnalyzed)]
        public string Trim { get; set; }

        [ElasticProperty(Name = "engineDescription", Index = FieldIndexOption.NotAnalyzed)]
        public string EngineDescription { get; set; }

        [ElasticProperty(Name = "transmissionDescription", Index = FieldIndexOption.NotAnalyzed)]
        public string TransmissionDescription { get; set; }


        [ElasticProperty(Name = "drivetrainDescription", Index = FieldIndexOption.NotAnalyzed)]
        public string DriveTrain { get; set; }

        [ElasticProperty(Name = "engineFuelType", Index = FieldIndexOption.NotAnalyzed)]
        public string FuelType { get; set; }


    }
}