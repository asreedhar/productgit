﻿using Autofac;

namespace MAX.Market
{
    public class MaxMarketModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MappingClient>().AsImplementedInterfaces();
            builder.RegisterType<MappingSearchProvider>().AsImplementedInterfaces();
        }
    }
}