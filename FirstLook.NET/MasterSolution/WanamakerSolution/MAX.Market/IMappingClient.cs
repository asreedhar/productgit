﻿using System.Collections.Generic;

namespace MAX.Market
{
    public interface IMappingClient
    {
        VehicleMappingResponse GetVehicleMarketInfo(int chromeStyleId);
    }
}