using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Elasticsearch.Net;
using Nest;

namespace MAX.Market
{
    public class MappingSearchProvider : IMappingSearchProvider
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ElasticClient _client;
        private readonly string _esIndexName;

        public MappingSearchProvider()
        {
            var esHost = ConfigurationManager.AppSettings["MarketMappingElasticSearchHost"];
            if (esHost == null) throw new ApplicationException("No application setting exists for: MarketMappingElasticSearchHost");
            _esIndexName = ConfigurationManager.AppSettings["MarketMappingElasticSearchIndexName"];
            if (esHost == null) throw new ApplicationException("No application setting exists for: MarketMappingElasticSearchIndexName");
            var esUser = ConfigurationManager.AppSettings["MarketMappingElasticSearchUserName"];
            if (esHost == null) throw new ApplicationException("No application setting exists for: MarketMappingElasticSearchUserName");
            var esPassword = ConfigurationManager.AppSettings["MarketMappingElasticSearchPassword"];
            if (esHost == null) throw new ApplicationException("No application setting exists for: MarketMappingElasticSearchPassword");

            var settings = new ConnectionSettings(new Uri(esHost), _esIndexName);
            Log.DebugFormat("ElasticSearch connection settings host:{0}, index: {1}, user: {2}", esHost, _esIndexName, esUser);
            settings.SetBasicAuthentication(esUser, esPassword);
            settings.ExposeRawResponse();

            _client = new ElasticClient(settings);

        }

        public VehicleSearchTermsByTrim GetVehicleSearchTerms(VehicleDescription vehicleDescription)
        {
            var searchTermsByTrim = new VehicleSearchTermsByTrim();

            var response = RunAggregationQuery(vehicleDescription);

            ExtractAggregations(response, searchTermsByTrim);

            return searchTermsByTrim;
        }

        private static void ExtractAggregations(ISearchResponse<VehicleInfo> response, VehicleSearchTermsByTrim attributes)
        {
            foreach (var t in response.Aggs.Terms(CommonStuff.AggregationKeyTrims).Items)
            {
                attributes.List.Add(t.Key, new VehicleSearchResults());
                foreach (var subaggregation in t.Aggregations)
                {
                    var aggbucket = subaggregation.Value as Bucket;

                    if (aggbucket != null)
                    {
                        foreach (var i in aggbucket.Items)
                        {
                            var ki = (KeyItem) i;

                            if (subaggregation.Key == CommonStuff.AggregationKeyEngines)
                                attributes.List[t.Key].Engines.Add(ki.Key);
                            if (subaggregation.Key == CommonStuff.AggregationKeyTransmissions)
                                attributes.List[t.Key].Transmissions.Add(ki.Key);
                            if (subaggregation.Key == CommonStuff.AggregationKeyFuelTypes)
                                attributes.List[t.Key].FuelTypes.Add(ki.Key);
                            if (subaggregation.Key == CommonStuff.AggregationKeyDriveTrains)
                                attributes.List[t.Key].DriveTrains.Add(ki.Key);
                        }
                    }
                }
            }
        }

        public VehicleDescription GetVehicleDescription(int chromeStyleId)
        {
            var query = Query<object>.Term("style.id", chromeStyleId);

            var response = _client.Search<VehicleInfo>(
                s => s.From(0)
                    .Size(1)
                    .Query(query)
                    .Fields(f => f.Data.ModelYear, f => f.Data.Make, f => f.Data.Model, f=>f.Data.Trim)
                    );
            if (response.Hits.Any())
            {
                if (response.Hits.Count() > 1)
                    Log.DebugFormat("Multiple records were found for ChromeStyleId {0}, but right or wrong, we just used the first one.", chromeStyleId);

                var modelYear = response.Hits.ElementAt(0).Fields.FieldValues<VehicleInfo, string>(f => f.Data.ModelYear).First();
                var make = response.Hits.ElementAt(0).Fields.FieldValues<VehicleInfo, string>(f => f.Data.Make).First();
                var model = response.Hits.ElementAt(0).Fields.FieldValues<VehicleInfo, string>(f => f.Data.Model).First();
                
                // This is probably a bug in the data, but some documents lack a trim.  So we need to check for it before we try to access it.
                var trimExistsInDictionary = response.Hits.ElementAt(0).Fields.FieldValuesDictionary.ContainsKey("data.trim");
                var trim = string.Empty;

                if (!trimExistsInDictionary)
                    Log.DebugFormat("No trim was found while building a VehicleDescription for this ChromeStyleId: {0}", chromeStyleId);
                else
                {
                    trim = response.Hits.ElementAt(0).Fields.FieldValues<VehicleInfo, string>(f => f.Data.Trim).First();    
                }

                return new VehicleDescription { Make = make, Model = model, Year = Int32.Parse(modelYear), Trim = trim};
            }

            Log.DebugFormat("GetVehicleDescription found no record for ChromeStyleId: {0}", chromeStyleId);

            return new VehicleDescription();
        }

        private ISearchResponse<VehicleInfo> RunAggregationQuery(VehicleDescription vehicleDescription)
        {
            var response = _client.Search<VehicleInfo>(
                s => s.Index(_esIndexName)
                    .Query(q =>
                        q.Term(x => x.Data.Make, vehicleDescription.Make)
                        && q.Term(x => x.Data.Model, vehicleDescription.Model)
                        && q.Term(x => x.Data.ModelYear, vehicleDescription.Year.ToString(CultureInfo.InvariantCulture))
                    )
                    .SearchType(SearchType.Count)
                    .Aggregations(a => a
                        .Terms(CommonStuff.AggregationKeyTrims, t => t.Field(x => x.Data.Trim)
                            .Aggregations(aa => aa
                                .Terms(CommonStuff.AggregationKeyEngines, v => v.Field(f => f.Data.EngineDescription))
                                .Terms(CommonStuff.AggregationKeyTransmissions,
                                    v => v.Field(f => f.Data.TransmissionDescription))
                                .Terms(CommonStuff.AggregationKeyFuelTypes, v => v.Field(f => f.Data.FuelType))
                                .Terms(CommonStuff.AggregationKeyDriveTrains, v => v.Field(f => f.Data.DriveTrain)))
                        )
                    )
                );
            return response;
        }
    }
}