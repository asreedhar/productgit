﻿using Nest;

namespace MAX.Market
{
    [ElasticType(Name = "vehicle_information_data")]
    public class VehicleInfo
    {
        [ElasticProperty(Name = "data")]
        public VehicleInfoData Data { get; set; }

        [ElasticProperty(Name = "style")]
        public VehicleInfoStyle Style { get; set; }
    
    }



}