﻿using System.Collections.Generic;

namespace MAX.Market
{
    public class VehicleSearchResults
    {
        public VehicleSearchResults()
        {
            Engines = new List<string>();
            Transmissions = new List<string>();
            DriveTrains = new List<string>();
            FuelTypes = new List<string>();
        }

        public List<string> Engines { get; internal set; }
        public List<string> Transmissions { get; internal set; }
        public List<string> DriveTrains { get; internal set; }
        public List<string> FuelTypes { get; internal set; }
    }
}