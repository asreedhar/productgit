﻿using System.Collections.Generic;

namespace MAX.Market
{
    public class VehicleMappingResponse
    {
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public IList<VehicleMarketAttributes> VehicleMarketAttributeList { get; set; }
    }
}