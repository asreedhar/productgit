﻿namespace MAX.Market
{
    public class VehicleDescription
    {
        internal int Year { get; set; }
        internal string Make { get; set; }
        internal string Model { get; set; }
        internal string Trim { get; set; }
    }
}