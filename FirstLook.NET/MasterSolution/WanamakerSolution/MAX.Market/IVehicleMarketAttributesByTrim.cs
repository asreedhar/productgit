﻿using System.Collections.Generic;

namespace MAX.Market
{
    public interface IVehicleMarketAttributes 
    {
        string Trim { get; }
        IList<string> Engines { get; }
        IList<string> Transmissions { get; }
        IList<string> DriveTrains { get; }
        IList<string> FuelTypes { get; }
    }

    public interface IVehicleMarketAttributesByTrim
    {
        IDictionary<string, IVehicleMarketAttributes> TrimAttributes { get; }
        IList<string> Trims { get; }
    }

}