﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using MAX.FirstLookServicesApiClient;

namespace MAX.Market
{
    public class MappingClient : IMappingClient
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IMappingSearchProvider SearchProvider { get; set; }

        public MappingClient(IMappingSearchProvider searchProvider)
        {
            SearchProvider = searchProvider;
        }

        /// <summary>
        /// For the vehicle identified by the inventory and dealer passed in, returns lists of attributes found in the market for various categories (Engines, Transmissions, DriveTrains, etc.)
        /// </summary>
        /// <param name="chromeStyleId"></param>
        /// <returns>The various lists of attributes for this vehicle</returns>
        public VehicleMappingResponse GetVehicleMarketInfo(int chromeStyleId)
        {
            var vehicleDescription = SearchProvider.GetVehicleDescription(chromeStyleId);

            Debug.WriteLine("Vehicle: {0}, {1}, {2}", vehicleDescription.Year, vehicleDescription.Make, vehicleDescription.Model);

            var mappingAttributes = SearchProvider.GetVehicleSearchTerms(vehicleDescription);

            var vehicleMarketAttributes = 
                mappingAttributes.List.Select(
                item => new VehicleMarketAttributes
            {
                Trim = item.Key,
                MatchesTargetVehicleTrim = item.Key == vehicleDescription.Trim,
                Engines = item.Value.Engines, 
                Transmissions = item.Value.Transmissions, 
                DriveTrains = item.Value.DriveTrains, 
                FuelTypes = item.Value.FuelTypes
            }).ToList();

            return new VehicleMappingResponse
            {
                Year = vehicleDescription.Year,
                Make = vehicleDescription.Make,
                Model = vehicleDescription.Model,
                VehicleMarketAttributeList = vehicleMarketAttributes
            };
        }
    }
}