﻿using System.Collections.Generic;

namespace MAX.Market
{
    public class VehicleSearchTermsByTrim
    {
        public VehicleSearchTermsByTrim()
        {
            List = new Dictionary<string, VehicleSearchResults>();
        }

        public Dictionary<string, VehicleSearchResults> List
        {
            get; internal set; 
        }
    }
}