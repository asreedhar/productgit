using Nest;

namespace MAX.Market
{
    public class VehicleInfoStyle
    {
        [ElasticProperty(Name = "id")]
        public int Id{ get; set; }

        [ElasticProperty(Name = "code")]
        public string Code { get; set; }

        [ElasticProperty(Name = "fullCode")]
        public string FullCode { get; set; }

    }
}