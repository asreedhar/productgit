namespace MAX.Market
{
    public static class CommonStuff
    {
        internal const string AggregationKeyTrims = "Trims";
        internal const string AggregationKeyEngines = "Engines";
        internal const string AggregationKeyTransmissions = "Transmissions";
        internal const string AggregationKeyDriveTrains = "DriveTrains";
        internal const string AggregationKeyFuelTypes = "FuelTypes";
    }
}