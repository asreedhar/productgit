namespace MAX.Market
{
    public interface IMappingSearchProvider
    {
        VehicleDescription GetVehicleDescription(int chromeStyleId);
        VehicleSearchTermsByTrim GetVehicleSearchTerms(VehicleDescription vehicleDescription);
    }
}