﻿using System;

namespace Merchandising.Release.Scheduling.Service.Core
{
    /// <summary>
    /// http://www.quartz-scheduler.org/generated/2.2.1/html/qs-all/#page/Quartz_Scheduler_Documentation_Set%2Fco-trg_crontriggers.html%23wwconnect_header
    /// </summary>
    class CronExpressionBuilder
    {
        
        private string Seconds { get; set; }
        private string Minutes { get; set; }
        private string Hours { get; set; }
        private string DayOfMonth { get; set; }
        private string Month { get; set; }
        private string DayOfWeek { get; set; }
        private string Year { get; set; }
        private string _expression;

        public CronExpressionBuilder() { }

        public CronExpressionBuilder(string expression)
        {
            _expression = expression;
        }
        public CronExpressionBuilder(string seconds, string minutes, string hours, string dayOfMonth, string month,
            string dayOfWeek, string year = "*")
        {
            Seconds = seconds;
            Minutes = minutes;
            Hours = hours;
            DayOfMonth = dayOfMonth;
            Month = month;
            DayOfWeek = dayOfWeek;
            Year = year;
        }
        public string ToCronExpression()
        {
            if (!String.IsNullOrWhiteSpace(_expression)) return _expression;

            return String.Format("{0} {1} {2} {3} {4} {5} {6}", Seconds, Minutes, Hours, DayOfMonth, Month, DayOfWeek, Year);
        }
        
    }
}
