﻿using System;
using Autofac.Features.OwnedInstances;
using FirstLook.Common.Core.Logging;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Core
{
    internal class UnitOfWorkJob<T> : IJob where T : IJob
    {
        private readonly ILog Log = LoggerFactory.GetLogger<UnitOfWorkJob<T>>();

        private Func<Owned<T>> JobFactory { get; set; }

        public UnitOfWorkJob(Func<Owned<T>> jobFactory)
        {
            JobFactory = jobFactory;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                using (var inner = JobFactory())
                {
                    inner.Value.Execute(context);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to Execute() inner job object.", ex);
                throw;
            }
        }
    }
}