using System;
using Autofac;
using FirstLook.Common.Core.Logging;
using Quartz;
using Quartz.Spi;

namespace Merchandising.Release.Scheduling.Service.Core
{
    internal class AutofacJobFactory : IJobFactory
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AutofacJobFactory>();
        private IComponentContext Context { get; set; }

        public AutofacJobFactory(IComponentContext context)
        {
            Context = context;
        }

        public IJob NewJob(TriggerFiredBundle bundle)
        {
            try
            {
                return (IJob)Context.Resolve(bundle.JobDetail.JobType);
            }
            catch (Exception ex)
            {
                var type = bundle == null || bundle.JobDetail == null ? null : bundle.JobDetail.JobType;
                Log.Error(
                    string.Format("Failed to create IJob object of type '{0}'. ", type),
                    ex);
                throw;
            }
        }
    }
}