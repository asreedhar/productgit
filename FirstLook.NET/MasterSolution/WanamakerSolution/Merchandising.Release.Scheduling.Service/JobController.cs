﻿using System;
using System.Configuration;
using System.Threading;
using AutoLoad;
using Merchandising.Release.Scheduling.Service.Core;
using Merchandising.Release.Scheduling.Service.Jobs;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace Merchandising.Release.Scheduling.Service
{
    internal class JobController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly TimeZoneInfo _centralTimezone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
        
        private IJobFactory JobFactory { get; set; }
        private IScheduler Scheduler { get; set; }

        public JobController(IJobFactory jobFactory)
        {
            JobFactory = jobFactory;
        }

        public void Start()
        {
            Log.InfoFormat("OnStart() for Release.Scheduling.Service version {0} has been called.", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);

            var schedulerFactory = new StdSchedulerFactory();
            Scheduler = schedulerFactory.GetScheduler();
            Scheduler.JobFactory = JobFactory;
            Scheduler.Start();

            SchedulePeriodicJob("BookoutChange", TimeSpan.FromMinutes(10), typeof(NewBookoutJob));
            // FB 28137 - PriceChangeJob was replaced by ListPriceChangeJob
            //SchedulePeriodicJob("PriceChange", TimeSpan.FromMinutes(5), typeof(PriceChangeJob));
            SchedulePeriodicJob("ListPriceChangeJob", TimeSpan.FromMinutes(5), typeof(ListPriceChangeJob));
            SchedulePeriodicJob("MsrpUnitCostChangeJob", TimeSpan.FromMinutes(5), typeof(MsrpUnitCostChangeJob));

            SchedulePeriodicJob("NewInventory", TimeSpan.FromMinutes(30), typeof(NewInventoryJob));
            SchedulePeriodicJob("AutoOfflineProcessor", TimeSpan.FromMinutes(30), typeof(UnitOfWorkJob<SendProcessAutoOfflineJob>));
            SchedulePeriodicJob("PhotoTransfer", TimeSpan.FromHours(2), typeof(PhotoTransferJob));

            // TH 04/14/2014 -  This job is hammering MaxAnalytics at the same time we're trying to load data on that side.
            //                  Per Chad Sosna, it's okay to run this aggregation job just once a day.
            // ScheduleDailyJob("GroupLevelDashboardAggregationJob_Noon", 12, 0, typeof(GroupLevelDashboardAggregationJob));
            ScheduleDailyJob("GroupLevelDashboardAggregationJob_8PM", 20, 0, typeof(GroupLevelDashboardAggregationJob));
            ScheduleDailyJob("GoogleAnalyticsGenerationJob_1AM", 1, 0, typeof(GoogleAnalyticsGenerationJob));


            int startHour;
            int startMinute;

            // AutoWindowStickerReport FB:30986
            if (!Int32.TryParse(ConfigurationManager.AppSettings["AutoWindowStickerReportScheduleHour"], out startHour))
                startHour = 17;

            if (!Int32.TryParse(ConfigurationManager.AppSettings["AutoWindowStickerReportScheduleMinute"], out startMinute))
                startMinute = 0;

            ScheduleDailyJob("Window sticker Report", startHour, startMinute, typeof(AutoWindowStickerReportJob));


            ScheduleDailyJob("EmailUpdatesJob", 7, 0, typeof(EmailUpdatesJob));
            ScheduleDailyJob("DailyBucketAlertsJob", 18, 0, typeof(BucketAlertsJob));
            ScheduleDailyJob("WeeklyBucketAlertsJob", 7, 0, typeof(BucketAlertsJob));
            ScheduleDailyJob("DiscountCampaignExpirationJob", 1, 0, typeof(DiscountCampaignExpirationJob));
            ScheduleDailyJob("Missed_DiscountCampaignExpirationJob", 6, 0, typeof(DiscountCampaignExpirationJob));

            ScheduleDailyJob("ReprocessInventoryForCampaignsJob_0730am", 7, 30, typeof(ReprocessInventoryForCampaignsJob));
            ScheduleDailyJob("ReprocessInventoryForCampaignsJob_3pm", 15, 0, typeof(ReprocessInventoryForCampaignsJob));
            ScheduleDailyJob("ReprocessInventoryForCampaignsJob_10pm", 22, 0, typeof(ReprocessInventoryForCampaignsJob));

            // Expire campaigns when the service is started.
            ScheduleImmediateJob("ImmediateDiscountCampaignExpiration", typeof(DiscountCampaignExpirationJob));

            //clean up jobs due to vehicles becoming active, etc
            ScheduleDailyJob("MissedAutoLoadNight", 2, 0, typeof(AutoLoadMissedVehiclesJob));
            ScheduleDailyJob("AutoLoadOpenRequeue", 18, 30, typeof(AutoLoadOpenRequeueJob));

            ScheduleDailyJob("MissedAutoApproveDay", 15, 0, typeof(AutoApproveMissedVehiclesJob));
            ScheduleDailyJob("MissedAutoApproveNight", 3, 0, typeof(AutoApproveMissedVehiclesJob));

            // FB: 31341 - slurpee requeue job
            SchedulePeriodicJob("SlurpeeRequeue", TimeSpan.FromHours(1), typeof(AutoLoadSlurpeeRequeueJob));

            // FB: 29544
            ScheduleDailyJob("AutoWindowStickerJob_0600", 6, 00, typeof(AutoWindowStickerJob));
            // FB: 30178
            ScheduleDailyJob("WindowStickerClickStatsJob_1230", 12, 30, typeof(WindowStickerClickStatsJob));

            // Trigger inactive inventory messages
            ScheduleImmediateJob("InactiveInventoryJob-Immediate", typeof(InactiveInventoryJob));
            SchedulePeriodicJob("InactiveInventoryJob-Every24Hours", TimeSpan.FromHours(24), typeof(InactiveInventoryJob));

            // Manufacturer CPO feed -> Certified update & Auto-Approve job
            SchedulePeriodicJob("AutoSaveAndApproveCpoJob", TimeSpan.FromHours(1), typeof(AutoSaveAndApproveCpoJob));

            // Dealer document publish job - publishes dealer documents to cloudsearch.
            ScheduleDailyJob("PublishDealerCloudSearchDocs1", 8, 0, typeof(DealerDocumentPublishJob));
            ScheduleDailyJob("PublishDealerCloudSearchDocs2", 12, 0, typeof(DealerDocumentPublishJob));
            ScheduleDailyJob("PublishDealerCloudSearchDocs3", 16, 0, typeof(DealerDocumentPublishJob));
            ScheduleDailyJob("PublishDealerCloudSearchDocs4", 20, 0, typeof(DealerDocumentPublishJob));

            // Photo url document publish job - FB 29104
            //SchedulePeriodicJob("PublishPhotoDocs-Every4Hours", TimeSpan.FromHours(4), typeof(PhotoDocumentPublishJob));
            //ScheduleImmediateJob("PublishPhotoDocs-Immediate", typeof(PhotoDocumentPublishJob));
            ScheduleDailyJob("PublishPhotoDocs-Daily", 0, 30, typeof(PhotoDocumentPublishJob));
            //ScheduleDailyJob("PublishPhotoDocs-DailyAt2pm", 14, 0, typeof(PhotoDocumentPublishJob));

            #region Internal AutoLoads
            // Scheduled internal autoloads - they run 60 minutes after the inventory loads are expected to complete and populate FLDW.
            // 
            // Standard inventory loads start at: 00:30, 04:30, 07:30, 10:30, 13:30, 16:00, 19:00
            // They complete in ~45 minutes. New data is avaialble in FLDW in ~60 minutes.

            // Note that PROD runs these on a scheudle, whereas internal environemnts only run the job when the service starts.  This is because internal
            // environments don't run scheduled inventory loads so it's pointless to run this job on a schedule.
            Thread.Sleep(10000);
            bool internalAutoLoadRunOnSchedule;
            Boolean.TryParse(ConfigurationManager.AppSettings["InternalAutoLoad_RunOnSchedule"], out internalAutoLoadRunOnSchedule);

            if (internalAutoLoadRunOnSchedule)
            {
                Log.Info("Configuring FordDirectAutoLoadJob to run on schedule.");
                ScheduleDailyJob("InternalAutoLoad_1700", 17, 0, typeof(FordDirectAutoLoadJob));
            }

            // VDS based autoloads
            ScheduleDailyJob("VDSAutoLoad_0530", 5, 30, typeof(VdsAutoLoadJob));
            ScheduleDailyJob("VDSAutoLoad_1430", 14, 30, typeof(VdsAutoLoadJob));
            ScheduleDailyJob("VDSAutoLoad_2030", 20, 30, typeof(VdsAutoLoadJob));

            #endregion

            ScheduleCloudSearchJobs();

            string bionicEnabled = ConfigurationManager.AppSettings["BionicReportEnabled"];
            if (!String.IsNullOrWhiteSpace(bionicEnabled) && "true".Equals(bionicEnabled.ToLowerInvariant()))
            {
                string bionicCronExpression = ConfigurationManager.AppSettings["BionicReportScheduleCronExpr"];
                CronExpressionBuilder expression;
                if (String.IsNullOrWhiteSpace(bionicCronExpression))
                {
                    expression = new CronExpressionBuilder("0", "0", "5", "6W", "*", "?");
                    Log.InfoFormat("Individual Cron expression was null or whitespace, defaulting to '{0}'", expression.ToCronExpression());
                }
                else
                {
                    expression = new CronExpressionBuilder(bionicCronExpression);
                }
                ScheduleCustomJob("BionicReportJob", expression, typeof(BionicReportJob));
                Log.Info("Bionic reports job scheduled");
            }
            else
            {
                Log.Info("Bionic reports disabled.");
            }

            string groupBionicEnabled = ConfigurationManager.AppSettings["GroupBionicReportEnabled"];
            if (!String.IsNullOrWhiteSpace(groupBionicEnabled) && "true".Equals(groupBionicEnabled.ToLowerInvariant()))
            {
                string groupBionicCronExpression = ConfigurationManager.AppSettings["GroupBionicScheduleCronExpr"];
                CronExpressionBuilder expression;
                if (String.IsNullOrWhiteSpace(groupBionicCronExpression))
                {
                    expression = new CronExpressionBuilder("0", "0", "5", "6W", "*", "?");
                    Log.InfoFormat("Group Cron expression was null or whitespace, defaulting to '{0}'", expression.ToCronExpression());
                }
                else
                {
                    expression = new CronExpressionBuilder(groupBionicCronExpression);
                }
                ScheduleCustomJob("GroupBionicReportJob", expression, typeof(GroupBionicReportJob));
                Log.Info("Group Bionic reports job scheduled.");
            }
            else
            {
                Log.Info("Group Bionic reports disabled.");
            }
            


#if DEBUG
//            ScheduleImmediateJob("BookoutChange", typeof (NewBookoutJob));
//            ScheduleImmediateJob("PriceChange", typeof (PriceChangeJob));
//            ScheduleImmediateJob("NewInventory", typeof (NewInventoryJob));
//            ScheduleImmediateJob("AutoOfflineProcessor", typeof(UnitOfWorkJob<SendProcessAutoOfflineJob>));
//            ScheduleImmediateJob("PhotoTransfer", typeof(PhotoTransferJob));
//
//            ScheduleImmediateJob("GroupLevelDashboardAggregationJob", typeof (GroupLevelDashboardAggregationJob));
//            ScheduleImmediateJob("DocumentUpdater", typeof(SendDocumentUpdatesJob));
//            ScheduleImmediateJob("EmailUpdatesJob", typeof(EmailUpdatesJob));

//            ScheduleImmediateJob("DailyBucketAlertsJob", typeof(BucketAlertsJob));
//            ScheduleImmediateJob("WeeklyBucketAlertsJob", typeof(BucketAlertsJob));
//
//            ScheduleImmediateJob("MissedAutoLoadNight", typeof (AutoLoadMissedVehiclesJob));
//            ScheduleImmediateJob("AutoLoadOpenRequeue", typeof(AutoLoadOpenRequeue));
//            ScheduleImmediateJob("MissedAutoApproveDay", typeof(AutoApproveMissedVehiclesJob));
//            ScheduleImmediateJob("MissedAutoApproveNight", typeof(AutoApproveMissedVehiclesJob));
//            ScheduleImmediateJob("TimeToMarketReport", typeof(TimeToMarketGenerationJob));
//            ScheduleImmediateJob("InternalAutoLoad", typeof(FordDirectAutoLoadJob));
//            ScheduleImmediateJob("GoogleAnalyticsGenerationJob_1AM", typeof(GoogleAnalyticsGenerationJob));
//            ScheduleImmediateJob("ImmediateDiscountCampaignExpirationJob", typeof(DiscountCampaignExpirationJob));

//            ScheduleImmediateJob("InventoryActivityJob", typeof(InventoryActivityJob));
//            ScheduleImmediateJob("WindowStickerClickStatsJob", typeof(WindowStickerClickStatsJob));
//            ScheduleImmediateJob("AutoSaveAndApproveCpoJob", typeof(AutoSaveAndApproveCpoJob));
//            ScheduleImmediateJob("DealerDocumentPublishJob", typeof(DealerDocumentPublishJob));
//            ScheduleImmediateJob("PublishPhotoDocs", typeof(PhotoDocumentPublishJob));

//            ScheduleImmediateJob("BionicReportJob", typeof(BionicReportJob));
//            ScheduleImmediateJob("GroupBionicReportJob", typeof(GroupBionicReportJob));

#endif
            Log.Info("Done scheduling in OnStart");
        }

	    private void ScheduleCloudSearchJobs()
	    {
			bool runCloudSearchReconciler;
		    if (bool.TryParse(ConfigurationManager.AppSettings["RunCloudSearchReconciler"], out runCloudSearchReconciler) 
				&& runCloudSearchReconciler)
		    {
			    int startHour, startMin;

			    if (!int.TryParse(ConfigurationManager.AppSettings["CSRStartHour"], out startHour))
			    {
				    startHour = 22;
			    }

			    if (!int.TryParse(ConfigurationManager.AppSettings["CSRStartMinute"], out startMin))
			    {
				    startMin = 0;
			    }
			    ScheduleDailyJob("CloudSearch CleanUp Daily", startHour, startMin, typeof (CloudSearchReconciler.CloudSearch.CloudSearchReconciler));
		    }
	    }

	    private void SchedulePeriodicJob(string name, TimeSpan interval, Type jobType)
        {
            var detail = new JobDetail(name, null, jobType);
            var trigger = new SimpleTrigger(name + "_IntervalTrigger")
                              {
                                  StartTimeUtc = DateTime.UtcNow, 
                                  RepeatInterval = interval, 
                                  RepeatCount = -1
                              };
            Scheduler.ScheduleJob(detail, trigger);
        }

        private void ScheduleDailyJob(string name, int hour, int minute, Type jobType)
        {
            var detail = new JobDetail(name, null, jobType);
            var trigger = TriggerUtils.MakeDailyTrigger(name + "_CronTrigger", hour, minute);
            Scheduler.ScheduleJob(detail, trigger);
        }

        private void ScheduleMonthlyJob(String name, int day, int hour, int minute, Type jobType)
        {
            var detail = new JobDetail(name, null, jobType);
            var trigger = TriggerUtils.MakeMonthlyTrigger(name + "_MonthlyCronTrigger", day, hour, minute);
            Scheduler.ScheduleJob(detail, trigger);
        }

        /// <summary>
        /// Schedule a cron job using cron expressions.
        /// </summary>
        /// <param name="name">Job name</param>
        /// <param name="cronExpression">Seconds Minutes Hours Day-of-Month Month Day-of-Week Year(optional)</param>
        /// <param name="jobType">Job type</param>
        private void ScheduleCustomJob(string name, CronExpressionBuilder cronExpression, Type jobType)
        {
            var detail = new JobDetail(name, null, jobType);
            
            var trigger = new CronTrigger(name + "_CustomCronTrigger", "CustomTriggerGroup");
            trigger.CronExpression = new CronExpression(cronExpression.ToCronExpression());
            trigger.TimeZone = _centralTimezone;
            Scheduler.ScheduleJob(detail, trigger);
        }

        private void ScheduleImmediateJob(string name, Type jobType)
        {
            var detail = new JobDetail(name, null, jobType);
            var trigger = TriggerUtils.MakeImmediateTrigger(name + "_ImmediateTrigger", 0, TimeSpan.Zero);
            Scheduler.ScheduleJob(detail, trigger);
        }

        public void Stop()
        {
            // end immediately
        }
    }
}
