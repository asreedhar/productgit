﻿using System;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using AmazonServices;
using AutoLoad;
using Autofac;
using BulkWindowSticker;
using FirstLook.Common.Core.IOC;
using MAX.BionicReports;
using MAX.Caching;
using Merchandising.Release.Scheduling.Service.CloudSearchReconciler;
using Merchandising.Release.Scheduling.Service.Core;
using Quartz;
using Merchandising.Messages;
using MerchandisingRegistry;
using Quartz.Spi;

namespace Merchandising.Release.Scheduling.Service
{
    static class Program
    {
        private static IContainer _container;

        public static void Main(string[] args)
        {
            SetupAutofac();
            SetupLogging();

            if (args.Any(s => StringComparer.InvariantCultureIgnoreCase.Compare(s, "-console") == 0))
                ExecuteConsoleMode();
            else
                ExecuteServiceMode();
        }

        private static void ExecuteServiceMode()
        {
            ServiceBase.Run(_container.Resolve<Service>());
        }

        private static void ExecuteConsoleMode()
        {
            var workers = _container.Resolve<JobController>();

            try
            {
                workers.Start();
                Console.WriteLine("Jobs scheduling started. Press enter to stop...");
                Console.ReadLine();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Failed to start job scheduling: " + ex.Message + "\r\n" + ex.StackTrace);
            }
            finally
            {
                Console.WriteLine("Cleaning up job scheduling.");
                workers.Stop();
            }
        }

        private static void SetupLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        private static void SetupAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new MerchandisingDomainModule());
            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterModule(new AmazonServicesModule());
			builder.RegisterModule(new BionicReportsModule());
			builder.RegisterModule(new CloudSearchReconciliationModule());

            RegisterAssemblyTypes(builder);

            _container = builder.Build();
            Registry.RegisterContainer(_container);
        }

        private static void RegisterAssemblyTypes(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(t => t.IsAssignableTo<IJob>()).AsSelf();
            builder.RegisterType<AutofacJobFactory>().As<IJobFactory>();
            builder.RegisterType<JobController>();
            builder.RegisterType<Service>();
            builder.RegisterType<FordDirectAutoLoadJob>();
            builder.RegisterGeneric(typeof (UnitOfWorkJob<>));
            builder.RegisterType<AppSettingsRepository>().As<IAppSettingsRepository>();
            builder.RegisterType<WindowStickerRepository>().As<IWindowStickerRepository>();
	        builder.RegisterType<MemcachedClientWrapper>().As<ICacheWrapper>();
	        builder.RegisterType<CacheKeyBuilder>().As<ICacheKeyBuilder>();
        }
    }
}
