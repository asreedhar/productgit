﻿using System;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class AutoLoadSlurpeeRequeueJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AutoLoadSlurpeeRequeueJob>();

        private readonly IQueueFactory _queueFactory;

        public AutoLoadSlurpeeRequeueJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting AutoLoudSlurpeeRequeueJob");
                var queue = _queueFactory.CreateAutoLoadInventory();

                var messages = SlurpeeRequeue.Fetch();
                foreach (var message in messages)
                {
                    queue.SendMessage(message, GetType().FullName);
                    Log.DebugFormat("AutoLoudSlurpeeRequeueJob sent for BU: {0}", message.BusinessUnitId);
                }
            }
            catch (Exception e)
            {
                Log.Error("Quartz: Error in AutoLoudSlurpeeRequeueJob", e);
            }
        }

    }
}
