﻿using System;
using System.IO;
using System.Linq;
using Core.Messaging;
using Quartz;


namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public abstract class EventJobBase<TValue> : IStatefulJob
    {
        private string _fileName;
        private IAppSettingsRepository _repository;

        protected EventJobBase(string fileName, IAppSettingsRepository repository)
        {   
            _fileName = fileName;
            _repository = repository;
        }

        private void SaveLastInventory(IFileStorage storage, int lastEventId)
        {
            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(lastEventId);
                writer.Flush();
                stream.Position = 0;
                storage.Write(_fileName, stream);
            }
        }

        internal int GetLastEventId(IFileStorage storage)
        {
            // change to prevent the Amazon bus from being flooded on a database (or S3) restore. tureen 10/17/2013 - FB:27762
            // maxiumum difference stored in config, if difference between file (on S3) and max database id is exceeded, the s3 values is reset so no events are posted

            int eventId = -1;
            int databaseMaxEventID = -1;
            int maxBusEventOverride = -1;
            bool bResetEventID = false;

            // get the max variance from the configuration
            string sMaxEvents = _repository.Fetch("maxBusEventDelta");
            if (!Int32.TryParse(sMaxEvents, out maxBusEventOverride))
                maxBusEventOverride = -1;

            // get the last event value from the database
            databaseMaxEventID = GetMaxEvent();

            // get the last processed value from the file on S3
            bool exists;
            using (var stream = storage.Exists(_fileName, out exists))
            {
                if (exists)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string value = reader.ReadToEnd();
                        if (!Int32.TryParse(value, out eventId))
                            eventId = -1;
                    }
                }
            }

            // if the value on S3 is not valid
            if (eventId == -1 )
                bResetEventID = true;

            // is s3 is higher than database, send database value
            if (eventId > databaseMaxEventID)
                bResetEventID = true;

            // if the max bus event variance is set and the difference is above the tolerance, reset the S3 value
            if (maxBusEventOverride > 0 && databaseMaxEventID - eventId >= maxBusEventOverride)
                bResetEventID = true;


            //  if a condition exists requires a reset, update the S3 value and return the database value instead of the last s3 value
            if (bResetEventID)
            {
                eventId = databaseMaxEventID;
                SaveLastInventory(storage, eventId);
            }

            return eventId;
        }

        protected void Process(IFileStorage storage)
        {
            int lastValue = GetLastEventId(storage);
            var newValues = GetNewValues(lastValue);

            foreach (var inventory in newValues)
                Publish(inventory);

            if (newValues.Length > 0) //list is sorted
                SaveLastInventory(storage, GetNewValue(newValues.First()));
        }

        protected abstract void Publish(TValue value);
        protected abstract TValue[] GetNewValues(int lastValue);
        //unit test
        public abstract int GetMaxEvent();
        protected abstract int GetNewValue(TValue value);

        public abstract void Execute(JobExecutionContext context);
    }
}
