﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using Core.Messaging;
using log4net;
using MAX.VDS;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Quartz;
using ILog = log4net.ILog;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class VdsAutoLoadJob : IStatefulJob
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IQueueFactory _queueFactory;

        public VdsAutoLoadJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting VdsAutoLoadJob");
                var queue = _queueFactory.CreateAutoLoadInventory();

                var configItems = GetConfigItems();
                IssueAutoloadMessages(queue, configItems);
            }
            catch (Exception e)
            {
                Log.Error("Quartz: Error in VdsAutoLoadJob", e);
            }
        }

        private IEnumerable<VdsAutoLoadConfigItem> GetConfigItems()
        {
            string configString = ConfigurationManager.AppSettings["VdsAutoloadConfigItems"];
            var parser = new VdsAutoLoadConfigParser();
            var configItems = parser.ParseVdsConfigItemsText(configString);
            return configItems;
        }

        private void IssueAutoloadMessages(IQueue<AutoLoadInventoryMessage> queue, IEnumerable<VdsAutoLoadConfigItem> configItems )
        {
            var senderName = GetType().FullName;

            foreach (var item in configItems)
            {
                foreach (var store in item.Stores)
                {
                    int businessUnitId;
                    bool convert = Int32.TryParse(store, out businessUnitId);

                    if (convert)
                    {
                        var message = new AutoLoadInventoryMessage(businessUnitId, AutoLoadInventoryType.NoStatus);                        
                        queue.SendMessage(message, senderName);
                    }
                }
            }
            
        }

    }
}
