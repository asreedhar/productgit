﻿using System;
using System.Reflection;
using System.Text;
using Core.Messages;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Quartz;
using System.Linq;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class NewInventoryJob : EventJobBase<NewInventoryMessage>
    {
        private static readonly TimeSpan QueueDelay = TimeSpan.FromSeconds(60);

        private IFileStoreFactory FileStoreFactory { get; set; }
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string FileName = "LastInventoryId";

        private IBus<NewInventoryMessage, NewInventoryMessage> _bus;

        public NewInventoryJob(IFileStoreFactory fileStoreFactory, IAppSettingsRepository repository)
            : base(FileName, repository)
        {
            FileStoreFactory = fileStoreFactory;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {
                var storage = FileStoreFactory.CreateFileStore();

                _bus = BusFactory.CreateNewInventory<NewInventoryMessage, NewInventoryMessage>();

                Process(storage);
            }
            catch (Exception e)
            {
                Log.Error("Error in NewInventoryJob", e);    
            }
        }

        protected override void Publish(NewInventoryMessage message)
        {
            _bus.Publish(message, GetType().FullName);
        }

        protected override NewInventoryMessage[] GetNewValues(int lastValue)
        {
            var newValues =  LastInventory.GetNewInventory(lastValue);
            Log.DebugFormat("{0} New Inventory Found", newValues.Length);

            if(newValues.Length > 0) //log info about new inventory
            {
                Log.DebugFormat("S3 last inventory value = {0}", lastValue);
                Log.DebugFormat("Range {0}-{1}", newValues[0].InventoryId, newValues[newValues.Length - 1].InventoryId);
                var stringValues = newValues.Select(x => x.InventoryId.ToString());
                Log.DebugFormat("Values = {0}", string.Join(",", stringValues));
            }

            return newValues;
        }

        public override int GetMaxEvent()
        {
            return LastInventory.GetMaxInventoryId();
        }

        protected override int GetNewValue(NewInventoryMessage value)
        {
            return value.InventoryId;
        }
    }
}
