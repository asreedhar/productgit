﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Release;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Merchandising.Messages.AutoApprove;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
	public class AutoSaveAndApproveCpoJob : IStatefulJob
	{
		private const string Username = "max_cpo_updater";

		private static readonly ILog Log = LoggerFactory.GetLogger<AutoSaveAndApproveCpoJob>();

		public IAdMessageSender MessageSender { get; set; }

		public AutoSaveAndApproveCpoJob()
		{
			MessageSender = Registry.Resolve<IAdMessageSender>();
		}

		public void Execute(JobExecutionContext context)
		{
			try
			{
				Log.Info("Quartz: Starting AutoSaveAndApproveCpoJob");

				// a dictionary of programs for each businessunit/make we encounter
				var dealerPrograms = new Dictionary<BusinessUnitMakeKey, IEnumerable<CertifiedProgramInfoTO>>();

				// get the inventory items that do not have the correct program saved based on the current feeds.
				var cpoUpdates = CpoFeedUpdates.Fetch();

				// process each update
				foreach (var cpoUpdate in cpoUpdates)
				{
					var programkey = new BusinessUnitMakeKey(cpoUpdate.BusinessUnitId, cpoUpdate.Make);

					if (!dealerPrograms.ContainsKey(programkey))
					{
						var certifiedProgamCommand = new CertifiedProgamsForDealerMakeCommand(cpoUpdate.BusinessUnitId, cpoUpdate.Make);
						AbstractCommand.DoRun(certifiedProgamCommand);
						dealerPrograms.Add(programkey, certifiedProgamCommand.Programs);
					}

					// we should at least get an empty list if nothing is valid for this dealer/make.
					if (dealerPrograms[programkey] == null)
						throw new ApplicationException(string.Format("No programs were found for businessunitId: {0}, make: {1}", programkey.BusinessUnitId, programkey.Make ));

					// valid programs for this dealer/make
					var cpoPrograms = dealerPrograms[programkey];

					// these should all be true, but not going to assume that.
					var oemCertified = cpoPrograms.Any(x => x.Id == cpoUpdate.CertifiedProgramId && x.ProgramType == "MFR");

					var certifiedId = string.Empty;  // we don't have any certifiedIds coming in on feeds (yet).

					// create or update the certification status for this vehicle
					VehicleCertification.CreateOrUpdate(cpoUpdate.BusinessUnitId, cpoUpdate.InventoryId, oemCertified,
						cpoUpdate.CertifiedProgramId, certifiedId, Username, true);

					// send an autoapprove message to incorporate the newly saved program & benefits into the ad
					MessageSender.SendAutoApproval(new AutoApproveMessage(cpoUpdate.BusinessUnitId, cpoUpdate.InventoryId, Username), GetType().FullName);
				}

				Log.DebugFormat("{0} AutoApprove Message Count: {1}", GetType().FullName, cpoUpdates.Length);
			}
			catch (Exception e)
			{
				Log.Error("Quartz: Error in AutoSaveAndApproveCpoJob", e);
			}	
		
		}


		private class BusinessUnitMakeKey
		{
			internal BusinessUnitMakeKey(int businessUnitId, string make)
			{
				BusinessUnitId = businessUnitId;
				Make = make;
			}
			internal int BusinessUnitId { get; set; }
			internal string Make { get; set; }
		}
	}
}