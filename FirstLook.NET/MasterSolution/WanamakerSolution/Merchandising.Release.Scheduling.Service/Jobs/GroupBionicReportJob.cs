﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using FirstLook.Merchandising.DomainModel.Release;
using MAX.BionicReports.Queues;
using MAX.BionicReports.Utility;
using MAX.Entities.Messages;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class GroupBionicReportJob : EmailJobBase
    {
        #region Properties
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int MinimumMaxVersion = 3;

        private readonly IFileStoreFactory _bionicFileStoreFactory;
        private readonly IBionicReportQueueFactory _bionicReportQueueFactory;
        private readonly IMaxDealersRepository _maxDealersRepository;
        #endregion

        #region Methods
        public GroupBionicReportJob(IBionicReportQueueFactory bionicReportQueueFactory, IFileStoreFactory bionicFileStoreFactory, IMaxDealersRepository maxDealersRepository)
        {
            _bionicReportQueueFactory = bionicReportQueueFactory;
            _bionicFileStoreFactory = bionicFileStoreFactory;
            _maxDealersRepository = maxDealersRepository;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("GroupBionicReportJob: Starting scheduled job");
                
                var trackingStore = _bionicFileStoreFactory.CreateBionicTrackingStorage();
                var queue = _bionicReportQueueFactory.CreateGroupBionicReportDataCollectionQueue();
                var whiteList = GetBionicWhiteList();
                var getMiscSettings = new GetMiscSettings(-1);
                var getBucketAlerts = new GetBucketAlerts(-1);

                var litmusUsername = ConfigurationManager.AppSettings["LitmusAPIAccountUsername"];
                var litmusPassword = ConfigurationManager.AppSettings["LitmusAPIAccountPassword"];
                var litmusEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["LitmusAPIEnabled"].ToLowerInvariant());
                
                string trackingCode;

                if (litmusEnabled)
                {
                    var trackingGenerator = new LitmusCurrentMonthCampaign(LitmusCurrentMonthCampaign.Type.GroupBionic,
                        trackingStore,
                        litmusUsername, litmusPassword);
                    if (!trackingGenerator.Success)
                    {
                        Log.Fatal("LitmusCampaign creator failed to create and or save campaign to persistence layer. Aborting email creation run.");
                        return;
                    }
                    else
                    {
                        trackingCode = trackingGenerator.TrackingCode;
                    }
                }
                else
                {
                    trackingCode = "<!--{emailAddress}-->"; // make email template transformer happy, it expects an {emailAddress} tag to replace
                }

                // Get the list of active business units for version
                var activeBusinessUnitIds = _maxDealersRepository.FindActiveMaxDealerIdsAboveVersion(MinimumMaxVersion).ToList();

                Log.Trace(String.Format("Active MAX dealers above version {0}: {1}", MinimumMaxVersion, activeBusinessUnitIds.Count));

                // Get the list of groups
                var ttmGroups = (
                    from dealerGroup in BusinessUnitFinder.Instance().FindAllDealerGroups(null)
                    from dealer in dealerGroup.Dealerships()
                    join active in activeBusinessUnitIds on dealer.Id equals active
                    // list of business units that are active and with TTM turned on
                    join ms in getMiscSettings.GetAllMiscSettings() on dealer.Id equals ms.BusinessUnitId
                    where ms.ShowTimeToMarket
                    // list of business units that have monthly group emails setup
                    join ba in getBucketAlerts.GetAllBucketAlertSettings() on dealer.Id equals ba.BusinessUnitId
                    where ba.Active && ba.GroupBionicHealthReportEmails.Length > 0
                    // with at least one email that can be sent to
                    from email in ba.GroupBionicHealthReportEmails
                    where !String.IsNullOrWhiteSpace(email) && CanSend(whiteList, email)
                    select new {groupId = dealerGroup.Id, businessUnitId = dealer.Id, ba.GroupBionicHealthReportEmails}).ToList();

                Log.Trace(String.Format("Count of TTM business units within groups: {0}", ttmGroups.Count));

                var finalGroups = new Dictionary<int, List<String>>();
                foreach (var g in ttmGroups)
                {
                    var allowedEmails =
                        g.GroupBionicHealthReportEmails.Where(
                            e => !String.IsNullOrWhiteSpace(e) && CanSend(whiteList, e)).ToList();

                    if (!allowedEmails.Any()) continue;

                    if (!finalGroups.ContainsKey(g.groupId))
                        finalGroups.Add(g.groupId, new List<String>());

                    finalGroups[g.groupId].AddRange(allowedEmails);
                }

                Log.Trace(String.Format("Final count of groups: {0}", finalGroups.Count));

                // creates the list of messages with the business unit ids and emails to start the process for
                var bionicMessages = (
                    from g in finalGroups
                    select new GroupBionicReportDataCollectionMessage(g.Key, String.Join(",", g.Value.Distinct()), trackingCode)).ToList();

                Log.Trace(String.Format("GroupBionicReportDataCollecitonMessages to be queued: {0}", bionicMessages.Count));

                // send the messages into the queue
                foreach (var message in bionicMessages)
                    queue.SendMessage(message, GetType().FullName);
            }
            catch (Exception e)
            {
                Log.Error("GroupBionicReportJob Error: ", e);
            }
            finally
            {
                Log.Info("GroupBionicReportJob: Ending scheduled job");
            }
        }
        #endregion
    }
}
