﻿using System;
using System.Collections.Generic;
using System.Configuration;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Merchandising.Messages.AutoWindowSticker;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class AutoWindowStickerReportJob : EmailJobBase
    {
        private readonly IQueueFactory _queueFactory;
        private static readonly ILog Log = LoggerFactory.GetLogger<AutoWindowStickerReportJob>();
        
        public AutoWindowStickerReportJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {

                Log.Debug("AutoWindowStickerReportJob: Starting schedule job");

                // create queue for sending messages
                var queue = _queueFactory.CreateWindowStickerReportQueue();

                // get email list from configuration settings
                var recipientList = ConfigurationManager.AppSettings["AutoWindowStickerReportEmailList"];
                if(string.IsNullOrWhiteSpace(recipientList))
                {
                    Log.Debug("AutoWindowStickerReportJob: No email addresses are configured");
                }
                else
                {
                    
                    // get the number of days
                    int daysToReport;
                    var configuredDays = ConfigurationManager.AppSettings["AutoWindowStickerReportDaysToReport"];
                    if (!int.TryParse(configuredDays, out daysToReport))
                        daysToReport = 14;

                    // get the white list from the config
                    var whiteList = GetWhiteList();

                    var potentialEmailList = recipientList.Split(',');
                    var validEmailList = new List<string>();

                    foreach (var potentialEmail in potentialEmailList)
                    {
                        // white list will be empty in prod, otherwise check it
                        if (CanSend(whiteList, potentialEmail))
                            validEmailList.Add(potentialEmail);
                        else
                            Log.DebugFormat("AutoWindowStickerReportJob: Non white list email: {0} eliminated",potentialEmail);
                    }

                    if (validEmailList.Count > 0)
                    {
                        // send message
                        var finalList = string.Join(",", validEmailList);
                        queue.SendMessage(new AutoWindowStickerReport(finalList, daysToReport), "AutoWindowStickerReportJob");

                    }
                    else
                    {
                        // log results
                        Log.Debug("AutoWindowStickerReportJob: No non-white listed emails are configured for job");
                    }


                }

                Log.Debug("AutoWindowStickerReportJob: Ending schedule job");


            }
            catch (Exception e)
            {
                Log.Error("AutoWindowStickerReportJob Error: ", e);
            }
            finally
            {
                Log.Info("AutoWindowStickerReportJob: Ending schedule job");
            }

        }

    }
}
