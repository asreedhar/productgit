﻿using System;
using System.Linq;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class PhotoDocumentPublishJob : IStatefulJob
    {
        private IDocumentPublishingQueueFactory QueueFactory { get; set; }
        private static readonly ILog Log = LoggerFactory.GetLogger<PhotoDocumentPublishJob>();

        public PhotoDocumentPublishJob(IDocumentPublishingQueueFactory queueFactory)
        {
            QueueFactory = queueFactory;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                var queue = QueueFactory.MaxPublishDealerPhotoDocsQueue();

                var messages = GetMessagesForActiveBusinessUnits();

                if (messages != null && messages.Length > 0)
                {
                    foreach (var message in messages)
                    {
                        Log.DebugFormat("Queueing message for BU:{0}", message.BusinessUnitId);

                        // send the message to the queue
                        queue.SendMessage(message, GetType().FullName);
                    }
                }

                Log.Info("Quartz: Queueing messages for photo document generation completed");
            }
            catch (Exception e)
            {
                Log.Error("Error in Photo document generation Job", e);
            }
        }

        private DealerCommand[] GetMessagesForActiveBusinessUnits()
        {
            return (new MaxDealersRepository()).FindAllActiveMaxDealers()
                .Select(dlr => new DealerCommand(dlr))
                .ToArray();
        }
    }
}