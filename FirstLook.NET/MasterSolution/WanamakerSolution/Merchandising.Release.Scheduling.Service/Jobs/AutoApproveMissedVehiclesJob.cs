﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Quartz;


namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class AutoApproveMissedVehiclesJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AutoApproveMissedVehiclesJob>();

        private IQueueFactory _queueFactory;

        public AutoApproveMissedVehiclesJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting AutoApproveMissedVehiclesJob");
                var queue = _queueFactory.CreateAutoApproveInventory();

                var messages = MissedAutoApprove.Fetch();
                Log.DebugFormat("MissedAutoApprove Message Count: {0}", messages.Length);
                foreach(var message in messages)
                    queue.SendMessage(message, GetType().FullName);
            }
            catch (Exception e)
            {
                Log.Error("Quartz: Error in AutoApproveMissedVehiclesJob", e);
            }
        }

        
    }
}
