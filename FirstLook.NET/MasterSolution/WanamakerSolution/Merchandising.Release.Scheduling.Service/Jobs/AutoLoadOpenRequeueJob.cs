﻿using System;
using System.Reflection;
using FirstLook.Common.Core.Logging;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class AutoLoadOpenRequeueJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AutoLoadOpenRequeueJob>();
        private static readonly string DeclaringTypeName = MethodBase.GetCurrentMethod().DeclaringType.Name;

        private readonly IQueueFactory _queueFactory;

        public AutoLoadOpenRequeueJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                var queue = _queueFactory.CreateAutoLoadInventory();

                // only inserting one row, the task know what to do
                var message = new AutoLoadInventoryMessage(0, AutoLoadInventoryType.OpenRequeue);

                queue.SendMessage(message, DeclaringTypeName);
                Log.Info("AutoLoad Open Requeue message sent");

            }
            catch (Exception e)
            {
                Log.Error("Quartz: Error in AutoLoadMissedVehiclesJob", e);
            }
        }

    }
}
