﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;

using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    /// <summary>
    /// Issue a InventoryActivityTaskStartMessage for inventory that has become inactive. 
    /// </summary>
    public class InactiveInventoryJob : IStatefulJob
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IQueueFactory _queueFactory;
        private readonly MaxDealersRepository _maxDealersRepository;

        public InactiveInventoryJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
            _maxDealersRepository = new MaxDealersRepository();
        }

        public void Execute(JobExecutionContext context)
        {
            var sw = new Stopwatch();
            sw.Start();
            Log.Info("InactiveInventoryJob starting.");

            // Get the active max dealers.
            IEnumerable<int> businessUnitIds = _maxDealersRepository.FindAllActiveMaxDealers();
            var buids = businessUnitIds.ToList();

            ProcessInActiveInventory(buids);

            sw.Stop();
            Log.InfoFormat("InactiveInventoryJob finished in {0}.", sw.Elapsed.ToString());
        }

        private void ProcessInActiveInventory(IEnumerable<int> businessUnitIds)
        {
            var buids = businessUnitIds.ToList();

            Log.DebugFormat("InactiveInventoryJob issuing InventoryActivityTaskStartMessage (process inactive inventory) for all {0} active MAX dealer", 
                buids.Count);

            // send the start message
            var startMessage = new InactiveInventoryTaskStartMessage
            {
                BusinessUnitIds = buids
            };
            var controlQueue = _queueFactory.CreateInactiveInventoryTaskControlQueue();
            controlQueue.SendMessage(startMessage, "InactiveInventoryJob");                
        }
    }
}
