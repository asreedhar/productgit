﻿using System;
using System.Collections.Generic;
using System.Reflection;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Merchandising.Messages;
using Merchandising.Messages.VehicleTransfer;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class PhotoTransferJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<PhotoTransferJob>();
        private IQueueFactory QueueFactory { get; set; }

        private IPhotoTransferDataAccess dao = null;

        public PhotoTransferJob(IQueueFactory queueFactory, IPhotoTransferDataAccess dao)
        {
            QueueFactory = queueFactory;
            this.dao = dao;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                var queue = QueueFactory.CreatePhotoTransferQueue();

                foreach (PhotoTransferData dto in dao.FetchNewTransfers())
                {
                    var msg = new PhotoTransferMessage(dto.Vin, dto.SourceBusinessUnitId, dto.SourceBusinessUnitCode, dto.SourceInventoryId, dto.DestinationBusinessUnitId, dto.DestinationBusinessUnitCode, dto.DestinationInventoryId);
                    Log.Debug("Queuing " + msg);
                    queue.SendMessage(msg, GetType().FullName);
                    dao.TrackTransfer(msg.SrcInventoryId, msg.DstInventoryId);
                }
            }
            catch (Exception e)
            {
                Log.Error("Error in PhotoTransferJob", e);
            }
        }
    }
}
