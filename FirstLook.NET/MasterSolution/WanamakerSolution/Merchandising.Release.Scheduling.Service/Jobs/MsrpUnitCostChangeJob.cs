﻿using System;
using System.IO;
using Core.Messages;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.QueueFactories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class MsrpUnitCostChangeJob : IStatefulJob
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IFileStoreFactory _fileStoreFactory;
        private IDiscountCampaignQueueFactory _queueFactory;
        private string _fileName = "LastMsrpUnitCostChangeCheck";

        public IFileStoreFactory FileStoreFactory
        {
            get
            {   
                if(_fileStoreFactory == null)
                    _fileStoreFactory = new FileStoreFactory();
                return _fileStoreFactory ;
            }
            set { _fileStoreFactory = value; }
        }

        public MsrpUnitCostChangeJob(IDiscountCampaignQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
        }
        public void Execute(JobExecutionContext context)
        {
            try
            {
                var storage = FileStoreFactory.CreateFileStore();
                var queue = _queueFactory.CreateApplyInventoryDiscountCampaignQueue();

                DateTime earliest = GetLastCheck(storage);
                DateTime lastest = LastPrice.GetDatabaseDateTime();

                InventoryMessage[] messages = GetUpdatedInventory(earliest, lastest);

                Log.InfoFormat("Processing MSRP and unit cost changes from {0} - {1}.  {2} message(s) to process.", earliest, lastest, messages.Length);

                foreach (var message in messages)
                {
					Log.InfoFormat("Sending cost_change message for BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
                    queue.SendMessage(new ApplyInventoryDiscountCampaignMessage(message.BusinessUnitId, message.InventoryId, "cost_change", true), this.GetType().Name);
                }

                Log.InfoFormat("Processing MSRP and unit cost completed");

                // save the date of the last check
                SaveLastCheck(storage, lastest);


            }
            catch (Exception e)
            {
                Log.Error("Error in MsrpUnitCostChangeJob", e);
            }
        }
        protected virtual InventoryMessage[] GetUpdatedInventory(DateTime earliest, DateTime lastest)
        {
            return LastPrice.GetInventoryWithMsrpUnitCostChanges(earliest, lastest);
        }
        protected virtual void SaveLastCheck(IFileStorage storage, DateTime lastCheck)
        {
            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(lastCheck.ToString());
                writer.Flush();
                stream.Position = 0;
                storage.Write(_fileName, stream);
            }
        }
        protected virtual DateTime GetLastCheck(IFileStorage storage)
        {
            DateTime lastCheck = LastPrice.GetDatabaseDateTime();

            // get the last time that we checked for a list price change
            bool exists;
            using (var stream = storage.Exists(_fileName, out exists))
            {
                if (exists)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string value = reader.ReadToEnd();
                        DateTime lastCheckFromS3;
                        if (DateTime.TryParse(value, out lastCheckFromS3))
                            lastCheck = lastCheckFromS3;
                    }
                }
            }

            return lastCheck;
        }
         
    }
}