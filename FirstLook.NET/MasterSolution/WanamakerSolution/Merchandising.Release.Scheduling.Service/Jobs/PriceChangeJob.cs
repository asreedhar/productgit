﻿using System;
using Core.Messages;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class PriceChangeJob : EventJobBase<PriceChangeMessage>
    {
        private IFileStoreFactory FileStoreFactory { get; set; }
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string FileName = "LastAipEventID";

        private IBus<PriceChangeMessage, PriceChangeMessage> _bus;

        //This is needed because there is a separate source of price change events.  IMT.dbo.Inventory_ListPriceHistory
        private readonly LphPriceChangeJob _lphPriceChangeJob;

        public PriceChangeJob(IFileStoreFactory fileStoreFactory,  IAppSettingsRepository repository)
            : base(FileName, repository)
        {
            FileStoreFactory = fileStoreFactory;

            _lphPriceChangeJob = new LphPriceChangeJob( fileStoreFactory, repository);
        }

        protected override void Publish(PriceChangeMessage message)
        {
            _bus.Publish(message, GetType().FullName);
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {
                var storage = FileStoreFactory.CreateFileStore();

                _bus = BusFactory.CreatePriceChange<PriceChangeMessage, PriceChangeMessage>();

                Process(storage);

                _lphPriceChangeJob.Execute( context );
            }
            catch(Exception e)
            {
                Log.Error("Error in PriceChangeJob", e);
            }
        }

        protected override PriceChangeMessage[] GetNewValues(int lastValue)
        {
            var newPriceChanges = LastPrice.GetNewPriceChanges(lastValue);
            Log.DebugFormat("{0} New Price Changes Found", newPriceChanges.Length);
            
            return newPriceChanges;
        }

        public override int GetMaxEvent()
        {
            return LastPrice.GetMaxEventId();
        }

        protected override int GetNewValue(PriceChangeMessage value)
        {
            return value.EventId;
        }

        private class LphPriceChangeJob : EventJobBase<PriceChangeMessage>
        {
            private IFileStoreFactory FileStoreFactory { get; set; }

            private const string LphFileName = "LastLphEventID";

            private IBus<PriceChangeMessage, PriceChangeMessage> _bus;

            public LphPriceChangeJob(IFileStoreFactory fileStoreFactory, IAppSettingsRepository repository)
                : base( LphFileName, repository)
            {
                FileStoreFactory = fileStoreFactory;
            }

            protected override void Publish (PriceChangeMessage message)
            {
                _bus.Publish(message, GetType().FullName);
            }

            public override void Execute (JobExecutionContext context)
            {
                try
                {
                    var storage = FileStoreFactory.CreateFileStore();

                    _bus = BusFactory.CreatePriceChange<PriceChangeMessage, PriceChangeMessage>();

                    Process( storage );
                }
                catch ( Exception e )
                {
                    Log.Error( "Error in PriceChangeJob", e );
                }
            }

            protected override PriceChangeMessage[] GetNewValues (int lastValue)
            {
                var newPriceChanges = LastPrice.GetNewLphPriceChanges( lastValue );
                Log.DebugFormat("{0} LphPrice Price Changes Found", newPriceChanges.Length);

                return newPriceChanges;
            }

            public override int GetMaxEvent ()
            {
                return LastPrice.GetLphMaxEventId();
            }

            protected override int GetNewValue (PriceChangeMessage value)
            {
                return value.EventId;
            }

        }
    }
}
