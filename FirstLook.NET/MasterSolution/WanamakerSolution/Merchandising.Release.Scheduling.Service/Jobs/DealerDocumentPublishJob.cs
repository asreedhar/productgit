using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using AmazingCloudSearch.MaxInterface;
using AmazingCloudSearch.MaxInterface.Documents;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Commands;
using log4net;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
	public class DealerDocumentPublishJob : IStatefulJob
	{
	    #region Logging

	    protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

	    #endregion Logging

		public void Execute(JobExecutionContext context)
		{
			/*
			This code could be risky because we could possibly end up with part of our updates/deletes taking place,
			which would leave our last snapshot recorded inconsistent with cloudsearch.  If you see exceptions in this code, we 
			should revisit this approach as well as figure out how to get cloudsearch consistent again.
			
			*Possible* refresh data plan:
			 * delete all dealer docs from cloudsearch domain
			 * truncate snapshot table
			 * run this job
			
			*/
            Log.InfoFormat("Beginning DealerDocumentPublishJob for {0}", DateTime.Now);
			var csDomain =
				CloudSearchFactory<DealerSearchDocument>.CreateCloudSearchConnection(
					ConfigurationManager.AppSettings["dealer_cloud_search_url"]);
	
			var getcmd = new GetDealerProfiles();
			AbstractCommand.DoRun(getcmd);
			var dealerSearchDocs = getcmd.ChangedDealerProfiles.Select(FromDealerProfile).ToList();

			if (dealerSearchDocs.Count > 0)
			{
                Log.InfoFormat("Adding {0} records to CloudSearch.", dealerSearchDocs.Count);
				var addresult = csDomain.Add(dealerSearchDocs);

				if (addresult.IsError)
					throw new ApplicationException(
						string.Format(
							"An error was encountered uploading dealer search documents.  Total document adds/updates attempted:{0}, total added:{1}",
							dealerSearchDocs.Count, addresult.adds));
			}

			var dealerSearchDeletes = getcmd.DeleteOwnerHandleList.Select(deleteOwnerHandle => new DealerSearchDocument {OwnerHandle = deleteOwnerHandle}).ToList();

			if (dealerSearchDeletes.Count > 0)
			{
                Log.InfoFormat("Deleting {0} records from CloudSearch.", dealerSearchDeletes.Count);
				var deleteResult = csDomain.Delete(dealerSearchDeletes);

				if (deleteResult.IsError)
					throw new ApplicationException(
						string.Format(
							"An error was encountered deleting dealer search documents.  Total document deletes attempted:{0}, total deleted:{1}",
							dealerSearchDeletes.Count, deleteResult.deletes));
			}


			if (dealerSearchDocs.Count > 0 || dealerSearchDeletes.Count > 0)
			{
				// we had something to actually do, and everything *seemed* to work, save the snapshot as having been published.
				Log.InfoFormat("Saving the current dealer search snapshot.");
                var saveSnapshotCommand = new SaveDealerProfilesAsPublished(getcmd.AllDealerProfiles);
				AbstractCommand.DoRun(saveSnapshotCommand);
			}
            Log.InfoFormat("Job Finished.");
		}

		private static DealerSearchDocument FromDealerProfile(DealerProfile dealerProfile)
		{
			return new DealerSearchDocument
			{
				Address = dealerProfile.Address,
				BusinessUnitCode = dealerProfile.BusinessUnitCode,
				BusinessUnitId = dealerProfile.BusinessUnitID,
				BusinessUnitName = dealerProfile.BusinessUnitName,
				City = dealerProfile.City,
				Description = dealerProfile.Description,
				Email = dealerProfile.Email,
				GroupId = dealerProfile.GroupId,
				OwnerHandle = dealerProfile.OwnerHandle,
				Phone = dealerProfile.Phone,
				State = dealerProfile.State,
				Url = dealerProfile.Url,
                LogoUrl = GetLogoUrl(dealerProfile.LogoUrl),
				ZipCode = dealerProfile.ZipCode,
				HasMaxForWebsite1Upgrade = dealerProfile.HasMaxForWebsite1,
				HasMaxForWebsite2Upgrade = dealerProfile.HasMaxForWebsite2,
				HasMobileShowroomUpgrade = dealerProfile.HasMobileShowroom,
				HasShowroomUpgrade = dealerProfile.HasShowroom,
                HasShowroomBookValuations = dealerProfile.HasShowroomBookValuations,
                HasNewVehiclesEnabled = dealerProfile.HasNewVehiclesEnabled,
                HasUsedVehiclesEnabled = dealerProfile.HasUsedVehiclesEnabled
			};
		}

	    private static string GetLogoUrl(string logoUrl)
	    {
	        if (string.IsNullOrEmpty(logoUrl))
	            return String.Empty;
	        
            if (Uri.IsWellFormedUriString(logoUrl, UriKind.Absolute))
	            return logoUrl;

	        return string.Format("{0}{1}", ConfigurationManager.AppSettings["photo_remote_root_url"], logoUrl);
	    }
	}
}