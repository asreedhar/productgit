using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages.QueueFactories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class DailyBucketAlertsJob : EmailJobBase
    {
        private IEmailQueueFactory QueueFactory { get; set; }
        private static readonly ILog Log = LoggerFactory.GetLogger<DailyBucketAlertsJob>();

        public DailyBucketAlertsJob(IEmailQueueFactory queueFactory)
        {
            QueueFactory = queueFactory;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Sending Email for bucket alerts");
                var queue = QueueFactory.CreateBucketAlert();
                var messages = GetBucketAlerts.GetActiveBusinessUnitAlerts();

                var whiteList = GetWhiteList();
                foreach (var message in messages)
                {
                    //See if the emails are in the white list. Prevent sending emails in non production environments except for the whitelist.
                    var settingsCommand = new GetBucketAlerts(message.BusinessUnitId);
                    AbstractCommand.DoRun(settingsCommand);

                    // check if we have any email addresses and check them against the whitelist
                    if (!settingsCommand.EmailBounced && 
                        (
                            settingsCommand.Emails.Length > 1 ||
                            (settingsCommand.Emails.Length == 1 && !String.IsNullOrWhiteSpace(settingsCommand.Emails[0]))
                        ) &&
                        CanSend(whiteList, settingsCommand.Emails))
                    {
                        // set weekly for processing on the task side
                        message.SendDaily = true;
                        // send the message to the queue
                        queue.SendMessage(message, GetType().FullName);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Error in Daily Bucket Alert Job", e);
            }
        }
            
    }
}