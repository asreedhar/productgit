﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BulkWindowSticker;
using Merchandising.Messages;
using Merchandising.Messages.AutoWindowSticker;
using Quartz;
using FirstLook.Common.Core.Logging;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class WindowStickerClickStatsJob : IStatefulJob
    {
        private IQueueFactory QueueFactory { get; set; }
        private static readonly ILog Log = LoggerFactory.GetLogger<WindowStickerClickStatsJob>();

        public WindowStickerClickStatsJob(IQueueFactory queueFactory)
        {
            QueueFactory = queueFactory;
        }
        public void Execute(JobExecutionContext context)
        {
            try
            {

                Log.Info("WindowStickerClickStatsJob: Starting schedule job");

                var queue = QueueFactory.CreateWindowStickClickStats();

                var windowStickerRepository = new WindowStickerRepository();

                // default to messages sent in last 2 weeks, task will handle the duplicates
                var links = windowStickerRepository.GetClickStatsLinks(DateTime.Now.AddDays(-14));

                foreach (string link in links)
                {
                    queue.SendMessage(new ClickStatsMessage(link), GetType().FullName);
                    Log.DebugFormat("WindowStickerClickStatsJob: Sent message to for link {0}", link);
                }


            }
            catch (Exception e)
            {
                Log.Error("WindowStickerClickStatsJob Error: ", e);
            }
            finally
            {
                Log.Info("WindowStickerClickStatsJob: Ending schedule job");
            }

        }


    }
}
