﻿using System;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.TimeToMarket.Queues;
using MAX.TimeToMarket.Repositories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    /// <summary>
    /// This job just adds messages to the TimeToMarket Amazon queue telling the system
    /// which businessUnitIds it needs to run the TimeToMarket report on.
    /// </summary>
    class TimeToMarketGenerationJob: IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<TimeToMarketGenerationJob>();

        private readonly ITimeToMarketQueueFactory _queueFactory;
        private readonly ITimeToMarketRepository _repository;

        public TimeToMarketGenerationJob(ITimeToMarketQueueFactory queueFactory, ITimeToMarketRepositoryFactory repositoryFactory)
        {
            _queueFactory = queueFactory;
			_repository = repositoryFactory.GetRepository(TimeToMarketRepositoryVersion.Version1);
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting TimeToMarketGenerationJob");
                var queue = _queueFactory.CreateTimeToMarketQueue();

                int[] businessUnits = _repository.GetMaxBusinessUnits(FirstLook.DomainModel.Oltp.Upgrade.Merchandising);

                Array.ForEach(businessUnits, unit => queue.SendMessage(new Messages.TimeToMarket.TimeToMarketGenerationMessage(unit), GetType().FullName));                
            }

            catch (Exception e)
            {
                Log.Error("Quartz: Error in TimeToMarketGenerationJob", e);
            }
        }
    }
}
