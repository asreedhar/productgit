﻿using System;
using System.Reflection;
using Core.Messages;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class NewBookoutJob : EventJobBase<NewBookoutMessage>
    {
        private IFileStoreFactory FileStoreFactory { get; set; }
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string FileName = "LastBookoutID";

        private IBus<NewBookoutMessage, NewBookoutMessage> _bus;

        public NewBookoutJob(IFileStoreFactory fileStoreFactory, IAppSettingsRepository repository)
            : base(FileName, repository)
        {
            FileStoreFactory = fileStoreFactory;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {
                var storage = FileStoreFactory.CreateFileStore();

                _bus = BusFactory.CreateNewBookout<NewBookoutMessage, NewBookoutMessage>();
               
               Process(storage);
            }
            catch (Exception e)
            {
                Log.Error("Error in NewBookoutJob", e);
            }
        }

        protected override void Publish(NewBookoutMessage value)
        {
            _bus.Publish(value, GetType().FullName);
        }

        protected override NewBookoutMessage[] GetNewValues(int lastValue)
        {
            var newValues = LastBookout.GetNewBookout(lastValue);
            Log.DebugFormat("{0} New Bookouts Found", newValues.Length);

            return newValues;
        }

        public override int GetMaxEvent()
        {
            return LastBookout.GetMaxBookoutId();
        }

        protected override int GetNewValue(NewBookoutMessage value)
        {
            return value.BookoutId;
        }
    }
}
