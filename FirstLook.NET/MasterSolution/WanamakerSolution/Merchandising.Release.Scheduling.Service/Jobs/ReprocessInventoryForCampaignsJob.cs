﻿using System;
using System.Linq;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using Merchandising.Messages.QueueFactories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class ReprocessInventoryForCampaignsJob : IStatefulJob
    {
		private static readonly ILog Log = LoggerFactory.GetLogger<ReprocessInventoryForCampaignsJob>();
        private readonly IDiscountCampaignQueueFactory _queueFactory;
        private readonly IDiscountPricingRepository _repository;

        public ReprocessInventoryForCampaignsJob (IDiscountCampaignQueueFactory queueFactory,
            IDiscountPricingRepository repository)
        {
            _queueFactory = queueFactory;
            _repository = repository;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting ReprocessInventoryForCampaignsJob");
                var queue = _queueFactory.CreateReprocessDealerInventoryForDiscountsQueue();

                var businessUnitsWithActiveCampaigns = _repository.GetBusinessUnitsWithActiveCampaigns();

                Log.InfoFormat("Found {0} business units with active campaigns.", businessUnitsWithActiveCampaigns.Count());

                Array.ForEach(businessUnitsWithActiveCampaigns, buId => queue.SendMessage(new Messages.InventoryDocuments.DealerCommand
                {
                    BusinessUnitId = buId,
                }, GetType().FullName));
                Log.Info("Quartz: Finished ReprocessInventoryForCampaignsJob");
            }

            catch (Exception e)
            {
                Log.Error("Quartz: Error in ReprocessInventoryForCampaignsJob", e);
            }
        }
    }
}

   
