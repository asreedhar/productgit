using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Workflow.AutoOffline;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class SendProcessAutoOfflineJob : IJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<SendProcessAutoOfflineJob>();
        private IAutoOfflineProcessor Processor { get; set; }

        public SendProcessAutoOfflineJob(IAutoOfflineProcessor processor)
        {
            Processor = processor;
        }

        public void Execute(JobExecutionContext context)
        {
            Log.Info("Sending ProcessAutoOfflineMessage for all business units.");
            Processor.SendProcessAutoOfflineMessage();
        }
    }
}