﻿using System;
using System.IO;
using System.Linq;
using Core.Messages;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    class ListPriceChangeJob : IStatefulJob
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IFileStoreFactory _fileStoreFactory;
        private IBus<PriceChangeMessage, PriceChangeMessage> _bus;
        private string _fileName = "LastPriceChangeCheck";

        public IFileStoreFactory FileStoreFactory
        {
            get
            {   
                if(_fileStoreFactory == null)
                    _fileStoreFactory = new FileStoreFactory();
                return _fileStoreFactory ;
            }
            set { _fileStoreFactory = value; }
        }
        public IBus<PriceChangeMessage, PriceChangeMessage> Bus
        {
            get { return _bus ?? BusFactory.CreatePriceChange<PriceChangeMessage, PriceChangeMessage>(); } 
            set { _bus = value; }
        }


        public ListPriceChangeJob()
        {
        }
        public void Execute(JobExecutionContext context)
        {
            try
            {
                var storage = FileStoreFactory.CreateFileStore();


                DateTime earliest = GetLastCheck(storage);
                DateTime lastest = LastPrice.GetDatabaseDateTime();

                PriceChangeMessage[] messages = GetUpdatedInventory(earliest, lastest);

                Log.InfoFormat("Processing list price changes from {0} - {1}.  {2} message(s) to process.", earliest, lastest, messages.Count());

                foreach (var message in messages)
                {
                    Bus.Publish(message, GetType().FullName);
                }

                // save the date of the last check
                SaveLastCheck(storage, lastest);

                Log.InfoFormat("Processing list price changes complete.");


            }
            catch (Exception e)
            {
                Log.Error("Error in ListPriceChangeJob", e);
            }
        }
        protected virtual PriceChangeMessage[] GetUpdatedInventory(DateTime earliest, DateTime lastest)
        {
            return LastPrice.GetInventoryWithPriceChanges(earliest, lastest);
        }
        protected virtual void SaveLastCheck(IFileStorage storage, DateTime lastCheck)
        {
            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(lastCheck.ToString());
                writer.Flush();
                stream.Position = 0;
                storage.Write(_fileName, stream);
            }
        }
        protected virtual DateTime GetLastCheck(IFileStorage storage)
        {
            DateTime lastCheck = LastPrice.GetDatabaseDateTime();

            // get the last time that we checked for a list price change
            bool exists;
            using (var stream = storage.Exists(_fileName, out exists))
            {
                if (exists)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string value = reader.ReadToEnd();
                        DateTime lastCheckFromS3;
                        if (DateTime.TryParse(value, out lastCheckFromS3))
                            lastCheck = lastCheckFromS3;
                    }
                }
            }

            return lastCheck;
        }
    }
}
