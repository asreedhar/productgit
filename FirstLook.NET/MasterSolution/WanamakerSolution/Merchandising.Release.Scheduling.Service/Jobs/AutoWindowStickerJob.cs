﻿using System;
using System.Collections.Generic;
using System.Linq;
using BulkWindowSticker;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Merchandising.Messages.AutoWindowSticker;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{

    class AutoWindowStickerJob : EmailJobBase
    {

        private IQueueFactory QueueFactory { get; set; }
        private readonly IWindowStickerRepository _windowStickerRepository;
        private static readonly ILog Log = LoggerFactory.GetLogger<AutoWindowStickerJob>();

        public AutoWindowStickerJob(IQueueFactory queueFactory, IWindowStickerRepository windowStickerRepository)
        {
            QueueFactory = queueFactory;
            _windowStickerRepository = windowStickerRepository;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {

                Log.Debug("AutoWindowStickerJob: Starting schedule job");
                
                var queue = QueueFactory.CreateAutoWindowSticker();

                var messages = _windowStickerRepository.GetDealersToAutoPrint();

                var whiteList = GetWhiteList();

                foreach (var message in messages)
                {
                    // check for whiteList!!!! (will be empty on prod)
                    if(!string.IsNullOrWhiteSpace(message.Email))
                    {
                        // set for white list emails
                        if (message.Email != null)
                        {
                            string[] potentialEmailList = message.Email.Split(',');
                            var validEmailList = new List<string>();

                            foreach (string potentialEmail in potentialEmailList)
                            {
                                // white list will be empty in prod, otherwise check it
                                if(CanSend(whiteList, potentialEmail))
                                    validEmailList.Add(potentialEmail);
                                else
                                    Log.DebugFormat("AutoWindowStickerJob: Non white list email: {0} eliminated for BU: {1}", potentialEmail, message.BusinessUnitId);
                            }

                            message.Email = validEmailList.Any() ? string.Join(",", validEmailList) : null;
                        }
                    }

                    // send message
                    queue.SendMessage(new AutoWindowStickerMessage(message.BusinessUnitId, message.Email, message.MinimumDays), GetType().FullName);
                    Log.DebugFormat("AutoWindowStickerJob: Sent message to for BU: {0} to : {1} email(s) with min days: {2}", message.BusinessUnitId, message.Email, message.MinimumDays);

                }

                Log.Debug("AutoWindowStickerJob: Ending schedule job");


            }
            catch (Exception e)
            {
                Log.Error("AutoWindowStickerJob Error: ", e);
            }
            finally
            {
                Log.Info("AutoWindowStickerJob: Ending schedule job");
            }

        }

    }

}
