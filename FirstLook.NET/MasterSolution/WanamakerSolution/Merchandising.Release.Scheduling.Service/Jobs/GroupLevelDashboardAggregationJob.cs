﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class GroupLevelDashboardAggregationJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<GroupLevelDashboardAggregationJob>();

        private IQueueFactory _queueFactory;
        private IGroupLevelDataCache _groupLevelDataCache;

        public GroupLevelDashboardAggregationJob(IQueueFactory queueFactory, IGroupLevelDataCache groupLevelDataCache)
        {
            _queueFactory = queueFactory;
            _groupLevelDataCache = groupLevelDataCache;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting GroupLevelDashboardAggregationJob");
                var queue = _queueFactory.CreateGroupLevelAggregation();

                var messages = GroupLevelDashboardAggregation.GetActiveGroups();
                foreach (var message in messages)
                {
                    _groupLevelDataCache.SetCachedState(message.GroupId, State.Pending);
                    queue.SendMessage(message, GetType().FullName);
                }
            }
            catch (Exception e)
            {
                Log.Error("Quartz: Error in GroupLevelDashboardAggregationJob", e);   
            }
         
        }

        
    }
}
