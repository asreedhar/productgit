﻿using System;
using System.Reflection;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class AutoLoadMissedVehiclesJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AutoLoadMissedVehiclesJob>();

        private IQueueFactory _queueFactory;

        public AutoLoadMissedVehiclesJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting AutoLoadMissedVehiclesJob");
                var queue = _queueFactory.CreateAutoLoadInventory();

                var messages = MissedAutoLoad.Fetch();
                foreach (var message in messages)
                    queue.SendMessage(message, GetType().FullName);
            }
            catch (Exception e)
            {
                Log.Error("Quartz: Error in AutoLoadMissedVehiclesJob", e);
            }
        }

    }
}
