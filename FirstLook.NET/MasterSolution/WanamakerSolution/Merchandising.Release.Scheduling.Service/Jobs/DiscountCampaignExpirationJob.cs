﻿using System;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using Merchandising.Messages.QueueFactories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class DiscountCampaignExpirationJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<DiscountCampaignExpirationJob>();
        private readonly IDiscountCampaignQueueFactory _queueFactory;
        private readonly IDiscountPricingRepository _repository;

        public DiscountCampaignExpirationJob(IDiscountCampaignQueueFactory queueFactory, IDiscountPricingRepository repository)
        {
            _queueFactory = queueFactory;
            _repository = repository;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Starting DiscountCampaignExpirationJob");
                var queue = _queueFactory.CreateDiscountCampaignExpirationQueue();

                var campaigns = _repository.GetExpiredDiscountCampaigns().ToArray();

                Log.InfoFormat("Found {0} discount campaigns to expire.", campaigns.Length);

                Array.ForEach(campaigns, campaign => queue.SendMessage(new Messages.DiscountCampaigns.DiscountCampaignExpirationMessage
                {
                    BusinessUnitId = campaign.BusinessUnitId,
                    CampaignId = campaign.CampaignId
                }, GetType().FullName));
                Log.Info("Quartz: Finished DiscountCampaignExpirationJob");
            }

            catch (Exception e)
            {
                Log.Error("Quartz: Error in DiscountCampaignExpirationJob", e);
            }
        }
    }
}