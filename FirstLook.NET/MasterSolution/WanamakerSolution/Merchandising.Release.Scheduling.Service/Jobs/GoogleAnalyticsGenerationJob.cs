﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Quartz;
using FirstLook.Common.Core.Logging;
using Merchandising.Messages;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    public class GoogleAnalyticsGenerationJob : IStatefulJob
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<GoogleAnalyticsGenerationJob>();

        private IQueueFactory _queueFactory;
        private IGoogleAnalyticsRepository _googleRepo;

        public GoogleAnalyticsGenerationJob(IQueueFactory queueFactory, IGoogleAnalyticsRepository googleRepo)
        {
            _queueFactory = queueFactory;
            _googleRepo = googleRepo;
        }

        public void Execute(JobExecutionContext context)
        {
            try
            {
                if (Log.IsDebugEnabled)
                    Log.Debug("Begin GoogleAnalyticsGenerationJob");
                
                var queue = _queueFactory.CreateGoogleAnalyticsQueue();
                
                // List of ProfileObjects
                List<GoogleAnalyticsProfile> profiles = _googleRepo.GetAllActiveProfiles();

                // Get the previous 13 months of data. This gives us the current month and the previous complete 12 months.
                DateRange dateRange = DateRange.MonthsFromDate(-13, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).FirstSecondOfMonth());

                if (Log.IsDebugEnabled)
                    Log.Debug("Queueing messages for " + profiles.Count + " GA Profiles across the date range: " + dateRange.ToString());

                // Send one message/task per active profile.
                profiles.ForEach(profile =>
                {
                    queue.SendMessage(new Messages.GoogleAnalytics.GoogleAnalyticsGenerateMessage(profile.BusinessUnitId, profile.Type.ToString(), dateRange), GetType().FullName);
                });

                if (Log.IsDebugEnabled)
                    Log.Debug("Done queueing messages");
            }
            catch (Exception e)
            {
                Log.Error("Quartz: Error in GoogleAnalyticsGenerationJob", e);
            }
        }
    }
}
