﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using FirstLook.Merchandising.DomainModel.Release;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.BionicReports.Queues;
using MAX.BionicReports.Utility;
using MAX.Entities.Messages;
using Merchandising.Messages;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    /// <summary>
    /// This job gets a list of active dealers and their emails, validates said emails, then sends a 
    /// message to an Amazon message queue telling the system which business unit ids need the report 
    /// to be generated for.
    /// </summary>
    public class BionicReportJob : EmailJobBase
    {
        #region Properties
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int MinimumMaxVersion = 3;

        private readonly IFileStoreFactory _bionicFileStoreFactory;
        private readonly IBionicReportQueueFactory _bionicReportQueueFactory;
        private readonly IMaxDealersRepository _maxDealersRepository;
        
        #endregion

        #region Methods
		public BionicReportJob(IBionicReportQueueFactory bionicReportQueueFactory, IFileStoreFactory bionicFileStoreFactory, IMaxDealersRepository maxDealersRepository)
        {
            _bionicReportQueueFactory = bionicReportQueueFactory;
		    _bionicFileStoreFactory = bionicFileStoreFactory;
		    _maxDealersRepository = maxDealersRepository;
        }

        public override void Execute(JobExecutionContext context)
        {
            using (log4net.ThreadContext.Stacks["callStackId"].Push(Guid.NewGuid().ToString()))
            {
                try
                {
                    Log.Info("BionicReportJob: Starting scheduled job");

                    var trackingStore = _bionicFileStoreFactory.CreateBionicTrackingStorage();
                    var queue = _bionicReportQueueFactory.CreateBionicReportDataCollectionQueue();
                    var whiteList = GetBionicWhiteList();
                    var getMiscSettings = new GetMiscSettings(-1);
                    var getBucketAlerts = new GetBucketAlerts(-1);

                    var litmusUsername = ConfigurationManager.AppSettings["LitmusAPIAccountUsername"];
                    var litmusPassword = ConfigurationManager.AppSettings["LitmusAPIAccountPassword"];
                    var litmusEnabled =
                        Convert.ToBoolean(ConfigurationManager.AppSettings["LitmusAPIEnabled"].ToLowerInvariant());

                    string trackingCode;

                    if (litmusEnabled)
                    {
                        var trackingGenerator = new LitmusCurrentMonthCampaign(LitmusCurrentMonthCampaign.Type.Bionic,
                            trackingStore,
                            litmusUsername, litmusPassword);
                        if (!trackingGenerator.Success)
                        {
                            Log.Fatal("LitmusCampaign creator failed to create and or save campaign to persistence layer. Aborting email creation run.");
                            return;
                        }
                        else
                            trackingCode = trackingGenerator.TrackingCode;
                    }
                    else
                    {
                        Log.Info("Litmus tracking campaigns disabled.");
                        trackingCode = "<!--{emailAddress}-->"; // make email template transformer happy, it expects an {emailAddress} tag to replace
                    }

                    // Get the list of active business units for version
                    var activeBusinessUnitIds =
                        _maxDealersRepository.FindActiveMaxDealerIdsAboveVersion(MinimumMaxVersion).ToList();

                    Log.InfoFormat("{0} activeBusinessUnitIds found.", activeBusinessUnitIds.Count());

                    // Get the list of active business units with TTM turned on
                    var timeToMarketActiveBusinessIds = (
                        from ms in getMiscSettings.GetAllMiscSettings()
                        where
                            ms.ShowTimeToMarket && !String.IsNullOrWhiteSpace(ms.DMSEmail) &&
                            activeBusinessUnitIds.Contains(ms.BusinessUnitId)
                        select ms.BusinessUnitId).ToList();

                    Log.InfoFormat("{0} timeToMarketActiveBusinessIds found.", timeToMarketActiveBusinessIds.Count());

                    // Get the list of business units that have monthly emails setup
                    var bucketAlertSettings = (
                        from ba in getBucketAlerts.GetAllBucketAlertSettings()
                        where
                            ba.Active && ba.BionicHealthReportEmails.Length > 0 &&
                            timeToMarketActiveBusinessIds.Contains(ba.BusinessUnitId)
                        select ba).ToList();

                    Log.InfoFormat("{0} bucketAlertSettings found.", bucketAlertSettings.Count());

                    var validBusinessUnitIds = (
                        from ba in bucketAlertSettings
                        from email in ba.BionicHealthReportEmails
                        where !String.IsNullOrWhiteSpace(email) && CanSend(whiteList, email)
                        select ba.BusinessUnitId).Distinct().ToList();

                    Log.InfoFormat("{0} validBusinessUnitIds found.", validBusinessUnitIds.Count());

                    // creates the list of messages with the business unit ids and emails to start the process for
                    var bionicReportMessages = (
                        from ba in bucketAlertSettings
                        where validBusinessUnitIds.Contains(ba.BusinessUnitId)
                        select
                            new BionicReportDataCollectionMessage(ba.BusinessUnitId,
                                String.Join(",", ba.BionicHealthReportEmails), trackingCode)).ToList();

                    Log.InfoFormat("{0} bionicReportMessages found.", bionicReportMessages.Count());

                    // send the messages into the queue
                    foreach (var message in bionicReportMessages)
                        queue.SendMessage(message, GetType().FullName);
                }
                catch (Exception e)
                {
                    Log.Error("BionicReportJob Error: ", e);
                }
                finally
                {
                    Log.Info("BionicReportJob: Ending scheduled job");
                }
            }
        }
        #endregion
    }
}
