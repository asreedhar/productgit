using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages.BucketAlerts;
using Merchandising.Messages.QueueFactories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class BucketAlertsJob : EmailJobBase
    {
        private IEmailQueueFactory QueueFactory { get; set; }
        private static readonly ILog Log = LoggerFactory.GetLogger<BucketAlertsJob>();

        public BucketAlertsJob(IEmailQueueFactory queueFactory)
        {
            QueueFactory = queueFactory;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Queueing messages for bucket alerts");

                var isDaily = String.Compare(context.JobDetail.Name, "DailyBucketAlertsJob", StringComparison.InvariantCulture) == 0;
                BucketAlertMessage[] messages = null;

                if (isDaily)
                    messages = GetBucketAlerts.GetActiveBusinessUnits();
                else
                {
                    if ((int)DateTime.Now.DayOfWeek == GetBucketAlerts.GetWeeklyEmailSendDay())
                        messages = GetBucketAlerts.GetActiveBusinessUnitAlerts();
                }

                var queue = QueueFactory.CreateBucketAlert();

                if (messages != null && messages.Length > 0)
                {
                    foreach (var message in messages)
                    {
                        Log.DebugFormat("Queueing {0} message for {1}", (isDaily ? "Daily" : "Weekly"),
                            message.BusinessUnitId);

                        // set for which processing on the task side
                        message.SendDaily = isDaily;
                        message.SendWeekly = !isDaily;

                        // send the message to the queue
                        queue.SendMessage(message, GetType().FullName);
                    }
                }

                Log.Info("Quartz: Queueing messages for bucket alerts completed");
            }
            catch (Exception e)
            {
                Log.Error("Error in Bucket Alert Job", e);
            }
        }
    }
}