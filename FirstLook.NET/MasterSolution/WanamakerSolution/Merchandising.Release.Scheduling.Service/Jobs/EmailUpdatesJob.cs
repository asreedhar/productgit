using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Release;
using Merchandising.Messages.QueueFactories;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.Jobs
{
    internal class EmailUpdatesJob : EmailJobBase
    {
        private IEmailQueueFactory QueueFactory { get; set; }
        private static readonly ILog Log = LoggerFactory.GetLogger<EmailUpdatesJob>();

        public EmailUpdatesJob(IEmailQueueFactory queueFactory)
        {
            QueueFactory = queueFactory;
        }

        public override void Execute(JobExecutionContext context)
        {
            try
            {
                Log.Info("Quartz: Sending Email for auto approved ads");
                var queue = QueueFactory.CreateEmail();

                var activityCommand = new AutoApproveActivity();
                AbstractCommand.DoRun(activityCommand);

                var whiteList = GetWhiteList();
                foreach (var message in activityCommand.Messages)
                {
                    if(CanSend(whiteList, message.Email))
                        queue.SendMessage(message, GetType().FullName);
                }
            }
            catch (Exception e)
            {
                Log.Error("Error in Email Job", e);
            }
        }
    }
}