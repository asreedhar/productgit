using Autofac;
using Merchandising.Release.Scheduling.Service.CloudSearchReconciler.CloudSearch;
using Quartz;

namespace Merchandising.Release.Scheduling.Service.CloudSearchReconciler
{
	public class CloudSearchReconciliationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<VehicleCloudSearchDataRepository>().As<IVehicleCloudSearchDataRepository>();
			builder.RegisterType<CloudSearch.CloudSearchReconciler>().As<IInterruptableJob>();
		}
	}
}