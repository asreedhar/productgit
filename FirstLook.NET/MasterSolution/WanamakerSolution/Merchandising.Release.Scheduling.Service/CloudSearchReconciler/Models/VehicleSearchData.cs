﻿using System;

namespace Merchandising.Release.Scheduling.Service.CloudSearchReconciler.Models
{
	public class VehicleSearchData : IComparable, IEquatable<VehicleSearchData>
	{
		public string Vin { get; set; }
		public Guid OwnerHandle { get; set; }

		public int CompareTo(VehicleSearchData obj)
		{
			return String.Compare(Vin, obj.Vin, StringComparison.InvariantCultureIgnoreCase);
		}

		public int CompareTo(object obj)
		{
			var vehicleSearchData = obj as VehicleSearchData;
			if (vehicleSearchData != null)
				return CompareTo(vehicleSearchData);

			throw new ApplicationException("Illegal comparison to type VehicleSearchData");
		}

		public bool Equals(VehicleSearchData other)
		{
			return other.Vin == Vin && other.OwnerHandle == OwnerHandle;
		}
	}
}