﻿namespace Merchandising.Release.Scheduling.Service.CloudSearchReconciler.Models
{
	public class VehicleDocumentInfo
	{
		public int BusinessUnitId { get; set; }
		public int Inventoryid { get; set; }
		public string Vin { get; set; }
	}
}
