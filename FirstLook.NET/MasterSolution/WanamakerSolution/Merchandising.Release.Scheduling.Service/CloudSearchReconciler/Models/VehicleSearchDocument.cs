﻿using AmazingCloudSearch.Contract;
using AmazingCloudSearch.Query;
using Newtonsoft.Json;

namespace Merchandising.Release.Scheduling.Service.CloudSearchReconciler.Models
{
	/// <summary>
	/// The stub of a vehicle search document.
	/// </summary>
	/// <remarks>
	/// TODO:	Flesh this out with the rest of the VehicleSearch data points
	///			if we want to do more than delete docs from the search domain.
	/// </remarks>
	public class VehicleSearchDocument : ICloudSearchDocument
	{
		[JsonProperty("id")]
		[IgnoreReturnField]
		public string Id {
            get { return string.Format("{0}_{1}", OwnerHandle.ToUpperInvariant(), Vin); }
			set { }
		}

		[JsonProperty("ownerhandle")]
		public string OwnerHandle { get; set; }

		[JsonProperty("vin")]
		public string Vin { get; set; }
	}
}