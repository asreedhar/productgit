using System;

namespace Merchandising.Release.Scheduling.Service.CloudSearchReconciler.Models
{
	public class DealerInfo
	{
		public int BusinessUnitId { get; set; }
		public Guid OwnerHandle { get; set; }
	}
}