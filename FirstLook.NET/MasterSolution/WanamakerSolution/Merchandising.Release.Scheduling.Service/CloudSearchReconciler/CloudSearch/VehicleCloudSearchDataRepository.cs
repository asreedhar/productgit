﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using AmazingCloudSearch.MaxInterface;
using AmazingCloudSearch.Query;
using AmazingCloudSearch.Query.Boolean;
using Dapper;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Core;
using log4net;
using Merchandising.Release.Scheduling.Service.CloudSearchReconciler.Models;


namespace Merchandising.Release.Scheduling.Service.CloudSearchReconciler.CloudSearch
{
	public interface IVehicleCloudSearchDataRepository
	{
		IEnumerable<DealerInfo> GetActiveDealers();
		IEnumerable<VehicleSearchData> GetRecentlyActiveInventoryForDealer(DealerInfo dealerInfo);
		IEnumerable<VehicleSearchData> GetCloudSearchVehicles(DealerInfo dealerInfo);
		int DeleteCloudSearchVehicles(IEnumerable<VehicleSearchData> deleteVehicles);
		IEnumerable<VehicleDocumentInfo> GetActiveVehicleData(int businessUnitId);
	}

	internal class VehicleCloudSearchDataRepository : IVehicleCloudSearchDataRepository
	{
	    #region Logging

	    protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

	    #endregion Logging


		public IEnumerable<DealerInfo> GetActiveDealers()
		{
			const string sql = @"dbo.GetCloudSearchEnabledBusinessUnits";
            Log.VerboseFormat("Getting Active Dealers from {0}", sql);
			using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
			{
				conn.Open();
				return conn.Query<DealerInfo>(sql, commandType: CommandType.StoredProcedure);
			}
		}

		public IEnumerable<VehicleSearchData> GetRecentlyActiveInventoryForDealer(DealerInfo dealerInfo)
		{
			const string sql = @"SELECT @OwnerHandle AS OwnerHandle, VIN AS Vin 
								FROM IMT.dbo.Inventory 
								JOIN IMT.dbo.Vehicle ON Vehicle.VehicleId = Inventory.VehicleId
								LEFT JOIN Merchandising.builder.OptionsConfiguration ON 
                                    OptionsConfiguration.businessUnitID = Inventory.BusinessUnitID 
                                    AND OptionsConfiguration.inventoryId = Inventory.InventoryID
								WHERE Inventory.BusinessUnitId = @BusinessUnitId AND (InventoryActive = 1 OR 
								Inventory.DeleteDt > @DeleteDate)
								AND ISNULL(doNotPostFlag, 0) = 0";


            
			var deleteDateString = ConfigurationManager.AppSettings["RecentlyActiveDays"];
			DateTime deleteDate;
			if(string.IsNullOrWhiteSpace(deleteDateString))
				deleteDate = DateTime.Now.AddDays(-45).Date;
			else
				deleteDate = DateTime.Now.AddDays(0 - int.Parse(deleteDateString)).Date;
            
            Log.TraceFormat("GetRecentlyActiveInventoryForDealer from {0}, @OwnerHandle:{1}, @BusinessUnitId:{2}, @DeleteDate:{3}", sql, dealerInfo.OwnerHandle, dealerInfo.BusinessUnitId, deleteDate);
			
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
			{
				conn.Open();
				return conn.Query<VehicleSearchData>(
					sql,
					new { dealerInfo.BusinessUnitId, dealerInfo.OwnerHandle, DeleteDate = deleteDate }, commandTimeout: 120);
			}
		}

		public IEnumerable<VehicleSearchData> GetCloudSearchVehicles(DealerInfo dealerInfo)
		{
			var csDomain = CloudSearchFactory<VehicleSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["vehicle_cloud_search_url"]);
			var bQuery = new BooleanQuery();
			var ownerCondition = new StringBooleanCondition("ownerhandle", dealerInfo.OwnerHandle.ToString().ToUpperInvariant());
			bQuery.Conditions.Add(ownerCondition);

			var searchQuery = new SearchQuery<VehicleSearchDocument> { BooleanQuery = bQuery, Size = 1000 };
			Log.TraceFormat("Querying CloudSearch: {0}", searchQuery.ToString());
            
            var searchResults = csDomain.Search(searchQuery);
            Log.TraceFormat("CloudSearch results: {0}", searchResults.ToJson());
			
            return searchResults.hits.hit.Select(hit => new VehicleSearchData { OwnerHandle = Guid.Parse(hit.data.OwnerHandle), Vin = hit.data.Vin });
		}

		public int DeleteCloudSearchVehicles(IEnumerable<VehicleSearchData> deleteVehicles)
		{
			var deleteDocuments =
				deleteVehicles.Select(dv => new VehicleSearchDocument { OwnerHandle = dv.OwnerHandle.ToString().ToUpperInvariant(), Vin = dv.Vin.ToUpperInvariant() }).ToList();
			var csDomain = CloudSearchFactory<VehicleSearchDocument>.CreateCloudSearchConnection(ConfigurationManager.AppSettings["vehicle_cloud_search_url"]);

            Log.DebugFormat("Deleting from CloudSearch domain: {0}", ConfigurationManager.AppSettings["vehicle_cloud_search_url"]);
            Log.TraceFormat("Delete query: {0}", deleteDocuments.ToJson());

		    var deleted = csDomain.Delete(deleteDocuments);
            Log.TraceFormat("CloudSearch Delete results: {0}", deleted.ToJson());
            
            return deleted.deletes;
		}

		public IEnumerable<VehicleDocumentInfo> GetActiveVehicleData(int businessUnitId)
		{
			const string sql = @"SELECT BusinessUnitId, InventoryId, Vin FROM FLDW.dbo.InventoryActive
				WHERE BusinessUnitId = @BusinessUnitId";

            Log.TraceFormat("GetActiveVehicleData with: {0}", sql);
			using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FLDW"].ConnectionString))
			{
				conn.Open();
				return conn.Query<VehicleDocumentInfo>(sql, new {BusinessUnitId = businessUnitId});
			}
		}
	}
}