﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core;
using log4net;
using Merchandising.Release.Scheduling.Service.CloudSearchReconciler.Models;
using Quartz;


namespace Merchandising.Release.Scheduling.Service.CloudSearchReconciler.CloudSearch
{
	public class CloudSearchReconciler : IInterruptableJob
	{
		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging
		
		private readonly IVehicleCloudSearchDataRepository _vehicleCloudSearchDataRepository;
		private readonly IAdMessageSender _messageSender;
		private readonly CancellationTokenSource _cancellationSource;
		private CancellationToken _cancellationToken;

		public CloudSearchReconciler(IVehicleCloudSearchDataRepository vehicleCloudSearchDataRepository, IAdMessageSender messageSender)
		{
			_vehicleCloudSearchDataRepository = vehicleCloudSearchDataRepository;
			_messageSender = messageSender;
			_cancellationSource = new CancellationTokenSource();
			_cancellationToken = _cancellationSource.Token;
			Log.InfoFormat("OnStart() for {0} (Scheduled job start).", typeof(CloudSearchReconciler));
		}

		public void Execute(JobExecutionContext context)
		{
			using (ThreadContext.Stacks["callStackId"].Push(Guid.NewGuid().ToString()))
			{
				RemoveStaleVehiclesFromCloudSearch();
			}
		}

		public void Interrupt()
		{
			_cancellationSource.Cancel();
		}

		#region DoWork methods

		private void RemoveStaleVehiclesFromCloudSearch()
		{
			var dealers = _vehicleCloudSearchDataRepository.GetActiveDealers().ToArray();
			Log.InfoFormat("Found {0} Business Units with the necessary upgrades.", dealers.Length);

			foreach (var dealerInfo in dealers)
			{
			    try
			    {
			        using (ThreadContext.Stacks["context"].Push(dealerInfo.ToJson()))
			        {
			            if (_cancellationToken.IsCancellationRequested)
			                break;
			            
                        var cloudSearchVehicles = _vehicleCloudSearchDataRepository.GetCloudSearchVehicles(dealerInfo).ToArray();
			            Log.DebugFormat("Found {0} vehicles in CloudSearch for BusinessUnitId: {1}", cloudSearchVehicles.Length, dealerInfo.BusinessUnitId);
			            Log.TraceFormat("CloudSearch VINs: {0}", string.Join(", ", cloudSearchVehicles.Select(csv => csv.Vin)));
                        
                        VehicleSearchData[] recentInventory;
			            try
			            {
			                recentInventory = _vehicleCloudSearchDataRepository.GetRecentlyActiveInventoryForDealer(dealerInfo).ToArray();
			            }
			            catch (Exception ex)
			            {
			                Log.Error("Error getting recent inventory from the database.", ex);
			                continue;
			            }

			            Log.DebugFormat("Found {0} recently active vehicles in the database for BusinessUnitId: {1}", recentInventory.Length, dealerInfo.BusinessUnitId);

			            var recentVins = recentInventory.Select(s => s.Vin.ToUpperInvariant()).ToArray();
                        Log.TraceFormat("Recent vins from DB: {0}", string.Join(",", recentVins));

			            var deleteVehicles = cloudSearchVehicles.Where(csVehicle => recentVins.All(rv => rv != csVehicle.Vin.ToUpperInvariant())).ToArray();
			            if (deleteVehicles.Length > 0)
			            {
			                Log.DebugFormat("Deleting {0} vehicles from CloudSearch for BusinessUnitId: {1}", deleteVehicles.Length, dealerInfo.BusinessUnitId);

			                var deleteVins = deleteVehicles.Select(dv => dv.Vin.ToUpperInvariant()).ToArray();

                            // A string that can be plugged directly into SQL to visually inspect results
                            var checkString = string.Join(", ", deleteVins.Select(dv => "'" + dv + "'"));
                            Log.TraceFormat("Delete VINs: {0}", checkString);

			                // Ensure that our list of delete vins does not intersect with our list of recent vins
			                Debug.Assert(deleteVins.Intersect(recentVins).Any() == false);

			                Debug.Assert(deleteVehicles.All(data => data.OwnerHandle.ToString().ToUpperInvariant() == dealerInfo.OwnerHandle.ToString().ToUpperInvariant()));

                            var deleted = _vehicleCloudSearchDataRepository.DeleteCloudSearchVehicles(deleteVehicles);
                            Log.DebugFormat("Deleted {0} records from CloudSearch for BusinessUnitId: {1}", deleted, dealerInfo.BusinessUnitId);
			                Log.InfoFormat("BusinessUnitId: {0}, ActiveVins: {1}, CloudSearchVins-Before: {2}, RemoveFromCloudSearch: {3}", 
                                dealerInfo.BusinessUnitId, recentVins.Length, cloudSearchVehicles.Length, deleted);
			            }
			            else
			            {
			                Log.InfoFormat("No vehicles found to delete from CloudSearch for BusinessUnitId: {0}", dealerInfo.BusinessUnitId);
			            }
			            RepublishAllDocuments(dealerInfo);
			        }
			    }
			    catch (Exception ex)
			    {
                    // General exception catch so we can at least attempt 
                    // to process each dealer if something bombs out with one.
			        Log.Error("Caught unexpected exception in CloudSearch Reconciler.", ex);
			    }
			}
			Log.InfoFormat("OnStop() for {0} version (end of job reached).", typeof(CloudSearchReconciler));
		}

		private void RepublishAllDocuments(DealerInfo dealerInfo)
		{
			var activeVehicles = _vehicleCloudSearchDataRepository.GetActiveVehicleData(dealerInfo.BusinessUnitId).ToArray();
			Log.InfoFormat("Sending PublishDocument notifications for {0} inventory items for BusinessUnit {1}.", activeVehicles.Length, dealerInfo.BusinessUnitId);
			foreach (var vehicleData in activeVehicles)
			{
                if (_cancellationToken.IsCancellationRequested)
                    break;

                Log.TraceFormat("PublishDocumentsNotification: {0}", vehicleData.ToJson());
				//_messageSender.SendPublishDocumentsNotification(vehicleData.BusinessUnitId, vehicleData.Inventoryid, vehicleData.Vin, "CloudSearchReconciler");
			}
			Log.InfoFormat("Finished Notifications.");
		}

		#endregion DoWork methods
	}
}
