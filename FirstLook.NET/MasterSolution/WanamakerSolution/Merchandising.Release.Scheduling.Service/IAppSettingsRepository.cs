﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Merchandising.Release.Scheduling.Service
{
    public interface IAppSettingsRepository
    {
        string Fetch(string key);
    }
}
