﻿using System;
using System.ServiceProcess;


namespace Merchandising.Release.Scheduling.Service
{
    internal partial class Service : ServiceBase
    {
        private JobController JobController { get; set; }
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Service(JobController jobController)
        {
            JobController = jobController;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Log.InfoFormat("OnStart() for {0} version {1} has been called.", typeof(Service), AppVersion());
            try
            {
                JobController.Start();
            }
            catch (Exception ex)
            {
                Log.Error("Failed to start scheduler.", ex);
                throw;
            }
            base.OnStart(args);
        }

        private static Version AppVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        }

        protected override void OnStop()
        {
            Log.InfoFormat("OnStop() for {0} version {1} has been called.", typeof(Service), AppVersion());
            try
            {
                JobController.Stop();
            }
            catch (Exception ex)
            {
                Log.Error("failed to stop scheduler.", ex);
                throw;
            }
            base.OnStop();
        }
    }
}
