using System.Collections.Generic;

namespace MAX.FirstLookServicesApiClient
{
    public class BooksList
    {
        public IEnumerable<string> Books { get; set; }
    }

    // My API client never throws an error.
    public class EmptyBooksList : BooksList
    { }
}