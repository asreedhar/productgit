using System;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using ServiceStack.ServiceClient.Web;

namespace MAX.FirstLookServicesApiClient
{
    public class FirstLookVehicleInfoClient : IFirstLookVehicleInfoClient
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private string FlUserName { get; set; }
        private string FlPassword { get; set; }

        public FirstLookVehicleInfoClient()
        {
            FlUserName = ConfigurationManager.AppSettings["FirstLookServiceApiUser"];
            if (FlUserName == null) throw new ApplicationException("No application setting found for: FlUserName");

            FlPassword = ConfigurationManager.AppSettings["FirstLookServiceApiPass"];
            if (FlPassword == null) throw new ApplicationException("No application setting found for: FlPassword");
        }

        public FirstLookVehicleInfoResponse GetVehicleInfo(int businessUnitId, int inventoryId)
        {
            var response = new FirstLookVehicleInfoResponse();

            try
            {
                var flServicesApiUrl = ConfigurationManager.AppSettings["FirstLookServicesApiUrl"];
                if (flServicesApiUrl == null) throw new ApplicationException("No application setting found for: FirstLookServicesApiUrl");
                var baseUrl = flServicesApiUrl.TrimEnd(new[] { '/', '\\' });
                var inventoryUrl = string.Format("{0}/pinginventory/{1}/{2}",baseUrl, businessUnitId, inventoryId);
                Log.DebugFormat("Getting vehicle information from: {0}", inventoryUrl);
                Debug.WriteLine(string.Format("Getting vehicle information from: {0}", inventoryUrl));
                
                var client = new JsonServiceClient(inventoryUrl)
                {
                    AlwaysSendBasicAuthHeader = true,
                    UserName = FlUserName,
                    Password = FlPassword
                };



                var continuationTask = client.GetTask<FirstLookVehicleInfoResponse>(inventoryUrl)
                    .ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            var aggregateException = task.Exception;
                            if (aggregateException == null) return;
                            foreach (var exception in aggregateException.InnerExceptions)
                            {
                                if (exception is WebServiceException)
                                {
                                    var wse = exception as WebServiceException;

                                    if (wse.StatusCode == 401)
                                    {
                                        Log.DebugFormat("Invalid user/password combination for user {0}", FlUserName);
                                    }
                                    else if (wse.StatusCode == 400 && wse.ResponseBody.Contains("Invalid input: DealerId"))
                                    {
                                        Log.DebugFormat("No information for businessUnitId: {0}.", businessUnitId);
                                    }
                                    else
                                    {
                                        Log.Error(String.Format("Received an unexpected error for businessUnitId: {0}. Error: {1}", businessUnitId, wse.ErrorMessage), wse);
                                    }
                                    Debug.WriteLine(string.Format("Web Service Exception Response Body: {0}", wse.ResponseBody));
                                }
                                else
                                {
                                    Log.Error("Received Non-WebServiceException in async call.", exception);
                                }
                                Debug.WriteLine(string.Format("exception message: {0}", exception.Message));

                            }
                        }
                        else // Successful execution
                        {
                            response = task.Result;                            
                        }
                    });

                if (!continuationTask.IsCompleted)
                    continuationTask.Wait();
            }
            catch (AggregateException ae)
            {
                Log.Error("Aggregate Error in async call.", ae);
            }

            return response;
        }
    

    }
}