﻿using Autofac;

namespace MAX.FirstLookServicesApiClient
{
    public class FirstLookServicesApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FirstLookBooksClient>().AsImplementedInterfaces();
            builder.RegisterType<FirstLookVehicleInfoClient>().AsImplementedInterfaces();
        }
    }
}
