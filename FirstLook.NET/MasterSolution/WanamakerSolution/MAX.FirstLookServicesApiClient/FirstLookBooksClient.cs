﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ServiceStack.Service;
using ServiceStack.ServiceClient.Web;

namespace MAX.FirstLookServicesApiClient
{
    internal class FirstLookBooksClient : IFirstLookBooksClient
    {
        #region Logging
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        #endregion Logging

        private BooksList _booksList;

        public BooksList GetDealerBooks(int businessUnitId, string apiUser, string apiPass)
        {
            _booksList = null;

            try
            {
                var baseUrl = ConfigurationManager.AppSettings["FirstLookServicesApiUrl"].TrimEnd(new[] {'/', '\\'});
                var booksUrl = string.Format("books/dealers/{0}", businessUnitId);
                Log.DebugFormat("Getting Book providers from: {0}/{1}", baseUrl, booksUrl);

                

                var client = GetClient(baseUrl, apiUser, apiPass);

                var continuationTask = client.GetTask<BooksList>(booksUrl)
                    .ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            var aggregateException = task.Exception;
                            if (aggregateException != null)
                            {
                                foreach (var exception in aggregateException.InnerExceptions)
                                {
                                    if (exception is WebServiceException)
                                    {
                                        var wse = exception as WebServiceException;

                                        if (wse.StatusCode == 401)
                                        {
                                            Log.DebugFormat("Invalid user/password combination for user {0}", apiUser);
                                        }
                                        else if (wse.StatusCode == 400 && wse.ResponseBody.Contains("Invalid input: DealerId"))
                                        {
                                            Log.DebugFormat("No bookout information for businessUnitId: {0}.", businessUnitId);
                                        }
                                        else
                                        {
                                            Log.Error(String.Format("Received an unexpected error for businessUnitId: {0}. Error: {1}", businessUnitId, wse.ErrorMessage), wse);
                                        }
                                    }
                                    else
                                    {
                                        Log.Error("Received Non-WebServiceException in async call.", exception);
                                    }
                                }
                            }
                            _booksList = new EmptyBooksList();
                        }
                        else // Successful execution
                        {
                            _booksList = task.Result;
                        }
                    });

                if (!continuationTask.IsCompleted)
                    continuationTask.Wait();
            }
            catch (AggregateException ae)
            {
                Log.Error("Aggregate Error in async call.", ae);
            }

            return _booksList;
        }

        private static IServiceClient GetClient(string url, string user, string pass)
        {
            return new JsonServiceClient(url)
            {
                AlwaysSendBasicAuthHeader = true,
                UserName = user,
                Password = pass
            };
        }



    }

    public static class ServiceStackAsyncTaskExtensions
    {
        /// <summary>
        /// Return a Task of <typeparam name=">TResponse"></typeparam> for async GET calls on ServiceStack clients.
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="client"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        /// <remarks>ServiceStack client v3 uses MS' Asynchronus Programming Model and I want a task.</remarks>
        public static Task<TResponse> GetTask<TResponse>(this IServiceClient client, string url)
        {
            var tcs = new TaskCompletionSource<TResponse>();

            client.GetAsync<TResponse>(url,
                tcs.SetResult,
                (response, exc) => tcs.SetException(exc)
            );

            return tcs.Task;
        }
    }
}
