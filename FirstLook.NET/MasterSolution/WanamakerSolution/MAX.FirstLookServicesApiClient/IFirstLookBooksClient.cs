﻿namespace MAX.FirstLookServicesApiClient
{
    public interface IFirstLookBooksClient
    {
        BooksList GetDealerBooks(int businessUnitId, string apiUser, string apiPass);
    }
}