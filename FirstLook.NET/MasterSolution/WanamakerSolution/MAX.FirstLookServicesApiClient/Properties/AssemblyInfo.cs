﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MAX.FirstLookServicesApiClient")]
[assembly: AssemblyDescription("A ServiceStack.Client wrapper for the FirstLook services api.")]
[assembly: AssemblyProduct("MAX.FirstLookServicesApiClient")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6667863c-95ae-4a50-9735-010f4c56a8a6")]

[assembly: InternalsVisibleTo("MAX.FirstLookServicesApiClient.Tests")]