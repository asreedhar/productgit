﻿namespace MAX.FirstLookServicesApiClient
{
    public interface IFirstLookVehicleInfoClient
    {
        FirstLookVehicleInfoResponse GetVehicleInfo(int businessUnitId, int inventoryId);
    }
}