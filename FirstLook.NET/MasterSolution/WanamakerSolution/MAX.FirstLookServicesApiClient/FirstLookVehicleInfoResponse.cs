﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MAX.FirstLookServicesApiClient
{
    [DataContract]
    public class FirstLookVehicleInfoResponse
    {
        [DataMember(Name = "inventory")]
        public FLInventory inventory { get; set; }
        [DataMember(Name = "trims")]
        public List<Trim> trims { get; set; }
    }

    public class FLInventory
    {
        public int year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string trim { get; set; }
    
    }

    [DataContract]
    public class Trim
    {
        [DataMember(Name = "ChromeStyleId")]
        public int ChromeStyleId { get; set; }
        [DataMember(Name = "Trim")]
        public string Description { get; set; }
    }
}