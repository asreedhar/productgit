﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.ModelBuilder.BusinessUnit
{
    public class Dealer
    {
        public static readonly string HttpContextKey = "BusinessUnitContextKey";

        
        public int DealerId { get; set; }
        
        public int PingIIDefaultSearchRadius { get; set; }
        
        public int PingIIMaxSearchRadius { get; set; }
        
        public int PingIIGuideBookId { get; set; }
        
        public int IsNewPing { get; set; }
        
        public string PingIIMarketListing { get; set; }
        
        public int PingIIRedRange1Value1 { get; set; }
        
        public int PingIIRedRange1Value2 { get; set; }
        
        public int PingIIYellowRange1Value1 { get; set; }
        
        public int PingIIYellowRange1Value2 { get; set; }
        
        public int PingIIYellowRange2Value1 { get; set; }
        
        public int PingIIYellowRange2Value2 { get; set; }
        
        public int PingIIGreenRange1Value1 { get; set; }
        
        public int PingIIGreenRange1Value2 { get; set; }
        
        public string ZipCode { get; set; }
        
        public string OwnerName { get; set; }
        
        public decimal Latitude { get; set; }
        
        public decimal Longitude { get; set; }
        
        public int UnitCostThresholdLower { get; set; }
        
        public int UnitCostThresholdUpper { get; set; }
        
        public int GuideBookID { get; set; }
        
        public int BookOut { get; set; }
        
        public decimal UnitCostThreshold { get; set; }
        
        public int GuideBook2Id { get; set; }
        
        public IList<int> DistanceList { get; set; }
        
        public int FavourableThreshold { get; set; }
        
        public int OriginalMSRP { get; set; }
        
        public int MarketAvgInternetPrice { get; set; }
        
        public int EdmundsTrueMarketValue { get; set; }
        
        public int NADARetailValue { get; set; }

        public int KBBRetailValue { get; set; }

    }
}

