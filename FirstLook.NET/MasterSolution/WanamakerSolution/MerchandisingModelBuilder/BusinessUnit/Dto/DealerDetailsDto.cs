﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.BusinessUnit.Dto
{
    [DataContract]
    public class DealerDetailsDto
    {
        [DataMember(Name = "dealerId")]
        public int DealerId { get; set; }
        [DataMember(Name = "pingIIDefaultSearchRadius")]
        public int PingIIDefaultSearchRadius { get; set; }
        [DataMember(Name = "pingIIMaxSearchRadius")]
        public int PingIIMaxSearchRadius { get; set; }
        [DataMember(Name = "pingIIGuideBookId")]
        public int PingIIGuideBookId { get; set; }
        [DataMember(Name = "isNewPing")]
        public int IsNewPing { get; set; }
        [DataMember(Name = "pingIIMarketListing")]
        public string PingIIMarketListing { get; set; }
        [DataMember(Name = "pingIIRedRange1Value1")]
        public int PingIIRedRange1Value1 { get; set; }
        [DataMember(Name = "pingIIRedRange1Value2")]
        public int PingIIRedRange1Value2 { get; set; }
        [DataMember(Name = "pingIIYellowRange1Value1")]
        public int PingIIYellowRange1Value1 { get; set; }
        [DataMember(Name = "pingIIYellowRange1Value2")]
        public int PingIIYellowRange1Value2 { get; set; }
        [DataMember(Name = "pingIIYellowRange2Value1")]
        public int PingIIYellowRange2Value1 { get; set; }
        [DataMember(Name = "pingIIYellowRange2Value2")]
        public int PingIIYellowRange2Value2 { get; set; }
        [DataMember(Name = "pingIIGreenRange1Value1")]
        public int PingIIGreenRange1Value1 { get; set; }
        [DataMember(Name = "pingIIGreenRange1Value2")]
        public int PingIIGreenRange1Value2 { get; set; }
        [DataMember(Name = "zipCode")]
        public string ZipCode { get; set; }
        [DataMember(Name = "ownerName")]
        public string OwnerName { get; set; }
        [DataMember(Name = "latitude")]
        public decimal Latitude { get; set; }
        [DataMember(Name = "longitude")]
        public decimal Longitude { get; set; }
        [DataMember(Name = "unitCostThresholdLower")]
        public int UnitCostThresholdLower { get; set; }
        [DataMember(Name = "unitCostThresholdUpper")]
        public int UnitCostThresholdUpper { get; set; }
        [DataMember(Name = "guideBookID")]
        public int GuideBookID { get; set; }
        [DataMember(Name = "bookOut")]
        public int BookOut { get; set; }
        [DataMember(Name = "unitCostThreshold")]
        public decimal UnitCostThreshold { get; set; }
        [DataMember(Name = "guideBook2Id")]
        public int GuideBook2Id { get; set; }
        [DataMember(Name = "distance")]
        public IList<int> DistanceList { get; set; }
        [DataMember(Name = "favourableThreshold")]
        public int FavourableThreshold { get; set; }
        [DataMember(Name = "originalMSRP")]
        public int OriginalMSRP { get; set; }
        [DataMember(Name = "marketAvgInternetPrice")]
        public int MarketAvgInternetPrice { get; set; }
        [DataMember(Name = "edmundsTrueMarketValue")]
        public int EdmundsTrueMarketValue { get; set; }
        [DataMember(Name = "nADARetailValue")]
        public int NADARetailValue { get; set; }
        [DataMember(Name = "kBBRetailValue")]
        public int KBBRetailValue { get; set; }


    }
}
