﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.Dto;

namespace FirstLook.Merchandising.ModelBuilder.BusinessUnit.BusinessUnitModel
{
    [DataContract]
    public class BusinessUnitModel
    {
        [DataMember(Name = "dealerInfo")]
        public DealerDetailsDto DealerInfo { get; set; }
    }
}
