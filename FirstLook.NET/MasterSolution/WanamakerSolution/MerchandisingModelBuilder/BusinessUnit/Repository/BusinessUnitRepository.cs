﻿using System;
using System.Reflection;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.Dto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Common.Core.IOC;
using log4net;

namespace FirstLook.Merchandising.ModelBuilder.BusinessUnit.Repository
{
    public class BusinessUnitRepository : IRepository<BusinessUnitModel.BusinessUnitModel, BusinessUnitDto>
    {
        #region Logging


        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


        #endregion Logging

        public BusinessUnitModel.BusinessUnitModel Fetch(BusinessUnitDto input)
        {
            BusinessUnitModel.BusinessUnitModel model = new BusinessUnitModel.BusinessUnitModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<BusinessUnitModel.BusinessUnitModel>>();

                model = service.GetData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method);

            }
            catch (Exception ex)
            {
                //throw ex;
                Log.Error("Exception in BusinessUnitRepository - BusinessUnitModel:Fetch ", ex);
            }

            return model;
        }

        public BusinessUnitModel.BusinessUnitModel Save(BusinessUnitDto input)
        {
            throw new NotImplementedException();
        }

        public BusinessUnitModel.BusinessUnitModel Update(BusinessUnitDto input)
        {
            throw new NotImplementedException();
        }

        public BusinessUnitModel.BusinessUnitModel Delete(BusinessUnitDto input)
        {
            throw new NotImplementedException();
        }
    }
}
