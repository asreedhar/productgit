﻿using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.ApproveAd;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingHistory;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SearchCriteriaDto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingHistory;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SearchCriteria;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.ApproveAd;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.PricingHistory;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.SearchCriteria;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.InventoryHeader;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.InventoryHeader;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.InventoryHeader;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.SaveTrim;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveTrim;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveTrim;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveInternetPrice;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveInternetPrice;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.SaveInternetPrice;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.PricingSummary;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingSummary;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingProof;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.PricingProof;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingProof;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.Repository;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.BusinessUnitModel;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.Dto;


namespace FirstLook.Merchandising.ModelBuilder
{
    public class MerchandisingModelBuilderRegistry : IRegistryModule
    {


        #region IRegistryModule Members

        public void Register(Autofac.ContainerBuilder builder)
        {
            //Max Pricing Tool
            builder.RegisterType<CarfaxRepository>().As<IRepository<CarfaxModel, CarfaxArgumentDto>>();
            builder.RegisterType<RestServiceGateway<CarfaxModel>>().As<IServiceGateway<CarfaxModel>>();
            //AutoCheck
            builder.RegisterType<AutocheckRepository>().As<IRepository<AutocheckModel, AutocheckArgumentDto>>();
            builder.RegisterType<RestServiceGateway<AutocheckModel>>().As<IServiceGateway<AutocheckModel>>();
            //MarketListings
            builder.RegisterType<MarketListingsRepository>().As<IRepository<MarketListingsModel, MarketListingsArgumentDto>>();
            builder.RegisterType<RestServiceGateway<MarketListingsModel>>().As<IServiceGateway<MarketListingsModel>>();
            //InventoryHeader
            builder.RegisterType<InventoryHeaderRepository>().As<IRepository<InventoryHeaderModel, InventoryHeaderArgumentDto>>();
            builder.RegisterType<RestServiceGateway<InventoryHeaderModel>>().As<IServiceGateway<InventoryHeaderModel>>();
            //Save Selected Trim
            builder.RegisterType<SaveTrimRepository>().As<IRepository<SaveTrimModel, SaveTrimArgumentDto>>();
            builder.RegisterType<RestServiceGateway<SaveTrimModel>>().As<IServiceGateway<SaveTrimModel>>();
            //Save Internet Price
            builder.RegisterType<SaveInternetPriceRepository>().As<IRepository<SaveInternetPriceModel, SaveInternetPriceArgumentDto>>();
            builder.RegisterType<RestServiceGateway<SaveInternetPriceModel>>().As<IServiceGateway<SaveInternetPriceModel>>();
            //Pricing Summary
            builder.RegisterType<PricingSummaryRepository>().As<IRepository<PricingSummaryModel, PricingSummaryArgumentDto>>();
            builder.RegisterType<RestServiceGateway<PricingSummaryModel>>().As<IServiceGateway<PricingSummaryModel>>();



            builder.RegisterType<PricingRepository>().As<IRepository<Rsponse.PricingHistoryModel, PricingHistoryArgumentDto>>();
            builder.RegisterType<RestServiceGateway<Rsponse.PricingHistoryModel>>().As<IServiceGateway<Rsponse.PricingHistoryModel>>();

            //Pricing Proof
            builder.RegisterType<PricingProofRepository>().As<IRepository<PricingProofModel, PricingProofArgumentDto>>();
            builder.RegisterType<RestServiceGateway<PricingProofModel>>().As<IServiceGateway<PricingProofModel>>();

            //Dealer
            builder.RegisterType<BusinessUnitRepository>().As<IRepository<BusinessUnitModel, BusinessUnitDto>>();
            builder.RegisterType<RestServiceGateway<BusinessUnitModel>>().As<IServiceGateway<BusinessUnitModel>>();

            //ApproveAd
            builder.RegisterType<ApproveAdRepository>().As<IRepository<UpdateInternetPriceNApproveAdModel, SaveInternetPriceNApproveAdArgumentDto>>();
            builder.RegisterType<RestServiceGateway<UpdateInternetPriceNApproveAdModel>>()
                .As<IServiceGateway<UpdateInternetPriceNApproveAdModel>>();

            //SearchCriteria
            builder.RegisterType<SearchCriteriaRepository>().As<IRepository<SearchCriteriaModel, SearchCriteriaArgumentDto>>();
            builder.RegisterType<RestServiceGateway<SearchCriteriaModel>>().As<IServiceGateway<SearchCriteriaModel>>();
        }

        #endregion
    }
}
