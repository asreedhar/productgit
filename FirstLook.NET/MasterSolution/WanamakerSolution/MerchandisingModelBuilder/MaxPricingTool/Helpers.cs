﻿using System.Configuration;
using System;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool
{
    public static class Helpers
    {
        public static string FlServiceBaseUrl { get { return ConfigurationManager.AppSettings["FirstLookServicesApiUrl"].TrimEnd(new[] { '/', '\\' }); } }
        public static string FlServiceApiUser
        {
            get
            {
                return ConfigurationManager.AppSettings["FirstLookServiceApiUser"];
            }
        }
        public static string FlServiceApiPass
        {
            get
            {
                return ConfigurationManager.AppSettings["FirstLookServiceApiPass"];
            }
        }

        public static string AutocheckUrl { get { return ConfigurationManager.AppSettings["AutocheckUrl"]; } }

        public static int MaxProfitLowCountThreshold { get { return Convert.ToInt32(ConfigurationManager.AppSettings["MaxProfitLowCountThreshold"]); } }
    }
}
