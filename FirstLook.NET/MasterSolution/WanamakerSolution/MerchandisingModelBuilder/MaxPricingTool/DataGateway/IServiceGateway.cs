﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway
{
    public interface IServiceGateway<out T>
    {
        /// <summary>
        /// Return Data from Url
        /// </summary>
        /// <param name="url">Service URL</param>
        /// <param name="apiUser">UserName</param>
        /// <param name="apiPass">Password</param>
        /// <param name="method">Service Method - GET,POST,PUT,DELETE</param>
        /// <returns></returns>
        T GetData(string url, string apiUser, string apiPass, string method);
        T PostData(string url, string apiUser, string apiPass, string method, object request);        
    }
}
