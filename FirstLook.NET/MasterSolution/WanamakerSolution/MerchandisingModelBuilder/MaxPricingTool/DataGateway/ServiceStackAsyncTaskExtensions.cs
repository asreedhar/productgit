﻿using System.Threading.Tasks;
using ServiceStack.Service;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway
{
    public static class ServiceStackAsyncTaskExtensions
    {
        /// <summary>
        /// Return a Task of <typeparam name=">TResponse"></typeparam> for async calls on ServiceStack clients.
        /// </summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="client"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        /// <remarks>ServiceStack client v3 uses MS' Asynchronus Programming Model and we want a task.</remarks>
        public static Task<TResponse> GetTask<TResponse>(this IServiceClient client, string url)
        {
            var tcs = new TaskCompletionSource<TResponse>();

            client.GetAsync<TResponse>(url,
                tcs.SetResult,
                (response, exc) => tcs.SetException(exc)
            );

            return tcs.Task;
        }

        public static Task<TResponse> PostTask<TResponse>(this IServiceClient client, object request, string url)
        {
            var tcs = new TaskCompletionSource<TResponse>();

            client.PostAsync<TResponse>(url, request,
                tcs.SetResult,
                (response, exc) => tcs.SetException(exc)
            );

            return tcs.Task;
        }
    }
}
