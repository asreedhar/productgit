﻿using System;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingSummary;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.PricingSummary
{
    public class PricingSummaryRepository : IRepository<PricingSummaryModel, PricingSummaryArgumentDto>
    {
        #region IRepository<AutocheckModel,AutocheckArgumentDto> Members

        public PricingSummaryModel Fetch(PricingSummaryArgumentDto input)
        {
            PricingSummaryModel model = new PricingSummaryModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<PricingSummaryModel>>();

                model = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method,input.PricingSummaryArguments);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }

        public PricingSummaryModel Save(PricingSummaryArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public PricingSummaryModel Update(PricingSummaryArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public PricingSummaryModel Delete(PricingSummaryArgumentDto input)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
