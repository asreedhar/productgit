﻿using System;
using System.Reflection;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingHistory;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingHistory;
using log4net;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.PricingHistory
{
   public class PricingRepository:IRepository<Rsponse.PricingHistoryModel,PricingHistoryArgumentDto>
   {
       #region Logging

       protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

       #endregion Logging

       #region IRepository<PricingHistoryModel,PricingHistoryArgumentDto> Members
       public Rsponse.PricingHistoryModel Fetch(PricingHistoryArgumentDto input)
       {
           Rsponse.PricingHistoryModel model = new Rsponse.PricingHistoryModel();
      
           try
           {
               var service = Registry.Resolve<IServiceGateway<Rsponse.PricingHistoryModel>>();
               model = service.GetData(input.Url,
                   input.ApiUser,
                   input.ApiPass, input.Method);
           }
           catch (Exception ex)
           {
               Log.Error("PricingRepository:Fetch", ex);
           }
           return model;
       }

       public Rsponse.PricingHistoryModel Save(PricingHistoryArgumentDto input)
       {
           throw new System.NotImplementedException();
       }

       public Rsponse.PricingHistoryModel Update(PricingHistoryArgumentDto input)
       {
           throw new System.NotImplementedException();
       }

       public Rsponse.PricingHistoryModel Delete(PricingHistoryArgumentDto input)
       {
           throw new System.NotImplementedException();
       }
       #endregion
   }
}
