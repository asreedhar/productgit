﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingProof;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingProof;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.PricingProof
{
    public class PricingProofRepository : IRepository<PricingProofModel, PricingProofArgumentDto>
    {
       
        public PricingProofModel Fetch(PricingProofArgumentDto input)
        {
            PricingProofModel PricingComparisonList = new PricingProofModel();            
            try
            {
                var service = Registry.Resolve<IServiceGateway<PricingProofModel>>();

                PricingComparisonList = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method, input.pricingCompare);

            }
            catch (Exception ex)
            {

            }
           
            return PricingComparisonList;
        }

        public PricingProofModel Save(PricingProofArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public PricingProofModel Update(PricingProofArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public PricingProofModel Delete(PricingProofArgumentDto input)
        {
            throw new NotImplementedException();
        }
    }
}
