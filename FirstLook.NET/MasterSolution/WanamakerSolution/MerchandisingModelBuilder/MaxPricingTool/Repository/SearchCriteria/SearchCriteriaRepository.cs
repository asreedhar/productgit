﻿
using System;
using System.Reflection;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SearchCriteriaDto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SearchCriteria;
using log4net;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.SearchCriteria
{
    public class SearchCriteriaRepository : IRepository<SearchCriteriaModel, SearchCriteriaArgumentDto>
    {
        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Logging


        #region IRepository<SearchCriteria,SearchCriteriaDto> Members

        public SearchCriteriaModel Fetch(SearchCriteriaArgumentDto input)
        {
            SearchCriteriaModel searchCriteria = new SearchCriteriaModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<SearchCriteriaModel>>();

                searchCriteria = service.GetData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method);

            }
            catch (Exception ex)
            {
                Log.Error("SearchCriteriaModel:Fetch", ex);
            }
            return searchCriteria;
        }

        public SearchCriteriaModel Save(SearchCriteriaArgumentDto input)
        {
            throw new System.NotImplementedException();
        }

        public SearchCriteriaModel Update(SearchCriteriaArgumentDto input)
        {
            SearchCriteriaModel searchCriteria = new SearchCriteriaModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<SearchCriteriaModel>>();

                searchCriteria = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method,input.SearchCriteriaDto);

            }
            catch (Exception ex)
            {

            }
            return searchCriteria;
        }

        public SearchCriteriaModel Delete(SearchCriteriaArgumentDto input)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
