﻿using System;
using System.Reflection;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.VehicleHistoryReport;
using log4net;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.VehicleHistoryReport
{
    public class AutocheckRepository:IRepository<AutocheckModel,AutocheckArgumentDto>
    {
        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Logging

        #region IRepository<AutocheckModel,AutocheckArgumentDto> Members

        public AutocheckModel Fetch(AutocheckArgumentDto input)
        {
            AutocheckModel model = new AutocheckModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<AutocheckModel>>();

                model = service.GetData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method);

            }
            catch (Exception ex)
            {
                Log.Error("AutoCheckRepository:Fetch", ex);
                if (ex.Message.Contains(VhrStringKeys.UnAuthorizedToVHR))
                {
                    model.HasPurchaseReportRights = false;
                    model.HasAutocheck = false;
                    model.Inspections = null;
                }
                else
                {
                    if (ex.Message.Contains(VhrStringKeys.UserMayOrderReport))
                    {
                        model.HasPurchaseReportRights = true;
                        model.HasAutocheck = true;
                        model.Inspections = null;
                    }
                }
            }

            return model;
        }

        public AutocheckModel Save(AutocheckArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public AutocheckModel Update(AutocheckArgumentDto input)
        {
            AutocheckModel model = new AutocheckModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<AutocheckModel>>();

                model = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method,new object());

            }
            catch (Exception ex)
            {
                Log.Error("AutocheckModel:Update", ex);
                if (ex.Message.Contains(VhrStringKeys.UnAuthorizedToVHR))
                {
                    model.HasPurchaseReportRights = false;
                    model.HasAutocheck = false;
                    model.Inspections = null;
                }
                else
                {
                    if (ex.Message.Contains(VhrStringKeys.UserMayOrderReport))
                    {
                        model.HasPurchaseReportRights = true;
                        model.HasAutocheck = true;
                        model.Inspections = null;
                    }
                }
            }

            return model;
        }

        public AutocheckModel Delete(AutocheckArgumentDto input)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
