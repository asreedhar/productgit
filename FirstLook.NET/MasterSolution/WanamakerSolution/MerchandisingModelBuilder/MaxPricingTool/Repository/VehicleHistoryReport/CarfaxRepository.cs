﻿using System;
using System.Reflection;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.VehicleHistoryReport;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.VehicleHistoryReport;
using log4net;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.VehicleHistoryReport
{
    public class CarfaxRepository:IRepository<CarfaxModel,CarfaxArgumentDto>
    {
        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Logging

        #region IRepository<CarfaxModel,CarfaxArgumentDto> Members

        /// <summary>
        /// This method will fetch data from Rest servcie and bind it with model
        /// </summary>
        /// <param name="input">Carfax Details</param>
        /// <returns>Carfax Model- Carfax Data from REST service</returns>
        public CarfaxModel Fetch(CarfaxArgumentDto input)
        {
            CarfaxModel model=new CarfaxModel();
            try
            {
                var service= Registry.Resolve<IServiceGateway<CarfaxModel>>();
                
                model = service.GetData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method);

            }
            catch (Exception ex)
            {
                Log.Error("CarfaxModel:Fetch", ex);
                if (ex.Message.Contains(VhrStringKeys.UnAuthorizedToVHR))
                {
                    model.HasPurchaseReportRights = false;
                }
                else
                {
                    if (ex.Message.Contains(VhrStringKeys.UserMayOrderReport))
                    {
                        model.Inspections = null;
                        model.HasPurchaseReportRights = true;
                        model.HasCarfax = true;
                    }
                    else
                    {
                        if (ex.Message.Contains(VhrStringKeys.NoCarfaxData))
                        {
                            model.HasPurchaseReportRights = true;
                            model.HasCarfax = true;
                            throw new Exception(VhrStringKeys.NoCarfaxData);                    
                        }
                    }
                }
            }

            return model;
        }

        public CarfaxModel Save(CarfaxArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public CarfaxModel Update(CarfaxArgumentDto input)
        {
            CarfaxModel model = new CarfaxModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<CarfaxModel>>();

                model = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method,new object());

            }
            catch (Exception ex)
            {
                Log.Error("CarfaxModel:Update", ex);
                if (ex.Message.Contains(VhrStringKeys.UserMayOrderReport))
                {
                    model.Inspections = null;
                    model.HasPurchaseReportRights = true;
                    model.HasCarfax = true;
                }
                else
                {
                    if (ex.Message.Contains(VhrStringKeys.NoCarfaxData))
                    {
                        model.HasPurchaseReportRights = true;
                        model.HasCarfax = true;
                        throw new Exception(VhrStringKeys.NoCarfaxData);
                    }
                }
            }

            return model;
        }

        public CarfaxModel Delete(CarfaxArgumentDto input)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
