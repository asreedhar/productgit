﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveInternetPrice;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveInternetPrice;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.SaveInternetPrice
{
    public class SaveInternetPriceRepository:IRepository<SaveInternetPriceModel,SaveInternetPriceArgumentDto>
    {


        public SaveInternetPriceModel Fetch(SaveInternetPriceArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public SaveInternetPriceModel Save(SaveInternetPriceArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public SaveInternetPriceModel Update(SaveInternetPriceArgumentDto input)
        {
            SaveInternetPriceModel model = new SaveInternetPriceModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<SaveInternetPriceModel>>();
                var request = new SaveInternetPriceArgumentDto() { NewListPrice = input.NewListPrice, UserName =input.UserName};
                model = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method, request);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }
        

        public SaveInternetPriceModel Delete(SaveInternetPriceArgumentDto input)
        {
            throw new NotImplementedException();
        }
    }
}
