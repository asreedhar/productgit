﻿using System;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.ApproveAd;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.ApproveAd
{
    public class ApproveAdRepository : IRepository<UpdateInternetPriceNApproveAdModel, SaveInternetPriceNApproveAdArgumentDto>
    {

        #region IRepository<UpdateInternetPriceNApproveAdModel,SaveInternetPriceNApproveAdArgumentDto> Members

        public UpdateInternetPriceNApproveAdModel Fetch(SaveInternetPriceNApproveAdArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public UpdateInternetPriceNApproveAdModel Save(SaveInternetPriceNApproveAdArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public UpdateInternetPriceNApproveAdModel Update(SaveInternetPriceNApproveAdArgumentDto input)
        {
            UpdateInternetPriceNApproveAdModel model = new UpdateInternetPriceNApproveAdModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<UpdateInternetPriceNApproveAdModel>>();
                var request = new ApproveAdInputDto() { NewPrice = input.NewPrice, OldPrice = input.OldPrice, SpecialPrice = input.SpecialPrice, UserName = input.UserName };
                model = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method, request);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }

        public UpdateInternetPriceNApproveAdModel Delete(SaveInternetPriceNApproveAdArgumentDto input)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
