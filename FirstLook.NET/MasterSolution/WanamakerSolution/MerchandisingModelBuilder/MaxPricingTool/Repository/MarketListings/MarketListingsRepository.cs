﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.MarketListings
{
    class MarketListingsRepository : IRepository<MarketListingsModel, MarketListingsArgumentDto>
    {
        #region IRepository<MarketListingsModel,MarketListingsArgumentDto> Members


        public MarketListingsModel Fetch(MarketListingsArgumentDto input)
        {           
            MarketListingsModel model = new MarketListingsModel();
            try
            {
                var service = Registry.Resolve<IServiceGateway<MarketListingsModel>>();

                model = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method, input.RequestData);
            }
            catch (Exception ex)
            {

            }

            return model;
        }

        public MarketListingsModel Save(MarketListingsArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public MarketListingsModel Update(MarketListingsArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public MarketListingsModel Delete(MarketListingsArgumentDto input)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
