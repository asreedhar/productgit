﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveTrim;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveTrim;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.SaveTrim
{
    class SaveTrimRepository:IRepository<SaveTrimModel,SaveTrimArgumentDto>
    {

        public SaveTrimModel Fetch(SaveTrimArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public SaveTrimModel Save(SaveTrimArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public SaveTrimModel Update(SaveTrimArgumentDto input)
        {
            SaveTrimModel model = new SaveTrimModel();
            try
            {
                var request = new SaveTrimArgumentDto() {ChromeStyleId=input.ChromeStyleId };
                var service = Registry.Resolve<IServiceGateway<SaveTrimModel>>();

                model = service.PostData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method, request);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }

        public SaveTrimModel Delete(SaveTrimArgumentDto input)
        {
            throw new NotImplementedException();
        }
    }
}
