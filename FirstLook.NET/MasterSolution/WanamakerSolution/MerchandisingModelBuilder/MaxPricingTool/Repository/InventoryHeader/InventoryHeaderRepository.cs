﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.InventoryHeader;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.InventoryHeader;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.DataGateway;
using log4net;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository.InventoryHeader
{
    class InventoryHeaderRepository:IRepository<InventoryHeaderModel,InventoryHeaderArgumentDto>
    {
        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Logging

        #region IRepository<InventoryHeaderModel,InventoryHeaderArgumentDto>

        public InventoryHeaderModel Fetch(InventoryHeaderArgumentDto input)
        {
            InventoryHeaderModel model = new InventoryHeaderModel();

            try
            {
                var service = Registry.Resolve<IServiceGateway<InventoryHeaderModel>>();

                model = service.GetData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method);
            }
            catch (Exception ex)
            {
                Log.Error("InventoryHeaderModel: Fetch", ex);
            }

            return model;
        }

        public InventoryHeaderModel Save(InventoryHeaderArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public InventoryHeaderModel Update(InventoryHeaderArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public InventoryHeaderModel Delete(InventoryHeaderArgumentDto input)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
