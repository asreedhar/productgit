﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary
{
    [DataContract]
    public class Graph
    {
        [DataMember(Name = "buckets")]
        public List<Bucket> Buckets { get; set; }
    }
}
