﻿using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary
{
    [DataContract]
    public class Bucket
    {
        [DataMember(Name = "bucketId")]
        public int BucketId { get; set; }

        [DataMember(Name = "bucketName")]
        public string BucketName { get; set; }

        [DataMember(Name = "bucketunits")]
        public int Bucketunits { get; set; }

        [DataMember(Name = "bucketPercentage")]
        public int BucketPercentage { get; set; }
    }
}
