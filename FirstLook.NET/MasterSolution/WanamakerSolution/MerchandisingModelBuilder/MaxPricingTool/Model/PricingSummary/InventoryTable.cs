﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary
{
    [DataContract]
    public class InventoryTable
    {
        [DataMember(Name = "inventory")]
        public List<Inventory> Inventory { get; set; } 
    }
}
