﻿using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary
{
    [DataContract]
    public class PricingSummaryModel
    {
        [DataMember(Name = "ownerName")]
        public string OwnerName { get; set; }

        [DataMember(Name = "pricingGraph")]
        public Graph PricingGraph { get; internal set; }

        [DataMember(Name = "pringGraphSummary")]
        public GraphSummary PringGraphSummary { get; internal set; }

        [DataMember(Name = "pricingInventoryTable")]
        public InventoryTable PricingInventoryTable { get; internal set; }

        [DataMember(Name = "bucketName")]
        public string BucketName { get; set; }
    }
}
