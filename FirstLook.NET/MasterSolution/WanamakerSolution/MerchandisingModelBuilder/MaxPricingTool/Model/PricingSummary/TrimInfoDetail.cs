﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary
{
    [DataContract]
    public class TrimInfoDetail
    {
        [DataMember(Name = "chromeStyleId")]
        public int ChromeStyleId { get; set; }

        [DataMember(Name = "trim")]
        public string Trim { get; set; }
    }
}
