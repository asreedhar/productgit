﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary
{
    [DataContract]
    public class BookDeatil
    {
        [DataMember(Name = "bookName")]
        public string BookName { get; set; }

        [DataMember(Name = "bookValue")]
        public int Bookvalue { get; set; }
    }
}
