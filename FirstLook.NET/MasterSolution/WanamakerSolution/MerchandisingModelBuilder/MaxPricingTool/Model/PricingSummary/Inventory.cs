﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingSummary
{
    [DataContract]
    public class Inventory
    {
        [DataMember(Name = "age")]
        public int Age { get; set; }

        [DataMember(Name = "vehicleDescription")]
        public string VehicleDescription { get; set; }

        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "vehicleColor")]
        public string VehicleColor { get; set; }

        [DataMember(Name = "vehicleMileage")]
        public int VehicleMileage { get; set; }

        [DataMember(Name = "unitCost")]
        public int UnitCost { get; set; }

        [DataMember(Name = "internetPrice")]
        public int InternetPrice { get; set; }

        [DataMember(Name = "pctMarketAverage")]
        public decimal? PctMarketAverage { get; set; }

        [DataMember(Name = "marketDaysSupply")]
        public int MarketDaysSupply { get; set; }

        [DataMember(Name = "priceComparisons")]
        public List<BookDeatil> PriceComparisons { get; set; }

        [DataMember(Name = "stockNumber")]
        public string StockNumber { get; set; }

        [DataMember(Name = "trimList")]
        public List<TrimInfoDetail> TrimList { get; set; }

        [DataMember(Name = "inventoryId")]
        public int InventoryId { get; set; }

        [DataMember(Name = "ComparisonColor")]
        public int ComparisonColor { get; set; }

        [DataMember(Name = "ageBucket")]
        public int AgeBucket { get; set; }
    }
}
