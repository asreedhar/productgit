﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.VehicleHistoryReport
{
    public class CarfaxModel
    {
        [DataMember(Name = "expirationDate")]
        public string ExpirationDate { get; internal set; }

        [DataMember(Name = "userName")]
        public string UserName { get; internal set; }

        [DataMember(Name = "reportType")]
        public string ReportType { get; internal set; }

        [DataMember(Name = "hasProblems")]
        public bool HasProblems { get; internal set; }

        [DataMember(Name = "ownerCount")]
        public int OwnerCount { get; internal set; }

        [DataMember(Name = "href")]
        public string Href { get; internal set; }

        [DataMember(Name = "inspections")]
        public IDictionary<string, bool> Inspections { get; internal set; }

        public bool HasPurchaseReportRights { get; set; }

        public bool HasCarfax { get; set; }
       
    }
}
