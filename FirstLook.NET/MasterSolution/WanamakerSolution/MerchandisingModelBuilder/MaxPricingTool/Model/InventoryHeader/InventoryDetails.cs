﻿using System;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.InventoryHeader
{
    [DataContract]
    public class InventoryDetails
    {
        [DataMember(Name = "inventoryId")]
        public int InventoryId { get; set; }
        [DataMember(Name = "businessUnitId")]
        public int BusinessUnitId { get; set; }
        [DataMember(Name = "vin")]
        public string Vin { get; set; }
        [DataMember(Name = "stockNumber")]
        public string StockNumber { get; set; }
        [DataMember(Name = "statusBucket")]
        public int? StatusBucket { get; set; }
        [DataMember(Name = "inventoryReceivedDate")]
        public DateTime InventoryReceivedDate { get; set; }
        [DataMember(Name = "year")]
        public int Year { get; set; }
        [DataMember(Name = "unitCost")]
        public decimal UnitCost { get; set; }
        [DataMember(Name = "acquisitionPrice")]
        public decimal? AcquisitionPrice { get; set; }
        [DataMember(Name = "certified")]
        public int? Certified { get; set; }
        [DataMember(Name = "certifiedId")]
        public string CertifiedId { get; set; }
        [DataMember(Name = "Make")]
        public string Make { get; set; }
        [DataMember(Name = "Model")]
        public string Model { get; set; }
        [DataMember(Name = "trim")]
        public int? ChromeStyleId { get; set; }
        [DataMember(Name = "trimName")]
        public string Trim { get; set; }
        [DataMember(Name = "tradePurchase")]
        public int TradePurchase { get; set; }
        [DataMember(Name = "mileage")]
        public int? Mileage { get; set; }
        [DataMember(Name = "listPrice")]
        public decimal? ListPrice { get; set; }
        [DataMember(Name = "baseColor")]
        public string BaseColor { get; set; }
        [DataMember(Name = "extColor1")]
        public string ExtColor1 { get; set; }
        [DataMember(Name = "exteriorColorCode")]
        public string ExteriorColorCode { get; set; }
        [DataMember(Name = "extColor2")]
        public string ExtColor2 { get; set; }
        [DataMember(Name = "exteriorColorCode2")]
        public string ExteriorColorCode2 { get; set; }
        [DataMember(Name = "interiorColor")]
        public string InteriorColor { get; set; }
        [DataMember(Name = "interiorColorCode")]
        public string InteriorColorCode { get; set; }
        [DataMember(Name = "vehicleLocation")]
        public string VehicleLocation { get; set; }
        [DataMember(Name = "inventoryStatusCD")]
        public int InventoryStatusCD { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }
        [DataMember(Name = "lastUpdateListPrice")]
        public decimal LastUpdateListPrice { get; set; }
        [DataMember(Name = "datePosted")]
        public DateTime DatePosted { get; set; }
        [DataMember(Name = "exteriorStatus")]
        public int ExteriorStatus { get; set; }
        [DataMember(Name = "interiorStatus")]
        public int InteriorStatus { get; set; }
        [DataMember(Name = "photoStatus")]
        public int PhotoStatus { get; set; }
        [DataMember(Name = "adReviewStatus")]
        public int AdReviewStatus { get; set; }
        [DataMember(Name = "pricingStatus")]
        public int PricingStatus { get; set; }
        [DataMember(Name = "postingStatus")]
        public int PostingStatus { get; set; }
        [DataMember(Name = "onlineFlag")]
        public int OnlineFlag { get; set; }
        [DataMember(Name = "inventoryType")]
        public int InventoryType { get; set; }
        [DataMember(Name = "desiredPhotoCount")]
        public int DesiredPhotoCount { get; set; }
        [DataMember(Name = "lotPrice")]
        public decimal LotPrice { get; set; }
        [DataMember(Name = "msrp")]
        public decimal Msrp { get; set; }
        [DataMember(Name = "specialPrice")]
        public decimal SpecialPrice { get; set; }
        [DataMember(Name = "dueForReprice")]
        public decimal DueForReprice { get; set; }
        [DataMember(Name = "lowActivityFlag")]
        public decimal LowActivityFlag { get; set; }
        [DataMember(Name = "hasCurrentBookValue")]
        public decimal HasCurrentBookValue { get; set; }
        [DataMember(Name = "hasKeyInformation")]
        public decimal HasKeyInformation { get; set; }
        [DataMember(Name = "hasCarfax")]
        public decimal HasCarfax { get; set; }
        [DataMember(Name = "hasCarfaxAdmin")]
        public decimal HasCarfaxAdmin { get; set; }
        [DataMember(Name = "CertificationName")]
        public string CertificationName { get; set; }
        [DataMember(Name = "certifiedProgramId")]
        public int CertifiedProgramId { get; set; }
        [DataMember(Name = "MakeId")]
        public int MakeId { get; set; }
        [DataMember(Name = "ModelId")]
        public int ModelId { get; set; }
        [DataMember(Name = "CarsDotComMakeID")]
        public int? CarsDotComMakeID { get; set; }
        [DataMember(Name = "CarsDotComModelId")]
        public int? CarsDotComModelId { get; set; }
        [DataMember(Name = "AutoTraderMake")]
        public string AutoTraderMake { get; set; }
        [DataMember(Name = "AutoTraderModel")]
        public string AutoTraderModel { get; set; }
    }
}
