﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.InventoryHeader
{
    [DataContract]
    public class InventoryHeaderModel
    {
        [DataMember(Name = "inventory")]
        public InventoryDetails InventoryInfo { get; set; }

        [DataMember(Name = "trims")]
        public IList<TrimInfo> TrimList { get; set; }

    }
}
