﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.InventoryHeader
{
    public class TrimInfo
    {
        public int ChromeStyleId { get; set; }
        public string Trim { get; set; }
    }
}
