﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SearchCriteria
{
    public class SearchCriteriaModel
    {
        public string Key { get; set; }

        public SearchCriteriaDto Criteria { get; set; }
    }
}
