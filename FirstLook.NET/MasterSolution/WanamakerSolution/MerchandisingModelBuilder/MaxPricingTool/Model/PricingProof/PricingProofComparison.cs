﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingProof
{
    [DataContract]
    public class PricingProofComparison
    {
        [DataMember]
        public int InventoryId { get; set; }
        [DataMember]
        public List<PricingComparisonDetail> PriceComparisonDetail { get; set; }
    }
}
