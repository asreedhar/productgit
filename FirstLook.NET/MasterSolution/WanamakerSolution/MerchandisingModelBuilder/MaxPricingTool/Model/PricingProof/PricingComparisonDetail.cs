﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingProof
{
    [DataContract]
    public class PricingComparisonDetail
    {
        [DataMember(Name = "BookId")]
        public int BookId { get; set; }
        [DataMember(Name = "BookName")]
        public string BookName { get; set; }
        [DataMember(Name = "BookValue")]
        public int BookValue { get; set; }
        [DataMember(Name = "IsValid")]
        public bool IsValid { get; set; }
        [DataMember(Name = "IsAccurate")]
        public bool IsAccurate { get; set; }
    }
}
