﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingProof
{
    [DataContract]
    public class PricingProofModel
    {
        [DataMember(Name = "PriceComaprisons")]
        public List<PricingProofComparison> PriceComparisonList { get; set; }        
    }
}
