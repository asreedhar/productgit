﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings
{
    [DataContract]
    public class FacetEquipments
    {
        [DataMember(Name = "key", EmitDefaultValue = false)]
        public string Key { get; set; }

        [DataMember(Name = "Level", EmitDefaultValue = false)]
        public int Level { get; set; }

        [DataMember(Name = "doc_count", EmitDefaultValue = false)]
        public int DocCount { get; set; }

    }
}
