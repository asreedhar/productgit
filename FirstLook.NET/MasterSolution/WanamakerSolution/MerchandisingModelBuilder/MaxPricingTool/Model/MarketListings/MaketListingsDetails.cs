﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings
{
    [DataContract]
   public class MaketListingsDetails
    {
        [DataMember(Name = "age")]
        public int? Age { get; set; }
        [DataMember(Name = "seller")]
        public string Seller { get; set; }
        [DataMember(Name = "certified")]
        public bool Certified { get; set; }
        [DataMember(Name = "color")]
        public string Color { get; set; }
        [DataMember(Name = "mileage")]
        public int? Mileage { get; set; }
        [DataMember(Name = "internetPrice")]
        public int? InternetPrice { get; set; }
        [DataMember(Name = "distance")]
        public int? Distance { get; set; }
        [DataMember(Name = "vin")]
        public string Vin { get; set; }
        [DataMember(Name = "year")]
        public int? Year { get; set; }
        [DataMember(Name = "make")]
        public string Make { get; set; }
        [DataMember(Name = "model")]
        public string Model { get; set; }
        [DataMember(Name = "trim")]
        public string Trim { get; set; }
    }
}
