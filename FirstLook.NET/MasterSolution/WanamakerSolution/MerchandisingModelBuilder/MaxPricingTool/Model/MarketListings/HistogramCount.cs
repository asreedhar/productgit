﻿using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings
{
    [DataContract]
    public class HistogramCount
    {
        [DataMember(Name = "key", EmitDefaultValue = false)]
        public int Key { get; set; }

        [DataMember(Name = "doc_count", EmitDefaultValue = false)]
        public int DocCount { get; set; }
    }
}
