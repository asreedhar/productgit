﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings
{
    [DataContract]
    public class MarketListingsModel
    {
        [DataMember(Name = "marketListings")]
        public List<MarketListingsResponse> MarketListingsResponse { get; set; }
    }
}
