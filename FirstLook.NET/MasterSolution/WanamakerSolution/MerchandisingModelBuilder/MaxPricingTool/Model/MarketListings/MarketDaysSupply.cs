﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings
{
    public class MarketDaysSupply
    {
        public int MarketDaysSupplyCount { get; set; }

        public int SearchTypeId { get; set; }
    }
}
