﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings
{
     [DataContract]
    public class SearchMarketListingInput
    {
        [DataMember(Name = "SearchMarketListings")]
         public List<SearchMarketListings> SearchMarketListings { get; set; }
    }
}
