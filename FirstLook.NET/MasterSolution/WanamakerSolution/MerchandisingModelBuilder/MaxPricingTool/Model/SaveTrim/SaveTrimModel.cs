﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveTrim
{
    [DataContract]
    public class SaveTrimModel
    {
        [DataMember(Name = "ChromeStyleId")]
        public int ChromeStyleId { get; set; }
        [DataMember(Name = "flag")]
        public bool Flag { get; set; }
    }
}
