﻿using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd
{
    [DataContract]
    public class UpdateInternetPriceNApproveAdModel
    {
        [DataMember]
        public bool AdApproved { get; set; }

        [DataMember]
        public UpdateListPriceResultsDto ChangeInternetPriceResults { get; set; }
    }
}
