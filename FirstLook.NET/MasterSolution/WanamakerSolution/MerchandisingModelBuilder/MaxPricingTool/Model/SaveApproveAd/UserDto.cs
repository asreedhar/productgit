﻿using System;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd
{
    [Serializable]
    public class UserDto
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
