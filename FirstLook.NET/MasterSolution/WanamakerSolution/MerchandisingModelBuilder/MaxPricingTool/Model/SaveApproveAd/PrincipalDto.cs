﻿using System;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd
{
    public class PrincipalDto
    {
        public string AuthorityName { get; set; }

        public UserDto User { get; set; }

        public Guid Handle { get; set; }
    }
}
