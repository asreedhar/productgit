﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd
{
    public class UpdateListPriceResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public UpdateListPriceArgumentsDto Arguments { get; set; }

        public InventoryListPrice ListPriceUpdate { get; set; }
    }
}
