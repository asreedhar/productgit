﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd
{
    public class InventoryListPrice
    {
        public int InventoryId { get; set; }

        public decimal OldListPrice { get; set; }

        public decimal NewListPrice { get; set; }
    }
}
