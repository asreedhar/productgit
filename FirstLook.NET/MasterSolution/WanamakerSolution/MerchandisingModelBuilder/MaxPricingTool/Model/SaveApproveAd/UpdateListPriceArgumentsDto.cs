﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveApproveAd
{
    public class UpdateListPriceArgumentsDto
    {
        public string UserName { get; set; }

        public int BusinessUnitId { get; set; }

        public int InventoryId { get; set; }

        public decimal NewListPrice { get; set; }
    }
}
