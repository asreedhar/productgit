﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SaveInternetPrice
{
    [DataContract]
    public class SaveInternetPriceModel
    {
        [DataMember(Name = "inventoryId")]
        public int InventoryId { get; set; }

        [DataMember(Name = "oldListprice")]
        public decimal OldListPrice { get; set; }

        [DataMember(Name = "newListPrice")]
        public decimal NewListPrice { get; set; }

    }
}
