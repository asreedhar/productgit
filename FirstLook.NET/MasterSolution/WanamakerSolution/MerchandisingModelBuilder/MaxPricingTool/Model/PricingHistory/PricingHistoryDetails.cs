﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingHistory
{
    public class PricingHistoryDetails
    {
        [DataMember(Name = "inventoryId")]
        public int InventoryId { get; internal set; }
        [DataMember(Name = "login")]
        public string Login { get; internal set; }
        [DataMember(Name = "listPrice")]
        public int ListPrice { get; internal set; }
        [DataMember(Name = "updatedDate")]
        public string UpdatedDate { get; internal set; }
    }
}
