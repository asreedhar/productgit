﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.PricingHistory
{
    public class Rsponse
    {
        [DataContract]
        public class PricingHistoryModel
        {

            [DataMember(Name = "pricinghistory")]
            public IList<PricingHistoryDetails> PricingHistory { get; set; }
        }

    }
}
