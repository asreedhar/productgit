﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wanamaker.WebApp.Areas.PricingAnalysis.Models
{
    public class UpdateInternetPriceNApproveAdModel
    {
        public bool AdApproved { get; set; }

        public UpdateListPriceResultsDto ChangeInternetPriceResults { get; set; }
    }

    public class UpdateListPriceResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public UpdateListPriceArgumentsDto Arguments { get; set; }

        public InventoryListPrice ListPriceUpdate { get; set; }

    }

    [Serializable]
    public class PrincipalDto
    {
        public string AuthorityName { get; set; }

        public UserDto User { get; set; }

        public Guid Handle { get; set; }
    }

    [Serializable]
    public class UserDto
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }

    public class UpdateListPriceArgumentsDto
    {
        public string UserName { get; set; }

        public int BusinessUnitId { get; set; }

        public int InventoryId { get; set; }

        public decimal NewListPrice { get; set; }
    }

    public class InventoryListPrice
    {
        public int InventoryId { get; set; }

        public decimal OldListPrice { get; set; }

        public decimal NewListPrice { get; set; }
    }
}