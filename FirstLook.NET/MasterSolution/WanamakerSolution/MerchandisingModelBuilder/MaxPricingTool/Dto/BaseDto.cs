﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto
{
    /// <summary>
    /// Base Class for Models that includes Properties required to Query REST service
    /// </summary>
    public class BaseDto
    {
        public string ApiUser { get; set; }
        public string ApiPass { get; set; }
        public long Timeout { get; set; }
        public string Url { get; set; }
        public string Method { get; set; }
    }
}
