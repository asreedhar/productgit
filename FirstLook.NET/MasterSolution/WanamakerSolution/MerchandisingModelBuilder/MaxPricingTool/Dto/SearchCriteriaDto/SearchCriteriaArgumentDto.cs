﻿
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SearchCriteriaDto
{
    [DataContract]
    public class SearchCriteriaArgumentDto:BaseDto
    {
         [DataMember(Name = "PostSearchCriteriaRequest")]
        public Model.SearchCriteria.SearchCriteriaDto SearchCriteriaDto { get; set; }
    }
   
}
