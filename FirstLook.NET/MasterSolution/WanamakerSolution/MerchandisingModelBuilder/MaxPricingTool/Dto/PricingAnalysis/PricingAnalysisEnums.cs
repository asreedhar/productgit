﻿namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingAnalysis
{
    public class PricingAnalysisEnums
    {
        public static readonly int DefaultPageSize = 10;

        public enum Aggregations
        {
            equipment_terms,
            price_stats,
            trim_terms,
            transmission_terms,
            drive_train_terms,
            oem_certified_terms,
            engine_terms,
            fuel_type_terms,
            odometer_stats
        }

        public const string AllTrimsDisplayName="All Trims";
        public const string AllTrimsDisplayId = "AllTrims";
        public const string UndefinedDisplayName = "Undefined Trims";
        public const string UndefinedDisplayId = "UndefinedTrims";
        public const string UndefinedFilter = "Undefined";
        public const string RecentActiveDisplayName = "Recent & Active";
        public const string RecentActiveDisplayId = "RecentActive";
        public const string ActiveDisplayName = "Active";
        public const string ActiveDisplayId = "Active";
    }
}