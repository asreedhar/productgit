﻿using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingProof
{
    [DataContract]
    public class PricingProofArgumentDto : BaseDto
    {
        [DataMember(Name = "PriceComaprison")]
        public PriceComaprison pricingCompare { get; set; }

    }
}
