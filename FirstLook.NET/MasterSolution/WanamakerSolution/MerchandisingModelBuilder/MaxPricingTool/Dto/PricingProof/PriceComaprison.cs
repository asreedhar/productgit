﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingProof
{
    [DataContract]
    public class PriceComaprison
    {
        [DataMember(Name = "InventoryIds")]
        public List<int> InventoryIds { get; set; }
    }
}
