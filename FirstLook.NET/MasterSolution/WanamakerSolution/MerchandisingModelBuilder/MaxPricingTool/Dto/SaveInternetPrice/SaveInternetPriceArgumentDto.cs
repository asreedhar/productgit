﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveInternetPrice
{
    public class SaveInternetPriceArgumentDto:BaseDto
    {
        public decimal NewListPrice { get; set; }
        public string  UserName { get; set; }
    }
}
