﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingSummary
{
    public class PricingSummaryDto
    {
        public int DealerId { get; set; }

        public string Mode { get; set; }

        public string SalesStrategy { get; set; }

        public int ColumnIndex { get; set; }

        public string Filtermode { get; set; }

        public string SortColumns { get; set; }

        public int MaximumRows { get; set; }

        public int StartRowIndex { get; set; }

        public string InventoryFilter { get; set; }

        public bool IsAggregateAndDetail { get; set; }
    }
}
