﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingSummary
{
    public class PricingSummaryArgumentDto : BaseDto
    {
        public PricingSummaryDto PricingSummaryArguments { get; set; }
    }
}
