﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.ApproveAd
{
    public class SaveInternetPriceNApproveAdArgumentDto:BaseDto
    {
        public decimal OldPrice { get; set; }
        public decimal NewPrice { get; set; }
        public decimal? SpecialPrice { get; set; }
        public string UserName { get; set; }
    }
}
