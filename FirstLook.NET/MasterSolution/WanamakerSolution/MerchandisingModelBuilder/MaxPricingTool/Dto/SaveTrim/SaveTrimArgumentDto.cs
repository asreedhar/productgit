﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SaveTrim
{
    public class SaveTrimArgumentDto:BaseDto
    {
        public int ChromeStyleId { get; set; }
    }
}
