﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings
{   
    [DataContract]
    public class MarketListingsArgumentDto:BaseDto
    {
        [DataMember(Name = "SearchMarketListings")]
        public MarketListingRequestDto RequestData { get; set; }
    }
}
