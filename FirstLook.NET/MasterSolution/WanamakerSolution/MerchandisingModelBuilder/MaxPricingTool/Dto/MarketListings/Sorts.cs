﻿
namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings
{
    public class Sorts
    {
        public string Field { get; set; }

        public string Order { get; set; }
    }
}
