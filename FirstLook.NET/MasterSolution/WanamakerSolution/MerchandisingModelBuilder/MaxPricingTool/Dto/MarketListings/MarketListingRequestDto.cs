﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings
{
    [DataContract]
    public class MarketListingRequestDto
    {
        [DataMember(Name = "searchmarketListings")]
        public List<SearchMarketListings> SearchMarketListings { get; set; }
    }
}
