﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Messages;
using FirstLook.Common.Core.Extensions;
using MAX.Entities;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.GoogleAnalytics;
using NUnit.Framework;

namespace Merchandising.Messages.Tests
{
    public class Tests
    {
        
        [Test]
        public void NewInventoryMessageToInventoryChangeMessage()
        {
            var newInventory = new NewInventoryMessage(200, 201);
            newInventory.AddError("I had an error");
            string json = newInventory.ToJson();

            var inventoryChange = json.FromJson<InventoryMessage>();
            Assert.IsTrue(inventoryChange.GetType() == typeof(InventoryMessage));
            Assert.IsTrue(inventoryChange.BusinessUnitId == 200);
            Assert.IsTrue(inventoryChange.InventoryId == 201);
            Assert.IsTrue(inventoryChange.Errors.Length == 1);
            Assert.IsTrue(inventoryChange.Errors[0] == "I had an error");
        }

        [Test]
        public void AutoApproveInventoryMessageTest()
        {
            var message = new AutoApproveInventoryMessage(200, AutoApproveStatus.OnForUsed, WorkflowType.CreateInitialAd);
            string json = message.ToJson();

            var returnedMessage = json.FromJson<AutoApproveInventoryMessage>();
            Assert.IsTrue(returnedMessage.BusinessUnitId == 200);
            Assert.IsTrue(returnedMessage.Status == AutoApproveStatus.OnForUsed);
            Assert.IsTrue(returnedMessage.Workflow == WorkflowType.CreateInitialAd);
        }

        [Test]
        public void DailyEmailMessageTest()
        {
            var message = new DailyEmailMessage(102225, "email@me.com");
            string json = message.ToJson();

            var returnedMessage = json.FromJson<DailyEmailMessage>();
            Assert.IsTrue(returnedMessage.BusinessUnitId == 102225);
            Assert.IsTrue(returnedMessage.Email == "email@me.com");

        }

        [Test]
        public void AutoApproveMessageTest()
        {
            var message = new AutoApproveMessage(102, 101, "user");
            string json = message.ToJson();

            var returnedMessage = json.FromJson<AutoApproveMessage>();
            Assert.IsTrue(returnedMessage.BusinessUnitId == 102);
            Assert.IsTrue(returnedMessage.InventoryId == 101);
            Assert.IsTrue(returnedMessage.UserName == "user");
        }

        [Test]
        public void AutoLoadResultMessageTest()
        {
            string xmlText = string.Empty;
            using (TextReader textReader = new StreamReader(typeof(Tests).Assembly.GetManifestResourceStream("Merchandising.Messages.Tests.Testfiles.FetchResponse.txt")))
                xmlText = textReader.ReadToEnd();
            //public AutoLoadResultMessage(int businessUnitId, int inventoryId, string result, Status status, Agent agent, int inventoryType)
            var message = new AutoLoadResultMessage(102, 101, xmlText, Status.Successful, Agent.DealerSpeed, 2);
            string json = message.ToJson();

            var returnedMessage = json.FromJson<AutoLoadResultMessage>();
            Assert.IsTrue(returnedMessage.BusinessUnitId == 102);
            Assert.IsTrue(returnedMessage.InventoryId == 101);
            Assert.IsTrue(returnedMessage.XmlResult == xmlText);
            Assert.IsTrue(returnedMessage.Status == Status.Successful);
            Assert.IsTrue(returnedMessage.Agent == Agent.DealerSpeed);
        }

        [Test]
        public void AutoLoadResultMessageNullTest()
        {
            var message = new AutoLoadResultMessage(102, 101, null, Status.NoVehicleFound, Agent.FordConnect, 2);
            string json = message.ToJson();

            var returnedMessage = json.FromJson<AutoLoadResultMessage>();
            Assert.IsTrue(returnedMessage.BusinessUnitId == 102);
            Assert.IsTrue(returnedMessage.InventoryId == 101);
            Assert.IsTrue(returnedMessage.XmlResult == null);
            Assert.IsTrue(returnedMessage.Status == Status.NoVehicleFound);
            Assert.IsTrue(returnedMessage.Agent == Agent.FordConnect);
        }

        [Test]
        public void AutoLoadResultMessageNoAgent()
        {
            string json = "{\"BusinessUnitId\":102,\"InventoryId\":101,\"XmlResult\":null,\"Status\":-98,\"Errors\":[]}";
            var returnedMessage = json.FromJson<AutoLoadResultMessage>();

            Assert.IsTrue(returnedMessage.BusinessUnitId == 102);
            Assert.IsTrue(returnedMessage.InventoryId == 101);
            Assert.IsTrue(returnedMessage.XmlResult == null);
            Assert.IsTrue(returnedMessage.Status == Status.NoVehicleFound);
            Assert.IsTrue(returnedMessage.Agent == Agent.Undefined);
        }

        [Test]
        public void AutoLoadResultMessageBackwardsCompatiable()
        {
            string json = "{\"BusinessUnitId\":102,\"InventoryId\":101,\"XmlResult\":null,\"BadCredentials\":false,\"Errors\":[]}";
            var returnedMessage = json.FromJson<AutoLoadResultMessage>();
            Assert.IsTrue(returnedMessage.BusinessUnitId == 102);
            Assert.IsTrue(returnedMessage.InventoryId == 101);
            Assert.IsTrue(returnedMessage.XmlResult == null);
            Assert.IsTrue(returnedMessage.Status == Status.Pending);
        }

        [Test]
        public void TestDeadLetterMessageTest()
        {
            var message = new AutoLoadResultMessage(102, 101, "abc", Status.Successful, Agent.GmGlobalConnect, 2);
            var deadLetterMessage = new DeadLetterMessage(message);
            string json = deadLetterMessage.ToJson();

            Assert.IsTrue(json == "{\"MessageType\":\"Merchandising.Messages.AutoLoad.AutoLoadResultMessage\",\"ErrorMessage\":\"{\\\"BusinessUnitId\\\":102,\\\"InventoryId\\\":101,\\\"XmlResult\\\":\\\"abc\\\",\\\"Status\\\":100,\\\"Agent\\\":2,\\\"InventoryType\\\":2,\\\"Errors\\\":[],\\\"Sent\\\":\\\"0001-01-01T00:00:00\\\",\\\"SentFrom\\\":\\\"\\\",\\\"Delay\\\":\\\"00:00:00\\\"}\",\"Errors\":[],\"Sent\":\"0001-01-01T00:00:00\",\"SentFrom\":\"\",\"Delay\":\"00:00:00\"}");
        }

        [Test]
        public void TestAutoLoadInventoryMessage()
        {
            var message = new AutoLoadInventoryMessage(101, AutoLoadInventoryType.Errors);
            string json = message.ToJson();

            var returnedMessage = json.FromJson<AutoLoadInventoryMessage>();
            Assert.IsTrue(returnedMessage.BusinessUnitId == 101);
            Assert.IsTrue(returnedMessage.Type == AutoLoadInventoryType.Errors);
            Assert.IsTrue(returnedMessage.Agent == Agent.Undefined);
            Assert.IsTrue(returnedMessage.Errors.Length == 0);
        }

        [Test]
        public void TestNoJsonLastRun()
        {
            string json = "{\"BusinessUnitId\":200,\"InventoryId\":201,\"Errors\":[]}";
            var returnedMessage = json.FromJson<NewInventoryMessage>();

            Assert.IsTrue(DateTime.UtcNow > returnedMessage.Sent + returnedMessage.Delay);
        }

        [Test]
        public void TestLastSent()
        {
            var newInventoryMessage = new NewInventoryMessage(200, 201);
            newInventoryMessage.Sent = new DateTime(2012, 10, 1);
            newInventoryMessage.Delay = TimeSpan.FromDays(1);
            string json = newInventoryMessage.ToJson();

            var returnedMessage = json.FromJson<NewInventoryMessage>();
            Assert.IsTrue(returnedMessage.Sent == newInventoryMessage.Sent);
            Assert.IsTrue(returnedMessage.Delay == newInventoryMessage.Delay);
        }

        [Test]
        public void TestEmptyRunSpan()
        {
            var newInventoryMessage = new NewInventoryMessage(200, 201);
            newInventoryMessage.Sent = DateTime.UtcNow;
            string json = newInventoryMessage.ToJson();

            var returnedMessage = json.FromJson<NewInventoryMessage>();
            Assert.IsTrue(returnedMessage.Sent == newInventoryMessage.Sent);
            Assert.IsTrue(returnedMessage.Sent.Kind == DateTimeKind.Utc);
            Assert.IsTrue(returnedMessage.Delay == TimeSpan.Zero);
            Assert.IsFalse(returnedMessage.Delay > TimeSpan.FromMinutes(10));
        }


        [Test]
        public void TestFetchMessage()
        {
            var message = new FetchMessage(101, 1, Agent.FordConnect, "test", "pass", "vin", 0);
            string json = message.ToJson();
            var returnedMessage = json.FromJson<FetchMessage>();

            Assert.IsTrue(returnedMessage.BusinessUnitId == message.BusinessUnitId);
            Assert.IsTrue(returnedMessage.InventoryType == message.InventoryType);
            Assert.IsTrue(returnedMessage.Password == message.Password);
            Assert.IsTrue(returnedMessage.Agent == message.Agent);
            Assert.IsTrue(returnedMessage.UserName == message.UserName);
            Assert.IsTrue(returnedMessage.Vin == message.Vin);
        }

        [TestCase(Description = "Can deserialize a good message")]
        public void Google_Analytics_Generate_Message_Deserialize()
        {
            string jsonText = string.Empty;
            FirstLook.Common.Core.DateRange testRange = new FirstLook.Common.Core.DateRange(2013, 05, 2013, 05);
            GoogleAnalyticsGenerateMessage testMessage = new GoogleAnalyticsGenerateMessage(102, "PC", testRange);

            var serialized = testMessage.ToJson();
            Assert.IsFalse(String.IsNullOrEmpty(serialized));

            var deserialized = serialized.FromJson<GoogleAnalyticsGenerateMessage>();

            Assert.IsTrue(deserialized.BusinessUnitId.Equals(102));
            Assert.IsTrue(deserialized.DateRange.Equals(testRange));
            Assert.IsTrue(deserialized.Force.Equals(false));
            Assert.IsTrue(deserialized.ProfileType.Equals("PC"));
        }

        [Test]
        public void Test_DiscountCampaignExpirationMessage()
        {
            var message = new DiscountCampaignExpirationMessage {BusinessUnitId = 1, CampaignId = 1};
            var json = message.ToJson();
            var returnedMessage = json.FromJson<DiscountCampaignExpirationMessage>();

            Assert.IsTrue(returnedMessage.CampaignId == 1);
            Assert.IsTrue(returnedMessage.BusinessUnitId == 1);
        }

        [Test]
        public void Test_ApplyInventoryDiscountCampaignMessage_Deserialize_ForceIsFalseByDefault()
        {
            var message = new ApplyInventoryDiscountCampaignMessage(1, 2, "testUser");
            var json = message.ToJson();

            var returnedMessage = json.FromJson<ApplyInventoryDiscountCampaignMessage>();

            Assert.That(returnedMessage.InventoryId == 2);
            Assert.That(returnedMessage.BusinessUnitId == 1);
            Assert.That(returnedMessage.ActivatedBy == "testUser");
            Assert.That(returnedMessage.Force == false);
        }

        [Test]
        public void Test_ApplyInventoryDiscountCampaignMessage_Deserialize_ForceCanBeTrue()
        {
            var message = new ApplyInventoryDiscountCampaignMessage(1, 2, "testUser", true);
            var json = message.ToJson();

            var returnedMessage = json.FromJson<ApplyInventoryDiscountCampaignMessage>();

            Assert.That(returnedMessage.InventoryId == 2);
            Assert.That(returnedMessage.BusinessUnitId == 1);
            Assert.That(returnedMessage.ActivatedBy == "testUser");
            Assert.That(returnedMessage.Force);
        }

        [Test]
        public void Test_BatchPriceChangeMessage_Deserialize()
        {
            var testIds = new List<int>() {1, 2, 3};
            var message = new BatchPriceChangeMessage(1, testIds, "testUser");
            var json = message.ToJson();

            var returnedMessage = json.FromJson<BatchPriceChangeMessage>();

            Assert.That(returnedMessage.BusinessUnitId == 1);
            Assert.That(returnedMessage.InventoryIds.Count() == 3);
            Assert.That(returnedMessage.UserName == "testUser");

            testIds.ForEach(id => Assert.That(returnedMessage.InventoryIds.Contains(id)));
            
        }
    }
}
