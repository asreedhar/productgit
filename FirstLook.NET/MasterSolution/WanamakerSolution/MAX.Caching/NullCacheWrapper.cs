using System;

namespace MAX.Caching
{
    public class NullCacheWrapper : ICacheWrapper
    {
        public bool Add(string key, object value)
        {
            return false;
        }

        public bool Add(string key, object value, DateTimeOffset absoluteExpiration)
        {
            return false;
        }

        public bool Set(string key, object value)
        {
            return false;
        }

        public bool Set(string key, object value, DateTimeOffset absoluteExpiration)
        {
            return false;
        }

        public object Get(string key)
        {
            return null;
        }

        public T Get<T>(string key)
        {
            return default(T);
        }

        public T ReadThroughCache<T>(string key, Func<T> readValue) where T : class
        {
            return null;
        }

        public T ReadThroughCache<T>(string key, Func<T> readValue, DateTimeOffset absoluteExpiration) where T : class
        {
            return null;
        }
    }
}