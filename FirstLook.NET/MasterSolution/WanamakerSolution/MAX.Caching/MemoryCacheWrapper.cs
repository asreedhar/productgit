﻿using System;
using System.Linq;
using System.Runtime.Caching;

namespace MAX.Caching
{
    /// <summary>
    /// A wrapper around System.Runtime.Caching.MemoryCache
    /// </summary>
    public class MemoryCacheWrapper : ICacheWrapper
    {
        private readonly MemoryCache _client;
        private const int DefaultTimeoutSuccess = 60;
        private const int DefaultTimeoutFailure = 5;

        public MemoryCacheWrapper()
        {
            _client = new MemoryCache("max");
        }

        private bool InvalidArgs(params object[] args)
        {
            return args.Any(a => a == null);
        }

        public bool Add(string key, object value)
        {
            if (InvalidArgs(key, value)) return false;
            return _client.Add(key, value, null);
        }

        public bool Add(string key, object value, DateTimeOffset absoluteExpiration)
        {
            if (InvalidArgs(key, value)) return false;
            return _client.Add(key, value, absoluteExpiration);
        }

        public bool Set(string key, object value)
        {
            if (InvalidArgs(key, value)) return false;
            _client.Set(key, value, null);
            return true;
        }

        public bool Set(string key, object value, DateTimeOffset absoluteExpiration)
        {
            if (InvalidArgs(key, value)) return false;
            _client.Set(key, value, absoluteExpiration);
            return true;            
        }

        public object Get(string key)
        {
            if (InvalidArgs(key)) return null;
            return _client.Get(key);
        }

        public T Get<T>(string key)
        {
            if (InvalidArgs(key)) return default(T);
            return (T)_client.Get(key);
        }
        public T ReadThroughCache<T>(string key, Func<T> readValue) where T : class
        {
            var raw = Get(key);
            if (raw == null)
            {
                try
                {
                    raw = readValue();
                }
                catch (Exception ex)
                {
                    raw = ex;
                }

                Set(key, raw, DateTime.Now.AddMinutes(raw is T ? DefaultTimeoutSuccess : DefaultTimeoutFailure));
            }

            var exception = raw as Exception;
            if (exception != null)
            {
                throw exception;
            }

            return (T)raw;
        }

        public T ReadThroughCache<T>(string key, Func<T> readValue, DateTimeOffset absoluteExpiration) where T : class
        {
            var raw = Get(key);
            if (raw == null)
            {
                try
                {
                    raw = readValue();
                }
                catch (Exception ex)
                {
                    raw = ex;
                }

                Set(key, raw, raw is T ? absoluteExpiration : DateTime.Now.AddMinutes(DefaultTimeoutFailure));
            }

            var exception = raw as Exception;
            if (exception != null)
            {
                throw exception;
            }

            return (T)raw;
        }
    }
}
