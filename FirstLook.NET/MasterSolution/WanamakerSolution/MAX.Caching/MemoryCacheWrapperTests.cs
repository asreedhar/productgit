﻿using System;
using System.Threading;
using NUnit.Framework;

// ReSharper disable UnusedMember.Global
namespace MAX.Caching
{
    [TestFixture]
    public class MemoryCacheWrapperTests
    {
        readonly ICacheWrapper _cache = new MemoryCacheWrapper();

        [Test]
        public void GetEmpty()
        {
            string key = Guid.NewGuid().ToString();
            var result = _cache.Get(key);
            Assert.IsNull(result);
        }

        [Test]
        public void Add()
        {
            string key = Guid.NewGuid().ToString();
            string value = Guid.NewGuid().ToString();
            var added = _cache.Add(key, value);
            Assert.IsTrue(added);
        }

        [Test]
        public void AddAndGet()
        {
            string key = Guid.NewGuid().ToString();
            string value = Guid.NewGuid().ToString();

            var added = _cache.Add(key, value);
            Assert.IsTrue(added);

            var result = _cache.Get(key);            
            Assert.AreSame(value, result);            
        }

        [Test]
        public void AddReturnsFalseWhenKeyExists()
        {
            string key = Guid.NewGuid().ToString();
            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();

            var added = _cache.Add(key, value1);
            Assert.IsTrue(added);

            added = _cache.Add(key, value2);
            Assert.IsFalse(added);

            var result = _cache.Get(key);
            Assert.AreEqual(value1, result);
        }

        [Test]
        public void AddWithExpiration()
        {
            string key = Guid.NewGuid().ToString();
            string value = Guid.NewGuid().ToString();

            var added = _cache.Add(key, value, new DateTimeOffset(DateTime.Now.AddSeconds(1)));
            Assert.IsTrue(added);

            var result = _cache.Get(key);
            Assert.AreSame(value, result);            

            Thread.Sleep(2000);

            var result2 = _cache.Get(key);
            Assert.IsNull(result2);            
        }

        [Test]
        public void SetReturnsTrueWhenKeyDoesNotExist()
        {
            string key = Guid.NewGuid().ToString();
            string value1 = Guid.NewGuid().ToString();

            var set = _cache.Set(key, value1);
            Assert.IsTrue(set);

            var result = _cache.Get(key);
            Assert.AreSame(value1, result);
        }

        [Test]
        public void SetReturnsTrueWhenKeyDoesExist()
        {
            string key = Guid.NewGuid().ToString();
            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();

            var set = _cache.Set(key, value1);
            Assert.IsTrue(set);
            var result = _cache.Get(key);
            Assert.AreSame(value1, result);

            set = _cache.Set(key, value2);
            Assert.IsTrue(set);            
            result = _cache.Get(key);
            Assert.AreSame(value2, result);            
        }

        [Test]
        public void SetWithExpiration()
        {
            string key = Guid.NewGuid().ToString();
            string value = Guid.NewGuid().ToString();

            var set = _cache.Set(key, value, new DateTimeOffset(DateTime.Now.AddSeconds(1)));
            Assert.IsTrue(set);

            var result = _cache.Get(key);
            Assert.AreSame(value, result);

            Thread.Sleep(6000);

            var result2 = _cache.Get(key);
            Assert.IsNull(result2);
        }

        [Test]
        public void SetNullValue()
        {
            string key = Guid.NewGuid().ToString();
            string value = null;

            var set = _cache.Set(key, value, new DateTimeOffset(DateTime.Now.AddSeconds(5)));
            Assert.IsFalse(set);
        }

        [Test]
        public void AddNullValue()
        {
            string key = Guid.NewGuid().ToString();
            string value = null;

            var set = _cache.Add(key, value, new DateTimeOffset(DateTime.Now.AddSeconds(5)));
            Assert.IsFalse(set);
        }

    }
}
