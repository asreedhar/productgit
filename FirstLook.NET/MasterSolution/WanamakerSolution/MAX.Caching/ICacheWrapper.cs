﻿using System;

namespace MAX.Caching
{
    public interface ICacheWrapper
    {
        bool Add(string key, object value);
        bool Add(string key, object value, DateTimeOffset absoluteExpiration);
        bool Set(string key, object value);
        bool Set(string key, object value, DateTimeOffset absoluteExpiration);
        object Get(string key);

        T Get<T>(string key);
        T ReadThroughCache<T>(string key, Func<T> readValue) where T : class;
        T ReadThroughCache<T>(string key, Func<T> readValue, DateTimeOffset absoluteExpiration) where T : class;
    }
}