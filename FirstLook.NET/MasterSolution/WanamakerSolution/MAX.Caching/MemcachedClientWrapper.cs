using System;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;

namespace MAX.Caching
{
    /// <summary>
    /// Important! MemcachedClient is a "heavy object", as in creating and initializing the client is quite expensive. 
    /// (It has a socket pool inside, needs to calculate the tables for the consistent hashing, etc.) So, DO NOT create 
    /// a client every time you want to perform an operation.
    /// 
    /// Create the cache as early as possible while your application is initializing, then share the instances amongst as 
    /// many parts/modules as you can.
    /// 
    /// https://github.com/enyim/EnyimMemcached/wiki/MemcachedClient-Usage
    /// </summary>
    public class MemcachedClientWrapper : ICacheWrapper
    {
        private readonly MemcachedClient _client;
        private const int DefaultTimeoutSuccess = 60;
        private const int DefaultTimeoutFailure = 5;

        #region Logging

        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Logging


        public MemcachedClientWrapper()
        {

            Log.Debug("constructing MemcachedClientWrapper (and MemcachedClient)");

            _client = new MemcachedClient(GetMemcachedConfig()); // Web config transforms are killing me. Just the Env from config settings
        }

        private IMemcachedClientConfiguration GetMemcachedConfig()
        {
            // TODO: Distributed settings, eg etcd/etcetera
            var config = new MemcachedClientConfiguration();
            config.SocketPool.ConnectionTimeout = new TimeSpan(0, 0, 0, 2);
            config.SocketPool.DeadTimeout = new TimeSpan(0, 0, 0, 10);
            config.SocketPool.QueueTimeout = new TimeSpan(0, 0, 0, 0, 100);
            config.SocketPool.MaxPoolSize = 20;
            config.SocketPool.MinPoolSize = 10;

            CacheEnvironment env;
            if (!Enum.TryParse(ConfigurationManager.AppSettings["env"], out env))
            {
                Log.Fatal("Memcached is not available without an 'env' AppSetting.");
                throw new ConfigurationErrorsException("Memcached is not available without an 'env' AppSetting.");
            }

            AddEnvironmentMemcachedServers(config, env);

            return config;
        }

        private void AddEnvironmentMemcachedServers(MemcachedClientConfiguration config, CacheEnvironment appSetting)
        {
            try
            {
                switch (appSetting)
                {

                    case CacheEnvironment.Dev:
                    case CacheEnvironment.Alpha:
                        config.AddServer("ord4memcached01.firstlook.biz", 11211);
                        break;
                    case CacheEnvironment.Beta:
                        config.AddServer("ord4memcached02.firstlook.biz", 11211);
                        config.AddServer("ord4memcached03.firstlook.biz", 11211);

                        break;
                    case CacheEnvironment.Prod:
                        config.AddServer("ord4memcached04.firstlook.biz", 11211);
                        config.AddServer("ord4memcached05.firstlook.biz", 11211);
                        config.AddServer("ord4memcached06.firstlook.biz", 11211);
                        break;
                }
            }
            catch(Exception e)
            {
                Log.Error("An error occured while implementing Memcached Client : " + e.Message);
            }
        }

        public bool Add(string key, object value)
        {
            return _client.Store(StoreMode.Add, key, value);
        }

        public bool Add(string key, object value, DateTimeOffset absoluteExpiration)
        {
            return _client.Store(StoreMode.Add, key, value, absoluteExpiration.DateTime);
        }

        public bool Set(string key, object value)
        {
            return _client.Store(StoreMode.Set, key, value);
        }

        public bool Set(string key, object value, DateTimeOffset absoluteExpiration)
        {
            return _client.Store(StoreMode.Set, key, value, absoluteExpiration.DateTime);
        }

        public object Get(string key)
        {
            return _client.Get(key);
        }

        public T Get<T>(string key)
        {
            var obj = _client.Get(key);
            return obj != null ? (T)_client.Get(key) : default(T);
        }

        public T ReadThroughCache<T>(string key, Func<T> readValue) where T : class
        {
            var raw = Get(key);
            if (raw == null)
            {
                try
                {
                    raw = readValue();
                }
                catch (Exception ex)
                {
                    raw = ex;
                }

                Set(key, raw, DateTime.Now.AddMinutes(raw is T ? DefaultTimeoutSuccess : DefaultTimeoutFailure));
            }

            var exception = raw as Exception;
            if (exception != null)
            {
                throw exception;
            }

            return (T)raw;
        }

        public T ReadThroughCache<T>(string key, Func<T> readValue, DateTimeOffset absoluteExpiration) where T : class
        {
            var raw = Get(key);

            if (raw == null)
            {
                Debug.WriteLine("object not located in cache, calling delegate...");

                try
                {
                    raw = readValue();
                }
                catch (Exception ex)
                {
                    raw = ex;
                }

                Set(key, raw, raw is T ? absoluteExpiration : DateTime.Now.AddMinutes(DefaultTimeoutFailure));
            }
            else
            {
                Debug.WriteLine("object retrieved from cache");
            }


            var exception = raw as Exception;
            if (exception != null)
            {
                throw exception;
            }

            return (T)raw;
        }
    }
}