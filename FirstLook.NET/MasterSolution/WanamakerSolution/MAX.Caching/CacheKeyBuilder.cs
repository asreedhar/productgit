﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace MAX.Caching
{
    public interface ICacheKeyBuilder
    {
        string CacheKey(IEnumerable<string> strings);
    }

    public class CacheKeyBuilder : ICacheKeyBuilder
    {
        private static string HashCacheKey(string cacheKey)
        {
            //hash the string as a GUID:
            byte[] hashBytes;
            using (var provider = new MD5CryptoServiceProvider())
            {
                var inputBytes = Encoding.Default.GetBytes(cacheKey);
                hashBytes = provider.ComputeHash(inputBytes);
            }
            return new Guid(hashBytes).ToString();
        }

        public string CacheKey(IEnumerable<string> strings)
        {
            string combined = string.Join(":", strings);
            return HashCacheKey(combined);
        }

    }
}
