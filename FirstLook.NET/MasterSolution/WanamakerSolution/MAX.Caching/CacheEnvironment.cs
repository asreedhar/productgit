﻿namespace MAX.Caching
{
    public enum CacheEnvironment
    {
        Dev,
        Alpha,
        Beta,
        Prod
    }
}