﻿using System.Linq;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;

namespace DemoMerchandisingTests.DemoBusinessUnitTests
{
    [TestFixture]
    public class DemoBusinessUnitRepositoryTests
    {
        const int DemoBuPadding = 10000000;

        [Test]
        public void Test_Create_Returns_DemoBusinessUnit()
        {
            var buRepo = new DemoBusinessUnitRepository();
            var demoBu = buRepo.Create("Automated Tests Demo");

            var compareBu = GetCompareUnit();

            Assert.IsInstanceOf<IDashboardBusinessUnit>(demoBu);
            Assert.IsTrue(demoBu.BusinessUnitId > compareBu.BusinessUnitId);
            Assert.IsTrue(demoBu.Code.StartsWith(compareBu.Code));
            Assert.IsTrue(demoBu.Name.Equals(compareBu.Name));
            Assert.IsTrue(demoBu.Type.Equals(compareBu.Type));

            buRepo.Delete(demoBu.BusinessUnitId);
        }

        [Test]
        public void Test_Can_Delete_DemoBusinessUnit()
        {
            var buRepo = new DemoBusinessUnitRepository();
            var demoBu = buRepo.Create("Automated Tests Demo");
            var fetchBu = buRepo.Fetch(demoBu.BusinessUnitId);

            Assert.AreEqual(demoBu.BusinessUnitId, fetchBu.BusinessUnitId);
            Assert.AreEqual(demoBu.Code, fetchBu.Code);

            buRepo.Delete(demoBu.BusinessUnitId);

            fetchBu = buRepo.Fetch(demoBu.BusinessUnitId);

            Assert.IsNull(fetchBu);
        }

        [Test]
        public void Test_Can_Fetch_DemoBusinessUnit()
        {
            var buRepo = new DemoBusinessUnitRepository();
            var demoBu = buRepo.Create("Automated Tests Demo");
            var fetchBu = buRepo.Fetch(demoBu.BusinessUnitId);

            Assert.AreEqual(demoBu.BusinessUnitId, fetchBu.BusinessUnitId);
            buRepo.Delete(demoBu.BusinessUnitId);
        }

        [Test]
        public void Test_Can_Create_DemoGroup()
        {
            var buRepo = new DemoBusinessUnitRepository();
            // 3 is int for BusinessUnitType.DealerGroup
            var groupBu = buRepo.Create("Automated Group Tests Demo", 3);

            Assert.AreEqual(groupBu.Type, 3);

            buRepo.Delete(groupBu.BusinessUnitId);
        }

        [Test]
        public void Test_Can_Add_And_Remove_DemoBusinessUnits_To_Group()
        {
            var buRepo = new DemoBusinessUnitRepository();

            // Setup
            /* 3 is int for BusinessUnitType.DealerGroup */
            var groupBu = buRepo.Create("Automated Group Tests Demo", 3);
            var bu1 = buRepo.Create("Auto GrBu 1");
            var bu2 = buRepo.Create("Auto GrBu 2");
            
            Assert.AreEqual(groupBu.Type, 3);
            Assert.AreEqual(bu1.Type, 4);
            Assert.AreEqual(bu2.Type, 4);

            buRepo.AddDashboardBusinessUnitToGroup(bu1.BusinessUnitId, groupBu.BusinessUnitId);
            buRepo.AddDashboardBusinessUnitToGroup(bu2.BusinessUnitId, groupBu.BusinessUnitId);

            var groupUnits = buRepo.FetchGroupMembersByGroupId(groupBu.BusinessUnitId);

            // Only two units are in the Group
            var dashboardBusinessUnits = groupUnits as DashboardBusinessUnit[] ?? groupUnits.ToArray();
            Assert.IsTrue(dashboardBusinessUnits.Count().Equals(2));

            // Each unit we just created is in the group
            Assert.IsNotNull(dashboardBusinessUnits.First(bu => bu.BusinessUnitId.Equals(bu1.BusinessUnitId)));
            Assert.IsNotNull(dashboardBusinessUnits.First(bu => bu.BusinessUnitId.Equals(bu2.BusinessUnitId)));


            // Clean up
            buRepo.RemoveDashboardBusinessUnitFromGroup(bu1.BusinessUnitId, groupBu.BusinessUnitId);
            buRepo.RemoveDashboardBusinessUnitFromGroup(bu2.BusinessUnitId, groupBu.BusinessUnitId);

            buRepo.Delete(groupBu.BusinessUnitId);
            buRepo.Delete(bu1.BusinessUnitId);
            buRepo.Delete(bu2.BusinessUnitId);

        }

        [Test]
        public void Test_Can_Fetch_Group_From_BusinessUnitId()
        {
            var buRepo = new DemoBusinessUnitRepository();

            // Setup
            /* 3 is int for BusinessUnitType.DealerGroup */
            var groupBu = buRepo.Create("Automated Group Tests Demo", 3);
            var bu1 = buRepo.Create("Auto GrBu 1");
            var bu2 = buRepo.Create("Auto GrBu 2");

            Assert.AreEqual(groupBu.Type, 3);
            Assert.AreEqual(bu1.Type, 4);
            Assert.AreEqual(bu2.Type, 4);

            buRepo.AddDashboardBusinessUnitToGroup(bu1.BusinessUnitId, groupBu.BusinessUnitId);
            buRepo.AddDashboardBusinessUnitToGroup(bu2.BusinessUnitId, groupBu.BusinessUnitId);

            var groupUnit = buRepo.FetchGroupByDealerId(bu1.BusinessUnitId);
            var groupUnits = buRepo.FetchGroupMembersByGroupId(groupUnit.BusinessUnitId);

            // Only two units are in the Group
            var dashboardBusinessUnits = groupUnits as DashboardBusinessUnit[] ?? groupUnits.ToArray();
            Assert.IsTrue(dashboardBusinessUnits.Count().Equals(2));

            // Each unit we just created is in the group
            Assert.IsNotNull(dashboardBusinessUnits.First(bu => bu.BusinessUnitId.Equals(bu1.BusinessUnitId)));
            Assert.IsNotNull(dashboardBusinessUnits.First(bu => bu.BusinessUnitId.Equals(bu2.BusinessUnitId)));


            // Clean up
            buRepo.RemoveDashboardBusinessUnitFromGroup(bu1.BusinessUnitId, groupBu.BusinessUnitId);
            buRepo.RemoveDashboardBusinessUnitFromGroup(bu2.BusinessUnitId, groupBu.BusinessUnitId);

            buRepo.Delete(groupBu.BusinessUnitId);
            buRepo.Delete(bu1.BusinessUnitId);
            buRepo.Delete(bu2.BusinessUnitId);
        }

        [Test]
        public void Test_Can_Fetch_Group_And_Members()
        {
            var buRepo = new DemoBusinessUnitRepository();

            // Setup
            /* 3 is int for BusinessUnitType.DealerGroup */
            var groupBu = buRepo.Create("Automated Group Tests Demo", 3);
            var bu1 = buRepo.Create("Auto GrBu 1");
            var bu2 = buRepo.Create("Auto GrBu 2");

            Assert.AreEqual(groupBu.Type, 3);
            Assert.AreEqual(bu1.Type, 4);
            Assert.AreEqual(bu2.Type, 4);

            buRepo.AddDashboardBusinessUnitToGroup(bu1.BusinessUnitId, groupBu.BusinessUnitId);
            buRepo.AddDashboardBusinessUnitToGroup(bu2.BusinessUnitId, groupBu.BusinessUnitId);

            var groupAndUnits = buRepo.FetchGroupAndMembers(bu1.BusinessUnitId);

            // Only three units are in the Group
            var dashboardBusinessUnits = groupAndUnits as DashboardBusinessUnit[] ?? groupAndUnits.ToArray();
            Assert.IsTrue(dashboardBusinessUnits.Count().Equals(3));

            // Each unit we just created is in the group
            Assert.IsNotNull(dashboardBusinessUnits.First(bu => bu.BusinessUnitId.Equals(bu1.BusinessUnitId)));
            Assert.IsNotNull(dashboardBusinessUnits.First(bu => bu.BusinessUnitId.Equals(bu2.BusinessUnitId)));
            
            // There is only one group type
            Assert.AreEqual(dashboardBusinessUnits.Count(bu => bu.Type == 3), 1);

            // There are exactly two dealer type
            Assert.AreEqual(dashboardBusinessUnits.Count(bu => bu.Type == 4), 2);

            // Clean up
            buRepo.RemoveDashboardBusinessUnitFromGroup(bu1.BusinessUnitId, groupBu.BusinessUnitId);
            buRepo.RemoveDashboardBusinessUnitFromGroup(bu2.BusinessUnitId, groupBu.BusinessUnitId);

            buRepo.Delete(groupBu.BusinessUnitId);
            buRepo.Delete(bu1.BusinessUnitId);
            buRepo.Delete(bu2.BusinessUnitId);
        }


        [Test]
        public void Test_IsDemoUnit_Pass_Fail()
        {
            Assert.IsTrue(DemoBusinessUnitRepository.IsDemoUnit(10000000));
            Assert.IsTrue(DemoBusinessUnitRepository.IsDemoUnit(20000000));
            Assert.IsFalse(DemoBusinessUnitRepository.IsDemoUnit(1));
            Assert.IsFalse(DemoBusinessUnitRepository.IsDemoUnit(9999999));
        }

        private static DashboardBusinessUnit GetCompareUnit()
        {
            var compareBu = new DashboardBusinessUnit
            {
                BusinessUnitId = DemoBuPadding,
                Code = "DMAUTOMATE",
                Name = "Automated Tests Demo",
                Type = 4
            };
            return compareBu;
        }
    }
}
