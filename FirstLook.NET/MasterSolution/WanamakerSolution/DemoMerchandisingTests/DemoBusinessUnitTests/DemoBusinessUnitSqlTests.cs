﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DemoMerchandisingTests.Properties;
using NUnit.Framework;
using System.Data.SqlClient;
using FirstLook.DomainModel.Oltp;

namespace DemoMerchandisingTests.DemoBusinessUnitTests
{
    [TestFixture]
    public class DemoBusinessUnitSqlTests
    {
        const string DemoBuInsert = "demo.BusinessUnit#Insert";
        const string DemoBuDeactivate = "demo.BusinessUnit#Deactivate";
        const string DemoBuDelete = "demo.BusinessUnit#Delete";
        const string DemoBuFetch = "demo.BusinessUnit#Fetch";
        const string DemoBuFetchgroup = "demo.BusinessUnit#FetchGroupMembers";
        const string DemoBuFetchgroupFromBu = "demo.BusinessUnit#FetchGroupAndMembersFromBusinessUnit";

        const string DemoBuGroupInsert = "demo.BusinessUnitRelationship#Insert";
        const string DemoBuGroupDelete = "demo.BusinessUnitRelationship#Delete";

        [TestCase("Test Script Demo Bu", BusinessUnitType.Dealer)]
        public void Can_Insert_Demo_BusinessUnit(string testBuName, BusinessUnitType buType)
        {
            using (var conn = new SqlConnection( Settings.Default.Merchandising ))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DemoBuInsert, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitName", testBuName);
                    cmd.Parameters.AddWithValue("@BusinessUnitTypeId", (int)buType);

                    using (var reader = cmd.ExecuteReader())
                    {
                        Assert.That(reader.HasRows);

                        while (reader.Read())
                        {
                            Assert.That(
                                reader["BusinessUnitId"] != DBNull.Value
                            );
                            Assert.That(Convert.ToInt32(reader["BusinessUnitId"]) > 0);

                            // Business Unit Name = passed in value
                            Assert.That(Convert.ToString(reader["BusinessUnit"]).Equals(testBuName));
                            // Business Unit Type matches passed in value
                            Assert.That(Convert.ToInt32(reader["BusinessUnitTypeId"]).Equals((int)buType));

                            // BU code starts with "DM" ie demo
                            Assert.That(Convert.ToString(reader["BusinessUnitCode"]).StartsWith("DM"));

                            // Is active by default
                            Assert.That(Convert.ToBoolean(reader["Active"]).Equals(true));

                            var buId = Convert.ToInt32(reader["BusinessUnitId"]);
                            // Clean up demo bu
                            using (var delConn = new SqlConnection(Settings.Default.Merchandising))
                            {
                                delConn.Open();
                                using (var delcmd = new SqlCommand(DemoBuDelete, delConn))
                                {
                                    delcmd.CommandType = CommandType.StoredProcedure;
                                    delcmd.Parameters.AddWithValue("@BusinessUnitId", buId);
                                    delcmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                }
            }
        }

        [TestCase("Test Script Demo Bu", BusinessUnitType.Dealer)]
        [TestCase("Demo Bu", BusinessUnitType.Dealer)]
        [TestCase("Test Script Demo Bu", BusinessUnitType.Dealer)]
        [TestCase("Test Scripts Demo Bu Up to 40 characters", BusinessUnitType.Dealer)]
        public void Can_Fetch_Demo_BusinessUnit(string testBuName, BusinessUnitType buType)
        {
            var testBu = InsertOneBu(testBuName, buType);

            // Test the fetch:
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DemoBuFetch, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", testBu.Id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        Assert.That(reader.HasRows);
                        while (reader.Read())
                        {
                            Assert.IsTrue(testBu.Id == Convert.ToInt32(reader["BusinessUnitId"]));
                            Assert.IsTrue(testBu.Name == Convert.ToString(reader["BusinessUnit"]));
                            Assert.IsTrue(testBu.BusinessUnitCode == Convert.ToString(reader["BusinessUnitCode"]));
                            Assert.IsTrue(testBu.BusinessUnitType == (BusinessUnitType)Convert.ToInt32(reader["BusinessUnitTypeId"]));
                        }
                    }
                }
            }

            DeleteOneBu(testBu);
        }


        [TestCase("Deactivate This Demo Bu", BusinessUnitType.Dealer)]
        public void Can_Deactivate_Demo_BUs(string testBuName, BusinessUnitType buType)
        {
            var testBu = InsertOneBu(testBuName, buType);

            Assert.IsNotNull(testBu);
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DemoBuDeactivate, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", testBu.Id);
                    cmd.ExecuteNonQuery();
                }
            }

            var compareBu = ReFetchBu(testBu);

            Assert.IsTrue(testBu.Id == compareBu.Id);
            Assert.IsFalse(compareBu.Active);

            DeleteOneBu(testBu);
        }


        [TestCase("Delete This Demo Bu", BusinessUnitType.Dealer)]
        public void Can_Delete_Demo_BUs(string testBuName, BusinessUnitType buType)
        {
            var testBu = InsertOneBu(testBuName, buType);

            Assert.IsNotNull(testBu);

            DeleteOneBu(testBu);

            var fetchDeletedBu = ReFetchBu(testBu);
            Assert.IsNull(fetchDeletedBu);
        }

        [TestCase("Create a Demo Group", BusinessUnitType.DealerGroup)]
        public void Can_Create_A_Demo_Group(string testGroupName, BusinessUnitType buType)
        {
            var testBu = InsertOneBu(testGroupName, buType);
            Assert.IsNotNull(testBu);
            Assert.IsInstanceOf<BusinessUnitType>(testBu.BusinessUnitType);
            Assert.AreEqual(BusinessUnitType.DealerGroup, testBu.BusinessUnitType);

            DeleteOneBu(testBu);
        }


        [TestCase("Fetch a Demo Group by Group Id", "DemoGroupBu1", "DemoGroupBu2", "DemoGroupBu3")]
        public void Demo_Group_Fetch_By_GroupId(string testGroupName, string testDealer1, string testDealer2, string testDealer3)
        {
            var groupBu = InsertOneBu(testGroupName, BusinessUnitType.DealerGroup);

            var bus = new List<BusinessUnit>
            {
                InsertOneBu(testDealer1, BusinessUnitType.Dealer),
                InsertOneBu(testDealer2, BusinessUnitType.Dealer),
                InsertOneBu(testDealer3, BusinessUnitType.Dealer)
            };

            // Create the relationship
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                bus.ForEach(bu =>
                {
                    using (var cmd = new SqlCommand(DemoBuGroupInsert, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@GroupId", groupBu.Id);
                        cmd.Parameters.AddWithValue("@BusinessUnitId", bu.Id);
                        cmd.ExecuteNonQuery();
                    }
                });
            }

            // Fetch all by group
            var groupBus = new List<BusinessUnit>();
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DemoBuFetchgroup, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupId", groupBu.Id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        Assert.That(reader.HasRows);
                        while (reader.Read())
                        {
                            groupBus.Add(BusinessUnitFromReader(reader));
                        }
                    }
                }
            }

            // Verify that the group fetch returns all our created Business Units
            bus.ForEach(bu => Assert.That(groupBus.Find(gbu => bu.Id == gbu.Id) != null));

            // Clean up relationship table
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                bus.ForEach(bu =>
                {
                    using (var cmd = new SqlCommand(DemoBuGroupDelete, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@GroupId", groupBu.Id);
                        cmd.Parameters.AddWithValue("@BusinessUnitId", bu.Id);
                        cmd.ExecuteNonQuery();
                    }
                });
            }

            bus.ForEach(DeleteOneBu);
            DeleteOneBu(groupBu);
        }

        [TestCase("Fetch a Demo Group By BuId", "DemoGroupBu1", "DemoGroupBu2", "DemoGroupBu3")]
        public void Demo_Group_Fetch_By_BusinessUnitId(string testGroupName, string testDealer1, string testDealer2, string testDealer3)
        {
            var groupBu = InsertOneBu(testGroupName, BusinessUnitType.DealerGroup);

            var bus = new List<BusinessUnit>
            {
                InsertOneBu(testDealer1, BusinessUnitType.Dealer),
                InsertOneBu(testDealer2, BusinessUnitType.Dealer),
                InsertOneBu(testDealer3, BusinessUnitType.Dealer)
            };

            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                foreach(var bu in bus)
                {
                    using (var cmd = new SqlCommand(DemoBuGroupInsert, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@GroupId", groupBu.Id);
                        cmd.Parameters.AddWithValue("@BusinessUnitId", bu.Id);
                        cmd.ExecuteNonQuery();
                    }
                }
            }

            var groupBus = new List<BusinessUnit>();
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DemoBuFetchgroupFromBu, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", bus.First().Id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        Assert.That(reader.HasRows);
                        while (reader.Read())
                        {
                            groupBus.Add(BusinessUnitFromReader(reader));
                        }
                    }
                }
            }

            // Verify that the group fetch returns all our created Business Units
            bus.ForEach(bu => Assert.That(groupBus.Find(gbu => bu.Id == gbu.Id) != null));

            // Clean up relationship table
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                bus.ForEach(bu =>
                {
                    using (var cmd = new SqlCommand(DemoBuGroupDelete, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@GroupId", groupBu.Id);
                        cmd.Parameters.AddWithValue("@BusinessUnitId", bu.Id);
                        cmd.ExecuteNonQuery();
                    }
                });
            }

            bus.ForEach(DeleteOneBu);
            DeleteOneBu(groupBu);
        }

        #region PrivateTestMethods
        private static BusinessUnit InsertOneBu(string testBuName, BusinessUnitType buType)
        {
            BusinessUnit testBu = null;

            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DemoBuInsert, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitName", testBuName);
                    cmd.Parameters.AddWithValue("@BusinessUnitTypeId", (int)buType);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            testBu = BusinessUnitFromReader(reader);
                        }
                        else
                        {
                            Console.WriteLine();
                            var errorString = new StringBuilder();
                            errorString.Append("\n").Append(cmd.CommandText).Append(" ");
                            foreach (SqlParameter sparm in cmd.Parameters)
                            {
                                errorString.Append(sparm.DbType.ToString()).Append(" ");
                                errorString.Append(sparm.ParameterName).Append(" ");
                                errorString.Append(sparm.Value).Append(" ");
                            }
                            errorString.Append("\n");
                            Console.WriteLine(errorString.ToString());
                        }
                    }
                }
            }

            return testBu;
        }

        private void DeleteOneBu(BusinessUnit testBu)
        {
            // Clean up demo bu
            using (var delConn = new SqlConnection(Settings.Default.Merchandising))
            {
                delConn.Open();
                using (var delcmd = new SqlCommand(DemoBuDelete, delConn))
                {
                    delcmd.CommandType = CommandType.StoredProcedure;
                    delcmd.Parameters.AddWithValue("@BusinessUnitId", testBu.Id);
                    delcmd.ExecuteNonQuery();
                }
            }
        }

        private static BusinessUnit ReFetchBu(BusinessUnit testBu)
        {
            BusinessUnit fetchBu = null;

            // Test the fetch:
            using (var conn = new SqlConnection(Settings.Default.Merchandising))
            {
                conn.Open();
                using (var cmd = new SqlCommand(DemoBuFetch, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", testBu.Id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                                fetchBu = BusinessUnitFromReader(reader);
                        }
                    }
                }
            }

            return fetchBu;
        }

        private static BusinessUnit BusinessUnitFromReader(SqlDataReader reader)
        {
            var bu = new BusinessUnit
            {
                BusinessUnitCode = Convert.ToString(reader["BusinessUnitCode"]),
                Id = Convert.ToInt32(reader["BusinessUnitId"]),
                Name = Convert.ToString(reader["BusinessUnit"]),
                BusinessUnitType = (BusinessUnitType) Convert.ToInt32(reader["BusinessUnitTypeId"])
            };

            return bu;
        }

        #endregion PrivateTestMethods

    }
}
