﻿using System.Collections.Generic;
using System.Web.Script.Serialization;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;
using Wanamaker.WebApp.Areas.Dashboard.Controllers;
using Bucket = FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels.Bucket;


namespace Wannamaker.WebApp.Tests.Dashboard
{
    [TestFixture]
    public class BucketsTests
    {
        private Buckets _buckets;

        [SetUp]
        public void TestSetup()
        {
            _buckets = new Buckets();
            _buckets.ActionBuckets = new List<Bucket>();
            _buckets.ActivityBuckets = new List<Bucket>();
            _buckets.SelectedFilterCount = 200;
            _buckets.LowActivityCount = 5;
            _buckets.NotOnlineCount = 8;
        }

        [Test]
        public void Low_activity_percent_should_be_low_activity_count_divided_by_selected_filter_count()
        {
            const int result = 2;
            Assert.IsTrue(_buckets.LowActivityPercent.Equals(result));
        }

        [Test]
        public void Low_activity_percent_should_be_zero_for_zero_selected_filter_count()
        {
            _buckets.SelectedFilterCount = 0;
            Assert.IsTrue(_buckets.LowActivityPercent.Equals(0));
        }

       

      

        [Test]
        public void Not_online_percent_should_be_zero_for_zero_selected_filter_count()
        {
            _buckets.SelectedFilterCount = 0;
            Assert.IsTrue(_buckets.NotOnlinePercent.Equals(0));
        }

        

        [Test]
        public void Auto_approve_status_Off_should_not_show_ad_review_bucket()
        {
            int businessUnitId = 1234;
              Mock<IDashboardRepository> dashboardRepository = new Mock<IDashboardRepository>();
            var autoApproveStatus = AutoApproveStatus.Off;
            var dashboardManager = new DashboardManager(dashboardRepository.Object, null, null, null, null);
            var adReviewApplicableResultUsed = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Used, autoApproveStatus);
            var adReviewApplicableResultNew = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.New, autoApproveStatus);
            var adReviewApplicableResultBoth = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Both, autoApproveStatus);

            Assert.IsTrue(adReviewApplicableResultUsed == false);
            Assert.IsTrue(adReviewApplicableResultNew == false);
            Assert.IsTrue(adReviewApplicableResultBoth == false);
        }

        [Test]
        public void Auto_approve_status_OnForAll_should_show_ad_review_bucket()
        {
            int businessUnitId = 1234;
            Mock<IDashboardRepository> dashboardRepository = new Mock<IDashboardRepository>();
            var autoApproveStatus = AutoApproveStatus.OnForAll;
            var dashboardManager = new DashboardManager(dashboardRepository.Object, null, null, null, null);
            var adReviewApplicableResultUsed = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Used, autoApproveStatus);
            var adReviewApplicableResultNew = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.New, autoApproveStatus);
            var adReviewApplicableResultBoth = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Both, autoApproveStatus);

            Assert.IsTrue(adReviewApplicableResultUsed);
            Assert.IsTrue(adReviewApplicableResultNew);
            Assert.IsTrue(adReviewApplicableResultBoth);
        }

        [Test]
        public void Auto_approve_status_OnForUsed_should_show_ad_review_bucket_for_used_and_both()
        {
            int businessUnitId = 1234;
            Mock<IDashboardRepository> dashboardRepository = new Mock<IDashboardRepository>();
            var autoApproveStatus = AutoApproveStatus.OnForUsed;

            var dashboardManager = new DashboardManager(dashboardRepository.Object, null, null, null, null);
            var adReviewApplicableResultUsed = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Used, autoApproveStatus);
            var adReviewApplicableResultNew = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.New, autoApproveStatus);
            var adReviewApplicableResultBoth = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Both, autoApproveStatus);

            Assert.IsTrue(adReviewApplicableResultUsed);
            Assert.IsTrue(adReviewApplicableResultNew == false);
            Assert.IsTrue(adReviewApplicableResultBoth);
        }

        [Test]
        public void Auto_approve_status_OnForNew_should_show_ad_review_bucket_for_new_and_both()
        {
            int businessUnitId = 1234;
            Mock<IDashboardRepository> dashboardRepository = new Mock<IDashboardRepository>();

            var autoApproveStatus = AutoApproveStatus.OnForNew;
            var dashboardManager = new DashboardManager(dashboardRepository.Object, null, null, null, null);
            var adReviewApplicableResultUsed = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Used, autoApproveStatus);
            var adReviewApplicableResultNew = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.New, autoApproveStatus);
            var adReviewApplicableResultBoth = dashboardManager.IsAdReviewApplicable(businessUnitId, VehicleType.Both, autoApproveStatus);

            Assert.IsTrue(adReviewApplicableResultUsed == false);
            Assert.IsTrue(adReviewApplicableResultNew);
            Assert.IsTrue(adReviewApplicableResultBoth);
        }

        [Test]
        public void Can_sort_buckets_by_bucket_ids()
        {
            var oBuckets = new List<FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket>
                                                                                               {
                                                                                                   new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket {BucketId = WorkflowType.CreateInitialAd },
                                                                                                   new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket {BucketId = WorkflowType.AdReviewNeeded },
                                                                                                   new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket {BucketId = WorkflowType.NoPrice}

                                                                                               };
            var sortById = new List<WorkflowType>() { WorkflowType.AdReviewNeeded, WorkflowType.NoPrice, WorkflowType.CreateInitialAd };

            var sortedList = DashboardManager.SortBucketsByIds(oBuckets, sortById);

            Assert.That(sortedList.Count == 3);

            Assert.That(sortedList[0].BucketId == WorkflowType.AdReviewNeeded);

            Assert.That(sortedList[1].BucketId == WorkflowType.NoPrice);

            Assert.That(sortedList[2].BucketId == WorkflowType.CreateInitialAd);
        }

        [Test]
        public void Can_sort_buckets_by_bucket_ids_less_than_list_to_sort()
        {
            var oBuckets = new List<FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket>
                                                                                               {
                                                                                                   new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket {BucketId = WorkflowType.CreateInitialAd },
                                                                                                   new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket {BucketId = WorkflowType.AdReviewNeeded },
                                                                                                   new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket {BucketId = WorkflowType.NoPrice}

                                                                                               };
            var sortById = new List<WorkflowType>() { WorkflowType.AdReviewNeeded,  WorkflowType.CreateInitialAd };

            var sortedList = DashboardManager.SortBucketsByIds(oBuckets, sortById);

            Assert.That(sortedList.Count == 3);

            Assert.That(sortedList[0].BucketId == WorkflowType.NoPrice);

            Assert.That(sortedList[1].BucketId == WorkflowType.AdReviewNeeded);

            Assert.That(sortedList[2].BucketId == WorkflowType.CreateInitialAd);
        }

        [Test]
        public void BucketsDataShouldReturnErrorMessageJsonForException()
        {
            var dashboardRepo = new Mock<IDashboardRepository>();
            var perfRepo = new Mock<ISitePerformanceRepository>();
			var websiteProviderMetricsMock = new Mock<IWebsiteProviderMetricsModel>();
            var ttmMock = new Mock<ITimeToMarketRepository>();
            var dealerServices = new Mock<IDashboardBusinessUnitRepository>();

            DashboardManager manager = new DashboardManager(dashboardRepo.Object, perfRepo.Object, null, ttmMock.Object, websiteProviderMetricsMock.Object);
            var controller = new HomeController(manager, dealerServices.Object);
            
            var result = controller.BucketsData(10139, "Use");
            var serializer = new JavaScriptSerializer(); 
            
            var output = serializer.Serialize(result.Data);  
            Assert.IsTrue(output.StartsWith("{\"errorMessage\""));
        }

        [Test]
        public void BucketsDataShouldReturnValidJson()
        {
            var dashboardRepo = new Mock<IDashboardRepository>();
            var perfRepo = new Mock<ISitePerformanceRepository>();

            var status = new OnlineStatus {AgeInDays = 2, NotOnlineCount = 30, TotalInventoryCount = 300};
            dashboardRepo.Setup(c => c.GetNotOnlineCount(It.IsAny<int>(), It.IsAny<VehicleType>())).Returns(status);


            var buckets = new List<FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket>()
                              {
                                  new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket
                                      {BucketCount = 20, BucketDescription =  "Not Online", BucketId = WorkflowType.CreateInitialAd, NavigationUrl = string.Empty},
                                      new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket
                                      {BucketCount = 20, BucketDescription =  "No CarFax", BucketId = WorkflowType.NoCarfax , NavigationUrl = string.Empty},
                                      new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket
                                      {BucketCount = 20, BucketDescription =  "LowActivity", BucketId = WorkflowType.LowActivity, NavigationUrl = string.Empty},
                                      new FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket
                                      {BucketCount = 20, BucketDescription =  "Offline", BucketId = WorkflowType.Offline, NavigationUrl = string.Empty},


                              };

            dashboardRepo.Setup(c => c.GetBucketCounts(It.IsAny<int>(), It.IsAny<VehicleType>())).Returns(buckets);

            var ttmMock = new Mock<ITimeToMarketRepository>();
            var websiteProviderMetricsMock = new Mock<IWebsiteProviderMetricsModel>();
            var dealerServices = new Mock<IDashboardBusinessUnitRepository>();

            DashboardManager manager = new DashboardManager(dashboardRepo.Object, perfRepo.Object, null, ttmMock.Object, websiteProviderMetricsMock.Object);
            var controller = new HomeController(manager, dealerServices.Object);

            var result = controller.BucketsData(10139, "Used");
            var serializer = new JavaScriptSerializer();

            var output = serializer.Serialize(result.Data);
            Assert.IsTrue(output.StartsWith("{\"ActionBuckets\""));
        }

    }
}
