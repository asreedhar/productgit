﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.Entities;
using Moq;
using NUnit.Framework;
using Wanamaker.WebApp.Areas.Dashboard.Controllers;

namespace Wannamaker.WebApp.Tests.Dashboard
{
    [TestFixture]
    public class SitePerformanceTests
    {
        private Mock<IDashboardRepository> _dashboardRepository;
        private Mock<ISitePerformanceRepository> _sitePerformanceRepository;
        private HomeController _controller;
        private const int BusinessUnitId = 1234;
        private Mock<IClock> _clock;
        private DateTime _startDate;
        private DateTime _endDate;
        private Mock<ITimeToMarketRepository> _timeToMarketRepository;
        private Mock<IWebsiteProviderMetricsModel> __websiteProviderMetricsModel;
        private Mock<IDashboardBusinessUnitRepository> _dealerServices;


        [SetUp]
        public void Setup()
        {
            var autoTrader = new Site {Id = 1, Name = "AutoTrader"};
            var cars = new Site{Id = 2, Name = "Cars.com"};
            _clock = new Mock<IClock>();
            _dashboardRepository = new Mock<IDashboardRepository>();
            _sitePerformanceRepository = new Mock<ISitePerformanceRepository>();
            _timeToMarketRepository = new Mock<ITimeToMarketRepository>();
            __websiteProviderMetricsModel = new Mock<IWebsiteProviderMetricsModel>();
            _dealerServices = new Mock<IDashboardBusinessUnitRepository>();

            _clock.SetupGet(x => x.CurrentTime).Returns(DateTime.Now);

            DashboardManager manager = new DashboardManager(_dashboardRepository.Object, _sitePerformanceRepository.Object, null, _timeToMarketRepository.Object, __websiteProviderMetricsModel.Object);
            _controller = new HomeController(manager, _dealerServices.Object);
            _endDate = _controller.GetEndDate(_clock.Object);
            _startDate = _controller.GetStartOfMonth(_endDate);

            var newSitePerformanceTrends = new Dictionary<string, List<SitePerformance>>() {
                {
                    "AutoTrader.com", 
                    new List<SitePerformance>() {
                        new SitePerformance{Site = autoTrader, AdPrintedCount = 30, DetailPageViewCount = 90, EmailLeadsCount = 50, MapsViewedCount = 88, PhoneLeadsCount = 87, SearchPageViewCount = 190},
                        new SitePerformance{Site = autoTrader, AdPrintedCount = 10, DetailPageViewCount = 70, EmailLeadsCount = 20, MapsViewedCount = 45, PhoneLeadsCount = 60, SearchPageViewCount = 120}
                    }
                },
                {
                    "Cars.com",
                    new List<SitePerformance>() {
                        new SitePerformance{Site = cars, AdPrintedCount = 20, DetailPageViewCount = 70, EmailLeadsCount = 20, MapsViewedCount = 65, PhoneLeadsCount = 60, SearchPageViewCount = 110},
                        new SitePerformance{Site = cars, AdPrintedCount = 20, DetailPageViewCount = 56, EmailLeadsCount = 65, MapsViewedCount = 98, PhoneLeadsCount = 45, SearchPageViewCount = 200}
                    }
                }
            };

            var usedSitePerformanceTrends = new Dictionary<string, List<SitePerformance>>() {
                {
                    "AutoTrader.com",
                    new List<SitePerformance>() {
                        new SitePerformance{Site = autoTrader, AdPrintedCount = 10, DetailPageViewCount = 70, EmailLeadsCount = 20, MapsViewedCount = 30, PhoneLeadsCount = 56, SearchPageViewCount = 150}
                    }
                },
                {
                    "Cars.com",
                    new List<SitePerformance>() {
                        new SitePerformance{Site = cars, AdPrintedCount = 20, DetailPageViewCount = 80, EmailLeadsCount = 20, MapsViewedCount = 45, PhoneLeadsCount = 40, SearchPageViewCount = 110}
                    }
                }
            };

            var bothSitePerformanceTrends = new Dictionary<string, List<SitePerformance>>() {
                {
                    "AutoTrader.com", 
                    new List<SitePerformance>() {
                        new SitePerformance{Site = autoTrader, AdPrintedCount = 30, DetailPageViewCount = 90, EmailLeadsCount = 50, MapsViewedCount = 88, PhoneLeadsCount = 87, SearchPageViewCount = 190},
                        new SitePerformance{Site = autoTrader, AdPrintedCount = 10, DetailPageViewCount = 70, EmailLeadsCount = 20, MapsViewedCount = 45, PhoneLeadsCount = 60, SearchPageViewCount = 120},
                        new SitePerformance{Site = autoTrader, AdPrintedCount = 10, DetailPageViewCount = 70, EmailLeadsCount = 20, MapsViewedCount = 30, PhoneLeadsCount = 56, SearchPageViewCount = 150}
                    }
                },
                {
                    "Cars.com",
                    new List<SitePerformance>() {
                        new SitePerformance{Site = cars, AdPrintedCount = 20, DetailPageViewCount = 70, EmailLeadsCount = 20, MapsViewedCount = 65, PhoneLeadsCount = 60, SearchPageViewCount = 110},
                        new SitePerformance{Site = cars, AdPrintedCount = 20, DetailPageViewCount = 56, EmailLeadsCount = 65, MapsViewedCount = 98, PhoneLeadsCount = 45, SearchPageViewCount = 200},
                        new SitePerformance{Site = cars, AdPrintedCount = 20, DetailPageViewCount = 80, EmailLeadsCount = 20, MapsViewedCount = 45, PhoneLeadsCount = 40, SearchPageViewCount = 110}
                    }
                }
            };
                        

          
            _sitePerformanceRepository.Setup(
                repo => repo.GetMonthlySiteTrends(BusinessUnitId, VehicleType.New, new FirstLook.Common.Core.DateRange(_startDate, _endDate)))
                    .Returns(newSitePerformanceTrends);
           

            _sitePerformanceRepository.Setup(
                repo => repo.GetMonthlySiteTrends(BusinessUnitId, VehicleType.Used, new FirstLook.Common.Core.DateRange(_startDate, _endDate)))
                    .Returns(usedSitePerformanceTrends);

            _sitePerformanceRepository.Setup(
                repo => repo.GetMonthlySiteTrends(BusinessUnitId, VehicleType.Both, new FirstLook.Common.Core.DateRange(_startDate, _endDate)))
                    .Returns(bothSitePerformanceTrends);
        }


       
        [TestCase("1/12/2011", "1/1/2011")]
        [TestCase("2/28/2011", "2/1/2011")]
        public void Can_get_start_of_month_from_current_time(string endDateStr, string expectedDateStr)
        {
            _clock.SetupGet(x => x.CurrentTime).Returns(DateTime.Parse(endDateStr));
            var endDate = _controller.GetEndDate(_clock.Object);
            var startDate = _controller.GetStartOfMonth(endDate);

            Assert.IsTrue(startDate.ToShortDateString().Equals(expectedDateStr));

        }
        [TestCase("2/1/2011", "1/1/2011", "1/31/2011")]
        [TestCase("2/2/2011", "1/1/2011", "1/31/2011")]
        [TestCase("2/3/2011", "1/1/2011", "1/31/2011")]
        [TestCase("3/3/2011", "2/1/2011", "2/28/2011")]
        [TestCase("3/2/2011", "2/1/2011", "2/28/2011")]
        [TestCase("3/1/2011", "2/1/2011", "2/28/2011")]
        //FB 19756
        public void First_3_days_of_month_should_return_previous_month_as_end_date(string endDateStr, string expectedStartDateStr, string expectedEndDateStr)
        {
            _clock.SetupGet(x => x.CurrentTime).Returns(DateTime.Parse(endDateStr));
            var endDate = _controller.GetEndDate(_clock.Object);
            var startDate = _controller.GetStartOfMonth(endDate);

            Assert.IsTrue(startDate.ToShortDateString().Equals(expectedStartDateStr));
            Assert.IsTrue(endDate.ToShortDateString().Equals(expectedEndDateStr));

        }
    }
}
