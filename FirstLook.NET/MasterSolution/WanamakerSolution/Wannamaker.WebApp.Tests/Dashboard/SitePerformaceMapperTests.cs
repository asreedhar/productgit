﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using NUnit.Framework;
using FirstLook.Common.Core;
using System;

namespace Wannamaker.WebApp.Tests.Dashboard
{

    public class SitePerformaceMapperTests
    {
        private readonly List<SitePerformance> _perfModel1 = new List<SitePerformance>
                                {
                                    new SitePerformance
                                        {
                                            AdPrintedCount = 1,
                                            EmailLeadsCount = 1,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = 40,
                                            SearchPageViewCount = 5000,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            Site = new Site{ Name = "AutoTrader.com",
                                        }},
                                    new SitePerformance
                                        {
                                            AdPrintedCount = 0,
                                            EmailLeadsCount = 1,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = 30,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            SearchPageViewCount = 15000,Site = new Site{ Name = "Cars.com"
                                        }}
                                };
        [TestCase]
        public void Can_get_site_perf_vm_from_model()
        {

            var results = SitePerformanceMapper.Map(_perfModel1);

            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count == 2);
            Assert.IsTrue(results["AutoTrader.com"].SiteName == "AutoTrader.com");
            Assert.IsTrue(results["Cars.com"].SiteName == "Cars.com");
        }

        [TestCase]
        public void Can_get_formatted_actual_values_from_model()
        {

            var results = SitePerformanceMapper.Map(_perfModel1);

            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count == 2);
            Assert.IsTrue(results["AutoTrader.com"].ActionValue == 2);
            Assert.IsTrue(results["AutoTrader.com"].SearchValue == 5000);
            Assert.IsTrue(results["AutoTrader.com"].DetailValue == 40);


            Assert.IsTrue(results["Cars.com"].ActionValue == 1);
            Assert.IsTrue(results["Cars.com"].SearchValue == 15000);
            Assert.IsTrue(results["Cars.com"].DetailValue == 30);

        }

        [TestCase(5000, 15000, 5000 * 100 / 15000, 15000 * 100 / 15000)]
        [TestCase(0, 0, 0, 0)]
        [TestCase(300, 0, 100, 0)]
        [TestCase(0, 200, 0, 100)]
        [TestCase(0, 0, 0, 0)]
        [TestCase(-1, 0, 0, 0)]
        [TestCase(0, -1, 0, 0)]
        public void Can_get_scaled_search_value_from_model(int searchVal1, int searchVal2, int scaledVal1, int scaledVal2)
        {

            var model = new List<SitePerformance>
                                {
                                    new SitePerformance
                                        {
                                            AdPrintedCount = 1,
                                            EmailLeadsCount = 1,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = 40,
                                            SearchPageViewCount = searchVal1,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            Site = new Site{ Name = "AutoTrader.com"
                                        }},
                                    new SitePerformance
                                        {
                                            AdPrintedCount = 0,
                                            EmailLeadsCount = 1,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = 30,
                                            SearchPageViewCount = searchVal2,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            Site = new Site{ Name = "Cars.com"
                                        }}
                                };

            var results = SitePerformanceMapper.Map(model);

            Assert.IsTrue(results != null);

            Assert.IsTrue(results.Count == 2);
        }


        [TestCase(200, 600, 27, 80)]
        [TestCase(600, 200, 80, 27)]
        [TestCase(0, 0, 0, 0)]
        [TestCase(500, 0, 80, 0)]
        [TestCase(0, 400, 0, 80)]
        [TestCase(0, 0, 0, 0)]
        [TestCase(-1, 0, 0, 0)]
        [TestCase(0, -1, 0, 0)]
        public void Can_get_scaled_detail_value_from_model(int detailVal1, int detailVal2, int scaledVal1, int scaledVal2)
        {

            var model = new List<SitePerformance>
                                {
                                    new SitePerformance
                                        {
                                            AdPrintedCount = 1,
                                            EmailLeadsCount = 1,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = detailVal1,
                                            SearchPageViewCount = 5000,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            Site = new Site{ Name = "AutoTrader.com"
                                        }},
                                    new SitePerformance
                                        {
                                            AdPrintedCount = 0,
                                            EmailLeadsCount = 1,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = detailVal2,
                                            SearchPageViewCount = 15000,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            Site = new Site{ Name = "Cars.com"
                                        }}
                                };

            var results = SitePerformanceMapper.Map(model);

            Assert.IsTrue(results != null);

            Assert.IsTrue(results.Count == 2);
        }

        [TestCase(5, 10, 30, 60)]
        [TestCase(5, 9, 33, 60)]
        [TestCase(0, 0, 0, 0)]
        [TestCase(5, 0, 60, 0)]
        [TestCase(0, 4, 0, 60)]
        [TestCase(0, 0, 0, 0)]
        [TestCase(-1, 0, 0, 0)]
        [TestCase(0, -1, 0, 0)]
        public void Can_get_scaled_action_value_from_model(int actionVal1, int actionVal2, int scaledVal1, int scaledVal2)
        {

            var model = new List<SitePerformance>
                                {
                                    new SitePerformance
                                        {
                                            AdPrintedCount = actionVal1,
                                            EmailLeadsCount = 0,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = 40,
                                            SearchPageViewCount = 5000,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            Site = new Site{ Name = "AutoTrader.com"
                                        }},
                                    new SitePerformance
                                        {
                                            AdPrintedCount = 0,
                                            EmailLeadsCount = actionVal2,
                                            MapsViewedCount = 0,
                                            PhoneLeadsCount = 0,
                                            DetailPageViewCount = 60,
                                            SearchPageViewCount = 15000,
                                            DateRange = DateRange.MonthOfDate(DateTime.Now),
                                            Site = new Site{ Name = "Cars.com"
                                        }}
                                };

            var results = SitePerformanceMapper.Map(model);

            Assert.IsTrue(results != null);

            Assert.IsTrue(results.Count == 2);
        }


    }
}
