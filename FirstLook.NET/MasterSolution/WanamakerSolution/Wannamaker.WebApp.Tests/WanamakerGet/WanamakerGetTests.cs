﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities.Enumerations;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NUnit.Framework;
using Wanamaker.WebApp.Areas.WanamakerGet.Controllers;
using Wanamaker.WebApp.Areas.WanamakerGet.Models;
using SiteBudgetCollection = System.Collections.Generic.Dictionary<int, System.Collections.Generic.List<FirstLook.Merchandising.DomainModel.Reports.SitePerformance.SiteBudget>>;

namespace Wannamaker.WebApp.Tests.Dashboard
{
    [TestFixture]
    public class WanamakerGetTests
    {
        #region Test Setup

        private Mock<IWebContext> WebContext;
        private Mock<ISiteBudgetRepository> SiteBudgetRepository;
        private Mock<IGoogleAnalyticsWebAuthorization> GoogleAnalyticsWebAuthorization;
        private Mock<IGoogleAnalyticsQuery> GoogleAnalyticsQuery;
        private Mock<IGoogleAnalyticsRepository> GoogleAnalyticsRepository;
        private Mock<IWebsiteProviderMetricsModel> WebsiteProviderMetricsModel;
        private Mock<IDealerListingSiteCredentialRepository> DealerListingSiteCredentialRepository;
        private Mock<IDealershipSegmentModel> DealershipSegmentModel;
        ///
        private WanamakerGetController Controller;

        public void TestInit()
        {
            WebContext = new Mock<IWebContext>();
            SiteBudgetRepository = new Mock<ISiteBudgetRepository>();
            GoogleAnalyticsWebAuthorization = new Mock<IGoogleAnalyticsWebAuthorization>();
            GoogleAnalyticsQuery = new Mock<IGoogleAnalyticsQuery>();
            GoogleAnalyticsRepository = new Mock<IGoogleAnalyticsRepository>();
            WebsiteProviderMetricsModel = new Mock<IWebsiteProviderMetricsModel>();
            DealerListingSiteCredentialRepository = new Mock<IDealerListingSiteCredentialRepository>();
            DealershipSegmentModel = new Mock<IDealershipSegmentModel>();

            Controller = new WanamakerGetController(
                WebContext.Object,
                SiteBudgetRepository.Object,
                GoogleAnalyticsWebAuthorization.Object,
                GoogleAnalyticsQuery.Object,
                GoogleAnalyticsRepository.Object,
                WebsiteProviderMetricsModel.Object,
                DealerListingSiteCredentialRepository.Object,
                DealershipSegmentModel.Object );
        }

        #endregion
        
        #region Test Helpers

        private bool JsonEquivalent( object x, object y )
        {
            return JsonConvert.SerializeObject( x ) == JsonConvert.SerializeObject( y );
        }

        #endregion

        [TestCase( 0,      null,     typeof(WanamakerGetException) )]
        [TestCase( 0,      "XXXXXX", typeof(WanamakerGetException) )]
        [TestCase( 103233, "XXXXXX", typeof(WanamakerGetException) )]
        [TestCase( 103233, "123456", typeof(WanamakerGetException) )]
        [TestCase( 103233, "103233", null )]
        public void Load_Exceptions( int http_BU_ID, string param_api_token, Type exception_type )
        {
            TestInit();

            var business_unit = http_BU_ID > 0 ? new BusinessUnit { Id = http_BU_ID } : null;
            WebContext.Setup( x => x.GetBusinessUnit() ).Returns( business_unit );

            try
            {
                var data = Controller.Load( param_api_token );
                var data_obj = WanamakerDataCollectionFormModel.fromJSON( data );

                Assert.That( data_obj, Is.Not.Null );
            }
            catch (Exception ex)
            {
                // if the test case specifies that an exception was expected, test it
                if( exception_type != null )
                    Assert.That( ex, Is.InstanceOf(exception_type) );
                else
                    throw;
            }
        }

        [Test]
        public void Load_SiteBudgets_Validity()
        {
            TestInit();

            int bu_id = 103233;
            DateTime now = DateTime.Now;
            DateTime this_month = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });
            
            SiteBudgetRepository.Setup( x => x.GetAllBudgetsForBusinessUnit( bu_id ))
                .Returns( new SiteBudgetCollection {
                    { (int)EdtDestinations.CarsDotCom, new List<SiteBudget> {
                        new SiteBudget { DestinationId = (int)EdtDestinations.CarsDotCom, BusinessUnitId = bu_id, MonthApplicable = this_month.AddMonths( 0 ), Budget = 1000, DescriptionText = null, UpdatedOn = now },
                        new SiteBudget { DestinationId = (int)EdtDestinations.CarsDotCom, BusinessUnitId = bu_id, MonthApplicable = this_month.AddMonths( -2 ), Budget = 800, DescriptionText = null, UpdatedOn = now },
                        new SiteBudget { DestinationId = (int)EdtDestinations.CarsDotCom, BusinessUnitId = bu_id, MonthApplicable = this_month.AddMonths( -5 ), Budget = 250, DescriptionText = null, UpdatedOn = now },
                    }},
                    { (int)EdtDestinations.autoUplinkUSA, new List<SiteBudget> {
                        new SiteBudget { DestinationId = (int)EdtDestinations.autoUplinkUSA, BusinessUnitId = bu_id, MonthApplicable = this_month.AddMonths( -1 ), Budget = 65, DescriptionText = null, UpdatedOn = now },
                    }},
                });
            
            var expected = new WanamakerDataCollectionFormModel {
                dealer_listing_sites = new List<DealerListingSite> {
                    new DealerListingSite { 
                        site_id = (int)EdtDestinations.CarsDotCom,
                        site_name = "Cars.com",
                        monthly_cost = new List<LinkedBudget> {
                            new LinkedBudget { month = this_month.AddMonths( 0 ), value = 1000, soft = false },
                            new LinkedBudget { month = this_month.AddMonths( -2 ), value = 800, soft = false },
                            new LinkedBudget { month = this_month.AddMonths( -5 ), value = 250, soft = false },
                        },
                    },
                },
            }.toJSON();

            var data = Controller.Load( param_api_token );
            Assert.That( data, Is.EqualTo( expected ));
        }

        [Test]
        public void Load_SiteCredentials_Validity()
        {
            TestInit();

            int bu_id = 103233;
            
            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            DealerListingSiteCredentialRepository.Setup( x => x.Load( bu_id, null, false ))
                .Returns( new List<DealerListingSiteCredential> {
                    new DealerListingSiteCredential { BusinessUnitId = bu_id, DestinationId = (int)EdtDestinations.CarsDotCom, Username = "holy_mackarel_5", Password = "h4ll3luj4h", ExternalID = "CARS17384" },
                });

            var expected = new WanamakerDataCollectionFormModel {
                dealer_listing_sites = new List<DealerListingSite> {
                    new DealerListingSite { site_id = (int)EdtDestinations.CarsDotCom, site_name = "Cars.com", username = "holy_mackarel_5", password = "h4ll3luj4h", dealer_id = "CARS17384" },
                },
            }.toJSON();

            var data = Controller.Load( param_api_token );
            Assert.That( data, Is.EqualTo( expected ));
        }

        [TestCase( "normal - no data" )]
        [TestCase( "normal - profiles can be retrieved & has active selection" )]
        [TestCase( "normal - account cleared after setup" )]
        public void Load_GoogleAnalytics_Validity( string data_variant )
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            string expected = null;

            switch( data_variant )
            {
                case "normal - no data":{
                    GoogleAnalyticsWebAuthorization.Setup( x => x.IsAuthorized( bu_id ))
                        .Returns( true );

                    GoogleAnalyticsQuery.Setup( x => x.GetProfiles() )
                        .Returns( new ProfileResult[0] );
            
                    expected = new WanamakerDataCollectionFormModel {
                        google_analytics = null,
                    }.toJSON();

                    var data = Controller.Load( param_api_token );
                    GoogleAnalyticsQuery.Verify( x => x.Init( bu_id ), Times.Once() );
                    Assert.That( data, Is.EqualTo( expected ));
                } break;

                case "normal - profiles can be retrieved & has active selection":{
                    GoogleAnalyticsWebAuthorization.Setup( x => x.IsAuthorized( bu_id ))
                        .Returns( true );

                    GoogleAnalyticsQuery.Setup( x => x.GetProfiles() )
                        .Returns( new [] {
                            new ProfileResult( 1337000, "lunchvote.net" ),
                            new ProfileResult( 1337320, "turntable.fm/electronic_dance_music" ),
                            new ProfileResult( 1337399, "grooveshark.com" ),
                        });
            
                    GoogleAnalyticsRepository.Setup( x => x.FetchProfile( bu_id, GoogleAnalyticsType.PC) )
                        .Returns( new GoogleAnalyticsProfile( bu_id, GoogleAnalyticsType.PC, 1337000, "lunchvote.net", (int)EdtDestinations.DealerDotCom ));

                    WebsiteProviderMetricsModel.Setup( x => x.GetGoogleVdpSites() )
                        .Returns( new List<EdtDestinations> {
                            EdtDestinations.DealerDotCom,
                        });

                    expected = new WanamakerDataCollectionFormModel {
                        google_analytics = new GoogleAnalyticsProfileWrapper {
                            google_analytics_profiles = new List<ProfileResult> {
                                new ProfileResult( 1337000, "lunchvote.net" ),
                            new ProfileResult( 1337320, "turntable.fm/electronic_dance_music" ),
                            new ProfileResult( 1337399, "grooveshark.com" ),
                            },
                            compatible_providers = new List<Provider> {
                                new Provider { id = (int)EdtDestinations.DealerDotCom, name = "Dealer.com" },
                                new Provider { id = 0, name = "Other" },
                            },
                            selected_profile_id = 1337000,
                            selected_profile_name = "lunchvote.net",
                            associated_provider_id = (int)EdtDestinations.DealerDotCom,
                        },
                    }.toJSON();

                    var data = Controller.Load( param_api_token );
                    GoogleAnalyticsQuery.Verify( x => x.Init( bu_id ), Times.Once() );
                    Assert.That( data, Is.EqualTo( expected ));
                } break;

                case "normal - account cleared after setup":{
                    GoogleAnalyticsWebAuthorization.Setup( x => x.IsAuthorized( bu_id ))
                        .Returns( false );

                    GoogleAnalyticsQuery.Setup( x => x.GetProfiles() )
                        .Returns( new [] {
                            new ProfileResult( 1337000, "lunchvote.net" ),
                            new ProfileResult( 1337320, "turntable.fm/electronic_dance_music" ),
                            new ProfileResult( 1337399, "grooveshark.com" ),
                        });
            
                    GoogleAnalyticsRepository.Setup( x => x.FetchProfile( bu_id, GoogleAnalyticsType.PC) )
                        .Returns( new GoogleAnalyticsProfile( bu_id, GoogleAnalyticsType.PC, 1337000, "lunchvote.net", (int)EdtDestinations.DealerDotCom ));

                    expected = new WanamakerDataCollectionFormModel {
                        google_analytics = null,
                    }.toJSON();
                    
                    var data = Controller.Load( param_api_token );
                    Assert.That( data, Is.EqualTo( expected ));
                } break;
            }
        }

        [Test]
        public void Load_DealerWebsiteSelection_Validity()
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            WebsiteProviderMetricsModel.Setup( x => x.GetProviders( new List<int> { -9001 }))
                .Returns( new List<WebsiteProvider> {
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.DealerDotCom, Name = "Dealer.com", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.eBizAutos, Name = "eBizAutos", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.Cobalt, Name = "Cobalt", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.ClickMotive, Name = "ClickMotive", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = -9001, Name = "GoDaddy.com", IsFreeform = true },
                });

            WebsiteProviderMetricsModel.Setup( x => x.GetSelectedProviderID( bu_id ))
                .Returns( -9001 );

            var expected = new WanamakerDataCollectionFormModel {
                dealer_vdps = new DealerVehicleDetailPage {
                    dealer_website_providers = new List<Provider> {
                        new Provider { id = (int)EdtDestinations.DealerDotCom, name = "Dealer.com" },
                        new Provider { id = (int)EdtDestinations.eBizAutos, name = "eBizAutos" },
                        new Provider { id = (int)EdtDestinations.Cobalt, name = "Cobalt" },
                        new Provider { id = (int)EdtDestinations.ClickMotive, name = "ClickMotive" },
                        new Provider { id = -9001, name = "GoDaddy.com" },
                    },
                    dealer_website_provider_id = -9001,
                },
            }.toJSON();
 
            var data = Controller.Load( param_api_token );
            Assert.That( data, Is.EqualTo( expected ));
        }

        [Test]
        public void Load_DealerWebsiteCredentials_Validity()
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            WebsiteProviderMetricsModel.Setup( x => x.GetProviders( (List<int>)null ))
                .Returns( new List<WebsiteProvider> {
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.DealerDotCom, Name = "Dealer.com", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.eBizAutos, Name = "eBizAutos", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.Cobalt, Name = "Cobalt", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.ClickMotive, Name = "ClickMotive", IsFreeform = false },
                });

            WebsiteProviderMetricsModel.Setup( x => x.GetSelectedProviderID( bu_id ))
                .Returns( (int)EdtDestinations.DealerDotCom );

            DealerListingSiteCredentialRepository.Setup( x => x.Load( bu_id, null, false ))
                .Returns( new List<DealerListingSiteCredential> {
                    new DealerListingSiteCredential { BusinessUnitId = bu_id, DestinationId = (int)EdtDestinations.DealerDotCom, Username = "BROTACULAR", Password = "bros4life" },
                });

            var expected = new WanamakerDataCollectionFormModel {
                dealer_vdps = new DealerVehicleDetailPage { 
                    dealer_website_providers = new List<Provider> {
                        new Provider { id = (int)EdtDestinations.DealerDotCom, name = "Dealer.com" },
                        new Provider { id = (int)EdtDestinations.eBizAutos, name = "eBizAutos" },
                        new Provider { id = (int)EdtDestinations.Cobalt, name = "Cobalt" },
                        new Provider { id = (int)EdtDestinations.ClickMotive, name = "ClickMotive" },
                    },
                    dealer_website_provider_id = (int)EdtDestinations.DealerDotCom,
                    vdp_data = new List<DealerVDPData> {
                        new DealerVDPData {
                            website_provider_id = (int)EdtDestinations.DealerDotCom,
                            username = "BROTACULAR", 
                            password = "bros4life" },
                    },
                },
            }.toJSON();

            var data = Controller.Load( param_api_token );
            Assert.That( data, Is.EqualTo( expected ));
        }

        [Test]
        public void Load_DealerWebsiteVDPs_Validity()
        {
            TestInit();

            int bu_id = 103233;
            DateTime now = DateTime.Now;
            DateTime this_month = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            DateTime some_months_ago = this_month.AddMonths( -1 * WanamakerGetController.ASSUMED_TIMESPAN_MONTHS );

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            WebsiteProviderMetricsModel.Setup( x => x.GetProviders( (List<int>)null ))
                .Returns( new List<WebsiteProvider> {
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.DealerDotCom, Name = "Dealer.com", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.eBizAutos, Name = "eBizAutos", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.Cobalt, Name = "Cobalt", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.ClickMotive, Name = "ClickMotive", IsFreeform = false },
                });

            WebsiteProviderMetricsModel.Setup( x => x.GetSelectedProviderID( bu_id ))
                .Returns( (int)EdtDestinations.DealerDotCom );

            WebsiteProviderMetricsModel.Setup( x => x.GetVDPs( bu_id, some_months_ago, this_month ))
                .Returns( new List<MonthlyWebsiteMetric> {
                    new MonthlyWebsiteMetric { BusinessUnitID = bu_id, WebsiteProviderID = (int)EdtDestinations.DealerDotCom, MonthApplicable = this_month.AddMonths( 0 ), UsedVDPs = 500, NewVDPs = 50 },
                    new MonthlyWebsiteMetric { BusinessUnitID = bu_id, WebsiteProviderID = (int)EdtDestinations.DealerDotCom, MonthApplicable = this_month.AddMonths( -1 ), UsedVDPs = 400, NewVDPs = 40 },
                    new MonthlyWebsiteMetric { BusinessUnitID = bu_id, WebsiteProviderID = (int)EdtDestinations.DealerDotCom, MonthApplicable = this_month.AddMonths( -2 ), UsedVDPs = 40, NewVDPs = 10 },
                });

            var expected = new WanamakerDataCollectionFormModel {
                dealer_vdps = new DealerVehicleDetailPage {
                    dealer_website_providers = new List<Provider> {
                        new Provider { id = (int)EdtDestinations.DealerDotCom, name = "Dealer.com" },
                        new Provider { id = (int)EdtDestinations.eBizAutos, name = "eBizAutos" },
                        new Provider { id = (int)EdtDestinations.Cobalt, name = "Cobalt" },
                        new Provider { id = (int)EdtDestinations.ClickMotive, name = "ClickMotive" },
                    },
                    dealer_website_provider_id = (int)EdtDestinations.DealerDotCom,
                    vdp_data = new List<DealerVDPData> {
                        new DealerVDPData {
                            website_provider_id = (int)EdtDestinations.DealerDotCom,
                            vdps = new List<DatedVDP> {
                                new DatedVDP { month = this_month.AddMonths( 0 ), used_value = 500, new_value = 50 },
                                new DatedVDP { month = this_month.AddMonths( -1 ), used_value = 400, new_value = 40 },
                                new DatedVDP { month = this_month.AddMonths( -2 ), used_value = 40, new_value = 10 },
                            },
                        },
                    },
                },
            }.toJSON();
        
            var data = Controller.Load( param_api_token );
            Assert.That( data, Is.EqualTo( expected ));
        }

        [TestCase( DealershipSegment.Undefined )]
        [TestCase( DealershipSegment.Domestic )]
        [TestCase( DealershipSegment.Highline )]
        [TestCase( DealershipSegment.Import )]
        public void Load_DealershipSegment_Validity( DealershipSegment segment )
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            DealershipSegmentModel.Setup( x => x.get( bu_id ))
                .Returns( segment );

            var expected = new WanamakerDataCollectionFormModel {
                dealership_segment = segment != DealershipSegment.Undefined ? new DealershipSegmentWrapper { segment = segment } : null
            }.toJSON();
        
            var data = Controller.Load( param_api_token );
            Assert.That( data, Is.EqualTo( expected ));
        }

        [Test]
        public void Save_SiteBudgets_Validity()
        {
            TestInit();

            int bu_id = 103233;
            DateTime now = DateTime.Now;
            DateTime this_month = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            DateTime some_months_ago = this_month.AddMonths( -1 * WanamakerGetController.ASSUMED_TIMESPAN_MONTHS );

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            var data = new WanamakerDataCollectionFormModel {
                dealer_listing_sites = new List<DealerListingSite> {
                    new DealerListingSite { 
                        site_id = (int)EdtDestinations.AutoTrader, 
                        site_name = "AutoTrader", 
                        monthly_cost = new List<LinkedBudget> {
                            new LinkedBudget { month = this_month.AddMonths( -6 ), value = 5000, soft = false },
                        },
                    },
                    new DealerListingSite { 
                        site_id = (int)EdtDestinations.CarsDotCom, 
                        site_name = "Cars.com", 
                        monthly_cost = new List<LinkedBudget> {
                        },
                    },
                },
            }.toJSON();

            var budget_data = new SiteBudgetCollection {
                { (int)EdtDestinations.AutoTrader, new List<SiteBudget> {
                    new SiteBudget { BusinessUnitId = bu_id, DestinationId = (int)EdtDestinations.AutoTrader, MonthApplicable = this_month.AddMonths( -6 ), Budget = 5000 },
                }},
            };

            Controller.Save( param_api_token, data );

            SiteBudgetRepository.Verify( x => x.ReplaceBudgetsForBusinessUnitId( bu_id, It.Is<SiteBudgetCollection>( k => JsonEquivalent( budget_data, k )), some_months_ago, this_month ), Times.Once() );
        }

        [Test]
        public void Save_SiteCredentials_Validity()
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            var data = new WanamakerDataCollectionFormModel {
                dealer_listing_sites = new List<DealerListingSite> {
                    new DealerListingSite {
                        site_id = (int)EdtDestinations.CarsDotCom, site_name = "Cars.com",
                        username = "Sir Mittens of Romnibus", password = "12345", dealer_id = "CARSX57891" },
                },
            }.toJSON();

            Controller.Save( param_api_token, data );

            var cred = new DealerListingSiteCredential {
                BusinessUnitId = bu_id, DestinationId = (int)EdtDestinations.CarsDotCom, 
                Username = "Sir Mittens of Romnibus", Password = "12345", ExternalID = "CARSX57891" };
            
            DealerListingSiteCredentialRepository.Verify( x => x.Save( It.Is<DealerListingSiteCredential>( k => JsonEquivalent( cred, k ))), Times.Once() );
        }

        [Test]
        public void Save_GoogleAnalytics_Validity()
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            GoogleAnalyticsQuery.Setup( x => x.GetProfiles() )
                .Returns( new [] {
                    new ProfileResult( 1337000, "lunchvote.net" ),
                    new ProfileResult( 1337320, "turntable.fm/electronic_dance_music" ),
                    new ProfileResult( 1337399, "grooveshark.com" ),
                });
            
            GoogleAnalyticsRepository.Setup( x => x.FetchProfile( bu_id, GoogleAnalyticsType.PC) )
                .Returns( new GoogleAnalyticsProfile( bu_id, GoogleAnalyticsType.PC, 1337000, "lunchvote.net", EdtDestinations.Undefined ));

            var data = new WanamakerDataCollectionFormModel {
                google_analytics = new GoogleAnalyticsProfileWrapper {
                    selected_profile_id = 1337000,
                    selected_profile_name = "lunchvote.net",
                    associated_provider_id = (int)EdtDestinations.DealerDotCom,
                },
            }.toJSON();

            Controller.Save( param_api_token, data );

            var profile = new GoogleAnalyticsProfile( bu_id, GoogleAnalyticsType.PC, 1337000, "lunchvote.net", EdtDestinations.DealerDotCom );

            GoogleAnalyticsRepository.Verify( x => x.CreateUpdateProfileAccount( It.Is<GoogleAnalyticsProfile>( k => JsonEquivalent( profile, k ))), Times.Once() );
        }

        [TestCase( DealershipSegment.Undefined )]
        [TestCase( DealershipSegment.Domestic )]
        [TestCase( DealershipSegment.Highline )]
        [TestCase( DealershipSegment.Import )]
        public void Save_DealershipSegment_Validity( DealershipSegment segment )
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            var data = new WanamakerDataCollectionFormModel {
                dealership_segment = new DealershipSegmentWrapper {
                    segment = segment
                },
            }.toJSON();

            Controller.Save( param_api_token, data );

            DealershipSegmentModel.Verify( x => x.set( bu_id, segment ), Times.Once() );
        }

        [TestCase( (int)EdtDestinations.DealerDotCom, 0, null )]
        [TestCase( -1, -109, "SuperDealerSiteDeluxe.com" )]
        [TestCase( -1, 0, null )]
        public void Save_DealerWebsiteSelection_Validity( int provider_id, int freeform_provider_id, string freeform_name )
        {
            TestInit();

            int bu_id = 103233;
            DateTime now = DateTime.Now;
            DateTime this_month = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            DateTime some_months_ago = this_month.AddMonths( -1 * WanamakerGetController.ASSUMED_TIMESPAN_MONTHS );

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            WebsiteProviderMetricsModel.Setup( x => x.GetProviders( (List<int>)null ))
                .Returns( new List<WebsiteProvider> {
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.DealerDotCom, Name = "Dealer.com", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.eBizAutos, Name = "eBizAutos", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.Cobalt, Name = "Cobalt", IsFreeform = false },
                    new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.ClickMotive, Name = "ClickMotive", IsFreeform = false },
                });

            WebsiteProviderMetricsModel.Setup( x => x.FindOrCreateFreeformProvider( freeform_name ))
                .Returns( freeform_provider_id );

            var data = new WanamakerDataCollectionFormModel {
                dealer_vdps = new DealerVehicleDetailPage {
                    dealer_website_provider_id = provider_id,
                    dealer_website_provider_freeform_name = freeform_name,
                    dealer_website_providers = new List<Provider> {
                        new Provider { id = (int)EdtDestinations.DealerDotCom, name = "Dealer.com" },
                        new Provider { id = (int)EdtDestinations.eBizAutos, name = "eBizAutos" },
                        new Provider { id = (int)EdtDestinations.Cobalt, name = "Cobalt" },
                        new Provider { id = (int)EdtDestinations.ClickMotive, name = "ClickMotive" },
                    },
                },
            }.toJSON();

            try
            {
                Controller.Save( param_api_token, data );
            }
            catch (WanamakerGetException ex)
            {
                if( provider_id == -1 && freeform_name == null )
                    Assert.Pass();
            }

            WebsiteProviderMetricsModel.Verify( x => x.SetVDPs( bu_id, It.IsAny<int>(), some_months_ago, this_month, It.IsAny<List<MonthlyWebsiteMetric>>() ), Times.Never() );
            if( provider_id != -1 || freeform_name == null ) {
                WebsiteProviderMetricsModel.Verify( x => x.SetSelectedProviderID( bu_id, provider_id ), Times.Once() );
            } else {
                WebsiteProviderMetricsModel.Verify( x => x.FindOrCreateFreeformProvider( freeform_name ), Times.Once() );
                WebsiteProviderMetricsModel.Verify( x => x.SetSelectedProviderID( bu_id, freeform_provider_id ), Times.Once() );
            }
        }

        [TestCase( (int)EdtDestinations.DealerDotCom, null, 0, null, "user", "pass", Description="set credentials for existing provider")]
        [TestCase( -1, "SuperDealerSiteDeluxe.com", -109, null, "user", "pass", Description="set credentials to new 'freeform' provider'")]
        [TestCase( -109, null, 0, "SuperDealerSiteDeluxe.com", "user", "pass", Description="set credentials for existing 'freeform' provider" )]
        public void Save_DealerWebsiteCredentials_Validity( int input_provider_id, string input_freeform_name, int existing_freeform_provider_id, string existing_freeform_name, string input_username, string input_password )
        {
            TestInit();

            int bu_id = 103233;

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            var provider_id_under_test = input_provider_id != -1 ? input_provider_id : existing_freeform_provider_id;

            var existing_freeform_ids = (List<int>)null;
            if( input_provider_id < 0 )
                existing_freeform_ids = new List<int> { existing_freeform_provider_id };
            var returned_provider_list = new List<WebsiteProvider> {
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.DealerDotCom, Name = "Dealer.com", IsFreeform = false },
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.eBizAutos, Name = "eBizAutos", IsFreeform = false },
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.Cobalt, Name = "Cobalt", IsFreeform = false },
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.ClickMotive, Name = "ClickMotive", IsFreeform = false },
            };
            if( existing_freeform_ids != null )
                returned_provider_list.Add( new WebsiteProvider { WebsiteProviderID = provider_id_under_test, Name = existing_freeform_name, IsFreeform = true });
            WebsiteProviderMetricsModel.Setup( x => x.GetProviders( existing_freeform_ids ))
                .Returns( returned_provider_list );

            WebsiteProviderMetricsModel.Setup( x => x.FindOrCreateFreeformProvider( input_freeform_name ))
                .Returns( existing_freeform_provider_id );

            var simulated_input_vdp_data = returned_provider_list.Select( 
                p => new DealerVDPData { website_provider_id = p.WebsiteProviderID, username = "", password = "", vdps = new List<DatedVDP>() }).ToList();
            var modified_vdp = simulated_input_vdp_data.Find( p => p.website_provider_id == provider_id_under_test );
            Assert.IsTrue( modified_vdp != null );
            modified_vdp.username = input_username;
            modified_vdp.password = input_password;

            var data = new WanamakerDataCollectionFormModel {
                dealer_vdps = new DealerVehicleDetailPage {
                    dealer_website_provider_id = input_provider_id,
                    dealer_website_provider_freeform_name = input_freeform_name,
                    vdp_data = simulated_input_vdp_data,
                },
            }.toJSON();

            Controller.Save( param_api_token, data );

            if( input_provider_id == -1 && input_freeform_name != null ) {
                WebsiteProviderMetricsModel.Verify( x => x.FindOrCreateFreeformProvider( input_freeform_name ), Times.Once() );
            }
            var cred = new DealerListingSiteCredential { BusinessUnitId = bu_id, DestinationId = provider_id_under_test, Username = input_username, Password = input_password };
            DealerListingSiteCredentialRepository.Verify( x => x.Save( It.Is<DealerListingSiteCredential>( k => JsonEquivalent( k, cred ))), Times.Once() );
        }

        public static object[] Save_DealerWebsiteVDPs_Validity_Cases = 
        {
            new object[] { (int)EdtDestinations.DealerDotCom, null, 0, null, 
                new[]{"2013/05/01",  "2013/04/01",  "2013/03/01",  "2012/05/01"},
                new[]{new[]{500,50}, new[]{400,40}, new[]{300,30}, new[]{10,1}}
            },
        };
        [Test, TestCaseSource("Save_DealerWebsiteVDPs_Validity_Cases")]
        public void Save_DealerWebsiteVDPs_Validity( int input_provider_id, string input_freeform_name, int existing_freeform_provider_id, string existing_freeform_name, string[] input_vdps_timestamps, int[][] input_vdps_table )
        {
            TestInit();

            int bu_id = 103233;
            DateTime now = DateTime.Now;
            DateTime this_month = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            DateTime some_months_ago = this_month.AddMonths( -1 * WanamakerGetController.ASSUMED_TIMESPAN_MONTHS );

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            var provider_id_under_test = input_provider_id != -1 ? input_provider_id : existing_freeform_provider_id;

            var existing_freeform_ids = (List<int>)null;
            if( input_provider_id < 0 )
                existing_freeform_ids = new List<int> { existing_freeform_provider_id };
            var returned_provider_list = new List<WebsiteProvider> {
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.DealerDotCom, Name = "Dealer.com", IsFreeform = false },
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.eBizAutos, Name = "eBizAutos", IsFreeform = false },
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.Cobalt, Name = "Cobalt", IsFreeform = false },
                new WebsiteProvider { WebsiteProviderID = (int)EdtDestinations.ClickMotive, Name = "ClickMotive", IsFreeform = false },
            };
            if( existing_freeform_ids != null )
                returned_provider_list.Add( new WebsiteProvider { WebsiteProviderID = provider_id_under_test, Name = existing_freeform_name, IsFreeform = true });
            WebsiteProviderMetricsModel.Setup( x => x.GetProviders( existing_freeform_ids ))
                .Returns( returned_provider_list );

            WebsiteProviderMetricsModel.Setup( x => x.FindOrCreateFreeformProvider( input_freeform_name ))
                .Returns( existing_freeform_provider_id );

            var simulated_input_vdp_data = returned_provider_list.Select( 
                p => new DealerVDPData { website_provider_id = p.WebsiteProviderID, vdps = new List<DatedVDP>() }).ToList();
            var modified_vdp = simulated_input_vdp_data.Find( p => p.website_provider_id == provider_id_under_test );
            Assert.IsTrue( modified_vdp != null );
            for( var row = 0; row < input_vdps_timestamps.Length; ++row )
            {
                modified_vdp.vdps.Add( new DatedVDP { 
                    month = DateTime.Parse( input_vdps_timestamps[row] ), 
                    used_value = input_vdps_table[row][0], 
                    new_value = input_vdps_table[row][1] });
            }
            var data = new WanamakerDataCollectionFormModel {
                dealer_vdps = new DealerVehicleDetailPage {
                    dealer_website_provider_id = input_provider_id,
                    dealer_website_provider_freeform_name = input_freeform_name,
                    vdp_data = simulated_input_vdp_data,
                },
            }.toJSON();

            Controller.Save( param_api_token, data );

            if( input_provider_id == -1 && input_freeform_name != null ) {
                WebsiteProviderMetricsModel.Verify( x => x.FindOrCreateFreeformProvider( input_freeform_name ), Times.Once() );
            }
            var simulated_db_input = modified_vdp.vdps.Select( v => 
                new MonthlyWebsiteMetric { BusinessUnitID = bu_id, WebsiteProviderID = provider_id_under_test, MonthApplicable = v.month, UsedVDPs = v.used_value, NewVDPs = v.new_value });
            WebsiteProviderMetricsModel.Verify( x => x.SetVDPs( bu_id, provider_id_under_test, some_months_ago, this_month, It.Is<List<MonthlyWebsiteMetric>>( k => JsonEquivalent( k, simulated_db_input ))), Times.Once() );
        }

        [Test]
        public void Save_Exceptions_BadJSON()
        {
            TestInit();

            int bu_id = 103233;
            DateTime now = DateTime.Now;
            DateTime this_month = new DateTime( now.Year, now.Month, 1, 0, 0, 0, 0, DateTimeKind.Utc );
            DateTime some_months_ago = this_month.AddMonths( -1 * WanamakerGetController.ASSUMED_TIMESPAN_MONTHS );

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            var data = JsonConvert.SerializeObject( new {
                dealer_listing_sites = new List<object> {
                    new { 
                        site_id = (int)EdtDestinations.AutoTrader, 
                        site_name = "AutoTrader", 
                        monthly_cost = new List<object> {
                            new { month = this_month.AddMonths( -6 ), value = "ajsdjf$%^@%ASJD", UNRECOGNIZED_FIELD = 5 },
                            new { month = DateTime.Now, value = null as object, UNRECOGNIZED_FIELD = 5 },
                        },
                    },
                    new { 
                        SITE_ID = (int)EdtDestinations.CarsDotCom, 
                        site_Id = (int)EdtDestinations.CarsDotCom, 
                        sITE_iD = (int)EdtDestinations.CarsDotCom,
                        siteid_ = (int)EdtDestinations.CarsDotCom,
                    },
                },
            }, new IsoDateTimeConverter() );

            try
            {
                Controller.Save( param_api_token, data );
            }
            catch( JsonReaderException ex )
            {
                Assert.Pass();
            }
        }

        [Test]
        public void FogBugz_26037__Wanamaker_Data_Collecting__Credential_not_getting_saved_at_Dealer_Website_section()
        {
            TestInit();

            int bu_id = 101006; // recorded from browser
            string data = "{\"dealer_listing_sites\":[{\"site_id\":2,\"site_name\":\"AutoTrader.com\",\"username\":\"hendrickcj\",\"password\":\"HAG57246\",\"dealer_id\":null,\"monthly_cost\":[{\"month\":\"2012-08-01T05:00:00.000Z\",\"value\":6145,\"soft\":false},{\"month\":\"2012-07-01T05:00:00.000Z\",\"value\":6145,\"soft\":false},{\"month\":\"2012-06-01T05:00:00.000Z\",\"value\":4995,\"soft\":false},{\"month\":\"2012-05-01T05:00:00.000Z\",\"value\":4995,\"soft\":false}]},{\"site_id\":1,\"site_name\":\"Cars.com\",\"username\":\"angela.holland@hendrickauto.com\",\"password\":\"Hendrick48\",\"dealer_id\":null,\"monthly_cost\":[{\"month\":\"2012-08-01T05:00:00.000Z\",\"value\":2150,\"soft\":false},{\"month\":\"2012-07-01T05:00:00.000Z\",\"value\":2150,\"soft\":false},{\"month\":\"2012-06-01T05:00:00.000Z\",\"value\":2150,\"soft\":false},{\"month\":\"2012-05-01T05:00:00.000Z\",\"value\":69,\"soft\":false}]}],\"google_analytics\":null,\"dealer_vdps\":{\"dealer_website_provider_id\":4,\"dealer_website_provider_freeform_name\":null,\"vdp_data\":[{\"website_provider_id\":4,\"username\":\"dealercom_user\",\"password\":\"dealercom_pass\",\"vdps\":[]},{\"website_provider_id\":5,\"username\":\"\",\"password\":\"\",\"vdps\":[]},{\"website_provider_id\":9,\"username\":\"\",\"password\":\"\",\"vdps\":[]},{\"website_provider_id\":10,\"username\":\"\",\"password\":\"\",\"vdps\":[]},{\"website_provider_id\":-1,\"username\":\"\",\"password\":\"\",\"vdps\":[]}]},\"dealership_segment\":{\"segment\":0}}"; // recorded from browser

            string param_api_token = bu_id.ToString();
            WebContext.Setup( x => x.GetBusinessUnit() )
                .Returns( new BusinessUnit { Id = bu_id });

            Controller.Save( param_api_token, data );

            var credential_expect_1 = new DealerListingSiteCredential {
                BusinessUnitId = bu_id,
                DestinationId = 4, // dealer.com
                Username = "dealercom_user",
                Password = "dealercom_pass",
            };

            DealerListingSiteCredentialRepository.Verify( x => x.Save( It.Is<DealerListingSiteCredential>( k => JsonEquivalent( k, credential_expect_1 ))), Times.Once() );

            var credential_expect_0 = new DealerListingSiteCredential {
                BusinessUnitId = bu_id,
                DestinationId = 4, // dealer.com
                Username = "",
                Password = "",
            };
        
            DealerListingSiteCredentialRepository.Verify( x => x.Save( It.Is<DealerListingSiteCredential>( k => JsonEquivalent( k, credential_expect_0 ))), Times.Never() );
        }
    }
}