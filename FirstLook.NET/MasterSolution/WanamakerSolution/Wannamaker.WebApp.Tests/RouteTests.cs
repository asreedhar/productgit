﻿using System;
using System.Web.Routing;
using Moq;
using System.Web;
using System.Reflection;
using System.Web.Mvc;
using NUnit.Framework;
using Wanamaker.WebApp.Areas.Dashboard;
using Wanamaker.WebApp.Areas.Inventory;

namespace Wannamaker.WebApp.Tests
{
    public class RouteTests
    {
        private HttpContextBase CreateHttpContext(string targetUrl = null,
                                                  string httpMethod = "GET")
        {
            // create the mock request 
            Mock<HttpRequestBase> mockRequest = new Mock<HttpRequestBase>();
            mockRequest.Setup(m => m.AppRelativeCurrentExecutionFilePath).Returns(targetUrl);
            mockRequest.Setup(m => m.HttpMethod).Returns(httpMethod);

            // create the mock response
            Mock<HttpResponseBase> mockResponse = new Mock<HttpResponseBase>();
            mockResponse.Setup(m => m.ApplyAppPathModifier(
                It.IsAny<string>())).Returns<string>(s => s);

            // create the mock context, using the request and response
            Mock<HttpContextBase> mockContext = new Mock<HttpContextBase>();
            mockContext.Setup(m => m.Request).Returns(mockRequest.Object);
            mockContext.Setup(m => m.Response).Returns(mockResponse.Object);
            // return the mocked context
            return mockContext.Object;
        }

        private bool TestIncomingRouteResult(RouteData routeResult, string controller,
            string action, object propertySet = null)
        {

            Func<object, object, bool> valCompare = (v1, v2) =>
            {
                return StringComparer.InvariantCultureIgnoreCase.Compare(v1, v2) == 0;
            };

            bool result = valCompare(routeResult.Values["controller"], controller)
                && valCompare(routeResult.Values["action"], action);

            if (propertySet != null)
            {
                PropertyInfo[] propInfo = propertySet.GetType().GetProperties();
                foreach (PropertyInfo pi in propInfo)
                {
                    if (!(routeResult.Values.ContainsKey(pi.Name)
                        && valCompare(routeResult.Values[pi.Name],
                            pi.GetValue(propertySet, null))))
                    {

                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        private void TestRouteMatch(string url, string controller, string action, object
            routeProperties = null, string httpMethod = "GET")
        {

            // Arrange
            RouteTable.Routes.Clear();

            var dashboardAreaRegistration = new DashboardAreaRegistration();
            var inventoryAreaRegistration = new InventoryAreaRegistration();
            dashboardAreaRegistration.RegisterArea(new AreaRegistrationContext(dashboardAreaRegistration.AreaName, RouteTable.Routes));
            inventoryAreaRegistration.RegisterArea(new AreaRegistrationContext(inventoryAreaRegistration.AreaName, RouteTable.Routes));
            RouteCollection routes = RouteTable.Routes;
            Wanamaker.WebApp.Global.RegisterRoutes(routes);
            
            // Act - process the route
            RouteData result = routes.GetRouteData(CreateHttpContext(url, httpMethod));
            
            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(TestIncomingRouteResult(result, controller, action, routeProperties));
        }

        private void TestRouteFail(string url, string httpMethod = "GET")
        {
            // Arrange
            RouteTable.Routes.Clear();
            var dashboardAreaRegistration = new DashboardAreaRegistration();
            dashboardAreaRegistration.RegisterArea(new AreaRegistrationContext(dashboardAreaRegistration.AreaName, RouteTable.Routes));


            RouteCollection routes = RouteTable.Routes;
            Wanamaker.WebApp.Global.RegisterRoutes(routes);
            
            // Act - process the route
            RouteData result = routes.GetRouteData(CreateHttpContext(url, httpMethod));
            
            // Assert
            Assert.IsTrue(result == null || result.Route == null);
        }



        [Test]
        public void TestIncomingRoutes()
        {
            TestRouteMatch("~/Dashboard.aspx", "Home", "IndexKO");
            TestRouteMatch("~/Dashboard.aspx/SearchMaxSE", "Home", "SearchMaxSE");
            TestRouteMatch("~/InventoryFilters.aspx/101090/Used", "Home", "BucketsData");
            TestRouteMatch("~/InventoryFilters.aspx/101091", "Home", "BucketsData");
            TestRouteMatch("~/InventoryMVC.aspx/SetChromeStyle", "Home", "SetChromeStyle");
            
            // New Car Pricing routes
            TestRouteMatch("~/InventorySearch.aspx/Years", "NewCarPricing", "GetSearchableYears");
            TestRouteMatch("~/InventorySearch.aspx/Makes", "NewCarPricing", "GetSearchableMakes");
            TestRouteMatch("~/InventorySearch.aspx/Models", "NewCarPricing", "GetSearchableModels");
            TestRouteMatch("~/InventorySearch.aspx/Trims", "NewCarPricing", "GetSearchableTrims");
            TestRouteMatch("~/InventorySearch.aspx/Bodys", "NewCarPricing", "GetSearchableBodys");
            TestRouteMatch("~/InventorySearch.aspx/Engines", "NewCarPricing", "GetSearchableEngines");
            TestRouteMatch("~/InventorySearch.aspx/Filter", "NewCarPricing", "GetSelectedVehicles");
            TestRouteMatch("~/NewCarPricing.aspx/Save", "NewCarPricing", "SavePricing");
            TestRouteMatch("~/NewCarPricing.aspx/Cancel", "NewCarPricing", "CancelCampaign");
            TestRouteMatch("~/NewCarPricing.aspx/ListCampaigns/123", "NewCarPricing", "ListCampaigns");
        }
    }
}
