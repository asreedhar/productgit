﻿using System.Collections.Generic;
using System.Web.UI;
using Moq;
using NUnit.Framework;
using Wanamaker.WebApp.Helpers;

namespace Wannamaker.WebApp.Tests.Helpers
{
    [TestFixture]
    public class HtmlHelpers
    {
        private Mock<IAttributeAccessor> CtrlMock;
        private IAttributeAccessor Ctrl;
        private Dictionary<string, string> Attributes;

        [SetUp]
        public void Setup()
        {
            Attributes = new Dictionary<string,string>();

            CtrlMock = new Mock<IAttributeAccessor>();
            CtrlMock
                .Setup(c => c.SetAttribute(It.IsAny<string>(), It.IsAny<string>()))
                .Callback<string, string>((k, v) => Attributes[k] = v);
            CtrlMock
                .Setup(c => c.GetAttribute(It.IsAny<string>()))
                .Returns<string>(k =>
                                     {
                                         string val;
                                         return Attributes.TryGetValue(k, out val) ? val : "";
                                     });
            Ctrl = CtrlMock.Object;
        }

        [TestCase("", "foo", "foo")]
        [TestCase("foo", "oof baz", "foo oof baz", Description = "Should append new classes to the end.")]
        [TestCase("foo", "foo", "foo", Description = "Should only add the class once.")]
        [TestCase("foo", "foo foo", "foo", Description = "Should only add the class once.")]
        [TestCase("", "foo foo", "foo", Description = "Should only add the class once.")]
        public void AddClass_should_add_classes_to_class_attribute(string startingAttributeValue, string classesToAdd, string expectedAttributeValue)
        {
            Ctrl.SetAttribute("class", startingAttributeValue);

            Ctrl.AddClass(classesToAdd);

            Assert.That(Ctrl.GetAttribute("class"), Is.EqualTo(expectedAttributeValue));
        }

        [TestCase("", "foo", "")]
        [TestCase("foo", "foo", "")]
        [TestCase("foo", "", "foo")]
        [TestCase("foo bar", "foo", "bar")]
        [TestCase("foo bar", "bar foo", "")]
        [TestCase("foo bar", "baz oof", "foo bar")]
        public void RemoveClass_should_remove_classes_from_class_attribute(string initialAttributeValue, string classesToRemove, string expectedAttributeValue)
        {
            Ctrl.SetAttribute("class", initialAttributeValue);

            Ctrl.RemoveClass(classesToRemove);

            Assert.That(Ctrl.GetAttribute("class"), Is.EqualTo(expectedAttributeValue));
        }

        [TestCase("", "foo", false)]
        [TestCase("foo", "foo", true)]
        [TestCase("bar", "foo", false)]
        [TestCase("baz bar", "baz", true)]
        [TestCase("baz bar", "bar", true)]
        [TestCase("baz bar", "foo", false)]
        public void HasClass_should_detect_presence_of_single_class_name(string initialAttributeValue, string classToDetect, bool expectedFound)
        {
            Ctrl.SetAttribute("class", initialAttributeValue);

            var found = Ctrl.HasClass(classToDetect);

            Assert.That(found, Is.EqualTo(expectedFound));
        }


        [TestCase("foo", "bar", true, "foo bar")]
        [TestCase("foo", "bar", false, "foo")]
        [TestCase("foo", "foo", false, "")]
        [TestCase("foo bar", "foo", false, "bar")]
        [TestCase("foo bar", "bar baz", true, "foo bar baz")]
        public void UpdateClass_should_remove_or_add_classes_depending_on_value_of_shouldAdd(string initialAttributeValue, 
            string classesToAddOrRemove, bool shouldAdd, string expectedAttributeValue)
        {
            Ctrl.SetAttribute("class", initialAttributeValue);

            Ctrl.UpdateClass(classesToAddOrRemove, shouldAdd);

            Assert.That(Ctrl.GetAttribute("class"), Is.EqualTo(expectedAttributeValue));
        }

        [TestCase("https://max.firstlook.biz/home", true)]
        [TestCase("https://betamax.firstlook.biz/home", true)]
        [TestCase("http://betamax.firstlook.biz/home", true)]
        [TestCase("http://max-b.int.firstlook.biz/home", true)]
        [TestCase("http://max-b.int.firstlook*biz/home", false)]
        [TestCase("http://blah/home", false)]
        [TestCase("http:/betamax.firstlook.biz/home", false)]
        [TestCase("http://betamax.firstlook.biz/homes", false)]
        [TestCase("betamax.firstlook.biz/homes", false)]
        public void FromRubyPageTests(string url, bool result)
        {
            Assert.IsTrue(Wanamaker.WebApp.Helpers.HtmlHelpers.IsRubyPageMatch(url) == result);
        }

    }
}