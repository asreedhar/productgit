﻿using System.Web.Mvc;
using Moq;
using NUnit.Framework;
using Wanamaker.WebApp.Areas.Diagnostics.Models;
using Wanamaker.WebApp.Areas.Diagnostics.Controllers;

namespace Wannamaker.WebApp.Tests.Areas.Diagnostics.Controllers
{
    [TestFixture]
    public class DiagnosticsControllerTest
    {
        [Test]
        public void IndexCoreTests()
        {
            var controller = new DiagnosticsController();
            var result = controller.Index(31) as ViewResult;
            var model = result.Model as DiagnosticsModel;

            Assert.AreEqual(model.MaxInitialEntryCount, 31, "Index's model should show changes to max entry limit parameter");

            result = controller.Index(101) as ViewResult;
            model = result.Model as DiagnosticsModel;

            Assert.AreEqual(model.MaxInitialEntryCount, 100, "Index's controller should clamp max entry count to 10 through 100");

            result = controller.Index(-1) as ViewResult;
            model = result.Model as DiagnosticsModel;

            Assert.AreEqual(model.MaxInitialEntryCount, 10, "Index's controller should clamp max entry count to 10 through 100");

            Assert.NotNull(model.StartDate, "StartDate should never be null");
            
            Assert.NotNull(model.Entries, "Entries should never be null");

        }
    }
}
