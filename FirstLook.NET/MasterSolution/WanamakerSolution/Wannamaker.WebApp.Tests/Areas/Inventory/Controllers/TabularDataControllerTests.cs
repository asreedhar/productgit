﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using MAX.Entities.Filters;
using Moq;
using NUnit.Framework;
using Wanamaker.WebApp.Areas.Inventory.Controllers;

namespace Wannamaker.WebApp.Tests.Areas.Inventory.Controllers
{
    [TestFixture]
    public class TabularDataControllerTests
    {
        private Mock<IDealerServices> _dealerServicesMock;
        private Mock<IInventoryDataRepository> _inventoryDataRepositoryMock;
        private IDealerServices _dealerServices;
        private IInventoryDataRepository _inventoryDataRepository;
        private Mock<IDealer> _dealerMock;
        private IDealer _dealer;

        private readonly IEnumerable<IInventoryData> _data = new[]
                                                        {
                                                            CreateInvData(10, stockNum: "A10"),
                                                            CreateInvData(20, stockNum: "A20"),
                                                            CreateInvData(30, stockNum: "A30")
                                                        };

        private Mock<IInventoryQueryBuilder> _inventoryQueryBuilderMock;
        private IInventoryQueryBuilder _inventoryQueryBuilder;
        private Func<IInventoryQueryBuilder> _inventoryQueryBuilderFactory;

        private static IInventoryData CreateInvData(int inventoryId, string vin = "", string stockNum = "")
        {
            var mock = new Mock<IInventoryData>();

            mock.Setup(d => d.InventoryID).Returns(inventoryId);
            mock.Setup(d => d.VIN).Returns(vin);
            mock.Setup(d => d.StockNumber).Returns(stockNum);

            return mock.Object;
        }

        [SetUp]
        public void Setup()
        {
            _dealerMock = new Mock<IDealer>();
            _dealer = _dealerMock.Object;
            _dealerMock.Setup(d => d.Id).Returns(42);

            _dealerServicesMock = new Mock<IDealerServices>();
            _dealerServices = _dealerServicesMock.Object;
            _dealerServicesMock.Setup(ds => ds.GetDealerByCode("windycit05")).Returns(_dealer);

            _inventoryDataRepositoryMock = new Mock<IInventoryDataRepository>();
            _inventoryDataRepository = _inventoryDataRepositoryMock.Object;
            _inventoryDataRepositoryMock.Setup(d => d.GetAllInventory(42)).Returns(_data);

            _inventoryQueryBuilderMock = new Mock<IInventoryQueryBuilder>();
            _inventoryQueryBuilder = _inventoryQueryBuilderMock.Object;
            _inventoryQueryBuilderFactory = () => _inventoryQueryBuilder;
        }

        [Test]
        public void GetInventoryCsv_with_contrived_query_builder_predicate_should_return_specific_inventory()
        {
            _inventoryQueryBuilderMock.Setup(qb => qb.ToPredicate()).Returns(() => d => d.InventoryID == 20); // Should only return InventoryIDs == 20

            var controller = new TabularDataController(_inventoryDataRepository, _dealerServices, _inventoryQueryBuilderFactory);
            var parameters = new TabularDataController.GetInventoryParameters
                                 {
                                     dealer = "windycit05",
                                 };

            var result = controller.GetInventoryCsv(parameters);

            Assert.That(result.Data.Select(d => d.Id).ToArray(), Is.EquivalentTo(new[] { 20 }));
        }

        [Test]
        public void GetInventoryCsv_with_default_parameters_should_not_set_any_query_parameters()
        {
            var controller = new TabularDataController(_inventoryDataRepository, _dealerServices, _inventoryQueryBuilderFactory);
            var parameters = new TabularDataController.GetInventoryParameters
            {
                dealer = "windycit05"
            };

            controller.GetInventoryCsv(parameters);

            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<WorkflowType>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<IWorkflow>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByNewOrUsed(It.IsAny<UsedOrNewFilter>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByAgeBucket(It.IsAny<int?>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByKeywords(It.IsAny<string>()), Times.Never());
        }

        [TestCase(WorkflowType.AllInventory)]
        [TestCase(WorkflowType.LowActivity)]
        public void GetInventoryCsv_with_filter_should_setup_query_with_FilterByWorkflow(WorkflowType type)
        {
            var controller = new TabularDataController(_inventoryDataRepository, _dealerServices, _inventoryQueryBuilderFactory);
            var parameters = new TabularDataController.GetInventoryParameters
            {
                dealer = "windycit05",
                filter = type

            };

            controller.GetInventoryCsv(parameters);

            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(type), Times.Once());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<IWorkflow>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByNewOrUsed(It.IsAny<UsedOrNewFilter>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByAgeBucket(It.IsAny<int?>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByKeywords(It.IsAny<string>()), Times.Never());
        }

        [TestCase(UsedOrNewFilter.New)]
        [TestCase(UsedOrNewFilter.Used)]
        [TestCase(UsedOrNewFilter.Used_and_New)]
        public void GetInventoryCsv_with_newused_should_setup_query_with_FilterByNewOrUsed(UsedOrNewFilter filter)
        {
            var controller = new TabularDataController(_inventoryDataRepository, _dealerServices, _inventoryQueryBuilderFactory);
            var parameters = new TabularDataController.GetInventoryParameters
            {
                dealer = "windycit05",
                newused = (int) filter
            };

            controller.GetInventoryCsv(parameters);

            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<WorkflowType>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<IWorkflow>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByNewOrUsed(filter), Times.Once());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByAgeBucket(It.IsAny<int?>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByKeywords(It.IsAny<string>()), Times.Never());
        }

        [TestCase(2)]
        [TestCase(4)]
        public void GetInventoryCsv_with_agebucket_should_setup_query_with_FilterByAgeBucket(int ageBucketId)
        {
            var controller = new TabularDataController(_inventoryDataRepository, _dealerServices, _inventoryQueryBuilderFactory);
            var parameters = new TabularDataController.GetInventoryParameters
            {
                dealer = "windycit05",
                agebucket = ageBucketId
            };

            controller.GetInventoryCsv(parameters);

            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<WorkflowType>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<IWorkflow>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByNewOrUsed(It.IsAny<UsedOrNewFilter>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByAgeBucket(ageBucketId), Times.Once());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByKeywords(It.IsAny<string>()), Times.Never());
        }

        [Test]
        public void GetInventoryCsv_with_search_should_setup_query_with_FilterByKeywords()
        {
            var controller = new TabularDataController(_inventoryDataRepository, _dealerServices, _inventoryQueryBuilderFactory);
            var parameters = new TabularDataController.GetInventoryParameters
            {
                dealer = "windycit05",
                search = "nifty motors"
            };

            controller.GetInventoryCsv(parameters);

            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<WorkflowType>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByWorkflow(It.IsAny<IWorkflow>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByNewOrUsed(It.IsAny<UsedOrNewFilter>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByAgeBucket(It.IsAny<int?>()), Times.Never());
            _inventoryQueryBuilderMock.Verify(qb => qb.FilterByKeywords("nifty motors"), Times.Once());
        }
    }
}