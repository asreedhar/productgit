﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;
using Moq;
using NUnit.Framework;
using Wanamaker.WebApp.Areas.Inventory.Controllers;
using Wanamaker.WebApp.Areas.Inventory.ViewModels;

namespace Wannamaker.WebApp.Tests.Areas.Inventory.Controllers
{
    [TestFixture]
    public class NewCarPricingControllerTest
    {
        private Mock<IInventorySearch> _inventorySearch;
        private Mock<IDiscountPricingCampaignManager> _mockDiscountPricingCampaignManager;

        [SetUp]
        public void Setup()
        {
            _inventorySearch = new Mock<IInventorySearch>();
            _inventorySearch.SetupGet(x => x.InventoryList).Returns(FakeInventoryDatas().ToList());
            _inventorySearch.Setup(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>())).Returns(FakeInventoryDatas());

            _mockDiscountPricingCampaignManager = new Mock<IDiscountPricingCampaignManager>();
        }

        [Test]
        public void GetSearchableYears_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            var yearsList =(NewCarPricingOptions) controller.GetSearchableYears(1).Data;
            // No selected inventoryIds
            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            Assert.That(yearsList.Options.Count == 6);
        }

        [Test]
        public void GetSearchableMakes_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            var cf = new CampaignFilter
            {
                Year = new List<string>
                {
                    CampaignFilter.Allyears
                }
            };

            var response = controller.GetSearchableMakes(1, cf);
            Assert.IsInstanceOf<NewCarPricingOptions>(response.Data);

            var data = (NewCarPricingOptions) response.Data;

            // We are not using an actual campaign filter, so the controller should return a list of the distinct Makes. 
            Assert.That(data.Options.Count == 3);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _inventorySearch.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
        }

        [Test]
        public void GetSearchableModels_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            var cf = new CampaignFilter
            {
                Year = new List<string>
                {
                    CampaignFilter.Allyears
                }
            };

            var response = controller.GetSearchableModels(1, cf);
            Assert.IsInstanceOf<NewCarPricingOptions>(response.Data);

            var data = (NewCarPricingOptions)response.Data;
            // We are not using an actual campaign filter, so the controller should return a list of the distinct Models. 
            Assert.That(data.Options.Count == 3);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _inventorySearch.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
        }

        [Test]
        public void GetSearchableTrims_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            var cf = new CampaignFilter
            {
                Year = new List<string>
                {
                    CampaignFilter.Allyears
                }
            };

            var response = controller.GetSearchableTrims(1, cf);
            Assert.IsInstanceOf<NewCarPricingOptions>(response.Data);

            var data = (NewCarPricingOptions)response.Data;
            // We are not using an actual campaign filter, so the controller should return a list of the distinct Trim. 
            Assert.That(data.Options.Count == 6);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _inventorySearch.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
        }

        [Test]
        public void GetSearchableBodys_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            var cf = new CampaignFilter
            {
                Year = new List<string>
                {
                    CampaignFilter.Allyears
                }
            };

            var response = controller.GetSearchableBodys(1, cf);
            Assert.IsInstanceOf<NewCarPricingOptions>(response.Data);

            var data = (NewCarPricingOptions)response.Data;
            // We are not using an actual campaign filter, so the controller should return a list of the distinct bodys. 
            Assert.That(data.Options.Count == 6);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _inventorySearch.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
        }

        [Test]
        public void GetSearchableEngines_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            var cf = new CampaignFilter
            {
                Year = new List<string>
                {
                    CampaignFilter.Allyears
                }
            };

            var response = controller.GetSearchableEngines(1, cf);
            Assert.IsInstanceOf<NewCarPricingOptions>(response.Data);

            var data = (NewCarPricingOptions)response.Data;
            // We are not using an actual campaign filter, so the controller should return a list of the distinct engines. 
            Assert.That(data.Options.Count == 2);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _inventorySearch.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
        }
        
        
        [Test]
        public void GetSelectedVehicles_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            var cf = new CampaignFilter
            {
                Year = new List<string>
                {
                    CampaignFilter.Allyears
                }
            };

            var response = controller.GetSelectedVehicles(1, cf);
            Assert.IsInstanceOf<NewCarPricingOptions>(response.Data);

            var data = (NewCarPricingOptions)response.Data;

            // We are not using an actual campaign filter, so the controller should return a list of the distinct engines. 
            Assert.That(data.InventoryIds.Count == 6);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _inventorySearch.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
        }

        [Test]
        public void CancelCampaign_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            _mockDiscountPricingCampaignManager.Setup(x => x.FetchCampaign(It.IsAny<int>(), It.IsAny<int>())).Returns(new DiscountPricingCampaignModel());
            _mockDiscountPricingCampaignManager.Setup(x => x.ExpireDiscountCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<string>()));
            
            controller.CancelCampaign(1, 1, "test");

            _mockDiscountPricingCampaignManager.Verify(x => x.FetchCampaign(It.IsAny<int>(), It.IsAny<int>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.ExpireDiscountCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void CancelCampaign_No_User_Throws_Exception()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);

            _mockDiscountPricingCampaignManager.Setup(x => x.FetchCampaign(It.IsAny<int>(), It.IsAny<int>())).Returns(new DiscountPricingCampaignModel());
            _mockDiscountPricingCampaignManager.Setup(x => x.ExpireDiscountCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<string>()));

            // No user provided, and no HTTPContext when running from tests, throw an exception.
            controller.CancelCampaign(1, 1);

            _mockDiscountPricingCampaignManager.Verify(x => x.FetchCampaign(It.IsAny<int>(), It.IsAny<int>()), Times.Never());
            _mockDiscountPricingCampaignManager.Verify(x => x.ExpireDiscountCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<string>()), Times.Never());
        }

        [Test]
        public void SavePricing_Test()
        {
            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            _mockDiscountPricingCampaignManager
                .Setup(x => x.SaveCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<IEnumerable<int>>()))
                .Returns(new DiscountPricingCampaignModel{CampaignId =  1});

            controller.SavePricing(1, new NewCarPricingModel
            {
                Year = new List<string>{"2013"},
                BodyStyle = new List<string>{"body style 1"},
                DiscountAmount = 100,
                EngineDescription = new List<CampaignEngineDescriptionInfo>
                {
                    new CampaignEngineDescriptionInfo {Description = "Engine Description"}
                },
                ExpirationDate = DateTime.Now.AddDays(10).Date.ToShortDateString(),
                InventoryIds = new List<int>{1,2,3},
                Make = new List<string>{"Make 1"},
                Model = new List<string>{"Model 1"},
                PriceBaseLine = PriceBaseLine.Msrp,
                RebateAmount = 10,
                Trim = new List<string>{"Trim 1"}
            }, "test");

            _mockDiscountPricingCampaignManager.Verify(x => x.SaveCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<IEnumerable<int>>()), Times.Once());
        }

        [Test]
        public void OldCampaignsStillWork()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                OldDiscountPricingCampaignModel()
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var response = controller.GetSelectedVehicles(1, new CampaignFilter
            {
                Year = new List<string>
                {
                    CampaignFilter.Allyears
                },
                EngineDescription = new List<string> {"Engine Option1"}
            });
            Assert.IsInstanceOf<NewCarPricingOptions>(response.Data);

            var data = (NewCarPricingOptions)response.Data;

            // We are not using an actual campaign filter, so the controller should return a list of the distinct engines. 
            Assert.That(data.InventoryIds.Count == 6);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _inventorySearch.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
        }

        [Test]
        public void ListCampaigns_Test()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                DiscountModel()
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var jsonResult = controller.ListCampaigns(1);

            Assert.IsInstanceOf < List < PricingCampaignViewModel > > (jsonResult.Data);

            var data = (List<PricingCampaignViewModel>)jsonResult.Data;

            Assert.That(data.Count == 1);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void ListCampaigns_Null_CampaignEngineDescriptionInfo()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                NullCampaignFilterValuesDiscountModel(new CampaignFilter
                {
                    BodyStyle = new List<string> { "body style 1" },
                    CampaignEngineDescription = new List<CampaignEngineDescriptionInfo> { null },
                    Make = new List<string> { "Make-1" },
                    Model = new List<string> { "Model-1" },
                    Trim = new List<string> { "Trim-1" },
                    VehicleType = VehicleType.New,
                    Year = new List<string> { CampaignFilter.Allyears }
                })
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var jsonResult = controller.ListCampaigns(1);

            Assert.IsInstanceOf<List<PricingCampaignViewModel>>(jsonResult.Data);

            var data = (List<PricingCampaignViewModel>)jsonResult.Data;

            Assert.That(data.Count == 1);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void ListCampaigns_Null_BodyStyle()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                NullCampaignFilterValuesDiscountModel(new CampaignFilter
                {
                    BodyStyle = new List<string> { null },
                    CampaignEngineDescription = new List<CampaignEngineDescriptionInfo> { new CampaignEngineDescriptionInfo { Description = "Engine Option1" } },
                    Make = new List<string> { "Make-1" },
                    Model = new List<string> { "Model-1" },
                    Trim = new List<string> { "Trim-1" },
                    VehicleType = VehicleType.New,
                    Year = new List<string> { CampaignFilter.Allyears }
                })
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var jsonResult = controller.ListCampaigns(1);

            Assert.IsInstanceOf<List<PricingCampaignViewModel>>(jsonResult.Data);

            var data = (List<PricingCampaignViewModel>)jsonResult.Data;

            Assert.That(data.Count == 1);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void ListCampaigns_Null_Make()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                NullCampaignFilterValuesDiscountModel(new CampaignFilter
                {
                    BodyStyle = new List<string> { "body style 1" },
                    CampaignEngineDescription = new List<CampaignEngineDescriptionInfo> { new CampaignEngineDescriptionInfo { Description = "Engine Option1" } },
                    Make = new List<string> { null },
                    Model = new List<string> { "Model-1" },
                    Trim = new List<string> { "Trim-1" },
                    VehicleType = VehicleType.New,
                    Year = new List<string> { CampaignFilter.Allyears }
                })
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var jsonResult = controller.ListCampaigns(1);

            Assert.IsInstanceOf<List<PricingCampaignViewModel>>(jsonResult.Data);

            var data = (List<PricingCampaignViewModel>)jsonResult.Data;

            Assert.That(data.Count == 1);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void ListCampaigns_Null_Model()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                NullCampaignFilterValuesDiscountModel(new CampaignFilter
                {
                    BodyStyle = new List<string> { "body style 1" },
                    CampaignEngineDescription = new List<CampaignEngineDescriptionInfo> { new CampaignEngineDescriptionInfo { Description = "Engine Option1" } },
                    Make = new List<string> { "Make-1" },
                    Model = new List<string> { null },
                    Trim = new List<string> { "Trim-1" },
                    VehicleType = VehicleType.New,
                    Year = new List<string> { CampaignFilter.Allyears }
                })
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var jsonResult = controller.ListCampaigns(1);

            Assert.IsInstanceOf<List<PricingCampaignViewModel>>(jsonResult.Data);

            var data = (List<PricingCampaignViewModel>)jsonResult.Data;

            Assert.That(data.Count == 1);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void ListCampaigns_Null_Trim()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                NullCampaignFilterValuesDiscountModel(new CampaignFilter
                {
                    BodyStyle = new List<string> { "body style 1" },
                    CampaignEngineDescription = new List<CampaignEngineDescriptionInfo> { new CampaignEngineDescriptionInfo { Description = "Engine Option1" } },
                    Make = new List<string> { "Make-1" },
                    Model = new List<string> { "Model-1" },
                    Trim = new List<string> { null },
                    VehicleType = VehicleType.New,
                    Year = new List<string> { CampaignFilter.Allyears }
                })
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var jsonResult = controller.ListCampaigns(1);

            Assert.IsInstanceOf<List<PricingCampaignViewModel>>(jsonResult.Data);

            var data = (List<PricingCampaignViewModel>)jsonResult.Data;

            Assert.That(data.Count == 1);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void ListCampaigns_Null_Year()
        {
            _mockDiscountPricingCampaignManager.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(new List<DiscountPricingCampaignModel>
            {
                NullCampaignFilterValuesDiscountModel(new CampaignFilter
                {
                    BodyStyle = new List<string> { "body style 1" },
                    CampaignEngineDescription = new List<CampaignEngineDescriptionInfo> { new CampaignEngineDescriptionInfo { Description = "Engine Option1" } },
                    Make = new List<string> { "Make-1" },
                    Model = new List<string> { "Model-1" },
                    Trim = new List<string> { "Trim-1" },
                    VehicleType = VehicleType.New,
                    Year = new List<string> { null }
                })
            });

            var controller = new NewCarPricingController(_inventorySearch.Object, _mockDiscountPricingCampaignManager.Object);
            var jsonResult = controller.ListCampaigns(1);

            Assert.IsInstanceOf<List<PricingCampaignViewModel>>(jsonResult.Data);

            var data = (List<PricingCampaignViewModel>)jsonResult.Data;

            Assert.That(data.Count == 1);

            _inventorySearch.Verify(x => x.Initialize(It.IsAny<int>(), It.IsAny<VehicleType>()), Times.Once());
            _mockDiscountPricingCampaignManager.Verify(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>()), Times.Once());
        }

        #region privates


        private static DiscountPricingCampaignModel OldDiscountPricingCampaignModel()
        {
            return new DiscountPricingCampaignModel
            {
                BusinessUnitId = 1,
                CampaignFilter = new CampaignFilter
                {
                    BodyStyle = new List<string> { "body style 1" },
                    EngineDescription = new List<string> { "Engine Option1" },
                    Make = new List<string> { "Make-1" },
                    Model = new List<string> { "Model-1" },
                    Trim = new List<string> { "Trim-1" },
                    VehicleType = VehicleType.New,
                    Year = new List<string> { CampaignFilter.Allyears }
                },
                CampaignId = 1,
                CreatedBy = "Test user",
                CreationDate = DateTime.Now,
                InventoryList = new List<InventoryDiscountPricingModel>
                {
                    new InventoryDiscountPricingModel {ActivatedBy = "Test user", CampaignId = 1, Active = true, InventoryId = 1},
                    new InventoryDiscountPricingModel {ActivatedBy = "Test user", CampaignId = 1, Active = true, InventoryId = 2}
                },
                DiscountType = PriceBaseLine.Invoice,
                ExpirationDate = DateTime.Now.AddDays(10),
                DealerDiscount = 100,
                HasBeenExpired = false,
                ManufacturerRebate = 0
            };
        }



        private static DiscountPricingCampaignModel DiscountModel()
        {
            return new DiscountPricingCampaignModel
            {
                BusinessUnitId = 1,
                CampaignFilter = new CampaignFilter
                {
                    BodyStyle = new List<string> {"body style 1"},
                    CampaignEngineDescription = new List<CampaignEngineDescriptionInfo>{ new CampaignEngineDescriptionInfo { Description = "Engine Option1" }},
                    Make = new List<string> {"Make-1"},
                    Model = new List<string> {"Model-1"},
                    Trim = new List<string> {"Trim-1"},
                    VehicleType = VehicleType.New,
                    Year = new List<string> {CampaignFilter.Allyears}
                },
                CampaignId = 1,
                CreatedBy = "Test user",
                CreationDate = DateTime.Now,
                InventoryList = new List<InventoryDiscountPricingModel>
                {
                    new InventoryDiscountPricingModel {ActivatedBy = "Test user", CampaignId = 1, Active = true, InventoryId = 1},
                    new InventoryDiscountPricingModel {ActivatedBy = "Test user", CampaignId = 1, Active = true, InventoryId = 2}
                },
                DiscountType = PriceBaseLine.Invoice,
                ExpirationDate = DateTime.Now.AddDays(10),
                DealerDiscount = 100,
                HasBeenExpired = false,
                ManufacturerRebate = 0
            };
        }

        private static DiscountPricingCampaignModel NullCampaignFilterValuesDiscountModel(CampaignFilter filter)
        {
            return new DiscountPricingCampaignModel
            {
                BusinessUnitId = 1,
                CampaignFilter = filter,
                CampaignId = 1,
                CreatedBy = "Test user",
                CreationDate = DateTime.Now,
                InventoryList = new List<InventoryDiscountPricingModel>
                {
                    new InventoryDiscountPricingModel {ActivatedBy = "Test user", CampaignId = 1, Active = true, InventoryId = 1},
                    new InventoryDiscountPricingModel {ActivatedBy = "Test user", CampaignId = 1, Active = true, InventoryId = 2}
                },
                DiscountType = PriceBaseLine.Invoice,
                ExpirationDate = DateTime.Now.AddDays(10),
                DealerDiscount = 100,
                HasBeenExpired = false,
                ManufacturerRebate = 0
            };
        }

        private static IEnumerable<IInventoryData> FakeInventoryDatas()
        {
            return new List<IInventoryData>
            {
                new InventoryData("stock-11", 1, "vin-1", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Make-1", "Model-1", "MarketClass-1", 1, "Trim-1", "", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 1","Engine Option1", null, true),
                new InventoryData("stock-21", 1, "vin-2", 1, false, "", null,  1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-2", "", 2, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 2","Engine Option1", null, true),
                new InventoryData("stock-31", 1, "vin-3", 1, false, "", null,  1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2011", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-3", "", 3, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 3","Engine Option1", null, true),
                new InventoryData("stock-12", 1, "vin-4", 1, false, "", null,  1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2010", "Make-1", "Model-2", "MarketClass-1", 1, "Trim-4", "afterMarketTrim-1", 4, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 4","Engine Option2", "eo2"),
                new InventoryData("stock-22", 1, "vin-5", 1, false, "", null,  1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2009", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-5", "", 5, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 5","Engine Option2", "eo2"),
                new InventoryData("stock-32", 1, "vin-6", 1, false, "", null,  1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2008", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-6", "", 6, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 6","Engine Option2", "eo2")
            };
        }
        #endregion privates
    }
}