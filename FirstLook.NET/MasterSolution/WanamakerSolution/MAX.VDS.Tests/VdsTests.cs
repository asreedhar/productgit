﻿using System;
using System.Collections.Generic;
using System.Linq;
using MAX.VDS.DataAccess;
using MAX.VDS.Parsing;
using Moq;
using NUnit.Framework;

namespace MAX.VDS.Tests
{
    [TestFixture]
    public class VdsTests
    {
        [Test]
        public void ParseOptionCodes_BmwOptionCodeWithLeadingZeroFixed()
        {
            var record = Mock.Of<IVehicleRecord>();
            record.ChromeStyleId = 1;
            record.Engine = "e";
            record.EngineCode = "e1";
            record.LoadDate = DateTime.Now;
            record.Make = "bmw";
            record.Model = "335";
            record.ModelCode = "F30";
            record.ModelYear = 2104;
            record.Transmission = "8 speed";
            record.TransmissionCode = "8AT";
            record.Trim = "d";
            record.VIN = "12345678901234567";

            record.OptionCodes = "0123,0456,789";

            var parser = new VdsRecordParser();
            var parsedOptions = parser.ParseOptionCodes("bmw", record.OptionCodes).ToList();
            Assert.AreEqual(3, parsedOptions.Count());
            Assert.AreNotEqual(record.OptionCodes, parsedOptions);

            var expected = new List<string> {"123", "456", "789"};
            Assert.IsTrue(expected.Intersect(parsedOptions).Count() == 3);
        }

        [Test]
        public void ParseOptionCodes_BmwOptionCodesExample_5UXWX5C57BL714847()
        {
            const string VIN = "5UXWX5C57BL714847";
            IVdsRepository repo = new VdsRepository();
            var vdsRecords = repo.GetVehicle(VIN).ToList();

            Assert.That(vdsRecords.Count() == 1);

            var optionCodes = vdsRecords.First().OptionCodes.Split(',');
            foreach (var code in optionCodes)
            {
                Assert.That(code.StartsWith("0"));                
            }

            // Now parse the record
            var parser = new VdsRecordParser();
            var parsedOptions = parser.ParseOptionCodes("bmw", optionCodes).ToList();

            Console.WriteLine("Parsed option codes: " + string.Join(",", parsedOptions));
            Assert.That(!parsedOptions.Any(o => o.StartsWith("0")));
        }

        [Test]
        public void Facade_HondaOptionCodesExample_JHLRE383X8C040334()
        {
            const string VIN = "JHLRE383X8C040334";
            var facade = new VdsDataFacade();
            var data = facade.GetChromeData(VIN);

            Assert.IsNotNull(data);
            Console.WriteLine("Country code: " + data.CountryCode);
            Console.WriteLine("Style Id: " + data.ChromeStyleId);
            Console.WriteLine("Option codes: " + string.Join(",", data.OptionCodes));

            Assert.AreEqual(1, data.CountryCode);
            Assert.AreEqual(293648, data.ChromeStyleId);
            Assert.AreEqual(2, data.OptionCodes.Count());
        }

        [Test]
        public void Facade_MockRepository_HappyPath()
        {
            var v = new VehicleRecord
            {
                ChromeStyleId = 1,
                Engine = "V8",
                EngineCode = "123",
                Make = "chevy",
                Model = "silverado",
                ModelCode = "100",
                ModelYear = 2014,
                OptionCodes = "a,b,c",
                Transmission = "A/T",
                TransmissionCode = "456",
                Trim = "t",
                VIN = "abc123"
            };

            var vm1 = new VehicleMasterRecord {AuditId = 1, ChromeStyleId = 1, OptionCodes = "d,e,f", VIN = "abc123"};

            var repoMock = new Mock<IVdsRepository>();
            repoMock.Setup(r => r.GetVehicle(It.IsAny<string>())).Returns( new List<IVehicleRecord>{v});
            repoMock.Setup(r => r.GetVehicleMaster(It.IsAny<string>())).Returns(new List<IVehicleMasterRecord> { vm1 });

            var facade = new VdsDataFacade(repoMock.Object);
            var data = facade.GetChromeData("abc123");

            Assert.IsNotNull(data);
            Assert.AreEqual(v.ChromeStyleId, data.ChromeStyleId);
            Assert.AreEqual(1, data.CountryCode);
            Assert.AreEqual(8, data.OptionCodes.Count());
            Assert.IsTrue(data.OptionCodes.Contains("a"));
            Assert.IsTrue(data.OptionCodes.Contains("b"));
            Assert.IsTrue(data.OptionCodes.Contains("c"));
            Assert.IsTrue(data.OptionCodes.Contains("d"));
            Assert.IsTrue(data.OptionCodes.Contains("e"));
            Assert.IsTrue(data.OptionCodes.Contains("f"));
            Assert.IsTrue(data.OptionCodes.Contains("123"));
            Assert.IsTrue(data.OptionCodes.Contains("456"));
        }

        [Test]
        public void Facade_MockRepository_ConflictingStylesOnMasterRecords()
        {
            var v = new VehicleRecord
            {
                Engine = "V8",
                EngineCode = "123",
                Make = "chevy",
                Model = "silverado",
                ModelCode = "100",
                ModelYear = 2014,
                OptionCodes = "a,b,c",
                Transmission = "A/T",
                TransmissionCode = "456",
                Trim = "t",
                VIN = "abc123"
            };

            var vm1 = new VehicleMasterRecord { AuditId = 1, ChromeStyleId = 1, OptionCodes = "d,e,f", VIN = "abc123" };
            var vm2 = new VehicleMasterRecord { AuditId = 1, ChromeStyleId = 2, OptionCodes = "d,e,f", VIN = "abc123" };

            var repoMock = new Mock<IVdsRepository>();
            repoMock.Setup(r => r.GetVehicle(It.IsAny<string>())).Returns(new List<IVehicleRecord> { v });
            repoMock.Setup(r => r.GetVehicleMaster(It.IsAny<string>())).Returns(new List<IVehicleMasterRecord> { vm1, vm2 });

            var facade = new VdsDataFacade(repoMock.Object);
            var data = facade.GetChromeData("abc123");

            Assert.IsNotNull(data);
            Assert.IsNull(data.ChromeStyleId);
            Assert.IsNull(data.CountryCode);
            Assert.IsNull(data.OptionCodes);
        }

        [Test]
        public void Facade_MockRepository_SameStylesOnMasterRecords()
        {
            var v = new VehicleRecord
            {
                Engine = "V8",
                EngineCode = "123",
                Make = "chevy",
                Model = "silverado",
                ModelCode = "100",
                ModelYear = 2014,
                OptionCodes = "a,b,c",
                Transmission = "A/T",
                TransmissionCode = "456",
                Trim = "t",
                VIN = "abc123"
            };

            var vm1 = new VehicleMasterRecord { AuditId = 1, ChromeStyleId = 2, OptionCodes = "d,e,f", VIN = "abc123", EngineCode = "21", TransmissionCode="12"};
            var vm2 = new VehicleMasterRecord { AuditId = 1, ChromeStyleId = 2, OptionCodes = "g", VIN = "abc123", EngineCode = "31", TransmissionCode = "13"};

            var repoMock = new Mock<IVdsRepository>();
            repoMock.Setup(r => r.GetVehicle(It.IsAny<string>())).Returns(new List<IVehicleRecord> { v });
            repoMock.Setup(r => r.GetVehicleMaster(It.IsAny<string>())).Returns(new List<IVehicleMasterRecord> { vm1, vm2 });

            var facade = new VdsDataFacade(repoMock.Object);
            var data = facade.GetChromeData("abc123");

            Assert.IsNotNull(data);
            Assert.AreEqual(2, data.ChromeStyleId);
            Assert.AreEqual(1, data.CountryCode);
            Assert.AreEqual(13, data.OptionCodes.Count());
            Assert.IsTrue(data.OptionCodes.Contains("a"));
            Assert.IsTrue(data.OptionCodes.Contains("b"));
            Assert.IsTrue(data.OptionCodes.Contains("c"));
            Assert.IsTrue(data.OptionCodes.Contains("d"));
            Assert.IsTrue(data.OptionCodes.Contains("e"));
            Assert.IsTrue(data.OptionCodes.Contains("f"));
            Assert.IsTrue(data.OptionCodes.Contains("g"));
            Assert.IsTrue(data.OptionCodes.Contains("123"));
            Assert.IsTrue(data.OptionCodes.Contains("456"));
            Assert.IsTrue(data.OptionCodes.Contains("21"));
            Assert.IsTrue(data.OptionCodes.Contains("12"));
            Assert.IsTrue(data.OptionCodes.Contains("31"));
            Assert.IsTrue(data.OptionCodes.Contains("13"));
        }
    }
}
