﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using AutoLoad.Services;
using MAX.Entities.Enumerations;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;
using Quartz;

namespace AutoLoad
{
    /// <summary>
    /// Responsible for creating FordDirectAutoLoadMessages. Creates one message for each dealer+year+make+model.
    /// These messages are created on a schedule to trigger downstream internal autoloads.
    /// </summary>
    public class FordDirectAutoLoadJob :  IStatefulJob
    {
        private readonly IQueueFactory _queueFactory;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly MerchandisingRepository _merchandisingRepository;

        public FordDirectAutoLoadJob(IQueueFactory queueFactory)
        {
            _queueFactory = queueFactory;
            _merchandisingRepository = new MerchandisingRepository();
        }

        public void Execute(JobExecutionContext context)
        {
            Log.Info("FordDirectAutoLoadJob starting run.");
            var thisYear = DateTime.UtcNow.Year;
            var dealerCodes = _merchandisingRepository.GetDealerCodes().ToList();
            var fordService = new FordWebService(new List<Make> {Make.Ford, Make.Lincoln},
                new List<Int32> {thisYear - 1, thisYear, thisYear + 1});
            var yearMakeModels = fordService.YearMakeModels;

            foreach (var dealerCode in dealerCodes)
            {
                try
                {
                    ProcessDealer(yearMakeModels, dealerCode);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Error encountered processing dealer: {0}. Moving on to next dealer. Exception: {1}", 
                        dealerCode.BusinessUnitId, ex);
                }
            }

            Log.Info("FordDirectAutoLoadJob finished run.");
        }

        private void ProcessDealer(List<YearMakeModel> yearMakeModels, DealerCode dealerCode)
        {
            foreach (var ymm in yearMakeModels)
            {
                try
                {
                    IssueMessage(dealerCode, ymm);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Error encountered issuing FordDirectAutoLoadMessage for dealer: {0}, ymm: {1}. Exception: {2}",
                                    dealerCode.BusinessUnitId,
                                    ymm, ex);
                }
            }
        }

        private void IssueMessage(DealerCode dealerCode, YearMakeModel ymm)
        {
            var message = new FordDirectAutoLoadMessage
                {
                    BusinessUnitId = dealerCode.BusinessUnitId,
                    DealerPaCode = dealerCode.DealerPaCode,
                    Year = ymm.Year,
                    Make = ymm.Make,
                    Model = ymm.Model
                };

            // put the request on the queue.
            var queue = _queueFactory.CreateFordDirectAutoLoadMessageQueue();
            queue.SendMessage(message, GetType().FullName);
        }
    }
}
