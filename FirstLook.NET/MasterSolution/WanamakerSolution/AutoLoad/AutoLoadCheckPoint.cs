﻿namespace AutoLoad
{
    /// <summary>
    /// Stores the state of the system at the last autoload job run. The things we care about are: which dealers have 
    /// autoload enabled, and what was the max (relevant) inventory id for each dealer?
    /// </summary>
    internal class AutoLoadCheckpoint
    {
        public AutoLoadDealerCheckpoint[] Checkpoints { get; set; }
    }

    internal class AutoLoadDealerCheckpoint
    {
        public int BusinessUnitId { get; set; }
        public int MaxInventoryId { get; set; }
    }
}
