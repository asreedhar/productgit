﻿using System;
using AutoLoad.FordNew.Domain;
using MAX.Threading;
using Merchandising.Messages;

namespace AutoLoad
{
    /// <summary>
    /// A marker interface.
    /// </summary>
    public interface IFordDirectAutoLoadThreadStarter : IRunWithCancellation 
    {}

    public class FordDirectAutoLoadThreadStarter : IFordDirectAutoLoadThreadStarter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IQueueFactory _queueFactory;
        private readonly IFileStoreFactory _fileStoreFactory;

        public FordDirectAutoLoadThreadStarter(IQueueFactory queueFactory, IFileStoreFactory fileStoreFactory)
        {
            _queueFactory = queueFactory;
            _fileStoreFactory = fileStoreFactory;
        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhenNoMessages )
        {
            Log.Info("Ford direct threads starting.");

            NamedBackgroundThread.Start("Worker - FordDirectAutoLoadMessageProcessor", 
                () => new FordDirectAutoLoadMessageProcessor(_queueFactory, _fileStoreFactory).Run(shouldCancel, blockWhenNoMessages));
        }
    }
}
