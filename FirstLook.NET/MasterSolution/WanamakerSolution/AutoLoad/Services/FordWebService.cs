﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Serialization;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.Entities.Enumerations;
using AutoLoad.Models;
using AutoLoad.FordNew.DataAccess;

namespace AutoLoad.Services
{
    /// <summary>
    /// Service for getting known vehicle models from ford direct's web API
    /// </summary>
    public class FordWebService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Uri _modelVersionsUri = new Uri("http://services.forddirect.fordvehicles.com/products/ModelVersions");

        /// <summary>
        /// Makes to filter results on
        /// </summary>
        private IEnumerable<Make> Makes { get; set; }
        /// <summary>
        /// Vehicle model years to limit request scope to
        /// </summary>
        private IEnumerable<Int32> Years { get; set; }
        /// <summary>
        /// Mockable web request
        /// </summary>
        private WebRequest _request;
        /// <summary>
        /// Persist results on this object
        /// </summary>
        private List<YearMakeModel> _store;
        /// <summary>
        /// Call the API/get results
        /// </summary>
        public List<YearMakeModel> YearMakeModels {
            get
            {
                if (_store == null)
                {
                    _store = GetYearMakeModels();
                }
                return _store;
            }
        }

        /// <param name="makes">Makes to filter responses on</param>
        /// <param name="years">Years to request from the API (one WebRequest per year)</param>
        public FordWebService(IEnumerable<Make> makes, IEnumerable<int> years)
        {
            Makes = makes;
            Years = years.Distinct();
        }

        /// <summary>
        /// For mocks/unit tests
        /// </summary>
        /// <param name="makes">Makes to filter responses on</param>
        /// <param name="years">Years to request from the API (one WebRequest per year)</param>
        /// <param name="request">mockable webRequest</param>
        public FordWebService(IEnumerable<Make> makes, IEnumerable<int> years, WebRequest request)
        {
            Makes = makes;
            Years = years.Distinct();
            _request = request;
        }
        /// <summary>
        /// Get a list of known year make models from ford direct's web API. One webrequest is made per year listed.
        /// </summary>
        /// <param name="makes">makes to filter results by</param>
        /// <param name="years">years to get all ymms by</param>
        /// <returns>non-null list of YearMakeModel</returns>
        private List<YearMakeModel> GetYearMakeModels()
        {
            var ymmList = new List<YearMakeModel>();
            var responses = new List<ModelVersionsReponse>();

            // request transcript per individual year since API doesn't seem to like a csv list of refinements
            //
            foreach (var year in Years) { // foolproof!
                var response = GetMvReponse(year);
                if (response != null) {
                    responses.Add(response);
                } else {
                    Log.TraceFormat("Response for year {0} was null.", year);
                }
            }

            // filter and join each year query response
            //
            foreach (var response in responses) {
                if (response.ModelVersions != null) {
                    if (response.ModelVersions.Count > 0) {
                        Log.TraceFormat("Deserialized ModelVersions. Found {0} Entries.",
                            response.ModelVersions.Count);

                        // filter results by make since API doesn't seem to like a csv list of refinements
                        //
                        foreach (var modelVersion in response.ModelVersions) {
                            var make = (Make) Enum.Parse(typeof (Make), modelVersion.Make);

                            if (Enum.IsDefined(typeof (Make), make)) {
                                if (Makes.Contains(make)) {
                                    var ymm = new YearMakeModel(modelVersion.Year, modelVersion.Make, modelVersion.Model);
                                    if (!ymmList.Contains(ymm)) {
                                        ymmList.Add(ymm);
                                        Log.TraceFormat("Adding {0} {1} {2}.",ymm.Year, ymm.Make, ymm.Model);
                                    }
                                } else {
                                    Log.TraceFormat("Make [{0}] wasn't in the list of expected makes. Skipping.", modelVersion.Make);
                                }
                            } else {
                                Log.TraceFormat("Make [{0}] wasn't found in Make enum. Skipping.", modelVersion.Make);
                            }
                        }
                    } else {
                        Log.TraceFormat("ModelVersions count was 0. No Entries.");
                    }
                } else {
                    Log.TraceFormat("ModelVersions was null. No Entries found.");
                }
            }
            return ymmList;
        }

        /// <summary>
        /// Builds url and makes webrequest to ford direct's web API
        /// </summary>
        /// <param name="year">year to make webrequest for</param>
        /// <returns>ModelVersionResponse or null</returns>
        private ModelVersionsReponse GetMvReponse(Int32 year)
        {
            ModelVersionsReponse response = null;
            
            var builder = new UriBuilder(_modelVersionsUri);
            builder.Query = "year=" + year;

            var webRequest = _request ?? WebRequest.Create(builder.Uri); // mock or make

            Log.TraceFormat("Requesting MakeModels from {0}.", builder.Uri);

            using (var webResponse = webRequest.GetResponse())
            {
                using (var responseStream = webResponse.GetResponseStream())
                {
                    var ymmXml = new StreamReader(responseStream).ReadToEnd();
                    Log.TraceFormat("Reading data from fordvehicles. Response Length: {0}", ymmXml.Length);
                    var ymmStream = new MemoryStream(Encoding.UTF8.GetBytes(ymmXml));
                    
                    try
                    {
                        response = DeserializeXmlStreamToObject<ModelVersionsReponse>(ymmStream);
                    } catch (InvalidOperationException xmlEx) {
                        ymmStream.Position = 0;
                        Log.ErrorFormat("Response from fordDirect's ModelVersion API is returning unexpected response. ({0}) : {1}", xmlEx, new StreamReader(ymmStream).ReadToEnd());
                    }
                    if (response != null) {
                        if (response.Status == null || !"OK".Equals(response.Status)) {
                            Log.Warn("ModelVersionsResponse object has non-'OK' status set or was null.");
                        }
                    }
                }
            }
            return response;
        }

        private static T DeserializeXmlStreamToObject<T>(Stream xmlStream)
        {
            var deserializer = new XmlSerializer(typeof(T));

            var o = (T)deserializer.Deserialize(xmlStream);

            if (o == null)
                Log.Info("XmlStream deserializer created a null object.");
            return o;
        }
    }
}
