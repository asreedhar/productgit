﻿using System;
using System.Xml.Serialization;

namespace AutoLoad.Models
{
    public class ModelVersion
    {
        [XmlAttribute("make")] public String Make;
        [XmlAttribute("model")] public String Model;
        [XmlAttribute("year")] public Int32 Year;
    }
}
