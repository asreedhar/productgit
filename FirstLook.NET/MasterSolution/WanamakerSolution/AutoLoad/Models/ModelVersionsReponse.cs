﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace AutoLoad.Models
{
    [XmlRoot("Response")]
    public class ModelVersionsReponse
    {
        [XmlAttribute("status")] public String Status;

        [XmlElement("ModelVersion")]
        public List<ModelVersion> ModelVersions;
    }
}
