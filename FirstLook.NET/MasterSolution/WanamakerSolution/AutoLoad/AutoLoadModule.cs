﻿using Autofac;

namespace AutoLoad
{
    public class AutoLoadModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FordDirectAutoLoadThreadStarter>().As<IFordDirectAutoLoadThreadStarter>();
            base.Load(builder);
        }
    }
}
