﻿using System.Linq;
using AutoLoad.FordNew.DataAccess;
using System;
using AutoLoad.FordNew.Domain;

namespace AutoLoad
{
    public class FordNewAutoLoadSubscription
    {
        private readonly MerchandisingRepository _merchandisingRepository;

        public FordNewAutoLoadSubscription()
        {
            _merchandisingRepository = new MerchandisingRepository();            
        }

        /// <summary>
        /// Subscribe the business unit to the pa code, returning the name associated with the pa code.
        /// If the pa code is invalid, return empty string and do not save the subscription.
        /// If the pa code is valid, save the subscription and return the name.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="dealerPaCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string Subscribe(int businessUnitId, string dealerPaCode, string userId)
        {
            var name = GetDealerName(dealerPaCode);
            if (string.IsNullOrWhiteSpace(name)) return string.Empty;

            _merchandisingRepository.SaveDealerCode(new DealerCode(businessUnitId, dealerPaCode), userId);
            return name;
        }

        public void Unsubscribe(int businessUnitId, string userId)
        {
            _merchandisingRepository.DeleteDealerCode(new DealerCode{BusinessUnitId = businessUnitId});
        }

        public string GetDealerPaCode(int businessUnitId)
        {
            var matches =
                _merchandisingRepository.GetDealerCodes()
                                        .Where(dc => dc.BusinessUnitId == businessUnitId)
                                        .Select(dc => dc.DealerPaCode);

            return matches.FirstOrDefault();
        }

        public string GetDealerName(string dealerPaCode)
        {
            var doc = new DealersService().GetDealer(Makes.Ford, dealerPaCode);

            if (doc.Root != null)
            {
                foreach (var dealer in doc.Root.Elements("Dealer"))
                {
                    string name = dealer.Element("Name").Value;
                    return name;
                }
            }
            return string.Empty;
        }

    }
}
