﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using Dapper;

namespace AutoLoad.FordNew.DataAccess
{
    public class MerchandisingRepository
    {

        public IEnumerable<Inventory> InventoryWithUnknownOptionsConfiguration(int businessUnitId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT IA.BusinessUnitID, IA.InventoryID, V.Vin, IA.StockNumber, IA.InventoryType
                        FROM 
	                        FLDW.dbo.InventoryActive IA
	                        JOIN FLDW.dbo.Vehicle V
		                        ON IA.VehicleID = V.VehicleID AND IA.BusinessUnitID = V.BusinessUnitID
	                        JOIN Merchandising.builder.OptionsConfiguration OC
		                        ON OC.BusinessUnitID = IA.BusinessUnitID AND OC.inventoryId = IA.InventoryID

                        WHERE IA.BusinessUnitID = @BusinessUnitID
                        AND OC.ChromeStyleID = -1                    
                    ";

                var inventory = sqlConnection.Query<Inventory>(query, new { BusinessUnitId = businessUnitId });
                sqlConnection.Close();

                return inventory;
            }
        }

        public IEnumerable<Inventory> InventoryWithNoOptionsConfiguration(int businessUnitId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT IA.BusinessUnitID, IA.InventoryID, V.Vin, IA.StockNumber, IA.InventoryType
                        FROM 
                            FLDW.dbo.InventoryActive IA
                            JOIN FLDW.dbo.Vehicle V
                                ON IA.VehicleID = V.VehicleID AND IA.BusinessUnitID = V.BusinessUnitID
                            LEFT OUTER JOIN Merchandising.builder.OptionsConfiguration OC
                                ON OC.BusinessUnitID = IA.BusinessUnitID AND OC.inventoryId = IA.InventoryID

                        WHERE IA.BusinessUnitID = @BusinessUnitID
                        AND OC.businessUnitID IS NULL                    
                    ";

                var inventory = sqlConnection.Query<Inventory>(query, new { BusinessUnitId = businessUnitId });
                sqlConnection.Close();

                return inventory;
            }
            
        }


        public IEnumerable<DealerCode> GetDealerCodes()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT BusinessUnitID, DealerPACode
                        FROM settings.FordAutoLoadNew_DealerCodes 
                    ";

                var codes = sqlConnection.Query<DealerCode>(query);
                sqlConnection.Close();

                return codes;
            }
        }

        public void DeleteDealerCode(DealerCode dealerCode)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                const string DELETE = @"DELETE settings.FordAutoLoadNew_DealerCodes
                                        WHERE BusinessUnitID = @businessUnitID";

                sqlConnection.Execute(DELETE,
                                        new
                                        {
                                            businessUnitId = dealerCode.BusinessUnitId
                                        });
                sqlConnection.Close();
            }
        }

        public void SaveDealerCode(DealerCode dealerCode, string userId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                const string UPSERT = @"
                    if exists(Select * From settings.FordAutoLoadNew_DealerCodes WHERE businessUnitID = @businessUnitId)
	                    BEGIN
		                    UPDATE settings.FordAutoLoadNew_DealerCodes
		                    SET dealerPaCode = @dealerPaCode, UpdatedBy=@userId, UpdatedOn=GETDATE()
		                    WHERE BusinessUnitID = @businessUnitID 
	                    END
                    ELSE
	                    BEGIN
		                    INSERT INTO settings.FordAutoLoadNew_DealerCodes
		                            ( businessUnitID ,
		                                dealerPaCode ,
		                                CreatedOn ,
		                                CreatedBy ,
		                                UpdatedOn ,
		                                UpdatedBy
		                            )
		                    VALUES  ( @businessUnitId , -- businessUnitID - int
		                                @dealerPaCode , -- dealerPaCode - varchar(20)
		                                GETDATE() , -- CreatedOn - datetime
		                                @userId , -- CreatedBy - varchar(32)
		                                GETDATE() , -- UpdatedOn - datetime
		                                @userId  -- UpdatedBy - varchar(32)
		                            )
                        END";

                sqlConnection.Execute(UPSERT,
                                        new
                                            {
                                                businessUnitId = dealerCode.BusinessUnitId,
                                                userId = userId,
                                                dealerPaCode = dealerCode.DealerPaCode
                                            });
                sqlConnection.Close();
            }
            
        }


        public IEnumerable<YearMakeModel> GetYearMakeModels()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT Year, Make, Model
                        FROM settings.FordAutoLoadNew_YearMakeModel 
                    ";

                var ymm = sqlConnection.Query<YearMakeModel>(query);
                sqlConnection.Close();

                return ymm;
            }
            
        }
    }

    public class DealerCode
    {
        public DealerCode() { }
        public DealerCode(int businessUnitId, string dealerPaCode)
        {
            BusinessUnitId = businessUnitId;
            DealerPaCode = dealerPaCode;
        }

        public int BusinessUnitId { get; set; }
        public string DealerPaCode { get; set; }
    }

    public class YearMakeModel : IEquatable<YearMakeModel>
    {
        public YearMakeModel() { }
        public YearMakeModel(int year, string make, string model)
        {
            Year = year;
            Make = make;
            Model = model;
        }

        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public bool Equals(YearMakeModel other)
        {
            return (other.Year == Year && other.Make == Make && other.Model == Model);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = 37;
                result *= 397;
                result += Year.GetHashCode();

                result *= 397;
                if (Make != null)
                    result += Make.GetHashCode();

                result *= 397;
                if (Model != null)
                    result += Model.GetHashCode();

                return result;
            }
        }

        public override string ToString()
        {
            return new StringBuilder().AppendFormat("{0} {1} {2}", Year, Make, Model).ToString();
        }
    }
}
