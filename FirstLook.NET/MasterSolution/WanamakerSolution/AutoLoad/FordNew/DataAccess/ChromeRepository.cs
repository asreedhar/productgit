using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace AutoLoad.FordNew.DataAccess
{
    public class ChromeRepository
    {
        public IEnumerable<ChromeStyle> GetStyles(string vin)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString))
            {
                conn.Open();

                // TODO: Refactor this to a sproc.
                const string stylesQuery = @"
                        DECLARE @vinPattern CHAR(9)
                        SET @vinPattern = SUBSTRING(@vin,0,9) + SUBSTRING(@vin,10,1)

                        SELECT S.CountryCode, S.StyleId, S.ModelYear, S.StyleCode, S.FullStyleCode, S.StyleName, S.MSRP, S.Trim, 
                               S.ManualTrans, S.AutoTrans, S.FrontWD,S.RearWD,S.AllWD,S.FourWD,S.StepSide,
                               S.StyleNameWOTrim, S.CFStyleName, S.CFModelName, S.CFDrivetrain, MC.MarketClass 
                        FROM VehicleCatalog.Chrome.Styles S
                            JOIN VehicleCatalog.Chrome.VINPatternStyleMapping VM
                                ON VM.ChromeStyleID = S.StyleID AND VM.CountryCode = S.CountryCode
                            JOIN VehicleCatalog.Chrome.VINPattern VP
                                ON VP.VINPatternID = VM.VINPatternID AND VP.CountryCode = VM.CountryCode
                            JOIN VehicleCatalog.Chrome.MktClass MC
                          ON S.MktClassID = MC.MktClassID AND S.CountryCode = MC.CountryCode
                        WHERE VP.VINPattern_Prefix = @vinPattern
                    ";

                var styles = conn.Query<ChromeStyle>(stylesQuery, new {vin }).ToList();

                // TODO: Refactor this to a sproc.
                // get the wheelbases
                const string wheelBasesQuery = @"
                        SELECT CountryCode, ChromeStyleId, WheelBase
                        FROM VehicleCatalog.Chrome.StyleWheelBase
                        WHERE ChromeStyleID IN @styles
                    ";

                IEnumerable<ChromeStyleWheelBase> wheelBases =
                    conn.Query<ChromeStyleWheelBase>(wheelBasesQuery,
                                                     new
                                                         {
                                                             @styles = styles.Select(s => s.StyleId)
                                                                             .Distinct()
                                                                             .ToArray()
                                                         });

                conn.Close();

                // Add the wheelbase values to the style.
                foreach(var wheelBase in wheelBases)
                {
                    ChromeStyleWheelBase @base = wheelBase; // avoid access to foreach var in closure
                    foreach(var style in styles.Where(s => s.StyleId==@base.ChromeStyleId && s.CountryCode==@base.CountryCode))
                    {
                        style.WheelBases.Add(wheelBase.WheelBase);                        
                    }
                }

                return styles;
            }
        }

        // Option kind ids
        private const int SEAT_KIND_ID = 27;
        private const int EXTERIOR_COLOR_KIND_ID = 68;
        private const int EQUIPMENT_PACKAGE_KIND_ID = 20;
        private const int ENGINE_KIND_ID = 6;
        private const int XMSN_KIND_ID = 7;
        private const int AXLE_RATIO = 8;

        public bool IsSeat(ChromeOption o)
        {
            if (o == null) return false;
            return o.OptionKindId == SEAT_KIND_ID;
        }

        public bool IsExteriorColor(ChromeOption o)
        {
            if (o == null) return false;
            return o.OptionKindId == EXTERIOR_COLOR_KIND_ID;
        }

        public bool IsEquipmentPackage(ChromeOption o)
        {
            if (o == null) return false;
            return o.OptionKindId == EQUIPMENT_PACKAGE_KIND_ID;
        }

        public bool IsEngine(ChromeOption o)
        {
            if (o == null) return false;
            return o.OptionKindId == ENGINE_KIND_ID;
        }

        public bool IsXmsn(ChromeOption o)
        {
            if (o == null) return false;
            return o.OptionKindId == XMSN_KIND_ID;
        }

        public bool IsAxleRatio(ChromeOption o)
        {
            if (o == null) return false;
            return o.OptionKindId == AXLE_RATIO;
        }

        public IEnumerable<ChromeOption> GetOptions(IEnumerable<int> styles)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query =
                    @"
                        SELECT	O.CountryCode, O.StyleId, O.OptionCode, O.OptionDesc, O.OptionKindId, 
                                O.TypeFilter,O.Availability,O.PON, O.ExtDescription, P.MSRP 

                        FROM	VehicleCatalog.Chrome.Options O
                                JOIN VehicleCatalog.Chrome.Prices P
                                    ON O.CountryCode = P.CountryCode AND O.StyleID = P.StyleID AND O.OptionCode = P.OptionCode
                        WHERE	O.StyleID IN @styles			
                        ORDER BY O.OptionKindID
                    ";

                var options = sqlConnection.Query<ChromeOption>(query, new {styles = styles.ToArray()});
                sqlConnection.Close();

                return options;
            }

        }

        public IEnumerable<ChromeColor> GetColors(IEnumerable<int> styles )
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query =
                    @"
                        SELECT	CountryCode, StyleId, IntManCode, 
                                IntDesc, 
                                Ext1Code,Ext1ManCode,Ext1Desc,
                                Ext2Code,Ext2ManCode,Ext2Desc,
                                Ext1MfrFullCode,
                                Ext2MfrFullCode
                        FROM	VehicleCatalog.Chrome.Colors
                        WHERE	StyleID IN @styles	
                    ";

                var colors = sqlConnection.Query<ChromeColor>(query, new { styles = styles.ToArray() });
                sqlConnection.Close();

                return colors;
            }
        }

        public IEnumerable<ChromeStandard> GetStandards(IEnumerable<int> styles, byte countryCode = 1)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString))
            {
                sqlConnection.Open();

                // TODO: Refactor this to a sproc.
                const string query =
                    @"
                    SELECT CountryCode, StyleID, HeaderID, Sequence, Standard, CategoryList
	                    FROM VehicleCatalog.Chrome.Standards
	                    WHERE CountryCode = countryCode
	                    AND StyleID IN @styles	
                    ";

                var standards = sqlConnection.Query<ChromeStandard>(query, new { styles = styles.ToArray() });
                sqlConnection.Close();

                return standards;
            }
        }

    }

    public class ChromeStyle
    {
        public ChromeStyle()
        {
            WheelBases = new List<decimal>();
        }

        public int CountryCode { get; set; }
        public int StyleId { get; set; }
        public int ModelYear { get; set; }
        public string StyleCode { get; set; }
        public string FullStyleCode { get; set; }
        public string StyleName { get; set; }
        public double Msrp { get; set; }
        public string Trim { get; set; }
        public string MarketClass { get; set; }

        public string ManualTrans { get; set; }
        public string AutoTrans { get; set; }
        public string FrontWd { get; set; }
        public string RearWd { get; set; }
        public string AllWd { get; set; }
        public string FourWd { get; set; }
        public string StepSide { get; set; }

        public string StyleNameWoTrim { get; set; }
        public string CfStyleName { get; set; }
        public string CfModelName { get; set; }
        public string CfDrivetrain { get; set; }

//        public decimal WheelBase { get; set; }

        public List<decimal> WheelBases { get; set; } 
    }

    public class ChromeStyleWheelBase
    {
        public int CountryCode { get; set; }
        public int ChromeStyleId { get; set; }
        public decimal WheelBase { get; set; }
    }

    public class ChromeOption
    {
        public int CountryCode { get; set; }
        public int StyleId { get; set; }
        public string OptionCode { get; set; }
        public string OptionDesc { get; set; }
        public int OptionKindId { get; set; }
        public string TypeFilter { get; set; }
        public string Availability { get; set; }
        public string Pon { get; set; }
        public string ExtDescription { get; set; }
        public decimal Msrp { get; set; }
    }

    public class ChromeColor
    {
        public int CountryCode { get; set; }
        public int StyleId { get; set; }

        public string IntManCode { get; set; }
        public string IntDesc { get; set; }

        public string Ext1Code { get; set; }
        public string Ext1ManCode { get; set; }
        public string Ext1Desc { get; set; }

        public string Ext2Code { get; set; }
        public string Ext2ManCode { get; set; }
        public string Ext2Desc { get; set; }

        public string Ext1MfrFullCode { get; set; }
        public string Ext2MfrFullCode { get; set; }
    }

    public class ChromeStandard
    {
        public byte CountryCode { get; set; }
        public int StyleId { get; set; }
        public int HeaderId { get; set; }
        public int Sequence { get; set; }
        public string Standard { get; set; }
        public string CategoryList { get; set; }

    }


}