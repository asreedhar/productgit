﻿using System;
using System.IO;
using System.Xml.Linq;
using AutoLoad.FordNew.Domain;
using Merchandising.Messages;

namespace AutoLoad.FordNew.DataAccess
{
    public class BuildDataRepository
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IFileStoreFactory _fileStoreFactory;
        private const string FOLDER_NAME = "FordDirect";

        public BuildDataRepository(IFileStoreFactory fileStoreFactory)
        {
            _fileStoreFactory = fileStoreFactory;
        }

        public XDocument FindDocument(YearMakeModel ymm, DealerCode dealerCode)
        {
            var fileStorage = _fileStoreFactory.CreateAutoLoadAudit();
            string fileName = FileName(ymm, dealerCode);
            bool exists = false;

            var stream = fileStorage.Exists(fileName, out exists);
            
            if (exists)
                return XDocument.Load(stream);
            else
                return null;
        }

        public string SaveDocument(VehicleDetails details, YearMakeModel ymm, DealerCode dealerCode)
        {
            var storage = _fileStoreFactory.CreateAutoLoadAudit();
            string fileName = FileName(ymm, dealerCode);

            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(details.Document.ToString());
                writer.Flush();
                stream.Position = 0;
                storage.Write(fileName, stream);
            }

            // TODO: Consider shredding the xml to store on doc per vin.

            // TODO: need to expand the API to return this. 
            return fileName;
        }


        
        internal string FolderName()
        {
            return FOLDER_NAME;
        }

        internal string FileName(YearMakeModel ymm, DealerCode dealerCode)
        {
            string timestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

            // 2013-Ford-Mustaing_12345-09894_20130521221916.xml
            return string.Format("{0}/{1}-{2}-{3}_{4}-{5}_{6}.xml", FolderName(),
                                 ymm.Year, ymm.Make, ymm.Model, dealerCode.BusinessUnitId,dealerCode.DealerPaCode,
                                 timestamp);
        }
    }
}
