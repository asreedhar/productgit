﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace AutoLoad.FordNew.DataAccess
{
    public class InventoryRepository
    {
        public IEnumerable<Inventory> ActiveInventory(int businessUnitId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["FLDW"].ConnectionString))
            {
                sqlConnection.Open();

                //TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT DISTINCT IA.BusinessUnitID, IA.InventoryID, V.Vin, IA.StockNumber, IA.InventoryType, M.Make, L.Line, S.Segment
                        FROM FLDW.dbo.InventoryActive IA
                        JOIN FLDW.dbo.Vehicle V
	                        ON IA.BusinessUnitID = V.BusinessUnitID AND IA.VehicleID = V.VehicleID
                        JOIN VehicleCatalog.Firstlook.VehicleCatalog VC
	                        ON SUBSTRING(V.Vin,0,9) + SUBSTRING(V.Vin,10,1)	= VC.SquishVIN
                        JOIN VehicleCatalog.FirstLook.Line L
		                        ON VC.LineID = L.LineID
                        JOIN VehicleCatalog.FirstLook.Make M
		                        ON L.MakeID = M.MakeID
                        JOIN VehicleCatalog.FirstLook.Segment S
	                        ON S.SegmentID = VC.SegmentID
                        WHERE IA.BusinessUnitID = @businessUnitId
                    ";

                var inventory = sqlConnection.Query<Inventory>(query, new { BusinessUnitId=businessUnitId}); 
                sqlConnection.Close();

                return inventory;
            }
        }
    }
}
