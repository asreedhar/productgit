namespace AutoLoad.FordNew.DataAccess
{
    public class Inventory
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string Vin { get; set; }
        public string StockNumber { get; set; }
        public int InventoryType { get; set; }

        public string Make { get; set; }
        public string Line { get; set; }
        public string Segment { get; set; }
    }
}