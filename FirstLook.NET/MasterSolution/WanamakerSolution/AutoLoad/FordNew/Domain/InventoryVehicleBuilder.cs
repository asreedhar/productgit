using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace AutoLoad.FordNew.Domain
{
    public class InventoryVehicleBuilder
    {
        public IEnumerable<InventoryVehicleBuild> VehicleBuilds(XDocument doc)
        {
            var vehicles = new List<InventoryVehicleBuild>();

            if (doc.Root != null)
            {
                foreach (var vehicle in doc.Root.Elements("InventoryVehicle"))
                {
                    var v = BuildInventoryVehicle(vehicle);
                    vehicles.Add(v);
                }
            }

            return vehicles;
        }

        public InventoryVehicleBuild BuildInventoryVehicle(XElement inventoryVehicleElement)
        {
            var iv = new InventoryVehicleBuild();

            iv.Product = GetElementValue(inventoryVehicleElement,"Product");
            iv.Vin = GetElementValue(inventoryVehicleElement, "Vin");
            iv.StockNumber = GetElementValue(inventoryVehicleElement,"StockNumber");
            iv.VehicleStage = GetElementValue(inventoryVehicleElement,"VehicleStage");
            iv.DealerPaCode = GetElementValue(inventoryVehicleElement,"DealerPACode");
            iv.WindowStickerUrl = GetElementValue(inventoryVehicleElement, "WindowStickerURL").Trim();

            iv.ImageToken = GetElementValue(inventoryVehicleElement,"ImageToken");
            iv.ConfigToken = GetElementValue(inventoryVehicleElement,"ConfigToken");

            //
            // Get the interior color name. 
            // TODO: Find the code from FDS. The code they return from "DC_InteriorColor_PartNumber" is an 
            //       internal part number which is not mapped in chrome.
            //
            // Note: requested attributes are returned as elements named "Attribute"
            // E.G.
            // <Attribute name="DC_InteriorColor_Name_EN">
            //   <Value>Stone</Value>
            // </Attribute>
            // <Attribute name="DC_InteriorColor_PartNumber">
            //   <Value>89L</Value>
            // </Attribute>
            // <Attribute name="SI3_Filters_InteriorColor"><Value>leather_stone</Value></Attribute>
            const string intColorNameAttr = "DC_InteriorColor_Name_EN";
            var attrElements = inventoryVehicleElement.Elements("Attribute").ToList();

            // color name
            var firstOrDefault = attrElements.FirstOrDefault(e => e.Attribute("name").Value == intColorNameAttr);
            if (firstOrDefault != null)
            {
                var xElement = firstOrDefault.Element("Value");
                iv.InteriorColorName = xElement != null ? xElement.Value : string.Empty;
            }
            else
            {
                iv.InteriorColorName = string.Empty;
            }

            // is the interior leather?
            const string intColorDescAttr = "SI3_Filters_InteriorColor";
            var interiorDescription = attrElements.FirstOrDefault(e => e.Attribute("name").Value == intColorDescAttr);
            if (interiorDescription != null)
            {
                var xElement = interiorDescription.Element("Value");
                if (xElement != null && xElement.Value.Contains("leather"))
                {
                    iv.LeatherInterior = true;
                }
                else
                {
                    iv.LeatherInterior = false;
                }
            }

            // Is the transmission automatic?
            const string xmsnAttr = "SI3_Filters_Transmission";
            var xmsnDescription = attrElements.FirstOrDefault(e => e.Attribute("name").Value == xmsnAttr);
            if (xmsnDescription != null)
            {
                var xElement = xmsnDescription.Element("Value");
                if (xElement != null && xElement.Value.Contains("automatic"))
                {
                    iv.AutomaticTransmission = true;
                }
                else
                {
                    iv.AutomaticTransmission = false;
                }
            }

            // drivetrain
            const string driveTrainAttr = "SI3_Filters_Drive";
            var driveTrainDesc = attrElements.FirstOrDefault(e => e.Attribute("name").Value == driveTrainAttr);
            if(driveTrainDesc != null )
            {
                var xElement = driveTrainDesc.Element("Value");
                iv.DriveTrain = xElement != null ? xElement.Value : string.Empty;
            }
            else
            {
                iv.DriveTrain = string.Empty;
            }

            // Trim name
            //const string trimAttr = "IQQV2_ModelSlices";
            const string trimAttr = "SI3_Trim_Definers"; // FB: 28797 - added per CNorton.  He will QA
            var trimDesc = attrElements.FirstOrDefault(e => e.Attribute("name").Value == trimAttr);
            if (trimDesc != null)
            {
                var xElement = trimDesc.Element("Value");
                iv.Trim = xElement != null ? xElement.Value : string.Empty;
            }
            else
            {
                iv.Trim = string.Empty;
            }

            var options = new List<string>();
            var parts = new List<string>();

            // parse the ConfigToken
            // Config[|Ford|F-150|2013|1|1.|301A.W1C.145.UH..88M.~VIRTUALPKGPART_B2CAC_36.~VIRTUALPKGPART_CBAAB_38.~VIRTUALPKGPART_CBFAB_39.~VIRTUALPKGPART_JCAAB_40.~VIRTUALPKGPART_JEDAD_46.~VIRTUALPKGPART_B2FAD_67.18C.775.SS5.64T.54H.89S.~SEXRAM.~VIRTUALPKGPART_BB7AC_51.~VIRTUALPKGPART_BSBAF_53.~VIRTUALPKGPART_BSBAZ_54.~VIRTUALPKGPART_HMIAD_56.~VIRTUALPKGPART_YCABC_57.~VIRTUALPKGPART_BYPAC_71.~BMCAA.~CLMAE.99M.RWD.CLFKP.~A9PAA.17P.~BBHAB.168.141.~BY2AA.90P.67K.~CHAAA.~DEEAA.91K.21S.~FMLAA.~FMNAA.50S.124.143.~J3KAA.595.58D.~YBBAA.1.91S.52G.X26.U31.~A4YAA.86X.~AARAA.~AAXAA.~AB2AA.85A.ADPAA.~BPCAA.535.~YEFAA.~YLAAA.~YPEAA.~YPJAA.61C.456.CCAB.~A2AAC.~A2BAC.~A9JAC.~AB5BG.~ABBAA.~ABEAA.572.~ACYAA.~AD6LA.~ADCAB.~AE8AB.~B2CAC.~B2FAD.~B2GAE.~B5VAV.~BBIAB.~BSLAB.~BSNAB.~BSRAA.~BY3AA.~BYBBY.~BYLAA.~BYQAB.~C1UAA.~C1VAA.159.~CB7AB.~CBAAB.~CBFAB.~CFFAB.~CHJAA.~CN7AF.~D19AH.~D1DAC.~D1GAC.~D1ZAB.DAAAD.~F2AAC.~FBAAD.~FEAAB.~FECAA.~FEFAH.~FKAAB.652.~GBTAG.~GRAAG.86X02.~GTEAB.~HDHAB.~HGAAB.~HJEAB.~HJFAB.~HTAAE.~HUKAA.~IBAAZ.~IBYAB.~IDGAA.~JBBAB.~JBDAH.~JCAAB.~JCBAA.~JCIAC.~JDAAB.~JEDAD.~YZSAJ.153.~JBCAA.CLO.446.T82.XLT.]
            var config = iv.ConfigToken.Replace("Config[", string.Empty).TrimEnd(']').Split('|');
            var configOptions = config[6].Split('.');

            iv.Make = config[1];
            iv.Model = config[2];
            iv.Year = config[3];

            iv.PreferredEquipmentPackage = configOptions[0];
            iv.ModelCode = configOptions[1];
            iv.WheelBase = configOptions[2];
            iv.ExteriorColorCode = configOptions[3];
            //            Cab = options[numOptions-3];  // these work for f-150
            //            Series = options[numOptions-2];


            foreach (string item in configOptions)
            {
                if (!options.Contains(item) && !item.StartsWith("~") && item.Trim().Length > 0)
                {
                    options.Add(item);
                }

                // I think the values starting with ~ are parts, not options.
                if (!parts.Contains(item) && item.StartsWith("~") && item.Trim().Length > 0)
                {
                    parts.Add(item);
                }

            }

            iv.Options = options;
            iv.Parts = parts;

            // TODO: Pricing data.
//            var pricingElement = inventoryVehicleElement.Element("Pricing");
//            iv.Pricing = BuildPricing(pricingElement);
            
            return iv;
        }

/*
        public InventoryVehicle.PricingData BuildPricing(XElement pricingElement)
        {
            var p = new InventoryVehicle.PricingData();

            var paElement = pricingElement.Element("PlanApplicability");
            var pa = new InventoryVehicle.PricingData.PlanApplicabilityData();
            pa.AZ = GetElementValue(paElement,"AZ");
            pa.FNX = GetElementValue(paElement,"FNX");
            pa.PRX = GetElementValue(paElement,"PRX");
            pa.SPX = GetElementValue(paElement,"SPX");

            p.PlanApplicability = pa;

            p.DealerPrice = GetElementValue(pricingElement, "DealerPrice");
            p.BaseMSRP = GetElementValue(pricingElement, "BaseMSRP");

        }

*/
        private string GetElementValue(XElement e, string name)
        {
            if( e == null ) 
                return string.Empty;

            var xElement = e.Element(name);
            return xElement != null ? xElement.Value : string.Empty;

        }
    }

}