﻿using System;
using System.Xml.Linq;

namespace AutoLoad.FordNew.Domain
{
    class DealersService
    {
        private ServiceProxy _proxy;
        private const string SERVICE_BASE = "http://services.forddirect.fordvehicles.com/dealer/Dealers";
        private const string DETAILS_BASE = "http://services.forddirect.fordvehicles.com/dealer/Details";

        public DealersService()
        {
            _proxy = new ServiceProxy();
        }

        public XDocument GetDealers(Makes make, int zip)
        {
            string url = String.Format("{0}?make={1}&postalCode={2}",SERVICE_BASE, GetMake(make), zip);
            return _proxy.DownloadData(url);
        }

        public XDocument GetDealers(Makes make, string stateCode)
        {
            string url = String.Format("{0}?make={1}&state={2}", SERVICE_BASE, GetMake(make), StateCode(stateCode));
            return _proxy.DownloadData(url);
        }

        public XDocument GetDealer(Makes make, string dealerPaCode)
        {
            string url = String.Format("{0}?make={1}&dealerPACode={2}", DETAILS_BASE, GetMake(make), dealerPaCode);
            return _proxy.DownloadData(url);            
        }

        private string StateCode(string stateCode)
        {
            return stateCode.ToUpper().Trim();
        }

        private string GetMake(Makes make)
        {
            if (make == Makes.Ford) return "Ford";
            throw new ArgumentException("Invalid make");
        }
    }
}
