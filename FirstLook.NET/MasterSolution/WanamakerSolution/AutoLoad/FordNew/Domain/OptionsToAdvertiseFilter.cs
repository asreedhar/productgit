﻿using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;

namespace AutoLoad.FordNew.Domain
{
    /// <summary>
    /// Not all options are worth advertising.  This filter finds the ones that are. Think of this like a more dynamic "white list". 
    /// </summary>
    internal class OptionsToAdvertiseFilter
    {
        private readonly ChromeRepository _chromeRepository;

        internal OptionsToAdvertiseFilter()
        {
            _chromeRepository = new ChromeRepository();
        }

        internal IEnumerable<ChromeOption> Filter(IEnumerable<ChromeOption> options)
        {
            if (options == null) return new List<ChromeOption>();

            var optionsToAdvertise = options.Where( o => o.Msrp > 0 || (IsKeyEquipment(o) && o.Msrp >= 0))
                                            .Distinct()
                                            .ToList();

            return optionsToAdvertise;
        }

        private bool IsKeyEquipment(ChromeOption o)
        {
            if (o == null) return false;

            var test =
                   _chromeRepository.IsEngine(o) ||
                   _chromeRepository.IsEquipmentPackage(o) ||
                   _chromeRepository.IsExteriorColor(o) ||
                   _chromeRepository.IsSeat(o) ||
                   _chromeRepository.IsXmsn(o);
                    // wheels?

            // include axle ratio for trucks
            // TODO: Do this without looking at inventory.
/*
            if (inventory != null)
            {
                if (inventory.Segment.ToLower().Trim() == "truck")
                {
                    test = test || _chromeRepository.IsAxleRatio(o);
                }
            }
*/

            return test;
        }
    }
}
