﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using log4net.Core;

namespace AutoLoad.FordNew.Domain
{
    internal class ChromeStyleMatcher
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly OptionCodeComparer _optionCodeComparer;

        public ChromeStyleMatcher()
        {
            _optionCodeComparer = new OptionCodeComparer();
        }

        /// <summary>
        /// Inspect the inventory, build data, available styles, available options, and available colors. Determine the correct
        /// style, options, and colors, returning them via the out params.
        /// 
        /// By default, returns a null style and color, and an empty (non-null) IEnumerable of options.
        /// 
        /// This function assumes that the list of styles passed in has already been filtered to only include those which
        /// are possible (by vin pattern match) for the vehicle. As such, if only one style is passed, it will be selected. 
        /// Do not pass this function a single style that can not possibly be associated with the vehicle.
        /// </summary>
        /// <param name="build">The build data for this car.</param>
        /// <param name="styles">The possible chrome styles.</param>
        /// <param name="options">The possible chrome options.</param>
        /// <param name="colors">The possible chrome colors.</param>
        /// <param name="foundStyle">The chrome style deduced from the build data.</param>
        /// <param name="foundOptions">The chrome options identified from the build data.</param>
        /// <param name="foundColor">The chrome color deduced from the build data.</param>
        internal void Match(InventoryVehicleBuild build, IEnumerable<ChromeStyle> styles,
                            IEnumerable<ChromeOption> options, IEnumerable<ChromeColor> colors,
                            out ChromeStyle foundStyle, out IEnumerable<ChromeOption> foundOptions,
                            out ChromeColor foundColor)
        {
            // setup default returns
            foundStyle = null;
            foundOptions = new List<ChromeOption>();
            foundColor = null;

            // get lists of our input enumerations
            var chromeStyles = styles as IList<ChromeStyle> ?? styles.ToList();
            var chromeOptions = options as IList<ChromeOption> ?? options.ToList();
            var chromeColors = colors as IList<ChromeColor> ?? colors.ToList();

            Log.DebugFormat("STYLE: Matching started for VIN: {0}, Year:{1}, Model: {2}, Trim: {3}, WheelBase: {4}", build.Vin, build.Year, build.Model, build.Trim, build.WheelBase.ToString());

            //
            // Find the style
            //

            // If there is only one possible style, pick it.
            if (chromeStyles.Select(s => s.StyleId).Distinct().Count() == 1)
            {
                Log.DebugFormat("STYLE: Style {0} picked because there is only one possible choice.", chromeStyles.First().StyleId);
                foundStyle = chromeStyles.First();
                foundColor = FindColor(build, chromeColors, foundStyle);
                foundOptions = FindOptions(build, foundColor, chromeOptions, foundStyle);
                return;
            }

            var styleFilters = ChromeStyleFilterBuilder.Build(build);

            // Apply filters to each style. If any match just 1 style, we are done. 
            foreach (var styleFilter in styleFilters)
            {
                try
                {
                    var matchingStyles = chromeStyles.Where(styleFilter.Predicate).ToList();
                    if (matchingStyles.Count() == 1)
                    {
                        Log.DebugFormat("STYLE: Style {0} - {1} picked via filter {2}", matchingStyles.First().StyleId, matchingStyles.First().StyleName, styleFilter.Id );

                        // we found the matching style
                        foundStyle = matchingStyles.First();
                        foundColor = FindColor(build, chromeColors, foundStyle);
                        foundOptions = FindOptions(build, foundColor, chromeOptions, foundStyle);
                        return;
                    }
                }
                catch (NullReferenceException ex)
                {
                    // some of the filters may look for data that may not be initialized
                    Log.Warn(ex);
                }
            }


            Log.Debug("STYLE: Unable to determine by filters alone. Inspecting options next.");

            // First, check whether any of the vehicle's options are only available on one style. If that's the case,
            // then pick that style.
            // ------  change to above ---------
            // FB: 28797 - SuperDuty false positives on option filtering, 2/12/2014 - tureen
            // options and standards might be mixed in the XML causing false positives.  The only truely known option is preferred package
            // changed to only test the preferred option (package)
            // 
            //foreach (var buildOption in build.Options)
            //{

            if(!String.IsNullOrWhiteSpace(build.PreferredEquipmentPackage))
            {
                string buildOption = build.PreferredEquipmentPackage;

                var stylesWithOption = chromeOptions.Where(o => _optionCodeComparer.Equals(o.OptionCode, buildOption))
                                              .Select(o => o.StyleId)
                                              .Distinct().ToList();

                if (stylesWithOption.Count() == 1)
                {
                    // make sure that style is available.
                    int styleId = stylesWithOption.First();

                    var style = chromeStyles.First(s => s.StyleId == styleId);

                    if (style != null)
                    {
                        Log.DebugFormat("STYLE: Style {0} - {1} picked because build option {2} is unique among possible styles.", style.StyleId, style.StyleName, buildOption);

                        foundStyle = style;
                        foundColor = FindColor(build, chromeColors, foundStyle);
                        foundOptions = FindOptions(build, foundColor, chromeOptions, foundStyle);
                        return;
                    }
                }
                else if (stylesWithOption.Count() > 1)
                {
                    Log.DebugFormat("STYLE: There are {0} styles which contain option {1}. Narrowing by filters within these styles.", stylesWithOption.Count, buildOption);

                    // can we narrow it down by applying style filters?
                    var candidateStyles = chromeStyles.Where(s => stylesWithOption.Contains(s.StyleId))
                                                      .ToList();

                    foreach (var styleFilter in styleFilters)
                    {
                        var matchingStyles = candidateStyles.Where(styleFilter.Predicate).ToList();
                        if (matchingStyles.Count() == 1 || matchingStyles.Select(s => s.StyleId).Distinct().Count() == 1)
                        {
                            Log.DebugFormat("STYLE: Style {0} - {1} picked because of option {2} and filter {3}", matchingStyles.First().StyleId, matchingStyles.First().StyleName, buildOption, styleFilter.Id);

                            // we found the matching style
                            foundStyle = matchingStyles.First();
                            foundColor = FindColor(build, chromeColors, foundStyle);
                            foundOptions = FindOptions(build, foundColor, chromeOptions, foundStyle);
                            return;
                        }
                    }
                }
            }
            

            // Cascade? 
            Log.WarnFormat("STYLE: Unable to determine style!");
        }

        private IEnumerable<ChromeOption> FindOptions(InventoryVehicleBuild build, ChromeColor color, IList<ChromeOption> options, 
            ChromeStyle style)
        {
            //
            // Find the options.
            //
            var matchedOptions = options.Where(
                                                o =>
                                                o.StyleId == style.StyleId &&
                                                o.CountryCode == style.CountryCode &&
                                                build.Options.Contains(o.OptionCode, _optionCodeComparer)
                                              )
                                        .Distinct()
                                        .ToList();

            var leatherOption = FindColorMatchedLeatherInterior(build, color, options, style);

            if (leatherOption != null)
            {
                matchedOptions.Add(leatherOption);
            }

            return matchedOptions.Distinct(new ChromeOptionComparer()).ToList();
        }

        private ChromeOption FindColorMatchedLeatherInterior(InventoryVehicleBuild build, ChromeColor color, IList<ChromeOption> options,
                                                             ChromeStyle style)
        {
            const string LEATHER = "leather";

            // Add the color-matched interior leather option if appropriate and we can find it.
            if (color != null && build.LeatherInterior)
            {
                // first try by matching the code
                var colorCodeMatchLeather = options.Where(o =>
                                                          _optionCodeComparer.Equals(o.OptionCode, color.IntManCode) &&
                                                          o.StyleId == style.StyleId &&
                                                          o.CountryCode == style.CountryCode &&
                                                          o.OptionDesc.ToLower().Contains(LEATHER) &&
                                                          o.Msrp >= 0 // set this to > 0 to restrict to non-standard
                                                         )
                                                   .ToList();

                if (colorCodeMatchLeather.Count > 0)
                {
                    return colorCodeMatchLeather.First();
                }

                // try to find it without the code - looking for an option with "leather" and the color name.
                var descMatchedLeather = options.Where(o =>
                                                              o.StyleId == style.StyleId &&
                                                              o.CountryCode == style.CountryCode &&
                                                              o.OptionDesc.ToLower().Contains(LEATHER) &&
                                                              o.Pon.ToLower().Contains(color.IntDesc.ToLower()) &&
                                                              o.Msrp >= 0 // set this to > 0 to restrict to non-standard
                                                             )
                                                       .ToList();

                if (descMatchedLeather.Count > 0)
                {
                    // which is best? the one with the shortest PON (we matched a higher % of the total characters in this string)
                    return descMatchedLeather.OrderBy(c => c.Pon.Length).First();
                }
            }

            return null;
        }

        private ChromeColor FindColor(InventoryVehicleBuild build, IList<ChromeColor> colors, ChromeStyle style)
        {
            // If the build's interior description contains the word "interior", remove it. It's not useful.
            string interiorColorName = build.InteriorColorName.ToLower().Replace("interior", string.Empty).Trim();

            var chromeColor = FindColor(build, colors, style, interiorColorName);

            if (chromeColor != null)
            {
                return chromeColor;
            }

            // look for matches on specific words in the interior color description
            return interiorColorName.Split(' ')
                .Select(word => FindColor(build, colors, style, word))
                .FirstOrDefault(result => result != null);

        }

        private ChromeColor FindColor(InventoryVehicleBuild build, IList<ChromeColor> colors, ChromeStyle style, string interiorColorName)
        {
            var matchedColors =
                colors.Where(c => (style == null || c.StyleId == style.StyleId && c.CountryCode == style.CountryCode) &&
                                  (c.Ext1MfrFullCode == build.ExteriorColorCode || c.Ext1ManCode == build.ExteriorColorCode)
                            )
                            .ToList();

            // however many records/objects we have, if there is just a single distinct pair of Ext1Desc, IntDesc, use them.
            var distinctMatchedColors = matchedColors.Select(c => new { c.Ext1Desc, c.IntDesc })
                                                     .Distinct()
                                                     .ToList();

            if (distinctMatchedColors.Count() == 1)
            {
                return matchedColors.First(c => c.Ext1Desc == distinctMatchedColors.First().Ext1Desc &&
                                                c.IntDesc == distinctMatchedColors.First().IntDesc);
            }

            // try looking at interior and exterior, liberally matching text for interior (using contains instead of ==)
            matchedColors = colors.Where(c =>
                                         (c.Ext1MfrFullCode == build.ExteriorColorCode || c.Ext1ManCode == build.ExteriorColorCode) &&
                                         (c.IntDesc.ToLower().Contains(interiorColorName) || interiorColorName.Contains(c.IntDesc))
                                        ).ToList();
            if (matchedColors.Any())
            {
                return matchedColors.First();
            }

            // Give up
            string message = string.Format("Vin:{0}, StyleId: {1}, ExteriorColorCode: {2}, InteriorColorName: {3}",
                                           build.Vin, style.StyleId,
                                           build.ExteriorColorCode, interiorColorName);

            Log.WarnFormat("Unable to determine color from build data for vehicle with data: {0}", message);
            return null;
        }


    }
}
