﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoLoad.FordNew.DataAccess;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace AutoLoad.FordNew.Domain
{
    /// <summary>
    /// This class processes messages of type FordDirectAutoLoadMessage. For each message it receives, it will collect
    /// data from the remote ford direct web service, decode it, and issue a VinDecodedMessage.  Its principal
    /// responsibilities are data acquisition and decoding.
    /// </summary>
    public class FordDirectAutoLoadMessageProcessor : TaskRunner<FordDirectAutoLoadMessage>
    {
        private const string PROCESS_NAME = "FordDirectAutoLoad";

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly BuildDataRepository _buildDataRepository;
        private readonly InventoryService _inventoryService;
        private readonly InventoryVehicleBuilder _builder;
        private readonly ChromeRepository _chromeRepository;
        private readonly ChromeStyleMatcher _matcher;

        // Injected dependencies
        public IQueueFactory QueueFactory { get; set; }
        public IFileStoreFactory FileStoreFactory { get; set; }

        public FordDirectAutoLoadMessageProcessor(IQueueFactory queueFactory, IFileStoreFactory fileStoreFactory)
            : base(queueFactory.CreateFordDirectAutoLoadMessageQueue())
        {
            _buildDataRepository = new BuildDataRepository(fileStoreFactory);
            _chromeRepository = new ChromeRepository();

            _inventoryService = new InventoryService();
            _builder = new InventoryVehicleBuilder();
            _matcher = new ChromeStyleMatcher();

            QueueFactory = queueFactory;
            FileStoreFactory = fileStoreFactory;
        }

        /// <summary>
        /// Process a message.
        /// </summary>
        /// <param name="message"></param>
        public override void Process(FordDirectAutoLoadMessage message)
        {
	        var threadId = string.Format("BusinessUnitId: {0}, DealerPaCode: {1}", message.BusinessUnitId, message.DealerPaCode);
			log4net.ThreadContext.Properties["threadId"] = threadId;

			//message.DealerPaCode
            Log.InfoFormat("Processing FordDirectAutoLoadMessage for BUID: {0}, PACODE: {1}, YEAR: {2}, MAKE: {3}, MODEL: {4}", 
                message.BusinessUnitId, message.DealerPaCode, message.Year, message.Make, message.Model);

            var ymm = new YearMakeModel {Make = message.Make, Model = message.Model, Year = message.Year};
            var dealerCode = new DealerCode
            {
                BusinessUnitId = message.BusinessUnitId,
                DealerPaCode = message.DealerPaCode
            };

            // The task processor base class has error handling logic to retry messages that generate exceptions.

            var builds = GetBuildData(ymm, dealerCode);


            // Decode each build that we found.
            foreach (var build in builds)
            {
                ProcessBuild(build);
            }
        }

        private List<InventoryVehicleBuild> GetBuildData(YearMakeModel ymm, DealerCode dealerCode)
        {
            // Get all the data from the ford service.
            var builds = new List<InventoryVehicleBuild>();

            Log.InfoFormat("Getting data for YMM: {0} {1} {2}", ymm.Year, ymm.Make, ymm.Model);

            var vehicleDetailsData = _inventoryService.GetVehicleDetails(ymm.Year, ymm.Make, ymm.Model,
                                                                         dealerCode.DealerPaCode);

            // Save the source docs.
            _buildDataRepository.SaveDocument(vehicleDetailsData, ymm, dealerCode);

            // Merge all the vehicle builds into one set.
            builds.AddRange(_builder.VehicleBuilds(vehicleDetailsData.Document));

            Log.InfoFormat("{0} Vehicle Builds found.", builds.Count);
            return builds;
        }

        private void ProcessBuild(InventoryVehicleBuild build)
        {
            // TODO: Move this to a different class / processor, responsible solely for decoding the data.
            Log.InfoFormat("Attempting to decode build data for vin {0}", build.Vin);

            ChromeStyle style;
            List<ChromeOption> options;
            ChromeColor color;

            DecodeVehicle(build, out style, out options, out color);

            // handle nulls
            if (options == null) options = new List<ChromeOption>();

            // get the options we want to advertise
            // TODO: Move this downstream. Should not be part of decoding.
            var optionsToAdvertise = (!options.Any())
                                         ? new List<ChromeOption>()
                                         : new OptionsToAdvertiseFilter().Filter(options).ToList();

            var logOptionsToAdvertise = new List<string>();
            optionsToAdvertise.ForEach(o => logOptionsToAdvertise.Add(string.Format("{0}=>{1}", o.OptionCode, o.Pon)));
            Log.InfoFormat("Options to be advertised: {0}", logOptionsToAdvertise.ToDelimitedString("|"));

            var logOptionsNotToAdvertise = new List<string>();
            var optionsNotAdvertised = options.Where(o => optionsToAdvertise.All(oa => oa.OptionCode != o.OptionCode)).ToList();
            optionsNotAdvertised.ForEach(o => logOptionsNotToAdvertise.Add(string.Format("{0}=>{1}", o.OptionCode, o.Pon)));
            Log.InfoFormat("Options NOT advertised: {0}", logOptionsNotToAdvertise.ToDelimitedString("|"));

            // Send the message to downstream subscriberse to save this decoded data.
            if(style != null) // rare case the we cannot find style FB: 31443
                SendVinDecodedMessage(build.Vin, style, optionsToAdvertise, color, string.Empty);
            else
                Log.InfoFormat("Cannot decode build data for VIN: {0}", build.Vin);
        
            // TODO: Send the URL to the S3 doc.
        }

        private void SendVinDecodedMessage(string vin, ChromeStyle style, IEnumerable<ChromeOption> options, ChromeColor color, string url)
        {
            Log.InfoFormat("Sending VinDecodedMessage for VIN {0}", vin);

            // Issue a SaveAutoLoad message
            var queue = QueueFactory.CreateVinDecodedQueue();
            var message = new VinDecodedMessage();

            message.Vin = vin;
            
            message.ChromeStyleId = style.StyleId;
            message.CountryCode = style.CountryCode;

            message.ExteriorManufacturerColorCode = color != null ? color.Ext1ManCode : string.Empty;
            message.ExteriorManufacturerColorDesc = color != null ? color.Ext1Desc : string.Empty ;

            message.InteriorManufacturerColorCode = color != null ? color.IntManCode : string.Empty;
            message.InteriorManufacturerColorDesc = color != null ? color.IntDesc : string.Empty; 

            string[] optionCodes = options.Select(o => o.OptionCode).ToArray();
            message.OptionCodes = optionCodes;

            message.SourceDataUrl = url;
            message.SourceProcessName = PROCESS_NAME;
            message.TimeStamp = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture);

            queue.SendMessage(message, GetType().FullName);
        }

        private void DecodeVehicle(InventoryVehicleBuild build,
                                    out ChromeStyle style, out List<ChromeOption> options, out ChromeColor color )
        {   
            Log.InfoFormat("Decoding vin {0}", build.Vin);

            // Get all possible chrome data (styles, options, color) for the vehicle, using a vin pattern match.
            // Try to zero in on the style using that data we have (wheelbase, model code, options)
            // If we get to a single style, or if we don't, audit our findings for future analysis.

            // Get all possible chrome data (styles, options, color) for the vehicle, using a vin pattern match.
            var styles = _chromeRepository.GetStyles(build.Vin)
                                          .Where(s => s.CountryCode == 1)
                                          .ToList(); // USA ONLY FOR NOW

            var distinctStyles = styles.Select(s => s.StyleId)
                                       .Distinct()
                                       .ToList();
            
            var possibleOptions = _chromeRepository.GetOptions(distinctStyles);
            var colors = _chromeRepository.GetColors(distinctStyles);

            ChromeStyle foundChromeStyle;
            IEnumerable<ChromeOption> foundOptions;
            ChromeColor foundColor;

            _matcher.Match( build, styles, possibleOptions, colors, out foundChromeStyle, out foundOptions, out foundColor);

            style = foundChromeStyle;
            options = foundOptions.ToList();
            color = foundColor;

            LogDecodedData(build, style, options, color);

            // Audit our findings for future analysis.
        }

        private static void LogDecodedData(InventoryVehicleBuild build, ChromeStyle style, List<ChromeOption> options, ChromeColor color)
        {
            // if chrome style is not found, display -1 for chrome style
            Log.InfoFormat(
                "Decoded vin: {0}, style: {1}, Decoded Options Count: {2}, Decoded options: {3}, Interior color: {4}, Exterior Color: {5}, Unidentified options: {6}",
                build.Vin, style != null ? style.StyleId : -1, GetDecodedOptionsCount(options), GetDecodedOptions(options), GetInteriorColor(color),
                GetExteriorColor(color), GetUnidentifiedOptions(options, build)
                );
        }

        private static string GetInteriorColor(ChromeColor color)
        {
            if (color == null) return string.Empty;
            if (color.IntDesc == null) return string.Empty;

            return color.IntDesc;
        }

        private static string GetExteriorColor(ChromeColor color)
        {
            if (color == null) return string.Empty;
            if (color.Ext1Desc == null) return string.Empty;

            return color.Ext1Desc;            
        }

        private static int GetDecodedOptionsCount(IEnumerable<ChromeOption> options)
        {
            // how many options did we decode?
            if (options == null) return 0;
            return options.Count();
        }

        private static string GetDecodedOptions(IEnumerable<ChromeOption> options)
        {
            var allOptions = new List<string>();
            foreach (var o in options.OrderBy(o => o.OptionCode))
            {
                allOptions.Add(string.Format("{0}=>{1}", o.OptionCode, o.Pon));
            }
            return allOptions.ToDelimitedString("|");
        }

        public static string GetUnidentifiedOptions(IList<ChromeOption> options, InventoryVehicleBuild build)
        {
            if (options == null || build == null || build.Options == null) return "Unidentified Options: N/A";

            // Log unidentified options.
            var unidentifiedOptions = new List<string>();
            var comparer = new OptionCodeComparer();

            foreach (var bldOption in build.Options.OrderBy(o => o))
            {
                if (!options.Select(o => o.OptionCode).Contains(bldOption, comparer))
                {
                    unidentifiedOptions.Add(bldOption);
                }
            }

            return String.Format("Unidentified Options: {0}", unidentifiedOptions.ToDelimitedString("|"));
        }
        
    }
}
