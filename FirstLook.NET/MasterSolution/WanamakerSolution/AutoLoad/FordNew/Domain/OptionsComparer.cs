using System;
using System.Collections.Generic;
using AutoLoad.FordNew.DataAccess;

namespace AutoLoad.FordNew.Domain
{
    /// <summary>
    /// Option comparison delegates to option code comparison.
    /// </summary>
    public class ChromeOptionComparer : IEqualityComparer<ChromeOption>
    {
        private readonly OptionCodeComparer _optionCodeComparer;

        public ChromeOptionComparer()
        {
            _optionCodeComparer = new OptionCodeComparer();
        }

        public bool Equals(ChromeOption x, ChromeOption y)
        {
            return _optionCodeComparer.Equals(x.OptionCode, y.OptionCode);
        }

        public int GetHashCode(ChromeOption obj)
        {
            return _optionCodeComparer.GetHashCode(obj.OptionCode);
        }
    }

    /// <summary>
    /// Compare option codes.
    /// </summary>
    public class OptionCodeComparer : IEqualityComparer<string>
    {
        // We have some custom logic around the "-" character
        public bool Equals(string x, string y)
        {
            if (string.IsNullOrWhiteSpace(x) || string.IsNullOrWhiteSpace(y)) return false;
            if( (x.StartsWith("-") && !y.StartsWith("-") ) || (!x.StartsWith("-") && y.StartsWith("-") ) ) return false;

            // check for exact match first
            if (x.Trim().ToLower() == y.Trim().ToLower())
                return true;

            // strip everything after the dash and compare
            return x.Split('-')[0].Trim().ToLower() == y.Split('-')[0].Trim().ToLower();

        }

        public int GetHashCode(string obj)
        {
            // careful, need to return the same hash for "similar" code or distinct linq statements will have undesired results
            // we are possible returning multiply matches from Chrome for a build option ... need to dedupe with comparer

            if (obj == null)
                return 0;
            
            if (String.IsNullOrWhiteSpace(obj))
                return obj.GetHashCode();
            
            return obj.Split('-')[0].GetHashCode();
        }
    }
}