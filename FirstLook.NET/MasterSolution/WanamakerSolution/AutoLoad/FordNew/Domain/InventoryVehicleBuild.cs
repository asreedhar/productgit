using System.Collections.Generic;

namespace AutoLoad.FordNew.Domain
{
    public class InventoryVehicleBuild
    {

        public InventoryVehicleBuild()
        {
            ExteriorColorCode = string.Empty;
            ModelCode = string.Empty;
            PreferredEquipmentPackage = string.Empty;
            Product = string.Empty;
            StockNumber = string.Empty;
            Vin = string.Empty;
            VehicleStage = string.Empty;
            DealerPaCode = string.Empty;
            ImageToken = string.Empty;
            ConfigToken = string.Empty;
            WindowStickerUrl = string.Empty;
            Make = string.Empty;
            Model = string.Empty;
            Year = string.Empty;
            WheelBase = string.Empty;

            Options = new List<string>();
            Parts = new List<string>();
            Pricing = new PricingData();

            InteriorColorName = string.Empty;
            LeatherInterior = false;
            AutomaticTransmission = false;
            DriveTrain = string.Empty;
            Trim = string.Empty;
        }


        public string ExteriorColorCode { get; set; }
        public string ModelCode { get; set; }
        public string PreferredEquipmentPackage { get; set; }

        public string Product { get; set; }
        public string StockNumber { get; set; }
        public string Vin { get; set; }
        public string VehicleStage { get; set; }
        public string DealerPaCode { get; set; }

        public string ImageToken { get; set; }
        public string ConfigToken { get; set; }
        public string WindowStickerUrl { get; set; }

        public string Make { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string WheelBase { get; set; }
/*
        public string Cab { get; set; }
        public string Series { get; set; }
*/

        public IEnumerable<string> Options { get; set; }
        public IEnumerable<string> Parts { get; set; }

        public PricingData Pricing { get; set; }

        public string InteriorColorName { get; set; }
        public bool LeatherInterior { get; set; }

        public bool AutomaticTransmission { get; set; }

        public string DriveTrain { get; set; }

        public string Trim { get; set; }

        public class PricingData
        {
            //      <PlanApplicability>
            //        <AZ>true</AZ>
            //        <FNX>true</FNX>
            //        <PRX>true</PRX>
            //        <SPX>true</SPX>
            //      </PlanApplicability>
            public class PlanApplicabilityData
            {
                public string AZ { get; set; }
                public string FNX { get; set; }
                public string PRX { get; set; }
                public string SPX { get; set; }
            }

            public PlanApplicabilityData PlanApplicability { get; set; }

            public string DealerPrice { get; set; }
            public string BaseMsrp { get; set; }
            public string BaseInvoice { get; set; }
            public string BaseAzPlan { get; set; }
            public string BaseXPlan { get; set; }
            public string DestinationDeliveryCharge { get; set; }
            public string Options { get; set; }
            public string OptionsAzPlan { get; set; }
            public string OptionsXPlan { get; set; }
            public string Msrp { get; set; }
            public string MsrpDiscount { get; set; }
            public string Invoice { get; set; }
            public string AzPlan { get; set; }
            public string XPlan { get; set; }
            public string XPlanDiscount { get; set; }
            public string AzPlanDiscount { get; set; }
            public string AzPlanDocFee { get; set; }
            public string XPlanDocFee { get; set; }
            public string ResidualizableMsrp { get; set; }
            public string ResidualizableAzPlan { get; set; }
            public string ResidualizableXPlan { get; set; }
            public string ResidualizableInvoice { get; set; }
            public string Adjustments { get; set; }

        }


    }
}