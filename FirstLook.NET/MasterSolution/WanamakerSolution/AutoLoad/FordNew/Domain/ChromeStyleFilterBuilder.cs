﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoLoad.FordNew.DataAccess;

namespace AutoLoad.FordNew.Domain
{
    internal static class ChromeStyleFilterBuilder
    {
        private const string AVAILABLE_AS_STANDARD_TEXT = "S";
        private const string AVAILABLE_AS_OPTIONAL_TEXT = "O";

        private static bool IsFwd(string test)
        {
            // different ways of saying "front wheel drive"
            var synonyms = new List<string> { "fwd" };
            return synonyms.Contains(test);
        }

        private static bool IsAwd(string test)
        {
            // different ways of saying "all wheel drive"
            var synonyms = new List<string> { "awd" };
            return synonyms.Contains(test);
        }

        private static bool IsRwd(string test)
        {
            // different ways of saying "rear wheel drive"
            var synonyms = new List<string> { "rwd", "4x2" };
            return synonyms.Contains(test);
        }

        private static bool Is4Wd(string test)
        {
            // different ways of saying "4 wheel drive"
            var synonyms = new List<string> { "4wd", "4x4" };
            return synonyms.Contains(test);
        }

        /// <summary>
        /// Build up a list of filters. By applying these filters to a style & build, we can
        /// determine whether the style is the correct one to use for the build.
        /// </summary>
        /// <param name="build"></param>
        /// <returns></returns>
        internal static List<ChromeStyleFilter> Build(InventoryVehicleBuild build)
        {
            //
            // Style filters based on comparing the vehicle build with the possible styles.
            //
            var styleFilters = new List<ChromeStyleFilter>();
            styleFilters.Add(new ChromeStyleFilter
                {
                    Predicate = s =>
                                s.StyleCode.Trim().Length > 0 &&
                                build.ModelCode.Trim().Length > 0 &&
                                s.StyleCode.Trim().ToLower() == build.ModelCode.Trim().ToLower(),
                    Id = "Style.StyleCode==Build.ModelCode"
                });

            styleFilters.Add( new ChromeStyleFilter
            {
                Predicate=   s =>
                             !String.IsNullOrWhiteSpace(s.FullStyleCode) &&
                             !String.IsNullOrWhiteSpace(build.ModelCode) &&
                             s.FullStyleCode.Trim().ToLower() == build.ModelCode.Trim().ToLower(),
                Id="Style.FullStyleCode==Build.ModelCode"
            });

            styleFilters.Add( new ChromeStyleFilter
            {
                Predicate=
                            s =>
                            !String.IsNullOrWhiteSpace(build.WheelBase) &&
                            s.WheelBases.Count(v => v.ToString("###") == build.WheelBase.Trim()) > 0,  // 3 characters only: 123.45 => "123"                        
                Id="Style.WheelBase==Build.WheelBase"
            });

            styleFilters.Add(new ChromeStyleFilter
            {
                Predicate=
                            s =>
                            !String.IsNullOrWhiteSpace(s.Trim) &&
                            !String.IsNullOrWhiteSpace(build.Model) &&
                            s.Trim.Trim().ToLower() == build.Model.Trim().ToLower(),
                Id="Style.Trim==Build.Model"
            });

            styleFilters.Add(new ChromeStyleFilter
            {
                Predicate=
                            s =>
                            !String.IsNullOrWhiteSpace(s.Trim) &&
                            !String.IsNullOrWhiteSpace(build.Trim) &&
                            s.Trim.Trim().ToLower() == build.Trim.Trim().ToLower(),
                Id="Style.Trim==Build.Trim"
            });
            // Trim & Wheelbase, a good one for trucks.
            styleFilters.Add(new ChromeStyleFilter
            {
                Predicate=
                            s =>
                            !String.IsNullOrWhiteSpace(s.Trim) &&
                            !String.IsNullOrWhiteSpace(build.Trim) &&
                            (s.Trim.Trim().ToLower() == build.Trim.Trim().ToLower()) &&
                            (
                                !String.IsNullOrWhiteSpace(build.WheelBase) &&
                                s.WheelBases.Count(v => v.ToString("###") == build.WheelBase.Trim()) > 0
                            ),
                Id="Style.WheelBase=Build.WheelBase && Style.Trim==Build.Trim"
            });

            // transmissions
            styleFilters.Add(new ChromeStyleFilter
            {
                Predicate=   
                            s =>
                            (s.AutoTrans == AVAILABLE_AS_STANDARD_TEXT || s.AutoTrans == AVAILABLE_AS_OPTIONAL_TEXT) &&
                            build.AutomaticTransmission,
                Id="Style.AutoTrans~=Build.AutomaticTransmission"
            });

            styleFilters.Add( new ChromeStyleFilter
            {
               Predicate= 
                            s =>
                            (s.ManualTrans == AVAILABLE_AS_STANDARD_TEXT || s.ManualTrans == AVAILABLE_AS_OPTIONAL_TEXT) &&
                            !build.AutomaticTransmission,
               Id="Style.ManualTrans~=build.AutomaticTransmission"
            });

            // drivetrains
            styleFilters.Add( new ChromeStyleFilter
            {
                Predicate=
                            s =>
                            (s.FrontWd == AVAILABLE_AS_STANDARD_TEXT || s.FrontWd == AVAILABLE_AS_OPTIONAL_TEXT) &&
                            IsFwd(build.DriveTrain),
                Id="Style.FrontWd~=Build.DriveTrain"
            });

            styleFilters.Add(new ChromeStyleFilter
            {
                Predicate=
                            s =>
                            (s.RearWd == AVAILABLE_AS_STANDARD_TEXT || s.RearWd == AVAILABLE_AS_OPTIONAL_TEXT) &&
                            IsRwd(build.DriveTrain),
                Id="Style.RearWd~=Build.DriveTrain"
            });

            styleFilters.Add( new ChromeStyleFilter 
            {
                Predicate=
                            s =>
                            (s.FourWd == AVAILABLE_AS_STANDARD_TEXT || s.FourWd == AVAILABLE_AS_OPTIONAL_TEXT) &&
                            Is4Wd(build.DriveTrain),
                Id="Style.FourWd~=Build.DriveTrain"
            });

            styleFilters.Add(new ChromeStyleFilter
            {
                Predicate = 
                            s =>
                            (s.AllWd == AVAILABLE_AS_STANDARD_TEXT || s.AllWd == AVAILABLE_AS_OPTIONAL_TEXT) &&
                            IsAwd(build.DriveTrain),
                Id="Style.AllWd~=Build.DriveTrain"
            });

            // FB:28797 filter added for trucks, model code, wheel base and trim is similar to chrome's trim
            styleFilters.Add(new ChromeStyleFilter
                {
                    Predicate =
                        s =>
                        (
                            !String.IsNullOrWhiteSpace(s.Trim) &&
                            !String.IsNullOrWhiteSpace(build.Trim) &&
                            build.Trim.Trim().ToLower().Contains(s.Trim.Trim().ToLower())
                        ) && // product name from xml contains chrome's trim attribute from styles
                        (
                            !String.IsNullOrWhiteSpace(build.WheelBase) &&
                            s.WheelBases.Count(v => v.ToString("###") == build.WheelBase.Trim()) > 0
                        ) && // wheel base from xml is in chromes wheelbase for a given style
                        (
                            !String.IsNullOrWhiteSpace(s.FullStyleCode) &&
                            !String.IsNullOrWhiteSpace(build.ModelCode) &&
                            s.FullStyleCode.Trim().ToLower() == build.ModelCode.Trim().ToLower()
                        ) && // model code from the xml is equal to chrome's fullStyleCode
                        (
                            !String.IsNullOrWhiteSpace(s.MarketClass) &&
                            s.MarketClass.Trim().ToLower().Contains("truck")
                        ) // market class has 'truck'
                    ,
                    Id = "s.Trim.Contained(build.Trim)&&WheelBase=WheelBase&&s.FullStyleCode==BuildModelCode"

                });
            return styleFilters;
        }
    }

    internal class ChromeStyleFilter
    {
        public Func<ChromeStyle, bool> Predicate { get; set; }
        public string Id { get; set; }
    }
}
