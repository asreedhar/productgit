﻿using System;
using System.Xml.Linq;

namespace AutoLoad.FordNew.Domain
{
    public class InventoryService
    {
        private readonly ServiceProxy _proxy;
        private const string SERVICE_BASE = "http://services.forddirect.fordvehicles.com/inventory/VehicleDetails";

        // this requests additional attributes from the service which allow us to figure out interior color
        private const string INTERIOR_COLOR =
            "returnAttributes=DC_InteriorColor_Name_EN;DC_InteriorColor_PartNumber;SI3_Filters_InteriorColor;SI3_Filters_Transmission;SI3_Filters_Drive;SI3_Trim_Definers;BP2_VehicleName_NamePlate;IQQV2_ModelSlices";

        public InventoryService()
        {
            _proxy = new ServiceProxy();
        }

        public VehicleDetails GetVehicleDetails(int year, string make, string model, string dealerPaCode)
        {
            // make=Ford&year=2013&model=Taurus&dealerPACode=09775
            string url = String.Format("{0}?make={1}&year={2}&model={3}&dealerPACode={4}&{5}", 
                SERVICE_BASE, make, year, model, dealerPaCode, INTERIOR_COLOR);

            XDocument doc = _proxy.DownloadData(url);

            VehicleDetails vd = new VehicleDetails();
            vd.DealerPaCode = dealerPaCode;
            vd.Document = doc;
            vd.Make = make;
            vd.Model = model;
            vd.Url = url;
            vd.Year = year;

            return vd;
        }
    }

    public class VehicleDetails
    {
        public XDocument Document { get; set; }
        public string Url { get; set; }

        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string DealerPaCode { get; set; }
    }
}
