﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Xml.Linq;

namespace AutoLoad.FordNew.Domain
{
    public class ServiceProxy
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public XDocument DownloadData(string site)
        {
            try
            {
                using (var client = new WebClient())
                {
                    // IE 6
                    /*
                                    client.Headers["User-Agent"] =
                                    "Mozilla/4.0 (Compatible; Windows NT 5.1; MSIE 6.0) " +
                                    "(compatible; MSIE 6.0; Windows NT 5.1; " +
                                    ".NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                    */

                    string proxyAddress = ConfigurationManager.AppSettings["ProxyAddress"];
                    var chunks = proxyAddress.Split(':');
                    var ip = chunks[0];
                    var port = Int32.Parse(chunks[1]);
                       
                    client.Proxy = new WebProxy(ip, port);

                    // Download data.
                    byte[] arr = client.DownloadData(site);

                    // convert it to an XDocument
                    using (var stream = new MemoryStream(arr, false))
                    {
                        var doc = XDocument.Load(stream);
                        return doc;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new XDocument();
            }
        }

    }
}
