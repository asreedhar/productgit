﻿namespace MAX.Entities
{
    public class BucketAlertThreshold
    {
        public int Id { get; set; }
        public int BusinessUnitId { get; set; }
        public bool Active { get; set; }
        public WorkflowType Bucket { get; set; }
        public int Limit { get; set; }
        // FB: 27993 - separate dashboard and email alerts
        public bool EmailActive { get; set; }
    }
}
