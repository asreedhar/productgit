﻿using System;
using Core.Messages;

namespace MAX.Entities.Messages
{
    public class GroupBionicBaseMessage : MessageBase
    {
        public Int32 GroupId { get; set; }
        public String EmailAddresses { get; set; }
        public String TrackingSnip { get; set; }

        protected GroupBionicBaseMessage(Int32 groupId, String emailAddresses, String trackingSnip)
        {
            GroupId = groupId;
            EmailAddresses = emailAddresses;
            TrackingSnip = trackingSnip;
        }
    }
}
