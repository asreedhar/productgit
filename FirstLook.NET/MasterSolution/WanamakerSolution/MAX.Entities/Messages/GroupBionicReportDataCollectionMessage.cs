﻿using System;

namespace MAX.Entities.Messages
{
    public class GroupBionicReportDataCollectionMessage : GroupBionicBaseMessage
    {
        public GroupBionicReportDataCollectionMessage(Int32 groupId, String emailAddresses, String trackingSnip)
            : base(groupId, emailAddresses, trackingSnip)
        {
        }
    }
}
