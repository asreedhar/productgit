﻿using System;
using Core.Messages;

namespace MAX.Entities.Messages
{
    public class BionicReportImageCreationMessage : BionicBaseMessage
    {
        public String DataFileKey { get; set; }

        public BionicReportImageCreationMessage(Int32 businessUnitId, String emailAddresses, String trackingSnip, String dataFileKey)
        {
            BusinessUnitId = businessUnitId;
            EmailAddresses = emailAddresses;
            DataFileKey = dataFileKey;
            TrackingSnip = trackingSnip;
        }
    }
}
