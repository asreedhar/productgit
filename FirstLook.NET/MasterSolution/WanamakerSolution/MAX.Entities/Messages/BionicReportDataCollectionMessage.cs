﻿using System;
using Core.Messages;

namespace MAX.Entities.Messages
{
    public class BionicReportDataCollectionMessage : BionicBaseMessage
    {

        public BionicReportDataCollectionMessage(Int32 businessUnitId, String emailAddresses, String trackingSnip)
        {
            BusinessUnitId = businessUnitId;
            EmailAddresses = emailAddresses;
            TrackingSnip = trackingSnip;
        }
    }
}
