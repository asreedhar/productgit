﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messages;

namespace MAX.Entities.Messages
{
    public class BionicBaseMessage : MessageBase
    {
        public Int32 BusinessUnitId { get; set; }
        public String EmailAddresses { get; set; }
        public String TrackingSnip { get; set; }
    }
}
