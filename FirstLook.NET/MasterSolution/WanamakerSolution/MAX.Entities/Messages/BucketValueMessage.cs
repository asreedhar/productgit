﻿using System;
using Core.Messages;

namespace MAX.Entities.Messages
{
    public class BucketValueMessage : MessageBase
    {
        public Int32 BusinessUnitId { get; set; }
        public String BusinessName { get; set; }
        public WorkflowType Bucket { get; set; }
        public String BucketDescription { get; set; }
        public DateTime Date { get; set; }
        public Int32 NewCount { get; set; }
        public Int32 UsedCount { get; set; }

        public BucketValueMessage(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }
    }
}
