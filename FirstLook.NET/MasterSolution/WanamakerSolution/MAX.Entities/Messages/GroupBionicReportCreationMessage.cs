﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAX.Entities.Messages
{
    public class GroupBionicReportCreationMessage : GroupBionicBaseMessage
    {
        public String DataFileKey { get; set; }

        public GroupBionicReportCreationMessage(Int32 groupId, String emailAddresses, String trackingSnip, String dataFileKey)
            : base(groupId, emailAddresses, trackingSnip)
        {
            DataFileKey = dataFileKey;
        }
    }
}
