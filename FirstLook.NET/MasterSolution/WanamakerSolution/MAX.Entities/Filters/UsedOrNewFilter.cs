namespace MAX.Entities.Filters
{
    public enum UsedOrNewFilter { Used_and_New = 0, New = 1, Used = 2 }
}
