﻿using System;
using FirstLook.Common.Core;
using MAX.Entities.Reports.TimeToMarket;

namespace MAX.Entities
{
    [Serializable]
    public class GroupBionicReportData
    {
        public int GroupId;
        public string EmailAddresses;
        public DateRange ReportDateRange;
        public string DMSName;
        public string DMSEmail;
        public int TimeToMarketGoalDays;
        public VehicleType VehicleType;
        public GroupReport TimeToMarketGroupReport;

        public GroupBionicReportData()
        {
            GroupId = 0;
            EmailAddresses = String.Empty;
            DMSName = String.Empty;
            DMSEmail = String.Empty;
            TimeToMarketGoalDays = 0;
            VehicleType = VehicleType.Used;
            TimeToMarketGroupReport = new GroupReport();
        }

        public GroupBionicReportData(int groupId) : this()
        {
            GroupId = groupId;
        }
    }

    public class EmptyGroupBionicReportData : GroupBionicReportData
    {}
}
