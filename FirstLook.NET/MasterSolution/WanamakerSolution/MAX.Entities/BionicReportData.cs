﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using MAX.Entities.Reports.TimeToMarket;

namespace MAX.Entities
{
    [Serializable]
    public class BionicReportData
    {
        public int BusinessUnitId;
        public string EmailAddresses;
        public DateRange ReportDateRange;
        public IList<BucketValue> BucketValues;
        public BusinessUnitReport TimeToMarketReport;
        public string DMSName;
        public string DMSEmail;
        public int TimeToMarketGoalDays;
        public VehicleType VehicleType;

        public BionicReportData()
        {
            BusinessUnitId = 0;
            EmailAddresses = String.Empty;
            BucketValues = new List<BucketValue>();
            TimeToMarketReport = new BusinessUnitReport();
            DMSName = String.Empty;
            DMSEmail = String.Empty;
            TimeToMarketGoalDays = 0;
            VehicleType = VehicleType.Used;
        }

        public BionicReportData(int businessUnitId) : this()
        {
            BusinessUnitId = businessUnitId;
        }
    }

	public class EmptyBionicReportData : BionicReportData 
	{}
}
