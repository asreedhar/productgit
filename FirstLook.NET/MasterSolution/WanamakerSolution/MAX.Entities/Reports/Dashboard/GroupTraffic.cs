﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MAX.Entities.Reports.Dashboard
{
    [DataContract]
    public class GroupTraffic
    {
        [DataMember]
        public List<IDashboardResponseModel> DealershipTrafficList { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        List<string> Errors { get; set; }

        public GroupTraffic(int groupId)
        {
            GroupId = groupId;
            DealershipTrafficList = new List<IDashboardResponseModel>();
            Errors = new List<string>();
        }
    }
}
