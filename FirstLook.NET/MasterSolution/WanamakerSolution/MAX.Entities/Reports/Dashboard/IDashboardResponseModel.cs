﻿using System.Collections.Generic;

namespace MAX.Entities.Reports.Dashboard
{
    public interface IDashboardResponseModel
    {
        /// <summary>
        /// User friendly errors to be displayed in the UI
        /// 'Errors' is explicitly skipped when enumerating data sets for dashboard charts.
        /// </summary>
        List<string> Errors { get; set; }
        /// <summary>
        /// The business unit that the data model belongs to
        /// </summary>
        int BusinessUnitId { get; set; }
        string DealershipName { get; set; }
    }
}
