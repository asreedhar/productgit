﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using MAX.Entities.Helpers.DashboardHelpers;

namespace MAX.Entities.Reports.Dashboard
{
    /// <summary>
    /// Model used by dashboard UI.
    /// Any renaming should be accounted for in consumers of this model (Javascript etc.)
    /// </summary>
    [DataContract]
    public class WebTraffic : IDashboardResponseModel
    {
        #region Properties
        [DataMember]
        public int BusinessUnitId { get; set; }
        [DataMember]
        public string BusinessUnitHome { get { return Helper.RewriteDealerSpecificURL(BusinessUnitId, "Default.aspx?ref=GLD"); } }
        [DataMember]
        public string DealershipName { get; set; }
        [DataMember]
        public TimeSpan AvgTimeOnPage { get; set; }
        [DataMember]
        public double AvgTimeOnPageMilliseconds { get { return AvgTimeOnPage.TotalMilliseconds; } }
        [DataMember]
        public double PageViews { get; set; }
        [DataMember]
        public double TotalDirectLeads { get; set; }
        [DataMember]
        public double TotalActions { get; set; }
        [DataMember]
        public List<string> Errors { get; set; }
        #endregion

        #region Methods
        #region Construction
        private WebTraffic()
        {
            AvgTimeOnPage = new TimeSpan();
            PageViews = 0;
            TotalDirectLeads = 0;
            TotalActions = 0;

            Errors = new List<string>();
        }

        public WebTraffic(int businessUnitId) : this()
        {
            BusinessUnitId = businessUnitId;
        }

        public WebTraffic(int businessUnitId, string dealershipName) : this(businessUnitId)
        {
            DealershipName = dealershipName;
        }
        #endregion

        public void Clone(WebTraffic source)
        {
            Debug.Assert(BusinessUnitId == source.BusinessUnitId);
            // should throw an error here if they aren't the same?

            DealershipName = source.DealershipName;
            AvgTimeOnPage = source.AvgTimeOnPage;
            PageViews = source.PageViews;
            TotalDirectLeads = source.TotalDirectLeads;
            TotalActions = source.TotalActions;

            Errors.Clear();
            Errors.AddRange(source.Errors);
        }
        #endregion
    }
}
