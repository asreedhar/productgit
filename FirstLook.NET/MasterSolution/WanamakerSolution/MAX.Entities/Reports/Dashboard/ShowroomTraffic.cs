﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using MAX.Entities.Helpers.DashboardHelpers;

namespace MAX.Entities.Reports.Dashboard
{
    /// <summary>
    /// Model used by dashboard UI.
    /// Any renaming should be accounted for in consumers of this model (Javascript etc.)
    /// 
    /// The dates in these objects are used for Highcharts Javascript.
    /// Highcharts requires the data to be ordered properly.
    /// To do a little heavy lifting, we are going to pre-sort dates (- OrderBy -). Javascript could override it later.
    /// 
    /// </summary>
    [DataContract]
    public class ShowroomTraffic : IDashboardResponseModel
    {
        public class DateValue
        {
            public DateTime Time { get; set; }
            public double Count { get; set; }
        }

        #region Properties
        [DataMember]
        public int BusinessUnitId { get; set; }
        [DataMember]
        public string DealershipName { get; set; }
        [DataMember]
        public int Shares { get; set; }
        [DataMember]
        public int Prints { get; set; }

        [IgnoreDataMember]
        public List<DateValue> DigitalShowroom = new List<DateValue>();
        [DataMember(Name="DigitalShowroom")]
        public List<DateValue> OrderedDigitalShowroom 
        {
            get { return DigitalShowroom.OrderBy(o => o.Time).ToList(); }
        }

        [IgnoreDataMember]
        public List<DateValue> MobileShowroom = new List<DateValue>();
        [DataMember(Name="MobileShowroom")]
        public List<DateValue> OrderedMobileShowroom
        {
            get { return MobileShowroom.OrderBy(o => o.Time).ToList(); }
        }

        [DataMember]
        public List<string> Errors { get; set; }

        [DataMember]
        public string BusinessUnitHome { get { return Helper.RewriteDealerSpecificURL(BusinessUnitId, "Default.aspx?ref=GLD"); } }
        [DataMember]
        public double DigitalShowroomCombinedCount { get { return DigitalShowroom.Sum(o => o.Count); } }
        [DataMember]
        public double MobileShowroomCombinedCount { get { return MobileShowroom.Sum(o => o.Count); } }
        [DataMember]
        public List<DateValue> Combined
        {
            get
            {
                if (DigitalShowroom != null && MobileShowroom != null)
                {
                    var uniqueDates = DigitalShowroom.Union(MobileShowroom).Select(i => i.Time).Distinct();
                    var union = DigitalShowroom.Union(MobileShowroom);
                    var result = new List<DateValue>();
                    foreach (var date in uniqueDates)
                    {
                        var existingValues = union.Where(i => i.Time.Equals(date));
                        double sum = 0.0;
                        foreach (var value in existingValues)
                        {
                            sum += value.Count;
                        }
                        result.Add(new DateValue { Count = sum, Time = date });
                    }
                    return result.OrderBy(o => o.Time).ToList();
                }
                return new List<DateValue>();
            }
        }
        [DataMember]
        public double CombinedCount {
            get { return Combined.Sum(o => o.Count); }
        }
        #endregion

        #region Methods
        #region Construction
        private ShowroomTraffic()
        {
            Errors = new List<string>();
            DigitalShowroom = new List<DateValue>();
            MobileShowroom = new List<DateValue>();
        }

        public ShowroomTraffic(int businessUnitId)
            : this()
        {
            BusinessUnitId = businessUnitId;
        }

        public ShowroomTraffic(int businessUnitId, string dealershipName)
            : this(businessUnitId)
        {
            DealershipName = dealershipName;
        }
        #endregion

        public void Clone(ShowroomTraffic source)
        {
            Debug.Assert(BusinessUnitId == source.BusinessUnitId);
            // should throw an error here if they aren't the same?

            DealershipName = source.DealershipName;
            Shares = source.Shares;
            Prints = source.Prints;

            DigitalShowroom.Clear();
            DigitalShowroom.AddRange(source.DigitalShowroom);

            MobileShowroom.Clear();
            MobileShowroom.AddRange(source.MobileShowroom);

            Errors.Clear();
            Errors.AddRange(source.Errors);
        }
        #endregion
    }
}
