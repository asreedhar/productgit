﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
//using MAX.Entities.Reports.TimeToMarket;
using MAX.Entities.Reports.TimeToMarket.Items;

namespace MAX.Entities.Reports.TimeToMarket
{
    [Serializable]
    public class GroupReport
    {
        #region fields

        private static readonly ILog Log = LoggerFactory.GetLogger<GroupReport>();
        private List<BusinessUnitReport> _businessUnits = new List<BusinessUnitReport>();
        
        #endregion fields

        #region Properties

        public int GroupId { get; set; }
        public DateRange DateRange { get; private set; }
        
        public int VehicleCount
        {
            get { return (BusinessUnits.Count == 0) ? 0 : BusinessUnits.Sum(buv => buv.VehicleCount); }
        }
        
        public List<BusinessUnitReport> BusinessUnits
        {
            get
            { 
                return _businessUnits; 
            }
            set 
            { 
                _businessUnits = value; 
            }
        }
        
        public double AverageDaysToPhotoOnline
        {
            get 
            {
                double businessUnitsAverageDays = BusinessUnits.Select(bu => bu.AverageDaysToPhotoOnline).Sum();
                var businessUnitsToCount = BusinessUnits.Count();

                // That's right, we're averaging the averages.
                // BUGZID: 26735
                return (businessUnitsToCount == 0) ? 0 : businessUnitsAverageDays / businessUnitsToCount;
            }
        }

        public double AverageDaysToCompleteAdOnline
        {
            get
            {
                // That's right, we're averaging the averages.
                // BUGZID: 26735
                var businessUnitsCompleteAdDays = BusinessUnits.Select(bu => bu.AverageDaysToCompleteAd).Sum();
                var businessUnitsToCount = BusinessUnits.Count();

                return (businessUnitsToCount == 0) ? 0 : businessUnitsCompleteAdDays / businessUnitsToCount;
            }
        }

        #endregion Properties

        #region Constructors

        public GroupReport() { }
        
        public GroupReport(int groupId, DateRange dateRange, DataTable data)
        {
            GroupId = groupId;
            DateRange = dateRange;

            for(var x = 0; x < data.Rows.Count; x++)
            {
                try
                {
                    DataRow dataRow = data.Rows[x];

                    int buid = Convert.ToInt32( dataRow["BusinessUnitId"] );
                    BusinessUnitReport bu = BusinessUnits.FirstOrDefault(test => test.BusinessUnitId == buid);
                    if (bu == null)
                    {
                        bu = new BusinessUnitReport {DateRange = DateRange};
                        BusinessUnits.Add(bu);
                    }
                    bu.Vehicles.Add(new ReportVehicle(dataRow));
                }
                catch(Exception e)
                {
                    Log.ErrorFormat("Error loading groupReport: {0}", e);
                }
            }
        }

        #endregion

    }
}