﻿namespace MAX.Entities.Reports.Bionic
{
	public enum ReportImageType
	{
		TimeToMarketComplete,
		TimeToMarketFirstPhoto,
		VehiclesWithNoPhotos,
		VehiclesWithNoPrice,
		VehiclesNotOnline
	}
}