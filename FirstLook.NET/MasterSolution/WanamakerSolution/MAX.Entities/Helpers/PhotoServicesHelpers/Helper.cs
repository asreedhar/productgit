﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using log4net;

namespace MAX.Entities.Helpers.PhotoServicesHelpers
{
    public class Helper
    {
        #region Logging
        private readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public VehiclesPhotosResult Parse(string xml)
        {
            var doc = XDocument.Parse(xml, LoadOptions.PreserveWhitespace);

            var photos = doc.Descendants("vinPhotos").Select(x => new VehiclesPhotosResult
            {
                Vehicles = x.Descendants("vinPhoto").Select(v => new VehiclePhotos
                {
                    Vin = v.Element("vin").Value,
                    PhotoUrls = v.Descendants("url").Select(u => u.Value).ToArray()
                }).ToArray(),
                Errors = new Error[0]
            });

            return photos.First();
        }

        public Dictionary<String, int> ParseVinCounts(string xml)
        {
            _log.InfoFormat("ParseVinCounts(xml.Length: {0}) started.", xml.Length);
            var startTime = DateTime.UtcNow;
            var results = new Dictionary<String, int>();

            try
            {
                var doc = XDocument.Parse(xml, LoadOptions.PreserveWhitespace);
                var vinPhotos = doc.Descendants("vinPhotos").FirstOrDefault(); // only retrieve the first business unit's counts
                if (vinPhotos != null)
                {
                    results = vinPhotos.Descendants("vinPhoto")
                        .Select(vinPhoto => new
                        {
                            vinElement = vinPhoto.Element("vin"),
                            photoUrls = vinPhoto.Descendants("url").Select(url => url.Value)
                        })
                        .Where(x => x.vinElement != null && !String.IsNullOrWhiteSpace(x.vinElement.Value))
                        .ToDictionary(x => x.vinElement.Value, x => x.photoUrls != null ? x.photoUrls.Count() : 0);
                }
            }
            catch (Exception ex)
            {
                _log.Error(String.Format("ParseVinCounts() failed. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime)), ex);
                throw;
            }

            _log.InfoFormat("ParseVinCounts() completed. timeElapsed: {0}", DateTime.UtcNow.Subtract(startTime));
            return results;
        }
    }
}
