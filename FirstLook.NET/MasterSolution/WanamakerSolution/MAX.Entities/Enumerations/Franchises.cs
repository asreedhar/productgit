﻿namespace MAX.Entities.Enumerations
{
    public enum Franchise
    {
        /* NOTE: These numeric values are used as FranchiseId values in the database.
         * Please do not change the numeric value of any of these items. Only add new items to the end of the list.
         */

        /* NOTE: An entry should be added to the Merchandising.settings.Franchise table for each entry added here.
         */

        None = 0,
        BMW = 1,
        Buick = 2,
        Cadillac = 3,
        Chevrolet = 4,
        GMC = 5,
        Honda = 6,
        Chrysler= 7,
        Ford = 8
    }
}
