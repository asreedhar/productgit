﻿using System.ComponentModel;

namespace MAX.Entities.Enumerations
{
    public enum GidProvider
    {
        [Description("Select One")]
        Unknown = 0,
        
        [Description("None")]
        None,
        
        [Description("Aultec")]
        Aultec,
        
        [Description("eBiz")]
        // ReSharper disable InconsistentNaming
        eBiz
        // ReSharper restore InconsistentNaming
    }
}
