﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAX.Entities.Enumerations
{
    public enum Make
    {
        Audi,
        BMW,
        Buick,
        Cadillac,
        Chevrolet,
        Chrysler,
        Dodge,
        Fiat,
        Ford, // needed for ford direct autoload
        Geo,
        GMC,
        Hummer,
        Hyundai,
        Jeep,
        Lexus,
        Lincoln,
        Mazda,
        MINI,
        Oldsmobile,
        Plymouth,
        Pontiac,
        Ram,
        Saturn,
        Scion,
        Toyota,
        Volkswagen
    }
}