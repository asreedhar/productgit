﻿
namespace MAX.Entities.Enumerations
{
    public enum DealershipSegment
    {
        Undefined = 0,
        Domestic  = 1,
        Import    = 2,
        Highline  = 3
    }
}
