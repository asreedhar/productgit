﻿using System.ComponentModel;

namespace MAX.Entities.Enumerations
{
    // The stored procedure Merchandising.settings.MiscSettings#Update used a condition that refers
    // to a value in this enum directly.  If you change the value for carsApi here, it will need to 
    // be updated in the procedure as well.
    public enum ReportDataSource
    {
        [Description("None")]
        None = 1,

        [Description("Aultec")]
        Aultec = 2,

        [Description("eBiz")]
// ReSharper disable once InconsistentNaming
        eBiz = 3,

        [Description("Cars API")]
        CarsApi = 4
    }
}
