﻿namespace MAX.Entities
{
    public enum WorkflowType
    {
        NotOnline = -1,
        None = 0, 
        // NOTE: Please leave these numbers and names unchanged. We serialize these values into durable 
        // queue messages and changing the spelling and value may disrupt the system.
        Offline = 1,
        NewInventory = 2,
        EquipmentNeeded = 3,
        AdReviewNeeded = 4,
        NoPhotos = 5,
        NoPrice = 6,
        CreateInitialAd = 7,
        OutdatedPrice = 9,
        OptimizedAds = 10,
        OptimizedEquipmentNeeded = 11,
        OptimizedNoPrice = 12,
        OptimizedNoLowPhotos = 13,
        DueForRepricing = 16,
        LowActivity = 17,
        AllInventory = 18,
        TrimNeeded = 19,
        BookValueNeeded = 20,
        NoPackages = 22,
        NoCarfax = 23,
        NeedsApprovalAll = 24,
        NeedsAdReviewAll = 25,
        NeedsPricingAll = 26,
        LowAdQualityAll = 27,
        NeedsPhotosEquipmentAll = 28,
        LowPhotos = 29,
        NoFavourablePriceComparisons=30
    }
}
