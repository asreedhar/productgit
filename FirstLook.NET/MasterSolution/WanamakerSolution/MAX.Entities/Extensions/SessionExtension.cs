﻿using System.Web.SessionState;
using MAX.Entities.Enumerations;
using FirstLook.Common.Core.Utilities;

namespace MAX.Entities.Extensions
{
    public static class SessionExtension
    {
        /// <summary>
        /// From the session, get the object associated with the key. If it is not set, assign the defaultValue to the session for provided key.
        /// </summary>
        /// <param name="session">Session to get values from</param>
        /// <param name="defaultValue">Value to assign to key if unassigned</param>
        /// <param name="key">Key used to retrieve session object</param>
        /// <returns></returns>
        public static T GetOrSetDefault<T>(this HttpSessionState session, T defaultValue, SessionKey key)
        {
            if (session.GetObject(key) == null)
            {
                session.SetObject(key, defaultValue);
                return defaultValue;
            }
            else
            {
                return (T)session.GetObject(key);
            }
        }

        /// <summary>
        /// Gets an object from the session.
        /// </summary>
        /// <param name="session">The http session to get the object from.</param>
        /// <param name="key">The lookup key for the associated object.</param>
        /// <returns>A nullable object.</returns>
        public static object GetObject(this HttpSessionState session, SessionKey key)
        {
            return session[key.ToString()];
        }

        /// <summary>
        /// Sets an object on the session.
        /// </summary>
        /// <param name="session">The http session to set the object on.</param>
        /// <param name="key">The key used to associate the object with.</param>
        /// <param name="o">The object to be added/updated on the session.</param>
        public static void SetObject(this HttpSessionState session, SessionKey key, object o)
        {
            session[key.ToString()] = o;
        }
    }
}
