﻿using System;
using System.Collections.Generic;
using System.Data;

namespace MAX.Entities.Extensions
{
    public static class DataReaderExtensions
    {
        public static IEnumerable<IDataRecord> Records(this IDataReader source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            while (source.Read())
            {
                yield return (source);
            }
        }
    }
}
