﻿using System;
using MAX.Entities.Messages;

namespace MAX.Entities
{
    public class BucketValue
    {
        public String BusinessUnitId_Bucket
        {
            get { return BusinessUnitId.ToString("D10") + Bucket; }
        }
        public Int32 BusinessUnitId { get; set; }
        public String BusinessName { get; set; }
        public WorkflowType Bucket { get; set; }
        public String BucketDescription { get; set; }
        public DateTime Date { get; set; }
        public Int32 NewCount { get; set; }
        public Int32 UsedCount { get; set; }

        public BucketValue()
        {
            Date = DateTime.Now;
        }

        public BucketValueMessage GetMessage()
        {
            return new BucketValueMessage(BusinessUnitId)
            {
                BusinessName = BusinessName,
                Bucket = Bucket,
                BucketDescription = BucketDescription,
                Date = Date,
                NewCount = NewCount,
                UsedCount = UsedCount
            };
        }

        public BucketValue SetFromMessage(BucketValueMessage message)
        {
            BusinessUnitId = message.BusinessUnitId;
            BusinessName = message.BusinessName;
            Bucket = message.Bucket;
            BucketDescription = message.BucketDescription;
            Date = message.Date;
            NewCount = message.NewCount;
            UsedCount = message.UsedCount;

            return this;
        }

	    public BucketValue Clone()
	    {
		    return new BucketValue
		    {
			    BusinessUnitId = BusinessUnitId,
			    Bucket = Bucket,
			    BucketDescription = BucketDescription,
			    BusinessName = BusinessName,
			    Date = Date,
			    NewCount = NewCount,
			    UsedCount = UsedCount
		    };
	    }

        public Int32 GetCount(VehicleType vehicleType)
        {
            var count = 0;

            if (vehicleType == VehicleType.Both || vehicleType == VehicleType.New)
                count += NewCount;
            if (vehicleType == VehicleType.Both || vehicleType == VehicleType.Used)
                count += UsedCount;

            return count;
        }
    }
}
