﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MAX.Entities.Enumerations;

namespace MAX.Entities
{
    [Serializable]
    public class MiscSetting
    {
        public readonly int BusinessUnitId;

        public bool HasSettings { get; set; }
        public bool OptimalFormat { get; set; }
        public Franchise Franchise { get; set; }
        public bool PriceNewCars { get; set; }
        public bool ShowDashboard { get; set; }
        public bool ShowGroupLevelDashboard { get; set; }
        public bool AnalyticsSuite { get; set; }
        public bool BatchAutoload { get; set; }
        public byte MaxVersion { get; set; }
        public bool WebLoaderEnabled { get; set; }
        public bool ModelLevelFrameworksEnabled { get; set; }
        public bool AutoOffline_WholesalePlanTrigger { get; set; }
        public bool ShowCtrGraph { get; set; }
        public DealershipSegment DealershipSegment { get; set; }
        public bool BulkPriceNewCars { get; set; }
        public bool showOnlineClassifiedOverview { get; set; }
        public bool ShowTimeToMarket { get; set; }
        public GidProvider NewGidProvider { get; set; }
        public GidProvider UsedGidProvider { get; set; }
        public ReportDataSource ReportDataSourceNew { get; set; }
        public ReportDataSource ReportDataSourceUsed { get; set; }
        public String DMSName { get; set; }
        public String DMSEmail { get; set; }

        public MiscSetting(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;

            HasSettings = false;
            OptimalFormat = false;
            Franchise = Franchise.None;
            PriceNewCars = false;
            ShowDashboard = false;
            ShowGroupLevelDashboard = false;
            AnalyticsSuite = false;
            BatchAutoload = false;
            MaxVersion = 0;
            WebLoaderEnabled = false;
            ModelLevelFrameworksEnabled = false;
            AutoOffline_WholesalePlanTrigger = false;
            ShowCtrGraph = false;
            DealershipSegment = DealershipSegment.Undefined;
            BulkPriceNewCars = false;
            showOnlineClassifiedOverview = false;
            ShowTimeToMarket = false;
            NewGidProvider = GidProvider.None;
            UsedGidProvider = GidProvider.None;
            ReportDataSourceNew = ReportDataSource.None;
            ReportDataSourceUsed = ReportDataSource.None;
            DMSName = String.Empty;
            DMSEmail = String.Empty;
        }

        public MiscSetting Clone()
        {
            return new MiscSetting(BusinessUnitId)
            {
                HasSettings = HasSettings,
                OptimalFormat = OptimalFormat,
                Franchise = Franchise,
                PriceNewCars = PriceNewCars,
                ShowDashboard = ShowDashboard,
                ShowGroupLevelDashboard = ShowGroupLevelDashboard,
                AnalyticsSuite = AnalyticsSuite,
                BatchAutoload = BatchAutoload,
                MaxVersion = MaxVersion,
                WebLoaderEnabled = WebLoaderEnabled,
                ModelLevelFrameworksEnabled = ModelLevelFrameworksEnabled,
                AutoOffline_WholesalePlanTrigger = AutoOffline_WholesalePlanTrigger,
                ShowCtrGraph = ShowCtrGraph,
                DealershipSegment = DealershipSegment,
                BulkPriceNewCars = BulkPriceNewCars,
                showOnlineClassifiedOverview = showOnlineClassifiedOverview,
                ShowTimeToMarket = ShowTimeToMarket,
                NewGidProvider = NewGidProvider,
                UsedGidProvider = UsedGidProvider,
                ReportDataSourceUsed = ReportDataSourceUsed,
                ReportDataSourceNew = ReportDataSourceNew,
                DMSName = DMSName,
                DMSEmail = DMSEmail
            };
        }
    }
}
