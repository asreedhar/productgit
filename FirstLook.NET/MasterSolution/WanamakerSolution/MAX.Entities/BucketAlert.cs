﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAX.Entities
{
    [Serializable]
    public class BucketAlert
    {
        public readonly int BusinessUnitId;
        public Boolean Active { get; set; }
        public String[] Emails { get; set; }
        public int NewUsed { get; set; }
        public Boolean EmailBounced { get; set; }
        public String[] WeeklyEmails { get; set; }
        public Boolean WeeklyEmailBounced { get; set; }
        public String[] BionicHealthReportEmails { get; set; }
        public Boolean BionicHealthReportEmailBounced { get; set; }
        public String[] GroupBionicHealthReportEmails { get; set; }
        public Boolean GroupBionicHealthReportEmailBounced { get; set; }


        public virtual BucketAlertThreshold[] Thresholds { get; set; }

        public BucketAlert(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
            Active = false;
            Emails = new String[0];
            EmailBounced = false;
            WeeklyEmails = new String[0];
            WeeklyEmailBounced = false;
            BionicHealthReportEmails = new String[0];
            BionicHealthReportEmailBounced = false;
            GroupBionicHealthReportEmails = new String[0];
            GroupBionicHealthReportEmailBounced = false;
        }

        public BucketAlert Clone()
        {
            return new BucketAlert(BusinessUnitId)
            {
                Active = Active,
                Emails = Emails,
                NewUsed = NewUsed,
                EmailBounced = EmailBounced,
                WeeklyEmails = WeeklyEmails,
                WeeklyEmailBounced = WeeklyEmailBounced,
                BionicHealthReportEmails = BionicHealthReportEmails,
                BionicHealthReportEmailBounced = BionicHealthReportEmailBounced,
                GroupBionicHealthReportEmails = BionicHealthReportEmails,
                GroupBionicHealthReportEmailBounced = BionicHealthReportEmailBounced
            };
        }
    }
}
