﻿using System.Text;
using System.Web;

namespace MAX.Entities
{
    public class RedirectEntry
    {
        public RedirectEntry(int businessUnitId, string url)
        {
            BusinessUnitId = businessUnitId;
            Url = url;
        }

        public int BusinessUnitId { get; private set; }
        public string Url { get; private set; }

        public static RedirectEntry FromToken(string token)
        {
            byte[] bytes = HttpServerUtility.UrlTokenDecode(token);
            token = Encoding.ASCII.GetString(bytes);
            string[] tokens = token.Split(':');
            return new RedirectEntry(int.Parse(tokens[0]), tokens[1]);
        }

        public string ToToken()
        {
            string token = string.Format("{0}:{1}", BusinessUnitId, Url);
            byte[] bytes = Encoding.ASCII.GetBytes(token);
            token = HttpServerUtility.UrlTokenEncode(bytes);
            return token;
        }
    }
}
