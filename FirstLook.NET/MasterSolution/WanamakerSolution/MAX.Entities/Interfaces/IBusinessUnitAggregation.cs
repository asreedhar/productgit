﻿
namespace MAX.Entities.Interfaces
{
    public interface IBusinessUnitAggregation
    {
        int BusinessUnitId { get; }
    }
}