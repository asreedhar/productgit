﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModel.IntegrationTests.Chrome.Mapper
{
    [TestFixture]
    public class ChromeStyleTests
    {
        private Mock<IChromeMapper> ChromeMapperMock;
        // This VIN1 was chosen because the vin pattern resolves to two distinct chrome style ids which can only
        // be distinguished by the full style code
        private const string VIN1 = "WBAKB8C58BCY65872";
        private const string VIN2 = "WBAKB8C55BCY64727";
        private const string VIN3 = "WMWZF3C51BTX82242";
        private const int VinPatternId1 = 109300; // VinPatternId1 for the specified VIN1.
        private const int VinPatternId2 = 109300;
        private const int VinPatternId3 = 114608;
        private const int STYLE_ID = 324322;
        private const int NO_STYLE = -1;

        [SetUp]
        public void Setup()
        {
            ChromeMapperMock = new Mock<IChromeMapper>();
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN1)).Returns(VinPatternId1);
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN2)).Returns(VinPatternId2);
            ChromeMapperMock.Setup(m => m.LookupVinPatternIdByVin(VIN3)).Returns(VinPatternId3);

            var b = new ContainerBuilder();
            b.RegisterType<NullCache>().As<ICache>();
            b.Register(c => ChromeMapperMock.Object).As<IChromeMapper>();
            var container = b.Build();
            Registry.RegisterContainer(container);
        }

        [TearDown]
        public void Teardown()
        {
            Registry.Reset();
        }

        [Test]
        public void InferStyleIdWithVinAndModelCodeWhenModelCodeContainsSpaces()
        {
            string oemCode = "1179 750Li";

            var style = ChromeStyle.InferStyleId(VIN1, new List<string>(), oemCode, string.Empty, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNotNull(style);
            Assert.AreEqual(STYLE_ID, style.StyleID);
        }

        [Test]
        public void InferStyleIdWithVinAndModelCodeWhenModelCodeContainsDash()
        {
            string oemCode = "1179 - 750Li";

            var style = ChromeStyle.InferStyleId(VIN1, new List<string>(), oemCode, string.Empty, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNotNull(style);
            Assert.AreEqual(STYLE_ID, style.StyleID);
        }

    

        [Test]
        public void FetchByVinAndOemStyleCode()
        {
            // this data should be stable over time.
            const string OEM_CODE = "1179";

            var styles = ChromeStyle.FetchByVinAndOemStyleCode(VIN1, OEM_CODE);

            Assert.IsNotNull(styles);
            Assert.AreEqual(1, styles.Count );
            Assert.AreEqual(STYLE_ID, styles.First().StyleID);
        }

        #region GetByStyleId

        [Test]
        public void GetByStyleId()
        {
            var styles = ChromeStyle.GetByStyleId(STYLE_ID);
            Assert.AreEqual(1, styles.Count);
            Assert.AreEqual(STYLE_ID, styles.First().StyleID);
        }

        
        #endregion


        #region InferStyleByModelAndOptions

        [Test]
        public void InferStyleByModelAndOptions()
        {
            var style = ChromeStyle.InferStyleByModelAndOptions(VIN1, "1179", null);
            Assert.IsNotNull(style);
        }


        #endregion

        #region InferStyleId

        [Test]
        public void InferStyleIdReturnsNullWhenMultipleVinMatches()
        {
            string vin = VIN2;  // two possible styles by vin pattern only.

            var matches = ChromeStyle.FetchList(vin);
            Assert.IsNotNull(matches);
            Assert.AreEqual(2, matches.Count);

            var style = ChromeStyle.InferStyleId(vin, null, null, null, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNull(style);
        }

        [Test]
        public void InferStyleIdReturnsStyleWhenOneVinMatches()
        {
            string vin = VIN3;
            var vinMatches = ChromeStyle.FetchList(vin);
            Assert.IsNotNull(vinMatches);
            Assert.AreEqual(1, vinMatches.Count);

            var style = ChromeStyle.InferStyleId(vin, null, null, null, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNotNull(style);
        }

        [Test]
        public void InferStyleByIdReturnsWhenPassedModel()
        {
            string vin = VIN2;  // two possible styles by vin pattern only.

            var style = ChromeStyle.InferStyleId(vin, null, "1179", null, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNotNull(style);
            Assert.AreEqual(324322, style.StyleID);

            style = ChromeStyle.InferStyleId(vin, null, "1179 750", null, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNotNull(style);
            Assert.AreEqual(324322, style.StyleID);

            style = ChromeStyle.InferStyleId(vin, null, "1179 - 750", null, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNotNull(style);
            Assert.AreEqual(324322, style.StyleID);

            style = ChromeStyle.InferStyleId(vin, null, "1193", null, TransmissionType.Unknown, DrivetrainType.Unknown);
            Assert.IsNotNull(style);
            Assert.AreEqual(324384, style.StyleID);
        }

        #endregion


        public void StylesMatch()
        {
            // Not sure how unknown styles should compare
            Assert.IsTrue(ChromeStyle.StylesMatch(0, 0));
            Assert.IsFalse(ChromeStyle.StylesMatch(-1, 0));

            Assert.IsFalse(ChromeStyle.StylesMatch(null, null));

            Mock<IChromeStyle> mockStyle = new Mock<IChromeStyle>();
            mockStyle.SetupGet(s => s.ChromeStyleID).Returns(0);
            Assert.IsTrue( ChromeStyle.StylesMatch(mockStyle.Object, 0));

            Assert.IsTrue(ChromeStyle.StylesMatch(0, 0));
            Assert.IsTrue(ChromeStyle.StylesMatch(-1, -1));
            Assert.IsTrue(ChromeStyle.StylesMatch(1, 1));
            Assert.IsFalse(ChromeStyle.StylesMatch(1, 2));
            Assert.IsFalse(ChromeStyle.StylesMatch(0, 1));
            Assert.IsFalse(ChromeStyle.StylesMatch(-1, 1));


        }

    }
    
}
