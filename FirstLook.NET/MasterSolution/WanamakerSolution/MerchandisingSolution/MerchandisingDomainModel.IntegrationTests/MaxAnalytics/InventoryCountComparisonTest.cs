﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Membership;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;
using MerchandisingRegistry;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModel.IntegrationTests.MaxAnalytics
{
	internal class Stats
	{
		private int[] Percentiles = new int[10];

		public int BusinessUnitsChecked;

		public int SitesChecked;
		
		public int ExactMatches;
		public int CloseMatches;

		public int BothHaveData;
		public int NeitherHasData;
		public int AnalyticsNoData;
		public int MaxNoData;

		public void ReportPercentDifference(int percentDiff)
		{
			if (percentDiff > 100)
				percentDiff = 100;

			int bucket = (int) Math.Floor(percentDiff/10.0) - 1;

			Percentiles[bucket] += 1;

		}

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.AppendLine(String.Format("Checked {0} business units ({1} sites)", BusinessUnitsChecked, SitesChecked));
			sb.AppendLine(String.Format("Sites with data (both systems): {0} ({1})", BothHaveData, SitesChecked > 0 ? (BothHaveData/SitesChecked).ToString("P") : "n/a" ));
			sb.AppendLine(String.Format("Sites with exact matches: {0} ({1})", ExactMatches, BothHaveData > 0 ? (ExactMatches/BothHaveData).ToString("P") : "n/a"));
			sb.AppendLine(String.Format("Max no data: {0} ({1})", MaxNoData, SitesChecked > 0 ? (MaxNoData/SitesChecked).ToString("P") : "n/a"));
			sb.AppendLine(String.Format("Analytics no data: {0} ({1})", AnalyticsNoData, SitesChecked > 0 ? (AnalyticsNoData/SitesChecked).ToString("P") : "n/a"));

			for (int i = Percentiles.Length - 1; i >= 0; i -= 1)
			{
				sb.AppendLine(String.Format("{0} matches: {1}", i, Percentiles[i]));
			}

			return sb.ToString();
		}
		
	}

	[TestFixture]
	public class InventoryCountComparisonTest
	{
		[SetUp]
		public void Setup()
		{
			var builder = new ContainerBuilder();
			builder.RegisterModule(new WebAppModule());
			var merchDomainReg = new MerchandisingDomainRegistry();
			merchDomainReg.Register(builder);
			builder.RegisterType<NullCache>().As<ICache>();

			var member = new Mock<IMember>();
			var memberContext = new Mock<IMemberContext>();
			memberContext.Setup(x => x.Current).Returns(member.Object);

			builder.RegisterInstance(memberContext.Object).As<IMemberContext>();

			var container = builder.Build();
			Registry.RegisterContainer(container);
		}

		[Test]
		public void Test()
		{
			var stats = new Stats();

			// start test

			IInventoryCountDao analyticsDao = Registry.Resolve<InventoryCountDao>();
			IInventoryCountDao legacyDao = Registry.Resolve<InventoryCountLegacyDao>();

			var businessUnits = GetBusinessUnitIds();
			//var businessUnits = new List<int>() {100147};

			var repository = Registry.Resolve<IDashboardBusinessUnitRepository>();
			
			foreach (var businessUnit in businessUnits)
			{

				var dealer = repository.GetBusinessUnit(businessUnit);
	
				var dealerInfoString = String.Format("{0}-{1}", dealer.Code, dealer.BusinessUnitId);

				// get some data from analytics webservice

				stats.BusinessUnitsChecked += 1;

				var analyticsList = GetRequestListForAnalytics();
				analyticsDao.GetInventoryCounts(analyticsList, dealer.BusinessUnitId, VehicleType.Both, new DateRange(DateTime.Parse("1/1/2013"), DateTime.Parse("1/7/2013")));

				// get data from legacy system

				var legacyList = GetRequestListForMax();
				var dateRange = new DateRange(DateTime.Parse("1/1/2013"), DateTime.Parse("1/7/2013"));
				legacyList.ForEach(i => i.DateRange = dateRange);	// don't know why!!!
				legacyDao.GetInventoryCounts(legacyList, dealer.BusinessUnitId, VehicleType.Both, dateRange);

				// compare

				foreach (var ws in analyticsList)
				{
					stats.SitesChecked += 1;

					if (ws.InventoryCount == null)
					{
						stats.AnalyticsNoData += 1;
						Trace.WriteLine(String.Format("NO DATA ({0},{1},Analytics)", dealerInfoString, ws.Site.Name));
					}

					var leg = legacyList.First(l => l.Site.Id == ws.Site.Id);
					if (leg.InventoryCount == null)
					{
						stats.MaxNoData += 1;
						Trace.WriteLine(String.Format("NO DATA ({0},{1},Max)", dealerInfoString, ws.Site.Name));
					}

					if (ws.InventoryCount == null || leg.InventoryCount == null)
					{
						continue;
					}

					stats.BothHaveData += 1;

					int diff = Math.Abs(leg.InventoryCount.Value - ws.InventoryCount.Value);
					if (diff == 0)
					{
						Trace.WriteLine(String.Format("EXACT MATCH ({0},{1})", dealerInfoString, ws.Site.Name));
						stats.ExactMatches += 1;
					}
					else
					{
						var avg = (leg.InventoryCount.Value + ws.InventoryCount.Value)/2.0;
						var percentDiff = diff/avg*100.0;
						stats.ReportPercentDifference((int) percentDiff);
					}

					Trace.WriteLine(dealerInfoString + " OK");
				}
			}

			Trace.WriteLine(stats);
		}

		private IList<int> GetBusinessUnitIds()
		{
			var finder = BusinessUnitFinder.Instance();
			
			var unique = new HashSet<int>();

			foreach (var bunit in finder.FindAllDealerGroups(null))
			{
				foreach (var dealer in bunit.Dealerships())
				{
					if (!unique.Contains(dealer.Id))
					{
						unique.Add(bunit.Id);
					}
				}
			}

			return unique.ToList();
		}

			

		private List<SitePerformance> GetRequestListForMax()
		{
			var list = new List<SitePerformance>
				{
					new SitePerformance() {Site = new Site() {Id = 2, Name = "AutoTrader.com"}},
					new SitePerformance() {Site = new Site() {Id = 1, Name = "Cars.com"}}
				};
			return list;
		}

		private List<SitePerformance> GetRequestListForAnalytics()
		{
			var list = new List<SitePerformance>
				{
					new SitePerformance() {Site = new Site() {Id = 2, Name = "AutoTrader"}},
					new SitePerformance() {Site = new Site() {Id = 1, Name = "Cars"}}
				};
			return list;
		}
	}
}
