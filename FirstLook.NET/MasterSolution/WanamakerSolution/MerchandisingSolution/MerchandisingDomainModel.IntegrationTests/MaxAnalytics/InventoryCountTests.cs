﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Membership;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using Moq;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount;
using Reports=FirstLook.Merchandising.DomainModel.Reports.SitePerformance;

namespace MerchandisingDomainModelTests.MaxAnalytics
{
	public class InventoryCountTests
	{
		
		[Test]
		public void WebserviceCallTest()
		{
			var units = new List<string>() {"KENNYROS07"};
			var ws = new InventoryCountWebservice("https://beta.test.maxanalytics.biz/analyticsws/");
			var result = ws.GetInventoryCount(units, "both", 2012, 01, 2013, 05);
			Assert.IsNotNull(result, "webservice didn't return nothin");	// if web service returns null, it means error - not no data
		}

		[Test]
		public void WrapperTest()
		{
			var buId = 105013;	// KENNYROS07
			var groupId = 1;	// fake group ID
			
			var dict = new Dictionary<int, List<SitePerformance>>();
			var list = new List<SitePerformance>();
			list.Add(new SitePerformance() {Site = new Reports.Site() {Id = 2, Name = "AutoTrader"}});
			list.Add(new SitePerformance() { Site = new Reports.Site() { Id = 1, Name = "Cars" } });
			dict.Add(buId, list);
			
			var dealerServices = new Mock<IDashboardBusinessUnitRepository>();
			dealerServices.Setup(x => x.GetBusinessUnitsfromGroup(groupId)).Returns(new[] {new DashboardBusinessUnit {BusinessUnitId = 105013, Code = "KENNYROS07"}});
			var memberContext = new Mock<IMemberContext>();
			var vehicleOnlineDao = new Mock<IDataOnlineRepository>();
			var inventoryCountWs = new InventoryCountWebservice("https://beta.test.maxanalytics.biz/analyticsws/");
			
			var dealer = Mock.Of<IDashboardBusinessUnit>();
			dealer.BusinessUnitId = buId;
			dealer.Code = "KENNYROS07";
			
			var dealers = new List<IDashboardBusinessUnit>() {dealer};

			var wrapper = new AnalyticsWrapper(memberContext.Object, dealerServices.Object, vehicleOnlineDao.Object, inventoryCountWs);
			wrapper.GetGroupInventoryCounts(dict, groupId, "Both", DateTime.Parse("1/1/2012"), DateTime.Parse("5/1/2013"));
			
			foreach (var site in dict)
			{
				Assert.IsTrue(site.Value.Count != 0);
			}
		}

	
	}
}
