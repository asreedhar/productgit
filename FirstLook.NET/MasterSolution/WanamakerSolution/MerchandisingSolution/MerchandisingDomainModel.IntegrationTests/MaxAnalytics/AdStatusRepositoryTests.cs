﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using MAX.Entities.Enumerations;
using Moq;
using NUnit.Framework;
using Dapper;

namespace MerchandisingDomainModel.IntegrationTests.MaxAnalytics
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable PossibleNullReferenceException

    [TestFixture]
    public class AdStatusRepositoryTests
    {
        [Test]
        public void GetAdStatusResults_ReturnsList_True()
        {
            var buRepoMock = new Mock<IDashboardBusinessUnitRepository>();

            var testBu = GetTestBusinessUnit();
            buRepoMock
                .Setup(bu => bu.GetBusinessUnit(It.IsAny<int>()))
                .Returns(testBu);
            
            var repo = new AdStatusRepository();
            /*
            var vins = GetTestVins(testBu);
            */
            var vins = new List<string> {"12345678901234567", "11223344556677889"};
	        var results = repo.GetAdStatusResults(new AdStatusArgs {Store = testBu.Code, vins = vins});

            foreach (var result in results)
            {
                Assert.That(result.Provider != GidProvider.Unknown);
                Assert.That(result.Site != Site.Unknown);
                Assert.That(((AdStatusResult)result).Ads.Any());
            }
        }

        private List<string> GetTestVins(IDashboardBusinessUnit testBu)
        {
            using (var conn = new SqlConnection(connectionString: ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                conn.Open();

                const string query = @"SELECT TOP 2 Vin FROM Merchandising.workflow.Inventory wi WHERE wi.BusinessUnitID = 101023";
                var vins = conn.Query<string>(query).ToList();

                return vins;
            }
        }

        private IDashboardBusinessUnit GetTestBusinessUnit()
        {
            using (var conn = new SqlConnection(connectionString: ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                conn.Open();

                const string query = @"
                        SELECT top 1 bu.businessUnitId AS BusinessUnitId, BusinessUnitTypeId AS Type, BusinessUnitCode AS Code, BusinessUnit AS Name
                        FROM IMT.dbo.BusinessUnit bu
                        JOIN Merchandising.settings.Merchandising merch ON
                            merch.businessUnitID = bu.BusinessUnitId AND merch.showTimeToMarket = 1
                        WHERE bu.Active = 1
                    ";

                var bu = conn.Query<DashboardBusinessUnit>(query).FirstOrDefault();
                return bu;
            }
        }
        
        [Test]
        public void GetAdStatusResults_ReturnsList_False()
        {
            var buRepoMock = new Mock<IDashboardBusinessUnitRepository>();
            buRepoMock
                .Setup(bu => bu.GetBusinessUnit(It.IsAny<int>()))
                .Returns(new DashboardBusinessUnit() { BusinessUnitId = 1, Code = "TESTBU", Name = "TestBusinessUnit", Type = 4 });

            var repo = new AdStatusRepository();


	        var results = repo.GetAdStatusResults(new AdStatusArgs {Store = "TESTBU", vins = new List<string> {"1234", ""}});

            Assert.That(!results.Any());
        }
    }

    // ReSharper restore InconsistentNaming
    // ReSharper restore PossibleNullReferenceException
}