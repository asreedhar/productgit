﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Autofac;
using FirstLook.Merchandising.DomainModel.Synonyms;
using MAX.Caching;
using NUnit.Framework;

namespace MerchandisingDomainModel.IntegrationTests.Synonyms
{
    [TestFixture]
    public class SynonymIntegrationTests
    {
        private IContainer _container;

        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CacheKeyBuilder>().As<ICacheKeyBuilder>();
            builder.RegisterType<MemcachedClientWrapper>().As<ICacheWrapper>();
            builder.RegisterType<SynonymDbFetcher>().As<ISynonymFetcher>();
            builder.RegisterType<CachedSynonymRepository>().As<ISynonymRepository>();
            builder.RegisterType<SynonymEvaluator>().As<ISynonymEvaluator>();

            _container = builder.Build();
        }


        [Test]
        public void BasicTest()
        {
            var repository = _container.Resolve<ISynonymRepository>();

            var cachedSynonymRepository = (CachedSynonymRepository) repository;
            cachedSynonymRepository.ClearCache();

            Assert.AreEqual(6, cachedSynonymRepository.Synonyms.Count); // table was initialized with 6 rows
            PrintSynonyms(cachedSynonymRepository);

            var evaluator = _container.Resolve<ISynonymEvaluator>();

            cachedSynonymRepository.ClearCache();

            Debug.WriteLine("Performing first comparison...");
            Assert.IsTrue("A/C".IsSynonymous("AC", evaluator));
            Debug.WriteLine("Finished performing first comparison.");
            Debug.WriteLine("");
            Debug.WriteLine("Performing second comparison...");
            Assert.IsTrue("A/C".IsSynonymous("AC", evaluator));
            Debug.WriteLine("Finished performing second comparison.");
            //Assert.IsFalse("AC".IsSynonymous("A/C", evaluator));
            //Assert.IsFalse("foo".IsSynonymous("bar", evaluator));
        }

        private static void PrintSynonyms(CachedSynonymRepository cachedSynonymRepository)
        {
            foreach (var s in cachedSynonymRepository.Synonyms)
            {
                Debug.WriteLine("Synonym: {0}|{1}", s.Value, s.SynonymousValue);
            }
        }
    }
}
