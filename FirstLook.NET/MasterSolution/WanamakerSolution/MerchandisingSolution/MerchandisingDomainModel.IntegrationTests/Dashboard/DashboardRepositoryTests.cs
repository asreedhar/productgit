﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Workflow;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;
using Google.Apis.Analytics.v3.Data;

namespace MerchandisingDomainModelTests.Dashboard
{
    [TestFixture]
    class DashboardRepositoryTests
    {
        //[SetUp]
        //public void TestSetup()
        //{

        //}

        private GoogleQueryParameters GetGoogleQueryParams()
        {
            return new GoogleQueryParameters()
            {
                Dimensions = "ga:source",
                Metrics = "ga:visits,ga:percentNewVisits,ga:visitBounceRate,ga:avgTimeOnSite,ga:pageviewsPerVisit",
                Sort = "-ga:visits"
            };
        }

        [Test]
        public void Invalid_BusinessUnitId_Throws_InvalidOperationException()
        {
            int businessUnitId = 1234;

            var workflowRepo = new Mock<IWorkflowRepository>();
            var googleAnalyticsQuery = new Mock<IGoogleAnalyticsQuery>();
            var googleAnalyticsRepo = new Mock<IGoogleAnalyticsRepository>();
            var googleAnalyticsWebAuthorization = new Mock<IGoogleAnalyticsWebAuthorization>();
            var googleAnalyticsResultCache = new Mock<IGoogleAnalyticsResultCache>();
            var maxAnalytics = new Mock<IMaxAnalytics>();
            var dealerRepository = new Mock<IDashboardBusinessUnitRepository>();
            var noSqlDbFactory = new Mock<INoSqlDbFactory>();

            FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardRepository repo = new FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardRepository(
                workflowRepo.Object,
                googleAnalyticsResultCache.Object,
                maxAnalytics.Object,
                dealerRepository.Object,
                noSqlDbFactory.Object
            );
            
            //Assert.Throws<InvalidOperationException>(() => repo.GetGoogleAnalyticsData(businessUnitId, GoogleAnalyticsType.PC, GetGoogleQueryParams(), DateTime.Now, DateTime.Now, 10));
        }

        [Test]
        public void Limit_Results_Are_Less_Or_Equal()
        {
            var workflowRepo = new Mock<IWorkflowRepository>();
            var googleAnalyticsRepo = new GoogleAnalyticsRepository();
            var googleAnalyticsWebAuthorization = new GoogleAnalyticsWebAuthorization(googleAnalyticsRepo);
            var googleAnalyticsQuery = new GoogleAnalyticsQuery(googleAnalyticsRepo);
            var maxAnalytics = new Mock<IMaxAnalytics>();

            int businessUnitId = GetActiveBusinessUnitId();
            int limitResults = 3;

            if (businessUnitId == 0)
            {
                throw new Exception("Invalid Test: Verify Business Unit Id");
            }

            //FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardRepository repo = new FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardRepository(
            //    workflowRepo.Object,
            //    googleAnalyticsQuery,
            //    googleAnalyticsRepo,
            //    googleAnalyticsWebAuthorization,
            //    maxAnalytics.Object
            //);

            //Google.Apis.Analytics.v3.Data.GaData data = repo.GetGoogleAnalyticsData(businessUnitId, GoogleAnalyticsType.PC, GetGoogleQueryParams(), DateTime.Now, DateTime.Now.AddMonths(1), 3);
            
            //Assert.LessOrEqual((decimal)limitResults, (decimal)data.Rows.Count);
        }

        [Test]
        public void Authorized_but_revoked_token_throws_AccessViolationException()
        {
            var workflowRepo = new Mock<IWorkflowRepository>();
            var googleAnalyticsRepo = new GoogleAnalyticsRepository();
            var googleAnalyticsWebAuthorization = new GoogleAnalyticsWebAuthorization(googleAnalyticsRepo);
            var googleAnalyticsQuery = new GoogleAnalyticsQuery(googleAnalyticsRepo);
            var maxAnalytics = new Mock<IMaxAnalytics>();

            int businessUnitId = GetActiveBusinessUnitId();
            int limitResults = 10;

            if (businessUnitId == 0)
            {
                throw new Exception("Invalid Test: Verify Business Unit Id");
            }

            //FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardRepository repo = new FirstLook.Merchandising.DomainModel.Dashboard.Concrete.DashboardRepository(
            //    workflowRepo.Object,
            //    googleAnalyticsQuery,
            //    googleAnalyticsRepo,
            //    googleAnalyticsWebAuthorization,
            //    maxAnalytics.Object
            //);

            GoogleTokens gaTokens = googleAnalyticsRepo.FetchTokens(businessUnitId);

            GoogleTokens dummyToken = new GoogleTokens(
                gaTokens.ClientId,
                gaTokens.ClientSecret,
                gaTokens.RefreshToken + "_fakedByTest"
            );

            //googleAnalyticsRepo.SetTokens(businessUnitId, dummyToken);

            //// Use an invalid access token to fake revocation
            //Assert.Throws<AccessViolationException>(() => repo.GetGoogleAnalyticsData(businessUnitId, GoogleAnalyticsType.PC, GetGoogleQueryParams(), DateTime.Now, DateTime.Now.AddMonths(1), limitResults));

            googleAnalyticsRepo.SetTokens(businessUnitId, gaTokens);
        }

        


        #region TestTools
        private string GetRefreshToken(int businessUnitId)
        {
            string refreshToken = String.Empty;

            using (SqlConnection con = GetOpenConnection())
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    // Get a totally valid business unit id
                    cmd.CommandText = @"
                        select RefreshToken from Merchandising.settings.GoogleAnalyticsToken WHERE BusinessUnitId = @businessUnitId
                    ";

                    cmd.CommandType = CommandType.Text;
                    IDbDataParameter param = cmd.CreateParameter();
                    param.ParameterName = "@businessUnitId";
                    param.Value = businessUnitId;

                    cmd.Parameters.Add(param);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            refreshToken = (reader["RefreshToken"] != DBNull.Value) ? reader["RefreshToken"].ToString() : String.Empty;
                        }
                    }
                }
            }

            return refreshToken;
        }

        private void UpdateRefreshToken(int businessUnitId, string fakeToken)
        {
            using (SqlConnection con = GetOpenConnection())
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    // Get a totally valid business unit id
                    cmd.CommandText = @"
                        UPDATE Merchandising.settings.GoogleAnalyticsToken SET RefreshToken=@refreshToken WHERE BusinessUnitId = @businessUnitId
                    ";

                    cmd.CommandType = CommandType.Text;
                    IDbDataParameter buidParam = cmd.CreateParameter();
                    buidParam.ParameterName = "@businessUnitId";
                    buidParam.Value = businessUnitId;

                    IDbDataParameter tokParam = cmd.CreateParameter();
                    tokParam.ParameterName = "@refreshToken";
                    tokParam.Value = fakeToken;

                    cmd.Parameters.Add(buidParam);
                    cmd.Parameters.Add(tokParam);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        private int GetActiveBusinessUnitId()
        {
            int businessUnitId = 0;
            using (SqlConnection con = GetOpenConnection())
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    // Get a totally valid business unit id
                    cmd.CommandText = @"
                        SELECT TOP 1 gat.BusinessUnitId from Merchandising.settings.GoogleAnalyticsToken gat
                        JOIN Merchandising.settings.GoogleAnalyticsProfile gap on gat.BusinessUnitId = gap.BusinessUnitId
                        WHERE Active = 1 AND Type = 1
                    ";

                    cmd.CommandType = CommandType.Text;

                    var res = cmd.ExecuteScalar();
                    if (res != null && res != DBNull.Value)
                        businessUnitId = (int)res;
                }
            }

            return businessUnitId;
        }

        private static SqlConnection GetOpenConnection()
        {
            var cs = ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString;
            var cn = new SqlConnection(cs);
            var success = false;
            try
            {
                cn.Open();
                success = true;
                return cn;
            }
            finally
            {
                if(!success)
                    cn.Close();
            }
        }

        #endregion TestTools
    }
}
