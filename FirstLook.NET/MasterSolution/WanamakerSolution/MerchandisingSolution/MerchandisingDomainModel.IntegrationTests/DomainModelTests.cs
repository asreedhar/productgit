﻿using System.Diagnostics;
using System.Threading;
using Autofac;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Commands;
using NUnit.Framework;
using ICache = FirstLook.Common.Core.ICache;

namespace MerchandisingDomainModel.IntegrationTests
{
    [TestFixture]
    public class DomainModelTests
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            Debug.WriteLine("Setup beginning...");

            var builder = new ContainerBuilder();
            builder.RegisterType<DotnetMemoryCacheWrapper>().As<ICache>();
            var container = builder.Build();
            Registry.RegisterContainer(container);

            Debug.WriteLine("Setup complete.");
        }

        [TestCase(101590, "1G1JC5SH1E4119633")]
        [TestCase(101590, "1G1JC5SH1E4119633")]
        [TestCase(101590, "1G1JC5SH1E4119633")]
        [TestCase(101590, "1G1JC5SH1E4119633")]
        [TestCase(101590, "1G1JC5SH1E4119633")]

        [TestCase(101590, "5UXZV4C54D0B01086")]
        [TestCase(101590, "5UXZV4C54D0B01086")]
        [TestCase(101590, "5UXZV4C54D0B01086")]
        [TestCase(101590, "5UXZV4C54D0B01086")]
        [TestCase(101590, "5UXZV4C54D0B01086")]
        public void TestGetOfflineStatus(int businessUnitId, string vin)
        {
            var cmd = new GetOfflineStatus(businessUnitId, vin);
            AbstractCommand.DoRun(cmd);
            Debug.WriteLine("Vehicle is offline: {0}", cmd.IsOffline);
        }

        [TestCase(101590, "5UXZV4C54D0B01086")]
        public void TestGetOfflineStatusCacheExpires(int businessUnitId, string vin)
        {
            var cmd = new GetOfflineStatus(businessUnitId, vin);
            AbstractCommand.DoRun(cmd);
            Debug.WriteLine("Vehicle is offline: {0}", cmd.IsOffline);

            Thread.Sleep(16000);

            cmd = new GetOfflineStatus(businessUnitId, vin);
            AbstractCommand.DoRun(cmd);
            Debug.WriteLine("Vehicle is offline: {0}", cmd.IsOffline);
        }

    }
}
