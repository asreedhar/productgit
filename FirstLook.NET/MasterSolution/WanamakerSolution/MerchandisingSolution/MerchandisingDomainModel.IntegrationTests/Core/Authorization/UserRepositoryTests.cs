﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModel.IntegrationTests.Core.Authorization
{
    [TestFixture]
    public class UserRepositoryTests
    {
        private Mock<IDealerServices> _dealerServicesMock;
        private IDealerServices _dealerServices;

        [SetUp]
        public void Setup()
        {
            _dealerServicesMock = new Mock<IDealerServices>();
            _dealerServices = _dealerServicesMock.Object;
        }

        [Test]
        public void GetUser_should_return_IUser_if_user_is_only_merchanding_user()
        {
            var repo = GetUserRepository();

            // Get a aspnet user that isn't a firstlook member.
            var data = GetRow(@"
select top 1 * from dbo.aspnet_Users 
where ApplicationId = '1609BF7F-2808-458C-8653-018C7D761753'
and UserName not in (select Login from IMT.dbo.Member)
");

            var userName = (string)data["UserName"];
            var user = repo.GetUser(userName);

            Assert.That(user, Is.Not.Null);
            Assert.That(user.UserName, Is.EqualTo(userName).IgnoreCase);
        }

        [Test]
        public void GetUser_should_return_IUser_if_user_is_only_IMT_member()
        {
            var repo = GetUserRepository();

            // Get a firstlook member that is not also an aspnet user
            var data = GetRow(@"
use Merchandising
select top 1 Login from IMT.dbo.Member
where Login not in (select UserName from dbo.aspnet_Users 
                    where ApplicationId = '1609BF7F-2808-458C-8653-018C7D761753')
    and Login <> ''
");
            var login = (string)data["Login"];
            
            var user = repo.GetUser(login);

            Assert.That(user, Is.Not.Null);
            Assert.That(user.UserName, Is.EqualTo(login).IgnoreCase);
        }

        private IUserRepository GetUserRepository()
        {
            return new UserRepository(_dealerServices);
        }

        private static DataRow GetRow(string query)
        {
            var tbl = GetData(query);
            if(tbl.Rows.Count != 1)
                throw new InvalidOperationException(
                    string.Format("Expected only 1 row but got {0} instead.", tbl.Rows.Count));
            return tbl.Rows[0];
        }

        private static DataTable GetData(string query)
        {
            using (var cn = GetOpenConnection())
            using (DbDataAdapter f = new SqlDataAdapter(query, cn))
            {
                var dt = new DataTable();
                f.Fill(dt);
                return dt;
            }
        }

        private static SqlConnection GetOpenConnection()
        {
            var cs = ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString;
            var cn = new SqlConnection(cs);
            var success = false;
            try
            {
                cn.Open();
                success = true;
                return cn;
            }
            finally
            {
                if(!success)
                    cn.Close();
            }
        }
    }
}