﻿using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Audit
{
    // needs to stay in sync with contents of [Merchandising].[settings].[EventTypes]
    public enum BusinessUnitEventType
    {
        Window_Sticker_Printed = 10,
        Buyers_Guide_Printed = 15,
        Vehicle_Photo_Added = 20
    }
    
    public class BusinessUnitEventLogEntry
    {
        public string CreatedBy;
        public int BusinessUnitID;
        public int EventType;
        
        public void Save()
        {
            var cn = Database.GetConnection( Database.MerchandisingDatabase );
            cn.Open();
            IDbCommand cmd;

            // insert given data with the websiteProviderID
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = 
                "INSERT INTO [audit].[BusinessUnitEventLog] ([CreatedBy],[BusinessUnitID],[EventDate],[EventType]) VALUES (@CreatedBy,@BusinessUnitID,@EventDate,@EventType)";
            
            cmd.AddRequiredParameter( "@CreatedBy", CreatedBy, DbType.String );
            cmd.AddRequiredParameter( "@BusinessUnitID", BusinessUnitID, DbType.Int32 );
            cmd.AddRequiredParameter( "@EventDate", DateTime.Now, DbType.DateTime );
            cmd.AddRequiredParameter( "@EventType", EventType, DbType.Int32 );
            
            cmd.ExecuteNonQuery();
            
            cmd.Dispose();

            cn.Dispose();
        }
    }
}
