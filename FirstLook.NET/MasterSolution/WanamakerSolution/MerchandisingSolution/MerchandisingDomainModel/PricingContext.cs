﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Pricing;

namespace FirstLook.Merchandising.DomainModel
{
    public class PricingContext
    {
        public string OwnerHandle { get; private set; }
        public string VehicleHandle { get; private set; }
        public string SearchHandle { get; private set; }

        private PricingContext()
        {
        }

        public static PricingContext FromMerchandising(MerchandisingContext context)
        {
            return FromMerchandising(context.BusinessUnitId, context.InventoryId);
        }

        public static PricingContext FromMerchandising(int businessUnitId, int inventoryId)
        {
            PricingContext context = new PricingContext();

            string ownerHandle = PricingData.GetOwnerHandle(businessUnitId);
            Pair<VehicleHandle, SearchHandle> pair = PricingData.GetVehicleHandle(ownerHandle, inventoryId);

            context.OwnerHandle = ownerHandle;
            context.VehicleHandle = pair.First.Value;
            context.SearchHandle = pair.Second.Value;

            return context;
        }

		public static PricingContext FromMerchandising(int businessUnitId)
		{
			PricingContext context = new PricingContext();

			string ownerHandle = PricingData.GetOwnerHandle(businessUnitId);

			context.OwnerHandle = ownerHandle;
			context.VehicleHandle = null;
			context.SearchHandle = null;

			return context;
		}

    }
}
