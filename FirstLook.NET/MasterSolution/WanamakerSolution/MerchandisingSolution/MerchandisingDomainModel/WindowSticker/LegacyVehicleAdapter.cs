using System;
using System.Collections.Generic;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;
using VehicleTemplatingDomainModel;


namespace FirstLook.Merchandising.DomainModel.WindowSticker
{
    public class LegacyVehicleAdapter : IVehicleAdapter {

        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(LegacyVehicleAdapter).FullName);
        #endregion

        // This controls how many equipment items from each tier will be exposed.
        private const int MAX_TIERED_EQUIPMENT = 999;

        private IVehicleTemplatingData _legacyVehicle;

        private readonly string _userName;

        private IVehicleTemplatingFactory _vehicleTemplatingFactory;
        public IVehicleTemplatingFactory VehicleTemplatingFactory
        {
            get { return _vehicleTemplatingFactory ?? Registry.Resolve<IVehicleTemplatingFactory>(); }
            set { _vehicleTemplatingFactory = value; }
        }

        /// <summary>
        /// Adapts the legacy vehicle data obtained by the specified ownerHandle & vehicleHandle
        /// to a VehicleData object.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns>VehicleData object representation of the LegacyVehicleData</returns>
        public VehicleData Adapt(string ownerHandle, string vehicleHandle){

            // Get owner type and owner entity, only allow dealers
            var owner = Owner.GetOwner(ownerHandle);
            
            if (owner.OwnerEntityType == OwnerEntityType.Dealer)
            {
                int businessUnitId = owner.OwnerEntityId;
                
                if (VehicleIsInventory(vehicleHandle))
                {
                    var inventoryId = GetVehicleEntityId(vehicleHandle);

                    _legacyVehicle = VehicleTemplatingFactory.GetVehicleTemplatingData(businessUnitId, inventoryId, _userName);

                    var useLotPrice = false;

                    try
                    {
                        var businessUnit = BusinessUnitFinder.Instance().Find(businessUnitId);
                        useLotPrice = businessUnit.DealerPreference.UseLotPrice;
                    }
                    catch (Exception exception)
                    {
                        
                        Log.ErrorFormat("There was an error determining the dealer preference for using lot price.  A default value of FALSE was used.  BusinessUnitId: {0}, exception message: {1}", businessUnitId, exception.Message );
                    }

                    return AdaptLegacyVehicle(_legacyVehicle, useLotPrice);
                }

                throw new ApplicationException("Only vehicles of type inventory are currently supported.");
            }

            throw new ApplicationException("Only owners of type dealer are currently supported.");
        }

        private static VehicleData AdaptLegacyVehicle(IVehicleTemplatingData legacyVehicle, bool useLotPrice)
        {
            
            decimal price = useLotPrice && legacyVehicle.InventoryItem.LotPrice > 0.0m
                                ? legacyVehicle.InventoryItem.LotPrice
                                : legacyVehicle.InventoryItem.ListPrice;

            var vehicleData = new VehicleData
                                  {
                                      ModelYear = int.Parse(legacyVehicle.Year),
                                      Make = legacyVehicle.Make,
                                      Model = legacyVehicle.Model,
                                      AgeInDays = legacyVehicle.InventoryItem.Age,
                                      // TODO: Find out where to get Series
                                      Series = null,
                                      Price = price,
                                      UnitCost = legacyVehicle.InventoryItem.UnitCost,
                                      Mileage = legacyVehicle.InventoryItem.MileageReceived,
                                      StockNumber = legacyVehicle.InventoryItem.StockNumber,
                                      Vin = legacyVehicle.InventoryItem.VIN,
                                      Engine = legacyVehicle.Engine,
                                      FuelType = legacyVehicle.VehicleAttributes.FuelType,
                                      Transmission = legacyVehicle.Transmission,
                                      Class = legacyVehicle.InventoryItem.MarketClass,
                                      Trim = legacyVehicle.Trim,
                                      BodyStyle = legacyVehicle.VehicleAttributes.CFBodyType,
                                      ExteriorColor = legacyVehicle.ExteriorColor,
                                      InteriorColor = legacyVehicle.InteriorColor,
                                      Drivetrain = legacyVehicle.Drivetrain,
                                      Certified = legacyVehicle.InventoryItem.Certified,
                                      Equipment = new List<string>(),
                                      Packages = new List<string>(),
                                      MpgCity = legacyVehicle.FuelCity,
                                      MpgHwy = legacyVehicle.FuelHwy,
                                      CertifiedID = legacyVehicle.CertifiedID,
                                      KBBBookValue = legacyVehicle.KBBBookValue,
                                      NADABookValue = legacyVehicle.NADABookValue,
                                      MSRP = legacyVehicle.InventoryItem.MSRP,
                                  };

            // Vehicle Description is a 'composite property'
            vehicleData.Description = legacyVehicle.InventoryItem.YearMakeModel + " " + vehicleData.Trim;

            var legacyEquipment = GetLegacyEquipment(legacyVehicle);

            vehicleData.Equipment.AddRange(legacyEquipment);

            var packages = GetPackages(legacyVehicle);

            vehicleData.Packages.AddRange(packages);

            Log.DebugFormat("Returning VehicleData with {0} equipment items", vehicleData.Equipment.Count);

            return vehicleData;
        }

        private static IList<string> GetPackages(IVehicleTemplatingData legacyVehicle)
        {
            return new List<string>(legacyVehicle.VehicleConfig.VehicleOptions.GetHighlightedPackages());
        }

        /// <summary>
        /// Takes a IVehicleTemplatingData returns a list of strings that represent its equipment.
        /// Equipment included (repsectively):
        /// Option Packages (Short Description or Summary)
        /// Tier1 Equipment
        /// Tier2 Equipment
        /// Tier3 Equipment
        /// Each tier has its equipment items sorted using the default comparer (currently their display priority).
        /// </summary>
        /// <param name="legacyVehicle">legacy vehicle to use</param>
        /// <returns>List of equipment items descriptions for the legacy vehicle</returns>
        private static IList<string> GetLegacyEquipment(IVehicleTemplatingData legacyVehicle)
        {
            /*
             * What about replacements?  
             * There is code that replaces multiple equipment items that refer to the same equipment category with
             * a single equipment item that is more comprehensive in nature.  
             * I.e., replace "Heated Seats" and "Leather Seats" with "Heated Leather Seats".
             * Right now, we access variables from DisplayEquipmentCollection at a level below the where the replacements occur.
             * If we want to utilize the replacements, we would need to just use the DisplayEquipmentCollection.SortedEquipmentList property.
             * By using this property, we will be working with a list of equipment that has the replacements.
             * dh - 01/06/2011
             */

            var legacyEquipment = new List<string>();

            // This little gem of a method actually controls what is returned by the GetListByTier calls.
            legacyVehicle.VehicleConfig.VehicleOptions.SetDisplayPreferences(new[] { MAX_TIERED_EQUIPMENT, MAX_TIERED_EQUIPMENT, MAX_TIERED_EQUIPMENT, MAX_TIERED_EQUIPMENT });

            // Get the option packages
            var optionPackages = new List<ITieredEquipment>(legacyVehicle.VehicleConfig.VehicleOptions.options);
            LogOptionPackages(optionPackages);

            // sort the option packages
            optionPackages.Sort();

            // We use the Summary property of the option package (the description is too long)
            foreach (var eq in optionPackages)
            {

                // we have two package types to support:
                // 1.  Standard Packages
                // 2.  Highlighted Packages
                // Standard Packages have empty Summary properties.  Highlighted Packages have both Summary and Description properties.
                // We should use the Summary when available (! null or empty) and use the Description otherwise.

                // get the summary
                string equipText = eq.GetSummary().Trim();

                if (string.IsNullOrEmpty(equipText))
                {   
                    // summary is unusable, get the description
                    equipText = eq.GetDescription().Trim();

                    if (string.IsNullOrEmpty(equipText))
                    {   // description is unusable, log this...
                        if (legacyVehicle.InventoryItem == null)
                        {
                            Log.InfoFormat("Equipment not used because Summary and Description were both null or empty:  Null InventoryItem reference:  legacyVehicle.InventoryItem == null");
                        }
                        else
                        {
                            Log.InfoFormat("Equipment not being used because Summary is null or empty and Description is null or empty. InventoryId: {0}", legacyVehicle.InventoryItem.InventoryID);    
                        }
                    }
                }

                // if we made it out of there with anything in the equipText 
                // member variable, use it.  Otherwise, don't...it was logged.
                if (! string.IsNullOrEmpty(equipText))
                    legacyEquipment.Add(equipText);
            }

            // Get the tiered equipment
            var tier1List = legacyVehicle.VehicleConfig.VehicleOptions.GetListByTier(1);
            var tier2List = legacyVehicle.VehicleConfig.VehicleOptions.GetListByTier(2);
            var tier3List = legacyVehicle.VehicleConfig.VehicleOptions.GetListByTier(3);

            // sort the tiered equipment
            tier1List.Sort();
            tier2List.Sort();
            tier3List.Sort();

            // log the tiered equipment
            LogTieredEquipmentList(1, tier1List);
            LogTieredEquipmentList(2, tier2List);
            LogTieredEquipmentList(3, tier3List);

            // combine the legacy tiered equipment together
            var legacyTieredEquipment = new List<ITieredEquipment>();
            legacyTieredEquipment.AddRange(tier1List);
            legacyTieredEquipment.AddRange(tier2List);
            legacyTieredEquipment.AddRange(tier3List);

            foreach (var eq in legacyTieredEquipment)
            {
                legacyEquipment.Add(eq.GetDescription());
            }

            return legacyEquipment;
        }

        private static void LogOptionPackages(IList<ITieredEquipment> equipment)
        {
            Log.DebugFormat("legacy vehicle option packages, count:{0}:", equipment.Count);
            foreach (var eq in equipment)
            {
                Log.DebugFormat("{0}: Summary:{1} , Description:{2}", equipment.IndexOf(eq), eq.GetSummary(), eq.GetDescription());
            }
        }

        private static void LogTieredEquipmentList(int tier, IList<ITieredEquipment> equipment)
        {
            Log.DebugFormat("legacy vehicle tier {0}, count:{1}:", tier, equipment.Count);
            foreach (var eq in equipment)
            {
                Log.DebugFormat("{0}: {1} (GetTier()={2})", equipment.IndexOf(eq), eq.GetDescription(), eq.GetTier());
            }
        }

        public LegacyVehicleAdapter(string userName)
        {
            _userName = userName;
        }
        
        #region Code to factor out sometime soon
        /*
         * 
         * These were put here to easily do the conversion from vehicle handle to vehicle entity id.
         * There is domain code in pricing that can be used to do the same thing.
         * 
         */
        private static bool VehicleIsInventory(string vehicleHandle)
        {
            if (string.IsNullOrEmpty(vehicleHandle))
                return false;

            return vehicleHandle.Substring(0, 1) == "1";
        }

        private static int GetVehicleEntityId(string vehicleHandle)
        {
            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ApplicationException("vehiclehandle was null or empty.");

            return int.Parse(vehicleHandle.Substring(1));
        }
        #endregion

    }
}
