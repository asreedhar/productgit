using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Text;
using AmazonServices.S3;
using Core.Messaging;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing
{
    /// <summary>
    /// Write a document to S3.  Yes, the abstractions on top of AWS are breaking down.
    /// </summary>
    internal class DocumentWriter
    {
        #region Logging
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        #endregion Logging

        public IFileStorage FileStorage { get; set; }

        public DocumentWriter(string bucketName, NameValueCollection headers = null, 
                              string contentType = "text/plain", char bucketNameDelimiter = '_',
                              bool useGlobalNamingRules = true)
        {
            FileStorage = new FileStore(bucketName, headers, contentType, bucketNameDelimiter, useGlobalNamingRules);    
        }

        public DocumentWriter(IFileStorage fileStorage)
        {
            FileStorage = fileStorage;
        }

        /// <summary>
        /// Save the document to Amazon S3
        /// </summary>
        /// <param name="documentInfo"></param>
        /// <returns></returns>
        public void Publish(DocumentInfo documentInfo)
        {
            var stream = new MemoryStream(Encoding.ASCII.GetBytes(documentInfo.Content));
            FileStorage.Write(documentInfo.Key, stream);
            Log.DebugFormat("MAX document published with key '{0}' written to S3 bucket {1}.", 
                documentInfo.Key, FileStorage.ContainerName());
        }
    }
}