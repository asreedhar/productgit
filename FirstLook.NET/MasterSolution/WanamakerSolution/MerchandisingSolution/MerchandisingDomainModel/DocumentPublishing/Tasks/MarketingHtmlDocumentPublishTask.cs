﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    public class MarketingHtmlDocumentPublishTask : MaxDocumentPublishTask<MaxDocumentPublishMessage>
    {
        private readonly IMarketingDocumentRepository _marketingDocumentRepository;

        public MarketingHtmlDocumentPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory,
            IMarketingDocumentRepository marketingDocumentRepository, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishMarketingHtmlDocumentsQueue(), fileStoreFactory, Encoding.UTF8, documentPublishMessager)
        {
            _marketingDocumentRepository = marketingDocumentRepository;
        }

        protected override PublicationInfo CreateDocument(MaxDocumentPublishMessage message)
        {
            Log.Info("Starting CreateDocument for MarketingHtmlDocumentPublishTask");

            var marketingHtml = _marketingDocumentRepository.GetMarketingHtml(message.BusinessUnitId, message.InventoryId);

            if (string.IsNullOrWhiteSpace(marketingHtml))
            {
                Log.WarnFormat("No HTML found for message: {0}", message.ToJson());
                return null;
            }
            // Push publication message to SNS / S3
            try
            {
                var vin = message.Vin;
                var amzHeaders = new NameValueCollection
					{
						{"x-amz-meta-businessunitid", message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
						{"x-amz-meta-inventoryid", message.InventoryId.ToString(CultureInfo.InvariantCulture)},
						{"x-amz-meta-vin", vin},
						{"x-amz-meta-generated_at", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)}
					};

                var fileStore = FileStoreFactory.MaxDocStorage(contentType: "text/html", headers: amzHeaders);
                var key = "marketing-html/" + vin + ".html";

                return new PublicationInfo
                {
                    Content = marketingHtml,
                    FileStore = fileStore,
                    Key = key,
                    Sender = GetType().FullName,
                    Vin = message.Vin,
                    MessageType = "marketing-html",
                    ContentType = "text/html"
                };
            }
            catch (Exception ex)
            {
                Log.Error("Unable to process marketing html", ex);
            }
            return null;
        }
    }
}