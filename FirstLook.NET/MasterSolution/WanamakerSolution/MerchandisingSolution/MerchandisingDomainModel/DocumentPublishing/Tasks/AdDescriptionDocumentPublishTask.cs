﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Xml.Serialization;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    class AdDescriptionDocumentPublishTask : MaxDocumentPublishTask<MaxDocumentPublishMessage>
    {
        public AdDescriptionDocumentPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory, 
            Encoding encoding, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishAdDescriptionDocumentsQueue(), fileStoreFactory, encoding, documentPublishMessager)
        {
        }

        protected override PublicationInfo CreateDocument(MaxDocumentPublishMessage message)
        {
            Log.DebugFormat("Processing MaxDocumentPublishMessage for vin {0}", message.Vin);

            MerchandisingDescription merchandisingDescription = 
                MerchandisingDescription.Fetch(message.BusinessUnitId,message.InventoryId);

            // try to get these.  
            string desc = null;
            string preview = null;

            if (merchandisingDescription.Description != null)
            {
                desc = merchandisingDescription.Description;
            }
            try
            {
                preview = merchandisingDescription.AdvertisementModel.Preview.Paragraph.Items.GetCombinedText();
            }
            catch (NullReferenceException)
            {
                // can't get the preview. something is null in those nested properties (which stink, by the way).
                // visitor?
            }

            AdDescriptionXMLExport adDescriptionXmlExport = new AdDescriptionXMLExport();

            if (desc == null && preview == null)
            {
                // publish the mostly empty doc.
                return PublishXmlDocument(message, adDescriptionXmlExport);
            }

            if (desc != null) adDescriptionXmlExport.Description = desc;
            if (preview != null) adDescriptionXmlExport.Preview = preview;
         
            // publish the doc w/ one or both properties set.
            return PublishXmlDocument(message, adDescriptionXmlExport);
        }

        private PublicationInfo PublishXmlDocument(MaxDocumentPublishMessage message, AdDescriptionXMLExport adDescriptionXmlExport)
        {
            string xmlToPublish = GetEncodedContentString(adDescriptionXmlExport, new XmlRootAttribute("AdDescription"));

            var amzHeaders = new NameValueCollection
            {
                {"x-amz-meta-businessunitid", message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-inventoryid", message.InventoryId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-vin", message.Vin}
            };

            var fileStore = FileStoreFactory.MaxDocStorage(contentType: "text/xml", headers: amzHeaders);
            var key = "adDescription/" + message.Vin + ".xml";

            return new PublicationInfo
            {
                Content = xmlToPublish,
                FileStore = fileStore,
                Key = key,
                Sender = GetType().FullName,
                Vin = message.Vin,
                MessageType = "adDescription",
                ContentType = "text/xml"
            };
        }
    }

    public class AdDescriptionXMLExport
    {
        public string Preview { get; set; }
        public string Description { get; set; }
    }

}
