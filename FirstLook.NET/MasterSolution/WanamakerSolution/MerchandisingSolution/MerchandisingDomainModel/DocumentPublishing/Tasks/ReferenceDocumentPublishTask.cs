﻿using System.Collections.Specialized;
using System.Globalization;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    public class ReferenceDocumentPublishTask : MaxDocumentPublishTask<MaxDocumentPublishMessage>
    {
        private readonly IReferenceDataRepository _referenceDataRepository;

        public ReferenceDocumentPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory,
            IReferenceDataRepository referenceDataRepository, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishReferenceDocumentsQueue(), fileStoreFactory, documentPublishMessager)
        {
            _referenceDataRepository = referenceDataRepository;
        }

        protected override PublicationInfo CreateDocument(MaxDocumentPublishMessage message)
        {
            Log.DebugFormat("Processing MaxDocumentPublishMessage for vin {0}", message.Vin);

            if (message.Vin == null)
                return null;

            // from the vin, get the selected equipment and publish that information as xml to the S3 document storage
            var referenceData = _referenceDataRepository
                .GetPublishableReferenceData(message.Vin);

            referenceData.FuelEconomyRatings = _referenceDataRepository.GetFuelEcomonayRating(message.BusinessUnitId,
                                                                                              message.InventoryId);

            var xmlToPublish = referenceData.ToXmlString();

            var amzHeaders = new NameValueCollection
            {
                {"x-amz-meta-businessunitid",   message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-inventoryid",      message.InventoryId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-vin",              message.Vin}
            };

            var fileStore = FileStoreFactory.MaxDocStorage(contentType: "text/xml", headers: amzHeaders);
            var key = "reference/" + message.Vin + ".xml";

            return new PublicationInfo
            {
                Content = xmlToPublish,
                FileStore = fileStore,
                Key = key,
                Sender = GetType().FullName,
                Vin = message.Vin,
                MessageType = "reference",
                ContentType = "text/xml"
            };
        }
    }
}
