using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.Messaging;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.IO;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    public abstract class MaxDocumentPublishTask<T> : TaskRunner<T> where T : DocumentPublishCommand
    {
        #region Logging
        protected readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        #endregion Logging

        protected readonly IFileStoreFactory FileStoreFactory;
        private readonly Encoding _encoding;
        private readonly IDocumentPublishMessager _documentPublishMessager;

        protected MaxDocumentPublishTask(IQueue<T> queue, IFileStoreFactory fileStoreFactory, IDocumentPublishMessager documentPublishMessager)
            : this(queue, fileStoreFactory, Encoding.UTF8, documentPublishMessager)
        { }

        protected MaxDocumentPublishTask(IQueue<T> queue, IFileStoreFactory fileStoreFactory, Encoding encoding, IDocumentPublishMessager documentPublishMessager)
             : base(queue)
        {
             FileStoreFactory = fileStoreFactory;
 
            var bus = BusFactory.CreatePublishDocumentsBus<T, T>();
            bus.Subscribe(queue);
 
            _encoding = encoding;
            _documentPublishMessager = documentPublishMessager;
        }

        protected MaxDocumentPublishTask(IQueue<T> queue, IFileStoreFactory fileStoreFactory, Encoding encoding,
                IBus<T, T> bus, IDocumentPublishMessager documentPublishMessager)
            : base(queue)
        {
            FileStoreFactory = fileStoreFactory;

            bus.Subscribe(queue);

            _encoding = encoding;
            _documentPublishMessager = documentPublishMessager;
        }

        
        /// <summary>
        /// Implement a handler that returns the necessary PublicationInfo and get publishing and notification for free.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected abstract PublicationInfo CreateDocument(T message);

        public override void Process(T message)
        {
	        try
            {
	            if (string.IsNullOrWhiteSpace(message.Vin))
	            {
		            Log.ErrorFormat("Unable to publish messages with no vin: {0}", message.ToJson());
		            return;
	            }

                if (IsOffline(message.BusinessUnitId, message.Vin))
                {
                    Log.InfoFormat("Will not post data for Offline vehicles.");
                    return;
                }

                var doc = CreateDocument(message);

                if (doc != null)
                {
                    Publish(doc);
                    Notify(doc);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);                
            }
        }

        /// <summary>
        /// Find out if this vehicle has been marked Offline.
        /// </summary>
        /// <remarks>
        /// Since this will be called several times in a row every time we generate new
        /// documents, I considered caching the results of the call. But I don't want 
        /// to miss publishing documents when the user accidentally marks a vehicle offline 
        /// and then changes their mind. It sure would be nice if we could cache 
        /// these results in a central location shared by all the servers, even 
        /// the webservers. Then we could clear the cache correctly as necessary.
        ///
        /// Update: We now use a CachedCommand to get this data using whatever ICache is injected.  
        /// I wanted to leave the informative comment above though.
        /// - dh, 2014-12-24
        /// 
        /// </remarks>
        /// <param name="businessUnitId"></param>
        /// <param name="vin"></param>
        /// <returns></returns>
        private static bool IsOffline(int businessUnitId, string vin)
        {
            var cmd = new GetOfflineStatus(businessUnitId, vin);
            AbstractCommand.DoRun(cmd);
            return cmd.IsOffline;
        }
        
        /// <summary>
        /// Publish an SNS message to the correct generic topic
        /// </summary>
        /// <param name="publicationInfo"></param>
        internal protected void Notify(PublicationInfo publicationInfo)
        {
            _documentPublishMessager.SendDocumentPublishNotification(publicationInfo);
        }


        /// <summary>
        /// Save the generated document to Amazon S3
        /// </summary>
        /// <param name="publicationInfo"></param>
        /// <returns></returns>
        protected void Publish(PublicationInfo publicationInfo)
        {
            
            // replaced to use windows default encoding or used when class is created
            //var stream = new MemoryStream(Encoding.ASCII.GetBytes(publicationInfo.Content));
            var stream = new MemoryStream(_encoding.GetBytes(publicationInfo.Content));

            publicationInfo.FileStore.Write(publicationInfo.Key, stream);

            Log.DebugFormat("MAX document published with key '{0}' written to S3 bucket {1}.", publicationInfo.Key, publicationInfo.FileStore.ContainerName());
        }
        public string GetEncodedContentString(object dataToSerialize, XmlRootAttribute xmlRoot)
        {
            return GetEncodedContentString(dataToSerialize, xmlRoot, _encoding);
        }
        /// <summary>
        /// serializes an object to XML with speical encoding logic 
        /// </summary>
        /// <param name="dataToSerialize"></param>
        /// <param name="xmlRoot"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string GetEncodedContentString(object dataToSerialize, XmlRootAttribute xmlRoot, Encoding encoding)
        {
            string content;

            var xmlSerializer = new XmlSerializer(dataToSerialize.GetType(), xmlRoot);

            var xmlWriterSettings = new XmlWriterSettings {Indent = true, Encoding = encoding};


            using (var stringWriter = new StringWriterWithEncoding(encoding))
            using (var xmlTextWriter = XmlWriter.Create(stringWriter, xmlWriterSettings))
            {
                xmlSerializer.Serialize(xmlTextWriter, dataToSerialize);
                xmlTextWriter.Flush();
                content = stringWriter.GetStringBuilder().ToString();
            }

            return content;
        }
    }
}