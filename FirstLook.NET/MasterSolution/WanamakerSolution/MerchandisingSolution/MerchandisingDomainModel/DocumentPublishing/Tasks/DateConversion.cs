using System;
using System.Globalization;
using System.Reflection;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    public class DateConversion
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public DateConversion()
        {
        }

        /// <summary>
        /// Convert a local timestamp to a UTC ticks string.
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        internal string LocalTimeStampToUtcTicksString(DateTime? timeStamp)
        {
            if (!timeStamp.HasValue)
            {
                Log.Warn("Specified timestamp is null.");
                return string.Empty;
            }

            string utcTicks = timeStamp.Value.ToUniversalTime().Ticks.ToString(CultureInfo.InvariantCulture);
            Log.DebugFormat("Specified local timestamp of {0} converted to {1} utc ticks.", 
                timeStamp.Value.ToString(CultureInfo.InvariantCulture), utcTicks);

            return utcTicks;
        }

        /// <summary>
        /// Convert a UTC ticks string to a local timestamp.
        /// </summary>
        /// <param name="utcTicks"></param>
        /// <returns></returns>
        internal DateTime? UtcTicksStringToLocalTimeStamp(string utcTicks)
        {
            if (string.IsNullOrWhiteSpace(utcTicks))
            {
                Log.Warn("Specified utc ticks is null or whitespace.");
                return null;
            }

            long utcTicksLong;
            bool parseSuccess = long.TryParse(utcTicks, out utcTicksLong);

            if (!parseSuccess)
            {
                Log.WarnFormat("Specified utc ticks value of {0} does not parse to long.", utcTicks);
                return null;
            }

            var t = new DateTime(utcTicksLong, DateTimeKind.Utc);
            return t.ToLocalTime();
        }
    }
}