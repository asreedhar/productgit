﻿using System.Collections.Specialized;
using System.Globalization;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using System;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    public class MaxEquipmentDocumentPublishTask : MaxDocumentPublishTask<MaxDocumentPublishMessage>
    {
        private readonly IEquipmentRepository _equipmentRepository;

        public MaxEquipmentDocumentPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory, 
            IEquipmentRepository equipmentRepository, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishEquipmentDocumentsQueue(), fileStoreFactory, documentPublishMessager)
        {
            _equipmentRepository = equipmentRepository;
        }

        protected override PublicationInfo CreateDocument(MaxDocumentPublishMessage message)
        {
            Log.DebugFormat("Processing MaxEquipmentDocumentPublishMessage for vin {0}", message.Vin);

            // from the vin, get the selected equipment and publish that information as xml to the S3 document storage
            var publishableVins = _equipmentRepository
                .GetPublishableVinEquipment(message.BusinessUnitId, message.InventoryId, message.Vin);

            if(publishableVins != null)
            {
                var amzHeaders = new NameValueCollection
                {
                    {"x-amz-meta-businessunitid",   message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
                    {"x-amz-meta-inventoryid",      message.InventoryId.ToString(CultureInfo.InvariantCulture)},
                    {"x-amz-meta-vin",              message.Vin},
                    {"x-amz-meta-generated_at",     DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)}
                };

                var fileStore = FileStoreFactory.MaxDocStorage(contentType: "text/xml", headers: amzHeaders);
                var key = "equipment/" + message.Vin + ".xml";

                return new PublicationInfo
                {
                    Content = publishableVins.ToXmlString(), 
                    FileStore = fileStore, 
                    Key = key, 
                    Sender = GetType().FullName,
                    Vin = message.Vin,
                    MessageType = "equipment",
                    ContentType = "text/xml"
                };
            }

            return null;
        }
    }
}
