﻿using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    public class PhotoDocumentPublishTask : MaxDocumentPublishTask<MaxPhotoPublishMessage>
    {
        public PhotoDocumentPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishPhotosQueue(), fileStoreFactory, Encoding.UTF8,  
            BusFactory.CreatePublishPhotoDocumentsBus<MaxPhotoPublishMessage, MaxPhotoPublishMessage>(), documentPublishMessager)
        {
        }

        protected override PublicationInfo CreateDocument(MaxPhotoPublishMessage message)
        {
            Log.DebugFormat("Processing MaxPhotoPublishMessage for vin {0}", message.Vin);

            var amzHeaders = new NameValueCollection
            {
                {"x-amz-meta-businessunitid", message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-vin", message.Vin}
            };

            var fileStore = FileStoreFactory.MaxDocStorage(contentType: "text/xml", headers: amzHeaders);
            var key = "photos/" + message.Vin + ".xml";

            return new PublicationInfo
            {
                Content = message.PhotoUrlsXml,
                FileStore = fileStore,
                Key = key,
                Sender = GetType().FullName,
                Vin = message.Vin,
                MessageType = "photos",
                ContentType = "text/xml"
            };
        }
    }
}
