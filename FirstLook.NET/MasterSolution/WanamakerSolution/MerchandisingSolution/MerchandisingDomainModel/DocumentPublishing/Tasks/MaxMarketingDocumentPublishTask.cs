﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
	public class MaxMarketingDocumentPublishTask : MaxDocumentPublishTask<MaxDocumentPublishMessage>
	{
		private readonly IMarketingDocumentRepository _marketingDocumentRepository;

        public MaxMarketingDocumentPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory, 
            IMarketingDocumentRepository marketingDocumentRepository, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishMarketingDocumentsQueue(), fileStoreFactory, Encoding.UTF8, documentPublishMessager)
		{
			_marketingDocumentRepository = marketingDocumentRepository;
		}

		protected override PublicationInfo CreateDocument(MaxDocumentPublishMessage message)
		{
			Log.Info("Starting CreateDocument for MaxMarketingDocumentPublishTask");

			var documentXml = _marketingDocumentRepository.GetMarketingXml(message.BusinessUnitId, message.InventoryId);

			if (string.IsNullOrWhiteSpace(documentXml))
			{
				Log.WarnFormat("No XML found for message: {0}", message.ToJson());
				return null;
			}
            // Push publication message to SNS / S3
			try
			{
				// Get rid of all the extra back slashes that this xml has (for legacy reasons)
				var xml = documentXml.Replace(@"\", "");

				// get the vin
				// ["salesaccelerator"]["vehicleyearmodel"]["vehiclesummary"]["vin"]
				var xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(xml);
				var vinNode = xmlDoc.SelectSingleNode("//salesaccelerator/vehicleyearmodel/vehiclesummary/vin");
				if (vinNode != null)
				{
					var vin = vinNode.InnerText;

					var amzHeaders = new NameValueCollection
					{
						{"x-amz-meta-businessunitid", message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
						{"x-amz-meta-inventoryid", message.InventoryId.ToString(CultureInfo.InvariantCulture)},
						{"x-amz-meta-vin", vin},
						{"x-amz-meta-generated_at", DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)}
					};

					var fileStore = FileStoreFactory.MaxDocStorage(contentType: "text/xml", headers: amzHeaders);
					var key = "marketing/" + vin + ".xml";
				    
					return new PublicationInfo
					{
						Content = xmlDoc.OuterXml,
						FileStore = fileStore,
						Key = key,
						Sender = GetType().FullName,
						Vin = message.Vin,
						MessageType = "marketing",
						ContentType = "text/xml"
					};
				}
				
				Log.ErrorFormat("Vin node is null for message: {0}", message.ToJson());
			}
			catch (Exception ex)
			{
				Log.Error("Unable to process marketing xml", ex);
			}
			return null;
		}
	}
}