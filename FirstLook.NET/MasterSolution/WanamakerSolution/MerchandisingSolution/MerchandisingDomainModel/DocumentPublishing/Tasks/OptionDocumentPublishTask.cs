﻿using System.Collections.Specialized;
using System.Globalization;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    public class OptionDocumentPublishTask : MaxDocumentPublishTask<MaxDocumentPublishMessage>
    {
        private readonly IOptionsRepository _optionsRepository;

        public OptionDocumentPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory,
            IOptionsRepository equipmentRepository, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishOptionDocumentsQueue(), fileStoreFactory, documentPublishMessager)
        {
            _optionsRepository = equipmentRepository;
        }

        protected override PublicationInfo CreateDocument(MaxDocumentPublishMessage message)
        {
            Log.DebugFormat("Processing MaxDocumentPublishMessage for vin {0}", message.Vin);

            // from the vin, get the selected equipment and publish that information as xml to the S3 document storage
            var optionsList = _optionsRepository
                .GetPublishableVinOptions(message.BusinessUnitId, message.InventoryId, message.Vin);

            if (optionsList == null || optionsList.Count == 0)
            {
                Log.Info("Empty options list encountered. We don't publish these.");
                return null; // returning null prevents the base class from publishing or sending notifications.
                // NOTE: The downside to this approach is that you won't be able to remove all options from a vehicle
                // and then publish that empty options data, but I think that's very uncommon.  It's much more common that
                // FLDW or some table that our repo queries is being reloaded and that our query returns no records, temporarily.
                // This is bad b/c we could end up puslishing an empty options list for a car that really does have options. ZB 11/3/2014
            }

            var xmlToPublish = OptionsToXml.ToXmlString(optionsList);

            var amzHeaders = new NameValueCollection
            {
                {"x-amz-meta-businessunitid",   message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-inventoryid",      message.InventoryId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-vin",              message.Vin}
            };

            var fileStore = FileStoreFactory.MaxDocStorage(contentType: "text/xml", headers: amzHeaders);
            var key = "options/" + message.Vin + ".xml";

            return new PublicationInfo
            {
                Content = xmlToPublish, 
                FileStore = fileStore, 
                Key = key, 
                Sender = GetType().FullName,
                Vin = message.Vin,
                MessageType = "options",
                ContentType = "text/xml"
            };
        }
    }
}
