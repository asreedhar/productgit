﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using FirstLook.Merchandising.DomainModel.Pricing;
using log4net;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    /// <summary>
    /// This goal of this task is to notify external systems about the used inventory that has recently
    /// become inactive in MAX.   
    /// 
    /// The task listens for task control messages.  When a start message is received, the task will find all of the
    /// inactive, used inventory for the specified business unit that is more recent than the
    /// specified "IgnoreEverythingEarlierThan" value. 
    ///    
    /// It will issue an InventoryActivityMessage for each inventory meeting the above criteria (signaling to downstream 
    /// clients that the inventory is inactive). 
    /// 
    /// If no start date is supplied in the start message, the task will use either (1) the task's last start time,
    /// or (2) 6 months ago if no last start time can be found.
    /// </summary>
    public class InactiveInventoryTask : TaskRunner<InactiveInventoryTaskStartMessage>
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly UsedInventoryRepository _usedInventoryRepository;
        private readonly IFileStorage _storage;
        private readonly DateConversion _dateConversion;
        private readonly IDocumentPublishMessager _messageSender;

        private const string FileName = "InactiveInventoryTask-LastStart";

        public InactiveInventoryTask(IQueueFactory queueFactory, IFileStoreFactory fileStoreFactory, IDocumentPublishMessager messageSender) 
            : base(queueFactory.CreateInactiveInventoryTaskControlQueue())
        {
            _usedInventoryRepository = new UsedInventoryRepository();
            _storage = fileStoreFactory.CreateFileStore();
            _dateConversion = new DateConversion();
            _messageSender = messageSender;
        }

        public override void Process(InactiveInventoryTaskStartMessage message)
        {
            var thisStartTime = DateTime.Now;
            var sw = new Stopwatch();
            sw.Start();
	        var threadId = string.Format("BusinessUnitIds {0}", message.BusinessUnitIds.ToDelimitedString(","));
			ThreadContext.Properties["threadId"] = threadId;
            Log.Info("InactiveInventoryTask started");

            // How far back should we look?
            var lastStartTime = GetLastStartTimeInLocalTimeZone(FileName, _storage);
            var ignoreBeforeThisDate = GetLowerBoundLocalTime(message.IgnoreEverythingEarlierThan, lastStartTime);
            Log.InfoFormat("Ignore before date set to {0}", ignoreBeforeThisDate.ToString(CultureInfo.InvariantCulture));

            foreach (var buid in message.BusinessUnitIds)
            {
                try
                {
                    Log.DebugFormat("Processing dealer {0} found in the message.", buid);
                    ProcessInactiveInventoryAtDealer(ignoreBeforeThisDate, buid);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Error processing dealer with businessUnitId {0}: {1}", buid, ex);
                }
            }
            // save our last start time.
            SetLastStartTimeInLocalTimeZone(thisStartTime, FileName, _storage);

            sw.Stop();
            Log.InfoFormat("InventoryActivityTask finished in {0}", sw.Elapsed.ToString());
        }


        private void ProcessInactiveInventoryAtDealer(DateTime ignoreBeforeThisDate, int businessUnitId)
        {
            Log.DebugFormat("Processing dealer {0}.", businessUnitId);

            // Get the used inventory summary data.
            var allUsedInventorySummaries = _usedInventoryRepository.GetUsedInventorySummary(businessUnitId).ToList();

            // Ignore any active inventory.
            var inactive = FilterInactive(allUsedInventorySummaries);

            // Ignore any records before our ignore date.
            var recentInactive = FilterCurrentInactiveInventory(inactive, ignoreBeforeThisDate).ToList();

            // Get the dealer's owner handle
            string ownerHandle;
            try
            {
                ownerHandle = PricingData.GetOwnerHandle(businessUnitId);
                Log.DebugFormat("Dealer has owner handle {0}", ownerHandle);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error getting owner handle for dealer {0}: {1}", businessUnitId, ex);
                return;
            }

            foreach (var inventory in recentInactive)
            {
                try
                {
                    Log.DebugFormat("Processing inventoryId {0}", inventory.InventoryId);

                    // TODO: get the handles via the summary query to avoid calling sproc in loop
                    var handles = PricingData.GetVehicleHandle(ownerHandle, inventory.InventoryId);

                    ProcessInActiveInventorySummary(businessUnitId, ownerHandle, inventory, handles);
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("Error processing inventoryId {0}: {1}", inventory.InventoryId, ex);
                }
            }
        }

        internal IEnumerable<InventorySummary> FilterInactive(IEnumerable<InventorySummary> input)
        {
            return input.Where(inv => !inv.InventoryActive);
        }

        /// <summary>
        /// Any inactive inventory with a delete date before our ignore date can be ignored.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="ignoreBeforeThisDate"></param>
        /// <returns></returns>
        internal IEnumerable<InventorySummary> FilterCurrentInactiveInventory(IEnumerable<InventorySummary> input, 
            DateTime ignoreBeforeThisDate)
        {
            var inputList = input.ToList();

            var inactiveRecent =
                inputList.Where(inv => !inv.InventoryActive)
                    .Where(inv => inv.DeleteDt.HasValue && inv.DeleteDt.Value >= ignoreBeforeThisDate)
                    .ToList();

            var inactiveNoDelete =
                inputList.Where(inv => !inv.InventoryActive)
                    .Where(inv => !inv.DeleteDt.HasValue)
                    .ToList();

            return inactiveRecent.Union(inactiveNoDelete);
        }



        internal void ProcessInActiveInventorySummary(int businessUnitId, string ownerHandle,
            InventorySummary summary, Pair<VehicleHandle, SearchHandle> handles)
        {
            _messageSender.PublishInactiveInventoryMessage(businessUnitId, ownerHandle, summary, handles);
        }

        /// <summary>
        /// The lower bound is either: 
        /// (1) a specified timestamp. 
        /// (2) our last start time,
        /// (3) a reasonable default offset
        /// They are prioritized in that order.
        /// </summary>
        /// <param name="ignoreEverythingEarlierThan"></param>
        /// <param name="lastStartTime"></param>
        /// <returns></returns>
        internal DateTime GetLowerBoundLocalTime(DateTime? ignoreEverythingEarlierThan, DateTime? lastStartTime)
        {
            if (ignoreEverythingEarlierThan.HasValue)
            {
                Log.DebugFormat("Using provided lower bound of {0}", ignoreEverythingEarlierThan.Value.ToString(CultureInfo.InvariantCulture));
                return ignoreEverythingEarlierThan.Value;
            }

            if (lastStartTime.HasValue)
            {
                Log.DebugFormat("Using last start time as lower bound of {0}", lastStartTime.Value.ToString(CultureInfo.InvariantCulture));
                return lastStartTime.Value;
            }

            // Assume we haven't run in 6 months if we can't determine the actual last start time.
            DateTime defaultStartTime = DateTime.Now.AddMonths(-6);
            Log.DebugFormat("Using default last start time as lower bound of {0}", defaultStartTime.ToString(CultureInfo.InvariantCulture));
            return defaultStartTime;
        }

        /// <summary>
        /// Retrieve our last start time, returned as a local timestamp.
        /// </summary>
        /// <returns></returns>
        internal DateTime? GetLastStartTimeInLocalTimeZone(string fileName, IFileStorage storage)
        {
            // get the last time that we started running.
            bool exists;
            using (var stream = storage.Exists(fileName, out exists))
            {
                if (exists)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string value = reader.ReadToEnd();
                        Log.DebugFormat("Last start time value found in S3. Raw value is {0}", value);

                        DateTime? savedDateTime = _dateConversion.UtcTicksStringToLocalTimeStamp(value);
                        if (savedDateTime.HasValue)
                        {
                            DateTime? lastStartTime = savedDateTime.Value;
                            Log.DebugFormat("Last start time value in S3 converted to local timestamp value of {0}",
                                lastStartTime.Value.ToString(CultureInfo.InvariantCulture));
                            return lastStartTime;
                        }
                    }
                }
            }

            Log.Warn("Last start time not found in S3 or is unusable.");
            return null;
        }

        /// <summary>
        /// Record our last start time, specified as a local timestamp.
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="fileName"></param>
        /// <param name="storage"></param>
        internal void SetLastStartTimeInLocalTimeZone(DateTime startTime, string fileName, IFileStorage storage)
        {
            string utcTicks = _dateConversion.LocalTimeStampToUtcTicksString(startTime);

            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(utcTicks);
                writer.Flush();
                stream.Position = 0;
                storage.Write(fileName, stream);
                Log.DebugFormat("Last start time set in S3. Set value is {0}", utcTicks);
            }
        }
    }
}
