﻿﻿using System;
﻿using System.Collections.Generic;
﻿using System.Data;
﻿using System.IO;
﻿using System.Reflection;
﻿using System.Xml;
﻿using System.Xml.Serialization;
﻿using Core.Messaging;
using FirstLook.Common.Core.PhotoServices;
﻿using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
﻿using FirstLook.DomainModel.Oltp;
﻿using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    class PhotoDocumentGeneration : TaskRunner<DealerCommand>
    {
        #region Logging
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        #endregion Logging

        private readonly IPhotoServices _photoServices;
        private readonly XmlSerializer _serializer;
        private readonly IDocumentPublishMessager _documentPublishMessager;

        public PhotoDocumentGeneration(IDocumentPublishingQueueFactory queueFactory, IPhotoServices photoServices, 
            IDocumentPublishMessager documentPublishMessager) : base(queueFactory.MaxPublishDealerPhotoDocsQueue())
        {
            _photoServices = photoServices;
            _serializer = new XmlSerializer(typeof(VehiclePhotos), new XmlRootAttribute("photoUrls"));
            _documentPublishMessager = documentPublishMessager;
        }

       public override void Process(DealerCommand message)
       {
            // Find BU Id from BU code
            var businessUnit = BusinessUnitFinder.Instance().Find(message.BusinessUnitId);
            var buCode = businessUnit.BusinessUnitCode;
            Log.DebugFormat("Processing photo publish message for BU {0}", buCode);

            var vins = GetActiveViNsForBu(message.BusinessUnitId);
            
            // Call aultech for all photo urls for that BU
            var photoUrls = _photoServices.GetPhotoUrlsByBusinessUnit(buCode, TimeSpan.FromMinutes(10));
            Log.DebugFormat("Aultech returned {0} vinPhotos for BU {1}", photoUrls.PhotosByVin.Count, buCode);

            foreach (var vin in vins)
            {
                //  If this Vin exists in the return from aultech, serialize it to xml.  Otherwise write out an empty xml
                IVehiclePhotos vehiclePhotos;
                var exists = photoUrls.PhotosByVin.TryGetValue(vin, out vehiclePhotos);

                var xmlToPublish = exists ? ToXmlString((VehiclePhotos) vehiclePhotos) : ToXmlString(new VehiclePhotos());

                _documentPublishMessager.SendPhotoPublishMessage(message.BusinessUnitId, vin, xmlToPublish);
            }
        }

       public string ToXmlString(VehiclePhotos photoUrls)
        {
            using (var stringWriter = new StringWriter())
            {
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    _serializer.Serialize(xmlTextWriter, photoUrls);
                    xmlTextWriter.Flush();
                    return stringWriter.GetStringBuilder().ToString();
                }
            }
        }

       private IEnumerable<string> GetActiveViNsForBu(int businessUnitId)
       {
           var vehicles = new List<string>();

           using (var con = Release.Database.GetConnection(Release.Database.MerchandisingDatabase))
           {
               con.Open();
               using (var cmd = con.CreateCommand())
               {
                   cmd.CommandText = "merchandising.ActiveVINsByBU#Fetch";
                   cmd.CommandType = CommandType.StoredProcedure;
                   Release.Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                   using (var reader = cmd.ExecuteReader())
                   {
                       while (reader.Read())
                       {
                           var id = reader.GetString(reader.GetOrdinal("VIN"));
                           vehicles.Add(id);
                       }

                       reader.NextResult();
                   }
               }
           }

           return vehicles;
       }
    }
}