﻿using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl.V_2_0;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;
using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks
{
    /// <summary>
    /// Monitors a queue, publishes carfax report urls - writes them to s3 as json docs and sends messages to the
    /// generic max-document-changes topic.  
    /// </summary>
    public class CarFaxReportUrlPublishTask : MaxDocumentPublishTask<MaxDocumentPublishMessage>
    {
        private readonly ICarFaxUrlBuilder _carFaxUrlBuilder;

        public CarFaxReportUrlPublishTask(IDocumentPublishingQueueFactory queueFactory, IFileStoreFactory fileStoreFactory, 
            ICarFaxUrlBuilder carFaxUrlBuilder, IDocumentPublishMessager documentPublishMessager)
            : base(queueFactory.MaxPublishCarFaxReportUrlsQueue(), fileStoreFactory, documentPublishMessager)
        {
            _carFaxUrlBuilder = carFaxUrlBuilder;
        }

        protected override PublicationInfo CreateDocument(MaxDocumentPublishMessage message)
        {
            Log.DebugFormat("Processing MaxDocumentPublishMessage for vin {0}", message.Vin);

            // from the vin, get the selected equipment and publish that information as xml to the S3 document storage
            string carFaxUserId = GetCarFaxUserId(message.BusinessUnitId);
            if (carFaxUserId == null)
            {
                Log.InfoFormat("No carfax userId found.");
                return null;
            }

            string url = _carFaxUrlBuilder.BuildUrl(carFaxUserId, message.Vin);
            if (string.IsNullOrWhiteSpace(url))
            {
                Log.WarnFormat("Carfax report url is null or empty.");
                return null;
            }

            var amzHeaders = new NameValueCollection
            {
                {"x-amz-meta-businessunitid",   message.BusinessUnitId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-inventoryid",      message.InventoryId.ToString(CultureInfo.InvariantCulture)},
                {"x-amz-meta-vin",              message.Vin}
            };

            var fileStore = FileStoreFactory.MaxDocStorage(contentType: "application/json", headers: amzHeaders);
            var key = "carfax/" + message.Vin + ".json";

            var carFaxReport = new
            {
                ReportUrl = url
            };

            // The publiction will use the default notification topic.
            return new PublicationInfo
            {
                Content = carFaxReport.ToJson(), 
                FileStore = fileStore, 
                Key = key, 
                Sender = GetType().FullName,
                Vin = message.Vin,
                MessageType = "carfax",
                ContentType = "application/json"
            };
        }

        /// <summary>
        /// If the damn VHR services exposed this directly, I wouldn't have to do this.  Those services need
        /// some love.  Right now they aren't very friendly.  Also, note that there are two of them - a v1 and v2.
        /// No client binding was ever created for v2, though it does seem to have more useful functionality.
        /// ZB 5/2014
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <returns></returns>
        private string GetCarFaxUserId(int businessUnitId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                const string QUERY = @"
                    SELECT UserName
                    FROM CarFax.Account CA
                    JOIN CarFax.Account_Dealer AD
                    ON CA.AccountID = AD.AccountID
                    WHERE AD.DealerId = @businessUnitId";

                var inventory = sqlConnection.Query<string>(QUERY, new { BusinessUnitId=businessUnitId}); 
                sqlConnection.Close();

                return inventory.FirstOrDefault();
            }
        }
    }
}
