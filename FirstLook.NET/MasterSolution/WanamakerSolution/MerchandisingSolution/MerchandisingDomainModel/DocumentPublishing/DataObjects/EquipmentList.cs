﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects
{
    [Serializable]
    public class EquipmentList 
    {
        [NonSerialized]
        [XmlIgnore]
        private List<Item> _equipmentItems = new List<Item>();

        [XmlElement]
        public List<Item> Equipment
        {
            get { return _equipmentItems; } 
            set { _equipmentItems = value; }
        }
    }

    [Serializable]
    public class Item
    {
        public string Description;
        public string Category;
        public int Tier;
        public bool IsStandard;
    }

    public static class EquipmentListExtensions
    {
        public static string ToXmlString(this EquipmentList vinData)
        {
            var serializer = new XmlSerializer(typeof(EquipmentList));

            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                serializer.Serialize(xmlTextWriter, vinData);
                xmlTextWriter.Flush();
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}