﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects
{
    [Serializable]
    public class ReferenceData
    {
        [NonSerialized]
        [XmlIgnore]
        private List<FuelEconomyRating> _ratingsList = new List<FuelEconomyRating>();

        public List<FuelEconomyRating> FuelEconomyRatings
        {
            get { return _ratingsList; } 
            set { _ratingsList = value; }
        }

        public string CrashTestRatings { get; set; }
        public string BodyStyle { get; set; }
        public List<CircleRatings> CircleRatings { get; set; }
    }

    public class CircleRatings
    {
        public int SourceID;
        public float CircleRating;
        public string Description;
    }

    public class FuelEconomyRating
    {
        public string Rating { get; set; }
        public string Value { get; set; }
    }


    public static class ReferenceDataExtensions
    {
        public static string ToXmlString(this ReferenceData vinData)
        {
            var serializer = new XmlSerializer(typeof(ReferenceData));

            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                serializer.Serialize(xmlTextWriter, vinData);
                xmlTextWriter.Flush();
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}