﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects
{
    [Serializable]
    public class Option
    {
        public string OptionText;
        public string DetailText;
        public string OptionCode;
        public bool Standard;
        public decimal Msrp;

        [XmlIgnore]
        public string Condition;
    }

    public static class OptionsToXml
    {
        public static string ToXmlString(IList<Option> options)
        {
            var serializer = new XmlSerializer(typeof(List<Option>), new XmlRootAttribute("OptionList"));
            
            using(var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                serializer.Serialize(xmlTextWriter, options);
                xmlTextWriter.Flush();
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}