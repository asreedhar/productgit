using Core.Messaging;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing
{
    /// <summary>
    /// Information about a publication.
    /// </summary>
    public sealed class PublicationInfo
    {
        // TODO: Change to max-docs-changes-public. Trying to make all resource names consistent.
        private const string DefaultNotificationTopic = @"max-document-changes";  

        public PublicationInfo()
        {
            NotificationTopic = DefaultNotificationTopic;
        }

        public string Content { get; set; }

        public string BucketName()
        {
            return FileStore.ContainerName();
        }

        public IFileStorage FileStore { get; set; }
        public string Key { get; set; }

        public string NotificationTopic { get; set; }
        public string Sender { get; set; }

        public string MessageType { get; set; }
        public string Vin { get; set; }

        public string ContentType { get; set; }
    }
}