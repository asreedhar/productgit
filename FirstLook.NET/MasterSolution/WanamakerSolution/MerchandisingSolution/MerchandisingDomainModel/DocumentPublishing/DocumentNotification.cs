using System;
using System.Globalization;
using System.Reflection;
using AmazonServices.SNS;
using FirstLook.Common.Core.Extensions;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing
{
    public class DocumentNotification
    {
        #region Logging
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        #endregion Logging

        /// <summary>
        /// Broadcast a document notification message to the correct SNS topic, returning the message id.
        /// </summary>
        /// <param name="documentInfo"></param>
        /// <param name="appendEnvironmentNameToTopicName"></param>
        /// <param name="useGlobalNamingRules"></param>
        /// <param name="topicNameDelimiter"></param>
        internal static string Broadcast(DocumentInfo documentInfo, 
                                       bool appendEnvironmentNameToTopicName = false, 
                                       bool useGlobalNamingRules = false,
                                       char topicNameDelimiter = '_')
        {
            var bucketName = documentInfo.FileStore.ContainerName();
            var key = documentInfo.Key;

            var topicMessage = new
            {
                bucket = bucketName,
                key,
                url = String.Format("https://s3.amazonaws.com/{0}/{1}", bucketName, key),
                generated_at = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                sender = documentInfo.Sender,
                doc_type = documentInfo.DocType
            };

            var messageJson = topicMessage.ToJson();

            var topicArn = GenericTopic.CreateTopic(documentInfo.NotificationTopic, appendEnvironmentNameToTopicName, 
                                                    useGlobalNamingRules, topicNameDelimiter);
            var messageId = GenericTopic.Publish(messageJson, topicArn);
            Log.DebugFormat("MAX document update message with id {0} published to SNS.", messageId);

            return messageId;
        }
    }
}