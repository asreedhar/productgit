﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;
using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories
{
    internal class UsedInventoryRepository
    {
        /// <summary>
        /// Return all used active inventory at the specified business unit.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <returns></returns>
        internal IEnumerable<InventoryData> GetUsedActiveInventory(int businessUnitId)
        {
            return InventoryDataSource.FetchList(businessUnitId, (int) VehicleTypeEnum.Used);
        }

        /// <summary>
        /// Return any used active inventory at the specified business unit and where the inventoryId
        /// is in the supplied list.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="inventoryIds"></param>
        /// <returns></returns>
        internal IEnumerable<InventoryData> GetUsedActiveInventory(int businessUnitId, IEnumerable<int> inventoryIds)
        {
            var allUsed = GetUsedActiveInventory(businessUnitId);
            return allUsed.Where(inv => inventoryIds.Contains(inv.InventoryID));
        }

        internal IEnumerable<InventorySummary> GetUsedInventorySummary(int businessUnitId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                const string QUERY = @"

                    SELECT I.InventoryID, CONVERT(BIT, I.InventoryActive) AS InventoryActive, I.InventoryReceivedDate, I.DeleteDt, V.Vin  
                    FROM IMT.dbo.Inventory I WITH(NOLOCK)
                    JOIN IMT.dbo.tbl_Vehicle V WITH(NOLOCK)
                        ON i.VehicleID = V.VehicleID
                    WHERE I.BusinessUnitID = @BusinessUnitId AND I.InventoryType = 2";

                var inventorySummaries = sqlConnection.Query<InventorySummary>(QUERY, new {BusinessUnitId = businessUnitId});
                sqlConnection.Close();
                return inventorySummaries;
            }
        }
    }

    public class InventorySummary
    {
        public int InventoryId { get; set; }
        public bool InventoryActive { get; set; }
        public DateTime InventoryReceivedDate { get; set; }
        public DateTime? DeleteDt { get; set; }
        public string Vin { get; set; }
    }
}
