﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories
{
    public interface IMaxDealersRepository
    {
        IEnumerable<int> FindAllActiveMaxDealers();
        IEnumerable<int> FindActiveMaxDealerIdsAboveVersion(int version);
    }

    public class MaxDealersRepository : IMaxDealersRepository
    {
        public IEnumerable<int> FindAllActiveMaxDealers()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IMT"].ConnectionString))
            {
                sqlConnection.Open();

                const string QUERY = @"
                        SELECT DISTINCT BU.BusinessUnitID
                        FROM IMT.dbo.BusinessUnit BU
                        JOIN IMT.dbo.DealerUpgrade DU
	                        ON DU.BusinessUnitID = BU.BusinessUnitID
                        WHERE 
	                        BU.Active = 1 AND
	                        DU.DealerUpgradeCD IN (23,24,25,26) AND -- dealer has at least one MAX product upgrades
	                        DU.Active = 1 AND
	                        (DU.StartDate IS NULL OR GETDATE() > DU.StartDate) AND
	                        (DU.EndDate IS NULL OR GETDATE() < DU.EndDate)";

                var businessUnitIds = sqlConnection.Query<int>(QUERY);
                sqlConnection.Close();
                return businessUnitIds;
            }
    
        }

        /// <summary>
        /// Get a list of BusinessUnitIds for active Max dealers that have a version greater than or equal to the passed parameter
        /// </summary>
        /// <param name="version">the Max version</param>
        /// <returns>a list of Business Unit Ids</returns>
        public IEnumerable<int> FindActiveMaxDealerIdsAboveVersion(int version)
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                connection.Open();

                var query = String.Format("SELECT BusinessUnitId FROM settings.Merchandising WHERE MaxVersion >= {0}", version);
                var maxDealerIdsAboveVersion = connection.Query<int>(query);
                connection.Close();

                var activeMaxDealerIds = FindAllActiveMaxDealers();
                return maxDealerIdsAboveVersion.Where(activeMaxDealerIds.Contains);
            }
        }
    }
}
