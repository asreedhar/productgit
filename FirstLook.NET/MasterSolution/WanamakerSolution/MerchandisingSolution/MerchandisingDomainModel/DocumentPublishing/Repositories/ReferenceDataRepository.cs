﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MvcMiniProfiler.Helpers.Dapper;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories
{
    public interface IReferenceDataRepository
    {
        ReferenceData GetPublishableReferenceData(string vin);
        List<FuelEconomyRating> GetFuelEcomonayRating(int businessUnit, int inventory);
    }

    internal class ReferenceDataRepository : IReferenceDataRepository
    {
        public ReferenceData GetPublishableReferenceData(string vin)
        {
            using (var sqlConnection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                sqlConnection.Open();
                const string statement = "builder.VinReferenceData#Fetch";

                var vinDataMultiple = sqlConnection.QueryMultiple(statement, new {Vin = vin},
                    commandType: CommandType.StoredProcedure);

                var vinDatas = vinDataMultiple.Read<RefData>().ToArray();
                var ratingData = vinDataMultiple.Read<CircleRatings>().ToList();

                var data = new ReferenceData();

                if (vinDatas.Length == 0)
                    return data;

                data.CircleRatings = ratingData;
                data.BodyStyle = vinDatas.First().BodyStyle;
                data.CrashTestRatings = vinDatas.First().CrashRating;
                /* FB: 29579
                data.FuelEconomyRatings.AddRange(
                    vinDatas.Select(x => new FuelEconomyRating { Rating = x.MpgRating, Value = x.MpgValue })
                );
                 */

                return data;
            }
        }

        public List<FuelEconomyRating> GetFuelEcomonayRating(int businessUnit, int inventory)
        {
            List<FuelEconomyRating> fuelEconomyRating = new List<FuelEconomyRating>();

            var vehicleConfig = VehicleConfiguration.FetchOrCreateDetailed(businessUnit, inventory, String.Empty);

            var consumerInfo = ConsumerInfo.Fetch(vehicleConfig.ChromeStyleID, vehicleConfig.VehicleOptions.GetCategoryIdList(), vehicleConfig.VehicleOptions.options);            

            if(consumerInfo.FuelCity != string.Empty)
                fuelEconomyRating.Add(new FuelEconomyRating { Rating = "EPA Fuel Economy Est - City (MPG)", Value = consumerInfo.FuelCityInt.ToString() });

            if (consumerInfo.FuelHwy != string.Empty)
                fuelEconomyRating.Add(new FuelEconomyRating { Rating = "EPA Fuel Economy Est - Hwy (MPG)", Value = consumerInfo.FuelHwyInt.ToString() });

            return fuelEconomyRating;

        }

        
        public class RefData
        {
            public string BodyStyle;
            public string CrashRating;
            public string MpgRating;
            public string MpgValue;
        }
    }
}