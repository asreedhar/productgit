using System;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.Pdf;
using log4net;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories
{
	public interface IMarketingDocumentRepository
	{
		string GetMarketingXml(int businessUnitId, int inventoryId);
	    string GetMarketingHtml(int businessUnitId, int inventoryId);
	}

	public class MarketingDocumentRepository : IMarketingDocumentRepository
	{
		private readonly IMarketListingSearchAdapter _searchAdapter;
		private readonly ILogger _logger;

		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging


		public MarketingDocumentRepository(IMarketListingSearchAdapter searchAdapter, ILogger logger)
		{
			_searchAdapter = searchAdapter;
			_logger = logger;
		}

		public string GetMarketingXml(int businessUnitId, int inventoryId)
		{
		    var pdfManager = GetPdfManager(businessUnitId, inventoryId);

		    if (pdfManager == null)
		        return string.Empty;

		    try
			{
				var documentXml = pdfManager.GetXml();
				return documentXml;
			}
			catch (Exception ex)
			{
				Log.Error("Error loading document xml", ex);
				return string.Empty;
			}
		}

        public string GetMarketingHtml(int businessUnitId, int inventoryId)
        {
            var pdfManager = GetPdfManager(businessUnitId, inventoryId);

            if (pdfManager == null)
                return string.Empty;

            try
            {
                var marketingHtml = pdfManager.ToHtml();
                return marketingHtml;
            }
            catch (Exception ex)
            {
                Log.Error("Error loading document xml", ex);
                return string.Empty;
            }
        }

	    internal PdfManager GetPdfManager(int businessUnitId, int inventoryId)
	    {
	        PricingContext pricingContext;
	        try
	        {
	            pricingContext = PricingContext.FromMerchandising(businessUnitId, inventoryId);
	        }
	        catch (SqlException sqx)
	        {
	            if (sqx.Errors.Count > 0)
	            {
	                var errorStringBuilder = new StringBuilder();
	                foreach (SqlError error in sqx.Errors)
	                {
	                    errorStringBuilder.AppendFormat("{0}\n", error.Message);
	                }
	                Log.ErrorFormat("{0}", errorStringBuilder);
	            }
	            return null;
	        }

	        Log.DebugFormat("pricing context gets ownerhandle: {0} vehiclehandle: {1}", pricingContext.OwnerHandle, pricingContext.VehicleHandle);

	        var businessUnit = BusinessUnitFinder.Instance().Find(businessUnitId);
	        //var publisher = new WebSitePdfPublisher(PricingContext.FromMerchandising(businessUnitId, inventoryId), _logger,
	        //    _searchAdapter,
	        //    businessUnit.HasDealerUpgrade(Upgrade.WebSitePDFMarketListings),
	        //    GetType().Name);

	        var deal = new Deal(_logger, _searchAdapter, pricingContext.OwnerHandle, pricingContext.VehicleHandle, pricingContext.SearchHandle)
	        {
	            PacketType = Packets.ValueAnalyzer,
	            ValueAnalyzerPacketOptions = {DisplayMarketListingsPermission = businessUnit.HasDealerUpgrade(Upgrade.WebSitePDFMarketListings)}
	        };

	        var pdfManager = PdfManager.FromDeal(deal, _logger);
	        return pdfManager;
	    }
	}
}