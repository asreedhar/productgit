﻿using System.Data;
using System.Linq;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects;
using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories
{
    public interface IEquipmentRepository
    {
        EquipmentList GetPublishableVinEquipment(int businessUnitId, int inventoryId, string vin);
    }

    public class EquipmentRepository : IEquipmentRepository
    {
        public EquipmentList GetPublishableVinEquipment(int businessUnitId, int inventoryId, string vin)
        {
            using (var sqlConnection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                sqlConnection.Open();
                const string statement = "builder.PublishableEquipment#Fetch";

                var vinDatas = sqlConnection.Query<PublishableVinData>(statement, new {BusinessUnitId = businessUnitId, InventoryId = inventoryId, Vin = vin}, commandType: CommandType.StoredProcedure).ToArray();

                var equipmentList = new EquipmentList();

                if (vinDatas.Length != 0)
                    equipmentList.Equipment.AddRange(
                        vinDatas.Select(x => new Item {Category = x.Category, Description = x.Equipment, IsStandard = x.Standard, Tier = x.Tier}).ToList()
                    );

                return equipmentList;
            }
        }

        // ReSharper disable ClassNeverInstantiated.Local
        /// <summary>
        /// This is just a POCO to use with Dapper. 
        /// Only used in this implementation, so I'm not moving it to its own file.
        /// </summary>
        class PublishableVinData
        {
            // ReSharper disable UnassignedField.Compiler
            public string Equipment;
            public string Category;
            public int Tier;
            public bool Standard;
            // ReSharper restore UnassignedField.Compiler
        }
        // ReSharper restore ClassNeverInstantiated.Local
    }

    
}
