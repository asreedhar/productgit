﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories
{
    public interface IOptionsRepository
    {
        IList<Option> GetPublishableVinOptions(int businessUnitId, int inventoryId, string vin);
    }

    public class OptionsRepository : IOptionsRepository
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IList<Option> GetPublishableVinOptions(int businessUnitId, int inventoryId, string vin)
        {
            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();

                // TODO: Refactor this to a sproc.
                const string query = @"
                        SELECT VDO.optionText, VDO.detailText, VDO.optionCode, 
                            VDO.isStandardEquipment as Standard, CONVERT(DECIMAL(10,2), p.Msrp) AS Msrp, p.Condition
                        FROM Merchandising.builder.VehicleDetailedOptions VDO
                        JOIN FLDW.dbo.InventoryActive IA
	                        ON VDO.businessUnitID = IA.BusinessUnitID
	                        AND VDO.inventoryID = IA.InventoryID
                        JOIN FLDW.dbo.Vehicle V
	                        ON IA.VehicleID = V.VehicleId AND IA.BusinessUnitID = V.BusinessUnitID
                        JOIN Merchandising.builder.OptionsConfiguration oc 
	                        ON oc.businessUnitID = VDO.businessUnitID AND oc.inventoryId = IA.InventoryID
                        JOIN VehicleCatalog.Chrome.Prices p
	                        ON p.StyleId = oc.chromeStyleID AND p.CountryCode = 1 AND p.OptionCode = VDO.optionCode

                        WHERE VDO.businessUnitID = @businessUnitId AND VDO.inventoryId = @inventoryId AND V.Vin = @vin
                        ORDER BY p.MSRP desc
                    ";

                var options = conn.Query<Option>(query, new { businessUnitId, inventoryId, vin }).ToList();
                
                var filteredOptions =
                    ChromeLogicEngine.GetBestOptionsByMsrp(options.Select(x => new ChromeMsrpCondition {Condition = x.Condition, Msrp = x.Msrp, OptionCode = x.OptionCode}));

                Log.DebugFormat("filtered options: {0}", filteredOptions.ToJson());

                var finalOptions = filteredOptions.Select(bestOption => options.First(opt => opt.OptionCode == bestOption.OptionCode &&
                                                                                             opt.Condition == bestOption.Condition &&
                                                                                             opt.Msrp == bestOption.Msrp))
                                                                                             
                    .ToList();

                return finalOptions;
            }
        }
    }
}
