﻿using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.VehicleHistoryReport.WebService.Client.Carfax;

namespace FirstLook.Merchandising.DomainModel.VehicleHistory
{
    internal class CarfaxServices : ICarfaxServices
    {
        private readonly IUserServices _userServices;

        public CarfaxServices(IUserServices userServices)
        {
            _userServices = userServices;
        }
        
        public bool HasAccount(int businessUnitId)
        {
            var username = GetCurrentUser();
            if (username == null)
                return false;

            var service = new CarfaxWebService {UserIdentityValue = new UserIdentity {UserName = username}};

            return service.HasAccount(businessUnitId);
        }

        private string GetCurrentUser()
        {
            var user = _userServices.Current;
            return user == null ? null : user.UserName;
        }
    }
}