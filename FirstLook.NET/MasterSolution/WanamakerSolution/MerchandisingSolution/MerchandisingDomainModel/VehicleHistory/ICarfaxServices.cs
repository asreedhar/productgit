﻿namespace FirstLook.Merchandising.DomainModel.VehicleHistory
{
    public interface ICarfaxServices
    {
        bool HasAccount(int businessUnitId);
    }
}