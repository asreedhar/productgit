﻿using System.Globalization;
using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.VehicleHistory
{
    internal class CachedCarfaxServices : ICarfaxServices
    {
        private readonly ICache _cache;
        private readonly ICarfaxServices _inner;

        private static readonly string CacheKeyPrefix = typeof (CachedCarfaxServices).FullName + "_";
        private const int TimeToLive = 300; // Five minutes

        public CachedCarfaxServices(ICache cache, ICarfaxServices inner)
        {
            _cache = cache;
            _inner = inner;
        }

        public bool HasAccount(int businessUnitId)
        {
            string cacheKey = CacheKeyPrefix + businessUnitId.ToString(CultureInfo.InvariantCulture);
            var hasAccount = (bool?)_cache.Get(cacheKey);
            if(!hasAccount.HasValue)
            {
                hasAccount = _inner.HasAccount(businessUnitId);
                _cache.Set(cacheKey, hasAccount, TimeToLive);
            }

            return hasAccount.Value;
        }
    }
}