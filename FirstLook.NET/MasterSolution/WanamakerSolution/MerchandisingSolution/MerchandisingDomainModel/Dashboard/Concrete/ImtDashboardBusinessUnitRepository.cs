﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Concrete
{
    internal class ImtDashboardBusinessUnitRepository : IDashboardBusinessUnitRepository
    {
        public IDashboardBusinessUnit GetBusinessUnit(int businessUnitId)
        {
            var businessUnit = BusinessUnitFinder.Instance().Find(businessUnitId);
            return new DashboardBusinessUnit() { BusinessUnitId = businessUnit.Id, Code = businessUnit.BusinessUnitCode, Name = businessUnit.Name};
        }

        public IDashboardBusinessUnit[] GetBusinessUnitsfromGroup(int groupId)
        {
            BusinessUnitFinder finder = BusinessUnitFinder.Instance();
            var units = finder.FindAllDealershipsByDealerGroup(groupId);
            return units.Select(x => new DashboardBusinessUnit() {BusinessUnitId = x.Id, Code = x.BusinessUnitCode, Name = x.Name}).
                    ToArray();
        }

        public IDashboardBusinessUnit[] FindAllDealershipsByDealerGroupAndMember(int groupId, string login)
        {
            BusinessUnitFinder finder = BusinessUnitFinder.Instance();
            var units = finder.FindAllDealershipsByDealerGroupAndMember(groupId, login);
            return units.Select(x => new DashboardBusinessUnit() { BusinessUnitId = x.Id, Code = x.BusinessUnitCode, Name = x.Name}).
                    ToArray();
        }

        public bool HasDealerUpgrade(int businessUnitId, int upgrade)
        {
            var businessUnit = BusinessUnitFinder.Instance().Find(businessUnitId);
            return businessUnit.HasDealerUpgrade((Upgrade) upgrade);
        }

        public IDashboardBusinessUnit GetGroup(int businessUnitId)
        {
            var businessUnit = BusinessUnitFinder.Instance().Find(businessUnitId);
            var dealerGroup = businessUnit.DealerGroup();
            return new DashboardBusinessUnit(){ BusinessUnitId = dealerGroup.Id, Code = dealerGroup.BusinessUnitCode, Name = dealerGroup.Name };
        }

    }
}
