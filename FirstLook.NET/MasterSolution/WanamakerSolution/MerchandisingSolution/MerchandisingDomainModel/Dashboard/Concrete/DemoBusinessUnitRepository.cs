﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.DomainModel.Oltp;
using System.Data.SqlClient;
using System.Configuration;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Concrete
{
    public class DemoBusinessUnitRepository
    {
        private const int DEMO_BU_PADDING = 10000000;

        
        private const string DEMO_BU_DEACTIVATE = "demo.BusinessUnit#Deactivate";
        private const string DEMO_BU_DELETE = "demo.BusinessUnit#Delete";
        private const string DEMO_BU_FETCH = "demo.BusinessUnit#Fetch";
        private const string DEMO_BU_FETCH_FROM_USER = "demo.BusinessUnit#FetchFromUsername";
        private const string DEMO_BU_FETCHGROUP_AND_MEMBERS = "demo.BusinessUnit#FetchGroupAndMembersFromBusinessUnit";
        private const string DEMO_BU_FETCHGROUP_FROM_BU = "demo.BusinessUnit#FetchGroupForBusinessUnit";
        private const string DEMO_BU_FETCHGROUP_MEMBERS = "demo.BusinessUnit#FetchGroupMembers";
        private const string DEMO_BU_INSERT = "demo.BusinessUnit#Insert";
        private const string DEMO_BU_GROUP_DELETE = "demo.BusinessUnitRelationship#Delete";
        private const string DEMO_BU_GROUP_INSERT = "demo.BusinessUnitRelationship#Insert";
        

        /// <summary>
        /// The stored procedure will set the business unit id and the business unit code. So this
        /// will return the new DashboardBusinessUnit.
        /// </summary>
        /// <param name="DashboardBusinessUnitName"></param>
        /// <param name="DashboardBusinessUnitType"></param>
        /// <returns></returns>
        public DashboardBusinessUnit Create(string DashboardBusinessUnitName, int DashboardBusinessUnitType = 4)
        {
            DashboardBusinessUnit DashboardBusinessUnit = null;
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_INSERT, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitName", DashboardBusinessUnitName);
                    cmd.Parameters.AddWithValue("@BusinessUnitTypeId", DashboardBusinessUnitType);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DashboardBusinessUnit = DemoDashboardBusinessUnitFromReader(reader);
                        }
                    }
                }
            }
            return DashboardBusinessUnit;
        }

        /// <summary>
        /// Get the DashboardBusinessUnit given a Business Unit Id
        /// </summary>
        /// <param name="DashboardBusinessUnitId"></param>
        /// <returns></returns>
        public DashboardBusinessUnit Fetch(int DashboardBusinessUnitId)
        {
            DashboardBusinessUnit fetchUnit = null;

            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_FETCH, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", DashboardBusinessUnitId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                                fetchUnit = DemoDashboardBusinessUnitFromReader(reader);
                        }
                    }
                }
            }

            return fetchUnit;
        }

        /// <summary>
        /// Get the Demo Business Unit from the login user name and the application.
        /// See Membership.Application for the application name
        /// </summary>
        /// <param name="username">Try HttpContext.Current.User.Identity.Name or MembershipUser user = System.Web.Security.Membership.GetUser("userName").UserName</param>
        /// <param name="applicationName">Membership.Application</param>
        /// <returns></returns>
        public DashboardBusinessUnit FetchFromUsername(string username, string applicationName)
        {
            DashboardBusinessUnit fetchUnit = null;
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_FETCH, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserName", username);
                    cmd.Parameters.AddWithValue("@ApplicationName", applicationName);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                                fetchUnit = DemoDashboardBusinessUnitFromReader(reader);
                        }
                    }
                }
            }

            return fetchUnit;
        }

        /// <summary>
        /// Get just the members of a group from the group id. Do not include the Group DashboardBusinessUnit
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<DashboardBusinessUnit> FetchGroupMembersByGroupId(int groupId)
        {
            List<DashboardBusinessUnit> groupUnit = new List<DashboardBusinessUnit>();
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_FETCHGROUP_MEMBERS, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupId", groupId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            groupUnit.Add(DemoDashboardBusinessUnitFromReader(reader));
                        }
                    }
                }
            }

            return groupUnit;
        }

        /// <summary>
        /// Get the Group DashboardBusinessUnit for a given DashboardBusinessUnit
        /// </summary>
        /// <param name="DashboardBusinessUnitId"></param>
        /// <returns></returns>
        public DashboardBusinessUnit FetchGroupByDealerId(int DashboardBusinessUnitId)
        {
            DashboardBusinessUnit groupUnit = null;
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_FETCHGROUP_FROM_BU, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", DashboardBusinessUnitId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            groupUnit = DemoDashboardBusinessUnitFromReader(reader);
                        }
                    }
                }
            }

            return groupUnit;
        }

        /// <summary>
        /// Get a collection of DashboardBusinessUnits that includes both the Group type and all Dealer type DashboardBusinessUnits
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <returns></returns>
        public IEnumerable<DashboardBusinessUnit> FetchGroupAndMembers(int businessUnitId)
        {
            List<DashboardBusinessUnit> groupUnit = new List<DashboardBusinessUnit>();
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_FETCHGROUP_AND_MEMBERS, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", businessUnitId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            groupUnit.Add(DemoDashboardBusinessUnitFromReader(reader));
                        }
                    }
                }
            }

            return groupUnit;
        }


        public void Deactivate(int DashboardBusinessUnitId)
        {
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_DEACTIVATE, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BusinessUnitId", DashboardBusinessUnitId);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int DashboardBusinessUnitId)
        {
            using (var delConn = GetConnection())
            {
                using (SqlCommand delcmd = new SqlCommand(DEMO_BU_DELETE, delConn))
                {
                    delcmd.CommandType = System.Data.CommandType.StoredProcedure;
                    delcmd.Parameters.AddWithValue("@BusinessUnitId", DashboardBusinessUnitId);
                    delcmd.ExecuteNonQuery();
                }
            }
        }
 
        public void AddDashboardBusinessUnitToGroup(int DashboardBusinessUnitId, int groupId)
        {
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_GROUP_INSERT, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupId", groupId);
                    cmd.Parameters.AddWithValue("@BusinessUnitId", DashboardBusinessUnitId);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RemoveDashboardBusinessUnitFromGroup(int DashboardBusinessUnitId, int groupId)
        {
            using (var conn = GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(DEMO_BU_GROUP_DELETE, conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupId", groupId);
                    cmd.Parameters.AddWithValue("@BusinessUnitId", DashboardBusinessUnitId);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static bool IsDemoUnit(int unit)
        {
            return unit >= DEMO_BU_PADDING;
        }

        private static SqlConnection GetConnection()
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ToString());
            conn.Open();
            return conn;
        }

        private static DashboardBusinessUnit DemoDashboardBusinessUnitFromReader(SqlDataReader reader)
        {
            return DemoDashboardBusinessUnitFromReader((System.Data.IDataReader)reader);
        }

        internal static DashboardBusinessUnit DemoDashboardBusinessUnitFromReader(System.Data.IDataReader reader)
        {
            DashboardBusinessUnit demoBu = new DashboardBusinessUnit();
            demoBu.BusinessUnitId = reader.IsDBNull(reader.GetOrdinal("BusinessUnitID")) ? 0 : reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
            demoBu.Name = reader.IsDBNull(reader.GetOrdinal("BusinessUnit")) ? "" : reader.GetString(reader.GetOrdinal("BusinessUnit"));
            demoBu.Type = reader.IsDBNull(reader.GetOrdinal("BusinessUnitTypeID")) ? 3 : reader.GetInt32(reader.GetOrdinal("BusinessUnitTypeID"));
            demoBu.Code = reader.IsDBNull(reader.GetOrdinal("BusinessUnitCode")) ? "" : reader.GetString(reader.GetOrdinal("BusinessUnitCode"));

            return demoBu;
        }
    }
}
