﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using Merchandising.Messages;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Common.Core;
using Bucket = FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Concrete
{
    internal class DashboardRepository : IDashboardRepository
    {
        private IWorkflowRepository WorkflowRepository { get; set; }

        private IGoogleAnalyticsResultCache _analyticsCache;
        private InventoryOnlineStateData _onlineStateData;
        private IDashboardBusinessUnitRepository _businessUnitRepository;
        private INoSqlDbFactory _noSqlDbFactory;

        public DashboardRepository(IWorkflowRepository workflowRepository, IGoogleAnalyticsResultCache analyticsCache, IMaxAnalytics maxAnalytics, IDashboardBusinessUnitRepository businessUnitRepository, INoSqlDbFactory noSqlDbFactory)
        {
            WorkflowRepository = workflowRepository;
            _analyticsCache = analyticsCache;
            _onlineStateData = new InventoryOnlineStateData(maxAnalytics);
            _businessUnitRepository = businessUnitRepository;
            _noSqlDbFactory = noSqlDbFactory;
        }

        public IDictionary<VehicleType, int> GetActiveInventoryCounts(int businessUnitId)
        {
            var activeInventoryCounts = new Dictionary<VehicleType, int>();

            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.DashboardData#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);


                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var inventoryType = reader.GetInt32(reader.GetOrdinal("InventoryType"));
                            var inventoryCount = reader.GetInt32(reader.GetOrdinal("InventoryCount"));
                            activeInventoryCounts.Add(inventoryType, inventoryCount);
                        }

                    }
                }
            }

            return activeInventoryCounts;

        }

        public Inventory GetInventoryFromStockNumber(int businessUnitId, string stockNumber)
        {
            Inventory data = null;
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.GetInventoryFromStockNumber";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "StockNumber", stockNumber, DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if(data == null) data = new Inventory();
                            data.StockNumber = stockNumber;
                            data.InventoryId = reader.GetInt32(reader.GetOrdinal("InventoryId"));
                            data.InventoryType = int.Parse(reader["InventoryType"].ToString());
                        }

                    }
                }
            }

            return data;

        }

        public bool IsDashboardEnabled(int businessUnitId)
        {
            var settings = GetMiscSettings.GetSettings( businessUnitId );
            return settings.HasSettings && settings.ShowDashboard;
        }

        public byte GetMaxVersion (int businessUnitId)
        {
            var settings = GetMiscSettings.GetSettings( businessUnitId );
            return settings.MaxVersion;
        }

        public bool HasDealerUpgrade (int businessUnitId, Upgrade upgrade)
        {
            return _businessUnitRepository.HasDealerUpgrade(businessUnitId, (int)upgrade);
        }

        public IEnumerable<Bucket> GetBucketCounts(int businessUnitId, VehicleType usedOrNewFilter)
        {
            var inventory = InventoryDataFilter.FetchListFromSession( businessUnitId ).FilterBy( usedOrNewFilter ).ToList();
            var minAgeInDays = GetPreferenceValue( businessUnitId );
            _onlineStateData.CrossFill( businessUnitId, usedOrNewFilter, minAgeInDays, inventory );

            var counts = WorkflowRepository.CalculateCounts( inventory );

            return WorkflowRepository.Workflows.Select(
                workflow => new Bucket {
                        BucketId = workflow.Type,
                        BucketCount = counts.GetCount(workflow.Type),
                        BucketDescription = workflow.Label,
                        NavigationUrl = workflow.GetNavigationUrl()
                    }
                ).ToList();
        }

        public int GetBucketTotal( int businessUnitId, VehicleType usedOrNewFilter, ISet<WorkflowType> bucketIds )
        {
            var inventory = InventoryDataFilter.FetchListFromSession( businessUnitId ).FilterBy( usedOrNewFilter ).ToList();
            var minAgeInDays = GetPreferenceValue( businessUnitId );
            _onlineStateData.CrossFill( businessUnitId, usedOrNewFilter, minAgeInDays, inventory );

            return WorkflowRepository.CalculateTotal( inventory, bucketIds );
        }

        public AutoApproveStatus GetAutoApproveStatus(int businessUnitId)
        {
            return GetAutoApproveSettings.GetSettings(businessUnitId).Status;
        }

        private static int GetPreferenceValue(int businessUnitId)
        {
            var settings = new GetReportSettings(businessUnitId);
            AbstractCommand.DoRun(settings);
            return settings.VehicleOnlineStatusReport_MinAgeDays;
        }

        public OnlineStatus GetNotOnlineCount( int businessUnitId, VehicleType usedOrNewFilter )
        {
            OnlineStatus onlineStatus;
          
            var inventory = InventoryDataFilter.FetchListFromSession(businessUnitId).FilterBy(usedOrNewFilter).ToList();
            var minAgeInDays = GetPreferenceValue(businessUnitId);
            int onlineSet =_onlineStateData.CrossFill(businessUnitId, usedOrNewFilter, minAgeInDays, inventory);

            onlineStatus = new OnlineStatus
            {
                TotalInventoryCount = inventory.Count,
                TotalNotOfflineReportCount = onlineSet,
                NoDataCount = inventory.Count(inv => inv.OnlineState == 0),  // 0 : No Data
                OnlineCount = inventory.Count(inv => inv.OnlineState == 1),  // 1 : Online
                NotOnlineCount = inventory.Count(inv => inv.OnlineState == 2)// 2 : Not Online
            };
            
            return onlineStatus;
        }

        public BucketAlertThreshold[] GetBucketAlertThresholds(int businessUnitId)
        {
            var settingsCommand = new GetBucketAlerts(businessUnitId);
            AbstractCommand.DoRun(settingsCommand);
            return settingsCommand.Thresholds;
        }

        public string ResolveDBEnum( string table, string key_column, string value_column, string key )
        {
            using( IDataConnection con = Database.GetConnection( Database.MerchandisingDatabase ))
            {
                con.Open();
                using( IDbCommand cmd = con.CreateCommand() )
                {
                    var builder = new SqlCommandBuilder();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = string.Format( "SELECT TOP 1 {0} FROM {1} WHERE {2} = @key_value",
                        value_column, table, key_column );
                    cmd.AddRequiredParameter( "key_value", key, DbType.String );
                    using( IDataReader reader = cmd.ExecuteReader() )
                    {
                        if( reader.Read() )
                        {
                            if( ! reader.IsDBNull(0) )
                                return reader[0].ToString();
                            else
                                return null;
                        }
                    }
                }
            }
            throw new KeyNotFoundException( "table="+table+", key_column="+key_column+", key=\""+key+"\"" );
        }

        public Dictionary<string, DealerSiteInformation> GetStaticDealerSiteData(IDashboardBusinessUnit unit)
        {
            var siteList = new Dictionary<int, DealerSiteInformation>();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                foreach( EdtDestinations site_id in Enum.GetValues(typeof(EdtDestinations )) )
                {
                    if( site_id == EdtDestinations.Undefined ) // EdtDestinations.Undefined
                        continue;
                    
                    siteList[(int)site_id] = new DealerSiteInformation{ CredentialStatus = DealerSiteCredentialStatus.NotEntered };

                    using( var cmd = con.CreateCommand() )
                    {
                        cmd.CommandText = "merchandising.DealerSiteCredentials#Fetch";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.AddRequiredParameter( "BusinessUnitId", unit.BusinessUnitId, DbType.Int32 );
                        cmd.AddRequiredParameter( "siteId", site_id, DbType.Int32 );
                        cmd.AddRequiredParameter( "allowLockedOutCredentials", true, DbType.Boolean );

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if( reader.GetBoolean( reader.GetOrdinal( "isLockedOut" )))
                                    siteList[(int)site_id].CredentialStatus = DealerSiteCredentialStatus.Invalid;
                                else if( ! reader.IsDBNull( reader.GetOrdinal( "username" ))
                                &&       ! reader.IsDBNull( reader.GetOrdinal( "encryptedPassword" )))
                                    siteList[(int)site_id].CredentialStatus = DealerSiteCredentialStatus.Valid;
                            }
                        }
                    }
                }
            }

            var siteListNames = new Dictionary<string, DealerSiteInformation>();
            foreach( var site_id in siteList.Keys )
            {
                var site_name = ResolveDBEnum( "settings.EdtDestinations", "destinationID", "description", site_id.ToString() );
                siteListNames.Add( site_name, siteList[site_id] );
            }
            return siteListNames;
        }

        public GoogleAnalyticsResult GetAllGoogleAnalyticsData(int businessUnitId, GoogleAnalyticsType gaProfileType, DateRange dateRange)
        {
            GoogleAnalyticsResult GoogleData = _analyticsCache.FetchRangedGoogleData(businessUnitId, gaProfileType, dateRange);

            return GoogleData;
        }
        public List<GoogleAnalyticsResult> GetAllGoogleAnalyticsDataUngrouped(int businessUnitId, GoogleAnalyticsType gaProfileType, DateRange dateRange, string[] queryNameFilter)
        {
            List<GoogleAnalyticsResult> GoogleData = _analyticsCache.FetchRangedGoogleDataUngrouped(businessUnitId, gaProfileType, dateRange);
            foreach( var result in GoogleData )
            {
                Dictionary<string, ReferralResult> filtered = new Dictionary<string, ReferralResult>();
                foreach (var key in result.Referrals.Keys)
                {
                    if( queryNameFilter.Contains( key ))
                        filtered.Add( key, result.Referrals[key] );
                }
                result.Referrals = filtered;
            }

            return GoogleData;
        }

        public DashboardSettingsModel GetDashboardSettings(int businessUnitId)
        {
            var model = new DashboardSettingsModel();
            SetMerchandisingAlertGraphSettings(businessUnitId, ref model);

            return model;
        }

        private void SetMerchandisingAlertGraphSettings(int businessUnitId, ref DashboardSettingsModel model)
        {
            var cmd = new GetReportSettings(businessUnitId);
            AbstractCommand.DoRun(cmd);

            model.MerchandisingAlertSelectionBucket = cmd.MerchandisingGraphDefault.ToString();
        }

        public IList<BucketValue> FetchBucketValuesReport(int businessUnitId, DateRange dateRange)
        {
            var bucketValues = new List<BucketValue>();

            var bucketList = WorkflowRepository.Workflows.Select(workflow => new BucketValue
            {
                BusinessUnitId = businessUnitId,
                Bucket = workflow.Type
            });

            foreach (var bucket in bucketList)
            {
                var queryKeys = new Dictionary<String, Object>
                {
                    {"BusinessUnitId_Bucket", bucket.BusinessUnitId_Bucket},
                    {"Date", dateRange}
                };
                var dbInterface = _noSqlDbFactory.GetBucketValueDb();

                bucketValues.AddRange(dbInterface.Query<BucketValue>(queryKeys));
            }

            return bucketValues;
        }
    }
}
