﻿
using System;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Concrete
{
    internal class Clock : IClock
    {
        public DateTime CurrentTime
        {
            get { return DateTime.Now; }
        }
    }
}
