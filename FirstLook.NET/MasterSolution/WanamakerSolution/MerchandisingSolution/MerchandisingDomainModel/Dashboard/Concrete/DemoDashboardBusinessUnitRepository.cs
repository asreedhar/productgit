﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Concrete
{
    internal class DemoDashboardBusinessUnitRepository : IDashboardBusinessUnitRepository
    {
        private DemoBusinessUnitRepository _demoRepository;

        public DemoDashboardBusinessUnitRepository()
        {
            _demoRepository = new DemoBusinessUnitRepository();
        }

        public IDashboardBusinessUnit GetBusinessUnit(int businessUnitId)
        {
            return _demoRepository.Fetch(businessUnitId);
        }

        public IDashboardBusinessUnit[] GetBusinessUnitsfromGroup(int groupId)
        {
            var units = _demoRepository.FetchGroupMembersByGroupId(groupId);
            return units.ToArray();
        }

        public IDashboardBusinessUnit[] FindAllDealershipsByDealerGroupAndMember(int groupId, string login)
        {
            throw new NotImplementedException();
        }

        public bool HasDealerUpgrade(int businessUnitId, int upgrade)
        {
            if (upgrade == 24) //max upgrade. All demo users have it.
                return true;

            return false;
        }

        public IDashboardBusinessUnit GetGroup(int businessUnitId)
        {
            return _demoRepository.FetchGroupByDealerId(businessUnitId);
        }
    }
}
