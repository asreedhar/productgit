﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Concrete
{
    public class DashboardBusinessUnit : IDashboardBusinessUnit
    {
        public static readonly string HttpContextKey = "DashboardBusinessUnit";

        public int BusinessUnitId { get; set; }
        public int Type { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
