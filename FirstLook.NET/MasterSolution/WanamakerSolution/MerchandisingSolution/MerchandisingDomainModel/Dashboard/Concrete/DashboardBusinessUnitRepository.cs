﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Concrete
{
    internal class DashboardBusinessUnitRepository : IDashboardBusinessUnitRepository
    {
        private static IDashboardBusinessUnitRepository GetInnerRepositry(int value)
        {
            if (DemoBusinessUnitRepository.IsDemoUnit(value))
                return new DemoDashboardBusinessUnitRepository();
            
            return new ImtDashboardBusinessUnitRepository();
        }

        public IDashboardBusinessUnit GetBusinessUnit(int businessUnitId)
        {
            return GetInnerRepositry(businessUnitId).GetBusinessUnit(businessUnitId);
        }

        public IDashboardBusinessUnit[] GetBusinessUnitsfromGroup(int groupId)
        {
            return GetInnerRepositry(groupId).GetBusinessUnitsfromGroup(groupId);
        }

        public IDashboardBusinessUnit[] FindAllDealershipsByDealerGroupAndMember(int groupId, string login)
        {
            return GetInnerRepositry(groupId).FindAllDealershipsByDealerGroupAndMember(groupId, login);
        }

        public bool HasDealerUpgrade(int businessUnitId, int upgrade)
        {
            return GetInnerRepositry(businessUnitId).HasDealerUpgrade(businessUnitId, upgrade);
        }

        public IDashboardBusinessUnit GetGroup(int businessUnitId)
        {
            return GetInnerRepositry(businessUnitId).GetGroup(businessUnitId);
        }

    }
}
