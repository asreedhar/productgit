﻿using System;
namespace FirstLook.Merchandising.DomainModel.Dashboard.Abstract
{
    public interface IClock
    {
        DateTime CurrentTime { get; }
    }
}
