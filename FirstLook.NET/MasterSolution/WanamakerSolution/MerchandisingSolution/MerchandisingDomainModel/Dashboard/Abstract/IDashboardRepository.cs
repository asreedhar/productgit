﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;
using Merchandising.Messages;
using Bucket = FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Abstract
{
    public interface IDashboardRepository
    {
        IDictionary<VehicleType, int> GetActiveInventoryCounts(int businessUnitId);

        Inventory GetInventoryFromStockNumber(int businessUnitId, string stockNumber);

        bool IsDashboardEnabled(int businessUnitId);

        byte GetMaxVersion (int businessUnitId);

        bool HasDealerUpgrade (int businessUnitId, Upgrade upgrade);

        IEnumerable<Bucket> GetBucketCounts(int businessUnitId, VehicleType usedOrNewFilter);

        int GetBucketTotal( int businessUnitId, VehicleType usedOrNewFilter, ISet<WorkflowType> bucketIds );

        AutoApproveStatus GetAutoApproveStatus(int businessUnitId);

        OnlineStatus GetNotOnlineCount(int businessUnitId, VehicleType usedOrNewFilter);

        BucketAlertThreshold[] GetBucketAlertThresholds(int businessUnitId);

        string ResolveDBEnum( string table, string key_column, string value_column, string key );

        Dictionary<string, Reports.SitePerformance.DealerSiteInformation> GetStaticDealerSiteData( IDashboardBusinessUnit unit );

        GoogleAnalyticsResult GetAllGoogleAnalyticsData(int businessUnitId, FirstLook.Merchandising.DomainModel.GoogleAnalytics.GoogleAnalyticsType gaProfileType, DateRange dateRange);
        List<GoogleAnalyticsResult> GetAllGoogleAnalyticsDataUngrouped(int businessUnitId, FirstLook.Merchandising.DomainModel.GoogleAnalytics.GoogleAnalyticsType gaProfileType, DateRange dateRange, string[] queryNameFilter);

        DashboardSettingsModel GetDashboardSettings(int businessUnitId);

        IList<BucketValue> FetchBucketValuesReport(int businessUnitId, DateRange dateRange);
    }
}
