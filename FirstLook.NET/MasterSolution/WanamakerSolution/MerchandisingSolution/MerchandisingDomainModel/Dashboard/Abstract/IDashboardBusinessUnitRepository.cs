﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Abstract
{
    public interface IDashboardBusinessUnitRepository
    {
        IDashboardBusinessUnit GetBusinessUnit(int businessUnitId);
        IDashboardBusinessUnit GetGroup(int businessUnitId);
        IDashboardBusinessUnit[] GetBusinessUnitsfromGroup(int groupId);

        IDashboardBusinessUnit[] FindAllDealershipsByDealerGroupAndMember(int groupId, string login);

        bool HasDealerUpgrade(int businessUnitId, int upgrade);
    }
}
