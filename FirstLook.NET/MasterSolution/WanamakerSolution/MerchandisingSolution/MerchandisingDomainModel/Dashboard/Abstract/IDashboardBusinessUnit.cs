﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Abstract
{
    public interface IDashboardBusinessUnit
    {
        int BusinessUnitId { get; set; }
        int Type { get; set; }
        string Code { get; set; }
        string Name { get; set; }
    }
}
