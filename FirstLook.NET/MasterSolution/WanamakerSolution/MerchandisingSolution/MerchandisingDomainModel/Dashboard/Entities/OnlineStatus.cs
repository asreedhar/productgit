﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Entities
{
    public class OnlineStatus
    {
        public int TotalInventoryCount { get; set; }
        public int TotalNotOfflineReportCount { get; set; }
        public int NoDataCount { get; set; }
        public int OnlineCount { get; set; }
        public int NotOnlineCount { get; set; }
        public int AgeInDays { get; set; }
        
        public double NotOnlinePercent
        {
            get
            {
                if (TotalNotOfflineReportCount == 0 || NotOnlineCount > TotalNotOfflineReportCount) return 0;

                var val = ((decimal)NotOnlineCount / TotalNotOfflineReportCount) * 100;
                return (val > 0 && val < 1) ? 1 : (int)Math.Round(val);
            }
        }
    }
}
