﻿using System.Collections.Generic;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Entities
{
    public enum MerchandisingGraphSettings
    {
        NoPhotos = 0,
        NoPrice = 1        
    }

    public class MerchandisingGraphSettingsHelper
    {
        private static readonly Dictionary<MerchandisingGraphSettings, WorkflowType> MapperDictionary = new Dictionary<MerchandisingGraphSettings, WorkflowType>()
        {
            {MerchandisingGraphSettings.NoPhotos, WorkflowType.NoPhotos},
            {MerchandisingGraphSettings.NoPrice, WorkflowType.NoPrice}
        };

        public static WorkflowType GetWorkflowType(MerchandisingGraphSettings settings)
        {
            WorkflowType wfType;
            MapperDictionary.TryGetValue(settings, out wfType);
            if(wfType == WorkflowType.None)
                wfType = WorkflowType.NoPhotos;

            return wfType;
        }
    }
}