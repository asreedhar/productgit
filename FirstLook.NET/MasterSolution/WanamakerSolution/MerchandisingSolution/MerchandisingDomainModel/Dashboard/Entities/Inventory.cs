﻿using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Entities
{
    public class Inventory
    {
        public string StockNumber { get; set; }

        public int InventoryId { get; set; }

        public VehicleType InventoryType { get; set; }

        public bool IsPreOwned
        {
            get { return InventoryType == VehicleType.Used; }
        }

    }
}
