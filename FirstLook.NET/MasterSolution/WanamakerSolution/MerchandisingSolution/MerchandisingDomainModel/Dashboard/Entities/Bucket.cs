﻿using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Dashboard.Entities
{
    public class Bucket
    {
        public WorkflowType BucketId { get; set; }
        public string BucketDescription { get; set; }
        public int BucketCount { get; set; }
        public string NavigationUrl { get; set; }
    }
}
