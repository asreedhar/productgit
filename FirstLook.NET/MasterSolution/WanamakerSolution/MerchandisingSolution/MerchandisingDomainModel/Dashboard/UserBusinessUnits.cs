﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Data;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Dashboard
{
    public class UserBusinessUnits
    {
        private const string CacheKey = "AdminUserBusinessUnitsCacheKey";

        private string _redirectUrl;
        private ICache _cache;
        private bool _isAdministrator;
        private int _currentBusinessUnit;

        public UserBusinessUnits(string redirectUrl, ICache cache, bool isAdministrator, int currentBusinessUnit)
        {
            _redirectUrl = redirectUrl;
            _cache = cache;
            _isAdministrator = isAdministrator;
            _currentBusinessUnit = currentBusinessUnit;
        }

        private string CreateRedirectUrl(int businessUnitId)
        {
            var redirector = new RedirectEntry(businessUnitId, _redirectUrl);
            var url = String.Format("{0}/workflow/link.aspx?link={1}",HttpContext.Current.Request.ApplicationPath, redirector.ToToken());
            return url;
        }

        public string GenerateJson(string user)
        {
            Group[] groups = null;

            if (_isAdministrator)
                groups = _cache.Get(CacheKey) as Group[];

            if (groups == null)
            {
                groups = FetchDbGroups(user);

                if (_isAdministrator)
                    _cache.Set(CacheKey, groups);
            }

            Array.ForEach(groups.SelectMany(x => x.BusinessUnits).Where(x => x.Selected).ToArray(), x => x.Selected = false);

            //var selectedBusinessUnit = groups.SelectMany(x => x.BusinessUnits).Where(x => x.Id == _currentBusinessUnit).SingleOrDefault();
            var group_businessUnits = groups.SelectMany(x => x.BusinessUnits);
            var current_equivalence = group_businessUnits.Where(x => x.Id == _currentBusinessUnit);
            var selectedBusinessUnit = current_equivalence.FirstOrDefault();

            if (selectedBusinessUnit != null)
                selectedBusinessUnit.Selected = true;

            string value = groups.ToJson();

            return value;
        }

        #region DbWork
        private Group[] FetchDbGroups(string user)
        {
            List<Group> groups = new List<Group>();
            using (IDataConnection con = Database.GetConnection(Database.IMTDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = GetQuery();
                    cmd.CommandType = CommandType.Text;

                    if (!_isAdministrator)
                        cmd.AddRequiredParameter("@MemberName", user, DbType.String);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int groupid = reader.GetInt32(reader.GetOrdinal("GroupId"));
                            int dealerid = reader.GetInt32(reader.GetOrdinal("DealerId"));
                            string groupName = reader.GetString(reader.GetOrdinal("GroupName"));
                            string dealerName = reader.GetString(reader.GetOrdinal("DealerName"));

                            Group group = groups.FirstOrDefault(x => x.Id == groupid);
                            if (group == null)
                            {
                                group = new Group() { Id = groupid, Name = groupName, BusinessUnits = new List<BusinessUnit>() };
                                groups.Add(group);
                            }

                            BusinessUnit bu = new BusinessUnit() { Id = dealerid, Name = dealerName, Link = CreateRedirectUrl(dealerid), Selected = false };
                            group.BusinessUnits.Add(bu);
                        }
                    }
                }

            }

            return groups.ToArray();
        }

        private string GetQuery()
        {
            string cmdText = String.Empty;
            if (_isAdministrator)
            {
                cmdText = @"
                        SELECT 
                            BuGroup.BusinessUnitID AS GroupId, BuGroup.BusinessUnit AS GroupName, 
                            BuDealers.BusinessUnitID AS DealerId, BuDealers.BusinessUnit AS DealerName
                        FROM dbo.BusinessUnit AS BuGroup
                        JOIN dbo.BusinessUnitRelationship br ON br.parentid = BuGroup.BusinessUnitId
                        JOIN dbo.BusinessUnit AS BuDealers ON BuDealers.BusinessUnitID = br.BusinessUnitID
                        WHERE
		                        BuDealers.Active = 1
                            AND     BuDealers.BusinessUnitTypeID = 4
                            AND     BuGroup.Active = 1
                            AND     BuGroup.BusinessUnitTypeID = 3 
                            
                            GROUP
                            BY      BuGroup.BusinessUnitID, BuDealers.BusinessUnitID, BuGroup.BusinessUnit, BuDealers.BusinessUnit
                            HAVING  COUNT(*) > 0
                            ORDER BY GroupName ASC, DealerName ASC";
            }
            else
            {
                cmdText = @"
                        DECLARE @MemberId int;
                        SET @MemberId = (SELECT MemberID FROM IMT.dbo.Member WHERE Login = @MemberName);

                        SELECT
	                        BU2.BusinessUnitID AS GroupId, BU2.BusinessUnit AS GroupName, BU1.BusinessUnitID AS DealerId, BU1.BusinessUnit DealerName
                        FROM
                                dbo.BusinessUnit BU1
                        JOIN	dbo.BusinessUnitRelationship BR1 ON BU1.BusinessUnitID = BR1.BusinessUnitID
                        JOIN	dbo.BusinessUnit BU2 ON BU2.BusinessUnitID = BR1.ParentID
                        JOIN	(
                                SELECT	BusinessUnitID
                                    FROM	[IMT].dbo.MemberAccess
                                    WHERE	MemberID = @MemberId
                                UNION ALL
                                    SELECT	B.BusinessUnitID
                                    FROM	[IMT].dbo.BusinessUnitRelationship B CROSS JOIN [IMT].dbo.Member M
                                WHERE	M.MemberID = @MemberId
                                    AND		M.MemberType = 1
                                ) MA1 ON MA1.BusinessUnitID = BU1.BusinessUnitID
                        JOIN	dbo.DealerPreference DP1 ON DP1.BusinessUnitID = BU1.BusinessUnitID
                        WHERE
                                BU1.BusinessUnitTypeID = 4
                        AND	    BU1.Active = 1 
                        AND     DP1.GoLiveDate IS NOT NULL
                        ORDER BY GroupName ASC, DealerName ASC
                    ";
            }
            return cmdText;
        }
        #endregion DbWork

        #region PrivateClasses
        private class Group
        {
            public string Name { get; set; }
            public int Id { get; set; }
            public List<BusinessUnit> BusinessUnits { get; set; }
        }

        private class BusinessUnit
        {
            public bool Selected { get; set; }
            public string Link { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
        }
        #endregion PrivateClasses
    }
}
