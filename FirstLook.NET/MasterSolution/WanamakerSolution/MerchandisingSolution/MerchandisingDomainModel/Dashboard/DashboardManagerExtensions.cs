﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using MAX.Entities;
using MAX.Entities.Helpers.DashboardHelpers;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Dashboard
{
    public static class DashboardManagerExtensions
    {
        private static readonly List<WorkflowType> _actionBucketIds = new List<WorkflowType> { 
          WorkflowType.NoPrice, 
          WorkflowType.CreateInitialAd, 
          WorkflowType.NoPhotos, 
          WorkflowType.NotOnline,
          WorkflowType.Offline, // does not really belong here as this is not actionable (requires explicit setting)
        };
        private static readonly List<WorkflowType> _activityBucketIds = new List<WorkflowType> { 
          WorkflowType.LowActivity, 
          WorkflowType.NoPackages, 
          WorkflowType.BookValueNeeded, 
          WorkflowType.NoCarfax, 
          WorkflowType.LowPhotos,
          WorkflowType.EquipmentNeeded, 
          WorkflowType.DueForRepricing,
          WorkflowType.TrimNeeded,
          WorkflowType.NoFavourablePriceComparisons
        };
        private static readonly List<WorkflowType> _maxBuckets = new List<WorkflowType> { 
          WorkflowType.AdReviewNeeded,
        };

        private static readonly ILog Log = LoggerFactory.GetLogger<DashboardManager>();

        public static Dictionary<string, bool> GetBucketsActive(this IDashboardManager manager, int businessUnitId)
        {
            var buckets_active = new Dictionary<string, bool>();
            var thresholds = manager.GetBucketAlertThresholds(businessUnitId);
            foreach (var t in thresholds)
                buckets_active.Add(t.Bucket.ToString(), t.Active);
            return buckets_active;
        }


        public static List<Entities.Bucket> GetBuckets(this IDashboardManager manager, int businessUnitId, VehicleType usedOrNewFilter, List<WorkflowType> bucketIds)
        {
            return manager.GetBuckets(businessUnitId, usedOrNewFilter, bucketIds).ToList();
        }

        public static Buckets GetBucketsData(this IDashboardManager manager, int businessUnitId, VehicleType usedOrNewFilter, bool isGroup = false)
        {
            using (MiniProfiler.Current.Step("Buckets Data"))
            {
                Log.Debug("Getting Buckets Data");
                var available_buckets = manager.GetBuckets(businessUnitId, usedOrNewFilter).ToList();
                var model = new Buckets();

                var allInventoryBucket = GetBuckets(manager, businessUnitId, usedOrNewFilter, null).Find(i => i.BucketId == WorkflowType.AllInventory);
                if( allInventoryBucket != null )
                    model.SelectedFilterCount = allInventoryBucket.BucketCount;

                //Get the current total
                var onlineStatus = manager.GetNotOnlineCount(businessUnitId, usedOrNewFilter);

                var thresholds = manager.GetBucketAlertThresholds(businessUnitId);

                using (MiniProfiler.Current.Step("Action Buckets"))
                {
                    Log.Debug("Getting action buckets");
                    Bucket notOnlineBucket;
                    using (MiniProfiler.Current.Step("Not Online Buckets"))
                    {
                        notOnlineBucket = new Bucket
                        {
                            Count = onlineStatus.NotOnlineCount,
                            DisplayName = "Not Online",
                            Id = "Offline",
                            NavigationUrl = Helper.GetNotOnlineUrl(usedOrNewFilter)
                        };
                    }

                    model.NotOnlineCount = notOnlineBucket.Count;

                    model.NotOnlineAgeInDays = onlineStatus.AgeInDays;

                    model.NotOnlinePercent = onlineStatus.NotOnlinePercent;

                    model.ActionBuckets = new List<Bucket>();

                    //Get the actionbuckets
                    var actionBucketsUnfiltered = GetBuckets(manager, businessUnitId, usedOrNewFilter, _actionBucketIds);
                    var actionBuckets = actionBucketsUnfiltered.Where(
                        bucket => thresholds.Any(threshold => threshold.Active && threshold.Bucket == bucket.BucketId)).ToList();
                    
                    var selectedActionBucketIds = new HashSet<WorkflowType>();

                    foreach (var actionBucket in actionBuckets)
                    {
                        selectedActionBucketIds.Add(actionBucket.BucketId);

                        var bucketVm = new Bucket
                        {
                            Count = actionBucket.BucketCount,
                            DisplayName = actionBucket.BucketDescription,
                            Id = actionBucket.BucketId.ToString(),
                            NavigationUrl = Helper.GetBucketNavigationUrl(actionBucket.BucketId, usedOrNewFilter)
                        };
                        //We get the offline bucket but it's not shown as part of the action buckets
                        if (bucketVm.Id == WorkflowType.Offline.ToString())
                        {
                            model.OfflineBucket = bucketVm;
                        }
                        else if (bucketVm.Id == WorkflowType.NotOnline.ToString()) // special bucket
                        {
                            model.ActionBuckets.Add(notOnlineBucket);
                        }
                        else
                        {
                            model.ActionBuckets.Add(bucketVm);
                        }
                    }

                    selectedActionBucketIds.Remove(WorkflowType.Offline); // should never be included in total
                    model.ActionBucketsTotal = manager.GetBucketTotal(businessUnitId, usedOrNewFilter, selectedActionBucketIds);

                }
                using (MiniProfiler.Current.Step("Activity Buckets"))
                {
                    Log.Debug("Getting Activity Buckets");
                    model.ActivityBuckets = new List<Bucket>();

                    //Get the activitybuckets
                    var activityBuckets = GetBuckets(manager, businessUnitId, usedOrNewFilter, _activityBucketIds).Where(bucket => thresholds.Any(threshold => threshold.Active && threshold.Bucket == bucket.BucketId));

                    var selectedActivityBucketIds = new HashSet<WorkflowType>();

                    foreach (var activityBucket in activityBuckets)
                    {
                        selectedActivityBucketIds.Add(activityBucket.BucketId);

                        var bucketVm = new Bucket
                        {
                            Count = activityBucket.BucketCount,
                            DisplayName = activityBucket.BucketDescription,
                            Id = activityBucket.BucketId.ToString(),
                            NavigationUrl = Helper.GetBucketNavigationUrl(activityBucket.BucketId, usedOrNewFilter)
                        };

                        if (bucketVm.Id == WorkflowType.LowActivity.ToString())
                        {
                            model.LowActivityCount = bucketVm.Count;
                            bucketVm.NavigationUrl = Helper.GetLowActivityReportUrl(usedOrNewFilter);
                        }

                        model.ActivityBuckets.Add(bucketVm);
                    }

                    model.ActivityBucketsTotal = manager.GetBucketTotal(businessUnitId, usedOrNewFilter, selectedActivityBucketIds);
                }

                using (MiniProfiler.Current.Step("Max Buckets"))
                {
                    Log.Debug("Getting Max Buckets");
                    var maxBuckets = manager.GetBuckets(businessUnitId, usedOrNewFilter, _maxBuckets);

                    Log.DebugFormat("Got Max Buckets: {0}", String.Join(":|:", maxBuckets.Select(bk => string.Format("{0}|{1}", bk.BucketId, bk.BucketCount))));

                    model.MaxBuckets = new List<Bucket>();
                    foreach (var result in maxBuckets.Select(bucket => new Bucket
                    {
                        Count = bucket.BucketCount,
                        DisplayName = bucket.BucketDescription,
                        Id = bucket.BucketId.ToString(),
                        NavigationUrl = Helper.GetBucketNavigationUrl(bucket.BucketId, usedOrNewFilter)
                    }))
                    {
                        model.MaxBuckets.Add(result);
                    }

                    Log.DebugFormat("model.MaxBuckets: {0} ", model.MaxBuckets.ToJson());
                }

                model.NeedsActionCount = model.ActionBuckets.Count > 0? model.ActionBuckets.Max(bucket => bucket.Count) : 0;

                return model;
            }

        }

    }
}
