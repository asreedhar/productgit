﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;
using MAX.Entities.Reports.TimeToMarket;
using Merchandising.Messages;
using Bucket = FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket;

namespace FirstLook.Merchandising.DomainModel.Dashboard
{
    public interface IDashboardManager
    {
        IDictionary<VehicleType, int> GetActiveInventoryCounts(int businessUnitId);
        
        GoogleAnalyticsResult GetAllGoogleAnalyticsData(int businessUnitId, GoogleAnalytics.GoogleAnalyticsType gaType, DateRange dateRange);
        List<GoogleAnalyticsResult> GetAllGoogleAnalyticsDataUngrouped(int businessUnitId, GoogleAnalytics.GoogleAnalyticsType gaType, DateRange dateRange, string[] queryNameFilter);

        BucketAlertThreshold[] GetBucketAlertThresholds(int businessUnitId);
        IEnumerable<Bucket> GetBuckets(int businessUnitId, VehicleType usedOrNewFilter, List<WorkflowType> bucketIds);
        IEnumerable<Bucket> GetBuckets(int businessUnitId, VehicleType usedOrNewFilter);
        OnlineStatus GetNotOnlineCount(int businessUnitId, VehicleType usedOrNewFilter);
        int GetBucketTotal(int businessUnitId, VehicleType usedOrNewFilter, ISet<WorkflowType> bucketIds);
        Dictionary<string, DealerSiteInformation> GetStaticDealerSiteData(IDashboardBusinessUnit unit);
        
        AutoApproveStatus GetAutoApproveStatus(int businessUnitId);
        string ResolveDBEnum(string table, string key_column, string value_column, string key);

        IEnumerable<SitePerformance> GetSitePerformanceData(int businessUnitId, VehicleType vehicleType, DateTime startDate, DateTime endDate);
        BusinessUnitReport GetTimeToMarket(int businessUnitId, VehicleType vehicleType, DateRange dateRange);
        Dictionary<string, List<SitePerformance>> GetMonthlySiteTrends(int businessUnitId, VehicleType vehicleType, DateRange dateRange);

        string GetMaxSeLink(IDashboardBusinessUnit businessUnit, string stockNumber, ref string message);
        byte GetMaxVersion(int businessUnitId);
        bool IsDashboardEnabled(int businessUnitId);
        bool SupportsMaxSe(int businessUnitId);
        bool HasDealerUpgrade(int businessUnitId, Upgrade upgrade);
        Dictionary<string, bool> GetRoles(string userName);
        DashboardSettingsModel GetDashboardSettings(int businessUnitId);
    }
}
