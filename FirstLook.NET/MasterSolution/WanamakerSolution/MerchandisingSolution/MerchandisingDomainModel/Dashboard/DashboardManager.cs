﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;
using MAX.Entities.Reports.TimeToMarket;
using Merchandising.Messages;
using Bucket = FirstLook.Merchandising.DomainModel.Dashboard.Entities.Bucket;

namespace FirstLook.Merchandising.DomainModel.Dashboard
{
    internal class DashboardManager : IDashboardManager
    {
        private readonly IDashboardRepository _repository;
        private readonly ISitePerformanceRepository _sitePerformanceRepository;
        private readonly IUserRepository _userRepository;
        private readonly ITimeToMarketRepository _ttmRepository;
        private readonly IWebsiteProviderMetricsModel _websiteProviderMetrics;
        private static readonly ILog Log = LoggerFactory.GetLogger<DashboardManager>();

        public DashboardManager(
            IDashboardRepository repository,
            ISitePerformanceRepository sitePerformanceRepository,
            IUserRepository userRepository,
            ITimeToMarketRepository ttmRepository,
            IWebsiteProviderMetricsModel websiteProviderMetrics )
        {
            _repository = repository;
            _sitePerformanceRepository = sitePerformanceRepository;
            _userRepository = userRepository;
            _ttmRepository = ttmRepository;
            _websiteProviderMetrics = websiteProviderMetrics;
        }

        public IDictionary<VehicleType, int> GetActiveInventoryCounts(int businessUnitId)
        {
            return _repository.GetActiveInventoryCounts(businessUnitId);
        }

        public bool SupportsMaxSe (int businessUnitId)
        {
            return HasDealerUpgrade( businessUnitId, Upgrade.Marketing );
        }

        public bool HasDealerUpgrade (int businessUnitId, Upgrade upgrade)
        {
            return _repository.HasDealerUpgrade( businessUnitId, upgrade );
        }

        public bool IsDashboardEnabled(int businessUnitId)
        {
            return _repository.IsDashboardEnabled(businessUnitId);
        }

        public byte GetMaxVersion(int businessUnitId)
        {
            return _repository.GetMaxVersion( businessUnitId );
        }

        public string GetMaxSeLink(IDashboardBusinessUnit businessUnit, string stockNumber, ref string message)
        {
            if (string.IsNullOrWhiteSpace(stockNumber))
            {
                message = "Stock number is blank.";
                return null;
            }

            stockNumber = stockNumber.Trim();

            if (!SupportsMaxSe(businessUnit.BusinessUnitId))
            {
                message = string.Format("{0} does not have Marketing upgrade.", businessUnit.Name);
                return null;
            }

            var inventory = _repository.GetInventoryFromStockNumber(businessUnit.BusinessUnitId, stockNumber);

            if (inventory == null)
            {
                message = string.Format("Sorry, we couldn't find the vehicle with stock number {0}.", stockNumber);
                
                return null;
            }

            if (!inventory.IsPreOwned)
            {
                message = "Max SE is only supported for Pre owned vehicles. ";
                return null;
            }

            return InventoryData.GetMaxSellingLink(businessUnit.BusinessUnitId, inventory.InventoryId,
                                                ConfigurationManager.AppSettings["marketing_page_from_host"]);
        }

        public IEnumerable<Bucket> GetBuckets(int businessUnitId, VehicleType usedOrNewFilter)
        {
            return GetBuckets(businessUnitId, usedOrNewFilter, null);
        }

        //Show the Ad review bucket if it's on for anything.
        public bool IsAdReviewApplicable(int businessUnitId, VehicleType usedOrNewFilter, AutoApproveStatus status)
        {

            if (status == AutoApproveStatus.Off) return false;

            if (status == AutoApproveStatus.OnForAll) return true;

            if (status == AutoApproveStatus.OnForNew && usedOrNewFilter == VehicleType.Used) return false;

            if (status == AutoApproveStatus.OnForUsed && usedOrNewFilter == VehicleType.New) return false;

            return true;

        }

        public AutoApproveStatus GetAutoApproveStatus(int businessUnitId)
        {
            return _repository.GetAutoApproveStatus(businessUnitId);
        }

        public IEnumerable<Bucket> GetBuckets(int businessUnitId, VehicleType usedOrNewFilter, List<WorkflowType> bucketIds)
        {
            Log.DebugFormat("DashboardManager.GetBuckets -> called with BUID:{0}, VehicleType: {1}",
                businessUnitId, usedOrNewFilter);

            if (bucketIds != null)
            {
                Log.DebugFormat("DashboardManager.GetBuckets -> Called with BucketIDs: {0}", String.Join("|", bucketIds));
            }

            var allBuckets = _repository.GetBucketCounts(businessUnitId, usedOrNewFilter);
            if(bucketIds == null) return allBuckets;

            Log.DebugFormat("DashboardManager.GetBuckets ->BucketCount: {0}", allBuckets.Count());

            if ( (bucketIds.Contains(WorkflowType.AdReviewNeeded) || bucketIds.Contains(WorkflowType.AdReviewNeeded)))
            {
                Log.Debug("DashboardManager.GetBuckets -> Contains WorkflowType.AdReviewNeeded");
                var status = _repository.GetAutoApproveStatus(businessUnitId);

                Log.DebugFormat("DashboardManager.GetBuckets -> AutoApproveStatus: {0}", status);
                //Logic to see if the Ad review neeed bucket should be shown.
                if ( bucketIds.Contains(WorkflowType.AdReviewNeeded))
                {
                    bool showAdReviewNeeded = IsAdReviewApplicable(businessUnitId, usedOrNewFilter, status);
                    Log.DebugFormat("DashboardManager.GetBuckets -> showAdReviewNeeded: {0}", showAdReviewNeeded);
                    if (!status.Equals(AutoApproveStatus.Off))
                        Log.DebugFormat("{0} should have max buckets", businessUnitId);
 
                    if (!showAdReviewNeeded)
                    {
                        Log.Debug("DashboardManager.GetBuckets -> Removing AdReviewNeeded from bucketIds");
                        bucketIds.Remove(WorkflowType.AdReviewNeeded);
                    }
                }
            }

            Log.DebugFormat("DashboardManager.GetBuckets -> About to filter based on the list of bucketIds: {0}", String.Join("|", bucketIds));
            //filter the list
            var filteredList =  allBuckets.Where(bucket => bucketIds.Contains(bucket.BucketId)).ToList();
            Log.DebugFormat("DashboardManager.GetBuckets -> unsorted filtered list: {0}", String.Join(":|:", filteredList.Select(bk => string.Format("{0}|{1}", bk.BucketId, bk.BucketCount))));
            
            //sorting by incoming list of bucketids
            return SortBucketsByIds(filteredList, bucketIds);

        }

        public int GetBucketTotal( int businessUnitId, VehicleType usedOrNewFilter, ISet<WorkflowType> bucketIds )
        {
            return _repository.GetBucketTotal( businessUnitId, usedOrNewFilter, bucketIds );
        }
        
        public static List<Bucket> SortBucketsByIds(IEnumerable<Bucket> listToSort, IList<WorkflowType> bucketIds)
        {
            return listToSort.OrderBy(x => bucketIds.IndexOf(x.BucketId)).ToList();
            
        }

        public IEnumerable<SitePerformance> GetSitePerformanceData(int businessUnitId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            var siteTrends = _sitePerformanceRepository.GetMonthlySiteTrends(businessUnitId, vehicleType, new DateRange(startDate, endDate));
            List<SitePerformance> model = new List<SitePerformance>();
            siteTrends.Values.ToList().ForEach(tdv =>
            {
                if (tdv.Count() > 0)
                {
                    var total = tdv.First();
                    total = tdv.Summation();
                    model.Add(total);
                }
            });

            return model != null ? model.OrderBy(x => x.Site.Name) : new List<SitePerformance>().AsEnumerable();
        }

        public Dictionary<string,List<SitePerformance>> GetMonthlySiteTrends(int businessUnitId, VehicleType vehicleType, DateRange dateRange)
        {
            Dictionary<string, List<SitePerformance>> trendData = _sitePerformanceRepository.GetMonthlySiteTrends( businessUnitId, vehicleType, dateRange );            

            return trendData;
        }

        public OnlineStatus GetNotOnlineCount(int businessUnitId, VehicleType usedOrNewFilter)
        {
            return _repository.GetNotOnlineCount(businessUnitId, usedOrNewFilter);
        }

        public BucketAlertThreshold[] GetBucketAlertThresholds(int businessUnitId)
        {
            return _repository.GetBucketAlertThresholds(businessUnitId);
        }


        public string ResolveDBEnum( string table, string key_column, string value_column, string key )
        {
            return _repository.ResolveDBEnum( table, key_column, value_column, key );
        }


        public Dictionary<string, DealerSiteInformation> GetStaticDealerSiteData(IDashboardBusinessUnit unit)
        {
            return _repository.GetStaticDealerSiteData( unit );
        }

        public GoogleAnalyticsResult GetAllGoogleAnalyticsData(int businessUnitId, GoogleAnalytics.GoogleAnalyticsType gaType, DateRange dateRange)
        {
            return _repository.GetAllGoogleAnalyticsData(businessUnitId, gaType, dateRange);
        }

        public List<GoogleAnalyticsResult> GetAllGoogleAnalyticsDataUngrouped(int businessUnitId, GoogleAnalytics.GoogleAnalyticsType gaType, DateRange dateRange, string[] queryNameFilter)
        {
            return _repository.GetAllGoogleAnalyticsDataUngrouped(businessUnitId, gaType, dateRange, queryNameFilter);
        }

        public Dictionary<string,bool> GetRoles( string userName )
        {
            var user = _userRepository.GetUser( userName );
            var roles = new Dictionary<string,bool>();
            // straight from [Merchandising].[dbo].[aspnet_Roles].[RoleName]
            var role_names = new string[]{ 
	            "Administrator",
	            "Approver",
	            "Builder",
	            "Certified Preowned Writer",
	            "Description Editor",
	            "Manager",
	            "MiniProfiler",
	            "Photographer",
	            "Pricer",
	            "Report Viewer",
	            "Sales Representative",
	            "Templater",
	            "User"
            };
            foreach( string r in role_names )
                roles[r] = user.IsInRole( r );
            
            return roles;
        }

        public DashboardSettingsModel GetDashboardSettings(int businessUnitId)
        {
            return _repository.GetDashboardSettings(businessUnitId);
        }

        public BusinessUnitReport GetTimeToMarket(int businessUnitId, VehicleType vehicleType, DateRange dateRange)
        {
            return _ttmRepository.FetchReport(businessUnitId, vehicleType, dateRange);
        }

        public List<MonthlyWebsiteMetric> GetVDPs( int businessUnitId, DateTime startDate, DateTime endDate )
        {
            return _websiteProviderMetrics.GetVDPs( businessUnitId, startDate, endDate );
        }
    }
}
