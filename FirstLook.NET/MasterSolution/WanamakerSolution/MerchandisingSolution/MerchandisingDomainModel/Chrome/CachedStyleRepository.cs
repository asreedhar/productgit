﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public class CachedStyleRepository : IStyleRepository
    {
        private readonly CachedSetBasedFetch<Style> _cache;
        private readonly IStyleRepository _inner;

        public CachedStyleRepository(ICache cache, IStyleRepository inner)
        {
            _cache = new CachedSetBasedFetch<Style>(
                cache, 12 * 60 * 60, "ChromeStyle_",
                ids => _inner.GetStyleById(ids), 
                style => style.StyleID);
            _inner = inner;
        }

        public IEnumerable<Style> GetStyleById(IEnumerable<int> ids)
        {
            return _cache.GetById(ids);
        }
    }
}