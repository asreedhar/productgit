﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public interface IStyleRepository
    {
        IEnumerable<Style> GetStyleById(IEnumerable<int> ids);
    }
}