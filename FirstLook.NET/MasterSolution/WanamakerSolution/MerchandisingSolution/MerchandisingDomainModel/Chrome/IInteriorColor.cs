namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public interface IInteriorColor
    {
        string Code { get; }
        string MfgCode { get; }
        string Desc { get; }
    }
}