﻿using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public class CachedSharedChromeDataRepository : ISharedChromeDataRepository
    {
        private readonly ICache _cache;
        private readonly ISharedChromeDataRepository _inner;

        private static readonly string CacheKey = typeof (CachedSharedChromeDataRepository).Name;
        private const int CacheExpirationPolicyInSeconds = 12*60*60;

        public CachedSharedChromeDataRepository(ICache cache, ISharedChromeDataRepository inner)
        {
            _cache = cache;
            _inner = inner;
        }

        public SharedChromeData GetData()
        {
            var data = (SharedChromeData)_cache.Get(CacheKey);
            if(data == null)
            {
                data = _inner.GetData();
                _cache.Set(CacheKey, data, CacheExpirationPolicyInSeconds);
            }
            return data;
        }
    }
}