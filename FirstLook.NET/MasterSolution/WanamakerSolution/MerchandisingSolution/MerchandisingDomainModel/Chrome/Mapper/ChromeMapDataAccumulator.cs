using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal class ChromeMapDataAccumulator
    {
        private class Data : IChromeMapData
        {
            public IDictionary<VinPart, VinPatternSuffixes> VinPatternLookup { get; set; }
            public VinPatternSuffix[] VinPatternSuffixList { get; set; }
            public byte[] VinPatternBuffer { get; set; }
            public IDictionary<int, int> VehicleCatalogLookup { get; set; }
        }

        private readonly Dictionary<int, int> VehicleCatalogIdToChromeStyleIdMap = new Dictionary<int, int>();
        private readonly List<byte> Buffer = new List<byte>();
        private readonly Dictionary<VinPart, int> Tokens9 = new Dictionary<VinPart, int>(new VinPartComparer(9));
        private readonly Dictionary<VinPart, int> Tokens7 = new Dictionary<VinPart, int>(new VinPartComparer(7));
        private readonly byte[] TokenBuffer = new byte[9];
        private readonly List<VinPatternSuffix> VinPatternSuffixList = new List<VinPatternSuffix>();
        private readonly Dictionary<int, VinPatternSuffixes> VinPatternLookup = new Dictionary<int, VinPatternSuffixes>();

        private int CurrentPrefixToken = -1;
        private int CurrentSuffixIndex;
        private int CurrentSuffixCount;

        public void AccumulateVehicleCatalogEntry(int vehicleCatalogId, int chromeStyleId)
        {
            VehicleCatalogIdToChromeStyleIdMap.Add(vehicleCatalogId, chromeStyleId);
        }

        public void AccumulateVinPatternEntry(string prefix, string suffix, int? chromeStyleId, int vinPatternId)
        {
            AccumulateVinPatternEntry(prefix.ToCharArray(), suffix.ToCharArray(), chromeStyleId, vinPatternId);
        }

        public void AccumulateVinPatternEntry(char[] prefix, char[] suffix, int? chromeStyleId, int vinPatternId)
        {
            if (prefix.Length != 9) throw new ArgumentOutOfRangeException("prefix", "Prefix must be 9 characters.");
            if (suffix.Length != 7) throw new ArgumentOutOfRangeException("suffix", "Suffix must be 7 characters.");

            var prefixToken = GetToken(prefix);
            var suffixToken = GetToken(suffix);

            if(prefixToken != CurrentPrefixToken)
            {
                CommitCurrentPrefix();
                SetCurrentPrefixToken(prefixToken);
            }

            VinPatternSuffixList.Add(
                new VinPatternSuffix(suffixToken, vinPatternId, chromeStyleId ?? -1));
            CurrentSuffixCount++;
        }

        private void SetCurrentPrefixToken(int prefixToken)
        {
            CurrentPrefixToken = prefixToken;
            CurrentSuffixIndex = VinPatternSuffixList.Count;
            CurrentSuffixCount = 0;
        }

        private void CommitCurrentPrefix()
        {
            if (CurrentPrefixToken == -1)
                return;

            VinPatternLookup.Add(CurrentPrefixToken,
                                 new VinPatternSuffixes(CurrentSuffixIndex, CurrentSuffixCount));
            
            CurrentPrefixToken = -1;
        }

        private int GetToken(char[] str)
        {
            var tokenLookup = str.Length == 9 ? Tokens9 : Tokens7;

            CopyCharArrayToUppercaseByteArray(str);

            var key = new VinPart(TokenBuffer, 0);

            int token;
            if (tokenLookup.TryGetValue(key, out token))
                return token;
            
            token = Buffer.Count;
            Buffer.AddRange(TokenBuffer.Take(str.Length));
            key = new VinPart(Buffer, token);
            tokenLookup.Add(key, token);

            return token;
        }

        private void CopyCharArrayToUppercaseByteArray(char[] str)
        {
            for (var i = 0; i < str.Length; i++)
                TokenBuffer[i] = (byte) char.ToUpperInvariant(str[i]);
        }

        public IChromeMapData GetData()
        {
            CommitCurrentPrefix();
            var vinPatternBuffer = Buffer.ToArray();
            return new Data
                       {
                           VehicleCatalogLookup = VehicleCatalogIdToChromeStyleIdMap,
                           VinPatternBuffer = vinPatternBuffer,
                           VinPatternSuffixList = VinPatternSuffixList.ToArray(),
                           VinPatternLookup =
                               VinPatternLookup.ToDictionary(kv => new VinPart(vinPatternBuffer, kv.Key), kv => kv.Value,
                                                             new VinPartComparer(9))
                       };
        }
    }
}