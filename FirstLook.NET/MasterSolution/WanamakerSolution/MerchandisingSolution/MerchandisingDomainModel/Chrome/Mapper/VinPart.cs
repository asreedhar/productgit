using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal struct VinPart
    {
        public IList<byte> Buffer;
        public int Offset;

        public VinPart(IList<byte> buffer, int offset) : this()
        {
            Buffer = buffer;
            Offset = offset;
        }
    }
}