namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal struct VinPatternMatch
    {
        public int ChromeStyleId { get; private set; }
        public int VinPatternId { get; private set; }
        public int Rank { get; private set; }

        public VinPatternMatch(int chromeStyleId, int vinPatternId, int rank) : this()
        {
            ChromeStyleId = chromeStyleId;
            VinPatternId = vinPatternId;
            Rank = rank;
        }
    }
}