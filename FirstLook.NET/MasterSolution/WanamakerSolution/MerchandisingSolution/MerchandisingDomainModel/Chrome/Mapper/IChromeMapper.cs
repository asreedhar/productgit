﻿namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    public interface IChromeMapper
    {
        int? LookupChromeStyleIdByVehicleCatalogId(int? vehicleCatalogId);
        int? LookupChromeStyleIdByVin(string vin);
        int? LookupVinPatternIdByVin(string vin);
    }
}