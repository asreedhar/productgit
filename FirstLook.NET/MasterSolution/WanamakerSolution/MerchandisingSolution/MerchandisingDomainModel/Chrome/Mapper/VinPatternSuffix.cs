namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal struct VinPatternSuffix
    {
        public int VinPatternSuffixOffset { get; private set; }
        public int VinPatternId { get; private set; }
        public int ChromeStyleId { get; private set; }

        public VinPatternSuffix(int vinPatternSuffixOffset, int vinPatternId, int chromeStyleId) : this()
        {
            VinPatternSuffixOffset = vinPatternSuffixOffset;
            VinPatternId = vinPatternId;
            ChromeStyleId = chromeStyleId;
        }
    }
}