using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal class ChromeMapCachedRepository : IChromeMapRepository
    {
        private static readonly string CacheKey = typeof (ChromeMapCachedRepository).FullName;
        private const int TimeoutInSeconds = 30*60;

        private IMemoryCache Cache { get; set; }
        private IChromeMapRepository Inner { get; set; }

        public ChromeMapCachedRepository(IMemoryCache cache, IChromeMapRepository inner)
        {
            Cache = cache;
            Inner = inner;
        }

        public IChromeMapData GetMapperData()
        {
            var data = (IChromeMapData) Cache.Get(CacheKey);
            if (data != null) return data;

            data = Inner.GetMapperData();
            Cache.Set(CacheKey, data, TimeoutInSeconds);

            return data;
        }
    }
}