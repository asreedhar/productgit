using System.Collections.Generic;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal class ChromeMapper : IChromeMapper
    {
        private IChromeMapRepository Repository { get; set; }

        public ChromeMapper(IChromeMapRepository repository)
        {
            Repository = repository;
        }

        public int? LookupChromeStyleIdByVehicleCatalogId(int? vehicleCatalogId)
        {
            if (vehicleCatalogId == null)
                return null;

            var data = Repository.GetMapperData();

            int chromeStyleId;
            if (!data.VehicleCatalogLookup.TryGetValue(vehicleCatalogId.Value, out chromeStyleId))
                return null;

            return chromeStyleId;
        }

        public int? LookupChromeStyleIdByVin(string vin)
        {
            return VinMatch(vin)
                .Where(r => r.ChromeStyleId != -1)
                .Select(r => (int?)r.ChromeStyleId)
                .FirstOrDefault();
        }

        public int? LookupVinPatternIdByVin(string vin)
        {
            return VinMatch(vin)
                .Select(r => (int?) r.VinPatternId)
                .FirstOrDefault();
        }

        private static readonly VinPatternMatch[] EmptyVinMatchArray = new VinPatternMatch[0];

        private IEnumerable<VinPatternMatch> VinMatch(string vin)
        {
            if (vin == null || vin.Length != 17)
                return EmptyVinMatchArray;

            var data = Repository.GetMapperData();

            var vinBuffer = ConvertVinToByteBuffer(vin);
            var vinPrefix = new VinPart(vinBuffer, 0);
            var vinSuffix = new VinPart(vinBuffer, 9);
            
            VinPatternSuffixes matchingSuffixes;
            if (!data.VinPatternLookup.TryGetValue(vinPrefix, out matchingSuffixes))
                return EmptyVinMatchArray;

            var vinMatchRankings = Enumerable
                .Range(matchingSuffixes.VinPatternSuffixListIndex, matchingSuffixes.VinPatternSuffixListCount)
                .Select(i =>
                            {
                                var suffix = data.VinPatternSuffixList[i];
                                var bufferSuffix = new VinPart(data.VinPatternBuffer, suffix.VinPatternSuffixOffset);
                                return new VinPatternMatch(suffix.ChromeStyleId, suffix.VinPatternId, CalculateRank(vinSuffix, bufferSuffix));
                            })
                .Where(vinMatch => vinMatch.Rank >= 0)
                .OrderByDescending(vinMatch => vinMatch.Rank).ThenBy(vinMatch => vinMatch.VinPatternId)
                .ToList();

            if (vinMatchRankings.Count <= 0)
                return EmptyVinMatchArray;

            var topRank = vinMatchRankings[0].Rank;

            return vinMatchRankings
                .TakeWhile(r => r.Rank == topRank);
        }

        private static byte[] ConvertVinToByteBuffer(string vin)
        {
            var buffer = new byte[16];
            int i;
            for (i = 0; i < 8; ++i) buffer[i] = (byte) char.ToUpperInvariant(vin[i]);
            for (i = 9; i < 17; ++i) buffer[i - 1] = (byte) char.ToUpperInvariant(vin[i]);
            return buffer;
        }

        private static int CalculateRank(VinPart vinToTest, VinPart vinPattern)
        {
            var rank = 0;
            for (var i = 0; i < 7; ++i)
            {
                var patternChar = vinPattern.Buffer[vinPattern.Offset + i];
                var vinChar = vinToTest.Buffer[vinToTest.Offset + i];

                if (patternChar == vinChar)
                    rank++;
                else if (patternChar != (byte)'*') // If the vin char doesn't match the pattern and the pattern char is not a wildcard then return -1 (No Match)
                    return -1;
            }

            return rank;
        }
    }
}