﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal class VinPartComparer : IEqualityComparer<VinPart>
    {
        public int PartLength { get; private set; }

        public VinPartComparer(int partLength)
        {
            PartLength = partLength;
        }

        public bool Equals(VinPart x, VinPart y)
        {
            var x_data = x.Buffer;
            var x_off = x.Offset;
            var x_stop = x_off + PartLength;
            var y_data = y.Buffer;
            var y_off = y.Offset;

            for (; x_off < x_stop; ++x_off, ++y_off)
                if (x_data[x_off] != y_data[y_off])
                    return false;

            return true;
        }

        public int GetHashCode(VinPart vpk)
        {
            var data = vpk.Buffer;
            unchecked
            {
                var result = 0;
                var stop = vpk.Offset + PartLength;
                for (var i = vpk.Offset; i < stop; i += 4)
                {
                    int accum = data[i];
                    if (i + 1 < stop) accum |= data[i + 1] << 8;
                    if (i + 2 < stop) accum |= data[i + 2] << 16;
                    if (i + 3 < stop) accum |= data[i + 3] << 24;
                    result = result*337 ^ accum;
                }
                return result;
            }
        }
    }
}