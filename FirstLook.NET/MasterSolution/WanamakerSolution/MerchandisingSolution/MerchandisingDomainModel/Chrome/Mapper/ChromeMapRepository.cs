using System;
using System.Data;
using FirstLook.Common.Core.Logging;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal class ChromeMapRepository : IChromeMapRepository
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<ChromeMapRepository>();

        public IChromeMapData GetMapperData()
        {
            using (MiniProfiler.Current.Step("ChromeMapRepository.GetMapperData()"))
            {
                if(Log.IsInfoEnabled)
                    Log.Info("Starting to execute Chrome.MappingData#Fetch");

                var t = DateTimeOffset.UtcNow;

                var accumulator = new ChromeMapDataAccumulator();

                ReadFromDatabase(accumulator);

                var data = accumulator.GetData();

                if(Log.IsInfoEnabled)
                    Log.InfoFormat(
                        "Completed packaging data from Chrome.MappingData#Fetch. ElapsedMs={0:0.0}, " +
                        "VinPatterBufferBytes={1}, VinPatternSuffixListEntries={2}, VinPatternLookupEntries={3}, VehicleCatalogLookupEntries={4}",
                        (DateTimeOffset.UtcNow - t).TotalMilliseconds,
                        data.VinPatternBuffer.Length, data.VinPatternSuffixList.Length, data.VinPatternLookup.Count,
                        data.VehicleCatalogLookup.Count);

                return data;
            }
        }

        private static void ReadFromDatabase(ChromeMapDataAccumulator accumulator)
        {
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Merchandising.Chrome.MappingData#Fetch";

                using (var r = cmd.ExecuteReader())
                {
                    ReadVehicleCatalogResultSet(accumulator, r);
                    ReadVinPatternResultSet(accumulator, r);
                }
            }
        }

        private static void ReadVinPatternResultSet(ChromeMapDataAccumulator accumulator, IDataReader r)
        {
            if (!r.NextResult())
                throw new InvalidOperationException("Insufficient result sets detected.");

            var prefixField = r.GetOrdinal("Prefix");
            var suffixField = r.GetOrdinal("Suffix");
            var chromeStyleIdField = r.GetOrdinal("ChromeStyleId");
            var vinPatternIdField = r.GetOrdinal("VINPatternId");

            var prefix = new char[9];
            var suffix = new char[7];

            while (r.Read())
            {
                r.GetChars(prefixField, 0, prefix, 0, 9);
                r.GetChars(suffixField, 0, suffix, 0, 7);

                accumulator.AccumulateVinPatternEntry(
                    prefix, suffix,
                    r.IsDBNull(chromeStyleIdField) ? (int?) null : r.GetInt32(chromeStyleIdField),
                    r.GetInt32(vinPatternIdField));
            }
        }

        private static void ReadVehicleCatalogResultSet(ChromeMapDataAccumulator accumulator, IDataReader r)
        {
            var vehicleCatalogIdField = r.GetOrdinal("VehicleCatalogId");
            var chromeStyleIdField = r.GetOrdinal("ChromeStyleId");

            while (r.Read())
            {
                accumulator.AccumulateVehicleCatalogEntry(
                    r.GetInt32(vehicleCatalogIdField),
                    r.GetInt32(chromeStyleIdField));
            }
        }
    }
}