namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal struct VinPatternSuffixes
    {
        public int VinPatternSuffixListIndex { get; private set; }
        public int VinPatternSuffixListCount { get; private set; }

        public VinPatternSuffixes(int vinPatternSuffixListIndex, int vinPatternSuffixListCount) : this()
        {
            VinPatternSuffixListIndex = vinPatternSuffixListIndex;
            VinPatternSuffixListCount = vinPatternSuffixListCount;
        }
    }
}