using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal interface IChromeMapData
    {
        IDictionary<VinPart, VinPatternSuffixes> VinPatternLookup { get; }
        VinPatternSuffix[] VinPatternSuffixList { get; }
        byte[] VinPatternBuffer { get; }
        IDictionary<int, int> VehicleCatalogLookup { get; }
    }
}