namespace FirstLook.Merchandising.DomainModel.Chrome.Mapper
{
    internal interface IChromeMapRepository
    {
        IChromeMapData GetMapperData();
    }
}