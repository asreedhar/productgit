﻿using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public class SharedChromeDataRepository : ISharedChromeDataRepository
    {
        public SharedChromeData GetData()
        {
            return SharedChromeData.Read();
        }
    }
}