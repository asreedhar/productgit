﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public interface IVinPattern
    {
        int VINPatternID { get; }
        string VINPattern { get; }
        int? Year { get; }
        string VINDivisionName { get; }
        string VINModelName { get; }
        string VINStyleName { get; }
        int? EngineTypeCategoryID { get; }
        string EngineSize { get; }
        string EngineCID { get; }
        int? FuelTypeCategoryID { get; }
        int? ForcedInductionCategoryID { get; }
        int? TransmissionTypeCategoryID { get; }
        string ManualTransAvail { get; }
        string AutoTransAvail { get; }
        string GVWRRange { get; }
        IList<int> ChromeStyleIds { get; }
    }
}