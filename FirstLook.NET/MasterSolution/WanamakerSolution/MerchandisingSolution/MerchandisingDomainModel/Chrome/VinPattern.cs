using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    internal class VinPattern : IVinPattern
    {
        public VinPattern(int vinPatternId, 
            string vinPattern, int? year, string vinDivisionName, string vinModelName, string vinStyleName,
            int? engineTypeCategoryId, string engineSize, string engineCid, 
            int? fuelTypeCategoryId, int? forcedInductionCategoryId, int? transmissionTypeCategoryId, 
            string manualTransAvail, string autoTransAvail, string gvwrRange, 
            IList<int> chromeStyleIds)
        {
            VINPatternID = vinPatternId;
            VINPattern = vinPattern;
            Year = year;
            VINDivisionName = vinDivisionName;
            VINModelName = vinModelName;
            VINStyleName = vinStyleName;
            EngineTypeCategoryID = engineTypeCategoryId;
            EngineSize = engineSize;
            EngineCID = engineCid;
            FuelTypeCategoryID = fuelTypeCategoryId;
            ForcedInductionCategoryID = forcedInductionCategoryId;
            TransmissionTypeCategoryID = transmissionTypeCategoryId;
            ManualTransAvail = manualTransAvail;
            AutoTransAvail = autoTransAvail;
            GVWRRange = gvwrRange;
            ChromeStyleIds = chromeStyleIds;
        }

        public int VINPatternID { get; private set; }
        public string VINPattern { get; private set; }
        public int? Year { get; private set; }
        public string VINDivisionName { get; private set; }
        public string VINModelName { get; private set; }
        public string VINStyleName { get; private set; }
        public int? EngineTypeCategoryID { get; private set; }
        public string EngineSize { get; private set; }
        public string EngineCID { get; private set; }
        public int? FuelTypeCategoryID { get; private set; }
        public int? ForcedInductionCategoryID { get; private set; }
        public int? TransmissionTypeCategoryID { get; private set; }
        public string ManualTransAvail { get; private set; }
        public string AutoTransAvail { get; private set; }
        public string GVWRRange { get; private set; }
        public IList<int> ChromeStyleIds { get; private set; }
    }
}