﻿using System.Linq;
using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public static class Extensions
    {
        public static IVinPattern GetVinPatternById(this IVinPatternRepository repository, int id)
        {
            return repository.GetVinPatternById(new[] {id}).SingleOrDefault();
        }

        public static Style GetStyleById(this IStyleRepository styleRepository, int styleId)
        {
            return styleRepository.GetStyleById(new[] {styleId}).FirstOrDefault();
        }
    }
}