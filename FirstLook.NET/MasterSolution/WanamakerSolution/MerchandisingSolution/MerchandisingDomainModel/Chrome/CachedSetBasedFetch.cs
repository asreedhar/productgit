using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public abstract class CachedSetBasedFetch
    {
        protected static readonly ILog Log = LoggerFactory.GetLogger<CachedSetBasedFetch>();
    }

    public class CachedSetBasedFetch<T> : CachedSetBasedFetch where T : class
    {
        private readonly ICache _cache;
        private readonly int _cacheRetentionInSeconds;
        private readonly string _cacheKeyPrefix;
        private readonly Func<IEnumerable<int>, IEnumerable<T>> _fetchFromInnerFunc;
        private readonly Func<T, int> _getIdFromItemFunc;

        public CachedSetBasedFetch(ICache cache, int cacheRetentionInSeconds, string cacheKeyPrefix, 
                                   Func<IEnumerable<int>, IEnumerable<T>> fetchFromInnerFunc,
                                   Func<T, int> getIdFromItemFunc)
        {
            _cache = cache;
            _cacheRetentionInSeconds = cacheRetentionInSeconds;
            _cacheKeyPrefix = cacheKeyPrefix;
            _fetchFromInnerFunc = fetchFromInnerFunc;
            _getIdFromItemFunc = getIdFromItemFunc;
        }

        public IEnumerable<T> GetById(IEnumerable<int> ids)
        {
            var idsToFetch = new List<int>();

            var itemsFromCache = 0;
            var itemsFromInner = 0;

            using (Log.StartLoggedScopeStopwatch(LogLevel.Info,
                log => log.Debug("Starting to fetch from ICache in GetById."),
                (log, t) => log.DebugFormat("Finished fetching from ICache in GetById. elapsedMs={0:0.0} itemType={1}", t.TotalMilliseconds,  typeof(T).FullName)))
            {
                foreach (var id in ids.Distinct())
                {
                    var item = FetchFromCache(id);
                    if (item == null)
                    {
                        idsToFetch.Add(id);
                    }
                    else
                    {
                        itemsFromCache++;
                        yield return item;
                    }
                }
            }

            if(Log.IsInfoEnabled)
                Log.InfoFormat("Fetched items from ICache in GetById. fetchCount={0}", itemsFromCache);

            if (idsToFetch.Count <= 0) yield break;

            using (Log.StartLoggedScopeStopwatch(LogLevel.Info,
                log => log.Info("Starting to tech from Inner in GetById."),
                (log, t) => log.InfoFormat("Finished fetching from Inner in GetById. elapsedMs={0:0.0} itemType={1}", t.TotalMilliseconds, typeof(T).FullName)))
            {
                foreach (var item in _fetchFromInnerFunc(idsToFetch))
                {
                    itemsFromInner++;
                    AddToCache(item);
                    yield return item;
                }
            }

            if(Log.IsInfoEnabled)
                Log.InfoFormat("Fetched items from Inner in GetById. fetchCount={0}", itemsFromInner);
        }

        private void AddToCache(T item)
        {
            _cache.Set(MakeCacheKey(_getIdFromItemFunc(item)), item, _cacheRetentionInSeconds);
        }

        private T FetchFromCache(int id)
        {
            return (T) _cache.Get(MakeCacheKey(id));
        }

        private string MakeCacheKey(int id)
        {
            return _cacheKeyPrefix + id.ToString(CultureInfo.InvariantCulture);
        }
    }
}