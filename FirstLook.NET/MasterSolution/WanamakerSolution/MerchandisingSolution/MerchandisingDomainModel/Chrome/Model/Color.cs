/* 
 * Chrome Color DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome Color DTO</summary>
    public partial class Color
    {
        public Color(
            byte countryCode = default(byte),
            int styleID = default(int),
            string ext1Code = default(string),
            string ext2Code = default(string),
            string intCode = default(string),
            string ext1ManCode = default(string),
            string ext2ManCode = default(string),
            string intManCode = default(string),
            string orderCode = default(string),
            string asTwoTone = default(string),
            string ext1Desc = default(string),
            string ext2Desc = default(string),
            string intDesc = default(string),
            string condition = default(string),
            string genericExtColor = default(string),
            string genericExt2Color = default(string),
            string ext1RGBHex = default(string),
            string ext2RGBHex = default(string),
            string ext1MfrFullCode = default(string),
            string ext2MfrFullCode = default(string))
        {
            CountryCode = countryCode;
            StyleID = styleID;
            Ext1Code = ext1Code;
            Ext2Code = ext2Code;
            IntCode = intCode;
            Ext1ManCode = ext1ManCode;
            Ext2ManCode = ext2ManCode;
            IntManCode = intManCode;
            OrderCode = orderCode;
            AsTwoTone = asTwoTone;
            Ext1Desc = ext1Desc;
            Ext2Desc = ext2Desc;
            IntDesc = intDesc;
            Condition = condition;
            GenericExtColor = genericExtColor;
            GenericExt2Color = genericExt2Color;
            Ext1RGBHex = ext1RGBHex;
            Ext2RGBHex = ext2RGBHex;
            Ext1MfrFullCode = ext1MfrFullCode;
            Ext2MfrFullCode = ext2MfrFullCode;
        }
    
        public byte CountryCode { get; private set; }
        public int StyleID { get; private set; }
        public string Ext1Code { get; private set; }
        public string Ext2Code { get; private set; }
        public string IntCode { get; private set; }
        public string Ext1ManCode { get; private set; }
        public string Ext2ManCode { get; private set; }
        public string IntManCode { get; private set; }
        public string OrderCode { get; private set; }
        public string AsTwoTone { get; private set; }
        public string Ext1Desc { get; private set; }
        public string Ext2Desc { get; private set; }
        public string IntDesc { get; private set; }
        public string Condition { get; private set; }
        public string GenericExtColor { get; private set; }
        public string GenericExt2Color { get; private set; }
        public string Ext1RGBHex { get; private set; }
        public string Ext2RGBHex { get; private set; }
        public string Ext1MfrFullCode { get; private set; }
        public string Ext2MfrFullCode { get; private set; }
    }
}
