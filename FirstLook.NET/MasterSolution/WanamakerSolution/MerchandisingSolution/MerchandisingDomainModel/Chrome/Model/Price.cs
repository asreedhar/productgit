/* 
 * Chrome Price DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome Price DTO</summary>
    public partial class Price
    {
        public Price(
            byte countryCode = default(byte),
            int styleID = default(int),
            int sequence = default(int),
            string optionCode = default(string),
            string priceRuleDesc = default(string),
            string condition = default(string),
            Decimal? invoice = default(Decimal?),
            Decimal? msrp = default(Decimal?),
            string priceState = default(string))
        {
            CountryCode = countryCode;
            StyleID = styleID;
            Sequence = sequence;
            OptionCode = optionCode;
            PriceRuleDesc = priceRuleDesc;
            Condition = condition;
            Invoice = invoice;
            MSRP = msrp;
            PriceState = priceState;
        }
    
        public byte CountryCode { get; private set; }
        public int StyleID { get; private set; }
        public int Sequence { get; private set; }
        public string OptionCode { get; private set; }
        public string PriceRuleDesc { get; private set; }
        public string Condition { get; private set; }
        public Decimal? Invoice { get; private set; }
        public Decimal? MSRP { get; private set; }
        public string PriceState { get; private set; }
    }
}
