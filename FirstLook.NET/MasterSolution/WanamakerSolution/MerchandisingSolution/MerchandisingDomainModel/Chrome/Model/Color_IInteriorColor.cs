﻿namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    public partial class Color : IInteriorColor
    {
        string IInteriorColor.Code
        {
            get { return IntCode; }
        }

        string IInteriorColor.MfgCode
        {
            get { return IntManCode; }
        }

        string IInteriorColor.Desc
        {
            get { return IntDesc; }
        }
    }
}