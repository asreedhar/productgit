/* 
 * Chrome Standard DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome Standard DTO</summary>
    public partial class Standard
    {
        public Standard(
            byte countryCode = default(byte),
            int styleID = default(int),
            int headerID = default(int),
            int sequence = default(int),
            string _standard = default(string),
            string categoryList = default(string),
            string header = default(string))
        {
            CountryCode = countryCode;
            StyleID = styleID;
            HeaderID = headerID;
            Sequence = sequence;
            _Standard = _standard;
            CategoryList = categoryList;
            Header = header;
        }
    
        public byte CountryCode { get; private set; }
        public int StyleID { get; private set; }
        public int HeaderID { get; private set; }
        public int Sequence { get; private set; }
        public string _Standard { get; private set; }
        public string CategoryList { get; private set; }
        public string Header { get; private set; }
    }
}
