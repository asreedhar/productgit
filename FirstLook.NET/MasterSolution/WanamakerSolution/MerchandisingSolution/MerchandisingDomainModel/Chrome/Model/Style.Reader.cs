/* 
 * Chrome  DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    public partial class Style
    {
        private class ReadContext
        {
            public ReadContext(IDataReader r)
            {
                _reader = r;
            }
        
            private readonly IDictionary<string, string> _strings = new Dictionary<string, string>(StringComparer.Ordinal);
            private readonly IDataReader _reader;
        
            private ILookup<int, Option> _option;
            private IDictionary<int, Model> _model;
            private ILookup<int, Standard> _standard;
            private ILookup<int, StyleCat> _styleCat;
            private ILookup<int, StyleGenericEquipment> _styleGenericEquipment;
            private ILookup<int, Color> _color;
            private ILookup<int, Price> _price;
            private IList<Style> _style;
        
            public IList<Style> ReadResults()
            {
                ReadOption();
                MoveToNextResultSet();
                ReadModel();
                MoveToNextResultSet();
                ReadStandard();
                MoveToNextResultSet();
                ReadStyleCat();
                MoveToNextResultSet();
                ReadStyleGenericEquipment();
                MoveToNextResultSet();
                ReadColor();
                MoveToNextResultSet();
                ReadPrice();
                MoveToNextResultSet();
                ReadStyle();
                
                return _style;
            }
            
            private void ReadOption()
            {
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var styleIDField = _reader.GetOrdinal("StyleID");
                var headerIDField = _reader.GetOrdinal("HeaderID");
                var sequenceField = _reader.GetOrdinal("Sequence");
                var optionCodeField = _reader.GetOrdinal("OptionCode");
                var optionDescField = _reader.GetOrdinal("OptionDesc");
                var optionKindIDField = _reader.GetOrdinal("OptionKindID");
                var categoryListField = _reader.GetOrdinal("CategoryList");
                var typeFilterField = _reader.GetOrdinal("TypeFilter");
                var availabilityField = _reader.GetOrdinal("Availability");
                var ponField = _reader.GetOrdinal("PON");
                var extDescriptionField = _reader.GetOrdinal("ExtDescription");
                var supportedLogicField = _reader.GetOrdinal("SupportedLogic");
                var unsupportedLogicField = _reader.GetOrdinal("UnsupportedLogic");
                var priceNotesField = _reader.GetOrdinal("PriceNotes");
                var headerField = _reader.GetOrdinal("Header");
                var optionKindField = _reader.GetOrdinal("OptionKind");
                
                var list = new List<Option>();
                
                while(_reader.Read())
                {
                    if(_reader.IsDBNull(styleIDField))
                        continue;
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new Option(
                        _reader.GetByte(countryCodeField),
                        styleId,
                        _reader.IsDBNull(headerIDField) ? (int?)null : _reader.GetInt32(headerIDField),
                        _reader.IsDBNull(sequenceField) ? (int?)null : _reader.GetInt32(sequenceField),
                        _reader.IsDBNull(optionCodeField) ? null : Intern(_reader.GetString(optionCodeField)),
                        _reader.IsDBNull(optionDescField) ? null : Intern(_reader.GetString(optionDescField)),
                        _reader.IsDBNull(optionKindIDField) ? (int?)null : _reader.GetInt32(optionKindIDField),
                        _reader.IsDBNull(categoryListField) ? null : Intern(_reader.GetString(categoryListField)),
                        _reader.IsDBNull(typeFilterField) ? null : Intern(_reader.GetString(typeFilterField)),
                        _reader.IsDBNull(availabilityField) ? null : Intern(_reader.GetString(availabilityField)),
                        _reader.IsDBNull(ponField) ? null : Intern(_reader.GetString(ponField)),
                        _reader.IsDBNull(extDescriptionField) ? null : Intern(_reader.GetString(extDescriptionField)),
                        _reader.IsDBNull(supportedLogicField) ? null : Intern(_reader.GetString(supportedLogicField)),
                        _reader.IsDBNull(unsupportedLogicField) ? null : Intern(_reader.GetString(unsupportedLogicField)),
                        _reader.IsDBNull(priceNotesField) ? null : Intern(_reader.GetString(priceNotesField)),
                        _reader.IsDBNull(headerField) ? null : Intern(_reader.GetString(headerField)),
                        _reader.IsDBNull(optionKindField) ? null : Intern(_reader.GetString(optionKindField))
                        ));
                }
                
                _option = list.Where(i => i.StyleID.HasValue).ToLookup(i => i.StyleID.GetValueOrDefault());
            }

            private void ReadModel()
            {
                var styleIDField = _reader.GetOrdinal("StyleID");
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var modelIDField = _reader.GetOrdinal("ModelID");
                var histModelIDField = _reader.GetOrdinal("HistModelID");
                var modelYearField = _reader.GetOrdinal("ModelYear");
                var divisionIDField = _reader.GetOrdinal("DivisionID");
                var subdivisionIDField = _reader.GetOrdinal("SubdivisionID");
                var modelNameField = _reader.GetOrdinal("ModelName");
                var effectiveDateField = _reader.GetOrdinal("EffectiveDate");
                var modelCommentField = _reader.GetOrdinal("ModelComment");
                var availabilityField = _reader.GetOrdinal("Availability");
                var histSubdivisionIDField = _reader.GetOrdinal("HistSubdivisionID");
                var subdivisionNameField = _reader.GetOrdinal("SubdivisionName");
                var manufacturerIDField = _reader.GetOrdinal("ManufacturerID");
                var divisionNameField = _reader.GetOrdinal("DivisionName");
                var manufacturerNameField = _reader.GetOrdinal("ManufacturerName");
                
                var list = new List<Model>();
                
                while(_reader.Read())
                {
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new Model(
                        styleId,
                        _reader.GetByte(countryCodeField),
                        _reader.GetInt32(modelIDField),
                        _reader.IsDBNull(histModelIDField) ? (int?)null : _reader.GetInt32(histModelIDField),
                        _reader.IsDBNull(modelYearField) ? (int?)null : _reader.GetInt32(modelYearField),
                        _reader.IsDBNull(divisionIDField) ? (int?)null : _reader.GetInt32(divisionIDField),
                        _reader.IsDBNull(subdivisionIDField) ? (int?)null : _reader.GetInt32(subdivisionIDField),
                        _reader.IsDBNull(modelNameField) ? null : Intern(_reader.GetString(modelNameField)),
                        _reader.IsDBNull(effectiveDateField) ? (DateTime?)null : _reader.GetDateTime(effectiveDateField),
                        _reader.IsDBNull(modelCommentField) ? null : Intern(_reader.GetString(modelCommentField)),
                        _reader.IsDBNull(availabilityField) ? null : Intern(_reader.GetString(availabilityField)),
                        _reader.IsDBNull(histSubdivisionIDField) ? (int?)null : _reader.GetInt32(histSubdivisionIDField),
                        _reader.IsDBNull(subdivisionNameField) ? null : Intern(_reader.GetString(subdivisionNameField)),
                        _reader.GetInt32(manufacturerIDField),
                        Intern(_reader.GetString(divisionNameField)),
                        _reader.IsDBNull(manufacturerNameField) ? null : Intern(_reader.GetString(manufacturerNameField))
                        ));
                }
                
                _model = list.ToDictionary(i => i.StyleID);
            }

            private void ReadStandard()
            {
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var styleIDField = _reader.GetOrdinal("StyleID");
                var headerIDField = _reader.GetOrdinal("HeaderID");
                var sequenceField = _reader.GetOrdinal("Sequence");
                var _standardField = _reader.GetOrdinal("Standard");
                var categoryListField = _reader.GetOrdinal("CategoryList");
                var headerField = _reader.GetOrdinal("Header");
                
                var list = new List<Standard>();
                
                while(_reader.Read())
                {
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new Standard(
                        _reader.GetByte(countryCodeField),
                        styleId,
                        _reader.GetInt32(headerIDField),
                        _reader.GetInt32(sequenceField),
                        _reader.IsDBNull(_standardField) ? null : Intern(_reader.GetString(_standardField)),
                        _reader.IsDBNull(categoryListField) ? null : Intern(_reader.GetString(categoryListField)),
                        _reader.IsDBNull(headerField) ? null : Intern(_reader.GetString(headerField))
                        ));
                }
                
                _standard = list.ToLookup(i => i.StyleID);
            }

            private void ReadStyleCat()
            {
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var styleIDField = _reader.GetOrdinal("StyleID");
                var categoryIDField = _reader.GetOrdinal("CategoryID");
                var featureTypeField = _reader.GetOrdinal("FeatureType");
                var sequenceField = _reader.GetOrdinal("Sequence");
                var stateField = _reader.GetOrdinal("State");
                
                var list = new List<StyleCat>();
                
                while(_reader.Read())
                {
                    if(_reader.IsDBNull(styleIDField))
                        continue;
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new StyleCat(
                        _reader.GetByte(countryCodeField),
                        styleId,
                        _reader.IsDBNull(categoryIDField) ? (int?)null : _reader.GetInt32(categoryIDField),
                        _reader.IsDBNull(featureTypeField) ? null : Intern(_reader.GetString(featureTypeField)),
                        _reader.IsDBNull(sequenceField) ? (int?)null : _reader.GetInt32(sequenceField),
                        _reader.IsDBNull(stateField) ? null : Intern(_reader.GetString(stateField))
                        ));
                }
                
                _styleCat = list.Where(i => i.StyleID.HasValue).ToLookup(i => i.StyleID.GetValueOrDefault());
            }

            private void ReadStyleGenericEquipment()
            {
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var styleIDField = _reader.GetOrdinal("ChromeStyleID");
                var categoryIDField = _reader.GetOrdinal("CategoryID");
                var styleAvailabilityField = _reader.GetOrdinal("StyleAvailability");
                
                var list = new List<StyleGenericEquipment>();
                
                while(_reader.Read())
                {
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new StyleGenericEquipment(
                        _reader.GetByte(countryCodeField),
                        _reader.GetInt32(styleIDField),
                        _reader.GetInt32(categoryIDField),
                        Intern(_reader.GetString(styleAvailabilityField))
                        ));
                }
                
                _styleGenericEquipment = list.ToLookup(i => i.StyleID);
            }

            private void ReadColor()
            {
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var styleIDField = _reader.GetOrdinal("StyleID");
                var ext1CodeField = _reader.GetOrdinal("Ext1Code");
                var ext2CodeField = _reader.GetOrdinal("Ext2Code");
                var intCodeField = _reader.GetOrdinal("IntCode");
                var ext1ManCodeField = _reader.GetOrdinal("Ext1ManCode");
                var ext2ManCodeField = _reader.GetOrdinal("Ext2ManCode");
                var intManCodeField = _reader.GetOrdinal("IntManCode");
                var orderCodeField = _reader.GetOrdinal("OrderCode");
                var asTwoToneField = _reader.GetOrdinal("AsTwoTone");
                var ext1DescField = _reader.GetOrdinal("Ext1Desc");
                var ext2DescField = _reader.GetOrdinal("Ext2Desc");
                var intDescField = _reader.GetOrdinal("IntDesc");
                var conditionField = _reader.GetOrdinal("Condition");
                var genericExtColorField = _reader.GetOrdinal("GenericExtColor");
                var genericExt2ColorField = _reader.GetOrdinal("GenericExt2Color");
                var ext1RGBHexField = _reader.GetOrdinal("Ext1RGBHex");
                var ext2RGBHexField = _reader.GetOrdinal("Ext2RGBHex");
                var ext1MfrFullCodeField = _reader.GetOrdinal("Ext1MfrFullCode");
                var ext2MfrFullCodeField = _reader.GetOrdinal("Ext2MfrFullCode");
                
                var list = new List<Color>();
                
                while(_reader.Read())
                {
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new Color(
                        _reader.GetByte(countryCodeField),
                        styleId,
                        _reader.IsDBNull(ext1CodeField) ? null : Intern(_reader.GetString(ext1CodeField)),
                        _reader.IsDBNull(ext2CodeField) ? null : Intern(_reader.GetString(ext2CodeField)),
                        _reader.IsDBNull(intCodeField) ? null : Intern(_reader.GetString(intCodeField)),
                        _reader.IsDBNull(ext1ManCodeField) ? null : Intern(_reader.GetString(ext1ManCodeField)),
                        _reader.IsDBNull(ext2ManCodeField) ? null : Intern(_reader.GetString(ext2ManCodeField)),
                        _reader.IsDBNull(intManCodeField) ? null : Intern(_reader.GetString(intManCodeField)),
                        _reader.IsDBNull(orderCodeField) ? null : Intern(_reader.GetString(orderCodeField)),
                        _reader.IsDBNull(asTwoToneField) ? null : Intern(_reader.GetString(asTwoToneField)),
                        _reader.IsDBNull(ext1DescField) ? null : Intern(_reader.GetString(ext1DescField)),
                        _reader.IsDBNull(ext2DescField) ? null : Intern(_reader.GetString(ext2DescField)),
                        _reader.IsDBNull(intDescField) ? null : Intern(_reader.GetString(intDescField)),
                        _reader.IsDBNull(conditionField) ? null : Intern(_reader.GetString(conditionField)),
                        _reader.IsDBNull(genericExtColorField) ? null : Intern(_reader.GetString(genericExtColorField)),
                        _reader.IsDBNull(genericExt2ColorField) ? null : Intern(_reader.GetString(genericExt2ColorField)),
                        _reader.IsDBNull(ext1RGBHexField) ? null : Intern(_reader.GetString(ext1RGBHexField)),
                        _reader.IsDBNull(ext2RGBHexField) ? null : Intern(_reader.GetString(ext2RGBHexField)),
                        _reader.IsDBNull(ext1MfrFullCodeField) ? null : Intern(_reader.GetString(ext1MfrFullCodeField)),
                        _reader.IsDBNull(ext2MfrFullCodeField) ? null : Intern(_reader.GetString(ext2MfrFullCodeField))
                        ));
                }
                
                _color = list.ToLookup(i => i.StyleID);
            }

            private void ReadPrice()
            {
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var styleIDField = _reader.GetOrdinal("StyleID");
                var sequenceField = _reader.GetOrdinal("Sequence");
                var optionCodeField = _reader.GetOrdinal("OptionCode");
                var priceRuleDescField = _reader.GetOrdinal("PriceRuleDesc");
                var conditionField = _reader.GetOrdinal("Condition");
                var invoiceField = _reader.GetOrdinal("Invoice");
                var msrpField = _reader.GetOrdinal("MSRP");
                var priceStateField = _reader.GetOrdinal("PriceState");
                
                var list = new List<Price>();
                
                while(_reader.Read())
                {
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new Price(
                        _reader.GetByte(countryCodeField),
                        styleId,
                        _reader.GetInt32(sequenceField),
                        _reader.IsDBNull(optionCodeField) ? null : Intern(_reader.GetString(optionCodeField)),
                        _reader.IsDBNull(priceRuleDescField) ? null : Intern(_reader.GetString(priceRuleDescField)),
                        _reader.IsDBNull(conditionField) ? null : Intern(_reader.GetString(conditionField)),
                        _reader.IsDBNull(invoiceField) ? (Decimal?)null : _reader.GetDecimal(invoiceField),
                        _reader.IsDBNull(msrpField) ? (Decimal?)null : _reader.GetDecimal(msrpField),
                        _reader.IsDBNull(priceStateField) ? null : Intern(_reader.GetString(priceStateField))
                        ));
                }
                
                _price = list.ToLookup(i => i.StyleID);
            }

            private void ReadStyle()
            {
                var countryCodeField = _reader.GetOrdinal("CountryCode");
                var styleIDField = _reader.GetOrdinal("StyleID");
                var histStyleIDField = _reader.GetOrdinal("HistStyleID");
                var modelIDField = _reader.GetOrdinal("ModelID");
                var modelYearField = _reader.GetOrdinal("ModelYear");
                var sequenceField = _reader.GetOrdinal("Sequence");
                var styleCodeField = _reader.GetOrdinal("StyleCode");
                var fullStyleCodeField = _reader.GetOrdinal("FullStyleCode");
                var styleNameField = _reader.GetOrdinal("StyleName");
                var trueBasePriceField = _reader.GetOrdinal("TrueBasePrice");
                var invoiceField = _reader.GetOrdinal("Invoice");
                var msrpField = _reader.GetOrdinal("MSRP");
                var destinationField = _reader.GetOrdinal("Destination");
                var styleCVCListField = _reader.GetOrdinal("StyleCVCList");
                var mktClassIDField = _reader.GetOrdinal("MktClassID");
                var styleNameWOTrimField = _reader.GetOrdinal("StyleNameWOTrim");
                var trimField = _reader.GetOrdinal("Trim");
                var passengerCapacityField = _reader.GetOrdinal("PassengerCapacity");
                var passengerDoorsField = _reader.GetOrdinal("PassengerDoors");
                var manualTransField = _reader.GetOrdinal("ManualTrans");
                var autoTransField = _reader.GetOrdinal("AutoTrans");
                var frontWDField = _reader.GetOrdinal("FrontWD");
                var rearWDField = _reader.GetOrdinal("RearWD");
                var allWDField = _reader.GetOrdinal("AllWD");
                var fourWDField = _reader.GetOrdinal("FourWD");
                var stepSideField = _reader.GetOrdinal("StepSide");
                var captionField = _reader.GetOrdinal("Caption");
                var availabilityField = _reader.GetOrdinal("Availability");
                var priceStateField = _reader.GetOrdinal("PriceState");
                var autoBuilderStyleIDField = _reader.GetOrdinal("AutoBuilderStyleID");
                var cFModelNameField = _reader.GetOrdinal("CFModelName");
                var cFStyleNameField = _reader.GetOrdinal("CFStyleName");
                var cFDrivetrainField = _reader.GetOrdinal("CFDrivetrain");
                var cFBodyTypeField = _reader.GetOrdinal("CFBodyType");
                
                var list = new List<Style>();
                
                while(_reader.Read())
                {
                    
                    var styleId = _reader.GetInt32(styleIDField);
                    
                    list.Add(new Style(
                        _reader.GetByte(countryCodeField),
                        styleId,
                        _reader.IsDBNull(histStyleIDField) ? (int?)null : _reader.GetInt32(histStyleIDField),
                        _reader.IsDBNull(modelIDField) ? (int?)null : _reader.GetInt32(modelIDField),
                        _reader.IsDBNull(modelYearField) ? (int?)null : _reader.GetInt32(modelYearField),
                        _reader.IsDBNull(sequenceField) ? (int?)null : _reader.GetInt32(sequenceField),
                        _reader.IsDBNull(styleCodeField) ? null : Intern(_reader.GetString(styleCodeField)),
                        _reader.IsDBNull(fullStyleCodeField) ? null : Intern(_reader.GetString(fullStyleCodeField)),
                        _reader.IsDBNull(styleNameField) ? null : Intern(_reader.GetString(styleNameField)),
                        _reader.IsDBNull(trueBasePriceField) ? null : Intern(_reader.GetString(trueBasePriceField)),
                        _reader.IsDBNull(invoiceField) ? (float?)null : _reader.GetFloat(invoiceField),
                        _reader.IsDBNull(msrpField) ? (float?)null : _reader.GetFloat(msrpField),
                        _reader.IsDBNull(destinationField) ? (float?)null : _reader.GetFloat(destinationField),
                        _reader.IsDBNull(styleCVCListField) ? null : Intern(_reader.GetString(styleCVCListField)),
                        _reader.IsDBNull(mktClassIDField) ? (byte?)null : _reader.GetByte(mktClassIDField),
                        _reader.IsDBNull(styleNameWOTrimField) ? null : Intern(_reader.GetString(styleNameWOTrimField)),
                        _reader.IsDBNull(trimField) ? null : Intern(_reader.GetString(trimField)),
                        _reader.IsDBNull(passengerCapacityField) ? (int?)null : _reader.GetInt32(passengerCapacityField),
                        _reader.IsDBNull(passengerDoorsField) ? (int?)null : _reader.GetInt32(passengerDoorsField),
                        _reader.IsDBNull(manualTransField) ? null : Intern(_reader.GetString(manualTransField)),
                        _reader.IsDBNull(autoTransField) ? null : Intern(_reader.GetString(autoTransField)),
                        _reader.IsDBNull(frontWDField) ? null : Intern(_reader.GetString(frontWDField)),
                        _reader.IsDBNull(rearWDField) ? null : Intern(_reader.GetString(rearWDField)),
                        _reader.IsDBNull(allWDField) ? null : Intern(_reader.GetString(allWDField)),
                        _reader.IsDBNull(fourWDField) ? null : Intern(_reader.GetString(fourWDField)),
                        _reader.IsDBNull(stepSideField) ? null : Intern(_reader.GetString(stepSideField)),
                        _reader.IsDBNull(captionField) ? null : Intern(_reader.GetString(captionField)),
                        _reader.IsDBNull(availabilityField) ? null : Intern(_reader.GetString(availabilityField)),
                        _reader.IsDBNull(priceStateField) ? null : Intern(_reader.GetString(priceStateField)),
                        _reader.IsDBNull(autoBuilderStyleIDField) ? null : Intern(_reader.GetString(autoBuilderStyleIDField)),
                        _reader.IsDBNull(cFModelNameField) ? null : Intern(_reader.GetString(cFModelNameField)),
                        _reader.IsDBNull(cFStyleNameField) ? null : Intern(_reader.GetString(cFStyleNameField)),
                        _reader.IsDBNull(cFDrivetrainField) ? null : Intern(_reader.GetString(cFDrivetrainField)),
                        _reader.IsDBNull(cFBodyTypeField) ? null : Intern(_reader.GetString(cFBodyTypeField)),
                        _color[styleId].ToList().AsReadOnly(),
                        GetDictionaryItem(_model, styleId),
                        _option[styleId].ToList().AsReadOnly(),
                        _price[styleId].ToList().AsReadOnly(),
                        _standard[styleId].ToList().AsReadOnly(),
                        _styleCat[styleId].ToList().AsReadOnly(),
                        _styleGenericEquipment[styleId].ToList().AsReadOnly()
                        ));
                }
                
                _style = list;
            }
        
            private string Intern(string val)
            {
                if (val == null)
                    return null;

                if(val.Length == 0)
                    return string.Empty;

                string interned;
                if(!_strings.TryGetValue(val, out interned))
                {
                    interned = val;
                    _strings.Add(interned, interned);
                }

                return interned;
            }
            
            private static T GetDictionaryItem<T>(IDictionary<int, T> items, int styleId)
            {
                T val;
                items.TryGetValue(styleId, out val);
                return val;
            }
            
            private void MoveToNextResultSet()
            {
                if(!_reader.NextResult())
                    throw new InvalidOperationException("Expected more result sets.");
            }
        }
    
        internal static IList<Style> ReadStyleById(IEnumerable<int> ids)
        {
            var xml = BuildXmlFromChromeStyleIds(ids);
        
            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandText = "Merchandising.Chrome.Style\x0023Fetch";
                cmd.CommandType = CommandType.StoredProcedure;
                
                AddVarcharMaxParameter(cmd, "@xml", xml);
                
                using(var r = cmd.ExecuteReader())
                {
                    return new ReadContext(r).ReadResults();
                }
            }
        }

        private static void AddVarcharMaxParameter(IDbCommand cmd, string name, string xml)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.DbType = DbType.AnsiString;
            p.Size = -1;
            p.Value = xml;
            cmd.Parameters.Add(p);
        }
        
        private static string BuildXmlFromChromeStyleIds(IEnumerable<int> chromeStyleIds)
        {
            var b = new StringBuilder();
            b.Append("<ids>");

            chromeStyleIds
                .Distinct()
                .OrderBy(id => id)
                .Aggregate(b, (builder, id) =>
                    {
                        builder.Append("<v>");
                        builder.Append(id.ToString(CultureInfo.InvariantCulture));
                        builder.Append("</v>");
                        return builder;
                    });

            b.Append("</ids>");
            return b.ToString();
        }
    }
}

