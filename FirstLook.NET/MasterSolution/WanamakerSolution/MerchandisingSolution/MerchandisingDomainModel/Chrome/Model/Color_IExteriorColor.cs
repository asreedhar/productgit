﻿namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    public partial class Color : IExteriorColor
    {
        string IExteriorColor.Ext1MfgCode
        {
            get { return Ext1ManCode; }
        }

        string IExteriorColor.Ext1Simple
        {
            get { return GenericExtColor; }
        }

        string IExteriorColor.Ext2MfgCode
        {
            get { return Ext2ManCode; }
        }

        string IExteriorColor.Ext2Simple
        {
            get { return GenericExt2Color; }
        }
    }
}