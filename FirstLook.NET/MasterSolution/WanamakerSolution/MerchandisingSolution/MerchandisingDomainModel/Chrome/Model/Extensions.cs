﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.VehicleVinData;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    public static class Extensions
    {
        public static string GetStyleDesc(this Style style)
        {
            var trim = !string.IsNullOrEmpty(style.Trim) 
                ? style.Trim 
                : "base";

            return !string.IsNullOrEmpty(style.StyleNameWOTrim)
                ? trim + " - " + style.StyleNameWOTrim
                : trim;
        }

        public static IEnumerable<IExteriorColor> GetExteriorColors(this Style style)
        {
            return style.Colors.Distinct(new ExteriorColorComparer());
        }

        public static IEnumerable<IInteriorColor> GetInteriorColors(this Style style)
        {
            return style.Colors.Distinct(new InteriorColorComparer());
        }

        public static IExteriorColor FirstOrDefault(this IEnumerable<IExteriorColor> colors, string ext1Code, string ext1Desc, string ext2Code, string ext2Desc)
        {
            ext1Code = ext1Code ?? "";
            ext1Desc = ext1Desc ?? "";
            ext2Code = ext2Code ?? "";
            ext2Desc = ext2Desc ?? "";
            IExteriorColor matchedByDesc = null;
            var cmp = StringComparer.InvariantCultureIgnoreCase;
            foreach(var color in colors)
            {
                if ((!string.IsNullOrEmpty(ext1Code) || !string.IsNullOrEmpty(ext2Code))
                        && cmp.Equals(GetCode(color.Ext1MfgCode, color.Ext1Code), ext1Code)
                        && cmp.Equals(GetCode(color.Ext2MfgCode, color.Ext2Code), ext2Code))
                    return color;

                if ((!string.IsNullOrEmpty(ext1Desc) || !string.IsNullOrEmpty(ext2Desc))
                        && cmp.Equals(color.Ext1Desc ?? "", ext1Desc) 
                        && cmp.Equals(color.Ext2Desc ?? "", ext2Desc))
                    matchedByDesc = matchedByDesc ?? color;
            }
            return matchedByDesc;
        }

        public static IInteriorColor FirstOrDefault(this IEnumerable<IInteriorColor> colors, string intCode, string intDesc)
        {
            var cmp = StringComparer.InvariantCultureIgnoreCase;
            IInteriorColor matchedByDesc = null;
            foreach(var c in colors)
            {
                if (!string.IsNullOrEmpty(intCode) && cmp.Equals(GetCode(c.MfgCode, c.Code), intCode))
                    return c;
                if (!string.IsNullOrEmpty(intDesc) && cmp.Equals(c.Desc, intDesc))
                    matchedByDesc = c;
            }
            return matchedByDesc;
        }

        private static string GetCode(string mfgCode, string code)
        {
            return string.IsNullOrEmpty(mfgCode) ? (code ?? "") : mfgCode;
        }

        public static PrimaryEquipmentConfiguration GetPrimaryEquipmentConfiguration(this Style style, SharedChromeData chromeData)
        {
            EquipmentObject engine_bucket = null,
                transmission_bucket = null,
                drivetrain_bucket = null,
                fuelsystem_bucket = null;

            var cmp = StringComparer.InvariantCultureIgnoreCase;
            foreach(var sge in style.StyleGenericEquipment.Where(s => cmp.Equals(s.StyleAvailability, "Standard")))
            {
                Category cat;
                if (!chromeData.Categories.TryGetValue(sge.CategoryID, out cat))
                    continue;

                if(cmp.Equals(cat.CategoryTypeFilter, "Engine"))
                {
                    engine_bucket = engine_bucket == null
                                        ? new EquipmentObject(sge.CategoryID)
                                        : EquipmentObject.NotSpecified;
                }

                if(cmp.Equals(cat.CategoryTypeFilter, "Transmission"))
                {
                    transmission_bucket = transmission_bucket == null
                                              ? new EquipmentObject(sge.CategoryID)
                                              : EquipmentObject.NotSpecified;
                }

                if(cmp.Equals(cat.CategoryTypeFilter, "Drivetrain"))
                {
                    drivetrain_bucket = drivetrain_bucket == null
                                            ? new EquipmentObject(sge.CategoryID)
                                            : EquipmentObject.NotSpecified;
                }

                if (cmp.Equals(cat.CategoryTypeFilter, "Fuel system"))
                {
                    fuelsystem_bucket = fuelsystem_bucket == null
                                            ? new EquipmentObject(sge.CategoryID)
                                            : EquipmentObject.NotSpecified;
                }
            }

            return new PrimaryEquipmentConfiguration(engine_bucket ?? EquipmentObject.NotSpecified,
                                                     transmission_bucket ?? EquipmentObject.NotSpecified,
                                                     drivetrain_bucket ?? EquipmentObject.NotSpecified,
                                                     fuelsystem_bucket ?? EquipmentObject.NotSpecified);
        }
    }
}