/* 
 * Chrome StyleCat DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome StyleCat DTO</summary>
    public partial class StyleCat
    {
        public StyleCat(
            byte countryCode = default(byte),
            int? styleID = default(int?),
            int? categoryID = default(int?),
            string featureType = default(string),
            int? sequence = default(int?),
            string state = default(string))
        {
            CountryCode = countryCode;
            StyleID = styleID;
            CategoryID = categoryID;
            FeatureType = featureType;
            Sequence = sequence;
            State = state;
        }
    
        public byte CountryCode { get; private set; }
        public int? StyleID { get; private set; }
        public int? CategoryID { get; private set; }
        public string FeatureType { get; private set; }
        public int? Sequence { get; private set; }
        public string State { get; private set; }
    }
}
