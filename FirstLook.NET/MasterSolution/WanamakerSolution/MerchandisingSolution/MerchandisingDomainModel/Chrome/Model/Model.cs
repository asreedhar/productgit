/* 
 * Chrome Model DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome Model DTO</summary>
    public partial class Model
    {
        public Model(
            int styleID = default(int),
            byte countryCode = default(byte),
            int modelID = default(int),
            int? histModelID = default(int?),
            int? modelYear = default(int?),
            int? divisionID = default(int?),
            int? subdivisionID = default(int?),
            string modelName = default(string),
            DateTime? effectiveDate = default(DateTime?),
            string modelComment = default(string),
            string availability = default(string),
            int? histSubdivisionID = default(int?),
            string subdivisionName = default(string),
            int manufacturerID = default(int),
            string divisionName = default(string),
            string manufacturerName = default(string))
        {
            StyleID = styleID;
            CountryCode = countryCode;
            ModelID = modelID;
            HistModelID = histModelID;
            ModelYear = modelYear;
            DivisionID = divisionID;
            SubdivisionID = subdivisionID;
            ModelName = modelName;
            EffectiveDate = effectiveDate;
            ModelComment = modelComment;
            Availability = availability;
            HistSubdivisionID = histSubdivisionID;
            SubdivisionName = subdivisionName;
            ManufacturerID = manufacturerID;
            DivisionName = divisionName;
            ManufacturerName = manufacturerName;
        }
    
        public int StyleID { get; private set; }
        public byte CountryCode { get; private set; }
        public int ModelID { get; private set; }
        public int? HistModelID { get; private set; }
        public int? ModelYear { get; private set; }
        public int? DivisionID { get; private set; }
        public int? SubdivisionID { get; private set; }
        public string ModelName { get; private set; }
        public DateTime? EffectiveDate { get; private set; }
        public string ModelComment { get; private set; }
        public string Availability { get; private set; }
        public int? HistSubdivisionID { get; private set; }
        public string SubdivisionName { get; private set; }
        public int ManufacturerID { get; private set; }
        public string DivisionName { get; private set; }
        public string ManufacturerName { get; private set; }
    }
}
