/* 
 * Chrome Style DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome Style DTO</summary>
    public partial class Style
    {
        public Style(
            byte countryCode = default(byte),
            int styleID = default(int),
            int? histStyleID = default(int?),
            int? modelID = default(int?),
            int? modelYear = default(int?),
            int? sequence = default(int?),
            string styleCode = default(string),
            string fullStyleCode = default(string),
            string styleName = default(string),
            string trueBasePrice = default(string),
            float? invoice = default(float?),
            float? msrp = default(float?),
            float? destination = default(float?),
            string styleCVCList = default(string),
            byte? mktClassID = default(byte?),
            string styleNameWOTrim = default(string),
            string trim = default(string),
            int? passengerCapacity = default(int?),
            int? passengerDoors = default(int?),
            string manualTrans = default(string),
            string autoTrans = default(string),
            string frontWD = default(string),
            string rearWD = default(string),
            string allWD = default(string),
            string fourWD = default(string),
            string stepSide = default(string),
            string caption = default(string),
            string availability = default(string),
            string priceState = default(string),
            string autoBuilderStyleID = default(string),
            string cFModelName = default(string),
            string cFStyleName = default(string),
            string cFDrivetrain = default(string),
            string cFBodyType = default(string),
            IList<Color> colors = default(IList<Color>),
            Model model = default(Model),
            IList<Option> options = default(IList<Option>),
            IList<Price> prices = default(IList<Price>),
            IList<Standard> standards = default(IList<Standard>),
            IList<StyleCat> styleCats = default(IList<StyleCat>),
            IList<StyleGenericEquipment> styleGenericEquipment = default(IList<StyleGenericEquipment>))
        {
            CountryCode = countryCode;
            StyleID = styleID;
            HistStyleID = histStyleID;
            ModelID = modelID;
            ModelYear = modelYear;
            Sequence = sequence;
            StyleCode = styleCode;
            FullStyleCode = fullStyleCode;
            StyleName = styleName;
            TrueBasePrice = trueBasePrice;
            Invoice = invoice;
            MSRP = msrp;
            Destination = destination;
            StyleCVCList = styleCVCList;
            MktClassID = mktClassID;
            StyleNameWOTrim = styleNameWOTrim;
            Trim = trim;
            PassengerCapacity = passengerCapacity;
            PassengerDoors = passengerDoors;
            ManualTrans = manualTrans;
            AutoTrans = autoTrans;
            FrontWD = frontWD;
            RearWD = rearWD;
            AllWD = allWD;
            FourWD = fourWD;
            StepSide = stepSide;
            Caption = caption;
            Availability = availability;
            PriceState = priceState;
            AutoBuilderStyleID = autoBuilderStyleID;
            CFModelName = cFModelName;
            CFStyleName = cFStyleName;
            CFDrivetrain = cFDrivetrain;
            CFBodyType = cFBodyType;
            Colors = colors;
            Model = model;
            Options = options;
            Prices = prices;
            Standards = standards;
            StyleCats = styleCats;
            StyleGenericEquipment = styleGenericEquipment;
        }
    
        public byte CountryCode { get; private set; }
        public int StyleID { get; private set; }
        public int? HistStyleID { get; private set; }
        public int? ModelID { get; private set; }
        public int? ModelYear { get; private set; }
        public int? Sequence { get; private set; }
        public string StyleCode { get; private set; }
        public string FullStyleCode { get; private set; }
        public string StyleName { get; private set; }
        public string TrueBasePrice { get; private set; }
        public float? Invoice { get; private set; }
        public float? MSRP { get; private set; }
        public float? Destination { get; private set; }
        public string StyleCVCList { get; private set; }
        public byte? MktClassID { get; private set; }
        public string StyleNameWOTrim { get; private set; }
        public string Trim { get; private set; }
        public int? PassengerCapacity { get; private set; }
        public int? PassengerDoors { get; private set; }
        public string ManualTrans { get; private set; }
        public string AutoTrans { get; private set; }
        public string FrontWD { get; private set; }
        public string RearWD { get; private set; }
        public string AllWD { get; private set; }
        public string FourWD { get; private set; }
        public string StepSide { get; private set; }
        public string Caption { get; private set; }
        public string Availability { get; private set; }
        public string PriceState { get; private set; }
        public string AutoBuilderStyleID { get; private set; }
        public string CFModelName { get; private set; }
        public string CFStyleName { get; private set; }
        public string CFDrivetrain { get; private set; }
        public string CFBodyType { get; private set; }
        public IList<Color> Colors { get; private set; }
        public Model Model { get; private set; }
        public IList<Option> Options { get; private set; }
        public IList<Price> Prices { get; private set; }
        public IList<Standard> Standards { get; private set; }
        public IList<StyleCat> StyleCats { get; private set; }
        public IList<StyleGenericEquipment> StyleGenericEquipment { get; private set; }
    }
}
