/* 
 * SharedChromeData Container DTO
 *
 * This DTO has been generated using GenerateSharedChromeDtosAndQuery.tt. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>SharedChromeData Container DTO</summary>
    public partial class SharedChromeData
    {
        private readonly IDictionary<int, Category> _categories;
        private readonly IDictionary<int, CategoryHeader> _categoryHeaders;

        public IDictionary<int, Category> Categories { get { return _categories; } }
        public IDictionary<int, CategoryHeader> CategoryHeaders { get { return _categoryHeaders; } }

        public SharedChromeData(
            IEnumerable<Category> categories = null,
            IEnumerable<CategoryHeader> categoryHeaders = null)
        {
            _categories = new ReadOnlyDictionary<int, Category>(categories == null ? null : categories.ToDictionary(i => i.CategoryID));
            _categoryHeaders = new ReadOnlyDictionary<int, CategoryHeader>(categoryHeaders == null ? null : categoryHeaders.ToDictionary(i => i.CategoryHeaderID));
        }
    }
}
