/* 
 * SharedChromeData Category DTO
 *
 * This DTO has been generated using GenerateSharedChromeDtosAndQuery.tt. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>SharedChromeData Category DTO</summary>
    public partial class Category
    {
        private readonly System.Int32 _categoryID;
        private readonly System.String _category;
        private readonly System.String _categoryTypeFilter;
        private readonly System.Int32? _categoryHeaderID;
        private readonly System.String _userFriendlyName;

        public System.Int32 CategoryID { get { return _categoryID; } }
        public System.String _Category { get { return _category; } }
        public System.String CategoryTypeFilter { get { return _categoryTypeFilter; } }
        public System.Int32? CategoryHeaderID { get { return _categoryHeaderID; } }
        public System.String UserFriendlyName { get { return _userFriendlyName; } }

        public Category(
            System.Int32 categoryID = default(System.Int32),
            System.String category = default(System.String),
            System.String categoryTypeFilter = default(System.String),
            System.Int32? categoryHeaderID = default(System.Int32?),
            System.String userFriendlyName = default(System.String))
        {
            _categoryID = categoryID;
            _category = category;
            _categoryTypeFilter = categoryTypeFilter;
            _categoryHeaderID = categoryHeaderID;
            _userFriendlyName = userFriendlyName;
        }
    }
}
