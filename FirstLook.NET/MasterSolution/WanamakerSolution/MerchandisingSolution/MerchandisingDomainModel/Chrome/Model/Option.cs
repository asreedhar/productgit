/* 
 * Chrome Option DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome Option DTO</summary>
    public partial class Option
    {
        public Option(
            byte countryCode = default(byte),
            int? styleID = default(int?),
            int? headerID = default(int?),
            int? sequence = default(int?),
            string optionCode = default(string),
            string optionDesc = default(string),
            int? optionKindID = default(int?),
            string categoryList = default(string),
            string typeFilter = default(string),
            string availability = default(string),
            string pon = default(string),
            string extDescription = default(string),
            string supportedLogic = default(string),
            string unsupportedLogic = default(string),
            string priceNotes = default(string),
            string header = default(string),
            string optionKind = default(string))
        {
            CountryCode = countryCode;
            StyleID = styleID;
            HeaderID = headerID;
            Sequence = sequence;
            OptionCode = optionCode;
            OptionDesc = optionDesc;
            OptionKindID = optionKindID;
            CategoryList = categoryList;
            TypeFilter = typeFilter;
            Availability = availability;
            PON = pon;
            ExtDescription = extDescription;
            SupportedLogic = supportedLogic;
            UnsupportedLogic = unsupportedLogic;
            PriceNotes = priceNotes;
            Header = header;
            OptionKind = optionKind;
        }
    
        public byte CountryCode { get; private set; }
        public int? StyleID { get; private set; }
        public int? HeaderID { get; private set; }
        public int? Sequence { get; private set; }
        public string OptionCode { get; private set; }
        public string OptionDesc { get; private set; }
        public int? OptionKindID { get; private set; }
        public string CategoryList { get; private set; }
        public string TypeFilter { get; private set; }
        public string Availability { get; private set; }
        public string PON { get; private set; }
        public string ExtDescription { get; private set; }
        public string SupportedLogic { get; private set; }
        public string UnsupportedLogic { get; private set; }
        public string PriceNotes { get; private set; }
        public string Header { get; private set; }
        public string OptionKind { get; private set; }
    }
}
