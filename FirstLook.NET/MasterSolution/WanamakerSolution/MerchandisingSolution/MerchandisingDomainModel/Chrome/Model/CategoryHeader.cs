/* 
 * SharedChromeData CategoryHeader DTO
 *
 * This DTO has been generated using GenerateSharedChromeDtosAndQuery.tt. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>SharedChromeData CategoryHeader DTO</summary>
    public partial class CategoryHeader
    {
        private readonly System.Int32 _categoryHeaderID;
        private readonly System.String _categoryHeader;

        public System.Int32 CategoryHeaderID { get { return _categoryHeaderID; } }
        public System.String _CategoryHeader { get { return _categoryHeader; } }

        public CategoryHeader(
            System.Int32 categoryHeaderID = default(System.Int32),
            System.String categoryHeader = default(System.String))
        {
            _categoryHeaderID = categoryHeaderID;
            _categoryHeader = categoryHeader;
        }
    }
}
