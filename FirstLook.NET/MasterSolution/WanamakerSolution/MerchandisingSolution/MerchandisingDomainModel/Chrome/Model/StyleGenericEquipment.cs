/* 
 * Chrome StyleGenericEquipment DTO
 *
 * This DTO has been generated using GenerateChromeDtosAndQuery.ps1. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome StyleGenericEquipment DTO</summary>
    public partial class StyleGenericEquipment
    {
        public StyleGenericEquipment(
            byte countryCode = default(byte),
            int styleID = default(int),
            int categoryID = default(int),
            string styleAvailability = default(string))
        {
            CountryCode = countryCode;
            StyleID = styleID;
            CategoryID = categoryID;
            StyleAvailability = styleAvailability;
        }
    
        public byte CountryCode { get; private set; }
        public int StyleID { get; private set; }
        public int CategoryID { get; private set; }
        public string StyleAvailability { get; private set; }
    }
}
