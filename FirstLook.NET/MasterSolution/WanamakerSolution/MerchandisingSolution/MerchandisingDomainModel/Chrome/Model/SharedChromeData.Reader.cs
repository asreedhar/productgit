/* 
 * SharedChromeData Container Reader
 *
 * This DTO has been generated using GenerateSharedChromeDtosAndQuery.tt. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>SharedChromeData Container DTO</summary>
    public partial class SharedChromeData
    {
        public static SharedChromeData Read()
        {
            return new ReaderContext().InnerRead();
        }

        private class ReaderContext
        {
            IList<Category> _categories;
            IList<CategoryHeader> _categoryHeaders;

            public SharedChromeData InnerRead()
            {
                using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
                using(var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.Chrome.SharedChromeData#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    using(var r = cmd.ExecuteReader())
                    {
                        _categories = ReadCategories(r);
                        NextResult(r);
                        _categoryHeaders = ReadCategoryHeaders(r);

                        return new SharedChromeData(
                            _categories,
                            _categoryHeaders);
                    }
                }
            }
        
            private IList<Category> ReadCategories(IDataReader r)
            {
                var categoryIDField = r.GetOrdinal("categoryID");
                var categoryField = r.GetOrdinal("category");
                var categoryTypeFilterField = r.GetOrdinal("categoryTypeFilter");
                var categoryHeaderIDField = r.GetOrdinal("categoryHeaderID");
                var userFriendlyNameField = r.GetOrdinal("userFriendlyName");

                var list = new List<Category>();
                while(r.Read())
                {
                    list.Add(
                        new Category(
                            r.GetInt32(categoryIDField),
                            r.IsDBNull(categoryField) ? null : Intern(r.GetString(categoryField)),
                            r.IsDBNull(categoryTypeFilterField) ? null : Intern(r.GetString(categoryTypeFilterField)),
                            r.IsDBNull(categoryHeaderIDField) ? (System.Int32?)null : r.GetInt32(categoryHeaderIDField),
                            r.IsDBNull(userFriendlyNameField) ? null : Intern(r.GetString(userFriendlyNameField))));
                }
                return list;
            }

            private IList<CategoryHeader> ReadCategoryHeaders(IDataReader r)
            {
                var categoryHeaderIDField = r.GetOrdinal("categoryHeaderID");
                var categoryHeaderField = r.GetOrdinal("categoryHeader");

                var list = new List<CategoryHeader>();
                while(r.Read())
                {
                    list.Add(
                        new CategoryHeader(
                            r.GetInt32(categoryHeaderIDField),
                            r.IsDBNull(categoryHeaderField) ? null : Intern(r.GetString(categoryHeaderField))));
                }
                return list;
            }

            private readonly Dictionary<string, string> _internedStrings = new Dictionary<string, string>(StringComparer.Ordinal);
            private string Intern(string value)
            {
                string v;
                if(!_internedStrings.TryGetValue(value, out v))
                {
                    v = value;
                    _internedStrings.Add(v, v);
                }
                return v;
            }

            private void NextResult(IDataReader r)
            {
                if(!r.NextResult())
                    throw new InvalidOperationException("Expected more result sets.");
            }
        }
    }
}
