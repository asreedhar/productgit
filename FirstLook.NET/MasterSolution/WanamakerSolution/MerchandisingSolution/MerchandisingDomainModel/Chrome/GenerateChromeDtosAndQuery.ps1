param(
    $Server = "BetaDb001"
)

# The basic idea is that this script will read the schema for a subset of chrome tables and 
# generate C# DTOs and related queries to populate the DTOs.

$Root = split-path $MyInvocation.MyCommand.Path
$Generator = split-path $MyInvocation.MyCommand.Path -leaf
$DbTools = @{ }
$Context = @{ 
    TablesInLoadOrder = new-object System.Collections.ArrayList;
}

function Close-DbConnection
{
    if($DbTools.DbConnection -ne $null)
    {
        $DbTools.DbConnection.Close()
    }
}

function Open-DbConnection
{
    param(
        [Parameter(Position = 0)]
        [string] $Server = ".",
        
        [Parameter(Position = 1)]
        [string] $Database = "master",
        
        [string] $User = $null,
        
        [string] $Password = $null
    )
    
    Close-DbConnection
    
    $b = new-object System.Data.SqlClient.SqlConnectionStringBuilder
    $b."Data Source" = $Server
    $b."Initial Catalog" = $Database
    $b."Application Name" = "PowerShell"
    if([string]::IsNullOrEmpty($User))
    {
        $b."Integrated Security" = $true
    }
    else
    {
        $b."User ID" = $User
        $b.Password = $Password
    }
    
    $cn = new-object System.Data.SqlClient.SqlConnection ($b.ConnectionString)
    if($cn.State -ne [System.Data.ConnectionState]::Open)
    {
        $cn.Open()
    }
    
    $DbTools.DbConnection = $cn
}

function Invoke-DbScript($sql, [HashTable] $ExtraProps)
{
    if($ExtraProps -eq $null)
    {
        $ExtraProps = @{ }
    }

    $cmd = $DbTools.DbConnection.CreateCommand()
    try
    {
        $cmd.CommandText = $sql
        $cmd.CommandTimeout = 3600
        
        $r = $cmd.ExecuteReader()
        try
        {
            $columns = $null
            while($r.Read() -and !$PSCmdlet.Stopping)
            {
                $result = @{} 
                
                if($columns -eq $null)
                {
                    $columns = new-object string[] $r.FieldCount
                    for($i = 0; $i -lt $r.FieldCount; ++$i)
                    {
                        $columns[$i] = $r.GetName($i)
                    }
                }
                
                for($i = 0; $i -lt $columns.Length; ++$i)
                {
                    $result[$columns[$i]] = $r[$i]
                }
                
                foreach($key in $ExtraProps.Keys)
                {
                    $result[$key] = $ExtraProps[$key]
                }
                
                new-object PSObject -Property $result
            }
        }
        finally
        {
            $r.Dispose()
        }
    }
    finally
    {
        $cmd.Dispose()
    }
}

function Get-DbTableColumns($tableDef, $Schema = $null, $Db = $null, $NameOverride = @{ })
{
    $tables = @{}

    foreach($tbl in $tableDef.Keys)
    {
        $item = $tableDef[$tbl]
        $item.Name = $tbl
        $sortKey = "$($item.Order)_$($item.Name)"
        $tables.$sortKey = $item
    }

    $tables.GetEnumerator() | Sort Name | foreach-Object {
        $item = $_.Value
        $cmd = "exec sp_columns '$($item.Name)'"
        if($Schema -ne $null) { $cmd += ", '$Schema'" }
        if($Db -ne $null) { $cmd += ", '$Db'" }
        
        $fields = @(
            Invoke-DbScript $cmd -ExtraProps @{ 
                Alias = $item.Alias; 
                ClrDataType = $null;
                ClrIsValueType = $null;
                ClrNullable = $null;
                ClrDataReaderExpression = $null;
                ClrName = $null;
            } | 
            foreach-object {
                switch($_.TYPE_NAME)
                {
                    "bigint" { $dt = "long"; $getter = { param($r, $ord) "$r.GetInt64($ord)" } }
                    "binary" { $dt = "byte[]"; $getter = { param($r, $ord) "(byte[])$r.GetValue($ord)" } }
                    "bit" { $dt = "bool"; $getter = { param($r, $ord) "$r.GetBoolean($ord)" } }
                    "char" { $dt = "string"; $getter = { param($r, $ord) "Intern($r.GetString($ord))" } }
                    "date" { $dt = "DateTime"; $getter = { param($r, $ord) "$r.GetDateTime($ord)" } }
                    "datetime" { $dt = "DateTime"; $getter = { param($r, $ord) "$r.GetDateTime($ord)" } }
                    "datetimeoffset" { $dt = "DateTimeOffset"; $getter = { param($r, $ord) "$r.GetDateTime($ord)" } }
                    "decimal" { $dt = "Decimal"; $getter = { param($r, $ord) "$r.GetDecimal($ord)" } }
                    "float" { $dt = "double"; $getter = { param($r, $ord) "$r.GetDouble($ord)" } }
                    "image" { $dt = "byte[]"; $getter = { param($r, $ord) "(byte[])$r.GetValue($ord)" } }
                    "int" { $dt = "int"; $getter = { param($r, $ord) "$r.GetInt32($ord)" } }
                    "money" { $dt = "Decimal"; $getter = { param($r, $ord) "$r.GetDecimal($ord)" } }
                    "nchar" { $dt = "string"; $getter = { param($r, $ord) "Intern($r.GetString($ord))" } }
                    "ntext" { $dt = "string"; $getter = { param($r, $ord) "Intern($r.GetString($ord))" } }
                    "numeric" { $dt = "Decimal"; $getter = { param($r, $ord) "$r.GetDecimal($ord)" } }
                    "nvarchar" { $dt = "string"; $getter = { param($r, $ord) "Intern($r.GetString($ord))" } }
                    "real" { $dt = "float"; $getter = { param($r, $ord) "$r.GetFloat($ord)" } }
                    "rowversion" { $dt = "byte[]"; $getter = { param($r, $ord) "(byte[])$r.GetValue($ord)"  } }
                    "smalldatetime" { $dt = "DateTime"; $getter = { param($r, $ord) "$r.GetDateTime($ord)" } }
                    "smallint" { $dt = "short"; $getter = { param($r, $ord) "$r.GetInt16($ord)" } }
                    "smallmoney" { $dt = "Decimal"; $getter = { param($r, $ord) "$r.GetDecimal($ord)" } }
                    "sql_variant" { $dt = "object"; $getter = { param($r, $ord) "$r.GetValue($ord)" } }
                    "text" { $dt = "string"; $getter = { param($r, $ord) "Intern($r.GetString($ord))" } }
                    "timestamp" { $dt = "byte[]"; $getter = { param($r, $ord) "(byte[])$r.GetValue($ord)" } }
                    "tinyint" { $dt = "byte"; $getter = { param($r, $ord) "$r.GetByte($ord)" } }
                    "uniqueidentifier" { $dt = "Guid"; $getter = { param($r, $ord) "$r.GetGuid($ord)" } }
                    "varbinary" { $dt = "byte[]"; $getter = { param($r, $ord) "(byte[])$r.GetValue($ord)" } }
                    "varchar" { $dt = "string"; $getter = { param($r, $ord) "Intern($r.GetString($ord))" } }
                    "xml" { throw "xml type not yet supported." } 
                    "time" { throw "time type not yet supported." }
                    default { throw "Unknown sql datatype: $($_.TYPE_NAME)." }
                }
                
                $isValueType = "string", "byte[]", "object" -notcontains $dt
                $nullable = $_.IS_NULLABLE -eq "YES"
                if($isValueType -and $nullable) { $dt += "?" }
                
                $_.ClrDataType = $dt
                $_.ClrIsValueType = $isValueType
                $_.ClrNullable = $nullable
                $_.ClrDataReaderExpression = $getter
                $_.ClrName = if($NameOverride.ContainsKey($_.COLUMN_NAME)) { $NameOverride[$_.COLUMN_NAME] } else { $_.COLUMN_NAME }
                
                $_
            })
            
        if($item.ExcludeField -ne $null)
        {
            $fields = @( $fields | where-object { $item.ExcludeField -notcontains $_.COLUMN_NAME } )
        }
        if($item.IncludeField -ne $null)
        {
            $fields = @( $fields | where-object { $item.IncludeField -contains $_.COLUMN_NAME } )
        }
        
        $fields | foreach-object { $_ }
    }
}

function Get-CamelCase($name)
{
    if($name -eq $null)
    {
        $name
    }
    else
    {
        $isAllUpper = $name -cmatch "^[A-Z0-9_]+$"
        $startsWithUnderScore = $name -match "^_"
        if($isAllUpper)
        {
            $name.ToLower()
        }
        elseif($startsWithUnderScore)
        {
            $name.Substring(0,2).ToLower() + $name.Substring(2)
        }
        else
        {
            $name.Substring(0,1).ToLower() + $name.Substring(1)
        }
    }
}

function Get-PropertyDefFromField
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $field
    )
    
    process {
        $dt = $field.ClrDataType
        $name = $field.ClrName
        "public $dt $name { get; private set; }"
    }
}

function Get-PropertyDefFromChildObjects
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $child
    )
    
    process {
        if($child -ne $null)
        {
            $cls = $child.Name
            $nameProp = $child.Value.PropName
            switch($child.Value.Relation)
            {
                "OneToMany" { $dt = "IList<$cls>" }
                "OneToOne" { $dt = "$cls" }
                default { throw "Invalid value for Relation" }
            }
            
            "public $dt $nameProp { get; private set; }"
        }
    }
}

function Get-ParameterDefFromField
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $field
    )
    
    process {
        $dt = $field.ClrDataType
        $name = Get-CamelCase $field.ClrName
        "$dt $name = default($dt)"
    }
}

function Get-ParameterDefFromChildObjects
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $child
    )
    
    process {
        if($child -ne $null)
        {
            $cls = $child.Name
            $name = Get-CamelCase $child.Value.PropName
            switch($child.Value.Relation)
            {
                "OneToMany" { $dt = "IList<$cls>" }
                "OneToOne" { $dt = "$cls" }
                default { throw "Invalid value for Relation" }
            }
            
            "$dt $name = default($dt)"
        }
    }
}

function Get-AssignmentFromField
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $field
    )
    
    process {
        $nameParam = Get-CamelCase $field.ClrName
        $nameProp = $field.ClrName
        $prefix = if($nameParam -ceq $nameProp) { "this." } else { "" }
        "$prefix$nameProp = $nameParam;"
    }
}

function Get-AssignmentFromChildObjects
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $child
    )
    
    process {
        if($child -ne $null)
        {
            $name = $child.Value.PropName
            $nameParam = Get-CamelCase $name
            $nameProp = $name
            $prefix = if($nameParam -ceq $nameProp) { "this." } else { "" }
            
            "$prefix$nameProp = $nameParam;"
        }
    }
}

function Write-DtoFile($tableData)
{
    $fields = $tableData.Fields
    $children = $tableData.ChildObjects.GetEnumerator() | Sort-Object Name
    $cls = $tableData.Cls
@"
/* 
 * Chrome $cls DTO
 *
 * This DTO has been generated using $Generator. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    /// <summary>Chrome $cls DTO</summary>
    public partial class $cls
    {
        public $cls(
            $(@($fields | Get-ParameterDefFromField) + @($children | Get-ParameterDefFromChildObjects) -join ",`r`n            "))
        {
            $(@($fields | Get-AssignmentFromField) + @($children | Get-AssignmentFromChildObjects) -join "`r`n            ")
        }
    
        $(@($fields | Get-PropertyDefFromField) + @($children | Get-PropertyDefFromChildObjects) -join "`r`n        ")
    }
}
"@ | set-content (join-path $Root "Model\$cls.cs")
}

function Get-Query
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $data
    )
    
    process {
@"
select
    $(($data.Fields | %{ "$(if($_.Alias -ne $null) { $_.Alias + '.' })$($_.COLUMN_NAME)" }) -join ",`r`n    ")
from 
    $($data.FromClause)
where 
    $($data.WhereClause)
"@
    }

}

function Write-SqlProc()
{
@"
-- Chrome Style Loader
-- This stored procedure has been generated by $Generator. 
-- DO NOT HAND EDIT THIS FILE.

use Merchandising
go

if object_id('chrome.Style#Fetch', 'P') is not null
    drop procedure chrome.Style#Fetch
go

create procedure chrome.Style#Fetch
    @xml varchar(max)
as

set nocount on
set transaction isolation level read uncommitted

declare @ids table (id int primary key)

-- Get the list of VINPatternIDs to retrieve
declare @hdoc int
exec sp_xml_preparedocument @hdoc OUTPUT, @xml

insert into @ids
select id
from openxml(@hdoc, '/ids/v', 10) with (id int 'text()') INP

exec sp_xml_removedocument @hdoc

$(
($Context.TablesInLoadOrder.GetEnumerator() | Get-Query) -join "`r`n`r`n"
)

go

grant execute on chrome.Style#Fetch to MerchandisingUser
go

"@ | set-content (join-path $Root "..\..\..\..\..\..\db-scripts\Merchandising\scripts\storedProcs\chrome\Style#Fetch.sql")
}

function Get-ReadContextFieldDef
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $table
    )
    
    process {
    
        $nameField = "_" + (Get-CamelCase $table."Cls")
        $nameCls = $table."Cls"
        
        if($table.ChildObjects.Count -gt 0)
        {
            $wrapper = "IList<"
        }
        else
        {
            $baseObject = $Context.TablesInLoadOrder | where { $_.ChildObjects.Count -gt 0 }
            $wrapper = switch($baseObject.ChildObjects[$nameCls].Relation)
            {
                "OneToMany" { "ILookup<int, " }
                "OneToOne" { "IDictionary<int, " }
                default { 
                    throw "Invalid value for Relation." 
                }
            }
        }
        
        "private $wrapper$nameCls> $nameField;"
    }
}

function Get-ReadContextLoadCall
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $table
    )
    
    process {
        $nameCls = $table."Cls"
        "Read$nameCls();"
    }
}

function Get-DataReaderExpression
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $field
    )

    process {
        $ordinalName = (Get-CamelCase $field.ClrName) + "Field"
        if($field.ClrNullable)
        {
            $nullExpr = if($field.ClrIsValueType) { "($($field.ClrDataType))null" } else { "null" }
            "_reader.IsDBNull($ordinalName) ? $nullExpr : " + 
                (& $field.ClrDataReaderExpression '_reader' $ordinalName)
        }
        else
        {
            & $field.ClrDataReaderExpression '_reader' $ordinalName
        }
    }
}

function Get-ReadContextLoadMethod
{
    param(
        [Parameter(ValueFromPipeline = $true)]
        $table
    )
    
    process {
        $nameCls = $table."Cls"
        $fields = $table.Fields
        $styleIdField = $fields | where { ($_.COLUMN_NAME -eq 'StyleID') -or ($_.COLUMN_NAME -eq 'ChromeStyleID') }
        $children = $table.ChildObjects
        $outputField = "_" + (Get-CamelCase $table."Cls")
@"
private void Read$nameCls()
            {
                $(
                    ($fields | foreach-object { 
                        $ordinalName = (Get-CamelCase $_.ClrName) + "Field"
                        $columnName = $_.COLUMN_NAME
                        "var $ordinalName = _reader.GetOrdinal(`"$columnName`");"
                    }) -join "`r`n                "
                )
                
                var list = new List<$nameCls>();
                
                while(_reader.Read())
                {
                    $(
                        if($styleIdField.ClrNullable) { 
                            "if(_reader.IsDBNull($((Get-CamelCase $styleIdField.ClrName) + "Field")))
                        continue;
                    "
                        }
                        else { 
                            "" 
                        }
                    )
                    var styleId = _reader.GetInt32($((Get-CamelCase $styleIdField.ClrName) + "Field"));
                    
                    list.Add(new $nameCls(
                        $(
                            @($fields | foreach-object {
                                if($_.COLUMN_NAME -eq 'StyleID') {
                                    "styleId"
                                } else {
                                    Get-DataReaderExpression $_
                                }
                            }) + 
                            @($children.GetEnumerator() | Sort-Object Name | foreach-object {
                                $cls = $_.Name
                                $field = "_" + (Get-CamelCase $cls)
                                switch($_.Value.Relation)
                                {
                                    "OneToMany" { "$field[styleId].ToList().AsReadOnly()" }
                                    "OneToOne" { "GetDictionaryItem($field, styleId)" }
                                    default { throw "Invalid value for Relation." }
                                }
                            }) -join ",`r`n                        "
                        )
                        ));
                }
                
                $(
                    if($table.ChildObjects.Count -gt 0)
                    {
                        "$outputField = list;"
                    }
                    else
                    {
                        $field = $fields | Where-Object { $_.COLUMN_NAME -eq "StyleID" }
                        $baseObject = $Context.TablesInLoadOrder | where { $_.ChildObjects.Count -gt 0 }
                        switch($baseObject.ChildObjects[$nameCls].Relation)
                        {
                            "OneToMany" { 
                                if($field.ClrNullable)
                                {
                                    "$outputField = list.Where(i => i.StyleID.HasValue).ToLookup(i => i.StyleID.GetValueOrDefault());"
                                }
                                else
                                {
                                    "$outputField = list.ToLookup(i => i.StyleID);"
                                }
                            }
                            "OneToOne" { 
                                if($field.ClrNullable)
                                {
                                    "$outputField = list.Where(i => i.StyleID.HasValue).ToDictionary(i => i.StyleID.GetValueOrDefault());"
                                }
                                else
                                {
                                    "$outputField = list.ToDictionary(i => i.StyleID);"
                                }
                            }
                            default { throw "Invalid value for Relation." }
                        }
                        
                    }
                )
            }
"@    
    }
}

function Write-DbReader()
{
@"
/* 
 * Chrome $cls DTO
 *
 * This DTO has been generated using $Generator. 
 * DO NOT HAND EDIT THIS FILE.
 *
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Chrome.Model
{
    public partial class Style
    {
        private class ReadContext
        {
            public ReadContext(IDataReader r)
            {
                _reader = r;
            }
        
            private readonly IDictionary<string, string> _strings = new Dictionary<string, string>(StringComparer.Ordinal);
            private readonly IDataReader _reader;
        
            $(
                ($Context.TablesInLoadOrder.GetEnumerator() | Get-ReadContextFieldDef) -join "`r`n            "
            )
        
            public IList<Style> ReadResults()
            {
                $(
                    ($Context.TablesInLoadOrder.GetEnumerator() | Get-ReadContextLoadCall) -join "`r`n                MoveToNextResultSet();`r`n                "
                )
                
                return _style;
            }
            
            $(
                ($Context.TablesInLoadOrder.GetEnumerator() | Get-ReadContextLoadMethod) -join "`r`n`r`n            "
            )
        
            private string Intern(string val)
            {
                if (val == null)
                    return null;

                if(val.Length == 0)
                    return string.Empty;

                string interned;
                if(!_strings.TryGetValue(val, out interned))
                {
                    interned = val;
                    _strings.Add(interned, interned);
                }

                return interned;
            }
            
            private static T GetDictionaryItem<T>(IDictionary<int, T> items, int styleId)
            {
                T val;
                items.TryGetValue(styleId, out val);
                return val;
            }
            
            private void MoveToNextResultSet()
            {
                if(!_reader.NextResult())
                    throw new InvalidOperationException("Expected more result sets.");
            }
        }
    
        internal static IList<Style> ReadStyleById(IEnumerable<int> ids)
        {
            var xml = BuildXmlFromChromeStyleIds(ids);
        
            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandText = "Merchandising.Chrome.Style\x0023Fetch";
                cmd.CommandType = CommandType.StoredProcedure;
                
                AddVarcharMaxParameter(cmd, "`@xml", xml);
                
                using(var r = cmd.ExecuteReader())
                {
                    return new ReadContext(r).ReadResults();
                }
            }
        }

        private static void AddVarcharMaxParameter(IDbCommand cmd, string name, string xml)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.DbType = DbType.AnsiString;
            p.Size = -1;
            p.Value = xml;
            cmd.Parameters.Add(p);
        }
        
        private static string BuildXmlFromChromeStyleIds(IEnumerable<int> chromeStyleIds)
        {
            var b = new StringBuilder();
            b.Append("<ids>");

            chromeStyleIds
                .Distinct()
                .OrderBy(id => id)
                .Aggregate(b, (builder, id) =>
                    {
                        builder.Append("<v>");
                        builder.Append(id.ToString(CultureInfo.InvariantCulture));
                        builder.Append("</v>");
                        return builder;
                    });

            b.Append("</ids>");
            return b.ToString();
        }
    }
}

"@ | set-content (join-path $Root "Model\Style.Reader.cs")
}

function Add-ClassDto
{
    param(
        $tableDef,
        $cls,
        $FromClause,
        $WhereClause,
        $ChildObjects,
        $NameOverride
    )
    $data = $PSBoundParameters
    if($data.NameOverride -eq $null) { $data.NameOverride = @{ } }
    $data.Fields = Get-DbTableColumns $tableDef Chrome -NameOverride $data.NameOverride
    if($data.ChildObjects -eq $null) { $data.ChildObjects = @{ } }
    foreach($child in $data.ChildObjects.GetEnumerator())
    {
        if($child.Value.Plural -eq $null)
        {
            $child.Value.Plural = $child.Name
        }
        switch($child.Value.Relation)
        {
            "OneToMany" { $child.Value.PropName = $child.Value.Plural }
            "OneToOne" { $child.Value.PropName = $child.Name }
            default { throw "Unknown value for Relation" }
        }
    }
    
    $Context.TablesInLoadOrder.Add($data) | Out-Null
}

Open-DbConnection $Server VehicleCatalog
try
{
    Add-ClassDto @{ 
            Options =    @{ Order = 1; Alias = "O"; };
            OptHeaders = @{ Order = 2; Alias = "OH"; ExcludeField = "HeaderID", "CountryCode"; };
            OptKinds =   @{ Order = 3; Alias = "OK"; ExcludeField = "OptionKindID", "CountryCode"; };
        } Option `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.Options O on I.id = O.StyleID
        left join VehicleCatalog.Chrome.OptHeaders OH on O.HeaderID = OH.HeaderID
        left join VehicleCatalog.Chrome.OptKinds OK on O.OptionKindID = OK.OptionKindID" `
        -WhereClause "O.CountryCode = 1
        and OH.CountryCode = 1
        and OK.CountryCode = 1"
        
    Add-ClassDto @{
            Styles =        @{ Order = 0; Alias = "S"; IncludeField = "StyleID" }
            Models =        @{ Order = 1; Alias = "MDL"; }
            Subdivisions =  @{ Order = 2; Alias = "SDV"; ExcludeField = "ModelYear", "SubdivisionID", "DivisionID", "CountryCode" }
            Divisions =     @{ Order = 3; Alias = "DIV"; ExcludeField = "DivisionID", "CountryCode" }
            Manufacturers = @{ Order = 4; Alias = "MFG"; ExcludeField = "ManufacturerID", "CountryCode" }
        } Model `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.Styles S on I.id = S.StyleID
        inner join VehicleCatalog.Chrome.Models MDL on S.ModelID = MDL.ModelID
        left join VehicleCatalog.Chrome.Subdivisions SDV on MDL.SubdivisionID = SDV.SubdivisionID
        left join VehicleCatalog.Chrome.Divisions DIV on MDL.DivisionID = DIV.DivisionID
        left join VehicleCatalog.Chrome.Manufacturers MFG on DIV.ManufacturerID = MFG.ManufacturerID" `
        -WhereClause "S.CountryCode = 1
        and MDL.CountryCode = 1
        and SDV.CountryCode = 1
        and DIV.CountryCode = 1
        and MFG.CountryCode = 1"
        
    Add-ClassDto @{
            Standards  = @{ Order = 0; Alias = "S"; }
            StdHeaders = @{ Order = 1; Alias = "H"; ExcludeField = "HeaderID", "CountryCode" }
        } Standard `
        -NameOverride @{ "Standard" = "_Standard" } `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.Standards S on I.id = S.StyleID
        left join VehicleCatalog.Chrome.StdHeaders H on S.HeaderID = H.HeaderID" `
        -WhereClause "S.CountryCode = 1
        and H.CountryCode = 1"
        
    Add-ClassDto @{
            StyleCats =       @{ Order = 0; Alias = "SC"; }
        } StyleCat `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.StyleCats SC on I.id = SC.StyleID" `
        -WhereClause "SC.CountryCode = 1"
        
    Add-ClassDto @{
            StyleGenericEquipment = @{ Order = 1; Alias = "SGE" }
        } StyleGenericEquipment `
        -NameOverride @{ "ChromeStyleID" = "StyleID" } `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.StyleGenericEquipment SGE on I.id = SGE.ChromeStyleID" `
        -WhereClause "SGE.CountryCode = 1"

    Add-ClassDto @{
            Colors = @{ Order = 1; Alias = "C"; }
        } Color `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.Colors C on I.id = C.StyleID" `
        -WhereClause "C.CountryCode = 1"
        
    Add-ClassDto @{
            Prices = @{ Order = 1; Alias = "P"; }
        } Price `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.Prices P on I.id = P.StyleID" `
        -WhereClause "P.CountryCode = 1"
    
    # Style always goes last
    Add-ClassDto @{ 
            Styles = @{ Order = 1; Alias = "S"; }; 
        } Style `
        -FromClause "@ids I
        inner join VehicleCatalog.Chrome.Styles S on I.id = S.StyleID" `
        -WhereClause "S.CountryCode = 1" `
        -ChildObjects @{
            Option = @{ Relation = "OneToMany"; Plural = "Options" }
            Model = @{ Relation = "OneToOne"; }
            Standard = @{ Relation = "OneToMany"; Plural = "Standards" }
            StyleCat = @{ Relation = "OneToMany"; Plural = "StyleCats" }
            Color = @{ Relation = "OneToMany"; Plural = "Colors" }
            Price = @{ Relation = "OneToMany"; Plural = "Prices" }
            StyleGenericEquipment = @{ Relation = "OneToMany"; Plural = "StyleGenericEquipment" }
        }
    
    $Context.TablesInLoadOrder | 
        Foreach-Object {
            Write-DtoFile $_
        }
 
    Write-SqlProc
    
    Write-DbReader
}
finally
{
    Close-DbConnection
}