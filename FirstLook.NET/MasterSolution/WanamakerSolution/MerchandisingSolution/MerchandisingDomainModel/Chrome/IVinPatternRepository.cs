﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public interface IVinPatternRepository
    {
        IEnumerable<IVinPattern> GetVinPatternById(IEnumerable<int> vinPatternIds);
    }
}