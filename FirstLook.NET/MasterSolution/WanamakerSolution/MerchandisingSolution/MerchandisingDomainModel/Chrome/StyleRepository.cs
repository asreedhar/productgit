﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public class StyleRepository : IStyleRepository
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<StyleRepository>();

        public IEnumerable<Style> GetStyleById(IEnumerable<int> ids)
        {
            if (Log.IsInfoEnabled)
                Log.Info("Starting to execute StyleRepository.GetStyleById()");

            var t = DateTimeOffset.UtcNow;

            var styles = Style.ReadStyleById(ids);

            if (Log.IsInfoEnabled)
                Log.InfoFormat("Completed StyleRepository.GetStyleById(). ElapsedMs={0:0.0} FetchCount={1}",
                               (DateTimeOffset.UtcNow - t).TotalMilliseconds, styles == null ? null : (int?)styles.Count);

            return styles;
        }
    }
}