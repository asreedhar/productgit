﻿using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public interface ISharedChromeDataRepository
    {
        SharedChromeData GetData();
    }
}