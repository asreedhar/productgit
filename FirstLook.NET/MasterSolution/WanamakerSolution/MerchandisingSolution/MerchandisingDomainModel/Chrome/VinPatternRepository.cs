﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Logging;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    internal class VinPatternRepository : IVinPatternRepository
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<VinPatternRepository>();

        public IEnumerable<IVinPattern> GetVinPatternById(IEnumerable<int> vinPatternIds)
        {
            using (MiniProfiler.Current.Step("VinPatternRepository.GetVinPatternById()"))
            {
                var xml = BuildXmlFromVinPatternIds(vinPatternIds);

                if(Log.IsInfoEnabled)
                    Log.Info("Starting to execute VinPatternRepository.GetVinPatternById()");

                var t = DateTimeOffset.UtcNow;
                var fetchCount = 0;

                using (var cn = Database.GetOpenConnection(Database.VehicleCatalogDatabase))
                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText =
                        @"
set nocount on
set transaction isolation level read uncommitted

declare @ids table (id int primary key)

-- Get the list of VINPatternIDs to retrieve
declare @hdoc int
exec sp_xml_preparedocument @hdoc OUTPUT, @xml

insert into @ids
select id
from openxml(@hdoc, '/ids/v', 10) with (id int 'text()') INP

exec sp_xml_removedocument @hdoc

-- Get the StyleIds for each VIN Pattern
select isnull(VPSM.VINPatternID, 0) as VINPatternID, 
    isnull(VPSM.ChromeStyleID, 0) as ChromeStyleID
from @ids INP
    inner join VehicleCatalog.Chrome.VINPatternStyleMapping VPSM
        on INP.id = VPSM.VINPatternID
    inner join VehicleCatalog.Chrome.Styles sty
        on sty.StyleID = VPSM.ChromeStyleID
where VPSM.CountryCode = 1

-- Get all the VIN Patterns
select VP.VINPatternID,
    VINPattern,
    [Year],
    VINDivisionName,
    VINModelName,
    VINStyleName,
    EngineTypeCategoryID,
    EngineSize,
    EngineCID,
    FuelTypeCategoryID,
    ForcedInductionCategoryID,
    TransmissionTypeCategoryID,
    ManualTransAvail,
    AutoTransAvail,
    GVWRRange
from 
    @ids INP
    inner join VehicleCatalog.Chrome.VINPattern VP on INP.id = VP.VINPatternID
where VP.CountryCode = 1
";

                    AddVarcharMaxParameter(cmd, "@xml", xml);

                    using (var r = cmd.ExecuteReader())
                    {
                        var styleIdsByVinPatternLookup =
                            ReadStyleIds(r)
                                .ToLookup(s => s.VinPatternId, s => s.StyleId);

                        MoveToNextResultSet(r);

                        foreach (var vinPattern in ReadVinPatterns(r, styleIdsByVinPatternLookup))
                        {
                            fetchCount++;
                            yield return vinPattern;
                        }
                    }
                }

                if (Log.IsInfoEnabled)
                    Log.InfoFormat("Completed VinPatternRepository.GetVinPatternById(). ElapsedMs={0:0.0} FetchCount={1}",
                                   (DateTimeOffset.UtcNow - t).TotalMilliseconds, fetchCount);
            }
        }

        private static void MoveToNextResultSet(IDataReader r)
        {
            if(!r.NextResult())
                throw new InvalidOperationException("Expected more result sets.");
        }

        private struct VinPatternStyle
        {
            public int StyleId { get; set; }
            public int VinPatternId { get; set; }
        }

        private static IEnumerable<VinPatternStyle> ReadStyleIds(IDataReader r)
        {
            var chromeStyleIdField = r.GetOrdinal("ChromeStyleID");
            var vinPatternIdField = r.GetOrdinal("VINPatternID");

            while (r.Read())
                yield return
                    new VinPatternStyle
                        {
                            StyleId = r.GetInt32(chromeStyleIdField), 
                            VinPatternId = r.GetInt32(vinPatternIdField)
                        };
        }

        private static IEnumerable<IVinPattern> ReadVinPatterns(IDataReader r, 
            ILookup<int, int> styleIdsByVinPatternLookup)
        {
            var strings = new Dictionary<string, string>(StringComparer.Ordinal);

            var vinPatternIdField = r.GetOrdinal("VINPatternID");
            var vinPatternField = r.GetOrdinal("VINPattern");
            var yearField = r.GetOrdinal("Year");
            var vinDivisionNameField = r.GetOrdinal("VINDivisionName");
            var vinModelNameField = r.GetOrdinal("VINModelName");
            var vinStyleNameField = r.GetOrdinal("VINStyleName");
            var engineTypeCategoryIdField = r.GetOrdinal("EngineTypeCategoryID");
            var engineSizeField = r.GetOrdinal("EngineSize");
            var engineCidField = r.GetOrdinal("EngineCID");
            var fuelTypeCategoryIdField = r.GetOrdinal("FuelTypeCategoryID");
            var forcedInductionCategoryIdField = r.GetOrdinal("ForcedInductionCategoryID");
            var transmissionTypeCategoryIdField = r.GetOrdinal("TransmissionTypeCategoryID");
            var manualTransAvailField = r.GetOrdinal("ManualTransAvail");
            var autoTransAvailField = r.GetOrdinal("AutoTransAvail");
            var gvwrRangeField = r.GetOrdinal("GVWRRange");

            while(r.Read())
            {
                var vinPatternId = r.GetInt32(vinPatternIdField);
                yield return
                    new VinPattern(
                        vinPatternId,
                        ReadNullableString(r, strings, vinPatternField),
                        r.IsDBNull(yearField) ? (int?) null : r.GetInt32(yearField),
                        ReadNullableString(r, strings, vinDivisionNameField),
                        ReadNullableString(r, strings, vinModelNameField),
                        ReadNullableString(r, strings, vinStyleNameField),
                        r.IsDBNull(engineTypeCategoryIdField) ? (int?) null : r.GetInt32(engineTypeCategoryIdField),
                        ReadNullableString(r, strings, engineSizeField),
                        ReadNullableString(r, strings, engineCidField),
                        r.IsDBNull(fuelTypeCategoryIdField) ? (int?) null : r.GetInt32(fuelTypeCategoryIdField),
                        r.IsDBNull(forcedInductionCategoryIdField)
                            ? (int?) null
                            : r.GetInt32(forcedInductionCategoryIdField),
                        r.IsDBNull(transmissionTypeCategoryIdField)
                            ? (int?) null
                            : r.GetInt32(transmissionTypeCategoryIdField),
                        ReadNullableString(r, strings, manualTransAvailField),
                        ReadNullableString(r, strings, autoTransAvailField),
                        ReadNullableString(r, strings, gvwrRangeField),
                        styleIdsByVinPatternLookup[vinPatternId].ToList().AsReadOnly());
            }
        }

        private static string ReadNullableString(IDataRecord r, IDictionary<string, string> strings, int ordinal)
        {
            return r.IsDBNull(ordinal) ? null : Intern(strings, r.GetString(ordinal));
        }

        private static string Intern(IDictionary<string, string> strings, string val)
        {
            if (val == null)
                return null;

            if(val.Length == 0)
                return string.Empty;

            string interned;
            if(!strings.TryGetValue(val, out interned))
            {
                interned = val;
                strings.Add(interned, interned);
            }

            return interned;
        }

        private static void AddVarcharMaxParameter(IDbCommand cmd, string name, string xml)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.DbType = DbType.AnsiString;
            p.Size = -1;
            p.Value = xml;
            cmd.Parameters.Add(p);
        }

        private static string BuildXmlFromVinPatternIds(IEnumerable<int> vinPatternIds)
        {
            var b = new StringBuilder();
            b.Append("<ids>");

            var vinPatternIdCount = 0;

            vinPatternIds
                .Distinct()
                .OrderBy(id => id)
                .Aggregate(b, (builder, id) =>
                    {
                        vinPatternIdCount++;

                        builder.Append("<v>");
                        builder.Append(id.ToString(CultureInfo.InvariantCulture));
                        builder.Append("</v>");
                        return builder;
                    });

            if(Log.IsInfoEnabled)
                Log.InfoFormat("Processing {0} distinct VINPatternIDs.", vinPatternIdCount);

            b.Append("</ids>");
            return b.ToString();
        }
    }
}