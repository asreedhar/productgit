using System.Collections.Generic;
using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    internal class CachedVinPatternRepository : IVinPatternRepository
    {
        private readonly IVinPatternRepository _inner;
        private readonly CachedSetBasedFetch<IVinPattern> _cache;

        public CachedVinPatternRepository(ICache cache, IVinPatternRepository inner)
        {
            _inner = inner;
            _cache = new CachedSetBasedFetch<IVinPattern>(
                cache, 12*60*60, "VinPattern_",
                ids => _inner.GetVinPatternById(ids),
                item => item.VINPatternID);
        }

        public IEnumerable<IVinPattern> GetVinPatternById(IEnumerable<int> vinPatternIds)
        {
            return _cache.GetById(vinPatternIds);
        }
    }
}