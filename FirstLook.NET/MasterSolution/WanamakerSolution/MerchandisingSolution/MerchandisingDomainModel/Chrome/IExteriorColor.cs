namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public interface IExteriorColor
    {
        string Ext1Code { get; }
        string Ext1MfgCode { get; }
        string Ext1Desc { get; }
        string Ext1Simple { get; }

        string Ext2Code { get; }
        string Ext2MfgCode { get; }
        string Ext2Desc { get; }
        string Ext2Simple { get; }
    }
}