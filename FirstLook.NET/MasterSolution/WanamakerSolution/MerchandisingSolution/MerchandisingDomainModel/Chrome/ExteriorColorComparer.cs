﻿using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public class ExteriorColorComparer : IComparer<IExteriorColor>, IEqualityComparer<IExteriorColor>
    {
        private readonly StringComparer _cmp = StringComparer.InvariantCultureIgnoreCase;

        public int Compare(IExteriorColor x, IExteriorColor y)
        {
            if (ReferenceEquals(x, y))
                return 0;
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            
            var c = _cmp.Compare(x.Ext1Desc, y.Ext1Desc);
            if (c != 0)
                return c;

            c = _cmp.Compare(x.Ext2Desc, y.Ext2Desc);
            if (c != 0)
                return c;

            c = _cmp.Compare(x.Ext1Simple, y.Ext1Simple);
            if (c != 0)
                return c;

            return _cmp.Compare(x.Ext2Simple, y.Ext2Simple);
        }

        public bool Equals(IExteriorColor x, IExteriorColor y)
        {
            if (ReferenceEquals(x, y))
                return true;
            if (x == null || y == null)
                return false;
            return _cmp.Equals(x.Ext1Desc, y.Ext1Desc)
                   && _cmp.Equals(x.Ext2Desc, y.Ext2Desc)
                   && _cmp.Equals(x.Ext1Simple, y.Ext1Simple)
                   && _cmp.Equals(x.Ext2Simple, y.Ext2Simple);
        }

        public int GetHashCode(IExteriorColor obj)
        {
            unchecked
            {
                return
                    (((_cmp.GetHashCode(obj.Ext1Desc ?? "")*397) ^ _cmp.GetHashCode(obj.Ext2Desc ?? "")*397) ^
                     _cmp.GetHashCode(obj.Ext1Simple ?? "")*397) ^ _cmp.GetHashCode(obj.Ext2Simple ?? "");
            }
        }
    }
}