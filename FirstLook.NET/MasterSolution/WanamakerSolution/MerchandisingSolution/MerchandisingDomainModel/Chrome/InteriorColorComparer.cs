using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Chrome
{
    public class InteriorColorComparer : IComparer<IInteriorColor>, IEqualityComparer<IInteriorColor>
    {
        private readonly StringComparer _cmp = StringComparer.InvariantCultureIgnoreCase;

        public int Compare(IInteriorColor x, IInteriorColor y)
        {
            if (ReferenceEquals(x, y))
                return 0;
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            return _cmp.Compare(x.Desc, y.Desc);
        }

        public bool Equals(IInteriorColor x, IInteriorColor y)
        {
            if (ReferenceEquals(x, y))
                return true;
            if (x == null || y == null)
                return false;
            return _cmp.Equals(x.Desc, y.Desc);
        }

        public int GetHashCode(IInteriorColor obj)
        {
            return _cmp.GetHashCode(obj.Desc ?? "");
        }
    }
}