﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
    internal partial class AnalyticsWrapper
    {
        private IDataSitePerformanceRepository GetSitePerformanceRepository()
        {
			return new DataSitePerformanceRepository(_dealerServices);
		}

        public List<SitePerformance> GetSitePerformance(int businessunitId, VehicleType vehicleType, DateTime start, DateTime end)
        {
            return GetSitePerformanceRepository().GetSitePerformance(businessunitId, vehicleType, start, end);
        }

        public Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime start, DateTime end)
        {
            return GetSitePerformanceRepository().GetGroupSitePerformance(groupId, vehicleType, start, end);
        }

        public List<SitePerformance> GetSitePerformanceTrend(int businessunitId, VehicleType vehicleType, DateTime start, DateTime end)
        {
            return GetSitePerformanceRepository().GetSitePerformanceTrends(businessunitId, vehicleType, start, end);
        }
	
    }
}
