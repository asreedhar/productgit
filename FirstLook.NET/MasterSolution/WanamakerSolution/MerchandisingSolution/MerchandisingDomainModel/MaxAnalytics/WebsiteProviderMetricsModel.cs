﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
    public interface IWebsiteProviderMetricsModel
    {
        List<WebsiteProvider> GetProviders( List<int> freeformProviderIDs = null );
        List<MonthlyWebsiteMetric> GetVDPs( int businessUnitID, int websiteProviderID, DateTime from, DateTime to );
        List<MonthlyWebsiteMetric> GetVDPs( int businessUnitID, DateTime from, DateTime to );
        int FindOrCreateFreeformProvider( string freeformWebsiteProviderName );
        void SetVDPs( int businessUnitID, int websiteProviderID, DateTime from, DateTime to, List<MonthlyWebsiteMetric> VDP_values );
        int? GetSelectedProviderID( int businessUnitID );
        void SetSelectedProviderID( int businessUnitID, int websiteProviderID );
        IEnumerable<EdtDestinations> GetGoogleVdpSites();
        IEnumerable<EdtDestinations> GetAvailableWebsiteDestinationIds();
    }
    
    public class WebsiteProviderMetricsModel : IWebsiteProviderMetricsModel
    {
        public List<WebsiteProvider> GetProviders( List<int> freeformProviderIDs = null )
        {
            var results = new List<WebsiteProvider>();

            var cn = Database.GetConnection( Database.MerchandisingDatabase );
            cn.Open();
            IDbCommand cmd;

            // always return curated ids
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [settings].[EdtDestinations] WHERE [isDealerWebsite] = 1";
            using( var r = cmd.ExecuteReader() )
            while( r.Read() )
            {
                results.Add( new WebsiteProvider {
                    WebsiteProviderID = (int) r["destinationID"],
                    Name = (string) r["description"],
                    IsFreeform = false
                });
            }
            cmd.Dispose();

            // also grab any freeform ids specified
            if( freeformProviderIDs != null && freeformProviderIDs.Count > 0 )
            {
                var provider_list = String.Join( ",", freeformProviderIDs );
                
                cmd = cn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [settings].[WebsiteProvidersFreeform] WHERE [WebsiteProviderID] IN ("+provider_list+")";
                using( var r = cmd.ExecuteReader() )
                while( r.Read() )
                {
                    results.Add( new WebsiteProvider {
                        WebsiteProviderID = (int) r["WebsiteProviderID"],
                        Name = (string) r["Name"],
                        IsFreeform = true
                    });
                }
                cmd.Dispose();
            }
            cn.Dispose();

            return results;
        }

        public List<MonthlyWebsiteMetric> GetVDPs( 
            int businessUnitID, int websiteProviderID,
            DateTime from, DateTime to )
        {
            var results = new List<MonthlyWebsiteMetric>();

            var cn = Database.GetConnection( Database.MerchandisingDatabase );
            var cmd = cn.CreateCommand();
            
            cn.Open();
            cmd.CommandType = CommandType.Text;
            if( websiteProviderID != -1 ) {
                cmd.CommandText = "SELECT * FROM [dashboard].[WebsiteMetrics] WHERE [BusinessUnitID] = @businessUnitID AND [WebsiteProviderID] = @websiteProviderID AND [MonthApplicable] >= @from AND [MonthApplicable] <= @to";
                cmd.AddRequiredParameter( "@websiteProviderID", websiteProviderID, DbType.Int32 );
            } else {
                cmd.CommandText = "SELECT * FROM [dashboard].[WebsiteMetrics] WHERE [BusinessUnitID] = @businessUnitID AND [MonthApplicable] >= @from AND [MonthApplicable] <= @to";
            }
            cmd.AddRequiredParameter( "@businessUnitID", businessUnitID, DbType.Int32 );
            cmd.AddRequiredParameter( "@from", from.ToString("yyyy/MM/01 00:00:00.000"), DbType.Date );
            cmd.AddRequiredParameter( "@to", to.ToString("yyyy/MM/01 00:00:00.000"), DbType.Date );
            using( var r = cmd.ExecuteReader() )
            while( r.Read() )
            {
                results.Add( new MonthlyWebsiteMetric {
                    BusinessUnitID = (int) r["BusinessUnitID"],
                    WebsiteProviderID = (int) r["WebsiteProviderID"],
                    MonthApplicable = (DateTime) r["MonthApplicable"],
                    UsedVDPs = (int) r["UsedVDPs"],
                    NewVDPs = (int) r["NewVDPs"]
                });
            }
            cmd.Dispose();
            cn.Dispose();

            return results;
        }

        public List<MonthlyWebsiteMetric> GetVDPs( 
            int businessUnitID,
            DateTime from, DateTime to )
        {
            return GetVDPs( businessUnitID, -1, from, to );
        }

        public int FindOrCreateFreeformProvider( string freeformWebsiteProviderName )
        {
            var cn = Database.GetConnection( Database.MerchandisingDatabase );
            cn.Open();
            IDbCommand cmd;

            // a freeform website provider name must be found or created, and then referenced.
            var websiteProviderID = 0;
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [settings].[WebsiteProvidersFreeform] WHERE [Name] = @name";
            cmd.AddRequiredParameter( "@name", freeformWebsiteProviderName, DbType.String );
            var r = cmd.ExecuteReader();
            if( r.Read() )
                websiteProviderID = (int) r["WebsiteProviderID"];
            r.Dispose();
            cmd.Dispose();
            // couldn't find, so create
            if( websiteProviderID == 0 )
            {
                cmd = cn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO [settings].[WebsiteProvidersFreeform] ([Name]) VALUES (@name)";
                cmd.AddRequiredParameter( "@name", freeformWebsiteProviderName, DbType.String );
                var returned_id = (int?) cmd.ExecuteScalar();
                cmd.Dispose();
                if( returned_id != null )
                    websiteProviderID = returned_id.Value;
                else 
                {
                    cmd = cn.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [settings].[WebsiteProvidersFreeform] WHERE [Name] = @name";
                    cmd.AddRequiredParameter( "@name", freeformWebsiteProviderName, DbType.String );
                    r = cmd.ExecuteReader();
                    r.Read();
                    websiteProviderID = (int) r["WebsiteProviderID"]; // this will (by design) throw an exception if it could not read
                    r.Dispose();
                    cmd.Dispose();
                }
            }
            return websiteProviderID;
        }

        public void SetVDPs( 
            int businessUnitID,
            int websiteProviderID,
            DateTime from,
            DateTime to,
            List<MonthlyWebsiteMetric> VDP_values )
        {
            if( VDP_values == null || VDP_values.Count == 0 )
                return;

            var cn = Database.GetConnection( Database.MerchandisingDatabase );
            cn.Open();
            IDbCommand cmd;

            // clear out VDP data for the same time frame
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE [dashboard].[WebsiteMetrics] WHERE BusinessUnitID = @businessUnitID AND WebsiteProviderID = @websiteProviderID AND MonthApplicable >= @from AND MonthApplicable <= @to";
            cmd.Parameters.Clear();
            cmd.AddRequiredParameter( "@businessUnitID", businessUnitID, DbType.Int32 );
            cmd.AddRequiredParameter( "@websiteProviderID", websiteProviderID, DbType.Int32 );
            cmd.AddRequiredParameter( "@from", from.ToString("yyyy/MM/01 00:00:00.000"), DbType.Date );
            cmd.AddRequiredParameter( "@to", to.ToString("yyyy/MM/01 00:00:00.000"), DbType.Date );
            cmd.ExecuteNonQuery();
            cmd.Dispose();

            // insert given data with the websiteProviderID
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO [dashboard].[WebsiteMetrics] ([BusinessUnitID],[WebsiteProviderID],[MonthApplicable],[UsedVDPs],[NewVDPs]) VALUES (@businessUnitID,@websiteProviderID,@monthApplicable,@usedVDPs,@newVDPs)";
            foreach( var metric in VDP_values )
            {
                cmd.Parameters.Clear();
                cmd.AddRequiredParameter( "@businessUnitID", businessUnitID, DbType.Int32 );
                cmd.AddRequiredParameter( "@websiteProviderID", websiteProviderID, DbType.Int32 );
                cmd.AddRequiredParameter( "@monthApplicable", metric.MonthApplicable.ToString("yyyy/MM/01 00:00:00.000"), DbType.Date );
                cmd.AddRequiredParameter( "@usedVDPs", metric.UsedVDPs, DbType.Int32 );
                cmd.AddRequiredParameter( "@newVDPs", metric.NewVDPs, DbType.Int32 );
                cmd.ExecuteNonQuery();
            }
            cmd.Dispose();

            cn.Dispose();
        }
    
        public int? GetSelectedProviderID( int businessUnitID )
        {
            var id = (int?)null;
            var cn = Database.GetConnection( Database.MerchandisingDatabase );
            var cmd = cn.CreateCommand();
            cn.Open();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT TOP 1 [SelectedWebsiteProvider] FROM [settings].[Merchandising] WHERE [businessUnitID] = @businessUnitID";
            cmd.AddRequiredParameter( "@businessUnitID", businessUnitID, DbType.Int32 );
            using( var r = cmd.ExecuteReader() )
            if( r.Read() )
            {
                var c = r.GetOrdinal("SelectedWebsiteProvider");
                if( !r.IsDBNull(c) )
                    id = r.GetInt32(c);
            }
            cmd.Dispose();
            cn.Dispose();
            return id;
        }

        public void SetSelectedProviderID( int businessUnitID, int websiteProviderID )
        {
            var cn = Database.GetConnection( Database.MerchandisingDatabase );
            var cmd = cn.CreateCommand();
            cn.Open();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE [settings].[Merchandising] SET [SelectedWebsiteProvider] = @websiteProviderID WHERE [businessUnitID] = @businessUnitID";
            cmd.AddRequiredParameter( "@businessUnitID", businessUnitID, DbType.Int32 );
            cmd.AddRequiredParameter( "@websiteProviderID", websiteProviderID, DbType.Int32 );
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            cn.Dispose();
        }

        public IEnumerable<EdtDestinations> GetGoogleVdpSites()
        {
            return EdtDestinationHelper.GetGoogleVdpSites();
        }

        public IEnumerable<EdtDestinations> GetAvailableWebsiteDestinationIds()
        {
            return EdtDestinationHelper.GetAvailableWebsiteDestinationIds();
        }
    }

    public class MonthlyWebsiteMetric
    {
        public int BusinessUnitID;
        public int WebsiteProviderID;
        public DateTime MonthApplicable;
        public int UsedVDPs;
        public int NewVDPs;
    }

    public class WebsiteProvider
    {
        public int WebsiteProviderID;
        public string Name;
        public bool IsFreeform;
    }
}
