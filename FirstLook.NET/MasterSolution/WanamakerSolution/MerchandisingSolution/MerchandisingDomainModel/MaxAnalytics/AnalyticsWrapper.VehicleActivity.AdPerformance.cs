﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
	internal partial class AnalyticsWrapper
	{
		public InventoryAdPerformace[] GetVehicleAdPerformace(int businessUnitId, int inventoryId)
		{
			var result = GetMaxDataVehicleAdPerformace(businessUnitId, inventoryId);

			return result;
		}

		private InventoryAdPerformace[] GetMaxDataVehicleAdPerformace(int businessUnitId, int inventoryId)
		{
			var dealer = _dealerServices.GetBusinessUnit(businessUnitId);
			var inventory = InventoryData.Fetch(businessUnitId, inventoryId);

			var postStores = new Dictionary<string, RequestModel[]>();
			postStores.Add("InventoryTuples", new[]
                                                  {
                                                      new RequestModel { Date = inventory.ReceivedDate.ToShortDateString(), Store = dealer.Code, Vin = inventory.VIN}
                                                  });

			string url = String.Format(VehicleActivityDetailsUrl, Settings.Default.MaxAnalytics);
			var model = MaxAnalyticsClient.PostModel<Dictionary<string, MaxDataActivity[]>>(url, postStores.ToJson());
			
			var inventoryAdPerformance = new List<InventoryAdPerformace>();

			if (model.ContainsKey(dealer.Code))
			{
				var preferred = PreferredProvider(model[dealer.Code]);
				foreach (var site in preferred)
				{
					foreach (var metric in site.VINS.First().Metrics) //only one vin
					{
						double clickThrough = metric.VehicleDetailPages/(double) metric.SearchPageResults;
						if (double.IsNaN(clickThrough) || double.IsPositiveInfinity(clickThrough) || double.IsNegativeInfinity(clickThrough))
							clickThrough = 0;

						inventoryAdPerformance.Add(new InventoryAdPerformace
							{
								DestinationId = (int) site.Site,
								Source = site.Site.ToDescription(),
								Date = metric.Date,
								SearchPageViews = metric.SearchPageResults,
								DetailedPageViews = metric.VehicleDetailPages,
								ClickThrough = clickThrough,
								RealPoint = true
							});

					}
				}
			}

			return inventoryAdPerformance.ToArray();
		}
	}
}
