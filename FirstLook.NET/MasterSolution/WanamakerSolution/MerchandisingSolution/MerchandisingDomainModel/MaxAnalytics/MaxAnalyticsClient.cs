﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Properties;
using MAX.Caching;
using MvcMiniProfiler;
using log4net;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{    
    public static class MaxAnalyticsClient
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MaxAnalyticsClient).FullName);
        
        private static readonly ICacheKeyBuilder CacheKeyBuilder = new CacheKeyBuilder();
        private static readonly ICacheWrapper CacheWrapper = new MemcachedClientWrapper();
        private const int CacheHours = 1;

        private static void SetBasicAuthHeader(WebRequest req)
        {
            string authInfo = string.Format("{0}:{1}", Settings.Default.MaxAnalyticsUser, Settings.Default.MaxAnalyticsPassword);
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            req.Headers["Authorization"] = "Basic " + authInfo;
        }

        // TODO: Add cache attribute
        public static TValue GetModel<TValue>(string url) where TValue : class
        {
            string methodName = MethodBase.GetCurrentMethod().Name;
            string typeName = typeof(TValue).Name;
            string cacheKey = CacheKeyBuilder.CacheKey( new[]{ methodName,typeName,url});

            // Check cache.
            var obj = CacheWrapper.Get<TValue>(cacheKey);
            if (obj != null)
            {
                Log.Debug("cache.hit");
                return obj;
            }
            Log.Debug("cache.miss");

	        using (MiniProfiler.Current.Step("Get " + url))
	        {
		        Log.InfoFormat("Start Analytics Get Request: {0}", url);
		        TValue model = null;
				
		        var request = WebRequest.Create(url) as HttpWebRequest;
		        request.Timeout = (int) TimeSpan.FromSeconds(20).TotalMilliseconds;

		        SetBasicAuthHeader(request);

		        using (MiniProfiler.Current.Step("Response"))
		        {
			        var requestTime = MiniProfiler.Current.Step("GetResponse()");
			        using (var response = request.GetResponse() as HttpWebResponse)
			        {
                        if(requestTime != null)
				            requestTime.Dispose();

				        using (var responseStream = response.GetResponseStream())
				        {
					        using (var textReader = new StreamReader(responseStream))
					        {
						        string json = null;
								Log.Info("Analytics Get Response Recieved");
						        using (MiniProfiler.Current.Step("ReadToEnd()"))
						        {
							        json = textReader.ReadToEnd();
						        }

								Log.DebugFormat("Analytics Get Response Content {0}", json);

						        using (MiniProfiler.Current.Step("FromJson()"))
						        {
							        model = json.FromJson<TValue>();
						        }
					        }
				        }
			        }
		        }
				
		        Log.DebugFormat("End Analytics Get Request: {0}", url);

                // Put it in the cache for an hour
                CacheWrapper.Set(cacheKey, model, DateTime.Now.AddHours(CacheHours));
		        return model;
	        }
        }

        // TODO: Add cache attribute
        public static TValue PostModel<TValue>(string url, string payload) where TValue : class
        {
            string methodName = MethodBase.GetCurrentMethod().Name;
            string typeName = typeof(TValue).Name;
            string cacheKey = CacheKeyBuilder.CacheKey(new[] { methodName, typeName, url, payload });

            // Check cache.
            var obj = CacheWrapper.Get(cacheKey);
            if (obj != null)
            {
                Log.Debug("cache.hit");
                return ((string)obj).FromJson<TValue>();
            }
            Log.Debug("cache.miss");

	        using (MiniProfiler.Current.Step("Post " + url))
	        {
				Log.InfoFormat("Start Analytics Post Request: {0}", url);
				Log.DebugFormat(" analytics payload: {0}", payload);

		        TValue model = null;

		        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
		        request.ServicePoint.Expect100Continue = false;
		        request.Method = "POST";
		        request.ContentType = "application/json";
                //the below adds the header [Accept-Encoding: gzip, deflate] so we get compressed responses and will automatically decompresses response.
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

		        SetBasicAuthHeader(request);

		        using (MiniProfiler.Current.Step("Request"))
		        {
			        using (var requestStream = request.GetRequestStream())
			        using (StreamWriter writer = new StreamWriter(requestStream))
			        {
				        writer.Write(payload);
			        }
		        }

		        using (MiniProfiler.Current.Step("Response"))
		        {
			        var responseTimer = MiniProfiler.Current.Step("GetResponse()");

			        using (var response = (HttpWebResponse) request.GetResponse())
			        {
                        if (responseTimer != null)
                            responseTimer.Dispose();

				        using (var responseStream = response.GetResponseStream())
				        {
					        using (var textReader = new StreamReader(responseStream))
					        {
						        string json = null;
								Log.Info("Analytics Post Response Recieved");
						        using (MiniProfiler.Current.Step("ReadToEnd()"))
						        {
							        json = textReader.ReadToEnd();
						            if (!string.IsNullOrWhiteSpace(json))
						            {
                                        var isset = CacheWrapper.Set(cacheKey, json, DateTime.Now.AddHours(CacheHours));
						            }
						        }

								Log.DebugFormat("Analytics Post Response Content {0}", json);

								using (MiniProfiler.Current.Step("FromJson()"))
						        {
							        model = json.FromJson<TValue>();
						        }
					        }
				        }
			        }
		        }

		        Log.DebugFormat("End Analytics Post Request: {0}", url);

		        return model;

	        }
        }
    }
}
