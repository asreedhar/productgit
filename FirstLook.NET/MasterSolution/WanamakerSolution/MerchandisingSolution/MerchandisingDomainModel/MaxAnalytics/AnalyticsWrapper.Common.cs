﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary;
using FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline.Client;
using log4net;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
	internal partial class AnalyticsWrapper : IMaxAnalytics
	{

		private static Comparer StringComparer = new Comparer();

		private IMemberContext _memberContext;
		private IDashboardBusinessUnitRepository _dealerServices;
		private IDataOnlineRepository _onlineRepository;
		private Context _dataContext;
		private Reports.VehicleActivity.Client.Context ActivityDataContext { get; set; }

		private readonly IInventoryCountDao _inventoryCountDao;

        private static readonly ILog Log = LogManager.GetLogger(typeof(AnalyticsWrapper).FullName);

		public AnalyticsWrapper(IMemberContext memberContext, IDashboardBusinessUnitRepository dealerServices, IDataOnlineRepository onlineRepository, IInventoryCountWebService inventoryCountWebservice)
		{
			_memberContext = memberContext;
			_dealerServices = dealerServices;
			_onlineRepository = onlineRepository;

			_dataContext = GetContext("Reports/VehiclesOnline.svc/", u => new Context(u));
			ActivityDataContext = GetVehicleActivityContext();

			_inventoryCountDao = new InventoryCountDao(inventoryCountWebservice, _dealerServices);
		}

		private bool UseMaxAnalyticsForGroup(int groupId, Func<int,bool> useForDataType)
		{
			var units = _dealerServices.GetBusinessUnitsfromGroup(groupId);
			return units.Any(unit => useForDataType(unit.BusinessUnitId));
		}

		private static IDataConnection GetConnection()
		{
			return Database.GetConnection(Database.MerchandisingDatabase);
		}

	

		/// <summary>
		/// Given sites with multiple providers per site, returns list with the preferred provider for each site
		/// e.g. given provider:site, use AutoTrader:AutoTrader over Fetch:AutoTrader
		/// </summary>
		public static IList<PROVIDERSITE> PreferredProvider<PROVIDERSITE>(IList<PROVIDERSITE> list) where PROVIDERSITE : ProviderSite
		{
			var preferred = new Dictionary<Site, PROVIDERSITE>();

            // Create initial mapping.  This prefers sites that have the site and provider set to the same value.
            // ie.  Cars-Cars or Autotrader-Autotrader

            list.Where(e => (int)e.Provider == (int)e.Site)	// where the site is the provider
                .ToList()
                .ForEach(e => preferred[e.Site] = e);	// this is the preffered provider

			// Prefer carsApi over cars for cars.com
            foreach (var ps in list)	
			{
				if (ps.Provider == Provider.carsApi)
                {
                    preferred.Remove(ps.Site);
                    preferred.Add(ps.Site, ps);
                }
			}
			
			return preferred.Values.ToList();

		}
	}
}
