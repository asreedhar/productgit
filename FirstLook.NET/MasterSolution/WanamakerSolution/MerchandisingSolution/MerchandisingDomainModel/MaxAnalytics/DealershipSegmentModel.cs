﻿using System.Data;
using FirstLook.Merchandising.DomainModel.Commands;
using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
    public interface IDealershipSegmentModel
    {
        DealershipSegment get( int businessUnitID );
        void set( int businessUnitID, DealershipSegment dealershipSegment );
    }

    public class DealershipSegmentModel : IDealershipSegmentModel
    {
        public DealershipSegment get( int businessUnitID )
        {
            var dealershipSegment = DealershipSegment.Undefined;
            using( var cn = Database.GetConnection( Database.MerchandisingDatabase ))
            using( var cmd = cn.CreateCommand() )
            {
                cn.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT TOP 1 [DealershipSegment] FROM [settings].[Merchandising] WHERE [businessUnitID] = @businessUnitID";
                cmd.AddRequiredParameter( "@businessUnitID", businessUnitID, DbType.Int32 );
                using( var r = cmd.ExecuteReader() )
                if( r.Read() )
                {
                    var c = r.GetOrdinal("DealershipSegment");
                    dealershipSegment = r.IsDBNull(c)? DealershipSegment.Undefined : (DealershipSegment)r.GetInt32(c);
                }
            }
            return dealershipSegment;
        }

        public void set( int businessUnitID, DealershipSegment dealershipSegment )
        {
            using( var cn = Database.GetConnection( Database.MerchandisingDatabase ))
            using( var cmd = cn.CreateCommand() )
            {
                cn.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "UPDATE [settings].[Merchandising] SET [DealershipSegment] = @dealershipSegment WHERE [businessUnitID] = @businessUnitID";
                cmd.AddRequiredParameter( "@businessUnitID", businessUnitID, DbType.Int32 );
                cmd.AddRequiredParameter( "@dealershipSegment", (int)dealershipSegment, DbType.Int32 );
                cmd.ExecuteNonQuery();
                GetMiscSettings.InvalidateCache( businessUnitID );
            }
        }
    }
}
