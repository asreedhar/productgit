﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using MAX.Entities.Enumerations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus
{
    [JsonConverter(typeof(AdStatusResultJsonConverter))]
    public class AdStatusResult : IAdStatusResult
    {
        public GidProvider Provider { get; set; }
        public Site Site { get; set; }
        public ReportDataSource DataSource { get; set; }
        public List<IMaxAnalyticsAd> Ads { get; set; } 
    }

	/// <summary>
    /// Using a custom json converter because we need to set some Enum values and I have to parse that out from the Description attribute.
    /// </summary>
    public class AdStatusResultJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var adResult = value as AdStatusResult;
            if (adResult != null)
            {
                writer.WriteStartObject();
                writer.WritePropertyName("Provider");
                serializer.Serialize(writer, adResult.Provider.ToString());
                writer.WritePropertyName("Site");
                serializer.Serialize(writer, adResult.Site.ToString());
                writer.WritePropertyName("Ads");
                writer.WriteStartArray();
                foreach (var ad in adResult.Ads)
                {
                    serializer.Serialize(writer, ad);
                }
                writer.WriteEndArray();
                writer.WriteEndObject();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var adResult = new AdStatusResult();

            var obj = JObject.Load(reader);
            if (obj["Provider"] != null)
            {
                GidProvider provider;
                Enum.TryParse(obj["Provider"].ToString(), out provider);
                adResult.Provider = provider;
            }
            if (obj["Site"] != null)
            {
                Site site;
                Enum.TryParse(obj["Site"].ToString(), out site);
                adResult.Site = site;
            }
            if (obj["Ads"] != null)
            {
                var goodAds = new List<IMaxAnalyticsAd>();
                var ads = obj["Ads"];
                foreach (var jTokenAd in ads)
                {
                    var ad = JObject.Load(jTokenAd.CreateReader());
                    var thisAd = new MaxAnalyticsAd();

                    if (ad["VIN"] != null) thisAd.VIN = ad["VIN"].ToObject<string>();
                    if (ad["Price"] != null) thisAd.Price = ad["Price"].ToObject<int>();
                    if (ad["Description"] != null) thisAd.Description = ad["Description"].ToString();
                    if (ad["PhotoUrls"] != null) thisAd.PhotoUrls = ad["PhotoUrls"].ToObject<List<string>>();
                    if (ad["StatusDate"] != null)
                    {
                        DateTime statusDateTime;
                        if (DateTime.TryParse(ad["StatusDate"].ToString(), out statusDateTime))
                        {
                            thisAd.StatusDate = statusDateTime;
                        }
                        else
                        {
                            thisAd.StatusDate = DateTime.Today;
                        }
                    }

                    goodAds.Add(thisAd);
                }
                adResult.Ads = goodAds;
            }

            return adResult;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(AdStatusResult);
        }
    }
}
