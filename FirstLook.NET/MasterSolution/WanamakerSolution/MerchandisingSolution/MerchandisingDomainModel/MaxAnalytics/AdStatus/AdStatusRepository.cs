﻿using System.Collections.Generic;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Properties;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus
{
    internal class AdStatusRepository : IAdStatusRepository
    {
        private const string AdStatusUrl = @"{0}reporting/adstatus?&format=json";

        public IEnumerable<IAdStatusResult> GetAdStatusResults(IAdStatusArgs args)
        {
            var url = string.Format(AdStatusUrl, Settings.Default.MaxAnalytics);
            return MaxAnalyticsClient.PostModel<List<AdStatusResult>>(url, args.ToJson());
        }
    }

	public class AdStatusArgs : IAdStatusArgs
	{
		public string Store { get; set; }
		// ReSharper disable InconsistentNaming
		public IEnumerable<string> vins { get; set; }
		// ReSharper restore InconsistentNaming
	}

	public interface IAdStatusArgs
	{
		string Store { get; set; }
	}

    public interface IAdStatusRepository
    {
        IEnumerable<IAdStatusResult> GetAdStatusResults(IAdStatusArgs args);
    }
}
