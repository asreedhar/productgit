﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus
{
    public class RequestModel
    {
        private List<string> _vins = new List<string>(); 
        
        public string Store;
        public List<string> Vins 
        {
            get { return _vins; }
            set { _vins = value; }
        }
    }
}
