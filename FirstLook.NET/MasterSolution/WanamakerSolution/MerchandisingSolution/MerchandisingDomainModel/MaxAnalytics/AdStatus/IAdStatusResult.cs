﻿using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus
{
	public interface IAdStatusResult
	{
		GidProvider Provider { get; set; }
		Site Site { get; set; }
        ReportDataSource DataSource { get; set; }
	}
}