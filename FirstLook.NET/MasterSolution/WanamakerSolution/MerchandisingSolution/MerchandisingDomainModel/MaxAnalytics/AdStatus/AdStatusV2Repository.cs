﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Properties;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus
{
	public class AdStatusV2Repository : IAdStatusRepository
	{
        private const string AdStatusUrl = @"{0}reporting/adstatus2?&format=json";

        public IEnumerable<IAdStatusResult> GetAdStatusResults(IAdStatusArgs args)
        {
			var url = string.Format(AdStatusUrl, Settings.Default.MaxAnalytics);
			return MaxAnalyticsClient.PostModel<List<AdStatusResultV2>>(url, args.ToJson());
        }
	}

	public class AdStatusV2Args : IAdStatusArgs
	{
		public string Store { get; set; }
		public int MinPhotoCount { get; set; }
		public IEnumerable<AdStatusVehicleTuples> InventoryTuples { get; set; } 
	}

	public class AdStatusVehicleTuples
	{
		private DateTime? _endDate;
		public string Vin { get; set; }
		public DateTime StartDate { get; set; }

		public DateTime EndDate
		{
			get
			{
				if (_endDate.HasValue)
					return _endDate.Value;
				return DateTime.Now;
			}
			set { _endDate = value; }
		}
	}
}