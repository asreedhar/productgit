using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus
{
    public class MaxAnalyticsAd : IMaxAnalyticsAd
    {
        public DateTime StatusDate;
        public string VIN { get; set; }
        public int Price;
        public List<string> PhotoUrls;
        public string Description;
    }

	public interface IMaxAnalyticsAd
	{
		string VIN { get; set; }
	}

	public class MaxAnalyticsDates : IMaxAnalyticsAd
	{
		public string VIN { get; set; }
		public DateTime? FirstPhotoDate { get; set; }
        public DateTime? FirstRequiredPhotoCountDate { get; set; }
		public DateTime? FirstNonZeroPriceDate { get; set; }
        public DateTime? FirstAdDate { get; set; }
	}
}