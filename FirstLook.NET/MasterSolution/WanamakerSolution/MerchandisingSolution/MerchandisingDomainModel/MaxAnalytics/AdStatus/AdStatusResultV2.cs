﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using MAX.Entities.Enumerations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus
{
	[JsonConverter(typeof(AdStatusResultV2JsonConverter))]
	public class AdStatusResultV2 : IAdStatusResult
	{
		public GidProvider Provider { get; set; }
		public Site Site { get; set; }
	    public ReportDataSource DataSource { get; set; }
	    public List<MaxAnalyticsDates> Ads { get; set; } 
	}


	public class AdStatusResultV2JsonConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var adResult = value as AdStatusResultV2;
			if (adResult != null)
			{
				writer.WriteStartObject();
				writer.WritePropertyName("Provider");
				serializer.Serialize(writer, adResult.Provider.ToString());
				writer.WritePropertyName("Site");
				serializer.Serialize(writer, adResult.Site.ToString());
				writer.WritePropertyName("Ads");
				writer.WriteStartArray();
				foreach (var ad in adResult.Ads)
				{
					serializer.Serialize(writer, ad);
				}
				writer.WriteEndArray();
				writer.WriteEndObject();
			}
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var adResult = new AdStatusResultV2();

			var obj = JObject.Load(reader);
            if (obj["Provider"] != null)
		    {
		        ReportDataSource dataSource;
		        Enum.TryParse(obj["Provider"].ToString(), out dataSource);
		        adResult.DataSource = dataSource;
                // Don't want to break callers who might need GIDProvider
		        var provider = GetGidProvider(dataSource);
		        adResult.Provider = provider;
		    }
			if (obj["Site"] != null)
			{
				Site site;
				Enum.TryParse(obj["Site"].ToString(), out site);
				adResult.Site = site;
			}
			if (obj["Ads"] != null)
			{
				var goodAds = new List<MaxAnalyticsDates>();
				var ads = obj["Ads"];
				foreach (var jTokenAd in ads)
				{
					var ad = JObject.Load(jTokenAd.CreateReader());
					var thisAd = new MaxAnalyticsDates();

					if (ad["VIN"] != null) thisAd.VIN = ad["VIN"].ToObject<string>();
					var dateFields = new[] { "FirstPhotoDate", "FirstAdDate", "FirstRequiredPhotoCountDate", "FirstNonZeroPriceDate" };
					foreach (var dateField in dateFields.Where(dateField => ad[dateField] != null))
					{
						DateTime statusDateTime;
						if (DateTime.TryParse(ad[dateField].ToString(), CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.AdjustToUniversal, out statusDateTime))
						{
							statusDateTime = statusDateTime.ToUniversalTime();
							var property = thisAd.GetType().GetProperty(dateField);
							property.SetValue(thisAd, statusDateTime, null);
						}
					}
					goodAds.Add(thisAd);
				}
				adResult.Ads = goodAds;
			}

			return adResult;
		}

	    private static GidProvider GetGidProvider(ReportDataSource dataSource)
	    {
	        GidProvider provider;
	        switch (dataSource)
	        {
	            case ReportDataSource.Aultec:
	                provider = MAX.Entities.Enumerations.GidProvider.Aultec;
	                break;
	            case ReportDataSource.eBiz:
	                provider = MAX.Entities.Enumerations.GidProvider.eBiz;
	                break;
	            case ReportDataSource.CarsApi:
	                provider = MAX.Entities.Enumerations.GidProvider.Unknown;
	                break;
	            default:
	                provider = MAX.Entities.Enumerations.GidProvider.Unknown;
	                break;
	        }
	        return provider;
	    }

	    public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(AdStatusResultV2);
		}
	}
}