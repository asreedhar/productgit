﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
	internal partial class AnalyticsWrapper
	{
		public void GetInventoryCounts(List<SitePerformance> sitePerformanceList, int businessUnitId, VehicleType vehicleType, DateRange dateRange)
		{
			_inventoryCountDao.GetInventoryCounts(sitePerformanceList, businessUnitId, vehicleType, dateRange);
		}

		public void GetGroupInventoryCounts(Dictionary<int, List<SitePerformance>> dictionary, int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
		{
			_inventoryCountDao.GetGroupInventoryCounts(dictionary, groupId, vehicleType, startDate, endDate);
		}
	}
}
