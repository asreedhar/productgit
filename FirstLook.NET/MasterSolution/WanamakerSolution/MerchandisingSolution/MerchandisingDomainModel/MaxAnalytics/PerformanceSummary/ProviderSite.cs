﻿using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using Newtonsoft.Json;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary
{
    internal class ProviderSite
    {
        [JsonConverter(typeof(CustomEnumConverter))]
        public Site Site { get; set; }

        [JsonConverter(typeof(CustomEnumConverter))]
        public Provider Provider { get; set; }
    }
}
