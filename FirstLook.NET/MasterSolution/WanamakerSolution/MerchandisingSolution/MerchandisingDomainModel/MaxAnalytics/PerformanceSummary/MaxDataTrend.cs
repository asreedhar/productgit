﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary
{
    internal class MaxDataTrend
    {
        public DateTime Month { get; set; }

        public int SearchResultPages { get; set; }
        public int VehicleDetailPages { get; set; }
        public int EmailLeads { get; set; }
        public int MapsViewed { get; set; }
        public int AdPrints { get; set; }
        public int PhoneLeads { get; set; }
        public int ChatRequests { get; set; }
    }
}
