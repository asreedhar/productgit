﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using Newtonsoft.Json;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary
{
    internal class MaxDataSitePerformance : ProviderSite
    {
        public int SearchResultPages { get; set; }
        public int VehicleDetailPages { get; set; }
        public int EmailLeads { get; set; }
        public int MapsViewed { get; set; }
        public int AdPrints { get; set; }
        public int PhoneLeads { get; set; }
        public int ChatRequests { get; set; }
        public DateTime Month { get; set; }
    }
}
