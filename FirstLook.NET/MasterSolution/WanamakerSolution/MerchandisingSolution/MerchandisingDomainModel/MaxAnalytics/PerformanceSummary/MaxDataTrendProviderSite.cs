﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary
{
    internal class MaxDataTrendProviderSite : ProviderSite
    {
        public MaxDataTrend[] Stats { get; set; }
    }
}
