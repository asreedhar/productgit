﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary
{
    public interface IDataSitePerformanceRepository
    {
        List<SitePerformance> GetSitePerformance(int businessunitId, VehicleType vehicleType, DateTime start, DateTime end);
        Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime start, DateTime end);

        List<SitePerformance> GetSitePerformanceTrends(int businessUnitId, VehicleType vehicleType, DateTime startDate, DateTime endDate);
    }
}
