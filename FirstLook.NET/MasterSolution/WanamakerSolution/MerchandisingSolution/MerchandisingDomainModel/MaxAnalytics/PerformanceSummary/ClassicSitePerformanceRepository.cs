﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary
{
    internal class ClassicSitePerformanceRepository : IDataSitePerformanceRepository
    {
        public List<SitePerformance> GetSitePerformanceTrends(int businessUnitId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            var result = new List<SitePerformance>();

            using (var cn = GetConnection())
            using (var cmd = cn.CreateCommand())
            {
                if (cn.State != ConnectionState.Open)
                    cn.Open();

                cmd.CommandText = "dashboard.GetMonthlySitePerformanceData";
                cmd.CommandType = CommandType.StoredProcedure;
                Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "VehicleType", (int)vehicleType, DbType.Int32);
                Database.AddRequiredParameter(cmd, "StartDate", startDate, DbType.DateTime);
                Database.AddRequiredParameter(cmd, "EndDate", endDate, DbType.DateTime);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var sp = new SitePerformance();

                        Site destination = new Site();
                        destination.Id = reader.GetInt32(reader.GetOrdinal("DestinationId"));
                        destination.Name = reader.GetString(reader.GetOrdinal("DestinationName"));
                        sp.Site = destination;

                        sp.SearchPageViewCount = reader.GetInt32(reader.GetOrdinal("SearchPageViewCountInPeriod"));
                        sp.DetailPageViewCount = reader.GetInt32(reader.GetOrdinal("DetailPageViewCountInPeriod"));
                        sp.MapsViewedCount = reader.GetInt32(reader.GetOrdinal("MapsViewedCountInPeriod"));
                        sp.PhoneLeadsCount = reader.GetInt32(reader.GetOrdinal("PhoneLeadsCountInPeriod"));
                        sp.AdPrintedCount = reader.GetInt32(reader.GetOrdinal("AdPrintedCountInPeriod"));
                        sp.EmailLeadsCount = reader.GetInt32(reader.GetOrdinal("EmailLeadsCountInPeriod"));
                        sp.ChatRequestsCount = reader.GetInt32(reader.GetOrdinal("ChatRequestsCountInPeriod"));

                        sp.DateRange = DateRange.MonthOfDate(DateTime.SpecifyKind(reader.GetDateTime(reader.GetOrdinal("Month")), DateTimeKind.Unspecified));
                        // Trend data is returned one month at a time.
                        sp.Months = 1;
                        result.Add(sp);
                    }
                }
            }

            return result;
        }

        public List<SitePerformance> GetSitePerformance(int businessunitId, VehicleType vehicleType, DateTime start, DateTime end)
        {
            var sitePerformanceList = new List<SitePerformance>();

            using (IDataConnection con = GetConnection())
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.GetSitePerformanceData";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessunitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "VehicleType", (int)vehicleType, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "StartDate", start, DbType.DateTime);
                    Database.AddRequiredParameter(cmd, "EndDate", end, DbType.DateTime);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var sp = new SitePerformance();

                            Site destination = new Site();
                            destination.Id = reader.GetInt32(reader.GetOrdinal("DestinationId"));
                            destination.Name = reader.GetString(reader.GetOrdinal("DestinationName"));
                            sp.Site = destination;

                            sp.SearchPageViewCount = reader.GetInt32(reader.GetOrdinal("SearchPageViewCountInPeriod"));
                            sp.DetailPageViewCount = reader.GetInt32(reader.GetOrdinal("DetailPageViewCountInPeriod"));
                            sp.MapsViewedCount = reader.GetInt32(reader.GetOrdinal("MapsViewedCountInPeriod"));
                            sp.PhoneLeadsCount = reader.GetInt32(reader.GetOrdinal("PhoneLeadsCountInPeriod"));
                            sp.AdPrintedCount = reader.GetInt32(reader.GetOrdinal("AdPrintedCountInPeriod"));
                            sp.EmailLeadsCount = reader.GetInt32(reader.GetOrdinal("EmailLeadsCountInPeriod"));
                            sp.ChatRequestsCount = reader.GetInt32(reader.GetOrdinal("ChatRequestsCountInPeriod"));

                            sitePerformanceList.Add(sp);
                        }
                    }
                }
            }

            return sitePerformanceList;
        }

        private IDataConnection GetConnection()
        {
            return Database.GetConnection(Database.MerchandisingDatabase);
        }

        public Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime start, DateTime end)
        {
            var dictionary = new Dictionary<int, List<SitePerformance>>();

            using (IDataConnection con = GetConnection())
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.GetSitePerformanceData#Group";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "groupID", groupId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "VehicleType", (int)vehicleType, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "StartDate", start, DbType.DateTime);
                    Database.AddRequiredParameter(cmd, "EndDate", end, DbType.DateTime);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var sp = new SitePerformance();

                            Site destination = new Site();
                            destination.Id = reader.GetInt32(reader.GetOrdinal("DestinationId"));
                            destination.Name = reader.GetString(reader.GetOrdinal("DestinationName"));
                            sp.Site = destination;

                            var businessunitid = reader.GetInt32(reader.GetOrdinal("BusinessunitID"));
                            var businessUnit = reader.GetString(reader.GetOrdinal("Businessunit"));
                            if (!dictionary.ContainsKey(businessunitid))
                                dictionary.Add(businessunitid, new List<SitePerformance>());

                            sp.BusinessUnitId = businessunitid;
                            sp.BusinessUnit = businessUnit;
                            sp.SearchPageViewCount = reader.GetInt32(reader.GetOrdinal("SearchPageViewCountInPeriod"));
                            sp.DetailPageViewCount = reader.GetInt32(reader.GetOrdinal("DetailPageViewCountInPeriod"));
                            sp.MapsViewedCount = reader.GetInt32(reader.GetOrdinal("MapsViewedCountInPeriod"));
                            sp.PhoneLeadsCount = reader.GetInt32(reader.GetOrdinal("PhoneLeadsCountInPeriod"));
                            sp.AdPrintedCount = reader.GetInt32(reader.GetOrdinal("AdPrintedCountInPeriod"));
                            sp.EmailLeadsCount = reader.GetInt32(reader.GetOrdinal("EmailLeadsCountInPeriod"));
                            sp.ChatRequestsCount = reader.GetInt32(reader.GetOrdinal("ChatRequestsCountInPeriod"));
                            sp.DateRange = DateRange.MonthOfDate(reader.GetDateTime(reader.GetOrdinal("Month")));
                            
                            // This trend data is always returned one month at a time.
                            sp.Months = 1;
                            dictionary[businessunitid].Add(sp);
                        }
                    }
                }
            }

            return dictionary;
        }

        public Dictionary<string, MaxDataTrendProviderSite[]> GetGroupSitePerformanceTrend(int groupId, VehicleType vehicleType, DateRange dateRange) 
        {
            throw new NotImplementedException();
        }
    }
}
