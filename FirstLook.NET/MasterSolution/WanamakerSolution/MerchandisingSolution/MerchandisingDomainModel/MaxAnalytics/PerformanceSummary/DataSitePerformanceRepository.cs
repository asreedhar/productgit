﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary
{
    internal class DataSitePerformanceRepository : IDataSitePerformanceRepository
    {
        private IDashboardBusinessUnitRepository _dealerServices;
        private const string performanceUrl = "{0}reporting/siteperformance/?BeginMonth={1}&BeginYear={2}&EndMonth={3}&EndYear={4}&InventoryType={5}&ShowTrendDetail={6}&format=json";

        private static string GetPerformanceUrl(DateRange dateRange, VehicleType vehicleType, bool showTrend, string[] stores = null)
        {
            string url = String.Format(performanceUrl,
                Settings.Default.MaxAnalytics, dateRange.StartDate.Month, dateRange.StartDate.Year, dateRange.EndDate.Month, dateRange.EndDate.Year, vehicleType.StringValue, showTrend
            );

            if (stores != null && stores.Length > 0)
            {
                url += "&Stores=" + String.Join(",", stores);
            }

            return url;
        }

        public DataSitePerformanceRepository(IDashboardBusinessUnitRepository dealerServices)
        {
            _dealerServices = dealerServices;
        }

        private static ProviderSite[] FilterList(IList<ProviderSite> filteredList)
        {
            //favor direct feeds. We can only have one feed per site. Favor Provider:AutoTrader and Provider:Cars, and Provider:carsApi
            var directs = filteredList.Where(x => (int)x.Provider == (int)x.Site || (int) x.Provider == 6).ToArray();
            
            foreach (var direct in directs)
            {
                bool bCarsApi = false;
                var sites = filteredList.Where(x => x.Site == direct.Site).ToArray();
                if (sites.Length >= 2) //if more sites
                {
                    foreach (var site in sites)
                    {
                        if ((int)site.Provider != (int)site.Site && site.Provider != Provider.carsApi) //remove ones that aren't equal but don't remove carsApi
                            filteredList.Remove(site);
                        if (site.Provider == Provider.carsApi) bCarsApi = true;
                    }
                }
                // if we still have more than one site and this is for Cars.com, remove the cars direct feed and keep carsApi
                if (sites.Length >= 2)
                {
                    foreach (var site in sites)
                    {
                        if ( site.Provider == Provider.Cars && bCarsApi ) 
                            filteredList.Remove(site);
                    }
                }
            }

            return filteredList.ToArray();
        }

        public List<SitePerformance> GetSitePerformance(int businessunitId, VehicleType vehicleType, DateTime start, DateTime end)
        {
            var dealer = _dealerServices.GetBusinessUnit(businessunitId);

            string url = GetPerformanceUrl(new DateRange(start, end), vehicleType, false, new string[] { dealer.Code });

            var model = MaxAnalyticsClient.GetModel<Dictionary<string, MaxDataSitePerformance[]>>(url);
            var returnModel = new List<SitePerformance>();
            if (model == null) //if we have no data.
                return returnModel;

            //only want one site.
            model[dealer.Code] = FilterList(model[dealer.Code].Cast<ProviderSite>().ToList())
                .Cast<MaxDataSitePerformance>().ToArray();

            foreach(var m in model[dealer.Code])
            {
                returnModel.Add(new SitePerformance()
                                    {
                                        Site = new Reports.SitePerformance.Site() {Id = (int)m.Site, Name = m.Site.ToDescription()},
                                        BusinessUnit = dealer.Name,
                                        BusinessUnitId = businessunitId,
                                        AdPrintedCount = m.AdPrints,
                                        SearchPageViewCount = m.SearchResultPages,
                                        DetailPageViewCount = m.VehicleDetailPages,
                                        ChatRequestsCount = m.ChatRequests,
                                        MapsViewedCount = m.MapsViewed,
                                        EmailLeadsCount = m.EmailLeads,
                                        PhoneLeadsCount = m.PhoneLeads,
                                        DateRange = new DateRange(start, end)
                                    });
            }

            return returnModel;
        }

        public Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime start, DateTime end)
        {
            var units = _dealerServices.GetBusinessUnitsfromGroup(groupId);
          
            var postStores = new Dictionary<string, string[]>();
            postStores.Add("Stores", units.Select(x => x.Code).ToArray());

            string url = GetPerformanceUrl(new DateRange(start, end), vehicleType, true);

            var returnModel = new Dictionary<int, List<SitePerformance>>();
            var model = MaxAnalyticsClient.PostModel<Dictionary<string, MaxDataTrendProviderSite[]>>(url, postStores.ToJson());
            if (model == null) //no data returned from service
                return returnModel;
            
            foreach(string dealerKey in model.Keys.ToArray()) //.ToArray avoids collection modified exceptions when filtering multiple sites
            {
                //only want one site.
                model[dealerKey] = FilterList(model[dealerKey].Cast<ProviderSite>().ToList())
                .Cast<MaxDataTrendProviderSite>().ToArray();

                var unit = units.Where(x => x.Code == dealerKey).Single();
                var sitePerformances = new List<SitePerformance>();
                // Loop through the dealers
                foreach (var m in model[dealerKey])
                {
                    if (m == null || m.Stats == null)
                        continue;

                    // Loop through the sites
                    m.Stats.ToList().ForEach( stat => {
                        SitePerformance sp = sitePerformances.FirstOrDefault( s => s.Site.Id == (int)m.Site && s.DateRange == DateRange.MonthOfDate(stat.Month) );
                        if(sp == null)
                        { 
                            sp = new SitePerformance()
                            {
                                Site = new Reports.SitePerformance.Site() { Id = (int)m.Site, Name = m.Site.ToDescription() },
                                BusinessUnitId = unit.BusinessUnitId,
                                BusinessUnit = unit.Name,
                                DateRange = DateRange.MonthOfDate( stat.Month )
                            };
                        }

                        sp.AdPrintedCount = stat.AdPrints;
                        sp.SearchPageViewCount = stat.SearchResultPages;
                        sp.DetailPageViewCount = stat.VehicleDetailPages;
                        sp.ChatRequestsCount = stat.ChatRequests;
                        sp.MapsViewedCount = stat.MapsViewed;
                        sp.EmailLeadsCount = stat.EmailLeads;
                        sp.PhoneLeadsCount = stat.PhoneLeads;

                        sitePerformances.Add(sp);
                    });
                }

                returnModel.Add(unit.BusinessUnitId, sitePerformances);
            }

            return returnModel;
        }

        public Dictionary<string, MaxDataTrendProviderSite[]> GetGroupSitePerformanceTrend(int groupId, VehicleType vehicleType, DateRange dateRange)
        {
            var units = _dealerServices.GetBusinessUnitsfromGroup(groupId);
            
            var postStores = new Dictionary<string, string[]>();
            postStores.Add("Stores", units.Select(x => x.Code).ToArray());

            string url = GetPerformanceUrl(dateRange, vehicleType, true);

            var returnModel = PostSitePerformanceTrend(url, postStores);

            return returnModel;
        }

        private Dictionary<string, MaxDataTrendProviderSite[]> PostSitePerformanceTrend(string url, Dictionary<string, string[]> postStores)
        {
            return MaxAnalyticsClient.PostModel<Dictionary<string, MaxDataTrendProviderSite[]>>(url, postStores.ToJson());
        }

        
        public List<SitePerformance> GetSitePerformanceTrends(int businessUnitId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            var dealer = _dealerServices.GetBusinessUnit(businessUnitId);

            DateRange dateRange = new DateRange(startDate, endDate);
            Dictionary<string, MaxDataTrendProviderSite[]> usedModel = null;
            Dictionary<string, MaxDataTrendProviderSite[]> newModel = null;

            var dic = new Dictionary<OnlineModel.Site, List<SitePerformance>>();

            if (vehicleType.Equals(VehicleType.Used) || vehicleType.Equals(VehicleType.Both))
            {
                string usedUrl = GetPerformanceUrl(dateRange, VehicleType.Used, true, new string[] { dealer.Code });
                usedModel = MaxAnalyticsClient.GetModel<Dictionary<string, MaxDataTrendProviderSite[]>>(usedUrl);
                //only want one site.
                if (usedModel != null)
                {
                    usedModel[dealer.Code] = FilterList(usedModel[dealer.Code].Cast<ProviderSite>().ToList())
                       .Cast<MaxDataTrendProviderSite>().ToArray();
                    
                    MapModelsToSitePerformance(dealer, usedModel, dic);
                }
            }

            if (vehicleType.Equals(VehicleType.New) || vehicleType.Equals(VehicleType.Both))
            {
                string newUrl = GetPerformanceUrl(dateRange, VehicleType.New, true, new string[] { dealer.Code });
                newModel = MaxAnalyticsClient.GetModel<Dictionary<string, MaxDataTrendProviderSite[]>>(newUrl);

                if (newModel != null)
                {
                    //only want one site.
                    newModel[dealer.Code] = FilterList(newModel[dealer.Code].Cast<ProviderSite>().ToList())
                      .Cast<MaxDataTrendProviderSite>().ToArray();
                    MapModelsToSitePerformance(dealer, newModel, dic);
                }
            }

            if (usedModel == null && newModel == null)
                return new List<SitePerformance>();

            return dic.Select(x => x.Value).SelectMany(x => x).ToList();
        }

        private static void MapModelsToSitePerformance(IDashboardBusinessUnit dealer, Dictionary<string, MaxDataTrendProviderSite[]> maxModel, Dictionary<OnlineModel.Site, List<SitePerformance>> dic)
        {
            foreach (var m in maxModel[dealer.Code])
            {
                foreach (var stat in m.Stats)
                {
                    SitePerformance sp = (dic.Keys.Contains(m.Site)) ? dic[m.Site].Where(x => x.DateRange.StartDate == stat.Month).FirstOrDefault() : null;
                    if (sp == null)
                    {
                        sp = new SitePerformance()
                        {
                            BusinessUnitId = dealer.BusinessUnitId,
                            BusinessUnit = dealer.Name,
                            DateRange = DateRange.MonthOfDate(DateTime.SpecifyKind(stat.Month, DateTimeKind.Unspecified)),
                            Months = 1,
                            Site = new Reports.SitePerformance.Site() { Id = (int)m.Site, Name = m.Site.ToDescription() }
                        };
                    }
                    
                    sp.AdPrintedCount += stat.AdPrints;
                    sp.ChatRequestsCount += stat.ChatRequests;
                    sp.EmailLeadsCount += stat.EmailLeads;
                    sp.MapsViewedCount += stat.MapsViewed;
                    sp.PhoneLeadsCount += stat.PhoneLeads;
                    sp.SearchPageViewCount += stat.SearchResultPages;
                    sp.DetailPageViewCount += stat.VehicleDetailPages;

                    if (!dic.ContainsKey(m.Site))
                    {
                        // Add a new list to the dictionary if it doesn't exist.
                        var list = new List<SitePerformance>();
                        list.Add(sp);
                        dic.Add(m.Site, list);
                    }
                    else if(!dic[m.Site].Contains(sp))
                    {
                        // only add a new SP to the list if it does not already exist because we are editing list items in place here.
                        dic[m.Site].Add(sp);
                    }
                }
            }
        }
    }
}
