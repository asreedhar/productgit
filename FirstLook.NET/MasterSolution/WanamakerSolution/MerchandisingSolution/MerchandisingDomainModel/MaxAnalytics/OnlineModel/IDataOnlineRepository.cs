﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Release;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    internal interface IDataOnlineRepository
    {
        int GetOnlineStaleDays();
        Dictionary<int,ListingSitePreferences> GetDealerInventoryListingPreferences(int businessUnitId);
        IVehiclesOnline[] GetVehicleInfo(int businessunitId, VehicleType usedOrNew, int minAge);
        MaxDataOnline GetOnlineVehicles(string dealerCode);
    }
}
