﻿namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    // TODO: We really don't need two of these. Just forces us to map between this one and GidProvider, whose values differ :(
    // Also, note how ProviderSite uses CustomEnumConverter to parse strings to these enum values. I suspect this is based
    // on string matching, but am not sure. Be careful renaming these.
    internal enum Provider
    {
        Unknown = 0,
        Cars = 1,
        AutoTrader = 2,
        Fetch = 3,
        Aultec = 4,
        eBiz = 5,
        carsApi = 6
    }
}
