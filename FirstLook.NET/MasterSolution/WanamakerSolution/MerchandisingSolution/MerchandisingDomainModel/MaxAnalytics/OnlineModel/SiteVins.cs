﻿using System;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    internal class SiteVins : ProviderSite
    {
        public bool HasData { get; set; }
        public DateTime ObservationTime { get; set; }
        public string[] Vins { get; set; }
    }
}
