﻿namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    internal class MaxDataOnline
    {
        public string[] Stores { get; set; }
        public SiteVins[] SiteVins { get; set; }
    }
}
