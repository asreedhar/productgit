﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    internal class CustomEnumConverter : StringEnumConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            object returnValue;
            try
            {
                returnValue = base.ReadJson(reader, objectType, existingValue, serializer);
            }
            catch (Exception) //if we can't parse the enum then we return Unknown.
            {
                returnValue = Enum.ToObject(objectType, 0);
            }

            return returnValue;
        }

    }
}
