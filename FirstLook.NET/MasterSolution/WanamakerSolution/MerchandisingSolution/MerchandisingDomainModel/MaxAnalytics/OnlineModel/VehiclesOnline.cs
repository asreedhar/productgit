﻿using System;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    public class VehiclesOnline : IVehiclesOnline
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string StockNumber { get; set; }
        public string Vin { get; set; }
        public decimal? InternetPrice { get; set; }
        public DateTime InventoryReceivedDate { get; set; }
        public int VehicleYear { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Trim { get; set; }
        public byte InventoryType { get; set; }
        public bool Certified { get; set; }
        public bool Approved { get; set; }

        public bool HasAtData { get; set; }
        public bool AtOnline { get; set; }

        public bool HasCarsData { get; set; }
        public bool CarsOnline { get; set; }

        public bool AtSent { get; set; }
        public bool CarsSent { get; set; }

        public string ZipCode { get; set; }

        public bool Online { get; set; }
        public bool HasData { get; set; }

       
    }
}
