﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    public static class SiteExtensions
    {
        public static string ToDescription(this Enum value)
        {
            // Get the Description attribute value for the enum value
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
           
            return string.Empty;
        }
    }

    public enum Site
    {
        [Description("Unknown")]
        Unknown = 0,
        
        [Description("Cars.com")]
        Cars = 1,
        
        [Description("AutoTrader.com")]
        AutoTrader = 2,

        [Description("eBiz")]
        eBiz = 3,

		[Description("RESERVED")]
		Reserved = 4	// we don't have a third provider yet, 
						// but reserve slot to maintain Site=Provider (int) enum relationship when we do add one
						// which is handy when trying to determine perferred provider when we have multiple for a site e.g. see PreferredProvider()
    }
}
