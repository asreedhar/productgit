﻿using System;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    public interface IVehiclesOnline
    {
        int BusinessUnitId { get; set; }
        int InventoryId { get; set; }
        string StockNumber { get; set; }
        string Vin { get; set; }
        decimal? InternetPrice { get; set; }
        DateTime InventoryReceivedDate { get; set; }
        int VehicleYear { get; set; }
        string Make { get; set; }
        string Model { get; set; }
        string Trim { get; set; }
        byte InventoryType { get; set; }
        bool Certified { get; set; }
        bool Approved { get; set; }

        //Is it online somewhere?
        bool Online { get; set; }
        bool HasData { get; set; }

        bool HasAtData { get; set; }
        bool AtOnline { get; set; }
        bool AtSent { get; set; }

        bool HasCarsData { get; set; }
        bool CarsOnline { get; set; }
        bool CarsSent { get; set; }

        string ZipCode { get; set; }
    }
}
