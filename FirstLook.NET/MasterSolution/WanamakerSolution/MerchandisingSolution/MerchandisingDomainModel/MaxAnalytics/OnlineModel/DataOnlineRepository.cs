﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using FirstLook.Common.Core.Extensions;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Release;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    internal class DataOnlineRepository : IDataOnlineRepository
    {
        public int GetOnlineStaleDays()
        {
            //move to stored proc
            int staleDays = -1;
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "settings.Environment#Fetch";
                    cmd.AddRequiredParameter("@SettingName", "VehicleOnlineStaleDataDays", DbType.String);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException("Expected Stale Days");

                        staleDays = int.Parse(reader.GetString(reader.GetOrdinal("SettingValue")));
                    }
                }
            }

            return staleDays;
        }

        // TODO: Do we need this any more?
        public Dictionary<int, ListingSitePreferences> GetDealerInventoryListingPreferences(int businessUnitId)
        {
            var prefs = ListingSitePreferences.FetchAll( 
                businessUnitId, 
                new List<int>{ 
                    (int)EdtDestinations.AutoTrader, 
                    (int)EdtDestinations.CarsDotCom
                });
            return prefs;
        }

        /// <summary>
        /// Returns the active inventory for the specified business unit.  Excludes inventory marked "do not post".
        /// </summary>
        /// <param name="businessunitId"></param>
        /// <param name="usedOrNew"></param>
        /// <param name="minAge"></param>
        /// <returns></returns>
        public IVehiclesOnline[] GetVehicleInfo(int businessunitId, VehicleType usedOrNew, int minAge)
        {
            var vehicleList = new List<IVehiclesOnline>();
            var bu = BusinessUnitFinder.Instance().Find(businessunitId);
            
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "workflow.GetVehiclesOnlineInfo";
                    cmd.AddRequiredParameter("@businessUnitId", businessunitId, DbType.Int32);
                    cmd.AddRequiredParameter("@usedOrNew", usedOrNew.IntValue, DbType.Int16);
                    cmd.AddRequiredParameter("@minAgeInDays", minAge, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));
                            var inventoryID = reader.GetInt32(reader.GetOrdinal("InventoryId"));
                            var stockNumber = reader.GetString(reader.GetOrdinal("StockNumber"));
                            var vin = reader.GetString(reader.GetOrdinal("Vin"));
                            var inventoryReceivedData = reader.GetDateTime(reader.GetOrdinal("InventoryReceivedDate"));
                            var vehicleYear = reader.GetInt32(reader.GetOrdinal("VehicleYear"));
                            string make = reader.GetString(reader.GetOrdinal("Make"));
                            string model = reader.GetString(reader.GetOrdinal("Model"));
                            string trim = reader.GetString(reader.GetOrdinal("Trim"));
                            byte inventoryType = reader.GetByte(reader.GetOrdinal("InventoryType"));
                            byte certified = reader.GetByte(reader.GetOrdinal("Certified"));
                            bool approved = reader.GetBoolean(reader.GetOrdinal("Approved"));

                            decimal? internetPrice = null; 
                            if (!reader.IsDBNull("InternetPrice"))
                                internetPrice = reader.GetDecimal(reader.GetOrdinal("InternetPrice"));

                            vehicleList.Add( new VehiclesOnline
                            {
                                BusinessUnitId = businessUnitId,
                                InventoryId = inventoryID,
                                StockNumber = stockNumber,
                                Vin = vin,
                                InternetPrice = internetPrice,
                                InventoryReceivedDate = inventoryReceivedData,
                                VehicleYear = vehicleYear,
                                Make = make,
                                Model = model,
                                Trim = trim,
                                InventoryType = inventoryType,
                                Certified = (certified != 0),
                                Approved = approved,
                                ZipCode = bu.ZipCode
                            });
                        }
                    }

                    return vehicleList.ToArray();
                }
            }
        }

        /// <summary>
        /// Call the max analytics "vehicles online" web service for the specified store.
        /// </summary>
        /// <param name="dealerCode"></param>
        /// <returns></returns>
        public MaxDataOnline GetOnlineVehicles(string dealerCode)
        {
            MaxDataOnline model = null;
            
            //we only have a self signed cert now in prod. This can be removed when we get a real one.
            ServicePointManager.ServerCertificateValidationCallback = ((object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => true);

            string url = string.Format("{0}reporting/vehiclesonline/?store={1}&format=json", Settings.Default.MaxAnalytics, dealerCode);
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.Timeout = (int)TimeSpan.FromSeconds(20).TotalMilliseconds;

            string authInfo = string.Format("{0}:{1}", Settings.Default.MaxAnalyticsUser, Settings.Default.MaxAnalyticsPassword);
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;

            // TODO: Call this async. No need to block.
            using (var response = request.GetResponse() as HttpWebResponse)
            using (var responseStream = response.GetResponseStream())
            using (var textReader = new StreamReader(responseStream))
            {
                string json = textReader.ReadToEnd();
                model = json.FromJson<MaxDataOnline>();
            }
            
            return model;
        }

    }
}
