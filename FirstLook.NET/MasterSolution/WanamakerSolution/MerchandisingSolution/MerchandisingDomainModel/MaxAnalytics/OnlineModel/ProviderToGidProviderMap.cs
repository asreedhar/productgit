﻿using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel
{
    internal static class ProviderToGidProviderMap
    {
        /// <summary>
        /// Sigh. Someone didn't think to make the Provider & GidProvider enums have the same values -
        /// or better yet, get rid of one of them - so we have to map them. 
        /// TODO: Get rid of one of these enums.  We don't need two. This forces us to make code changes
        /// as we add providers.
        /// </summary>
        /// <returns></returns>
        internal static bool AreEquivalent(GidProvider gidProvider, Provider provider)
        {
            if (gidProvider == GidProvider.Aultec) return provider == Provider.Aultec;
            if (gidProvider == GidProvider.eBiz) return provider == Provider.eBiz;

            // all others "gid providers" are faux providers (cars & autotrader are "direct feeds"). 
            return false;
        }
    }
}
