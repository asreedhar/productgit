﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Reports.VehicleActivity.Client;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
	/// <summary>
	/// "Low Activity" report DAOs
	/// </summary>
	internal partial class AnalyticsWrapper
	{
		private const string VehicleActivityUrl = @"{0}reporting/vehicleactivitysplit?&format=json";
		private const string VehicleActivityDetailsUrl = @"{0}reporting/vehicleactivity?&format=json";

		public List<VehicleActivity> GetLowActivity(int businessUnitId, int? inventoryType, int? minAge)
		{
			var result = GetMaxDataActivity(businessUnitId, inventoryType, minAge);

			return result;
		}

		private List<VehicleActivity> GetMaxDataActivity(int businessUnitId, int? inventoryType, int? minAge)
		{
			var dealer = _dealerServices.GetBusinessUnit(businessUnitId);
			var vehicleActivity = GetVehicleActivityInfo(businessUnitId, inventoryType, minAge);

			var fetchModelList = new List<ActivitySplitRequestModel>();
			foreach (var activity in vehicleActivity)
			{
				var date = activity.InventoryReceivedDate.ToShortDateString();
				fetchModelList.Add(new ActivitySplitRequestModel()
				{
				    Store = dealer.Code, 
                    Vin = activity.VIN, 
                    StartDate = date, 
                    SplitDate = activity.LastRepriceDate.ToShortDateString()
				});
			}

		    var model = new Dictionary<string, MaxDataActivitySplit[]> ();
		    // Don't make the request without a model list. Causes errors on our side and on maxanalytics.
            if (fetchModelList.Any())
		    {
                var postStores = new Dictionary<string, ActivitySplitRequestModel[]>();
                postStores.Add("InventoryTuples", fetchModelList.ToArray());
		        try
		        {
		            string url = String.Format(VehicleActivityUrl, Settings.Default.MaxAnalytics);
		            // TODO: Wrap this (all?) call to MaxAnalyticsClient.PostModel in a try/catch to keep 400s from throwing exceptions 
		            model = MaxAnalyticsClient.PostModel<Dictionary<string, MaxDataActivitySplit[]>>(url, postStores.ToJson());
		        }
		        catch (WebException webException)
		        {
                    using (var response = webException.Response)
                    {
                        var httpResponse = (HttpWebResponse)response;
                        // Swallow 400 errors the service returns for no inventory found.
                        if(httpResponse.StatusCode != HttpStatusCode.BadRequest)
                        {
                            var errorText = string.Empty;
                            using (var data = response.GetResponseStream())
                            {
                                if (data != null)
                                {
                                    using (var reader = new StreamReader(data))
                                    {
                                        errorText = reader.ReadToEnd();
                                    }
                                }
                            }
                            Log.Fatal(String.Format("Max Analytics service returned an unexpected error. {0}", errorText), webException);
                        }
                    }
		        }
		    }
		    var list = new List<VehicleActivity>();

			if (!model.ContainsKey(dealer.Code))
			{
				// no data
				return list;
			}

			var preferredSites = PreferredProvider(model[dealer.Code].ToList<ProviderSite>());

            // FB:28400: make sure all activity has both dest 1 (cars) and 2 (autoTrader) for vehicle activity report which is a crosstab of this data
            // this is to mimic the cartesion join in the view used for legacy data (dataServices), tureen 12/12/2013
            bool bCarsDotCom = false; // dest = 1
		    bool bAutoTrader = false; // dest - 2

			foreach (var activity in vehicleActivity)
			{
				foreach (MaxDataActivitySplit site in preferredSites)
				{
					var newActivity = Clone(activity);
					list.Add(newActivity);

					newActivity.DestinationID = (int)site.Site;

                    if (newActivity.DestinationID == 1)
                        bCarsDotCom = true;
                    else if (newActivity.DestinationID == 2)
                        bAutoTrader = true;
                           

					//newActivity.SearchCount = site.VinActivitySplits.Where(x => x.Vin == activity.VIN).Select(x => x.Metrics.SearchPageResults);

                    
					newActivity.SearchCount = site.VinActivitySplits
					                              .Where(x => x.Vin == activity.VIN)
					                              .Select(x => x.Metrics.SearchPageResults)
					                              .DefaultIfEmpty(0).FirstOrDefault();

					newActivity.DetailsPageViewCount = site.VinActivitySplits
					                                       .Where(x => x.Vin == activity.VIN)
					                                       .Select(x => x.Metrics.VehicleDetailPages)
					                                       .DefaultIfEmpty(0)
					                                       .FirstOrDefault();

					newActivity.ActionCount = site.VinActivitySplits
						.Where(x => x.Vin == activity.VIN)
						.Select(x => x.Metrics.ActionCount)
						.DefaultIfEmpty(0)
						.FirstOrDefault();

					var searchCountBeforeLastReprice = site.VinActivitySplits
					                                       .Where(x => x.Vin == activity.VIN)
					                                       .Select(x => x.Metrics.PreSplitSearchPageResults)
					                                       .DefaultIfEmpty(0)
					                                       .FirstOrDefault();

					var detailedCountBeforeLastReprice = site.VinActivitySplits
					                                         .Where(x => x.Vin == activity.VIN)
					                                         .Select(x => x.Metrics.PreSplitVehicleDetailPages)
					                                         .DefaultIfEmpty(0)
					                                         .FirstOrDefault();


					newActivity.SearchPageViewsSinceLastReprice = newActivity.SearchCount - searchCountBeforeLastReprice;
					newActivity.DetailPageViewsSinceLastReprice = newActivity.DetailsPageViewCount - detailedCountBeforeLastReprice;

					newActivity.CTRSinceLastReprice = 0;
					if (newActivity.SearchPageViewsSinceLastReprice > 0)
						newActivity.CTRSinceLastReprice = newActivity.DetailPageViewsSinceLastReprice / (float)newActivity.SearchPageViewsSinceLastReprice;

					newActivity.CTR = 0;
					if (newActivity.SearchCount.Value > 0)
						newActivity.CTR = newActivity.DetailsPageViewCount / (float)newActivity.SearchCount;

                    newActivity.EmailCount = site.VinActivitySplits
                                                 .Where(x => x.Vin == activity.VIN)
                                                 .Select(x => x.Metrics.PostSplitEmailLeads)
					                             .DefaultIfEmpty(0)
					                             .FirstOrDefault();
                    newActivity.MapCount = site.VinActivitySplits
                                               .Where(x => x.Vin == activity.VIN)
                                               .Select(x => x.Metrics.PostSplitMapsViewed)
					                           .DefaultIfEmpty(0)
					                           .FirstOrDefault();
                    newActivity.PrintCount = site.VinActivitySplits
                                                 .Where(x => x.Vin == activity.VIN)
                                                 .Select(x => x.Metrics.PostSplitAdPrints)
					                             .DefaultIfEmpty(0)
					                             .FirstOrDefault();
				}

                // FB:28400: add blank activity if not found in analytic data
                if(!bCarsDotCom)
                {
                    var newActivity = Clone(activity);
                    list.Add(newActivity);
                    newActivity.DestinationID = 1;
                }

                if (!bAutoTrader)
                {
                    var newActivity = Clone(activity);
                    list.Add(newActivity);
                    newActivity.DestinationID = 2;
                }

			}

			return list;
		}

	
		private VehicleActivity[] GetVehicleActivityInfo(int businessUnitIdParameter, int? inventoryType, int? minAge)
		{
			int newUsedInventory = inventoryType ?? 0;
			int minAgeValue = minAge ?? 0;

			var vehicleList = new List<VehicleActivity>();
			using (var con = Database.GetConnection(Database.MerchandisingDatabase))
			{
				con.Open();
				using (var cmd = con.CreateCommand())
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = "Reports.GetVehicleActivityInfo";
					cmd.AddRequiredParameter("@businessUnitId", businessUnitIdParameter, DbType.Int32);
					cmd.AddRequiredParameter("@usedOrNew", newUsedInventory, DbType.Int16);
					cmd.AddRequiredParameter("@minAgeInDays", minAgeValue, DbType.Int32);

					using (var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							var businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));
							var inventoryID = reader.GetInt32(reader.GetOrdinal("InventoryId"));
							var vin = reader.GetString(reader.GetOrdinal("Vin"));
							var stockNumber = reader.GetString(reader.GetOrdinal("StockNumber"));

							var year = reader.GetInt32(reader.GetOrdinal("Year"));
							string make = reader.GetString(reader.GetOrdinal("Make"));
							string model = reader.GetString(reader.GetOrdinal("Model"));
							string trim = reader.GetString(reader.GetOrdinal("Trim"));

							var mileage = reader.GetInt32(reader.GetOrdinal("Mileage"));
							var ageInDays = reader.GetInt32(reader.GetOrdinal("AgeInDays"));
							var usedNew = reader.GetByte(reader.GetOrdinal("InventoryType"));

                            decimal? internetPrice = null; // FB: 28341 -- check for nulls
							if(!reader.IsDBNull("InternetPrice"))
                                internetPrice = reader.GetDecimal(reader.GetOrdinal("InternetPrice"));

							decimal? pctAvgMarketPrice = null;
							if (!reader.IsDBNull(reader.GetOrdinal("PctAvgMarketPrice")))
								pctAvgMarketPrice = reader.GetDecimal(reader.GetOrdinal("PctAvgMarketPrice"));

							int? avgMarketPrice = null;
							if (!reader.IsDBNull(reader.GetOrdinal("AvgMarketPrice")))
								avgMarketPrice = reader.GetInt32(reader.GetOrdinal("AvgMarketPrice"));

							DateTime? approvalDate = null;
							if (!reader.IsDBNull(reader.GetOrdinal("ApprovalDate")))
								approvalDate = reader.GetDateTime(reader.GetOrdinal("ApprovalDate"));

							var inventoryReceivedDate = reader.GetDateTime(reader.GetOrdinal("InventoryReceivedDate"));
							var lastRepriceDate = reader.GetDateTime(reader.GetOrdinal("LastRepriceDate"));

							var lowActivityThreshold = reader.GetFloat(reader.GetOrdinal("LowActivityThreshold"));
							var highActivityThreshold = reader.GetFloat(reader.GetOrdinal("HighActivityThreshold"));
							var minSearchCountThreshold = reader.GetInt32(reader.GetOrdinal("MinSearchCountThreshold"));

							vehicleList.Add(new VehicleActivity()
							{
								BusinessUnitID = businessUnitId,
								InventoryID = inventoryID,
								VIN = vin,
								StockNumber = stockNumber,
								Year = year,
								Make = make,
								Model = model,
								Trim = trim,
								Mileage = mileage,
								AgeInDays = ageInDays,
								InventoryType = usedNew,
								InternetPrice = internetPrice,
								PctAvgMarketPrice = pctAvgMarketPrice,
								AvgMarketPrice = avgMarketPrice,
								ApprovalDate = approvalDate,
								LowActivityThreshold = lowActivityThreshold,
								HighActivityThreshold = highActivityThreshold,
								MinSearchCountThreshold = minSearchCountThreshold,
								InventoryReceivedDate = inventoryReceivedDate,
								LastRepriceDate = lastRepriceDate
							});
						}
					}
				}
			}


			return vehicleList.ToArray();
		}

		private static VehicleActivity Clone(VehicleActivity activity)
		{
			return new VehicleActivity()
			{
				BusinessUnitID = activity.BusinessUnitID,
				InventoryID = activity.InventoryID,
				VIN = activity.VIN,
				StockNumber = activity.StockNumber,
				Year = activity.Year,
				Make = activity.Make,
				Model = activity.Model,
				Trim = activity.Trim,
				Mileage = activity.Mileage,
				AgeInDays = activity.AgeInDays,
				InventoryType = activity.InventoryType,
				InternetPrice = activity.InternetPrice,
				PctAvgMarketPrice = activity.PctAvgMarketPrice,
				AvgMarketPrice = activity.AvgMarketPrice,
				ApprovalDate = activity.ApprovalDate,
				LowActivityThreshold = activity.LowActivityThreshold,
				HighActivityThreshold = activity.HighActivityThreshold,
				MinSearchCountThreshold = activity.MinSearchCountThreshold,
				InventoryReceivedDate = activity.InventoryReceivedDate,
				LastRepriceDate = activity.LastRepriceDate
			};
		}



		private static IQueryable<VehicleActivity> SetupWebServiceFilters(int? inventoryTypeFilter, int? minAgeFilter, IQueryable<VehicleActivity> query)
		{
			if (inventoryTypeFilter.HasValue)
				query = query.Where(va => va.InventoryType == inventoryTypeFilter.Value);

			if (minAgeFilter.HasValue)
				query = query.Where(va => va.AgeInDays >= minAgeFilter.Value);

			return query;
		}
	}
}
