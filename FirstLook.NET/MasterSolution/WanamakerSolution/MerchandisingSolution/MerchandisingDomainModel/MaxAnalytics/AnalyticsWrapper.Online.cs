﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Properties;
using MAX.Entities;
using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
    internal partial class AnalyticsWrapper
    {
        // TODO: Why is activity stuff even in here? I thought this was the wrapper for "vehicle online"
        private static Reports.VehicleActivity.Client.Context GetVehicleActivityContext()
        {
            var baseUri = new Uri(Settings.Default.DataWebServices, UriKind.Absolute);
            var vehicleActivityUri = new Uri(baseUri, "Reports/VehicleActivity.svc/");
            return new Reports.VehicleActivity.Client.Context(vehicleActivityUri);
        }

        /// <summary>
        /// Return the max analytics "vehicle online" data
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <returns></returns>
        private MaxDataOnline FetchMaxOnlineData(int businessUnitId)
        {

            var unit = _dealerServices.GetBusinessUnit(businessUnitId);
			var maxDataOnline = _onlineRepository.GetOnlineVehicles(unit.Code);

            return maxDataOnline;
        }

        /// <summary>
        /// Modify the activeInventory objects passed in with the correct values for HasData, Online,
        /// HasAtData, AtOnline, AtSent, HasCarsData, CarsOnline, CarsSent.
        /// </summary>
        /// <param name="businessUnitId">The dealer</param>
        /// <param name="activeInventory">Active posted inventory.</param>
        /// <param name="maxDataOnline">Online data from max analytics</param>
        /// <param name="newGidProvider">Who is the dealer's new GID provider?</param>
        /// <param name="usedGidProvider">Who is the dealer's used GID provider?</param>
        internal void MergeWithMaxData(int businessUnitId, IEnumerable<IVehiclesOnline> activeInventory,
            MaxDataOnline maxDataOnline, GidProvider newGidProvider, GidProvider usedGidProvider)
        {
            MergeWithMaxData(businessUnitId, activeInventory, maxDataOnline, newGidProvider, usedGidProvider, ReportDataSource.None, ReportDataSource.None);
        }

        /// <summary>
        /// Modify the activeInventory objects passed in with the correct values for HasData, Online,
        /// HasAtData, AtOnline, AtSent, HasCarsData, CarsOnline, CarsSent.
        /// </summary>
        /// <param name="businessUnitId">The dealer</param>
        /// <param name="activeInventory">Active posted inventory.</param>
        /// <param name="maxDataOnline">Online data from max analytics</param>
        /// <param name="newGidProvider">Who is the dealer's new GID provider?</param>
        /// <param name="usedGidProvider">Who is the dealer's used GID provider?</param>
        /// <param name="newReportDataSource">Who is the dealer's new GID provider?</param>
        /// <param name="usedReportDataSource">Who is the dealer's used GID provider?</param>
        internal void MergeWithMaxData(int businessUnitId, IEnumerable<IVehiclesOnline> activeInventory,
            MaxDataOnline maxDataOnline, GidProvider newGidProvider, GidProvider usedGidProvider,
            ReportDataSource newReportDataSource, ReportDataSource usedReportDataSource)
        {

            foreach (var inventory in activeInventory)
            {
                // Don't capture the loop variable in a closure in the subsequent LINQ expressions.
                IVehiclesOnline inventoryClosureCopy = inventory;

                // Assume we don't have any data and the car is not online anywhere.
                inventory.Online = inventory.HasData =
                inventory.HasAtData = inventory.AtOnline = inventory.AtSent =
                inventory.HasCarsData = inventory.CarsOnline = inventory.CarsSent 
                = false;

                // The GIDProvider setting is no longer used for vehicle online reports.
                //GidProvider gidProvider = GetGidProvider(inventory, newGidProvider, usedGidProvider);
                ReportDataSource reportDataSource = GetReportDataSource(inventory, newReportDataSource,
                    usedReportDataSource);
                var provider = GetProvider(reportDataSource);

                // If we don't know the GID provider, the car doesn't "have data". Its "online" state is technically unknown,
                // but I think it's safe to call it "offline".  Calling code should inspect HasData first, and if it's false,
                // the Online value should not be trusted.  Online should probably be a bool?, but I'd break callers
                // if I did that.
                if (provider == Provider.Unknown)
                {
                    inventory.HasData = false;
                    inventory.Online = false;
                }
                else
                {
                    // We know the GID provider (or we have carsApi set up for this store), the car "has data".
                    // TODO: Perhaps HasData should be renamed something like HasGidProvider, since that's what it means.
                    inventory.HasData = true;

                    // If the vin is found from any site from the provider, it's "online". else it's "not online"
                    var isVinFoundAnywhereFromReportDataSource =
                            maxDataOnline.SiteVins.Any(x => x.Provider == provider &&
                                                            x.Vins.Contains(inventoryClosureCopy.Vin, StringComparer));
                    inventory.Online = isVinFoundAnywhereFromReportDataSource;
                }
     
                // Site-specific "has data", "online", and "sent' (deprecated) are only derived from direct-feeds.
                // If we need to take into account the dealer listing preferences logic(where do we think we send new, used, certified) -
                // an earlier implementation of that logic can be found in this file's history. I'd prefer to implement it as a 
                // private method that builds an additional Func<SiteVins,bool> predicate, which we can then tack on to to
                // these LINQ expressions as an additional Where() clause.
                var isVinFoundInAutoTraderDirect =
                    maxDataOnline.SiteVins
                                 .Where(x => x.Provider == Provider.AutoTrader && x.Site == Site.AutoTrader)
                                 .Any(x => x.Vins.Contains(inventoryClosureCopy.Vin, StringComparer));
                inventory.HasAtData = isVinFoundInAutoTraderDirect;
                inventory.AtOnline = isVinFoundInAutoTraderDirect;
                inventory.AtSent = isVinFoundInAutoTraderDirect;

                var isVinFoundInCarsDirect =
                    maxDataOnline.SiteVins
                                 .Where(x => x.Provider == Provider.Cars && x.Site == Site.Cars)
                                 .Any(x => x.Vins.Contains(inventoryClosureCopy.Vin, StringComparer));
                inventory.HasCarsData = isVinFoundInCarsDirect;
                inventory.CarsOnline = isVinFoundInCarsDirect;
                inventory.CarsSent = isVinFoundInCarsDirect;
            }   
        }

        private Provider GetProvider(ReportDataSource reportDataSource)
        {
            var provider = Provider.Unknown;

            switch (reportDataSource)
            {
                case ReportDataSource.Aultec:
                    provider = Provider.Aultec;
                    break;
                case ReportDataSource.eBiz:
                    provider = Provider.eBiz;
                    break;
                case ReportDataSource.CarsApi:
                    provider = Provider.carsApi;
                    break;
                default:
                    provider = Provider.Unknown;
                    break;
            }
            return provider;
        }

        /// <summary>
        /// Determine the GID provider based on whether the inventory is used or new.
        /// Return GidProvider.None if we don't know.
        /// </summary>
        /// <param name="inventoryRecord"></param>
        /// <param name="newGidProvider"></param>
        /// <param name="usedGidProvider"></param>
        internal static GidProvider GetGidProvider(IVehiclesOnline inventoryRecord, GidProvider newGidProvider,
            GidProvider usedGidProvider)
        {
            var gidProvider = GidProvider.None;
            switch (inventoryRecord.InventoryType)
            {
                case (int) VehicleTypeEnum.New:
                    gidProvider = newGidProvider;
                    break;
                case (int) VehicleTypeEnum.Used:
                    gidProvider = usedGidProvider;
                    break;
            }
            return gidProvider;
        }

        /// <summary>
        /// Determine the report data source based on whether the inventory is used or new.
        /// Return GidProvider.Unknown if we don't know.
        /// </summary>
        /// <param name="inventoryRecord"></param>
        /// <param name="newSource"></param>
        /// <param name="usedSource"></param>
        internal static ReportDataSource GetReportDataSource(IVehiclesOnline inventoryRecord, ReportDataSource newSource,
            ReportDataSource usedSource)
        {
            var dataSource = ReportDataSource.None;
            switch (inventoryRecord.InventoryType)
            {
                case (int)VehicleTypeEnum.New:
                    dataSource = newSource;
                    break;
                case (int)VehicleTypeEnum.Used:
                    dataSource = usedSource;
                    break;
            }
            return dataSource;
        }

        private static T GetContext<T>(string relativeUrl, Func<Uri, T> creator)
        {
            var baseUri = new Uri(Settings.Default.DataWebServices, UriKind.Absolute);
            var svcUri = new Uri(baseUri, relativeUrl);
            return creator(svcUri);
        }

        

        /// <summary>
        /// Call max analytics to get the vehicle online data. Returns data for all active inventory
        /// that is not marked "do not post". Returns null if no "vehicle online" data found in max analytics.
        /// </summary>
        /// <param name="businessunitId"></param>
        /// <param name="inventoryType"></param>
        /// <param name="minAge"></param>
        /// <returns></returns>
        public IVehiclesOnline[] GetOnlineVehicles(int businessunitId, int? inventoryType, int? minAge)
        {
            var onlineData = FetchMaxOnlineData(businessunitId);
            int newUsedInventory = inventoryType ?? 0;
            int minAgeValue = minAge ?? 20000;

            IVehiclesOnline[] activeInventory = _onlineRepository.GetVehicleInfo(businessunitId, newUsedInventory, minAgeValue);
            if (onlineData == null) return activeInventory;

            // Who is GID for this dealer?
            var settings = GetMiscSettings.GetSettings(businessunitId);
            var newGidProvider = settings.NewGidProvider;
            var usedGidProvider = settings.UsedGidProvider;

            // And the report data source?
            var newReportDataSource = settings.ReportDataSourceNew;
            var usedReportDataSource = settings.ReportDataSourceUsed;

            MergeWithMaxData(businessunitId, activeInventory, onlineData, newGidProvider, usedGidProvider, newReportDataSource, usedReportDataSource);

            return activeInventory;
        }

        // TODO: Why is this defined here? It doesn't seem to do anything different from the normal stringcomparison
        private class Comparer : IEqualityComparer<string>
        {
            public bool Equals(string x, string y)
            {
                return String.Compare(x, y, true) == 0;
            }

            public int GetHashCode(string obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}
