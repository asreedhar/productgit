﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Reports.VehicleActivity.Client;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics
{
    public interface IMaxAnalytics : IInventoryCountDao
    {
        IVehiclesOnline[] GetOnlineVehicles(int businessunitId, int? inventoryType, int? minAge);

        List<VehicleActivity> GetLowActivity(int businessUnitId, int? inventoryType, int? minAge);
        InventoryAdPerformace[] GetVehicleAdPerformace(int businessUnitId, int inventoryId);
		
        List<SitePerformance> GetSitePerformance(int businessunitId, VehicleType vehicleType, DateTime start, DateTime end);
        Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime start, DateTime end);

        List<SitePerformance> GetSitePerformanceTrend(int businessunitId, VehicleType vehicleType, DateTime start, DateTime end);
    }
}
