﻿namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity
{
	internal class VinActivitySplit
	{
		public string Vin { get; set; }
		public ActivityMetricsSplit Metrics { get; set; }
	}
}