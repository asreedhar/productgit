﻿using System;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity
{
    internal class ActivityMetrics
    {
        public DateTime Date { get; set; }
        public int SearchPageResults { get; set; }
        public int VehicleDetailPages { get; set; }
        public int EmailLeads { get; set; }
        public int MapsViewed { get; set; }
        public int AdPrints { get; set; }

        public int ActionCount
        {
            get { return EmailLeads + MapsViewed + AdPrints; }
        }
    }

	internal class ActivityMetricsSplit
	{
		public int SearchPageResults { get; set; }
		public int VehicleDetailPages { get; set; }
		public int EmailLeads { get; set; }
		public int MapsViewed { get; set; }
		public int AdPrints { get; set; }

		public int PreSplitSearchPageResults { get; set; }
		public int PreSplitVehicleDetailPages { get; set; }
		public int PreSplitEmailLeads { get; set; }
		public int PreSplitMapsViewed { get; set; }
		public int PreSplitAdPrints { get; set; }

        // note: this looks different because the data service returns "totals" and "pre-split" data; these are derived.
		public int PostSplitSearchPageResults  { get { return SearchPageResults - PreSplitSearchPageResults; } }
		public int PostSplitVehicleDetailPages { get { return VehicleDetailPages - PreSplitVehicleDetailPages; } }
		public int PostSplitEmailLeads         { get { return EmailLeads - PreSplitEmailLeads; } }
		public int PostSplitMapsViewed         { get { return MapsViewed - PreSplitMapsViewed; } }
		public int PostSplitAdPrints           { get { return AdPrints - PreSplitAdPrints; } }

		public int ActionCount
		{
			get { return EmailLeads + MapsViewed + AdPrints; }
		}
		public int PreSplitActionCount
		{
			get { return PreSplitEmailLeads + PreSplitMapsViewed + PreSplitAdPrints; }
		}
		public int PostSplitActionCount
		{
			get { return PostSplitEmailLeads + PostSplitMapsViewed + PostSplitAdPrints; }
		}
	
	}

}
