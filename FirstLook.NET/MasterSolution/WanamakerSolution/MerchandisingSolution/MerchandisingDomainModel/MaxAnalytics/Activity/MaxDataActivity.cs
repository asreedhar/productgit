﻿using FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity
{
    internal class MaxDataActivity : ProviderSite
    {
// ReSharper disable InconsistentNaming
		// This is used in deserialization from the webservice - just leave it as/is.
        public ActivityVin[] VINS { get; set; }
// ReSharper restore InconsistentNaming
    }
}
