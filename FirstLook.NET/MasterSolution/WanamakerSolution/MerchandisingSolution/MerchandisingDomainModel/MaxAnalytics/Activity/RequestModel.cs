﻿
namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity
{
    public class RequestModel
    {
        public string Date { get; set; }
        public string Vin { get; set; }
        public string Store { get; set; }
    }
}
