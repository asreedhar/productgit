﻿
namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity
{
    internal class ActivityVin
    {
        public string Vin { get; set; }
        public ActivityMetrics[] Metrics { get; set; }
    }
}
