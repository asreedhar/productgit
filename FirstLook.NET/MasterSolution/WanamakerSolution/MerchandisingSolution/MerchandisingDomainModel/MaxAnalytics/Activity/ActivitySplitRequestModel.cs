﻿namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity
{
	public class ActivitySplitRequestModel
	{
		public string Vin { get; set; }
		public string Store { get; set; }
		public string StartDate { get; set; }
		public string SplitDate { get; set; }

	}
}