﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount
{
	internal interface IInventoryCountWebService
	{
		/// <param name="stores">business unit codes</param>
		/// <param name="inventoryType">NEW|USED|BOTH</param>
		/// <param name="beginMonth"></param>
		/// <param name="beginYear"></param>
		/// <param name="endMonth"></param>
		/// <param name="endYear"></param>
		/// <returns></returns>
		InventoryCountResult GetInventoryCount(IEnumerable<String> stores, string inventoryType, int beginYear, int beginMonth, int endYear, int endMonth);
	}
}
