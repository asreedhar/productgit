﻿
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount
{
	internal class ProviderSiteCount : ProviderSite
	{
		public long Count;
	}

	internal class InventoryCountResult : Dictionary<string, IList<ProviderSiteCount>> {}
}
