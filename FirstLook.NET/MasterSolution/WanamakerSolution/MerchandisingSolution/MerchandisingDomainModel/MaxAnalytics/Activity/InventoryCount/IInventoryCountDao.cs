﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount
{
	public interface IInventoryCountDao
	{
		void GetInventoryCounts(List<SitePerformance> sitePerformanceList, int businessUnitId, VehicleType vehicleType, DateRange dateRange);
		void GetGroupInventoryCounts(Dictionary<int, List<SitePerformance>> dictionary, int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate);
	}
}
