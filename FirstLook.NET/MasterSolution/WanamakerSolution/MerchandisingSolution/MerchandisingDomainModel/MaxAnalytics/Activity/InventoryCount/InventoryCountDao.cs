﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount
{
	internal class InventoryCountDao : IInventoryCountDao
	{
		private static readonly ILog Log = LoggerFactory.GetLogger(typeof(InventoryCountDao));

		private readonly IInventoryCountWebService _inventoryCountWs;
		private readonly IDashboardBusinessUnitRepository _dealerServices;

		public InventoryCountDao(IInventoryCountWebService inventoryCountWs, IDashboardBusinessUnitRepository dealerServices)
		{
			_inventoryCountWs = inventoryCountWs;
			_dealerServices = dealerServices;
		}

		public void GetInventoryCounts(List<SitePerformance> sitePerformanceList, int businessUnitId, VehicleType vehicleType, DateRange dateRange)
		{
			var dealers = new List<IDashboardBusinessUnit>() { _dealerServices.GetBusinessUnit(businessUnitId)};
			var dict = new Dictionary<int, List<SitePerformance>> {{businessUnitId, sitePerformanceList}};
			GetGroupInventoryCounts(dict, dealers, vehicleType, dateRange.StartDate, dateRange.EndDate);
		}

		public void GetGroupInventoryCounts(Dictionary<int, List<SitePerformance>> dictionary, int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
		{
			var dealers = _dealerServices.GetBusinessUnitsfromGroup(groupId);
			GetGroupInventoryCounts(dictionary, dealers, vehicleType, startDate, endDate);
		}

		public void GetGroupInventoryCounts(Dictionary<int, List<SitePerformance>> dictionary, IList<IDashboardBusinessUnit> dealers, VehicleType vehicleType, DateTime startDate, DateTime endDate)
		{
			var buCodeToId = (from dealer in dealers
							  select new { dealer.BusinessUnitId, Code = dealer.Code })
							  .ToDictionary(k => k.Code, v => v.BusinessUnitId);

			var results = _inventoryCountWs.GetInventoryCount(buCodeToId.Keys, vehicleType.StringValue, startDate.Year, startDate.Month, endDate.Year, endDate.Month);

			foreach (var result in results)
			{
				var buCode = result.Key;
				var wsCounts = result.Value;

				var preferred = AnalyticsWrapper.PreferredProvider(wsCounts);
				
				var unitId = buCodeToId[buCode];
				
				foreach (var sp in dictionary[unitId])
				{
					ProviderSiteCount matches = preferred.FirstOrDefault(ws => (int) ws.Site == sp.Site.Id);
					if (matches != null)
					{
						sp.InventoryCount = (int) matches.Count;
					}
				}
			}
		}
	}
}

