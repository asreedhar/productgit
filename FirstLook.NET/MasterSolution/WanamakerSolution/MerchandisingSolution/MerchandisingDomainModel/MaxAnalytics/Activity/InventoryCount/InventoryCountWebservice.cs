﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Properties;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount
{
	internal class InventoryCountWebservice : IInventoryCountWebService
	{
		private readonly string webserviceUrl;

		public InventoryCountWebservice()
		{
			this.webserviceUrl = Settings.Default.MaxAnalytics.TrimEnd('/');
		}

		public InventoryCountWebservice(string webserviceUrl)
		{
			this.webserviceUrl = webserviceUrl.TrimEnd('/');
		}

		public InventoryCountResult GetInventoryCount(IEnumerable<string> stores, string inventoryType, int beginYear, int beginMonth, int endYear, int endMonth)
		{
			
			var url = BuildUrl(webserviceUrl, /*stores list*/ null, inventoryType, beginYear, beginMonth, endYear, endMonth);

			
			var postStores = new Dictionary<string, IList<string>>() { {"Stores", new List<string>()}};

			var storesList = stores as IList<string> ?? stores.ToList();
			storesList.ToList().ForEach(store => postStores["Stores"].Add(store));

			return MaxAnalyticsClient.PostModel<InventoryCountResult>(url, postStores.ToJson());
		}

		/// <summary>
		/// Returns valid URL to inventorycount web service
		/// </summary>
		/// <param name="webserviceUrl"></param>
		/// <param name="stores">If stores list is long, you can pass null and POST it instead</param>
		/// <param name="type"></param>
		/// <param name="beginYear"></param>
		/// <param name="beginMonth"></param>
		/// <param name="endYear"></param>
		/// <param name="endMonth"></param>
		/// <returns></returns>
		private static string BuildUrl(string webserviceUrl, IEnumerable<string> stores, string type, int beginYear, int beginMonth, int endYear, int endMonth)
		{
			return String.Format(
				"{0}/reporting/inventorycount?tenant=Max{1}&inventoryType={2}&beginYear={3}&beginMonth={4}&endYear={5}&endMonth={6}&format=json",
				webserviceUrl,
				stores != null ? "&stores=" + String.Join(",", stores) : "",
				type, beginYear, beginMonth, endYear, endMonth
			);
		}
	}
}
