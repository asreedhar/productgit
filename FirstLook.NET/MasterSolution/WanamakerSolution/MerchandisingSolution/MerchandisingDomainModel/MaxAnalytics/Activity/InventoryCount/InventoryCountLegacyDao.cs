﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount
{
	public class InventoryCountLegacyDao : IInventoryCountDao
	{
		public void GetInventoryCounts(List<SitePerformance> sitePerformanceList, int businessUnitId, VehicleType vehicleType, DateRange dateRange)
		{
			using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
			{
				if (con.State != ConnectionState.Open)
					con.Open();

				using (var cmd = con.CreateCommand())
				{
					cmd.CommandText = "dashboard.DealerListingSiteInventoryCounts";
					cmd.CommandType = CommandType.StoredProcedure;
					Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
					Database.AddRequiredParameter(cmd, "VehicleType", (int)vehicleType, DbType.Int32);
					Database.AddRequiredParameter(cmd, "StartDate", dateRange.StartDate, DbType.DateTime);
					Database.AddRequiredParameter(cmd, "EndDate", dateRange.EndDate, DbType.DateTime);

					using (var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							var description = reader.GetString(reader.GetOrdinal("description"));
							var count = reader.GetInt32(reader.GetOrdinal("count"));
							var month = reader.IsDBNull(reader.GetOrdinal("month")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("month"));

							if (month > DateTime.MinValue)
							{
								var site_perf = sitePerformanceList.FirstOrDefault(s => s.Site.Name == description && s.DateRange.StartDate == month);
								if (site_perf != null)
									site_perf.InventoryCount = count;
							}
						}
					}
				}
			}
		}

		public void GetGroupInventoryCounts(Dictionary<int, List<SitePerformance>> dictionary, int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
		{
			using (IDataConnection con =  Database.GetConnection(Database.MerchandisingDatabase))
			{
				if (con.State != ConnectionState.Open)
					con.Open();

				using (var cmd = con.CreateCommand())
				{
					//This proc doesn't perform well.
					cmd.CommandText = "dashboard.DealerListingSiteInventoryCountsTrend#Group";
					cmd.CommandType = CommandType.StoredProcedure;
					Database.AddRequiredParameter(cmd, "GroupID", groupId, DbType.Int32);
					Database.AddRequiredParameter(cmd, "VehicleType", (int)vehicleType, DbType.Int32);
					Database.AddRequiredParameter(cmd, "StartDate", startDate, DbType.DateTime);
					Database.AddRequiredParameter(cmd, "EndDate", endDate, DbType.DateTime);

					using (var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							var destinationId = reader.GetInt32(reader.GetOrdinal("DestinationId"));
							var businessunitId = reader.GetInt32(reader.GetOrdinal("BusinessunitID"));
							var count = reader.GetInt32(reader.GetOrdinal("count"));
							var month = reader.GetDateTime(reader.GetOrdinal("month"));

							if (dictionary.ContainsKey(businessunitId))
							{
								var site = dictionary[businessunitId].Where(x => x.Site.Id == destinationId && x.DateRange.StartDate == month).SingleOrDefault();
								if (site != null)
									site.InventoryCount = count;
							}
						}
					}
				}
			}
		}
	}
}
