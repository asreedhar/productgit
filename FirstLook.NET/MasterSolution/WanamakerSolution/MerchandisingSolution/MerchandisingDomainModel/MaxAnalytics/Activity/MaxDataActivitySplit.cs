﻿using FirstLook.Merchandising.DomainModel.MaxAnalytics.PerformanceSummary;

namespace FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity
{
	internal class MaxDataActivitySplit : ProviderSite
	{
		public VinActivitySplit[] VinActivitySplits { get; set; }
	}
}