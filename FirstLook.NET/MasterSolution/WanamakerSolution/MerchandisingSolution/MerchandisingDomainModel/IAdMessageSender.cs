using System.Collections;
using System.Collections.Generic;
using Core.Messages;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.DocumentPublishing;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using FirstLook.Merchandising.DomainModel.Pricing;
using MAX.Entities.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.GroupLevel;
using Merchandising.Messages.WebLoader;

namespace FirstLook.Merchandising.DomainModel
{
    public interface IAdMessageSender
    {
        /// <summary>
        /// Generates ad automatically. When certain vehicle attributes change
        /// </summary>
        /// <param name="message"></param>
        /// <param name="isNew">Is this a new car?</param>
        void SendAutoApproval(AutoApproveMessage message, bool isNew, string sentFrom);

        void BootStrapGroupLevelIfNecessary(GroupLevelAggregationMessage message, string sentFrom);

        /// <summary>
        /// This will work for 9.2.  AutoApprove is either on or off.  It doesn't vary by new/used.
        /// </summary>
        /// <param name="message"></param>
        void SendAutoApproval(AutoApproveMessage message, string sentFrom);
        void SendAutoApproval(IEnumerable<int> inventoryIds, int businessUnitId, string user, string sentFrom);
        void SendApproval(ApproveMessage message, string sentFrom);

        void SendBatchAutoLoadInventory(AutoLoadInventoryMessage message, string sentFrom);
        void SendAutoApprovalInventory(AutoApproveInventoryMessage message, string sentFrom);
	    void SendAutoApproveAllInventory(int businessUnitId, string sentFrom);
		void SendStyleSet(int businessUnitId, int inventoryId, string user, string sentFrom);

        void SendBatchPriceChange(BatchPriceChangeMessage message, string sentFrom);
        void SendPricingCampaignMessage(int businessUnitId, int inventoryId, string userName, string sentFrom);
	    void SendPublishDocumentsNotification(int businessUnitId, int inventoryId, string vin, string sentFrom);
	    void SendPublishAllDocumentNotifications(int businessUnitId, string sentFrom);

        void SendCreatePdfMessage(int businessUnitId, int inventoryId, string approvalUser);
        void PublishMaxDocumentPublishMessage(string vin, int businessUnitId, int inventoryId, string sentFrom);
        void SendBucketValueMessage(BucketValueMessage message, string sentFrom);
        void SendProcessAutoOfflineMessage(int? businessUnitId, string sentFrom);

        bool SendMessageAsync { get; set; }
    }

    public interface IDocumentPublishMessager
    {
        void PublishInactiveInventoryMessage(int businessUnitId, string ownerHandle,
            InventorySummary summary, Pair<VehicleHandle, SearchHandle> handles);

        void SendDocumentPublishNotification(PublicationInfo publicationInfo);

        void SendPhotoPublishMessage(int businessUnitId, string vin, string xmlToPublish);
    }

    public interface IPricingMessager
    {
        void SendBatchPriceMessages(int businessUnitId, IEnumerable<int> inventoryIDs);
        void SendApplyInventoryDiscountCampaignMessage(StyleSetMessage message);

        void SendApplyInventoryDiscountCampaignMessage(int businessUnitId, IEnumerable<int> inventoryIds,
            string activatedBy, bool force);
        void SendApplyDiscountMessage(int businessUnitId, int inventoryId, string discountActivatedBy);
        void SendInventoryChangedMessage(int businessUnitId, int inventoryId);

        // Queue Creation
        IQueue<StyleSetMessage> CreateStyleSetQueue();
        IQueue<ApplyInventoryDiscountCampaignMessage> CreateApplyInventoryDiscountCampaignQueue();

        // Bus Creation
        IBus<StyleSetMessage, StyleSetMessage> CreateStyleSetBus();
    }

    public interface IWebloaderMessager
    {
        void SendPhotoUploadMessage(PhotoUploadMessage[] photoMessages, int photoCount);
        void SendPhotoUploadMessage(PhotoUploadMessage message);
    }
}