﻿using System.Configuration;

namespace FirstLook.Merchandising.DomainModel.MaxPricingTool
{
    public static class Helpers
    {
        public static string FlServiceBaseUrl { get { return ConfigurationManager.AppSettings["FirstLookServicesApiUrl"].TrimEnd(new[] { '/', '\\' }); } }
        public static string FlServiceApiUser
        {
            get
            {
                return ConfigurationManager.AppSettings["FirstLookServiceApiUser"];
            }
        }
        public static string FlServiceApiPass
        {
            get
            {
                return ConfigurationManager.AppSettings["FirstLookServiceApiPass"];
            }
        }
    }
}
