﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using ServiceStack.Service;
using ServiceStack.ServiceClient.Web;

namespace FirstLook.Merchandising.DomainModel.MaxPricingTool.DataGateway
{
    public class RestServiceGateway<T>:IServiceGateway<T>
    {
        #region Logging
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        #endregion Logging

        #region IServiceGateway<T> Members

        /// <summary>
        /// Fetch Data from URL. If there is any exception throw it to parent.
        /// </summary>
        /// <param name="url">Service URL </param>
        /// <param name="apiUser">User Name</param>
        /// <param name="apiPass">Password</param>
        /// <param name="method">Service Method</param>
        /// <returns>Object Data</returns>
        public T GetData(string url, string apiUser, string apiPass,string method)
        {
            string errorMessage = "";
            try
            {

                var client = GetClient(url, apiUser, apiPass, method);

                var continuationTask = client.GetTask<T>(url)
                    .ContinueWith((Task<T> task) =>
                                  {
                                      if (task.IsFaulted)
                                      {
                                          var aggregateException = task.Exception;
                                          if (aggregateException != null)
                                          {
                                              foreach (var exception in aggregateException.InnerExceptions)
                                              {
                                                  if (exception is WebServiceException)
                                                  {
                                                      var wse = exception as WebServiceException;

                                                      if (wse.StatusCode == 401)
                                                      {
                                                          Log.DebugFormat(
                                                              "Invalid user/password combination for user {0}", apiUser);
                                                          errorMessage = wse.ResponseBody;
                                                          throw wse;
                                                      }
                                                      else if (wse.StatusCode == 400)
                                                      {
                                                          errorMessage = wse.ResponseBody;
                                                          throw wse;
                                                      }
                                                      else
                                                      {
                                                          Log.Error(
                                                              String.Format("Received an unexpected error. Error: {0}",
                                                                  wse.ErrorMessage), wse);
                                                          errorMessage = wse.ResponseBody;
                                                          throw wse;
                                                      }
                                                  }
                                                  else
                                                  {
                                                      Log.Error("Received Non-WebServiceException in async call.",
                                                          exception);
                                                      throw exception;
                                                  }

                                              }
                                              throw aggregateException;
                                          }
                                          throw new Exception("Null Exception");
                                      }
                                      else // Successful execution
                                      {
                                          return task.Result;
                                      }
                                  });

                //if (!continuationTask.IsCompleted)
                //    continuationTask.Wait();

                return continuationTask.Result;
            }
            catch (AggregateException ae)
            {
                Log.Error("Aggregate Error in async call.", ae);
                throw ae;
            }
            finally
            {
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    throw new Exception(errorMessage);
                }
            }
        }

        /// <summary>
        /// Returns Service Client for Specified URL including basic authentication
        /// </summary>
        /// <param name="url">Service URL</param>
        /// <param name="user">User Name</param>
        /// <param name="pass">Password</param>
        /// <param name="method">Method - GET,POST,PUT, DELETE</param>
        /// <returns></returns>
        private static IServiceClient GetClient(string url, string user, string pass, string method)
        {
            return new JsonServiceClient(url)
            {
                AlwaysSendBasicAuthHeader = true,
                UserName = user,
                Password = pass,
                HttpMethod = method
            };
        }
        #endregion
    }
}
