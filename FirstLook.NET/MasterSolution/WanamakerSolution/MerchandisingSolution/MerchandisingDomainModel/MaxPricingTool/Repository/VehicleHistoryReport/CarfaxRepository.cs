﻿using System;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.MaxPricingTool.DataGateway;
using FirstLook.Merchandising.DomainModel.MaxPricingTool.Dto.VehicleHistoryReport;
using FirstLook.Merchandising.DomainModel.MaxPricingTool.Model.VehicleHistoryReport;

namespace FirstLook.Merchandising.DomainModel.MaxPricingTool.Repository.VehicleHistoryReport
{
    public class CarfaxRepository:IRepository<CarfaxModel,CarfaxArgumentDto>
    {
        #region IRepository<CarfaxModel,CarfaxArgumentDto> Members

        /// <summary>
        /// This method will fetch data from Rest servcie and bind it with model
        /// </summary>
        /// <param name="input">Carfax Details</param>
        /// <returns>Carfax Model- Carfax Data from REST service</returns>
        public CarfaxModel Fetch(CarfaxArgumentDto input)
        {
            CarfaxModel model=new CarfaxModel();
            try
            {
                var service= Registry.Resolve<IServiceGateway<CarfaxModel>>();
                
                model = service.GetData(input.Url,
                    input.ApiUser,
                    input.ApiPass, input.Method);

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains(VhrStringKeys.UserMayOrderReport))
                {
                    model.HasPurchaseReportRights = true;
                }
            }

            return model;
        }

        public CarfaxModel Save(CarfaxArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public CarfaxModel Update(CarfaxArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public CarfaxModel Delete(CarfaxArgumentDto input)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
