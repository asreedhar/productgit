﻿
namespace FirstLook.Merchandising.DomainModel.MaxPricingTool.Repository.VehicleHistoryReport
{
    public class VhrStringKeys
    {
        readonly static public string UserMayOrderReport = "User may attempt to order report.";
        readonly static public string DefaultArea = "DefaultArea";
        readonly static public string DefaultPeriod = "DefaultPeriod";
    }

    public class CarfaxItemsKeys
    {
        readonly static public string SingleOwner = "1-owner";
        readonly static public string BuyBackGurantee = "buy back guarantee";
        readonly static public string TotalLoss = "no total loss reported";
        readonly static public string FrameDamage = "no frame damage reported";
        readonly static public string AirbagDeployment = "no airbag deployment reported";
        readonly static public string OdometerRollback = "no odometer rollback reported";
        readonly static public string Accidents = "no accidents / damage reported";
        readonly static public string ManufacturerRecall = "no manufacturer recalls reported";
    }
}
