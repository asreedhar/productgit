﻿using System;
using FirstLook.Merchandising.DomainModel.MaxPricingTool.Dto.VehicleHistoryReport;
using FirstLook.Merchandising.DomainModel.MaxPricingTool.Model.VehicleHistoryReport;

namespace FirstLook.Merchandising.DomainModel.MaxPricingTool.Repository.VehicleHistoryReport
{
    public class AutocheckRepository:IRepository<AutocheckModel,AutocheckArgumentDto>
    {
        #region IRepository<AutocheckModel,AutocheckArgumentDto> Members

        public AutocheckModel Fetch(AutocheckArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public AutocheckModel Save(AutocheckArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public AutocheckModel Update(AutocheckArgumentDto input)
        {
            throw new NotImplementedException();
        }

        public AutocheckModel Delete(AutocheckArgumentDto input)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
