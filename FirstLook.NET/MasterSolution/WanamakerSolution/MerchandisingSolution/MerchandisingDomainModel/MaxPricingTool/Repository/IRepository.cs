﻿
namespace FirstLook.Merchandising.DomainModel.MaxPricingTool.Repository
{
    public interface IRepository<out TResult,in TInput >
    {
        TResult Fetch(TInput input);
        TResult Save(TInput input);
        TResult Update(TInput input);
        TResult Delete(TInput input);
    }
}
