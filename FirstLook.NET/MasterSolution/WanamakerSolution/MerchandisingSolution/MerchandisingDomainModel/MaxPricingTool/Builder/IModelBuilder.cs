﻿
namespace FirstLook.Merchandising.DomainModel.MaxPricingTool.Builder
{
    public interface IModelBuilder<TViewModel>
    {
        /// <summary>
        /// Build ViewModel data.
        /// </summary>
        /// <returns>ViewModel object</returns>
        TViewModel Build();

        /// <summary>
        /// Update or Refresh View Model Data based on model
        /// </summary>
        /// <param name="model">Model Data</param>
        /// <returns>Updated ViewModel</returns>
        TViewModel Rebuild(TViewModel model);
    } 
}
