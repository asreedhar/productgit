﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    public class CachedStatus
    {
        public CachedStatus(State state, DateTime stamp, float percentComplete)
        {
            State = state;
            TimeStamp = stamp;
            PercentComplete = percentComplete;
        }

        [ScriptIgnore]
        public State State { get; set; }

        //just for javascript serialization
        public string Status
        {
            get { return State.ToString(); }
        }

        public DateTime TimeStamp { get; set; }
        public float PercentComplete { get; set; }

        public static CachedStatus FromString(string s)
        {
            var status = new CachedStatus(State.Empty, DateTime.MinValue.ToUniversalTime(), 0);

            if (!String.IsNullOrEmpty(s))
            {
                var values = Regex.Split(s, @"\s*;\s*[;\s*]?");
                if (values.Length == 2)
                    status = new CachedStatus((State) Enum.Parse(typeof (State), values[0]),
                                              DateTime.Parse(values[1]).ToUniversalTime(), 0);
                else if (values.Length == 3)
                    status = new CachedStatus((State) Enum.Parse(typeof (State), values[0]),
                                              DateTime.Parse(values[1]).ToUniversalTime(), float.Parse(values[2]));
            }

            return status;
        }

        public override string ToString()
        {
            //State;UTC time;percentComplete
            return string.Format(@"{0};{1};{2}", State, TimeStamp.ToString("yyyy-MM-ddTHH:mm:ssZ"), PercentComplete);
        }
    }
}
