﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    public interface IGroupLevelDataCache
    {
        string CreateCurrentDashBoardDataKey(int groupId, string usedNewFilter);
        string CreateCurrentSitePerformanceDataKey(int groupId, string usedNewFilter, int month, int year);

        void WriteSitePerformanceData(int groupId, string usedNewFilter, int month, int year, string json);
        void WriteDashboardData(int groupId, string usedNewFilter, string json);

        string GetCachedData(string key);

        void SetCachedState(int groupId, float percentComplete);
        void SetCachedState(int groupId, State state);
        CachedStatus GetCachedStatus(int groupId);
    }
}
