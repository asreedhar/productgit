﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    public enum State
    {
        Empty,
        Pending,
        Processing,
        Failed,
        Cached
    }
}
