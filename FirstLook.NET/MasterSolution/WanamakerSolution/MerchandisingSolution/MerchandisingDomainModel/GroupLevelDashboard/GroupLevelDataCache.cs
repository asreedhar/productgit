﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core.Messaging;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    internal class GroupLevelS3DataCache : IGroupLevelDataCache
    {
        private const string FolderName = "GroupLevelCache";
        private const string DashBoardData = "DashBoardData";
        private const string SitePerformance = "SitePerformance";
        private const string State = "State";

        private IFileStorage _storage;
        //private bool _isAsync = false;
        //public bool IsAsync {
        //    get { return _isAsync; }
        //    set { _isAsync = value; }
        //}
        public GroupLevelS3DataCache(IFileStoreFactory factory, bool isAsync=false)
        {
            _storage = factory.CreateFileStore(isAsync);
        }

        public string CreateCurrentDashBoardDataKey(int groupId, string usedNewFilter)
        {
            return string.Format(@"{0}/{1}/Current/{2}", FolderName, groupId, DashBoardData + "-" + usedNewFilter);
        }

        private static string CreateDateDashBoardDataKey(int groupId, string usedNewFilter)
        {
            return string.Format(@"{0}/{1}/{2}/{3}", FolderName, groupId, DateTime.Now.ToString("yyyy-MM-dd"), DashBoardData + "-" + usedNewFilter);
        }

        public string CreateCurrentSitePerformanceDataKey(int groupId, string usedNewFilter, int month, int year)
        {
            var date = new DateTime(year, month, 1);
            return string.Format(@"{0}/{1}/Current/{2}", FolderName, groupId, SitePerformance + "-" + date.ToString("yyyy-MM") + "-" + usedNewFilter);
        }

        private static string CreateSitePerformanceDataKey(int groupId, string usedNewFilter, int month, int year)
        {
            var date = new DateTime(year, month, 1);
            return string.Format(@"{0}/{1}/{2}/{3}", FolderName, groupId, DateTime.Now.ToString("yyyy-MM-dd"), SitePerformance + "-" + date.ToString("yyyy-MM") + "-" + usedNewFilter);
        }

        private static void WriteValue(IFileStorage storage, string key, string txt)
        {
            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(txt);
                writer.Flush();
                stream.Position = 0;
                storage.Write(key, stream);
            }
        }

        private static string GetStateFileName(int groupId)
        {
            return string.Format(@"{0}/{1}/Current/{2}", FolderName, groupId, State);
        }

        public void SetCachedState(int groupId, float percentComplete)
        {
            CachedStatus currentStatus = GetCachedStatus(groupId);
            currentStatus.PercentComplete = percentComplete;
            WriteStatus(groupId, currentStatus);
        }

        public void SetCachedState(int groupId, State state)
        {
            var status = new CachedStatus(state, DateTime.UtcNow, 0);
            
            if (state == GroupLevelDashboard.State.Pending || state == GroupLevelDashboard.State.Processing) //Here we don't want to change the timestamp.
            {
                var currentStatus = GetCachedStatus(groupId);
                status.TimeStamp = currentStatus.TimeStamp;
            }

            if (state == GroupLevelDashboard.State.Cached)
                status.PercentComplete = 1;

            WriteStatus(groupId, status);
        }

        private void WriteStatus(int groupId, CachedStatus status)
        {
            WriteValue(_storage, GetStateFileName(groupId), status.ToString());
        }

        public CachedStatus GetCachedStatus(int groupId)
        {
            string value = GetCachedData(GetStateFileName(groupId));
            var status = CachedStatus.FromString(value);

            return status;
        }

        public string GetCachedData(string key)
        {
            bool exists;
            string txt = null;
            using (var stream = _storage.Exists(key, out exists))
            {
                if (exists)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        txt = reader.ReadToEnd();
                    }
                }
            }

            return txt;
        }

        public void WriteSitePerformanceData(int groupId, string usedNewFilter, int month, int year, string json)
        {
            WriteValue(_storage, CreateCurrentSitePerformanceDataKey(groupId, usedNewFilter, month, year), json);
            WriteValue(_storage, CreateSitePerformanceDataKey(groupId, usedNewFilter, month, year), json);
        }

        public void WriteDashboardData(int groupId, string usedNewFilter, string json)
        {
            WriteValue(_storage, CreateCurrentDashBoardDataKey(groupId, usedNewFilter), json);
            WriteValue(_storage, CreateDateDashBoardDataKey(groupId, usedNewFilter), json);
        }

    }
}
