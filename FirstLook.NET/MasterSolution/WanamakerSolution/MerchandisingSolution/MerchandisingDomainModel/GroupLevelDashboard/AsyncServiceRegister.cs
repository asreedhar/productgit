﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Autofac.Core.Lifetime;
using FirstLook.Common.Core.IOC;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    public class AsyncServiceRegister : IRegistryModule
    {
        public void Register(Autofac.ContainerBuilder builder)
        {

            builder = new ContainerBuilder();
            var components = Registry.Container.ComponentRegistry.Registrations
                    .Where(cr => cr.Activator.LimitType != typeof(LifetimeScope))
                    .Where(cr => cr.Activator.LimitType != typeof(GroupLevelS3DataCache));
            foreach (var c in components)
            {
                builder.RegisterComponent(c);
            }

            foreach (var source in Registry.Container.ComponentRegistry.Sources)
            {
                builder.RegisterSource(source);
            }
            
            builder.Register(c => new GroupLevelS3DataCache(c.Resolve<IFileStoreFactory>(), true)).As<IGroupLevelDataCache>();
            Registry.Container = builder.Build();
            
            
        }
    }
}
