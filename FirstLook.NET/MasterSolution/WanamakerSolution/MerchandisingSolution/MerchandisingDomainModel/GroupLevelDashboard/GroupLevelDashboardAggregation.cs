﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using Merchandising.Messages.GroupLevel;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    public static class GroupLevelDashboardAggregation
    {
        public static GroupLevelAggregationMessage[] GetActiveGroups()
        {
            var messages = new List<GroupLevelAggregationMessage>();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GroupLevelDashboardActive#Fetch";
                    cmd.CommandType = CommandType.Text;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int groupid = reader.GetInt32(reader.GetOrdinal("parentid"));
                            messages.Add(new GroupLevelAggregationMessage(groupid));
                        }
                    }

                }

            }

            return messages.ToArray();
        }
    }
}
