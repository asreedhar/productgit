﻿using System;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using MAX.Entities;
using MAX.Entities.Reports.TimeToMarket;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    public interface IGroupDashboardManager
    {
        GroupSitePerformanceData[] GenerateSitePerformance(int groupId, string usedNewFilter, int month, int year);
        GroupSitePerformanceData[] GenerateSitePerformance(int groupId, string usedNewFilter, int month, int year, Action<float> progress);

        GroupSitePerformanceData[] FetchSitePerformance(int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate);

        GroupDashboardModel[] FetchDashboardData(int groupId, string usedNewFilter);

        GroupDashboardModel[] GenerateDashboardData(int groupId, string usedNewFilter);
        GroupDashboardModel[] GenerateDashboardData(int groupId, string usedNewFilter, Action<float> progress);

        GoogleAnalyticsResult[] GetAllGoogleAnalyticsData(int groupId, GoogleAnalyticsType gaType, DateRange dateRange);
        GoogleAnalyticsResult[] GenerateAllGoogleAnalyticsData(int groupId, GoogleAnalyticsType gaType, DateRange dateRange);
        GoogleAnalyticsResult[] GenerateAllGoogleAnalyticsData(int groupId, GoogleAnalyticsType gaType, DateRange dateRange, Action<float> progress);

        GroupReport FetchGroupReport(int groupId, VehicleType vehicleType, DateRange dateRange);

        void SaveCachedState(int groupId, float percentComplete);
        void SaveCachedState(int groupId, State state);
        CachedStatus FetchCachedStatus(int groupId);

        void SaveDealerSiteBudget(int businessUnitId, string siteName, int budgetValue, int month, int year);
    }
}
