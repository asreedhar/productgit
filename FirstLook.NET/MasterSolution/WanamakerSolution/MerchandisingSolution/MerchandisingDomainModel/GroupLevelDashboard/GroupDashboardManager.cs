﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Serialization;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using MAX.Entities;
using MAX.Entities.Helpers.DashboardHelpers;
using MAX.Entities.Reports.TimeToMarket;

namespace FirstLook.Merchandising.DomainModel.GroupLevelDashboard
{
    internal class GroupDashboardManager : IGroupDashboardManager
    {
        private IGroupLevelDataCache _groupLevelDataCache;
        private IDashboardManager _manager;
        private ISitePerformanceRepository _sitePerformanceRepository;
        private IGoogleAnalyticsResultCache _googleCache;
        private readonly ITimeToMarketRepository _ttmRepository;
        private IDashboardBusinessUnitRepository _dealerRepository;

        public GroupDashboardManager(IGroupLevelDataCache groupLevelDataCache, IDashboardManager manager, ISitePerformanceRepository sitePerformanceRepository, 
            IGoogleAnalyticsResultCache googleCache, ITimeToMarketRepository timeToMarketRepo, IDashboardBusinessUnitRepository dealerRepository)
        {
            _groupLevelDataCache = groupLevelDataCache;
            _manager = manager;
            _sitePerformanceRepository = sitePerformanceRepository;
            _googleCache = googleCache;
            _ttmRepository = timeToMarketRepo;
            _dealerRepository = dealerRepository;
        }

        private void CacheSitePerformanceDataModel(int groupId, string type, int month, int year, GroupSitePerformanceData[] model)
        {
            string json = new JavaScriptSerializer().Serialize(model);
            _groupLevelDataCache.WriteSitePerformanceData(groupId, type, month, year, json);
        }

        public GroupSitePerformanceData[] GenerateSitePerformance(int groupId, string usedNewFilter, int month, int year)
        {
            return GenerateSitePerformance(groupId, usedNewFilter, month, year, null);
        }

        public GroupSitePerformanceData[] GenerateSitePerformance(int groupId, string usedNewFilter, int month, int year, Action<float> progress)
        {
            var startDate = new DateTime(year, month, 1);
            var endDate = new DateTime(year, month, 1).AddMonths(1).AddSeconds(-1); //Need to subtract 1 second here for db to not round to next month

            var model = _sitePerformanceRepository.GetGroupSitePerformance(groupId, usedNewFilter, startDate, endDate);
            var siteDataMap = SitePerformanceMapper.Map(model, startDate, endDate);

            if (progress != null)
                progress(1.0f);

            CacheSitePerformanceDataModel(groupId, usedNewFilter, month, year, siteDataMap.ToArray());
            
            return siteDataMap.ToArray();
        }

        public GroupSitePerformanceData[] FetchSitePerformance(int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            var model = _sitePerformanceRepository.GetGroupSitePerformance(groupId, vehicleType, startDate, endDate);
            var siteDataMap = SitePerformanceMapper.Map(model, startDate, endDate);

            return siteDataMap.ToArray();
        }

        public void SaveDealerSiteBudget(int businessUnitId, string siteName, int budgetValue, int month, int year)
        {
            string siteId = _manager.ResolveDBEnum("settings.EdtDestinations", "description", "destinationID", siteName);
            if (siteId == null)
                throw new DataException("site not found: " + siteName);
            var budget = new SiteBudget
            {
                BusinessUnitId = businessUnitId,
                DestinationId = int.Parse(siteId),
                DescriptionText = "",
                MonthApplicable = new DateTime(year, month, 1),
                Budget = budgetValue
            };
            var sb_repo = new SiteBudgetRepository();
            sb_repo.InsertOrUpdate(budget);
        }

        public GroupDashboardModel[] FetchDashboardData(int groupId, string usedNewFilter)
        {
            string json = _groupLevelDataCache.GetCachedData(_groupLevelDataCache.CreateCurrentDashBoardDataKey(groupId, usedNewFilter));

            GroupDashboardModel[] model;
            if (json == null)
                model = GenerateDashboardData(groupId, usedNewFilter);
            else
                model = new JavaScriptSerializer().Deserialize<GroupDashboardModel[]>(json);

            return model;
        }

        public GroupDashboardModel[] GenerateDashboardData(int groupId, string usedNewFilter)
        {
            return GenerateDashboardData(groupId, usedNewFilter, null);
        }

        public GroupDashboardModel[] GenerateDashboardData(int groupId, string usedNewFilter, Action<float> progress)
        {
            var units = _dealerRepository.GetBusinessUnitsfromGroup(groupId);
          
            var list = new List<GroupDashboardModel>();
            foreach (var unit in units)
            {
                if (progress != null)
                    progress(list.Count / (float)units.Length);

                if (! _dealerRepository.HasDealerUpgrade(unit.BusinessUnitId, (int)Upgrade.Merchandising))
                    continue;

                var model = GetGroupDashboardData(unit, usedNewFilter);
                list.Add(model);
            }

            CacheDashboardDataModel(groupId, usedNewFilter, list.ToArray());
            return list.ToArray();
        }

        private void CacheDashboardDataModel(int groupId, string usedNewFilter, GroupDashboardModel[] model)
        {
            string json = new JavaScriptSerializer().Serialize(model);
            _groupLevelDataCache.WriteDashboardData(groupId, usedNewFilter, json);
        }

        protected Buckets GetGroupContextBucketsData(IDashboardManager manager, int businessUnitId, VehicleType usedOrNewFilter)
        {
            var buckets_data = manager.GetBucketsData(businessUnitId, usedOrNewFilter, true);
            foreach (var bucket_list in new[] { buckets_data.ActionBuckets, buckets_data.ActivityBuckets, buckets_data.MaxBuckets })
                foreach (var bucket in bucket_list)
                    bucket.NavigationUrl = Helper.RewriteDealerSpecificURL(businessUnitId, bucket.NavigationUrl);
            buckets_data.OfflineBucket.NavigationUrl = Helper.RewriteDealerSpecificURL(businessUnitId, buckets_data.OfflineBucket.NavigationUrl);
            return buckets_data;
        }

        private GroupDashboardModel GetGroupDashboardData(IDashboardBusinessUnit unit, VehicleType vehicleType)
        {
            return new GroupDashboardModel
            {
                BusinessUnit = unit.Name,
                BusinessUnitId = unit.BusinessUnitId,
                BusinessUnitHome = Helper.RewriteDealerSpecificURL(unit.BusinessUnitId, "Default.aspx?ref=GLD"),
                Data = new DashboardModel
                {
                    DealerSiteData = _manager.GetStaticDealerSiteData(unit),
                    BucketsActive = _manager.GetBucketsActive(unit.BusinessUnitId), //extension
                    AutoApprove = _manager.GetAutoApproveStatus(unit.BusinessUnitId),
                    BucketsData = GetGroupContextBucketsData(_manager, unit.BusinessUnitId, vehicleType),
                    NotOnlineUrl = Helper.RewriteDealerSpecificURL(unit.BusinessUnitId, Helper.GetNotOnlineUrl(vehicleType)),
                    LowActivityReportUrl = Helper.RewriteDealerSpecificURL(unit.BusinessUnitId, Helper.GetLowActivityReportUrl(vehicleType))
                }
            };
        }

        public void SaveCachedState(int groupId, float percentComplete)
        {
            _groupLevelDataCache.SetCachedState(groupId, percentComplete);
        }

        public void SaveCachedState(int groupId, State state)
        {
            _groupLevelDataCache.SetCachedState(groupId, state);
        }

        public CachedStatus FetchCachedStatus(int groupId)
        {
            return _groupLevelDataCache.GetCachedStatus(groupId);
        }

        public GoogleAnalyticsResult[] GetAllGoogleAnalyticsData(int groupId, GoogleAnalyticsType gaType, DateRange dateRange)
        {
            IEnumerable<GoogleAnalyticsResult> analyticsData = _googleCache.FetchGroupRangedGoogleData(groupId, gaType, dateRange);

            return analyticsData.ToArray();
        }

        public GoogleAnalyticsResult[] GenerateAllGoogleAnalyticsData(int groupId, GoogleAnalyticsType gaType, DateRange dateRange)
        {
            return GenerateAllGoogleAnalyticsData(groupId, gaType, dateRange, null);
        }

        public GoogleAnalyticsResult[] GenerateAllGoogleAnalyticsData(int groupId, GoogleAnalyticsType gaType, DateRange dateRange, Action<float> progress)
        {
            IEnumerable<GoogleAnalyticsResult> analyticsData = _googleCache.GenerateGroupRangedGoogleData(groupId, gaType, dateRange);
            
            if (progress != null)
                progress(1.0f);

            return analyticsData.ToArray();
        }


        public GroupReport FetchGroupReport(int groupId, VehicleType vehicleType, DateRange dateRange)
        {
            return _ttmRepository.FetchGroupReport(groupId, vehicleType, dateRange);
        }
    }
}
