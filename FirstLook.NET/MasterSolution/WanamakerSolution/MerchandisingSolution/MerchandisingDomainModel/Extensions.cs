﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace FirstLook.Merchandising.DomainModel
{
    public static class Extensions
    {
        private static readonly Regex EndingColonRegex = new Regex(@"[:\s]*$");
        private static readonly Regex StartingEqualRegex = new Regex(@"^[=\s]*");

        public static string CleanUpHeader(this string header)
        {
            header = StartingEqualRegex.Replace(header, "");
            header = EndingColonRegex.Replace(header, "");
            return header;
        }

        public static string HumanizeString(this string source)
        {
            StringBuilder sb = new StringBuilder();
            char last = char.MinValue;
            foreach (char c in source)
            {
                if (char.IsLower(last) && char.IsUpper(c))
                {
                    sb.Append(' ');
                }
                sb.Append(c);
                last = c;
            }
            return sb.ToString();
        }

        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }
    }
}
