﻿using System;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Extensions;

namespace FirstLook.Merchandising.DomainModel
{
    internal static partial class Database
    {
        #region Non-Nullable Read Extensions

        public static Int16 GetInt16(this IDataRecord record, string columnName)
        {
            return record.GetInt16(record.GetOrdinal(columnName));
        }

        public static int GetInt32(this IDataRecord record, string columnName)
        {
            return record.GetInt32(record.GetOrdinal(columnName));
        }

        public static Int64 GetInt64(this IDataRecord record, string columnName)
        {
            return record.GetInt64(record.GetOrdinal(columnName));
        }

        public static string GetString(this IDataRecord record, string columnName)
        {
            return record.GetString(record.GetOrdinal(columnName));
        }

        public static DateTime GetDateTime(this IDataRecord record, string columnName)
        {
            return record.GetDateTime(record.GetOrdinal(columnName));
        }

        public static bool GetBoolean(this IDataRecord record, string columnName)
        {
            return record.GetBoolean(record.GetOrdinal(columnName));
        }
        public static decimal GetDecimal(this IDataRecord record, string columnName)
        {
            return record.GetDecimal(record.GetOrdinal(columnName));
        }
        public static double GetDouble(this IDataRecord record, string columnName)
        {
            return record.GetDouble(record.GetOrdinal(columnName));
        }
        public static float GetFloat(this IDataRecord record, string columnName)
        {
            return record.GetFloat(record.GetOrdinal(columnName));
        }

        public static char GetChar(this IDataRecord record, string columnName)
        {
            return record.GetChar(record.GetOrdinal(columnName));
        }

        public static byte GetByte(this IDataRecord record, string columnName)
        {
            return record.GetByte(record.GetOrdinal(columnName));
        }


        #endregion Non-Nullable Read Extensions


        // ReSharper disable InconsistentNaming
        public static bool IsDBNull(this IDataRecord record, string columnName)
        {
            return record.IsDBNull(record.GetOrdinal(columnName));
        }
        // ReSharper restore InconsistentNaming

        public static T GetObject<T>(this IDataRecord record, string columnName) where T : new()
        {
            return ( record.HasColumn(columnName).Equals(false) || record.IsDBNull(columnName)) ? new T() : record.GetString(columnName).FromJson<T>();
        }
    }
}