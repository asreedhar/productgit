﻿
namespace FirstLook.Merchandising.DomainModel.Reports
{
    public interface IBusinessUnitAggregation
    {
        int BusinessUnitId { get; }
    }
}