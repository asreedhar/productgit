﻿namespace FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers
{
    public enum MessageType { Warning, Information, Error, Success }
    public class CustomMessage
    {


        public string MessageText { get; set; }
        public MessageType MessageType { get; set; }
        public string MessageTypeText
        {
            get { return MessageType.ToString() ; }
        }
        public static CustomMessage Empty
        {
            get
            {
                return new CustomMessage
                           {
                               MessageText = string.Empty,
                               MessageType = MessageType.Information
                           };
            }
        }
  
        }
    
}