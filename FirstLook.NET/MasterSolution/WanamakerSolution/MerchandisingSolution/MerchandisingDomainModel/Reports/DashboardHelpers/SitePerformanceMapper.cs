﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using MAX.Entities.Helpers.DashboardHelpers;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers
{
    //This class converts the site performance data to the site performance view model
    //Calculates the scale factor
    public class SitePerformanceMapper
    {
        public static List<GroupSitePerformanceData> Map(Dictionary<int, List<SitePerformance.SitePerformance>> map, DateTime startDate, DateTime endDate)
        {
            var list = new List<GroupSitePerformanceData>();
            foreach(var key in map.Keys)
            {
                var sites = Map(map[key]);
                var businessUnitId = map[key].First().BusinessUnitId;
                var businessUnit = map[key].First().BusinessUnit;

                var groupData = new GroupSitePerformanceData
                {
                    BusinessUnit = businessUnit,
                    BusinessUnitId = businessUnitId,
                    BusinessUnitHome = Helper.RewriteDealerSpecificURL(businessUnitId, "Default.aspx?ref=GLD"),
                    Data = new SitePerformanceData
                    {
                        StartDate = startDate,
                        EndDate = endDate,
                        Sites = sites
                    }
                };

                list.Add(groupData);
            }

            return list;
        }

        public static Dictionary<string, SitePerformanceVm> Map(IEnumerable<SitePerformance.SitePerformance> sitePerformancesModel)
        {
            var sitePerformacesVm = new Dictionary<string, SitePerformanceVm>();
           
            if (sitePerformancesModel != null && sitePerformancesModel.Count() > 0)
            {
                foreach (var sitePerf in sitePerformancesModel)
                {
                    var modelVm = new SitePerformanceVm
                    {
                        SearchValue = sitePerf.SearchPageViewCount,
                        DetailValue = sitePerf.DetailPageViewCount,

                        ActionValue = sitePerf.TotalActions,

                        AdPrintValue = sitePerf.AdPrintedCount,
                        EmailValue = sitePerf.EmailLeadsCount,
                        PhoneValue = sitePerf.PhoneLeadsCount,
                        MapValue = sitePerf.MapsViewedCount,
                        ChatValue = sitePerf.ChatRequestsCount,

                        // If we set the months (eg GroupSummation) use that, else calculate the months
                        Months = (sitePerf.Months> 0) ? sitePerf.Months : sitePerf.DateRange.AsMonths().Count(),

                        ClickThroughRate = Math.Round(sitePerf.ClickThroughRate*100, 2, MidpointRounding.AwayFromZero),
                        ConversionRate = Math.Round(sitePerf.ConversionRateFromDetailPage*100, 2, MidpointRounding.AwayFromZero),

                        SiteName = sitePerf.Site.Name,

                        Budget = sitePerf.Budget,
                        BenchMarkLeadCost = sitePerf.BenchMarkLeadCost,
                        BenchMarkImpressionCost = sitePerf.BenchMarkImpressionCost,
                        
                        InventoryCount = sitePerf.InventoryCount
                    };


                    sitePerformacesVm.Add(key: modelVm.SiteName, value: modelVm);                    
                }
            }
            return sitePerformacesVm;
        }

    }
}