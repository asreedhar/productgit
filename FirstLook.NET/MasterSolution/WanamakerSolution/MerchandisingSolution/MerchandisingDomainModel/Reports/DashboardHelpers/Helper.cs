﻿using System.Web;
using FirstLook.Merchandising.DomainModel.Release;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers
{
    public static class Helper
    {
        public static string RewriteDealerSpecificURL(int businessUnitId, string originalURL)
        {
            var absolute_url = "/merchandising/" + originalURL;
            var redirector = new RedirectEntry(businessUnitId, absolute_url);
            var url = "workflow/link.aspx?link=" + redirector.ToToken();
            return url;
        }

        public static string GetNotOnlineUrl(VehicleType usedOrNewFilter)
        {
            return IsReportViewer ? string.Format("Reports/VehicleOnlineStatusReport.aspx?usednew={0}", (int)usedOrNewFilter) : "#";
        }

        public static string GetLowActivityReportUrl(VehicleType usedOrNewFilter)
        {
            return IsReportViewer ? string.Format("Reports/VehicleActivityReport.aspx?usednew={0}", (int)usedOrNewFilter) : "#";
        }

        public static string GetBucketNavigationUrl(WorkflowType bucketId, VehicleType usedOrNewFilter)
        {
            return string.Format("Workflow/Inventory.aspx?usednew={0}&filter={1}", (int)usedOrNewFilter,
                                                             bucketId);
        }

        public static bool IsReportViewer
        {
            get
            {
                if (HttpContext.Current == null)
                    return true;

                return (HttpContext.Current.User.IsInRole("Report Viewer") || IsAdministrator);
            }
        }

        public static bool IsAdministrator
        {
            get
            {
                return HttpContext.Current != null && HttpContext.Current.User.IsInRole("Administrator");
            }
        }

    }
}
