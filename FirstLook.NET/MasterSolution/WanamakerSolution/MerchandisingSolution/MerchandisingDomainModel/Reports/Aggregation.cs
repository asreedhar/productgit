﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Reports
{
    public class Aggregation<TValue>
    {
        private IEnumerable<TValue> _sourceList;
        private TValue _target;

        public Aggregation(IEnumerable<TValue> sourceList, TValue target)
        {
            _sourceList = sourceList;
            _target = target;
        }

        private void SumDictionary(IDictionary source, IDictionary target)
        {
            foreach(var key in source.Keys)
            {
                if(!target.Contains(key))
                {
                    //create type
                    target.Add(key, CreateType(source[key]));
                }

                Sum(source[key], target[key]);
            }
        }

        private static object CreateType(object value)
        {
            var constructor = value.GetType().GetConstructor(Type.EmptyTypes);
            return constructor.Invoke(null);
        }

        //base class does addition
        protected virtual double Operation(double target, double source)
        {
            target += source;
            return target;
        }

        private void Sum(object source, object target)
        {
            var sourcePropertyInfo = source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in sourcePropertyInfo)
            {
                var sourceValue = property.GetValue(source, null);
                if (sourceValue == null || !property.CanWrite) //continue loop if source is null or not writable. Nothing to aggregate
                    continue;
                
                var targetProperty = target.GetType().GetProperty(property.Name);
                var targetValue = targetProperty.GetValue(target, null);

                double doubleValue;
                    
                if (Double.TryParse(sourceValue.ToString(), out doubleValue)) //if we can get to a double we sum
                {
                    object objectValue = targetProperty.GetValue(target, null);
                    double targetDouble = Convert.ToDouble(objectValue);

                    var hasNonAggregate = property.GetCustomAttributes(typeof(NonAggregateAttribute), true).Count() == 1;
                    if (targetDouble != 0 && hasNonAggregate) //we don't want to aggregate properties with the NonAggregateAttribute
                        continue;

                    targetDouble = Operation(targetDouble, doubleValue);

                    if (targetProperty.PropertyType.IsGenericType && targetProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        var underlyingType = targetProperty.PropertyType.GetGenericArguments()[0];
                        objectValue = Convert.ChangeType(targetDouble, underlyingType);   
                    }
                    else
                    {
                        objectValue = Convert.ChangeType(targetDouble, targetProperty.PropertyType);   
                    }

                    targetProperty.SetValue(target, objectValue, null);
                }
                else if(property.PropertyType == typeof(String)) //just assign over
                {
                    targetProperty.SetValue(target, sourceValue, null);
                }
                else if (typeof(IDictionary).IsAssignableFrom(property.PropertyType)) //hashtable
                {                    
                    if (targetValue == null) //create object if null
                    {
                        object newObject = CreateType(sourceValue);
                        targetProperty.SetValue(target, newObject, null);
                    }
                    SumDictionary(sourceValue as IDictionary, targetProperty.GetValue(target, null) as IDictionary);
                }
                else if(typeof(ValueType).IsAssignableFrom(property.PropertyType)) //value type. Just assign over
                {
                    targetProperty.SetValue(target, sourceValue, null);
                }
                else //just an object. 
                {
                    if (targetValue == null)
                    {
                        object newObject = CreateType(sourceValue);
                        targetProperty.SetValue(target, newObject, null);
                    }
                    Sum(sourceValue, targetProperty.GetValue(target, null));
                }
            }
        }


        public void Process()
        {
            foreach(var source in _sourceList)
                Sum(source, _target);
        }
    }

}