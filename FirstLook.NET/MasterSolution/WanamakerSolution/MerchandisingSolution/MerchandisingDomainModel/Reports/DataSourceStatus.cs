using System;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Reports
{
    [Serializable]
    public class DataSourceStatus
    {
        public int BusinessUnitId { get; set; }
        public EdtDestinations Destination { get; set; }
        public DateTime? LastLoadedTime { get; set; }
        public bool HasCredentials { get; set; }
        public bool HasInvalidCredentials { get; set; }
    }
}