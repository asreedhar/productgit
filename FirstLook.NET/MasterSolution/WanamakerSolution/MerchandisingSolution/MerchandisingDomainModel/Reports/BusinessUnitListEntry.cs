﻿namespace FirstLook.Merchandising.DomainModel.Reports
{
    public class BusinessUnitListEntry
    {
        public int BusinessUnitID { get; set; }
        public string Name { get; set; } 
    }
}