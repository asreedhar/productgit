﻿namespace FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace
{
    public interface IPerformanceRepository
    {
        InventoryAdPerformace[] GetAdPerformace(int businessUnitId, int inventoryId);
        PriceEvent[] GetPriceEvents(int inventoryId);
    }
}
