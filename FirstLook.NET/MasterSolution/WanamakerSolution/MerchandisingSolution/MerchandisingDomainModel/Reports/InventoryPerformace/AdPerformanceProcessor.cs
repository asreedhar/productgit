﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace
{
    //Fills in CTR gaps and calculates CTR per day by doing deltas
    public class AdPerformanceProcessor
    {
        private static void GetNextExpectedValue(DateTime date, out int expectedDay, out int expectedMonth, out int expectedYear)
        {
            int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);

            expectedMonth = date.Month;
            expectedYear = date.Year;
            expectedDay = date.Day + 1;
            if (expectedDay > daysInMonth)
            {
                expectedDay = 1;
                expectedMonth = date.Month + 1;
            }
            if (expectedMonth > 12)
            {
                expectedMonth = 1;
                expectedYear = date.Year + 1;
            }
        }

        private static void FindGaps(IDictionary<int, List<InventoryAdPerformace>> sources)
        {
            foreach (var key in sources.Keys)
            {
                var data = sources[key];

                int currentIndex = 0;

                while (true)
                {
                    DateTime currentDate = data[currentIndex].Date;
                    int expectedDay;
                    int expectedMonth;
                    int expectedYear;
                    GetNextExpectedValue(currentDate, out expectedDay, out expectedMonth, out expectedYear);

                    if (currentIndex + 1 >= data.Count)
                        break;

                    DateTime nextDate = data[currentIndex + 1].Date;
                    if (nextDate.Day != expectedDay
                        || nextDate.Month != expectedMonth
                        || nextDate.Year != expectedYear)
                    {
                        //have a gap
                        data.Insert(currentIndex + 1, new InventoryAdPerformace()
                        {
                            DestinationId = key,
                            Source = data[currentIndex].Source,
                            Date = new DateTime(expectedYear, expectedMonth, expectedDay),
                            DetailedPageViews = -1,
                            SearchPageViews = -1,
                            RealPoint = false
                        });
                    }

                    currentIndex++;
                }
            }
        }

        //Uses a linear interpolation to fill in gaps between two real points
        private static void FillGap(IList<InventoryAdPerformace> data, int startIndex, int endIndex)
        {
            double endClickRate = data[endIndex].DetailedPageViews/ (double) data[endIndex].SearchPageViews;
            if (double.IsNaN(endClickRate) || double.IsPositiveInfinity(endClickRate) || double.IsNegativeInfinity(endClickRate))
                endClickRate = 0;

            double startClickRate = data[startIndex].DetailedPageViews / (double)data[startIndex].SearchPageViews;
            if (double.IsNaN(startClickRate) || double.IsPositiveInfinity(startClickRate) || double.IsNegativeInfinity(startClickRate))
                startClickRate = 0;

            double clickDeltaY = endClickRate - startClickRate;
            int deltaX = endIndex - startIndex;

            double clickRateSlope = clickDeltaY/deltaX;

            int equationX = 1;
            for(int x = startIndex + 1; x < endIndex; x++)
            {
                Debug.Assert(!data[x].RealPoint, "Data being filled in should not be a real data point");
                double clickThroughRate = (clickRateSlope*equationX) + startClickRate;

                data[x].ClickThrough = clickThroughRate;
                equationX++;
            }
        }

        private static void LinearFillGaps(IDictionary<int, List<InventoryAdPerformace>> sources)
        {
            foreach (var key in sources.Keys)
            {
                var data = sources[key];

                int x = 0;
                while (true)
                {
                    //skip till we see a gap
                    while (x < data.Count && data[x].RealPoint) 
                        x++;

                    int startGapIndex = x - 1;

                    //see how big gap is
                    while (x < data.Count && !data[x].RealPoint)
                        x++;

                    int endGapIndex = x;
                    if (x >= data.Count)
                        break;
                    
                    FillGap(data, startGapIndex, endGapIndex);
                }
            }
        }

        public static InventoryAdPerformace[] Process(InventoryAdPerformace[] performanceData)
        {
            var dictionary = performanceData.GroupBy(x => x.DestinationId).ToDictionary(x => x.Key, x => x.ToList());
            FindGaps(dictionary);
            LinearFillGaps(dictionary);

            var list = new List<InventoryAdPerformace>();
            foreach(var key in dictionary.Keys)
                list.AddRange(dictionary[key]);

            return list.ToArray();
        }
    }
}
