﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace
{
    internal class PerformanceRepository : IPerformanceRepository
    {
        private IMaxAnalytics _maxAnalytics;

        public PerformanceRepository(IMaxAnalytics maxAnalytics)
        {
            _maxAnalytics = maxAnalytics;
        }

        public InventoryAdPerformace[] GetAdPerformace(int businessUnitId, int inventoryId)
        {
            return _maxAnalytics.GetVehicleAdPerformace(businessUnitId, inventoryId);
        }

        public PriceEvent[] GetPriceEvents(int inventoryId)
        {
            var priceEvents = new List<PriceEvent>();

            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.PriceEvents#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string source = reader.GetString(reader.GetOrdinal("Source"));
                            DateTime date = reader.GetDateTime(reader.GetOrdinal("RepriceDate"));
                            decimal price = reader.GetDecimal(reader.GetOrdinal("NewPrice"));

                            priceEvents.Add(new PriceEvent(){Date = date, Source = source, Price = price});
                        }
                    }
                }
            }

            return priceEvents.ToArray();
        }
    }
}
