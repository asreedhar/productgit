﻿using System;

namespace FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace
{
    public class InventoryAdPerformace
    {
        public int DestinationId { get; set; }
        public string Source { get; set; }
        public DateTime Date { get; set; }
        public double ClickThrough { get; set; }
        public int SearchPageViews { get; set; }
        public int DetailedPageViews { get; set; }
        public bool RealPoint { get; set; }
    }
}
