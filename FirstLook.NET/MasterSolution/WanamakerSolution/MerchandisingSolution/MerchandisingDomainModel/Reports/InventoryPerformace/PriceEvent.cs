﻿using System;

namespace FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace
{
    public class PriceEvent
    {
        public string Source { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }
    }
}
