using System;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class SiteBudget
    {
        public int BusinessUnitId { get; set; }
        public int DestinationId { get; set; }
        public DateTime MonthApplicable { get; set; }
        public int? Budget { get; set; }
        public string DescriptionText { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}