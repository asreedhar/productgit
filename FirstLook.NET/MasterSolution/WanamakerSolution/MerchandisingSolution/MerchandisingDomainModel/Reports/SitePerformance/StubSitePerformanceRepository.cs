using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class StubSitePerformanceRepository : ISitePerformanceRepository
    {
        public Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            return new Dictionary<int, List<SitePerformance>>();
        }

        public IEnumerable<SitePerformance> GetSitePerformances(int businessUnitId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {

            if (businessUnitId == 101619) // Windy City Honda
            {
                if (vehicleType == VehicleType.Both)
                    return new List<SitePerformance> { TestDataSet.AutoTrader, TestDataSet.CarsDotCom, TestDataSet.DealerWebsite };
                if (vehicleType == VehicleType.New)
                    return new List<SitePerformance> { TestDataSet.NewAutoTrader, TestDataSet.CarsDotCom, TestDataSet.DealerWebsite };
                if (vehicleType == VehicleType.Used)
                    return new List<SitePerformance> { TestDataSet.UsedAutoTrader, TestDataSet.CarsDotCom, TestDataSet.DealerWebsite };
            }

            return new List<SitePerformance> { TestDataSet.AutoTrader };
        }

        public Dictionary<string, List<SitePerformance>> GetMonthlySiteTrends(int businessUnitId, VehicleType vehicleType, DateRange dateRange)
        {
            return new Dictionary<string, List<SitePerformance>>();
        }

        public DateTime? GetEarliestDataPoint(int businessUnitId, int siteId)
        {
            return null;
        }


        public static class TestDataSet
        {
            public static SitePerformance AutoTrader;
            public static SitePerformance NewAutoTrader;
            public static SitePerformance UsedAutoTrader;

            public static SitePerformance CarsDotCom;
            public static SitePerformance DealerWebsite;

            static TestDataSet()
            {
                AutoTrader = new SitePerformance
                {
                    Site = new Site { Id = 1, Name = "AutoTrader.com" },
                    SearchPageViewCount = 566,
                    DetailPageViewCount = 250,
                    MapsViewedCount = 5,
                    PhoneLeadsCount = 10,
                    EmailLeadsCount = 21,
                    AdPrintedCount = 2
                };

                NewAutoTrader = new SitePerformance
                {
                    Site = new Site { Id = 1, Name = "AutoTrader.com" },
                    SearchPageViewCount = 200,
                    DetailPageViewCount = 100,
                    MapsViewedCount = 3,
                    PhoneLeadsCount = 7,
                    EmailLeadsCount = 5,
                    AdPrintedCount = 1
                };

                UsedAutoTrader = new SitePerformance
                {
                    Site = new Site { Id = 1, Name = "AutoTrader.com" },
                    SearchPageViewCount = 366,
                    DetailPageViewCount = 150,
                    MapsViewedCount = 2,
                    PhoneLeadsCount = 3,
                    EmailLeadsCount = 17,
                    AdPrintedCount = 1
                };

                CarsDotCom = new SitePerformance
                {
                    Site = new Site { Id = 2, Name = "Cars.com" },
                    SearchPageViewCount = 512,
                    DetailPageViewCount = 241,
                    MapsViewedCount = 10,
                    PhoneLeadsCount = 17,
                    EmailLeadsCount = 21,
                    AdPrintedCount = 3
                };

                DealerWebsite = new SitePerformance
                {
                    Site = new Site { Id = 3, Name = "DealerWebsite.com" },
                    SearchPageViewCount = 288,
                    DetailPageViewCount = 102,
                    MapsViewedCount = 2,
                    PhoneLeadsCount = 3,
                    EmailLeadsCount = 15,
                    AdPrintedCount = 1
                };
            }

        }
    }
}