﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.DomainModel.Enumerations;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class DealerSiteInformation
    {
        public DealerSiteCredentialStatus CredentialStatus { get; set; }
    }
}
