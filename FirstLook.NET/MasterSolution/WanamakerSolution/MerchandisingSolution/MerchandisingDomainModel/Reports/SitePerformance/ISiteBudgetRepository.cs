using System;
using System.Collections.Generic;
// type aliases
using SiteBudgetCollection = System.Collections.Generic.Dictionary<int,System.Collections.Generic.List<FirstLook.Merchandising.DomainModel.Reports.SitePerformance.SiteBudget>>;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public interface ISiteBudgetRepository
    {
        void InsertOrUpdate(SiteBudget siteBudget);
        List<SiteBudget> GetBudgetsByDate(int businessUnitId, DateTime? queryDate);
        SiteBudgetCollection GetAllBudgetsForBusinessUnit(int businessUnitId);
        void ReplaceBudgetsForBusinessUnitId( int businessUnitId, SiteBudgetCollection siteBudgets, DateTime monthRangeBegin, DateTime monthRangeEnd );
    }
}