﻿using System.Collections.Generic;
using System.Data;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class BusinessUnitRepository : IBusinessUnitRepository
    {
        public IEnumerable<BusinessUnit> GetBusinessUnitsByLogin(string login)
        {
            var businessUnits = new List<BusinessUnit>();

            if (string.IsNullOrEmpty(login)) return businessUnits;

            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = string.Format(@"SELECT BusinessUnitID, BusinessUnit FROM Reports.BusinessUnitByMember WHERE Login = '{0}'", login);
                command.CommandType = CommandType.Text;

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var id = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                    var name = reader.GetString(reader.GetOrdinal("BusinessUnit"));

                    businessUnits.Add(new BusinessUnit { Id = id, Name = name });
                }
            }

            return businessUnits;
        }

    }
}
