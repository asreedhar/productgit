﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    internal class SitePerformanceRepository : ISitePerformanceRepository
    {
        private ISiteBudgetRepository _budgetRepository;
        private IMaxAnalytics _maxAnalytics;

        private IGoogleAnalyticsWebAuthorization _googleWebAuthorization;
        private IGoogleAnalyticsRepository _googleRepository;
        private IGoogleAnalyticsResultCache _googleCache;

        private ILog Log = LoggerFactory.GetLogger<SitePerformanceRepository>();

        public SitePerformanceRepository(ISiteBudgetRepository budgetRepository, 
            IGoogleAnalyticsRepository gaRepo, IGoogleAnalyticsWebAuthorization gaWebAuth, IGoogleAnalyticsResultCache googleCache, IMaxAnalytics maxAnalytics)
        {
            _budgetRepository = budgetRepository;
            _googleWebAuthorization = gaWebAuth;
            _googleRepository = gaRepo;
            _googleCache = googleCache;
            _maxAnalytics = maxAnalytics;
        }
        
        private IDataConnection GetConnection()
        {
            return Database.GetConnection(Database.MerchandisingDatabase);
        }

        #region GroupLevel

        private void GetGroupBenchMark(Dictionary<int, List<SitePerformance>> dictionary, int groupId, VehicleType vehicleType)
        {
            using (IDataConnection con = GetConnection())
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.Benchmarking#Group";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "GroupID", groupId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "VehicleType", vehicleType.IntValue, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read()) //only 1 possible result now
                        {
                            var siteId = reader.GetByte(reader.GetOrdinal("SiteId"));
                            var businessunitId = reader.GetInt32(reader.GetOrdinal("BusinessunitID"));
                            var benchMarkImpressionCost = (double)reader.GetDecimal(reader.GetOrdinal("ImpressionCost"));
                            var benchMarkLeadCost = (double)reader.GetDecimal(reader.GetOrdinal("LeadCost"));

                            if (dictionary.ContainsKey(businessunitId))
                            {
                                var site = dictionary[businessunitId].Where(x => x.Site.Id == siteId).SingleOrDefault();
                                if (site != null)
                                {
                                    site.BenchMarkImpressionCost = benchMarkImpressionCost;
                                    site.BenchMarkLeadCost = benchMarkLeadCost;
                                }
                            }
                        }
                    }
                }
            }   
        }

        private void GetGroupListingSitesInventoryCounts(Dictionary<int, List<SitePerformance>> dictionary, int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            _maxAnalytics.GetGroupInventoryCounts(dictionary, groupId, vehicleType, startDate, endDate);
        }

        private void GetGroupBudget(Dictionary<int, List<SitePerformance>> dictionary, int groupId, DateTime startDate, DateTime endDate)
        {
            using (IDataConnection con = GetConnection())
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.SiteBudget#FetchGroupTrend";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "groupID", groupId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "StartDate", startDate, DbType.DateTime);
                    Database.AddRequiredParameter(cmd, "EndDate", endDate, DbType.DateTime);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var destinationId = reader.GetInt32(reader.GetOrdinal("DestinationId"));
                            var businessunitId = reader.GetInt32(reader.GetOrdinal("BusinessunitID"));
                            var budget = reader.GetInt32(reader.GetOrdinal("Budget"));
                            var date = reader.GetDateTime(reader.GetOrdinal("Date"));

                            if (dictionary.ContainsKey(businessunitId))
                            {
                                try
                                {
                                    var site = dictionary[businessunitId].Where(x => x.Site.Id == destinationId && x.DateRange.StartDate == date).SingleOrDefault();
                                    if (site != null)
                                        site.Budget = budget;
                                }
                                catch (Exception) { }
                            }
                        }
                    }
                }
            }
        }

        private Dictionary<int, List<SitePerformance>> GetGroupAutomatedInputSitePerformance(int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            return _maxAnalytics.GetGroupSitePerformance(groupId, vehicleType, startDate, endDate);
        }

        public Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            Log.Debug("Getting Group Site Performance");
            var dictionary = GetGroupAutomatedInputSitePerformance(groupId, vehicleType, startDate, endDate);
            
            Log.Debug("Getting Group Google Site Performance");
            GetGroupSitePerformanceAll(dictionary, groupId, vehicleType, startDate, endDate);

            Log.Debug("Getting Group Budget");
            GetGroupBudget(dictionary, groupId, startDate, endDate);

            Log.Debug("Getting Group Listing Site Inventory Counts");
            GetGroupListingSitesInventoryCounts(dictionary, groupId, vehicleType, startDate, endDate);
            
            // Group Benchmark expects one SitePerformance per site. So combine group trends before calling it.
            dictionary = CombineGroupTrends(dictionary, startDate, endDate);

            Log.Debug("Getting Group Benchmarks");
            GetGroupBenchMark(dictionary, groupId, vehicleType);
            
            return dictionary;
        }

        private Dictionary<int, List<SitePerformance>> CombineGroupTrends(Dictionary<int, List<SitePerformance>> dictionary, DateTime startDate, DateTime endDate)
        {
            Dictionary<int, List<SitePerformance>> filteredDictionary = new Dictionary<int, List<SitePerformance>>();
            Log.Debug("Combining groups");
            foreach (int buid in dictionary.Keys)
            {
                List<SitePerformance> businessUnitSitePerformanceList = new List<SitePerformance>();
                var site_names = dictionary[buid].Where( x => x.BusinessUnitId == buid).Select(x => x.Site.Name).Distinct().ToList();
                foreach( var site in site_names )
                {
                    SitePerformance performanceSum = new SitePerformance();
                    IEnumerable<SitePerformance> siteSpecificMonths = 
                        dictionary[buid].Where(bud => bud != null && bud.Site != null && bud.Site.Name != null && bud.Site.Name.Equals(site));
                    if (siteSpecificMonths.Count() > 0)
                    {
                        Log.DebugFormat("CombineGroupTrends: BusinessUnitId - {0},  SiteName - {1}, MonthCount - {2}", buid, siteSpecificMonths.First().BusinessUnitId, siteSpecificMonths.Count());
                        performanceSum = siteSpecificMonths.Summation();

                        var sd = siteSpecificMonths.Min(db => db.DateRange.StartDate);
                        var ed = siteSpecificMonths.Max(db => db.DateRange.EndDate);
                        performanceSum.DateRange = new DateRange(sd, ed);

                        businessUnitSitePerformanceList.Add(performanceSum);
                    }
                }
                if(businessUnitSitePerformanceList.Count > 0)
                    filteredDictionary.Add(buid, businessUnitSitePerformanceList);
            }

            Log.Debug("Done combining groups");
            return filteredDictionary;
        }

        private void GetGroupSitePerformanceAll(Dictionary<int, List<SitePerformance>> dictionary, int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            foreach(KeyValuePair<int, List<SitePerformance>> kvp in dictionary)
            {
                int businessUnitId = kvp.Key;
                List<SitePerformance> perfDataList = kvp.Value;

                // Get the google and manual site vdps and add them in.
                SitePerformance dealerSpecificSitePerf = GetDealerSitePerformanceResults(businessUnitId, new DateRange(startDate, endDate), vehicleType);
                if (dealerSpecificSitePerf != null && !dealerSpecificSitePerf.Site.Id.Equals((int)Postings.EdtDestinations.Undefined))
                    perfDataList.Add(dealerSpecificSitePerf);
            }
        }


        #endregion

        private void GetBudget(IEnumerable<SitePerformance> sitePerformances, int businessUnitId, DateRange dateRange)
        {
            var siteBudgets = new List<SiteBudget[]>();

            foreach (DateRange month in sitePerformances.Select(sp => sp.DateRange).Distinct())
            {
                var siteBudget = _budgetRepository.GetBudgetsByDate(businessUnitId, month.StartDate);
    
                // Set the month applicable to be the date requested instead of the date the budget was set.
                siteBudget.ToList().ForEach(sb => sb.MonthApplicable = month.StartDate);
                siteBudgets.Add(siteBudget.ToArray());
            }

            foreach (SitePerformance sitePerformance in sitePerformances)
            {
                sitePerformance.Budget = siteBudgets.SelectMany(x => x)
                    .Where(sb => sb.MonthApplicable == sitePerformance.DateRange.StartDate && sb.DestinationId == sitePerformance.Site.Id)
                    .Sum(sb => sb.Budget);
            }
        }

        private void GetBenchMark(IEnumerable<SitePerformance> sitePerformances, int businessUnitId, VehicleType vehicleType)
        {
            var settings = GetMiscSettings.GetSettings(businessUnitId);
            var dealerSegment = settings.DealershipSegment;

            foreach(var sitePerformance in sitePerformances)
            {
                if (sitePerformance.Site.Id < 0) //our freeform website collection page has a negative site id's
                    return;

                 using (IDataConnection con = GetConnection())
                 {
                     con.Open();

                     using (var cmd = con.CreateCommand())
                     {
                         cmd.CommandText = "dashboard.Benchmarking#Fetch";
                         cmd.CommandType = CommandType.StoredProcedure;
                         cmd.AddParameterWithValue("SiteId", sitePerformance.Site.Id);
                         cmd.AddParameterWithValue("VehicleType", vehicleType.IntValue);
                         cmd.AddParameterWithValue("DealerSegmentType", (int)dealerSegment);
                         cmd.AddParameterWithValue("EndDate", sitePerformance.DateRange.EndDate);

                         using (var reader = cmd.ExecuteReader())
                         {
                             while (reader.Read()) //only 1 possible result now
                             {
                                 sitePerformance.BenchMarkImpressionCost = (double)reader.GetDecimal(reader.GetOrdinal("ImpressionCost"));
                                 sitePerformance.BenchMarkLeadCost = (double)reader.GetDecimal(reader.GetOrdinal("LeadCost"));
                             }
                         }
                     }
                 }
            }
        }

        private void GetInventoryCounts(List<SitePerformance> sitePerformanceList, int businessUnitId, VehicleType vehicleType, DateRange dateRange)
        {
	        _maxAnalytics.GetInventoryCounts(sitePerformanceList, businessUnitId, vehicleType, dateRange);
        }

        public Dictionary<string,List<SitePerformance>> GetMonthlySiteTrends(int businessUnitId, VehicleType vehicleType, DateRange dateRange)
        {

            Log.Debug("Getting Monthly Site Trends");
            var result = new Dictionary<string, List<SitePerformance>>();

            Log.Debug("Get Site Performance Trends");
            // autotrader, cars
            List<SitePerformance> trends = _maxAnalytics.GetSitePerformanceTrend(businessUnitId, vehicleType, dateRange.StartDate, dateRange.EndDate);

            Log.Debug("Get Google/Manual Site Performance Trends");
            // dealer website
            DealerVDPData mergedVDPs = GetMergedVDPs(businessUnitId, dateRange, vehicleType);
            if( mergedVDPs != null && (!mergedVDPs.website_provider_id.Equals((int)Postings.EdtDestinations.Undefined)))
            {
                Log.Debug("Found Google/Manual -- merging in to list");
                trends.AddRange( 
                    mergedVDPs.vdps
                        .Where(v => 
                            (vehicleType.Equals(VehicleType.New)) ? v.new_value > 0 : 
                            (vehicleType.Equals(VehicleType.Used)) ? v.used_value > 0 :
                            v.both_value > 0)
                            .Select( v => new SitePerformance {
                                Site = new Site() { Id= mergedVDPs.website_provider_id, Name = mergedVDPs.website_provider_name },
                                DateRange = DateRange.MonthOfDate( DateTime.SpecifyKind(v.month, DateTimeKind.Unspecified)),
                                DetailPageViewCount = (vehicleType.Equals(VehicleType.New)) ? v.new_value : (vehicleType.Equals(VehicleType.Used)) ? v.used_value : v.both_value,
                                Months = 1
                            })
                );
            }

            Log.Debug("Getting Inventory counts");
            GetInventoryCounts(trends, businessUnitId, vehicleType, dateRange);

            Log.Debug("Getting Benchmarks");
            GetBenchMark(trends, businessUnitId, vehicleType);

            Log.Debug("Getting Budgets");
            GetBudget(trends, businessUnitId, dateRange);

            Log.Debug("Re-organizing SitePerformance List");
            // re-organize by site
            foreach (var p in trends)
            {
                List<SitePerformance> sitePerformance = null;
                string siteName = p.Site.Name;
                if( siteName == null )
                    continue; // should not happen but apparently does; why?
                if (result.ContainsKey(siteName))
                {
                    sitePerformance = result[siteName];
                }
                else
                {
                    sitePerformance = new List<SitePerformance>();
                    result.Add(siteName, sitePerformance);
                }

                sitePerformance.Add(p);
            }

            Log.Debug("DONE - Getting Monthly Site Trends");
            return result;
        }
        
        private List<SitePerformance> GetAutomatedInputSitePerformance(int businessUnitId, VehicleType vehicleType, DateTime startDate, DateTime endDate)
        {
            return _maxAnalytics.GetSitePerformance(businessUnitId, vehicleType, startDate, endDate);
        }

        private DealerVDPData GetMergedVDPs(int businessUnitId, DateRange dateRange, VehicleType vehicleType)
        {
            DealerVDPData googleList = GetGoogleSiteVdps(businessUnitId, dateRange, vehicleType);
            DealerVDPData manualList = GetManualVdps(businessUnitId, dateRange, vehicleType);

            /*************
             * TH: 2013-05-09
             *  It is possible to set up more than one manual provider on the wanamaker get page. We only want one.
             *  And, if they have Google Analytics Profiles set up, we only want to use that one.
             *  But if they have GA, and matching manual vdps, use the manual entry for any matching months.
             ************/

            int providerId = 0;
            bool hasGoogle = (googleList != null && googleList.vdps.Count > 0);
            bool hasManual = (manualList != null && manualList.vdps.Count > 0);

            DealerVDPData mergedVdps = new DealerVDPData();

            if (hasGoogle)
            {
                providerId = googleList.website_provider_id;
            }

            if (hasManual && hasGoogle && manualList.website_provider_id.Equals(googleList.website_provider_id))
            {
                // we have matching manual and google data, merge them
                mergedVdps.website_provider_id = providerId;
                mergedVdps.website_provider_name = googleList.website_provider_name;

                dateRange.AsMonths().ToList().ForEach(month =>
                {
                    DatedVDP manual = null;
                    DatedVDP google = null;

                    if (manualList != null)
                        manual = (DatedVDP)manualList.vdps.FirstOrDefault(mvdpVal => mvdpVal.month == month.StartDate);
                    if (googleList != null)
                        google = (DatedVDP)googleList.vdps.FirstOrDefault(gvdpVal => gvdpVal.month == month.StartDate);

                    // Make sure these values are not null before adding them to the collection of DatedVDPs
                    if (manual != null)
                        mergedVdps.vdps.Add(manual);
                    else if (google != null)
                        mergedVdps.vdps.Add(google);
                });
            }
            else if (hasGoogle)
            {
                mergedVdps = googleList;
            }
            else if (hasManual)
            {
                mergedVdps = manualList;
            }

            return mergedVdps;
        }

        private SitePerformance GetDealerSitePerformanceResults(int businessUnitId, DateRange dateRange, VehicleType vehicleType)
        {
            var mergedVdps = GetMergedVDPs(businessUnitId, dateRange, vehicleType);

            // Now we have the set of datedvdps to use, morph that into a SitePerformance object.
            return DealerVdpDataToSitePerformance(businessUnitId, dateRange, vehicleType, mergedVdps.website_provider_id, mergedVdps);
        }

        private SitePerformance DealerVdpDataToSitePerformance(int businessUnitId, DateRange dateRange, VehicleType vehicleType, int providerId, DealerVDPData mergedVdps)
        {
            int detailPageViews = 0;
            int monthsWithData = 0;
            if (vehicleType.Equals(VehicleType.New))
            {
                var newVdps = mergedVdps.vdps.Where(mgvdp => mgvdp.new_value > 0);
                detailPageViews = newVdps.Sum(mgvdp => mgvdp.new_value);
                monthsWithData = newVdps.Count();
            }
            else if (vehicleType.Equals(VehicleType.Used))
            {
                var usedVdps = mergedVdps.vdps.Where(mgvdp => mgvdp.used_value > 0);
                detailPageViews = usedVdps.Sum(mgvdp => mgvdp.used_value);
                monthsWithData = usedVdps.Count();
            }
            else if (vehicleType.Equals(VehicleType.Both))
            {
                var bothVdps = mergedVdps.vdps.Where(mgvdp => mgvdp.both_value > 0);
                detailPageViews = bothVdps.Sum(mgvdp => mgvdp.both_value);
                monthsWithData = bothVdps.Count();
            }

            SitePerformance MorphedSitePerformance = new SitePerformance()
            {
                BusinessUnitId = businessUnitId,
                Site = new Site
                {
                    Id = providerId,
                    Name = mergedVdps.website_provider_name
                },
                Months = monthsWithData,
                DetailPageViewCount = detailPageViews,
                DateRange = dateRange
            };

            return MorphedSitePerformance;
        }
        
        private DealerVDPData GetGoogleSiteVdps(int businessUnitId, DateRange dateRange, VehicleType vehicleType)
        {
            DealerVdpList dealerVdpList = new DealerVdpList();
            
            DealerVDPData dealerVdpData = new DealerVDPData();

            if (_googleWebAuthorization.IsAuthorized(businessUnitId))
            {
                // We only allow one profile type right now, so always use that.
                GoogleAnalyticsProfile gaProf = _googleRepository.FetchProfile(businessUnitId, GoogleAnalyticsType.PC);
                if (gaProf != null)
                {
                    
                    if (gaProf.SiteProvider != Postings.EdtDestinations.Undefined)
                    {
                        Dictionary<DateTime, VDPResult> gvdpTrends = _googleCache.FetchVDPTrend(businessUnitId, dateRange);

                        if (gvdpTrends.Count > 0)
                        {
                            DealerVDPData vdpData;

                            int providerId = (int)gaProf.SiteProvider;

                            bool alreadyExists = dealerVdpList.TryGetValue(providerId, out vdpData);
                            if (!alreadyExists)
                                vdpData = new DealerVDPData();

                            vdpData.website_provider_id = providerId;
                            vdpData.website_provider_name = EnumHelper.GetEnumDescription(typeof(Postings.EdtDestinations), gaProf.SiteProvider);
                            
                            foreach ( KeyValuePair<DateTime, VDPResult> row in gvdpTrends)
                            {
                                vdpData.vdps.Add(MapRowToVdp(row, vehicleType));
                            }

                            if (!alreadyExists)
                                dealerVdpList.Add(providerId, vdpData);

                        }
                    }
                }
            }

            // We only want to support one for now. When we add support for more profile types we'll be able to change this 
            // back so we can return the list instead of the first match.
            if (dealerVdpList.Count > 0)
            {
                dealerVdpData = dealerVdpList.First().Value;
            }

            return dealerVdpData;
        }

        private DatedVDP MapRowToVdp(KeyValuePair<DateTime, VDPResult> row, VehicleType vehicleType)
        {
            return new DatedVDP()
            {
                month = row.Key,
                new_value  = row.Value != null? row.Value.New : 0,
                used_value = row.Value != null? row.Value.Used : 0,
                both_value = row.Value != null? row.Value.Both : 0
            };
        }

        private DealerVDPData GetManualVdps(int businessUnitId, DateRange dateRange, VehicleType vehicleType)
        {
            DealerVdpList dealerVdpList = new DealerVdpList();
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    /***
                     * TH: 2013-05-09
                     *  As of today we're limiting this storedproc so it only returns data for the 
                     *  "currently selected website provider" (Merchandising.settings.Merchandising.SelectedWebsiteProvider)
                     *  remove the join settings.Merchandising in this Proc to return all data for all providers
                     ***/
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "dashboard.WebsiteMetrics#FetchRange";
                    command.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);
                    command.AddRequiredParameter("@StartDate", dateRange.StartDate, DbType.DateTime);
                    command.AddRequiredParameter("@EndDate", dateRange.EndDate, DbType.DateTime);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DealerVDPData vdpData;
                            
                            int providerId = reader.GetInt32(reader.GetOrdinal("WebsiteProviderID"));

                            bool alreadyExists = dealerVdpList.TryGetValue(providerId, out vdpData);
                            
                            if(!alreadyExists)
                                vdpData = new DealerVDPData();

                            vdpData.website_provider_id = providerId;
                            vdpData.website_provider_name = reader.GetString(reader.GetOrdinal("WebsiteProviderName"));

                            vdpData.vdps.Add(
                                new DatedVDP()
                                {
                                    month = reader.GetDateTime(reader.GetOrdinal("MonthApplicable")),
                                    new_value = reader.GetInt32(reader.GetOrdinal("NewVDPs")),
                                    used_value = reader.GetInt32(reader.GetOrdinal("UsedVDPs")),
                                    both_value = reader.GetInt32(reader.GetOrdinal("NewVDPs")) + reader.GetInt32(reader.GetOrdinal("UsedVDPs"))
                                });

                            if (alreadyExists == false)
                                dealerVdpList.Add(providerId, vdpData);
                        }
                    }
                }
            }

            DealerVDPData dealerVdpData = new DealerVDPData();
            if (dealerVdpList.Count > 0)
                dealerVdpData = dealerVdpList.First().Value;

            return dealerVdpData;
        }
      
        public DateTime? GetEarliestDataPoint(int businessUnitId, int siteId)
        {
            using (var connection = GetConnection())
            {
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "dashboard.GetEarliestDataPoint";

                    command.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);
                    command.AddRequiredParameter("SiteId", siteId, DbType.Int32);

                    var date = command.ExecuteScalar();

                    return (date is DBNull) ? null : (DateTime?)date;
                }
            }
        }

        private class DealerVdpList : Dictionary<int, DealerVDPData> { }

        private class DealerVDPData
        {
            public int website_provider_id;
            public string website_provider_name;
            public string username;
            public string password;
            public List<DatedVDP> vdps = new List<DatedVDP>();
        }

        private class DatedVDP
        {
            public DateTime month;
            public int used_value;
            public int new_value;
            public int both_value;
        }

    }
}
