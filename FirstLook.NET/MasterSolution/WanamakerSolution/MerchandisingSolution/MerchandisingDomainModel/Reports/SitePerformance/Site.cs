﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class Site
    {
        [NonAggregate]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
