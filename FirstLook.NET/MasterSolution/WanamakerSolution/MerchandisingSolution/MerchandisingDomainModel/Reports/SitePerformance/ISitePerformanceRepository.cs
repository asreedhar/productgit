﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public interface ISitePerformanceRepository
    {
        Dictionary<int, List<SitePerformance>> GetGroupSitePerformance(int groupId, VehicleType vehicleType, DateTime startDate, DateTime endDate);

        Dictionary<string,List<SitePerformance>> GetMonthlySiteTrends(int businessUnitId, VehicleType vehicleType, DateRange dateRange);
        DateTime? GetEarliestDataPoint(int businessUnitId, int siteId);
    }
}
