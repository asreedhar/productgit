using System.Collections.Generic;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class StubBusinessUnitRepository : IBusinessUnitRepository
    {
        public IEnumerable<BusinessUnit> GetBusinessUnitsByLogin(string login)
        {
            var windyCityPontiac = new BusinessUnit { Id = 101621, Name = "Windy City Pontiac" };
            var windyCityHonda = new BusinessUnit { Id = 101619, Name = "Windy City Honda" };
            var windyCityBmw = new BusinessUnit { Id = 105239, Name = "Windy City BMW" };
            var windyCityToyota = new BusinessUnit { Id = 101620, Name = "Windy City Toyota" };
            var windyCityChevrolet = new BusinessUnit { Id = 101590, Name = "Windy City Chevrolet" };

            return new List<BusinessUnit> { windyCityBmw, windyCityChevrolet, windyCityHonda, windyCityToyota, windyCityPontiac };
        }
    }
}