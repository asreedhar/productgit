﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using System.Linq;
// type aliases
using SiteBudgetCollection = System.Collections.Generic.Dictionary<int,System.Collections.Generic.List<FirstLook.Merchandising.DomainModel.Reports.SitePerformance.SiteBudget>>;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class SiteBudgetRepository : ISiteBudgetRepository
    {
        public void InsertOrUpdate(SiteBudget siteBudget)
        {
            using (var connection = GetConnection())
            using (var cmd = connection.CreateCommand())
            {
                connection.Open();
                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "settings.SiteBudget#Update";

                cmd.AddRequiredParameter("BusinessUnitId", siteBudget.BusinessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("DestinationId", siteBudget.DestinationId, DbType.Int32);
                // Note: DateTime passed in to MonthApplicable will be automatically mapped (by the stored proc.) to the "First Moment of the First Day of the Containing Month"
                //   for example:   2012-11-02 17:15:39.850 --> 2012-11-01 00:00:00.000
                cmd.AddRequiredParameter("MonthApplicable", siteBudget.MonthApplicable, DbType.DateTime); 
                cmd.AddRequiredParameter("Budget", (siteBudget.Budget == 0 ? null : siteBudget.Budget), DbType.Int32); 
                cmd.AddRequiredParameter("DescriptionText", siteBudget.DescriptionText, DbType.String);
                
                cmd.ExecuteNonQuery();
            }
        }

        public List<SiteBudget> GetBudgetsByDate(int businessUnitId, DateTime? monthApplicable = null)
        {
            if( monthApplicable == null )
                monthApplicable = DateTime.Now;

            var siteBudgets = new List<SiteBudget>();

            using (var connection = GetConnection())
            using (var cmd = connection.CreateCommand())
            {
                connection.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "settings.SiteBudget#FetchList";

                cmd.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("MonthApplicable", monthApplicable, DbType.DateTime);

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.IsDBNull(reader.GetOrdinal("Budget")))
                        continue;

                    var budget = SiteBudget_fromReader(reader);

                    if (budget.Budget > 0 )
                        siteBudgets.Add(budget);
                }
            }
            
            return siteBudgets;
        }

        public void ClearCurrentBudget(SiteBudget siteBudget)
        {
            siteBudget.Budget = null;
            siteBudget.DescriptionText = null;

            InsertOrUpdate( siteBudget );
        }

        public SiteBudgetCollection GetAllBudgetsForBusinessUnit( int businessUnitId )
        {
            var budgets = new SiteBudgetCollection();
            using (var connection = GetConnection())
            using (var cmd = connection.CreateCommand())
            {
                connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = 
                    "SELECT * FROM Merchandising.settings.SiteBudget WHERE BusinessUnitId = @businessUnitId";
                cmd.AddRequiredParameter( "@businessUnitId", businessUnitId, DbType.Int32 );
                //
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var budget = SiteBudget_fromReader( reader );
                    //
                    List<SiteBudget> site = null;
                    if( budgets.ContainsKey( budget.DestinationId ))
                        site = budgets[ budget.DestinationId ];
                    else {
                        site = new List<SiteBudget>();
                        budgets[ budget.DestinationId ] = site;
                    }
                    site.Add( budget );
                }
            }
            return budgets;
        }

        public void ReplaceBudgetsForBusinessUnitId( int businessUnitId, SiteBudgetCollection siteBudgets, DateTime monthRangeBegin, DateTime monthRangeEnd )
        {
            foreach( var site_id in siteBudgets.Keys )
            {
                var site = siteBudgets[ site_id ];

                using (var connection = GetConnection())
                {
                    connection.Open();

                    // clear out all budget data in the given time range
                    using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = 
                            "DELETE FROM Merchandising.settings.SiteBudget WHERE BusinessUnitId = @BusinessUnitId AND DestinationId = @DestinationId AND MonthApplicable >= @monthRangeBegin AND MonthApplicable <= @monthRangeEnd";
                        cmd.AddRequiredParameter( "@BusinessUnitId", businessUnitId, DbType.Int32 );
                        cmd.AddRequiredParameter( "@DestinationId", site_id, DbType.Int32 );
                        cmd.AddRequiredParameter( "@monthRangeBegin", monthRangeBegin, DbType.DateTime );
                        cmd.AddRequiredParameter( "@monthRangeEnd", monthRangeEnd, DbType.DateTime );
                        //
                        cmd.ExecuteNonQuery();
                    }

                    // save each explicitly-defined (non-soft) budget given
                    foreach( var budget in site )
                    {
                        InsertOrUpdate( budget );
                    }
                }
            }
        }

        protected static IDataConnection GetConnection()
        {
            return Database.GetConnection(Database.MerchandisingDatabase);
        }

        protected static SiteBudget SiteBudget_fromReader(IDataRecord record)
        {
            return new SiteBudget
            {
                BusinessUnitId =  record.GetInt32(    record.GetOrdinal("BusinessUnitId")),
                DestinationId =   record.GetInt32(    record.GetOrdinal("DestinationId")),
                MonthApplicable = record.GetDateTime( record.GetOrdinal("MonthApplicable")),
                Budget =          record.IsDBNull(    record.GetOrdinal("Budget"))          ? 0 :                record.GetInt32( record.GetOrdinal("Budget")),
                DescriptionText = record.IsDBNull(    record.GetOrdinal("DescriptionText")) ? null :             record.GetString( record.GetOrdinal("DescriptionText")),
                UpdatedOn =       record.IsDBNull(    record.GetOrdinal("UpdatedOn"))       ? (new DateTime()) : record.GetDateTime( record.GetOrdinal("UpdatedOn"))
            };
        }
    }
}
