﻿using FirstLook.Common.Core;
using MAX.Entities.Interfaces;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public class SitePerformance : IBusinessUnitAggregation
    {
        public Site Site { get; set; }

        public DateRange DateRange { get; set; }

        [NonAggregate]
        public int BusinessUnitId { get; set; }
        
        public string BusinessUnit { get; set; }

        public int SearchPageViewCount { get; set; }
        public int DetailPageViewCount { get; set; }
        public int EmailLeadsCount { get; set; }
        public int PhoneLeadsCount { get; set; }
        public int MapsViewedCount { get; set; }
        public int AdPrintedCount { get; set; }
        public int ChatRequestsCount { get; set; }

        public int Months { get; set; }
        
        public int? Budget { get; set; }

        [NonAggregate]
        public double? BenchMarkLeadCost { get; set; }
        [NonAggregate]
        public double? BenchMarkImpressionCost { get; set; }

        public int? InventoryCount { get; set; }       

        public int TotalLeads
        {
            get
            {
                int sum = EmailLeadsCount + PhoneLeadsCount;
                if (sum < 0) //sometimes negative in data
                    sum = 0;

                return sum;
            }
        }

        public int TotalActions
        {
            get
            {
                int sum = EmailLeadsCount + PhoneLeadsCount + MapsViewedCount + AdPrintedCount;
                if (sum < 0) //sometimes negative in data
                    sum = 0;

                return sum;
            }
        }

        public decimal ClickThroughRate
        {
            get
            {
               if (SearchPageViewCount == 0) return 0;
               return (decimal)DetailPageViewCount / SearchPageViewCount;
            }
        }

        public decimal ConversionRateFromDetailPage
        {
            get
            {
                if (DetailPageViewCount == 0)
                    return 0;

                return (decimal)TotalActions / DetailPageViewCount;
            }
        }

    }
}
