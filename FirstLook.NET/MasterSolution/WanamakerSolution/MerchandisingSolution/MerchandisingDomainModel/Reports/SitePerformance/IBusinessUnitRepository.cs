using System.Collections.Generic;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Reports.SitePerformance
{
    public interface IBusinessUnitRepository
    {
        IEnumerable<BusinessUnit> GetBusinessUnitsByLogin(string login);
    }
}