﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Reports
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NonAggregateAttribute : Attribute
    {
    }
}
