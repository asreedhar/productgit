﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using MAX.Entities.Reports.TimeToMarket;
using MAX.Entities.Reports.TimeToMarket.Items;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class GroupTimeToMarketReportVm
    {
        private readonly GroupReport _report;
        private readonly List<TimeToMarketGroupBusinessUnitReportVm> _businessUnits = new List<TimeToMarketGroupBusinessUnitReportVm>();

        public GroupTimeToMarketReportVm(GroupReport report)
        {
            _report = report;

            _report.BusinessUnits.ForEach(bu => 
                _businessUnits.Add(new TimeToMarketGroupBusinessUnitReportVm(bu)));
        }

        public int GroupId
        {
            get { return _report.GroupId; }
        }

        public DateRange DateRange
        {
            get { return _report.DateRange; }
        }

        public int VehicleCount
        {
            get { return _report.VehicleCount; }
        }

        public List<TimeToMarketGroupBusinessUnitReportVm> BusinessUnits
        {
            get { return _businessUnits; }
        }

        public double AverageDaysToPhotoOnline
        {
            get { return _report.AverageDaysToPhotoOnline; }
        }

        public double AverageDaysToCompleteAdOnline
        {
            get { return _report.AverageDaysToCompleteAdOnline; }
        }

        public double LowestMinDaysToPhotoOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Min(bu => bu.MinDaysPhoto));
            }
        }

        public double HighestMinDaysToPhotoOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Max(bu => bu.MinDaysPhoto));
            }
        }

        public double LowestMaxDaysToPhotoOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Min(bu => bu.MaxDaysPhoto));
            }
        }

        public double HighestMaxDaysToPhotoOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Max(bu => bu.MaxDaysPhoto));
            }
        }

        public double LowestAvgDaysToPhotoOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Min(bu => bu.AverageDaysToPhotoOnline));
            }
        }

        public double HighestAvgDaysToPhotoOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Max(bu => bu.AverageDaysToPhotoOnline));
            }
        }

        public double LowestMinDaysToAdOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Min(bu => bu.AverageDaysToCompleteAd));
            }
        }

        public double HighestMinDaysToAdOnline
        {
            get
            {
                return (BusinessUnits.Count <= 0 ? 0 : BusinessUnits.Max(bu => bu.AverageDaysToCompleteAd));
            }
        }
    }

    public class TimeToMarketGroupBusinessUnitReportVm
    {
        private readonly BusinessUnitReport _businessUnitReport;

        public TimeToMarketGroupBusinessUnitReportVm(BusinessUnitReport businessUnitReport)
        {
            _businessUnitReport = businessUnitReport;
        }
        
        public List<DailyAverage> Days { get { return _businessUnitReport.Days; } }
        public DateRange DateRange { get { return _businessUnitReport.DateRange; } }
        public string BusinessUnitHome { get { return _businessUnitReport.BusinessUnitHome; } }
        public int BusinessUnitId { get { return _businessUnitReport.BusinessUnitId; } }
        public string BusinessUnit { get { return _businessUnitReport.BusinessUnit; } }
        public int VehicleCount { get { return _businessUnitReport.VehicleCount; } }
        public int DaysCount { get { return _businessUnitReport.DaysCount; } }
        public int MinDaysPhoto { get { return _businessUnitReport.MinDaysPhoto; } }
        public int MaxDaysPhoto { get { return _businessUnitReport.MaxDaysPhoto; } }
        public int MinDaysComplete { get { return _businessUnitReport.MinDaysComplete; } }
        public int MaxDaysComplete { get { return _businessUnitReport.MaxDaysComplete; } }
        public double AverageDaysToPhotoOnline { get { return _businessUnitReport.AverageDaysToPhotoOnline; } }
        public double AverageDaysToCompleteAd { get { return _businessUnitReport.AverageDaysToCompleteAd; } }
    }
}
