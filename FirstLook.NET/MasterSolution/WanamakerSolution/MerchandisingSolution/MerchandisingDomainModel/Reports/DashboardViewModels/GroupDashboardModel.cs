﻿namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class GroupDashboardModel
    {
        public int BusinessUnitId { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHome { get; set; }
        
        public DashboardModel Data { get; set; }
    }
}