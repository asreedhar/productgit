﻿namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class DashboardSettingsModel
    {
        public int BusinessUnitId { get; set; }
        public bool HasAnalyticsSuite { get; set; }
        public bool ShowTimeToMarket { get; set; }
        public bool ShowOnlineClassifiedOverview { get; set; }
        public string MerchandisingAlertSelectionBucket { get; set; }
        public string InventoryBaseUrl { get; set; }
        public bool HasDigitalShowroom { get; set; }
        public bool HasWebsite20 { get; set; }
        public bool HasShowroomReporting { get; set; }
    }
}