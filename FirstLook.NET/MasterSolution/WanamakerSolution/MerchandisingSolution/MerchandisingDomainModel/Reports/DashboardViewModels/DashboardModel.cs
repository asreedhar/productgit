﻿using System.Collections.Generic;
using System.Web.Mvc;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using MAX.Entities;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class DashboardModel
    {
        public Dictionary<string, DealerSiteInformation> DealerSiteData { get; set; }

        public VehicleType SelectedUsedNewFilter { get; set; }

        public IEnumerable<SelectListItem> UsedNewFilters { get; set; }

        public int SelectedFilterCount { get; set; }

        public Buckets BucketsData { get; set; }

        public string BulkUploadUrl { get; set; }

        public string NotOnlineUrl { get; set; }

        public string LowActivityReportUrl { get; set; }

        public IDashboardBusinessUnit BusinessUnit { get; set; }
        public bool HasAnalyticsSuite { get; set; }

        public SitePerformanceData PerformanceData { get; set; }

        public Dictionary<string, bool> BucketsActive { get; set; } // bucket id -> is active

        public AutoApproveStatus AutoApprove { get; set; }

        public DashboardSettingsModel DashboardSettings { get; set; }
        public string DashboardSettingsJson { get { return DashboardSettings.ToJson(); } }
    }


}