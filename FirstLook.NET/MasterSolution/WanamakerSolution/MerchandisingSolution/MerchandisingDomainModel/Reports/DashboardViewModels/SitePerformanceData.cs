﻿using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class SitePerformanceData
    {
        public Dictionary<string, SitePerformanceVm> Sites { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string TimePeriod { get { return string.Format("{0} - {1}", StartDate.ToShortDateString(), EndDate.ToShortDateString()); } }


    }
}