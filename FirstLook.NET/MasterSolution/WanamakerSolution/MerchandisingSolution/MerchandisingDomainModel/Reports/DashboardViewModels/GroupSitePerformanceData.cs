﻿
namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class GroupSitePerformanceData
    {
        public int BusinessUnitId { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHome { get; set; }
        public SitePerformanceData Data { get; set; }
    }
}