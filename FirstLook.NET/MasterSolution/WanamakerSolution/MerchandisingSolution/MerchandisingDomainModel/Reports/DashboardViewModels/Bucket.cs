﻿namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class Bucket
    {
        public string DisplayName { get; set; }
        public string NavigationUrl { get; set; }
        public string Id { get; set; }
        public int Count { get; set; }
       
    }
}