﻿namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class SitePerformanceVm
    {
        public string SiteName { get; set; }

        public int SearchValue { get; set; }

        public int DetailValue { get; set; }

        public int ActionValue { get; set; }

        public decimal ClickThroughRate { get; set; } 

        public decimal ConversionRate { get; set; }

        public int Months { get; set; }

        public int? Budget { get; set; }

        public double? BenchMarkLeadCost { get; set; }
        public double? BenchMarkImpressionCost { get; set; }

        public int? InventoryCount { get; set; }

        public int EmailValue { get; set; }

        public int PhoneValue { get; set; }

        public int AdPrintValue { get; set; }

        public int MapValue { get; set; }

        public int ChatValue { get; set; }
    }
}