﻿
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class SearchMaxSEResult
    {
        public string MaxSEUrl { get; set; }

        public CustomMessage StatusMessage { get; set; }
    }
}