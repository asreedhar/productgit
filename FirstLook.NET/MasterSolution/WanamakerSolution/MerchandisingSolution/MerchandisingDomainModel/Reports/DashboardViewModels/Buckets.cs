﻿using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class Buckets
    {
        public List<Bucket> ActionBuckets { get; set; }

        public List<Bucket> ActivityBuckets { get; set; }

        public List<Bucket> MaxBuckets { get; set; }

        public int ActionBucketsTotal { get; set; }

        public int ActivityBucketsTotal { get; set; }

        public int NeedsActionCount { get; set; }

        public int SelectedFilterCount { get; set; }

        public int LowActivityCount { get; set; }

        //We need the special handling because we round off to a whole number
        //if between 0 and 1 we don't want 0 but 1
        public int LowActivityPercent
        {
            get
            {
                if (SelectedFilterCount == 0 || LowActivityCount > SelectedFilterCount) return 0;

                var val = ((decimal)LowActivityCount / SelectedFilterCount) * 100;
                return (val > 0 && val < 1) ? 1 : (int)Math.Round(val);
            }
        }

        public int NotOnlineCount { get; set; }


        //This calculation is done not on the basis of total inventory but the days specified in the settings

        public double NotOnlinePercent { get; set; }

        public int NotOnlineAgeInDays { get; set; }

        public Bucket OfflineBucket { get; set; }
    }
}