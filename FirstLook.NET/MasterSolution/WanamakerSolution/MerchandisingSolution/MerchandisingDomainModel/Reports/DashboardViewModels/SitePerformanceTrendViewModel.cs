﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Reports;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class SitePerformanceTrendViewModel
    {
        private Dictionary<string, List<SitePerformance.SitePerformance>> _trendData = new Dictionary<string, List<SitePerformance.SitePerformance>>();

        public SitePerformanceData Totals
        {
            get;
            set;
        }

        public Dictionary<string, List<SitePerformance.SitePerformance>> Trends
        {
            get { return _trendData; }
            set { _trendData = value; }
        }
        
        public SitePerformanceTrendViewModel() { }

        public SitePerformanceTrendViewModel(Dictionary<string, List<SitePerformance.SitePerformance>> trendData, SitePerformanceData mappedTotals)
        {
            // TODO: Complete member initialization
            this.Trends = trendData;
            this.Totals = mappedTotals;
        }
    }
}