﻿namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class UserPermissions
    {
        public int[] UserBusinessUnits { get; set; }
        public int[] AnalyticsSuiteBusinessUnits { get; set; }
        public int[] OnlineClassifiedOverviewBusinessUnits { get; set; }
        public int[] TimeToMarketBusinessUnits { get; set; }
        public int[] ShowroomBusinessUnits { get; set; }
        public int[] Website20BusinessUnits { get; set; }
        public int[] ShowroomReportsBusinessUnits { get; set; }
        public bool IsReportViewer { get; set; }
    }
}