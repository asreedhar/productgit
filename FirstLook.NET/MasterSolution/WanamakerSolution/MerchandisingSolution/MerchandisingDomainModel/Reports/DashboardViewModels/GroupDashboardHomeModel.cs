﻿using System.Collections.Generic;
using System.Web.Mvc;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels
{
    public class GroupDashboardHomeModel
    {
        public VehicleType SelectedUsedNewFilter { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public IEnumerable<SelectListItem> UsedNewFilters { get; set; }
        public int SelectedFilterCount { get; set; }

        public string UserDealers { get; set; }
    }
}