﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Reports
{
    [Serializable]
    public class BusinessUnitReport : IBusinessUnitAggregation, IEquatable<BusinessUnitReport>
    {
        #region fields

        private List<ReportVehicle> _vehicles = new List<ReportVehicle>();
        private List<DailyAverage> _days = new List<DailyAverage>();

        #endregion fields

        #region Properties

        public List<ReportVehicle> Vehicles 
        { 
            get { return _vehicles; } 
            set { _vehicles = value; }
        }
        
        public List<DailyAverage> Days 
        { 
            get { return _days; } 
            set { _days = value; } 
        }
        
        public DateRange DateRange { get; set; }
        public string BusinessUnitHome { get { return Helper.RewriteDealerSpecificURL(BusinessUnitId, "Default.aspx?ref=GLD"); } }

        // Calculated values
        // ReSharper disable PossibleNullReferenceException
        public int BusinessUnitId { get { return ( VehicleCount == 0) ? -1 : Vehicles.FirstOrDefault().BusinessUnitId; } }
        public string BusinessUnit { get { return (VehicleCount == 0) ? string.Empty : Vehicles.FirstOrDefault().BusinessUnit; } }
        // ReSharper restore PossibleNullReferenceException
        public int VehicleCount { get { return Vehicles.Count; } }
        public int DaysCount { get { return Days.Count; } }
        
        public int MinDaysPhoto { get { return ( VehicleCount == 0) ? 0 : Convert.ToInt32(Vehicles.Min(veh => veh.ElapsedDaysPhoto)); } }
        public int MaxDaysPhoto { get { return ( VehicleCount == 0) ? 0 : Convert.ToInt32(Vehicles.Max(veh => veh.ElapsedDaysPhoto)); } }
        public int MinDaysComplete { get { return (VehicleCount == 0) ? 0 : Convert.ToInt32(Vehicles.Min(veh => veh.ElapsedDaysCompleteAd)); } }
        public int MaxDaysComplete { get { return (VehicleCount == 0) ? 0 : Convert.ToInt32(Vehicles.Max(veh => veh.ElapsedDaysCompleteAd)); } }

        public double AverageDaysToPhotoOnline
        {
            get
            {
                var applicableVehicles = Vehicles.Where(veh => (veh.DateOfFirstPhoto.Equals(DateTime.MinValue)).Equals(false)).ToArray();
                var applicableVehicleCount = applicableVehicles.Count();
                return (applicableVehicleCount == 0) ? 0 : (applicableVehicles.Sum(veh => veh.ElapsedDaysPhoto) / applicableVehicleCount);
            }
        }

        public double AverageDaysToCompleteAd
        {
            get
            {
                var applicableVehicles = Vehicles.Where(veh => (veh.DateOfFirstCompleteAd.Equals(DateTime.MinValue)).Equals(false)).ToArray();
                var applicableVehicleCount = applicableVehicles.Count();
                return (applicableVehicleCount == 0) ? 0 : (applicableVehicles.Sum(veh => veh.ElapsedDaysCompleteAd) / applicableVehicleCount);
            }
        }

        #endregion Properties

        public bool Equals(BusinessUnitReport other)
        {
            return BusinessUnitId == other.BusinessUnitId;
        }

    }
}