﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.Entities.Reports.TimeToMarket;
using MAX.Entities.Reports.TimeToMarket.Items;
using VehicleType = MAX.Entities.VehicleType;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket
{
	public class TimeToMarketRepository : ITimeToMarketRepository
    {
        private readonly IAdStatusRepository _adStatusRepository;
	    private readonly IInventoryDataRepository _inventoryDataRepository;
	    private readonly IDashboardBusinessUnitRepository _businessUnitRepository;
	    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly Lazy<IPhotoServices> PhotoServices =
            new Lazy<IPhotoServices>(Registry.Resolve<IPhotoServices>,
                                     true);

        private IVehiclesPhotosResult _photoResults;
        private readonly List<IInventoryData> _inventoryData = new List<IInventoryData>();
        private DateTime? _batchTimeStamp;
        private IEnumerable<IAdStatusResult> _gidResults;
	    private GidProviders _gidProviders;
		private int _businessUnitId;

		public TimeToMarketRepository(IAdStatusRepository adStatusRepository, IInventoryDataRepository inventoryDataRepository, IDashboardBusinessUnitRepository businessUnitRepository)
	    {
		    _adStatusRepository = adStatusRepository;
		    _inventoryDataRepository = inventoryDataRepository;
		    _businessUnitRepository = businessUnitRepository;
	    }

	    #region Implement Interface

        public void Initialize(int businessUnitId, GidProviders gidProviders)
        {
            _photoResults = null;
            _batchTimeStamp = null;
            _inventoryData.Clear();
	        GetInventoryData(businessUnitId);
            _gidResults = null;
	        _gidProviders = gidProviders;
	        _businessUnitId = businessUnitId;
        }

        /// <summary>
        /// Returns a list of vins that are missing some data in our tracking table
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="reportStartDate"></param>
        /// <returns></returns>
        public IEnumerable<RowItem> GetVinData(int businessUnitId, DateTime reportStartDate)
        {
            var vins = new List<RowItem>();
            Log.Debug("Getting VINs to update");
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.TimeToMarket#VehiclesWithMissingData";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter("@StartDate", reportStartDate, DbType.DateTime);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            vins.Add(new RowItem(reader));
                        }
                    }
                }
            }

            // Some vehicles can be marked as offline, do not include those vehicles in the list of "to be updated" items.
            vins = FilterDoNotPostInventory(vins);

            Log.DebugFormat("Found {0} VINs to update", vins.Count);

            return vins;
        }

        #region Fill Methods

        #region Fill by GID data methods

        public void FillPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            Log.DebugFormat("AdStatusResults - PhotosFirstDate");
            UpdateGidData(
                updatableVins,
                item => item.PhotosFirstDate.HasValue.Equals(false),
				result => ((MaxAnalyticsAd)result).PhotoUrls != null && ((MaxAnalyticsAd)result).PhotoUrls.Any(),
                (itemToUpdate, matchingAd) => itemToUpdate.PhotosFirstDate = ((MaxAnalyticsAd)matchingAd).StatusDate
            );
        }

        public void FillMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            Log.DebugFormat("AdStatusResults - MinimumPhotoFirstDate");
            var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
            if (!rowItems.Any())
                return;

            var businessUnitId = rowItems.First().BusinessUnitId;
            var minimumPhotos = GetMinimumPhotoCount(businessUnitId);

            UpdateGidData(
                rowItems,
                item => item.MinimumPhotoFirstDate.HasValue.Equals(false),
                result => ((MaxAnalyticsAd)result).PhotoUrls != null && ((MaxAnalyticsAd)result).PhotoUrls.Count() >= minimumPhotos,
                (itemToUpdate, matchingAd) => itemToUpdate.MinimumPhotoFirstDate = ((MaxAnalyticsAd)matchingAd).StatusDate
            );
        }

        public void FillPriceFirstDate(IEnumerable<RowItem> updatableVins)
        {
            Log.DebugFormat("AdStatusResults - PriceFirstDate");
            UpdateGidData(
                updatableVins,
                item => item.PriceFirstDate.HasValue.Equals(false),
                result => ((MaxAnalyticsAd)result).Price > 0,
                (itemToUpdate, matchingAd) => itemToUpdate.PriceFirstDate = ((MaxAnalyticsAd)matchingAd).StatusDate
            );
        }

        public void FillDescriptionFirstDate(IEnumerable<RowItem> updatableVins)
        {
            Log.DebugFormat("AdStatusResults - DescriptionFirstDate");
            UpdateGidData(
                updatableVins,
                item => item.DescriptionFirstDate.HasValue.Equals(false),
                result => !string.IsNullOrWhiteSpace(((MaxAnalyticsAd)result).Description),
                (itemToUpdate, matchingAd) => itemToUpdate.DescriptionFirstDate = ((MaxAnalyticsAd)matchingAd).StatusDate
            );
        }

        public void FillGidFirstDate(IEnumerable<RowItem> updatableVins)
        {
            Log.DebugFormat("AdStatusResults - GidFirstDate");
            UpdateGidData(
                updatableVins,
                item => item.GidFirstDate.HasValue.Equals(false),
                result => (!string.IsNullOrWhiteSpace(((MaxAnalyticsAd)result).Description) || ((MaxAnalyticsAd)result).PhotoUrls.Any() || ((MaxAnalyticsAd)result).Price > 0),
                (itemToUpdate, matchingAd) => itemToUpdate.GidFirstDate = ((MaxAnalyticsAd)matchingAd).StatusDate
            );
        }
        #endregion Fill by GID data methods

        #region TimeToMax dates (original calculations prior to FB 29032)
        public void FillMAXPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            var rowItems = updatableVins as IList<RowItem> ?? updatableVins.ToList();
            if (!rowItems.Any()) return;
            Log.Debug("MAXPhotoFirstDate");
            var vinsWithNewPhotos = FillByPhotoCount(rowItems.First().BusinessUnitId,
                rowItems.Where(ttmri => ttmri.MAXPhotoFirstDate.HasValue.Equals(false)).Select(ttmri => ttmri.VIN),
                1,
                reportStartDate
            ).ToArray();

            // Set the PhotosFirstDate accordingly
            Log.DebugFormat("Found {0}", vinsWithNewPhotos.Length);

            Array.ForEach(vinsWithNewPhotos, vin =>
            {
                rowItems.ToList().Find(
                    ttmri =>
                        ttmri.VIN.Equals(vin) &&
                        ttmri.MAXPhotoFirstDate.HasValue.Equals(false)
                    ).MAXPhotoFirstDate = GetBatchTimeStamp();
            });
        }
        public void FillMAXMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate)
        {
            var enumerable = updatableVins as RowItem[] ?? updatableVins.ToArray();
            var rowItems = updatableVins as IList<RowItem> ?? enumerable.ToList();
            if (!rowItems.Any()) return;
            Log.DebugFormat("MAXMinimumPhotoFirstDate");

            var businessUnitId = rowItems.First().BusinessUnitId;
            var minimumPhotos = GetMinimumPhotoCount(businessUnitId);

            var vinsWithNewMinimumPhotos = FillByPhotoCount(businessUnitId,
                rowItems.Where(ttmri => ttmri.MAXMinimumPhotoFirstDate.HasValue.Equals(false)).Select(ttmri => ttmri.VIN),
                minimumPhotos,
                reportStartDate
            ).ToArray();

            Log.DebugFormat("Found {0}", vinsWithNewMinimumPhotos.Length);
            // Set the PhotosFirstDate accordingly
            Array.ForEach(vinsWithNewMinimumPhotos, vin =>
            {
                enumerable.ToList().Find(
                    ttmri =>
                        ttmri.VIN.Equals(vin) &&
                        ttmri.MAXMinimumPhotoFirstDate.HasValue.Equals(false)
                    ).MAXMinimumPhotoFirstDate = GetBatchTimeStamp();
            });
        }
        public void FillMAXPriceFirstDate(IEnumerable<RowItem> updatableInventory)
        {
            var updatableInv = updatableInventory as RowItem[] ?? updatableInventory.ToArray();
            if (updatableInv.Any())
            {
                Log.Debug("MAXPriceFirstDate");

                var needsPriceVins = updatableInv.Where(ttmri => ttmri.MAXPriceFirstDate.HasValue.Equals(false)).ToArray();
                Log.DebugFormat("{0} vehicles missing MAXPriceFirstDate", needsPriceVins.Length);

                const WorkflowType wft = WorkflowType.NoPrice;
                var bin = Bin.Create(wft);

                if (needsPriceVins.Any())
                {
                    // A list of the Inventory for this businessUnit that does not have a price.
                    
                    var id = new List<InventoryData>();
					id.AddRange(GetInventoryData(needsPriceVins.First().BusinessUnitId).Select(data => data as InventoryData));

                    var noPriceList = InventoryDataFilter.WorkflowSearch(needsPriceVins.First().BusinessUnitId, id, 0, 10000, String.Empty, bin);

                    // A collection of inventory that needs prices, except where it appears in the "NoPriceList" bucket. So, if it does not appear in 
                    // that bucket, it has a price!
                    var hasPriceVins = needsPriceVins.Select(uv => uv).Except(noPriceList.Select(npl => new RowItem
                    {
                        BusinessUnitId = npl.BusinessUnitID,
                        InventoryId = npl.InventoryID,
                        VIN = npl.VIN,
                        StockNumber = npl.StockNumber
                    })).ToArray();

                    Log.DebugFormat("After checking {0}", hasPriceVins.Count());

                    // foreach updatableVin
                    // if that vin .PriceFirstDate.HasValue.Equals(false)
                    // and that inventoryid is in hasPriceVins
                    // set the PriceFirstDate to GetBatchTimeStamp();
                    Array.ForEach(hasPriceVins.ToArray(), vin =>
                    {
                        updatableInv.ToList().Find(
                            ttmri =>
                                ttmri.Equals(vin) &&
                                ttmri.MAXPriceFirstDate.HasValue.Equals(false)
                        ).MAXPriceFirstDate = GetBatchTimeStamp();
                    });
                }
            }
        }
        public void FillMAXDescriptionFirstDate(IEnumerable<RowItem> updatableVins)
        {
            var updateVins = updatableVins as RowItem[] ?? updatableVins.ToArray();
            if (updateVins.Any())
            {
                Log.Debug("MAXDescriptionFirstDate");
                var needsDescVins = updateVins.Where(ttmri => ttmri.MAXDescriptionFirstDate.HasValue.Equals(false)).ToArray();

                if (needsDescVins.Any())
                {
                    var hasDescriptionVins = new List<string>();
                    Array.ForEach(needsDescVins.ToArray(), npv =>
                    {
                        // LastDescriptionPublicationDate is datePosted in the view: Merchandising.workflow.Inventory
                        if (GetInventoryData(needsDescVins.First().BusinessUnitId).Find(id => id.VIN.Equals(npv.VIN) && id.LastDescriptionPublicationDate.HasValue) != null)
                        {
                            hasDescriptionVins.Add(npv.VIN);
                        }
                    });

                    Log.DebugFormat("Found {0} after checking", hasDescriptionVins.Count);

                    // foreach updatableVin
                    // if that vin .DescriptionFirstDate.HasValue.Equals(false)
                    // and that inventoryid is in hasDescription
                    // set the DescriptionFirstDate to GetBatchTimeStamp();
                    Array.ForEach(hasDescriptionVins.ToArray(), vin =>
                    {
                        updateVins.ToList().Find(
                            ttmri =>
                                ttmri.VIN.Equals(vin) &&
                                ttmri.MAXDescriptionFirstDate.HasValue.Equals(false)
                        ).MAXDescriptionFirstDate = GetBatchTimeStamp();
                    });
                }
            }
        }

        /// <summary>
        /// Always run this last. We set adcomplete based on how other other fields are set by this process.
        /// </summary>
        /// <param name="updatableVins"></param>
        public void FillCompleteAdFirstDate(IEnumerable<RowItem> updatableVins)
        {
            var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
            if (rowItems.Any())
            {
                Log.DebugFormat("CompleteAdFirstDate");

                // For complete ad first get the correct AdCompleteFlags setting for this businessUnitId
                // then check the updatable vins for records that do not have an adcomplete date
                // check those vins for fields matching the adcompleteflags setting
                // set adcomplete if the times match the current setting.
                var buCompleteFlags = GetAdCompleteFlags(rowItems.First().BusinessUnitId);
                Log.DebugFormat("buCompleteFlags = {0}", buCompleteFlags);

                rowItems.AsParallel().ToList().FindAll(completeVins => (
                    completeVins.AdApprovalFirstDate.HasValue && 
                    (!buCompleteFlags.HasFlag(AdCompleteFlags.MinimumPhotos) || completeVins.MinimumPhotoFirstDate.HasValue) &&
                    (!buCompleteFlags.HasFlag(AdCompleteFlags.Description) || completeVins.DescriptionFirstDate.HasValue) &&
                    (!buCompleteFlags.HasFlag(AdCompleteFlags.Price) || completeVins.PriceFirstDate.HasValue))
                ).ForEach(
                    setDate =>
                    {
                        var latestDate = new List<DateTime?>
                        {
                            setDate.PhotosFirstDate,
                            setDate.MinimumPhotoFirstDate,
                            setDate.PriceFirstDate,
                            setDate.DescriptionFirstDate
                        }
                        .Where(dt => dt.HasValue)
                        .OrderByDescending(dt => dt.Value)
                        .First();

                        if (latestDate != null && latestDate.Value != DateTime.MinValue)
                            setDate.CompleteAdFirstDate = latestDate.Value;
                    }
                );
            }
        }

        public void FillAdApprovalFirstDate(IEnumerable<RowItem> updatableVins)
        {
            var rowItems = updatableVins as RowItem[] ?? updatableVins.ToArray();
            if (rowItems.Any())
            {
                Log.DebugFormat("AdApprovalFirstDate");
                var firstAdApprovalDates = GetAdApprovalDates(rowItems.Where(ttmri => ttmri.AdApprovalFirstDate.HasValue.Equals(false)));
                Log.DebugFormat("Found {0}", firstAdApprovalDates.Count);
                
                if (firstAdApprovalDates.Count > 0)
                {
                    foreach (var kvp in firstAdApprovalDates)
                    {
                        rowItems.ToList().Find(ttmri => ttmri.InventoryId.Equals(kvp.Key)).AdApprovalFirstDate =  new DateTime(kvp.Value.Year, kvp.Value.Month, kvp.Value.Day);
                    }
                }
            }
        }

        #endregion TimeToMax dates

        #endregion Fill Methods

        #region FetchReports

        public BusinessUnitReport FetchReport(int businessUnitId, VehicleType vehicleType, DateRange dateRange)
        {
            var report = new BusinessUnitReport {DateRange = dateRange};

            using (var data = GetBusinessUnitReportReader(businessUnitId, vehicleType, dateRange))
            {
                if (data.Rows.Count > 0)
                {
                    report.Vehicles.Clear();
                    foreach(var dr in data.Rows)
                        report.Vehicles.Add(new ReportVehicle(dr as DataRow));
                }
            }
            
            // Get Daily Averages
            // Foreach day in the range find vehicles with photos and count them
            // and find vehicles with complete ads and count those.
            foreach (var day in dateRange.AsDays())
            {
                var tdai = new DailyAverage();

                DateRange startDay = day;
                var photoVehicles = report.Vehicles.Where(veh => veh.DateOfFirstPhoto.Date.Equals(startDay.StartDate.Date)).ToArray();
                var completeVehicles = report.Vehicles.Where(veh => veh.DateOfFirstCompleteAd.Date.Equals(startDay.StartDate.Date)).ToArray();

                if (photoVehicles.Any() || completeVehicles.Any())
                {
                    tdai.Date = day.StartDate;
                    if(photoVehicles.Any())
                        tdai.AvgElapsedDaysPhoto = (int)(Math.Ceiling(photoVehicles.Sum(pv => pv.ElapsedDaysPhoto) / photoVehicles.Count()));
                    if (completeVehicles.Any())
                    {
                        var count = completeVehicles.Count();
                        double su = completeVehicles.Sum(veh => veh.ElapsedDaysCompleteAd);

                        tdai.AvgElapsedDaysAdComplete = (int)(Math.Ceiling(su / count));
                    }
                    report.Days.Add(tdai);
                }
            }

            return report;
        }

        public GroupReport FetchGroupReport(int groupId, VehicleType vehicleType, DateRange dateRange)
        {
            var report = new GroupReport();

            using (var data = GetGroupReportReader(groupId, vehicleType, dateRange))
            {
                if (data.Rows.Count > 0)
                {
                    report = new GroupReport(groupId, dateRange, data);
                }
            }

            return report;
        }

        internal virtual DataTable GetBusinessUnitReportReader(int businessUnitId, VehicleType vehicleType, DateRange dateRange)
        {
            var dt = new DataTable();
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.TimeToMarket#BusinessUnitReportFetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);

                    cmd.AddRequiredParameter("@StartDate", dateRange.StartDate, DbType.DateTime);
                    cmd.AddRequiredParameter("@EndDate", dateRange.EndDate, DbType.DateTime);
                    cmd.AddRequiredParameter("@VehicleType", (int)vehicleType, DbType.Int32);

                    dt.Load(cmd.ExecuteReader());
                }
            }
            return dt;
        }

        internal virtual DataTable GetGroupReportReader(int groupId, VehicleType vehicleType, DateRange dateRange)
        {
            var data = new DataTable();
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.TimeToMarket#GroupReportFetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("@GroupId", groupId, DbType.Int32);
                    cmd.AddRequiredParameter("@StartDate", dateRange.StartDate, DbType.DateTime);
                    cmd.AddRequiredParameter("@EndDate", dateRange.EndDate, DbType.DateTime);
                    cmd.AddRequiredParameter("@VehicleType", (int)vehicleType, DbType.Int32);

                    data.Load(cmd.ExecuteReader());
                }
            }

            return data;
        }

        #endregion Fetch Reports

        #region Misc

        /// <summary>
        /// Load up the results from maxanalytics
        /// </summary>
        /// <param name="where"></param>
        public virtual IEnumerable<IAdStatusResult> GetGidDataForVins(IEnumerable<RowItem> @where)
        {
            // analytics service wrapper.
            var rowItems = @where as RowItem[] ?? @where.ToArray();
            Log.DebugFormat("Call MaxAnalytics AdStatus for {0} VINs", rowItems.Length);

            if (!rowItems.Any())
                return new List<AdStatusResult>();

	        var adStatusArgs = new AdStatusArgs
	        {
		        Store = _businessUnitRepository.GetBusinessUnit(rowItems.First().BusinessUnitId).Code,
		        vins = rowItems.Select(x => x.VIN)
	        };

            _gidResults = _adStatusRepository.GetAdStatusResults(adStatusArgs).ToArray();
            Log.DebugFormat("MaxAnalytics AdStatus results for {0} VINs", _gidResults.Count());
            return _gidResults;
        }

        /// <summary>
        /// Using the dbo.TimeToMarketTableType data table, update the TimeToMarketTracking table with the correct values.
        /// </summary>
        /// <param name="vinsToUpdate"></param>
        public void UpdateVins(IEnumerable<RowItem> vinsToUpdate)
        {
	        var toUpdate = vinsToUpdate as RowItem[] ?? vinsToUpdate.ToArray();
	        // We don't want to store empty records so only update records with data
            var nonEmptyVins = toUpdate.Where( ttmi => 
                ttmi.AdApprovalFirstDate.HasValue ||
                ttmi.CompleteAdFirstDate.HasValue ||
                ttmi.DescriptionFirstDate.HasValue ||
                ttmi.GidFirstDate.HasValue ||
                ttmi.MinimumPhotoFirstDate.HasValue ||
                ttmi.PhotosFirstDate.HasValue ||
                ttmi.PriceFirstDate.HasValue ||
                ttmi.MAXDescriptionFirstDate.HasValue ||
                ttmi.MAXMinimumPhotoFirstDate.HasValue ||
                ttmi.MAXPhotoFirstDate.HasValue ||
                ttmi.MAXPriceFirstDate.HasValue
            ).ToArray();
            
			Log.InfoFormat("Calling upsert with {0} non-empty vehicle rows", nonEmptyVins.Length);
            Log.DebugFormat("InventoryIds: {0}", string.Join(", ", nonEmptyVins.Select(x => x.InventoryId)));

            if (nonEmptyVins.Any())
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[Database.MerchandisingDatabase].ConnectionString))
                {
                    if (conn.State != ConnectionState.Open && conn.State != ConnectionState.Connecting)
                        conn.Open();
                    using (var cmd = new SqlCommand("dashboard.TimeToMarket#UpsertData", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
	                    var dt = GetUpdateDataTable(nonEmptyVins);
	                    // I Nedd log4net to support Trace level Log.DebugFormat("DataTable: {0}", dt.ToJson());
                        var tableParam = cmd.Parameters.AddWithValue("@VinList", dt);
                        tableParam.SqlDbType = SqlDbType.Structured;
                        tableParam.TypeName = "dbo.TimeToMarketTableType";
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Get a list of BusinessUnitIds by upgrade number
        /// </summary>
        /// <param name="upgrade"></param>
        /// <returns></returns>
        public int[] GetMaxBusinessUnits(Upgrade upgrade)
        {
            var businessUnits = new List<int>();

            using (var con = Database.GetConnection(Database.IMTDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "[dbo].[DealersWithUpgrade#Fetch]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("@UpgradeId", upgrade, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var buId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            if (!businessUnits.Contains(buId))
                                businessUnits.Add(buId);
                        }
                    }
                }
            }
            return businessUnits.ToArray();
        }

        #endregion Misc

        #endregion Implement Interface

        #region private methods

        private List<RowItem> FilterDoNotPostInventory(List<RowItem> vins)
        {
            var filteredItems = new List<RowItem>();
            if (vins.Count > 0)
            {
                const WorkflowType wft = WorkflowType.Offline;
                var bin = Bin.Create(wft);

                var buid = vins.First().BusinessUnitId;

                // A list of the Inventory for this businessUnit that does not have a price.
                var id = new List<InventoryData>();
				id.AddRange(GetInventoryData(buid).Select(data => data as InventoryData));
                var offlineVehicles = InventoryDataFilter.WorkflowSearch(buid, id, 0, 10000, String.Empty, bin);

                filteredItems = vins.Select(v => v).Except(offlineVehicles.Select(npl => new RowItem
                {
                    BusinessUnitId = npl.BusinessUnitID,
                    VIN = npl.VIN,
                    InventoryId = npl.InventoryID,
                    StockNumber = npl.StockNumber
                })).ToList();
            }
            return filteredItems;
        }

        internal virtual AdCompleteFlags GetAdCompleteFlags(int businessUnitId)
        {
            var reportSettings = new GetReportSettings(businessUnitId);
            AbstractCommand.DoRun(reportSettings);
            return (AdCompleteFlags)reportSettings.AdCompleteFlags;
        }

        private DateTime? GetBatchTimeStamp()
        {
            if (_batchTimeStamp.HasValue.Equals(false))
                _batchTimeStamp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            return _batchTimeStamp;
        }

        internal virtual List<IInventoryData> GetInventoryData(int businessUnitId)
        {
            if (_inventoryData.Count == 0)
            {
                _inventoryData.AddRange(_inventoryDataRepository.GetAllInventory(businessUnitId));
				Log.DebugFormat("Found {0} vehicles to update. InventoryIDs: {1}", _inventoryData.Count, string.Join(", ", _inventoryData.Select(x => x.InventoryID)));
            }
            return _inventoryData;
        }

        internal virtual int GetMinimumPhotoCount(int businessUnitId)
        {
            var desiredPhotoCount = 12;
            try
            {
                desiredPhotoCount = MerchandisingPreferences.Fetch(businessUnitId).DesiredPhotoCount;
            }
            catch (ApplicationException aex)
            {
                // They have no preferences set.
                if (aex.Message != "Preferences Item not found.")
                    throw;
            }

            return desiredPhotoCount;
        }

        internal virtual List<string> GetVinsWithNewExternalPhotos(int businessUnitId, List<string> vinsToCheck, int minimumCount, DateTime reportStartDate)
        {
            var vins = new List<string>();
            try
            {
                using (var con = Database.GetConnection(Database.MerchandisingDatabase))
                {
                    con.Open();

                    using (var cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "dashboard.TimeToMarket#FetchAULtecInventoryImageUrls";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);
                        cmd.AddRequiredParameter("@StartDate", reportStartDate, DbType.DateTime);

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var vin = reader.GetString(reader.GetOrdinal("VIN"));
                                var urls = (reader.IsDBNull(reader.GetOrdinal("ImageURLs"))) ? "" : reader.GetString(reader.GetOrdinal("ImageURLs"));

                                if (urls.Split(',').Count() >= minimumCount)
                                    if (!vins.Contains(vin))
                                        vins.Add(vin);
                            }
                        }
                    }
                }
            }
            catch (SqlException sqx)
            {
                if (sqx.Number == -2)
                    Log.ErrorFormat("SQL Timeout in GetVinsWithNewExternalPhotos: BusinessUnitId: {0}, StartDate: {1}", businessUnitId, reportStartDate);
                else throw;
            }

            Log.DebugFormat("Get photos from DataFeeds for vehicles with at least {0} photo(s) found {1} vehicles", minimumCount, vins.Count);
            vins.RemoveAll(vr => vinsToCheck.Contains(vr).Equals(false));
            Log.DebugFormat("After removing extraneous items: {0}", vins.Count);

            return vins;
        }

        private IEnumerable<string> GetVinsWithNewInternalPhotos(int businessUnitId, IEnumerable<string> vinsToCheck, int minimumphotos = 1)
        {
            var internalPhotos = new List<string>();
            var vehicles = GetInternalPhotos(businessUnitId);

            // Loop through the list of vinsToCheck instead of the vehicles found because it should be much smaller.
            foreach (var vin in vinsToCheck)
            {
                IVehiclePhotos photos;
                vehicles.PhotosByVin.TryGetValue(vin, out photos);
                if (
                    photos != null
                    && photos.PhotoUrls.Count() >= minimumphotos
                    && !internalPhotos.Contains(vin)
                )
                {
                    internalPhotos.Add(vin);
                }
            }
            return internalPhotos;
        }

        private IVehiclesPhotosResult GetInternalPhotos(int businessUnitId)
        {
            return _photoResults ??
                   (_photoResults =
                       PhotoServices.Value.GetPhotoUrlsByBusinessUnit(businessUnitId, TimeSpan.FromSeconds(30.0)));
        }

        private IEnumerable<string> FillByPhotoCount(int businessUnitId, IEnumerable<string> vinsNeedPhotos, int minimumPhotoCount, DateTime reportStartDate)
        {
            var vinsWithPhotos = new List<string>();
            var needPhotos = vinsNeedPhotos as IList<string> ?? vinsNeedPhotos.ToList();
            if (needPhotos.Any())
            {
                // A list of vins with photos from our external feed.
                IEnumerable<string> vinsWithExternalPhotos = GetVinsWithNewExternalPhotos(businessUnitId, needPhotos.ToList(), minimumPhotoCount, reportStartDate);

                // The remaining vins we need to check from our internal publisher
                var internalVinsToCheck = needPhotos.Except(vinsWithExternalPhotos).ToArray();

                Log.DebugFormat("Need to check AULTec webservice for {0} vehicles", internalVinsToCheck.Count());
                // The list of vins with photos in the internal feed.
                IEnumerable<string> vinsWithNewInternalPhotos = GetVinsWithNewInternalPhotos(businessUnitId, internalVinsToCheck, minimumPhotoCount);

                vinsWithPhotos = vinsWithExternalPhotos.Concat(vinsWithNewInternalPhotos).ToList();
            }
            Log.DebugFormat("After checking both sources vinsWithPhotos is {0}", vinsWithPhotos.Count());
            return vinsWithPhotos;
        }
        
        /// <summary>
        /// Create a datatable to pass a list to a stored procedure. WooHoo SQLServer 2k8!
        /// http://stackoverflow.com/questions/5595353/how-to-pass-table-value-parameters-to-stored-procedure-from-net-code
        /// </summary>
        /// <param name="vinsToUpdate"></param>
        /// <returns></returns>
        private DataTable GetUpdateDataTable(IEnumerable<RowItem> vinsToUpdate)
        {
            var table = new DataTable();
            table.Columns.AddRange(RowItemHelpers.GetDataColumns());
            vinsToUpdate.ToList().ForEach(itemRow => RowItemHelpers.LoadDataRow(itemRow, table));
            
            return table;
        }

        /// <summary>
        /// Create a datatable to pass a list to a stored procedure. WooHoo SQLServer 2k8!
        /// http://stackoverflow.com/questions/5595353/how-to-pass-table-value-parameters-to-stored-procedure-from-net-code
        /// </summary>
        /// <param name="vins"></param>
        /// <returns></returns>
        private static DataTable BuildStringDataTable(IEnumerable<int> vins)
        {
            var table = new DataTable();
            table.Columns.Add("VIN", typeof(string));
            foreach (var item in vins)
            {
                table.Rows.Add(item.ToString(CultureInfo.InvariantCulture));
            }
            return table;
        }


        private static Dictionary<int, DateTime> GetAdApprovalDates(IEnumerable<RowItem> vehiclesMissingDates)
        {
            var vins = new Dictionary<int, DateTime>();

            var missingDates = vehiclesMissingDates as RowItem[] ?? vehiclesMissingDates.ToArray();
            if (missingDates.Any())
            {
                var businessUnitId = missingDates.First().BusinessUnitId;
                
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[Database.MerchandisingDatabase].ConnectionString))
                {
                    if (conn.State != ConnectionState.Open && conn.State != ConnectionState.Connecting)
                        conn.Open();
                    using (var cmd = new SqlCommand("dashboard.TimeToMarket#FirstAdPostedFetch", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@BusinessUnitId", businessUnitId);
                        var tableParam = cmd.Parameters.AddWithValue("@inventoryIds", BuildStringDataTable(missingDates.Select(vmd => vmd.InventoryId)));
                        tableParam.SqlDbType = SqlDbType.Structured;
                        tableParam.TypeName = "dbo.StringValTableType";

                        IDataReader reader = cmd.ExecuteReader();
                        while(reader.Read())
                        {
                            if(!reader.IsDBNull(reader.GetOrdinal("ReleaseTime")))
                            vins.Add(
                                reader.GetInt32(reader.GetOrdinal("InventoryId")), 
                                reader.GetDateTime(reader.GetOrdinal("ReleaseTime"))
                            );
                        }
                    }
                }
            }

            return vins;
        }

        internal IEnumerable<IMaxAnalyticsAd> GetGidDataForVehicle(string vin)
        {
            var invData = GetInventoryData(_businessUnitId).Find(x => String.Equals(x.VIN, vin, StringComparison.InvariantCultureIgnoreCase));
            if (invData == null)
                return new MaxAnalyticsAd[] {};

	        GidProvider gidProvider;
			_gidProviders.TryGetValue(invData.InventoryType, out gidProvider);

            if (gidProvider <= GidProvider.None)
                return null;

            // Take all the AdStatusResults for the correct provider which also match the vin
            var providerAds = _gidResults
                .Where(providerResult =>
                    providerResult.Provider == gidProvider &&
                    ((AdStatusResult)providerResult).Ads.Any(ad => String.Equals(ad.VIN, vin, StringComparison.InvariantCultureIgnoreCase)));

            // select many to project and flatten the results into one collection
            var vinAds = providerAds.SelectMany(pa => ((AdStatusResult)pa).Ads.Where(ad => String.Equals(ad.VIN, vin, StringComparison.InvariantCultureIgnoreCase)));
            
            return vinAds;
        }


        private void UpdateGidData(
            IEnumerable<RowItem> allVehiclesThatCouldBeUpdated,
            Predicate<RowItem> rowCanBeUpdated,
            Predicate<IMaxAnalyticsAd> matchAdStatus,
            Action<RowItem, IMaxAnalyticsAd> updateRowItem
        )
        {
            var eligibleVehicles = allVehiclesThatCouldBeUpdated.Where(i => rowCanBeUpdated(i));
            
            foreach (var vehicleThatShouldBeUpdated in eligibleVehicles)
            {
                var vehicleAdResults = GetGidDataForVehicle((vehicleThatShouldBeUpdated.VIN));
                if (vehicleAdResults == null)
                    continue;

                var maxAnalyticsAds = vehicleAdResults as MaxAnalyticsAd[] ?? vehicleAdResults.ToArray();
                if (maxAnalyticsAds.Any(ad => matchAdStatus(ad)))
                {
                    updateRowItem(vehicleThatShouldBeUpdated, 
                        maxAnalyticsAds
                            .Where(ad => matchAdStatus(ad))
                            .OrderBy(ad =>((MaxAnalyticsAd)ad).StatusDate) // Order the ads by date so we get the *first* time
                            .First()
                        );
                }
            }


            //foreach(var vehicleThatShouldBeUpdated in 
            //    from timeToMarketRow in 
            //        allVehiclesThatCouldBeUpdated.Where(i => rowCanBeUpdated(i)) 
            //    let result = GetGidDataForVehicle(timeToMarketRow.VIN) 
            //    where result != null && result.Any( ad => matchAdStatus(ad))
            //    select timeToMarketRow)
            //{
            //    // We matched an ad, call the update function
            //    updateRowItem(vehicleThatShouldBeUpdated);
            //    Log.DebugFormat("Updated GID result: {0}", vehicleThatShouldBeUpdated.VIN);
            //}
        }

        #endregion private methods


        
    }
}
