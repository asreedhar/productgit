﻿using System;
namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket
{
    public interface IReportStartDates
    {
        DateTime CompleteStartDate { get; }
        DateTime FirstPhotoStartDate { get; }
        DateTime CarsApiStartDate { get; }
        void Initialize(int businessUnitId, DateTime defaultTime);
    }
}
