﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Enumerations;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items;
using MAX.Entities;
using MAX.Entities.Reports.TimeToMarket;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket
{
    public interface ITimeToMarketRepository
    {
        IEnumerable<RowItem> GetVinData(int businessUnitId, DateTime reportStartDate);
        void UpdateVins(IEnumerable<RowItem> vinsToUpdate);
        int[] GetMaxBusinessUnits(FirstLook.DomainModel.Oltp.Upgrade upgrade);

        void FillCompleteAdFirstDate(IEnumerable<RowItem> updatableVins);
        void FillAdApprovalFirstDate(IEnumerable<RowItem> updatableVins);

        void FillPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillPriceFirstDate(IEnumerable<RowItem> updatableVins);
        void FillDescriptionFirstDate(IEnumerable<RowItem> updatableVins);
        void FillGidFirstDate(IEnumerable<RowItem> updatableVins);

        void FillMAXPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillMAXMinimumPhotosFirstDate(IEnumerable<RowItem> updatableVins, DateTime reportStartDate);
        void FillMAXPriceFirstDate(IEnumerable<RowItem> updatableVins);
        void FillMAXDescriptionFirstDate(IEnumerable<RowItem> updatableVins);

        void Initialize(int businessUnitId, GidProviders gidProviders);

        BusinessUnitReport FetchReport(int businessUnitId, VehicleType vehicleType, DateRange dateRange);
        GroupReport FetchGroupReport(int groupId, VehicleType vehicleType, DateRange dateRange);
        IEnumerable<IAdStatusResult> GetGidDataForVins(IEnumerable<RowItem> @where);
    }
}
