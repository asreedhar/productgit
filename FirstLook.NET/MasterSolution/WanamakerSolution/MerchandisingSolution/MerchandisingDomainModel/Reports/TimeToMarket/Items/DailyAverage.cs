﻿using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items
{
    [Serializable]
    public class DailyAverage
    {
        public DateTime Date { get; set; }

        public int AvgElapsedDaysPhoto { get; set; }
        public int AvgElapsedDaysAdComplete { get; set; }

        internal DailyAverage() { }

        internal DailyAverage(IDataReader reader)
        {
            if (!reader.IsClosed)
            {
                Date = (reader.IsDBNull(reader.GetOrdinal("Date"))) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("Date"));

                var avgDaysPhoto = (reader.IsDBNull(reader.GetOrdinal("PhotosAverageDays"))) ? 0 : reader.GetInt32(reader.GetOrdinal("PhotosAverageDays"));
                AvgElapsedDaysPhoto = (avgDaysPhoto < 0) ? 0 : avgDaysPhoto;
                
                var avgDaysComplete = (reader.IsDBNull(reader.GetOrdinal("CompleteAdAverageDays"))) ? 0 : reader.GetInt32(reader.GetOrdinal("CompleteAdAverageDays"));
                AvgElapsedDaysAdComplete = (avgDaysComplete < 0) ? 0 : avgDaysComplete;
            }
        }
    }
}