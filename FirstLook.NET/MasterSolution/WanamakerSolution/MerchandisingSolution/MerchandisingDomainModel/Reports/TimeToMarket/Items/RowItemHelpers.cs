﻿using System;
using System.Collections.Generic;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items
{
    public static class RowItemHelpers
    {
        public static DataColumn[] GetDataColumns()
        {
            return new List<DataColumn>() {
                new DataColumn( "BusinessUnitId", typeof(int) ),
                new DataColumn( "InventoryId", typeof(int) ),
                new DataColumn( "VIN", typeof(string) ),
                new DataColumn( "GIDFirstDate", typeof(DateTime) ),
                new DataColumn( "PhotosFirstDate", typeof(DateTime) ),
                new DataColumn( "DescriptionFirstDate", typeof(DateTime) ),
                new DataColumn( "PriceFirstDate", typeof(DateTime) ),
                new DataColumn( "AdApprovalFirstDate", typeof(DateTime) ),
                new DataColumn( "CompleteAdFirstDate", typeof(DateTime) ),
                new DataColumn( "MinimumPhotosFirstDate", typeof(DateTime) ),
                new DataColumn( "MAXMinimumPhotosFirstDate", typeof(DateTime) ),
                new DataColumn( "MAXPhotosFirstDate", typeof(DateTime) ),
                new DataColumn( "MAXPriceFirstDate", typeof(DateTime) ),
                new DataColumn( "MAXDescriptionFirstDate", typeof(DateTime) ),

            }.ToArray();
        }

        public static void LoadDataRow(RowItem itemRow, DataTable table)
        {
            table.LoadDataRow(
                new object[] {
                    itemRow.BusinessUnitId,
                    itemRow.InventoryId,
                    itemRow.VIN,
                    itemRow.GidFirstDate,
                    itemRow.PhotosFirstDate,
                    itemRow.DescriptionFirstDate,
                    itemRow.PriceFirstDate,
                    itemRow.AdApprovalFirstDate,
                    itemRow.CompleteAdFirstDate,
                    itemRow.MinimumPhotoFirstDate,
                    itemRow.MAXMinimumPhotoFirstDate,
                    itemRow.MAXPhotoFirstDate,
                    itemRow.MAXPriceFirstDate,
                    itemRow.MAXDescriptionFirstDate
                },
                true
            );
        }

    }
}