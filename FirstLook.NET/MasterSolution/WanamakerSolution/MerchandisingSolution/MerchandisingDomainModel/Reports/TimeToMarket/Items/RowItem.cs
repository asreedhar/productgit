﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Data;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items
{
    public class RowItem : IEquatable<RowItem>, IEqualityComparer<RowItem>
    {

        #region Private fields
        private DateTime? _gidFirstDate;
        private DateTime? _photosFirstDate;
        private DateTime? _descriptionFirstDate;
        private DateTime? _priceFirstDate;
        private DateTime? _adApprovalFirstDate;
        private DateTime? _completeAdFirstDate;
        private DateTime? _minimumPhotoFirstDate;
        private DateTime? _maxMinimumPhotoFirstDate;
        private DateTime? _maxPhotoFirstDate;
        private DateTime? _maxPriceFirstDate;
        private DateTime? _maxDescriptionFirstDate;
        #endregion Private fields

        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string VIN { get; set; }
        public string StockNumber { get; set; }
		public VehicleType InventoryType { get; set; }
		public bool InventoryActive { get; set; }

        public DateTime InventoryReceivedDate { get; set; }
		public DateTime? DeleteDate { get; set; }
        public DateTime? Inserted { get; private set; }
        public DateTime? Updated { get; private set; }

		#region Fixed Dates

		public DateTime? AdApprovalFirstDate
		{
			get
			{
				return _adApprovalFirstDate;
			}
			set
			{
				if (value != null && _adApprovalFirstDate.HasValue.Equals(false))
					_adApprovalFirstDate = value;
			}
		}

		public DateTime? GidFirstDate {
            get
            {
                return _gidFirstDate;
            }
            set
            {
                if (value != null && _gidFirstDate.HasValue.Equals(false))
                    _gidFirstDate = value;
            }
        }

		#endregion Fixed Dates


		#region TimeToMarket data set by AdStatus calls

		public DateTime? PhotosFirstDate { get; set; }
        public DateTime? DescriptionFirstDate { get; set; }
        public DateTime? PriceFirstDate { get; set; }
		public DateTime? CompleteAdFirstDate { get; set; }
		public DateTime? MinimumPhotoFirstDate { get; set; }
		
		#endregion TimeToMarket data set by AdStatus calls


		#region TimeToMax dates
		
		public DateTime? MAXMinimumPhotoFirstDate
        {
            get
            {
                return _maxMinimumPhotoFirstDate;
            }
            set
            {
                if (value != null && _maxMinimumPhotoFirstDate.HasValue.Equals(false))
                    _maxMinimumPhotoFirstDate = value;
            }
        }
        public DateTime? MAXPhotoFirstDate
        {
            get
            {
                return _maxPhotoFirstDate;
            }
            set
            {
                if (value != null && _maxPhotoFirstDate.HasValue.Equals(false))
                    _maxPhotoFirstDate = value;
            }
        }
        public DateTime? MAXPriceFirstDate
        {
            get
            {
                return _maxPriceFirstDate;
            }
            set
            {
                if (value != null && _maxPriceFirstDate.HasValue.Equals(false))
                    _maxPriceFirstDate = value;
            }
        }
        public DateTime? MAXDescriptionFirstDate
        {
            get
            {
                return _maxDescriptionFirstDate;
            }
            set
            {
                if (value != null && _maxDescriptionFirstDate.HasValue.Equals(false))
                    _maxDescriptionFirstDate = value;
            }
        }

		#endregion TimeToMax dates


		public RowItem() { }

        public RowItem(IDataRecord reader)
        {
            BusinessUnitId           = (reader.IsDBNull(reader.GetOrdinal("BusinessUnitId"))) ? 0 : reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));
            InventoryId              = (reader.IsDBNull(reader.GetOrdinal("InventoryId"))) ? 0 : reader.GetInt32(reader.GetOrdinal("InventoryId"));
            StockNumber              = (reader.IsDBNull(reader.GetOrdinal("StockNumber"))) ? String.Empty : reader.GetString(reader.GetOrdinal("StockNumber"));
            VIN                      = (reader.IsDBNull(reader.GetOrdinal("VIN"))) ? String.Empty : reader.GetString(reader.GetOrdinal("VIN"));
            InventoryReceivedDate    = (reader.IsDBNull(reader.GetOrdinal("InventoryReceivedDate")) ? DateTime.MaxValue : reader.GetDateTime(reader.GetOrdinal("InventoryReceivedDate")));
            Inserted                 = (reader.IsDBNull(reader.GetOrdinal("Inserted"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("Inserted"));
            Updated                  = (reader.IsDBNull(reader.GetOrdinal("Updated"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("Updated"));
            GidFirstDate             = (reader.IsDBNull(reader.GetOrdinal("GIDFirstDate"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("GIDFirstDate"));
            PhotosFirstDate          = (reader.IsDBNull(reader.GetOrdinal("PhotosFirstDate"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("PhotosFirstDate"));
            DescriptionFirstDate     = (reader.IsDBNull(reader.GetOrdinal("DescriptionFirstDate"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("DescriptionFirstDate"));
            PriceFirstDate           = (reader.IsDBNull(reader.GetOrdinal("PriceFirstDate"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("PriceFirstDate"));
            AdApprovalFirstDate      = (reader.IsDBNull(reader.GetOrdinal("AdApprovalFirstDate"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("AdApprovalFirstDate"));
            CompleteAdFirstDate      = (reader.IsDBNull(reader.GetOrdinal("CompleteAdFirstDate"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("CompleteAdFirstDate"));
            MinimumPhotoFirstDate    = (reader.IsDBNull(reader.GetOrdinal("MinimumPhotosFirstDate"))) ? null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("MinimumPhotosFirstDate"));
            MAXMinimumPhotoFirstDate = (reader.IsDBNull(reader.GetOrdinal("MAXMinimumPhotosFirstDate"))) ? null : (DateTime?) reader.GetDateTime("MAXMinimumPhotosFirstDate");
            MAXPhotoFirstDate        = (reader.IsDBNull(reader.GetOrdinal("MAXPhotosFirstDate"))) ? null : (DateTime?) reader.GetDateTime("MAXPhotosFirstDate");
            MAXPriceFirstDate        = (reader.IsDBNull(reader.GetOrdinal("MAXPriceFirstDate"))) ? null : (DateTime?)reader.GetDateTime("MAXPriceFirstDate");
            MAXDescriptionFirstDate  = (reader.IsDBNull(reader.GetOrdinal("MAXDescriptionFirstDate"))) ? null : (DateTime?)reader.GetDateTime("MAXDescriptionFirstDate");

	        if (reader.HasColumn("InventoryType"))
	        {
		        InventoryType = reader.GetInt32(reader.GetOrdinal("InventoryType"));
	        }
	        if (reader.HasColumn("InventoryActive"))
	        {
		        InventoryActive = reader.GetBoolean(reader.GetOrdinal("InventoryActive"));
	        }
	        if (reader.HasColumn("DeleteDt"))
	        {
		        DeleteDate = (reader.IsDBNull(reader.GetOrdinal("DeleteDt"))) ? null : (DateTime?) reader.GetDateTime("DeleteDt");
	        }
        }

        public bool Equals(RowItem other)
        {
            return (this.BusinessUnitId == other.BusinessUnitId &&
                this.InventoryId == other.InventoryId &&
                this.VIN == other.VIN &&
                this.StockNumber == other.StockNumber);
        }

        public bool Equals(RowItem x, RowItem y)
        {
            return (
                x.BusinessUnitId == y.BusinessUnitId &&
                x.InventoryId == y.InventoryId &&
                x.VIN == y.VIN &&
                x.StockNumber == y.StockNumber);
        }

        public int GetHashCode(RowItem obj)
        {
            return obj.GetHashCode();   
        }

        // Required for Linq.Except
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + BusinessUnitId.GetHashCode();
                hash = hash * 23 + InventoryId.GetHashCode();
                hash = hash * 23 + VIN.GetHashCode();
                hash = hash * 23 + StockNumber.GetHashCode();
                return hash;
            }
        }

        internal RowItem Clone()
        {
            return new RowItem()
            {
                BusinessUnitId           = this.BusinessUnitId,
                InventoryId              = this.InventoryId,
                StockNumber              = this.StockNumber,
                VIN                      = this.VIN,
                InventoryReceivedDate    = this.InventoryReceivedDate,
                Inserted                 = this.Inserted,
                Updated                  = this.Updated,
                GidFirstDate             = this.GidFirstDate,
                PhotosFirstDate          = this.PhotosFirstDate,
                DescriptionFirstDate     = this.DescriptionFirstDate,
                PriceFirstDate           = this.PriceFirstDate,
                AdApprovalFirstDate      = this.AdApprovalFirstDate,
                CompleteAdFirstDate      = this.CompleteAdFirstDate,
                MinimumPhotoFirstDate    = this.MinimumPhotoFirstDate,
                MAXMinimumPhotoFirstDate = this.MAXMinimumPhotoFirstDate,
                MAXPhotoFirstDate        = this.MAXPhotoFirstDate,
                MAXPriceFirstDate        = this.MAXPriceFirstDate,
                MAXDescriptionFirstDate  = this.MAXDescriptionFirstDate,
				InventoryActive			 = this.InventoryActive,
				InventoryType			 = this.InventoryType,
				DeleteDate				 = this.DeleteDate
            };
        }
    }
}
