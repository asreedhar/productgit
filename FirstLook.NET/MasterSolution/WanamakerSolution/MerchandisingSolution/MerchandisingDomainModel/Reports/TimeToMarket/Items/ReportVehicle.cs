﻿using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket.Items
{
    [Serializable]
    public class ReportVehicle : IBusinessUnitAggregation
    {        
        public int BusinessUnitId { get; set; }
        public string BusinessUnit { get; set; }
        public int VehicleId { get; set; }
        public int InventoryId { get; set; }
        public string VIN;
        public string StockNumber;
        public string YearMakeModelTrim;
        public DateTime DateAddedToInventory;
        public DateTime DateOfFirstPhoto;
        public DateTime DateOfFirstCompleteAd;

        public byte InventoryActive { get; set; }
        
        // Calculate the difference in days, not 24 hour periods.
        public double ElapsedDaysPhoto 
        { 
            get 
            {
                var totDays = DateOfFirstPhoto.Date.Subtract(DateAddedToInventory.Date).TotalDays;

                if (DateOfFirstPhoto.Equals(DateTime.MinValue))
                    return totDays;
                
                return (totDays < 0) ? 0 : totDays;
            } 
        }

        public double ElapsedDaysCompleteAd
        {
            get
            {
                var totDays = DateOfFirstCompleteAd.Date.Subtract(DateAddedToInventory.Date).TotalDays;

                if (DateOfFirstCompleteAd.Equals(DateTime.MinValue))
                    return totDays;
                
                return (totDays < 0) ? 0 : totDays;
            }
        }
        
        public ReportVehicle() { }

        public ReportVehicle(DataRow dataRow)
        {
            // TODO: Complete member initialization
            BusinessUnitId = (dataRow["BusinessUnitID"] == DBNull.Value) ? 0 : Convert.ToInt32(dataRow["BusinessUnitID"]);
            if (dataRow.Table.Columns.Contains("BusinessUnit"))
                BusinessUnit = (dataRow["BusinessUnit"] == DBNull.Value) ? String.Empty : Convert.ToString(dataRow["BusinessUnit"]);
            InventoryId = (dataRow["InventoryID"] == DBNull.Value) ? 0 : Convert.ToInt32(dataRow["InventoryID"]);

            DateAddedToInventory = (dataRow["DateAddedToInventory"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dataRow["DateAddedToInventory"]);
            DateOfFirstPhoto = (dataRow["PhotosFirstDate"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dataRow["PhotosFirstDate"]);
            DateOfFirstCompleteAd = (dataRow["CompleteAdFirstDate"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(dataRow["CompleteAdFirstDate"]);

            InventoryActive = (dataRow["inventoryActive"] == DBNull.Value) ? byte.MinValue : Convert.ToByte(dataRow["inventoryActive"]); // BUGZID: 26724 

            // Group report does not contain the columns below
            if (dataRow.Table.Columns.Contains("VehicleID"))
                VehicleId = (dataRow["VehicleID"] == DBNull.Value) ? 0 : Convert.ToInt32(dataRow["VehicleID"]);
            if (dataRow.Table.Columns.Contains("VIN"))
                VIN = (dataRow["VIN"] == DBNull.Value) ? String.Empty : Convert.ToString(dataRow["VIN"]);
            if (dataRow.Table.Columns.Contains("StockNumber"))
                StockNumber = (dataRow["StockNumber"] == DBNull.Value) ? String.Empty : Convert.ToString(dataRow["StockNumber"]);

            if (dataRow.Table.Columns.Contains("Year"))
            {
                var year = (dataRow["Year"] == DBNull.Value) ? String.Empty : Convert.ToString(dataRow["Year"]);
                var make = (dataRow["Make"] == DBNull.Value) ? String.Empty : Convert.ToString(dataRow["Make"]);
                var model = (dataRow["Model"] == DBNull.Value) ? String.Empty : Convert.ToString(dataRow["Model"]);
                var trim = (dataRow["Trim"] == DBNull.Value) ? String.Empty : Convert.ToString(dataRow["Trim"]);
                YearMakeModelTrim = String.Format("{0} {1} {2} {3}", year, make, model, trim);
            }
        }
    }
}