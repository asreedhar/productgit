﻿using System;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Properties;
using System.Data;
using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.Reports.TimeToMarket
{
    internal class ReportStartDates : IReportStartDates
    {
        public DateTime FirstPhotoStartDate { get; private set; }
        public DateTime CompleteStartDate { get; private set; }
        public DateTime CarsApiStartDate { get; private set; }

        public void Initialize(int businessUnitId, DateTime defaultTime)
        {
            SetReportStartDate(businessUnitId, defaultTime);
            GetReportStartDates(businessUnitId);
        }

        /// <summary>
        /// Set missing dates in the report start table.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="defaultTime"></param>
        private void SetReportStartDate(int businessUnitId, DateTime defaultTime)
        {
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.TimeToMarket#SetReportStartDate";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter("@ReportStartDate", defaultTime, DbType.DateTime);
                    cmd.AddRequiredParameter("@HasCarsApi", HasCarsApi(businessUnitId), DbType.Boolean);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Loads the dates from the database.
        /// </summary>
        /// <param name="businessUnitId"></param>
        private void GetReportStartDates(int businessUnitId)
        {
            CompleteStartDate = Settings.Default.TimeToMarketLaunchDate;
            FirstPhotoStartDate = Settings.Default.TimeToMarketLaunchDate;
            CarsApiStartDate = Settings.Default.CarsApiLaunchDate;

            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "dashboard.TimeToMarket#GetReportStartDate";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        if(reader.IsDBNull(reader.GetOrdinal("ReportStartDate")).Equals(false))
                            CompleteStartDate = reader.GetDateTime(reader.GetOrdinal("ReportStartDate"));
                        if (reader.IsDBNull(reader.GetOrdinal("FirstPhotoReportStartDate")).Equals(false))
                            FirstPhotoStartDate = reader.GetDateTime(reader.GetOrdinal("FirstPhotoReportStartDate"));
                        if (reader.IsDBNull(reader.GetOrdinal("CarsApiStartDate")).Equals(false))
                            CarsApiStartDate = reader.GetDateTime(reader.GetOrdinal("CarsApiStartDate"));
                    }
                }
            }
        }

        public bool HasCarsApi(int businessUnitId)
        {
            var settings = GetMiscSettings.GetSettings(businessUnitId);
            return settings.HasSettings &&
                   (settings.ReportDataSourceNew == ReportDataSource.CarsApi ||
                    settings.ReportDataSourceUsed == ReportDataSource.CarsApi);
        }
    }
}
