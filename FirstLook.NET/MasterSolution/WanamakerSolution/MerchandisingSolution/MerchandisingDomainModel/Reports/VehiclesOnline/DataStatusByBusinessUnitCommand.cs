using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline
{
    public class DataStatusByBusinessUnitCommand : AbstractCommand
    {
        public int BusinessUnitId { get; private set; }
        public IEnumerable<DataSourceStatus> DataSourceStatusList { get; private set; }

        public DataStatusByBusinessUnitCommand(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }

        protected override void Run()
        {
            var items = new List<DataSourceStatus>();

            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandText = "merchandising.VehiclesOnline#DataStatusByBusinessUnit";
                cmd.CommandType = CommandType.StoredProcedure;

                Database.AddRequiredParameter(cmd, "@businessUnitId", BusinessUnitId, DbType.Int32);

                using(var reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        var status = new DataSourceStatus
                                         {
                                             BusinessUnitId = reader.GetInt32(0),
                                             Destination = reader.IsDBNull(1) 
                                                 ? EdtDestinations.Undefined 
                                                 : (EdtDestinations)reader.GetInt32(1),
                                             LastLoadedTime = reader.IsDBNull(2) 
                                                 ? (DateTime?) null 
                                                 : DateTime.SpecifyKind(reader.GetDateTime(2), DateTimeKind.Local),
                                             HasCredentials = reader.GetBoolean(3),
                                             HasInvalidCredentials = reader.GetBoolean(4)
                                         };
                        items.Add(status);
                    }
                }
            }

            DataSourceStatusList = items.AsReadOnly();
        }

        public static IEnumerable<DataSourceStatus> Execute(int businessUnitId)
        {
            var cmd = new DataStatusByBusinessUnitCommand(businessUnitId);
            DoRun(cmd);
            return cmd.DataSourceStatusList;
        }
    }
}