﻿using System.Data.Services.Client;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline.Client
{
    public partial class Context
    {
        public IQueryable<VehiclesOnline> GetVehiclesOnline(string memberName)
        {
            return CreateServiceOperationQuery<VehiclesOnline>(memberName, "GetVehiclesOnline");
        }

        private IQueryable<T> CreateServiceOperationQuery<T>(string memberName, string entitySetName)
        {
            if (memberName == null)
                throw new DataServiceClientException("memberName is a required parameter");

            return CreateQuery<T>(entitySetName).AddQueryOption("memberName", "'" + memberName + "'");
        }
    }
}