﻿using System;
using System.Globalization;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline.Client
{
    public partial class VehiclesOnline : IVehiclesOnline
    {
        public bool Online { get; set; }
        public bool HasData { get; set; }

        public decimal? InternetPrice { get; set; }
       
    }
}