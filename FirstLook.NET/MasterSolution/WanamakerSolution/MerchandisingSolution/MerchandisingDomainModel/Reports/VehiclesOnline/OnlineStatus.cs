﻿namespace FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline
{
    public enum OnlineStatus
    {
        All,
        Online,
        NotOnline
    }
}