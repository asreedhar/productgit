﻿using System;
using System.Globalization;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline
{
    public class VehicleOnlineBinder : IVehiclesOnline
    {
        private readonly IVehiclesOnline _online;

        public VehicleOnlineBinder(IVehiclesOnline online)
        {
            _online = online;
        }

        public string[] GetPhrases()
        {
            return new[]
                       {
                           StatusDescription,
                           Make,
                           Model,
                           Trim,
                           Vin,
                           StockNumber,
                           Approved ? "Approved" : "",
                           VehicleYear.ToString(CultureInfo.InvariantCulture)
                       };
        }

        #region Vehicle info

        #region Url Properties
        public string EStockCardBaseUrl { get; set; }
        public string ExternalImageBaseUrl { get; set; }
        public string MerchandisingBaseUrl { get; set; }
        #endregion Url Properties

        public int AgeInDays
        {
            get
            {
                var ts = DateTime.Today.Subtract(InventoryReceivedDate);
                return Math.Max(0, ts.Days);
            }
        }

        public string ApprovedDescription
        {
            get { return Approved ? "Yes" : "No"; }
        }

        public string ApprovalSummaryUrl
        {
            get { return string.Format("{0}/Workflow/RedirectToInventoryItem.aspx?bu={1}&inv={2}", MerchandisingBaseUrl, BusinessUnitId, InventoryId); }
        }

        public string EStockCardUrl
        {
            get { return InventoryData.GetEStockLink(EStockCardBaseUrl, StockNumber, InventoryType == 2); }
        }

        public string InventoryTypeDescription
        {
            get { return InventoryType == 1 ? "New" : "Used"; }
        }

        public bool IsNotOnline
        {
            get
            {
                return !IsOnline;
            }
        }

        public bool IsOnline
        {
            get
            {
                return Online;
            }
        }

        public string PhotosUrl
        {
            get { return string.Format("{0}/Workflow/RedirectToInventoryItem.aspx?bu={1}&inv={2}&pg=photos", MerchandisingBaseUrl, BusinessUnitId, InventoryId); }
        }

        public int PhotoCount { get; set; }

        public string StatusDescription
        {
            get
            {
                if (!HasData)
                    return "N/A";

                return Online ? "Online" : "Not Online";
            }
        }

        public string VehicleDescription
        {
            get
            {
                return string.Format("{0} {1} {2}{3}",
                    VehicleYear, Make, Model,
                    string.IsNullOrEmpty(Trim) ? "" : " " + Trim);
            }
        }

        #region Interface IVehiclesOnline

        public int BusinessUnitId
        {
            get {return _online.BusinessUnitId;}
            set { _online.BusinessUnitId = value; }
        }

        public int InventoryId
        {
            get {return _online.InventoryId;}
            set { _online.InventoryId = value; }
        }

        public string StockNumber
        {
            get { return _online.StockNumber; }
            set { _online.StockNumber = value; }
        }

        public string Vin
        {
            get { return _online.Vin; }
            set { _online.Vin = value; }
        }

        public DateTime InventoryReceivedDate
        {
            get { return _online.InventoryReceivedDate; }
            set { _online.InventoryReceivedDate = value; }
        }

        public int VehicleYear
        {
            get { return _online.VehicleYear;}
            set { _online.VehicleYear = value;}
        }

        public string Make
        {
            get { return _online.Make; }
            set { _online.Make = value; }
        }

        public string Model
        {
            get { return _online.Model; }
            set { _online.Model = value; }
        }

        public string Trim
        {
            get { return _online.Trim; }
            set { _online.Trim = value; }
        }

        public byte InventoryType
        {
            get { return _online.InventoryType; }
            set { _online.InventoryType = value; }
        }

        public bool Certified
        {
            get { return _online.Certified; }
            set { _online.Certified = value; }
        }

        public bool Approved
        {
            get { return _online.Approved; }
            set { _online.Approved = value; }
        }

        public string ZipCode
        {
            get { return _online.ZipCode; }
            set { _online.ZipCode = value; }
        }
        
        #endregion Interface IVehiclesOnline
        
        #endregion Vehicle info

        #region Cars.com status and strings

        public bool HasCarsData
        {
            get { return _online.HasCarsData; }
            set { _online.HasCarsData = value; }
        }

        public bool CarsOnline
        {
            get { return _online.CarsOnline; }
            set { _online.CarsOnline = value; }
        }

        public bool CarsSent
        {
            get { return _online.CarsSent; }
            set { _online.CarsSent = value; }
        }

        public bool? CarsStatus
        {
            get { return !HasCarsData ? (bool?)null : CarsOnline; }
        }

        public string OnlineConfirmedStatus
        {
            get
            {
                return GetConfirmedStatus(Online, HasData);
            }
            
        }
        
        public string CarsConfirmedStatus
        {
            get
            {
                return GetConfirmedStatus(CarsOnline, CarsSent, HasCarsData);
            }
        }
        
        public string CarsStatusDescription
        {
            get { return GetStatusDescription(CarsStatus); }
        }
        
        public string CarsStatusIconUrl
        {
            get { return GetStatusIconUrl(HasCarsData, CarsSent || CarsOnline); }
        }
        
        public string CarsDotComUrl
        {
            get { return string.Format("http://www.cars.com/for-sale/searchresults.action?zc={0}&rd=10&kw={1}", ZipCode, StockNumber); }
        }
        
        #endregion Cars.com status and strings

        #region AutoTrader status and strings

        public bool HasAtData
        {
            get { return _online.HasAtData; }
            set { _online.HasAtData = value; }
        }

        public bool AtOnline
        {
            get { return _online.AtOnline; }
            set { _online.AtOnline = value; }
        }

        public bool AtSent
        {
            get { return _online.AtSent; }
            set { _online.AtSent = value; }
        }

        public bool? AtStatus
        {
            get { return !HasAtData ? (bool?)null : AtOnline; }
        }

        public string AtConfirmedStatus
        {
            get
            {
                return GetConfirmedStatus(AtOnline, AtSent, HasAtData);
            }
        }

        public string AtStatusDescription
        {
            get { return GetStatusDescription(AtStatus); }
        }

        public string AutoTraderUrl
        {
            get { return string.Format("http://www.autotrader.com/cars-for-sale/searchresults.xhtml?zip={0}&keywords={1}", ZipCode, Vin); }
        }

        public string OnlineStatusIconUrl
        {
            get { return GetStatusIconUrl(HasData, Online); }
        }

        public string AtStatusIconUrl
        {
            get
            {
                return GetStatusIconUrl(HasAtData, AtSent || AtOnline);
            }
        }

        private static string GetConfirmedStatus(bool online, bool hasData)
        {
            return (online) ? "Online" : (hasData) ? "Not Online" : "N/A";
        }

        private static string GetConfirmedStatus(bool online, bool sent, bool hasData)
        {
            return (online || sent) ? "Online" : (hasData) ? "Not Online" : "N/A";
        }

        private static string GetStatusDescription(bool? status)
        {
            if (status.HasValue)
                return status.Value ? "Confirmed" : "X";

            return "N/A";
        }

        // Wow, this is uber-fragile. don't move those image files or rename them!
        // TODO: Don't reference image files in the UI, please
        private string GetStatusIconUrl(bool hasData, bool confirmed)
        {
            if(hasData || confirmed)
                return string.Format("{0}{1}/Themes/MAX3.0/Images/icon_{2}.png", ExternalImageBaseUrl, MerchandisingBaseUrl, (confirmed) ? "GreenCheckmark" : "Cancel");

            return string.Format("{0}{1}/Themes/MAX3.0/Images/icon_{2}.png", ExternalImageBaseUrl, MerchandisingBaseUrl, @"NA");
        }

        #endregion string status

        public decimal? InternetPrice
        {
            get {return _online.InternetPrice;}
            set { _online.InternetPrice = value; }
        }

        public bool Online
        {
            get { return _online.Online; }
            set { _online.Online = value; }
        }

        public bool HasData
        {
            get { return _online.HasData; }
            set { _online.HasData = value; }
        }

    }
}
