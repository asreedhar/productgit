﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline.Client;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline
{
    public class ReportModel
    {
        private IMaxAnalytics _maxAnalytics;

        private IMemberContext MemberContext { get; set; }
        private IPhotoServices PhotoServices { get; set; }
        private IKeywordFilterFactory<VehicleOnlineBinder> KeywordFilterFactory { get; set; }

        public ReportModel(IMemberContext memberContext, IPhotoServices photoServices, IMaxAnalytics maxAnalytics)
        {
            MemberContext = memberContext;
            PhotoServices = photoServices;
            _maxAnalytics = maxAnalytics;
            KeywordFilterFactory = new KeywordFilterFactory<VehicleOnlineBinder>();
        }

        private static T GetContext<T>(string relativeUrl, Func<Uri, T> creator)
        {
            var baseUri = new Uri(Settings.Default.DataWebServices, UriKind.Absolute);
            var svcUri = new Uri(baseUri, relativeUrl);
            return creator(svcUri);
        }

        private static Services.BusinessUnit.Context GetBusinessUnitContext()
        {
            return GetContext("Data/BusinessUnit.svc/", u => new Services.BusinessUnit.Context(u));
        }

        public VehicleOnlineBinder[] GetVehiclesOnline()
        {
            return GetVehiclesOnline(0, 0, 2);
        }

        public VehicleOnlineBinder[] GetVehiclesOnline(int businessUnitIdFilter, int? inventoryTypeFilter, int? minAgeFilter)
        {
            var photoCountTask = Task<IVehiclesPhotosResult>.Factory.StartNew(
                    () => PhotoServices.GetPhotoUrlsByBusinessUnit(businessUnitIdFilter, TimeSpan.FromSeconds(30.0)));

            // TODO: We should probably be calling this async as well, since it makes remote calls to the max analytics web service.
            IVehiclesOnline[] reportData = _maxAnalytics.GetOnlineVehicles(businessUnitIdFilter, inventoryTypeFilter, minAgeFilter);
            var binder = reportData.Select(x => new VehicleOnlineBinder(x)).ToArray();

            IVehiclesPhotosResult vehiclesPhotosResult;
            using (MiniProfiler.Current.Step("Waiting for GetPhotoUrlsByBusinessUnit"))
                vehiclesPhotosResult = photoCountTask.Result;

            MergePhotoCountsWithReportData(binder, vehiclesPhotosResult);
            ApplyBaseUrlsToReportData(binder);

            return binder;
        }

        public VehicleOnlineBinder[] FilterGridData(IEnumerable<VehicleOnlineBinder> data, OnlineStatus? statusFilter, string textFilter)
        {
            data = SetupStatusFilter(statusFilter, data);
            data = SetupKeywordFilter(textFilter, data);

            return data.ToArray();
        }

        public IEnumerable<BusinessUnitListEntry> GetBusinessUnitList()
        {
            var businessUnitContext = GetBusinessUnitContext();
            var memberLogin = MemberContext.Current.UserName;
            return from businessUnit in businessUnitContext.BusinessUnitByMember
                   where businessUnit.Login == memberLogin
                   orderby businessUnit.BusinessUnit
                   select
                       new BusinessUnitListEntry
                           {Name = businessUnit.BusinessUnit, BusinessUnitID = businessUnit.BusinessUnitID};
        }

        private static void ApplyBaseUrlsToReportData(IEnumerable<VehicleOnlineBinder> reportData)
        {
            var estockcardBaseUrl = ConfigurationManager.AppSettings["estock_host_name"] ?? string.Empty;
            var merchandisingBaseUrl = HttpContext.Current.Request.ApplicationPath;
            var externalImageBaseUrl = ConfigurationManager.AppSettings["external_image_domain"] ?? string.Empty;

            foreach (var vo in reportData)
            {
                vo.EStockCardBaseUrl = estockcardBaseUrl;
                vo.MerchandisingBaseUrl = merchandisingBaseUrl;
                vo.ExternalImageBaseUrl = externalImageBaseUrl;
            }
        }

        private static void MergePhotoCountsWithReportData(IEnumerable<VehicleOnlineBinder> reportData, IVehiclesPhotosResult photoData)
        {
            foreach (var record in reportData)
            {
                IVehiclePhotos vehiclePhotos;
                record.PhotoCount =
                    photoData.PhotosByVin.TryGetValue(record.Vin, out vehiclePhotos) ?
                    vehiclePhotos.PhotoUrls.Length :
                    0;
            }
        }

        private static IEnumerable<VehicleOnlineBinder> SetupStatusFilter(OnlineStatus? statusFilter, IEnumerable<VehicleOnlineBinder> query)
        {
            if (statusFilter.HasValue && statusFilter.Value != OnlineStatus.All)
            {
                switch (statusFilter.Value)
                {
                    case OnlineStatus.Online:
                        query =
                            query.Where(
                                vo => vo.HasData && vo.Online);
                        break;
                    case OnlineStatus.NotOnline:
                        query =
                            query.Where(
                                vo => vo.HasData && !vo.Online);
                        break;
                }
            }
            return query;
        }


        private IEnumerable<VehicleOnlineBinder> SetupKeywordFilter(string textFilter, IEnumerable<VehicleOnlineBinder> intermediate)
        {
            var keywordFilter = KeywordFilterFactory.GetFilter(textFilter, vo => vo.GetPhrases());
            if (keywordFilter != null)
                intermediate = intermediate.Where(keywordFilter);
            return intermediate;
        }
    }
}