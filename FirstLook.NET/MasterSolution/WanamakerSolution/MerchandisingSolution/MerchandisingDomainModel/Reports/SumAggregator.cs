﻿using System.Collections.Generic;
using System.Linq;
using MAX.Entities.Interfaces;

namespace FirstLook.Merchandising.DomainModel.Reports
{
    public static class SumAggregator
    {
        public static TValue[] Summation<TValue>(this IEnumerable<IEnumerable<TValue>> sources) where TValue : IBusinessUnitAggregation, new()
        {
            //get set of businessunits
            var businessUnitsIds = sources
               .SelectMany(x => x)
               .Select(x => x.BusinessUnitId)
               .Distinct()
               .ToArray();

            var resultList = new List<TValue>();

            foreach (var businessUnitsId in businessUnitsIds) //go through each businessunit and sum across months
            {
                //select each business unit
                var monthInputs =
                      sources.SelectMany(x => x).Where(x => x.BusinessUnitId == businessUnitsId).Select(x => x).
                          ToArray();

                var result = Summation(monthInputs);
                resultList.Add(result);
            }

            return resultList.ToArray();
        }

        public static TValue Summation<TValue>(this IEnumerable<TValue> sources) where TValue : new()
        {
            var target = new TValue();
            var agg = new Aggregation<TValue>(sources, target);
            agg.Process();

            return target;
        }
    }
}
