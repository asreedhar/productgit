using System;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Reports.VehicleActivity.Client
{
    public partial class VehicleActivity
    {
        public const int CtrRoundingDigits = 4;

        public int? PhotoCount { get; set; }

        public int? EmailCount { get; set; }
        public int? MapCount { get; set; }
        public int? PrintCount { get; set; }
    
        public DateTime InventoryReceivedDate { get; set; }
        public DateTime LastRepriceDate { get; set; }

        public string DestinationName
        {
            get
            {
               switch((EdtDestinations)DestinationID)
               {
                   case EdtDestinations.CarsDotCom:
                       return "Cars.com";
                   case EdtDestinations.AutoTrader:
                       return "AutoTrader.com";
                   default:
                       return ((EdtDestinations)DestinationID).ToString();
               }
            }
        }

        public bool LowActivity
        {
            get
            {
                return (SearchPageViewsSinceLastReprice ?? 0) >= MinSearchCountThreshold
                       && Math.Round(CTRSinceLastReprice ?? 0, CtrRoundingDigits) <= Math.Round(LowActivityThreshold, CtrRoundingDigits);
            }
        }

        public bool HighActivity
        {
            get
            {
                return (SearchPageViewsSinceLastReprice ?? 0) >= MinSearchCountThreshold
                       && Math.Round(CTRSinceLastReprice ?? 0, CtrRoundingDigits) >= Math.Round(HighActivityThreshold, CtrRoundingDigits);
            }
        }

        public string EstockCardBaseUrl { get; set; }

        public string MerchandisingBaseUrl { get; set; }

        public string EstockCardUrl
        {
            get
            {
                return
                    string.Format(
                        "javascript:void(window.open('{0}/IMT/EStock.go?stockNumberOrVin={1}&productMode=edge&inventoryType={2}', '_blank'))",
                        EstockCardBaseUrl, StockNumber, InventoryType == 1 ? "new" : "used");
            }
        }

        public string ApprovalPageUrl { get { return BuildWorkflowLink(); } }
        public string PhotosPageUrl { get { return BuildWorkflowLink("photos"); } }
        public string PricingPageUrl { get { return BuildWorkflowLink("pricing"); } }

        private string BuildWorkflowLink(string pg = null)
        {
            return string.Format(
                "javascript:void(window.open('{0}/Workflow/RedirectToInventoryItem.aspx?bu={1}&inv={2}{3}', '_blank'))",
                MerchandisingBaseUrl, BusinessUnitID, InventoryID,
                pg != null ? "&pg=" + pg : "");
        }

        /// <summary>
        /// Returns true if it is obvious that the "since last reprice" stats are hosed.
        /// </summary>
        public bool SinceLastRepriceDataIsSuspect
        {
            get
            {
                // negative numbers in any of these just don't make sense
                return CTRSinceLastReprice < 0  
                       || DetailPageViewsSinceLastReprice < 0
                       || SearchPageViewsSinceLastReprice < 0;
            }
        }

		public string ToLowActivityString()
		{
			return string.Format(
				"VehicleActivity: Destination Id/Name: {0}/{1}, SRP:{2}, SRP_SLR:{3}, MinSearchCountThreshold:{4}, LowActivityThreshold:{5}, CTRSinceLastReprice: {6} ", 
				DestinationID, DestinationName, 
				SearchCount, SearchPageViewsSinceLastReprice, MinSearchCountThreshold, LowActivityThreshold, CTRSinceLastReprice);
		}
 
    

    }
}