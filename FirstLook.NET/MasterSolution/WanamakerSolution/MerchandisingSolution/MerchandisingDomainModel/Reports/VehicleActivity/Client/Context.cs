﻿using System.Data.Services.Client;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Reports.VehicleActivity.Client
{
    public partial class Context
    {
        public IQueryable<VehicleActivity> GetVehicleActivity(string memberName)
        {
            return CreateServiceOperationQuery<VehicleActivity>(memberName, "GetVehicleActivity");
        }

        private IQueryable<T> CreateServiceOperationQuery<T>(string memberName, string entitySetName)
        {
            if (string.IsNullOrWhiteSpace(memberName))
                throw new DataServiceClientException("memberName is a required parameter");

            return CreateQuery<T>(entitySetName).AddQueryOption("memberName", "'" + memberName + "'");
        }
    }
}