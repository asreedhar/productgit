
using System;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Reports.VehicleActivity
{
    public class FlatVehicleActivity
    {
        public FlatVehicleActivity(Client.VehicleActivity activity)
        {
            BusinessUnitID = activity.BusinessUnitID;
            InventoryID = activity.InventoryID;
            VIN = activity.VIN;
            StockNumber = activity.StockNumber;
            Year = activity.Year;
            Make = activity.Make;
            Model = activity.Model;
            Trim = activity.Trim;
            Mileage = activity.Mileage;
            AgeInDays = activity.AgeInDays;
            InventoryType = activity.InventoryType;
            InternetPrice = activity.InternetPrice;
            PctAvgMarketPrice = activity.PctAvgMarketPrice;
            AvgMarketPrice = activity.AvgMarketPrice;
            LowActivityThreshold = activity.LowActivityThreshold;
            MinSearchCountThreshold = activity.MinSearchCountThreshold;

            Update(activity);
        }

        public int BusinessUnitID { get; private set; }
        public int InventoryID { get; private set; }
        public string VIN { get; private set; }
        public string StockNumber { get; private set; }

        public int? Year { get; private set; }
        public string Make { get; private set; }
        public string Model { get; private set; }
        public string Trim { get; private set; }

        public int? Mileage { get; private set; }
        public int? AgeInDays { get; private set; }
        public byte? InventoryType { get; private set; }

        public decimal? InternetPrice { get; private set; }
        public decimal? PctAvgMarketPrice { get; private set; }
        public int? AvgMarketPrice { get; private set; }

        public float LowActivityThreshold { get; private set; }
        public float HighActivityThreshold { get; private set; }
        public int MinSearchCountThreshold { get; private set; }

        private Client.VehicleActivity CarsDotComVehicleActivity { get; set; }

        private Client.VehicleActivity AutoTraderVehicleActivity { get; set; }


        public void Update(Client.VehicleActivity vehicleActivity)
        {

            if (vehicleActivity.DestinationID == 1)
            {
                CarsDotComVehicleActivity = vehicleActivity;
            }
            if (vehicleActivity.DestinationID == 2)
            {
                AutoTraderVehicleActivity = vehicleActivity;
            }
        }

        public DateTime? ApprovalDate
        {
            // the fact that the dates are split by destination id is confusing and meaningless.  Just in case they are different, we will report the latest one.
            get
            {
                if (AutoTraderVehicleActivity.ApprovalDate.HasValue)
                {
                    if (CarsDotComVehicleActivity.ApprovalDate.HasValue)
                    {
                        return AutoTraderVehicleActivity.ApprovalDate.Value >
                               CarsDotComVehicleActivity.ApprovalDate.Value
                            ? AutoTraderVehicleActivity.ApprovalDate
                            : CarsDotComVehicleActivity.ApprovalDate;
                    }
                    return AutoTraderVehicleActivity.ApprovalDate;
                }
                return CarsDotComVehicleActivity.ApprovalDate;
            }
        }

        // AutoTrader properties
        public int? AutoTraderOverallSearchCount
        {
            get { return AutoTraderVehicleActivity.SearchCount; }
        }

        public int? AutoTraderOverallDetailsPageViewCount
        {
            get { return AutoTraderVehicleActivity.DetailsPageViewCount; }
        }

        public float? AutoTraderOverallCTR
        {
            get { return AutoTraderVehicleActivity.CTR; }
        }

        public int? AutoTraderOverallActionCount
        {
            get { return AutoTraderVehicleActivity.ActionCount; }
        }

        public int? AutoTraderSearchPageViewsSinceLastReprice
        {
            get
            {
                return AutoTraderVehicleActivity.SinceLastRepriceDataIsSuspect
                    ? null
                    : AutoTraderVehicleActivity.SearchPageViewsSinceLastReprice;

            }
        }

        public int? AutoTraderDetailPageViewsSinceLastReprice
        {
            get
            {
                return AutoTraderVehicleActivity.SinceLastRepriceDataIsSuspect
                    ? null
                    : AutoTraderVehicleActivity.DetailPageViewsSinceLastReprice;
            }
        }

        public float? AutoTraderCTRSinceLastReprice
        {
            get
            {
                return AutoTraderVehicleActivity.SinceLastRepriceDataIsSuspect
                    ? null
                    : AutoTraderVehicleActivity.CTRSinceLastReprice;
            }
        }

        public float? AutoTraderContactsSinceLastReprice
        {
            get
            {
                return AutoTraderVehicleActivity.SinceLastRepriceDataIsSuspect
                  ? null
                  : (AutoTraderVehicleActivity.EmailCount + AutoTraderVehicleActivity.MapCount + AutoTraderVehicleActivity.PrintCount);
            }
        }
        public float? AutoTraderConversionRateSinceLastReprice
        {
            get
            {
                return (AutoTraderVehicleActivity.SinceLastRepriceDataIsSuspect || AutoTraderVehicleActivity.DetailPageViewsSinceLastReprice == 0)
                  ? null
                  : (float?)(AutoTraderVehicleActivity.EmailCount + AutoTraderVehicleActivity.MapCount + AutoTraderVehicleActivity.PrintCount) / (float?)AutoTraderVehicleActivity.DetailPageViewsSinceLastReprice;
            }
        }



    // CarsDotCom properties
        public int? CarsDotComOverallSearchCount
        {
            get { return CarsDotComVehicleActivity.SearchCount; }
        }

        public int? CarsDotComOverallDetailsPageViewCount
        {
            get { return CarsDotComVehicleActivity.DetailsPageViewCount; }
        }

        public float? CarsDotComOverallCTR
        {
            get { return CarsDotComVehicleActivity.CTR; }
        }

        public int? CarsDotComOverallActionCount
        {
            get { return CarsDotComVehicleActivity.ActionCount; }
        }

        public int? CarsDotComSearchPageViewsSinceLastReprice
        {
            get
            {
                return CarsDotComVehicleActivity.SinceLastRepriceDataIsSuspect
                    ? null
                    : CarsDotComVehicleActivity.SearchPageViewsSinceLastReprice;
            }
        }

        public int? CarsDotComDetailPageViewsSinceLastReprice
        {
            get
            {
                return CarsDotComVehicleActivity.SinceLastRepriceDataIsSuspect
                  ? null
                  : CarsDotComVehicleActivity.DetailPageViewsSinceLastReprice;
            }
        }

        public float? CarsDotComCTRSinceLastReprice
        {
            get
            {
                return CarsDotComVehicleActivity.SinceLastRepriceDataIsSuspect
                  ? null
                  : CarsDotComVehicleActivity.CTRSinceLastReprice;
            }
        }

        public float? CarsDotComContactsSinceLastReprice
        {
            get
            {
                return CarsDotComVehicleActivity.SinceLastRepriceDataIsSuspect
                  ? null
                  : (CarsDotComVehicleActivity.EmailCount + CarsDotComVehicleActivity.MapCount + CarsDotComVehicleActivity.PrintCount);
            }
        }
        public float? CarsDotComConversionRateSinceLastReprice
        {
            get
            {
                return (CarsDotComVehicleActivity.SinceLastRepriceDataIsSuspect || CarsDotComVehicleActivity.DetailPageViewsSinceLastReprice == 0)
                  ? null
                  : (float?)(CarsDotComVehicleActivity.EmailCount + CarsDotComVehicleActivity.MapCount + CarsDotComVehicleActivity.PrintCount) / (float?)CarsDotComVehicleActivity.DetailPageViewsSinceLastReprice;
            }
        }



        public const int CtrRoundingDigits = 4;

        public int? PhotoCount { get; set; }

        public bool LowActivity
        {
            get { return AutoTraderVehicleActivity.LowActivity || CarsDotComVehicleActivity.LowActivity; }
        }

        public bool HighActivity
        {
            get{ return (AutoTraderVehicleActivity.HighActivity || CarsDotComVehicleActivity.HighActivity); }
        }

        public string EstockCardBaseUrl { get; set; }

        public string MerchandisingBaseUrl { get; set; }

        public string EstockCardUrl
        {
            get
            {
                return
                    string.Format(
                        "javascript:void(window.open('{0}/IMT/EStock.go?stockNumberOrVin={1}&productMode=edge&inventoryType={2}', '_blank'))",
                        EstockCardBaseUrl, StockNumber, InventoryType == 1 ? "new" : "used");
            }
        }

        public string ApprovalPageUrl { get { return BuildWorkflowLink(); } }
        public string PhotosPageUrl { get { return BuildWorkflowLink("photos"); } }
        public string PricingPageUrl { get { return BuildWorkflowLink("pricing"); } }

        private string BuildWorkflowLink(string pg = null)
        {
            return string.Format(
                "javascript:void(window.open('{0}/Workflow/RedirectToInventoryItem.aspx?bu={1}&inv={2}{3}', '_blank'))",
                MerchandisingBaseUrl, BusinessUnitID, InventoryID,
                pg != null ? "&pg=" + pg : "");
        }



    }
}