﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.Properties;
using FirstLook.Merchandising.DomainModel.Reports.VehicleActivity.Client;
using MvcMiniProfiler;
//        FirstLook.Merchandising.DomainModel.Reports.VehicleActivity
namespace FirstLook.Merchandising.DomainModel.Reports.VehicleActivity
{
    public class ReportModel
    {
        private IMemberContext MemberContext { get; set; }
        private IPhotoServices PhotoServices { get; set; }
        private IKeywordFilterFactory<Client.VehicleActivity> KeywordFilterFactory { get; set; }
        private IMaxAnalytics MaxAnalytics { get; set; }

        public ReportModel(IMemberContext memberContext, IPhotoServices photoServices, IMaxAnalytics maxAnalytics)
        {
            MemberContext = memberContext;
            PhotoServices = photoServices;
            KeywordFilterFactory = new KeywordFilterFactory<Client.VehicleActivity>();
            MaxAnalytics = maxAnalytics;
        }

        private static Services.BusinessUnit.Context GetBusinessUnitContext()
        {
            var baseUri = new Uri(Settings.Default.DataWebServices, UriKind.Absolute);
            var businessUnitUri = new Uri(baseUri, "Data/BusinessUnit.svc/");
            return new Services.BusinessUnit.Context(businessUnitUri);
        }

        public FlatVehicleActivity[] GetVehicleActivity(int businessUnitIdFilter, int? inventoryTypeFilter, string textFilter, int? minAgeFilter, string activityFilter)
        {
            var photoCountTask = Task<IVehiclesPhotosResult>.Factory.StartNew(
                    () => PhotoServices.GetPhotoUrlsByBusinessUnit(businessUnitIdFilter, TimeSpan.FromSeconds(30.0)));

            Client.VehicleActivity[] reportData;
            using (MiniProfiler.Current.Step("Web Svc: GetVehicleActivity"))
                reportData = ExecuteVehicleActivityQuery(businessUnitIdFilter, inventoryTypeFilter, textFilter, minAgeFilter, activityFilter);

            var flatReportData = FlattenReportData(reportData);

            IVehiclesPhotosResult vehiclesPhotosResult;
            using (MiniProfiler.Current.Step("Waiting for GetPhotoUrlsByBusinessUnit"))
                vehiclesPhotosResult = photoCountTask.Result;

            MergePhotoCountsWithReportData(flatReportData, vehiclesPhotosResult);
            ApplyBaseUrlsToReportData(flatReportData);

            return flatReportData;
        }

        private FlatVehicleActivity[] FlattenReportData(IEnumerable<Client.VehicleActivity> reportData)
        {
            var output = new Dictionary<InventoryKey, FlatVehicleActivity>();

            foreach (var vehicleActivity in reportData)
            {
                var key = new InventoryKey(vehicleActivity);
                if (output.ContainsKey(key))
                {
                    output[key].Update(vehicleActivity);
                }
                else
                {
                    output.Add(new InventoryKey(vehicleActivity), new FlatVehicleActivity(vehicleActivity));
                }
            }

            return output.Values.ToArray();
        }

        private void ApplyBaseUrlsToReportData(IEnumerable<FlatVehicleActivity> reportData)
        {
            var estockcardBaseUrl = ConfigurationManager.AppSettings["estock_host_name"] ?? string.Empty;
            var merchandisingBaseUrl = HttpContext.Current.Request.ApplicationPath;

            foreach(var va in reportData)
            {
                va.EstockCardBaseUrl = estockcardBaseUrl;
                va.MerchandisingBaseUrl = merchandisingBaseUrl;
            }
        }

        private Client.VehicleActivity[] ExecuteVehicleActivityQuery(int businessUnitIdFilter, int? inventoryTypeFilter, 
            string textFilter, int? minAgeFilter, string activityFilter)
        {
            var allVehicles = MaxAnalytics.GetLowActivity(businessUnitIdFilter, inventoryTypeFilter, minAgeFilter);

            var intermediate = SetupPostFilters(textFilter, activityFilter, allVehicles.ToArray());

            return intermediate.ToArray();
        }
        
        private class InventoryKey
        {
            private int BusinessUnitId { get; set; }
            private string VIN { get; set; }
            private string StockNumber { get; set; }

            public InventoryKey(Client.VehicleActivity va)
                : this(va.BusinessUnitID, va.VIN, va.StockNumber)
            {
            }

            private InventoryKey(int businessUnitId, string vin, string stockNumber)
            {
                BusinessUnitId = businessUnitId;
                VIN = vin;
                StockNumber = stockNumber;
            }

            private bool Equals(InventoryKey other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return other.BusinessUnitId == BusinessUnitId && Equals(other.VIN, VIN) && Equals(other.StockNumber, StockNumber);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != typeof(InventoryKey)) return false;
                return Equals((InventoryKey)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var result = BusinessUnitId;
                    result = (result * 397) ^ (VIN != null ? VIN.GetHashCode() : 0);
                    result = (result * 397) ^ (StockNumber != null ? StockNumber.GetHashCode() : 0);
                    return result;
                }
            }
        }

        private class CTRData
        {
            public InventoryKey Key { get; set; }
            public double CTR { get; set; }
            public double LowActivityThreshold { get; internal set; }
            public double HighActivityThreshold { get; internal set; }
        }

        private IEnumerable<Client.VehicleActivity> SetupPostFilters(string textFilter, string activityFilter, Client.VehicleActivity[] data)
        {
            IEnumerable<Client.VehicleActivity> intermediate = data;

            intermediate = SetupLowAndHighActivityFilter(activityFilter, data, intermediate);
            intermediate = SetupKeywordFilter(textFilter, intermediate);

            return intermediate;
        }

        private IEnumerable<Client.VehicleActivity> SetupKeywordFilter(string textFilter, IEnumerable<Client.VehicleActivity> intermediate)
        {
            var keywordFilter = KeywordFilterFactory.GetFilter(textFilter, GetPhrases);
            if (keywordFilter != null)
                intermediate = intermediate.Where(keywordFilter);
            return intermediate;
        }

        private static IEnumerable<string> GetPhrases(Client.VehicleActivity va)
        {
            //yield return va.DestinationName;
            yield return va.Make;
            yield return va.Model;
            yield return va.StockNumber;
            yield return va.Trim;
            yield return va.VIN;
            yield return va.Year.HasValue ? va.Year.Value.ToString(CultureInfo.InvariantCulture) : null;
        }

        private static IEnumerable<Client.VehicleActivity> SetupLowAndHighActivityFilter(string activityFilter, IEnumerable<Client.VehicleActivity> data,
                                                                 IEnumerable<Client.VehicleActivity> intermediate)
        {
            Func<CTRData, bool> activityPredicate;

            Func<Client.VehicleActivity[], double> ctrSelect = null;

            switch (activityFilter)
            {
                case "LOW":
                    activityPredicate = ctr => Math.Round(ctr.CTR, Client.VehicleActivity.CtrRoundingDigits) <= 
                        Math.Round(ctr.LowActivityThreshold, Client.VehicleActivity.CtrRoundingDigits);

                    ctrSelect = g => g.Min(va =>(va.DetailPageViewsSinceLastReprice ?? 0.0)/(va.SearchPageViewsSinceLastReprice ?? 1.0));
                    break;
                case "HIGH":
                    activityPredicate = ctr => Math.Round(ctr.CTR, Client.VehicleActivity.CtrRoundingDigits) >=
                        Math.Round(ctr.HighActivityThreshold, Client.VehicleActivity.CtrRoundingDigits);

                    ctrSelect = g => g.Max(va => (va.DetailPageViewsSinceLastReprice ?? 0.0) / (va.SearchPageViewsSinceLastReprice ?? 1.0));
                    break;
                default:
                    activityPredicate = null;
                    break;
            }

            if (activityPredicate != null)
            {
                var includedVehicles =
                    new HashSet<InventoryKey>(
                        data
                            .Where(
                                d => d.SearchPageViewsSinceLastReprice.HasValue
                                     && d.SearchPageViewsSinceLastReprice >= d.MinSearchCountThreshold
                                     && d.SearchPageViewsSinceLastReprice > 0)
                            .GroupBy(d => new InventoryKey(d))
                            .Select(g => new CTRData
                                             {
                                                 Key = g.Key, 
                                                 CTR = ctrSelect(g.ToArray()),
                                                 LowActivityThreshold = g.Select(va => (double?)va.LowActivityThreshold).FirstOrDefault() ?? 0.02,
                                                 HighActivityThreshold = g.Select(va => (double?)va.HighActivityThreshold).FirstOrDefault() ?? 0.04
                                             })
                            .Where(activityPredicate)
                            .Select(g => g.Key));
                intermediate = intermediate.Where(va => includedVehicles.Contains(new InventoryKey(va)));
            }
            return intermediate;
        }


        private static void MergePhotoCountsWithReportData(IEnumerable<FlatVehicleActivity> reportData, IVehiclesPhotosResult photoData)
        {
            foreach (var record in reportData)
            {
                IVehiclePhotos vehiclePhotos;
                record.PhotoCount =
                    photoData.PhotosByVin.TryGetValue(record.VIN, out vehiclePhotos) ?
                    vehiclePhotos.PhotoUrls.Length :
                    0;
            }
        }

        public IEnumerable<BusinessUnitListEntry> GetBusinessUnitList()
        {
            var businessUnitContext = GetBusinessUnitContext();
            var memberLogin = MemberContext.Current.UserName;
            return from businessUnit in businessUnitContext.BusinessUnitByMember
                where businessUnit.Login == memberLogin
                orderby businessUnit.BusinessUnit
                select new BusinessUnitListEntry { Name = businessUnit.BusinessUnit, BusinessUnitID = businessUnit.BusinessUnitID };
        }
    }
}

