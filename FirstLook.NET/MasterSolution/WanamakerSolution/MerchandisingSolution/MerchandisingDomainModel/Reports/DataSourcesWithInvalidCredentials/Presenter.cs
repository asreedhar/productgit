using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Reports.DataSourcesWithInvalidCredentials
{
    public class Presenter : IPresenter
    {
        [Serializable]
        public class State
        {
            public int? CachedBusinessUnitId { get; set; }
            public DataSourceInfo[] CachedDataSourceInfo { get; set; }
            public int? BusinessUnitId { get; set; }
        }

        private Func<int, IEnumerable<DataSourceStatus>> GetDataFunc { get; set; }
        private EdtDestinations[] Destinations { get; set; }
        private DateTime StaleThreshold { get; set; }

        private int? BusinessUnitId { get; set; }

        private int? CachedBusinessUnitId { get; set; }
        private DataSourceInfo[] CachedDataSourceInfo { get; set; }

        public Presenter(Func<int, IEnumerable<DataSourceStatus>> getDataFunc, double staleDataCutoff, EdtDestinations[] destinations, DateTime referenceTime)
        {
            if(getDataFunc == null)
                throw new ArgumentNullException("getDataFunc");
            if(destinations == null)
                throw new ArgumentNullException("destinations");
            if(referenceTime.Kind != DateTimeKind.Utc)
                throw new ArgumentException("Time must be in UTC format.", "referenceTime");

            GetDataFunc = getDataFunc;
            Destinations = destinations;
            StaleThreshold = referenceTime.AddDays(-staleDataCutoff);
        }

        public object GetState()
        {
            return new State
                       {
                           BusinessUnitId = BusinessUnitId, 
                           CachedBusinessUnitId = CachedBusinessUnitId,
                           CachedDataSourceInfo = CachedDataSourceInfo
                       };
        }

        public void SetState(object obj)
        {
            var state = obj as State;
            if(state == null) return;
            BusinessUnitId = state.BusinessUnitId;
            CachedBusinessUnitId = state.CachedBusinessUnitId;
            CachedDataSourceInfo = state.CachedDataSourceInfo;
        }

        public void SetBusinessUnitId(int? businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }

        public IEnumerable<DataSourceInfo> DataSourceInformation
        {
            get
            {
                if (IsCachedDataSourceInfoStale())
                {
                    CachedDataSourceInfo = ReadDataSourceInfo();
                    CachedBusinessUnitId = BusinessUnitId;
                }

                return CachedDataSourceInfo;
            }
        }

        private bool IsCachedDataSourceInfoStale()
        {
            return CachedDataSourceInfo == null
                   || CachedBusinessUnitId != BusinessUnitId;
        } 

        private DataSourceInfo[] ReadDataSourceInfo()
        {
            if (!BusinessUnitId.HasValue)
                return new DataSourceInfo[0];

            var lookup =
                (GetDataFunc(BusinessUnitId.Value) ?? new DataSourceStatus[0]).ToDictionary(
                    status => status.Destination);

            return (from destination in Destinations
                    let rawData = GetRawDataFromLookup(lookup, destination)
                    select rawData == null
                               ? new DataSourceInfo(destination, Status.MissingCredentials | Status.StaleData, null)
                               : new DataSourceInfo(destination, CalcStatus(rawData), rawData.LastLoadedTime)
                   ).ToArray();
        }

        private static DataSourceStatus GetRawDataFromLookup(IDictionary<EdtDestinations, DataSourceStatus> lookup, EdtDestinations destination)
        {
            DataSourceStatus rawData;
            lookup.TryGetValue(destination, out rawData);
            return rawData;
        }

        private Status CalcStatus(DataSourceStatus dataSource)
        {
            var status = new Status();

            if (!dataSource.HasCredentials)
                status |= Status.MissingCredentials;
            else if (dataSource.HasInvalidCredentials)
                status |= Status.InvalidCredentials;

            if (!dataSource.LastLoadedTime.HasValue || dataSource.LastLoadedTime.Value.ToUniversalTime() < StaleThreshold)
                status |= Status.StaleData;

            return status;
        }
    }
}