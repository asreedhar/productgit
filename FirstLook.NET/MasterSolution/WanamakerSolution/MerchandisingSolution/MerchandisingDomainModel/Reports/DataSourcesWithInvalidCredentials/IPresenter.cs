﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Reports.DataSourcesWithInvalidCredentials
{
    public interface IPresenter
    {
        object GetState();

        void SetState(object state);

        void SetBusinessUnitId(int? businessUnitId);

        IEnumerable<DataSourceInfo> DataSourceInformation { get; }
    }
}