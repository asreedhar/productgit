using System;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Reports.DataSourcesWithInvalidCredentials
{
    [Serializable]
    public class DataSourceInfo
    {
        public EdtDestinations Destination { get; private set; }
        public Status Status { get; private set; }
        public DateTime? LastLoadedTime { get; private set; }

        public bool TestStatus(Status flags)
        {
            return (Status & flags) == flags;
        }

        public DataSourceInfo(EdtDestinations destination, Status status, DateTime? lastLoadedTime)
        {
            Destination = destination;
            Status = status;
            LastLoadedTime = lastLoadedTime;
        }
    }
}