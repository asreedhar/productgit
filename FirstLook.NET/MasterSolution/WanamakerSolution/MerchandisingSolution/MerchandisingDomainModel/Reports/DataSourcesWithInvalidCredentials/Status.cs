using System;

namespace FirstLook.Merchandising.DomainModel.Reports.DataSourcesWithInvalidCredentials
{
    [Flags]
    public enum Status
    {
        Ok = 0,
        MissingCredentials = 1,
        InvalidCredentials = 2,
        StaleData = 4,
        InsufficientPrivileges = 8
    }
}