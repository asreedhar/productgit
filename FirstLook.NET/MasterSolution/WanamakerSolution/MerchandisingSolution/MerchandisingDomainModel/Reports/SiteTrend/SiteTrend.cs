﻿using System;

namespace FirstLook.Merchandising.DomainModel.Reports
{
    public class SiteTrend
    {
        public int site_id;
        public string site_name;
        public DateTime applies_ts;
        
        public int used_vdps;
        public int new_vdps;
        public int both_vdps;
    }
}
