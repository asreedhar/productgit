﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MerchandisingDomainModel")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("MerchandisingDomainModel")]

// Make internals visible to test project
[assembly:InternalsVisibleTo("DynamicProxyGenAssembly2")]

[assembly: InternalsVisibleTo("PricingDomainModel_Test")]

[assembly: InternalsVisibleTo("PricingDomainModel_IntegrationTests")]

[assembly: InternalsVisibleTo("MerchandisingDomainModelTests")]
[assembly: InternalsVisibleTo("TimeToMarketTests")]
[assembly: InternalsVisibleTo( "MerchandisingDomainModel.IntegrationTests" )]

[assembly: InternalsVisibleTo("MerchandisingRegistry")]

[assembly: InternalsVisibleTo("Merchandising.Release.Tasks.Service.Tests")]


[assembly: InternalsVisibleTo("Wannamaker.WebApp.Tests")]
