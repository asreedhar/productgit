﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Enumerations
{
    /// <summary>
    /// Stored procs rely on these values, don't change them.
    /// </summary>
    [Flags]
    public enum AdCompleteFlags
    {
        MinimumPhotos = 1,
        Description = 2,
        Price = 4
    }
}
