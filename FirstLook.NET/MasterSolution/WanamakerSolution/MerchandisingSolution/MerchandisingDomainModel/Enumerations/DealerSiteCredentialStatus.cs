﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Enumerations
{
    public enum DealerSiteCredentialStatus
    {
        Invalid = -1,
        NotEntered = 0,
        Valid = 1,
    }
}
