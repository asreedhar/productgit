using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Alerts
{
    public class Subscription
    {
        public string Email { get; private set; }
        private int ReportId { get; set; }
        private List<int> Days { get; set; }
        public int TimeOfDay { get; private set; }

        private Subscription()
        {
            Days = new List<int>();
        }

        private Subscription(IDataRecord reader)
        {            
            ReportId = reader.GetInt32(reader.GetOrdinal("reportId"));
            TimeOfDay = reader.GetInt32(reader.GetOrdinal("timeOfDay"));
            Email = reader.GetString(reader.GetOrdinal("emailAddress"));
            Days = new List<int>();
            for (int i = 1; i <= 7; i++)
            {
                if (0 != (int) reader[i.ToString()])
                {
                    Days.Add(i);
                }
            }            
        }

        public bool SubscribesOnDay(int dayOfWeek)
        {
            return Days.Contains(dayOfWeek);
        }


        /// <summary>
        /// Factory method.
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="businessUnitId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public static Subscription Fetch(int reportId, int businessUnitId, int memberId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "alerts.subscriptions#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ReportId", reportId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "MemberId", memberId, DbType.Int32);
                    using(IDataReader reader = cmd.ExecuteReader())
                    {
                        return reader.Read() ? new Subscription(reader) : new Subscription();
                    }
                }
            }
        }

        public static void AddSubscription(int reportId, int businessUnitId, int memberId, string memberLogin, string email, int dayOfWeek, int timeOfDay)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "alerts.subscription#Create";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ReportId", reportId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "MemberId", memberId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);
                    Database.AddRequiredParameter(cmd, "EmailAddress", email, DbType.String);
                    Database.AddRequiredParameter(cmd, "DayOfWeek", dayOfWeek, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "TimeOfDay", timeOfDay, DbType.Int32);
                    cmd.ExecuteNonQuery();
                    
                }
            }
        }

        public static void DeleteSubscriptions(int businessUnitId, int memberId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "alerts.subscriptions#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "MemberId", memberId, DbType.Int32);
                    
                    cmd.ExecuteNonQuery();

                }
            }
        }

        

    }
}
