using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;

namespace FirstLook.Merchandising.DomainModel.Barcodes
{
    /// <summary>
    /// http://en.wikipedia.org/wiki/Code_39
    /// </summary>
    public class Code39
    {
    	private const int ITEM_SEP_HEIGHT=3;
		
		SizeF _titleSize=SizeF.Empty;
		SizeF _barCodeSize=SizeF.Empty;
		SizeF _codeStringSize=SizeF.Empty;
		
		#region Barcode Title 

        public string Title { get; set; }

        public Font TitleFont { get; set; }

        #endregion

		#region Barcode code string

        public bool ShowCodeString { get; set; }

        public Font CodeStringFont { get; set; }

        #endregion

		#region Barcode Font

		private Font _c39Font;

        public string FontFileName { get; set; }

        public string FontFamilyName { get; set; }

        public float FontSize { get; set; }

        private Font Code39Font
		{
			get
			{
				if (_c39Font==null)
				{
					// Load the barcode font			
					PrivateFontCollection pfc=new PrivateFontCollection();
					pfc.AddFontFile(FontFileName);

                    FontFamily family=new FontFamily(FontFamilyName,pfc);			
					_c39Font=new Font(family,FontSize);
				}
				return _c39Font;
			}
		}

		#endregion

		public Code39()
		{
		    FontSize = 12;
		    FontFamilyName = null;
		    FontFileName = null;
		    ShowCodeString = false;
		    Title = null;
		    TitleFont=new Font("Arial",10);
			CodeStringFont=new Font("Arial",10);
		}
		
		#region Barcode Generation

		public Bitmap GenerateBarcode(string barCode)
		{
			
			int bcodeWidth=0;
			int bcodeHeight=0;

			// Get the image container...
			Bitmap  bcodeBitmap =CreateImageContainer(barCode, ref bcodeWidth, ref bcodeHeight);
			Graphics objGraphics = Graphics.FromImage(bcodeBitmap);

			// Fill the background			
			objGraphics.FillRectangle(new SolidBrush(Color.White), new Rectangle(0,0,bcodeWidth,bcodeHeight));

			int vpos=0;

			// Draw the title string
			if (Title!=null)			
			{
				objGraphics.DrawString(Title, TitleFont, new SolidBrush(Color.Black),XCentered((int)_titleSize.Width,bcodeWidth),vpos);
				vpos+=(((int)_titleSize.Height)+ITEM_SEP_HEIGHT);
			}
			// Draw the barcode
			objGraphics.DrawString(barCode, Code39Font, new SolidBrush(Color.Black),XCentered((int)_barCodeSize.Width,bcodeWidth),vpos);

			// Draw the barcode string
			if (ShowCodeString)
			{
				vpos+=(((int)_barCodeSize.Height));
				objGraphics.DrawString(barCode, CodeStringFont, new SolidBrush(Color.Black),XCentered((int)_codeStringSize.Width,bcodeWidth),vpos);
			}

			// return the image...									
			return bcodeBitmap;			
		}		

		private Bitmap CreateImageContainer(string barCode, ref int bcodeWidth, ref int bcodeHeight)
		{
		    // Create a temporary bitmap...
			Bitmap tmpBitmap = new Bitmap(1,1,PixelFormat.Format32bppArgb); 
			Graphics objGraphics = Graphics.FromImage(tmpBitmap); 

			// calculate size of the barcode items...
			if (Title!=null)			
			{
				_titleSize=objGraphics.MeasureString(Title,TitleFont);				
				bcodeWidth=(int)_titleSize.Width;
				bcodeHeight=(int)_titleSize.Height+ITEM_SEP_HEIGHT;
			}

			_barCodeSize=objGraphics.MeasureString(barCode,Code39Font);								
			bcodeWidth=Max(bcodeWidth,(int)_barCodeSize.Width);
			bcodeHeight+=(int)_barCodeSize.Height;
			
			if (ShowCodeString)
			{
				_codeStringSize=objGraphics.MeasureString(barCode,CodeStringFont);
				bcodeWidth=Max(bcodeWidth,(int)_codeStringSize.Width);
				bcodeHeight+=(ITEM_SEP_HEIGHT+(int)_codeStringSize.Height);
			}
			
			// dispose temporary objects...
			objGraphics.Dispose();
			tmpBitmap.Dispose();

			return (new Bitmap(bcodeWidth,bcodeHeight,PixelFormat.Format32bppArgb));
		}

		#endregion


		#region Auxiliary Methods

		private static int Max(int v1, int v2)
		{
			return (v1>v2 ? v1 : v2 );
		}

		private static int XCentered(int localWidth, int globalWidth)
		{
			return ((globalWidth-localWidth)/2);
		}

		#endregion
    }
}
