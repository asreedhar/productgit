using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace FirstLook.Merchandising.DomainModel.Barcodes
{
    public class BarCodeHelper
    {
        
        public static byte[] CreateCode39(string code, int barSize, bool showCodeString, string title, string fontFileName)
		{

			Code39 c39 = new Code39();

			// Create stream....
			MemoryStream ms = new MemoryStream();
			c39.FontFamilyName="Free 3 of 9";
			c39.FontFileName= fontFileName;
            
		
			c39.FontSize=barSize;
			c39.ShowCodeString=showCodeString;
            if (!string.IsNullOrEmpty(title))
            {
                c39.Title = title;
            }

            Bitmap objBitmap=c39.GenerateBarcode(code);
			objBitmap.Save(ms, ImageFormat.Png); 
 
			//return bytes....
			return ms.GetBuffer();			
		}
    }
}
