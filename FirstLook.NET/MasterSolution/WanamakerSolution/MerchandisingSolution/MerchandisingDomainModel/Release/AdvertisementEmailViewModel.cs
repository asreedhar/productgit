﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class AdvertisementEmailViewModel
    {
        public string MakeModel { get; set; }
        public string InternetPrice { get; set; }
        public string Mileage { get; set; }
        public string Vin { get; set; }
        public string StockNumber { get; set; }
        public string EditLink { get; set; }

        public string Advertisement { get; set; }
        public string Disclaimer { get; set; }
    }
}
