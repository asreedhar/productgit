namespace FirstLook.Merchandising.DomainModel.Release
{
    public enum ThirdPartyIds
    {
        Unspecified = 0,
        
        //FROM IMT.dbo.InternetAdvertiser_ThirdPartyEntity
        AllianceMarketing = 6491,
        Auction123 = 6328,
        AutoLotManager = 7860,
        AutoRevo = 6909,
        AutoTrader = 3684,
        AutoUplink = 5034,
        Beaverton = 6480,
        CarsDotCom = 3685,
        CDMData = 6518,
        Damson = 7767,
        DealerDotCom = 7088,
        DealerFusion = 7633,
        DealerSource = 7224,
        DealerSpecialties = 6479,
        devVenture = 6919,
        DiamondLot = 7444,
        eBizAutos = 3686,
        HaselwoodEDT = 7273,
        HomeNet = 4895,
        LookAtUsedCars = 6930,
        LotBoys = 7616,
        LowBook = 6329,
        NetLook = 6561,
        Nexteppe = 7406,
        ReynoldsWS = 6931,
        TradeMotion = 7363,
        VinSolutions = 6562,
        WorldNow = 6478


    }
}
