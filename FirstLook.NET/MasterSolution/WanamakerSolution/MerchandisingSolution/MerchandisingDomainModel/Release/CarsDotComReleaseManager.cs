namespace FirstLook.Merchandising.DomainModel.Release
{
	public class CarsDotComReleaseManager : EdgeReleaseManager
	{
		public CarsDotComReleaseManager(int dealerId) : base(dealerId)
		{
		}

        public static CarsDotComReleaseManager Fetch(int dealerId)
        {
            return new CarsDotComReleaseManager(dealerId);
        }

        protected override int ThirdPartyId
        {
            get { return (int)ThirdPartyIds.CarsDotCom; }
        }


    }
}
