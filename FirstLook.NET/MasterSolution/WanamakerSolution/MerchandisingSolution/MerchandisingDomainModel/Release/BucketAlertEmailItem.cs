﻿using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class BucketAlertEmailItem
    {
        public BucketAlertEmailItem(string label, int count, WorkflowType bucketType)
        {
            Label = label;
            Count = count;
            BucketType = bucketType;
        }

        public string Label { get; set; }
        public int Count { get; set; }
        public WorkflowType BucketType { get; set; }
    }
}
