using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Data;
using IDataConnection = FirstLook.Common.Data.IDataConnection;

namespace FirstLook.Merchandising.DomainModel.Release
{
    internal static class Database
    {
        private const string MERCHANDISING_DATABASE = "Merchandising";
        private const string CHROME_DATABASE = "Merchandising";
        private const string VEHICLE_CATALOG_DATABASE = "chrome";
        private const string IMT_DATABASE = "imt";
        
        public static string MerchandisingDatabase
        {
            get { return MERCHANDISING_DATABASE; }
        }
        public static string ChromeDatabase
        {
            get { return CHROME_DATABASE; }
        }
        public static string ImtDatabase
        {
            get { return IMT_DATABASE; }
        }
        
        public static string VehicleCatalogDatabase
        {
            get { return VEHICLE_CATALOG_DATABASE; }
        }

        public static IDataConnection GetConnection(string databaseName)
        {
            return SimpleQuery.ConfigurationManagerConnection(databaseName);
        }

        public static void AddRequiredParameter(IDbCommand command, string parameterName, object parameterValue,
                                                DbType dbType)
        {
            if (parameterValue == null)
            {
                SimpleQuery.AddNullValue(command, parameterName, dbType);
            }
            else
            {
                SimpleQuery.AddWithValue(command, parameterName, parameterValue, dbType);
            }
        }

        public static void AddWithValue(IDbCommand command, string parameterName, object parameterValue, DbType dbType)
        {
            SimpleQuery.AddWithValue(command, parameterName, parameterValue, dbType);
        }

        public static void AddNullValue(IDbCommand command, string parameterName, DbType dbType)
        {
            SimpleQuery.AddNullValue(command, parameterName, dbType);
        }

        public static bool GetPossibleNull(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return false;
            }
            return 1 <= reader.GetByte(reader.GetOrdinal(column));
        }

        public static string GetNullableString(IDataRecord reader, string column)
        {
            return reader.IsDBNull(reader.GetOrdinal(column)) ? string.Empty : reader.GetString(reader.GetOrdinal(column));
        }

        public static decimal? GetNullableDecimal(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetDecimal(ord);
        }

        public static int? GetNullableInt32(IDataRecord reader, string column)
        {
            if (!reader.HasColumn(column))
                return null;

            var ord = reader.GetOrdinal(column);
            if(reader.IsDBNull(ord))
                return null;

            return reader.GetInt32(ord);
        }
    }
}