﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class EmailViewModel
    {
        public string Date { get; set; }
        public AdvertisementEmailViewModel[] Advertisements { get; set; }
    }
}
