﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Antlr.StringTemplate;
using FirstLook.Merchandising.DomainModel.Release.Tasks;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class EmailStringTemplate
    {
        private EmailViewModel _model;

        public EmailStringTemplate(EmailViewModel model)
        {
            _model = model;
        }

        public string Run()
        {
            string emailBody;
            using (TextReader textReader = new StreamReader(typeof(DailyEmailTask).Assembly.GetManifestResourceStream("FirstLook.Merchandising.DomainModel.Release.EmailTemplates.AutoApprovedAdvertisements.txt")))
            {
                var textTemplate = textReader.ReadToEnd();
              
                var template = new StringTemplate(textTemplate);
                template.SetAttribute("model", _model);
                emailBody = template.ToString();
            }

            return emailBody;
        }
    }
}
