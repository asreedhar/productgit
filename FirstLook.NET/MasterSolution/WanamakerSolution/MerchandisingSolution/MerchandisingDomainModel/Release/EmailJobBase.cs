﻿using System.Configuration;
using System.Linq;
using FirstLook.Common.Core.Utilities;
using Quartz;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public abstract class EmailJobBase : IStatefulJob
    {
        private const string EmailWhiteListKey = "email_whitelist";
        private const string BionicWhiteListKey = "bionic_email_whitelist";

        public static string GetWhiteList()
        {
            return ConfigurationManager.AppSettings[EmailWhiteListKey];
        }

        public static string GetBionicWhiteList()
        {
            return ConfigurationManager.AppSettings[BionicWhiteListKey];
        }

        public static bool CanSend(string whiteList, string email)
        {
            // emptyWhitelistAllowsAll = true, because there is no whitelist on prod.
            // whiteListSeparator = ; in email lists -- why not ,?
            return WhiteListHelper.IsInWhiteList(email, whiteList, emptyWhitelistAllowsAll: true, whiteListSeparator: ';');
        }

        public static bool CanSend(string whiteList, string[] email)
        {
            // emptyWhitelistAllowsAll = true, because there is no whitelist on prod.
            return email.All(eml => WhiteListHelper.IsInWhiteList(eml, whiteList, emptyWhitelistAllowsAll: true, whiteListSeparator: ';'));
        }

        public abstract void Execute(JobExecutionContext context);
    }
}