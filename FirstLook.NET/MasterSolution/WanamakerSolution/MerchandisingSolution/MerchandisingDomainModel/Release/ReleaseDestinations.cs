namespace FirstLook.Merchandising.DomainModel.Release
{
    public enum ReleaseDestinations
    {
        CarsDotCom = 1,
        AutoTrader = 2,
        DealerSpecialties = 3,
        DealerDotCom = 4,
        EBizAutos = 5,
        AutoUplink = 7,
        CDMData = 8
    }
}
