﻿using System.IO;
using Antlr.StringTemplate;
using FirstLook.Merchandising.DomainModel.Release.Tasks;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class BucketAlertEmailTemplate
    {
         private BucketAlertEmailViewModel _model;

        public BucketAlertEmailTemplate(BucketAlertEmailViewModel model)
        {
            _model = model;
        }

        public string Run()
        {
            string emailBody;

            using (TextReader textReader = new StreamReader(typeof(BucketAlertTask).Assembly.GetManifestResourceStream("FirstLook.Merchandising.DomainModel.Release.EmailTemplates.BucketAlert.txt")))
            {
                var textTemplate = textReader.ReadToEnd();
              
                var template = new StringTemplate(textTemplate);
                template.SetAttribute("model", _model);
                emailBody = template.ToString();
            }

            return emailBody;
        }
    }
}
