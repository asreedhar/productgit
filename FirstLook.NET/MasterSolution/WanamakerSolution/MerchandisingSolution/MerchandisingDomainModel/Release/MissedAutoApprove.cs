﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using MAX.Entities;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public static class MissedAutoApprove
    {
        public static AutoApproveInventoryMessage[] Fetch()
        {
            var messages = new List<AutoApproveInventoryMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Release.MissedAutoApprove#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            AutoApproveStatus status = (AutoApproveStatus)reader.GetInt32(reader.GetOrdinal("Status"));
                            messages.Add(new AutoApproveInventoryMessage(businessUnitId, status, WorkflowType.CreateInitialAd));
                        }
                    }
                }
            }

            return messages.ToArray();
        }
    }
}
