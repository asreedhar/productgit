using System;
using System.Data;
using System.Net;
using FirstLook.Common.Data;


namespace FirstLook.Merchandising.DomainModel.Release
{
    public class ThirdPartyCredential
    {
        /// <summary>
        /// maintained in this class for privacy reasons - prevents requiring public access to this method
        /// </summary>
        /// <param name="dealer"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        internal static NetworkCredential FetchDestinationLogin(Dealer dealer, ReleaseDestinations destination)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Release.ThirdPartyLogin#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "BusinessUnitId", dealer.Id, DbType.Int32);
                    Database.AddWithValue(cmd, "DestinationId", destination, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new NetworkCredential(
                                reader.GetString(reader.GetOrdinal("userName")),
                                reader.GetString(reader.GetOrdinal("password"))
                                );
                        }

                        throw new Exception("Login information invalid for BusinessUnitID (" +
                                                       dealer.Id + ") to Destination (" + destination + ")");
                    }
                }
            }
        }
    }
}