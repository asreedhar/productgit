﻿using System;
using System.Collections.Generic;
using System.Data;
using Core.Messages;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class LastPrice
    {
        public static int GetMaxEventId()
        {
            int eventId = -1;
            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    //replace with proc.
                    command.CommandText = "SELECT Max(AIP_EventID) as max FROM IMT.dbo.AIP_Event";

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException("Result expected");

                        eventId = reader.GetInt32(reader.GetOrdinal("max"));
                    }
                }
            }

            return eventId;
        }

        public static int GetLphMaxEventId ()
        {
            int eventId = -1;
            using ( IDataConnection connection = Database.GetConnection( Database.ImtDatabase ) )
            {
                connection.Open();
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    //replace with proc.
                    command.CommandText = "SELECT Max(ListPriceHistoryID) as max FROM IMT.dbo.Inventory_ListPriceHistory lph where exists ( select * from FLDW.dbo.InventoryActive ia where ia.InventoryID = lph.InventoryID )";

                    using ( var reader = command.ExecuteReader() )
                    {
                        if ( !reader.Read() )
                            throw new InvalidOperationException( "Result expected" );

                        eventId = reader.GetInt32( reader.GetOrdinal( "max" ) );
                    }
                }
            }

            return eventId;
        }

        public static PriceChangeMessage[] GetNewPriceChanges(int lastEventId)
        {
            var priceChange = new List<PriceChangeMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    //replace with proc.
                    command.CommandText = String.Format("SELECT AIP_EventID, I.BusinessUnitID, I.InventoryID, Notes2 as OldPrice, Value as NewPrice FROM IMT.dbo.AIP_Event PE JOIN IMT.dbo.Inventory I ON I.InventoryID = PE.InventoryID WHERE AIP_EventTypeID = 5 AND AIP_EventID > {0} ORDER BY PE.AIP_EventID DESC", lastEventId);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int eventId = reader.GetInt32(reader.GetOrdinal("AIP_EventID"));
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            int inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));

                            priceChange.Add(new PriceChangeMessage(eventId, businessUnitId, inventoryId));
                        }
                    }
                }
            }

            return priceChange.ToArray();
        }

        public static PriceChangeMessage[] GetNewLphPriceChanges (int lastEventId)
        {
            var priceChange = new List<PriceChangeMessage>();
            using ( IDataConnection connection = Database.GetConnection( Database.ImtDatabase ) )
            {
                connection.Open();
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    //replace with proc.
                    command.CommandText = String.Format("SELECT ListPriceHistoryID, I.BusinessUnitID, I.InventoryID,  LPH.ListPrice FROM IMT.dbo.Inventory_ListPriceHistory LPH JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = LPH.InventoryID WHERE  ListPriceHistoryID > {0} AND LPH.ListPrice IS NOT NULL ORDER BY LPH.ListPriceHistoryID desc", lastEventId);

                    using ( var reader = command.ExecuteReader() )
                    {
                        while ( reader.Read() )
                        {
                            int eventId = reader.GetInt32( reader.GetOrdinal( "ListPriceHistoryID" ) );
                            int businessUnitId = reader.GetInt32( reader.GetOrdinal( "BusinessUnitID" ) );
                            int inventoryId = reader.GetInt32( reader.GetOrdinal( "InventoryID" ) );

                            priceChange.Add( new PriceChangeMessage( eventId, businessUnitId, inventoryId ) );
                        }
                    }
                }
            }

            return priceChange.ToArray();
        }

        public static  PriceChangeMessage[] GetInventoryWithPriceChanges(DateTime changeBegin, DateTime changeEnd)
        {

            var priceChange = new List<PriceChangeMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetListPriceChanges";
                    command.AddParameterWithValue("@begin_time", changeBegin);
                    command.AddParameterWithValue("@end_time", changeEnd);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            int inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));

                            priceChange.Add(new PriceChangeMessage(-1, businessUnitId, inventoryId));
                        }
                    }
                }
            }

            return priceChange.ToArray();

        }
        public static InventoryMessage[] GetInventoryWithMsrpUnitCostChanges(DateTime changeBegin, DateTime changeEnd)
        {

            var inventoryChange = new List<InventoryMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "GetMsrpUnitCostChanges";

                    command.AddParameterWithValue("@begin_time", changeBegin);
                    command.AddParameterWithValue("@end_time", changeEnd);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            int inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));

                            inventoryChange.Add(new InventoryMessage(businessUnitId, inventoryId));
                        }
                    }
                }
            }

            return inventoryChange.ToArray();

        }
        public static DateTime GetDatabaseDateTime()
        {

            DateTime currentTime = DateTime.Now;

            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    //replace with proc.
                    command.CommandText = String.Format(@"
                                                SELECT currentTime = GETDATE()
                                            ");

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            currentTime = reader.GetDateTime(reader.GetOrdinal("currentTime"));
                        }
                    }
                }
            }

            return currentTime;

        }


    }
}
