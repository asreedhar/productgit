﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public static class MissedAutoLoad
    {
        public static AutoLoadInventoryMessage[] Fetch()
        {
            var messages = new List<AutoLoadInventoryMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Release.MissedAutoLoad#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            messages.Add(new AutoLoadInventoryMessage(businessUnitId, AutoLoadInventoryType.NoStatus));
                        }
                    }
                }
            }

            return messages.ToArray();
        }
    }
}
