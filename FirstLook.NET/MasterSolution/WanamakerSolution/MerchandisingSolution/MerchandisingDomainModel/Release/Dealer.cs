using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Release
{
	public class Dealer
	{
	    public Dealer(int id, string name, List<ReleaseDestination> destinations)
		{
			Id = id;
			Name = name;
			Destinations = destinations;
		}


	    public int Id { get; private set; }

	    public string Name { get; private set; }


	    internal Dealer(IDataRecord reader)
        {
            Id = reader.GetInt32(reader.GetOrdinal("dealerId"));
            Name = reader.GetString(reader.GetOrdinal("dealerName"));
            Destinations = new List<ReleaseDestination>();
        }

	    public List<ReleaseDestination> Destinations { get; private set; }

	    public void AddDestination(ReleaseDestination destination)
        {
            Destinations.Add(destination);
        }

        public IList<ApprovedInventory> GetApprovedInventory(AdvertisementStatus status)
        {
            var approvedInventory = new List<ApprovedInventory>();
            
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.DealerApprovedInventory#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "AdvertisementStatus", (int) status, DbType.Int32);
                    Database.AddWithValue(cmd, "BusinessUnitID", Id, DbType.Int32);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int inventoryID = reader.GetInt32(reader.GetOrdinal("InventoryID"));
                            string approvedUser = reader.GetString(reader.GetOrdinal("ApprovedByMemberLogin"));
                            approvedInventory.Add(new ApprovedInventory() {InventoryID = inventoryID, ApprovalUser = approvedUser} );
                        }
                    }
                }
            }

            return approvedInventory;
        }

		public static IEnumerable<Dealer> GetDealers(AdvertisementStatus status)
		{
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.DealerWithAdvertisements#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "AdvertisementStatus", (int)status, DbType.Int32);

                    Dictionary<int, Dealer> adsDict = new Dictionary<int, Dealer>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Dealer dealer = new Dealer(reader);
                            adsDict.Add(dealer.Id, dealer);
                        }

                        return adsDict.Values;
                    }
                }
            }
		}

        public static IEnumerable<int> GetDealerIdsForDistributionProviderId(ReleaseDestinations providerId)
        {
            List<int> dealers = new List<int>();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.DealerIdsForDistributionProviderId#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "ProviderId", (int) providerId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("businessUnitId"));
                            dealers.Add(id);
                        }

                        reader.NextResult();
                                                
                    }
                }
            }

            return dealers;
        }
	}
}