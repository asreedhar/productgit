﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Core.Messages;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public static class LastBookout
    {
        public static int GetMaxBookoutId()
        {
            int maxBookoutId = -1;
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Release.MaxBookoutId";

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException("Result expected");

                        maxBookoutId = reader.GetInt32(reader.GetOrdinal("max"));
                    }
                }
            }

            return maxBookoutId;
        }

        public static NewBookoutMessage[] GetNewBookout(int lastBookoutId)
        {
            var newBookouts = new List<NewBookoutMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Release.GetLatestBookouts";

                    Database.AddRequiredParameter(command, "BookoutId", lastBookoutId, DbType.Int32);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int bookoutId = reader.GetInt32(reader.GetOrdinal("BookoutID"));
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            int inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));
                            newBookouts.Add(new NewBookoutMessage(bookoutId, businessUnitId, inventoryId));
                        }
                    }
                }
            }

            return newBookouts.ToArray();
        }
    }
}
