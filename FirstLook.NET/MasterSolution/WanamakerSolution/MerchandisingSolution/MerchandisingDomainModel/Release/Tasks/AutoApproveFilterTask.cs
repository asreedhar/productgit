﻿using Core.Messages;
using Core.Messaging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
    /// <summary>
    /// Issue auto-approve messages in response to new inventory, price changes, style changes.
    /// </summary>
    internal class AutoApproveFilterTask : TaskRunner<InventoryMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IAdMessageSender _messageSender;

        private const string User = "max_user";

        public AutoApproveFilterTask(IQueueFactory queueFactory, IAdMessageSender messageSender)
            : base(queueFactory.CreateInventoryChanged())
        {
            //subscribe to the buses
            var inventoryBus = BusFactory.CreateNewInventory<NewInventoryMessage, InventoryMessage>();
            inventoryBus.Subscribe(ReadQueue);

            var priceBus = BusFactory.CreatePriceChange<PriceChangeMessage, InventoryMessage>();
            priceBus.Subscribe(ReadQueue);

            var styleSetBus = BusFactory.CreateStyleSet<StyleSetMessage, InventoryMessage>();
            styleSetBus.Subscribe(ReadQueue);

            _messageSender = messageSender;
        }

        public override void Process(InventoryMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
            Log.DebugFormat("Inventory Changed: BU = {0}, InvID = {1}", message.BusinessUnitId, message.InventoryId);

            var businessUnit = BusinessUnitFinder.Instance().Find(message.BusinessUnitId);
            var settings = GetAutoApproveSettings.GetSettings(message.BusinessUnitId);

            //If dealer doesn't have pre approve settings, they are off, or doesnt have the upgrade we return
            if (!settings.HasSettings || settings.Status == AutoApproveStatus.Off ||
                !businessUnit.HasDealerUpgrade(Upgrade.Merchandising))
                return;

            Log.DebugFormat("Inventory Changed: Auto Approve valid vehicle: BU = {0}, InvID = {1}",
                message.BusinessUnitId, message.InventoryId);
            bool bSendMessage = false;
            if (settings.Status == AutoApproveStatus.OnForAll)
            {
                bSendMessage = true;
            }
            else
            {
                //see if car is new or used
                var invData = InventoryData.Fetch(message.BusinessUnitId, message.InventoryId);

                if (!invData.IsNew() && settings.Status == AutoApproveStatus.OnForUsed)
                    bSendMessage = true;
                else if (invData.IsNew() && settings.Status == AutoApproveStatus.OnForNew)
                    bSendMessage = true;
            }

            if (bSendMessage)
            {
                _messageSender.SendAutoApproval(new AutoApproveMessage(message.BusinessUnitId, message.InventoryId, User), GetType().FullName);
            }

        }
    }
}
