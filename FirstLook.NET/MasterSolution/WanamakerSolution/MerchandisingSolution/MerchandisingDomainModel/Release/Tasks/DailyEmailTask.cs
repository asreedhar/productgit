﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using Core.Messaging;
using FirstLook.Common.Core.Command;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
    internal class DailyEmailTask : TaskRunner<DailyEmailMessage>
    {
        private IEmail _emailProvider;

        public DailyEmailTask(IEmailQueueFactory factory, IEmail emailProvider)
            : base(factory.CreateEmail())
        {
            _emailProvider = emailProvider;
        }

        public override void Process(DailyEmailMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, Email: {1}", message.BusinessUnitId, message.Email);
            //get list of autoapproved inventoryIDs
            var inventoryCommand = new AutoApproveActivityInventory(message.BusinessUnitId);
            AbstractCommand.DoRun(inventoryCommand);

            if (inventoryCommand.InventoryIDs.Length > 0) //only send email if cars are to be reviewed
            {
                var adList = new List<AdvertisementEmailViewModel>();

                AdvertisementConverter converter = new AdvertisementConverter();
                foreach (var inventoryid in inventoryCommand.InventoryIDs)
                {
                    string editLink = string.Format("{0}Workflow/{1}?bu={2}&inv={3}",
                                                    ConfigurationManager.AppSettings["merchandising_host_path"],
                                                    "RedirectToInventoryItem.aspx", message.BusinessUnitId, inventoryid);
                    var inventoryData = InventoryData.Fetch(message.BusinessUnitId, inventoryid);

                    var adModel = new AdvertisementEmailViewModel()
                                      {
                                          InternetPrice = string.Format("{0:c0}", inventoryData.ListPrice),
                                          MakeModel = inventoryData.YearMakeModel + " " + inventoryData.Trim,
                                          Mileage = inventoryData.DisplayMileage,
                                          Vin = inventoryData.VIN,
                                          StockNumber = inventoryData.StockNumber,
                                          EditLink = editLink
                                      };

                    var description = MerchandisingDescription.Fetch(message.BusinessUnitId, inventoryid);
                    adModel.Advertisement = converter.ToHtml(description.AdvertisementModel);
                    adModel.Disclaimer = description.Footer;
                    adList.Add(adModel);
                }

                var emailModel = new EmailViewModel() {Advertisements = adList.ToArray()};
                emailModel.Date = DateTime.Now.ToString();

                var emailTemplate = new EmailStringTemplate(emailModel);
                var emailBody = emailTemplate.Run();

                var businessUnit = BusinessUnitFinder.Instance().Find(message.BusinessUnitId);

                MailMessage emailMessage = new MailMessage("helpdesk@maxdigital.com", message.Email);
                emailMessage.IsBodyHtml = true;
                emailMessage.Subject = "Daily Auto-Approve for " + businessUnit.ShortName;
                emailMessage.Body = emailBody;

                try
                {
                    _emailProvider.SendEmail(emailMessage);
                    AutoApproveActivity.SetBounced(message.BusinessUnitId, false);
                }
                catch (Exception e)
                {
                    if (_emailProvider.IsHardBounce(e))
                        AutoApproveActivity.SetBounced(message.BusinessUnitId, true);
                    else
                        throw;
                }
                
            }
        }
    }
}
