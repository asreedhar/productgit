﻿using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using MAX.Entities;
using MAX.Entities.Messages;
using Merchandising.Messages;
using Merchandising.Messages.BucketAlerts;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
    internal class BucketValueTask : TaskRunner<BucketValueMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly INoSqlDbFactory _noSqlDbFactory;

        public BucketValueTask(IEmailQueueFactory factory, INoSqlDbFactory noSqlDbFactory)
            : base(factory.CreateBucketValueQueue())
        {
            _noSqlDbFactory = noSqlDbFactory;
        }

        public override void Process(BucketValueMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, WorkflowType: {1}", message.BusinessUnitId, message.Bucket);
            Log.Debug("BucketValueTask Processing - Begin");

            var bucketValue = (new BucketValue()).SetFromMessage(message);

            var dbInterface = _noSqlDbFactory.GetBucketValueDb();
            dbInterface.Write(bucketValue);

            Log.Debug("BucketValueTask Processing - End");
        }
    }
}
