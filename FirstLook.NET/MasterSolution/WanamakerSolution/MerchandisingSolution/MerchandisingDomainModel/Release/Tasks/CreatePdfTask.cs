﻿using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.Pdf;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages;
using Core.Messaging;
using Merchandising.Messages.AutoApprove;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
   
    internal class CreatePdfTask : TaskRunner<CreatePdfMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IMarketListingSearchAdapter _searchAdapter;
        private ILogger _logger;

        public CreatePdfTask(IQueueFactory factory, IMarketListingSearchAdapter searchAdapter, ILogger logger)
            : base(factory.CreatePdf())
        {
            _searchAdapter = searchAdapter;
            _logger = logger;
        }

        public override void Process(CreatePdfMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
	        InventoryData inventory;
			//try
			//{
		        inventory = InventoryData.Fetch(message.BusinessUnitId, message.InventoryId);
			//}
			//catch (InventoryException iex)
			//{
			//    Log.Error("CreatePdf Inventory not found", iex);
			//    return; // Stop cycling on inactive inventory
			//}
			
            if(inventory.IsNew()) //only do pdf's for used cars.
                return;

            var businessUnit = BusinessUnitFinder.Instance().Find(message.BusinessUnitId);
            if (businessUnit.HasDealerUpgrade(Upgrade.Marketing) && businessUnit.HasDealerUpgrade(Upgrade.WebSitePDF))
            {
                Log.DebugFormat("Publishing PDF for BusinessUnitID: {0}, InventoryID: {1}, userName: {2}", message.BusinessUnitId, message.InventoryId, message.UserName);
                var publisher = new WebSitePdfPublisher(PricingContext.FromMerchandising(message.BusinessUnitId, message.InventoryId), _logger,
                                                _searchAdapter,
                                                businessUnit.HasDealerUpgrade(Upgrade.WebSitePDFMarketListings),
                                                message.UserName);

                var publishedUrl = publisher.Publish();
				Log.InfoFormat("CreatePdf Completed with publishedUrl = {0}", publishedUrl);
            }
        }
    }
}
