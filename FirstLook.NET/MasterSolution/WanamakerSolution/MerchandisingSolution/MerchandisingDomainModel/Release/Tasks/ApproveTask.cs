﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Transactions;
using System.Web.Services.Protocols;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Distribution.WebService.Client.Distributor;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Microsoft.JScript;
using Convert = System.Convert;
using VehicleType = FirstLook.Distribution.WebService.Client.Distributor.VehicleType;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
    internal class ApproveTask : TaskRunner<ApproveMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IAdMessageSender _messageSender;
       
        public ApproveTask(IQueueFactory queueFactory, IAdMessageSender messageSender)
            : base(queueFactory.CreateApprove())
        {
            _messageSender = messageSender;
        }

        public override void Process(ApproveMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
            Log.DebugFormat("Releasing Ad: BU = {0}, InvID = {1}", message.BusinessUnitId, message.InventoryId);
            RunReleaseOperations(message.BusinessUnitId, message.InventoryId, message.UserName);
        }

        private void RunReleaseOperations(int businessUnitId, int inventoryId, string approvalUser)
        {
            Exception exception = null;
            
            IReleaseManager mgr = ReleaseManagerFactory.GetReleaseManager(businessUnitId, 1);
            var advert = Advertisement.GetAdvertisement(businessUnitId, inventoryId);

            MerchandisingDescription merchandisingDescription = null;
            try
            {
                if (ConfigurationManager.AppSettings["EnableDTC"] == "1")
                {
                    // Release the ad in an ambient DTC transaction
                    using (TransactionScope txn = new TransactionScope())
                    {
                        merchandisingDescription = ReleaseAdvertisement(businessUnitId, inventoryId, mgr, advert);
                        txn.Complete();
                    }
                }
                else
                {
                    merchandisingDescription = ReleaseAdvertisement(businessUnitId, inventoryId, mgr, advert);
                }
            }
            catch (Exception e)
            {
                exception = e;
                Log.Error(e);
                advert.Failure(e);
            }

            if (exception != null) //requeue message to try again until error limit is reached
                throw exception;

            if (merchandisingDescription != null)
            {
                bool bugzid24049 = false;
                if (advert.VehicleIsNew) // do not call distribution API when Auto Approving new cars sent to eBiz                            
                {
                    var ebizUsers = new HashSet<int>(Dealer.GetDealerIdsForDistributionProviderId(ReleaseDestinations.EBizAutos));
                    bugzid24049 = ebizUsers.Contains(businessUnitId);
                }

                if (ConfigurationManager.AppSettings["CallDistributionWebService"] == "1" && !bugzid24049)
                {
                    DoDistributorWebServiceIntegration(merchandisingDescription, advert);
                    Log.DebugFormat("DistributorWebServiceIntegration() called with description '{0}' and ad id '{1}'",
                        merchandisingDescription, advert.Id);
                }
            }

            //now, check for expired adverts
            foreach (int expiredInventoryId in Advertisement.GetExpiredAds(businessUnitId))
            {
                Log.DebugFormat("Ad {0} is expired at dealer {1}. Generating new Ad.", expiredInventoryId, businessUnitId);

                //NOTE:  The mediator needs a memberLogin that has access to Carfax reports - probably the last approver of the ad is most appropriate
                TemplateMediator mediator = new TemplateMediator(businessUnitId, expiredInventoryId, string.Empty);
                MerchandisingDescription newDesc = mediator.GenerateDescription();
                newDesc.Save(businessUnitId, expiredInventoryId, EventLogHelper.SERVICE_NAME);
                StepStatusCollection
                    .Fetch(businessUnitId, expiredInventoryId)
                    .SetPostingStatus(AdvertisementPostingStatus.PendingApproval)
                    .Save(EventLogHelper.SERVICE_NAME);
            }

            //Create the Pdf
            _messageSender.SendCreatePdfMessage(businessUnitId, inventoryId, approvalUser);
        }

        private static MerchandisingDescription ReleaseAdvertisement(int businessUnitID, int inventoryId, IReleaseManager mgr, Advertisement advert)
        {
            //Advertisement(object) invariant
            MerchandisingDescription desc = null;

            //store all the inventoryIds released for this Dealer-ReleaseDestination

            //get latest description...
            desc = MerchandisingDescription.Fetch(businessUnitID, inventoryId);

            //this is an unfortunate problem due to the way we are publishing and scheduling today.  If a user
            //publishes an item for release in the future, then saves a draft in the system, we do not save these
            //as separate descriptions.  The latter actually overwrites the former.  Hence, I store the draft info in case it has changed
            //then I fill the description item (which contains possibly erroneous metadata now), with the released advert, footer, and expiry
            //finally, I publish to accelerator, where the new "acceleratorID" is set on the description
            //I replace the draft info (so it will remain in wanamaker) and save

            string draftDesc = desc.Description;
            string draftFooter = desc.Footer;
            DateTime? draftExp = desc.ExpiresOn;

            desc.Description = advert.Text;
            desc.Footer = advert.Footer;
            desc.ExpiresOn = advert.ExpiresOn;

            //update with advert text

            Log.Debug("Saving to Market.Merchandising tables and IMT.InternetAdvertisement.");
            desc.Publish(businessUnitID, advert.InventoryId, advert.ApprovedByMemberLogin);
            Log.DebugFormat("MerchandisingDescription {0} published for dealer {1} inventory {2}, approved by {3}",
                desc.AdvertisementId, businessUnitID, advert.InventoryId, advert.ApprovedByMemberLogin);

            desc.Description = draftDesc;
            desc.Footer = draftFooter;
            desc.ExpiresOn = draftExp;

            // update the merchandising database
            Log.Debug("Saving to Merchandising");
            desc.Save(businessUnitID, advert.InventoryId, advert.ApprovedByMemberLogin);

            Log.DebugFormat("MerchandisingDescription {0} saved for dealer {1} inventory {2}, approved by {3}",
                desc.AdvertisementId, businessUnitID, advert.InventoryId, advert.ApprovedByMemberLogin);

            // releasing to firstlook
            Log.Debug("");
            mgr.Release(advert);
            advert.Success();
            Log.Debug("Ad relased and ad.Success() called.");

            return desc;
        }

        private static void DoDistributorWebServiceIntegration(MerchandisingDescription desc, Advertisement theAd)
        {
            Log.Debug("Preparing message for distributor web service");

            Distribution.WebService.Client.Distributor.Dealer dealer = new Distribution.WebService.Client.Distributor.Dealer();
            dealer.Id = desc.BusinessUnitId;

            Vehicle vehicle = new Vehicle();
            vehicle.Id = theAd.InventoryId;
            vehicle.VehicleType = VehicleType.Inventory;
            Log.DebugFormat("VehicleId {0}, VehicleType {1}", vehicle.Id, vehicle.VehicleType);

            TextContent mainAd = new TextContent();
            mainAd.Text = desc.Description + Environment.NewLine + desc.Footer;
            Log.DebugFormat("MainAd contents '{0}'", mainAd.Text);

            TextContent htmlAd = new TextContent();
            htmlAd.Text = theAd.HtmlDescription;
            Log.DebugFormat("HtmlDescription contents '{0}'", htmlAd.Text);

            Content[] contents = new Content[] { mainAd, htmlAd };
            Distribution.WebService.Client.Distributor.Advertisement advertisement =
                new Distribution.WebService.Client.Distributor.Advertisement();
            advertisement.Contents = contents;

            //description cannot be sent alone, must always send price and description or just the price.
            Price price = new Price();
            price.PriceType = PriceType.Internet;
            decimal adPrice = theAd.SpecialPrice ?? theAd.Price;
            price.Value = Convert.ToInt32(adPrice);
            Log.DebugFormat("Price is {0} and priceType is {1}", price.Value, price.PriceType);

            BodyPart[] bodyParts = new BodyPart[] { price, advertisement };
            MessageBody body = new MessageBody();
            body.Parts = bodyParts;

            Message message = new Message();
            message.From = dealer;
            message.Subject = vehicle;
            message.Body = body;

            Log.DebugFormat("Message From: {0}, Subject: {1}, Body: price and advertisement", dealer.Id, vehicle.Id);

            try
            {
                Distributor distributor = new Distributor(); //url set by proper App-*.config file.
                string distributorUrl = ConfigurationManager.AppSettings["DistributorWebService_Uri"];

                if (distributorUrl != null)
                {
                    distributor.Url = distributorUrl;
                }
                distributor.Distribute(message);
                Log.DebugFormat("Disributor sent message to url '{0}'", distributorUrl);
            }
            catch (SoapException e)
            {
                Log.Error("Soap Exception: Could not update Advertisement through Distributor web service!", e);
                EventLogHelper.LogError(e, desc.BusinessUnitId, true);
                throw new ApplicationException(
                    string.Format(
                        "SoapException: Could not update Advertisement through Distributor web service! Error:{0}",
                        e.Message));
            }
            catch (WebException e)
            {
                Log.Error("Web Exception: Could not update Advertisement through Distributor web service!", e);
                EventLogHelper.LogError(e, desc.BusinessUnitId, true);
                throw new ApplicationException(
                    string.Format(
                        "WebException: Could not update Advertisement through Distributor web service! Error:{0}",
                        e.Message));
            }
            catch (Exception e)  
            {
                Log.Error("Exception: Could not update Advertisement through Distributor web service!", e);
                throw new ApplicationException(
                    string.Format(
                        "Exception: Could not update Advertisement through Distributor web service! Error:{0}",
                        e.Message));
            }

        }
    }
}
