﻿using System;
using System.Reflection;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
    internal class AutoApproveTask : TaskRunner<AutoApproveMessage>
    {
        private readonly IDiscountPricingCampaignManager _discountPricingCampaignManager;
        private readonly IAdMessageSender _messageSender;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        
        public AutoApproveTask(IQueueFactory factory, IDiscountPricingCampaignManager discountPricingCampaignManager, IAdMessageSender messageSender)
            : base(factory.CreateAutoApprove())
        {
            _discountPricingCampaignManager = discountPricingCampaignManager;
            _messageSender = messageSender;

        }

        public override void Process(AutoApproveMessage message)
        {
            Log.DebugFormat("Auto Generating Advertisement: BU = {0}, InvID = {1}", message.BusinessUnitId,
                message.InventoryId);

            var mediator = new TemplateMediator(message.BusinessUnitId, message.InventoryId, message.UserName);
            if (mediator.VehicleData.VehicleConfig.ChromeStyleID == -1)
            {
                //no chrome style id. We don't generate ad advertisement.
                return;
	    }
	    
            var desc = mediator.GenerateDescription();
            desc.Save(message.BusinessUnitId, message.InventoryId, message.UserName);

            var releaseDate = DateTime.Now;

            // Find out if there are any active discounts for this inventory
            int? manufacturerRebate = null;
            int? dealerDiscount = null;

            var discountCampaign = _discountPricingCampaignManager.FetchCampaignForInventory(
                message.BusinessUnitId, message.InventoryId);

            // Only negative MSRP discounts need to have the rebate and incentive fields set.
            if (discountCampaign != null && discountCampaign.DiscountType == PriceBaseLine.Msrp)
            {
                manufacturerRebate = (discountCampaign.ManufacturerRebate < 0)
                    ? Convert.ToInt32(discountCampaign.ManufacturerRebate)
                    : (int?) null;
                dealerDiscount = (discountCampaign.DealerDiscount < 0)
                    ? Convert.ToInt32(discountCampaign.DealerDiscount)
                    : (int?) null;
            }

            //Update Posting.VehicleAdScheduling
            ApproveForRelease.Run(
                message.BusinessUnitId, message.InventoryId,
                releaseDate, message.UserName,
                desc.Description, desc.Footer,
                mediator.VehicleData.VehicleConfig.VehicleOptions.GetOrderedDescriptionText(message.BusinessUnitId),
                mediator.VehicleData.InventoryItem.FlListPrice,
                mediator.VehicleData.InventoryItem.SpecialPrice,
                desc.ExpiresOn,
                mediator.VehicleData.VehicleConfig.GetExteriorColorDescription(),
                mediator.VehicleData.VehicleConfig.InteriorColor,
                mediator.VehicleData.VehicleConfig.InteriorType,
                AutoRevoMapper.Instance().ExecuteMapping(message.BusinessUnitId, mediator.VehicleData),
                mediator.VehicleData.VehicleConfig.VehicleOptions.options.GetOptionCodes(","),
                true,
                Convert.ToInt32(mediator.VehicleData.InventoryItem.MSRP),
                manufacturerRebate,
                dealerDiscount
                );

            _messageSender.SendApproval(new ApproveMessage(message.BusinessUnitId, message.InventoryId, message.UserName),
                GetType().FullName);

            _messageSender.PublishMaxDocumentPublishMessage(mediator.VehicleData.InventoryItem.VIN, message.BusinessUnitId, message.InventoryId, "AutoApproveTask");
        }
    }
}
