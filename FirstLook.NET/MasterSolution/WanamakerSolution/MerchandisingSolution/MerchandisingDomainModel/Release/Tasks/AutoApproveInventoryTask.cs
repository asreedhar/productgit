﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
    /// <summary>
    /// Issue auto-approve messages for all of a dealer's new and/or used inventory.
    /// </summary>
    internal class AutoApproveInventoryTask : TaskRunner<AutoApproveInventoryMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IAdMessageSender _messageSender;

        private const string User = "max_user";

        public AutoApproveInventoryTask(IQueueFactory queueFactory, IAdMessageSender messageSender)
            : base(queueFactory.CreateAutoApproveInventory())
        {
            _messageSender = messageSender;
        }

        private void SendApprovalMessage(List<InventoryData> inventorydata, int businessUnit, int newUsed, WorkflowType workflow)
        {
            Bin bin = Bin.Create(workflow, newUsed);
            List<InventoryData> inventoryDataList = InventoryDataFilter.WorkflowSearch(businessUnit, inventorydata, 0, 10000, String.Empty, bin);
            var inventoryIdList = from x in inventoryDataList select x.InventoryID;
            _messageSender.SendAutoApproval(inventoryIdList, businessUnit, User, GetType().FullName);
        }

        public override void Process(AutoApproveInventoryMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, WorkflowType: {1}", message.BusinessUnitId, message.Workflow);
            Log.DebugFormat("Auto Approving --Ad Approval Needed--: BU = {0}", message.BusinessUnitId);

            var list = new List<InventoryData>();
            if (message.Status != AutoApproveStatus.Off)
                list = InventoryDataSource.FetchList(message.BusinessUnitId);

            if (message.Status == AutoApproveStatus.OnForUsed || message.Status == AutoApproveStatus.OnForAll)
                //used or both
                SendApprovalMessage(list, message.BusinessUnitId, InventoryData.UsedVehicle, message.Workflow);

            if (message.Status == AutoApproveStatus.OnForNew || message.Status == AutoApproveStatus.OnForAll)
                //new or both
                SendApprovalMessage(list, message.BusinessUnitId, InventoryData.NewVehicle, message.Workflow);
        }
    }
}
