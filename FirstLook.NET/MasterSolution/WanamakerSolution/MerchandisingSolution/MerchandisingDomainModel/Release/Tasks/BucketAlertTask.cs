﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using Core.Messaging;
using FirstLook.Common.Core.Command;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports.VehiclesOnline;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;
using MAX.Entities.Messages;
using Merchandising.Messages;
using Merchandising.Messages.BucketAlerts;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.Release.Tasks
{
    internal class BucketAlertTask : TaskRunner<BucketAlertMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IEmail _emailProvider;
        private readonly IDashboardRepository _dashboardRepository;
        private readonly INoSqlDbFactory _noSqlDbFactory;
        private readonly IAdMessageSender _messageSender;

        private readonly IQueue<BucketValueMessage> _bucketValueQueue;

        public BucketAlertTask(IEmailQueueFactory factory, IEmail emailProvider, IDashboardRepository dashboardRepository, INoSqlDbFactory noSqlDbFactory, IAdMessageSender messageSender)
            : base(factory.CreateBucketAlert())
        {
            _emailProvider = emailProvider;
            _dashboardRepository = dashboardRepository;
            _noSqlDbFactory = noSqlDbFactory;
            _messageSender = messageSender;

            _bucketValueQueue = factory.CreateBucketValueQueue();
        }

        public override void Process(BucketAlertMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, SendDaily: {1}, SendWeekly: {2}", message.BusinessUnitId, message.SendDaily, message.SendWeekly);
            Log.Debug("BucketAlertTask Processing - Begin");

            // the message must be daily or weekly. Log and return so that message can be deleted from the queue.
            if (!message.SendDaily && !message.SendWeekly)
            {
                Log.Error("Daily or Weekly must be specified for Bucket Alerts");
                return;
            }
            // if both are set, deactivate weekly, because we can only do one at a time
            if (message.SendDaily && message.SendWeekly)
                message.SendWeekly = false;

            var businessUnit = GetBusinessUnit(message.BusinessUnitId);
            var bucketValues = GetBucketValues(businessUnit.Id, businessUnit.Name, message.WriteToDb);
            var alertCommand = RetrieveBucketAlerts(businessUnit.Id); //Fetch preferences

            if (!alertCommand.Active) return; // if the dealer is an active and valid dealer, but does not have thresholds set up, just return

            var model = new BucketAlertEmailViewModel(businessUnit.Name);

            var invalidCredentials = GetCredentials(businessUnit.Id);

            foreach (var threshold in alertCommand.Thresholds)
            {
                if (invalidCredentials && (threshold.Bucket == WorkflowType.NotOnline || threshold.Bucket == WorkflowType.LowActivity))
                    continue;

                var bucket = bucketValues.FirstOrDefault(b => b.Bucket == threshold.Bucket);
                if (bucket == null)
                    continue;

                var description = bucket.Bucket == WorkflowType.NotOnline ? "No AD Online" : bucket.BucketDescription;
                var bucketCount = 0;

                if (alertCommand.NewUsed == VehicleType.Both || alertCommand.NewUsed == VehicleType.New)
                    bucketCount += bucket.NewCount;
                if (alertCommand.NewUsed == VehicleType.Both || alertCommand.NewUsed == VehicleType.Used)
                    bucketCount += bucket.UsedCount;

                if (bucketCount >= threshold.Limit && threshold.EmailActive)
                    model.Add(new BucketAlertEmailItem(description, bucketCount, threshold.Bucket));
            }

            // get the totals to be displayed in the email
            model.LowQualityCount = _dashboardRepository.GetBucketTotal(businessUnit.Id, alertCommand.NewUsed, new HashSet<WorkflowType>(model.LowQuality.Select(x => x.BucketType)));
            model.OnlineGapCount = _dashboardRepository.GetBucketTotal(businessUnit.Id, alertCommand.NewUsed, new HashSet<WorkflowType>(model.OnlineGaps.Select(x => x.BucketType)));

            #region Send an email if there are alerts
            if (model.HasLowQuality || model.HasOnlineGaps)
            {
                // get the links for the email
                var dashboardLink = string.Format("/merchandising/Dashboard.aspx?usedOrNewFilter={0}", (VehicleType)alertCommand.NewUsed);
                var dashEntry = new RedirectEntry(businessUnit.Id, dashboardLink);
                model.OnlineGapLink = string.Format("{0}Workflow/{1}?link={2}", ConfigurationManager.AppSettings["merchandising_host_path"], "Link.aspx", dashEntry.ToToken());

                var inventoryLink = string.Format("/merchandising/Workflow/Inventory.aspx?usednew={0}&filter=LowAdQualityAll", alertCommand.NewUsed);
                var inventoryEntry = new RedirectEntry(businessUnit.Id, inventoryLink);
                model.LowQualityLink = string.Format("{0}Workflow/{1}?link={2}", ConfigurationManager.AppSettings["merchandising_host_path"], "Link.aspx", inventoryEntry.ToToken());

                var settingsEntry = new RedirectEntry(businessUnit.Id, "/merchandising/CustomizeMAX/DealerPreferences.aspx");
                model.SettingsLink = string.Format("{0}Workflow/{1}?link={2}", ConfigurationManager.AppSettings["merchandising_host_path"], "Link.aspx", settingsEntry.ToToken());

                // save current bounced status for each
                var dailyBounced = alertCommand.EmailBounced;
                var weeklyBounced = alertCommand.WeeklyEmailBounced;

                var emailTemplate = new BucketAlertEmailTemplate(model);
                var emailBody = emailTemplate.Run();
                var emailToList = message.SendWeekly ? alertCommand.WeeklyEmails : alertCommand.Emails;
                var whiteList = EmailJobBase.GetWhiteList();

                if (EmailJobBase.CanSend(whiteList, emailToList))
                {
                    try
                    {
                        var emailMessage = new MailMessage
                        {
                            From = new MailAddress("helpdesk@maxdigital.com"),
                            IsBodyHtml = true,
                            Subject = "Digital Inventory Quality Analysis " + businessUnit.ShortName,
                            Body = emailBody
                        };

                        if(!emailToList.Any()) throw new ArgumentNullException(); // trap for when the emailToList is empty
                        Array.ForEach(emailToList, x => emailMessage.To.Add(x));
                        

                        _emailProvider.SendEmail(emailMessage);

                        weeklyBounced = !message.SendWeekly && weeklyBounced;
                        dailyBounced = !message.SendDaily && dailyBounced;
                    }
                    catch (ArgumentNullException argumentNullException)
                    {
                        // eat the message because the email address is null
                        weeklyBounced = message.SendWeekly || weeklyBounced;
                        dailyBounced = message.SendDaily || dailyBounced;

                        Log.Debug("Argument Null Exception", (Exception)argumentNullException);
                    }
                    catch (ArgumentException argumentException)
                    {
                        // eat the message because the email address is an empty string
                        weeklyBounced = message.SendWeekly || weeklyBounced;
                        dailyBounced = message.SendDaily || dailyBounced;

                        Log.Debug("Argument Exception", (Exception)argumentException);
                    }
                    catch (FormatException formatException)
                    {
                        // eat the message because the email address is invalid or not supported
                        weeklyBounced = message.SendWeekly || weeklyBounced;
                        dailyBounced = message.SendDaily || dailyBounced;

                        Log.Debug("Format Exception", (Exception)formatException);
                    }
                    catch (Exception e)
                    {
                        if (_emailProvider.IsHardBounce(e))
                        {
                            // eat the message because one of the email addresses has been bounced by Amazon
                            weeklyBounced = message.SendWeekly || weeklyBounced;
                            dailyBounced = message.SendDaily || dailyBounced;

                            Log.Debug("AWS Hardbounced the email");
                        }
                        else
                        {
                            Log.Error("BucketAlertTask encountered an error in sending email", e);
                            message.WriteToDb = false; // prevent writing to NoSQL again

                            // return any other exceptions
                            throw;
                        }
                    }

                    // record any change to bounce activity
                    SetBounced(businessUnit.Id, dailyBounced, weeklyBounced);
                }
            }
            #endregion

            Log.Debug("BucketAlertTask Processing - End");
        }

        private List<BucketValue> GetBucketValues(Int32 businessUnitId, String name, Boolean writeToDb)
        {
            var bucketValues = new List<BucketValue>();

            var newBucketCounts = _dashboardRepository.GetBucketCounts(businessUnitId, VehicleType.New).ToList();
            var usedBucketCounts = _dashboardRepository.GetBucketCounts(businessUnitId, VehicleType.Used).ToList();
            var buckets = newBucketCounts.Select(b => new {b.BucketId, b.BucketDescription})
                                         .Concat(usedBucketCounts.Select(b => new {b.BucketId, b.BucketDescription}))
                                         .Distinct();

            foreach(var bucket in buckets)
            {
                if(bucket.BucketId == WorkflowType.None)
                    continue;

                var bucketValue = new BucketValue()
                {
                    BusinessUnitId = businessUnitId,
                    BusinessName = name,
                    Bucket = bucket.BucketId,
                    BucketDescription = bucket.BucketDescription
                };

                var newBucket = newBucketCounts.FirstOrDefault(b => b.BucketId == bucket.BucketId);
                if (newBucket != null)
                    bucketValue.NewCount = newBucket.BucketCount;

                var usedBucket = usedBucketCounts.FirstOrDefault(b => b.BucketId == bucket.BucketId);
                if (usedBucket != null)
                    bucketValue.UsedCount = usedBucket.BucketCount;

                bucketValues.Add(bucketValue);
            }

            if (writeToDb)
            {
                var dbInterface = _noSqlDbFactory.GetBucketValueDb();

                foreach (var bucketValue in bucketValues)
                {
                    if (dbInterface != null)
                        dbInterface.BeginWrite(bucketValue, EndWrite,
                            new BucketAlertTaskAsyncState {BucketValue = bucketValue, DbInterface = dbInterface});
                    else
                        _messageSender.SendBucketValueMessage(bucketValue.GetMessage(), GetType().FullName);
                }
            }

            return bucketValues;
        }

        private void EndWrite(IAsyncResult asyncResult)
        {
            BucketAlertTaskAsyncState state = null;

            try
            {
                state = (BucketAlertTaskAsyncState) asyncResult.AsyncState;
                var dbInterface = state.DbInterface;

                dbInterface.EndWrite(asyncResult);
            }
            catch (Exception ex)
            {
                if(state != null)
                    _messageSender.SendBucketValueMessage(state.BucketValue.GetMessage(), GetType().FullName);
            }
        }

        #region Virtual methods to allow for Mock Testing
        virtual public GetBucketAlerts RetrieveBucketAlerts(int businessUnitId)
        {
            var bucketAlerts = new GetBucketAlerts(businessUnitId);
            AbstractCommand.DoRun(bucketAlerts);

            return bucketAlerts;
        }

        virtual public void SetBounced(int businessUnitId, bool dailyBounced = false, bool weeklyBounced = false)
        {
            GetBucketAlerts.SetBounced(businessUnitId, dailyBounced, weeklyBounced);
        }

        virtual public BusinessUnit GetBusinessUnit(int businessUnitId)
        {
            return BusinessUnitFinder.Instance().Find(businessUnitId);
        }

        virtual public bool GetCredentials(int businessUnitId)
        {
            var dataSources = DataStatusByBusinessUnitCommand.Execute(businessUnitId);
            
            return dataSources.Any(s => s.HasInvalidCredentials || !s.HasCredentials);
        }
        #endregion
    }

    class BucketAlertTaskAsyncState
    {
        public INoSqlDb DbInterface { get; set; }
        public BucketValue BucketValue { get; set; }
    }
}
