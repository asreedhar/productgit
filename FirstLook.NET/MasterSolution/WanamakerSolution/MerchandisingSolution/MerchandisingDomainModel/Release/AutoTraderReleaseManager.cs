namespace FirstLook.Merchandising.DomainModel.Release
{
	public class AutoTraderReleaseManager : EdgeReleaseManager
	{
        public AutoTraderReleaseManager(int dealerId) : base(dealerId)
		{
		}

        public static AutoTraderReleaseManager Fetch(int dealerId)
        {
            return new AutoTraderReleaseManager(dealerId);
        }

        protected override int ThirdPartyId
        {
            get { return (int)ThirdPartyIds.AutoTrader; }
        }


    }
}
