﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class ApprovedInventory
    {
        public int InventoryID { get; set; }
        public string ApprovalUser { get; set; }
    }
}
