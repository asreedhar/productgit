using System.Data;
using FirstLook.Common.Data;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class ListingSitePreferences
    {
        internal ListingSitePreferences(IDataReader reader)
        {
            Name = reader.GetString(reader.GetOrdinal("description"));
            ListNew = reader.GetBoolean(reader.GetOrdinal("ListNew"));
            ListUsed = reader.GetBoolean(reader.GetOrdinal("ListUsed"));
            ListCertified = reader.GetBoolean(reader.GetOrdinal("ListCertified"));
            SendPhotos = reader.GetBoolean(reader.GetOrdinal("sendPhotos"));
            SendOptions = reader.GetBoolean(reader.GetOrdinal("sendOptions"));
            SendDescription = reader.GetBoolean(reader.GetOrdinal("sendDescription"));
            SendPricing = reader.GetBoolean(reader.GetOrdinal("sendPricing"));
            SendVideos = reader.GetBoolean(reader.GetOrdinal("sendVideos"));
            SendNewCars = reader.GetBoolean(reader.GetOrdinal("sendNewCars"));
            SendUsedCars = reader.GetBoolean(reader.GetOrdinal("sendUsedCars"));
            Active = reader.GetBoolean(reader.GetOrdinal("activeFlag"));
            ReleaseEnabled = reader.GetBoolean(reader.GetOrdinal("releaseEnabled"));
            CollectionEnabled = reader.GetBoolean(reader.GetOrdinal("collectionEnabled"));
            ListingSiteDealerId = Database.GetNullableString(reader, "listingSiteDealerId");
        }

        public ListingSitePreferences()
        {
            Name = "";

            ListNew = true;
            ListUsed = true;
            ListCertified = true;
        }

        public ListingSitePreferences(
            string name,
            bool listNew,
            bool listUsed,
            bool listCertified,
            bool sendPhotos,
            bool sendOptions,
            bool sendDescription,
            bool sendPricing,
            bool sendVideos,
            bool sendNewCars,
            bool sendUsedCars,
            bool active,
            bool releaseEnabled,
            bool collectionEnabled)
        {
            Name = name;
            ListNew = listNew;
            ListUsed = listUsed;
            ListCertified = listCertified;
            SendPhotos = sendPhotos;
            SendOptions = sendOptions;
            SendDescription = sendDescription;
            SendPricing = sendPricing;
            SendVideos = sendVideos;
            SendNewCars = sendNewCars;
            SendUsedCars = sendUsedCars;
            Active = active;
            ReleaseEnabled = releaseEnabled;
            CollectionEnabled = collectionEnabled;
        }

        public string ListingSiteDealerId { get; set; }

        public string Name { get; set; }

        public bool ListNew { get; set; }
        public bool ListUsed { get; set; }
        public bool ListCertified { get; set; }

        public bool SendPhotos { get; set; }

        public bool SendOptions { get; set; }

        public bool SendDescription { get; set; }

        public bool SendPricing { get; set; }

        public bool SendVideos { get; set; }

        public bool SendNewCars { get; set; }

        public bool SendUsedCars { get; set; }

        public bool Active { get; set; }

        public bool ReleaseEnabled { get; set; }

        public bool CollectionEnabled { get; set; }

        public static ListingSitePreferences Fetch( Dealer dealer, ReleaseDestinations destination )
        {
            return Fetch( dealer.Id, (int)destination );
        }
        
        public static Dictionary<int,ListingSitePreferences> FetchAll( int dealer_id, List<int> destination_list )
        {
            var result = new Dictionary<int,ListingSitePreferences>();
            foreach( var destination_id in destination_list )
                result.Add( destination_id, Fetch( dealer_id, destination_id ));
            return result;
        }

        public static ListingSitePreferences Fetch( int businessUnitID, int destinationID )
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "release.ListingSitePreferences#Fetch";
                    Database.AddWithValue(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddWithValue(cmd, "DestinationId", destinationID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.Read())
                        {
                            return new ListingSitePreferences(reader);
                        }
                        return new ListingSitePreferences();
                    }
                }
            }
        }

    }
}