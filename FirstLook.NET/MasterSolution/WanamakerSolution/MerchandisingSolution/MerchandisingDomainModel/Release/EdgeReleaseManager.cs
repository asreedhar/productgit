using System;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Release
{
	public abstract class EdgeReleaseManager : IReleaseManager
	{
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

	    protected int DealerId { get; private set; }

	    protected EdgeReleaseManager(int dealerId)
		{
			DealerId = dealerId;
		}

		#region IReleaseManager Members

		public virtual void Release(Advertisement advertisement)
		{
            // Get the MPG data - fb 30051.
		    int mpgCity, mpgHwy;
		    GetMpg(advertisement, out mpgCity, out mpgHwy);

		    //write the advertisement text and price to FirstLook IMT
            //CALL release.AdvertisementEdge#Write
            advertisement.ReleaseToFirstLook(DealerId, ThirdPartyId, mpgCity, mpgHwy);
		}

	    private void GetMpg(Advertisement advertisement, out int mpgCity, out int mpgHwy)
	    {
	        try
	        {
                var vehicleConfig = VehicleConfiguration.FetchOrCreateDetailed(DealerId, advertisement.InventoryId, String.Empty);
                var consumerInfo = ConsumerInfo.Fetch(vehicleConfig.ChromeStyleID, vehicleConfig.VehicleOptions.GetCategoryIdList(),
                    vehicleConfig.VehicleOptions.options);
                mpgCity = consumerInfo.FuelCityInt;
                mpgHwy = consumerInfo.FuelHwyInt;
	        }
	        catch (Exception ex)
	        {
	            Log.ErrorFormat("Error getting MPG data", ex);
	            mpgCity = 0;
	            mpgHwy = 0;
	        }
	    }

	    protected abstract int ThirdPartyId
        { 
            get;
        }
		#endregion
	}
}
