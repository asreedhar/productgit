using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.Release
{
    /// <summary>
    /// This class should be reconciled with Postings.Advertisement class.  They are essentially the same thing.
    /// </summary>
    public class Advertisement
    {
        public int Id { get; private set; }
        public int InventoryId { get; private set; }
        private int inventoryType;
        public string Text { get; private set; }
        public string Footer { get; private set; }
        public string EquipmentList { get; private set; }
        public decimal Price { get; private set; }
        public string StockNumber { get; private set; }
        public string ApprovedByMemberLogin { get; private set; }
        public DateTime? ExpiresOn { get; private set; }
        public string ExteriorColor { get; set; }
        public string InteriorColor { get; set; }
        public string InteriorType { get; set; }
        public string MappedOptionIds { get; set; }
        public string OptionCodes { get; set; }
        public string HighlightCalloutText { get; private set; }
        public string SeoKeywords { get; private set; }
        public string HtmlDescription { get; set; }
        public decimal? SpecialPrice { get; set; }
        public int? Msrp { get; set; }
        public int? DealerDiscount { get; set; }
        public int? ManufacturerRebate { get; set; }

        public string ExteriorColorCode { get; private set; }
        public string InteriorColorCode { get; private set; }

        public Boolean VehicleIsNew
        {
            get
            {
                return inventoryType == InventoryData.NewVehicle;
            }
        }
                        
        internal Advertisement( IDataRecord reader, int businessUnitId )
        {
            Id = reader.GetInt32(reader.GetOrdinal("advertisementId"));
            InventoryId = reader.GetInt32( reader.GetOrdinal( "inventoryId" ) );
            inventoryType = reader.GetByte( reader.GetOrdinal("inventoryType") );
            Text = reader.GetString( reader.GetOrdinal( "merchandisingDescription" ) );
            Footer = reader.GetString( reader.GetOrdinal( "footer" ) );
            EquipmentList = reader.GetString(reader.GetOrdinal("equipmentList"));
            Price = reader.GetDecimal( reader.GetOrdinal( "listPrice" ) );
            StockNumber = reader.GetString(reader.GetOrdinal("stockNumber"));
            ApprovedByMemberLogin = reader.GetString(reader.GetOrdinal("approvedByMemberLogin"));
            
            object val = reader.GetValue(reader.GetOrdinal("expiresOn"));
            if( val != DBNull.Value ) { ExpiresOn = (DateTime) val; }

            ExteriorColor = Database.GetNullableString(reader, "exteriorColor");
            InteriorColor = Database.GetNullableString(reader, "interiorColor");
            InteriorType = Database.GetNullableString(reader, "interiorType");
            MappedOptionIds = Database.GetNullableString(reader, "mappedOptionIds");
            OptionCodes = Database.GetNullableString(reader, "optionCodes");
            HighlightCalloutText = Database.GetNullableString(reader, "highlightCalloutText");
            SeoKeywords = Database.GetNullableString( reader, "seoKeywords" );
            SpecialPrice = Database.GetNullableDecimal(reader, "SpecialPrice");

            Msrp = Database.GetNullableInt32(reader, "MSRP");
            ManufacturerRebate = Database.GetNullableInt32(reader, "ManufacturerRebate");
            DealerDiscount = Database.GetNullableInt32(reader, "DealerDiscount");

            HtmlDescription = GetHtml(businessUnitId);
            SetInteriorExteriorCodes(businessUnitId);
        }

        private void SetInteriorExteriorCodes(int businessUnitId)
        {
            VehicleConfiguration vehConfig = VehicleConfiguration.FetchOrCreateDetailed(businessUnitId, InventoryId, String.Empty);

            ExteriorColorCode = vehConfig.ExteriorColorCode;
            InteriorColorCode = vehConfig.InteriorColorCode;
        }

        private string GetHtml(int businessUnitId)
        {
            string html;
            var description = MerchandisingDescription.Fetch(businessUnitId, InventoryId);
            if (description.AdvertisementModel != null)
            {
                var converter = new AdvertisementConverter();
                var footerModule = CreateFooterModule();
                description.AdvertisementModel.Children.Add(footerModule);
                html = converter.ToHtml(description.AdvertisementModel);
            }
            else //format the old way if no model
            {
                var htmlFormatter = new HtmlFormatter(Text, "<br /><br />", "<br />");
                html = htmlFormatter.GetHtmlFormat();
            }

            return html;
        }

        private Module CreateFooterModule()
        {
            var footerModule = new Module();

            var footerHeader = new Header();
            footerHeader.Children.Add( new Item { Text = Footer } );
            footerModule.Children.Add( footerHeader );

            var footerParagraph = new Paragraph();
            footerParagraph.Children.Add( new Item { Text =  string.Empty} ); //paragraph is required for the module
            footerModule.Children.Add(footerParagraph);

            return footerModule;
        }

        public static IEnumerable<int> GetExpiredAds(int businessUnitID)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.ExpiredAdvertisements#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitID, DbType.Int32);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<int> retList = new List<int>();
                        while (reader.Read())
                        {
                            retList.Add(reader.GetInt32(reader.GetOrdinal("inventoryId")));
                        }
                        return retList;
                    }
                }
            }

        }

        public static Advertisement GetAdvertisement(int businessUnitId, int inventoryId)
        {
            Advertisement ad;

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.Advertisement#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddWithValue(cmd, "InventoryId", inventoryId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if(!reader.Read())
                            throw new InvalidOperationException("Result not found");

                        ad = new Advertisement(reader, businessUnitId);                          
                    }
                }
            }

            return ad;
        }

        public static IEnumerable<Advertisement> GetAdvertisements(int businessUnitId,
                                                                   AdvertisementStatus status)
        {
            //Get all advertisements for the dealer to a given destination
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                Dictionary<int, Advertisement> adsDict = new Dictionary<int, Advertisement>();
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.Advertisements#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddWithValue(cmd, "AdvertisementStatus", (int) status, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Advertisement ad = new Advertisement(reader, businessUnitId);
                            adsDict[ad.InventoryId] = ad;
                        }
                        return adsDict.Values;
                    }
                }
            }
        }

        public void Retry()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.Advertisement#UpdateStatus";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "AdvertisementId", Id, DbType.Int32);
                    Database.AddWithValue(cmd, "AdvertisementStatus", (int)AdvertisementStatus.ApprovedForFutureRelease, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Success()
        {
            //set the scheduled advertisement to released b/c it succeeded
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.Advertisement#UpdateStatus";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "AdvertisementId", Id, DbType.Int32);
                    Database.AddWithValue(cmd, "AdvertisementStatus", (int) AdvertisementStatus.Released, DbType.Int32);


                    //set this scheduled advertisement to released
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Failure(Exception exception)
        {
            //log the posting error
            //do NOT delete the posting...
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.Advertisement#UpdateStatus";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "AdvertisementId", Id, DbType.Int32);
                    Database.AddWithValue(cmd, "AdvertisementStatus", (int) AdvertisementStatus.Error, DbType.Int32);

                    //set this ad status to error
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private static int? GetReleaseToFirstLookSqlTimeout ()
        {
            var timeoutSetting = System.Configuration.ConfigurationManager.AppSettings["ReleaseToFirstlookSqlTimeout"];
            if ( timeoutSetting == null ) return null;
            int timeout;
            if ( int.TryParse( timeoutSetting, out timeout ) ) return timeout;
            return null;
        }

        public void ReleaseToFirstLook(int dealerId, int thirdPartyId, int mpgCity = 0, int mpgHwy = 0)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    
                    cmd.CommandText = "release.AdvertisementEdge#Release";
                    // calls: Interface.Advertisement#Release
                    // inserts to: postings.Publications
                    // calls: Interface.ConditionInformation#Release 
                   
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddWithValue(cmd, "BusinessUnitId", dealerId, DbType.Int32);
                    Database.AddWithValue(cmd, "Text", (Text + " " + Footer).Trim(" ".ToCharArray()), DbType.String);
                    Database.AddWithValue(cmd, "ThirdPartyId", thirdPartyId, DbType.Int32);
                    Database.AddWithValue(cmd, "ListPrice", Price, DbType.Decimal);
                    Database.AddWithValue(cmd, "InventoryId", InventoryId, DbType.Int32);
                    Database.AddWithValue(cmd, "OptionsText", EquipmentList, DbType.String);
                    Database.AddWithValue(cmd, "ApprovedByMemberLogin", ApprovedByMemberLogin, DbType.String);
                    Database.AddWithValue(cmd, "HtmlMerchandisingDescription", HtmlDescription, DbType.String);
                    Database.AddWithValue(cmd, "EquipmentList", EquipmentList, DbType.String);

                    int? specialPrice = (SpecialPrice.HasValue) ? (int) Math.Round(SpecialPrice.Value) : new int?();
                    Database.AddRequiredParameter(cmd, "SpecialPrice", specialPrice, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "ExteriorColorCode", String.IsNullOrEmpty(ExteriorColorCode) ? null : ExteriorColorCode, DbType.String);
                    Database.AddRequiredParameter(cmd, "InteriorColorCode", String.IsNullOrEmpty(InteriorColorCode) ? null : InteriorColorCode, DbType.String);

                    // mpg - fb 30051
                    if (mpgCity > 0)
                    {
                        Database.AddWithValue(cmd, "GasMileageCity", mpgCity, DbType.Int32 );
                    }
                    if (mpgHwy > 0)
                    {
                        Database.AddWithValue(cmd, "GasMileageHighway", mpgHwy, DbType.Int32);                        
                    }


                    if (ExpiresOn.HasValue)
                    {
                        Database.AddWithValue(cmd, "ExpiresOn", ExpiresOn, DbType.DateTime);
                    }
                    if (!string.IsNullOrEmpty(ExteriorColor))
                    {
                        Database.AddWithValue(cmd, "ExteriorColor", ExteriorColor, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(InteriorColor))
                    {
                        Database.AddWithValue(cmd, "InteriorColor", InteriorColor, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(InteriorType))
                    {
                        Database.AddWithValue(cmd, "InteriorType", InteriorType, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(MappedOptionIds))
                    {
                        Database.AddWithValue(cmd, "MappedOptionIds", MappedOptionIds, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(OptionCodes))
                    {
                        Database.AddWithValue(cmd, "OptionCodes", OptionCodes, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(SeoKeywords))
                    {
                        Database.AddWithValue(cmd, "Keywords", SeoKeywords, DbType.String);
                    }
                    if (!string.IsNullOrEmpty(HighlightCalloutText))
                    {
                        Database.AddWithValue(cmd, "HighlightText", HighlightCalloutText, DbType.String);
                    }

                    if(Msrp.HasValue)
                        cmd.AddRequiredParameter("MSRP", Msrp, DbType.Int32);
                    if(ManufacturerRebate.HasValue)
                        cmd.AddRequiredParameter("ManufacturerRebate", ManufacturerRebate, DbType.Int32);
                    if(DealerDiscount.HasValue)
                        cmd.AddRequiredParameter("DealerDiscount", DealerDiscount, DbType.Int32);

                     var timeout = GetReleaseToFirstLookSqlTimeout();
                     if ( timeout.HasValue ) cmd.CommandTimeout = timeout.Value;


                    int successValue = Convert.ToInt32(cmd.ExecuteScalar());
                    if (successValue < 1)
                    {
                        throw new ApplicationException("Could not release advertisement " + Id +
                                                       " StockNumber: " + StockNumber);
                    }
                    
                }
            }
        }
    }
}