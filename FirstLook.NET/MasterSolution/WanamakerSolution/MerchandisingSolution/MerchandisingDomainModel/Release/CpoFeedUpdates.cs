using System;
using System.Collections.Generic;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Release
{
	public static class CpoFeedUpdates
	{
		public static InventoryCpoUpdate[] Fetch()
		{
			var updates = new List<InventoryCpoUpdate>();
			using (var connection = Database.GetConnection(Database.ImtDatabase))
			{
				connection.Open();
				using (var command = connection.CreateCommand())
				{
					command.CommandText = "Certified.GetInventoryInconsistentWithMfrFeed";
					command.CommandType = CommandType.StoredProcedure;

					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							var businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
							var inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));
							var make = reader.GetString(reader.GetOrdinal("Make"));
							var certifiedProgramId = reader.GetInt32(reader.GetOrdinal("CertifiedProgramID"));
							var currentOEMCertifiedStatus = Convert.ToBoolean(reader.GetByte(reader.GetOrdinal("CurrentOEMCertificationStatus")));
							updates.Add(new InventoryCpoUpdate(businessUnitId, inventoryId, make, certifiedProgramId, currentOEMCertifiedStatus));
						}
					}
				}
			}
			return updates.ToArray();
		}

		public class InventoryCpoUpdate
		{
			public InventoryCpoUpdate(int businessUnitId, int inventoryId, string make, int certifiedProgramId, bool currentOEMCertifiedStatus)
			{
				BusinessUnitId = businessUnitId;
				InventoryId = inventoryId;
				CertifiedProgramId = certifiedProgramId;
				CurrentOEMCertificationStatus = currentOEMCertifiedStatus;
				Make = make;
			}

			public int BusinessUnitId { get; private set; }
			public int InventoryId { get; private set; }
			public string Make { get; private set; }
			public int CertifiedProgramId { get; private set; }
			public bool CurrentOEMCertificationStatus { get; private set; }
		}

	}
}