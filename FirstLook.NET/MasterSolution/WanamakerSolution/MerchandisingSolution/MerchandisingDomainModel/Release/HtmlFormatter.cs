using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Merchandising.DomainModel.Templating;


namespace FirstLook.Merchandising.DomainModel.Release
{
    public class HtmlFormatter
    {
        private static readonly Regex NewlinePattern = new Regex("(=){7,70}");
        private static readonly Regex HeaderPattern = new Regex("={6}([^:]+):");
        
        private string _orignalAd;
        private string _headerReplaceString;

        public HtmlFormatter(string unFormattedAd, string openTag, string closeTag)
        {
            _headerReplaceString = string.Format("{0}$1{1}", openTag, closeTag);
            _orignalAd = unFormattedAd;
        }

        private string ProcessMainAd(string ad)
        {
            string returnAd = NewlinePattern.Replace(ad, "<br />");
            return HeaderPattern.Replace(returnAd, _headerReplaceString);
        }

        public string GetHtmlFormat()
        {
            return ProcessMainAd(_orignalAd);
        }
       
    }
}
