namespace FirstLook.Merchandising.DomainModel.Release
{
    /// <summary>
    /// The values of the enums should match the database: Merchandising.postings.AdvertisementStatuses
    /// </summary>
	public enum AdvertisementStatus
    {
        /// <summary>
        /// error was encountered in release of advertisement
        /// 
        /// statusId = 0, statusDescription = 'Error'
        /// </summary>
        Error = 0,

        /// <summary>
        /// release approved and in the future
        /// 
        /// statusId = 1, statusDescription = 'Future'
        /// </summary>
		ApprovedForFutureRelease = 1,

        /// <summary>
        /// release approved and due for submission
        /// 
        /// statusId = 2, statusDescription = 'Pending Posting'
        /// </summary>
		ApprovedPendingRelease = 2,

        /// <summary>
        /// service is actually in process of releasing the advert now, aka "Posting In Process"
        /// 
        /// statusId = 3, statusDescription = 'Posting In Process'
        /// </summary>
		ReleaseInProgress = 3,

        /// <summary>
        /// advert was released successfully
        /// 
        /// statusId = 4, statusDescription = 'Released'
        /// </summary>
		Released = 4
	}
}
