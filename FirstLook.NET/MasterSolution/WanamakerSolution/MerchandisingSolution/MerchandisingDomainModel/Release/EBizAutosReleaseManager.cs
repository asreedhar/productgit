namespace FirstLook.Merchandising.DomainModel.Release
{
	public class EBizAutosReleaseManager : EdgeReleaseManager
	{
        public EBizAutosReleaseManager(int dealerId) : base(dealerId)
		{
		}

        public static EBizAutosReleaseManager Fetch(int dealerId)
        {
            return new EBizAutosReleaseManager(dealerId);
        }

        protected override int ThirdPartyId
        {
            get { return (int)ThirdPartyIds.eBizAutos; }
        }


    }
}
