using System.Data;

namespace FirstLook.Merchandising.DomainModel.Release
{
	public class ReleaseDestination
	{
	    public string Name { get; private set; }
	    public int Id { get; private set; }

	    internal ReleaseDestination(IDataRecord reader)
        {
            Id = reader.GetInt32(reader.GetOrdinal("destinationId"));
            Name = reader.GetString(reader.GetOrdinal("destinationName"));
        }

	}
}
