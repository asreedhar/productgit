﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Core.Messages;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public static class LastInventory
    {
        public static int GetMaxInventoryId()
        {
            int maxInventoryId = -1;
            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    //replace with proc.
                    command.CommandText = "SELECT max(InventoryID) as max FROM FLDW.dbo.InventoryActive";

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException("Result expected");

                        maxInventoryId = reader.GetInt32(reader.GetOrdinal("max"));
                    }
                }
            }

            return maxInventoryId;
        }

        public static NewInventoryMessage[] GetNewInventory(int lastInventoryId)
        {
            var newInventory = new List<NewInventoryMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    //replace with proc.
                    command.CommandText = String.Format("SELECT BusinessUnitID, InventoryID FROM FLDW.dbo.InventoryActive WHERE InventoryID > {0} ORDER BY InventoryID DESC", lastInventoryId);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            int inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));
                            newInventory.Add(new NewInventoryMessage(businessUnitId, inventoryId));
                        }
                    }
                }
            }

            return newInventory.ToArray();
        }

        public static bool ExistsInFldw(NewInventoryMessage message)
        {
            bool exists = false;
            using (IDataConnection connection = Database.GetConnection(Database.ImtDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    //replace with proc.
                    command.CommandText = String.Format("IF EXISTS (SELECT * FROM FLDW.dbo.InventoryActive WHERE InventoryID = {0}) select 1 as 'Exist' else select 0 as 'Exist'", message.InventoryId);

                    using (var reader = command.ExecuteReader())
                    {
                        if(!reader.Read())
                            throw new InvalidOperationException("result expected");

                        exists = reader.GetInt32(reader.GetOrdinal("Exist")) == 0 ? false : true;
                    }
                }
            }

            return exists;
        }
    }
}
