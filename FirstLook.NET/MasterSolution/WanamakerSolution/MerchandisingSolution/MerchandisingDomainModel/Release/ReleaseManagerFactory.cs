using System;

namespace FirstLook.Merchandising.DomainModel.Release
{
    
	public static class ReleaseManagerFactory
	{
        public static IReleaseManager GetReleaseManager(int businessUnitID, int destinationID)
        {
            switch (destinationID)
            {
                case (int) ReleaseDestinations.CarsDotCom: // cars
                    return CarsDotComReleaseManager.Fetch(businessUnitID);
                case (int) ReleaseDestinations.AutoTrader: // at
                    return AutoTraderReleaseManager.Fetch(businessUnitID);
                case (int) ReleaseDestinations.EBizAutos:
                    return EBizAutosReleaseManager.Fetch(businessUnitID);
                default:
                    return new ErrorReleaseManager(businessUnitID, destinationID);
            }
        }


        private class ErrorReleaseManager : IReleaseManager
        {
            private int _dealerId;
            private int _releaseDestinationId;

            public ErrorReleaseManager(int dealerId, int releaseDestinationId)
            {
                _dealerId = dealerId;
                _releaseDestinationId = releaseDestinationId;
            }

            public void Release(Advertisement advertisement)
            {
                throw new InvalidOperationException(string.Format("Dealer {0} trying to release ad to {1} which is no longer valid", _dealerId, _releaseDestinationId));
            }

        }
	}
}