﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using Merchandising.Messages;
using Merchandising.Messages.AutoLoad;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public static class SlurpeeRequeue
    {
        public static AutoLoadInventoryMessage[] Fetch()
        {
            var messages = new List<AutoLoadInventoryMessage>();
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "[Release].[AutoLoadSlurpeeRequeue#Fetch]";

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            messages.Add(new AutoLoadInventoryMessage(businessUnitId, AutoLoadInventoryType.SlurpeeRequeue));
                        }
                    }
                }
            }

            return messages.ToArray();
        }
    }
}
