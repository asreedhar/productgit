using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Net;
using System.Transactions;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Distribution.WebService.Client.Distributor;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.Pdf;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using System.Linq;
using Quartz;
using VehicleType = FirstLook.Distribution.WebService.Client.Distributor.VehicleType;

namespace FirstLook.Merchandising.DomainModel.Release
{
    /* Implements IStatefulJob because we don't want two threads Executing at once. This can happen if this job takes
     * longer than the trigger schedule. We dont' want to do more work than neccessary.
     * This code was not written for thread safety nor is it transacted.*/
    public class AdvertisementPublisher : IStatefulJob 
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Injected properties
        public IMarketListingSearchAdapter SearchAdapter { get; set;}      
        public ILogger Logger { get; set; } /*Need for the Deal object logging*/

        /// <summary>
        /// Contains IDs of dealers who use Ebiz distributor
        /// </summary>
        HashSet<int> ebizUsers;

        /// <summary>
        /// The status flag used to publish the ad.
        /// </summary>
        private readonly AdvertisementStatus _advertisementStatus;

        private readonly IDiscountPricingCampaignManager _discountPricingCampaignManager;

        public AdvertisementPublisher() : this(AdvertisementStatus.ApprovedPendingRelease)
        {
        }

        /// <summary>
        /// Used in the winform app so we can run publishing on arbitrary AdvertisementStatus's.
        /// </summary>
        /// <param name="advertisementStatus"></param>
        public AdvertisementPublisher(AdvertisementStatus advertisementStatus)
        {
            Registry.BuildUp(this);
            _advertisementStatus = advertisementStatus;

            _discountPricingCampaignManager = Registry.Resolve<IDiscountPricingCampaignManager>();
        }

        public void Execute(JobExecutionContext context)
        {
            EventLogHelper.LogTick();
             
            try
            {
                RunReleaseOperation(_advertisementStatus);
            }
            catch (Exception ex)
            {
                Log.Error("Error in Release Processor.", ex);
            }
        }

        #region Worker Thread methods

        protected void RunReleaseOperation(AdvertisementStatus status)
        {
            Log.DebugFormat("RunReleaseOperation() called for AdvertisementStatus '{0}'", status);
            Log.DebugFormat("CallDistributionWebService = {0}, CallReleaseRobots = {1}", ConfigurationManager.AppSettings["CallDistributionWebService"], ConfigurationManager.AppSettings["CallReleaseRobots"]);
            Log.DebugFormat("Version: {0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);

            ebizUsers = new HashSet<int>(Dealer.GetDealerIdsForDistributionProviderId(ReleaseDestinations.EBizAutos));

            IEnumerable<Dealer> dealers = Dealer.GetDealers(status);
            foreach (Dealer dealer in dealers)
            {
                try
                {
                    PublishPdfsForDealer(dealer, status);

                    Log.DebugFormat("Trying to release ads for dealer '{0}'", dealer.Id);
                    ReleaseToDestinations(dealer);
                }
                catch (Exception ex)
                {
                    string error = String.Format("Error releasing ads for dealer {0}", dealer.Id);
                    Log.Error(error,ex);
                    EventLogHelper.LogError(ex, dealer.Id, true);
                }
            }
        
            Log.Debug("RunReleaseOperation() done");
        }

        public void ReleaseToDestinations(Dealer dealer)
        {
            Log.DebugFormat("ReleaseToDestinations() called for dealer '{0}'", dealer.Id);

            bool released;

            try  //if the release manager isn't configured properly at initialization it CAN throw an exception, which would short circuit...everybody!  So we should catch that guy
            {
                EventLogHelper.LogRelease(dealer.Id);

                // TH 2013-09-30: We don't release to multiple destinations, so I removed the destinationId loop.
                // Now we just always get the autotrader release manager because it doesn't matter anyway.
                IReleaseManager mgr = ReleaseManagerFactory.GetReleaseManager(dealer.Id, 1);
                IEnumerable<Advertisement> ads = Advertisement.GetAdvertisements(dealer.Id, AdvertisementStatus.ApprovedPendingRelease);

                var advertisements = ads as Advertisement[] ?? ads.ToArray();

                Log.DebugFormat("Trying to Release {0} ads for dealer {1}", advertisements.Count(), dealer.Id);
                IDictionary<int, int> publishedInventoryIds = new Dictionary<int, int>();
                foreach (Advertisement advert in advertisements)
                {
                    // reset release flag to false
                    released = false;
                        
                    Log.DebugFormat("Trying to release ad {0}", advert.Id);

                    //prevent multiple Advertisements for an Inventory record to be sent out to a Destination.
                    if (!publishedInventoryIds.ContainsKey(advert.InventoryId))
                    {
                        Log.DebugFormat("This ad {0} is NOT already published.", advert.Id);
                        publishedInventoryIds.Add(advert.InventoryId, advert.InventoryId);

                        MerchandisingDescription merchandisingDescription;

                        try
                        {
                            if (ConfigurationManager.AppSettings["EnableDTC"] == "1")
                            {
                                // Release the ad in an ambient DTC transaction
                                using (TransactionScope txn = new TransactionScope())
                                {
                                    merchandisingDescription = ReleaseAdvertisement(dealer, mgr, advert);
                                    txn.Complete();
                                }
                            }
                            else
                            {
                                merchandisingDescription = ReleaseAdvertisement(dealer, mgr, advert);
                            }
                              
                            released = true;
                        }
                        catch(SqlException e) //catch sql exceptino
                        {
                            switch ( e.Number )
                            {
                                case 2: /* Error establishing connection */
                                case 53: /* Error establishing connection */
                                case -1: /* Error establishing connection */
                                case -2: /* Client Timeout */
                                case 18470: /* Login failed for user. Account is disabled */
                                case 701: /* Out of Memory */ 
                                case 1204: /* Lock Issue */ 
                                case 1205: /* Deadlock Victim */ 
                                case 1222: /* Lock Request Timeout */ 
                                case 8645: /* Timeout waiting for memory resource */ 
                                case 8651: /* Low memory condition */
                                    try
                                    {
                                        Log.Error("Timeout or connection issue. Requeueuing", e);
                                        EventLogHelper.LogError(e, dealer.Id, true);
                                        advert.Retry();
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("Exception encountered logging ad release error", ex);
                                    }
                                    break;
                                default:
                                    RecordFailure(advert, dealer, e);
                                    break;
                            }

                            continue;
                        }
                        catch (Exception e)
                        {
                            RecordFailure(advert, dealer, e);
                            continue;
                        }

                        // do not call distribution API when Auto Approving new cars sent to eBiz                            
                        bool bugzid24049 = advert.VehicleIsNew && ebizUsers.Contains(dealer.Id);

                        if (merchandisingDescription != null && released)
                        {
                            if (bugzid24049)
                            {
                                Log.Debug("Will not call DistributionWebService because bugzid24049");
                            } 
                            else
                            {
                                if (ConfigurationManager.AppSettings["CallDistributionWebService"] == "1")
                                {
                                    DoDistributorWebServiceIntegration(merchandisingDescription, advert);
                                    Log.DebugFormat("DistributorWebServiceIntegration() called with description '{0}' and ad id '{1}'",
                                        merchandisingDescription, advert.Id);
                                }
                                else
                                {
                                    Log.Debug("DistributionWebService not called because CallDistributionWebService is set to 1 is config file.");
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.ErrorFormat("This ad {0} is already published.", advert.Id);

                        string errorMsg = string.Format("Tried to publish Ad for InventoryID {0} a second time", advert.InventoryId);
                        Exception theException = new Exception(errorMsg);
                        EventLogHelper.LogError(theException, dealer.Id, false);
                        advert.Failure(theException);
                            
                        // why not continue?  ZB
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ad release Error", ex);
                EventLogHelper.LogError(ex, dealer.Id, true);
            }

            //now, check for expired adverts
            foreach (int expiredInventoryId in Advertisement.GetExpiredAds(dealer.Id))
            {
                Log.DebugFormat("Ad {0} is expired at dealer {1}. Generating new Ad.", expiredInventoryId, dealer.Id);

                //NOTE:  The mediator needs a memberLogin that has access to Carfax reports - probably the last approver of the ad is most appropriate
                TemplateMediator mediator = new TemplateMediator(dealer.Id, expiredInventoryId, string.Empty);
                MerchandisingDescription newDesc = mediator.GenerateDescription();
                newDesc.Save(dealer.Id, expiredInventoryId, EventLogHelper.SERVICE_NAME);
                StepStatusCollection
                    .Fetch(dealer.Id, expiredInventoryId)
                    .SetPostingStatus(AdvertisementPostingStatus.PendingApproval)
                    .Save(EventLogHelper.SERVICE_NAME);

            }
        }

        private static void RecordFailure(Advertisement advert, Dealer dealer, Exception e)
        {
            Log.Error("Exception encountered during ad release.  Transaction rolled back.", e);
            try
            {
                EventLogHelper.LogError(e, dealer.Id, true);
                advert.Failure(e);
            }
            catch (Exception ex)
            {
                Log.Error("Exception encountered logging ad release error", ex);
            }
            
        }

        private void PublishPdfsForDealer(Dealer dealer, AdvertisementStatus status)
        {
            var approvedInventoryList = dealer.GetApprovedInventory(status);
            Log.DebugFormat("Publishing {0} pdfs for dealer {1}", approvedInventoryList.Count, dealer.Id);

            foreach (ApprovedInventory inventory in approvedInventoryList)
            {
                var businessUnit = BusinessUnitFinder.Instance().Find(dealer.Id);
                if (businessUnit.HasDealerUpgrade(Upgrade.Marketing) && businessUnit.HasDealerUpgrade(Upgrade.WebSitePDF))
                {
                    Log.DebugFormat("Publishing PDF for Dealer: {0}, InventoryID: {1}, userName: {2}", dealer.Id, inventory.InventoryID, inventory.ApprovalUser);
                    WebSitePdfPublisher publisher = new WebSitePdfPublisher(PricingContext.FromMerchandising(dealer.Id, inventory.InventoryID), Logger,
                                                SearchAdapter,
                                                businessUnit.HasDealerUpgrade(Upgrade.WebSitePDFMarketListings),
                                                inventory.ApprovalUser);

                    publisher.Publish();
                }
            }
        }

        /// <summary>
        /// Refactored mega method just to break it down into more comprehensible chunks.
        /// </summary>
        /// <param name="dealer"></param>
        /// <param name="mgr"></param>
        /// <param name="advert"></param>
        /// <returns></returns>
        private static MerchandisingDescription ReleaseAdvertisement(Dealer dealer, IReleaseManager mgr, Advertisement advert)
        {
            //Advertisement(object) invariant
            MerchandisingDescription desc = null;

            //store all the inventoryIds released for this Dealer-ReleaseDestination

            //get latest description...
            desc = MerchandisingDescription.Fetch(dealer.Id, advert.InventoryId);

            //this is an unfortunate problem due to the way we are publishing and scheduling today.  If a user
            //publishes an item for release in the future, then saves a draft in the system, we do not save these
            //as separate descriptions.  The latter actually overwrites the former.  Hence, I store the draft info in case it has changed
            //then I fill the description item (which contains possibly erroneous metadata now), with the released advert, footer, and expiry
            //finally, I publish to accelerator, where the new "acceleratorID" is set on the description
            //I replace the draft info (so it will remain in wanamaker) and save

            string draftDesc = desc.Description;
            string draftFooter = desc.Footer;
            DateTime? draftExp = desc.ExpiresOn;

            desc.Description = advert.Text;
            desc.Footer = advert.Footer;
            desc.ExpiresOn = advert.ExpiresOn;

            //update with advert text

            Log.Debug("Saving to Market.Merchandising tables and IMT.InternetAdvertisement.");
            desc.Publish(dealer.Id, advert.InventoryId, advert.ApprovedByMemberLogin);
            Log.DebugFormat("MerchandisingDescription {0} published for dealer {1} inventory {2}, approved by {3}",
                desc.AdvertisementId, dealer.Id, advert.InventoryId, advert.ApprovedByMemberLogin);

            desc.Description = draftDesc;
            desc.Footer = draftFooter;
            desc.ExpiresOn = draftExp;

            // update the merchandising database
            Log.Debug("Saving to Merchandising");
            desc.Save(dealer.Id, advert.InventoryId, advert.ApprovedByMemberLogin);

            Log.DebugFormat("MerchandisingDescription {0} saved for dealer {1} inventory {2}, approved by {3}",
                desc.AdvertisementId, dealer.Id, advert.InventoryId, advert.ApprovedByMemberLogin);

            // releasing to firstlook
            Log.Debug("");
            mgr.Release(advert);
            advert.Success();
            Log.Debug("Ad relased and ad.Success() called.");

            return desc;
        }

        private static void DoDistributorWebServiceIntegration(MerchandisingDescription desc, Advertisement theAd)
        {
            Log.Debug("Preparing message for distributor web service");

            Distribution.WebService.Client.Distributor.Dealer dealer = new Distribution.WebService.Client.Distributor.Dealer();
            dealer.Id = desc.BusinessUnitId;

            Vehicle vehicle = new Vehicle();
            vehicle.Id = theAd.InventoryId;
            vehicle.VehicleType = VehicleType.Inventory;
            Log.DebugFormat("VehicleId {0}, VehicleType {1}", vehicle.Id, vehicle.VehicleType);

            TextContent mainAd = new TextContent();
            mainAd.Text = desc.Description + Environment.NewLine + desc.Footer;
            Log.DebugFormat("MainAd contents '{0}'", mainAd.Text);

            TextContent htmlAd = new TextContent();
            htmlAd.Text = theAd.HtmlDescription;
            Log.DebugFormat("HtmlDescription contents '{0}'", htmlAd.Text);

            Content[] contents = new Content[] { mainAd, htmlAd };
            Distribution.WebService.Client.Distributor.Advertisement advertisement =
                new Distribution.WebService.Client.Distributor.Advertisement();
            advertisement.Contents = contents;

            //description cannot be sent alone, must always send price and description or just prtheice.
            Price price = new Price();
            price.PriceType = PriceType.Internet;
            decimal adPrice = theAd.SpecialPrice ?? theAd.Price;
            price.Value = Convert.ToInt32(adPrice);
            Log.DebugFormat("Price is {0} and priceType is {1}", price.Value, price.PriceType);

            BodyPart[] bodyParts = new BodyPart[] { price, advertisement };
            MessageBody body = new MessageBody();
            body.Parts = bodyParts;

            Message message = new Message();
            message.From = dealer;
            message.Subject = vehicle;
            message.Body = body;

            Log.DebugFormat("Message From: {0}, Subject: {1}, Body: price and advertisement", dealer.Id, vehicle.Id);

            try
            {
                Distributor distributor = new Distributor(); //url set by proper App-*.config file.
                string distributorUrl = ConfigurationManager.AppSettings["DistributorWebService_Uri"];
                
                if (distributorUrl != null)
                {
                    distributor.Url = distributorUrl;
                }
                distributor.Distribute(message);
                Log.DebugFormat("Disributor sent message to url '{0}'", distributorUrl);
            }
            catch (WebException e) /*Catches connection type errors*/
            {
                Log.Error("Could not update Advertisement through Distributor web service!", e);
                EventLogHelper.LogError(e, desc.BusinessUnitId, true);
                throw new ApplicationException(string.Format("Could not update Advertisement through Distributor web service! Error:{0}", e.Message));
            }
            catch (Exception e)  /*All Others*/
            {
                Log.Error("Could not update Advertisement through Distributor web service!", e);
                throw new ApplicationException(string.Format("Could not update Advertisement through Distributor web service! Error:{0}", e.Message));
            }
            
        }

        #endregion //Worker Thread methods

    }
}
