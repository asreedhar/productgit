﻿using System.Collections.Generic;
using System.Linq;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public class BucketAlertEmailViewModel
    {
        private static readonly WorkflowType[] LowQualityItems = new[] {

            WorkflowType.TrimNeeded,
            WorkflowType.DueForRepricing,
            WorkflowType.EquipmentNeeded,
            WorkflowType.LowActivity,
            WorkflowType.LowPhotos,
            WorkflowType.NoPackages,
            WorkflowType.BookValueNeeded,
            WorkflowType.NoCarfax,
            //Added new bucket
            WorkflowType.NoFavourablePriceComparisons 
        };

        public BucketAlertEmailViewModel(string businessUnit)
        {
            BusinessUnit = businessUnit;
            OnlineGaps = new List<BucketAlertEmailItem>();
            LowQuality = new List<BucketAlertEmailItem>();
        }

        public string BusinessUnit { get; private set; }

        // all workflowTypes NOT included in lowQualityItem (aka merchadising alerts)
        //WorkflowType.NotOnline,
        //WorkflowType.NoPrice,
        //WorkflowType.NoPhotos,
        //WorkflowType.CreateInitialAd,

        public IList<BucketAlertEmailItem> OnlineGaps { get; private set; }
        public string OnlineGapLink { get; set; }
        public int OnlineGapCount { get; set; }

        // all workflowTypes defined in lowQualityItems (aka merchandising alerts)
        //WorkflowType.TrimNeeded,
        //WorkflowType.DueForRepricing,
        //WorkflowType.EquipmentNeeded,
        //WorkflowType.LowActivity,
        //WorkflowType.LowPhotos,
        //WorkflowType.NoPackages,
        //WorkflowType.BookValueNeeded,
        //WorkflowType.NoCarfax
        public IList<BucketAlertEmailItem> LowQuality { get; private set; }
        public string LowQualityLink { get; set; }
        public int LowQualityCount { get; set; }

        public string SettingsLink { get; set; }

        public void Add(BucketAlertEmailItem item)
        {
            if (LowQualityItems.Contains(item.BucketType))
                LowQuality.Add(item);
            else
                OnlineGaps.Add(item);
        }

        public int Count
        {
            get { return LowQuality.Count + OnlineGaps.Count; }
        }

        public bool HasLowQuality
        {
            get { return LowQuality.Count > 0; }
        }

        public bool HasOnlineGaps
        {
            get { return OnlineGaps.Count > 0; }
        }

    }
}
