using System;
using System.Configuration;
using System.Diagnostics;
using System.Net.Mail;

namespace FirstLook.Merchandising.DomainModel.Release
{
    public static class EventLogHelper
    {
        public const string SOURCE_NAME = "Prototype Advertisement Release Service";
        public const string LOG_NAME = "Application";
        public const string SERVICE_NAME = "release service";

        public static void LogMessage(string msg, EventLogEntryType logEntryType)
        {
            if (!EventLog.SourceExists(SOURCE_NAME))
            {
                EventLog.CreateEventSource(SOURCE_NAME, LOG_NAME);
            }

            EventLog log = new EventLog();
            log.Source = SOURCE_NAME;
            log.WriteEntry(msg, logEntryType);
        }

        public static void LogTick()
        {
            LogMessage("I'm alive!", EventLogEntryType.Information);
        }

        public static void LogRelease(int businessUnitId)
        {
            LogMessage(string.Format("Releasing data for {0}", businessUnitId), EventLogEntryType.Information);
        }

        public static void LogMessage(string msg)
        {
            LogMessage(msg, EventLogEntryType.Information);
        }

        public static void LogError(Exception ex, int dealerId, bool sendMail)
        {
            string logMsg = string.Format("Exception for dealerId ({0}) {1} \nSTACK:{2}", dealerId, ex.Message, ex.StackTrace);
            LogMessage(logMsg, EventLogEntryType.Error);

            if (!sendMail) return;
            
            try
            {
                string fromEmail = ConfigurationManager.AppSettings["fromEmail"];
                string toEmail = ConfigurationManager.AppSettings["toEmail"];
                var emailSubject = GetEmailSubject();

                if (!string.IsNullOrEmpty(fromEmail) && !string.IsNullOrEmpty(toEmail))
                {
                    MailMessage mail = new MailMessage(
                        fromEmail,
                        toEmail,
                        emailSubject, 
                        logMsg);

                    SmtpClient sender = new SmtpClient();
                    sender.Send(mail);
                }
            }
            catch (Exception mailEx)
            {
                LogError(mailEx, dealerId, false);
            }
        }

        private static string GetEmailSubject()
        {
            var suffix = ConfigurationManager.AppSettings["buildEnvironment"] == null 
                   ? string.Empty 
                   : string.Format( " - Produced from {0}", ConfigurationManager.AppSettings["buildEnvironment"] );
            return string.Format("Max AD : Release Processor Error Report {0}", suffix);
        }

       
    }
}
