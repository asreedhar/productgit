namespace FirstLook.Merchandising.DomainModel.Release
{
	public interface IReleaseManager
	{
		void Release(Advertisement advertisement);
	}
}
