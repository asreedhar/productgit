﻿using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel
{
    public class LocateVehicleResults
    {
        public bool FoundResult { get; set; }

        public WorkflowType ActiveListType { get; set; }
        public int? AgeBucketId { get; set; }
        public int UsedOrNewFilter { get; set; }
    }
}
