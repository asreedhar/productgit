using System;

namespace FirstLook.Merchandising.DomainModel
{
    [Serializable]
    public class Pair<TK, TV>
    {
        private readonly TK _first;
        private readonly TV _second;

        public Pair(TK first, TV second)
        {
            _first = first;
            _second = second;
        }

        public TK First
        {
            get { return _first; }
        }

        public TV Second
        {
            get { return _second; }
        }
    }
}