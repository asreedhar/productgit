using System;
using System.Data;
using System.Web.Security;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel
{
    /// <summary>
    /// A way to create users.
    /// </summary>
    public static class MerchandisingMember
    {
        /// <summary>
        /// Create a user and don't return anything, since the only call to this (from Admin.aspx) was not capturing the return value.
        /// Parameters were also removed as they were not being used.
        /// </summary>
        /// <param name="username"></param>
        public static void CreateMerchandisingUser(string username)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();
                    cmd.CommandText = "merchandising.createUser";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "MemberLogin", username, DbType.String);
                   
                    cmd.ExecuteNonQuery();

                    MembershipCreateStatus status;
                    Membership.CreateUser(username, "testAbcdefg", Guid.NewGuid().ToString(), "a", "b", true, out status);
                    return;
                }
            }
        }
    }
}
