﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.Postings;
using MAX.Entities;
using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class BenchmarkingDataSource
    {
        
        #region Database and object construction
        /// <summary>
        /// Get a list of BenchmarkingRecords from the database.
        /// </summary>
        /// <param name="Site"></param>
        /// <param name="YearMonth"></param>
        /// <returns></returns>
        public static List<BenchmarkingRecord> LoadList(EdtDestinations Site, DateTime YearMonth)
        {
            List<BenchmarkingRecord> resultList = new List<BenchmarkingRecord>();

            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "dbo.BenchmarkingData#Fetch";
                command.CommandType = CommandType.StoredProcedure;
                command.AddRequiredParameter("@SiteId", (int)Site, DbType.Int32);
                command.AddRequiredParameter("@Year", YearMonth.Year, DbType.Int32);
                command.AddRequiredParameter("@Month", YearMonth.Month, DbType.Int32);
                using (IDataReader reader = command.ExecuteReader())
                    while (reader.Read())
                    {
                        BenchmarkingRecord record = new BenchmarkingRecord(reader);
                        resultList.Add(record);
                       
                    }
            }
            return resultList;
        }

        /// <summary>
        /// Get a fleshed out BenchmarkData object, even if it's empty.
        /// </summary>
        /// <param name="site"></param>
        /// <param name="YearMonth"></param>
        /// <returns></returns>
        public static BenchmarkDestinationObj LoadObject(EdtDestinations site, DateTime YearMonth)
        {
            BenchmarkDestinationObj result = new BenchmarkDestinationObj(site);

            List<BenchmarkingRecord> records = LoadList(site, YearMonth);
            foreach (BenchmarkingRecord record in records)
            {
                if (result != null)
                {
                    // The structure of this object doesn't change, only the costs are different, so....
                    result.Site = record.SiteId;
                    result.Date = record.Date;

                    // ...find and set the appropriate values.
                    result.VehTypes.Find(dst => dst.VehType.Equals(record.UsedNew)).Segments.Find(seg => seg.Segment.Equals(record.DealershipSegmentType)).Costs.Find(co => co.CostType == BenchmarkCostType.Impression).Cost = record.ImpressionCost;
                    result.VehTypes.Find(dst => dst.VehType.Equals(record.UsedNew)).Segments.Find(seg => seg.Segment.Equals(record.DealershipSegmentType)).Costs.Find(co => co.CostType == BenchmarkCostType.DirectLead).Cost = record.LeadCost;
                }
            }
            return result;
        }

        public static void Save(List<BenchmarkingRecord> rows)
        {
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            {
                foreach (BenchmarkingRecord row in rows)
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "[dbo].[BenchmarkingData#Upsert]";
                        command.CommandType = CommandType.StoredProcedure;
                        
                        command.AddRequiredParameter("@SiteId", (int)row.SiteId, DbType.Int32);
                        command.AddRequiredParameter("@Year", row.Date.Year, DbType.Int32);
                        command.AddRequiredParameter("@Month", row.Date.Month, DbType.Int32);
                        command.AddRequiredParameter("@DealershipSegmentType", row.DealershipSegmentType, DbType.Byte);
                        command.AddRequiredParameter("@VehicleType", row.UsedNew, DbType.Byte);
                        command.AddRequiredParameter("@ImpressionCost", row.ImpressionCost, DbType.Decimal);
                        command.AddRequiredParameter("@LeadCost", row.LeadCost, DbType.Decimal);
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion Database and object construction



        #region Data Objects
        public enum BenchmarkCostType
        {
            Impression,
            DirectLead
        }

        public class BenchmarkCostObjects
        {
            public BenchmarkCostType CostType { get; set; }
            public decimal Cost { get; set; }
        }

        public class BenchmarkSegments
        {
            public DealershipSegment Segment { get; set; }
            public List<BenchmarkCostObjects> Costs { get; set; }
        }

        public class BenchmarkVehTypes
        {
            public VehicleTypeEnum VehType { get; set; }
            public List<BenchmarkSegments> Segments { get; set; }
        }

        public class BenchmarkDestinationObj
        {
            public EdtDestinations Site { get; set; }
            public List<BenchmarkVehTypes> VehTypes { get; set; }
            public DateTime Date { get; set; }

            /// <summary>
            ///  We want these in a particular order.
            /// </summary>
            private VehicleTypeEnum[] orderedVehicleTypes = new VehicleTypeEnum[] {
                VehicleTypeEnum.Used,
                VehicleTypeEnum.New,
                VehicleTypeEnum.Both
            };

            // Stub it out.
            public BenchmarkDestinationObj(EdtDestinations site)
            {
                Site = site;
                VehTypes = new List<BenchmarkVehTypes>();

                foreach (VehicleTypeEnum vt in orderedVehicleTypes)
                {
                    BenchmarkVehTypes vtypes = new BenchmarkVehTypes()
                    {
                        VehType = vt,
                        Segments = new List<BenchmarkSegments>()
                    };

                    foreach (DealershipSegment segment in EnumHelper.GetValues<DealershipSegment>())
                    {
                        // Do not use "undefined". It is not a valid value.
                        if (segment != DealershipSegment.Undefined)
                        {
                            BenchmarkSegments seg = new BenchmarkSegments()
                            {
                                Segment = segment,
                                Costs = new List<BenchmarkCostObjects>()
                            };

                            foreach (BenchmarkCostType ct in EnumHelper.GetValues<BenchmarkCostType>())
                            {
                                BenchmarkCostObjects co = new BenchmarkCostObjects()
                                {
                                    CostType = ct,
                                    Cost = 0M
                                };

                                seg.Costs.Add(co);
                            }
                            vtypes.Segments.Add(seg);
                        }
                    }

                    VehTypes.Add(vtypes);
                }
            }
        }

        public class BenchmarkingRecord
        {
            public EdtDestinations SiteId { get; set; }
            public VehicleTypeEnum UsedNew { get; set; }
            public DealershipSegment DealershipSegmentType { get; set; }
            public decimal ImpressionCost { get; set; }
            public decimal LeadCost { get; set; }
            public DateTime Date { get; set; }

            public BenchmarkingRecord(IDataReader reader)
            {
                int month = reader.GetInt32(reader.GetOrdinal("Month"));
                int year = reader.GetInt32(reader.GetOrdinal("Year"));
                Date = new DateTime(year, month, 1);
                SiteId = (EdtDestinations)reader.GetByte(reader.GetOrdinal("SiteId"));
                UsedNew = (VehicleTypeEnum)reader.GetByte(reader.GetOrdinal("VehicleType"));
                DealershipSegmentType = (DealershipSegment)reader.GetByte(reader.GetOrdinal("DealershipSegmentType"));
                ImpressionCost = reader.GetDecimal(reader.GetOrdinal("ImpressionCost"));
                LeadCost = reader.GetDecimal(reader.GetOrdinal("LeadCost"));
            }

            public BenchmarkingRecord() { }
        }
        #endregion Data Objects

    }
}
