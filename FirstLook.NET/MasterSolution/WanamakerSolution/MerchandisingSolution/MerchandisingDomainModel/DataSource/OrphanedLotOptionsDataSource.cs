﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class OrphanedLotOptionsDataSource
    {
        public GenericChoiceCollection Fetch(int chromeStyleID, int businessUnitID, int inventoryID)
        {
            //chrome displayed equipment
            var chromeChoices = new GenericChoicesDataSource();
            var displayedEquipment = chromeChoices.Fetch(chromeStyleID, 0);

            //saved equipment
            var savedEquipment = GenericEquipmentCollection.FetchSelections(businessUnitID, inventoryID);

            //can't use link because collections are not generic
            var returnCollection = new GenericChoiceCollection();
            for (int x = 0; x < savedEquipment.Count; x++)
            {
                GenericEquipment equipment = savedEquipment.Item(x);

                //These should not be in the other Category. They are controlled by the Vehicle Configuration on the Equipment Screen and explicitly removed from the Exterior and Interior sections
                if (equipment.Category.Header == "Engine" || equipment.Category.Header == "Transmission" || equipment.Category.Header == "Drivetrain" || equipment.Category.Header == "Fuel")
                    continue;

                bool found = false;
                for (int y = 0; y < displayedEquipment.Count; y++ )
                {
                    if (displayedEquipment[y].ID == equipment.Category.CategoryID)
                    {
                        found = true;
                        break;
                    }
                }

                if(!found)
                    returnCollection.Add(new GenericChoice() { ID = equipment.Category.CategoryID, Header = equipment.Category.Header, Description = equipment.Category.Description, IsStandard = equipment.IsStandard, Selected = false });    
            }

            return returnCollection;
                
        }

    }
}
