using System.Data;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class LotDataSource
    {
        public static LotVehicleData Fetch(string vin)
        {
            using (IDbConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText =
                        @"
                        
                        SELECT top(1)
                            VIN, 
                            StockNumber, 
                            ChromeStyleId, 
                            OptionCodes, 
                            Color
                        FROM lot.listing.vehicle veh
                        JOIN lot.listing.color co
                        ON co.colorid = veh.colorid
                        WHERE vin = @Vin
                    ";
                    cmd.CommandType = CommandType.Text;

                    Database.AddRequiredParameter(cmd, "Vin", vin, DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        return reader.Read() ? new LotVehicleData(reader) : new LotVehicleData();
                    }
                }
            }
        }
    }
}
