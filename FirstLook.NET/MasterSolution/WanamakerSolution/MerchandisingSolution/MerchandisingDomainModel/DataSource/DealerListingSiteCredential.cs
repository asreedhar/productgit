﻿using System.Collections.Generic;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public interface IDealerListingSiteCredentialRepository
    {
        List<DealerListingSiteCredential> Load( int BusinessUnitId, int? DestinationId = null, bool OnlyActiveReleaseSites = false );
        void Save( DealerListingSiteCredential row );
    }
    
    public class DealerListingSiteCredentialRepository : IDealerListingSiteCredentialRepository
    {
        public List<DealerListingSiteCredential> Load( int BusinessUnitId, int? DestinationId = null, bool OnlyActiveReleaseSites = false )
        {
            var result = new List<DealerListingSiteCredential>();
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "Release.ListingSitePreferences#Fetch";
                command.CommandType = CommandType.StoredProcedure;
                command.AddRequiredParameter("BusinessUnitId", BusinessUnitId, DbType.Int32);
                command.AddRequiredParameter("DestinationId", DestinationId, DbType.Int32);
                command.AddRequiredParameter("OnlyActiveReleaseSites", OnlyActiveReleaseSites, DbType.Boolean);
                using (IDataReader reader = command.ExecuteReader())
                while( reader.Read() )
                {
                    result.Add( new DealerListingSiteCredential() {
                        BusinessUnitId = BusinessUnitId,
                        DestinationId = reader.IsDBNull(reader.GetOrdinal("DestinationId"))? 0 : reader.GetInt32(reader.GetOrdinal("DestinationId")),
                        Username = reader.IsDBNull(reader.GetOrdinal("Username"))? null : reader.GetString(reader.GetOrdinal("Username")),
                        Password = reader.IsDBNull(reader.GetOrdinal("Password"))? null : reader.GetString(reader.GetOrdinal("Password")),
                        ExternalID = reader.IsDBNull(reader.GetOrdinal("ListingSiteDealerId"))? null : reader.GetString(reader.GetOrdinal("ListingSiteDealerId"))
                    });
                    
                }
            }
            return result;
        }

        public void Save( DealerListingSiteCredential row )
        {
            if( row == null )
                return;
            
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "Release.ListingSitePreferences#CredentialUpsert";
                command.CommandType = CommandType.StoredProcedure;
                command.AddRequiredParameter("BusinessUnitId", row.BusinessUnitId, DbType.Int32);
                command.AddRequiredParameter("DestinationId", row.DestinationId, DbType.Int32);
                command.AddRequiredParameter("Username", row.Username, DbType.String);
                command.AddRequiredParameter("Password", row.Password, DbType.String);
                command.AddRequiredParameter("ListingSiteDealerId", row.ExternalID, DbType.String);
                command.ExecuteNonQuery();
            }
        }
    }

    public class DealerListingSiteCredential
    {
        public int BusinessUnitId;
        public int DestinationId;
        public string Username;
        public string Password;
        public string ExternalID;
    }
}
