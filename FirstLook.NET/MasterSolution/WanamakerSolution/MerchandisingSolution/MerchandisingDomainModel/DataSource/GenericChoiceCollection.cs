using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class GenericChoiceCollection : StateManagedCollection
    {
        private static readonly Type[] KnownTypes = new[] { typeof(GenericChoice) };

        public void Add(GenericChoice choice)
        {
            ((IList) this).Add(choice);
        }
       
        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0: return new GenericChoice();
            }
            throw new ArgumentOutOfRangeException("index");
        }

        protected override Type[] GetKnownTypes()
        {
            return KnownTypes;
        }
       
        public int IndexOf(GenericChoice choice)
        {
            return ((IList)this).IndexOf(choice);
        }

        public void Insert(int index, GenericChoice choice)
        {
            ((IList)this).Insert(index, choice);
        }

        protected override void OnValidate(object o)
        {
            base.OnValidate(o);

            if (!(o is GenericChoice))
            {
                throw new ArgumentException("DataControlFieldCollection_InvalidType");
            }
        }

        public void Remove(GenericChoice choice)
        {
            ((IList)this).Remove(choice);
        }

        public void RemoveAt(int index)
        {
            ((IList)this).RemoveAt(index);
        }

        protected override void SetDirtyObject(object item)
        {
            ((GenericChoice)item).SetDirty();
        }

        [Browsable(false)]
        public GenericChoice this[int index]
        {
            get
            {
                return (GenericChoice)((IList)this)[index];
            }
        }

        
    }
}
