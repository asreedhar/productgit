using System;
using System.Data;
using System.Web.UI;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class GenericChoice : IStateManager
    {
        private StateBag _viewState;
        private bool _trackViewState;

        public int ID
        {
            get
            {
                object value = ViewState["Id"];
                if (value == null)
                    return 0;
                return (int)value;
            }
// ReSharper disable MemberCanBePrivate.Global // I believe the framework calls this.
            set
// ReSharper restore MemberCanBePrivate.Global
            {
                if (ID != value)
                {
                    if (value == 0)
                    {
                        ViewState.Remove("Id");
                    }
                    else
                    {
                        ViewState["Id"] = value;
                    }
                }
            }
        }

        public string Description
        {
            get
            {
                string str = (string)ViewState["Desc"];
                return string.IsNullOrEmpty(str) ? string.Empty : str;
            }
            set
            {
                if (string.Compare(Description, value, StringComparison.InvariantCulture) == 0) return;
                
                if (string.IsNullOrEmpty(value))
                {
                    ViewState.Remove("Desc");
                }
                else
                {
                    ViewState["Desc"] = value;
                }
            }
        }

        public string Header
        {
            get
            {
                string str = (string)ViewState["Hdr"];
                return string.IsNullOrEmpty(str) ? string.Empty : str;
            }
            set
            {
                if (string.Compare(Description, value, StringComparison.InvariantCulture) == 0) return;
                
                if (string.IsNullOrEmpty(value))
                {
                    ViewState.Remove("Hdr");
                }
                else
                {
                    ViewState["Hdr"] = value;
                }
            }
        }


        public bool IsStandard
        {
            get
            {
                object value = ViewState["std"];
                return value != null && (bool) value;
            }
            set
            {
                if (Selected == value) return;
                
                if (value == false)
                {
                    ViewState.Remove("std");
                }
                else
                {
                    ViewState["std"] = value;
                }
            }
        }


        public bool Selected
        {
            get
            {
                object value = ViewState["sel"];
                if (value == null)
                    return false;
                return (bool)value;
            }
// ReSharper disable UnusedMember.Global
            set
// ReSharper restore UnusedMember.Global
            {
                if (Selected == value) return;
                
                if (value == false)
                {
                    ViewState.Remove("sel");
                }
                else
                {
                    ViewState["sel"] = value;
                }
            }
        }

        public GenericChoice()
        {
            ID = 0;
            Description = string.Empty;
        }
        internal GenericChoice(IDataRecord reader)
        {
            ID = reader.GetInt32(reader.GetOrdinal("categoryID"));
            Description = reader.GetString(reader.GetOrdinal("UserFriendlyName"));
            IsStandard = reader.GetBoolean(reader.GetOrdinal("IsStandardEquipment"));
            Header = reader.GetString(reader.GetOrdinal("categoryheader"));
        }

        internal void SetDirty()
        {
            ViewState.SetDirty(true);
        }

        protected StateBag ViewState
        {
            get
            {
                if (_viewState == null)
                {
                    _viewState = new StateBag(false);

                    if (_trackViewState)
                    {
                        ((IStateManager)_viewState).TrackViewState();
                    }
                }

                return _viewState;
            }
        }

        #region IStateManager Members

        protected bool IsTrackingViewState
        {
            get
            {
                return _trackViewState;
            }
        }

        bool IStateManager.IsTrackingViewState
        {
            get { return IsTrackingViewState; }
        }

        protected void LoadViewState(object state)
        {
            object[] values = (object[])state;

            if (values[0] != null)
            {
                ((IStateManager)ViewState).LoadViewState(values[0]);
            }
        }

        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        public object SaveViewState()
        {
            object[] state = new object[1];

            if (_viewState != null)
            {
                state[0] = ((IStateManager)_viewState).SaveViewState();
            }

            return state;
        }

        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        protected void TrackViewState()
        {
            _trackViewState = true;

            if (_viewState != null)
            {
                ((IStateManager)_viewState).TrackViewState();
            }
        }

        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

        #endregion
    }
}
