using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Pricing;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class PricingDataSource
    {
        public static List<PricingData> FetchBasicPricing(int businessUnitId, int inventoryId)
        {
            List<PricingData> retList = new List<PricingData>();
            try
            {
                
                retList.Add(PricingData.GetMarketPricing(businessUnitId, inventoryId));
            }
            catch
            {
                retList.Add(new PricingData());
            }
            return retList;
        }
    }
}
