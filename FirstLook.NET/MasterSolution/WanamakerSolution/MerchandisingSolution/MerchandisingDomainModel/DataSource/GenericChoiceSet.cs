using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class GenericChoiceSet : IComparable, IStateManager
    {
        private List<GenericChoice> _options;

        public List<GenericChoice> Options
        {
            get { return _options ?? (_options = new List<GenericChoice>()); }
            set { _options = value; }
        }

        public string Header { get; set; }

        public GenericChoiceSet(string header)
        {
            IsTrackingViewState = false;
            Header = header;
            _options = new List<GenericChoice>();
        }


        public void Add(GenericChoice choice)
        {
            _options.Add(choice);
        }

        public int CompareTo(object obj)
        {
            if (obj is GenericChoiceSet)
            {
                GenericChoiceSet y = obj as GenericChoiceSet;
                return _options.Count.CompareTo(y._options.Count);                
            }
            throw new ApplicationException("Can only compare to other GenericChoiceSets");
        }

        public void LoadViewState(object state)
        {
            if (state != null) return;
            
            Header = string.Empty;
            _options = new List<GenericChoice>();
        }

        public object SaveViewState()
        {
            object[] state = new object[2];
            state[0] = Header;
         
            ArrayList al = new ArrayList();
            
            foreach (GenericChoice gc in _options)
            {
                al.Add(gc.SaveViewState());
            }
            state[1] = al;
            return state;
        }

        public void TrackViewState()
        {
            IsTrackingViewState = true;
        }

        public bool IsTrackingViewState { get; private set; }
    }
}
