using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.DataSource
{
    public class GenericChoicesDataSource
    {
        public GenericChoiceCollection Fetch(int chromeStyleID, int categoryCategory)
        {
            return Fetch(chromeStyleID, categoryCategory, true);
        }

        public GenericChoiceCollection Fetch(int chromeStyleID, int categoryCategory, bool lengthOrdered)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "chrome.getStyleCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", categoryCategory, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        GenericChoiceCollection gcc = new GenericChoiceCollection();
                        while (reader.Read())
                        {
                            gcc.Add(new GenericChoice(reader));
                        }
                        return gcc;
                    }
                }
            }
        }
        public static GenericChoiceCollection FetchStyleEquipment(int chromeStyleID, int categoryCategory)
        {
            
            // only way to get all equipment for chome style (including engine, trans, ect) without writing new stored proc
            
            GenericChoiceCollection gcc = new GenericChoiceCollection();

            if(chromeStyleID > 0)
            {
                using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
                {
                    con.Open();

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        // load standard equipment
                        cmd.CommandText = "chrome.getStandardEquipmentCategories";
                        cmd.CommandType = CommandType.StoredProcedure;

                        Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", categoryCategory, DbType.Int32);
                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                gcc.Add(new GenericChoice(reader));
                            }
                        }

                         // load optional equipment
                        cmd.Parameters.Clear();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "chrome.getOptionalEquipmentCategories";

                        Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", categoryCategory, DbType.Int32);
                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                gcc.Add(new GenericChoice(reader));
                            }
                        }
                    }
                }

            }

            return gcc;
        }

    }
}
