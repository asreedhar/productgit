﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.DiscountCampaigns;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public class DiscountPricingCampaignManager : IDiscountPricingCampaignManager
    {
        private readonly IDiscountPricingRepository _pricingRepository;
        private readonly IAdMessageSender _messageSender;
        private readonly IInventorySearch _inventorySearch;
        private static readonly ILog Log = LoggerFactory.GetLogger<DiscountPricingCampaignManager>();
        private const string DiscountExceedsPrice = "Discount exceeds price";

        public DiscountPricingCampaignManager(IDiscountPricingRepository pricingRepository,
            IAdMessageSender messageSender, IInventorySearch inventorySearch)
        {
            _pricingRepository = pricingRepository;
            _messageSender = messageSender;
            _inventorySearch = inventorySearch;
        }

        public DiscountPricingCampaignModel SaveCampaign(DiscountPricingCampaignModel dcp, IEnumerable<int> inventoryIds)
        {
			Log.DebugFormat("Manager save campaign");
            var campaign = _pricingRepository.SaveCampaign(dcp);

            var ids = inventoryIds as int[] ?? inventoryIds.ToArray();
            if (!ids.Any())
            {
				Log.Debug("Campaign returned with no inventory.");
                ids = GetCampaignInventory(campaign);
            }

            var inventoryDiscountPricingModels = ids.Select(x => new InventoryDiscountPricingModel
            {
                ActivatedBy = campaign.CreatedBy,
                Active = true,
                CampaignId = campaign.CampaignId,
                InventoryId = x
            }).ToArray();

			Log.DebugFormat("Saved campaign, adding {0} inventory items.", inventoryDiscountPricingModels.Length);
			// Save CampaignInventory returns a list of the inventory items that were successfully added
            campaign.InventoryList = SaveCampaignInventory(campaign, inventoryDiscountPricingModels).ToList();

			// Fire off the price change messages for the inventory in this campaign
            SendBatchPriceChangeMessages(campaign
				.InventoryList
				.Select(il => new ApplyInventoryDiscountCampaignMessage(campaign.BusinessUnitId, il.InventoryId, campaign.CreatedBy)),
				_messageSender);

			// We've just changed prices, make sure the session data is up to date.
            InventoryDataFilter.UpdateSessionData(dcp.BusinessUnitId);

            return campaign;
        }

        public DiscountPricingCampaignModel FetchCampaign(int businessUnitId, int campaignId)
        {
            var campaigns = _pricingRepository.FetchBusinessUnitPricingModels(businessUnitId, true, campaignId);
            return campaigns.FirstOrDefault();
        }

        public DiscountPricingCampaignModel FetchCampaignForInventory(int businessUnitId, int inventoryId, bool onlyActive = true)
        {
            var campaigns = _pricingRepository.FetchCampaignsByInventory(inventoryId, onlyActive);
            return campaigns.LastOrDefault();
        }

        public void ExpireDiscountCampaign(DiscountPricingCampaignModel campaign, string expiredByUser)
        {
            Log.InfoFormat("Begin expiring campaignid {0} - {1}", campaign.CampaignId, expiredByUser);
            var messages = new List<ApplyInventoryDiscountCampaignMessage>();
            
            campaign.InventoryList.ForEach(x =>
            {
                var message = ExpireDiscountInventory(campaign, x, expiredByUser);
                if(message != null)
                    messages.Add(message);
            });

            SendBatchPriceChangeMessages(messages, _messageSender);

            _pricingRepository.ExpireDiscountCampaign(campaign);
            
            Log.InfoFormat("Finished expiring campaignid {0}", campaign.CampaignId);
        }

        public void ExpireAllCampaignsForInventory(int inventoryId, string expiredByUser, bool sendMessages = false, InventoryExclusionType? exclusionType = null)
        {
	        Log.DebugFormat("Getting all campaigns for InventoryId {0}", inventoryId);
            var inventoryCampaigns = _pricingRepository.FetchCampaignsByInventory(inventoryId);
            var messages = new List<ApplyInventoryDiscountCampaignMessage>();
            if (inventoryCampaigns != null)
            {
	            Log.DebugFormat("Found {0} campaigns to expire.", inventoryCampaigns.Count);
	            inventoryCampaigns.ForEach(x =>
	            {
		            var message = ExpireDiscountInventory(
			            x,
			            x.InventoryList.First(y => y.InventoryId.Equals(inventoryId)),
			            expiredByUser, exclusionType);
		            if (message != null)
		            {
			            messages.Add(message);
		            }
	            });
            }

            if(sendMessages && messages.Any())
                SendBatchPriceChangeMessages(messages, _messageSender);
        }


	    public List<DiscountPricingCampaignModel> FetchActiveCampaignsNoInventory(int businessUnitId)
	    {
			return _pricingRepository.FetchBusinessUnitPricingModels(businessUnitId, false, null)
				.Where(dpcm => dpcm.HasBeenExpired == false)
				.ToList();
	    }

        public List<DiscountPricingCampaignModel> FetchAllCampaigns(int businessUnitId, bool onlyActive)
        {
            var campaigns = _pricingRepository.FetchBusinessUnitPricingModels(businessUnitId, true, null);
            
            if (onlyActive)
            {
                // Filter out inactive campaigns
                campaigns = campaigns.Where(x => x.HasBeenExpired.Equals(false)).ToList();
                // Filter out inventory that is currently not active in this campaign
                campaigns.ForEach(x => { x.InventoryList = x.InventoryList.Where(y => y.Active).ToList(); });
            }

            return campaigns;
        }

        public InventoryDiscountPricingModel AddInventoryToCampaign(DiscountPricingCampaignModel campaignModel, InventoryDiscountPricingModel inventoryModel)
        {
			Log.DebugFormat("Adding InventoryId to Campaign.");
            var appliedDiscountModel = new InventoryDiscountPricingModel();
            var inventoryList = new List<InventoryDiscountPricingModel> { inventoryModel };
            
			// Add this single inventory item to the existing campaign
            var savedInventory = SaveCampaignInventory(campaignModel, inventoryList);

			// Fire off the price change messages for the inventory in this campaign
			SendBatchPriceChangeMessages(savedInventory
				.Select(il => new ApplyInventoryDiscountCampaignMessage(campaignModel.BusinessUnitId, il.InventoryId, campaignModel.CreatedBy)),
				_messageSender);

            return appliedDiscountModel;
        }

        public void ApplyDiscount(int businessUnitId, int inventoryId, string username, string sentFrom, IAdMessageSender messageSender = null)
        {
            if(messageSender != null)
                messageSender.SendStyleSet(businessUnitId, inventoryId, username, sentFrom);

            // Run the command instead of sending a message so the InventoryData.Refresh that follows will get the newly calculated price.
            var cmd = new ApplyDiscountCampaignToInventoryCommand(this, _inventorySearch, businessUnitId, inventoryId, username);
            AbstractCommand.DoRun(cmd);
        }

	    public void LoadInventoryForCampaign(IEnumerable<DiscountPricingCampaignModel> discountPricingCampaignModels)
	    {
		    _pricingRepository.PopulateCampaignInventory(discountPricingCampaignModels);
	    }
        public void LoadExclusionsForCampaign(IEnumerable<DiscountPricingCampaignModel> discountPricingCampaignModels)
        {
            _pricingRepository.PopulateCampaignExclusions(discountPricingCampaignModels);
        }



        #region Private Methods

        /// <summary>
        /// Apply campaign prices to inventory.
        /// </summary>
        /// <param name="discountCampaignModel">The discount campaign model used for the inventory.</param>
        /// <param name="inventoryData">Inventory to apply campaigns to.</param>
        /// <param name="userName">The user name used for history audits.</param>
        private void ApplyCampaignPrices(DiscountPricingCampaignModel discountCampaignModel, IEnumerable<IInventoryData> inventoryData, string userName)
        {
            var calculator = CampaignPriceCalculatorFactory.GetCalculator(discountCampaignModel);

            var messages = new List<ApplyInventoryDiscountCampaignMessage>();

            foreach (var data in inventoryData)
            {
                var price = calculator.CalculatePrice(data);
                if (price > 0)
                {
                    SetSpecialPrice(data.InventoryID, price, userName);

                    messages.Add(new ApplyInventoryDiscountCampaignMessage(discountCampaignModel.BusinessUnitId,
                        data.InventoryID, discountCampaignModel.CreatedBy));
                }
                else
                {
                    var message = ExpireDiscountInventory(
                        discountCampaignModel,
                        discountCampaignModel.InventoryList.First(x => x.InventoryId.Equals(data.InventoryID)),
                        DiscountExceedsPrice);

                    if(message != null)
                        messages.Add(message);
                }
            }

            SendBatchPriceChangeMessages(messages, _messageSender);
        }

        private IEnumerable<InventoryDiscountPricingModel> SaveCampaignInventory(
			DiscountPricingCampaignModel campaignModel,
            IEnumerable<InventoryDiscountPricingModel> inventoryDiscountPricingModels)
        {
	        if (inventoryDiscountPricingModels == null || campaignModel == null)
	        {
		        Log.ErrorFormat("Called SaveCampaignInventory with null {0}", campaignModel == null ? "campaignModel" : "inventoryDiscountPricingModels");
		        return new List<InventoryDiscountPricingModel>();
	        }

            var discountPricingModels = inventoryDiscountPricingModels.ToList();

			Log.DebugFormat("Save campaign inventory for Campaign {0} InventoryIds: {1}", campaignModel.CampaignId, discountPricingModels.Select(x => x.InventoryId).ToDelimitedString(","));

			var calculator = CampaignPriceCalculatorFactory.GetCalculator(campaignModel);
			
			// _inventorySearch is a set of predicates for matching inventory items to campaign filters
			_inventorySearch.Initialize(campaignModel.BusinessUnitId, campaignModel.CampaignFilter.VehicleType);
			var inventoryData = _inventorySearch.ForInventoryIds(discountPricingModels.Select(x => x.InventoryId).ToList()).ToList();
	        
			Log.DebugFormat("Found {0} inventoryids for campaign {1}. {2}", inventoryData.Count, campaignModel.CampaignId, inventoryData.Select(x => x.InventoryID).ToDelimitedString(","));

	        var removeList = new List<int>();

			foreach (var dpm in discountPricingModels)
	        {
                // first make sure this inventory item is not in another campaign
				Log.DebugFormat("Expiring all campaigns for InventoryId: {0}", dpm.InventoryId);
                ExpireAllCampaignsForInventory(dpm.InventoryId, dpm.ActivatedBy);

		        var modelInventoryData = inventoryData.FirstOrDefault(id => id.InventoryID == dpm.InventoryId);
		        if (modelInventoryData != null)
		        {
			        var price = calculator.CalculatePrice(modelInventoryData);
					Log.DebugFormat("Calculated SpecialPrice: {0}", price);
			        if (price > 0)
			        {
				        Log.InfoFormat("Adding inventory to campaign with price: {0}, {1}, {2}", dpm.InventoryId, campaignModel.CampaignId, price);
				        _pricingRepository.SaveInventory(dpm, price);
			        }
			        else
			        {
				        Log.WarnFormat("Tried to add inventory to campaign, but price was bad: {0}, {1}, {2}", dpm.InventoryId, campaignModel.CampaignId, price);
				        removeList.Add(dpm.InventoryId);
			        }
		        }
		        else
		        {
					Log.WarnFormat("Tried to add inventory to a campaign, but inventory data search returned no results: {0}, {1}, {2}", dpm.InventoryId, campaignModel.CampaignId, campaignModel.CampaignFilter);
					removeList.Add(dpm.InventoryId);
		        }
	        }

	        discountPricingModels.RemoveAll(dpm => removeList.Contains(dpm.InventoryId));
            return discountPricingModels;
        }

        private ApplyInventoryDiscountCampaignMessage ExpireDiscountInventory(DiscountPricingCampaignModel campaign,
            InventoryDiscountPricingModel inventoryDiscountPricingModel, string expiredByUser, InventoryExclusionType? exclusionType = null)
        {
            Log.InfoFormat("Expiring discount inventory: {0} - {1} - {2}", campaign.CampaignId,
                inventoryDiscountPricingModel.InventoryId, expiredByUser);

            if (inventoryDiscountPricingModel.Active == false)
            {
                Log.InfoFormat("Inventory {1} has already been deactivated for campaign {0} by {2}",
                    inventoryDiscountPricingModel.CampaignId, inventoryDiscountPricingModel.InventoryId,
                    inventoryDiscountPricingModel.DeactivatedBy);
                return null;
            }
			
			// Set the expiration data on this record.
            var deactivatedInventory = _pricingRepository.DeactivateInventoryForDiscountCampaign(campaign.CampaignId, expiredByUser,
                inventoryDiscountPricingModel.InventoryId, exclusionType);

            // just set the log message
            foreach (var vehicle in deactivatedInventory)
            {
                if (vehicle.OriginalNewCarPrice.HasValue)
                    Log.InfoFormat("Resetting Inventory to original price {0} - {1}", vehicle.InventoryId,
                                   vehicle.OriginalNewCarPrice);
                else
                    Log.InfoFormat("Inventory did not have original price {0}", vehicle.InventoryId);
            }


            //// If they had a manual price before this campaign was created, set that price back on this vehicle.
            //if (inventoryDiscountPricingModel.OriginalNewCarPrice.HasValue)
            //{
            //    Log.InfoFormat("Resetting Inventory to original price {0} - {1}",
            //        inventoryDiscountPricingModel.InventoryId, inventoryDiscountPricingModel.OriginalNewCarPrice.Value);
            //    SetSpecialPrice(inventoryDiscountPricingModel.InventoryId, inventoryDiscountPricingModel.OriginalNewCarPrice.Value);
            //}
            //else
            //{
            //    Log.InfoFormat("Inventory did not have original price {0}", inventoryDiscountPricingModel.InventoryId);

            //    var calculator = CampaignPriceCalculatorFactory.GetCalculator(campaign);
            //    _inventorySearch.Initialize(campaign.BusinessUnitId, campaign.CampaignFilter.VehicleType);

            //    // re calculate the discounted price. 
            //    var price =
            //        calculator.CalculatePrice(
            //            _inventorySearch.ForInventoryIds(new List<int> {inventoryDiscountPricingModel.InventoryId})
            //                .FirstOrDefault());
            //    // If the discounted price is > 0, remove it?
            //    // Why wouldn't we always remove it?
            //    // because a manual price change might not match the special price we are trying to remove here.
            //    // but this still seems wrong.
            //    if (price > 0)
            //    {
            //        Log.InfoFormat("Removing SpecialPrice {0} from InventoryId: {1}", price, inventoryDiscountPricingModel.InventoryId);
            //        RemoveSpecialPrice(inventoryDiscountPricingModel.InventoryId, price);
            //    }
				
            //    Log.WarnFormat("SpecialPrice {0} for InventoryId: {1} does not match, will not be removed.", price, inventoryDiscountPricingModel.InventoryId);
            //}

            Log.InfoFormat("Sending autoapprove message for buid: {0} , inventory: {1}", campaign.BusinessUnitId,
                inventoryDiscountPricingModel.InventoryId);

            Log.InfoFormat("Finished expiring discount inventory {0}", inventoryDiscountPricingModel.InventoryId);

            return new ApplyInventoryDiscountCampaignMessage(campaign.BusinessUnitId,
                inventoryDiscountPricingModel.InventoryId, campaign.CreatedBy);
        }

        internal protected virtual void RemoveSpecialPrice(int inventoryId, decimal price)
        {
            var priceCommand = new RemoveSpecialPrice(inventoryId, price);
            AbstractCommand.DoRun(priceCommand);
        }

        internal protected virtual void SetSpecialPrice(int inventoryId, decimal priceDecimal, string userName)
        {
            var priceCommand = new SetSpecialPrice(inventoryId, priceDecimal, userName);
            AbstractCommand.DoRun(priceCommand);
        }

        private int[] GetCampaignInventory(DiscountPricingCampaignModel dcp)
        {
			Log.Debug("Searching inventory data for vehicles that match campaign filter.");
            _inventorySearch.Initialize(dcp.BusinessUnitId, dcp.CampaignFilter.VehicleType);

            return _inventorySearch.ByCampaignFilter(dcp.CampaignFilter).Select(x => x.InventoryID).ToArray();
        }

        /// <summary>
        /// Inventory may have several auto approve messages generated by this process. We only want to send one message per. So batch them all together and send that off.
        /// </summary>
        private static void SendBatchPriceChangeMessages(IEnumerable<ApplyInventoryDiscountCampaignMessage> messageList, IAdMessageSender messageSender)
        {
            var finalMessages = messageList
                .GroupBy(m => m.InventoryId)
                .Select(am => am.First());

            var messages = finalMessages as ApplyInventoryDiscountCampaignMessage[] ?? finalMessages.ToArray();
            if (messages.Any())
            {
                var message = new BatchPriceChangeMessage(
                    messages.First().BusinessUnitId,
                    messages.Select(x => x.InventoryId),
                    messages.First().ActivatedBy
                );

                Log.InfoFormat("Price Change messages sent for businessUnitId: {0}, and inventoryIds: {1}", message.BusinessUnitId, string.Join(", ", message.InventoryIds));
                messageSender.SendBatchPriceChange(message, MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }
        }

        #endregion Private Methods
    }
}