﻿using System;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    internal class InvoiceCampaignPriceCalculator : ICampaignPriceCalculator
    {
        private readonly decimal _totalDiscount;
        
        public InvoiceCampaignPriceCalculator(DiscountPricingCampaignModel campaign)
        {
            _totalDiscount = campaign.DealerDiscount + campaign.ManufacturerRebate;
        }

        public decimal CalculatePrice(IInventoryData inventoryData)
        {
            return inventoryData != null ? Calculate(inventoryData.UnitCost) : 0;
        }

        private decimal Calculate(decimal unitCost)
        {
            if (unitCost > 0 && Math.Abs(_totalDiscount) < unitCost)
                return unitCost + _totalDiscount;

            return 0;
        }
    }
}