﻿namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    /// <summary>
    /// this should match with the table merchandising.merchandising.DiscountPricingCampaignExclusionType
    /// </summary>
    public enum InventoryExclusionType
    {
        ManualReprice = 1
    }
}
