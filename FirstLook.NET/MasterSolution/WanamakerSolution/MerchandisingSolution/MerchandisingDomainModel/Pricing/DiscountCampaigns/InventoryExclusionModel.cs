﻿using System;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{

    public class InventoryExclusionModel
    {
        public int InventoryId { get; set; }
        public InventoryExclusionType InventoryExclusionType { get; set; }
        public int CampaignId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
