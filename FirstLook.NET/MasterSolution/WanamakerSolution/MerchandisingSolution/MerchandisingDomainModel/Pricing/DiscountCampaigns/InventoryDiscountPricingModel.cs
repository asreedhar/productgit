﻿using System.Data;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public class InventoryDiscountPricingModel
    {
		public InventoryDiscountPricingModel() {}

	    public InventoryDiscountPricingModel(IDataReader reader)
	    {
		    Active = reader.GetBoolean("Active");
		    CampaignId = reader.GetInt32("CampaignID");
		    InventoryId = reader.GetInt32("InventoryId");
		    OriginalNewCarPrice = Database.GetNullableDecimal(reader, "OriginalNewCarPrice");
		    ActivatedBy = Database.GetNullableString(reader, "ActivatedBy");
		    DeactivatedBy = Database.GetNullableString(reader, "DeactivatedBy");
	    }
        public int InventoryId { get; set; }
        public decimal? OriginalNewCarPrice { get; set; }
        public bool Active { get; set; }
        public int CampaignId { get; set; }
        public string ActivatedBy { get; set; }
        public string DeactivatedBy { get; set; }
    }
}