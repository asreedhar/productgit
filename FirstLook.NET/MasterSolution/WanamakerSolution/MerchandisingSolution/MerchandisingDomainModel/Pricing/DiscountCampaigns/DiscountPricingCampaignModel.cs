﻿using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public class DiscountPricingCampaignModel
    {
        public int CampaignId { get; set; }
        public int BusinessUnitId { get; set; }
        public PriceBaseLine DiscountType { get; set; }
        public decimal DealerDiscount { get; set; }
        public decimal ManufacturerRebate { get; set; }
        
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string CreatedBy { get; set; }
        public List<InventoryDiscountPricingModel> InventoryList { get; set; }
        public List<InventoryExclusionModel> InventoryExclusionList { get; set; }
        public CampaignFilter CampaignFilter { get; set; }
        public bool HasBeenExpired { get; set; }
    }
}