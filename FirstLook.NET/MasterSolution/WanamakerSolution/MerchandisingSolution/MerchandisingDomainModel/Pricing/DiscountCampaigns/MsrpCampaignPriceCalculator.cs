﻿using System;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    internal class MsrpCampaignPriceCalculator : ICampaignPriceCalculator
    {
        private readonly decimal _totalDiscount;

        public MsrpCampaignPriceCalculator(DiscountPricingCampaignModel campaign)
        {
            _totalDiscount = campaign.DealerDiscount + campaign.ManufacturerRebate;
        }

        public decimal CalculatePrice(IInventoryData inventoryData)
        {
            return inventoryData != null ? Calculate(inventoryData.MSRP) : 0;
        }

        private decimal Calculate(decimal msrp)
        {
            if (msrp > 0 && Math.Abs(_totalDiscount) < msrp)
                return msrp + _totalDiscount;

            return 0;
        }
    }
}