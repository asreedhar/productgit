﻿namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public enum PriceBaseLine
    {
        Msrp,
        Invoice
    }
}