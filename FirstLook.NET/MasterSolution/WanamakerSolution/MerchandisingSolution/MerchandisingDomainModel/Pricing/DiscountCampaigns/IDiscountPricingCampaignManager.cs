﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public interface IDiscountPricingCampaignManager
    {
        DiscountPricingCampaignModel SaveCampaign(DiscountPricingCampaignModel dcp, IEnumerable<int> inventoryIds);
        DiscountPricingCampaignModel FetchCampaign(int businessUnitId, int campaignId);
        DiscountPricingCampaignModel FetchCampaignForInventory(int businessUnitId, int inventoryId, bool onlyActive = true);
        
        void ExpireDiscountCampaign(DiscountPricingCampaignModel campaign, string expiredByUser);
        void ExpireAllCampaignsForInventory(int inventoryId, string expiredByUser, bool sendMessages = false, InventoryExclusionType? exclusionType = null);
        List<DiscountPricingCampaignModel> FetchAllCampaigns(int businessUnitId, bool onlyActive);
		List<DiscountPricingCampaignModel> FetchActiveCampaignsNoInventory(int businessUnitId);

        InventoryDiscountPricingModel AddInventoryToCampaign(DiscountPricingCampaignModel campaignModel, InventoryDiscountPricingModel inventoryModel);
	    void LoadInventoryForCampaign(IEnumerable<DiscountPricingCampaignModel> discountPricingCampaignModels);
        void LoadExclusionsForCampaign(IEnumerable<DiscountPricingCampaignModel> discountPricingCampaignModels);

        void ApplyDiscount(int businessUnit, int inventoryId, string username, string sentFrom, IAdMessageSender messageSender = null);
    }
}