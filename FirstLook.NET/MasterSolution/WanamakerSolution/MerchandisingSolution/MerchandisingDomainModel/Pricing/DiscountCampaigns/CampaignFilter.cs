﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Extensions;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    [Serializable]
    public class CampaignFilter
    {
        public const string Allyears = "All";
        // Existing campaigns do not have VehicleType so default to new
        private VehicleType _vehicleType = VehicleType.New;

        private IEnumerable<string> _year = new List<string>();
        private IEnumerable<string> _make = new List<string>();
        private IEnumerable<string> _model = new List<string>();
        private IEnumerable<string> _trim = new List<string>();
        private IEnumerable<string> _bodyStyle = new List<string>();
        private IEnumerable<string> _engineDescription = new List<string>();
        private IEnumerable<CampaignEngineDescriptionInfo> _engineDescriptionInfo = new List<CampaignEngineDescriptionInfo>();

        public CampaignFilter()
        {
            VehicleType = VehicleType.New;
            Year = new List<string>();
            Make = new List<string>();
            Model = new List<string>();
            Trim = new List<string>();
            CampaignEngineDescription = new List<CampaignEngineDescriptionInfo>();
            BodyStyle = new List<string>();
        }

        public VehicleType VehicleType
        {
            get { return _vehicleType ?? VehicleType.New; }
            set { _vehicleType = value; }
        }

        public IEnumerable<string> Year
        {
            get { return _year ?? new List<string>(); }
            set { _year = value; }
        }

        public IEnumerable<string> Make
        {
            get { return _make ?? new List<string>(); }
            set { _make = value; }
        }

        public IEnumerable<string> Model
        {
            get { return _model ?? new List<string>(); }
            set { _model = value; }
        }

        public IEnumerable<string> Trim
        {
            get { return _trim ?? new List<string>(); }
            set { _trim = value; }
        }

        public IEnumerable<string> BodyStyle
        {
            get { return _bodyStyle ?? new List<string>(); }
            set { _bodyStyle = value; }
        }

        // version 1, engine description is just a string
        public IEnumerable<string> EngineDescription
        {
            get { return _engineDescription ?? new List<string>(); }
            set { _engineDescription = value; }
        }

        //version 2, engine descriptions are more robust
        public IEnumerable<CampaignEngineDescriptionInfo> CampaignEngineDescription
        {
            get { return _engineDescriptionInfo ?? new List<CampaignEngineDescriptionInfo>(); }
            set { _engineDescriptionInfo = value; }
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            //builder.Append((VehicleType == VehicleType.Both) ? "New or Used" : VehicleType.StringValue);

            if (Year.Any(x => x != null))
                builder.Append(String.Join(",", Year));
            if (Make.Any(x => x != null))
                builder.Append(" - " + String.Join(",", Make));
            if (Model.Any(x => x != null))
                builder.Append(" - " + String.Join(",", Model));
            if (Trim.Any(x => x != null))
                builder.Append(" - " + String.Join(",", Trim));
            if (BodyStyle.Any(x => x != null))
                builder.Append(" - " + SmartTruncateJoin(BodyStyle));
            if (CampaignEngineDescription.Any(x => x != null ))
            {
                builder.Append(" - " + SmartTruncateJoin(CampaignEngineDescription.Select(x => x.Description)));
            }

            return builder.ToString();
        }

        private static string SmartTruncateJoin(IEnumerable<string> fieldsList )
        {
            var sb = fieldsList.Select(x => x.SmartTruncate(18));
            return String.Join(",", sb);
        }

    }
}