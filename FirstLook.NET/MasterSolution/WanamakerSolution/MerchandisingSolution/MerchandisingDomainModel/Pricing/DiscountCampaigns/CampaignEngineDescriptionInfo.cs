﻿namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public class CampaignEngineDescriptionInfo
    {
        public string Description { get; set; }
        public bool IsStandard { get; set; }
        public string OptionCode { get; set; }
    }
}