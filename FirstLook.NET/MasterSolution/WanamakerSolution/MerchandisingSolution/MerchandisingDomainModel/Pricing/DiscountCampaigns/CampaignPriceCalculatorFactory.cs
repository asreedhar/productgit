﻿using System;
using System.ComponentModel;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public static class CampaignPriceCalculatorFactory
    {
        public static ICampaignPriceCalculator GetCalculator(DiscountPricingCampaignModel campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign", @"You must provide a valid DiscountPricingCampaignModel");
            
            switch (campaign.DiscountType)
            {
                case PriceBaseLine.Invoice:
                    return new InvoiceCampaignPriceCalculator(campaign);
                    
                case PriceBaseLine.Msrp:
                    return new MsrpCampaignPriceCalculator(campaign);
                default:
                    throw new InvalidEnumArgumentException("campaign", (int) campaign.DiscountType, typeof(PriceBaseLine));
            }
        }
    }
}