﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using FirstLook.Common.Core.Extensions;
using log4net;
using MAX.Caching;
using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public class DiscountPricingRepository : IDiscountPricingRepository
    {
	    private readonly ICacheWrapper _cacheWrapper;
	    private readonly ICacheKeyBuilder _cacheKeyBuilder;

	    private const int CacheMinutes = 20;

	    #region Logging

	    protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

	    #endregion Logging

	    public DiscountPricingRepository(ICacheWrapper cacheWrapper, ICacheKeyBuilder cacheKeyBuilder)
	    {
		    _cacheWrapper = cacheWrapper;
		    _cacheKeyBuilder = cacheKeyBuilder;
	    }

	    #region Implement Interface
        public List<DiscountPricingCampaignModel> FetchBusinessUnitPricingModels(int businessUnitId, bool includeInventory, int? campaignId)
        {
            var campaigns = new List<DiscountPricingCampaignModel>();
	        var cacheKeyList = new List<string>
	        {
		        "DiscountCampaigns",
		        businessUnitId.ToString(CultureInfo.InvariantCulture),
		        includeInventory.ToString()
	        };
	        if (campaignId.HasValue)
		        cacheKeyList.Add(campaignId.Value.ToString(CultureInfo.InvariantCulture));

			var cacheKey = _cacheKeyBuilder.CacheKey(cacheKeyList);

            var cachedCampaigns = _cacheWrapper.Get<List<DiscountPricingCampaignModel>>(cacheKey);

			// Check cache.
			if (cachedCampaigns != null && (cachedCampaigns).Any())
	        {
		        if (campaignId.HasValue)
			        campaigns.AddRange(cachedCampaigns.Where(discountCampaign => discountCampaign.CampaignId == campaignId));
		        else
			        campaigns = cachedCampaigns;

				// Do not count a cache hit that returns nothing.
				if (campaigns.Any())
				{
					Log.DebugFormat("DiscountCampaign cache.hit");
					return campaigns;
				}
	        }

			Log.DebugFormat("DiscountCampaign cache.miss");
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
					Log.DebugFormat("calling merchandising.DiscountPricingCampaign#Fetch buid: {0} campaignId: {1}", businessUnitId, campaignId.HasValue ? campaignId.Value.ToString() : "");
                    cmd.CommandText = "merchandising.DiscountPricingCampaign#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddParameterWithValue("@BusinessUnitId", businessUnitId);
                    if(campaignId.HasValue)
                        cmd.AddParameterWithValue("@CampaignId",campaignId.Value);
                    
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            campaigns.Add(CampaignModel(reader));
                        }
                    }
                }
            }
			if(includeInventory)
				PopulateCampaignInventory(campaigns);

			if(campaigns.Any())
				_cacheWrapper.Add(cacheKey, campaigns, new DateTimeOffset(DateTime.Now.AddMinutes(CacheMinutes)));

            return campaigns;
        }

	    

	    public List<DiscountPricingCampaignModel> FetchCampaignsByInventory(int inventoryId, bool onlyActive = true)
        {
            var campaigns = new List<DiscountPricingCampaignModel>();
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
					Log.DebugFormat("calling merchandising.DiscountPricingCampaignInventory#FetchCampaign inventoryid: {0} only active {1}", inventoryId, onlyActive );
                    cmd.CommandText = "merchandising.DiscountPricingCampaignInventory#FetchCampaign";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddParameterWithValue("@InventoryId",inventoryId);
					cmd.AddParameterWithValue("@OnlyActive", onlyActive);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var campaign = CampaignModel(reader);
                            campaign.InventoryList.Add(InventoryCampaignModel(reader));
                            campaigns.Add(campaign);
                        }
                    }
                }
            }

            return campaigns;
        }

        public DiscountPricingCampaignModel SaveCampaign(DiscountPricingCampaignModel model)
        {
			Log.DebugFormat("Saving campaign model: {0}", model.ToJson());
            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (model.CampaignId > 0)
                    {
						Log.DebugFormat("merchandising.DiscountPricingCampaign#Update {0}, {1}", model.BusinessUnitId, model.CampaignId);
                        cmd.CommandText = "merchandising.DiscountPricingCampaign#Update";
                        cmd.AddParameterWithValue("@CampaignId",model.CampaignId);
                    }
                    else
                    {
						Log.DebugFormat("merchandising.DiscountPricingCampaign#Insert {0}, {1}", model.BusinessUnitId, model.CampaignFilter.ToJson());
	                    cmd.CommandText = "merchandising.DiscountPricingCampaign#Insert";
                    }

                    cmd.AddParameterWithValue("@BusinessUnitId",model.BusinessUnitId);
                    cmd.AddParameterWithValue("@DiscountType",(int)model.DiscountType);
                    cmd.AddParameterWithValue("@DealerDiscount", model.DealerDiscount);
                    cmd.AddParameterWithValue("@ManufacturerRebate", model.ManufacturerRebate);
                    cmd.AddParameterWithValue("@ExpirationDate",model.ExpirationDate);
                    cmd.AddParameterWithValue("@CreatedBy",model.CreatedBy);
                    cmd.AddParameterWithValue("@CampaignFilter",model.CampaignFilter.ToJson());

                    using (var reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            model = CampaignModel(reader);    
                        }
                    }
                }
            }

			// We've just saved campaign info, do not read inventory information from the cache.
			Log.DebugFormat("Saved campaign. Loading inventory items list.");
            LoadCampaignInventory(model, false);

            return model;
        }

        public InventoryDiscountPricingModel SaveInventory(InventoryDiscountPricingModel model, decimal price)
        {
            if(model.CampaignId == 0)
                throw new ArgumentException(@"A valid Discount CampaignId is required", "model");

            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
					Log.DebugFormat("merchandising.DiscountPricingCampaignInventory#Insert CampaignId: {0} InventoryId: {1}", model.CampaignId, model.InventoryId);
                    cmd.CommandText = "merchandising.DiscountPricingCampaignInventory#Insert";
                    cmd.AddParameterWithValue("@CampaignId", model.CampaignId);
                    cmd.AddParameterWithValue("@InventoryId", model.InventoryId);
                    cmd.AddParameterWithValue("@ActivatingUser", model.ActivatedBy);
					cmd.AddParameterWithValue("@SpecialPrice", price);

                    using (var reader = cmd.ExecuteReader())
                    while(reader.Read())
                        model = InventoryCampaignModel(reader);
                }
            }
			var cacheKey =
					_cacheKeyBuilder.CacheKey(new[] { "DiscountCampaignInventory", model.CampaignId.ToString(CultureInfo.InvariantCulture) });

			// Remove campaign inventory from the cache.
	        _cacheWrapper.Set(cacheKey, null);
            return model;
        }

        public void ExpireDiscountCampaign(DiscountPricingCampaignModel model)
        {
            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
					Log.DebugFormat("merchandising.DiscountPricingCampaign#Expire BusinessUnitId: {0} CampaignId: {1}", model.BusinessUnitId, model.CampaignId);
                    cmd.CommandText = "merchandising.DiscountPricingCampaign#Expire";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddParameterWithValue("@BusinessUnitId",model.BusinessUnitId); 
                    cmd.AddParameterWithValue("@CampaignId",model.CampaignId);
                    
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<InventoryDiscountPricingModel> DeactivateInventoryForDiscountCampaign(int campaignId, string deactivatingUser, int? inventoryId = null, InventoryExclusionType? exclusionType = null)
        {
            var model = new List<InventoryDiscountPricingModel>();
			Log.DebugFormat("Deactivating inventory for campaign {0}", campaignId);
            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
	                Log.DebugFormat("merchandising.DiscountPricingCampaignInventory#Deactivate CampaignId: {0} InventoryId: {1} User: {2}", campaignId, inventoryId, deactivatingUser);
                    cmd.CommandText = "merchandising.DiscountPricingCampaignInventory#Deactivate";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddParameterWithValue("@CampaignId",campaignId);
                    cmd.AddParameterWithValue("@DeactivatingUser",deactivatingUser);
                    if(inventoryId.HasValue)
                        cmd.AddParameterWithValue("@InventoryId",inventoryId);
                    if(exclusionType.HasValue)
                        cmd.AddParameterWithValue("@ExclusionId", (byte)exclusionType);

                    using(var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            model.Add(InventoryCampaignModel(reader));
                    }
                }
            }

            return model;
        }

        public List<DiscountPricingCampaignModel> GetExpiredDiscountCampaigns()
        {
            var campaigns = new List<DiscountPricingCampaignModel>();
            
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
					Log.DebugFormat("merchandising.DiscountPricingCampaign#FetchDueToExpire ExpireDate: {0}", DateTime.Today.Date);
                    cmd.CommandText = "merchandising.DiscountPricingCampaign#FetchDueToExpire";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddParameterWithValue("ExpireDate",DateTime.Today);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            campaigns.Add(CampaignModel(reader));
                        }
                    }
                }
            }

            PopulateCampaignInventory(campaigns);

            return campaigns;
        }

        public int[] GetBusinessUnitsWithActiveCampaigns()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                sqlConnection.Open();
				Log.DebugFormat("GetBusinessUnitsWithActiveCampaigns");
                const string query = @"select distinct(businessunitid) from merchandising.DiscountPricingCampaign where HasBeenExpired = 0";

                var businessUnitIds = sqlConnection.Query<int>(query).ToArray();
                sqlConnection.Close();
				Log.DebugFormat("Found {0} business units with active campaigns.", businessUnitIds.Length);
                return businessUnitIds;
            }
        }
        

        #endregion Implement Interface



        #region Private Methods
        private static InventoryDiscountPricingModel InventoryCampaignModel(IDataRecord reader)
        {
            return new InventoryDiscountPricingModel
            {
                Active = reader.GetBoolean("Active"),
                CampaignId = reader.GetInt32("CampaignID"),
                InventoryId = reader.GetInt32("InventoryId"),
                OriginalNewCarPrice = Database.GetNullableDecimal(reader, "OriginalNewCarPrice"),
                ActivatedBy = Database.GetNullableString(reader, "ActivatedBy"),
                DeactivatedBy = Database.GetNullableString(reader,"DeactivatedBy")
            };
        }

        public void PopulateCampaignInventory(IEnumerable<DiscountPricingCampaignModel> campaignModels)
        {
            foreach (var model in campaignModels)
            {
                LoadCampaignInventory(model);
            }
        }

        public void PopulateCampaignExclusions(IEnumerable<DiscountPricingCampaignModel> campaignModels)
        {
            foreach (var model in campaignModels)
            {
                LoadCampaignExclusions(model);
            }
        }

        private void LoadCampaignInventory(DiscountPricingCampaignModel model, bool readFromCache = true)
        {
            var cacheKey =
                    _cacheKeyBuilder.CacheKey(new[] { "DiscountCampaignInventory", model.CampaignId.ToString(CultureInfo.InvariantCulture) });

            if (readFromCache)
            {
                var cachedCampaigns = _cacheWrapper.Get<List<InventoryDiscountPricingModel>>(cacheKey);

                // Check cache.
                if (cachedCampaigns != null)
                {
                    var inventoryList = cachedCampaigns;
                    if (inventoryList.Any())
                    {
                        Log.DebugFormat("DiscountCampaignInventory cache.hit");
                        model.InventoryList = inventoryList;
                        return;
                    }
                }
                Log.DebugFormat("DiscountCampaignInventory cache.miss");
            }
            else
            {
                Log.Debug("Forced skip cache");
            }

            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    Log.DebugFormat("DiscountPricingCampaign#FetchAffectedInventory {0} {1}", model.BusinessUnitId, model.CampaignId);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[merchandising].[DiscountPricingCampaign#FetchAffectedInventory]";
                    cmd.AddParameterWithValue("@BusinessUnitId", model.BusinessUnitId);
                    cmd.AddParameterWithValue("@CampaignId", model.CampaignId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            model.InventoryList.Add(InventoryCampaignModel(reader));
                    }
                }
            }
            if (model.InventoryList.Any())
            {
                Log.DebugFormat("Caching inventory list for campaign {0}. List count {1}", model.CampaignId, model.InventoryList.Count);
                _cacheWrapper.Set(cacheKey, model.InventoryList, new DateTimeOffset(DateTime.Now.AddMinutes(CacheMinutes)));
            }
        }

        private void LoadCampaignExclusions(DiscountPricingCampaignModel model, bool readFromCache = true)
        {
            var cacheKey =
                    _cacheKeyBuilder.CacheKey(new[] { "DiscountCampaignExclusion", model.CampaignId.ToString(CultureInfo.InvariantCulture) });

            if (readFromCache)
            {
                var cachedCampaigns = _cacheWrapper.Get <List<InventoryExclusionModel>>(cacheKey);

                // Check cache.
                if (cachedCampaigns != null)
                {
                    var exclusionList = cachedCampaigns;
                    if (exclusionList.Any())
                    {
                        Log.DebugFormat("DiscountCampaignExclusion cache.hit");
                        model.InventoryExclusionList = exclusionList;
                        return;
                    }
                }
                Log.DebugFormat("DiscountCampaignExclusion cache.miss");
            }
            else
            {
                Log.Debug("Forced skip cache");
            }

            // no cache available ... load that *)*(*&*!!!
            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    Log.DebugFormat("Loading discount campaign exclusions from database for {0} {1}", model.BusinessUnitId, model.CampaignId);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"
                                    SELECT DiscountPricingCampaignExclusion.CampaignID, DiscountPricingCampaignExclusion.InventoryId, ExclusionId, DiscountPricingCampaignExclusion.CreationDate, DiscountPricingCampaignExclusion.CreatedBy
	                                    FROM merchandising.DiscountPricingCampaignExclusion
	                                    INNER JOIN merchandising.DiscountPricingCampaign ON DiscountPricingCampaignExclusion.CampaignID = DiscountPricingCampaign.CampaignID
	                                    WHERE DiscountPricingCampaignExclusion.CampaignID = @CampaignId
	                                    AND DiscountPricingCampaign.BusinessUnitId = @BusinessUnitId
                                        ";
                    
                    cmd.AddParameterWithValue("@BusinessUnitId", model.BusinessUnitId);
                    cmd.AddParameterWithValue("@CampaignId", model.CampaignId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            model.InventoryExclusionList.Add(
                                new InventoryExclusionModel { 
                                                                CampaignId = Convert.ToInt32(reader["CampaignID"]), 
                                                                InventoryId = Convert.ToInt32(reader["InventoryId"]),
                                                                InventoryExclusionType = (InventoryExclusionType)Convert.ToByte(reader["ExclusionId"]),
                                                                CreatedBy = reader["CreatedBy"].ToString(),
                                                                CreationDate = Convert.ToDateTime(reader["CreationDate"])
                                                            }
                                                    );

                        }
                    }
                }
            }
            if (model.InventoryExclusionList.Any())
            {
                Log.DebugFormat("Caching exclusion list for campaign {0}. List count {1}", model.CampaignId, model.InventoryExclusionList.Count);
                _cacheWrapper.Set(cacheKey, model.InventoryExclusionList, new DateTimeOffset(DateTime.Now.AddMinutes(CacheMinutes)));
            }
        }


        private static DiscountPricingCampaignModel CampaignModel(IDataRecord reader)
        {
            return new DiscountPricingCampaignModel
            {
                BusinessUnitId = reader.GetInt32("BusinessUnitId"),
                CampaignId = reader.GetInt32("CampaignID"),
                CreatedBy = reader.GetString("CreatedBy"),
                CreationDate = reader.GetDateTime("CreationDate"),
                DiscountType = (PriceBaseLine)reader.GetInt32("DiscountType"),
                DealerDiscount = reader.GetDecimal("DealerDiscount"),
                ManufacturerRebate = reader.GetDecimal("ManufacturerRebate"),
                ExpirationDate = reader.GetDateTime("ExpirationDate"),
                InventoryList = new List<InventoryDiscountPricingModel>(),
                InventoryExclusionList = new List<InventoryExclusionModel>(),
                CampaignFilter = reader.GetObject<CampaignFilter>("CampaignFilter"),
                HasBeenExpired = !reader.IsDBNull("HasBeenExpired") && reader.GetBoolean("HasBeenExpired")
            };
        }

        #endregion Private Methods
    }
}