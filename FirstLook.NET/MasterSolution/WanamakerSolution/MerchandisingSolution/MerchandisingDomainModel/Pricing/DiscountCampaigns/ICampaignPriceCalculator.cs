﻿using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public interface ICampaignPriceCalculator
    {
        decimal CalculatePrice(IInventoryData inventoryData);
    }
}