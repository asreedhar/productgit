﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns
{
    public interface IDiscountPricingRepository
    {
        List<DiscountPricingCampaignModel> FetchBusinessUnitPricingModels(int businessUnitId, bool includeInventory, int? campaignId);
        List<DiscountPricingCampaignModel> FetchCampaignsByInventory(int inventoryId, bool onlyActive = true);
	    void PopulateCampaignInventory(IEnumerable<DiscountPricingCampaignModel> campaignModels);
        void PopulateCampaignExclusions(IEnumerable<DiscountPricingCampaignModel> campaignModels);

        DiscountPricingCampaignModel SaveCampaign(DiscountPricingCampaignModel model);
        InventoryDiscountPricingModel SaveInventory(InventoryDiscountPricingModel model, decimal price);

        void ExpireDiscountCampaign(DiscountPricingCampaignModel model); 
        
        // Deactivate inventory for a campaign
        List<InventoryDiscountPricingModel> DeactivateInventoryForDiscountCampaign(int campaignId, string deactivatingUser, int? inventoryId = null, InventoryExclusionType? exclusionType = null);
        
        
        List<DiscountPricingCampaignModel> GetExpiredDiscountCampaigns();

        int[] GetBusinessUnitsWithActiveCampaigns();
    }
}