using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Pricing
{
    public class PricingData
    {
        public enum PricingRisk : int
        {
            OverPriced = 0,
            Underpriced,
            AtMarket
        }

        public int Risk
        {
            get { return (int)_risk; }
            set { _risk = (PricingRisk)value; }
        }

        private PricingRisk _risk;
        private int unitCost;
        private int listPrice;
        private int minPctMarketAvg;
        private int maxPctMarketAvg;
        private int minGrossProfit;
        private int minPctMarketValue;
        private int maxPctMarketValue;
        private int pctAvgMarketPrice;
        private int minComparableMarketPrice;
        private int maxComparableMarketPrice;
        private int numListings;
        private int numComparableListings;
        private int? marketDaySupply;

        public PricingData(int unitCost, int listPrice, int minPctMarketAvg, int maxPctMarketAvg, int minGrossProfit, int minPctMarketValue, int maxPctMarketValue, int pctAvgMarketPrice, int minComparableMarketPrice, int maxComparableMarketPrice, int numListings, int numComparableListings, int marketDaySupply)
        {
            this.unitCost = unitCost;
            this.listPrice = listPrice;
            this.minPctMarketAvg = minPctMarketAvg;
            this.maxPctMarketAvg = maxPctMarketAvg;
            this.minGrossProfit = minGrossProfit;
            this.minPctMarketValue = minPctMarketValue;
            this.maxPctMarketValue = maxPctMarketValue;
            this.pctAvgMarketPrice = pctAvgMarketPrice;
            this.minComparableMarketPrice = minComparableMarketPrice;
            this.maxComparableMarketPrice = maxComparableMarketPrice;
            this.numListings = numListings;
            this.numComparableListings = numComparableListings;
            this.marketDaySupply = marketDaySupply;
        }

        public int UnitCost
        {
            get { return unitCost; }
        }

        public int ListPrice
        {
            get { return listPrice; }
        }

        public int MinPctMarketAvg
        {
            get { return minPctMarketAvg; }
        }

        public int MaxPctMarketAvg
        {
            get { return maxPctMarketAvg; }
        }

        public int? MarketDaySupply
        {
            get { return marketDaySupply; }
            set { marketDaySupply = value; }
        }
        public string MarketDaySupplyDisplay
        {
            get
            {
                if (MarketDaySupply.HasValue)
                {
                    return MarketDaySupply.Value.ToString();
                }
                return "--";
            }
        }

        public int MinGrossProfit
        {
            get { return minGrossProfit; }
        }

        public int MinPctMarketValue
        {
            get { return minPctMarketValue; }
        }

        public int MaxPctMarketValue
        {
            get { return maxPctMarketValue; }
        }

        public int PctAvgMarketPrice
        {
            get { return pctAvgMarketPrice; }
        }
        public string PctAvgMarketPriceDisplay
        {
            get
            {
                if (PctAvgMarketPrice > 0)
                {
                    return PctAvgMarketPrice.ToString();
                }
                return "--";
            }
        }
        public int MinComparableMarketPrice
        {
            get { return minComparableMarketPrice; }
        }

        public int MaxComparableMarketPrice
        {
            get { return maxComparableMarketPrice; }
        }

        public int NumListings
        {
            get { return numListings; }
        }

        public int NumComparableListings
        {
            get { return numComparableListings; }
        }

        public PricingData()
        {

        }
        public PricingData(IDataRecord reader)
        {
            unitCost = GetNullInt(reader, "inventoryUnitCost");
            listPrice = GetNullInt(reader, "inventoryListPrice");
            minPctMarketAvg = reader.GetInt32(reader.GetOrdinal("minPctMarketAvg"));
            maxPctMarketAvg = reader.GetInt32(reader.GetOrdinal("maxPctMarketAvg"));
            minGrossProfit = reader.GetInt32(reader.GetOrdinal("minGrossProfit"));
            minPctMarketValue = GetNullInt(reader, "minPctMarketValue");
            maxPctMarketValue = GetNullInt(reader, "maxPctMarketValue");
            pctAvgMarketPrice = GetNullInt(reader, "pctAvgMarketPrice");
            minComparableMarketPrice = GetNullInt(reader, "minComparableMarketPrice");
            maxComparableMarketPrice = GetNullInt(reader, "maxComparableMarketPrice");
            numListings = reader.GetInt32(reader.GetOrdinal("numListings"));
            numComparableListings = reader.GetInt32(reader.GetOrdinal("numComparableListings"));
            marketDaySupply = Database.GetNullableInt(reader, "marketDaySupply");
        }

        public static Pair<VehicleHandle, SearchHandle> GetVehicleHandle(string oh, int inventoryID)
        {
            //using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            using (IDataConnection con = Database.GetConnection(Database.MarketDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Pricing.HandleLookup_Vehicle";
                    //cmd.CommandText = "INTERFACELINK.MerchandisingInterface.Interface.HandleLookup_Vehicle";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "OwnerHandle", oh, DbType.String);
                    Database.AddRequiredParameter(cmd, "VehicleEntityTypeID", 1, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "VehicleEntityID", inventoryID, DbType.Int32);
                    cmd.CommandTimeout = 0;
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        return new Pair<VehicleHandle, SearchHandle>(
                            new VehicleHandle(Convert.ToString(reader["VehicleHandle"])),
                            new SearchHandle(Convert.ToString(reader["SearchHandle"])));
                    }
                }
            }
        }

        public static string GetOwnerHandle(int requestingBusinessUnitID)
        {
            using (IDataConnection con = Database.GetConnection(Database.MarketDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    //cmd.CommandText = "INTERFACELINK.MerchandisingInterface.Interface.OwnerHandle";
                    cmd.CommandText = "Pricing.OwnerHandle";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "DealerID", requestingBusinessUnitID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        return (string)reader["OwnerHandle"];
                    }
                }
            }
        }



        public static PricingData GetMarketPricing(int businessUnitID, int inventoryId)
        {
            string oh = GetOwnerHandle(businessUnitID);
            Pair<VehicleHandle, SearchHandle> vhAndSh = GetVehicleHandle(oh, inventoryId);
            return GetMarketPricing(oh, vhAndSh.First.Value, vhAndSh.Second.Value);
        }
        private static PricingData GetMarketPricing(string oh, string vh, string sh)
        {
            //using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            using (IDataConnection con = Database.GetConnection(Database.MarketDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    //cmd.CommandText = "INTERFACELINK.MerchandisingInterface.Interface.MarketPricing";
                    cmd.CommandText = "Pricing.MarketPricing";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "OwnerHandle", oh, DbType.String);
                    Database.AddRequiredParameter(cmd, "SearchHandle", sh, DbType.String);
                    Database.AddRequiredParameter(cmd, "VehicleHandle", vh, DbType.String);
                    Database.AddRequiredParameter(cmd, "SearchTypeID", 1, DbType.Int32); //set to year make model search

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new PricingData(reader);
                        }
                        else
                        {
                            throw new ApplicationException("No pricing information found...");
                        }

                    }
                }
            }
        }

        public static int GetNullInt(IDataRecord reader, string columnName)
        {
            int i = reader.GetOrdinal(columnName);
            object o = reader.GetValue(i);
            return (o == DBNull.Value) ? -1 : Convert.ToInt32(o);

        }

        public PricingRisk GetPricingRisk()
        {
            if (pctAvgMarketPrice < MinPctMarketAvg)
            {
                return PricingRisk.Underpriced;
                //return "Underpriced";
            }
            if (pctAvgMarketPrice > MaxPctMarketAvg)
            {
                //return "Potentially Overpriced";
                return PricingRisk.OverPriced;
            }
            //return "Priced At Market";
            return PricingRisk.AtMarket;
        }

    }
}
