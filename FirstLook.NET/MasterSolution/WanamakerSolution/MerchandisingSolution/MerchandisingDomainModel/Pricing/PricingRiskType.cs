using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Pricing
{
    public class PricingRiskType
    {
        // When Mode is 'A'
        // 1 -- 0-21
        // 2 -- 22-35
        // 3 -- 36-45
        // 4 -- 46-55
        // 5 -- 56+

        // When Mode is 'R'
        // 1 -- UnderPricing Risk
        // 2 -- Priced at Market
        // 3 -- OverPricing Risk
        // 4 -- Zero/No Price
        // 5 -- Not Analyzed

        public int ColumnIndex { get; private set; }

        public string Description { get; private set; }

        public string Mode { get; private set; }

        public PricingRiskType(IDataRecord reader, string modeOfPricing)
        {
            ColumnIndex = GetNullInt(reader, "ColumnIndex");
            Description = reader.GetString(reader.GetOrdinal("Description"));
            Mode = modeOfPricing;
        }

        public static int GetNullInt(IDataRecord reader, string columnName)
        {
            int i = reader.GetOrdinal(columnName);
            object o = reader.GetValue(i);
            return (o == DBNull.Value) ? -1 : Convert.ToInt32(o);
        }

    }
}
