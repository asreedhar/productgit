using System;

namespace FirstLook.Merchandising.DomainModel.Pricing
{
    [Serializable]
    public class SearchHandle
    {
        private readonly string _sh;

        public string Value
        {
            get { return _sh; }
        }

        public SearchHandle(string sh)
        {
            _sh = sh;
        }
    }
}
