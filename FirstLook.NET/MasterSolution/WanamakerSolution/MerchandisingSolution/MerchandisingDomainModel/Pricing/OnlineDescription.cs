namespace FirstLook.Merchandising.DomainModel.Pricing
{
    public class OnlineDescription
    {
        public OnlineDescription()
        {
            Description = string.Empty;
            SourceRowId = 0;
            ProviderId = 0;
        }
        public OnlineDescription(string description, int sourceRowId, int providerId)
        {
            Description = description;
            SourceRowId = sourceRowId;
            ProviderId = providerId;
        }

        public string Description { get; set; }

        public int SourceRowId { get; set; }

        public int ProviderId { get; set; }
    }
}
