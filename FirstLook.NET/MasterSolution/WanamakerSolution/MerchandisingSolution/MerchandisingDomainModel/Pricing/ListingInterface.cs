using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Pricing
{
    public class ListingInterface
    {
       
        


        private static OnlineDescription GetOnlineDescriptionAndId(string vin, int providerId)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Postings.OnlineDescription#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "VIN", vin, DbType.String);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            if (reader.Read())
                            {
                                return
                                    new OnlineDescription(reader.GetString(reader.GetOrdinal("sellerDescription")),
                                    reader.GetInt32(reader.GetOrdinal("sourceRowId")),
                                    providerId);
                            }
                        }
                        catch
                        {
                            //silent catch block, shady -bf.
                            // ouch - zb
                        }

                        throw new ApplicationException("Vehicle not found in online descriptions");

                    }
                }
            }
        }

        public static string GetAutoTraderUrl(int businessUnitId, int inventoryId, string year)
        {
            string oh = PricingData.GetOwnerHandle(businessUnitId);
            Pair<VehicleHandle, SearchHandle> vhs = PricingData.GetVehicleHandle(oh, inventoryId); //vh, sh

            DataTable table = FindByOwnerHandleAndVehicleHandleAndProvider(
                oh,
                vhs.Second.Value,
                vhs.First.Value,
                4);

            foreach (DataRow row in table.Rows)
            {
                StringBuilder autoTraderUrl = new StringBuilder();
                autoTraderUrl.Append(
                    "http://www.autotrader.com/fyc/searchresults.jsp?advanced=&num_records=25&certified=&isp=y&search=y&lang=en&search_type=used&min_price=&max_price=&rdpage=100");
                autoTraderUrl.Append("&make=").Append(row["ProviderMake"]);
                autoTraderUrl.Append("&model=").Append(row["ProviderModel"]);
                autoTraderUrl.Append("&start_year=").Append(year);
                autoTraderUrl.Append("&end_year=").Append(year);
                autoTraderUrl.Append("&distance=").Append("75");
                autoTraderUrl.Append("&address=").Append(row["ZipCode"]);
                return autoTraderUrl.ToString();
            }
            
            return string.Empty;
        }

        public static string GetCarsUrl(int businessUnitId, int inventoryId)
        {
            string oh = PricingData.GetOwnerHandle(businessUnitId);
            Pair<VehicleHandle, SearchHandle> vhs = PricingData.GetVehicleHandle(oh, inventoryId); //vh, sh

            DataTable table = FindByOwnerHandleAndVehicleHandleAndProvider(
                oh,
                vhs.Second.Value,
                vhs.First.Value,
                2);

            foreach (DataRow row in table.Rows)
            {
                string carsDotComUrl =
                    "http://www.cars.com/go/search/search_results.jsp?numResultsPerPage=25&makeid=%make%&dgid=&pageNumber=0&zc=%zip%&cid=&amid=&certifiedOnly=false&sortorder=descending&searchType=22&criteria=K-%7CE-ALL%7CM-_%make%_%7CH-%7CD-_%model%_%7CN-N%7CR-%distance%%7CI-1%7CP-PRICE+descending%7CQ-descending%7CY-_%year%_%7CX-popular%7CZ-%zip%&modelid=%model%&tracktype=usedcc&sortfield=PRICE+descending&aff=national&dlid=&cname=&largeNumResultsPerPage=500&paId=&aff=national";
                carsDotComUrl = carsDotComUrl.Replace("%make%", row["ProviderMake"].ToString());
                carsDotComUrl = carsDotComUrl.Replace("%model%", row["ProviderModel"].ToString());
                carsDotComUrl = carsDotComUrl.Replace("%zip%", row["ZipCode"].ToString());
                carsDotComUrl = carsDotComUrl.Replace("%distance%", "75");
                carsDotComUrl = carsDotComUrl.Replace("%year%", row["VehicleYear"].ToString());
                return carsDotComUrl;
            }
        
            return string.Empty;
        }

        private static DataTable FindByOwnerHandleAndVehicleHandleAndProvider(String ownerHandle, String searchHandle, String vehicleHandle, int providerId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", @"Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", @"Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", @"Cannot be null or empty");

            //cars.com id =2, autotrader id = 4
            if (providerId != 2 && providerId != 4)
                throw new ArgumentOutOfRangeException("providerId", @"Not a valid Listing Provider ID");

            
            using (IDataConnection con = Database.GetConnection(Database.MarketDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Pricing.ListingProviderMakeModel";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "OwnerHandle", ownerHandle, DbType.String);
                    Database.AddRequiredParameter(cmd, "SearchHandle", searchHandle, DbType.String);
                    Database.AddRequiredParameter(cmd, "VehicleHandle", vehicleHandle, DbType.String);
                    Database.AddRequiredParameter(cmd, "ProviderID", providerId.ToString(CultureInfo.InvariantCulture),
                                          DbType.String);

                    SqlDataAdapter sda = new SqlDataAdapter();
                    sda.SelectCommand = (SqlCommand) cmd;
                    DataSet ds = new DataSet();
                    sda.Fill(ds);
                    return ds.Tables[0];

                }
            }
        }
    }
}