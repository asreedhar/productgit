using System;

namespace FirstLook.Merchandising.DomainModel.Pricing
{
    [Serializable]
    public class VehicleHandle
    {
        private readonly string _vh;

        public string Value
        {
            get { return _vh; }
        }

        public VehicleHandle(string vh)
        {
            _vh = vh;
        }
    }
}
