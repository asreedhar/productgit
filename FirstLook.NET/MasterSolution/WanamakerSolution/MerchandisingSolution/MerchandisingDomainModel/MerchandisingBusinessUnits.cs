using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Common.ActiveRecord;
using FirstLook.Common.Core.Logging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;

namespace FirstLook.Merchandising.DomainModel
{
    // ReSharper disable ClassNeverInstantiated.Global
    // This class is used by DealerSelector.aspx.cs and a few other pages.

    /// <summary>
    /// Contains logic required to retrieve Dealer Groups and Dealerships from the database for the MAX Merchandising application.
    /// </summary>
    class MerchandisingBusinessUnits
    {
        private static readonly MerchandisingBusinessUnits finder = new MerchandisingBusinessUnits();
        private static readonly ILog Log = LoggerFactory.GetLogger<MerchandisingBusinessUnits>();

        public static MerchandisingBusinessUnits Instance()
        {
            return finder;
        }

        /// <summary>
        /// Gets a list of all Business Units that are set up in the MAX Merchandising application.
        /// </summary>
        /// <returns>List of all Business Units that are set up in the MAX Merchandising application.</returns>
        private static List<Int32> GetAllMerchandisingBusinessUnits()
        {
            List<Int32> businessUnitCollection = new List<int>();
            
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString);
            con.Open();
            
            SqlCommand select = con.CreateCommand();
            select.CommandText = "SELECT businessUnitID FROM settings.Merchandising";
            
            SqlDataReader reader = select.ExecuteReader();
            
            while (reader.Read())
            {
                businessUnitCollection.Add(Convert.ToInt32(reader["businessUnitID"]));
            }
            
            reader.Close();
            con.Close();

            return businessUnitCollection;
        }

        /// <summary>
        /// Gets a list of all Business Unit Ids that are set up in the MAX Merchandising application with a Max version 
        /// at or above the specified version.
        /// </summary>
        /// <param name="version">The MaxVersion to check for</param>
        /// <returns>A list of all Business Unit Ids that are set up in the MAX Merchandising application with a Max version 
        /// at or above the specified version.</returns>
        private static List<Int32> GetAllActiveMerchandisingBusinessUnitIdsAboveVersion(Int32 version)
        {
            var businessUnitCollection = new List<Int32>();
            var commandText = String.Format("SELECT businessUnitID FROM settings.Merchandising WHERE MaxVersion >= {0}", version);

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                using (var select = new SqlCommand(commandText, connection))
                {
                    try
                    {
                        connection.Open();
                        var reader = select.ExecuteReader();
                        var activeMaxDealers = (new MaxDealersRepository()).FindAllActiveMaxDealers();

                        businessUnitCollection.AddRange(
                            from dr in reader.Cast<IDataRecord>()
                            where activeMaxDealers.Contains(dr.GetInt32(0))
                            select dr.GetInt32(0));
                    }
                    catch (SqlException sqlEx)
                    {
                        Log.Error("Error in Getting Merchandising Business Units by Version", sqlEx);
                    }
                }
            }

            return businessUnitCollection;
        }

        // ReSharper disable UnusedMember.Global
        // ReSharper disable UnusedParameter.Global
        // these methods are used by DealerSelector.aspx.cs zb

        /// <summary>
        /// Gets a list of all Dealer Groups which are set up in the MAX Merchandising application.
        /// </summary>
        /// <param name="member">The Member that is currently logged in to the system.</param>
        /// <param name="filter">The keywords to filter the results on.</param>
        /// <returns>List of all Dealer Groups which are set up in the MAX Merchandising application.</returns>        
        public ICollection<BusinessUnit> FindAllMerchandisingDealerGroups(Member member, string filter)
        {
            List<Int32> wanamakerBusinessUnits = GetAllMerchandisingBusinessUnits();
            IList<BusinessUnit> allDealerGroups = BusinessUnitFinder.Instance().FindAllDealerGroups(filter);

            List<BusinessUnit> wanamakerDealerGroups = new List<BusinessUnit>();
            foreach (Int32 item in wanamakerBusinessUnits)
            {
                BusinessUnit dealerGroup = BusinessUnitFinder.Instance().FindDealerGroupByDealership(item);
                if (dealerGroup == null || wanamakerDealerGroups.Contains(dealerGroup)) continue;
                
                if (allDealerGroups.Contains(dealerGroup))
                    wanamakerDealerGroups.Add(dealerGroup);
            }
            wanamakerDealerGroups.Sort(SortBusinessUnits);
            return wanamakerDealerGroups;
        }

        /// <summary>
        /// Gets a list of all Dealer Groups that belong to the specified Member.
        /// </summary>
        /// <param name="member">The Member that is currently logged in to the system.</param>
        /// <param name="filter">The keywords to filter the results on.</param>
        /// <returns></returns>
        public ICollection<BusinessUnit> FindAllDealerGroupsForMember(Member member, string filter)
        {
            return BusinessUnitFinder.Instance().FindAllDealerGroupsForMember(member, filter);
        }

        /// <summary>
        /// Gets a list of all Dealers that belong to the specified Member within the specified Dealer Group.
        /// </summary>
        /// <param name="member">The Member that is currently logged in to the system.</param>
        /// <param name="dealerGroupID">The Dealer Group to retrieve Dealerships for.</param>
        /// <param name="filter">The keywords to filter the results on.</param>
        /// <returns></returns>
        public ICollection<BusinessUnit> FindAllDealersForMemberAndDealerGroup(Member member, Int32 dealerGroupID, string filter)
        {
            return BusinessUnitFinder.Instance().FindAllDealersForMemberAndDealerGroup(member, dealerGroupID, filter);
        }

        /// <summary>
        /// Gets a list of all Dealerships which are set up in the MAX Merchandising application and belong to the specified Dealer Group.
        /// </summary>
        /// <param name="dealerGroupID">The Dealer Group to retrieve the Dealerships for.</param>
        /// <param name="member">The Member that is currently logged in to the system.</param>
        /// <param name="filter">The keywords to filter the results on.</param>
        /// <returns>A list of all Dealerships which are set up in the MAX Merchandising application and belong to the specified Dealer Group.</returns>
        public ICollection<BusinessUnit> FindAllMerchandisingDealershipsByGroup(Int32 dealerGroupID, Member member, string filter)
        {
            List<Int32> wanamakerBusinessUnits = GetAllMerchandisingBusinessUnits();
            IList<BusinessUnit> allDealerships = BusinessUnitFinder.Instance().FindAllDealershipsByDealerGroupAndMember(dealerGroupID, member.Id, filter);

            List<BusinessUnit> wanamakerDealerships = new List<BusinessUnit>();
            foreach (BusinessUnit item in allDealerships)
            {
                if (wanamakerBusinessUnits.Contains(item.Id))
                    wanamakerDealerships.Add(item);
            }
            wanamakerDealerships.Sort(SortBusinessUnits);
            return wanamakerDealerships;
        }

        /// <summary>
        /// Gets a list of all Business Units which are set up in the MAX Merchandising application with a Max version 
        /// at or above the specified version.
        /// </summary>
        /// <param name="version">The MaxVersion to check for</param>
        /// <returns>A list of Business Units which are set up in the MAX Merchandising application that are at or above
        /// the specified version</returns>
        public IEnumerable<BusinessUnit> FindAllMerchandisingDealershipsAboveVersion(Int32 version)
        {
            var businessUnitIds = GetAllActiveMerchandisingBusinessUnitIdsAboveVersion(version);
            var activeBusinessUnits =
                BusinessUnitFinder.Instance().Find(BusinessUnitFinder.Instance().CreateWhereClause("Active", true));

            return activeBusinessUnits.Where(dlr => businessUnitIds.Contains(dlr.Id));
        }

        /// <summary>
        /// Comparer method used to sort a list of Business Units.
        /// </summary>
        /// <param name="a">The first business unit for the compare operation.</param>
        /// <param name="b">The second business unit for the compare operation.</param>
        /// <returns>-1 if a comes before b, 0 if they are equal, and 1 if b comes before a.</returns>
        private static int SortBusinessUnits(BusinessUnit a, BusinessUnit b)
        {
            return a.Name.CompareTo(b.Name);
        }

        // ReSharper restore UnusedParameter.Global
        // ReSharper restore UnusedMember.Global

    }
    // ReSharper restore ClassNeverInstantiated.Global

}
