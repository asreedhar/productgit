﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Data;
using Newtonsoft.Json;

namespace FirstLook.Merchandising.DomainModel.SnippetCollector
{
    public class SnippetCollectorDataAccess
    {
        public enum SnippetDataType { Snippet, Tag, Source };

        public string GetAllSnippets ( string json )
        {
            var trimIdContainer = new
            {
                ids = string.Empty
            };

            var trimIds = JsonConvert.DeserializeAnonymousType( json, trimIdContainer );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Snippets#Fetch";
                        command.CommandType = CommandType.StoredProcedure;
                        if ( trimIds != null )
                        {
                            Database.AddRequiredParameter( command, "@ChromeStyleId", trimIds.ids, DbType.String );
                        }

                        SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                        DataSet dataSet = new DataSet();
                        adapter.Fill( dataSet );

                        return JsonConvert.SerializeObject( dataSet.Tables[ 0 ] );
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public string GetSnippetsByStyle ( string json )
        {
            var trimIdContainer = new
            {
                ids = string.Empty
            };

            var trimIds = JsonConvert.DeserializeAnonymousType( json, trimIdContainer );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {

                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        List<SnippetTo> snippetTos = new List<SnippetTo>();
                        command.CommandText = "Snippets.SnippetsByChromeStyle#Fetch";
                        command.CommandType = CommandType.StoredProcedure;
                        if ( trimIds != null )
                        {
                            Database.AddRequiredParameter( command, "@ChromeStyleId", trimIds.ids, DbType.String );
                        }

                        using ( IDataReader reader = command.ExecuteReader() )
                        {

                            while ( reader.Read() )
                            {
                                snippetTos.Add( new SnippetTo()
                                {
                                    Id = reader.GetInt32( reader.GetOrdinal( "marketingId" ) ),
                                    MarketingText = reader.GetString( reader.GetOrdinal( "marketingText" ) ),
                                    SourceName = reader.GetString( reader.GetOrdinal( "sourceName" ) )
                                } );
                            }
                        }

                        var snippetsContainer = new { Snippets = snippetTos };

                        return JsonConvert.SerializeObject( snippetsContainer );
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public DataSet GetAllSources ()
        {
            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.MarketingSource#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                    DataSet dataSet = new DataSet();
                    adapter.Fill( dataSet );

                    return dataSet;
                }
            }
        }

        public DataSet GetAllTags ()
        {
            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.Tag#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                    DataSet dataSet = new DataSet();
                    adapter.Fill( dataSet );

                    return dataSet;
                }
            }
        }

        public DataSet GetMakes ()
        {
            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.Makes#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                    DataSet dataSet = new DataSet();
                    adapter.Fill( dataSet );

                    return dataSet;
                }
            }
        }

        public string GetModels () { return GetModels( "" ); }  // Gets all models
        public string GetModels ( string json )                 // Gets models by make id
        {
            var make = string.IsNullOrEmpty( json ) ? null : ( VehicleMakeTo )JsonConvert.DeserializeObject( json, typeof( VehicleMakeTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Models#Fetch";
                        command.CommandType = CommandType.StoredProcedure;
                        if ( make != null )
                        {
                            Database.AddRequiredParameter( command, "@DivisionId", make.Id, DbType.Int32 );
                        }

                        SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                        DataSet dataSet = new DataSet();
                        adapter.Fill( dataSet );

                        return JsonConvert.SerializeObject( dataSet );
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public DataSet GetModels ( string[] makeIds )                 // Gets models by make ids
        {
            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase , false) )
            {
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.Models#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter( command, "@DivisionId", String.Join( ",", makeIds ), DbType.String );

                    SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                    DataSet dataSet = new DataSet();
                    adapter.Fill( dataSet );

                    return dataSet;
                }
            }
        }

        public DataSet GetModelsFromChromeStyle ( string styles )
        {
            if ( styles == null ) throw new ArgumentNullException( "styles", "No chrome style Ids provided to GetModelsFromChromeStyle() method." );

            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.ModelsByChromeStyle#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter( command, "@ChromeStyleIds", styles, DbType.String );

                    SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                    DataSet dataSet = new DataSet();
                    adapter.Fill( dataSet );

                    return dataSet;
                }
            }
        }

        public DataSet GetMakesFromModels ( string modelIds )
        {
            if ( modelIds == null ) throw new ArgumentNullException( "modelIds", "No chrome style Ids provided to GetModelsFromChromeStyle() method." );

            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.MakesByModels#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter( command, "@ModelIds", modelIds, DbType.String );

                    SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                    DataSet dataSet = new DataSet();
                    adapter.Fill( dataSet );

                    return dataSet;
                }
            }
        }

        public SnippetTo GetSnippetById ( int snippetId )
        {
            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                connection.Open();

                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.Snippets#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter( command, "@MarketingId", snippetId, DbType.Int32 );

                    using ( IDataReader reader = command.ExecuteReader() )
                    {
                        if ( !reader.Read() ) return null;

                        var json = "{ \"Id\":\"" + reader.GetValue( reader.GetOrdinal( "marketingId" ) ) + "\","
                                                + "\"MarketingText\":\"" + reader.GetValue( reader.GetOrdinal( "marketingText" ) ) + "\","
                                                + "\"Source\":\"" + reader.GetValue( reader.GetOrdinal( "sourceId" ) ) + "\","
                                                + "\"IsVerbatim\":\"" + reader.GetValue( reader.GetOrdinal( "isVerbatim" ) ) + "\","
                                                + "\"Tags\":\"" + reader.GetValue( reader.GetOrdinal( "tags" ) ) + "\","
                                                + "\"Styles\":\"" + reader.GetValue( reader.GetOrdinal( "styles" ) ) + "\""
                                                + "}";
                        return ( SnippetTo )JsonConvert.DeserializeObject( json, typeof( SnippetTo ) );

                    }

                }
            }
        }

        public string GetTrims () { return GetTrims( "" ); }  // Gets all trims
        public string GetTrims ( string modelIds )                // Gets trims by modelId[s] id
        {
            var modelTo = ( VehicleModelTo )JsonConvert.DeserializeObject( modelIds, typeof( VehicleModelTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Trims#Fetch";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@ModelIds", modelTo.Id, DbType.String );

                        SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                        DataSet dataSet = new DataSet();
                        adapter.Fill( dataSet );

                        return JsonConvert.SerializeObject( dataSet.Tables[ 0 ] );
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public DataSet GetTrims ( string[] modelIds )
        {
            if ( modelIds.Length < 1 ) throw new ArgumentNullException( "modelIds", "No chrome style Ids provided to GetModelsFromChromeStyle() method." );

            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                using ( IDbCommand command = connection.CreateCommand() )
                {
                    command.CommandText = "Snippets.Trims#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter( command, "@ModelIds", String.Join( ",", modelIds ), DbType.String );

                    SqlDataAdapter adapter = new SqlDataAdapter( ( SqlCommand )command );

                    DataSet dataSet = new DataSet();
                    adapter.Fill( dataSet );

                    return dataSet;
                }
            }
        }

        public string AddSnippet ( string json )
        {
            var snippet = ( SnippetTo )JsonConvert.DeserializeObject( json, typeof( SnippetTo ) );

            try
            {

                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Snippet#Insert";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@MarketingText", Microsoft.JScript.GlobalObject.unescape( Uri.UnescapeDataString( snippet.MarketingText ) ), DbType.String );
                        Database.AddRequiredParameter( command, "@MarketingType", 2, DbType.Int32 );
                        Database.AddRequiredParameter( command, "@SourceId", snippet.Source, DbType.Int32 );
                        Database.AddRequiredParameter( command, "@IsVerbatim", snippet.IsVerbatim, DbType.Boolean );
                        Database.AddRequiredParameter( command, "@Tags", snippet.Tags, DbType.String );
                        Database.AddRequiredParameter( command, "@Styles", snippet.Styles, DbType.String );

                        using ( IDataReader reader = command.ExecuteReader() )
                        {
                            reader.Read();
                            return "{ \"id\":\"" + reader.GetValue( 0 ) + "\" }";
                        }
                    }
                }

            }
            catch ( Exception exception )
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public string UpdateSnippet ( string json )
        {
            var snippet = ( SnippetTo )JsonConvert.DeserializeObject( json, typeof( SnippetTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Snippet#Update";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@Id", snippet.Id, DbType.Int32 );
                        Database.AddRequiredParameter( command, "@MarketingText", Microsoft.JScript.GlobalObject.unescape( Uri.UnescapeDataString( snippet.MarketingText ) ), DbType.String );
                        Database.AddRequiredParameter( command, "@MarketingType", 2, DbType.Int32 );
                        Database.AddRequiredParameter( command, "@SourceId", snippet.Source, DbType.Int32 );
                        Database.AddRequiredParameter( command, "@IsVerbatim", snippet.IsVerbatim, DbType.Boolean );
                        Database.AddRequiredParameter( command, "@Tags", snippet.Tags, DbType.String );
                        Database.AddRequiredParameter( command, "@Styles", snippet.Styles, DbType.String );

                        using ( IDataReader reader = command.ExecuteReader() )
                        {
                            reader.Read();
                            return "{ \"id\":\"" + reader.GetValue( 0 ) + "\" }";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public string AddSource ( string json )
        {
            // Prevent duplicates
            if ( IsDuplicate( SnippetDataType.Source, json ) ) return "{ \"id\":\"\", \"error\":\"This source already exists.\" }";

            var source = ( SourceTo )JsonConvert.DeserializeObject( json, typeof( SourceTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase , false) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.MarketingSource#Insert";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@SourceName", source.SourceName, DbType.String );
                        Database.AddRequiredParameter( command, "@HasCopyright", source.HasCopyright, DbType.Boolean );

                        using ( IDataReader reader = command.ExecuteReader() )
                        {
                            reader.Read();
                            return "{ \"id\":\"" + reader.GetValue( reader.GetOrdinal( "id" ) )
                                    + "\", \"SourceName\":\"" + reader.GetValue( reader.GetOrdinal( "sourceName" ) )
                                    + "\", \"HasCopyright\":\"" + reader.GetValue( reader.GetOrdinal( "hasCopyright" ) ) + "\" }";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public string UpdateSource ( string json )
        {
            var source = ( SourceTo )JsonConvert.DeserializeObject( json, typeof( SourceTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.MarketingSource#Update";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@Id", source.Id, DbType.Int32 );
                        Database.AddRequiredParameter( command, "@SourceName", Uri.UnescapeDataString( source.SourceName ), DbType.String );
                        Database.AddRequiredParameter( command, "@HasCopyright", source.HasCopyright, DbType.Boolean );

                        using ( IDataReader reader = command.ExecuteReader() )
                        {
                            reader.Read();
                            return "{ \"id\":\"" + reader.GetValue( 0 ) + "\" }";
                        }
                    }
                }
            }
            catch ( Exception exception )
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }
        }

        public string AddTag ( string json )
        {
            // Prevent duplicates
            if ( IsDuplicate( SnippetDataType.Tag, json ) ) return "{ \"id\":\"\", \"error\":\"This source already exists.\" }";

            var tag = ( TagTo )JsonConvert.DeserializeObject( json, typeof( TagTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Tag#Insert";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@Tag", tag.Tag, DbType.String );
                        Database.AddRequiredParameter( command, "@NewId", null, DbType.Int32 );

                        using ( IDataReader reader = command.ExecuteReader() )
                        {
                            reader.Read();
                            return "{ \"id\":\"" + reader.GetValue( reader.GetOrdinal( "id" ) ) + "\", \"Tag\":\"" + reader.GetValue( reader.GetOrdinal( "description" ) ) + "\" }";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public string UpdateTag ( string json )
        {
            var tag = ( TagTo )JsonConvert.DeserializeObject( json, typeof( TagTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Tag#Update";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@Id", tag.Id, DbType.Int32 );
                        Database.AddRequiredParameter( command, "@Description", Uri.UnescapeDataString( tag.Tag ), DbType.String );

                        using ( IDataReader reader = command.ExecuteReader() )
                        {
                            reader.Read();
                            return "{ \"id\":\"" + reader.GetValue( 0 ) + "\" }";
                        }
                    }
                }
            }
            catch ( Exception exception )
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }
        }

        public string DeleteSnippet ( string json )
        {
            var snippetTo = ( SnippetTo )JsonConvert.DeserializeObject( json, typeof( SnippetTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Snippet#Delete";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@SnippetId", snippetTo.Id, DbType.Int32 );

                        command.ExecuteNonQuery();
                        return "{ \"id\":\"" + snippetTo.Id + "\" }";

                    }
                }

            }
            catch ( Exception exception )
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public string DeleteTag ( string json )
        {
            var tag = ( TagTo )JsonConvert.DeserializeObject( json, typeof( TagTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.CommandText = "Snippets.Tag#Delete";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@TagId", tag.Id, DbType.Int32 );

                        object results = command.ExecuteScalar();
                        int rows = ( int )results;

                        if ( rows >= 1 )
                        {
                            if ( rows == 1 ) return "{ \"id\":\"" + tag.Id + "\", \"Tag\":\"" + tag.Tag + "\" }";
                            return "{ \"error\":\"Error: " + results + " records deleted.\" }";
                        }

                        return "{ \"id\":\"\", \"Tag\":\"" + tag.Tag + "\" }";

                    }
                }
            }
            catch ( Exception exception )
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public string UndoTagDelete ( string json )
        {
            var tag = ( TagTo )JsonConvert.DeserializeObject( json, typeof( TagTo ) );

            try
            {
                using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
                {
                    connection.Open();

                    using ( IDbCommand command = connection.CreateCommand() )
                    {

                        command.CommandText = "Snippets.Tag#UndoDelete";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "@TagId", tag.Id, DbType.Int32 );

                        using ( IDataReader reader = command.ExecuteReader() )
                        {
                            reader.Read();
                            return "{ \"id\":\"" + reader.GetOrdinal( "id" ) + "\", \"Tag\":\"" + reader.GetOrdinal( "description" ) + "\" }";
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return "{ \"error\":\"" + Uri.EscapeDataString( exception.Message ) + "\" }";
            }

        }

        public static bool IsDuplicate ( SnippetDataType type, string json )
        {
            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase, false ) )
            {
                connection.Open();

                using ( IDbCommand command = connection.CreateCommand() )
                {
                    switch ( type )
                    {
                        case SnippetDataType.Source:
                            var source = ( SourceTo )JsonConvert.DeserializeObject( json, typeof( SourceTo ) );
                            command.CommandText = "SELECT id, sourceName, hasCopyright FROM Snippets.MarketingSource WHERE sourceName = '" + source.SourceName + "'";
                            break;

                        case SnippetDataType.Tag:
                            var tag = ( TagTo )JsonConvert.DeserializeObject( json, typeof( TagTo ) );
                            command.CommandText = "SELECT id, description FROM Snippets.Tag WHERE description = '" + tag.Tag + "'";
                            break;
                    }
                    using ( IDataReader reader = command.ExecuteReader() )
                    {
                        return ( ( SqlDataReader )reader ).HasRows;
                    }
                }
            }
        }

        public class SnippetTo
        {
            public int? Id { get; set; }
            public string MarketingText { get; set; }
            public int Source { get; set; }
            public string SourceName { get; set; }
            public int Rating { get; set; }
            public bool IsVerbatim { get; set; }
            public string Tags { get; set; }
            public string Styles { get; set; }
        }

        public class SourceTo
        {
            public int Id { get; set; }
            public string SourceName { get; set; }
            public bool HasCopyright { get; set; }
        }

        public class TagTo
        {
            public int Id { get; set; }
            public string Tag { get; set; }
        }

        public class VehicleMakeTo
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class VehicleModelTo
        {
            // This id is a string to support multiple (comma-delimited) ids
            public string Id { get; set; }
            public string Name { get; set; }
        }

        public class VehicleTrimTo
        {
            public int StyleID { get; set; }
            public string StyleName { get; set; }
        }
    }
}
