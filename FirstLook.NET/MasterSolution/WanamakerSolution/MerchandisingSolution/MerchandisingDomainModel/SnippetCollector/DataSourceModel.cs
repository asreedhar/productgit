using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;

namespace FirstLook.Merchandising.DomainModel.SnippetCollector
{
    [DataObject(true)]
// ReSharper disable UnusedMember.Global
    // used by VehicleSetCreator.ascx
    public class DataSourceModel
// ReSharper restore UnusedMember.Global
    {
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static DataTable GetModels(int divisionID)
        {
            const string QUERY = @"select DISTINCT ModelName
                            FROM Chrome.Models CM, Chrome.Divisions CD
                            Where CM.DivisionID = CD.DivisionID
                            AND CD.DivisionID = @divisionID";

            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = QUERY;
                    cmd.CommandType = CommandType.Text;
                   
                    Database.AddRequiredParameter(cmd, "@divisionID", divisionID, DbType.Int32);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = (SqlCommand)cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Models");
                    return ds.Tables["Models"];
                }
            }
        }
    }
}
