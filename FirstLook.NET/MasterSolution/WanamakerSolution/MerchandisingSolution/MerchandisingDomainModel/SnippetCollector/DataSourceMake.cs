using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;

namespace FirstLook.Merchandising.DomainModel.SnippetCollector
{
    [DataObject(true)]
// ReSharper disable UnusedMember.Global
    // used by VehicleSetCreator.ascx
    public class DataSourceMake
// ReSharper restore UnusedMember.Global
    {
        [DataObjectMethod(DataObjectMethodType.Select, true)]
// ReSharper disable UnusedMember.Global
        // used by VehicleSetCreator.ascx
        public static DataTable GetMake()
// ReSharper restore UnusedMember.Global
        {
            const string QUERY = @"select DISTINCT DivisionName, DivisionID from Chrome.Divisions 
            ORDER BY divisionName";
            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
 
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    
                    cmd.CommandText = QUERY;
                    cmd.CommandType = CommandType.Text;  
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = (SqlCommand) cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Makes");
                    return ds.Tables["Makes"];
                }
            }
        }
    }
}
