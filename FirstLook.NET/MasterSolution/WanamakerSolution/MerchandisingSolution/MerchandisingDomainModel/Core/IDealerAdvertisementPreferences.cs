using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface IDealerAdvertisementPreferences
    {
        string DefaultTemplateName { get; set; }
        bool UseCallToActionInPreview { get; set; }
        int MinGoodPriceReduction { get; set; }
        int MaxOptionsCountInPreview { get; set; }
        bool PreviewEquipmentIsReusable { get; set; }
        int MaxCountMarketingReviews { get; set; }
        int MaxConditionSummaryLength { get; set; }
        int PreferredDescriptionLength { get; set; }
        int DefaultTemplateId { get; set; }
        int HighReconditioningValue { get; set; }
        int MaxMileageThatIsLow { get; set; }
        int MaxCountJdPowerRatings { get; set; }
        float MinValidJdPowerRating { get; set; }
        BookoutCategory PreferredBookCategory { get; set; }
        int MinOptionsPerGroup { get; set; }
        int MaxOptions { get; set; }
        decimal MinValidListPrice { get; set; }
        int MaxCountForStripped { get; set; }
        int MinDescriptionLength { get; set; }
        int HighPassengerCountForSuvs { get; set; }
        int HighPassengerCountForTrucks { get; set; }
        int BusinessUnitId { get; set; }
        List<GasMileagePreference> GasMileageBySegment { get; set; }
        string TextSeparator { get; set; }
        int CountTier1ForLoaded { get; set; }
        int[] MaxOptionsByTier { get; set; }
        decimal GoodBookDifferential { get; set; }
        decimal GoodMsrpDifferential { get; set; }
        int MinNumberForCountBelowBookDisplay { get; set; }
        int MinNumberForCountBelowPriceDisplay { get; set; }
        int GoodWarrantyMileageRemaining { get; set; }
        int MaxWarrantiesToDisplay { get; set; }
        int GoodCrashTestRating { get; set; }
        int MaxCrashTestRatingsToDisplay { get; set; }
        bool UseLongDescriptionAsDefault { get; set; }
        int DesiredPreviewLength { get; set; }
        int GetGoodHighwayMileage(int segmentId);
        int GetGoodCityMileage(int segmentId);
    }
}