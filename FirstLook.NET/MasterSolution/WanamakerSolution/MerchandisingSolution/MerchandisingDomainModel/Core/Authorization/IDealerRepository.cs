namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal interface IDealerRepository
    {
        IDealerData GetData();
    }
}