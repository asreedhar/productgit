﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Logging;
using FirstLook.DomainModel.Oltp;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal sealed class DealerRepository : IDealerRepository
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<DealerRepository>();

        public IDealerData GetData()
        {
            using (MiniProfiler.Current.Step("DealerRepository.GetData()"))
            {
                if(Log.IsInfoEnabled)
                    Log.Info("Starting to execute DealerRepository.GetData()");

                var t = DateTimeOffset.UtcNow;

                var list = ReadDataFromDb();
                var data = new DealerData(list);

                if (Log.IsInfoEnabled)
                    Log.InfoFormat("Completed DealerRepository.GetData(). ElapsedMs={0:0.0}",
                                   (DateTimeOffset.UtcNow - t).TotalMilliseconds);

                return data;
            }
        }

        private static IList<IDealer> ReadDataFromDb()
        {
            IList<IDealer> list;
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "dbo.Dealers#Fetch";

                using (var r = cmd.ExecuteReader())
                {
                    var upgrades = ReadUpgrades(r);
                    r.NextResult();
                    list = ReadDealers(r, upgrades);

                    if(Log.IsInfoEnabled)
                        Log.InfoFormat(
                            "Exececuted DealerRepository.ReadDataFromDb(). DealerCount={0}, DealerUpgradeCount={1}",
                            list.Count, upgrades.Count);
                }
            }
            return list;
        }

        private static ISet<DealerUpgradeKey> ReadUpgrades(IDataReader r)
        {
            var upgrades = new HashSet<DealerUpgradeKey>();

            var businessUnitIdField = r.GetOrdinal("BusinessUnitID");
            var dealerUpgradeCdField = r.GetOrdinal("DealerUpgradeCD");

            while (r.Read())
            {
                upgrades.Add(
                    new DealerUpgradeKey(
                        r.GetInt32(businessUnitIdField),
                        (Upgrade)r.GetByte(dealerUpgradeCdField)));
            }

            return upgrades;
        }

        private static IList<IDealer> ReadDealers(IDataReader r, ISet<DealerUpgradeKey> upgrades)
        {
            var list = new List<IDealer>();

            var businessUnitIdField = r.GetOrdinal("BusinessUnitID");
            var handleField = r.GetOrdinal("Handle");
            var businessUnitCodeField = r.GetOrdinal("BusinessUnitCode");
            var businessUnitField = r.GetOrdinal("BusinessUnit");
            var webLoaderEnabledField = r.GetOrdinal("WebLoaderEnabled");

            while (r.Read())
            {
                list.Add(
                    new Dealer(
                        r.GetInt32(businessUnitIdField),
                        r.IsDBNull(handleField) ? (Guid?)null : r.GetGuid(handleField),
                        r.GetString(businessUnitCodeField),
                        r.GetString(businessUnitField),
                        upgrades,
                        !r.IsDBNull(webLoaderEnabledField) && r.GetBoolean(webLoaderEnabledField)));
            }

            return list.AsReadOnly();
        }
    }
}