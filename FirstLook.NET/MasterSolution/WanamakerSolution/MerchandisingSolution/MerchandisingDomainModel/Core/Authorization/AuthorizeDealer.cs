using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using FirstLook.Common.Core.Logging;
using FirstLook.DomainModel.Oltp;

// ReSharper disable ConditionIsAlwaysTrueOrFalse -- I don't trust this assertion for IPrincipal.Identity

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal class AuthorizeDealer : IAuthorizeDealer
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AuthorizeDealer>();
        private readonly IUserServices _userServices;
        private readonly IDealerServices _dealerServices;

        public AuthorizeDealer(IUserServices userServices, IDealerServices dealerServices)
        {
            _userServices = userServices;
            _dealerServices = dealerServices;
        }

        public AuthorizationResult IsAuthorized(IPrincipal principal, string dealerCode, IList<Upgrade> assertDealerUpgrades = null, 
                                                bool assertDealerHasWebLoaderUpgrade = false)
        {
            var user = _userServices.GetUser(principal);
            if (user == null)
            {
                if (Log.IsInfoEnabled)
                    Log.InfoFormat("{0} username='{1}'",
                                   Resources.NotAuthenticated,
                                   principal != null && principal.Identity != null ? principal.Identity.Name : null);
                return new AuthorizationResult(Resources.NotAuthenticated);
            }

            var dealer = _dealerServices.GetDealerByCode(dealerCode);
            if(dealer == null)
            {
                if (Log.IsInfoEnabled)
                    Log.InfoFormat("{0} dealer='{1}'",
                                   Resources.DealerNotFound,
                                   dealerCode);
                return new AuthorizationResult(Resources.DealerNotFound, true);
            }

            if(!user.HasAccessTo(dealer))
            {
                if(Log.IsInfoEnabled)
                    Log.InfoFormat("{0} user='{1}' dealer='{2}'",
                                   Resources.NoAccessToDealer,
                                   user.UserName, dealer.Code);

                return new AuthorizationResult(Resources.NoAccessToDealer);
            }

            if(assertDealerUpgrades != null && assertDealerUpgrades.Count > 0 && !assertDealerUpgrades.All(dealer.HasUpgrade))
            {
                if (Log.IsInfoEnabled)
                    Log.InfoFormat("{0} dealer='{1}' assertDealerUpgrades='{2}'",
                                   Resources.NoDealerUpgrade,
                                   dealer.Code,
                                   string.Join(", ", assertDealerUpgrades));

                return new AuthorizationResult(Resources.NoDealerUpgrade);
            }

            if(assertDealerHasWebLoaderUpgrade && !dealer.HasWebLoader)
            {
                if (Log.IsInfoEnabled)
                    Log.InfoFormat("{0} dealer='{1}' assertDealerHasWebLoaderUpgrade=true",
                                   Resources.NoDealerUpgrade, dealer.Code);

                return new AuthorizationResult(Resources.NoDealerUpgrade);
            }

            return AuthorizationResult.Authorized;
        }

        private static class Resources
        {
            public static string NotAuthenticated
            {
                get { return "Access Denied: User is not authenticated."; }
            }

            public static string NoAccessToDealer
            {
                get { return "Access Denied: User does not have access to specified dealer."; }
            }

            public static string NoDealerUpgrade
            {
                get { return "Access Denied: Dealer does not have required upgrades."; }
            }

            public static string DealerNotFound
            {
                get { return "Dealer not found."; }
            }
        }
    }
}