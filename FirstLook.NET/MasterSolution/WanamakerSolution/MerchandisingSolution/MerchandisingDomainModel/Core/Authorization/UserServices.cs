using System.Security.Principal;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Core.Authentication;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal sealed class UserServices : IUserServices
    {
        private readonly IUserRepository _repository;
        private readonly IAuthenticateUser _authenticateUser;
        private readonly ICurrentPrincipal _currentPrincipal;
        private static readonly ILog Log = LoggerFactory.GetLogger<UserServices>();

        public UserServices(IUserRepository repository, IAuthenticateUser authenticateUser, ICurrentPrincipal currentPrincipal)
        {
            _repository = repository;
            _authenticateUser = authenticateUser;
            _currentPrincipal = currentPrincipal;
        }

        public bool AuthenticateAndSetCurrent(BasicCredential credential)
        {
            if(credential != null)
                Log.DebugFormat("Authenticating credentials:{0}", credential.ToJson());

            var user = credential == null ? null : LookupUserFromRepository(credential.Username);
            var authenticationType = user == null ? null : _authenticateUser.Authenticate(credential);
            var principal = authenticationType == null ? null : new UserPrincipal(user, authenticationType);

            _currentPrincipal.Current = principal;
            
            return authenticationType != null;
        }

        public IUser Current
        {
            get
            {
                return GetUser(_currentPrincipal.Current);
            }
        }

        public IUser GetUser(IPrincipal principal)
        {
            return UnwrapPrincipal(principal)
                ?? LookupUserFromRepository(GetUsernameFromPrincipal(principal));
        }

        private static IUser UnwrapPrincipal(IPrincipal princ)
        {
            var userProvider = princ as IUserProvider;
            return userProvider == null ? null : userProvider.User;
        }

        private IUser LookupUserFromRepository(string username)
        {
            return username != null ? _repository.GetUser(username) : null;
        }

        private static string GetUsernameFromPrincipal(IPrincipal principal)
        {
            var identity = principal != null ? principal.Identity : null;
            var username = identity != null ? identity.Name : null;
            return username;
        }
    }
}