﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using FirstLook.Common.Core.Logging;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal class AuthorizeUser : IAuthorizeUser
    {
        private readonly IUserServices _userServices;
        private static readonly ILog Log = LoggerFactory.GetLogger<AuthorizeUser>();

        public AuthorizeUser(IUserServices userServices)
        {
            _userServices = userServices;
        }

        public AuthorizationResult IsAuthorized(IPrincipal principal, IList<string> assertAllRoles = null, IList<string> assertAnyRoles = null)
        {
            var user = _userServices.GetUser(principal);
            if (user == null)
            {
                if(Log.IsInfoEnabled)
                    Log.InfoFormat("{0} username='{1}'",
                                   Resources.NotAuthenticated,
// ReSharper disable ConditionIsAlwaysTrueOrFalse -- I don't trust this assertion for IPrincipal.Identity
                                   principal != null && principal.Identity != null ? principal.Identity.Name : null);
// ReSharper restore ConditionIsAlwaysTrueOrFalse
                return new AuthorizationResult(Resources.NotAuthenticated);
            }

            if (assertAllRoles != null && assertAllRoles.Count > 0 && !assertAllRoles.All(user.IsInRole))
            {
                if (Log.IsInfoEnabled)
                    Log.InfoFormat("{0} username='{1}' assertAllRoles='{2}'",
                                   Resources.NotEnoughPermissions,
                                   user.UserName,
                                   string.Join(",", assertAllRoles));
                return new AuthorizationResult(Resources.NotEnoughPermissions);
            }

            if (assertAnyRoles != null && assertAnyRoles.Count > 0 && !assertAnyRoles.Any(user.IsInRole))
            {
                if (Log.IsInfoEnabled)
                    Log.InfoFormat("{0} username='{1}' assertAnyRoles='{2}'",
                                   Resources.NotEnoughPermissions,
                                   user.UserName,
                                   string.Join(",", assertAnyRoles));
                return new AuthorizationResult(Resources.NotEnoughPermissions);
            }

            return AuthorizationResult.Authorized;
        }

        private static class Resources
        {
            public static string NotAuthenticated
            {
                get { return "Access Denied: User not authenticated."; }
            }

            public static string NotEnoughPermissions
            {
                get { return "Access Denied: User does not have sufficient permissions."; }
            }
        }
    }
}