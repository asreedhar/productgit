﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Logging;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal class DealerData : IDealerData
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<DealerData>();

        public IList<IDealer> AllDealers { get; private set; }
        public IDictionary<int, IDealer> DealersById { get; private set; }
        public IDictionary<string, IDealer> DealersByCode { get; private set; }

        public DealerData(IList<IDealer> allDealers)
        {
            AllDealers = allDealers;
            DealersById = AllDealers.ToDictionary(d => d.Id, d => d);
            DealersByCode = GetDistinctDealersByCode(AllDealers);
        }

        private static IDictionary<string, IDealer> GetDistinctDealersByCode(IEnumerable<IDealer> allDealers)
        {
            var dealersByCode = new Dictionary<string, IDealer>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var dealer in allDealers)
            {
                if (dealersByCode.ContainsKey(dealer.Code))
                {
                    if (Log.IsWarnEnabled)
                    {
                        Log.WarnFormat("Duplicate business unit code detected. {0}/{1} is a duplicate of {2}/{3}.",
                                       dealersByCode[dealer.Code].Id, dealersByCode[dealer.Code].Code,
                                       dealer.Id, dealer.Code);
                    }
                }
                else
                {
                    dealersByCode.Add(dealer.Code, dealer);
                }
            }

            return dealersByCode;
        }
    }
}