using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IDealerServices
    {
        IDealer GetDealerById(int id);
        IDealer GetDealerByCode(string code);

        IEnumerable<IDealer> GetAllDealers();
        IEnumerable<IDealer> GetDealersById(IEnumerable<int> ids);
    }
}