using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Logging;
using FirstLook.DomainModel.Oltp;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal sealed class UserRepository : IUserRepository
    {
        private readonly IDealerServices _dealerServices;
        private static readonly ILog Log = LoggerFactory.GetLogger<UserRepository>();

        public UserRepository(IDealerServices dealerServices)
        {
            _dealerServices = dealerServices;
        }

        public IUser GetUser(string username)
        {
            using (MiniProfiler.Current.Step("UserRepository.GetUser()"))
            {
                if (Log.IsInfoEnabled)
                    Log.Info("Starting to execute UserRepository.GetData()");

                var t = DateTimeOffset.UtcNow;

                var data = ReadUserFromDb(username);

                if (Log.IsInfoEnabled)
                    Log.InfoFormat("Completed UserRepository.GetUser(). ElapsedMs={0:0.0}",
                                   (DateTimeOffset.UtcNow - t).TotalMilliseconds);

                return data;
            }
        }

        private IUser ReadUserFromDb(string username)
        {
            username = username.ToLowerInvariant();
            Log.DebugFormat("Looking up user: {0}", username);

            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "dbo.User#Fetch";

                cmd.AddRequiredParameter("@login", username, DbType.String);

                Log.Debug("Executing user reader.");
                using(var r = cmd.ExecuteReader())
                {
                    var accum = new Accumulator(r, username, _dealerServices);
                    accum.ReadImtUser();
                    accum.MoveNext();
                    accum.ReadImtDealers();
                    accum.MoveNext();
                    accum.ReadAspnetUserId();
                    accum.MoveNext();
                    accum.ReadAspnetRoles();

                    Log.Debug("returning a user");

                    return accum.ToUser();
                }
            }
        }

        private class Accumulator
        {
            private readonly IDataReader _reader;
            private int? _memberId;
            private string _firstName;
            private string _lastName;
            private bool _imtIsAdmin;
            private readonly List<int> _businessUnits;
            private Guid? _userId;
            private readonly List<string> _roleNames;
            private readonly string _userName;
            private readonly IDealerServices _dealerServices;

            public Accumulator(IDataReader dataReader, string userName, IDealerServices dealerServices)
            {
                _reader = dataReader;
                _userName = userName;
                _businessUnits = new List<int>();
                _roleNames = new List<string>();
                _dealerServices = dealerServices;
            }

            public void MoveNext()
            {
                if(!_reader.NextResult())
                    throw new InvalidOperationException("Failed to find expected result set.");
            }

            private void ThrowIfMoreRecords()
            {
                if (_reader.Read())
                    throw new InvalidOperationException("Too many records detected.");
            }

            public void ReadImtUser()
            {
                if (!_reader.Read())
                    return;

                var memberIdField = _reader.GetOrdinal("MemberId");
                var firstNameField = _reader.GetOrdinal("FirstName");
                var lastnameField = _reader.GetOrdinal("LastName");
                var memberTypeField = _reader.GetOrdinal("MemberType");

                _memberId = _reader.IsDBNull(memberIdField) ? (int?) null : _reader.GetInt32(memberIdField);
                _firstName = _reader.IsDBNull(firstNameField) ? null : _reader.GetString(firstNameField);
                _lastName = _reader.IsDBNull(lastnameField) ? null : _reader.GetString(lastnameField);
                _imtIsAdmin = !_reader.IsDBNull(memberTypeField) 
                    && _reader.GetInt32(memberTypeField) == (int) MemberType.Administrator;

                ThrowIfMoreRecords();
            }

            public void ReadImtDealers()
            {
                var businessUnitIdField = _reader.GetOrdinal("BusinessUnitId");

                while(_reader.Read())
                {
                    _businessUnits.Add(_reader.GetInt32(businessUnitIdField));
                }
            }

            public void ReadAspnetUserId()
            {
                if (!_reader.Read())
                    return;

                var userIdField = _reader.GetOrdinal("UserId");

                _userId = _reader.IsDBNull(userIdField) ? (Guid?) null : _reader.GetGuid(userIdField);

                ThrowIfMoreRecords();
            }

            public void ReadAspnetRoles()
            {
                var rolenameField = _reader.GetOrdinal("Rolename");

                while(_reader.Read())
                {
                    _roleNames.Add(_reader.GetString(rolenameField));
                }
            }

            public IUser ToUser()
            {
                return new User(_memberId, _userId, _userName, _firstName, _lastName,
                    _imtIsAdmin, _roleNames, _dealerServices, _businessUnits);
            }
        }
    }
}