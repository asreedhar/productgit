﻿using System;
using System.Web.Mvc;
using FirstLook.Common.Core.IOC;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization.Mvc
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AuthorizeDealerAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly IAuthorizeDealer _authorizeDealer;
        private readonly IUnauthorizedResultFactory _unauthorizedResultFactory;

        public AuthorizeDealerAttribute(IAuthorizeDealer authorizeDealer, IUnauthorizedResultFactory unauthorizedResultFactory)
        {
            _authorizeDealer = authorizeDealer;
            _unauthorizedResultFactory = unauthorizedResultFactory;
            DealerParameter = "dealer";
        }

        public AuthorizeDealerAttribute()
            : this(Registry.Resolve<IAuthorizeDealer>(), Registry.Resolve<IUnauthorizedResultFactory>())
        {
        }

        public string DealerParameter { get; set; }

        public string AssertDealerUpgrades
        {
            get { return _upgradeList.Original; }
            set { _upgradeList = new DealerUpgradeList(value); }
        }
        private DealerUpgradeList _upgradeList = DealerUpgradeList.Empty;


        public virtual void OnAuthorization(AuthorizationContext filterContext)
        {
            filterContext.Result = AuthorizeCore(filterContext);
        }

        private ActionResult AuthorizeCore(AuthorizationContext filterContext)
        {
            var principal = filterContext.HttpContext.User;
            var dealerCode = (string)filterContext.RouteData.Values[DealerParameter];
            var authResult = _authorizeDealer.IsAuthorized(principal, dealerCode, _upgradeList.List);

            return authResult.IsAuthorized
                ? null
                : _unauthorizedResultFactory.CreateResult(authResult.Message);
        }
    }
}