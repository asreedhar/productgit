﻿using System;
using System.Web.Mvc;
using FirstLook.Common.Core.IOC;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization.Mvc
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AuthorizeUserAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly IAuthorizeUser _authorizeUser;
        private readonly IUnauthorizedResultFactory _unauthorizedResultFactory;

        public AuthorizeUserAttribute()
            : this(Registry.Resolve<IAuthorizeUser>(), Registry.Resolve<IUnauthorizedResultFactory>())
        {
        }

        internal AuthorizeUserAttribute(IAuthorizeUser authorizeUser, IUnauthorizedResultFactory unauthorizedResultFactory)
        {
            _authorizeUser = authorizeUser;
            _unauthorizedResultFactory = unauthorizedResultFactory;
        }

        public string AssertAllRoles
        {
            get { return _assertAllRoles.Original; }
            set { _assertAllRoles = new RoleList(value); }
        }
        private RoleList _assertAllRoles = RoleList.Empty;

        public string AssertAnyRoles
        {
            get { return _assertAnyRoles.Original; }
            set { _assertAnyRoles = new RoleList(value); }
        }
        private RoleList _assertAnyRoles = RoleList.Empty;

        public virtual void OnAuthorization(AuthorizationContext filterContext)
        {
            filterContext.Result = AuthorizeCore(filterContext);
        }

        protected virtual ActionResult AuthorizeCore(AuthorizationContext filterContext)
        {
            var principal = filterContext.HttpContext.User;
            var authMsg = _authorizeUser.IsAuthorized(principal, _assertAllRoles.List, _assertAnyRoles.List);

            return authMsg.IsAuthorized
                       ? null
                       : _unauthorizedResultFactory.CreateResult(authMsg.Message);
        }
    }
}