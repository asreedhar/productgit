﻿using System;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal struct DealerUpgradeKey : IEquatable<DealerUpgradeKey>
    {
        public int Id { get; set; }
        public Upgrade Upgrade { get; set; }

        public DealerUpgradeKey(int id, Upgrade upgrade) : this()
        {
            Id = id;
            Upgrade = upgrade;
        }

        public bool Equals(DealerUpgradeKey other)
        {
            return other.Id == Id && Equals(other.Upgrade, Upgrade);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != typeof (DealerUpgradeKey)) return false;
            return Equals((DealerUpgradeKey) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id*397) ^ Upgrade.GetHashCode();
            }
        }
    }
}