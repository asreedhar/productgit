﻿using System.Collections.Generic;
using System.Security.Principal;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IAuthorizeUser
    {
        AuthorizationResult IsAuthorized(IPrincipal principal, IList<string> assertAllRoles = null,
                                         IList<string> assertAnyRoles = null);
    }
}