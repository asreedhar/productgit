namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IUserCachedRepository : IUserRepository
    {
        void InvalidateUserCache(string username);
    }
}