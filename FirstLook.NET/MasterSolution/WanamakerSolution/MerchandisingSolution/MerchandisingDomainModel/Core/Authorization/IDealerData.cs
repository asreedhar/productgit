using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal interface IDealerData
    {
        IList<IDealer> AllDealers { get; }
        IDictionary<int, IDealer> DealersById { get; }
        IDictionary<string, IDealer> DealersByCode { get; }
    }
}