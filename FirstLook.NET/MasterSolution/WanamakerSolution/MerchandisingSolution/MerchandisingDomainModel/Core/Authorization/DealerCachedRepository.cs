﻿using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal sealed class DealerCachedRepository : IDealerRepository
    {
        private static readonly string CacheKey = typeof(DealerCachedRepository).FullName;
        private const int CacheExpiration = 60*5; // 5 minutes

        private readonly ICache Cache;
        private readonly IDealerRepository Inner;

        public DealerCachedRepository(IDealerRepository inner, ICache cache)
        {
            Cache = cache;
            Inner = inner;
        }

        public IDealerData GetData()
        {
            var data = Cache.Get(CacheKey) as IDealerData;
            if(data == null)
            {
                data = Inner.GetData();
                Cache.Set(CacheKey, data, CacheExpiration);
            }
            return data;
        }
    }
}