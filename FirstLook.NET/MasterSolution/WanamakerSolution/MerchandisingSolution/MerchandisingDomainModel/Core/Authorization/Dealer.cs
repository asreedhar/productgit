using System;
using System.Collections.Generic;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal sealed class Dealer : IDealer
    {
        public Guid? OwnerHandle { get; private set; }

        public int Id { get; private set; }
        
        public string Code { get; private set; }
        
        public string Name { get; private set; }
        
        public bool HasUpgrade(Upgrade upgrade)
        {
            return Upgrades.Contains(new DealerUpgradeKey(Id, upgrade));
        }

        public bool HasWebLoader { get; private set; }

        private ISet<DealerUpgradeKey> Upgrades { get; set; }

        internal Dealer(int id, Guid? handle, string code, string name, ISet<DealerUpgradeKey> upgrades, bool hasWebLoader)
        {
            Id = id;
            OwnerHandle = handle;
            Code = code;
            Name = name;
            Upgrades = upgrades;
            HasWebLoader = hasWebLoader;
        }
    }
}