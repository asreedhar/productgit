﻿using System.Collections.Generic;
using System.Security.Principal;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IAuthorizeDealer
    {
        AuthorizationResult IsAuthorized(IPrincipal principal, string dealerCode,
                                         IList<Upgrade> assertDealerUpgrades = null,
                                         bool assertDealerHasWebLoaderUpgrade = false);
    }
}