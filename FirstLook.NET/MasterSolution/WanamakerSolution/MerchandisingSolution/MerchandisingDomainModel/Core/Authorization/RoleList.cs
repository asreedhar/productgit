using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public class RoleList
    {
        private readonly string _original;
        private readonly Lazy<IList<string>> _list; 

        public RoleList(string original)
        {
            _original = original ?? "";
            _list = new Lazy<IList<string>>(
                ParseList, LazyThreadSafetyMode.PublicationOnly);
        }

        public string Original { get { return _original; } }

        public IList<string> List {
            get { return _list.Value; } 
        }

        private IList<string> ParseList()
        {
            return _original
                .Split(',')
                .Select(s => s.Trim())
                .Where(s => !String.IsNullOrEmpty(s))
                .ToList()
                .AsReadOnly();
        }

        public static readonly RoleList Empty = new RoleList("");
    }
}