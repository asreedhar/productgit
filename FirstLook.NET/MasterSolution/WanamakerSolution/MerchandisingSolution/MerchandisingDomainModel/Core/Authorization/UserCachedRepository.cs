﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public class UserCachedRepository : IUserCachedRepository
    {
        private readonly ICache _cache;
        private readonly IUserRepository _inner;
        private static readonly string CacheKeyPrefix = typeof (UserCachedRepository).FullName + "_";
        private const int SecondsToExpire = 5*60; // 5 minutes
        private static readonly ILog Log = LoggerFactory.GetLogger<UserCachedRepository>();

        public UserCachedRepository(ICache cache, IUserRepository inner)
        {
            _cache = cache;
            _inner = inner;
        }

        public IUser GetUser(string username)
        {
            if (username == null)
                throw new ArgumentNullException("username");

            Log.Debug("called GetUser");
            var cacheKey = CacheKeyPrefix + username;
            Log.DebugFormat("Checking user cache for {0}", cacheKey);
            var user = (IUser) _cache.Get(cacheKey);
            if(user == null)
            {
                Log.DebugFormat("Cache user not found.");
                user = _inner.GetUser(username);

                _cache.Set(cacheKey, user ?? NullUserObj, SecondsToExpire);
            }

            return user is NullUser ? null : user;
        }

        public void InvalidateUserCache(string username)
        {
            if(username == null)
                throw new ArgumentNullException("username");

            var cacheKey = CacheKeyPrefix + username;
            _cache.Delete(cacheKey);
        }

        private static readonly IUser NullUserObj = new NullUser();

        private class NullUser : IUser
        {
            #region Not Implemented - this is just a marker class.
            public int? ImtId
            {
                get { return null; }
            }

            public Guid? MerchandisingId
            {
                get { return null; }
            }

            public string UserName
            {
                get { return null; }
            }

            public string FirstName
            {
                get { return null; }
            }

            public string LastName
            {
                get { return null; }
            }

            public bool IsAdmin
            {
                get { return false; }
            }

            public bool IsInRole(string role)
            {
                return false;
            }

            public bool HasAccessTo(IDealer dealerOrDealerGroup)
            {
                return false;
            }

            public IEnumerable<IDealer> GetDealers()
            {
                return Enumerable.Empty<IDealer>();
            }

            #endregion
        }
    }
}