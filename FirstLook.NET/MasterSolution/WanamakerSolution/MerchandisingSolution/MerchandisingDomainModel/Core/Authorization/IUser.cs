using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IUser
    {
        int? ImtId { get; }
        Guid? MerchandisingId { get; }
        string UserName { get; }
        string FirstName { get; }
        string LastName { get; }

        bool IsAdmin { get; }
        bool IsInRole(string role);
        
        bool HasAccessTo(IDealer dealerOrDealerGroup);
        IEnumerable<IDealer> GetDealers();
    }
}