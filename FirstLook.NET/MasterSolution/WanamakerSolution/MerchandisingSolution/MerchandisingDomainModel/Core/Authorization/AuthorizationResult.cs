namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public class AuthorizationResult
    {
        public string Message { get; private set; }
        public bool IsAuthorized { get { return Message == null; } }
        public bool NotFound { get; private set; }

        public static readonly AuthorizationResult Authorized = new AuthorizationResult(null);

        public AuthorizationResult(string message, bool resourceNotFound = false)
        {
            Message = string.IsNullOrWhiteSpace(message) ? null : message;
            NotFound = resourceNotFound;
        }
    }
}