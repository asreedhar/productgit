﻿namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IUserProvider
    {
        IUser User { get; }
    }
}