﻿using System.Web.Mvc;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IUnauthorizedResultFactory
    {
        ActionResult CreateResult(string details);
    }
}