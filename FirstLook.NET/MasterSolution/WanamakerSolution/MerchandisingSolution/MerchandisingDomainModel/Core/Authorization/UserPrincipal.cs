﻿using System.Security.Principal;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public class UserPrincipal : IPrincipal, IUserProvider
    {
        public readonly IUser _inner;
        private readonly IIdentity _identity;

        public UserPrincipal(IUser user, string authenticationType = null)
        {
            _inner = user;
            _identity = new UserIdentity(user, authenticationType);
        }

        public IUser User
        {
            get { return _inner; }
        }

        public bool IsInRole(string role)
        {
            return _inner.IsInRole(role);
        }

        public IIdentity Identity
        {
            get { return _identity; }
        }

        private class UserIdentity : IIdentity
        {
            private readonly IUser _inner;
            private readonly string _authenticationType;

            public UserIdentity(IUser user, string authenticationType)
            {
                _inner = user;
                _authenticationType = authenticationType;
            }

            public string Name
            {
                get { return _inner.UserName; }
            }

            public string AuthenticationType
            {
                get { return _authenticationType; }
            }

            public bool IsAuthenticated
            {
                get { return !string.IsNullOrEmpty(_authenticationType); }
            }
        }
    }
}