using System;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IDealer
    {
        Guid? OwnerHandle { get; }
        int Id { get; }

        string Code { get; }
        string Name { get; }

        bool HasUpgrade(Upgrade upgrade);

        bool HasWebLoader { get; }
    }
}