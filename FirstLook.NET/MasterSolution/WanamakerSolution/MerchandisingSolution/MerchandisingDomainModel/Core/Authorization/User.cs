using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal sealed class User : IUser
    {
        private readonly IDealerServices _dealerServices;
        public const string AdministratorRole = "Administrator";

        private readonly HashSet<string> _roles;
        private readonly HashSet<int> _dealers;

        internal User(
            int? imtId, Guid? merchandisingId,
            string username, string firstName, string lastName, bool isAdmin, 
            IEnumerable<string> roles, 
            IDealerServices dealerServices, IEnumerable<int> dealers)
        {
            _dealerServices = dealerServices;
            ImtId = imtId;
            MerchandisingId = merchandisingId;
            UserName = username;
            FirstName = firstName;
            LastName = lastName;
            _roles = new HashSet<string>(roles, StringComparer.InvariantCultureIgnoreCase);
            _dealers = new HashSet<int>(dealers);

            IsAdmin = _roles.Contains(AdministratorRole) && isAdmin;
        }

        public int? ImtId { get; private set; }
        
        public Guid? MerchandisingId { get; private set; }

        public string UserName { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public bool IsAdmin { get; private set; }

        public bool IsInRole(string role)
        {
            return _roles.Contains(role);
        }

        public bool HasAccessTo(IDealer dealerOrDealerGroup)
        {
            return dealerOrDealerGroup != null 
                && (IsAdmin || _dealers.Contains(dealerOrDealerGroup.Id));
        }

        public IEnumerable<IDealer> GetDealers()
        {
            return IsAdmin 
                ? _dealerServices.GetAllDealers() 
                : _dealerServices.GetDealersById(_dealers);
        }
    }
}