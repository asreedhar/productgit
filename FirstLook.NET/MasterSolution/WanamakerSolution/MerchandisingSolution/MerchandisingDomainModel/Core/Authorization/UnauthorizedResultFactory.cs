﻿using System.Web.Configuration;
using System.Web.Mvc;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal class UnauthorizedResultFactory : IUnauthorizedResultFactory
    {
        private readonly string _redirectTo;

        public UnauthorizedResultFactory()
            : this(GetWebAppCustomErrorsSection())
        {
        }

        public UnauthorizedResultFactory(CustomErrorsSection section)
        {
            var customError = section.Errors["403"];
            if (customError != null)
                _redirectTo = customError.Redirect;
        }

        private static CustomErrorsSection GetWebAppCustomErrorsSection()
        {
            var config = WebConfigurationManager.OpenWebConfiguration("~/");
            return (CustomErrorsSection)config.GetSection("system.web/customErrors");
        }

        public ActionResult CreateResult(string details)
        {
            return _redirectTo != null
                       ? (ActionResult) new RedirectResult(_redirectTo)
                       : new UnauthorizedResult {Message = details};
        }
    }
}