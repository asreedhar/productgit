using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public class DealerUpgradeList
    {
        private readonly string _dealerUpgradeCsv;
        private readonly Lazy<IList<Upgrade>> _dealerUpgradeList;
        private static readonly IList<Upgrade> EmptyList = new Upgrade[0];
            
        public DealerUpgradeList(string dealerUpgradeCsv)
        {
            _dealerUpgradeCsv = dealerUpgradeCsv ?? "";
            _dealerUpgradeList = new Lazy<IList<Upgrade>>(
                ParseDealerUpgradeCsv,
                LazyThreadSafetyMode.PublicationOnly);
        }

        public string Original { get { return _dealerUpgradeCsv; } }
        public IList<Upgrade> List { get { return _dealerUpgradeList.Value; } }
        public static readonly DealerUpgradeList Empty = new DealerUpgradeList("");

        private IList<Upgrade> ParseDealerUpgradeCsv()
        {
            return String.IsNullOrWhiteSpace(_dealerUpgradeCsv)
                       ? EmptyList
                       : _dealerUpgradeCsv
                             .Split(',')
                             .Select(ParseUpgrade)
                             .ToList()
                             .AsReadOnly();
        }

        private static Upgrade ParseUpgrade(string s)
        {
            Upgrade upg;
            if (!Enum.TryParse(s, true, out upg))
                throw new InvalidOperationException(
                    "Invalid upgrade code '" + (s ?? "") + "'");
            return upg;
        }
    }
}