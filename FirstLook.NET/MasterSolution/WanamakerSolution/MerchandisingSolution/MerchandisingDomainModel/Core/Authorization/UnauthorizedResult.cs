﻿using System.Globalization;
using System.IO;
using System.Text;
using System.Web.Mvc;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public class UnauthorizedResult : ActionResult
    {
        public const string DefaultMessage = "Unauthorized";
        public string Message { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            var description = Message ?? DefaultMessage;
            var response = context.HttpContext.Response;
            response.StatusCode = 401;
            response.StatusDescription = description;
            response.ContentType = "text/html";
            response.Charset ="utf-8";

            using (var str = new MemoryStream(200))
            {
                using (var wr = new StreamWriter(str, Encoding.UTF8))
                {
                    wr.WriteLine("<!doctype html>");
                    
                    wr.Write("<html><head><title>");
                    wr.Write(description);
                    wr.Write("</head></title>");
                    wr.WriteLine();

                    wr.Write("<body><h1>");
                    wr.Write(description);
                    wr.Write("</h1></body></html>");
                }

                var buffer = str.GetBuffer();
                response.Headers.Add("Content-Length", buffer.Length.ToString(CultureInfo.InvariantCulture));
                response.OutputStream.Write(buffer, 0, buffer.Length);
            }
        }
    }
}