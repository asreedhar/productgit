using System.Collections.Generic;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    internal class DealerServices : IDealerServices
    {
        private readonly IDealerRepository Repository;

        internal DealerServices(IDealerRepository repository)
        {
            Repository = repository;
        }

        public IDealer GetDealerById(int id)
        {
            IDealer dealer;
            Repository.GetData().DealersById.TryGetValue(id, out dealer);
            return dealer;
        }

        public IDealer GetDealerByCode(string code)
        {
            IDealer dealer;
            Repository.GetData().DealersByCode.TryGetValue(code, out dealer);
            return dealer;
        }

        public IEnumerable<IDealer> GetAllDealers()
        {
            return Repository.GetData().AllDealers;
        }

        public IEnumerable<IDealer> GetDealersById(IEnumerable<int> ids)
        {
            var dealerLookup = Repository.GetData().DealersById;
            return ids.Select(id => GetDealerOrDefault(dealerLookup, id)).Where(d => d != null);
        }

        private static IDealer GetDealerOrDefault(IDictionary<int, IDealer> lookup, int id)
        {
            IDealer dealer;
            lookup.TryGetValue(id, out dealer);
            return dealer;
        }
    }
}