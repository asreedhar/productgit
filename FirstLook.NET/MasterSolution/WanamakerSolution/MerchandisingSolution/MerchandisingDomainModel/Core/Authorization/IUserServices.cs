using System.Security.Principal;
using FirstLook.Merchandising.DomainModel.Core.Authentication;

namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IUserServices
    {
        /// <summary>
        /// Authenticates the supplied credential and sets the current principal to the user 
        /// if the credential was authentic.
        /// </summary>
        /// <param name="credential">The username and password combination.</param>
        /// <returns>Returns true if the credential was valid. Otherwise false.</returns>
        bool AuthenticateAndSetCurrent(BasicCredential credential);

        IUser Current { get; }

        IUser GetUser(IPrincipal principal);
    }
}