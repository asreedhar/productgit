﻿namespace FirstLook.Merchandising.DomainModel.Core.Authorization
{
    public interface IUserRepository
    {
        IUser GetUser(string username);
    }
}