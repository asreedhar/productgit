﻿namespace FirstLook.Merchandising.DomainModel.Core
{
    internal class ColdWeatherEquipmentIdentifier
    {
        /// <summary>
        /// Cold-weather equipment contains special words.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        internal bool IsColdWeatherEquipment(string description)
        {
            if( string.IsNullOrEmpty(description)) return false;

            var desc = description.ToUpper();

            return  desc.Contains("HEATED") ||
                    desc.Contains("COLD") ||
                    desc.Contains("WINTER") ||
                    desc.Contains("SNOW") ||
                    desc.Contains("ALL WEATHER");
        }
    }
}
