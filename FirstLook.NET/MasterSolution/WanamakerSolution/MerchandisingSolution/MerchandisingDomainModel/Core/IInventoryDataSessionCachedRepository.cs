namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface IInventoryDataSessionCachedRepository : IInventoryDataRepository
    {
        void ClearSessionData();
    }
}