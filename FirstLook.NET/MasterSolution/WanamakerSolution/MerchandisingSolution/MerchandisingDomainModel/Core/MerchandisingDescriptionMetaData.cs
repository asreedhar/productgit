﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core
{
    internal class MerchandisingDescriptionMetaData
    {
        internal MerchandisingDescriptionMetaData()
        {
            RenderedTemplateItems = new List<RenderedTemplateItem>();
            Description = string.Empty;
        }

        internal IList<RenderedTemplateItem> RenderedTemplateItems { get; set; }
        internal string Description { get; set; }
    }
}