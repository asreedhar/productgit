﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private readonly IDictionary<TKey, TValue> _inner;
        private readonly Dictionary<TKey, TValue> _empty = new Dictionary<TKey, TValue>();

        public ReadOnlyDictionary()
            : this(null)
        {
        }

        public ReadOnlyDictionary(IDictionary<TKey, TValue> inner)
        {
            _inner = inner ?? _empty;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _inner.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _inner.GetEnumerator();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw NewNotSupportedException();
        }

        public void Clear()
        {
            throw NewNotSupportedException();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _inner.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _inner.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw NewNotSupportedException();
        }

        public int Count { get { return _inner.Count; } }
        public bool IsReadOnly { get { return true; } }
        public bool ContainsKey(TKey key)
        {
            return _inner.ContainsKey(key);
        }

        public void Add(TKey key, TValue value)
        {
            throw NewNotSupportedException();
        }

        public bool Remove(TKey key)
        {
            throw NewNotSupportedException();
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _inner.TryGetValue(key, out value);
        }

        public TValue this[TKey key]
        {
            get { return _inner[key]; }
            set { throw NewNotSupportedException(); }
        }

        public ICollection<TKey> Keys { get { return _inner.Keys; } }
        public ICollection<TValue> Values { get { return _inner.Values; } }

        private static Exception NewNotSupportedException()
        {
            return new NotSupportedException("The dictionary is read-only.");
        }
    }
}