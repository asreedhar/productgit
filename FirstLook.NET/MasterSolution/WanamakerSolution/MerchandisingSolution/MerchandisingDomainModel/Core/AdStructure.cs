﻿using System.Text.RegularExpressions;
using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.Core
{
    internal class AdStructure
    {
        private readonly Regex _headerRegex;

        internal AdStructure()
        {
            _headerRegex = new Regex(@"(?<prefix>(=){2,70})(?<text>[\d\w\s | %/'+-]+?)(?<postfix>[.:!?|(~){1,10}|(...)]+)");
        }

        /// <summary>
        /// Given some text, does it contain a header?  
        /// </summary>
        /// <param name="text">The text to scan</param>
        /// <returns>A pair (tuple).  The boolean tells you whether it contains a header, the string is the header.</returns>
        internal Tuple<bool, string> IsHeader(string text)
        {
            bool isHeader = _headerRegex.IsMatch(text);

            if (!isHeader)
            {
                return new Tuple<bool, string>(false, string.Empty);
            }

            string header = _headerRegex.Match(text).Groups["text"].Value;
            return new Tuple<bool, string>(true, header);
        }

    }
}
