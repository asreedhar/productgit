﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface IInventoryDataRepository
    {
        IEnumerable<IInventoryData> GetAllInventory(int businessUnitId);
    }
}