namespace FirstLook.Merchandising.DomainModel.Core
{
    internal interface ITextFragmentFormatter
    {
        string Format(string inString);
    }
}