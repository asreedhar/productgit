﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.TemplatingModel
{
    public interface ITemplateVisitor
    {
        void Visit(AdTemplate template);
        void Visit(BlurbTemplate blurb);
        void Visit(BlurbText blurbText);
        void Visit(VehicleProfile profile);
    }
}
