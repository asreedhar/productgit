﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Core.TemplatingModel
{
    public class BlurbTemplate : TemplateElement
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(BlurbTemplate).FullName);
        #endregion

        public int Sequence { get; set; }
        public int Priority { get; set; }
        public bool IsRequired { get; set; }

        public int BlurbID { get; set; }
        public string BlurbName {get; set;}
        public string TemplateText { get; set; }
        public string TemplateCondition { get; set; }

        public BlurbTemplate ChildItem { get; set; }

        public BlurbTemplate()
        {
            BlurbName = String.Empty;
            TemplateText = String.Empty;
            TemplateCondition = "false";
        }

        public override void Accept(ITemplateVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
