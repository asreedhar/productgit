﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace FirstLook.Merchandising.DomainModel.Core.TemplatingModel
{
    public abstract class TemplateElement
    {
        public IList<TemplateElement> Children { get; private set; }

        protected TemplateElement()
        {
            Children = new List<TemplateElement>();
        }

        public abstract void Accept(ITemplateVisitor visitor);
    }
}
