﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Core.TemplatingModel
{
    public class BlurbText : TemplateElement
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(BlurbText).FullName);
        #endregion

        public int BlurbTextID { get; set; }
        public string PreGenerateText { get; set; }
        public string PostGenerateText { get; set; }
        public bool Active { get; set; }
        public bool IsLongForm { get; set; }
        public int? ThemeId { get; set; }
        public VehicleProfile Profile { get; set; }

        public override void Accept(ITemplateVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
