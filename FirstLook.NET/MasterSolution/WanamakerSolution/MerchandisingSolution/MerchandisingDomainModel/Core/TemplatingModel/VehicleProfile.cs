﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Core.TemplatingModel
{
    public class VehicleProfile : TemplateElement
    {   
        public bool? CertifiedStatusFilter { get; set; }
        public List<Segments> Segments { get; set; }
        public int? StartYear { get; set; }
        public int? EndYear { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
        public decimal? MinPrice { get; set; }
        public decimal? MaxPrice { get; set; }

        public int Id { get; set; }
        public string Title { get; set; }
        public List<string> Makes { get; set; }
        public List<string> Models { get; set; }
        public List<string> Trims { get; set; }
        public List<string> MktClasses { get; set; }

        public VehicleProfile()
        {
            Id = -1;
            Title = string.Empty;
            Makes = new List<string>();
            Models = new List<string>();
            Trims = new List<string>();
            Segments = new List<Segments>();
            MktClasses = new List<string>();
        }

        public override void Accept(ITemplateVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
