﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Core.TemplatingModel
{
    public class AdTemplate : TemplateElement
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(AdTemplate).FullName);
        #endregion

        public int OwnerBusinessUnitId { get; set; }
        public bool IsActive { get; set; }
        public int TemplateID { get; set; }
        public int? VehicleProfileId { get; set; }
        public string Name { get; set; }

        public override void Accept(ITemplateVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
