﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Core.TemplatingModel
{
    public class TemplatingModelFactory
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(TemplatingModelFactory).FullName);
        #endregion

        public AdTemplate CreateAdTemplateModel(XmlReader reader)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            return ParseAdTemplate(doc);
        }

        private static VehicleProfile ParseProfile(XmlNode node)
        {
            var vehicleProfile = new VehicleProfile();
            if (node == null || node.Attributes["vehicleProfileId"] == null)
                return vehicleProfile;

            int? tmpId = Database.GetNullableInt(node, "vehicleProfileId");
            if (!tmpId.HasValue || tmpId <= 0)
                return vehicleProfile;

            vehicleProfile.Id = tmpId.Value;
            vehicleProfile.Title = node.Attributes["title"].Value;
            vehicleProfile.Makes = new List<string>(node.Attributes["makeIdList"].Value.Split(",".ToCharArray(),
                StringSplitOptions.RemoveEmptyEntries));
            vehicleProfile.Models = new List<string>(node.Attributes["modelList"].Value.Split(",".ToCharArray(),
                StringSplitOptions.RemoveEmptyEntries));
            vehicleProfile.Trims = new List<string>(node.Attributes["trimList"].Value.Split(",".ToCharArray(),
                StringSplitOptions.RemoveEmptyEntries));

            string segIdList = node.Attributes["segmentIdList"].Value;
            SetSegmentList(vehicleProfile, segIdList);

            vehicleProfile.MktClasses =
                new List<string>(node.Attributes["marketClassIdList"].Value.Split(",".ToCharArray(),
                                                                                   StringSplitOptions.RemoveEmptyEntries));

            vehicleProfile.MaxPrice = Database.GetNullableDecimal(node, "maxPrice");
            vehicleProfile.MinPrice = Database.GetNullableDecimal(node, "minPrice");

            vehicleProfile.MaxAge = Database.GetNullableInt(node, "maxAge");
            vehicleProfile.MinAge = Database.GetNullableInt(node, "minAge");

            vehicleProfile.EndYear = Database.GetNullableInt(node, "endYear");
            vehicleProfile.StartYear = Database.GetNullableInt(node, "startYear");

            vehicleProfile.CertifiedStatusFilter = Database.GetNullableBoolean(node, "certifiedStatusFilter");
            
            return vehicleProfile;
        }


        private static void SetSegmentList(VehicleProfile vehicleProfile, string segIdList)
        {
            vehicleProfile.Segments = new List<Segments>();

            foreach (string segId in segIdList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                int segIdVal = Int32.Parse(segId);
                vehicleProfile.Segments.Add((Segments)segIdVal);
            }
        }

        private static BlurbText ParseBlurbText(XmlNode node)
        {
            var blurbText = new BlurbText();
            if (node == null)
            {
                Log.Error("xNode is null.");
                throw new ArgumentException(@"xNode is null.", "node");
            }

            if (node.Attributes == null)
            {
                Log.Error("xNode.Attributes is null.");
                throw new ArgumentException(@"xNode.Attributes is null.", "node");
            }

            blurbText.BlurbTextID = Int32.Parse(node.Attributes["blurbTextID"].Value);
            blurbText.PreGenerateText = Convert.ToString(node.Attributes["blurbPreText"].Value);
            blurbText.PostGenerateText = Convert.ToString(node.Attributes["blurbPostText"].Value);
            blurbText.Active = Convert.ToBoolean(Convert.ToInt32(node.Attributes["active"].Value));
            blurbText.IsLongForm = Convert.ToBoolean(Convert.ToInt32(node.Attributes["isLongForm"].Value));
            blurbText.ThemeId = Database.GetNullableInt(node, "themeId");
            blurbText.Profile = node.ChildNodes.Count > 0 ? ParseProfile(node.ChildNodes[0]) : new VehicleProfile();

            return blurbText;
        }

        private static BlurbTemplate ParseBlurbTemplate(XmlNode node)
        {
            var blurbTemplate = new BlurbTemplate();
            if (node.Attributes == null)
                return blurbTemplate;

            if (node.Attributes.Count < 5 || !node.HasChildNodes)
                return blurbTemplate;

            blurbTemplate.Sequence = Convert.ToInt32(node.Attributes["sequence"].Value);
            blurbTemplate.Priority = Convert.ToInt32(node.Attributes["priority"].Value);
            blurbTemplate.IsRequired = Convert.ToBoolean(Convert.ToInt32(node.Attributes["required"].Value));

            return FillBlurbTemplate(blurbTemplate, node["BlurbTemplate"]);
        }

        private static BlurbTemplate FillBlurbTemplate(BlurbTemplate blurbTemplate, XmlNode node)
        {
            if (node == null)
            {
                Log.Error("The xNode is null.");
                throw new ApplicationException("The xNode is null.");
            }
            if (node.Attributes == null)
            {
                Log.Error("The XmlNode.Attributes is null.");
                throw new ApplicationException("The XmlNode.Attributes is null.");
            }

            if (node.Attributes.Count >= 4)
            {
                blurbTemplate.BlurbID = Convert.ToInt32(node.Attributes["blurbID"].Value);
                blurbTemplate.BlurbName = Convert.ToString(node.Attributes["blurbName"].Value);
                blurbTemplate.TemplateText = Convert.ToString(node.Attributes["blurbTemplate"].Value);
                blurbTemplate.TemplateCondition = Convert.ToString(node.Attributes["blurbCondition"].Value);
                try
                {
                    foreach (XmlNode xn in node.ChildNodes)
                    {
                        blurbTemplate.Children.Add(ParseBlurbText(xn));
                    }
                }
                catch (Exception ex)
                {
                    string error = String.Format("Error encountered processing XmlNode with OuterXml: '{0}'", node.OuterXml);
                    Log.Error(error, ex);
                }
            }
            else
            {
                Log.WarnFormat("It seems that if xNode.Attributes.Count < 4, the logical thing to do is absolutely nothing. Sigh. xNode.OuterXml is: '{0}'", node.OuterXml);
            }

            return blurbTemplate;
        }

        private static AdTemplate ParseAdTemplate(XmlNode parentNode)
        {
            var adTemplate = new AdTemplate();

            Dictionary<int, BlurbTemplate> childItems = new Dictionary<int, BlurbTemplate>();

            foreach (XmlNode node in parentNode.SelectNodes("//AdTemplate"))
            {
                if (node.Attributes == null || node.Attributes["templateId"] == null)
                {
                    Log.Error("node.Attributes is null or node.Attributes['templateId'] is null");
                    throw new ApplicationException("node.Attributes is null or node.Attributes['templateId'] is null");
                }

                adTemplate.TemplateID = Int32.Parse(node.Attributes["templateId"].Value);
                adTemplate.VehicleProfileId = Database.GetNullableInt(node, "vehicleProfileId");
                adTemplate.Name = node.Attributes["name"].Value;
                adTemplate.IsActive = Convert.ToBoolean(Convert.ToInt32(node.Attributes["active"].Value));
                adTemplate.OwnerBusinessUnitId = Int32.Parse(node.Attributes["businessUnitId"].Value);
            }

            foreach (XmlNode node in parentNode.SelectNodes("//TemplateItem"))
            {
                if (node.Attributes == null || node.Attributes["parentID"] == null)
                {
                    Log.Error("node.Attributes is null or node.Attributes['parentID'] is null");
                    throw new ApplicationException("node.Attributes is null or node.Attributes['parentID'] is null");
                }

                int parentID = Convert.ToInt32(node.Attributes["parentID"].Value);
                if (parentID > 0)
                {
                    childItems.Add(parentID, ParseBlurbTemplate(node));
                }
                else
                {
                    adTemplate.Children.Add(ParseBlurbTemplate(node));
                }
            }

            foreach (KeyValuePair<int, BlurbTemplate> kvp in childItems)
            {
                foreach (BlurbTemplate ti in adTemplate.Children)
                {
                    if (ti.BlurbID == kvp.Key)
                    {
                        ti.ChildItem = kvp.Value;
                    }
                }
            }

            return adTemplate;
        }
    }
}
