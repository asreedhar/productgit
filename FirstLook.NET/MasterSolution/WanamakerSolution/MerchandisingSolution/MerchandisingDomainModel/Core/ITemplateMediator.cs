﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface ITemplateMediator
    {
        IVehicleTemplatingFactory VehicleTemplatingFactory { get; set; }
        IVehicleTemplatingData VehicleData { get; set; }
        int? ThemeId { get; }
        bool TemplateHasTagline { get; }
        List<int> BlockedItems { get; set; }
        int TemplateId { get; set; }
        bool IsInLongMode { get; set; }
        bool HasTagline { get; }
        Dictionary<string, string> MerchandisingPointsMap { get; }
        void ResetDefaultTemplateSettings(int styleId);
        string GetUiText(string field);

        /// <summary>
        /// method is used to evaluate conditional text, toggling the system to record when it is checking
        /// the condition so it doesn't log data access as acutal use of the data
        /// </summary>
        /// <param name="conditionText"></param>
        /// <returns></returns>
        bool EvaluateCondition(string conditionText);

        /// <summary>
        /// used to log which description data points are added to the description
        /// </summary>
        /// <param name="itemType"></param>
        void DescriptionContentAdded(MerchandisingDescriptionItemType itemType);

        /// <summary>
        /// Returns the current description
        /// </summary>
        /// <returns></returns>
        MerchandisingDescription GetCurrentMerchandisingDescription();

        /// <summary>
        /// will take the current description if exists, otherwise, generates one
        /// </summary>
        /// <returns></returns>
        MerchandisingDescription GetOrCreateMerchandisingDescription();

        MerchandisingDescription GetOrCreateMerchandisingDescription(bool hasTrim);
        MerchandisingDescription GetOrCreateMerchandisingDescription(int templateId, int? useThemeId, int maxThemeSentences);

        /// <summary>
        /// Gets the current description if exists, otherwise it will create with the given params
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="useThemeId"></param>
        /// <param name="maxThemeSentences"></param>
        /// <param name="hasTrim"></param>
        /// <returns></returns>
        MerchandisingDescription GetOrCreateMerchandisingDescription(int templateId, int? useThemeId, int maxThemeSentences, bool hasTrim);

        /// <summary>
        /// Forces the regeneration of a template's text
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="useThemeId"></param>
        /// <param name="maxThemeSentences"></param>
        /// <returns></returns>
        MerchandisingDescription ReGenerateTemplateText(Advertisement editedModel, int? templateId, int? useThemeId, int maxThemeSentences);

        MerchandisingDescription GenerateDescription();

        /// <summary>
        /// overload to handle missing character limit
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="useThemeId"></param>
        /// <param name="maxThemeSentences"></param>
        /// <returns></returns>
        MerchandisingDescription GenerateDescription(int? templateId, int? useThemeId, int maxThemeSentences);

        void RegisterPreviewItems(IList<PreviewItem> previewItems);
        PreviewItem[] GetPreviewItems();
        int CalulateFooterLength();
    }
}
