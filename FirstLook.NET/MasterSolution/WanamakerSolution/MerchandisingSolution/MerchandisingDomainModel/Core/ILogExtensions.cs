﻿using System;
using log4net;
namespace FirstLook.Merchandising.DomainModel.Core
{
    /// <summary>
    /// Provides Trace and Verbose log levels for even more granularity.
    /// </summary>
    /// <remarks>
    /// Trace is a higher level than Verbose. Noted here because it was counter-intuitive for me. -Travis
    ///     * ERROR
    ///     * WARN
    ///     * INFO
    ///     * DEBUG
    ///     * TRACE
    ///     * VERBOSE
    /// </remarks>
    public static class ILogExtensions
    {
        public static void Trace(this ILog log, string message, Exception exception)
        {
            log.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,
                log4net.Core.Level.Trace, message, exception);
        }

        public static void Trace(this ILog log, string message)
        {
            log.Trace(message, null);
        }

        public static void TraceFormat(this ILog log, string message, params object[] argStrings)
        {
            log.Trace(string.Format(message, argStrings), null);
        }

        public static void Verbose(this ILog log, string message, Exception exception)
        {
            log.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,
                log4net.Core.Level.Verbose, message, exception);
        }

        public static void Verbose(this ILog log, string message)
        {
            log.Verbose(message, null);
        }

        public static void VerboseFormat(this ILog log, string message, params object[] argStrings)
        {
            log.Verbose(string.Format(message, argStrings), null);
        }
    }
}
