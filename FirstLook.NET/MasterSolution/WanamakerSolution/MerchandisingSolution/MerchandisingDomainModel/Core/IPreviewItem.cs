﻿using System;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface IPreviewItem
    {
        double GetWeight();
        int GetTierNumber();
        PreviewGroupType GroupType { get; }
        string Key { get; set; }
        double GetRandomizedWeight(Random random);
        int CompareTo(IWeightedItem other);
        string ToString(string separator);
        string ToString();
    }
}
