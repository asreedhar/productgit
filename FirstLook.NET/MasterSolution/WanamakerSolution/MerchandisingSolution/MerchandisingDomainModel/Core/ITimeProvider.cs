using System;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface ITimeProvider
    {
        DateTimeOffset UtcNow { get; }
    }
}