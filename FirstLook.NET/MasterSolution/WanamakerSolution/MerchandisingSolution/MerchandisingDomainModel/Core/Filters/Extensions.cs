﻿using MAX.Entities.Filters;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    public static class Extensions
    {
        public static void FilterByNewOrUsed(this IInventoryQueryBuilder builder, int newOrUsed)
        {
            builder.FilterByNewOrUsed((UsedOrNewFilter) newOrUsed);
        }
    }
}