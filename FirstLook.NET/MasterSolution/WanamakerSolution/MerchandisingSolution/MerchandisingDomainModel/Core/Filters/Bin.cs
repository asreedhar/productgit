﻿using System;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    public class Bin
    {
        public Predicate<IInventoryData> IsMatch { get; private set; }

        public Bin(Predicate<IInventoryData> isMatch)
        {
            IsMatch = isMatch;
        }

        public bool Filter(IInventoryData inventoryData)
        {
            return IsMatch(inventoryData);
        }

        public static Bin Create(string searchText)
        {
            var filter = SearchFilter.GetKeywordFilter(searchText);
            return new Bin(inv => filter(inv));
        }

        public static Bin Create(WorkflowType workflowType, 
            int usedOrNewFilter = 0, 
            int? ageBucketId = null, 
            string searchText = "")
        {
            return
                new Bin(CreatePredicate(workflowType, ConditionFilters.CreateFromInt(usedOrNewFilter),
                                        AgeBucketFilters.CreateFromBucketFilterMode(ageBucketId), searchText));
        }

        public static Predicate<IInventoryData> CreatePredicate(WorkflowType workflowType, Predicate<IInventoryData> conditionFilter, Predicate<IInventoryData> ageBucketFilter, string searchText)
        {
            return SearchFilter.HasSearchText(searchText) 
                ? BuildSearchTextFilter(searchText) 
                : BuildWorkflowFilter(workflowType, conditionFilter, ageBucketFilter);
        }

        private static Predicate<IInventoryData> BuildWorkflowFilter(WorkflowType workflowType, Predicate<IInventoryData> conditionFilter, Predicate<IInventoryData> ageBucketFilter)
        {
            var workflows = Registry.Resolve<IWorkflowRepository>();
            var workflow = workflows.GetWorkflow(workflowType);
            var filter = workflow.Filter;

            var filter1 = conditionFilter != null ? inv => conditionFilter(inv) && filter(inv) : filter;
            var filter2 = ageBucketFilter != null ? inv => ageBucketFilter(inv) && filter1(inv) : filter1;

            return filter2;
        }

        private static Predicate<IInventoryData> BuildSearchTextFilter(string searchText)
        {
            var filter = SearchFilter.GetKeywordFilter(searchText);
            return d => filter(d);
        }
    }
}
