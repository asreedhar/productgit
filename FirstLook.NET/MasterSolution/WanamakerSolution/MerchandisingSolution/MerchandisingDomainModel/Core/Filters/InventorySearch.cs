﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Google.Apis.Util;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    public class InventorySearch : IInventorySearch
    {
        private readonly INewCarPricingRepository _repository;
        public List<IInventoryData> InventoryList { get; private set; }
        public VehicleType VehicleType { get; private set; }

        public InventorySearch(INewCarPricingRepository repository)
        {
            _repository = repository;
        }

        // Constructor takes an inventory list, and a vehicle type, then filters by that vehicle type.
        public void Initialize(int businessUnitId, VehicleType vehicleType)
        {
            VehicleType = vehicleType;
            
            InventoryList = GetInventoryData(businessUnitId).Where(x => vehicleType.Equals(VehicleType.Both) || (x.InventoryType == (int) vehicleType) ).ToList();
        }

        public virtual IEnumerable<IInventoryData> GetInventoryData(int businessUnitId)
        {
            return _repository.GetNewCarPricingInventory(businessUnitId);
        }

        public IEnumerable<IInventoryData> ForInventoryIds(List<int> inventoryIds)
        {
            return InventoryList.FindAll(x => inventoryIds.Contains(x.InventoryID));
        }

        public IEnumerable<IInventoryData> ByCampaignFilter(CampaignFilter filter)
        {
            return InventoryList.FindAll(CreateVehicleTypePredicate(filter.VehicleType))
                .FindAll(CreateYearsPredicate(filter.Year))
                .FindAll(CreateMakesPredicate(filter.Make))
                .FindAll(CreateModelsPredicate(filter.Model))
                .FindAll(CreateTrimPredicate(filter.Trim))
                .FindAll(CreateBodyStylePredicate(filter.BodyStyle))
                .FindAll(CreateEngineDescriptionPredicate(filter));
        }

        #region Predicates
        private static Predicate<IInventoryData> CreateYearsPredicate(IEnumerable<string> yearsEnumerable)
        {
            var yl = yearsEnumerable.Where(x => x != null).ToList().ConvertAll(x => x.ToUpperInvariant().Trim());
            if (yl.Contains(CampaignFilter.Allyears.ToUpperInvariant().Trim()))
                return data => true;

            return data => yl.Contains(data.Year);
        }

        private static Predicate<IInventoryData> CreateMakesPredicate(IEnumerable<string> makesEnumerable)
        {
            // Make sure we can do a comparison
            var makesList = (makesEnumerable != null) ? makesEnumerable.Where(x => x != null).ToList().ConvertAll(x => x.ToUpperInvariant().Trim()) : new List<string>();

            return makesList.Any()
                ? (Predicate<IInventoryData>) (data => makesList.Contains(data.Make.ToUpperInvariant().Trim()))
                : (data => true);

        }

        private static Predicate<IInventoryData> CreateModelsPredicate(IEnumerable<string> modelsEnumerable)
        {
            var modelsList = (modelsEnumerable != null) ? modelsEnumerable.Where(x => x != null).ToList().ConvertAll(x => x.ToUpperInvariant().Trim()) : new List<string>();
            
            return modelsList.Any()
                ? (Predicate<IInventoryData>) (data => modelsList.Contains(data.Model.ToUpperInvariant().Trim()))
                : (data => true);
        }

        private static Predicate<IInventoryData> CreateTrimPredicate(IEnumerable<string> trimEnumerable)
        {
            var trimsList = (trimEnumerable != null) ? trimEnumerable.Where(x => x != null).ToList().ConvertAll(x => x.ToUpperInvariant().Trim()) : new List<string>();

            // First we check to see if there are any provided trims,
            // then we see if this data has an aftermarkettrim, if so, does it match
            // if there is no custom trim, does the regular trim match?
            // if there is no regular trim, does the trimsList contain "BASE"
            return trimsList.Any()
                ? (Predicate<IInventoryData>)(data =>
                {
                    if (!data.AfterMarketTrim.IsNullOrEmpty() || !data.Trim.IsNullOrEmpty())
                    {
                        return trimsList.Contains(data.AfterMarketTrim.IsNullOrEmpty() ? data.Trim.ToUpperInvariant().Trim() : data.AfterMarketTrim.ToUpperInvariant().Trim());
                    }
                    return String.IsNullOrWhiteSpace(data.Trim) && trimsList.Contains(@"BASE");
                }) : (data => true);
        }

        private static Predicate<IInventoryData> CreateBodyStylePredicate(IEnumerable<string> bodyStylEnumerable)
        {
            var bodyStyleList = (bodyStylEnumerable != null) ? bodyStylEnumerable.Where(x => x != null).ToList().ConvertAll(x => x.ToUpperInvariant().Trim()) : new List<string>();
            return bodyStyleList.Any()
                ? (Predicate<IInventoryData>) (data => bodyStyleList.Contains(data.BodyStyleName.ToUpperInvariant().Trim()))
                : (data => true);
        }

        private static Predicate<IInventoryData> CreateEngineDescriptionPredicate(CampaignFilter filter)
        {
            return (filter.CampaignEngineDescription.IsNullOrEmpty())
                ? CreateLegacyEngineDescriptionPredicate(filter.EngineDescription)
                : CreateNewEngineDescriptionPredicate(filter.CampaignEngineDescription);
        }

        private static Predicate<IInventoryData> CreateLegacyEngineDescriptionPredicate(IEnumerable<string> engineDescriptionEnumerable)
        {
            var engineDescriptionList = (engineDescriptionEnumerable != null) ? engineDescriptionEnumerable.Where(x => x != null).ToList().ConvertAll(x => x.ToUpperInvariant().Trim()) : new List<string>();
            return engineDescriptionList.Any()
                ? (Predicate<IInventoryData>)(data => engineDescriptionList.Contains(data.EngineDescription.ToUpperInvariant().Trim()))
                : (data => true);
        }

        private static Predicate<IInventoryData> CreateNewEngineDescriptionPredicate(IEnumerable<CampaignEngineDescriptionInfo> engineDescriptionInfoEnumerable)
        {
            var engineDescriptionInfo = (engineDescriptionInfoEnumerable != null) ? engineDescriptionInfoEnumerable.Where(x => x != null).ToList() : new List<CampaignEngineDescriptionInfo>();
            return engineDescriptionInfo.Any(x => x != null)
                ? (Predicate<IInventoryData>)(data => (data.IsStandardEngine)
                    ? engineDescriptionInfo.Any(x => x != null &&
                        x.IsStandard &&
                        x.Description.Trim().ToUpperInvariant().Trim().Equals(data.EngineDescription.Trim().ToUpperInvariant().Trim()))
                    : engineDescriptionInfo.Any(
                        x =>
                            x != null &&
                            x.IsStandard.Equals(false) &&
                            x.OptionCode.IsNotNullOrEmpty() &&
                            x.Description.Trim().ToUpperInvariant().Trim().Equals(data.EngineDescription.Trim().ToUpperInvariant().Trim())))
                : (data => true);
        }

        private static Predicate<IInventoryData> CreateVehicleTypePredicate(VehicleType vehicleType)
        {
            return data => (vehicleType == VehicleType.Both) || data.InventoryType == vehicleType;
        }

        #endregion Predicates
    }
}