﻿using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    internal static class SearchFilter
    {
        internal static readonly string NoTextSearch = "";

        private static readonly IKeywordFilterFactory<IInventoryData> KeywordFilterFactory = new KeywordFilterFactory<IInventoryData>();

        private static IEnumerable<string> GetPhrases(IInventoryData data)
        {
            yield return data.Make;
            yield return data.Model;
            yield return data.Year;
            yield return data.Trim;
            yield return data.BaseColor;
            yield return data.StockNumber;
            yield return data.VIN;
        }

        internal static bool HasSearchText(string textSearch)
        {
            return !string.IsNullOrEmpty(textSearch);
        }

        internal static Func<IInventoryData, bool> GetKeywordFilter(string searchExpression)
        {
            return KeywordFilterFactory.GetFilter(searchExpression, GetPhrases);
        }
    }
}
