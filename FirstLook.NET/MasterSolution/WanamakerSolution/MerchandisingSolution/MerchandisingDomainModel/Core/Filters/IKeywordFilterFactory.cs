using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    internal interface IKeywordFilterFactory<T>
    {
        Func<T, bool> GetFilter(string searchText, Func<T, IEnumerable<string>> getPhrases);
    }
}