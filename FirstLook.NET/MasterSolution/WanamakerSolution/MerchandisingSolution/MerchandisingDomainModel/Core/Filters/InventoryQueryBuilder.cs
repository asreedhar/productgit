using System;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using MAX.Entities.Filters;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    internal class InventoryQueryBuilder : IInventoryQueryBuilder
    {
        private readonly IWorkflowRepository _workflows;
        private UsedOrNewFilter? _usedOrNewFilter;
        private Predicate<IInventoryData> _workflowPredicate;
        private int? _ageBucketFilter;
        private Func<IInventoryData, bool> _keywordPredicate;

        public InventoryQueryBuilder(IWorkflowRepository workflows)
        {
            _workflows = workflows;
        }

        public void FilterByWorkflow(WorkflowType type)
        {
            var workflow = _workflows.GetWorkflow(type);
            _workflowPredicate = workflow.Filter;
        }

        public void FilterByWorkflow(IWorkflow type)
        {
            _workflowPredicate = type.Filter;
        }

        public void FilterByNewOrUsed(UsedOrNewFilter newOrUsed)
        {
            _usedOrNewFilter = newOrUsed == UsedOrNewFilter.Used_and_New
                                   ? (UsedOrNewFilter?) null
                                   : newOrUsed;
        }

        public void FilterByAgeBucket(int? ageBucketId)
        {
            _ageBucketFilter = ageBucketId;
        }

        public void FilterByKeywords(string text)
        {
            _keywordPredicate = SearchFilter.HasSearchText(text)
                                    ? SearchFilter.GetKeywordFilter(text)
                                    : null;
        }

        public Func<IInventoryData, bool> ToPredicate()
        {
            return _keywordPredicate
                   ?? (d => (_workflowPredicate == null || _workflowPredicate(d))
                            && (!_usedOrNewFilter.HasValue || d.InventoryType == (int) _usedOrNewFilter.Value)
                            && (!_ageBucketFilter.HasValue || d.AgeBucketId == _ageBucketFilter));
        }
    }
}