﻿using System;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    public static class ConditionFilters
    {
        public static readonly Predicate<IInventoryData> New = inv => inv.IsNew();
        public static readonly Predicate<IInventoryData> Used = inv => !inv.IsNew();

        public static Predicate<IInventoryData> CreateFromInt(int usedOrNewFilter)
        {
            switch(usedOrNewFilter)
            {
                case 0:
                    return null;
                case 1:
                    return New;
                case 2:
                    return Used;
                default:
                    throw new InvalidOperationException("Invalid usedOrNewFilter value.");
            }
        }
    }
}
