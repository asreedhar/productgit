﻿using System;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using MAX.Entities.Filters;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    public interface IInventoryQueryBuilder
    {
        void FilterByWorkflow(WorkflowType type);
        void FilterByWorkflow(IWorkflow type);

        void FilterByNewOrUsed(UsedOrNewFilter newOrUsed);

        void FilterByAgeBucket(int? ageBucketId);

        void FilterByKeywords(string text);

        Func<IInventoryData, bool> ToPredicate();
    }
}