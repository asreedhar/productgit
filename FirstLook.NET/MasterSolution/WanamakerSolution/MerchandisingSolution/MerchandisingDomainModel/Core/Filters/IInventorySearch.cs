﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using MAX.Entities;


namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    public interface IInventorySearch
    {
        void Initialize(int businessUnitId, VehicleType vehicleType);
        
        List<IInventoryData> InventoryList { get; }
        
        IEnumerable<IInventoryData> ForInventoryIds(List<int> inventoryIds);
        IEnumerable<IInventoryData> ByCampaignFilter(CampaignFilter filter);

        IEnumerable<IInventoryData> GetInventoryData(int businessUnitId);
    }
}