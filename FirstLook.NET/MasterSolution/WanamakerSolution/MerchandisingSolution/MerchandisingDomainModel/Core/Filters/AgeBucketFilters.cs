﻿using System;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    public abstract class AgeBucketFilters
    {
        public static Predicate<IInventoryData> CreateFromBucketFilterMode(int? ageBucketId)
        {
            return ageBucketId == null
                       ? (Predicate<IInventoryData>) null
                       : (inv => inv.AgeBucketId == ageBucketId);
        }
    }
}