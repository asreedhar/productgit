﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Core.Filters
{
    internal class KeywordFilterFactory<T> : IKeywordFilterFactory<T>
    {
        public Func<T, bool> GetFilter(string searchText, Func<T, IEnumerable<string>> getPhrases)
        {
            var keywords = GetKeywords(searchText);
            if (keywords == null || keywords.Count <= 0)
                return null;

            return data => IsTextMatch(data, getPhrases, keywords);
        }

        private static bool IsTextMatch(T data, Func<T, IEnumerable<string>> getPhrases, IEnumerable<string> keywords)
        {
            var phrases = getPhrases(data).Where(phrase => !string.IsNullOrWhiteSpace(phrase)).ToArray();
            return keywords.All(
                keyword => phrases.Any(
                    phrase => phrase.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0));
        }

        private static HashSet<string> GetKeywords(string textFilter)
        {
            if (string.IsNullOrWhiteSpace(textFilter))
                return null;

            return new HashSet<string>(
                textFilter.Split(new[] {' ', '\t', '\r', '\n', '.'}, StringSplitOptions.RemoveEmptyEntries),
                StringComparer.CurrentCultureIgnoreCase);
        }
    }
}