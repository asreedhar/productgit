﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Antlr.StringTemplate;
using log4net;
using FirstLook.Common.Core.Extensions;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public class Templater
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(Templater).FullName);
        #endregion

        public const string LIST_NAME = "separator=\", \"";
       
        private static readonly Regex VARIABLE_REGEX = new Regex(@"\$[\d\w\.]*[\d\w]+[\(\)]*\$");           // $foo$, $foo()$, $foo.bar$, $foo.bar()$ 
        private static readonly Regex VAR_FUNC_REGEX = new Regex(@"\$[\d\w\.]*[\d\w]+;[\w\s\d]+=[,""\s]*\$");   // $foo; seperator=","$, $foo.bar; seperator=","$

        public static Regex VariableRegex()
        {
            return VARIABLE_REGEX;
        }

        public static Regex VariableFuncRegex()
        {
            return VAR_FUNC_REGEX;
        }

        private static string StripBase(string input)
        {
            return input.Replace("base.", "");
        }

        public static string DoTemplating(string toTemplate, object data)
        {
            Log.DebugFormat("DoTemplating() called with template '{0}'", toTemplate);

            try
            {
                // Strip out "base."
                toTemplate = StripBase(toTemplate);

                // Apply seperator="," function to all template attributes: $foo$ -> $foo; separator=","$
                toTemplate = ApplyTemplateToVariables(toTemplate, LIST_NAME);

                // Extract attributes without function names - e.g. $foo$
                var attributes = GetTemplateAttributes(toTemplate, true);

                var t = new StringTemplate(toTemplate);

                // Set values in string template.
                foreach (var attr in attributes)
                {
                    Log.DebugFormat("Extracting property value for {0}", attr );
                    var value = ExtractPropertyValue(data, attr);

                    Log.DebugFormat( "Setting attribute {0} with value {1}", attr, value );
                    t.SetAttribute(StripDollarDelimiters(attr), value);
                }

                var result = t.ToString();

                Log.DebugFormat("Result is '{0}'", result);

                return result;
            }
            catch (Exception ex)
            {
                Log.Error("An error occurred while templating.", ex);
            }
            return string.Empty;
        }

        /// <summary>
        /// Apply the template to all variables detected in the input.  Variables are delimited with $, e.g. $var$
        /// </summary>
        /// <param name="input"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public static string ApplyTemplateToVariables(string input, string template)
        {
            // Rewrite dollars regex matches using specified temlate.  
            // E.g. if template == "list()", and input is "$base.Foo$", return "$base.Foo:list()$"

            // Find all matches.
            return VARIABLE_REGEX.Replace(input, m => ApplyTemplate(m.ToString(), template));
        }

        /// <summary>
        /// variable: $a$
        /// template: list()
        /// returns: $a:list()$
        /// </summary>
        /// <param name="variable">variable must start and end with $, e.g. $abc$</param>
        /// <param name="template">will be appended to end of variable.</param>
        /// <returns></returns>
        public static string ApplyTemplate(string variable, string template)
        {
            // various edge cases
            if (string.IsNullOrEmpty(variable)) return string.Empty;
            if (!string.IsNullOrEmpty(variable) && string.IsNullOrEmpty(template)) return variable;
            if (string.IsNullOrEmpty(variable) && string.IsNullOrEmpty(template)) return string.Empty;

            // drop last char
            var first = variable.Substring(0, variable.Length - 1);

            StringBuilder sb = new StringBuilder();
            return sb.Append(first)
                .Append("; ")
                .Append(template)
                .Append("$")
                .ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="templatedPropertyName"></param>
        /// <returns></returns>
        public static object ExtractPropertyValue(object obj, string templatedPropertyName)
        {
            // templatedPropertyName: $Foo$ 
            // obj: an object that has property Foo
            // returns: value of obj.Foo

            string propName;

            // strip out formatting around property name, $Foo:list()$ -> Foo
            if( VAR_FUNC_REGEX.IsMatch(templatedPropertyName) )
            {
                propName = VAR_FUNC_REGEX.Match(templatedPropertyName).ToString();

                // strip the leading $ and everything after the :                
                propName = StripFunctionFromAttribute(propName);
                propName = StripDollarDelimiters(propName);
            }
            else if( VARIABLE_REGEX.IsMatch(templatedPropertyName))
            {
                propName = VARIABLE_REGEX.Match(templatedPropertyName).ToString();

                // strip the dollar signs.
                propName = propName.Substring(1, propName.Length - 2);
            }
            else
            {
                Log.WarnFormat("Object doesn't contain a property named '{0}'", templatedPropertyName);
                return null;
            }

            // Get the property value from the object.
            try
            {
                object value = obj.GetType().GetProperty(propName).GetValue(obj, null);
                return value;
            }
            catch (Exception ex)
            {
                string error = String.Format("Error getting property '{0}' value from object.", propName);
                Log.Error(error, ex);
                return null;
            }
        }

        private static string StripFunctionFromAttribute(string attrWithFunction)
        {
            // Does it start and stop with $ ?
            var delimited = attrWithFunction.StartsWith("$") && attrWithFunction.EndsWith("$");

            // get rid of everything after the first function :                
            var splitByColon = attrWithFunction.Split(';');
            var attr = splitByColon[0];

            if( delimited )
            {
                attr += "$";
            }

            return attr;
        }

        public static string StripDollarDelimiters(string input)
        {
            return input.Replace("$", "");
        }


        /// <summary>
        /// Get template attributes exactly as they appear, with function names.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<string> GetTemplateAttributes(string input)
        {
            return GetTemplateAttributes(input, false);
        }

        /// <summary>
        /// Get template attributes with or without function names.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<string> GetTemplateAttributes(string input, bool stripFunctions)
        {
            Log.DebugFormat("GetTemplateAttributes() called with input '{0}'", input);

            //input: "hello $world$, how $are:list()$ you?
            // output when stripFunctions == false : $world", $are:list()$
            // output when stripFunctions == true :  $world", $are$

            List<string> output = new List<string>();

            // $a.b:c()$
            foreach(Match match in VAR_FUNC_REGEX.Matches(input))
            {
                if( stripFunctions )
                {
                    // $a.b$
                    output.Add(StripFunctionFromAttribute(match.ToString()));
                }
                else
                {
                    // $a.b:c()$
                    output.Add(match.ToString());                    
                }
            }

            // $a$
            foreach (Match match in VARIABLE_REGEX.Matches(input))
            {
                output.Add(match.ToString());
            }

            var distinct = output.Distinct().ToList();

            Log.DebugFormat("{0} attributes found", distinct.Count );
            Log.DebugFormat("Returning these attributes: '{0}'", distinct.ToDelimitedString(","));

            return distinct;
        }

    }
}
