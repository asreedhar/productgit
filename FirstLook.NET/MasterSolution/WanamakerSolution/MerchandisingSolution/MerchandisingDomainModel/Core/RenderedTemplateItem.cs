﻿using FirstLook.Merchandising.DomainModel.Templating;

namespace FirstLook.Merchandising.DomainModel.Core
{
    internal class RenderedTemplateItem
    {
        public TemplateItem Item { get; set; }
        public string FormattedText { get; set; }
        public string RawText { get; set; }

        public RenderedTemplateItem(TemplateItem item, string formattedText, string rawText)
        {
            Item = item;
            FormattedText = formattedText;
            RawText = rawText;
        }
    }
}
