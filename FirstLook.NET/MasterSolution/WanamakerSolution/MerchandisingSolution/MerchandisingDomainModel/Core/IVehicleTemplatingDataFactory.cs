﻿using FirstLook.Merchandising.DomainModel.Templating;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface IVehicleTemplatingFactory
    {
        IVehicleTemplatingData GetVehicleTemplatingData(int businessUnitId, int inventoryId, string userName, ITemplateMediator mediator = null);
    }
}