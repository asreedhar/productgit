﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class PreviewParagraph : AdvertisementElement
    {
        public PreviewItem[] Items
        {
            get
            {
                return Children.Where(x => typeof(PreviewItem)
                       .IsInstanceOfType(x))
                       .Cast<PreviewItem>().ToArray();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
