﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public abstract class AdvertisementElement
    {
        public IList<AdvertisementElement> Children { get; private set; }

        protected AdvertisementElement()
        {
            Children = new List<AdvertisementElement>();
        }

        public abstract void Accept(IAdvertismentModelVisitor visitor);
    }
}
