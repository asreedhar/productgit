﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class Paragraph : AdvertisementElement
    {
        public Item[] Items
        {
            get
            {
                return Children.Where(x => typeof(Item)
                       .IsInstanceOfType(x))
                       .Cast<Item>().ToArray();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
