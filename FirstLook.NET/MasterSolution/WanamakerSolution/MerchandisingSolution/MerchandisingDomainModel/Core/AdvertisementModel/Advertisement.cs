﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class Advertisement : AdvertisementElement
    {
        public const string SecondVersion = "2";

        public string Version { get; set; }
        public int AdTemplateID { get; set; }
        public string Name { get; set; }

        public Advertisement()
        {
            Version = SecondVersion;
            AdTemplateID = -1;
            Name = String.Empty;
        }

        public Preview Preview
        {
            get
            {
                return Children.Where(x => typeof(Preview)
                    .IsInstanceOfType(x))
                    .Cast<Preview>().ToList().FirstOrDefault();
            }
        }

        public Module[] Modules
        {
            get
            {
                return Children.Where(x => typeof(Module)
                    .IsInstanceOfType(x))
                    .Cast<Module>().ToArray();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
