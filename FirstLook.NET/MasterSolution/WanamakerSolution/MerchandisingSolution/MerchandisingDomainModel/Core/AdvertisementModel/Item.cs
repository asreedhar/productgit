﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class Item : AdvertisementElement
    {
        public string Text { get; set; }
        public bool Edited { get; set; }

        public Item()
        {
            Text = string.Empty;
            Edited = false;
        }

        public int BlurbTemplateID
        {
            get
            {
                if (BlurbTemplate == null)
                    return -1;
                
                return BlurbTemplate.ID;
            }
        }

        public PreBlurb PreBlurb
        {
            get
            {
                return Children.Where(x => typeof(PreBlurb)
                    .IsInstanceOfType(x))
                    .Cast<PreBlurb>().ToList().FirstOrDefault();
            }
        }

        public PostBlurb PostBlurb
        {
            get
            {
                return Children.Where(x => typeof(PostBlurb)
                    .IsInstanceOfType(x))
                    .Cast<PostBlurb>().ToList().FirstOrDefault();
            }
        }

        public BlurbTemplate BlurbTemplate
        {
            get
            {
                return Children.Where(x => typeof(BlurbTemplate)
                    .IsInstanceOfType(x))
                    .Cast<BlurbTemplate>().ToList().FirstOrDefault();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
