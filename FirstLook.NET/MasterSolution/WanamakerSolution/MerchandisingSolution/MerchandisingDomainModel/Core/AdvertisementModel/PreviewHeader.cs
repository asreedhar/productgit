﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class PreviewHeader : AdvertisementElement
    {
        public PreviewItem Item
        {
            get
            {
                return Children.Where(x => typeof(PreviewItem)
                       .IsInstanceOfType(x))
                       .Cast<PreviewItem>().ToList().FirstOrDefault();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
