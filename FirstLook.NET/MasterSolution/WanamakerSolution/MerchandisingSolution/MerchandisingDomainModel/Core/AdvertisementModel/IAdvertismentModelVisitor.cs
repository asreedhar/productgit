﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public interface IAdvertismentModelVisitor
    {
        void Visit(Advertisement advertisement);
        
        void Visit(Preview preview);
        void Visit(PreviewHeader previewHeader);
        void Visit(PreviewParagraph previewParagraph);
        void Visit(PreviewItem previewItem);
        void Visit(PricePreviewItem previewItem);
        void Visit(Rule rule);

        void Visit(Module module);
        void Visit(Header header);
        void Visit(Paragraph paragraph);
        void Visit(Item item);
        void Visit(PriceItem item);
        void Visit(PreBlurb preBlurb);
        void Visit(BlurbTemplate blurbTemplate);
        void Visit(PostBlurb postBlurb);
    }
}
