﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class Preview : AdvertisementElement
    {
        public PreviewHeader Header
        {
            get
            {
                return Children.Where(x => typeof(PreviewHeader)
                       .IsInstanceOfType(x))
                       .Cast<PreviewHeader>().ToList().FirstOrDefault();   
            }
        }

        public PreviewParagraph Paragraph
        {
            get
            {
                return Children.Where(x => typeof (PreviewParagraph)
                       .IsInstanceOfType(x))
                       .Cast<PreviewParagraph>().ToList().FirstOrDefault();   
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
