﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class Module : AdvertisementElement
    {
        public Header Header
        {
            get
            {
                return Children.Where(x => typeof(Header)
                       .IsInstanceOfType(x))
                       .Cast<Header>().ToList().FirstOrDefault();
            }
        }

        public Paragraph Paragraph
        {
            get
            {
                return Children.Where(x => typeof(Paragraph)
                       .IsInstanceOfType(x))
                       .Cast<Paragraph>().ToList().FirstOrDefault();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
