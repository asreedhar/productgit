﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    public static class ModelExtensions
    {
        public static string GetCombinedText(this IEnumerable<PreviewItem> items)
        {
            return String.Join("", items.Select(x => x.Text).ToArray());
        }

        public static string GetCombinedText(this IEnumerable<Item> items)
        {
            return String.Join("", items.Select(x => x.Text).ToArray());
        }
    }
}
