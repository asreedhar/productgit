﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    public class AdvertisementConverter
    {
        public Advertisement EmptyModel
        {
            get
            {
                return FromText(string.Empty);
            }
        }

        public Advertisement FromText(string text)
        {
            var advertisement = new Advertisement();
            
            var preview = new Preview();
            advertisement.Children.Add(preview);

            var previewHeader = new PreviewHeader() { };
            preview.Children.Add(previewHeader);
            previewHeader.Children.Add(new PreviewItem() { Text = "PREVIEW" });

            var previewParagraph = new PreviewParagraph();
            preview.Children.Add(previewParagraph);
            previewParagraph.Children.Add(new PreviewItem() { Text = "" });

            var module = new Module();
            advertisement.Children.Add(module);

            var header = new Header();
            module.Children.Add(header);
            header.Children.Add(new Item() {Text = ""});

            var paragraph = new Paragraph();
            module.Children.Add(paragraph);
            paragraph.Children.Add(new Item(){Text = text});

            return advertisement;
        }

        public Advertisement FromJson(string json)
        {
            JsonModelParser parser = new JsonModelParser();
            return parser.Parse(json);
        }

        public string ToJson(Advertisement advertisement)
        {
            JsonModelBuilder builder = new JsonModelBuilder(advertisement);
            return builder.Build();
        }

        public string ToText(Advertisement advertisement)
        {
            TextBuilder builder = new TextBuilder(advertisement);
            return builder.Build();
        }

        public string ToHtml(Advertisement advertisement)
        {
            var htmlBuilder = new HtmlBuilder(advertisement);
            return htmlBuilder.Build();
        }

        public Advertisement FromXml(string xml) 
        {
            XmlModelParser parser = new XmlModelParser();
            return parser.Parse(xml);
        }

        public string ToXml(Advertisement advertisement)
        {
            XmlModelBuilder builder = new XmlModelBuilder();
            builder.Build(advertisement);
            return builder.Xml;
        }
    }
}
