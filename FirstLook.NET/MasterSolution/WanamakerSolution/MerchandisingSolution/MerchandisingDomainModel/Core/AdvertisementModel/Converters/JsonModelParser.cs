﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    internal class JsonModelParser
    {
        private JsonReader _reader;
        private Advertisement _advertisement;

        private Rule CreateRule()
        {
            Rule rule = new Rule();
            
            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == "ID")
                    {
                        _reader.Read();
                        rule.ID = new Guid((string)_reader.Value);
                    }
                }
            }

            return rule;
        }

        private PreviewItem CreatePreviewItem()
        {
            PreviewItem item = new PreviewItem();

            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == Variables.TypeProperty)
                    {
                        _reader.Read();
                        if ((string)_reader.Value == Variables.PriceItemProperty)
                            item = new PricePreviewItem();
                    }
                    if (propertyName == "Edited")
                    {
                        _reader.Read();
                        item.Edited = bool.Parse((string) _reader.Value);
                    }

                    if (propertyName == Variables.TEXT)
                    {
                        _reader.Read();
                        item.Text = (string) _reader.Value;
                    }

                    if(propertyName == "Rule")
                    {
                        item.Children.Add(CreateRule());
                    }
                }
            }

            return item;
        }

        private PreviewParagraph CreatePreviewParagraph()
        {
            PreviewParagraph paragraph = new PreviewParagraph();
            while (_reader.Read() && _reader.TokenType != JsonToken.EndArray)
                paragraph.Children.Add(CreatePreviewItem());

            return paragraph;
        }

        private PreviewHeader CreatePreviewHeader()
        {
            PreviewHeader header = new PreviewHeader();
            header.Children.Add(CreatePreviewItem());
            return header;
        }

        private Preview CreatePreview()
        {
            Preview preview = new Preview();
            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == Variables.HEADER)
                        preview.Children.Add(CreatePreviewHeader());
                    if (propertyName == Variables.PARAGRAPH)
                        preview.Children.Add(CreatePreviewParagraph());
                }
            }

            return preview;
        }

        private PostBlurb CreatePostBlurb()
        {
            PostBlurb postBlurb = new PostBlurb();

            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;

                    if (propertyName == "ID")
                    {
                        _reader.Read();
                        postBlurb.ID = (int)((long)_reader.Value);
                    }

                    if (propertyName == Variables.TEXT)
                    {
                        _reader.Read();
                        postBlurb.Text = (string)_reader.Value;
                    }
                }

            }

            return postBlurb;
        }

        private PreBlurb CreatePreBlurb()
        {
            PreBlurb preblurb = new PreBlurb();

            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;

                    if (propertyName == "ID")
                    {
                        _reader.Read();
                        preblurb.ID = (int)((long)_reader.Value);
                    }

                    if (propertyName == Variables.TEXT)
                    {
                        _reader.Read();
                        preblurb.Text = (string)_reader.Value;
                    }
                }

            }

            return preblurb;
        }

        private BlurbTemplate CreateBlurbTemplate()
        {
            BlurbTemplate blurbTemplate = new BlurbTemplate();

            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;

                    if (propertyName == "ID")
                    {
                        _reader.Read();
                        blurbTemplate.ID = (int)((long)_reader.Value);
                    }

                    if (propertyName == Variables.TEXT)
                    {
                        _reader.Read();
                        blurbTemplate.Text = (string)_reader.Value;
                    }

                    if (propertyName == Variables.NAME)
                    {
                        _reader.Read();
                        blurbTemplate.Name = (string)_reader.Value;
                    }
                }

            }

            return blurbTemplate;
            
        }

        private AdvertisementElement CreateBlurb()
        {
            AdvertisementElement blurb = new PreBlurb();

            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == Variables.TypeProperty)
                    {
                        _reader.Read();

                        if ((string)_reader.Value == Variables.BlurbTemplateProperty)
                        {
                            blurb = CreateBlurbTemplate();
                            break;
                        }

                        if ((string)_reader.Value == Variables.PreBlurbProperty)
                        {
                            blurb = CreatePreBlurb();
                            break;
                        }

                        if ((string)_reader.Value == Variables.PostBlurbProperty)
                        {
                            blurb = CreatePostBlurb();
                            break;
                        }
                    }
                }
            }

            return blurb;
        }


        private Item CreateItem()
        {
            Item item = new Item();

            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == Variables.TypeProperty)
                    {
                        _reader.Read();
                        if ((string)_reader.Value == Variables.PriceItemProperty)
                            item = new PriceItem();
                    }
                    if (propertyName == "Edited")
                    {
                        _reader.Read();
                        item.Edited = bool.Parse((string)_reader.Value);
                    }

                    if (propertyName == Variables.TEXT)
                    {
                        _reader.Read();
                        item.Text = (string)_reader.Value;
                    }

                    if (propertyName == Variables.BlurbsProperty)
                    {
                        while (_reader.Read() && _reader.TokenType != JsonToken.EndArray)
                            item.Children.Add(CreateBlurb());
                    }
                }
            }

            return item;
        }

        private Paragraph CreateParagraph()
        {
            Paragraph paragraph = new Paragraph();
            while (_reader.Read() && _reader.TokenType != JsonToken.EndArray)
                paragraph.Children.Add(CreateItem());
            return paragraph;
        }

        private Header CreateHeader()
        {
            Header header = new Header();
            header.Children.Add(CreateItem());
            return header;
        }

        private Module CreateModule()
        {
            Module module = new Module();

            while (_reader.Read() && _reader.TokenType != JsonToken.EndObject)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == Variables.HEADER)
                        module.Children.Add(CreateHeader());
                    if (propertyName == Variables.PARAGRAPH)
                        module.Children.Add(CreateParagraph());
                }
            }

            return module;
        }

        private void ParseModules()
        {
            while (_reader.Read() && _reader.TokenType != JsonToken.EndArray)
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == Variables.TypeProperty)
                    {
                        _reader.Read();

                        if ((string)_reader.Value == Variables.PreviewType)
                            _advertisement.Children.Add(CreatePreview());
                        if ((string)_reader.Value == Variables.ModuleType)
                            _advertisement.Children.Add(CreateModule());
                    }
                }
            }
        }

        public Advertisement Parse(string json)
        {
            _advertisement = new Advertisement();
            StringReader reader = new StringReader(json);
            _reader = new JsonTextReader(reader);

            while (_reader.Read())
            {
                if (_reader.TokenType == JsonToken.PropertyName)
                {
                    string propertyName = _reader.Value as string;
                    if (propertyName == Variables.ModulesProperty)
                        ParseModules();
                    
                    if (propertyName == Variables.VER)
                    {
                        _reader.Read();
                        _advertisement.Version = (string)_reader.Value;
                    }

                    if(propertyName == Variables.ADTEMPLATEID)
                    {
                        _reader.Read();
                        _advertisement.AdTemplateID = (int)((long)_reader.Value);
                    }

                    if(propertyName == Variables.NAME)
                    {
                        _reader.Read();
                        _advertisement.Name = (string) _reader.Value;
                    }
                }
            }

            return _advertisement;
        }
    }
}
