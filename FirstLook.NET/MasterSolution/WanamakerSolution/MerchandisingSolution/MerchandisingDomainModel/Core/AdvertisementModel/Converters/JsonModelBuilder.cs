﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    internal class JsonModelBuilder : IAdvertismentModelVisitor
    {
        private readonly Advertisement _advertisement;
        private JsonWriter _writer;

        public JsonModelBuilder(Advertisement advertisement)
        {
            _advertisement = advertisement;
        }

        public string Build()
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            _writer = new JsonTextWriter(sw);

            _advertisement.Accept(this);

            return sb.ToString();
        }

        private void VisitChildren(AdvertisementElement element)
        {
            foreach (var child in element.Children)
                child.Accept(this);
        }

        public void Visit(Advertisement advertisement)
        {
            _writer.WriteStartObject();
            _writer.WritePropertyName(Variables.ModulesProperty);
            
            _writer.WriteStartArray();
            VisitChildren(advertisement);
            _writer.WriteEndArray();

            _writer.WritePropertyName(Variables.VER);
            _writer.WriteValue(_advertisement.Version);

            _writer.WritePropertyName(Variables.ADTEMPLATEID);
            _writer.WriteValue(_advertisement.AdTemplateID);

            _writer.WritePropertyName(Variables.NAME);
            _writer.WriteValue(_advertisement.Name);
            
            _writer.WriteEndObject();

            _writer.Close();
        }

        public void Visit(Preview preview)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.PreviewType);
            
            VisitChildren(preview);

            _writer.WriteEndObject();
        }

        public void Visit(PreviewHeader previewHeader)
        {
            _writer.WritePropertyName(Variables.HEADER);
            
            previewHeader.Item.Accept(this);
        }

        public void Visit(PreviewParagraph previewParagraph)
        {
            _writer.WritePropertyName(Variables.PARAGRAPH);
            _writer.WriteStartArray();

            VisitChildren(previewParagraph);

            _writer.WriteEndArray();
        }

        private void WritePreviewItemProperties(PreviewItem previewItem)
        {
            _writer.WritePropertyName("Edited");
            _writer.WriteValue(previewItem.Edited.ToString().ToLower());

            _writer.WritePropertyName(Variables.TEXT);
            _writer.WriteValue(previewItem.Text);

            VisitChildren(previewItem);
        }

        public void Visit(PreviewItem previewItem)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.ItemProperty);
            
            WritePreviewItemProperties(previewItem);
            
            _writer.WriteEndObject();
        }

        public void Visit(PricePreviewItem previewItem)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.PriceItemProperty);

            WritePreviewItemProperties(previewItem);

            _writer.WriteEndObject();
        }

        public void Visit(Rule rule)
        {
            _writer.WritePropertyName("Rule");
            _writer.WriteStartObject();

            _writer.WritePropertyName("ID");
            _writer.WriteValue(rule.ID.ToString());

            _writer.WriteEndObject();
        }

        public void Visit(Module module)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.ModuleType);

            VisitChildren(module);

            _writer.WriteEndObject();
        }

        public void Visit(Header header)
        {
            _writer.WritePropertyName(Variables.HEADER);

            header.Item.Accept(this);
        }

        public void Visit(Paragraph paragraph)
        {
            _writer.WritePropertyName(Variables.PARAGRAPH);
            _writer.WriteStartArray();

            VisitChildren(paragraph);

            _writer.WriteEndArray();
        }

        private void WriteItemProperties(Item item)
        {
            _writer.WritePropertyName("Edited");
            _writer.WriteValue(item.Edited.ToString().ToLower());

            _writer.WritePropertyName(Variables.TEXT);
            _writer.WriteValue(item.Text);

            _writer.WritePropertyName("BlurbTemplateID");
            _writer.WriteValue(item.BlurbTemplateID);

            if (item.Children.Count > 0)
            {
                _writer.WritePropertyName(Variables.BlurbsProperty);

                _writer.WriteStartArray();
                VisitChildren(item);
                _writer.WriteEndArray();
            }
        }

        public void Visit(Item item)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.ItemProperty);

            WriteItemProperties(item);

            _writer.WriteEndObject();
        }

        public void Visit(PriceItem item)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.PriceItemProperty);

            WriteItemProperties(item);

            _writer.WriteEndObject();
        }

        public void Visit(PreBlurb preBlurb)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.PreBlurbProperty);

            _writer.WritePropertyName("ID");
            _writer.WriteValue(preBlurb.ID);

            _writer.WritePropertyName(Variables.TEXT);
            _writer.WriteValue(preBlurb.Text);

            VisitChildren(preBlurb);

            _writer.WriteEndObject();
        }

        public void Visit(BlurbTemplate blurbTemplate)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.BlurbTemplateProperty);

            _writer.WritePropertyName(Variables.NAME);
            _writer.WriteValue(blurbTemplate.Name);

            _writer.WritePropertyName(Variables.TEXT);
            _writer.WriteValue(blurbTemplate.Text);

            _writer.WritePropertyName("ID");
            _writer.WriteValue(blurbTemplate.ID);

            VisitChildren(blurbTemplate);

            _writer.WriteEndObject();
        }

        public void Visit(PostBlurb postBlurb)
        {
            _writer.WriteStartObject();

            _writer.WritePropertyName(Variables.TypeProperty);
            _writer.WriteValue(Variables.PostBlurbProperty);

            _writer.WritePropertyName("ID");
            _writer.WriteValue(postBlurb.ID);

            _writer.WritePropertyName(Variables.TEXT);
            _writer.WriteValue(postBlurb.Text);

            VisitChildren(postBlurb);

            _writer.WriteEndObject();
        }

        
    }
}
