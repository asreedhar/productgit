﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    internal static class Variables
    {
        public const string VER = "Version";
        public const string AD = "Advertisement";
        public const string ADTEMPLATEID = "AdTemplateID";
        public const string NAME = "Name";
        
        public const string PREVIEW = "Preview";

        public const string MODULE = "Module";
        public const string HEADER = "Header";
        public const string PARAGRAPH = "Paragraph";
        public const string ITEM = "Item";
        public const string TEXT = "Text";

        //JSON
        public const string ModulesProperty = "Modules";
        public const string TypeProperty = "Type";
        public const string ItemProperty = "Item";
        public const string PriceItemProperty = "PriceItem";
        public const string PreviewType = "Preview";
        public const string ModuleType = "Module";
        public const string BlurbsProperty = "Blurbs";

        public const string PreBlurbProperty = "PreBlurb";
        public const string BlurbTemplateProperty = "BlurbTemplate";
        public const string PostBlurbProperty = "PostBlurb";
        
    }
}
