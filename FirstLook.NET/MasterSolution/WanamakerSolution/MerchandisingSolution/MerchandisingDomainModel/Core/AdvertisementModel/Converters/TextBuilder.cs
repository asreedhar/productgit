﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    internal class TextBuilder : IAdvertismentModelVisitor
    {
        private const string PRE_HEADER = "======";

        private readonly Advertisement _advertisement;
        private readonly StringBuilder _advertisementText;

        public TextBuilder(Advertisement advertisement)
        {
            _advertisement = advertisement;
            _advertisementText = new StringBuilder();
        }

        public string Build()
        {
            _advertisement.Accept(this);
            return _advertisementText.ToString();
        }

        private void VisitChildren(AdvertisementElement element)
        {
            foreach (var child in element.Children)
                child.Accept(this);
        }

        void IAdvertismentModelVisitor.Visit(Advertisement advertisement)
        {
            VisitChildren(_advertisement);
        }

        void IAdvertismentModelVisitor.Visit(Preview preview)
        {
            string previewText = preview.Paragraph.Items.GetCombinedText();
            _advertisementText.Append(previewText);
        }

        void IAdvertismentModelVisitor.Visit(PreviewHeader previewHeader)
        {
        }

        void IAdvertismentModelVisitor.Visit(PreviewParagraph previewParagraph)
        {
        }

        void IAdvertismentModelVisitor.Visit(PreviewItem previewItem)
        {
        }

        void IAdvertismentModelVisitor.Visit(PricePreviewItem previewItem)
        {
        }

        void IAdvertismentModelVisitor.Visit(Rule rule)
        {
        }

        void IAdvertismentModelVisitor.Visit(Module module)
        {
            string moduleText = module.Paragraph.Items.GetCombinedText();
            _advertisementText.Append(PRE_HEADER + module.Header.Item.Text + ": " + moduleText);
        }

        void IAdvertismentModelVisitor.Visit(Header header)
        {
        }

        void IAdvertismentModelVisitor.Visit(Paragraph paragraph)
        {
        }

        void IAdvertismentModelVisitor.Visit(Item item)
        {
        }

        void IAdvertismentModelVisitor.Visit(PriceItem item)
        {
        }

        void IAdvertismentModelVisitor.Visit(PreBlurb preBlurb)
        {
        }

        void IAdvertismentModelVisitor.Visit(BlurbTemplate blurbTemplate)
        {
        }

        void IAdvertismentModelVisitor.Visit(PostBlurb postBlurb)
        {
        }

        
    }
}
