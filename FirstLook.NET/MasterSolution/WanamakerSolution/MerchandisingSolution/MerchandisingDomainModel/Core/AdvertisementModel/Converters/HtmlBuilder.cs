﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    internal class HtmlBuilder : IAdvertismentModelVisitor
    {
        private const string PRE_HEADER = "<br /><br />";
        private const string POST_HEADER = "<br />";

        private readonly Advertisement _advertisement;
        private readonly StringBuilder _advertisementText;

        private bool _showPreHeader;

        public HtmlBuilder(Advertisement advertisement)
        {
            _advertisement = advertisement;
            _advertisementText = new StringBuilder();
            _showPreHeader = false;
        }

        private void VisitChildren(AdvertisementElement element)
        {
            foreach (var child in element.Children)
                child.Accept(this);
        }

        public string Build()
        {
            _advertisement.Accept(this);
            return _advertisementText.ToString();
        }
        
        void IAdvertismentModelVisitor.Visit(Advertisement advertisement)
        {
            VisitChildren(_advertisement);
        }

        void IAdvertismentModelVisitor.Visit(Preview preview)
        {
            string previewText = preview.Paragraph.Items.GetCombinedText();
            if (!string.IsNullOrEmpty(previewText))
                _showPreHeader = true;

            _advertisementText.Append(previewText);
        }

        void IAdvertismentModelVisitor.Visit(PreviewHeader previewHeader)
        {
        }

        void IAdvertismentModelVisitor.Visit(PreviewParagraph previewParagraph)
        {
        }

        void IAdvertismentModelVisitor.Visit(PreviewItem previewItem)
        {
        }

        void IAdvertismentModelVisitor.Visit(PricePreviewItem previewItem)
        {
        }

        void IAdvertismentModelVisitor.Visit(Rule rule)
        {
        }

        void IAdvertismentModelVisitor.Visit(Module module)
        {
            string moduleText = module.Paragraph.Items.GetCombinedText(); 
            if (_showPreHeader)
            {
                _advertisementText.Append(PRE_HEADER + module.Header.Item.Text + POST_HEADER + moduleText);
            }
            else //if there is no preview we don't want to show <br/> as first characters due to autotrader
            {
                if (string.IsNullOrEmpty(module.Header.Item.Text))
                    _advertisementText.Append(moduleText);
                else
                    _advertisementText.Append(module.Header.Item.Text + POST_HEADER + moduleText);

                _showPreHeader = true;
            }
        }

        void IAdvertismentModelVisitor.Visit(Header header)
        {
        }

        void IAdvertismentModelVisitor.Visit(Paragraph paragraph)
        {
        }

        void IAdvertismentModelVisitor.Visit(Item item)
        {
        }

        void IAdvertismentModelVisitor.Visit(PriceItem item)
        {
        }

        void IAdvertismentModelVisitor.Visit(PreBlurb preBlurb)
        {
        }

        void IAdvertismentModelVisitor.Visit(BlurbTemplate blurbTemplate)
        {
        }

        void IAdvertismentModelVisitor.Visit(PostBlurb postBlurb)
        {
        }               

    }
}
