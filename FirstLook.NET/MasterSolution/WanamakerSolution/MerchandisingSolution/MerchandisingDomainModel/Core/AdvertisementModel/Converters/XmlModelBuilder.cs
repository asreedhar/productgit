﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    internal class XmlModelBuilder : IAdvertismentModelVisitor
    {
        private XmlDocument _document;
        private XmlNode _parentXmlNode;

        public XmlModelBuilder()
        {
            _document = new XmlDocument();
        }

        public void Build(Advertisement advertisement)
        {
            advertisement.Accept(this);
        }

        public string Xml
        {
            get
            {
                return _document.InnerXml;
            }
        }

        private void VisitChildren(AdvertisementElement element, XmlNode parent)
        {
            foreach (var child in element.Children)
            {
                _parentXmlNode = parent;
                child.Accept(this);
            }
        }

        public void Visit(Advertisement advertisement)
        {
            XmlNode rootNode = _document.CreateElement(Variables.AD);

            var versionAttribute = _document.CreateAttribute(Variables.VER);
            var adTemplateAttribute = _document.CreateAttribute(Variables.ADTEMPLATEID);
            var nameAttribute = _document.CreateAttribute(Variables.NAME);
            versionAttribute.Value = advertisement.Version;
            adTemplateAttribute.Value = advertisement.AdTemplateID.ToString();
            nameAttribute.Value = advertisement.Name;

            rootNode.Attributes.Append(versionAttribute);
            rootNode.Attributes.Append(adTemplateAttribute);
            rootNode.Attributes.Append(nameAttribute);

            _document.AppendChild(rootNode);
            VisitChildren(advertisement, rootNode);
        }

        public void Visit(Preview preview)
        {
            XmlNode previewNode = _document.CreateElement(Variables.PREVIEW);
            _parentXmlNode.AppendChild(previewNode);

            VisitChildren(preview, previewNode);
        }

        public void Visit(PreviewHeader previewHeader)
        {
            XmlNode previewHeaderNode = _document.CreateElement("PreviewHeader");
            _parentXmlNode.AppendChild(previewHeaderNode);

            VisitChildren(previewHeader, previewHeaderNode);
        }

        public void Visit(PreviewParagraph previewParagraph)
        {
            XmlNode previewParagraphNode = _document.CreateElement("PreviewParagraph");
            _parentXmlNode.AppendChild(previewParagraphNode);

            VisitChildren(previewParagraph, previewParagraphNode);
        }

        private void BuildPreviewItem(PreviewItem previewItem, XmlNode previewItemNode)
        {
            var editedAttribute = _document.CreateAttribute("Edited");
            editedAttribute.Value = previewItem.Edited.ToString().ToLower();
            previewItemNode.Attributes.Append(editedAttribute);

            XmlNode textNode = _document.CreateElement("Text");
            textNode.InnerText = previewItem.Text;
            previewItemNode.AppendChild(textNode);

            VisitChildren(previewItem, previewItemNode);
        }

        public void Visit(PreviewItem previewItem)
        {
            XmlNode previewItemNode = _document.CreateElement("PreviewItem");
            _parentXmlNode.AppendChild(previewItemNode);

            BuildPreviewItem(previewItem, previewItemNode);
        }        

        public void Visit(PricePreviewItem previewItem)
        {
            XmlNode priceItemNode = _document.CreateElement("PricePreviewItem");
            _parentXmlNode.AppendChild(priceItemNode);

            BuildPreviewItem(previewItem, priceItemNode);
        }

        public void Visit(Rule rule)
        {
            XmlNode ruleNode = _document.CreateElement("Rule");
            _parentXmlNode.AppendChild(ruleNode);

            var iDAttribute = _document.CreateAttribute("ID");
            iDAttribute.Value = rule.ID.ToString().ToUpper();
            ruleNode.Attributes.Append(iDAttribute);

            VisitChildren(rule, ruleNode);
        }

        public void Visit(Module module)
        {
            XmlNode moduleNode = _document.CreateElement(Variables.MODULE);
            _parentXmlNode.AppendChild(moduleNode);

            VisitChildren(module, moduleNode);
        }

        public void Visit(Header header)
        {
            XmlNode headerNode = _document.CreateElement(Variables.HEADER);
            _parentXmlNode.AppendChild(headerNode);

            VisitChildren(header, headerNode);
        }

        public void Visit(Paragraph paragraph)
        {
            XmlNode paragraphNode = _document.CreateElement(Variables.PARAGRAPH);
            _parentXmlNode.AppendChild(paragraphNode);

            VisitChildren(paragraph, paragraphNode);
        }


        private void BuildItem(Item item, XmlNode itemNode)
        {
            var editedAttribute = _document.CreateAttribute("Edited");
            editedAttribute.Value = item.Edited.ToString().ToLower();
            itemNode.Attributes.Append(editedAttribute);

            XmlNode textNode = _document.CreateElement("Text");
            textNode.InnerText = item.Text;
            itemNode.AppendChild(textNode);

            if (item.Children.Count > 0) //ad blurb element
            {
                XmlNode blurbNode = _document.CreateElement("Blurbs");
                itemNode.AppendChild(blurbNode);

                var templateIDAttribute = _document.CreateAttribute("BlurbTemplateID");
                templateIDAttribute.Value = item.BlurbTemplateID.ToString();
                blurbNode.Attributes.Append(templateIDAttribute);

                VisitChildren(item, blurbNode);
            }
        }

        public void Visit(Item item)
        {
            XmlNode itemNode = _document.CreateElement(Variables.ITEM);
            _parentXmlNode.AppendChild(itemNode);

            BuildItem(item, itemNode);
        }

        public void Visit(PriceItem item)
        {
            XmlNode priceItemNode = _document.CreateElement("PriceItem");
            _parentXmlNode.AppendChild(priceItemNode);

            BuildItem(item, priceItemNode);
        }

        public void Visit(PreBlurb preBlurb)
        {
            XmlNode preNode = _document.CreateElement("PreBlurb");
            _parentXmlNode.AppendChild(preNode);

            var IDAttribute = _document.CreateAttribute("ID");
            IDAttribute.Value = preBlurb.ID.ToString();
            preNode.Attributes.Append(IDAttribute);

            preNode.InnerText = preBlurb.Text;

            VisitChildren(preBlurb, preNode);
        }

        public void Visit(BlurbTemplate blurbTemplate)
        {
            XmlNode blurbTemplateNode = _document.CreateElement("BlurbTemplate");
            _parentXmlNode.AppendChild(blurbTemplateNode);

            var IDAttribute = _document.CreateAttribute("ID");
            IDAttribute.Value = blurbTemplate.ID.ToString();
            blurbTemplateNode.Attributes.Append(IDAttribute);

            var nameAttribute = _document.CreateAttribute("Name");
            nameAttribute.Value = blurbTemplate.Name;
            blurbTemplateNode.Attributes.Append(nameAttribute);

            blurbTemplateNode.InnerText = blurbTemplate.Text;

            VisitChildren(blurbTemplate, blurbTemplateNode);
        }

        public void Visit(PostBlurb postBlurb)
        {
            XmlNode postNode = _document.CreateElement("PostBlurb");
            _parentXmlNode.AppendChild(postNode);

            var IDAttribute = _document.CreateAttribute("ID");
            IDAttribute.Value = postBlurb.ID.ToString();
            postNode.Attributes.Append(IDAttribute);

            postNode.InnerText = postBlurb.Text;

            VisitChildren(postBlurb, postNode);
        }

    }
}
