﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters
{
    internal class XmlModelParser
    {
        public Advertisement Parse(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            string version = doc.DocumentElement.Attributes[Variables.VER].Value;
            if (version == "1") 
                return FromClassicXml(xml);

            return ParseAdvertisement(doc.DocumentElement);
        }

        private static string GetFirstValue(XContainer element, string xpath)
        {
            return element.Descendants(xpath).First().Value;
        }

        private static Advertisement FromClassicXml(string xml) //1.0 model
        {
            Advertisement advertisement = new Advertisement();
            advertisement.Version = "1.5";
            XDocument doc = XDocument.Parse(xml);
            XElement root = doc.Root;

            if (root == null)
                throw new ApplicationException("No Advertisement root found.");

            var previewElements = root.Descendants(Variables.PREVIEW);
            var moduleElements = root.Descendants(Variables.MODULE);

            if (previewElements.Count() != 1)
                throw new ArgumentException("A preview Element is required");

            if (previewElements.Count() == 1)
            {
                var node = previewElements.First();
                var preview = new Preview();
                advertisement.Children.Add(preview);

                var header = new PreviewHeader();
                preview.Children.Add(header);
                header.Children.Add(new PreviewItem() { Text = GetFirstValue(node, Variables.HEADER) });

                var paragraph = new PreviewParagraph();
                preview.Children.Add(paragraph);
                paragraph.Children.Add(new PreviewItem() { Text = GetFirstValue(node, Variables.PARAGRAPH) });
            }

            foreach (var module in moduleElements)
            {
                var modelModule = new Module();
                advertisement.Children.Add(modelModule);

                var header = new Header();
                modelModule.Children.Add(header);
                header.Children.Add(new Item() { Text = GetFirstValue(module, Variables.HEADER) });

                var paragraph = new Paragraph();
                modelModule.Children.Add(paragraph);
                paragraph.Children.Add(new Item() { Text = GetFirstValue(module, Variables.PARAGRAPH) });
            }

            return advertisement;
        }

        private static Advertisement ParseAdvertisement(XmlNode node)
        {
            Advertisement advertisement = new Advertisement();
            advertisement.Version = node.Attributes[Variables.VER].Value;
            advertisement.Name = node.Attributes[Variables.NAME].Value;
            advertisement.AdTemplateID = int.Parse(node.Attributes[Variables.ADTEMPLATEID].Value);

            if (node.SelectSingleNode(Variables.PREVIEW) != null) //if there is a preview
                advertisement.Children.Add(ParsePreview(node.SelectSingleNode(Variables.PREVIEW)));

            foreach (XmlNode module in node.SelectNodes(Variables.MODULE))
                advertisement.Children.Add(ParseModule(module));

            return advertisement;
        }

        private static Module ParseModule(XmlNode node)
        {
            Module module = new Module();
            module.Children.Add(ParseHeader(node.SelectSingleNode(Variables.HEADER)));
            module.Children.Add(ParseParagraph(node.SelectSingleNode(Variables.PARAGRAPH)));

            return module;
        }


        private static Header ParseHeader(XmlNode node)
        {
            Header header = new Header();
            header.Children.Add(ParseItem(node.SelectSingleNode(Variables.ITEM), new Item()));

            return header;
        }

        private static Paragraph ParseParagraph(XmlNode node)
        {
            Paragraph paragraph = new Paragraph();
            foreach (XmlNode childNode in node.SelectNodes(Variables.ITEM))
                paragraph.Children.Add(ParseItem(childNode, new Item()));

            foreach (XmlNode childNode in node.SelectNodes("PriceItem"))
                paragraph.Children.Add(ParseItem(childNode, new PriceItem()));

            return paragraph;

        }

        private static Item ParseItem(XmlNode node, Item item)
        {
            XmlNode textNode = node.SelectSingleNode(Variables.TEXT);
            item.Text = textNode.InnerText;
            item.Edited = bool.Parse(node.Attributes["Edited"].Value);

            XmlNode blurbs = node.SelectSingleNode("Blurbs");
            if (blurbs != null)
            {
                foreach (XmlNode childNode in blurbs.ChildNodes)
                    item.Children.Add(ParseBlurbs(childNode));
            }

            return item;
        }

        private static AdvertisementElement ParseBlurbs(XmlNode node)
        {
            if (node.Name == "PreBlurb")
            {
                PreBlurb preBlurb = new PreBlurb();
                preBlurb.ID = int.Parse(node.Attributes["ID"].Value);
                preBlurb.Text = node.InnerText;
                return preBlurb;
            }

            if (node.Name == "BlurbTemplate")
            {
                BlurbTemplate template = new BlurbTemplate();
                template.ID = int.Parse(node.Attributes["ID"].Value);
                template.Name = node.Attributes["Name"].Value;
                template.Text = node.InnerText;
                return template;
            }

            if (node.Name == "PostBlurb")
            {
                PostBlurb postBlurb = new PostBlurb();
                postBlurb.ID = int.Parse(node.Attributes["ID"].Value);
                postBlurb.Text = node.InnerText;
                return postBlurb;
            }

            throw new InvalidOperationException("Unrecognizable blurb");
        }

        private static Preview ParsePreview(XmlNode node)
        {
            Preview preview = new Preview();
            preview.Children.Add(ParsePreviewHeader(node.SelectSingleNode("PreviewHeader")));
            preview.Children.Add(ParsePreviewParagraph(node.SelectSingleNode("PreviewParagraph")));

            return preview;
        }

        private static PreviewParagraph ParsePreviewParagraph(XmlNode node)
        {
            PreviewParagraph paragraph = new PreviewParagraph();
            
            foreach(XmlNode childNode in node.ChildNodes)
            {
                if(childNode.Name == "PreviewItem")
                    paragraph.Children.Add(ParsePreviewItem(childNode, new PreviewItem()));
                if(childNode.Name == "PricePreviewItem")
                    paragraph.Children.Add(ParsePreviewItem(childNode, new PricePreviewItem()));
            }

            return paragraph;
        }

        private static PreviewHeader ParsePreviewHeader(XmlNode node)
        {
            PreviewHeader header = new PreviewHeader();
            header.Children.Add(ParsePreviewItem(node.SelectSingleNode("PreviewItem"), new PreviewItem()));

            return header;
        }

        private static PreviewItem ParsePreviewItem(XmlNode node, PreviewItem item)
        {
            XmlNode textNode = node.SelectSingleNode("Text");
            item.Text = textNode.InnerText;
            item.Edited = bool.Parse(node.Attributes["Edited"].Value);

            XmlNode ruleNode = node.SelectSingleNode("Rule");
            if (ruleNode != null)
                item.Children.Add(ParseRule(ruleNode));

            return item;
        }

        private static Rule ParseRule(XmlNode node)
        {
            Rule rule = new Rule();
            rule.ID = new Guid(node.Attributes["ID"].Value);

            return rule;
        }

    }
}
