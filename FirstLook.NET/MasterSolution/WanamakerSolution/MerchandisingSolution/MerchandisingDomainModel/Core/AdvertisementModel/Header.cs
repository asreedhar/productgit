﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class Header : AdvertisementElement
    {
        public Item Item
        {
            get
            {
                return Children.Where(x => typeof(Item)
                       .IsInstanceOfType(x))
                       .Cast<Item>().ToList().FirstOrDefault();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
