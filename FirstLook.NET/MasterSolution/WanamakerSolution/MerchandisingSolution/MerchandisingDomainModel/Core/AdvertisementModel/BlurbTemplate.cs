﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class BlurbTemplate : AdvertisementElement
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
