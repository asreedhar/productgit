﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.AdvertisementModel
{
    public class PreviewItem : AdvertisementElement
    {
        public bool Edited { get; set;}
        public string Text { get; set; }

        public PreviewItem()
        {
            Text = String.Empty;
            Edited = false;
        }

        public Rule Rule
        {
            get
            {
                return Children.Where(x => typeof(Rule)
                    .IsInstanceOfType(x))
                    .Cast<Rule>().ToList().FirstOrDefault();
            }
        }

        public override void Accept(IAdvertismentModelVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
