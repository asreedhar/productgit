﻿namespace FirstLook.Merchandising.DomainModel.Core.Authentication
{
    public interface IAuthenticateUser
    {
        /// <summary>
        /// Authenticates a credential.
        /// </summary>
        /// <param name="credential">The credential to authenticate.</param>
        /// <returns>Returns null if the credential is invalid. Otherwise returns the AuthenticationType. <seealso cref="System.Security.Principal.IIdentity"/></returns>
        string Authenticate(BasicCredential credential);
    }
}