﻿using System.Security.Principal;
using System.Web;

namespace FirstLook.Merchandising.DomainModel.Core.Authentication
{
    public class CurrentAspNetPrincipal : ICurrentPrincipal
    {
        public IPrincipal Current
        {
            get { return HttpContext.Current.User; }
            set { HttpContext.Current.User = value; }
        }
    }
}