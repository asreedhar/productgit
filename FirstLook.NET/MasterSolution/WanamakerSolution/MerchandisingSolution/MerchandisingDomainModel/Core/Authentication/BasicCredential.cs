﻿namespace FirstLook.Merchandising.DomainModel.Core.Authentication
{
    public class BasicCredential
    {
        public BasicCredential(string username, string password)
        {
            Username = username ?? "";
            Password = password ?? "";
        }

        public string Username { get; private set; }
        public string Password { get; private set; }

        public bool Equals(BasicCredential other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Username, Username) && Equals(other.Password, Password);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (BasicCredential)) return false;
            return Equals((BasicCredential) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Username.GetHashCode()*397) ^ Password.GetHashCode();
            }
        }
    }
}