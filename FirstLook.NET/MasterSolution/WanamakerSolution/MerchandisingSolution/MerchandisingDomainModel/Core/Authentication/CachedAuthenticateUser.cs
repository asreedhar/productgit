﻿using System;
using System.Text;
using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.Core.Authentication
{
    public class CachedAuthenticateUser : IAuthenticateUser
    {
        private readonly ICache _cache;
        private readonly IAuthenticateUser _inner;
        private readonly ITimeProvider _time;

        public const int CacheIntervalInSeconds = 90;
        private static readonly string BaseCacheKey = typeof (CachedAuthenticateUser).FullName + "_";
        private static readonly Encoding Encoding = new UTF8Encoding(false);

        public CachedAuthenticateUser(ICache cache, IAuthenticateUser inner, ITimeProvider time)
        {
            _cache = cache;
            _inner = inner;
            _time = time;
        }

        public string Authenticate(BasicCredential credential)
        {
            if(credential == null)
                throw new ArgumentNullException("credential");
            
            var cacheKey = BaseCacheKey + BytesToHex(HashUsernameAndPassword(credential.Username, credential.Password));
            var passwordHash = BytesToHex(HashString(credential.Password));

            var now = _time.UtcNow;
            var dto = (UserDTO) _cache.Get(cacheKey);
            if(dto == null || !dto.Validate(credential.Username, passwordHash, now))
            {
                var authenticationType = _inner.Authenticate(credential);
                dto = new UserDTO(credential.Username, passwordHash, now, authenticationType);
                _cache.Set(cacheKey, dto, CacheIntervalInSeconds);
            }

            return dto.AuthenticationType;
        }

        private class UserDTO
        {
            public UserDTO(string username, string passwordHash, DateTimeOffset now, string authenticatedType)
            {
                Timestamp = now;
                PasswordHash = passwordHash;
                UserName = username;
                AuthenticationType = authenticatedType;
            }

            public bool Validate(string username, string passwordHash, DateTimeOffset now)
            {
                var timeSpan = now < Timestamp ? Timestamp - now : now - Timestamp;
                return timeSpan.TotalSeconds <= CacheIntervalInSeconds &&
                        StringComparer.InvariantCultureIgnoreCase.Equals(UserName, username) &&
                        StringComparer.InvariantCulture.Equals(PasswordHash, passwordHash);
            }

            private string UserName { get; set; }
            private string PasswordHash { get; set; }
            private DateTimeOffset Timestamp { get; set; }
            public string AuthenticationType { get; private set; }
        }

        private static byte[] HashUsernameAndPassword(string username, string password)
        {
            return HashString(string.Format("{0}:{1}", username, password));
        }

        private static byte[] HashString(string value)
        {
            using (var hashAlg = System.Security.Cryptography.SHA256.Create())
            {
                var bytes = Encoding.GetBytes(value);
                return hashAlg.ComputeHash(bytes);
            }
        }

        // From http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa-in-c#answer-632920
        private static string BytesToHex(byte[] barray)
        {
            var c = new char[barray.Length * 2];
            for (var i = 0; i < barray.Length; ++i)
            {
                var b = ((byte)(barray[i] >> 4));
                c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = ((byte)(barray[i] & 0xF));
                c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }

            return new string(c);
        }
    }
}