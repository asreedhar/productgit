﻿using System;
using System.Configuration;
using CASWrapper;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration;

namespace FirstLook.Merchandising.DomainModel.Core.Authentication
{
    public class AuthenticateUserWithCasClient : IAuthenticateUser
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AuthenticateUserWithCasClient>();

        private const string CasServiceUrlKey = "CasWebServiceUrl";

        public string Authenticate(BasicCredential credential)
        {
            var serviceUrl = GetCasServiceUrl();
            var wrapper = new CasServiceWrapper(serviceUrl);

            try
            {
                if(Log.IsInfoEnabled)
                {
                    Log.InfoFormat("Authenticating. UserName = '{0}'", credential.Username);
                }

                // We don't actually need to create a ticket. We just need to know if the credentials are authentic.
                bool isAuthenticated;
                wrapper.CreateTicket(credential.Username, credential.Password, out isAuthenticated);

                if(Log.IsInfoEnabled)
                {
                    Log.InfoFormat("Finished Authenticating. UserName = '{0}', IsAuthenticated = {1}", credential.Username,
                                   isAuthenticated ? "true" : "false");
                }

                return isAuthenticated ? "Basic-CAS" : null;
            }
            catch (Exception ex)
            {
                Log.Error("Failed while communicated with the CAS SOAP service.", ex);
                return null;
            }
        }

        private static string GetCasServiceUrl()
        {
            var url = ConfigurationManager.AppSettings[CasServiceUrlKey];
            if (!string.IsNullOrEmpty(url))
                return url;

            var configuration =
                (CentralAuthenticationServiceConfigurationSection) ConfigurationManager.GetSection("centralAuthenticationService")
                ?? new CentralAuthenticationServiceConfigurationSection();

            return configuration.ValidateUrl;
        }
    }
}