﻿using System.Security.Principal;

namespace FirstLook.Merchandising.DomainModel.Core.Authentication
{
    public interface ICurrentPrincipal
    {
        IPrincipal Current { get; set; }
    }
}