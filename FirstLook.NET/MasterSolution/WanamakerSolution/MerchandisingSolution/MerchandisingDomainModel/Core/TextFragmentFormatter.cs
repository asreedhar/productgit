﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace FirstLook.Merchandising.DomainModel.Core
{
    internal class TextFragmentFormatter : ITextFragmentFormatter
    {
        public virtual string Format(string inString)
        {
            if( string.IsNullOrEmpty( inString ) ) { return string.Empty; }

            string formatted = inString;

            formatted = ProcessExceptions( formatted );
            formatted = CleanUpIrregularities(formatted);
            formatted = RemoveDelimiters(formatted);
            formatted = FixGrammer(formatted);

            // Formatting out equals signs.
            formatted = ReplaceEqualSpace(formatted);
            formatted = RemoveRepeatedCharacters(formatted);

            if( formatted.Trim().Length == 0 ) { return string.Empty; }

            return formatted.TrimStart() + " ";
        }

        internal static string ReplaceEqualSpace( string inString )
        {
            // "= =" -> "=" 
            Regex equals = new Regex("= =");
            return equals.Replace(inString, "=");
            
        }

        internal static string RemoveRepeatedCharacters(string inString)
        {
            // two or more equals are considered to be a delimiter.
            Regex equals = new Regex("(=){2,}");
            inString = equals.Replace(inString, " ");

            Regex tilda = new Regex("(~){2,}");
            inString = tilda.Replace(inString, " ");

            Regex comma = new Regex("(,){2,}");
            inString = comma.Replace(inString, " ");

            // Any repetition of period should be rewritten as a single period.
            Regex period = new Regex("(\\.){2,}");
            inString = period.Replace(inString, ". ");

            return inString;
        }

        internal static string FixGrammer(string inString)
        {
            // Get rid of leading and trailing spaces.
            inString = inString.Trim();

            // Sentences should not begin with "and"
            if (inString.StartsWith("and", true, CultureInfo.CurrentCulture))
            {
                inString = inString.Substring(3, inString.Length - 3);
            }
            
            // Sentences should not begin with "or"
            inString = Regex.Replace(inString, @"^or(\W)", "$1", RegexOptions.IgnoreCase);

            // Sentences should not end with ","
            if (inString.EndsWith(",", true, CultureInfo.CurrentCulture))
            {
                inString = inString.Substring(0, inString.Length - 1);
            }

            // Sentences should not begin with ","
            if (inString.StartsWith(",", true, CultureInfo.CurrentCulture))
            {
                inString = inString.Substring(1, inString.Length-1);
            }

            // sentences should not end with "-"
            if (inString.EndsWith("-", true, CultureInfo.CurrentCulture))
            {
                inString = inString.Substring(0, inString.Length - 1);
            }


            return inString;
        }

        internal static string RemoveDelimiters(string inString)
        {
            // Get rid of leading and trailing spaces.
            inString = inString.Trim();

            // These occasionally render.
            if (inString.Equals("+") || inString.Equals("*"))
            {
                return string.Empty;
            }

            return inString;
        }

        internal static string CleanUpIrregularities(string inString)
        {
            // Get rid of leading and trailing spaces.
            inString = inString.Trim();

            //remove all ", ." ", ~" ", !" or ", *"  with 
            Regex rg1 = new Regex("[,][\\s]*[\\.\\*\\~!\\-]");
            while (rg1.IsMatch(inString))
            {
                Match mt = rg1.Match(inString);
                //remove all but the first punctuation...add back a single space
                inString = inString.Remove(mt.Index, mt.Length - 1) + " ";
            }

            //remove all "! [punctuation]" ". [punctuation]"  "? [punctuation]"  or ": [punctuation]"   with 
            Regex rg5 = new Regex("[,!?][\\s]*[\\.?!\\-,]");
            while (rg5.IsMatch(inString))
            {
                Match mt = rg5.Match(inString);
                //remove all but the first space...
                inString = inString.Remove(mt.Index + 1, mt.Length - 1) + " ";
            }


            //remove ., and . , and ~, and ~ ,
            Regex rg3 = new Regex("[\\.|\\~][\\s]*[,]");
            inString = rg3.Replace(inString, ". ");
            Regex rg6 = new Regex("[\\~][\\s]*[,\\.\\:!?]");
            inString = rg6.Replace(inString, "~ ");

            inString = Regex.Replace( inString, "[\\;][\\.|\\,]", ";" );

            Regex rg4 = new Regex("[\\:][\\s]*[\\.,\\-\\~]");
            inString = rg4.Replace(inString, ": ");

            Regex rg = new Regex("[\\dA-Za-z][\\s]{2,}[\\dA-Za-z]");
            while (rg.IsMatch(inString))
            {
                Match mt = rg.Match(inString);
                //remove all but the first space...
                inString = inString.Remove(mt.Index + 2, mt.Length - 3);
            }

            Regex rg2 = new Regex("[\\s]{2,}");
            inString = rg2.Replace(inString, " ");

            rg2 = new Regex("[\\.,\\-\\~]+[\\s]+\\.\\.\\.");
            inString = rg2.Replace(inString, "...");

            inString = Regex.Replace(inString, "[\\s]+[\\.]", "");
            return inString.Trim(" ".ToCharArray());
        }

        private static string ProcessExceptions(string inString)
        {
            //auto trans
            Regex rg1 = new Regex("[Aa][/][Tt]");
            inString = rg1.Replace(inString, "Automatic Transmission");

            //auto trans
            Regex rg2 = new Regex("[Mm][/][Tt]");
            inString = rg2.Replace(inString, "Manual Transmission");

            return inString;
        }
    }
}
