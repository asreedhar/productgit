using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.MarketingData;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface IVehicleTemplatingData
    {
        string HighInternetPrice { get; }
        bool IsPriceReduced { get; }
        bool IsPriceReducedEnough { get; }
        bool HasDisplayablePrice { get; }
        string ListPrice { get; }
        bool HasGoodPriceBelowMSRP { get; }
        string OriginalMsrp { get; }
        bool HasFinancingAvailable { get; }
        string SpecialFinancing { get; }
        decimal PreferredBookValue { get; }
        bool HasGoodPriceBelowBook { get; }
        BookoutCollection Bookouts { get; }
        string PreferredBook { get; }
        string TargetPriceDisplay { get; }
        decimal TargetPrice { get; }
        int CountBelowBook { get; }
        int CountInPriceRange { get; }
        int CountBelowTwentyThousandDollars { get; }
        int CountBelowFifteenThousandDollars { get; }
        int CountBelowTenThousandDollars { get; }
        int CountBelowNineThousandDollars { get; }
        int CountBelowEightThousandDollars { get; }
        int CountBelowSevenThousandDollars { get; }
        string DisclaimerDate { get; }
        DateTime? ExpirationDate { get; set; }
        VehicleAttributes VehicleAttributes { get; set; }
        IVehicleConfiguration VehicleConfig { get; set; }
        ITemplateMediator Mediator { get; set; }
        
        IInventoryData InventoryItem { get; set; }
        
        DealerAdvertisementPreferences Preferences { get; set; }
        bool IsLowMilesPerYear { get; }
        bool IsNewVehicle { get; }
        bool HasInteriorType { get; }
        string MilesPerYearCategory { get; }
        string Make { get; }
        string Model { get; }
        string Year { get; }
        bool HasTrim { get; }
        string Trim { get; }
        string Mileage { get; }
        bool HasColors { get; }
        string InteriorType { get; }
        string Colors { get; }
        string InteriorColor { get; }
        string ExteriorColor { get; }
        string StockNumber { get; }
        bool HasCertificationPreview { get; }
        string CertificationPreview { get; }
        bool HasCertificationDetails { get; }
        string CertificationDetails { get; }
        bool IsCertified { get; }
        string Certified { get; }
        string CertificationTitle { get; }
        string CertifiedID { get; }
        string FuelCity { get; }
        string FuelHwy { get; }
        bool GetsGoodGasMileage { get; }
        string BestGasMileage { get; }
        bool HasRoadsideAssistance { get; }
        string RoadsideAssistance { get; }
        ConsumerInfo ConsInfo { get; set; }
        bool HasGoodCrashTestRatings { get; }
        string DriverFrontCrash { get; }
        string DriverSideCrash { get; }
        string RolloverRating { get; }
        string BasicWarranty { get; }
        string DrivetrainWarranty { get; }
        CarfaxInterface CarfaxInfo { get; set; }
        bool IsOneOwner { get; }
        bool IsCleanCarfaxReport { get; }
        string CleanCarfaxReport { get; }
        string Carfax1Owner { get; }
        bool HasBuybackGuarantee { get; }
        string BuybackGuarantee { get; }
        AutoCheckInterface AutoCheckInfo { get; }
        bool IsAutoCheckOneOwner { get; }
        string AutoCheckOneOwner { get; }
        bool IsAutoCheckAssured { get; }
        string AutoCheckAssured { get; }
        bool IsCleanAutoCheckReport { get; }
        string CleanAutoCheckReport { get; }
        bool HasGoodAutoCheckScore { get; }
        string AutoCheckScore { get; }
        bool HasHighlightedPackages { get; }
        List<PreviewEquipmentItem> PreviewEquipment { get; }
        bool HasMostSearchedEquipmentAvailable { get; }
        bool HasTier1EquipmentAvailable { get; }
        bool HasEnoughTier1EquipmentAvailable { get; }
        bool HasTier2EquipmentAvailable { get; }
        bool HasEnoughTier2EquipmentAvailable { get; }
        string AfterMarketEquipment { get; }
        string Loaded { get; }
        List<string> MajorEquipmentOfInterest { get; }
        bool HasValuedOptions { get; }
        List<string> ValuedOptions { get; }
        bool HasTruckOptions { get; }
        List<string> TruckOptions { get; }
        bool HasSafetyOptions { get; }
        List<string> SafetyOptions { get; }
        bool HasConvenienceOptions { get; }
        List<string> ConvenienceOptions { get; }
        bool HasLuxuryOptions { get; }
        List<string> LuxuryOptions { get; }
        bool HasSportyOptions { get; }
        List<string> SportyOptions { get; }
        bool HasThemedEquipment { get; }
        List<string> ThemedEquipment { get; }
        List<string> HighlightedPackages { get; }
        List<string> Tier1Equipment { get; }
        List<string> Tier2Equipment { get; }
        List<string> Tier3Equipment { get; }
        string Engine { get; }
        string EngineOfInterest { get; }
        string EngineDetails { get; }
        string Horsepower { get; }
        string Drivetrain { get; }
        string DrivetrainOfInterest { get; }
        string Transmission { get; }
        string TransmissionOfInterest { get; }
        string FuelType { get; }
        string FuelTypeOfInterest { get; }
        ModelMarketingCollection MarketingCollection { get; }
        string BestModelAward { get; }
        bool HasAwards { get; }
        string ModelAwards { get; }
        string MarketingPreview { get; }
        string MarketingSuperlative { get; }
        string MarketingSentence { get; }
        string MarketingSummary { get; }
        MarketingDataCollection MarketingList { get; }
        bool HasMarketingPreviewSuperlative { get; }
        bool HasMarketingSuperlative { get; }
        bool HasMarketingData { get; }
        JdPowerRatingCollection JdPowerRatings { get; }
        bool HasGoodJdPowerRatings { get; }
        string GoodJdPowerRatings { get; }
        string JdPowerRatingPreview { get; }
        bool HasConditionHighlights { get; }
        bool HasCondition { get; }
        string ConditionSentence { get; }
        string ConditionSummary { get; }
        bool HasConditionDetails { get; }
        string PreviewHighlights { get; }
        decimal ReconditioningValue { get; }
        string ReconditioningText { get; }
        bool HasKeyInformation { get; }
        string AdditionalInformation { get; }
        bool HasMissionPreviewStatment { get; }
        string MissionPreviewStatement { get; }
        IPreviewPreferences UsedPreviewPreferences { get; }
        IPreviewPreferences NewPreviewPreferences { get; }
        IPreviewPreferences PreviewPrefs { get; }
        bool HasAvailableBookout(BookoutCategory category);
        string PriceBelowBook { get; }
        string RoughPriceBelowBook { get;}
        string RoughPriceBelowMSRP { get;}
        string PriceBelowMSRP { get; }

        /// <summary>
        /// Set the financing special for a vehicle
        /// </summary>
        /// <param name="specialId"></param>
        /// <param name="memberLogin"></param>
        void SetFinancingSpecial(int specialId, string memberLogin);

        string YearMakeModel { get; }
        bool HasHighPassengerCountForClass();
        string GoodWarranties { get; }
        string GoodCrashTests { get; }
        bool HasEquipment(string equipmentDescription);
        string GetCondition();
        List<string> LimitCount(List<string> values);
        bool EvaluateCondition(string templateCondition);
        bool EvaluateSingleCondition(string condition);
        bool EvaluateSingleCondition(TemplateCondition condition);
        T GetRandomValue<T>(T[] array);
        T[] RandomPermutation<T>(List<T> array);
        List<IWeightedItem> RandomWeightedPermutation(List<IWeightedItem> array);
        int Next();
        int Next(int minValue, int maxValue);
        int Next(int maxValue);
        double NextDouble();
        void NextBytes(byte[] buffer);

        // returns the names of option packages
        List<string> HighlightedPackageSummaries { get; }
        decimal KBBBookValue { get; }
        decimal NADABookValue { get; }

        void AddedDescriptionContent(MerchandisingDescriptionItemType itemType);
    }
}