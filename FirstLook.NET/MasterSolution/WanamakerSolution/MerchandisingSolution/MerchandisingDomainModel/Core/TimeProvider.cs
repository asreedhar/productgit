using System;

namespace FirstLook.Merchandising.DomainModel.Core
{
    internal class TimeProvider : ITimeProvider
    {
        public DateTimeOffset UtcNow
        {
            get { return DateTimeOffset.UtcNow; }
        }
    }
}