using System;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.Core
{
    public interface IInventoryData
    {
        decimal? LastUpdateListPrice { get; set; }
        decimal ListPrice { get; set; } //could be SpecialPrice or FlListPrice depending on new car and whether it was changed
        decimal FlListPrice { get; }
        decimal LotPrice { get; }
        decimal UnitCost { get; set; }
        decimal MSRP { get; }
        decimal? SpecialPrice { get; }

        DueForRepricingMode IsDueForRepricing { get; }
        int? AgeBucketId { get; set; }
        
        int PhotoCount { get; }
        int DesiredPhotoCount { get; set; }
        string ThumbnailUrl { get; }

        CertificationType CertificationTypeId { get; }
        string CertificationTitle { get; }
        int InventoryType { get; set; }
        MerchandisingBucket? StatusBucket { get; set; }
        string VehicleLocation { get; set; }

        string BaseColor { get; set; }
        string Ext1Desc { get; }
        string Ext1Code { get; }
        string Ext2Desc { get; }
        string Ext2Code { get; }
        string IntDesc { get; }
        string IntCode { get; }

        int SegmentId { get; set; }
        bool NeedsInterior { get; set; }
        DateTime? LastDescriptionPublicationDate { get; set; }
        DateTime ReceivedDate { get; }
        bool HasBeenPublished { get; }
        string PercentComplete { get; }
        bool IsLowActivity { get; set; }
        DateTime InServiceDate { get; }
        int? ChromeStyleId { get; set; }
        int? VehicleCatalogId { get; set; }
        int InventoryID { get; }
        bool Certified { get; set; }
        string CertifiedID { get; set; }
	    int? CertifiedProgramId { get; set; }
	    string TradeOrPurchase { get; }
        int MileageReceived { get; set; }
        string DisplayMileage { get; }
        string StockNumber { get; set; }
// ReSharper disable InconsistentNaming
        string VIN { get; set; }
// ReSharper restore InconsistentNaming
        int Age { get; }
        string Year { get; set; }
        string Make { get; set; }
        string Model { get; set; }
        string YearMakeModel { get; }
        string Trim { get; set; }
        string AfterMarketTrim { get; set; }
        string MarketClass { get; set; }
        string GetMilesPerYearDescription { get; }
        string IdentifierText { get; }
        string FullHeaderText { get; }
        string HeaderText { get; }
        bool NeedsPricing { get; set; }
        bool NeedsPhotos { get; set; }
        bool NeedsExterior { get; set; }
        bool NeedsAdReview { get; set; }
        bool HasCurrentBookValue { get; }
        bool NoPackages { get; }
        bool PendingApproval { get; set; }
        bool OutdatedPrice { get; }
        bool IsPriceReduced { get; }
        int? HighInternetPrice { get; }
        bool IsPreOwned();
        bool IsNew();
        string LastDescriptionPublishedDateText();

        int GetMilesPerYear();
        MilesPerYearCategories GetMilesPerYearCategory();
        string GetGoogleLink();
        bool IsPriceReducedEnough(int minGoodPriceReduction);
        string GetEStockLink();
        bool HasCarfax { get; }

        int BusinessUnitID { get; }

        int? AutoloadStatusTypeId { get; }

        DateTime? AutoloadEndTime { get; }

        int? OnlineState { get; set; }

        string BodyStyleName { get; set; }
        string EngineDescription { get; set; }
        string EngineOptionCode { get; set; }
        bool IsStandardEngine { get; set; }
        bool AutoWindowStickerPrinted { get; }
        bool isFavourable { get; set; }
    }
}