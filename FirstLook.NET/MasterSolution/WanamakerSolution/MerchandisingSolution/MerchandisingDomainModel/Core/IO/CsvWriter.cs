﻿using System;
using System.IO;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.IO
{
    /// <summary>
    /// CsvWriter attempts to generate CSV content that is compliant with 
    /// </summary>
    public class CsvWriter : IDisposable
    {
        private const string ContentTypeWithHeader = "text/csv;charset=utf-8;header=present";
        private const string ContentTypeWithoutHeader = "text/csv;charset=utf-8;header=absent";
        private static readonly char[] CharsNeedingEscape = new[] { ',', '"', '\r', '\n' };

        private readonly bool HasHeader;
        private readonly StreamWriter Writer;
        private int ColumnCount;
        private int Column;
        private int Row;

        public CsvWriter(Stream innerStream, bool hasHeader = true)
        {
            HasHeader = hasHeader;
            var encoding = new UTF8Encoding(false);
            Writer = new StreamWriter(
                new NonClosingStreamWrapper(innerStream),
                encoding) {NewLine = "\r\n"};
        }

        public void Dispose()
        {
            Writer.Dispose();
            EndRecord();
            CheckMinimumRowCount();
        }

        public void StartRecord()
        {
            EndRecord();

            ++Row;
            if (Row > 1)
                Writer.WriteLine();

            Column = 0;
        }

        private void EndRecord()
        {
            if(Row == 1)
            {
                ColumnCount = Column;
                CheckMinimumColumnCount();
            }
            else if(Row > 1)
            {
                CheckColumnCount();
            }
        }

        private void CheckMinimumColumnCount()
        {
            if (Row > 0 && ColumnCount <= 0)
                throw new InvalidOperationException("Column count must be greater than zero.");
        }

        private void CheckColumnCount()
        {
            if (Row > 1 && Column != ColumnCount)
                throw new InvalidOperationException(
                    string.Format("Incorrect number of columns written on row {0}. Expected {1} but found {2}.",
                                  Row, ColumnCount, Column));
        }

        private void CheckMinimumRowCount()
        {
            if(HasHeader && Row <= 0)
                throw new InvalidOperationException(
                    "At least one row must be written if hasHeader = true.");
        }

        private void CheckRowCountBeforeWrite()
        {
            if(Row <= 0)
                throw new InvalidOperationException("StartRecord must be called before WriteValue.");
        }

        public void WriteValue(string value)
        {
            CheckRowCountBeforeWrite();

            ++Column;
            if(Column > 1)
                Writer.Write(",");

            Writer.Write(
                NeedsEscaping(value)
                ? EscapeValue(value)
                : value);
        }

        private static bool NeedsEscaping(string value)
        {
            return value.IndexOfAny(CharsNeedingEscape) > -1;
        }

        private static string EscapeValue(string value)
        {
            return "\"" + value.Replace("\"", "\"\"") + "\"";
        }

        public void WriteDate(DateTime dateTime)
        {
            WriteValue(dateTime.ToString("M/d/yyyy"));
        }

        public string ContentTypeHeader
        {
            get { return HasHeader ? ContentTypeWithHeader : ContentTypeWithoutHeader; }
        }
    }
}