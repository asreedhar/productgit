﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Core.IO
{
    /// <summary>
    /// Subclass the StringWriter class and override the default encoding
    ///
    /// Note: This class was "borrowed" by the class with the same name in the MAX.BuildRequest project in order to break the dependecy on that project.  
    /// If we fix this class, the other class might need fixing too, or even better,  we could just consolidate the classes 
    /// into some common low-lying project that both projects can reference.
    /// dhillis - 2014-10-07
    /// </summary>
    /// <param name="encoding"></param>
    /// <returns></returns>
    public class StringWriterWithEncoding : System.IO.StringWriter
    {
        private readonly Encoding _encoding;

        public StringWriterWithEncoding(Encoding encoding)
        {
            _encoding = encoding;
        }

        public override Encoding Encoding
        {
            get { return _encoding ?? base.Encoding; }
        }
    }
}
