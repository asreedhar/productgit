﻿using System;
using System.IO;

namespace FirstLook.Merchandising.DomainModel.Core.IO
{
    // Inspired by mention of NonClosingStreamWrapper in
    // http://stackoverflow.com/questions/1084813/why-a-binarywriter-closes-the-outer-stream-on-disposal-and-how-to-prevent-that#answer-1084819
    public class NonClosingStreamWrapper : Stream
    {
        private readonly Stream InnerStream;
        private bool Closed;

        public NonClosingStreamWrapper(Stream innerStream)
        {
            if(innerStream == null)
                throw new ArgumentNullException("innerStream");

            InnerStream = innerStream;
        }

        private void CheckClosed()
        {
            if(Closed)
                throw new InvalidOperationException("Stream has been closed.");
        }

        public override void Flush()
        {
            CheckClosed();
            InnerStream.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            CheckClosed();
            return InnerStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            CheckClosed();
            InnerStream.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            CheckClosed();
            return InnerStream.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckClosed();
            InnerStream.Write(buffer, offset, count);
        }

        public override bool CanRead
        {
            get { return !Closed && InnerStream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return !Closed && InnerStream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return !Closed && InnerStream.CanWrite; }
        }

        public override long Length
        {
            get { 
                CheckClosed();
                return InnerStream.Length;
            }
        }

        public override long Position
        {
            get 
            { 
                CheckClosed();
                return InnerStream.Position;
            }
            set
            {
                CheckClosed();
                InnerStream.Position = value;
            }
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
        {
            CheckClosed();
            return InnerStream.BeginRead(buffer, offset, count, callback, state);
        }

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
        {
            CheckClosed();
            return InnerStream.BeginWrite(buffer, offset, count, callback, state);
        }

        public override bool CanTimeout
        {
            get
            {
                return !Closed && InnerStream.CanTimeout;
            }
        }

        public override void Close()
        {
            if(!Closed)
            {
                Flush();
            }
            Closed = true;
        }

        public override int EndRead(IAsyncResult asyncResult)
        {
            CheckClosed();
            return InnerStream.EndRead(asyncResult);
        }

        public override void EndWrite(IAsyncResult asyncResult)
        {
            CheckClosed();
            InnerStream.EndWrite(asyncResult);
        }

        public override int ReadByte()
        {
            CheckClosed();
            return InnerStream.ReadByte();
        }

        public override int ReadTimeout
        {
            get
            {
                CheckClosed();
                return InnerStream.ReadTimeout;
            }
            set
            {
                CheckClosed();
                InnerStream.ReadTimeout = value;
            }
        }

        public override void WriteByte(byte value)
        {
            CheckClosed();
            InnerStream.WriteByte(value);
        }

        public override int WriteTimeout
        {
            get
            {
                CheckClosed();
                return InnerStream.WriteTimeout;
            }
            set
            {
                CheckClosed();
                InnerStream.WriteTimeout = value;
            }
        }

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            throw new NotImplementedException();
        }

        protected override void Dispose(bool disposing)
        {
            Close();
        }

        public override object InitializeLifetimeService()
        {
            throw new NotImplementedException();
        }
    }
}