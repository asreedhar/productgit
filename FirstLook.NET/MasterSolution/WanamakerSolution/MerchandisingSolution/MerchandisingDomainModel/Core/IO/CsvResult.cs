using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FirstLook.Merchandising.DomainModel.Core.IO
{
    public class CsvResult<T> : ActionResult
    {
        public IEnumerable<T> Data { get; private set; }
        private Action<CsvWriter> WriteHeaderRecord { get; set; }
        private Action<CsvWriter, T> WriteRecord { get; set; }

        public CsvResult(IEnumerable<T> data, Action<CsvWriter> writeHeaderRecord = null, Action<CsvWriter, T> writeRecord = null)
        {
            Data = data;
            WriteHeaderRecord = writeHeaderRecord ?? CreateWriteHeaderRecordFunc();
            WriteRecord = writeRecord ?? CreateWriteRecordFunc();
        }

        private static Action<CsvWriter> CreateWriteHeaderRecordFunc()
        {
            var props = ModelMetadataProviders.Current.GetMetadataForProperties(null, typeof (T));
            return writer =>
                       {
                           writer.StartRecord();
                           foreach (var prop in props.OrderBy(p => p.Order))
                               writer.WriteValue(prop.DisplayName ?? prop.PropertyName);
                       };
        }

        private static Action<CsvWriter, T> CreateWriteRecordFunc()
        {
            return (writer, row) =>
                       {
                           writer.StartRecord();
                           var metadata = ModelMetadataProviders.Current.GetMetadataForType(() => row, typeof (T));
                           foreach(var prop in metadata.Properties)
                               WriteDisplayValue(writer, prop);
                       };
        }

        private static void WriteDisplayValue(CsvWriter wr, ModelMetadata prop)
        {
            if(prop.ModelType == typeof(DateTime) && prop.DisplayFormatString == null)
                wr.WriteDate((DateTime)prop.Model);
            else 
                wr.WriteValue(string.Format(prop.DisplayFormatString ?? "{0}", prop.Model));
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            
            using(var writer = new CsvWriter(response.OutputStream))
            {
                response.ContentType = writer.ContentTypeHeader;
                WriteHeaderRecord(writer);
                foreach (var rec in Data)
                    WriteRecord(writer, rec);
            }
        }
    }
}