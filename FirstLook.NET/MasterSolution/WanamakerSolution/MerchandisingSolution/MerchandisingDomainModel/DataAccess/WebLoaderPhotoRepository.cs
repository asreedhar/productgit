﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using FirstLook.Common.Data;
using log4net.Config;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class WebLoaderPhotoRepository : IWebLoaderPhotoRepository
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public WebLoaderPhoto[] OrderPhotos(Guid batch)
        {
            var photos = new List<WebLoaderPhoto>();

            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#Order";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "Batch", batch, DbType.Guid);

                    using (var reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            int inventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));
                            string stockNumber = reader.GetString(reader.GetOrdinal("StockNumber"));
                            string vin = reader.GetString(reader.GetOrdinal("Vin"));
                            Guid handle = reader.GetGuid(reader.GetOrdinal("Handle"));
                            DateTime taken = reader.GetDateTime(reader.GetOrdinal("Taken"));
                            int position = reader.GetInt32(reader.GetOrdinal("Position"));

                            photos.Add(new WebLoaderPhoto(inventoryId, stockNumber, vin, handle, taken, position));
                        }
                    }
                }
            }

            return photos.ToArray();
        }

        public bool Exists(int inventoryId, Guid guid)
        {
            bool exists = false;
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#Exists";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "InventoryID", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(command, "Handle", guid, DbType.Guid);

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException(String.Format("Error occured while attempting to determine if row already exists for Inv id {0}, Handle {1}", inventoryId, guid));

                        exists = reader.GetInt32(reader.GetOrdinal("Exist")) == 0 ? false : true;
                    }
                }
            }

            return exists;
        }

        public bool ExistsForInventory(int inventoryId)
        {
            bool exists = false;
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#ExistsForInventory";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "InventoryID", inventoryId, DbType.Int32);

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException(String.Format("Error occured while attempting to determine if row already exists for Inv id {0}", inventoryId));

                        exists = reader.GetInt32(reader.GetOrdinal("Exist")) == 0 ? false : true;
                    }
                }
            }

            return exists;
        }

        public int GetOffsetForInventory(int inventoryId)
        {
            int offset;
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#Get_Offset_For_Inventory";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddRequiredParameter("InventoryID", inventoryId, DbType.Int32);

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException(String.Format("Error occured while attempting to determine position offset for Inv id {0}", inventoryId));

                        offset = reader.GetInt32(reader.GetOrdinal("Offset"));
                    }
                }
            }

            return offset; 
        }

        public WebLoaderBatchStatus[] FetchBatchStatus(int inventoryId)
        {
            var batchStatus = new List<WebLoaderBatchStatus>();

            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#BatchComplete";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "InventoryId", inventoryId, DbType.Int32);
                    
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Guid batch = reader.GetGuid(reader.GetOrdinal("Batch"));
                            DateTime? batchComplete = Database.GetNullableDateTime(reader, "BatchComplete");
                            DateTime taken = reader.GetDateTime(reader.GetOrdinal("Taken"));
                            bool isComplete = reader.GetBoolean(reader.GetOrdinal("IsComplete"));
                            batchStatus.Add(new WebLoaderBatchStatus(batch, batchComplete, taken, isComplete));
                        }
                    }
                }
            }

            return batchStatus.ToArray();
        }

        public int Insert(int inventoryId, int position, DateTime created, Guid batch, Guid rnd, string userName)
        {
            int batchCount = 0;
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#Insert";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "InventoryID", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(command, "Taken", created, DbType.DateTime);
                    Database.AddRequiredParameter(command, "Batch", batch, DbType.Guid);
                    Database.AddRequiredParameter(command, "Handle", rnd, DbType.Guid);
                    Database.AddRequiredParameter(command, "Position", position, DbType.Int32);
                    Database.AddRequiredParameter(command, "UserName", userName, DbType.String);

                    var outParameter = SimpleQuery.AddOutParameter(command, "BatchCount", false, DbType.Int32);

                    command.ExecuteNonQuery();
                    batchCount = (Int32)outParameter.Value;
                }
            }

            return batchCount;
        }

        public void InsertWithOffset(int inventoryId, int position, DateTime created, Guid batch, Guid rnd, string userName, int offset, string appVersion)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#InsertWithOffset";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "InventoryID", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(command, "Taken", created, DbType.DateTime);
                    Database.AddRequiredParameter(command, "Batch", batch, DbType.Guid);
                    Database.AddRequiredParameter(command, "Handle", rnd, DbType.Guid);
                    Database.AddRequiredParameter(command, "Position", position, DbType.Int32);
                    Database.AddRequiredParameter(command, "UserName", userName, DbType.String);
                    Database.AddRequiredParameter(command, "Offset", offset, DbType.Int32);
                    Database.AddRequiredParameter(command, "Version", appVersion, DbType.String);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void SetError(int inventoryId, Guid guid)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#Error";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "InventoryID", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(command, "Handle", guid, DbType.Guid);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void MarkUploaded(int inventoryId, Guid guid)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {                    
                    command.CommandText = "merchandising.WebLoaderPhotos#Uploaded";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "InventoryID", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(command, "Handle", guid, DbType.Guid);

                    command.ExecuteNonQuery();
                }
            }
        }

        public WebLoaderPhoto GetPhoto(int inventoryId, string username, Guid handle, string taken, int position, out string firstName, out string lastName)
        {
            WebLoaderPhoto photo = null;
            firstName = "";
            lastName = "";
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#FetchVehicleDetails";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddRequiredParameter("InventoryID", inventoryId, DbType.Int32);
                    command.AddRequiredParameter("Login", username, DbType.String);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var stockNumber = reader.GetString(reader.GetOrdinal("StockNumber"));
                            var vin = reader.GetString(reader.GetOrdinal("Vin"));
                            firstName = reader.GetString(reader.GetOrdinal("FirstName"));
                            lastName = reader.GetString(reader.GetOrdinal("LastName"));
                            DateTime tTime = DateTime.ParseExact(taken, "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
                            photo = new WebLoaderPhoto(inventoryId, stockNumber, vin, handle, tTime.ToLocalTime(), position);
                        }
                    }
                }
            }

            return photo;
        }

        public void UpdatePositionOffset(Guid batchId, int positionOffset)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#UpdatePositionOffset";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "Batch", batchId, DbType.Guid);
                    Database.AddRequiredParameter(command, "Offset", positionOffset, DbType.Int32);

                    command.ExecuteNonQuery();
                }
            } 
        }

        public int GetPositionOffset(Guid batchId)
        {
            int positionOffset;
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "merchandising.WebLoaderPhotos#FetchPositionOffset";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "Batch", batchId, DbType.Guid);

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                           Log.DebugFormat("Error occurred while attempting to retrieve position offset for batch {0}.  Using zero default.", batchId);
                            positionOffset = 0;
                        }
                        else
                            positionOffset = reader.GetInt32("PositionOffset");
                    }
                }
            }

            return positionOffset;
        }
    }
}
