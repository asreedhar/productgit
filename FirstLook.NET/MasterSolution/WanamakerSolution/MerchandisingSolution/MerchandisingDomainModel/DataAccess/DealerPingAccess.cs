﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public class DealerPingAccess 
    {
        public static bool CheckDealerPingAccess(int businessUnitId)
        {
            using (var con = Database.GetOpenConnection(Database.IMTDatabase))
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = "dbo.DealerInfoFetch";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);

                using (var reader = cmd.ExecuteReader())
                {
                    int isNewPing = 0;
                    if (reader.Read()) //if it has a record...return it
                    {
                         isNewPing =  reader.GetInt32("IsNewPing");
                    }
                    return isNewPing > 0 ? true : false;
                }
            }
        }

        
    }
}
