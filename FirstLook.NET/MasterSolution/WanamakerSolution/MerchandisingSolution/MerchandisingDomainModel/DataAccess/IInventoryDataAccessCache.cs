namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public interface IInventoryDataAccessCache : IInventoryDataAccess
    {
        void Clear(int businessUnitID, int inventoryId);
    }
}