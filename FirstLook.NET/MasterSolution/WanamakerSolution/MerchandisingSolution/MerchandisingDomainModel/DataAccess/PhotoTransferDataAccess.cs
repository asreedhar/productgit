﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class PhotoTransferDataAccess : IPhotoTransferDataAccess
    {
        public IList<PhotoTransferData> FetchNewTransfers()
        {
            var records = new List<PhotoTransferData>();

            using (var connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "dbo.photoTransfers#selectNew";
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var r = new PhotoTransferData();

                            r.Vin = reader.GetString(reader.GetOrdinal(("Vin")));
                            r.SourceBusinessUnitId = reader.GetInt32(reader.GetOrdinal("srcBusinessUnitId"));
                            r.SourceBusinessUnitCode = reader.GetString(reader.GetOrdinal("srcBusinessUnitCode"));
                            r.SourceInventoryId = reader.GetInt32(reader.GetOrdinal("srcInventoryId"));

                            r.DestinationBusinessUnitId = reader.GetInt32(reader.GetOrdinal("dstBusinessUnitId"));
                            r.DestinationBusinessUnitCode = reader.GetString(reader.GetOrdinal("dstBusinessUnitCode"));
                            r.DestinationInventoryId = reader.GetInt32(reader.GetOrdinal("dstInventoryId"));

                            records.Add(r);                                                    
                        }
                    }
                }
            }

            return records;
        }

        /// <summary>
        /// Add a photo for the destination inventory
        /// </summary>
        /// <param name="dstInventoryId"></param>
        /// <param name="photoUrl"></param>
        /// <param name="photoSequenceNumber"></param>
        /// <param name="photoIsPrimary"></param>
        public void TransferPhoto(int dstInventoryId, string photoUrl, int photoSequenceNumber, bool photoIsPrimary)
        {
            using (var connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "dbo.photoTransfers#transferPhoto";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("destinationInventoryId", dstInventoryId, DbType.Int32);
                    command.AddRequiredParameter("photoUrl", photoUrl, DbType.String);
                    command.AddRequiredParameter("photoSequenceNumber", photoSequenceNumber, DbType.Int32);
                    command.AddRequiredParameter("photoIsPrimary", photoIsPrimary ? 1 : 0, DbType.Boolean); // TODO: DBType is bit... not sure if this is going to work

                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Insert record into PhotoTransfers table for tracking purposes
        /// </summary>
        /// <param name="srcInventoryId"></param>
        /// <param name="dstInventoryId"></param>
        public void TrackTransfer(int srcInventoryId, int dstInventoryId)
        {
            using (var connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "dbo.photoTransfers#insert";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("srcInventoryId", srcInventoryId, DbType.Int32);
                    command.AddRequiredParameter("dstInventoryId", dstInventoryId, DbType.Int32);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateTransferStatus(int dstInventoryId, TransferStatusCode code)
        {
            using (var connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "dbo.photoTransfers#updateStatus";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("dstInventoryId", dstInventoryId, DbType.Int32);
                    command.AddRequiredParameter("statusCode", (int) code, DbType.Int32);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
