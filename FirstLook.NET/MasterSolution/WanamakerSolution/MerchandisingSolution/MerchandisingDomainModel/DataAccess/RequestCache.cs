﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal static class RequestCache
    {
        public static bool Contains<TValue>(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Items.Contains(key) && HttpContext.Current.Items[key] is TValue)
                    return true;
            
            return false;
        }
        
        public static TValue Fetch<TValue>(string key) where TValue : class 
        {
            if (HttpContext.Current != null && HttpContext.Current.Items.Contains(key))
                    return HttpContext.Current.Items[key] as TValue;

            return null;
        }

        public static void Save<TValue>(string key, TValue value)
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Items.Contains(key))
                    HttpContext.Current.Items[key] = value;
                else
                    HttpContext.Current.Items.Add(key, value);
            }
        }

        public static void Remove(string key)
        {
            if (HttpContext.Current != null)
                HttpContext.Current.Items.Remove(key);
        }
    }
}
