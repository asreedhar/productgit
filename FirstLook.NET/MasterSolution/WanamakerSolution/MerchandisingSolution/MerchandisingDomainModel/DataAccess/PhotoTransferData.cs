﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public class PhotoTransferData
    {
        public string Vin { get; set; }

        public int SourceBusinessUnitId { get; set; }
        public string SourceBusinessUnitCode { get; set; }
        public int SourceInventoryId { get; set; }

        public int DestinationBusinessUnitId { get; set; }
        public string DestinationBusinessUnitCode { get; set; }
        public int DestinationInventoryId { get; set; }
    }
}
