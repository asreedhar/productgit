﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public class MappedThirdPartyOptionTransfer
    {
        public GenericEquipmentCollection Equipment { get; set; }
    }
}
