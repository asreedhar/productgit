﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document;
using log4net;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class VehicleConfigurationDataAccess : IVehicleConfigurationDataAccess
    {
        private readonly IAdMessageSender _messageSender;
        private IChromeMapper ChromeStyleMapper { get; set; }

        private static readonly ILog Log = LogManager.GetLogger(typeof(VehicleConfigurationDataAccess).FullName);

        public VehicleConfigurationDataAccess(IChromeMapper chromeStyleMapper, IAdMessageSender messageSender)
        {
            _messageSender = messageSender;
            ChromeStyleMapper = chromeStyleMapper;
        }

        public VehicleDataTransfer Fetch(int businessUnitId, int inventoryId, string memberLogin)
        {
            VehicleDataTransfer dataTransfer = new VehicleDataTransfer();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbTransaction trans = con.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "builder.getOrCreateDetailedVehicleConfiguration";
                        cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);

                        try
                        {
                            using (IDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    var vinField = reader.GetOrdinal("VIN");
                                    var vehicleCatalogField = reader.GetOrdinal("VehicleCatalogID");
                                    var chromeStyleField = reader.GetOrdinal("ChromeStyleID");

                                    var vin = reader.IsDBNull(vinField) ? null : reader.GetString(vinField);
                                    var vehicleCatalogId = reader.IsDBNull(vehicleCatalogField)
                                                               ? (int?) null
                                                               : reader.GetInt32(vehicleCatalogField);
                                    var chromeStyleId = reader.IsDBNull(chromeStyleField)
                                                            ? (int?) null
                                                            : reader.GetInt32(chromeStyleField);

                                    var styleID = (chromeStyleId == -1 ? null : chromeStyleId)
                                                  ?? ChromeStyleMapper.LookupChromeStyleIdByVehicleCatalogId(vehicleCatalogId)
                                                  ?? ChromeStyleMapper.LookupChromeStyleIdByVin(vin)
                                                  ?? -1;

                                    int special = -1;
                                    if (reader["specialID"] != DBNull.Value)
                                    {
                                        special = reader.GetInt32(reader.GetOrdinal("specialID"));
                                    }

                                    object tt = reader.GetValue(reader.GetOrdinal("tireTread"));
                                    if (tt != DBNull.Value)
                                    {
                                        dataTransfer.TireTread = (int) tt;
                                    }

                                    dataTransfer.ChromeStyleID = styleID;
                                    dataTransfer.OriginalChromeStyleID = chromeStyleId ?? -1;

                                    dataTransfer.SpecialID = special;

                                    dataTransfer.InteriorColor = reader.GetString(reader.GetOrdinal("intColor"));
                                    dataTransfer.ExteriorColor1 = reader.GetString(reader.GetOrdinal("extColor1"));
                                    dataTransfer.ExteriorColor2 = reader.GetString(reader.GetOrdinal("extColor2"));
                                    dataTransfer.VehicleCondition = reader.GetString(reader.GetOrdinal("vehicleCondition"));

                                    dataTransfer.ExteriorColorCode = reader.IsDBNull(reader.GetOrdinal("exteriorColorCode")) ? "" : reader.GetString(reader.GetOrdinal("exteriorColorCode"));
                                    dataTransfer.ExteriorColorCode2 = reader.IsDBNull(reader.GetOrdinal("exteriorColorCode2")) ? "" : reader.GetString(reader.GetOrdinal("exteriorColorCode2"));
                                    dataTransfer.InteriorColorCode = reader.IsDBNull(reader.GetOrdinal("interiorColorCode")) ? "" : reader.GetString(reader.GetOrdinal("interiorColorCode"));

                                    string afterMktString = reader.GetString(reader.GetOrdinal("afterMarketCategoryString")).Trim(" ".ToCharArray());

                                    dataTransfer.AfterMarketEquipment = String.IsNullOrEmpty(afterMktString) ?
                                        new List<string>() :
                                        new List<string>(afterMktString.Split(",".ToCharArray()));
                                    
                                    dataTransfer.AfterMarketTrim = reader.GetString(reader.GetOrdinal("afterMarketTrim"));

                                    dataTransfer.InventoryId = reader.GetInt32(reader.GetOrdinal("inventoryId"));
                                    dataTransfer.BusinessUnitid = businessUnitId;
                                    dataTransfer.ReconditioningText = reader.GetString(reader.GetOrdinal("reconditioningText"));
                                    dataTransfer.ReconditioningValue = reader.GetDecimal(reader.GetOrdinal("reconditioningValue"));
                                    dataTransfer.LotDataImportLock = reader.GetBoolean(reader.GetOrdinal("lotDataImportLock"));

                                    dataTransfer.Version = new byte[8];
                                    reader.GetBytes(reader.GetOrdinal("version"), 0, dataTransfer.Version, 0, 8);


                                    dataTransfer.IsDealerMaintained = reader.GetBoolean(reader.GetOrdinal("isDealerMaintained"));
                                    dataTransfer.IsFullyDetailed = reader.GetBoolean(reader.GetOrdinal("isFullyDetailed"));
                                    dataTransfer.HasNoPanelScratches = reader.GetBoolean(reader.GetOrdinal("hasNoPanelScratches"));
                                    dataTransfer.HasNoVisibleDents = reader.GetBoolean(reader.GetOrdinal("hasNoVisibleDents"));
                                    dataTransfer.HasNoVisibleRust = reader.GetBoolean(reader.GetOrdinal("hasNoVisibleRust"));
                                    dataTransfer.HasNoKnownAccidents = reader.GetBoolean(reader.GetOrdinal("hasNoKnownAccidents"));
                                    dataTransfer.HasNoKnownBodyWork = reader.GetBoolean(reader.GetOrdinal("hasNoKnownBodyWork"));
                                    dataTransfer.HasPassedDealerInspection = reader.GetBoolean(reader.GetOrdinal("hasPassedDealerInspection"));
                                    dataTransfer.HaveAllKeys = reader.GetBoolean(reader.GetOrdinal("haveAllKeys"));
                                    dataTransfer.NoKnownMechanicalProblems = reader.GetBoolean(reader.GetOrdinal("noKnownMechanicalProblems"));
                                    dataTransfer.HadAllRequiredScheduledMaintenancePerformed = reader.GetBoolean(reader.GetOrdinal("hadAllRequiredScheduledMaintenancePerformed"));
                                    dataTransfer.HaveServiceRecords = reader.GetBoolean(reader.GetOrdinal("haveServiceRecords"));
                                    dataTransfer.HaveOriginalManuals = reader.GetBoolean(reader.GetOrdinal("haveOriginalManuals"));
                                }

                                dataTransfer.Options = new DetailedEquipmentCollection();
                                dataTransfer.Generics = new GenericEquipmentCollection();
                                dataTransfer.Replacements = new GenericEquipmentReplacementCollection();

                                if (reader.NextResult())
                                {
                                    dataTransfer.DealerAdvertisementPreferences = DealerAdvertisementPreferences.Fetch(businessUnitId);

                                    dataTransfer.Generics = new GenericEquipmentCollection(reader);

                                    reader.NextResult();
                                    dataTransfer.Options = new DetailedEquipmentCollection(reader, dataTransfer.Generics);
                                    dataTransfer.Replacements = GenericEquipmentReplacementCollection.Fetch(businessUnitId, dataTransfer.Generics);
                                }

                                dataTransfer.Conditions = new VehicleConditionCollection();
                                if (reader.NextResult())
                                {
                                    dataTransfer.Conditions = new VehicleConditionCollection(reader);
                                }

                                dataTransfer.Notes = reader.NextResult() 
                                    ? new LoaderNoteCollection(reader) 
                                    : new LoaderNoteCollection();

                                dataTransfer.KeyInformationList = new List<KeyInformation>();
                                if (reader.NextResult())
                                {
                                    Log.Debug("Loading key information from reader.");
                                    while (reader.Read())
                                    {
                                        dataTransfer.KeyInformationList.Add(new KeyInformation(reader));
                                    }
                                }

                            }

                        }
                        catch (Exception e)
                        {
                            var ex = new InventoryException(e.Message, inventoryId);
                            Log.Error("Exception encountered while loading.", ex);
                            throw ex;
                        }

                    }
                    trans.Commit();
                   
                }
            }

            CheckAndSendSetStyleMessage(businessUnitId, inventoryId, memberLogin, dataTransfer);
            return dataTransfer;
        }

        private void CheckAndSendSetStyleMessage(int businessUnitId, int inventoryId, string memberLogin, VehicleDataTransfer dataTransfer)
        {
            // if the original chromestyleid does not matche the current chrome style id, the trim was changed by this process
            if (dataTransfer.OriginalChromeStyleID != dataTransfer.ChromeStyleID) {
                //save here before we send the set style on the bus
                SaveChromeStyle(businessUnitId, inventoryId, dataTransfer.ChromeStyleID);
                _messageSender.SendStyleSet(businessUnitId, inventoryId, memberLogin, GetType().FullName);
            }
        }

        private void SaveNotes(VehicleDataTransfer transfer, string memberLogin)
        {
            transfer.Notes.Save(transfer.BusinessUnitid, transfer.InventoryId, memberLogin);
        }

        private void SaveVehicleConditions(VehicleDataTransfer transfer)
        {
            transfer.Conditions.Save(transfer.BusinessUnitid, transfer.InventoryId);
        }

        private void SaveEquipment(VehicleDataTransfer transfer)
        {
            transfer.Generics.Save(transfer.BusinessUnitid, transfer.InventoryId);
            transfer.Options.Save(transfer.BusinessUnitid, transfer.InventoryId);
        }

        private void SaveKeyInformation(VehicleDataTransfer transfer)
        {
            KeyInformation.DeleteAll(transfer.BusinessUnitid, transfer.InventoryId);
            if (transfer.KeyInformationList.Count > 0)
            {
                using (IDbConnection con = Database.GetConnection(Database.MerchandisingDatabase))
                {
                    foreach (KeyInformation item in transfer.KeyInformationList)
                    {
                        item.Save(transfer.BusinessUnitid, transfer.InventoryId, con);
                    }
                }
            }
        }

        private void SaveBaseData(VehicleDataTransfer transfer, string memberLogin)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandText = "builder.VehicleConfiguration#Save";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", transfer.BusinessUnitid, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryID", transfer.InventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "chromeStyleID", transfer.ChromeStyleID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "extColor1", transfer.ExteriorColor1, DbType.String);
                    Database.AddRequiredParameter(cmd, "extColor2", transfer.ExteriorColor2, DbType.String);
                    Database.AddRequiredParameter(cmd, "intColor", transfer.InteriorColor, DbType.String);
                    Database.AddRequiredParameter(cmd, "AfterMarketTrim", transfer.AfterMarketTrim, DbType.String);
                    Database.AddRequiredParameter(cmd, "Condition", transfer.VehicleCondition, DbType.String);
                    Database.AddRequiredParameter(cmd, "AdditionalInfoText", string.Empty, DbType.String);

                    Database.AddRequiredParameter(cmd, "ExteriorColorCode", transfer.ExteriorColorCode, DbType.String);
                    Database.AddRequiredParameter(cmd, "ExteriorColorCode2", transfer.ExteriorColorCode2, DbType.String);
                    Database.AddRequiredParameter(cmd, "InteriorColorCode", transfer.InteriorColorCode, DbType.String);

                    Database.AddRequiredParameter(cmd, "SpecialID", transfer.SpecialID, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "TireTread", transfer.TireTread, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "ReconditioningText", transfer.ReconditioningText, DbType.String);
                    Database.AddRequiredParameter(cmd, "ReconditioningValue", transfer.ReconditioningValue, DbType.Decimal);
                    //Database.AddWithValue(cmd, "CertificationTypeId", certificationTypeId, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);
                    Database.AddRequiredParameter(cmd, "Version", transfer.Version, DbType.Binary);

                    string aftMktList = "";
                    foreach (string eq in transfer.AfterMarketEquipment)
                    {
                        aftMktList += eq + ",";
                    }
                    aftMktList = aftMktList.Trim(",".ToCharArray());

                    Database.AddRequiredParameter(cmd, "afterMarketCodeList", aftMktList, DbType.String);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void SaveConditionChecklist(VehicleDataTransfer transfer)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "builder.ConditionChecklist#Save";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", transfer.BusinessUnitid, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryID", transfer.InventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "isDealerMaintained", transfer.IsDealerMaintained, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "isFullyDetailed", transfer.IsFullyDetailed, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoPanelScratches", transfer.HasNoPanelScratches, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoVisibleDents", transfer.HasNoVisibleDents, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoVisibleRust", transfer.HasNoVisibleRust, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoKnownAccidents", transfer.HasNoKnownAccidents, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoKnownBodyWork", transfer.HasNoKnownBodyWork, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasPassedDealerInspection", transfer.HasPassedDealerInspection, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "haveAllKeys", transfer.HaveAllKeys, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "noKnownMechanicalProblems", transfer.NoKnownMechanicalProblems, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hadAllRequiredScheduledMaintenancePerformed", transfer.HadAllRequiredScheduledMaintenancePerformed, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "haveServiceRecords", transfer.HaveServiceRecords, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "haveOriginalManuals", transfer.HaveOriginalManuals, DbType.Boolean);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Save(VehicleDataTransfer transfer, string memberLogin)
        {
            SaveBaseData(transfer, memberLogin);
            SaveConditionChecklist(transfer);
            SaveEquipment(transfer);
            SaveVehicleConditions(transfer);
            SaveNotes(transfer, memberLogin);
            SaveKeyInformation(transfer);
            UpdateTransferVersion(transfer);
        }

        public void SaveChromeStyle(int businessUnitId, int inventoryId, int chromeStyleId)
        {
            using (var con = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.updateVehicleStyleID";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);
                cmd.AddRequiredParameter("ChromeStyleID", chromeStyleId, DbType.Int32);

                cmd.ExecuteNonQuery();
            }
        }

        public void SaveGenerics(GenericEquipmentCollection generics, int businessUnitId, int inventoryId)
        {
            //save all (changed to call more efficient save at gec level) FB: 30042, tureen 6/19/2014
            generics.Save(businessUnitId, inventoryId);
        }

        private static void UpdateTransferVersion(VehicleDataTransfer transfer)
        {
            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                cmd.CommandText =
                    @"
                        SELECT version FROM builder.OptionsConfiguration 
                        WHERE BusinessUnitID = @bu AND InventoryId = @inv";

                cmd.AddRequiredParameter("@bu", transfer.BusinessUnitid, DbType.Int32);
                cmd.AddRequiredParameter("@inv", transfer.InventoryId, DbType.Int32);
                
                transfer.Version = (byte[])cmd.ExecuteScalar();
            }
        }
    }
}
