﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public enum TransferStatusCode
    {
        Queued,
        Pending,                // processing has started
        Success,                // one or more photos transferred
        NoSourcePhotos,         // AulTec had no photos of source inventory
        DestinationHasPhotos,   // Destination vehicle already has photos
        GeneralFailure,         // generic failure, something bad happened and we've stopped retrying.
        UnknownBusinessUnitId   // Source or destination business unit is invalid
    }

    public interface IPhotoTransferDataAccess
    {

        /// <summary>
        /// Returns info for vehicles that have been moved from one dealership (source) to another related dealership (destination)
        /// </summary>
        IList<PhotoTransferData> FetchNewTransfers();

        // Add photo to destination inventory
        void TransferPhoto(int dstInventoryId, string photoUrl, int photoSequenceNumber, bool photoIsPrimary);

        /// <summary>
        /// Flags the transfer of a vehicle as processed so that is is not returned in the next call to FetchNewTransfers()
        /// </summary>
        void UpdateTransferStatus(int dstInventoryId, TransferStatusCode code);

        /// <summary>
        /// Adds record to PhotoTransfers table for tracking. Prevents us from trying to process the transfer again and 
        /// provides auditing info.
        /// </summary>
        void TrackTransfer(int srcInventoryId, int dstInventoryId);

    }
}
