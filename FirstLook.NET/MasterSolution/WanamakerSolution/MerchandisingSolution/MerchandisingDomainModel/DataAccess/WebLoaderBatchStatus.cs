﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public class WebLoaderBatchStatus
    {
        public WebLoaderBatchStatus(Guid batch, DateTime? batchComplete, DateTime taken, bool isComplete)
        {
            Batch = batch;
            BatchComplete = batchComplete;
            Taken = taken;
            IsComplete = isComplete;
        }

        public Guid Batch { get; private set; }
        public DateTime? BatchComplete { get; private set; }
        public DateTime Taken { get; private set; }
        public bool IsComplete { get; private set; }
    }
}
