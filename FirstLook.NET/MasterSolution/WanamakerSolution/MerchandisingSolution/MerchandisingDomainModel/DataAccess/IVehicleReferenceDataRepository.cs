﻿using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public interface IVehicleReferenceDataRepository
    {
        void Save(string vin, ReferenceDataType dataType, string dataSource, string value);
    }

    class VehicleReferenceDataRepository : IVehicleReferenceDataRepository
    {
        public void Save(string vin, ReferenceDataType dataType, string dataSource, string value)
        {
            using (var sqlConnection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                sqlConnection.Open();
                const string sql = @"[builder].[VehicleReferenceData#Upsert] @Vin, @DataTypeId, @DataSource, @Value";

                sqlConnection.Execute(sql, new {Vin = vin, DataTypeId = (int) dataType, DataSource = dataSource, Value = value});
            }
        }
    }

    /// <summary>
    /// Keep this in sync with the table Merchandising.builder.ReferenceDataTypes 
    /// in order to maintain referential integrity
    /// </summary>
    public enum ReferenceDataType
    {
        OriginalMsrp = 1
    }
}