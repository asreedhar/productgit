﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public class WebLoaderPhoto
    {
        public WebLoaderPhoto(int inventoryId, string stockNumber, string vin, Guid handle, DateTime taken, int position)
        {
            InventoryId = inventoryId;
            StockNumber = stockNumber;
            Vin = vin;
            Taken = taken;
            Handle = handle;
            Position = position;
        }

        public int InventoryId { get; private set; }
        public string StockNumber { get; private set; }
        public string Vin { get; private set; }
        public int Position { get; private set; }
        public DateTime Taken { get; private set; }
        public Guid Handle { get; private set; }
    }
}
