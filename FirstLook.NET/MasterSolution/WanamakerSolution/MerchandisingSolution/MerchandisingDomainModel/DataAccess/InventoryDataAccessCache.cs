﻿using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class InventoryDataAccessCache : IInventoryDataAccessCache
    {
        private const string PreKey = "InventoryDataAccessCache";

        private readonly IInventoryDataAccess _impl;

        public InventoryDataAccessCache(IInventoryDataAccess impl)
        {
            _impl = impl;
        }

        public InventoryData Fetch(int businessUnitId, int inventoryId)
        {
            string key = PreKey + businessUnitId + inventoryId;
            if (RequestCache.Contains<InventoryData>(key))
                return RequestCache.Fetch<InventoryData>(key);

            var data  = _impl.Fetch(businessUnitId, inventoryId);
            RequestCache.Save(key, data);
            return data;
        }

        public void Clear(int businessUnitId, int inventoryId)
        {
            var key = PreKey + businessUnitId + inventoryId;
            if (RequestCache.Contains<InventoryData>(key))
            {
                RequestCache.Remove(key);
            }
        }
    }
}
