﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class MappedThirdPartyOptionCache : IMappedThirdPartyOptionDataAccess
    {
        private const string PreKey = "MappedThirdPartyOptionCache";

        private IMappedThirdPartyOptionDataAccess _impl;
        
        public MappedThirdPartyOptionCache(IMappedThirdPartyOptionDataAccess impl)
        {
            _impl = impl;
        }

        public MappedThirdPartyOptionTransfer Fetch(int businessUnitId, int inventoryId)
        {
            string key = PreKey + businessUnitId + inventoryId;
            if (RequestCache.Contains<MappedThirdPartyOptionTransfer>(key))
                return RequestCache.Fetch<MappedThirdPartyOptionTransfer>(key);

            var transfer = _impl.Fetch(businessUnitId, inventoryId);
            RequestCache.Save(key, transfer);
            return transfer;
        }

    }
}
