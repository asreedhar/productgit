﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class MappedThirdPartyOptionDataAccess : IMappedThirdPartyOptionDataAccess
    {
        public MappedThirdPartyOptionTransfer Fetch(int businessUnitId, int inventoryId)
        {
            MappedThirdPartyOptionTransfer transfer = new MappedThirdPartyOptionTransfer();
            transfer.Equipment = new GenericEquipmentCollection();
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.vehicleOptionsMapped#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        transfer.Equipment = new GenericEquipmentCollection(reader);
                    }
                }
            }

            return transfer;
        }

    }
}
