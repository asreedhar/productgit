﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class VehicleConfigurationCache : IVehicleConfigurationDataAccess
    {
        private const string PreKey = "VehicleConfigurationCache";
        
        private IVehicleConfigurationDataAccess _impl;

        public VehicleConfigurationCache(IVehicleConfigurationDataAccess impl)
        {
            _impl = impl;
        }

        private string GetKey(int businessUnitId, int inventoryId)
        {
            return PreKey + businessUnitId + inventoryId;
        }

        public VehicleDataTransfer Fetch(int businessUnitId, int inventoryId, string memberLogin)
        {
            string key = GetKey(businessUnitId, inventoryId);
            if (RequestCache.Contains<VehicleDataTransfer>(key))
                return RequestCache.Fetch<VehicleDataTransfer>(key);

            var transfer = _impl.Fetch(businessUnitId, inventoryId, memberLogin);
            RequestCache.Save(key, transfer);
            return transfer;
        }

        public void Save(VehicleDataTransfer transfer, string memberLogin)
        {
            _impl.Save(transfer, memberLogin);

            string key = GetKey(transfer.BusinessUnitid, transfer.InventoryId);
            RequestCache.Save(key, transfer);
        }

        public void SaveChromeStyle(int businessUnitId, int inventoryId, int chromeStyleId)
        {
            _impl.SaveChromeStyle(businessUnitId, inventoryId, chromeStyleId);

            string key = GetKey(businessUnitId, inventoryId);
            if (RequestCache.Contains<VehicleDataTransfer>(key))
            {
                var transfer = RequestCache.Fetch<VehicleDataTransfer>(key);
                transfer.ChromeStyleID = chromeStyleId;
            }
        }

        public void SaveGenerics(GenericEquipmentCollection generics, int businessUnitId, int inventoryId)
        {
            _impl.SaveGenerics(generics, businessUnitId, inventoryId);

            string key = GetKey(businessUnitId, inventoryId);
            if (RequestCache.Contains<VehicleDataTransfer>(key))
            {
                var transfer = RequestCache.Fetch<VehicleDataTransfer>(key);
                transfer.Generics = generics;
            }
        }

    }
}
