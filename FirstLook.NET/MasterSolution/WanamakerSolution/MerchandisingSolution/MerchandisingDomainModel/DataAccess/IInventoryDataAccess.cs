﻿using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public interface IInventoryDataAccess
    {
        InventoryData Fetch(int businessUnitId, int inventoryId);
    }
}
