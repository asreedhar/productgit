﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public interface IVehicleConfigurationDataAccess
    {
        VehicleDataTransfer Fetch(int businessUnitId, int inventoryId, string memberLogin);
        void Save(VehicleDataTransfer transfer, string memberLogin);

        void SaveChromeStyle(int businessUnitId, int inventoryId, int chromeStyleId);
        void SaveGenerics(GenericEquipmentCollection generics, int businessUnitId, int inventoryId);
    }
}
