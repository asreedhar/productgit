﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public interface IWebLoaderPhotoRepository
    {
        bool Exists(int inventoryId, Guid guid);
        bool ExistsForInventory(int inventoryId);
        int GetOffsetForInventory(int inventoryId);
        int Insert(int inventoryId, int position, DateTime created, Guid batch, Guid rnd, string userName);
        void InsertWithOffset(int inventoryId, int position, DateTime created, Guid batch, Guid rnd, string userName, int offset, string appVersion);
        WebLoaderBatchStatus[] FetchBatchStatus(int inventoryId);
        WebLoaderPhoto[] OrderPhotos(Guid batch);
        void MarkUploaded(int inventoryId, Guid guid);
        void SetError(int inventoryId, Guid guid);
        WebLoaderPhoto GetPhoto(int inventoryId, string username, Guid handle, string taken, int position, out string firstName, out string lastName);
        void UpdatePositionOffset(Guid batchId, int positionOffset);
        int GetPositionOffset(Guid batchId);
    }
}
