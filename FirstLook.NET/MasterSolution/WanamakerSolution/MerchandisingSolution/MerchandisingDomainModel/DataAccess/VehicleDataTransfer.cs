﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public class VehicleDataTransfer
    {
        public int? TireTread { get; set; }
        public int ChromeStyleID { get; set; }
        public int OriginalChromeStyleID { get; set; }
        public int SpecialID { get; set; }
        public string InteriorColor { get; set; }
        public string ExteriorColor1 { get; set; }
        public string ExteriorColor2 { get; set; }
        public string VehicleCondition { get; set; }
        public List<string> AfterMarketEquipment { get; set; }
        public string AfterMarketTrim { get; set; }
        public int InventoryId { get; set; }
        public int BusinessUnitid { get; set; }
        public string ReconditioningText { get; set; }
        public decimal ReconditioningValue { get; set; }
        public bool LotDataImportLock { get; set; }
        public byte[] Version { get; set; }


        public bool IsDealerMaintained { get; set; }
        public bool IsFullyDetailed { get; set; }
        public bool HasNoPanelScratches { get; set; }
        public bool HasNoVisibleDents { get; set; }
        public bool HasNoVisibleRust { get; set; }
        public bool HasNoKnownAccidents { get; set; }
        public bool HasNoKnownBodyWork { get; set; }
        public bool HasPassedDealerInspection { get; set; }
        public bool HaveAllKeys { get; set; }
        public bool NoKnownMechanicalProblems { get; set; }
        public bool HadAllRequiredScheduledMaintenancePerformed { get; set; }
        public bool HaveServiceRecords { get; set; }
        public bool HaveOriginalManuals { get; set; }

        public DealerAdvertisementPreferences DealerAdvertisementPreferences { get; set; }
        public DetailedEquipmentCollection Options { get; set; }
        public GenericEquipmentCollection Generics { get; set; }
        public GenericEquipmentReplacementCollection Replacements { get; set; }

        public VehicleConditionCollection Conditions { get; set; }

        public LoaderNoteCollection Notes { get; set; }

        public List<KeyInformation> KeyInformationList { get; set; }

        public string InteriorColorCode { get; set; }
        public string ExteriorColorCode { get; set; }
        public string ExteriorColorCode2 { get; set; }
    }
}
