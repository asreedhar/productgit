﻿using System.Data;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    internal class InventoryDataAccess : IInventoryDataAccess
    {
        public InventoryData Fetch(int businessUnitId, int inventoryId)
        {
            using (var con = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.getInventoryItem";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read()) //if it has a record...return it
                    {
                        var inventoryReader = new InventoryDataReader(reader, businessUnitId);
                        return inventoryReader.ReadRecord();
                    }

                    throw new InventoryException(
                        "Inventory Item not found. This commonly happens when using multiple dealerships.", inventoryId);
                }
            }
        }
    }
}
