﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.DataAccess
{
    public interface IMappedThirdPartyOptionDataAccess
    {
        MappedThirdPartyOptionTransfer Fetch(int businessUnitId, int inventoryId);
    }
}
