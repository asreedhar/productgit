﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class TemplateFilter
    {
       private ITemplateDataRepository _repository;

        public TemplateFilter()
        {
            _repository = new TemplateDataRepository();
        }

        public TemplateFilter(ITemplateDataRepository repository)
        {
            _repository = repository;
        }

        public Dictionary<int, string> SelectTemplates(int businessUnitId, IInventoryData inventoryData)
        {
            using (MiniProfiler.Current.Step("TemplateFilter.SelectTemplates"))
            {
                var data = _repository.GetTemplateDataForBusinessUnit(businessUnitId).ToList();

                var filteredTemplates = data.Where(t => t.VehicleProfile == null || t.VehicleProfile.IsMatch(inventoryData));
                return filteredTemplates.ToDictionary(t => t.Id, t => t.Name);
            }
        }
    }

}