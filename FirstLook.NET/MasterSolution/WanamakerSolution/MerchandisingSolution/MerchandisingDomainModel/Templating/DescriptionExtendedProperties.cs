using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class DescriptionExtendedProperties
    {
        public DescriptionExtendedProperties(IEnumerable<MerchandisingDescriptionItemType> contents)
            : this()
        {
            foreach (MerchandisingDescriptionItemType itemType in contents)
            {
                AddMappedDescriptionItem(itemType);
            }
        }

        public DescriptionExtendedProperties()
        {
            AvailableCarfax = false;
            AvailableCertified = false;
            AvailableEdmundsTrueMarketValue = false;
            AvailableKelleyBlueBook = false;
            AvailableNada = false;
            AvailableTagline = false;
            AvailableHighValueOptions = false;
            AvailableStandardOptions = false;
            IncludedCarfax = false;
            IncludedCertified = false;
            IncludedEdmundsTrueMarketValue = false;
            IncludedKelleyBlueBook = false;
            IncludedNada = false;
            IncludedTagline = false;
            IncludedHighValueOptions = false;
            IncludedStandardOptions = false;
        }

        public DescriptionExtendedProperties(bool availableCarfax, bool availableCertified,
                                             bool availableEdmundsTrueMarketValue, bool availableKelleyBlueBook,
                                             bool availableNada, bool availableTagline, bool availableHighValueOptions,
                                             bool availableStandardOptions, bool includedCarfax, bool includedCertified,
                                             bool includedEdmundsTrueMarketValue, bool includedKelleyBlueBook,
                                             bool includedNada, bool includedTagline, bool includedHighValueOptions,
                                             bool includedStandardOptions) : this()
        {
            AvailableCarfax = availableCarfax;
            AvailableCertified = availableCertified;
            AvailableEdmundsTrueMarketValue = availableEdmundsTrueMarketValue;
            AvailableKelleyBlueBook = availableKelleyBlueBook;
            AvailableNada = availableNada;
            AvailableTagline = availableTagline;
            AvailableHighValueOptions = availableHighValueOptions;
            AvailableStandardOptions = availableStandardOptions;
            IncludedCarfax = includedCarfax;
            IncludedCertified = includedCertified;
            IncludedEdmundsTrueMarketValue = includedEdmundsTrueMarketValue;
            IncludedKelleyBlueBook = includedKelleyBlueBook;
            IncludedNada = includedNada;
            IncludedTagline = includedTagline;
            IncludedHighValueOptions = includedHighValueOptions;
            IncludedStandardOptions = includedStandardOptions;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="other"></param>
        internal DescriptionExtendedProperties(DescriptionExtendedProperties other)
            : this(other.AvailableCarfax, other.AvailableCertified,
                   other.AvailableEdmundsTrueMarketValue, other.AvailableKelleyBlueBook,
                   other.AvailableNada, other.AvailableTagline,
                   other.AvailableHighValueOptions, other.AvailableStandardOptions,
                   other.IncludedCarfax, other.IncludedCertified,
                   other.IncludedEdmundsTrueMarketValue, other.IncludedKelleyBlueBook,
                   other.IncludedNada, other.IncludedTagline,
                   other.IncludedHighValueOptions, other.IncludedStandardOptions)
        {
        }

        internal DescriptionExtendedProperties(IDataRecord reader)
        {
            AvailableCarfax = reader.GetBoolean(reader.GetOrdinal("availableCarfax"));
            AvailableCertified = reader.GetBoolean(reader.GetOrdinal("availableCertified"));
            AvailableEdmundsTrueMarketValue = reader.GetBoolean(reader.GetOrdinal("availableEdmundsTrueMarketValue"));
            AvailableKelleyBlueBook = reader.GetBoolean(reader.GetOrdinal("availableKelleyBlueBook"));
            AvailableNada = reader.GetBoolean(reader.GetOrdinal("availableNada"));
            AvailableTagline = reader.GetBoolean(reader.GetOrdinal("availableTagline"));
            AvailableHighValueOptions = reader.GetBoolean(reader.GetOrdinal("availableHighValueEquipment"));
            AvailableStandardOptions = reader.GetBoolean(reader.GetOrdinal("availableStandardEquipment"));
            IncludedCarfax = reader.GetBoolean(reader.GetOrdinal("includedCarfax"));
            IncludedCertified = reader.GetBoolean(reader.GetOrdinal("includedCertified"));
            IncludedEdmundsTrueMarketValue = reader.GetBoolean(reader.GetOrdinal("includedEdmundsTrueMarketValue"));
            IncludedKelleyBlueBook = reader.GetBoolean(reader.GetOrdinal("includedKelleyBlueBook"));
            IncludedNada = reader.GetBoolean(reader.GetOrdinal("includedNada"));
            IncludedTagline = reader.GetBoolean(reader.GetOrdinal("includedTagline"));
            IncludedHighValueOptions = reader.GetBoolean(reader.GetOrdinal("includedHighValueEquipment"));
            IncludedStandardOptions = reader.GetBoolean(reader.GetOrdinal("includedStandardEquipment"));
        }

        public bool AvailableCarfax { get; set; }

        public bool AvailableCertified { get; set; }

        public bool AvailableEdmundsTrueMarketValue { get; set; }

        public bool AvailableKelleyBlueBook { get; set; }

        public bool AvailableNada { get; set; }

        public bool AvailableTagline { get; set; }

        public bool AvailableHighValueOptions { get; set; }

        public bool AvailableStandardOptions { get; set; }

        public bool IncludedCarfax { get; set; }

        public bool IncludedCertified { get; set; }

        public bool IncludedEdmundsTrueMarketValue { get; set; }

        public bool IncludedKelleyBlueBook { get; set; }

        public bool IncludedNada { get; set; }

        public bool IncludedTagline { get; set; }

        public bool IncludedHighValueOptions { get; set; }

        public bool IncludedStandardOptions { get; set; }

        public static DescriptionExtendedProperties Generate(IVehicleTemplatingData vehicleData,
                                                             MerchandisingDescription merchandisingDescription)
        {
            var extProps = new DescriptionExtendedProperties(merchandisingDescription.ExtendedProperties);
            extProps.AvailableCarfax = vehicleData.EvaluateSingleCondition(TemplateCondition.IsOneOwner);
            extProps.AvailableCertified = vehicleData.EvaluateSingleCondition(TemplateCondition.IsCertified);
            extProps.AvailableEdmundsTrueMarketValue = false;
            extProps.AvailableHighValueOptions = vehicleData.EvaluateSingleCondition(TemplateCondition.HasTier1Equipment);
            extProps.AvailableKelleyBlueBook = vehicleData.HasAvailableBookout(BookoutCategory.KbbRetail);
            extProps.AvailableNada = vehicleData.HasAvailableBookout(BookoutCategory.NadaCleanRetail);
            extProps.AvailableStandardOptions = extProps.AvailableHighValueOptions ||
                                                vehicleData.EvaluateSingleCondition(TemplateCondition.HasTier2Equipment);
            extProps.AvailableTagline = vehicleData.EvaluateSingleCondition(TemplateCondition.HasTagline);
            extProps.IncludedTagline = extProps.AvailableTagline;
            return extProps;
        }

        public void AddMappedDescriptionItem(MerchandisingDescriptionItemType itemType)
        {
            switch (itemType)
            {
                case MerchandisingDescriptionItemType.CertifiedType:
                case MerchandisingDescriptionItemType.DetailedCertified:
                    IncludedCertified = true;
                    AvailableCertified = true;
                    break;
                case MerchandisingDescriptionItemType.Tagline:
                    IncludedTagline = true;
                    AvailableTagline = true;
                    break;
                case MerchandisingDescriptionItemType.Tier1Equipment:
                case MerchandisingDescriptionItemType.Tier2Equipment:
                    IncludedHighValueOptions = true;
                    IncludedStandardOptions = true;
                    AvailableHighValueOptions = true;
                    AvailableStandardOptions = true;
                    break;
                case MerchandisingDescriptionItemType.Carfax1Owner:
                    IncludedCarfax = true;
                    AvailableCarfax = true;
                    break;
                case MerchandisingDescriptionItemType.NadaValue:
                    IncludedNada = true;
                    AvailableNada = true;
                    break;
                case MerchandisingDescriptionItemType.KelleyBlueBookValue:
                    IncludedKelleyBlueBook = true;
                    AvailableKelleyBlueBook = true;
                    break;

                case MerchandisingDescriptionItemType.HighlightedPackages:
                    AvailableHighValueOptions = true;
                    IncludedHighValueOptions = true;
                    break;

                default:
                    break;
            }
        }
    }
}