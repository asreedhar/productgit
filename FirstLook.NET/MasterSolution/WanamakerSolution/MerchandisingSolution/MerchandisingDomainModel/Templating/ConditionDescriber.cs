using System;
using System.Collections.Generic;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class ConditionDescriber : RandomUtility
    {
        private readonly List<ConditionItem> _items;
        
        public ConditionDescriber()
        {
            
        }
        public ConditionDescriber(ConditionChecklist checklist, VehicleConditionCollection conditions, int tireTread)
        {

            _items = new List<ConditionItem>();

            if (checklist.HadAllRequiredScheduledMaintenancePerformed)
            {
                _items.Add(new ConditionItem(ConditionItem.HadAllRequiredScheduledMaintenance));
            }
            if (checklist.HasNoKnownAccidents)
            {
                _items.Add(new ConditionItem(ConditionItem.NoKnownAccidents));
            }
            if (checklist.HasNoKnownBodyWork)
            {
                _items.Add(new ConditionItem(ConditionItem.NoKnownBodyWork));
            }
            if (checklist.HasNoPanelScratches)
            {
                _items.Add(new ConditionItem(ConditionItem.NoPanelScratches));
            }
            if (checklist.HasNoVisibleDents)
            {
                _items.Add(new ConditionItem(ConditionItem.NoVisibleDents));
            }
            if (checklist.HasNoVisibleRust)
            {
                _items.Add(new ConditionItem(ConditionItem.NoVisibleRust));
            }
            if (checklist.HasPassedDealerInspection)
            {
                _items.Add(new ConditionItem(ConditionItem.PassedDealerInspection));
            }
            if (checklist.HaveAllKeys)
            {
                _items.Add(new ConditionItem(ConditionItem.HaveAllKeys));
            }
            if (checklist.HaveOriginalManuals)
            {
                _items.Add(new ConditionItem(ConditionItem.HaveOriginalManuals));
            }
            if (checklist.HaveServiceRecords)
            {
                _items.Add(new ConditionItem(ConditionItem.HaveServiceRecords));
            }
            if (checklist.IsDealerMaintained)
            {
                _items.Add(new ConditionItem(ConditionItem.DealerMaintained));
            }
            if (checklist.IsFullyDetailed)
            {
                _items.Add(new ConditionItem(ConditionItem.IsFullyDetailed));
            }
            if (checklist.NoKnownMechanicalProblems)
            {
                _items.Add(new ConditionItem(ConditionItem.NoMechanicalProblems));
            }
            if (tireTread >= 85)
            {
                _items.Add(new ConditionItem(ConditionItem.TireTreadLikeNew));
            }else if (tireTread >= 65)
            {
                _items.Add(new ConditionItem(ConditionItem.TireTreadGoodAmountRemaining));
                
            }

            //add condition levels
            int condLevel = conditions.GetLevel((int) VehicleConditionType.Exterior);
            if (condLevel == VehicleCondition.CONDITION_LEVEL_EXCELLENT)
            {
                _items.Add(new ConditionItem(ConditionItem.ExcellentExterior));
            }
            
            
            condLevel = conditions.GetLevel((int)VehicleConditionType.Interior);
            if (condLevel == VehicleCondition.CONDITION_LEVEL_EXCELLENT)
            {
                _items.Add(new ConditionItem(ConditionItem.ExcellentInterior));
            }            

            _items = ConditionRuleBag.ProcessRuleList(_items, this);
            
        }

        public string GetDescription(int maxChars)
        {
            StringBuilder sb = new StringBuilder();
            
            int itemCt = 0;
            while (sb.Length <= maxChars && itemCt < _items.Count)
            {
                sb.Append(_items[itemCt].ToString(this)).Append("  ");
                itemCt++;
            }
            return sb.ToString();
        }

        private int _currentIndex;
        public string PopDescription(int maxCount)
        {
            int lastIndex = Math.Min(_items.Count, maxCount + _currentIndex);
            StringBuilder sb = new StringBuilder();                        
            while (_currentIndex < lastIndex)
            {
                sb.Append(_items[_currentIndex].ToString(this)).Append("  ");
                _currentIndex++;
            }
            return sb.ToString();
        }

        public bool HasDescriptionsRemaining
        {
            get { return _items.Count > _currentIndex; }
        }

        
    }
}
