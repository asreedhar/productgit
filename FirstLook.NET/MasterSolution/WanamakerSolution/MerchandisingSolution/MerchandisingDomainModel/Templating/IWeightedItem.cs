using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public interface IWeightedItem : IComparable<IWeightedItem>
    {
        /// <summary>
        /// weight of object from zero to 1
        /// </summary>
        double GetWeight();

        /// <summary>
        /// Get the tier of the equipment - allows full separation of weighting into tiers
        /// </summary>
        /// <returns>integer value of tier...lower is more important</returns>
        int GetTierNumber();

        /// <summary>
        /// should be randomly weighted and deterministic for a given run so a sort works properly
        /// </summary>
        /// <returns></returns>
        double GetRandomizedWeight(Random random);

        bool Edited { get;}
        string EditedText { get; }
        int EditedOrder { get;}

        string ToString();
    }

    public class StochasticWeightedItemComparer : IComparer<IWeightedItem>
    {
        private readonly Random _random;

        public StochasticWeightedItemComparer()
        {
            _random = new Random(DateTime.Now.Millisecond);
        }

        #region IComparer<IWeightedItem> Members

        /// <summary>
        /// returns the weighted items by ranking tiers first then the item weight on a randomized basis
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(IWeightedItem x, IWeightedItem y)
        {
            if(x.Edited && !y.Edited)
                return -1;
            if(x.Edited && y.Edited)
            {
                if (x.EditedOrder < y.EditedOrder)
                    return -1;
                    
                return 1;
            }
            if (!x.Edited && y.Edited)
                return 1;


            int comparison = x.GetTierNumber().CompareTo(y.GetTierNumber());
            if (comparison == 0)
            {
                comparison = y.GetRandomizedWeight(_random).CompareTo(x.GetRandomizedWeight(_random));
            }
            return comparison;
        }

        #endregion
    }
}