using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Templating.Previews;
using FirstLook.Merchandising.DomainModel.Vehicles;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public static class MostSearchedTermMapper
    {
        static readonly List<MostSearchedTerm> Instance;

        static MostSearchedTermMapper()
        {
            Instance = new List<MostSearchedTerm>();

            //load up the list from the database...     
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.mostSearchedTerms#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Instance.Add(new MostSearchedTerm(reader));
                        }
                    }
                }
            } 
        }

        public static List<PreviewEquipmentItem> GetHighValuePackageTitles(DetailedEquipmentCollection equipment)
        {
            List<PreviewEquipmentItem> retList = new List<PreviewEquipmentItem>();
            List<int> usedIds = new List<int>();

            if (equipment.Count > 0)
            {
                List<ICategorizedEquipment> mstEquip = new List<ICategorizedEquipment>();
                foreach (MostSearchedTerm mst in Instance)
                {
                    mstEquip.Add(mst);
                }
                List<DetailedEquipment> mostSearchedAndHighValue = equipment.FindAllMatching(mstEquip);
                
                foreach (DetailedEquipment equip in mostSearchedAndHighValue)
                {
                    retList.Add(new PreviewEquipmentItem(equip.OptionText.Trim()));
                    foreach (CategoryLink cl in equip.categoryAdds)
                    {
                        usedIds.Add(cl.CategoryID);
                    }
                }                
            }

            return retList;
        }



        /*
         * We want to always include these generics in the preview 
         * 
         * Navigation - 1066
         * Leather Seats - 1078
         * Third Row Seat - 1073
         * DVD	- 1215
         * Sunroof - 1067, 1068, 1069, 1132, 1144
         * 
        */
        private static PreviewGenericEquipmentItem CreateHighValueEquipmentItem(string description, int categoryId)
        {
            var alwaysIncludeCategoryIDs = new [] { 1066, 1078, 1073, 1215, 1067, 1068, 1069, 1132, 1144 };

            if (alwaysIncludeCategoryIDs.Contains(categoryId))
                return new PreviewGenericEquipmentItem(description, true, categoryId);


            return new PreviewGenericEquipmentItem( description, false, categoryId );
        }

        public static Pair<List<PreviewGenericEquipmentItem>, List<int>>  GetMostSearchedPreviewTerms(GenericEquipmentCollection equipment)
        {

            var retList = new List<PreviewGenericEquipmentItem>();
            List<string> doNotReuseList = new List<string>();
            
            List<int> includedMstCategoryIds = new List<int>();

            foreach (MostSearchedTerm mst in Instance)
            {

                if (equipment.Contains(mst.CategoryId))                 //if the generic list on the vehicle contains the id found in our most searched list, add a descriptor
                {
                    int id = mst.CategoryId;
                    includedMstCategoryIds.Add(id);

                    string highValueDescriptor = mst.ToString();
                    bool alreadyUsed = doNotReuseList.Contains(highValueDescriptor);
                    if (!alreadyUsed)   //we won't reuse one that's been represented already
                    {                   //not used yet, so let's add it to the output and log
                        retList.Add(CreateHighValueEquipmentItem(highValueDescriptor, mst.CategoryId));  //add the descriptor to our list
                        doNotReuseList.AddRange(mst.ValidDescriptors);  //we must track these so we don't duplicate them...save the full list                        
                    }
                }
            }
            return new Pair<List<PreviewGenericEquipmentItem>, List<int>>( retList, includedMstCategoryIds );
        }
    }
}
