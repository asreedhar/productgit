using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public interface ITemplateDataRepository
    {
        IEnumerable<TemplateData> GetTemplateDataForBusinessUnit(int businessUnit);
    }
}