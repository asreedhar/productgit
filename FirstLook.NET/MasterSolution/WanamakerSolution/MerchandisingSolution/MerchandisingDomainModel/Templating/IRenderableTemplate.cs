namespace FirstLook.Merchandising.DomainModel.Templating
{
    public interface IRenderableTemplate
    {
        string Name { get; set; }

        int VehicleProfileId { get; set; }

        int OwnerBusinessUnitId { get; set; }

        bool HasTagline();
    }
}