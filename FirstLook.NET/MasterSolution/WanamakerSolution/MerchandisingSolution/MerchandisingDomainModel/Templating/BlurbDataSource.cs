using System.Collections.Generic;
using MerchandisingLibrary;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class BlurbDataSource
    {
        public TemplateBlurb TemplateBlurbSelect(int businessUnitID, int blurbID)
        {
            return TemplateBlurb.Fetch(businessUnitID, blurbID);
        }

        public void TemplateBlurbUpdate(int businessUnitID, TemplateBlurb blurb)
        {
            blurb.Save(businessUnitID);
            return;
        }

        public void TemplateBlurbDelete(int businessUnitID, int blurbID)
        {
            TemplateBlurb.Delete(businessUnitID, blurbID);
        }

        public BlurbTextCollection TemplateTextSamplesSelect(int businessUnitID, int blurbID)
        {
            return BlurbTextCollection.FetchTextSamples(businessUnitID, blurbID);
        }

        public void TemplateBlurbCreate(int businessUnitId, TemplateBlurb blurb)
        {
            blurb.Save(businessUnitId);
        }

        public void TemplateBlurbCreate(TemplateBlurb blurb)
        {
            blurb.Save();
        }

        public void TemplateSampleCreatePlaceholder(int businessUnitID, int blurbID)
        {
            TemplateSampleCreateOrUpdate(businessUnitID, blurbID, -1, string.Empty, string.Empty, true, false, (-1).ToString(), (-1).ToString(), false);
        }
        public void TemplateSampleCreateOrUpdate(int businessUnitID, int blurbID, int blurbTextID,
                                                 string preGenerateText, string postGenerateText, bool active,
                                                 bool isLongForm, string vehicleProfileId, string themeId, bool isHeader)
        {
            int? tmpTheme = null;
            if (!string.IsNullOrEmpty(themeId))
            {
                tmpTheme = int.Parse(themeId);
            }

            int vehicleprofile = int.Parse(vehicleProfileId);

            try
            {
                BlurbText bt = BlurbText.Fetch(blurbTextID, businessUnitID);
                bt.PostGenerateText = postGenerateText;
                bt.PreGenerateText = preGenerateText;
                bt.Active = active;
                bt.Match.Id = vehicleprofile;
                bt.IsLongForm = isLongForm;
                bt.ThemeId = tmpTheme;
                bt.IsHeader = isHeader;
                bt.Save(businessUnitID, blurbID);
            }
            catch
            {
                //not found, so we need to create it...
                BlurbText.Create(businessUnitID, blurbID, preGenerateText, postGenerateText, isLongForm,
                                 vehicleprofile, tmpTheme, isHeader);
            }
        }

        public void TemplateSampleDelete(int businessUnitID, int blurbTextID)
        {
            BlurbText.Delete(businessUnitID, blurbTextID);
        }

        public List<TemplateItem> TemplateItemsSelect(int templateID, int businessUnitID, bool orderByPriority)
        {
            AdTemplate adT = AdTemplate.Fetch(templateID, businessUnitID);
            if (orderByPriority)
            {
                List<TemplateItem> tempItems = adT.TemplateItems;
                tempItems.Sort();
                return tempItems;
            }

            return adT.TemplateItems;
        }


        public bool UpdateSequence(int templateId, int blurbId, int sequence, int priority, int businessUnitId)
        {
            return TemplateItem.UpdateSequence(templateId, blurbId, sequence, priority, businessUnitId);
        }


        public void Update(TemplateItem item)
        {
            item.Save();
        }

        public AdTemplate TemplateSelect(int templateID, int businessUnitID)
        {
            return AdTemplate.Fetch(templateID, businessUnitID);
        }

        public Dictionary<int, string> AvailableTemplatesSelect(int businessUnitID)
        {
            return AdTemplate.FetchTemplateList(businessUnitID, true, false);
        }

        public Dictionary<int, string> InactiveTemplatesSelect(int businessUnitID)
        {
            return AdTemplate.FetchTemplateList(businessUnitID, false, true);
        }


        public VehicleProfile TemplateRulesSelect(int templateID)
        {
            return VehicleProfile.Fetch(templateID);
        }

        public void TemplateDelete(int templateID, int businessUnitID)
        {
            AdTemplate.Delete(templateID, businessUnitID);
        }
    }
}