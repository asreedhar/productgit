using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    /// <summary>
    /// Summary description for DisplayPriorityDataSource
    /// </summary>
    public class DisplayPriorityDataSource
    {
        public static void UpdateDisplayDescription(int rankingID, string description)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.equipmentDisplayDescription#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "RankingID", rankingID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "DisplayDescription", description, DbType.String);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateSticky(int rankingID, bool isSticky)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.equipmentSticky#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "RankingID", rankingID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "IsSticky", isSticky, DbType.Boolean);

                    cmd.ExecuteNonQuery();
                }
            }
        }


        public void Update(int rankingID, int displayPriority, int tier)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.EquipmentDisplayRankings#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "RankingID", rankingID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "DisplayPriority", displayPriority, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Tier", tier, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public DataSet Fetch(int businessUnitID, int tierNumber)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.equipmentDisplayRankings#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "TierNumber", tierNumber, DbType.Int32);

                    var ds = new DataSet();

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        ds.Load(reader, LoadOption.OverwriteChanges, new[] {"tbl"});
                        return ds;
                    }
                }
            }
        }
    }
}