using System.Collections.Generic;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class TemplateDataRepository : ITemplateDataRepository
    {
        public IEnumerable<TemplateData> GetTemplateDataForBusinessUnit(int businessUnit)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                var templateData = new List<TemplateData>();

                connection.Open();

                var commandText = string.Format(@"
                    SELECT * 
                    FROM templates.AdTemplates templates
                    LEFT JOIN templates.VehicleProfiles profiles
                    ON profiles.vehicleProfileId = templates.vehicleProfileID
                    WHERE templates.businessUnitID = '{0}' AND templates.active = 1", businessUnit);

                command.CommandType = CommandType.Text;
                command.CommandText = commandText;

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var data = new TemplateData();

                    data.Id = reader.GetInt32(reader.GetOrdinal("templateID"));
                    data.Name = reader.GetString(reader.GetOrdinal("name"));

                    var profileId = Database.GetNullableInt(reader, "vehicleProfileId");
                    if (profileId.HasValue)
                    {
                        data.VehicleProfile = new VehicleProfile(reader);
                    }

                    templateData.Add(data);
                }


                return templateData;
            }
        }
    }
}