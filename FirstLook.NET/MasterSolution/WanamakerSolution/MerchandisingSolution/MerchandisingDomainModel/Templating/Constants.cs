﻿

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public static class Constants
    {
        //2457 is the Luxury/Key Features Enhanced+ framework at the firstlook systems level
        public static int DEFAULT_TEMPLATE_ID = 2457;
    }
}
