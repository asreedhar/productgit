using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using FirstLook.Common.Data;
using MerchandisingLibrary;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class TemplateCategory
    {
        public int CategoryID { get; set; }

        public string CategoryName { get; set; }

        public List<TemplateBlurb> Blurbs { get; set; }

        public TemplateCategory()
        {
            Blurbs = new List<TemplateBlurb>();
            CategoryName = "";
        }


        public static string FetchXml(int businessUnitID, bool useDefaults)
        {
            
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase, false))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getAvailableTemplateItemList";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);

                    //NOTE USES SQL EXPLICITLY
                    XmlReader xr = ((SqlCommand) cmd).ExecuteXmlReader();
                    xr.MoveToContent();
                    return xr.ReadOuterXml();
                
                }
            }
            
        }
        public static List<TemplateCategory> FetchCategoryList()
        {
            // Template categories are blurb types in the database (not blurb categories)
            
            List<TemplateCategory> myList = new List<TemplateCategory>();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase, false))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getAllSnippetTypes";
                    cmd.CommandType = CommandType.StoredProcedure;

                    IDataReader myReader  = cmd.ExecuteReader();

                    while (myReader.Read())
                    {
                        myList.Add(
                                new TemplateCategory
                                    {
                                        CategoryID = Convert.ToInt32(myReader["blurbTypeId"]),
                                        CategoryName = myReader["blurbTypeName"].ToString()
                                    }
                                );
                    }



                }
            }

            return myList;


        }



    }
}
