using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;

using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class StringTemplateHelper
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(StringTemplateHelper).FullName);
        #endregion

        public static string stringTemplateBaseObjectName = "base";

        internal static string EncodeString(string input, Dictionary<string, string> map)
        {
            // strip [ and ] from input
            var temp = StripBrackets(input);

            // Look it up in the map
            if (map.ContainsKey(temp))
            {
                string value = map[temp];

                Log.DebugFormat("MerchandisingPointsMap contains the key '{0}'", temp);
                Log.DebugFormat("merchandisingPointsMap['{0}'] has value '{1}'", temp, value);

                return value;
            }

            // Form name by convention.
            Log.WarnFormat("MerchandisingPointsMap DOES NOT contains the key '{0}'", temp);
            string convention = string.Format("${0}.{1}$", stringTemplateBaseObjectName, FormNameByConvention(input));

            Log.DebugFormat("Value set to '{0}'", convention);
            return convention;
        }

        internal static string GetValidStringTemplate(string codeLevelTemplate, string displayTagDelimStart,
                                      string displayTagDelimEnd,
                                      Dictionary<string, string> merchandisingPointsMap)
        {

            Log.DebugFormat(
                "GetValidStringTemplate() called with codeLevelTemplate:{0}, displayTagDelimStart:{1}, displayTagDelimEnd:{2}, merchandisingPointsMap:{3}",
                codeLevelTemplate, displayTagDelimStart, displayTagDelimEnd, PrintDictionary(merchandisingPointsMap));

            string temp = Sanitize(codeLevelTemplate);

            // Regex to find [abc def] tags
            Regex regexTags = new Regex(@"\[[\d\w\s-/]+\]");
            var output = regexTags.Replace(temp, m => EncodeString(m.ToString(), merchandisingPointsMap));            

            // temp should now be valid.
            Log.DebugFormat("Returning '{0}'", output);
            return output;
        }

        private static string FormNameByConvention(string input)
        {
            Log.DebugFormat("FormNameByConvention() called with input '{0}'", input);
            var temp = StripBrackets(input);

            string output = string.Empty;

            var words = temp.Split(' ');
            foreach (var word in words)
            {
                output += word.ToProperCase();
            }
            Log.DebugFormat("Calculated property name is '{0}'", output);
            return output;
        }

        private static string StripBrackets(string input)
        {
            var temp = input.Replace("[", string.Empty);
            temp = temp.Replace("]", string.Empty);
            return temp;
        }

        


/*        public static void GetValidStringTemplate(string codeLevelTemplate, string displayTagDelimStart,
                                      string displayTagDelimEnd, 
                                      Dictionary<string, string> merchandisingPointsMap,
                                      out string text, out string propertyName)
        {

            Log.DebugFormat("GetValidStringTemplate() called with codeLevelTemplate:{0}, displayTagDelimStart:{1}, displayTagDelimEnd:{2}, merchandisingPointsMap:{3}",
                codeLevelTemplate, displayTagDelimStart, displayTagDelimEnd, PrintDictionary(merchandisingPointsMap));

            //currently assume no nested tags
            string workingText = Sanitize(codeLevelTemplate);
            string tempTag = "";
            int stNdx;
            string propName = string.Empty;

            while (-1 < (stNdx = workingText.IndexOf(displayTagDelimStart)))
            {
                int endNdx = workingText.IndexOf(displayTagDelimEnd);
                int len = endNdx - stNdx - displayTagDelimEnd.Length;
             
                Log.DebugFormat("Looping with stNdx:{0}, endNdx:{1}, len:{2}", stNdx, endNdx, len);
                
                if (len > 0)
                {
                    tempTag = workingText.Substring(stNdx + displayTagDelimStart.Length, len);
                    //-4+1 for length computation
                    //process the tempTag
                    Log.DebugFormat("Len > 0 and tempTag has been set to {0}", tempTag);
                }
                else
                {
                    Log.Debug("Len must not be > 0.");
                }

                workingText = workingText.Remove(stNdx, len + displayTagDelimStart.Length + displayTagDelimEnd.Length);
                Log.DebugFormat("Working text has been set to '{0}'", workingText);

                if (merchandisingPointsMap.ContainsKey(tempTag))
                {
                    Log.DebugFormat("MerchandisingPointsMap contains the key '{0}'", tempTag);
                    Log.DebugFormat("merchandisingPointsMap['{0}'] has value '{1}'", tempTag, merchandisingPointsMap[tempTag]);

                    if (!String.IsNullOrEmpty(merchandisingPointsMap[tempTag]))
                    {
                        Log.DebugFormat("merchandisingPointsMap['{0}'] has value '{1}'.", tempTag, merchandisingPointsMap[tempTag]);
                        workingText = workingText.Insert(stNdx, merchandisingPointsMap[tempTag]);                        
                        Log.DebugFormat("workingText set to '{0}'", workingText);

                        propName = merchandisingPointsMap[tempTag];
                        Log.DebugFormat("propName set to '{0}", propName);
                        break;
                    }
                    
                    Log.WarnFormat("merchandisingPointsMap['{0}'] IS null or empty.", tempTag);
                    /*
                     * THIS SECTION USED TO ALLOW AN INDIVIDUAL PIECE OF EQUIPMENT TO BE ADDED TO THE TEMPLATE WHEN IT WAS ON THE VEHICLE
                     * else
                    {
                        //check included equipment categories to test tag against if not found in lookup
                        if (vehicleData.VehicleConfig.VehicleOptions.Contains(tempTag))
                        {
                            workingText = workingText.Insert(stNdx, tempTag);
                        }
                    }
                }
                else
                {
                    Log.WarnFormat("MerchandisingPointsMap DOES NOT contains the key '{0}'", tempTag);

                    //our final mode is to add a default tag that will access a data member directly on the object with no transformation
                    //as long as there is a corresponding StringTemplate accessor method/property on the base object
                    workingText = workingText.Insert(stNdx, 
                                                     string.Format("${0}.{1}$", 
                                                        stringTemplateBaseObjectName, 
                                                        tempTag));

                    Log.DebugFormat("WorkingText set to '{0}'", workingText);
                    propName = tempTag;
                    break;
                }
            }

            Log.DebugFormat("Returning workingText: '{0}'", workingText);
            Log.DebugFormat("Returning property name '{0}'", propName);

            // returns working text, prop name
            text = workingText;
            propertyName = propName;
        }*/

/*
        private static string ExtractPropertyName(string encodedPropertyName)
        {
            Log.DebugFormat("ExtractPropertyName() called with encodedPropertyName '{0}'", encodedPropertyName);

            // converts $base.PropName$ to "PropName"
            if( encodedPropertyName.StartsWith("$base.") && 
                encodedPropertyName.EndsWith("$") &&
                !encodedPropertyName.Equals("$base.$"))
            {
                int len = encodedPropertyName.Length;

                // Get rid of starting $base. and ending $
                return encodedPropertyName.Substring(6, len - 7);
            }
            return encodedPropertyName;
        }
*/

        public static string RenderWithVehicleData(string validTemplateText,                                                    
                                                   IVehicleTemplatingData vehicleData)
        {
            Log.DebugFormat("RenderWithVehicleData() called with validTemplateText: {0}, vehicleData: {1}", 
                validTemplateText, vehicleData);   

            // Check inputs.
            if( string.IsNullOrEmpty(validTemplateText) || vehicleData == null )
            {
                Log.WarnFormat("Invalid inputs. validTemplateText: '{0}', vehicleData: {1}", 
                    validTemplateText, vehicleData);

                Log.Warn("Returning empty string.");

                return string.Empty;
            }

            return Templater.DoTemplating(validTemplateText, vehicleData);
        }


        public static string Sanitize(string text)
        {
            Log.DebugFormat("Sanitize() called with input text '{0}'", text);
            var sanitized = text.Replace( "\\", "\\\\" );
            sanitized = sanitized.Replace( "$", "\\$" );
            Log.DebugFormat("Returning sanitized text '{0}'", sanitized);
            return sanitized;
        }

        private static string PrintDictionary(Dictionary<string, string> dictionary)
        {
            if( dictionary == null )
            {
                return "Dictionary is empty.";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("Dictionary: ");

            foreach( var key in dictionary.Keys )
            {
                sb.Append(";Key: '");
                sb.Append(key);
                sb.Append("', Value: '");
                sb.Append(dictionary[key]);
                sb.Append("'");
            }

            return sb.ToString();
        }
    }
}
