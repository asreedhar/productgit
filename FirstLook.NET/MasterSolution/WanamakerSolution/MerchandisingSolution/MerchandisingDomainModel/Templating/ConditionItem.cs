namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class ConditionItem
    {
        public const int PassedDealerInspection = 1;
        public const int NoMechanicalProblems = 2;
        public const int NoKnownAccidents = 3;
        public const int NoKnownBodyWork = 4;
        public const int DealerMaintained = 5;
        public const int HaveServiceRecords = 6;
        public const int HadAllRequiredScheduledMaintenance = 7;
        public const int HaveAllKeys = 8;
        public const int HaveOriginalManuals = 9;
        public const int IsFullyDetailed = 10;
        public const int NoPanelScratches = 11;
        public const int NoVisibleDents = 12;
        public const int NoVisibleRust = 13;
        public const int TireTreadGoodAmountRemaining = 14;
        public const int TireTreadLikeNew = 15;

        public const int GoodExterior = 50;
        public const int GoodInterior = 51; 

        public const int ExcellentExterior = 70;
        public const int ExcellentInterior = 71;
        public const int ExcellentSeats = 72;
        public const int ExcellentPaint = 73;
        public const int ExcellentCarpet = 74;


        public const int HadAllMaintananceHaveAllRecords = 101;
        public const int HaveAllKeysAndManuals = 102;
        public const int PassedDealerInspectionAndDetailed = 103;
        public const int DealerMaintainedHaveAllRecords = 104;
        public const int NoScratchesDents = 105;
        public const int NoScratchesRust = 106;
        public const int NoDentsRust = 107;

        public const int ExcellentExteriorAndPaint = 110;
        public const int ExcellentInteriorAndSeats = 111;
        public const int ExcellentSeatsAndCarpet = 112;


        public const int NoMechanicalProblemsAccidentsOrBodywork = 203;
        public const int NoScratchesDentsRust = 204;
        public const int DealerMaintainedHadAllMaintananceHaveAllRecords = 205;


        public ConditionItem(int itemType)
        {            
            ItemType = itemType;
        }

        public int ItemType { get; set; }

        public string ToString(RandomUtility randomizer)
        {            
            string s = Resources.ConditionDescriptions.ResourceManager.GetString("d" + ItemType);
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            string[] s2 = s.Split("|".ToCharArray());
            return randomizer.GetRandomValue(s2);
        }
    }
}
