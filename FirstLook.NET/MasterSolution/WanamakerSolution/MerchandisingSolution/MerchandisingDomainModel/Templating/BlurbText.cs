using System;
using System.Data;
using System.Xml;
using FirstLook.Common.Data;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class BlurbText
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(BlurbText).FullName);
        #endregion


        public BlurbText()
        {
            PostGenerateText = "";
            PreGenerateText = "";
            Match = new VehicleProfile();

            PreBlurbEdited = false;
        }

        public BlurbText(int blurbTextID, string preGenerateText, string postGenerateText) : this()
        {
            BlurbTextID = blurbTextID;
            PreGenerateText = preGenerateText;
            PostGenerateText = postGenerateText;
        }

        internal BlurbText(IDataRecord reader)
        {
            BlurbTextID = reader.GetInt32(reader.GetOrdinal("BlurbTextID"));
            PreGenerateText = reader.GetString(reader.GetOrdinal("blurbPreText"));
            PostGenerateText = reader.GetString(reader.GetOrdinal("blurbPostText"));
            IsLongForm = reader.GetBoolean(reader.GetOrdinal("isLongForm"));
            IsHeader = reader.GetBoolean(reader.GetOrdinal("isHeader"));
            ThemeId = Database.GetNullableInt(reader, "themeId");
            Theme = Database.GetNullableString(reader, "themeName");
            Active = reader.GetBoolean(reader.GetOrdinal("active"));
            Match = new VehicleProfile(reader);

            PreBlurbEdited = false;
        }

        public BlurbText(XmlNode xNode)
        {
            if( xNode == null )
            {
                Log.Error("xNode is null.");
                throw new ArgumentException(@"xNode is null.", "xNode");
            }

            if( xNode.Attributes == null )
            {
                Log.Error("xNode.Attributes is null.");
                throw new ArgumentException(@"xNode.Attributes is null.", "xNode");                
            }

            BlurbTextID = Int32.Parse(xNode.Attributes["blurbTextID"].Value);
            PreGenerateText = Convert.ToString(xNode.Attributes["blurbPreText"].Value);
            PostGenerateText = Convert.ToString(xNode.Attributes["blurbPostText"].Value);
            Active = Convert.ToBoolean(Convert.ToInt32(xNode.Attributes["active"].Value));
            IsLongForm = Convert.ToBoolean(Convert.ToInt32(xNode.Attributes["isLongForm"].Value));
            IsHeader = Convert.ToBoolean(Convert.ToInt32(xNode.Attributes["isHeader"].Value));
            ThemeId = Database.GetNullableInt(xNode, "themeId");
            Match = xNode.ChildNodes.Count > 0 ? new VehicleProfile(xNode.ChildNodes[0]) : new VehicleProfile();

            PreBlurbEdited = false;
        }

        public VehicleProfile Match { get; set; }
        public string Theme { get; set; }
        public int? ThemeId { get; set; }
        public bool Active { get; set; }
        public bool IsLongForm { get; set; }
        public int BlurbTextID { get; set; }
        public string PreGenerateText { get; set; }
        public string PostGenerateText { get; set; }
        public bool IsHeader { get; set; }
        public bool PreBlurbEdited { get; set; }

        public string ConsolidatedText
        {
            get { return PreGenerateText + " " + PostGenerateText; }
        }

        public bool IsThemeMatch(int? testTheme)
        {
            Log.DebugFormat("IsThemeMatch() called with theme {0}", testTheme);

            //either both are unthemed, or the themes match
            //return (!themeId.HasValue && !testTheme.HasValue) ||
            //       (themeId.HasValue && themeId.Value == testTheme.GetValueOrDefault(-1));
            bool result = ThemeId.GetValueOrDefault(-1) == testTheme.GetValueOrDefault(-1);
                //use default to match on two unthemed items

            Log.DebugFormat("Result is {0}", result);

            return result;
        }

        public string Render(string bodyText)
        {
            Log.DebugFormat("Render() called with bodyText '{0}'", bodyText);
            string result = string.Format("{0}{1}{2}", PreGenerateText, bodyText, PostGenerateText);

            Log.DebugFormat("Result is '{0}'", result);
            return result;
        }

        /// <summary>
        /// Inserts into db.
        /// </summary>
        /// <param name="businessUnitID"></param>
        /// <param name="blurbID"></param>
        /// <param name="preText"></param>
        /// <param name="postText"></param>
        /// <param name="isLongForm"></param>
        /// <param name="vehicleProfileId"></param>
        /// <param name="themeId"></param>
        public static void Create(int businessUnitID, int blurbID, string preText, string postText, bool isLongForm,
                                  int vehicleProfileId, int? themeId, bool isHeader)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.insertSnippetText";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbPreText", string.Empty + preText, DbType.String);
                    Database.AddRequiredParameter(cmd, "BlurbPostText", string.Empty + postText, DbType.String);
                    Database.AddRequiredParameter(cmd, "IsLongForm", isLongForm, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "IsHeader", isHeader, DbType.Boolean);

                    if (vehicleProfileId > 0)
                    {
                        Database.AddRequiredParameter(cmd, "VehicleProfileId", vehicleProfileId, DbType.String);
                    }

                    if (themeId.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "ThemeId", themeId.Value, DbType.Int32);
                    }

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Save as ... saves current blurbtext to the specified business unit id and blurb id.
        /// </summary>
        /// <param name="businessUnitID"></param>
        /// <param name="blurbID"></param>
        public void Save(int businessUnitID, int blurbID)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.updateOrCreateSampleText";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbTextID", BlurbTextID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbPreText", string.Empty + PreGenerateText, DbType.String);
                    Database.AddRequiredParameter(cmd, "BlurbPostText", string.Empty + PostGenerateText, DbType.String);
                    Database.AddRequiredParameter(cmd, "Active", Active, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "IsLongForm", IsLongForm, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "IsHeader", IsHeader, DbType.Boolean);

                    if (Match.Id > 0)
                    {
                        Database.AddRequiredParameter(cmd, "VehicleProfileId", Match.Id, DbType.String);
                    }

                    if (ThemeId.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "ThemeId", ThemeId.Value, DbType.Int32);
                    }


                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Enables this blurb.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="blurbTextID"></param>
        /// <param name="enabled"></param>
        public static void SetEnabled(int businessUnitId, int blurbTextID, bool enabled)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.SampleText#SetEnabled";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BlurbTextID", blurbTextID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Enabled", enabled, DbType.Boolean);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Get the specified blurb.
        /// </summary>
        /// <param name="blurbTextID"></param>
        /// <param name="businessUnitID"></param>
        /// <returns></returns>
        public static BlurbText Fetch(int blurbTextID, int businessUnitID)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getSampleText";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BlurbTextID", blurbTextID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new BlurbText(reader);
                        }
                        throw new ApplicationException("Sample Text not found by its ID");
                    }
                }
            }
        }

        /// <summary>
        /// Delete's the specified blurb.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="blurbTextId"></param>
        public static void Delete(int businessUnitId, int blurbTextId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.SampleText#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbTextID", blurbTextId, DbType.Int32);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void LoadViewState(object state)
        {
            var vals = state as object[];
            if (vals == null || vals.Length != 5)
            {
                Log.Debug("ViewState must be 5 objects for BlurbText");
                throw new ApplicationException("ViewState must be 5 objects for BlurbText");
            }

            BlurbTextID = (int) vals[0];
            PreGenerateText = (string) vals[1];
            PostGenerateText = (string) vals[2];
            IsLongForm = (bool) vals[3];
            
            var tmpTheme = (int) vals[4];
            if (tmpTheme == -1)
            {
                ThemeId = null;
            }
            else
            {
                ThemeId = tmpTheme;
            }
        }

        public object SaveViewState()
        {
            var retObj = new object[4];
            retObj[0] = BlurbTextID;
            retObj[1] = PreGenerateText;
            retObj[2] = PostGenerateText;
            retObj[3] = IsLongForm;
            retObj[4] = ThemeId.GetValueOrDefault(-1);
            return retObj;
        }
    }
}
