using System;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public enum MerchandisingPointTypes
    {
        CustomText = 1,
        Attribute = 2,
        HardCodeRule = 3,
        BasicInfo = 4,
        ConsumerInfo = 5,
        CarfaxInfo = 6
    }

    public enum HardCodeIDs
    {
        Top3Safety = 1,
        Top3Luxury = 2,
        Top3Convenience = 3,
        Top3ConsumerInfo = 4,
        PriceBelowKBB = 5,
        LowMilesPerYear = 6,
        AllSelectedOptions = 7,
        AllStandardEquipment = 8,
        AllAfterMarketFeatures = 9
    }

    public enum BasicInfoIDs
    {
        YearMakeModel = 1,
        Trim = 2,
        Interior = 3,
        InteriorColor = 4,
        ExteriorColor = 5,
        Engine = 6,
        Condition = 7,
        Mileage = 8,
        AfterMarketTrim = 9
    }

    public enum ConsumerInfoIDs
    {
        GasMileageCity = 1,
        GasMileageHwy = 2,
        GasMileageHwyCity = 3,
        CrashTestFront = 4,
        CrashTestSide = 5,
        RolloverRating = 6,
        BasicWarranty = 7,
        DrivetrainWarranty = 8
    }

    public enum CarfaxInfoIDs
    {
        OneOwner = 1,
        BuyBackGuarantee = 2
    }


    [Serializable]
    public class MerchandisingPoint
    {
        public MerchandisingPoint()
        {
        }

        public MerchandisingPoint(string description, MerchandisingPointTypes ruleType)
        {
            Description = description;
            RuleType = ruleType;
        }

        public MerchandisingPoint(string description, MerchandisingPointTypes ruleType, string ruleParameter)
        {
            Description = description;
            RuleType = ruleType;
            RuleParameter = ruleParameter;
        }

        public MerchandisingPoint(string description, MerchandisingPointTypes ruleType, string ruleParameter,
                                  int pointID)
        {
            Description = description;
            RuleType = ruleType;
            RuleParameter = ruleParameter;
            PointID = pointID;
        }

        public string Description { get; set; }

        public MerchandisingPointTypes RuleType { get; set; }

        public int PointID { get; set; }

        public string RuleParameter { get; set; }

        public virtual string EvaluatePointForDescription(IVehicleTemplatingData vehicleData)
        {
            return Description;
        }
    }
}