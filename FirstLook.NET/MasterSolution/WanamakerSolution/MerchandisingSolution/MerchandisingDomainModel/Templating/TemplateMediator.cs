using System.Linq;
using System.Collections.Generic;
using System.Text;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;
using MerchandisingLibrary;
using MvcMiniProfiler;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    

    public class TemplateMediator : ITemplateMediator
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(TemplateMediator).FullName);
        #endregion

        private IVehicleTemplatingFactory _vehicleTemplatingFactory;

        public IVehicleTemplatingFactory VehicleTemplatingFactory
        {
            get { return _vehicleTemplatingFactory ?? Registry.Resolve<IVehicleTemplatingFactory>(); }
            set { _vehicleTemplatingFactory = value; }
        }

        private readonly int _businessUnitId;
        private MerchandisingDescription _generatedDescription;
        
        private bool _isInLongMode;
//        private bool _isInPropertyOrConditionCheck;
       
        private Dictionary<string, string> _merchandisingPointsMap;
        private AdTemplate _template;        
        private IVehicleTemplatingData _vehicleData;
        private AdTemplateSettings _currentAdSettings;

        private IList<PreviewItem> _previewItems;

        public TemplateMediator(int businessUnitId, int inventoryId, string memberLogin)
        {
            BlockedItems = new List<int>();
            
            _businessUnitId = businessUnitId;

            //fetch the vehicle data bag and the description if it already exists...
            _vehicleData = VehicleTemplatingFactory.GetVehicleTemplatingData(businessUnitId, inventoryId, memberLogin, this);
            
            //fetch the current description (generated on a previous occasion)
            _generatedDescription = MerchandisingDescription.Fetch(businessUnitId, inventoryId);
            
            //set the themeId to that used for the last description
            if (_generatedDescription.IsNew)
            {
                _currentAdSettings = AdTemplateSettings.FetchDefaultTemplateSettings(businessUnitId, _vehicleData.VehicleConfig.ChromeStyleID,
                                                                _vehicleData.InventoryItem);               
            }
            else
            {
                _currentAdSettings = _generatedDescription.GetLastAdTemplateSettings();

                var template = AdTemplate.Fetch(TemplateId, _businessUnitId);

                if (AdTemplate.BusinessUnitDoesNotHaveTemplate(_businessUnitId, TemplateId))
                {
                    _currentAdSettings = AdTemplateSettings.FetchDefaultTemplateSettings(businessUnitId, _vehicleData.VehicleConfig.ChromeStyleID, _vehicleData.InventoryItem);
                    _generatedDescription.TemplateId = _currentAdSettings.TemplateId;
                }
            }

        }

        public void ResetDefaultTemplateSettings(int styleId)
        {
            _currentAdSettings = AdTemplateSettings.FetchDefaultTemplateSettings(_businessUnitId, styleId, _vehicleData.InventoryItem);               
        }

        public string GetUiText(string field)
        {
//            _isInPropertyOrConditionCheck = true;
            string retVal = string.Empty;
            switch (field.ToLower())
            {
                case "engine":
                    retVal = VehicleData.Engine;
                    break;
                case "drivetrain":
                    retVal = VehicleData.Drivetrain;
                    break;
                case "transmission":
                    retVal = VehicleData.Transmission;
                    break;
                default:
                    break;
            }
//            _isInPropertyOrConditionCheck = false;
            return retVal;
        }
        public IVehicleTemplatingData VehicleData
        {
            get { return _vehicleData; }
            set { _vehicleData = value; }
        }

        public int? ThemeId
        {
            get { return _currentAdSettings.GetThemeId(); }
        }
        public bool TemplateHasTagline
        {
            get
            {
                return Template.HasTagline();
            }
        }
        private AdTemplate Template
        {
            get
            {
                if (_template == null)
                {
                    ConfigureTemplate(TemplateId, ThemeId);
                }
                return _template;
            }            
        }

        public List<int> BlockedItems { get; set; }

        public int TemplateId
        {
            get
            {
                return _currentAdSettings.GetTemplateIdOrDefault(VehicleData.Preferences.DefaultTemplateId);                
            }
            set { _currentAdSettings.TemplateId = value; }
        }

        public bool IsInLongMode
        {
            get { return _currentAdSettings.GetThemeId().HasValue || _isInLongMode; }
            set { _isInLongMode = value; }
        }

        public bool HasTagline
        {
            get { return Template.HasTagline(); }
        }

        public Dictionary<string, string> MerchandisingPointsMap
        {
            get {
                return _merchandisingPointsMap ??
                       (_merchandisingPointsMap = MerchandisingPointsDataSource.TemplateConversionSelect());
            }
        }
        /// <summary>
        /// method is used to evaluate conditional text, toggling the system to record when it is checking
        /// the condition so it doesn't log data access as acutal use of the data
        /// </summary>
        /// <param name="conditionText"></param>
        /// <returns></returns>
        public bool EvaluateCondition(string conditionText)
        {
            Log.DebugFormat("EvaluateCondition() called for conditionText '{0}'", conditionText);            

            //turn off the mediator notices from the data
//            _isInPropertyOrConditionCheck = true;
            bool isConditionOk = VehicleData.EvaluateCondition(conditionText);
//            _isInPropertyOrConditionCheck = false;

            Log.DebugFormat("Returning '{0}'", isConditionOk);
            return isConditionOk;
        }

        
        
        /// <summary>
        /// used to log which description data points are added to the description
        /// </summary>
        /// <param name="itemType"></param>
        public void DescriptionContentAdded(MerchandisingDescriptionItemType itemType)
        {
            Log.DebugFormat("DescriptionContentAdded() called with MerchandisingDescriptionItemType '{0}'", itemType);

            if (!_generatedDescription.ContainsItemType(itemType))
            {
                Log.DebugFormat("Adding itemType '{0}' to the generated description.", itemType);
                _generatedDescription.AddItemType(itemType);
            }
            else
            {
                Log.DebugFormat("Generated description already contains itemType '{0}'.", itemType);
            }


/*
            if (!_isInPropertyOrConditionCheck)
            {
                Log.DebugFormat("Adding itemType {0} to the generated description.", itemType);
                _generatedDescription.AddItemType(itemType);
            }            
            else
            {
                if( !_generatedDescription.ContainsItemType(itemType))
                {
                    Log.ErrorFormat("IsInPropertyOrConditionCheck. Ignoring call, and description does NOT already contain the MerchandisingDescriptionItemType {0}.  We have a threading problem.", itemType);                                        
                }
            }
*/
        }

        //if different from the current template, it fetches the new template (adjusting for the vehicle data)
        //also sets the theme id (should this be a prop on the template?
        private void ConfigureTemplate(int? templateId, int? useThemeId)
        {
            Log.DebugFormat("ConfigureTemplate() called with templateId {0} and useThemeId {1}", templateId, useThemeId);

            _currentAdSettings.SetThemeId(useThemeId);

            //only bother if the new template is not the same as the old            
            if (_template == null || (TemplateId != templateId))
            {
                _currentAdSettings.TemplateId = templateId;
                
                // fetch this new template
                _template = AdTemplate.Fetch(TemplateId, _businessUnitId);

                // Adjust for vehicle.
                _template.AdjustForVehicle(VehicleData.InventoryItem);

//                _isInPropertyOrConditionCheck = true;
                _generatedDescription.ExtendedProperties = 
                    DescriptionExtendedProperties.Generate(VehicleData, _generatedDescription);
                
                //PRESENTLY, tagline is always included if it exists...(except when you run out of space...not accounted for here...) 
                //could handle thru the template rendering cycle or break out...also could include it via <Tagline> in the templateBlurb which would make this special case go away
//                _isInPropertyOrConditionCheck = false;
            }

        }

        /// <summary>
        /// Returns the current description
        /// </summary>
        /// <returns></returns>
        public MerchandisingDescription GetCurrentMerchandisingDescription()
        {
            return _generatedDescription;
        }

        /// <summary>
        /// will take the current description if exists, otherwise, generates one
        /// </summary>
        /// <returns></returns>
        public MerchandisingDescription GetOrCreateMerchandisingDescription()
        {
            return GetOrCreateMerchandisingDescription(TemplateId, ThemeId, 1, true);
        }

        public MerchandisingDescription GetOrCreateMerchandisingDescription(bool hasTrim)
        {
            return GetOrCreateMerchandisingDescription(TemplateId, ThemeId, 1, hasTrim);
        }


        public MerchandisingDescription GetOrCreateMerchandisingDescription(int templateId, int? useThemeId, int maxThemeSentences)
        {
            return GetOrCreateMerchandisingDescription(templateId, useThemeId, maxThemeSentences, true);
        }

        /// <summary>
        /// Gets the current description if exists, otherwise it will create with the given params
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="useThemeId"></param>
        /// <param name="maxThemeSentences"></param>
        /// <param name="hasTrim"></param>
        /// <returns></returns>
        public MerchandisingDescription GetOrCreateMerchandisingDescription(int templateId, int? useThemeId, int maxThemeSentences, bool hasTrim)
        {
            if (!string.IsNullOrEmpty(_generatedDescription.Description) && !_generatedDescription.IsAutoPilot
                &&
                (!_generatedDescription.TemplateId.HasValue ||
                 _generatedDescription.TemplateId.GetValueOrDefault(-1) == templateId)
                &&
                (!_generatedDescription.ThemeId.HasValue ||
                 _generatedDescription.ThemeId.GetValueOrDefault(-1) == useThemeId.GetValueOrDefault(-1))
                 &&
                 (_generatedDescription.AdvertisementModel != null && _generatedDescription.AdvertisementModel.Version == Advertisement.SecondVersion))
            {
                return _generatedDescription;
            }

            if (!hasTrim)
            {
                AdvertisementConverter converter = new AdvertisementConverter();
                _generatedDescription.AdvertisementModel = converter.EmptyModel;
                _generatedDescription.OldAdvertisementModel = converter.EmptyModel;
                return _generatedDescription;
            }

            //otherwise, generate it
            return GenerateDescription(templateId, useThemeId, maxThemeSentences);
        }

        /// <summary>
        /// Forces the regeneration of a template's text
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="useThemeId"></param>
        /// <param name="maxThemeSentences"></param>
        /// <returns></returns>
        public MerchandisingDescription ReGenerateTemplateText(Advertisement editedModel, int? templateId, int? useThemeId, int maxThemeSentences)
        {
            byte[] ver = _generatedDescription.Version;
            bool autoPilot = _generatedDescription.IsAutoPilot;
            
            _generatedDescription = GenerateDescription(editedModel, templateId, useThemeId, maxThemeSentences);
            _generatedDescription.Version = ver;
            _generatedDescription.IsAutoPilot = autoPilot;
            return _generatedDescription;
        }

        
        public MerchandisingDescription GenerateDescription()
        {
            return GenerateDescription(TemplateId, null, 1);
        }

        
        /// <summary>
        /// overload to handle missing character limit
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="useThemeId"></param>
        /// <param name="maxThemeSentences"></param>
        /// <returns></returns>
        public MerchandisingDescription GenerateDescription(int? templateId, int? useThemeId, int maxThemeSentences)
        {   
            AdvertisementConverter converter = new AdvertisementConverter();
            Advertisement model = converter.EmptyModel;

            if (_generatedDescription.AdvertisementModel != null &&
                _generatedDescription.AdvertisementModel.Version == Advertisement.SecondVersion)
                //if we have the new model then merge that
                model = _generatedDescription.AdvertisementModel;

            return GenerateDescription(model, templateId, useThemeId, maxThemeSentences);   
        }

        private MerchandisingDescription GenerateDescription(Advertisement editedModel, int? templateId, int? useThemeId, int maxThemeSentences)
        {
            return GenerateDescription(editedModel, templateId, useThemeId, maxThemeSentences, VehicleData.Preferences.PreferredDescriptionLength);
        }

        /// <summary>
        /// Creates the code level template text for a set of template items.  Identifying tags are not
        /// replaced with representative data.  When insertData is false, the data input is used to evaluate conditions to determine 
        /// if sections of the template should be included.
        /// 
        /// if insertData is true, representative data values are inserted into any applicable data merge tags
        /// </summary>
        /// <param name="useThemeId"></param>
        /// <param name="characterLimit">Max number of characters to display</param>
        /// <param name="templateId"></param>
        /// <param name="maxThemeSentences"></param>
        /// <returns></returns>
        private MerchandisingDescription GenerateDescription(Advertisement editedModel, int? templateId, int? useThemeId, int maxThemeSentences, int characterLimit)
        {
            using (MiniProfiler.Current.Step("Generate Description"))
            {
                ConfigureTemplate(templateId, useThemeId);
                return GenerateDescription(editedModel, true, maxThemeSentences, characterLimit);
            }
        }


        /// <summary>
        /// 
        /// </summary>        
        /// <param name="insertData">Determines whether the output should be the template representation of the description or the rendered template output with merged data</param>
        /// <param name="characterLimit"></param>        
        /// <param name="maxThemeSentences"></param>
        /// <returns></returns>
        private MerchandisingDescription GenerateDescription(Advertisement editedModel, bool insertData, int maxThemeSentences, int characterLimit)
        {
            // Make sure the description's properites are correct.  Maybe we can do this earlier.
            _generatedDescription.IsLongForm = IsInLongMode;
            _generatedDescription.TemplateId = TemplateId;
            _generatedDescription.ThemeId = _currentAdSettings.GetThemeId();
            _generatedDescription.BusinessUnitId = _businessUnitId;

            // Build up the description.
            Template.BuildUpDescription(editedModel, maxThemeSentences, characterLimit, insertData, this,
                                        ref _generatedDescription, VehicleData);



            return _generatedDescription;
        }

        public void RegisterPreviewItems(IList<PreviewItem> previewItems)
        {
            _previewItems = previewItems;
        }

        public PreviewItem[] GetPreviewItems()
        {
            return _previewItems.ToArray();
        }

        public int CalulateFooterLength ()
        {
           _generatedDescription.ResetDisclaimer();
           return _generatedDescription.Footer.Length;
        }
    }
}