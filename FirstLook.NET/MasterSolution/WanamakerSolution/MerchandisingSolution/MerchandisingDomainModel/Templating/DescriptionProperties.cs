using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class DescriptionProperties
    {
        public DescriptionProperties()
        {
            Author = string.Empty;
            Created = DateTime.Now;
            Accessed = DateTime.Now;
            Modified = DateTime.Now;
            EditDuration = 1;
            RevisionNumber = 0;
            HasEditedBody = false;
            HasEditedFooter = false;
        }

        public DescriptionProperties(string author, DateTime created, DateTime accessed, DateTime modified,
                                     int editDuration, int revisionNumber, bool hasEditedBody, bool hasEditedFooter)
        {
            Author = author;
            Created = created;
            Accessed = accessed;
            Modified = modified;
            EditDuration = editDuration;
            RevisionNumber = revisionNumber;
            HasEditedBody = hasEditedBody;
            HasEditedFooter = hasEditedFooter;
        }

        internal DescriptionProperties(IDataRecord reader)
        {
            Author = reader.GetString(reader.GetOrdinal("Author"));
            Created = reader.GetDateTime(reader.GetOrdinal("Created"));
            Accessed = reader.GetDateTime(reader.GetOrdinal("Accessed"));
            Modified = reader.GetDateTime(reader.GetOrdinal("Modified"));
            EditDuration = reader.GetInt32(reader.GetOrdinal("EditDuration"));
            RevisionNumber = reader.GetInt32(reader.GetOrdinal("RevisionNumber"));
            HasEditedBody = reader.GetBoolean(reader.GetOrdinal("HasEditedBody"));
            HasEditedFooter = reader.GetBoolean(reader.GetOrdinal("HasEditedFooter"));
        }

        public string Author { get; set; }

        public DateTime Created { get; set; }

        public DateTime Accessed { get; set; }

        public DateTime Modified { get; set; }

        public int EditDuration { get; set; }

        public int RevisionNumber { get; set; }

        public bool HasEditedBody { get; set; }

        public bool HasEditedFooter { get; set; }
    }
}