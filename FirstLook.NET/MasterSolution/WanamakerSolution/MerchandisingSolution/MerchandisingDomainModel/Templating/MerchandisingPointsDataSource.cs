using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    /// <summary>
    /// Summary description for MerchandisingPointsDataSource
    /// </summary>
    public class MerchandisingPointsDataSource
    {
        public static Dictionary<string, string> TemplateConversionSelect()
        {
            return TemplateConversionSelect(0);
        }
        public static Dictionary<string, string> TemplateConversionSelect(int businessUnitID)
        {
            Dictionary<string, string> retDict = new Dictionary<string, string>();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getTemplateConversions";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string currKey = reader.GetString(reader.GetOrdinal("templateDisplayText"));
                            if (!retDict.ContainsKey(currKey))
                            {
                                retDict.Add(currKey,
                                            reader.GetString(reader.GetOrdinal("dataAccessString")));
                            }
                        }


                        return retDict;
                    }
                }
            }
        }
    }
}