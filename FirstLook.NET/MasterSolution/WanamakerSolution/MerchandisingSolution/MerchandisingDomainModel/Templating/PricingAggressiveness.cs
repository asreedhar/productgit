namespace FirstLook.Merchandising.DomainModel.Templating
{
    public enum PricingAggressiveness
    {
        Undefined = 0,
        Low, //normal pricing
        Medium, //aggressive pricing for reduced cost
        High //very aggressive pricing just prior to wholesale
    }
}
