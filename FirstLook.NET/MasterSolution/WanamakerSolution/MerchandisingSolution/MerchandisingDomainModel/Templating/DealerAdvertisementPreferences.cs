using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Distribution.WebService.Client.Distributor;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using IDataConnection = FirstLook.Common.Data.IDataConnection;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class DealerAdvertisementPreferences : IDealerAdvertisementPreferences
    {
        private int businessUnitId;
        private int countTier1ForLoaded;
        private int? defaultTemplateId;
        private string defaultTemplateName;

        public string DefaultTemplateName
        {
            get { return defaultTemplateName; }
            set { defaultTemplateName = value; }
        }

        private List<GasMileagePreference> gasMileageBySegment;
        private decimal goodBookDifferential = 300;


        private int goodCrashTestRating = 4;
        private decimal goodMsrpDifferential = 10000;

        private int goodWarrantyMileageRemaining;
        private int highPassengerCountForSuvs = 6;
        private int highPassengerCountForTrucks = 5;
        private int highReconditioningValue = 800;

        private int maxCountForStripped = 6;
        private int maxCountJdPowerRatings = 2;

        private int maxCrashTestRatingsToDisplay = 2;
        private int maxMileageThatIsLow = 72000;

        // Equipment defaults
        private const int MAX_TIER_0 = 0;
        private const int MAX_TIER_1 = 20;
        private const int MAX_TIER_2 = 10;
        private const int MAX_TIER_3 = 4;

        private int maxOptions = 20;
        private int maxOptionsCountInPreview = 15;
        private int[] maxOptionsByTier = new int[4] { MAX_TIER_0, MAX_TIER_1, MAX_TIER_2, MAX_TIER_3 };

        internal static int[] DefaultMaxOptionsByTier()
        {
            return new int[4] { MAX_TIER_0, MAX_TIER_1, MAX_TIER_2, MAX_TIER_3 };
        }

        private int maxWarrantiesToDisplay = 2;
        private int minDescriptionLength = 250;
        private int minNumberForCountBelowBookDisplay = 10;
        private int minNumberForCountBelowPriceDisplay = 10;
        private int minOptionsPerGroup = 2;
        private float minValidJdPowerRating = 3.5F;
        private decimal minValidListPrice = 500.0M;
        private BookoutCategory preferredBookCategory = BookoutCategory.NadaCleanRetail;
        private BookoutCategory secondaryBookCategory = BookoutCategory.NadaCleanRetail;
        private BookoutCategory prefferedBookoutCategory = BookoutCategory.NadaCleanRetail;

        private int preferredDescriptionLength = 2000;

        private string textSeparator = "~";
        private bool useLongDescriptionAsDefault = false;
        private int desiredPreviewLength = 150;


        private int minGoodPriceReduction = 300;
        private bool useCallToActionInPreview = true;

        private int maxConditionSummaryLength = 150;

        private int maxCountMarketingReviews = 3;
        private bool previewEquipmentIsReusable = true;
        private int minFavourablePriceComparisons = 4;
        private bool isEdmundsActive = false;
        private bool isKbbConsumerToolEnabled = false;

        #region PriceProofPointsVariables

        private int thresholdColor = 0;
        private int kbb = 0;
        private int nada = 0;
        private int marketAvg = 0;
        private int msrp = 0;
        private int edmunds = 0;


        #endregion

        public int MinFavourablePriceComparisons
        {
            get { return minFavourablePriceComparisons; }
            set { minFavourablePriceComparisons = value; }
        }

        public bool UseCallToActionInPreview
        {
            get { return useCallToActionInPreview; }
            set { useCallToActionInPreview = value; }
        }

        public int MinGoodPriceReduction
        {
            get { return minGoodPriceReduction; }
            set { minGoodPriceReduction = value; }
        }

        public int MaxOptionsCountInPreview
        {
            get { return maxOptionsCountInPreview; }
            set { maxOptionsCountInPreview = value; }
        }


        public bool PreviewEquipmentIsReusable
        {
            get { return previewEquipmentIsReusable; }
            set { previewEquipmentIsReusable = value; }
        }

        public int MaxCountMarketingReviews
        {
            get { return maxCountMarketingReviews; }
            set { maxCountMarketingReviews = value; }
        }

        public int MaxConditionSummaryLength
        {
            get { return maxConditionSummaryLength; }
            set { maxConditionSummaryLength = value; }
        }

        #region PriceProofPoints

        public int ThresholdColor
        {
            get { return thresholdColor; }
            set { thresholdColor = value; }
        }
        public int Kbb
        {
            get { return kbb; }
            set { kbb = value; }
        }

        public int Nada
        {
            get { return nada; }
            set { nada = value; }

        }

        public int MarketAverage
        {
            get { return marketAvg; }
            set { marketAvg = value; }
        }

        public int Msrp
        {
            get { return msrp; }
            set { msrp = value; }
        }

        public int Edmunds
        {
            get { return edmunds; }
            set { edmunds = value; }
        }

        #endregion

        public DealerAdvertisementPreferences()
        {
            gasMileageBySegment = new List<GasMileagePreference>();
            //TODO:  Setup default gas mileage prefs

        }


        public DealerAdvertisementPreferences(int businessUnitId)
        {
            this.businessUnitId = businessUnitId;
        }


        public DealerAdvertisementPreferences(int businessUnitId, int[] maxOptionsByTier, decimal goodBookDifferential,
                                              decimal goodMsrpDifferential, int minNumberForCountBelowBookDisplay,
                                              int minNumberForCountBelowPriceDisplay, int goodWarrantyMileageRemaining,
                                              int maxWarrantiesToDisplay, int goodCrashTestRating,
                                              int maxCrashTestsToDisplay, int countTier1ForLoaded, string textSeparator,
                                              List<GasMileagePreference> gasMileageBySegment, int defaultTemplateId,
                                              bool useLongDescriptionAsDefault, int desiredPreviewLength, bool useCallToActionInPreview,
                                              int minGoodPriceReduction, float minValidJdPowerRating, int minFavourablePriceComparisons,
                                              int thresholdColor, int kbb, int nada, int marketAverage, int msrp, int edmunds)
        {
            this.businessUnitId = businessUnitId;
            this.maxOptionsByTier = maxOptionsByTier;
            this.goodBookDifferential = goodBookDifferential;
            this.goodMsrpDifferential = goodMsrpDifferential;
            this.minNumberForCountBelowBookDisplay = minNumberForCountBelowBookDisplay;
            this.minNumberForCountBelowPriceDisplay = minNumberForCountBelowPriceDisplay;
            this.goodWarrantyMileageRemaining = goodWarrantyMileageRemaining;
            this.maxWarrantiesToDisplay = maxWarrantiesToDisplay;
            this.goodCrashTestRating = goodCrashTestRating;
            maxCrashTestRatingsToDisplay = maxCrashTestsToDisplay;
            this.countTier1ForLoaded = countTier1ForLoaded;
            this.gasMileageBySegment = gasMileageBySegment;
            this.textSeparator = textSeparator;
            this.defaultTemplateId = defaultTemplateId;
            this.useLongDescriptionAsDefault = useLongDescriptionAsDefault;
            this.desiredPreviewLength = desiredPreviewLength;
            this.minGoodPriceReduction = minGoodPriceReduction;
            this.useCallToActionInPreview = useCallToActionInPreview;
            this.minValidJdPowerRating = minValidJdPowerRating;
            this.minFavourablePriceComparisons = minFavourablePriceComparisons;

            //added new
            this.thresholdColor = thresholdColor;
            this.kbb = kbb;
            this.nada = nada;
            marketAvg = marketAverage;
            this.msrp = msrp;
            this.edmunds = edmunds;

        }

        internal DealerAdvertisementPreferences(int businessUnitId, IDataReader reader)
        {
            this.businessUnitId = businessUnitId;
            gasMileageBySegment = new List<GasMileagePreference>();


            if (reader.Read())
            {
                maxOptionsByTier = new int[]
                                       {
                                           reader.GetInt32(reader.GetOrdinal("maxOptionsTier0")),
                                           reader.GetInt32(reader.GetOrdinal("maxOptionsTier1")),
                                           reader.GetInt32(reader.GetOrdinal("maxOptionsTier2")),
                                           reader.GetInt32(reader.GetOrdinal("maxOptionsTier3"))
                                       };
                goodBookDifferential = reader.GetDecimal(reader.GetOrdinal("goodBookDifferential"));
                goodMsrpDifferential = reader.GetDecimal(reader.GetOrdinal("goodMsrpDifferential"));
                minNumberForCountBelowBookDisplay =
                    reader.GetInt32(reader.GetOrdinal("minNumberForCountBelowBookDisplay"));
                minNumberForCountBelowPriceDisplay =
                    reader.GetInt32(reader.GetOrdinal("minNumberForCountBelowPriceDisplay"));
                goodWarrantyMileageRemaining = reader.GetInt32(reader.GetOrdinal("goodWarrantyMileageRemaining"));
                maxWarrantiesToDisplay = reader.GetInt32(reader.GetOrdinal("maxWarrantiesToDisplay"));
                goodCrashTestRating = reader.GetInt32(reader.GetOrdinal("goodCrashTestRating"));
                maxCrashTestRatingsToDisplay = reader.GetInt32(reader.GetOrdinal("maxCrashTestRatingsToDisplay"));
                countTier1ForLoaded = reader.GetInt32(reader.GetOrdinal("countTier1ForLoaded"));
                textSeparator = reader.GetString(reader.GetOrdinal("textSeparator"));
                int? BookOutPreferenceId = Database.GetNullableInt(reader, "BookOutPreferenceId");
                preferredBookCategory =
                    (BookoutCategory)(BookOutPreferenceId == null ? 0 : BookOutPreferenceId);
                BookOutPreferenceId = Database.GetNullableInt(reader, "GuideBook2BookOutPreferenceId");
                secondaryBookCategory =
                    (BookoutCategory)(BookOutPreferenceId == null ? 0 : BookOutPreferenceId);
                int? prefferedId = Database.GetNullableInt(reader, "preferredBookoutCategoryId");
                prefferedBookoutCategory = (BookoutCategory)(prefferedId == null ? 0 : prefferedId);
                isEdmundsActive =
                 Convert.ToBoolean(reader.GetValue(reader.GetOrdinal("EdmudsActive")));

                isKbbConsumerToolEnabled = Convert.ToBoolean(Database.GetNullableBoolean(reader, "KBBConsumerToolEnabled"));

                int? tempId = Database.GetNullableInt(reader, "defaultTemplateId");
                if (tempId.HasValue)
                {
                    defaultTemplateId = tempId.Value;
                }

                defaultTemplateName = reader.GetString(reader.GetOrdinal("defaultTemplateName"));
                useLongDescriptionAsDefault = reader.GetBoolean(reader.GetOrdinal("useLongDescriptionAsDefault"));
                desiredPreviewLength = reader.GetInt32(reader.GetOrdinal("desiredPreviewLength"));
                useCallToActionInPreview = reader.GetBoolean(reader.GetOrdinal("useCallToActionInPreview"));
                minGoodPriceReduction = reader.GetInt32(reader.GetOrdinal("minGoodPriceReduction"));
                minFavourablePriceComparisons = reader.GetInt32(reader.GetOrdinal("FavourableComparisonThreshold"));
                minValidJdPowerRating = reader.GetFloat(reader.GetOrdinal("minValidJdPowerRating"));
                thresholdColor = reader.GetInt32(reader.GetOrdinal("Threshold_ProofPoint"));
                kbb = reader.GetInt32(reader.GetOrdinal("KBB_ProofPoint"));
                nada = reader.GetInt32(reader.GetOrdinal("NADA_ProofPoint"));
                marketAvg = reader.GetInt32(reader.GetOrdinal("MktAvg_ProofPoint"));
                msrp = reader.GetInt32(reader.GetOrdinal("MSRP_ProofPoint"));
                edmunds = reader.GetInt32(reader.GetOrdinal("Edmunds_ProofPoint"));
                //move reader to next resultset, which contains rows for gasMileagePreferences
                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        gasMileageBySegment.Add(new GasMileagePreference(
                                                    reader.GetInt32(reader.GetOrdinal("segmentId")),
                                                    reader.GetString(reader.GetOrdinal("segment")),
                                                    reader.GetInt32(reader.GetOrdinal("goodCityGasMileage")),
                                                    reader.GetInt32(reader.GetOrdinal("goodHighwayGasMileage"))));
                    }
                }



            }
        }


        public int PreferredDescriptionLength
        {
            get { return preferredDescriptionLength; }
            set { preferredDescriptionLength = value; }
        }

        public int DefaultTemplateId
        {
            get
            {
                if (defaultTemplateId.HasValue)
                {
                    return defaultTemplateId.Value;
                }
                return Constants.DEFAULT_TEMPLATE_ID;
            }
            set { defaultTemplateId = value; }
        }

        public int HighReconditioningValue
        {
            get { return highReconditioningValue; }
            set { highReconditioningValue = value; }
        }

        public int MaxMileageThatIsLow
        {
            get { return maxMileageThatIsLow; }
            set { maxMileageThatIsLow = value; }
        }

        public int MaxCountJdPowerRatings
        {
            get { return maxCountJdPowerRatings; }
            set { maxCountJdPowerRatings = value; }
        }

        public float MinValidJdPowerRating
        {
            get { return minValidJdPowerRating; }
            set { minValidJdPowerRating = value; }
        }

        public BookoutCategory PreferredBookCategory
        {
            get { return preferredBookCategory; }
            set { preferredBookCategory = value; }
        }

        public int MinOptionsPerGroup
        {
            get { return minOptionsPerGroup; }
            set { minOptionsPerGroup = value; }
        }

        public int MaxOptions
        {
            get { return maxOptions; }
            set { maxOptions = value; }
        }

        public decimal MinValidListPrice
        {
            get { return minValidListPrice; }
            set { minValidListPrice = value; }
        }

        public int MaxCountForStripped
        {
            get { return maxCountForStripped; }
            set { maxCountForStripped = value; }
        }

        public int MinDescriptionLength
        {
            get { return minDescriptionLength; }
            set { minDescriptionLength = value; }
        }

        public int HighPassengerCountForSuvs
        {
            get { return highPassengerCountForSuvs; }
            set { highPassengerCountForSuvs = value; }
        }

        public int HighPassengerCountForTrucks
        {
            get { return highPassengerCountForTrucks; }
            set { highPassengerCountForTrucks = value; }
        }

        public int BusinessUnitId
        {
            get { return businessUnitId; }
            set { businessUnitId = value; }
        }

        public List<GasMileagePreference> GasMileageBySegment
        {
            get { return gasMileageBySegment; }
            set { gasMileageBySegment = value; }
        }

        public string TextSeparator
        {
            get { return textSeparator; }
            set { textSeparator = value; }
        }

        public int CountTier1ForLoaded
        {
            get { return countTier1ForLoaded; }
            set { countTier1ForLoaded = value; }
        }

        public int[] MaxOptionsByTier
        {
            get { return maxOptionsByTier; }
            set { maxOptionsByTier = value; }
        }

        public decimal GoodBookDifferential
        {
            get { return goodBookDifferential; }
            set { goodBookDifferential = value; }
        }

        public decimal GoodMsrpDifferential
        {
            get { return goodMsrpDifferential; }
            set { goodMsrpDifferential = value; }
        }

        public int MinNumberForCountBelowBookDisplay
        {
            get { return minNumberForCountBelowBookDisplay; }
            set { minNumberForCountBelowBookDisplay = value; }
        }

        public int MinNumberForCountBelowPriceDisplay
        {
            get { return minNumberForCountBelowPriceDisplay; }
            set { minNumberForCountBelowPriceDisplay = value; }
        }

        public int GoodWarrantyMileageRemaining
        {
            get { return goodWarrantyMileageRemaining; }
            set { goodWarrantyMileageRemaining = value; }
        }

        public int MaxWarrantiesToDisplay
        {
            get { return maxWarrantiesToDisplay; }
            set { maxWarrantiesToDisplay = value; }
        }

        public int GoodCrashTestRating
        {
            get { return goodCrashTestRating; }
            set { goodCrashTestRating = value; }
        }

        public int MaxCrashTestRatingsToDisplay
        {
            get { return maxCrashTestRatingsToDisplay; }
            set { maxCrashTestRatingsToDisplay = value; }
        }

        public BookoutCategory SecondaryBookCategory
        {
            get { return secondaryBookCategory; }
            set { secondaryBookCategory = value; }
        }

        public BookoutCategory PrefferedBookoutCategory
        {
            get { return prefferedBookoutCategory; }
            set { prefferedBookoutCategory = value; }

        }

        public bool IsEdmundsActive
        {
            get { return isEdmundsActive; }
            set { isEdmundsActive = value; }
        }

        public bool IsKbbConsumerActive
        {
            get { return isKbbConsumerToolEnabled; }
            set { isKbbConsumerToolEnabled = value; }
        }


        public static DealerAdvertisementPreferences Fetch(int businessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.MerchandisingDescriptionPreferences#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new DealerAdvertisementPreferences(businessUnitId, reader);
                    }
                }
            }
        }


        public static void UpdateOrCreate(DealerAdvertisementPreferences preferences)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.MerchandisingDescriptionPreferences#UpdateOrCreate";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitId", preferences.businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "goodBookDifferential", preferences.goodBookDifferential, DbType.Decimal);
                    Database.AddRequiredParameter(cmd, "goodMsrpDifferential", preferences.goodMsrpDifferential, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "maxOptionsTier0", preferences.maxOptionsByTier[0], DbType.Int32);
                    Database.AddRequiredParameter(cmd, "maxOptionsTier1", preferences.maxOptionsByTier[1], DbType.Int32);
                    Database.AddRequiredParameter(cmd, "maxOptionsTier2", preferences.maxOptionsByTier[2], DbType.Int32);
                    Database.AddRequiredParameter(cmd, "maxOptionsTier3", preferences.maxOptionsByTier[3], DbType.Int32);

                    Database.AddRequiredParameter(cmd, "goodWarrantyMileageRemaining", preferences.goodWarrantyMileageRemaining,
                                          DbType.Int32);
                    Database.AddRequiredParameter(cmd, "goodCrashTestRating ", preferences.goodCrashTestRating, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "maxWarrantiesToDisplay", preferences.maxWarrantiesToDisplay,
                                          DbType.Int32);
                    Database.AddRequiredParameter(cmd, "maxCrashTestRatingsToDisplay", preferences.maxCrashTestRatingsToDisplay,
                                          DbType.Int32);

                    Database.AddRequiredParameter(cmd, "minNumberForCountBelowBookDisplay",
                                          preferences.minNumberForCountBelowBookDisplay, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "minNumberForCountBelowPriceDisplay",
                                          preferences.minNumberForCountBelowPriceDisplay, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "countTier1ForLoaded", preferences.countTier1ForLoaded, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "textSeparator", preferences.textSeparator, DbType.String);
                    Database.AddRequiredParameter(cmd, "preferredBookoutCategoryId", preferences.prefferedBookoutCategory,
                                          DbType.Int32);

                    Database.AddRequiredParameter(cmd, "defaultTemplateId", preferences.defaultTemplateId, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "useLongDescriptionAsDefault", preferences.useLongDescriptionAsDefault, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "desiredPreviewLength", preferences.desiredPreviewLength, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "useCallToActionInPreview", preferences.useCallToActionInPreview, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "minGoodPriceReduction", preferences.minGoodPriceReduction, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "minValidJdPowerRating", preferences.minValidJdPowerRating, DbType.Double);
                    Database.AddRequiredParameter(cmd, "minFavourablePriceComparisons", preferences.minFavourablePriceComparisons, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Threshold_ProofPoint", preferences.thresholdColor, DbType.Int16);
                    Database.AddRequiredParameter(cmd, "KBB_ProofPoint", preferences.kbb, DbType.Int16);
                    Database.AddRequiredParameter(cmd, "NADA_ProofPoint", preferences.nada, DbType.Int16);
                    Database.AddRequiredParameter(cmd, "MktAvg_ProofPoint", preferences.marketAvg, DbType.Int16);
                    Database.AddRequiredParameter(cmd, "MSRP_ProofPoint", preferences.msrp, DbType.Int16);
                    Database.AddRequiredParameter(cmd, "Edmunds_ProofPoint", preferences.edmunds, DbType.Int16);
                    cmd.ExecuteNonQuery();
                }

                foreach (GasMileagePreference pref in preferences.GasMileageBySegment)
                {
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "Settings.gasMileagePreferences#UpdateOrCreate";
                        cmd.CommandType = CommandType.StoredProcedure;

                        Database.AddRequiredParameter(cmd, "BusinessUnitId", preferences.businessUnitId, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "segmentId", pref.SegmentId, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "goodCityGasMileage", pref.CityMileage, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "goodHighwayGasMileage", pref.HighwayMileage, DbType.Int32);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }


        public bool UseLongDescriptionAsDefault
        {
            get { return useLongDescriptionAsDefault; }
            set { useLongDescriptionAsDefault = value; }
        }

        public int DesiredPreviewLength
        {
            get { return desiredPreviewLength; }
            set { desiredPreviewLength = value; }
        }

        public int GetGoodHighwayMileage(int segmentId)
        {
            int retVal = 0;
            foreach (GasMileagePreference gmp in gasMileageBySegment)
            {
                if (gmp.SegmentId == segmentId)
                {
                    retVal = gmp.HighwayMileage;
                    break;
                }
            }
            return retVal;
        }

        public int GetGoodCityMileage(int segmentId)
        {
            int retVal = 0;
            foreach (GasMileagePreference gmp in gasMileageBySegment)
            {
                if (gmp.SegmentId == segmentId)
                {
                    retVal = gmp.CityMileage;
                    break;
                }
            }
            return retVal;
        }
    }
}