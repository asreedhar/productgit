using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class AutoRevoMapper : IDataMapper
    {
        public static AutoRevoMapper Instance()
        {
            return new AutoRevoMapper();
        }
        public string ExecuteMapping(int businessUnitId, IVehicleTemplatingData data)
        {
            List<AutoRevoOptions> retList = new List<AutoRevoOptions>();
            
            //add any carfax maps
            retList.AddRange(GetCarfaxMapping(data));

            //add chrome options mapping
            retList.AddRange(GetAutoRevoOptionsFromChrome(businessUnitId, data.VehicleConfig.InventoryId));
            
            //data.VehicleConfig.Conditions
            StringBuilder sb = new StringBuilder();
            foreach (AutoRevoOptions option in retList)
            {
                sb.Append((int) option);
                sb.Append(",");
            }
            return sb.ToString().TrimEnd(",".ToCharArray()); //trim trailing comma
        }

        private static IEnumerable<AutoRevoOptions> GetCarfaxMapping(IVehicleTemplatingData data)
        {
            
            List<AutoRevoOptions> retList = new List<AutoRevoOptions>();
            if (data.IsOneOwner)
            {
                retList.Add(AutoRevoOptions.OneOwner);
            }

            if (data.CarfaxInfo.HasNoAccidentsOrDamageReported)
            {
                retList.Add(AutoRevoOptions.NoKnownAccidents);
            }
            return retList;
        }

        private static IEnumerable<AutoRevoOptions> GetAutoRevoOptionsFromChrome(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.AutoRevoMappedEquipment#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<AutoRevoOptions> retList = new List<AutoRevoOptions>();
                        while (reader.Read())
                        {
                            retList.Add((AutoRevoOptions)reader.GetInt32(reader.GetOrdinal("autoRevoOptionId")));
                        }
                        return retList;
                    }
                }
            }
        }
    }
}
