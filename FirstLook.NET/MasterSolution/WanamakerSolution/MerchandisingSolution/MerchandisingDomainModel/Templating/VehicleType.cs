using System;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using System.ComponentModel;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    /// <summary>
    /// The easy way to handle the class below.
    /// To compare/use the string value, use FirstLook.Common.Core.Utilities and:
    /// EnumHelper.GetEnumDescription(VehicleTypeEnum.Both);
    /// </summary>
    public enum VehicleTypeEnum
    {
        [Description("Both")]
        Both = 0,
        [Description("New")]
        New = 1,
        [Description("Used")]
        Used = 2
    }

    public class VehicleType
    {
        public static VehicleType Both = new VehicleType(intValue: 0, stringValue: "Both");
        public static VehicleType New = new VehicleType(intValue: 1, stringValue: "New");
        public static VehicleType Used = new VehicleType(intValue: 2, stringValue: "Used");

        private readonly int intValue;
        private readonly string stringValue;

        private VehicleType(int intValue, string stringValue)
        {
            this.intValue = intValue;
            this.stringValue = stringValue;
        }

        public int IntValue
        {
            get { return intValue; }
        }

        public string StringValue
        {
            get { return stringValue; }
        }

        public static implicit operator int(VehicleType vehicleType)
        {
            return vehicleType.IntValue;
        }

        public static implicit operator string(VehicleType vehicleType)
        {
            return vehicleType.StringValue;
        }

        public static implicit operator UsedOrNewFilter(VehicleType vehicleType)
        {
            return (UsedOrNewFilter) vehicleType.IntValue;
        }

        public override string ToString()
        {
            return StringValue;
        }

        public static implicit operator VehicleType(int integer)
        {
            if (integer == 0) return Both;
            if (integer == 1) return New;
            if (integer == 2) return Used;

            throw new InvalidCastException("Unable to convert VehicleType from int");
        }

        public static implicit operator VehicleType(string stringVal)
        {
            if (stringVal == Both.StringValue) return Both;
            if (stringVal == New.StringValue) return New;
            if (stringVal == Used.StringValue) return Used;

            throw new InvalidCastException("Unable to convert VehicleType from string");
        }

        public static implicit operator VehicleType(UsedOrNewFilter enumVal)
        {
            return (int) enumVal;
        }
    }
}