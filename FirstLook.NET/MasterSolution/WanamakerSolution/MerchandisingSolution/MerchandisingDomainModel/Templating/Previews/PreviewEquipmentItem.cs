﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public class PreviewEquipmentItem
    {
        public PreviewEquipmentItem(string description)
            :this(description, false)
        {    
        }

        public PreviewEquipmentItem(string description, bool mustInclude)
        {
            Description = description;
            MustInclude = mustInclude;
        }

        public string Description { get; set; }
        public bool MustInclude { get; set; }
    }
}
