﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public class PreviewGenericEquipmentItem : PreviewEquipmentItem
    {
        public PreviewGenericEquipmentItem (string description, int categoryId)
            : this( description, new List<int> { categoryId } )
        {

        }
        public PreviewGenericEquipmentItem (string description, bool mustInclude, int categoryId)
            : this( description, mustInclude, new List<int> { categoryId } )
        {
        }

        public PreviewGenericEquipmentItem (string description, List<int> categoryId)
            : base( description, false )
        {
            IncludedCategoryIds = categoryId;
        }

        public PreviewGenericEquipmentItem (string description, bool mustInclude, List<int> categoryIds)
            : base( description, mustInclude )
        {
            Description = description;
            MustInclude = mustInclude;
            IncludedCategoryIds = categoryIds;
        }


        public List<int> IncludedCategoryIds { get; set; }
    }
}
