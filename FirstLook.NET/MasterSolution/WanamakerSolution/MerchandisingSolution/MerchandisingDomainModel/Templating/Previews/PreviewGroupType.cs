namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public enum PreviewGroupType
    {
        Unknown = 0,
        RiskReduction = 1,
        Affordability,
        KeyFeatures,
        Safety,
        VehicleBasics,
        CallToAction,
    }
}
