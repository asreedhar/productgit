using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Templating.Previews.DAL;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public class PreviewGenerator : RandomUtility
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(PreviewGenerator).FullName);
        #endregion


        private const string INTERNAL_SEPARATOR = ", ";

        private static readonly string[] CallToActionStrings = new[]
                                                                   {
                                                                        " CLICK ME!", 
                                                                        " CLICK NOW!",
                                                                        " READ MORE!", 
                                                                        " AND MORE!",
                                                                        " SEE MORE!"
                                                                   };

        private static readonly string[] SeparatorStrings = new[] {" == ", " -- ", "... "};

        private static readonly string[] PriceFormatStrings = new[]
                                                                  {
                                                                      "PRICED TO MOVE {0} below {1}!",
                                                                      //"GREAT DEAL {0} below {1}.", // FB: 26640 - remove non-compliant phrases from adds, tureen 10/31/2013
                                                                      "{0} below {1}!"
                                                                  };

        private static readonly string[] FuelEconomyFormatStrings = new[]
                                                                        {
                                                                            "EPA {0}/{1}!",
                                                                            "FUEL EFFICIENT {0}/{1}!"
                                                                        };

        private static readonly string[] LowMilesFormatStrings = new[]
                                                                     {
                                                                         "ONLY {0} Miles!!!", "LOW MILES - {0}!",
                                                                         "GREAT MILES {0}!"
                                                                     };

        private static readonly string[] WarrantyFormatStrings = new[] {"Warranty {0}"};

        private static readonly string[] PriceReducedFormatStrings = new[]
                                                                         {
                                                                             "REDUCED FROM {0}!", "PRICE DROP FROM {0}",
                                                                             "JUST REPRICED FROM {0}", "WAS {0}"
                                                                         };

        private int _desiredPreviewLength;
        private readonly ITemplateMediator _templateMediator;
        private string _separator;
        private List<PreviewItem> _textList;
        private PreviewItem _callToAction;

        #region Injected dependencies

        private IPreviewPreferencesDao _previewPreferencesDAO;
        private IPreviewPreferencesDao PreviewPreferencesDAO
        {
            get
            {
                if (_previewPreferencesDAO == null )
                {
                    Log.Debug("Creating new PreviewPreferenceDao.");
                    _previewPreferencesDAO = Registry.Resolve<IPreviewPreferencesDao>(); 
                }
                return _previewPreferencesDAO;
            }
            set { _previewPreferencesDAO = value; }
        }


        #endregion

        public PreviewGenerator(int desiredPreviewLength, ITemplateMediator templateMediator)
        {
            Log.DebugFormat("PreviewGenerator ctor called with desiredPreviewLength: {0}, with templateMediator: {1}",
                            desiredPreviewLength, templateMediator);

            _desiredPreviewLength = desiredPreviewLength;
            _templateMediator = templateMediator;
            _textList = new List<PreviewItem>();

            _callToAction = new PreviewItem(() => true,
                                () => GetRandomValue(CallToActionStrings), 1,
                                1.0, PreviewGroupType.CallToAction, BlurbCategory.Regular, new Guid("{A81EB1F3-C38E-441a-85F4-B54786B6E671}"));
        }

        private void LoadUsedPreviewItems(IVehicleTemplatingData dataBag, IDealerAdvertisementPreferences preferences)
        {
            Log.DebugFormat("LoadUsedPreviewItems() called with dataBag: {0} and preferences: {1}", dataBag, preferences);

            IPreviewPreferences previewPrefs = dataBag.UsedPreviewPreferences;

            Log.DebugFormat("The IPreviewPreference that will be used is {0}", previewPrefs);

            //clear the list of values
            _textList = new List<PreviewItem>();

            //begin building list
            _textList.Add(new PreviewItem(() => previewPrefs.UseOneOwner && dataBag.IsOneOwner,
                                          () => dataBag.Carfax1Owner, 0, 1.0,
                                         PreviewGroupType.RiskReduction, BlurbCategory.Regular, new Guid("{772F7C7C-2B1C-467a-8AE7-7540D7AD0D94}")));
            _textList.Add(
                new PreviewItem(() => previewPrefs.UsePriceBelowBook && dataBag.HasGoodPriceBelowBook,
                                () => string.Format(GetRandomValue(PriceFormatStrings),
                                                    dataBag.PriceBelowBook, dataBag.PreferredBook), 1,
                                1.0, PreviewGroupType.Affordability, BlurbCategory.Price, new Guid("{D14ABC22-350C-4625-9BEA-99209F04B9C3}")));
            
            //This blurb should be editable by the user and so isn't a price blurb. It always shows the HighInternetPrice which doesn't change.
            _textList.Add(
                new PreviewItem(() => previewPrefs.UsePriceReduced && dataBag.IsPriceReducedEnough,
                                () => string.Format(GetRandomValue(PriceReducedFormatStrings),
                                                    dataBag.HighInternetPrice), 0, 1.0,
                                PreviewGroupType.Affordability, BlurbCategory.Regular, new Guid("{F394A275-67D0-4f2d-BDE4-4D14344F8713}")));

            _textList.Add(new PreviewItem(() => previewPrefs.UseCertified && dataBag.IsCertified,
                                          () => dataBag.HasCertificationPreview ? dataBag.CertificationPreview : dataBag.Certified, 0,
                                          1.0,
                                          PreviewGroupType.RiskReduction, BlurbCategory.Regular, new Guid("{F64582AF-E829-45f5-B80E-538E375722F4}")));

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseVehicleCondition && dataBag.HasCondition,
                             dataBag.GetCondition,
                             0, 0.7,
                             PreviewGroupType.RiskReduction, BlurbCategory.Regular, new Guid("{E7320948-EC1A-4a62-9F13-D7E51A83396B}"))
                );
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseMissionMarker && dataBag.HasMissionPreviewStatment,
                              () => dataBag.MissionPreviewStatement,
                             0, 1.0, PreviewGroupType.VehicleBasics, BlurbCategory.Regular, new Guid("{C3D85918-1DDA-4fa7-B4E9-43E369D8BA6D}"))
                );

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseJdPowerRatings && dataBag.HasGoodJdPowerRatings,
                              () => dataBag.JdPowerRatingPreview,
                             1, 0.8, PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{550A2131-A7DE-4c8c-BBE3-770F6E0EF60F}"))
                );

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseModelAwards && dataBag.HasAwards,
                              () => dataBag.BestModelAward,
                             1, 0.8,
                             PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{EC9B889F-2272-4988-A7DC-A462F828A1F1}")));
            //try the fuel economy
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseFuelEconomy && dataBag.GetsGoodGasMileage,
                              () => string.Format(GetRandomValue(FuelEconomyFormatStrings), dataBag.FuelHwy,
                                                  dataBag.FuelCity),
                             1, 0.7,
                             PreviewGroupType.Affordability, BlurbCategory.Regular, new Guid("{897E95BD-D87D-4299-AC49-390064059FF9}"))
                );
            //low mileage
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseLowMilesPerYear && dataBag.IsLowMilesPerYear,
                              () => string.Format(GetRandomValue(LowMilesFormatStrings), dataBag.Mileage),
                             1, 0.7,
                             PreviewGroupType.RiskReduction, BlurbCategory.Regular, new Guid("{3950352E-2111-47b0-998F-0D4C48309128}"))
                );

            LoadPreviewEquipmentIndividually( previewPrefs, dataBag );

            //financing
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseFinancingSpecial && dataBag.HasFinancingAvailable,
                              () => dataBag.SpecialFinancing,
                             2, 0.5,
                             PreviewGroupType.Affordability, BlurbCategory.Regular, new Guid("{1C80B7C1-AC98-4672-8FEC-A19D9927EB63}"))
                );


            //add one condition sentence
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseVehicleConditionStatements && dataBag.HasConditionHighlights,
                              () => dataBag.ConditionSentence,
                             2, 0.3,
                             PreviewGroupType.RiskReduction, BlurbCategory.Regular, new Guid("{5E4C8BBF-BFDF-4572-A060-29377AC765B6}"))
                );

            //safety
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseCrashTestRatings && dataBag.HasGoodCrashTestRatings,
                              () => dataBag.ConsInfo.GetGoodCrashRatingText(preferences.GoodCrashTestRating, 1,
                                                                            false, true),
                             2, 0.3,
                             PreviewGroupType.Safety, BlurbCategory.Regular, new Guid("{2AF98B35-A482-405a-9C21-F2F93B656EC9}"))
                );

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseWarranties 
                                  && dataBag.InventoryItem.IsNew() // Disabling for used cars because we can't tell what vehicles have applicable warranties.
                                  && dataBag.ConsInfo.HasGoodWarrantyRemaining(
                                        preferences.GoodWarrantyMileageRemaining,
                                        dataBag.InventoryItem.MileageReceived,
                                        dataBag.InventoryItem.InServiceDate),
                              () => string.Format(GetRandomValue(WarrantyFormatStrings),
                                                  dataBag.ConsInfo.GetGoodWarrantyDescriptions(
                                                      preferences.GoodWarrantyMileageRemaining,
                                                      dataBag.InventoryItem.MileageReceived,
                                                      dataBag.InventoryItem.InServiceDate, 1)),
                             2, 0.3,
                             PreviewGroupType.RiskReduction, BlurbCategory.Regular, new Guid("{A15A1180-0EE7-4003-9E0D-AA3021B85144}"))
                );


            //add marketing text - taking the most appropriate first
            if (dataBag.HasMarketingPreviewSuperlative)
            {
                _textList.Add(new PreviewItem(
                                  () => previewPrefs.UseExpertReviews,
                                  () => dataBag.MarketingPreview,
                                 2, 0.75,
                                 PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{41AD37C6-A85A-4372-83EC-B0BB0538B03A}"))
                    );
            }
            else if (dataBag.HasMarketingSuperlative)
            {
                _textList.Add(new PreviewItem(
                                  () => previewPrefs.UseExpertReviews,
                                  () => dataBag.MarketingSuperlative,
                                 2, 0.45,
                                 PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{F1631DDE-1CE2-4b31-9A87-4EAE73B3A5C5}"))
                    );
            }
            else if (dataBag.HasMarketingData)
            {
                _textList.Add(new PreviewItem(
                                  () => previewPrefs.UseExpertReviews,
                                  () => dataBag.MarketingSentence,
                                 2, 0.3,
                                 PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{4C6EF286-2977-492e-BEAB-F0D8BFD6F7D9}"))
                    );
            }

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseTrim && dataBag.HasTrim,
                              () =>
                              {
                                  string trim = dataBag.Trim;
                                  if (!string.IsNullOrEmpty(trim))
                                  {
                                      trim += " trim";
                                  }
                                  return trim;
                              },
                             2, 0.3,
                             PreviewGroupType.VehicleBasics, BlurbCategory.Regular, new Guid("{01B9E68D-16D1-4db2-A869-76A86238EA39}"))
                );

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseColors && dataBag.HasColors,
                              () => dataBag.Colors,
                             2, 0.3,
                             PreviewGroupType.VehicleBasics, BlurbCategory.Regular, new Guid("{DDA49BEF-861D-4c3b-AF5F-AEB6660A6DEB}"))
                );
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseKeyInfoCheckboxes && dataBag.HasKeyInformation,
                              () => dataBag.AdditionalInformation,
                             2, 0.5,
                             PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{A25D90FA-9537-4920-9998-D98DC0DDF95C}"))
                );

            Log.DebugFormat("_textList has been loaded with all {0} PreviewItems.", _textList.Count);
        }

        private void LoadNewPreviewItems(IVehicleTemplatingData dataBag, IDealerAdvertisementPreferences preferences)
        {
            Log.DebugFormat("LoadNewPreviewItems() called with dataBag: {0} and preferences: {1}", dataBag, preferences);

            // should it really just be pulling from dataBag as in UsedPreview?  new and scared to change, tureen 11/4/2013
            IPreviewPreferences previewPrefs = PreviewPreferencesDAO.Fetch(preferences.BusinessUnitId, 1);
            
            Log.DebugFormat("The IPreviewPreference that will be used is {0}", previewPrefs);

            //clear the list of values
            _textList = new List<PreviewItem>();

            //begin building list
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseModelAwards && dataBag.HasAwards,
                              () => dataBag.BestModelAward,
                             1, 0.8,
                             PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{4C4D10AE-C02A-44dc-91FE-AC4FD765266D}"))
                );

            //try the fuel economy
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseFuelEconomy && dataBag.GetsGoodGasMileage,
                              () => string.Format(GetRandomValue(FuelEconomyFormatStrings), dataBag.FuelHwy,
                                                  dataBag.FuelCity),
                             1, 0.7,
                             PreviewGroupType.Affordability, BlurbCategory.Regular, new Guid("{3E459F80-5C9A-49d3-ADDC-315CBA281D3D}"))
                );
            

            LoadPreviewEquipmentIndividually(previewPrefs, dataBag);

            //financing
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseFinancingSpecial && dataBag.HasFinancingAvailable,
                              () => dataBag.SpecialFinancing,
                             1, 0.8,
                             PreviewGroupType.Affordability, BlurbCategory.Regular, new Guid("{C165E302-57E4-4b65-8517-4E83C77F733D}"))
                );

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseJdPowerRatings && dataBag.HasGoodJdPowerRatings,
                              () => dataBag.JdPowerRatingPreview,
                             1, 0.6,
                             PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{52790237-4965-4443-947C-7EC2E6F723B0}"))
                );

            //safety
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseCrashTestRatings && dataBag.HasGoodCrashTestRatings,
                              () => dataBag.ConsInfo.GetGoodCrashRatingText(preferences.GoodCrashTestRating, 1,
                                                                            false, true),
                             2, 0.3,
                             PreviewGroupType.Safety, BlurbCategory.Regular, new Guid("{788A31D8-9424-4585-A325-4B773991B891}"))
                );

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseWarranties &&
                                    dataBag.ConsInfo.HasGoodWarrantyRemaining(
                                        preferences.GoodWarrantyMileageRemaining,
                                        dataBag.InventoryItem.MileageReceived,
                                        dataBag.InventoryItem.InServiceDate),
                              () => string.Format(GetRandomValue(WarrantyFormatStrings),
                                                  dataBag.ConsInfo.GetGoodWarrantyDescriptions(
                                                      preferences.GoodWarrantyMileageRemaining,
                                                      dataBag.InventoryItem.MileageReceived,
                                                      dataBag.InventoryItem.InServiceDate, 1)),
                             2, 0.3,
                             PreviewGroupType.RiskReduction, BlurbCategory.Regular, new Guid("{5F5AA3DA-0692-4818-BB43-82736C2B97A6}"))
                );


            //add marketing text - taking the most appropriate first
            if (dataBag.HasMarketingPreviewSuperlative)
            {
                _textList.Add(new PreviewItem(
                                  () => previewPrefs.UseExpertReviews,
                                  () => dataBag.MarketingPreview,
                                 2, 0.75,
                                 PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{EB534BAA-D9D0-402c-9BA5-0B3CC0B8B535}"))
                    );
            }
            else if (dataBag.HasMarketingSuperlative)
            {
                _textList.Add(new PreviewItem(
                                  () => previewPrefs.UseExpertReviews,
                                  () => dataBag.MarketingSuperlative,
                                 2, 0.45,
                                 PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{58829C26-8EA8-4888-8E31-2305F3914F8F}"))
                    );
            }
            else if (dataBag.HasMarketingData)
            {
                _textList.Add(new PreviewItem(
                                  () => previewPrefs.UseExpertReviews,
                                  () => dataBag.MarketingSentence,
                                 2, 0.3,
                                 PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{3EB06E80-A744-4962-8BD5-AFC6332ADC06}"))
                    );
            }

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseTrim && dataBag.HasTrim,
                              () =>
                              {
                                  string trim = dataBag.Trim;
                                  if (!string.IsNullOrEmpty(trim))
                                  {
                                      trim += " trim";
                                  }
                                  return trim;
                              },
                             1, 0.9,
                             PreviewGroupType.VehicleBasics, BlurbCategory.Regular, new Guid("{5A0B2111-716C-46b4-96E4-E30B9C70325C}"))
                );

            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseColors && dataBag.HasColors,
                              () => dataBag.Colors,
                             1, 0.9,
                             PreviewGroupType.VehicleBasics, BlurbCategory.Regular, new Guid("{1E13C8DE-7795-46b7-A114-F606218430AB}"))
                );
            _textList.Add(new PreviewItem(
                              () => previewPrefs.UseKeyInfoCheckboxes && dataBag.HasKeyInformation,
                              () => dataBag.AdditionalInformation,
                             2, 0.5,
                             PreviewGroupType.KeyFeatures, BlurbCategory.Regular, new Guid("{60D4A020-6947-4be3-B0D9-16724385580F}"))
                );

            Log.DebugFormat("_textList has been loaded with all {0} PreviewItems.", _textList.Count);
        }

        private void LoadPreviewEquipmentIndividually(IPreviewPreferences previewPrefs, IVehicleTemplatingData dataBag)
        {
            Log.DebugFormat("LoadPreviewEquipmentIndividually called with previewPrefs: {0} and dataBag: {1}", previewPrefs, dataBag);

            IList<string> encountered = new List<string>();

            // Neccessary null checks - law of demeter, anyone ???
            if (dataBag != null && dataBag.VehicleConfig != null && 
                dataBag.VehicleConfig.VehicleOptions != null &&  dataBag.VehicleConfig.VehicleOptions.options != null )
            {
                Log.DebugFormat("DetailedEquipment count is {0}", dataBag.VehicleConfig.VehicleOptions.options.Count);

                // Get Option names first.
                foreach( DetailedEquipment option in dataBag.VehicleConfig.VehicleOptions.options)
                {
                    // don't access modified closure.
                    string previewText = option.OptionTextPreview;

                    var item = new PreviewItemNonRandom(() => previewPrefs.UseKeyVehicleEquipment,
                                                        () => previewText,
                                                        1,
                                                        0.01, //0.75,
                                                        PreviewGroupType.KeyFeatures);

                    _textList.Add( item );
                    Log.DebugFormat(
                        "Adding PreviewItemNonRandom {0} to _textList.  This is an option package aka DetailedEquipment.", item);

                    encountered.Add( previewText );
                    Log.DebugFormat("Detailed option preview text {0} added to encountered list.", previewText);
                }
                
            }
            else
            {
                Log.DebugFormat("We aren't going to process DetailedEquipment aka Option Packages because one or more of the following are null: dataBag, dataBag.VehicleConfig, dataBag.VehicleConfig.VehicleOptions, dataBag.VehicleConfig.VehicleOptions.options");
            }

            // load each preview equipment into the text list
            if (dataBag == null)
            {
                Log.Warn("dataBag (IVehicleTemplatingData) is null.  We are returning early.");
                return;
            }

            if( dataBag.PreviewEquipment == null )
            {
                Log.Warn("dataBag.PreviewEquipment (a list of strings of generic options) is null.  We are returning early.");
                return;                
            }
            
            Log.DebugFormat("dataBag.PreviewEquipment contains {0} items.", dataBag.PreviewEquipment.Count);

            foreach( var equip in dataBag.PreviewEquipment )
            {
                // Add it only if we don't already have something similar.
                if (encountered.Contains(equip.Description, new SimilarStringComparer()))
                {
                    Log.DebugFormat("The 'encountered' list already contains an item which is similar to the current string, {0}.  We will not add the current string, as it would seem redundant.", equip);
                    continue;
                }

                // Don't access modified closure
                string equipText = equip.Description;

                PreviewItem item;
                if(equip.MustInclude)
                {
                    item = new PreviewItemNonRandom(
                                () => previewPrefs.UseKeyVehicleEquipment,
                                () => equipText,
                                1, //Keep 1st tier so this group doesn't start preview
                                1, //want high non random priority
                                PreviewGroupType.KeyFeatures);
                    
                }
                else
                {
                    item = new PreviewItemNonRandom(
                               () => previewPrefs.UseKeyVehicleEquipment,
                               () => equipText,
                               1,
                               0.01,
                               PreviewGroupType.KeyFeatures);
                }

                _textList.Add(item);
                Log.DebugFormat(
                    "Adding PreviewItemNonRandom {0} to _textList.  This is a generic option.", item);

                encountered.Add( equipText );
                Log.DebugFormat("Generic option preview text {0} added to encountered list.", equipText);
            }
        }

        public string GetPreview(IDealerAdvertisementPreferences Preferences,
                                 ThemeType theme)
        {
            return GetPreview(Preferences, theme, false);
        }


        private void SetEditedItems()
        {
            var editedPreviewItems = _templateMediator.GetPreviewItems().Where(x => x.Edited);
            int editedOrder = 1;

            var listWithPreview = new List<PreviewItem>(); //include call to action in search
            listWithPreview.AddRange(_textList);
            listWithPreview.Add(_callToAction);

            foreach(var previewItem in editedPreviewItems)
            {
                var items = listWithPreview.Where(x => x.ID == previewItem.Rule.ID).ToList();
                if(items != null && items.Count > 0)
                {
                    if(items.Count == 1)
                    {
                        items[0].Edited = true;
                        items[0].EditedText = previewItem.Text;
                        items[0].EditedOrder = editedOrder;
                    }
                    else
                    {
                        var first = items[0];
                        first.Edited = true;
                        first.EditedText = previewItem.Text;
                        first.EditedOrder = editedOrder;

                        _textList.RemoveAll(x => x.ID == previewItem.Rule.ID);
                        _textList.Add(first);
                    }
                  
                    editedOrder++;
                }
            }
        }

        public string GetPreview(IDealerAdvertisementPreferences Preferences,
                                 ThemeType theme, bool allowTruncation)
        {
            var dataBag = _templateMediator.VehicleData;
            Log.DebugFormat(
                "GetPreview() called with IVehicleTemplatingData: {0}, IDealerAdvertisementPreferences: {1}, ThemeType: {2}, and allowTruncation: {3}",
                dataBag, Preferences, theme, allowTruncation);

            // GET a separator
            _separator = GetRandomValue(SeparatorStrings);
            Log.DebugFormat("Separator set to {0}", _separator);

            //load the preview set
            if (dataBag.InventoryItem.IsNew())
            {
                LoadNewPreviewItems(dataBag, Preferences);
            }
            else
            {
                LoadUsedPreviewItems(dataBag, Preferences);
            }

           
            SetEditedItems();
            //now that we have an eligible list, take a permutation and render...targeting the desired length
            List<IWeightedItem> myVals = RandomWeightedPermutation(_textList.Cast<IWeightedItem>().ToList());

            
            //GET THE closing preview item
            string callToAction = string.Empty;
            if (Preferences.UseCallToActionInPreview)
            {
                callToAction =_callToAction.ToString().Trim();
            }

            //convert textList into a set of grouped descriptions ready for concatenation
            int tgtLen = Preferences.DesiredPreviewLength - callToAction.Length;

            // create string builder with preview set

            // Get group strings and preview items.
            IList<string> groupStrings;
            IList<Core.AdvertisementModel.PreviewItem> previewItems;

            // Render the preview.
            GetRenderedPreviewItems(myVals, tgtLen, allowTruncation, out groupStrings, out previewItems);

            var itemText = previewItems.Select(x => x.Text);
            var sb = new StringBuilder(string.Join("", itemText.ToArray()));

            // truncate the string to the desired length (with ellipsis on end followed by callToAction)
            if (allowTruncation)
            {
                sb.Length = Math.Min(tgtLen - 4, sb.Length);
                sb.Append("...");
            }

            sb.Append(callToAction);

            if (Preferences.UseCallToActionInPreview) //add call to action only if they want it
            {
                var previewItem = new Core.AdvertisementModel.PreviewItem() {Text = callToAction, Edited = _callToAction.Edited};
                previewItem.Children.Add(new Rule() {ID = _callToAction.ID});
                previewItems.Add(previewItem);
            }
            else //tack on the padding to the last item
            {
                if(previewItems.Count > 0)
                    previewItems.Last().Text = previewItems.Last().Text + callToAction; //here call to action is just spaces
            }

            // Produce the final preview text.
            sb = FirstLetterUppercase(sb);
            var finalPreviewText =  sb.ToString();

            PreviewFormatter formatter = new PreviewFormatter();
            finalPreviewText = formatter.Format( finalPreviewText );

            CleanupPreviewItems(previewItems);
            // Inform the mediator about this preview items.
            _templateMediator.RegisterPreviewItems( previewItems );

            // Return the final preview text.
            return finalPreviewText;
        }

        internal static void CleanupPreviewItems(IList<Core.AdvertisementModel.PreviewItem> previewItems)
        {
            //Clean up double commas. Not sure why they are happening. Releated to some kind of server state. Bugzid: 18720

            foreach (var previewItem in previewItems)
            {
                previewItem.Text = Regex.Replace(previewItem.Text, @"\s*,+", ",", RegexOptions.IgnoreCase);
                previewItem.Text = Regex.Replace(previewItem.Text, @"(\d,)(\s+)(\d)", "$1$3", RegexOptions.IgnoreCase);
            }
        }

        /// <summary>
        /// convert the weighted items into a set of descriptions grouped by previewGroupType
        /// </summary>
        /// <param name="myVals"></param>
        /// <param name="targetLength"></param>
        /// <param name="orderedGroups"></param>
        /// <returns></returns>
        private Dictionary<PreviewGroupType, IList<PreviewItem>> ProcessPreviewItems(IEnumerable<IWeightedItem> myVals,
                                                                                           int targetLength,
                                                                                           ref List<PreviewGroupType> orderedGroups,
                                                                                           bool allowTruncation)
        {
            var previewItemsByGroup = new Dictionary<PreviewGroupType, IList<PreviewItem>>();

            //len tracks the total length of text generated thus far
            int len = 0;
            foreach (IWeightedItem item in myVals)
            {
                var previewItem = (PreviewItem) item; // unsafe cast.  zb
                
                if (!previewItemsByGroup.ContainsKey(previewItem.GroupType))
                {
                    previewItemsByGroup.Add(previewItem.GroupType, new List<PreviewItem>() );
                    orderedGroups.Add(previewItem.GroupType);
                }

                // process the item into a string so we can see how long it is.
                string txt = previewItem.EstimateSize(INTERNAL_SEPARATOR);

                if ( string.IsNullOrWhiteSpace( txt ) ) continue;

                if (allowTruncation)
                {
                    //add that text item to the sections set
                    previewItemsByGroup[previewItem.GroupType].Add( previewItem ); //Append(txt);
                    len += txt.Length;

                    //when we exceed the target, break
                    if (len >= targetLength) break;
                }
                else //truncation not allowed, so we'll only add if it fits
                {
                    if (len + txt.Length <= targetLength) //test the potential length...only use if short enough
                    {
                        previewItemsByGroup[previewItem.GroupType].Add( previewItem ); //Append(txt);
                        len += txt.Length;

                        //as soon as long enough, break out
                        //we use 12 as a rough size constraint, b/c there aren't many phrases
                        //that would be useful and less than 12 chars "BMW Certified" alone is 13 chars
                        //this prevents needless rendering operations that consume resources
                        if (len >= targetLength - 12) break;
                    }
                }
            }            

            return previewItemsByGroup;
        }


        /// <summary>
        /// Take an enumerable weighted items and turn into a list of strings
        /// </summary>
        /// <param name="myVals"></param>
        /// <param name="targetLength"></param>
        /// <param name="allowTruncation"></param>
        /// <param name="groupStrings"></param>
        /// <param name="previewItems"></param>
        /// <returns></returns>
        private void GetRenderedPreviewItems(IEnumerable<IWeightedItem> myVals, int targetLength,
                                                     bool allowTruncation, out IList<string> groupStrings,
                                                     out IList<Core.AdvertisementModel.PreviewItem> previewItems)
        {
            //get the needed items processed and get the groups in the order they should be rendered
            //ordered groups contains the PreviewGroupTypes in the order they should be rendered
            var orderedGroups = new List<PreviewGroupType>();
            var previewItemsByGroup =  ProcessPreviewItems(myVals, targetLength, ref orderedGroups, allowTruncation);

            var outputStrings = new List<string>();
            var outputItems = new List<Core.AdvertisementModel.PreviewItem>();

            // join groups in desired order w/ group separators in between...
            foreach (PreviewGroupType groupType in orderedGroups)
            {
                if (previewItemsByGroup.ContainsKey(groupType) && previewItemsByGroup[groupType].Count > 0)
                {
                    StringBuilder groupStringBuilder = new StringBuilder();
                    foreach( var item in previewItemsByGroup[groupType] )
                    {
                        // ignore empty items.
                        if( item.ToString(INTERNAL_SEPARATOR).Trim().Length > 0 )
                        {
                            groupStringBuilder.Append(item.ToString(INTERNAL_SEPARATOR));

                            var previewItem = new Core.AdvertisementModel.PreviewItem();
                            if(item.BlurbCategory == BlurbCategory.Price)
                                previewItem = new PricePreviewItem();
                                
                            previewItem.Text = item.ToString(INTERNAL_SEPARATOR);
                            previewItem.Edited = item.Edited;
                            previewItem.Children.Add(new Rule(){ID=item.ID});

                            var existingPreviewItem = outputItems.Where(x => x.Rule.ID == previewItem.Rule.ID).SingleOrDefault();
                            if (existingPreviewItem != null)
                                existingPreviewItem.Text = existingPreviewItem.Text.TrimEnd() + ", " + previewItem.Text;
                            else
                                outputItems.Add(previewItem);                            
                        }
                    }

                    if (outputItems.Count > 0 && groupStringBuilder.Length > 0)
                    {
                        outputItems.Last().Text = outputItems.Last().Text.TrimEnd(INTERNAL_SEPARATOR.ToCharArray());

                        string groupEndString = outputItems.Last().Text;
                        if(groupType != orderedGroups.Last() && !groupEndString.TrimEnd().EndsWith("."))
                            groupEndString = outputItems.Last().Text + ".";

                        outputItems.Last().Text = TextFragmentFormatter.CleanUpIrregularities(groupEndString); //Put in periods to end groups
                        outputItems.Last().Text = outputItems.Last().Text + " "; //final space 
                    }

                    outputStrings.Add(groupStringBuilder.ToString().TrimEnd(INTERNAL_SEPARATOR.ToCharArray()));
                }
            }


            //Capitialize first letter
            if (outputItems.Count > 0 && outputItems.First().Text.Length > 0)
            {
                var builder = new StringBuilder(outputItems.First().Text);
                builder[0] = char.ToUpper(builder[0]);
                outputItems.First().Text = builder.ToString();
            }

            // Assign outputs.
            groupStrings = outputStrings;
            previewItems = outputItems;
        }

        private StringBuilder FirstLetterUppercase(StringBuilder inStr)
        {
            //make sure first letter is caps
            if (inStr.Length > 0)
            {
                inStr[0] = char.ToUpper(inStr[0]);
            }
            return inStr;
        }
    }
}