﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public class PreviewEquipmentItemDuplicateRemover
    {
        private readonly GenericEquipmentReplacementCollection _replacementRules;
        private readonly List<PreviewEquipmentItem> _previewItems;
        private List<PreviewEquipmentItem> _filteredList;
        private List<int> _excludedCategories;

        public PreviewEquipmentItemDuplicateRemover (GenericEquipmentReplacementCollection replacementRules, List<PreviewEquipmentItem> previewItems)
        {
            _replacementRules = replacementRules;
            _previewItems = previewItems;
        }

        public List<PreviewEquipmentItem> RemoveDuplicates ()
        {
            if ( !ValidatePreviewItems() ) return _previewItems;
            if ( !ValidateReplacementRules() ) return _previewItems;

            ApplyGenericEquipmentReplacementRules();
            return _filteredList;
        }

        private bool ValidateReplacementRules ()
        {
            return _replacementRules != null && _replacementRules.Count > 0;
        }

        private bool ValidatePreviewItems ()
        {
            return _previewItems != null && _previewItems.Count > 0;
        }

        private void ApplyGenericEquipmentReplacementRules ()
        {
            InitialSetup();
            var replacementPreviewItems = CreateReplacementPreviewItemsForApplicableRules();
            AddPreviewItemsToFilteredList();
            _filteredList.AddRange( replacementPreviewItems );
        }

        private void InitialSetup ()
        {
            _replacementRules.SortByCategoryIdCountDesc();
            _filteredList = new List<PreviewEquipmentItem>();
            _excludedCategories = new List<int>();
        }

        private void AddPreviewItemsToFilteredList ()
        {
            foreach ( var previewEquipmentItem in _previewItems )
            {
                if ( previewEquipmentItem is PreviewGenericEquipmentItem )
                {
                    ProcessPreviewGenericEquipmentItem( previewEquipmentItem as PreviewGenericEquipmentItem);
                }
                else
                {
                    ProcessPreviewEquipmentItem(previewEquipmentItem);
                }
            }
        }

        private void ProcessPreviewGenericEquipmentItem (PreviewGenericEquipmentItem previewGenericEquipmentItem)
        {
            if ( ExcludedCategoriesContainCategories( previewGenericEquipmentItem.IncludedCategoryIds ) ) return;
            _filteredList.Add( new PreviewEquipmentItem( previewGenericEquipmentItem.Description, previewGenericEquipmentItem.MustInclude ) );
            _excludedCategories.AddRange( previewGenericEquipmentItem.IncludedCategoryIds );
        }

        private void ProcessPreviewEquipmentItem (PreviewEquipmentItem previewEquipmentItem)
        {
            _filteredList.Add( new PreviewEquipmentItem( previewEquipmentItem.Description, previewEquipmentItem.MustInclude ) );
        }

        

        private IEnumerable<PreviewEquipmentItem> CreateReplacementPreviewItemsForApplicableRules ()
        {
            var retList = new List<PreviewEquipmentItem>();
            var ruleCount = _replacementRules.Count;
            for ( var i = 0; i < ruleCount; ++i )
            {
                var gme = _replacementRules.Item( i );
                if ( !DoPreviewItemsIncludeRule( gme ) || gme.IsWhollyContained( _excludedCategories ) ) continue;
                retList.Add( new PreviewEquipmentItem( gme.Description, true ) );
                _excludedCategories.AddRange( from CategoryLink category in gme.IncludesCategories
                                              select category.CategoryID );
            }
            return retList;
        }

        private bool DoPreviewItemsIncludeRule (GenericEquipmentReplacement rule)
        {
            return PreviewItemsContainIncludedCateogries( rule ) &&
                   PreviewItemsDoNotContainExcludedCategories(rule);

        }

        private bool PreviewItemsContainIncludedCateogries(GenericEquipmentReplacement rule)
        {
            return rule.IncludesCategories.Cast<CategoryLink>().All(category => PreviewItemsContainCategory(category.CategoryID));
        }

        private bool PreviewItemsDoNotContainExcludedCategories (GenericEquipmentReplacement rule)
        {
            return rule.ExcludesCategories.Cast<CategoryLink>().All(category => !PreviewItemsContainCategory(category.CategoryID));
        }

        private bool PreviewItemsContainCategory (int categoryId)
        {
            foreach ( var item in _previewItems )
            {
                if (!(item is PreviewGenericEquipmentItem)) continue;
                var previewGenericEquipmentItem = item as PreviewGenericEquipmentItem;
                if ( previewGenericEquipmentItem.IncludedCategoryIds.Contains( categoryId ) )
                {
                    return true;
                }
            }
            return false;

        }
        private bool ExcludedCategoriesContainCategories (IEnumerable<int> cateogryIds)
        {
            return cateogryIds.All( cateogryId => _excludedCategories.Contains( cateogryId ) );
        }

    }
}
