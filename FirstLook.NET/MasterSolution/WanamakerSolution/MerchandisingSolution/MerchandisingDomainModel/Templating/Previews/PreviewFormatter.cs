using System.Text.RegularExpressions;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    internal class PreviewFormatter : ITextFragmentFormatter
    {
        public string Format( string inString )
        {
            int length = inString.Length;

            inString = TextFragmentFormatter.CleanUpIrregularities( inString );

            //bool containsEllipses = Regex.IsMatch( inString, @"\.\.\." );
            //if( containsEllipses )
            //{
            //    Match match = Regex.Match( inString, "\\.[\\s]+$", RegexOptions.RightToLeft | RegexOptions.Multiline );
            //    if( match.Index > 0 )
            //    {
            //        inString = inString.Insert( match.Index, ".." );
            //    }
            //}

            if( inString.Length > length )
            {
                inString = inString.Substring( 0, length );
            }
            else if( inString.Length < length )
            {
                inString = inString.PadRight( length, ' ' );
            }

            return inString;
        }
    }
}