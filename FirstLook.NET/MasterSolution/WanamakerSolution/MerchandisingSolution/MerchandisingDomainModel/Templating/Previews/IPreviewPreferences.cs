namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public interface IPreviewPreferences
    {
        int BusinessUnitId { get; set; }
        bool UseDrivetrain { get; set; }
        int PreferenceId { get; set; }
        int NewOrUsed { get; set; }
        bool UseMissionMarker { get; set; }
        bool UsePriceBelowBook { get; set; }
        bool UseOneOwner { get; set; }
        bool UsePriceReduced { get; set; }
        bool UseCertified { get; set; }
        bool UseVehicleCondition { get; set; }
        bool UseVehicleConditionStatements { get; set; }
        bool UseJdPowerRatings { get; set; }
        bool UseModelAwards { get; set; }
        bool UseExpertReviews { get; set; }
        bool UseCrashTestRatings { get; set; }
        bool UseWarranties { get; set; }
        bool UseFuelEconomy { get; set; }
        bool UseLowMilesPerYear { get; set; }
        bool UseFinancingSpecial { get; set; }
        bool UseTrim { get; set; }
        bool UseColors { get; set; }
        bool UseKeyInfoCheckboxes { get; set; }
        bool UseKeyVehicleEquipment { get; set; }
    }
}