﻿using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews.DAL
{
    public class PreviewPreferencesDao : IPreviewPreferencesDao
    {

        /// <summary>
        /// Fetch the preview preferences for a dealership
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="newOrUsed">1 for New, 2 for Used Vehicles</param>
        /// <returns></returns>
        public IPreviewPreferences Fetch(int businessUnitId, int newOrUsed)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.PreviewPreferences#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "newOrUsed", newOrUsed, DbType.Int32);

                    con.Open();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            PreviewPreferences pp = new PreviewPreferences(
                                reader.GetInt32(reader.GetOrdinal("preferenceId")),
                                reader.GetInt32(reader.GetOrdinal("businessUnitId")),
                                reader.GetInt32(reader.GetOrdinal("newOrUsed")),
                                reader.GetBoolean(reader.GetOrdinal("useMissionMarker")),
                                reader.GetBoolean(reader.GetOrdinal("usePriceBelowBook")),
                                reader.GetBoolean(reader.GetOrdinal("useOneOwner")),
                                reader.GetBoolean(reader.GetOrdinal("usePriceReduced")),
                                reader.GetBoolean(reader.GetOrdinal("useCertified")),
                                reader.GetBoolean(reader.GetOrdinal("useVehicleCondition")),
                                reader.GetBoolean(reader.GetOrdinal("useVehicleConditionStatements")),
                                reader.GetBoolean(reader.GetOrdinal("useJdPowerRatings")),
                                reader.GetBoolean(reader.GetOrdinal("useModelAwards")),
                                reader.GetBoolean(reader.GetOrdinal("useExpertReviews")),
                                reader.GetBoolean(reader.GetOrdinal("useCrashTestRatings")),
                                reader.GetBoolean(reader.GetOrdinal("useWarranties")),
                                reader.GetBoolean(reader.GetOrdinal("useFuelEconomy")),
                                reader.GetBoolean(reader.GetOrdinal("useLowMilesPerYear")),
                                reader.GetBoolean(reader.GetOrdinal("useFinancingSpecial")),
                                reader.GetBoolean(reader.GetOrdinal("useTrim")),
                                reader.GetBoolean(reader.GetOrdinal("useColors")),
                                reader.GetBoolean(reader.GetOrdinal("useKeyInfoCheckboxes")),
                                reader.GetBoolean(reader.GetOrdinal("useKeyVehicleEquipment")),
                                reader.GetBoolean(reader.GetOrdinal("useDrivetrain"))
                                );
                            pp.BusinessUnitId = businessUnitId; //set to requesting buid in case it came from default
                            return pp;
                        }
                        return new PreviewPreferences(businessUnitId, newOrUsed);
                    }
                }
            }
        }


        public void Upsert(IPreviewPreferences preferences)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.PreviewPreferences#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "preferenceId", preferences.PreferenceId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "businessUnitId", preferences.BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "newOrUsed", preferences.NewOrUsed, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "useMissionMarker", preferences.UseMissionMarker, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "usePriceBelowBook", preferences.UsePriceBelowBook, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useOneOwner", preferences.UseOneOwner, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "usePriceReduced", preferences.UsePriceReduced, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useCertified", preferences.UseCertified, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useVehicleCondition", preferences.UseVehicleCondition, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useVehicleConditionStatements", preferences.UseVehicleConditionStatements, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useJdPowerRatings", preferences.UseJdPowerRatings, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useModelAwards", preferences.UseModelAwards, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useExpertReviews", preferences.UseExpertReviews, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useCrashTestRatings", preferences.UseCrashTestRatings, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useWarranties", preferences.UseWarranties, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useFuelEconomy", preferences.UseFuelEconomy, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useLowMilesPerYear", preferences.UseLowMilesPerYear, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useFinancingSpecial", preferences.UseFinancingSpecial, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useTrim", preferences.UseTrim, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useColors", preferences.UseColors, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useKeyInfoCheckboxes", preferences.UseKeyInfoCheckboxes, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useKeyVehicleEquipment", preferences.UseKeyVehicleEquipment, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useDrivetrain", preferences.UseDrivetrain, DbType.Boolean);

                    con.Open();
                    //upsert data
                    cmd.ExecuteNonQuery();
                }
            }
        }


        // Needed for ODS binding.
        public void Upsert(bool useMissionMarker, bool usePriceBelowBook, bool useOneOwner, bool usePriceReduced,
            bool useCertified, bool useVehicleCondition, bool useVehicleConditionStatements, bool useJdPowerRatings,
            bool useModelAwards, bool useExpertReviews, bool useCrashTestRatings, bool useWarranties,
            bool useFuelEconomy, bool useLowMilesPerYear, bool useFinancingSpecial, bool useTrim, bool useColors,
            bool useKeyInfoCheckboxes, bool useKeyVehicleEquipment, bool useDrivetrain, int PreferenceId, 
            int NewOrUsed, int BusinessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.PreviewPreferences#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "preferenceId", PreferenceId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "businessUnitId", BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "newOrUsed", NewOrUsed, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "useMissionMarker", useMissionMarker, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "usePriceBelowBook", usePriceBelowBook, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useOneOwner", useOneOwner, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "usePriceReduced", usePriceReduced, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useCertified", useCertified, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useVehicleCondition", useVehicleCondition, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useVehicleConditionStatements", useVehicleConditionStatements, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useJdPowerRatings", useJdPowerRatings, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useModelAwards", useModelAwards, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useExpertReviews", useExpertReviews, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useCrashTestRatings", useCrashTestRatings, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useWarranties", useWarranties, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useFuelEconomy", useFuelEconomy, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useLowMilesPerYear", useLowMilesPerYear, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useFinancingSpecial", useFinancingSpecial, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useTrim", useTrim, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useColors", useColors, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useKeyInfoCheckboxes", useKeyInfoCheckboxes, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useKeyVehicleEquipment", useKeyVehicleEquipment, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "useDrivetrain", useDrivetrain, DbType.Boolean);

                    con.Open();
                    //upsert data
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
