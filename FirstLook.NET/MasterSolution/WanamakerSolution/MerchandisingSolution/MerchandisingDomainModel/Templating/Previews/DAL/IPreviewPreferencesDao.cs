namespace FirstLook.Merchandising.DomainModel.Templating.Previews.DAL
{
    public interface IPreviewPreferencesDao
    {
        /// <summary>
        /// Fetch the preview preferences for a dealership
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="newOrUsed">1 for New, 2 for Used Vehicles</param>
        /// <returns></returns>
        IPreviewPreferences Fetch(int businessUnitId, int newOrUsed);

        /// <summary>
        /// Performs an insert or update.  Commonly known as "save". Sigh.
        /// </summary>
        /// <param name="preferences"></param>
        void Upsert(IPreviewPreferences preferences);
    }
}