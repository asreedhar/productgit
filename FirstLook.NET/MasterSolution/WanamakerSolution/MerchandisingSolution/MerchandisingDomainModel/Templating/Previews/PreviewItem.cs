using System;
using FirstLook.Common.Core.DTO;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public class PreviewItem : IWeightedItem, ISurrogateKey, IPreviewItem
    {
        private readonly double _weight;
        private readonly int _tierNumber;
        private string _text;
        private readonly PreviewGroupType _groupType;
        private readonly BlurbCategory _blurbCategory;
        private readonly Guid _id;

        public delegate string PreviewDataProcessor();
        public delegate bool PreviewConditionProcessor();

        private PreviewDataProcessor _textProcessor;
        private PreviewConditionProcessor _conditionProcessor;
        //private string separator;
        private bool _disableSeparator;

        public PreviewItem(PreviewConditionProcessor conditionProcessor, PreviewDataProcessor textProcessor, int tierNumber, double weight,
                           PreviewGroupType groupType, BlurbCategory blurbCategory, Guid id)
            : this(conditionProcessor, textProcessor, tierNumber, weight, groupType, blurbCategory, id, false)
        {
            
        }
        public PreviewItem(PreviewConditionProcessor conditionProcessor, PreviewDataProcessor processor, int tierNumber, double weight, 
                           PreviewGroupType groupType, BlurbCategory blurbCategory, Guid id, bool disableSeparator)
        {
            _conditionProcessor = conditionProcessor;
            _textProcessor = processor;
            _tierNumber = tierNumber;
            _weight = weight;
            _groupType = groupType;
            _blurbCategory = blurbCategory;
            _id = id;
            _disableSeparator = disableSeparator;
            Edited = false;
            EditedOrder = 0;
            EditedText = String.Empty;
            // Add key
        }

        public bool Edited { get; set; }
        public string EditedText { get; set; }
        public int EditedOrder { get; set; }

        public BlurbCategory BlurbCategory
        {
            get
            {
                return _blurbCategory;
            }
        }

        public Guid ID
        {
            get
            {
                return _id;
            }
        }

        public double GetWeight()
        {
            return _weight;
        }

        public int GetTierNumber()
        {
            return _tierNumber;
        }

        private double randomWt = -1;
        public PreviewGroupType GroupType
        {
            get
            {
                return _groupType;
            }
        }
        public virtual double GetRandomizedWeight(Random random)
        {
            if (randomWt < 0)
            {
                randomWt = random.NextDouble() * GetWeight();
            }
            return randomWt;
        }

        public int CompareTo(IWeightedItem other)
        {
            return _weight.CompareTo(other.GetWeight());
        }

        public virtual string EstimateSize(string separator)
        {
            return ToString(separator);
        }

        public virtual string ToString(string separator)
        {
            if (Edited)
                return EditedText;

            if (!_conditionProcessor())
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(_text))
            {
                _text = _textProcessor();

                //only add the separator if text has data
                if (!_disableSeparator && !string.IsNullOrEmpty(_text))
                {
                    _text += separator;
                }
            }
            return _text;
            
        }
   
        public override string ToString()
        {
            return ToString(" ");
        }

        #region Implementation of ISurrogateKey

        public string Key { get; set; }

        #endregion
    }
}
