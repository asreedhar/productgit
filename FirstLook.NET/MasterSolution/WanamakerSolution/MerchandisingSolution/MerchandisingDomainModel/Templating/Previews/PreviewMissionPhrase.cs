using System.Configuration;
using MerchandisingLibrary;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    public class PreviewMissionPhrase
    {
        public static PreviewMissionPhrase Fetch(int businessUnitId)
        {
            return new PreviewMissionPhrase(BlurbTextCollection.FetchTextSamples(businessUnitId,
                                                 int.Parse(ConfigurationManager.AppSettings["MissionPreviewBlurbId"])));
        }

        private readonly BlurbTextCollection _sampleText;
        public PreviewMissionPhrase(BlurbTextCollection collection)
        {
            _sampleText = collection;
        }
        public bool HasPhrases()
        {
            return _sampleText.Count > 0;
        }
        public string GetPreviewPhrase(int? themeId)
        {
            BlurbText text = _sampleText.GetRandomPhrase(themeId);
            if (text != null)
            {
                return text.Render(string.Empty);
            }
            return string.Empty;
        }
    }
}
