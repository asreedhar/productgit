namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    internal class PreviewPreferences : IPreviewPreferences
    {
        public PreviewPreferences()
        {
        }

        public PreviewPreferences(int businessUnitId, int newOrUsed)
        {
            BusinessUnitId = businessUnitId;
            NewOrUsed = newOrUsed;
        }

        public PreviewPreferences(int preferenceId, int businessUnitId, int newOrUsed, bool useMissionMarker,
                                  bool usePriceBelowBook, bool useOneOwner, bool usePriceReduced, bool useCertified,
                                  bool useVehicleCondition, bool useVehicleConditionStatements, bool useJdPowerRatings,
                                  bool useModelAwards, bool useExpertReviews, bool useCrashTestRatings,
                                  bool useWarranties, bool useFuelEconomy, bool useLowMilesPerYear,
                                  bool useFinancingSpecial, bool useTrim, bool useColors, bool useKeyInfoCheckboxes,
                                  bool useKeyVehicleEquipment, bool useDrivetrain)
        {
            PreferenceId = preferenceId;
            BusinessUnitId = businessUnitId;
            NewOrUsed = newOrUsed;
            UseMissionMarker = useMissionMarker;
            UsePriceBelowBook = usePriceBelowBook;
            UseOneOwner = useOneOwner;
            UsePriceReduced = usePriceReduced;
            UseCertified = useCertified;
            UseVehicleCondition = useVehicleCondition;
            UseVehicleConditionStatements = useVehicleConditionStatements;
            UseJdPowerRatings = useJdPowerRatings;
            UseModelAwards = useModelAwards;
            UseExpertReviews = useExpertReviews;
            UseCrashTestRatings = useCrashTestRatings;
            UseWarranties = useWarranties;
            UseFuelEconomy = useFuelEconomy;
            UseLowMilesPerYear = useLowMilesPerYear;
            UseFinancingSpecial = useFinancingSpecial;
            UseTrim = useTrim;
            UseColors = useColors;
            UseKeyInfoCheckboxes = useKeyInfoCheckboxes;
            UseKeyVehicleEquipment = useKeyVehicleEquipment;
            UseDrivetrain = useDrivetrain;
        }

        public int BusinessUnitId { get; set; }

        public bool UseDrivetrain { get; set; }

        public int PreferenceId { get; set; }

        public int NewOrUsed { get; set; }

        public bool UseMissionMarker { get; set; }

        public bool UsePriceBelowBook { get; set; }

        public bool UseOneOwner { get; set; }

        public bool UsePriceReduced { get; set; }

        public bool UseCertified { get; set; }

        public bool UseVehicleCondition { get; set; }

        public bool UseVehicleConditionStatements { get; set; }

        public bool UseJdPowerRatings { get; set; }

        public bool UseModelAwards { get; set; }

        public bool UseExpertReviews { get; set; }

        public bool UseCrashTestRatings { get; set; }

        public bool UseWarranties { get; set; }

        public bool UseFuelEconomy { get; set; }

        public bool UseLowMilesPerYear { get; set; }

        public bool UseFinancingSpecial { get; set; }

        public bool UseTrim { get; set; }

        public bool UseColors { get; set; }

        public bool UseKeyInfoCheckboxes { get; set; }

        public bool UseKeyVehicleEquipment { get; set; }
    }
}