﻿using System;

namespace FirstLook.Merchandising.DomainModel.Templating.Previews
{
    /// <summary>
    /// A PreviewItem, without all the random nonsense.
    /// </summary>
    internal class PreviewItemNonRandom : PreviewItem 
    {
        public PreviewItemNonRandom(PreviewConditionProcessor conditionProcessor, PreviewDataProcessor textProcessor, 
                                    int tierNumber, double weight, PreviewGroupType groupType) :
            base(conditionProcessor, textProcessor, tierNumber, weight, groupType, BlurbCategory.Regular, new Guid("{73B466F1-C705-4313-8E5D-4CEF0DB373BE}"))
        {            
        }
        public PreviewItemNonRandom(PreviewConditionProcessor conditionProcessor, PreviewDataProcessor processor, 
            int tierNumber, double weight, PreviewGroupType groupType, bool disableSeparator) :
            base(conditionProcessor, processor, tierNumber, weight, groupType, BlurbCategory.Regular, new Guid("{73B466F1-C705-4313-8E5D-4CEF0DB373BE}"), disableSeparator)
        {
        }

        /// <summary>
        /// Just ignore the randomness.  Our weight is fixed.
        /// </summary>
        /// <param name="random"></param>
        /// <returns></returns>
        public override double GetRandomizedWeight(Random random)
        {
            return GetWeight();
        }

        public override string EstimateSize(string separator)
        {
            if ( Edited ) return base.EstimateSize(separator);

            return "," + base.EstimateSize(separator);
        }

    }

}
