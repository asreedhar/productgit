using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using log4net;
using MerchandisingLibrary;
using MvcMiniProfiler;
using VehicleDataAccess;

using CoreUtilities = FirstLook.Common.Core.Utilities;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class AdTemplate : RandomUtility
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(AdTemplate).FullName);
        #endregion

        public int TemplateID = -1;
        
        public int GetItemCountWithMatchingTheme(int? themeId)
        {
            int ct = 0;
            foreach (TemplateItem ti in TemplateItems)
            {
                if (ti.HasThemeMatch(themeId))
                {
                    ct++;
                }
            }
            return ct;
        }

        public int OwnerBusinessUnitId { get; set; }

        public bool IsActive { get; set; }

        public const string CODE_TEMPLATE_START_TAG = "[";
        public const string CODE_TEMPLATE_END_TAG = "]";
        public readonly List<TemplateItem> TemplateItems = new List<TemplateItem>();

        public int? VehicleProfileId { get; set; }

        public string Name { get; set; }

        //insanity - blow this out into a separate table...it does not belong lumped in w/ snippets
        public bool HasTagline()
        {
            bool retVal = false;

            if (Contains(30))
            {
                retVal = GetBlurbById(30).SampleTextList.Count > 0;
            }
            return retVal;
        }

        public void Save(int businessUnitId)
        {
            Log.DebugFormat("Save() called for businessUnitId {0}", businessUnitId);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.template#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "TemplateID", TemplateID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);                    
                    Database.AddRequiredParameter(cmd, "VehicleProfileId", VehicleProfileId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Name", Name, DbType.String);
                    Database.AddRequiredParameter(cmd, "Active", IsActive, DbType.Boolean);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// this function is used to reorder groups of template blurbs (snippets).  
        /// The boundaries used for the reordering are currently set as a list of IDs 
        /// passed into the function
        /// These IDs will define where the "blocks" of grouped items begin and end
        /// </summary>
        /// <param name="metaTemplateLeaderIds"></param>
        /// <param name="moveFromMetaPosition"></param>
        /// <param name="moveToMetaPosition"></param>
        /// <param name="businessUnitId"></param>
        public void ReorderSections(List<int> metaTemplateLeaderIds, int moveFromMetaPosition, 
                                    int moveToMetaPosition, int businessUnitId)
        {
            Log.DebugFormat("ReorderSections() called with leaderIds '{0}', moveFrom '{1}', moveTo '{2}', businessUnitId '{3}'",
                metaTemplateLeaderIds.ToDelimitedString(","), moveFromMetaPosition, moveToMetaPosition, businessUnitId);

            Log.DebugFormat("Initial template order is '{0}'", PrintItemList(TemplateItems));

            if (metaTemplateLeaderIds.Count < Math.Max(moveToMetaPosition, moveFromMetaPosition)) 
                throw new ArgumentException("Count of meta template leader ids must be greater than the position to move to or from");
            
            if (moveFromMetaPosition < 0 || moveToMetaPosition < 0) 
                throw new ArgumentException("Meta positions must be >= 0");

            if (metaTemplateLeaderIds.Count <= 0) 
                throw new ArgumentException("There must be meta template leader ids");

            //get count moving and sequence of meta start/end         
            //get count this will relocate and sequence of group start/end
           
            //moving up, foreach item, if sequence in 
            bool movingLater = moveFromMetaPosition < moveToMetaPosition;
            int currMetaNdx = -1;
            List<List<TemplateItem>> groupedItems = new List<List<TemplateItem>>();
            TemplateItems.Sort(delegate (TemplateItem t1, TemplateItem t2)
                                   {
                                       return t1.Sequence.CompareTo(t2.Sequence);
                                   });
            foreach (TemplateItem ti in TemplateItems)
            {
                //if it is a meta header...
                if (metaTemplateLeaderIds.Contains(ti.BlurbID))
                {
                    groupedItems.Add(new List<TemplateItem>());
                    currMetaNdx++;
                }
                if (currMetaNdx >= 0)
                {
                    groupedItems[currMetaNdx].Add(ti);
                }
            }

            //count items in the "moving" index
            int movingCt = groupedItems[moveFromMetaPosition].Count;
            int relocatedCt = 0;

            //NOTE - this could be fancified by just toggling a positive/negative based on direction...thought this would be easier to understand later though
            if (movingLater)
            {
                //count the items after the moving one and in the "relocated" group
                for (int i = moveFromMetaPosition + 1; i <= moveToMetaPosition;i++)
                {
                    relocatedCt += groupedItems[i].Count;
                    //we already know the count that are being "moved", so alter the sequences of these cats
                    foreach (TemplateItem ti in groupedItems[i])
                    {
                        ti.Sequence -= movingCt;
                        TemplateItem.UpdateSequence(TemplateID, ti.BlurbID, ti.Sequence, ti.Priority, businessUnitId);
                    }
                }
            }else
            {
                //count the items after the moving one and in the "relocated" group (this time stepping backwards
                for (int i = moveFromMetaPosition - 1; i >= moveToMetaPosition; i--)
                {
                    relocatedCt += groupedItems[i].Count;
                    //we already know the count that are being "moved", so alter the sequences of these cats
                    foreach (TemplateItem ti in groupedItems[i])
                    {
                        ti.Sequence += movingCt;
                        TemplateItem.UpdateSequence(TemplateID, ti.BlurbID, ti.Sequence, ti.Priority, businessUnitId);
                    }
                }                
            }

            //now that we have the relocated count and the moving count, we can adjust the sequence of these accordingly
            foreach (TemplateItem ti in groupedItems[moveFromMetaPosition])
            {
                if (movingLater)
                {
                    //we need to move these later
                    ti.Sequence += relocatedCt;
                }
                else
                {
                    ti.Sequence -= relocatedCt;
                }
                TemplateItem.UpdateSequence(TemplateID, ti.BlurbID, ti.Sequence, ti.Priority, businessUnitId);
            }

            TemplateItems.Sort((t1, t2) => t1.Sequence.CompareTo(t2.Sequence));

            Log.DebugFormat("Final template order is '{0}'", PrintItemList(TemplateItems));
        }

        public static int CopyTemplate(int templateIdToCopy, string nameOfNewTemplate, int businessUnitIdOfCopier)
        {
            Log.DebugFormat("CopyTemplate() called with templateIdToCopy '{0}', nameOfNewTemplate '{1}', businessUnitIdOfCopier '{2}'",
                templateIdToCopy, nameOfNewTemplate, businessUnitIdOfCopier);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.Template#Copy";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitIdOfCopier, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "TemplateIDToCopy", templateIdToCopy, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "NameOfNewTemplate", nameOfNewTemplate, DbType.String);

                    object val = cmd.ExecuteScalar();
                    if (val != DBNull.Value)
                    {
                        return (int) val;
                    }
                    throw new ApplicationException("Could not copy ad template!");
                    
                }
            }
        }

        public bool ReorderItems(int itemToSwapIndex1, int itemToSwapIndex2)
        {
            Log.DebugFormat("ReorderItems() called with itemToSwapIndex1 {0} and itemToSwapIndex2 {1}",
                itemToSwapIndex1, itemToSwapIndex2);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.reorderTemplateItems";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "TemplateID", TemplateID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID1", TemplateItems[itemToSwapIndex1].BlurbID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID2", TemplateItems[itemToSwapIndex2].BlurbID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Seq1", TemplateItems[itemToSwapIndex1].Sequence, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Seq2", TemplateItems[itemToSwapIndex2].Sequence, DbType.Int32);

                    int ret = Convert.ToInt32(cmd.ExecuteNonQuery());
                    return ret > 0;
                }
            }
        }

        public List<TemplateItem> GetRandomSetOfItems(TemplateMediator mediator)
        {
            Log.Debug("GetSomeRandomSetOfItems() called.");

            List<TemplateItem> retList = new List<TemplateItem>();
            Random rand = new Random(DateTime.Now.Millisecond);
            foreach (TemplateItem ti in TemplateItems)
            {
                if (ti.IsRequired)
                {
                    retList.Add(ti);
                }
                else if (mediator.VehicleData.EvaluateCondition(ti.TemplateCondition))  //this breaks the pattern - can we delegate?
                {
                    if (rand.Next(0, 100) > 20)
                    {
                        retList.Add(ti);
                    }
                }
            }

            Log.DebugFormat("Returning {0}", PrintItemList(retList));
            return retList;
        }

        public bool Contains(int blurbID)
        {
            Log.DebugFormat("Contains() called for blurbId '{0}'", blurbID);

            foreach (TemplateItem ti in this.TemplateItems)
            {
                if (ti.BlurbID == blurbID)
                {
                    Log.Debug("Blurb found.");
                    return true;
                }
            }
            Log.Debug("Blurb not found.");
            return false;
        }

        public TemplateItem GetBlurbById(int blurbID)
        {
            Log.DebugFormat("GetBlurbById() called for blurbID '{0}'", blurbID);

            foreach (TemplateItem ti in TemplateItems)
            {
                if (ti.BlurbID == blurbID)
                {
                    Log.DebugFormat("Returning TemplateItem {0}", PrintItem(ti));
                    return ti;
                }
            }

            throw (new ApplicationException("Snippet not found by ID in GetBlurbById"));
        }

        public void AdjustForVehicle(IInventoryData vehicleData)
        {
            foreach (TemplateItem ti in TemplateItems)
            {
                ti.AdjustForVehicle(vehicleData);
            }
        }
        
        public static void AddItemToTemplate(int templateID, int blurbID)
        {
            Log.DebugFormat("AddItemToTemplate() called for templateID {0} and blurbID {1}", templateID, blurbID);   

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.insertItem";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "TemplateID", templateID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);
                    
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void RemoveItemFromTemplate(int templateID, int blurbID)
        {
            Log.DebugFormat("RemoveItemFromTemplate() called for templateID {0} and blurbID {1}", templateID, blurbID);
            
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.removeItem";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "TemplateID", templateID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }



        public static Dictionary<int, string> FetchTemplateList(int businessUnitID, bool includeActive, bool includeInactive)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getTemplateList";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "IncludeActive", includeActive, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "IncludeInactive", includeInactive, DbType.Boolean);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        
                        Dictionary<int, string> retD = new Dictionary<int, string>();
                        while (reader != null && reader.Read())
                        {
                            retD.Add(reader.GetInt32(reader.GetOrdinal("templateID")), 
                                     reader.GetString(reader.GetOrdinal("templateName")));
                        }

                        return retD;
                    }
                }
            }
        }

        #region Constructors

        public AdTemplate()
        {
            Name = "";
        }

        internal AdTemplate(XmlReader reader) : this()
        {
            Dictionary<int, TemplateItem> childItems = new Dictionary<int, TemplateItem>();

            XmlDocument doc = new XmlDocument();
            doc.Load(reader);

            foreach (XmlNode node in doc.GetElementsByTagName("AdTemplate"))
            {
                if ( node.Attributes == null || node.Attributes["templateId"] == null )
                {
                    Log.Error("node.Attributes is null or node.Attributes['templateId'] is null");
                    throw new ApplicationException("node.Attributes is null or node.Attributes['templateId'] is null");
                }

                TemplateID = Int32.Parse(node.Attributes["templateId"].Value);
                VehicleProfileId = Database.GetNullableInt(node, "vehicleProfileId");
                Name = node.Attributes["name"].Value;
                IsActive = Convert.ToBoolean(Convert.ToInt32(node.Attributes["active"].Value));
                OwnerBusinessUnitId = Int32.Parse(node.Attributes["businessUnitId"].Value);
            }

            foreach (XmlNode node in doc.GetElementsByTagName("TemplateItem"))
            {
                if (node.Attributes == null || node.Attributes["parentID"] == null)
                {
                    Log.Error("node.Attributes is null or node.Attributes['parentID'] is null");
                    throw new ApplicationException("node.Attributes is null or node.Attributes['parentID'] is null");
                }

                int parentID = Convert.ToInt32(node.Attributes["parentID"].Value);
                if (parentID > 0)
                {
                    childItems.Add(parentID, new TemplateItem(node));
                }
                else
                {
                    TemplateItems.Add(new TemplateItem(node));
                }
            }

            foreach (KeyValuePair<int, TemplateItem> kvp in childItems)
            {
                foreach (TemplateItem ti in TemplateItems)
                {
                    if (ti.BlurbID == kvp.Key)
                    {
                        ti.ChildItem = kvp.Value;
                    }
                }
            }

        }

        #endregion




        public static bool BusinessUnitDoesNotHaveTemplate(int businessUnitID, int templateID)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase, false))
            using (var command = connection.CreateCommand())
            {
                connection.Open();

                command.CommandText = "select count(templateId) from templates.AdTemplates where templateId = @TemplateId and businessUnitId = @BusinessUnitId";
                command.CommandType = CommandType.Text;
               
                command.AddRequiredParameter("@TemplateId", templateID, DbType.Int32);
                command.AddRequiredParameter("@BusinessUnitId", businessUnitID, DbType.Int32);
                
                var count = Convert.ToInt32(command.ExecuteScalar());

                return count == 0;
            }
        }

        public static AdTemplate Fetch(int templateID, int businessUnitID)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase, false))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getTemplate";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "TemplateID", templateID, DbType.Int32);
                    
                    using (XmlReader reader = ((SqlCommand)cmd).ExecuteXmlReader())
                    {
                        return new AdTemplate(reader);
                    }
                }
            }
        }

        public static void Create(int businessUnitId, string name)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.template#Create";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Name", name, DbType.String);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        
        public static AdTemplate BestTemplateSelect(int businessUnitID, int chromeStyleID, InventoryData vehicleData)
        {
            //select makeID, modelID, marketClassID
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getMatchingTemplates";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "year", vehicleData.Year, DbType.String);
                    Database.AddRequiredParameter(cmd, "listPrice", vehicleData.ListPrice, DbType.Decimal);
                    Database.AddRequiredParameter(cmd, "makeName", vehicleData.Make, DbType.String);
                    Database.AddRequiredParameter(cmd, "modelName", vehicleData.Model, DbType.String);
                    Database.AddRequiredParameter(cmd, "age", vehicleData.Age, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "chromeStyleID", chromeStyleID, DbType.Int32);

                    //we could implement lots of fancy logic here, to decide if the distance is too small, make some other
                    //discriminating feature more important or weight the gauge values...
                    //gaugeDistanceValues.Add((int)reader["templateID"], (int)reader["distance"]);
                    object o = cmd.ExecuteScalar();
                    if (o == null || o == DBNull.Value)
                    {
                        return Fetch(Constants.DEFAULT_TEMPLATE_ID, businessUnitID);
                    }
                    return Fetch(Convert.ToInt32(cmd.ExecuteScalar()), businessUnitID);
                }
            }
        }        

        public static void Delete(int templateID, int businessUnitID)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.deleteTemplate";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "TemplateID", templateID, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
            
        }

        /// <summary>
        /// Build up a MerchandisingDescription.  Specifically, set the meta-data and the Description text.
        /// </summary>
        /// <param name="maxThemeSentences"></param>
        /// <param name="characterLimit"></param>
        /// <param name="mergeData"></param>
        /// <param name="sampleTextIds"></param>
        /// <param name="mediator"></param>
        /// <param name="description"></param>
        /// <param name="vehicleData"></param>
        /// <returns></returns>
        internal void BuildUpDescription(Advertisement editedModel, int maxThemeSentences, int characterLimit, bool mergeData, TemplateMediator mediator, ref MerchandisingDescription description, 
            IVehicleTemplatingData vehicleData)
        {
            using (MiniProfiler.Current.Step("BuildUpDescription"))
            {
                AdvertisementBuilder builder = new AdvertisementBuilder(this, editedModel);
                var advertismentModel = builder.Build(mediator, mergeData, characterLimit);

                AdvertisementConverter converter = new AdvertisementConverter();
                description.Description = converter.ToText(advertismentModel);


                description.OldAdvertisementModel = description.AdvertisementModel;
                if (description.OldAdvertisementModel == null)
                    //generate model with entire ad as paragraph for the OldAdvertisement
                {
                    var converted = converter.FromText(description.Description);

                    Log.DebugFormat("We have no model.  Converting model from description text.  Converted is '{0}'",
                                    converted);

                    description.OldAdvertisementModel = converted;
                }

                description.AdvertisementModel = advertismentModel;

                // Now we setup some additional properties of the description.
                Log.DebugFormat("Setting Ad expiration to '{0}'", vehicleData.ExpirationDate);
                description.ExpiresOn = vehicleData.ExpirationDate;

                Log.DebugFormat("Setting LastUpdatedListPrice to '{0}'", vehicleData.InventoryItem.ListPrice);
                description.LastUpdateListPrice = vehicleData.InventoryItem.ListPrice;

                Log.DebugFormat("Setting SampleTextIds to '{0}'", builder.SampleTextIds.ToDelimitedString(","));
                description.SampleTextIds = builder.SampleTextIds;

                description.ResetDisclaimer();

                Log.Debug("BuildUpDescription() finished.");
            }
        }
        

        public string ProcessItem(TemplateItem item, TemplateMediator mediator)
        {
            return ProcessItem(item, false, false, mediator);
        }
        
        /// <summary>
        /// Processes the template item to create representative text based on the available vehicle data
        /// </summary>
        /// <param name="item"></param>
        /// <param name="processTemplateData">Determines if the actual vehicle data should be included in the rendered description.  If false, it will remain as a template (e.g. New Price is [List Price]!)</param>
        /// <param name="useThemes"></param>
        /// <param name="mediator"></param>
        /// <returns></returns>
        public string ProcessItem(TemplateItem item, bool processTemplateData, bool useThemes, TemplateMediator mediator)
        {
            Log.DebugFormat("ProcessItem() called for TemplateItem:{0}, processTemplateData:{1}, useThemes:{2}, mediator:{3}",
                PrintItem(item), processTemplateData, useThemes, mediator);

            if( item == null || mediator == null)
            {
                Log.ErrorFormat("Item '{0}' or TemplateMediator {1} were null.", PrintItem(item), mediator);
                return string.Empty;
            }

            //If the item has been edited we keep the text
            if (item.Edited)
            {
                if (useThemes)
                {
                    Log.DebugFormat(
                        "We are using themes, generating  random string for this item within the theme {0}",
                        mediator.ThemeId);
                    item.ToStringRand(mediator.ThemeId);
                }
                else
                {
                    Log.DebugFormat(
                        "We are not using themese, generating random string for this item, specifying a null theme id.");
                    item.ToStringRand(null);
                }

                return item.EditedText;
            }

            string retStr = String.Empty;

            //test if the it can be conditionally included based on the conddition
            bool isConditionOk = string.IsNullOrEmpty(item.TemplateCondition) || mediator.EvaluateCondition(item.TemplateCondition);


            Log.DebugFormat("The mediator evaluated the condition {0} to {1}", item.TemplateCondition, isConditionOk);

            if (isConditionOk) //if the condition is true, convert to string
            {
                //pass along the useTheme flag
                if (useThemes)
                {
                    Log.DebugFormat(
                        "We are using themes, generating  random string for this item within the theme {0}",
                        mediator.ThemeId);
                    retStr = item.ToStringRand(mediator.ThemeId);
                }else
                {
                    Log.DebugFormat(
                        "We are not using themese, generating random string for this item, specifying a null theme id.");
                    retStr = item.ToStringRand(null);
                }
                Log.DebugFormat("The generated text is {0}", retStr);
            }
            else if (item.hasChild()) //if dependent children, call it on the chlid - these are essentially fall through cases for the snippets
            {
                Log.Debug("The condition did not pass, however the item has a child.  We will process the child.");
                retStr = ProcessItem(item.ChildItem, processTemplateData, useThemes, mediator);
            }

            if (!processTemplateData)
            {
                Log.DebugFormat("We are not processing template data, returning the text '{0}' for display.", retStr);
                return retStr; //if not processing, return the template text for display
            }
            if(String.IsNullOrEmpty(retStr))
            {
                Log.DebugFormat("The generated text is '', returning it for display.");
                return retStr; //if not processing, return the template text for display
            }

            //else, generate the text from the data and template
            Log.DebugFormat("Invoking StringTemplateHelper to generate text using ANTLR.");

            
            string text = StringTemplateHelper.GetValidStringTemplate(retStr, CODE_TEMPLATE_START_TAG,
                                                    CODE_TEMPLATE_END_TAG, 
                                                    mediator.MerchandisingPointsMap);

            Log.DebugFormat("StringTemplateHelper.GetValidStringTemplate() returned text {0}.", text);
            Log.Debug("This will be passed to RenderWithVehicleData().");

            var textWithVehicleData = StringTemplateHelper.RenderWithVehicleData(text, mediator.VehicleData);
            Log.DebugFormat("The templated ANTLR text rendered with vehicle data is {0}", textWithVehicleData);

            Log.DebugFormat("Returning {0}", textWithVehicleData);
            return textWithVehicleData;
        }

        private static string PrintItem(TemplateItem item)
        {
            if( item == null )
            {
                return "TemplateItem is null";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("\n\nTemplateItem: ");
            sb.AppendFormat("\n\tActiveSampleTextId : '{0}'", item.ActiveSampleTextId);
            sb.AppendFormat("\n\tBlurbId: '{0}'", item.BlurbID);
            sb.AppendFormat("\n\tBlurbName: '{0}'", item.BlurbName);
            sb.AppendFormat("\n\tBlurbTypeId : '{0}'", item.BlurbTypeId );
            sb.AppendFormat("\n\tBlurbTypeName : '{0}'", item.BlurbTypeName);
            sb.AppendFormat("\n\tIsRequired : '{0}'", item.IsRequired);
            sb.AppendFormat("\n\tOwnerId : '{0}'", item.OwnerId);
            sb.AppendFormat("\n\tPriority : '{0}'", item.Priority);

            // sample text list collectionbase
            StringBuilder sbBlurbText = new StringBuilder();
            foreach( BlurbText blurb in item.SampleTextList)
            {
                sbBlurbText.Append("\n\t\tBlurbText: ");
                sbBlurbText.AppendFormat("\n\t\t\tActive: {0}", blurb.Active);
                sbBlurbText.AppendFormat("\n\t\t\tBlurbTextID: {0}", blurb.BlurbTextID);
                sbBlurbText.AppendFormat("\n\t\t\tConsolidatedText: {0}", blurb.ConsolidatedText);
                sbBlurbText.AppendFormat("\n\t\t\tIsLongForm: {0}", blurb.IsLongForm);
                sbBlurbText.AppendFormat("\n\t\t\tMatch: {0}", blurb.Match);
                sbBlurbText.AppendFormat("\n\t\t\tPostGenerateText: {0}", blurb.PostGenerateText);
                sbBlurbText.AppendFormat("\n\t\t\tPreGenerateText: {0}", blurb.PreGenerateText);
                sbBlurbText.AppendFormat("\n\t\t\tTheme: {0}", blurb.Theme);
                sbBlurbText.AppendFormat("\n\t\t\tThemeId: {0}", blurb.ThemeId);
            }

            sb.AppendFormat("\n\tSampleTextList : '{0}'", sbBlurbText);
            sb.AppendFormat("\n\tSequence : '{0}'", item.Sequence);
            sb.AppendFormat("\n\tTemplateText: '{0}'", item.TemplateText );
            sb.AppendFormat("\n\tTemplateCondition: '{0}'", item.TemplateCondition );

            if( item.hasChild() )
            {
                sb.AppendFormat("\n\t\tChildItem : '{0}'", PrintItem(item.ChildItem));
            }

            return sb.ToString();
        }

        private static string PrintItemList(IEnumerable<TemplateItem> items)
        {
            if( items == null )
            {
                return "List is null.";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("TemplateItem List: ");

            foreach( var item in items )
            {
                sb.Append(PrintItem(item));
            }

            sb.Append("End of TemplateItem list.");

            return sb.ToString();
        }

    }
}