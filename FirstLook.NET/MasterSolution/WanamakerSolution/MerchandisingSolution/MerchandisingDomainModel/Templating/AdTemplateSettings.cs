using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class AdTemplateSettings
    {
        private int? templateId;
        private List<int> themeSet;
        private PricingAggressiveness pricingMode = PricingAggressiveness.Low;

        public int? TemplateId
        {
            get { return templateId; }
            set { templateId = value; }
        }

        public int GetTemplateIdOrDefault(int defaultValue)
        {
            return templateId.GetValueOrDefault(defaultValue);
        }

        /// <summary>
        /// Dumbed down version for compatibility with older single theme approach
        /// </summary>
        /// <returns></returns>
        public int? GetThemeId()
        {
            if (ThemeSet.Count > 0)
            {
                return ThemeSet[0];
            }
            return null;
        }

        /// <summary>
        /// Dumbed down version for compatibiliy w/ single theme approach
        /// </summary>
        /// <param name="themeId"></param>
        public void SetThemeId(int? themeId)
        {
            //clear the set, and add the single theme if it has a value
            themeSet = new List<int>();
            if (themeId.HasValue)
            {                
                themeSet.Add(themeId.Value);
            }
        }

        public List<int> ThemeSet
        {
            get { return themeSet; }
            set { themeSet = value; }
        }
        
        public PricingAggressiveness PricingMode
        {
            get { return pricingMode; }
            set { pricingMode = value; }
        }

        public AdTemplateSettings(int? templateId, List<int> themeSet, PricingAggressiveness pricingMode)
        {
            this.templateId = templateId;
            this.themeSet = themeSet;
            this.pricingMode = pricingMode;
        }

        public AdTemplateSettings(int? templateId, List<int> themeSet, int? pricingMode)
        {
            this.templateId = templateId;
            this.themeSet = themeSet;
            try
            {

                this.pricingMode = (PricingAggressiveness) pricingMode.GetValueOrDefault(1);
            }catch
            {
                this.pricingMode = PricingAggressiveness.Low;
            }            
        }

        public static void SetAsDefaultForStyle(int businessUnitId, int templateId, int? themeSetId, int chromeStyleId, int vehicleType)
        {
            //select makeID, modelID, marketClassID
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.defaultTemplateMapping#Insert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "templateId", templateId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "themeSetId", themeSetId, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "chromeStyleId", chromeStyleId, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "vehicleType", vehicleType, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        
        public static int CreateDefaultTemplateSet(int businessUnitId, int templateId, int? themeSetId,
            int? marketClassId, int? makeId, int? modelId, string modelName, int? modelYear, string trim, int vehicleType)
        {
            //select makeID, modelID, marketClassID
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.defaultTemplateMapping#Insert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "templateId", templateId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "themeSetId", themeSetId, DbType.Int32);
                    

                    if (marketClassId.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "marketClassId", marketClassId.Value, DbType.Int32);
                    }

                    if (makeId.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "divisionId", makeId.Value, DbType.Int32);
                    }

                    if (modelId.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "modelId", modelId.Value, DbType.Int32);
                    }

                    if (!string.IsNullOrEmpty(modelName))
                    {
                        Database.AddRequiredParameter(cmd, "modelName", modelName, DbType.String);
                    }

                    if (modelYear.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "modelYear", modelYear.Value, DbType.Int32);
                    }

                    if (trim != null)
                    {
                        Database.AddRequiredParameter(cmd, "trim", trim, DbType.String);
                    }

                    Database.AddRequiredParameter(cmd, "vehicleType", vehicleType, DbType.Int32);

                    cmd.ExecuteNonQuery();

                    return 1;
                }
            }
        }

        /// <summary>
        /// Retrieves the default templateId for a given vehicle.  InventoryData is used to determine aging and pricing constraints for mapping of 
        /// templates to vehicles (planned)
        /// </summary>
        /// <param name="businessUnitID"></param>
        /// <param name="chromeStyleID"></param>
        /// <param name="vehicleData"></param>
        /// <returns></returns>
        public static AdTemplateSettings FetchDefaultTemplateSettings(int businessUnitID, int chromeStyleID, IInventoryData vehicleData)
        {
            //select makeID, modelID, marketClassID
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.defaultTemplate#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitID", businessUnitID, DbType.Int32);
                    
                    //Database.AddWithValue(cmd, "listPrice", vehicleData.ListPrice, DbType.Decimal);
                    //Database.AddWithValue(cmd, "age", vehicleData.Age, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "chromeStyleID", chromeStyleID, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "vehicleType", vehicleData.InventoryType, DbType.Int32);

                    //we could implement lots of fancy logic here, to decide if the distance is too small, make some other
                    //discriminating feature more important or weight the gauge values...
                    //gaugeDistanceValues.Add((int)reader["templateID"], (int)reader["distance"]);
                    int? templateId = null;
                    int? pricingAggressiveness = null;
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            object o = reader["templateId"];
                            if (o != DBNull.Value)
                            {
                                templateId = (int) o;
                            }
                            
                            //grab pricing aggressiveness here too
                            pricingAggressiveness = (int) reader["pricingAggressiveness"];
                        }

                        List<int> themes = new List<int>();
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                themes.Add((int)reader["themeId"]);
                            }
                        }
                        return new AdTemplateSettings(templateId, themes, pricingAggressiveness);
                    }                    
                }
            }
        }        
 

    }
}
