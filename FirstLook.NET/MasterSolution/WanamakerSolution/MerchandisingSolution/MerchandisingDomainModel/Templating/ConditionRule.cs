using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class ConditionRule : ConditionItem
    {
        public ConditionRule(int itemType, List<int> items) : base(itemType)
        {
            Items = items;
        }

        public List<int> Items { get; set; }
    }
}
