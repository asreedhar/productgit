using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class CustomDealerImage
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string ImageUrl { get; set; }

        public CustomDealerImage()
        {
        }

        internal CustomDealerImage(IDataRecord reader)
        {
            Id = reader.GetInt32(reader.GetOrdinal("dealerCustomImageId"));
            ImageUrl = reader.GetString(reader.GetOrdinal("imageUrl"));
            Title = reader.GetString(reader.GetOrdinal("title"));

        }
        public static List<CustomDealerImage> Fetch(int businessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText =
                        @"settings.DealerCustomImages#Fetch";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<CustomDealerImage> imgs = new List<CustomDealerImage>();
                        
                        while (reader.Read())
                        {
                            imgs.Add(new CustomDealerImage(reader));
                        }
                        return imgs;
                    }
                }
            }
        }
        public static void Create(int businessUnitId, string title, string imageUrl)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText =
                        @"settings.DealerCustomImage#Insert";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Title", title, DbType.String);
                    Database.AddRequiredParameter(cmd, "ImageUrl", imageUrl, DbType.String);
                    

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void Delete(int businessUnitId, int customImageId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText =
                        @"settings.DealerCustomImage#Delete";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "DealerCustomImageId", customImageId, DbType.String);                    

                    cmd.ExecuteNonQuery();
                }
            }
        }

        
    }
}
