namespace FirstLook.Merchandising.DomainModel.Templating
{
    public interface IVehicleProfileRepository
    {
        VehicleProfile GetProfile(int vehicleProfileId);
    }
}