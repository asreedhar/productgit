namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class VehicleProfileRepository : IVehicleProfileRepository
    {
        public VehicleProfile GetProfile(int vehicleProfileId)
        {
            return VehicleProfile.Fetch(vehicleProfileId);
        }
    }
}