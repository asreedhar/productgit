namespace FirstLook.Merchandising.DomainModel.Templating
{
    public enum MerchandisingActivities
    {
        Building = 1,
        Photos = 2,
        Pricing = 3,
        Editing = 4,
        Approving = 5
    }
}