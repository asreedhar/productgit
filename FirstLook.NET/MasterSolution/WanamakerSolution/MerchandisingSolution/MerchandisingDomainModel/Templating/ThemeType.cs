namespace FirstLook.Merchandising.DomainModel.Templating
{
    public enum ThemeType
    {
        None = 0,
        Family,
        Green,
        StudentTeen,
        Urban,
        WorkUtility,
        RecreationTravel,
        Sport,
        Value,
        Luxury
    }
}
