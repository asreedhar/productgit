namespace FirstLook.Merchandising.DomainModel.Templating
{
    public enum LowActivityAlerts
    {
        LowSearchAlert = 1,
        LowClickthruAlert = 2,
        UnderpricingAlert = 3,
        OverpricingAlert = 4,
        NegativeTrending = 5
    }
}