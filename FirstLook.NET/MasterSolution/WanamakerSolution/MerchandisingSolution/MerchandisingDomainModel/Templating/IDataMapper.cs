using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public interface IDataMapper
    {
        string ExecuteMapping(int businessUnitId, IVehicleTemplatingData data);
    }
}