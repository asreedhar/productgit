﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class ModelLevelFrameworkService
    {
        public void PushFrameworks(int businessUnitId)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                connection.Open();

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "templates.PushModelLevelFrameworks";

                command.AddRequiredParameter("StoreId", businessUnitId, DbType.Int32);

                command.ExecuteNonQuery();
            }

        }

    }
}
