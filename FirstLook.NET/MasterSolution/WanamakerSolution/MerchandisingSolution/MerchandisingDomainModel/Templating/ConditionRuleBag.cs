using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public static class ConditionRuleBag
    {
        private static readonly List<ConditionRule> Instance;

        static ConditionRuleBag()
        {
            Instance = new List<ConditionRule>();

            //combined no scratches, dents, rust (+ 3 pairwise combinations)
            Instance.Add(
                new ConditionRule(ConditionItem.NoScratchesDentsRust,
                                  new List<int>(new[]
                                                    {
                                                        ConditionItem.NoPanelScratches, ConditionItem.NoVisibleDents,
                                                        ConditionItem.NoVisibleRust
                                                    }
                                      )
                    )
                );

            Instance.Add(
                new ConditionRule(ConditionItem.NoScratchesDents,
                                  new List<int>(new[] {ConditionItem.NoPanelScratches, ConditionItem.NoVisibleDents}
                                      )
                    )
                );
            Instance.Add(
                new ConditionRule(ConditionItem.NoScratchesRust,
                                  new List<int>(new[] {ConditionItem.NoPanelScratches, ConditionItem.NoVisibleRust}
                                      )
                    )
                );
            Instance.Add(
                new ConditionRule(ConditionItem.NoDentsRust,
                                  new List<int>(new[] {ConditionItem.NoVisibleRust, ConditionItem.NoVisibleDents}
                                      )
                    )
                );

            //Combined No Mech Probs, Accidents, bodywork
            Instance.Add(
                new ConditionRule(ConditionItem.NoMechanicalProblemsAccidentsOrBodywork,
                                  new List<int>(new[]
                                                    {
                                                        ConditionItem.NoMechanicalProblems, ConditionItem.NoKnownAccidents,
                                                        ConditionItem.NoKnownBodyWork
                                                    }
                                      )
                    )
                );

            //combined pass inspection and detailed
            Instance.Add(
                new ConditionRule(ConditionItem.PassedDealerInspectionAndDetailed,
                                  new List<int>(new[]
                                                    {
                                                        ConditionItem.PassedDealerInspection, ConditionItem.IsFullyDetailed
                                                    }
                                      )
                    )
                );

            Instance.Add(
                new ConditionRule(ConditionItem.HadAllMaintananceHaveAllRecords,
                                  new List<int>(new[]
                                                    {
                                                        ConditionItem.HadAllRequiredScheduledMaintenance,
                                                        ConditionItem.HaveServiceRecords
                                                    }
                                      )
                    )
                );
            Instance.Add(
                new ConditionRule(ConditionItem.DealerMaintainedHaveAllRecords,
                                  new List<int>(new[] {ConditionItem.DealerMaintained, ConditionItem.HaveServiceRecords}
                                      )
                    )
                );

            Instance.Add(
                new ConditionRule(ConditionItem.HaveAllKeysAndManuals,
                                  new List<int>(new[] {ConditionItem.HaveAllKeys, ConditionItem.HaveOriginalManuals}
                                      )
                    )
                );

            //excellent ratings pairs
            Instance.Add(
                new ConditionRule(ConditionItem.ExcellentExteriorAndPaint,
                                  new List<int>(new[] {ConditionItem.ExcellentExterior, ConditionItem.ExcellentPaint}
                                      )
                    )
                );
            Instance.Add(
                new ConditionRule(ConditionItem.ExcellentInteriorAndSeats,
                                  new List<int>(new[] {ConditionItem.ExcellentInterior, ConditionItem.ExcellentSeats}
                                      )
                    )
                );
            Instance.Add(
                new ConditionRule(ConditionItem.ExcellentSeatsAndCarpet,
                                  new List<int>(new[] {ConditionItem.ExcellentSeats, ConditionItem.ExcellentCarpet}
                                      )
                    )
                );

            Instance.Add(
                new ConditionRule(ConditionItem.DealerMaintainedHadAllMaintananceHaveAllRecords,
                                  new List<int>(new[]
                                                    {
                                                        ConditionItem.DealerMaintained,
                                                        ConditionItem.HadAllRequiredScheduledMaintenance,
                                                        ConditionItem.HaveServiceRecords
                                                    }
                                      )
                    )
                );
        }

        public static List<ConditionItem> ProcessRuleList(List<ConditionItem> items, RandomUtility randomizer)
        {
            //find all rules that are totally contained
            List<ConditionRule> apply = 
                Instance.FindAll(conditionRule => conditionRule.Items.FindAll(
                                                    itemId => items.Exists(ci => ci.ItemType == itemId))
                                               .Count == conditionRule.Items.Count);

            foreach (ConditionRule conditionRule in apply)
            {
                //remove the items that are now represented in the "apply" set
                int itemCount = items.Count;
                
                ConditionRule rule = conditionRule; // avoid access to modified closure in items.RemoveAll()
                items.RemoveAll(conditionItem => rule.Items.Contains(conditionItem.ItemType));

                if (itemCount != items.Count)
                {
                    items.Add(conditionRule);
                }
            }


            return new List<ConditionItem>(randomizer.RandomPermutation(items));
        }
    }
}