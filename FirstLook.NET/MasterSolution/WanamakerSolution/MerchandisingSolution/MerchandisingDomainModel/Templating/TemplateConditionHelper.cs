using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Merchandising.DomainModel.Vehicles;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class TemplateConditionHelper
    {
        public static List<string> GetConditionDisplayNames()
        {
            List<string> retStrings = new List<string>();
            Regex rg = new Regex("([a-z])([A-Z])|([A-Za-z])([0-9])|([0-9])([A-Za-z])");
            foreach (string cond in Enum.GetNames(typeof(TemplateCondition)))
            {

                retStrings.Add(rg.Replace(cond, delegate(Match m) { return m.Groups[1].Value + " " + m.Groups[2].Value; }));
            }
            retStrings.Remove(TemplateCondition.Undefined.ToString());
            return retStrings;

        }
        public static TemplateCondition GetCondition(string conditionText)
        {
            conditionText = CleanConditionText(conditionText);
            try
            {
                return (TemplateCondition)Enum.Parse(typeof(TemplateCondition), conditionText, true);
            }
            catch
            {
                return TemplateCondition.Never;
            }
        }


        private static string CleanConditionText(string conditionText)
        {
            conditionText = conditionText.Replace(" ", "");
            //conditionText = conditionText.Replace("&gt;", ">");
            //conditionText = conditionText.Replace("&lt;", "<");
            //conditionText = conditionText.Trim("<> ".ToCharArray()); //how should we deal with &gt; or &lt;
            conditionText = conditionText.Trim((" " + AdTemplate.CODE_TEMPLATE_START_TAG + AdTemplate.CODE_TEMPLATE_END_TAG).ToCharArray());
            return conditionText;
        }

        
 
    }
}
