using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using FirstLook.Common.Core;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class BlurbTextCollection : CollectionBase
    {

        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(BlurbTextCollection).FullName);
        #endregion


        public BlurbTextCollection() : base()
        {
            
        }
        


        public static BlurbTextCollection FetchTextSamples(int businessUnitID, int blurbID)
        {
            BlurbTextCollection retBTC = new BlurbTextCollection();
            
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getSampleTextList";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        
                        while (reader.Read())
                        {
                            retBTC.Add(new BlurbText(reader));
                        }
                        if (retBTC.Count == 0)
                        {
                            //add one empty one
                            retBTC.Add(new BlurbText(-1, "", ""));
                        }
                        return retBTC;
                    }
                }
            }
        }

        public int Add(BlurbText textToAdd)
        {
            return this.InnerList.Add(textToAdd);
        }

        public BlurbText Select(int blurbTextId)
        {
            foreach (BlurbText bt in this.InnerList)
            {
                if (bt.BlurbTextID == blurbTextId)
                {
                    return bt;
                }
            }

            return null;
        }

        public void Remove(int blurbTextId)
        {
            BlurbTextCollection toRemove = new BlurbTextCollection();
            foreach (BlurbText bt in this.InnerList)
            {
                if (bt.BlurbTextID == blurbTextId)
                {
                    toRemove.Add(bt);
                }
            }

            foreach (BlurbText bt in toRemove)
            {
                List.Remove(bt);
            }
        }
        public string GetSamplePreText(int index)
        {
            if (this.InnerList.Count > index)
            {
                return Get(index).PreGenerateText;
            }
            return string.Empty;
        }
        public bool HasThemeMatch(int? themeId)
        {
            if (themeId.HasValue)
            {
                foreach (BlurbText text in List)
                {
                    if (text.ThemeId.HasValue && text.ThemeId.Value == themeId.Value)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public void AdjustForVehicle(IInventoryData vehicleData)
        {
            List<int> toRemove = new List<int>();
            foreach (BlurbText text in List)
            {
                if (!text.Match.IsMatch(vehicleData))
                {
                    toRemove.Add(text.BlurbTextID);
                }
            }

            foreach (int removeId in toRemove)
            {
                Remove(removeId);
            }
        }

        /// <summary>
        /// Fetches a random phrase that matches the themeId.  IF none is found, null is returned
        /// </summary>
        /// <param name="themeId"></param>
        /// <returns>BlurbText matching the theme.  If none is found, null is returned</returns>
        public BlurbText GetRandomPhrase(int? themeId)
        {
            Log.DebugFormat("GetRandomPhrase() called with themeId '{0}'", themeId);

            Random rand = new Random(DateTime.Now.Millisecond);

            //indices that qualify given the mode...
            List<int> possibleNdx = new List<int>();
            List<int> possibleUnthemedNdx = new List<int>();

            Log.DebugFormat("List.Count is {0}", List.Count);

            for (int i = 0; i < List.Count; i++)
            {
                BlurbText bt = Get(i);
                Log.DebugFormat( "The following BlurbText was selected: {0}", StandardObjectDumper.Write(bt, 1));

                //if the theme matches, add as a possibility
                if (bt.IsThemeMatch(themeId))
                {
                    Log.DebugFormat("This BlurbText is a theme match.  Adding the index {0} to possibleNdx.", i);
                    possibleNdx.Add(i);
                }

                //if we ask for a theme and it doesn't have one, then it is an unthemed possibility
                if (!bt.ThemeId.HasValue)
                {
                    Log.DebugFormat("The BlurbText has no theme value. Adding the index {0} to possibleUnthemedNdx.", i);
                    possibleUnthemedNdx.Add(i);
                }

            }

            // select a random index from the available ones...
            if (possibleNdx.Count > 0)
            {
                int tmpNdx = rand.Next(0, possibleNdx.Count);
                Log.DebugFormat("Selected random index {0} from theme match list possibleNdx", tmpNdx);

                BlurbText result = Get(possibleNdx[tmpNdx]);
                Log.DebugFormat("Returning themed BlurbText '{0}'", StandardObjectDumper.Write(result,1));

                return result;
            }
            
            if (possibleUnthemedNdx.Count > 0)  //if none were there, use an unthemed item
            {
                int val = rand.Next(0, possibleUnthemedNdx.Count);
                Log.DebugFormat("Selected random index {0} from un-themed list possibleUnthemedNdx", val);

                BlurbText result = Get(possibleUnthemedNdx[val]);
                Log.DebugFormat("Returning unthemed BlurbText '{0}'", StandardObjectDumper.Write(result, 1));
                return result;
            }

            Log.Info("No possible themed or unthemed blurb texts found. Returning null.");
            return null;
        }
        
        public BlurbText Get(int index)
        {
            Log.DebugFormat("Get() called with index {0}", index);
            return (BlurbText) InnerList[index];
        }

        public BlurbText this[int index]
        {
            get { return Get(index); }
            set { InnerList[index] = value; }
        }

        public void LoadViewState(object state)
        {
            Log.Debug("LogViewState() called.");

            ArrayList al = state as ArrayList;
            if (al == null)
            {
                Log.Error("BlurbTextCollection viewstate must be an array list");
                throw new ApplicationException("BlurbTextCollection viewstate must be an array list");
            }

            List.Clear();
            foreach (object obj in al)
            {
                BlurbText bt = new BlurbText();
                bt.LoadViewState(obj);
            }
        }

        public object SaveViewState()
        {
            Log.Debug("SaveViewState() called.");

            ArrayList al = new ArrayList();
            foreach (BlurbText bt in this)
            {
                al.Add(bt.SaveViewState());
            }
            return al;
        }

    }
}
