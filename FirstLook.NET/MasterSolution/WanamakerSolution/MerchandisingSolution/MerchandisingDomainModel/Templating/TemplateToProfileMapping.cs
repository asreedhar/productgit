namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class TemplateToProfileMapping
    {
        public TemplateToProfileMapping(int id, string name, int? vehicleProfileId)
        {
            TemplateId = id;
            TemplateName = name;
            VehicleProfileId = vehicleProfileId;
        }

        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public int? VehicleProfileId { get; set; }
    }
}