using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class VehicleProfile
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(VehicleProfile).FullName);
        #endregion

        #region Constructors

        public VehicleProfile(string title, List<string> makes, List<string> models, List<string> trims,
                              List<Segments> segments, List<string> mktClasses, int? startYear, int? endYear,
                              int? minAge, int? maxAge, decimal? minPrice, decimal? maxPrice,
                              bool? certifiedStatusFilter)
        {
            Id = -1;
            Title = title;
            Makes = makes;
            Models = models;
            Trims = trims;
            Segments = segments;
            MktClasses = mktClasses;
            StartYear = startYear;
            EndYear = endYear;
            MinAge = minAge;
            MaxAge = maxAge;
            MinPrice = minPrice;
            MaxPrice = maxPrice;
            CertifiedStatusFilter = certifiedStatusFilter;

            VehicleType = MAX.Entities.VehicleType.Both;
        }

        internal VehicleProfile(IDataRecord reader)
            : this()
        {
            int? tmpId = Database.GetNullableInt(reader, "vehicleProfileId");
            if (!tmpId.HasValue || tmpId <= 0) return; //if no profile was present, then it matches any vehicle, return

            Id = tmpId.Value;

            Title = reader.GetString(reader.GetOrdinal("title"));
            Makes =
                new List<string>(reader.GetString(reader.GetOrdinal("makeIdList")).Split(",".ToCharArray(),
                                                                                         StringSplitOptions.
                                                                                             RemoveEmptyEntries));
            Models =
                new List<string>(reader.GetString(reader.GetOrdinal("modelList")).Split(",".ToCharArray(),
                                                                                        StringSplitOptions.
                                                                                            RemoveEmptyEntries));
            Trims =
                new List<string>(reader.GetString(reader.GetOrdinal("trimList")).Split(",".ToCharArray(),
                                                                                       StringSplitOptions.
                                                                                           RemoveEmptyEntries));

            string segIdList = reader.GetString(reader.GetOrdinal("segmentIdList"));
            SetSegmentList(segIdList);
            MktClasses = new List<string>(reader.GetString(reader.GetOrdinal("marketClassIdList")).Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

            MaxPrice = Database.GetNullableDecimal(reader, "maxPrice");
            MinPrice = Database.GetNullableDecimal(reader, "minPrice");

            MaxAge = Database.GetNullableInt(reader, "maxAge");
            MinAge = Database.GetNullableInt(reader, "minAge");

            EndYear = Database.GetNullableInt(reader, "endYear");
            StartYear = Database.GetNullableInt(reader, "startYear");

            CertifiedStatusFilter = Database.GetNullableBoolean(reader, "certifiedStatusFilter");

            var type = Database.GetNullableInt(reader, "vehicleType");
            VehicleType = type ?? MAX.Entities.VehicleType.Both;
        }

        public VehicleProfile(XmlNode xNode)
            : this()
        {
            if (xNode == null || xNode.Attributes["vehicleProfileId"] == null)
            {

                return;
            }

            int? tmpId = Database.GetNullableInt(xNode, "vehicleProfileId");
            if (!tmpId.HasValue || tmpId <= 0) return;

            Id = tmpId.Value;

            Title = xNode.Attributes["title"].Value;

            Makes =
                new List<string>(xNode.Attributes["makeIdList"].Value.Split(",".ToCharArray(),
                                                                            StringSplitOptions.RemoveEmptyEntries));
            Models =
                new List<string>(xNode.Attributes["modelList"].Value.Split(",".ToCharArray(),
                                                                           StringSplitOptions.RemoveEmptyEntries));
            Trims =
                new List<string>(xNode.Attributes["trimList"].Value.Split(",".ToCharArray(),
                                                                          StringSplitOptions.RemoveEmptyEntries));

            string segIdList = xNode.Attributes["segmentIdList"].Value;
            SetSegmentList(segIdList);

            MktClasses =
                new List<string>(xNode.Attributes["marketClassIdList"].Value.Split(",".ToCharArray(),
                                                                                   StringSplitOptions.RemoveEmptyEntries));

            MaxPrice = Database.GetNullableDecimal(xNode, "maxPrice");
            MinPrice = Database.GetNullableDecimal(xNode, "minPrice");

            MaxAge = Database.GetNullableInt(xNode, "maxAge");
            MinAge = Database.GetNullableInt(xNode, "minAge");

            EndYear = Database.GetNullableInt(xNode, "endYear");
            StartYear = Database.GetNullableInt(xNode, "startYear");

            CertifiedStatusFilter = Database.GetNullableBoolean(xNode, "certifiedStatusFilter");

            var type = Database.GetNullableInt(xNode, "vehicleType");
            VehicleType = type ?? MAX.Entities.VehicleType.Both;
        }

        public VehicleProfile()
        {
            Id = -1;
            Title = string.Empty;
            Makes = new List<string>();
            Models = new List<string>();
            Trims = new List<string>();
            Segments = new List<Segments>();
            MktClasses = new List<string>();
            VehicleType = MAX.Entities.VehicleType.Both;
        }

        #endregion


        #region Properties

        public string Title { get; set; }

        public bool? CertifiedStatusFilter { get; set; }

        public List<Segments> Segments { get; set; }

        public int? StartYear { get; set; }

        public int? EndYear { get; set; }

        public int? MinAge { get; set; }

        public int? MaxAge { get; set; }

        public decimal? MinPrice { get; set; }

        public decimal? MaxPrice { get; set; }

        public int Id { get; set; }

        public List<string> Makes { get; set; }

        public List<string> Models { get; set; }

        public List<string> Trims { get; set; }

        public List<string> MktClasses { get; set; }

        public string StartYearDescription
        {
            get
            {
                if (StartYear.HasValue)
                {
                    return StartYear.ToString();
                }
                return "Any Year";
            }
        }

        public string EndYearDescription
        {
            get
            {
                if (EndYear.HasValue)
                {
                    return EndYear.ToString();
                }
                return "Any Year";
            }
        }

        public string MinAgeDescription
        {
            get
            {
                if (MinAge.HasValue)
                {
                    return MinAge + " Days";
                }
                return "Any Age";
            }
        }

        public string MaxAgeDescription
        {
            get
            {
                if (MaxAge.HasValue)
                {
                    return MaxAge + " Days";
                }
                return "Any Age";
            }
        }

        public string MinPriceDescription
        {
            get
            {
                if (MinPrice.HasValue)
                {
                    return String.Format("{0:c0}", MinPrice);
                }
                return "Any Price";
            }
        }

        public string MaxPriceDescription
        {
            get
            {
                if (MaxPrice.HasValue)
                {
                    return String.Format("{0:c0}", MaxPrice);
                }
                return "Any Price";
            }
        }

        #endregion

        public override string ToString()
        {
            var list = getSummaryList();
            StringBuilder sb = new StringBuilder();

            foreach (var key in list.Keys)
            {
                sb.Append(key);
                sb.Append(list[key]);
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private void SetSegmentList(string segIdList)
        {
            Segments = new List<Segments>();

            foreach (string segId in segIdList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                int segIdVal = Int32.Parse(segId);
                Segments.Add((Segments)segIdVal);
            }
        }

        private IStyleRepository _styleRepository;
        public IStyleRepository StyleRepository
        {
            get
            {
                return _styleRepository ?? (_styleRepository = Registry.Resolve<IStyleRepository>());
            }
            internal set
            {
                _styleRepository = value;
            }
        }

        public bool IsMatch(IInventoryData data)
        {
            string model;

            if (data.ChromeStyleId.HasValue)
            {
                var style = StyleRepository.GetStyleById(data.ChromeStyleId.Value);
                if (style == null) return false;

                model = style.Model != null ? style.Model.ModelName : data.Model;
            }
            else
            {
                model = data.Model;
            }

            int yr = Int32.Parse(data.Year);
            var result = (
                    (Makes.Count == 0 || Makes.Any(m=> MakesMatch(m, data.Make)))
                    && (Models.Count == 0 || Models.Any(m => ModelsMatch(m, model)))
                    && (Trims.Count == 0 || Trims.Any(t => TrimsMatch(t, data.Trim)))
                    && (Segments.Count == 0 || Segments.Contains((Segments)data.SegmentId))
                    && (MktClasses.Count == 0 || MktClasses.Contains(data.MarketClass))
                    && (!StartYear.HasValue || yr >= StartYear)
                    && (!EndYear.HasValue || yr <= EndYear)
                    && (!MinAge.HasValue || data.Age >= MinAge)
                    && (!MaxAge.HasValue || data.Age <= MaxAge)
                    && (!MinPrice.HasValue || data.ListPrice >= MinPrice)
                    && (!MaxPrice.HasValue || data.ListPrice <= MaxPrice)
                    && (!CertifiedStatusFilter.HasValue || data.Certified == CertifiedStatusFilter.Value)
                    && (VehicleType == MAX.Entities.VehicleType.Both || VehicleType == data.InventoryType || ((data.InventoryType != MAX.Entities.VehicleType.New) && (data.InventoryType != MAX.Entities.VehicleType.Used)))
                    );
                    
            //account for non-priced vehicles?
            Log.DebugFormat("Result is: '{0}'", result);
            return result;
        }

        private bool TrimsMatch(string profileTrim, string inventoryTrim)
        {
            return string.IsNullOrEmpty(inventoryTrim) || profileTrim.ToLower() == inventoryTrim.ToLower();
        }

        private bool MakesMatch(string profileMake, string inventoryMake)
        {
            return profileMake.ToLower() == inventoryMake.ToLower();
        }

        private bool ModelsMatch(string profileModel, string inventoryModel)
        {
            return profileModel.ToUpper().StartsWith(inventoryModel.ToUpper()) || inventoryModel.ToUpper().StartsWith(profileModel.ToUpper());
        }

        /// <summary>
        /// Fetch the profile from the db.
        /// </summary>
        /// <param name="vehicleProfileId"></param>
        /// <returns></returns>
        public static VehicleProfile Fetch(int vehicleProfileId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.VehicleProfile#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "VehicleProfileId", vehicleProfileId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new VehicleProfile(reader);
                        }
                        return new VehicleProfile();
                    }
                }
            }
        }

        public Dictionary<string, string> getSummaryList()
        {
            var retList = new Dictionary<string, string>();
            if (Makes.Count > 0)
            {
                retList.Add("Makes:", GetMakeList().Replace(",", ", "));
            }

            if (Models.Count > 0)
            {
                retList.Add("Models:", GetModelList().Replace(",", ", "));
            }
            if (Trims.Count > 0)
            {
                retList.Add("Trims:", GetTrimList().Replace(",", ", "));
            }

            if (Segments.Count > 0)
            {
                retList.Add("Segments:", GetSegmentList().Replace(",", ", "));
            }

            if (MktClasses.Count > 0)
            {
                retList.Add("Mkt Classes:", GetMktClassList().Replace(",", ", "));
            }

            if (StartYear.HasValue || EndYear.HasValue)
            {
                retList.Add("Year Range:", StartYearDescription + " to " + EndYearDescription);
            }

            if (MinAge.HasValue || MaxAge.HasValue)
            {
                retList.Add("Age Range:", MinAgeDescription + " to " + MaxAgeDescription);
            }

            if (MinPrice.HasValue || MaxPrice.HasValue)
            {
                retList.Add("Price Range:", MinPriceDescription + " to " + MaxPriceDescription);
            }

            if (CertifiedStatusFilter.HasValue)
            {
                if (CertifiedStatusFilter.Value)
                {
                    retList.Add("Certified Filter:", "Only Certified Vehicles");
                }
                else
                {
                    retList.Add("Certified Filter:", "Only NON-Certified Vehicles");
                }
            }

            retList.Add("Vehicle Type: ", GetVehicleType());

            return retList;
        }

        private string GetVehicleType()
        {
            if (VehicleType == MAX.Entities.VehicleType.New)
            {
                return MAX.Entities.VehicleType.New;
            }

            if (VehicleType == MAX.Entities.VehicleType.Used)
            {
                return MAX.Entities.VehicleType.Used;
            }

            return MAX.Entities.VehicleType.Both;
        }

        private string GetMakeList()
        {
            Log.Debug("GetMakeList() called.");
            var sb = new StringBuilder();
            foreach (string make in Makes)
            {
                sb.Append(make);
                sb.Append(",");
            }
            string results = sb.ToString().TrimEnd(",".ToCharArray());
            Log.DebugFormat("Results are: '{0}'", results);
            return results;
        }

        private string GetModelList()
        {
            Log.Debug("GetModelList() called.");
            var sb = new StringBuilder();
            foreach (string model in Models)
            {
                sb.Append(model);
                sb.Append(",");
            }
            string results = sb.ToString().TrimEnd(",".ToCharArray());
            Log.DebugFormat("Results are: '{0}'", results);
            return results;
        }

        private string GetTrimList()
        {
            Log.Debug("GetTrimList() called.");
            var sb = new StringBuilder();
            foreach (string trim in Trims)
            {
                sb.Append(trim);
                sb.Append(",");
            }
            string results = sb.ToString().TrimEnd(",".ToCharArray());
            Log.DebugFormat("Results are: '{0}'", results);
            return results;
        }

        private string GetYearList()
        {
            Log.Debug("GetYearList() called.");
            var sb = new StringBuilder();

            int min = 1980;
            int max = DateTime.Today.Year + 3; //add some default buffer
            if (StartYear.HasValue)
            {
                min = StartYear.Value;
            }
            if (EndYear.HasValue)
            {
                max = EndYear.Value;
            }

            for (int yr = min; yr <= max; yr++)
            {
                sb.Append(yr);
                sb.Append(",");
            }

            string result = sb.ToString().TrimEnd(",".ToCharArray());
            Log.DebugFormat("Results are: '{0}'", result);
            return result;
        }

        private string GetMktClassList()
        {
            Log.Debug("GetMktClassList() called.");
            var sb = new StringBuilder();
            foreach (string mktCls in MktClasses)
            {
                sb.Append(mktCls);
                sb.Append(",");
            }
            string result = sb.ToString().TrimEnd(",".ToCharArray());
            Log.DebugFormat("Results are: '{0}'", result);
            return result;
        }

        private string GetSegmentIdList()
        {
            Log.DebugFormat("GetSegmentIdList() called.");
            var sb = new StringBuilder();
            foreach (Segments seg in Segments)
            {
                sb.Append(((int)seg));
                sb.Append(",");
            }
            string result = sb.ToString().TrimEnd(",".ToCharArray());
            Log.DebugFormat("Results are: '{0}'", result);
            return result;
        }

        private string GetSegmentList()
        {
            Log.Debug("GetSegmentList() called.");

            var sb = new StringBuilder();
            foreach (Segments seg in Segments)
            {
                sb.Append(seg.ToString());
                sb.Append(",");
            }
            string result = sb.ToString().TrimEnd(",".ToCharArray());
            Log.DebugFormat("Results are: '{0}'", result);
            return result;
        }

        /// <summary>
        /// Save the specified profile (update or create) for the specified business unit id. 
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="profile"></param>
        public static void Update(int businessUnitId, VehicleProfile profile)
        {
            Log.DebugFormat("Update() called for businessUnitId: {0} and profile {1}", businessUnitId, profile);
            profile.Save(businessUnitId);
        }

        /// <summary>
        /// Save the profile. Update Or Create
        /// </summary>
        /// <param name="businessUnitId"></param>
        public void Save(int businessUnitId)
        {
            Log.DebugFormat("Save() calleld for businessUnitId {0}", businessUnitId);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.VehicleProfile#UpdateOrCreate";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "VehicleProfileId", Id, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "title", Title, DbType.String);

                    Database.AddRequiredParameter(cmd, "makeIdList", GetMakeList(), DbType.String);
                    Database.AddRequiredParameter(cmd, "modelList", GetModelList(), DbType.String);
                    Database.AddRequiredParameter(cmd, "trimList", GetTrimList(), DbType.String);

                    Database.AddRequiredParameter(cmd, "segmentIdList", GetSegmentIdList(), DbType.String);
                    Database.AddRequiredParameter(cmd, "marketClassIdList", GetMktClassList(), DbType.String);

                    Database.AddRequiredParameter(cmd, "startYear", StartYear, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "endYear", EndYear, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "minAge", MinAge, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "maxAge", MaxAge, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "minPrice", MinPrice, DbType.Decimal);
                    Database.AddRequiredParameter(cmd, "maxPrice", MaxPrice, DbType.Decimal);

                    Database.AddRequiredParameter(cmd, "certifiedStatusFilter", CertifiedStatusFilter, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "vehicleType", VehicleType, DbType.Int32);

                    var val = (int)cmd.ExecuteScalar();
                    if (val > -1)
                    {
                        Id = val;
                    }
                }
            }
        }

        /// <summary>
        /// Get all possible models that are in the make list and year list.
        /// </summary>
        /// <returns></returns>
        public List<string> GetPossibleModels()
        {
            string makeList = GetMakeList();
            string yearList = GetYearList();

            Log.DebugFormat("GetPossibleModels() called.  Calling chrome.models#Fetch with MakeList: '{0}' and YearList: '{1}'", makeList, yearList);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "chrome.models#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "MakeList", makeList, DbType.String);
                    Database.AddRequiredParameter(cmd, "YearList", yearList, DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        //move to second result set (first has vin pattern id in it)
                        var retList = new List<string>();
                        while (reader.Read())
                        {
                            retList.Add((string)reader["modelName"]);
                        }

                        Log.DebugFormat("Returned models are: '{0}'", retList.ToDelimitedString(","));
                        return retList;
                    }
                }
            }
        }

        public List<string> GetPossibleTrims()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "chrome.trims#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ModelList", GetModelList(), DbType.String);
                    Database.AddRequiredParameter(cmd, "YearList", GetYearList(), DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        //move to second result set (first has vin pattern id in it)
                        var retList = new List<string>();
                        while (reader.Read())
                        {
                            retList.Add((string)reader["trim"]);
                        }
                        return retList;
                    }
                }
            }
        }

        public int VehicleType { get; set; }
    }
}