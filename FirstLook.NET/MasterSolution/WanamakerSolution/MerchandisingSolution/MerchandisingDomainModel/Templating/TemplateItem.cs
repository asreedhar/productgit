using System;
using System.Data;
using System.Xml;
using FirstLook.Common.Data;
using MerchandisingLibrary;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    [Serializable]
    public class TemplateItem : TemplateBlurb, IComparable
    {
        internal TemplateItem()
        {
        }

        internal TemplateItem(XmlNode node) : base(node["BlurbTemplate"])
        {
            if( node.Attributes == null )
                return;

            if (node.Attributes.Count < 5 || !node.HasChildNodes) 
                return;

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Sequence = Convert.ToInt32(node.Attributes["sequence"].Value);
            Priority = Convert.ToInt32(node.Attributes["priority"].Value);
            IsRequired = Convert.ToBoolean(Convert.ToInt32(node.Attributes["required"].Value));
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public static bool UpdateSequence(int templateId, int blurbId, int sequence, int priority, int businessUnitId)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.templateItem#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "TemplateID", templateId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Sequence", sequence, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Priority", priority, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);

                    int ret = Convert.ToInt32(cmd.ExecuteNonQuery());
                    return ret > 0;
                    
                }
            }
        }

        public virtual int Sequence { get; set; }

        public virtual int Priority { get; set; }

        public virtual bool IsRequired { get; set; }

        public virtual TemplateItem ChildItem { get; set; }

        #region IComparable Members

        public virtual int CompareTo(object obj)
        {
            TemplateItem y = obj as TemplateItem;
            if (y == null)
                return 1;
            
            if (Edited && !y.Edited)
                return -1;
            if (Edited && y.Edited)
                return 0;
            if (y.Edited && !Edited)
                return 1;

            return Priority.CompareTo(y.Priority);
        }

        #endregion

        public virtual bool hasChild()
        {
            return (ChildItem != null);
        }


        public virtual void LoadViewState(object state)
        {
            object[] objAra = state as object[];
            if (objAra == null || objAra.Length != 5) throw new ApplicationException("ViewState for TemplateITem must be object array length 5");

            Priority = (int) objAra[0];
            BlurbID = (int)objAra[1];
            BlurbName = (string)objAra[2];
            TemplateText = (string)objAra[3];
            SampleTextList = new BlurbTextCollection();
            SampleTextList.LoadViewState(objAra[4]);
        }

        public virtual object SaveViewState()
        {
            object[] retVal = new object[5];
            retVal[0] = Priority;
            retVal[1] = BlurbID;
            retVal[2] = BlurbName;
            retVal[3] = TemplateText;
            retVal[4] = SampleTextList.SaveViewState();
            return retVal;
        }

      
    }
}