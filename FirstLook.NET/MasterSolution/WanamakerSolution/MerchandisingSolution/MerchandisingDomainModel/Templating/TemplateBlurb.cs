using System;
using System.Data;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Templating;
using log4net;
using System.Collections.Generic;

namespace MerchandisingLibrary
{
    /// <summary>
    /// This class is a real piece of work.  Several of the constructors don't initialize the member variables properly, ToString()
    /// can return null, etc...
    /// </summary>
    [Serializable]
    public class TemplateBlurb
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(TemplateBlurb).FullName);
        #endregion


        private BlurbText currText;

        private int blurbID;

        public int BlurbID
        {
            get { return blurbID; }
            set { blurbID = value; }
        }

        public BlurbText ActiveBlurbText
        {
            get
            {
                return currText;
            }
        }

        public int? ActiveSampleTextId
        {
            get
            {
                if (currText == null)
                {
                    return null;
                }
                return currText.BlurbTextID;
            }
        }

        private int ownerId;

        public int OwnerId
        {
            get { return ownerId; }
            set { ownerId = value; }
        }

        private string blurbName = "";

        public string BlurbName
        {
            get { return blurbName; }
            set { blurbName = value; }
        }
	
        private BlurbTextCollection sampleTextList = new BlurbTextCollection();
        public BlurbTextCollection SampleTextList
        {
            get { return sampleTextList; }
            set { sampleTextList = value; }
        }

        private string templateText = "";

        public string TemplateText
        {
            get { return templateText; }
            set { templateText = value; }
        }

        private string templateCondition;

        public string TemplateCondition
        {
            get { return templateCondition; }
            set { templateCondition = value; }
        }

        private int blurbTypeId;

        public int BlurbTypeId
        {
            get { return blurbTypeId; }
            set { blurbTypeId = value; }
        }

        private string blurbTypeName;

        public string BlurbTypeName
        {
            get { return blurbTypeName; }
            set { blurbTypeName = value; }
        }

        public BlurbCategory BlurbCategory { get; set; }
        public bool Edited { get; set; }
        public string EditedText { get; set; }
    
        public void AdjustForVehicle(IInventoryData vehicleData)
        {
            SampleTextList.AdjustForVehicle(vehicleData);
        }
        public bool HasThemeMatch(int? themeId) 
        {
            return SampleTextList.HasThemeMatch(themeId);
        }

        public void SetEditedSampleText(int blurbTextID, string text)
        {
            var sampleText = SampleTextList.Select(blurbTextID);
            if (sampleText != null)
            {
                sampleText.PreBlurbEdited = true;
                sampleText.PreGenerateText = text;
                currText = sampleText;
            }
        }

        public void MoveNext(int? themeId)
        {
            Log.DebugFormat("MoveNext() called. CurrText is presently '{0}'", currText);
            if (currText == null || !currText.PreBlurbEdited)
                currText = SampleTextList.GetRandomPhrase(themeId);
            Log.DebugFormat("CurrText is now set to '{0}'", currText);
        }
        
        public override string ToString()
        {
            return ToString(currText);
        }

        public string ToStringRand(int? themeId)
        {
            Log.DebugFormat("ToStringRand() called with themeId '{0}'", themeId);

            MoveNext(themeId);

            var result = ToString();
            Log.DebugFormat("Returning result: '{0}'", result);
            return result;
        }

        public string ToString(int i)
        {
            Log.DebugFormat("ToString() called with int {0}", i);

            if (sampleTextList.Count > i)
            {
                Log.DebugFormat("sampleTextList.Count is > {0}. Returning templateText '{1}'", i, templateText);
                return ToString(sampleTextList.Get(i));
            }

            Log.DebugFormat("sampleTextList.Count <= {0}. Returning templateText '{1}'", i, templateText);
            return templateText;
        }

        public string ToString(BlurbText sampleText)
        {
            Log.DebugFormat("ToString() called with BlurbText '{0}'", StandardObjectDumper.Write(sampleText,1));

            if (sampleText != null)
            {
                Log.DebugFormat("Calling BlurbText.Render() with templateText '{0}'", templateText );
                string result = sampleText.Render(templateText);
                Log.DebugFormat("Result is: '{0}'", result);
                return result;
            }

            Log.DebugFormat("The supplied BlurbText is null.  Returning templateText '{0}' instead.", templateText);
            return templateText;
        }

       
        /// <summary>
        /// Parameterized ctor.
        /// </summary>
        /// <param name="blurbID"></param>
        /// <param name="blurbName"></param>
        /// <param name="collection"></param>
        /// <param name="templateText"></param>
        /// <param name="templateCondition"></param>
        public TemplateBlurb(int blurbID, string blurbName, BlurbTextCollection collection, string templateText, string templateCondition)
        {
            this.blurbName = blurbName;
            this.blurbID = blurbID;
            sampleTextList = collection;
            this.templateText = templateText;
            this.templateCondition = templateCondition;

            InitializeToSensibleDefaults();
        }

        /// <summary>
        /// Default ctor.
        /// </summary>
        public TemplateBlurb()
        {
            InitializeToSensibleDefaults();
        }

        /// <summary>
        /// There were far too many unsafe constructors in this class, presenting many ways for different member variables to be null 
        /// and cause problems later.
        /// </summary>
        private void InitializeToSensibleDefaults()
        {
            Log.Debug("InitializeToSensibleDefaults() called.");

            // I don't know how to safely initialize currText.  Crap.
            if( currText == null )
            {
                currText = new BlurbText(); // TODO: Make BurbText() do something useful.
            }

            if (blurbName == null)
            {
                blurbName = string.Empty;
            }

            if( sampleTextList == null )
            {
                sampleTextList = new BlurbTextCollection();
            }

            if( templateText == null )
            {
                templateText = string.Empty;
            }

            if( templateCondition == null )
            {
                templateCondition = "false";
            }

            if( blurbTypeName == null )
            {
                blurbTypeName = string.Empty;
            }




        }

        /// <summary>
        /// A ctor taking a data reader.
        /// </summary>
        /// <param name="reader"></param>
        internal TemplateBlurb(IDataReader reader)
        {
            int ct = 0;
            sampleTextList = new BlurbTextCollection();

            while (reader.Read())
            {
                
                if (ct == 0)
                {
                    //set initial values for blurb
                    blurbID = reader.GetInt32(reader.GetOrdinal("blurbID"));
                    blurbTypeId = reader.GetInt32(reader.GetOrdinal("blurbTypeId"));
                    blurbName = reader.GetString(reader.GetOrdinal("blurbName"));
                    templateText = reader.GetString(reader.GetOrdinal("blurbTemplate"));
                    templateCondition = reader.GetString(reader.GetOrdinal("blurbCondition"));
                    BlurbCategory = (BlurbCategory)reader.GetInt32(reader.GetOrdinal("blurbCategory"));
                    Edited = false;
                    EditedText = String.Empty;
                    ownerId = reader.GetInt32(reader.GetOrdinal("businessUnitId"));
                    ct = 1;
                }
                try
                {
                    sampleTextList.Add(new BlurbText(reader.GetInt32(reader.GetOrdinal("BlurbTextID")),
                                                     reader.GetString(reader.GetOrdinal("blurbPreText")),
                                                     reader.GetString(reader.GetOrdinal("blurbPostText"))));
                }
                catch(Exception ex)
                {
                    Log.Warn("The following exception was just suppressed.", ex);
                }
            }

            InitializeToSensibleDefaults();
        }

        /// <summary>
        /// A ctor tatking an XmlNode.
        /// </summary>
        /// <param name="xNode"></param>
        public TemplateBlurb(XmlNode xNode)
        {
            sampleTextList = new BlurbTextCollection();

            if( xNode == null )
            {
                Log.Error("The xNode is null.");
                throw new ApplicationException("The xNode is null.");
            }
            if( xNode.Attributes == null )
            {
                Log.Error("The XmlNode.Attributes is null.");
                throw new ApplicationException("The XmlNode.Attributes is null.");                
            }

            if (xNode.Attributes.Count >= 4)
            {
                blurbID = Convert.ToInt32(xNode.Attributes["blurbID"].Value);
                blurbName = Convert.ToString(xNode.Attributes["blurbName"].Value);
                templateText = Convert.ToString(xNode.Attributes["blurbTemplate"].Value);
                templateCondition = Convert.ToString(xNode.Attributes["blurbCondition"].Value);
                BlurbCategory = (BlurbCategory) Convert.ToInt32(xNode.Attributes["blurbCategory"].Value);
                Edited = false;
                EditedText = String.Empty;

                try
                {
                    foreach (XmlNode xn in xNode.ChildNodes)
                    {
                        if (xn.Attributes != null && xn.Attributes.Count > 0)
                            sampleTextList.Add(new BlurbText(xn));
                    }
                }
                catch(Exception ex)
                {
                    string error = String.Format("Error encountered processing XmlNode with OuterXml: '{0}'", xNode.OuterXml);
                    Log.Error(error, ex);
                }
            }
            else
            {
                Log.WarnFormat("It seems that if xNode.Attributes.Count < 4, the logical thing to do is absolutely nothing. Sigh. xNode.OuterXml is: '{0}'", xNode.OuterXml);
            }

            InitializeToSensibleDefaults();
        }

        /// <summary>
        /// Delete the blurb.
        /// </summary>
        /// <param name="businessUnitID"></param>
        /// <param name="blurbID"></param>
        public static void Delete(int businessUnitID, int blurbID)
        {
            Log.DebugFormat("Delete() called for businessUnitId {0} and blurbId {1}", businessUnitID, blurbID);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.deleteTemplateItem";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);
                    cmd.ExecuteNonQuery();
                }
            }
            
        }

        public bool IsOwner(int businessUnitId)
        {        
            Log.DebugFormat("IsOwner() called with businessUnitId {0}", businessUnitId);
            bool result = businessUnitId == ownerId;

            Log.DebugFormat("Result is: {0}", result);
            return result;
        }

        /// <summary>
        /// Fetch the specified blurb from the db.
        /// </summary>
        /// <param name="businessUnitID"></param>
        /// <param name="blurbID"></param>
        /// <returns></returns>
        public static TemplateBlurb Fetch(int businessUnitID, int blurbID)
        {
            Log.DebugFormat("Fetch() called for businessUnitID {0} and blurbID {1}", businessUnitID, blurbID);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getTemplateItem";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                     
                        TemplateBlurb ret = new TemplateBlurb(reader);
                        return ret;
                    }
                }
            }
            
        }

        public void Save()
        {
            Save(0);
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="businessUnitId"></param>
        public void Save(int businessUnitId)
        {
            Log.DebugFormat("Save() called with businessUnitId {0}", businessUnitId);

            // only the owner of the blurb can change it...
            if (!IsOwner(businessUnitId))
            {
                Log.DebugFormat("{0} is not the owner.  Only the owner of the blurb can change it.", businessUnitId);
                return;
            }

            Log.Debug("Proceeding with save.");
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.updateOrCreateTemplateItem";
                    cmd.CommandType = CommandType.StoredProcedure;
                 
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbID", blurbID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BlurbName", blurbName, DbType.String);
                    Database.AddRequiredParameter(cmd, "BlurbTemplate", templateText, DbType.String);
                    Database.AddRequiredParameter(cmd, "BlurbTypeId", blurbTypeId, DbType.Int32);

                    if (String.IsNullOrEmpty(templateCondition))
                    {
                        Database.AddRequiredParameter(cmd, "BlurbCondition", String.Empty, DbType.String);                        
                    }
                    else
                    {
                        Database.AddRequiredParameter(cmd, "BlurbCondition", templateCondition, DbType.String);
                    }
                    
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public override bool Equals(object obj)
        {
            TemplateBlurb blurb = obj as TemplateBlurb;
            if( blurb == null )
            {
                Log.DebugFormat(
                    "The other object {0} is not a TemplateBlurb and is not equal to this TemplateBlurb {1}", obj, this);
                return false;
            }

            bool equals =  blurb.blurbID == blurbID &&
                           blurb.blurbName == blurbName &&
                           blurb.blurbTypeId == blurbTypeId &&
                           blurb.ownerId == ownerId &&
                           blurb.templateCondition == templateCondition;

            Log.DebugFormat("The other object is a TemplateBlurb. Equals() returning {0}", equals);
            return equals;
        }

        public override int GetHashCode()
        {
            int hashcode = blurbID ^ blurbName.GetHashCode() ^ blurbTypeId ^ ownerId ^ templateCondition.GetHashCode();
            Log.DebugFormat("This TemplateBlurb has hashcode: {0}", hashcode);
            return hashcode;
        }

        public static List<TemplateBlurb> FetchBlurbsByType(int businessUnitID, int blurbTypeID)
        {
            // returns a list of blurbs for a business unit and the default (100150) for a blurb Type
            
            List<TemplateBlurb> blurbList = new List<TemplateBlurb>();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase, false))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {

                    cmd.CommandText = "[templates].[getAvailableTemplateItemListByType]";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddParameterWithValue("@businessUnitID", businessUnitID);
                    cmd.AddParameterWithValue("@blurbTypeID", blurbTypeID);

                    IDataReader myReader = cmd.ExecuteReader();

                    while (myReader.Read())
                    {
                        blurbList.Add(
                                    new TemplateBlurb
                                    {
                                        blurbID = Convert.ToInt32(myReader["blurbId"]),
                                        blurbName = myReader["blurbName"].ToString()
                                    }
                                );
                    }

                }

                return blurbList;
            }
        }
    }
}
