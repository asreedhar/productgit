using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Templating;

namespace MerchandisingLibrary
{
    public class MerchandisingAlert
    {
        private const double negativeTrendingThreshold = 0.6; //default to 60% of past value
        private const double clickThruAverage = 0.026;
        private const int dailySearchAverage = 75; 
        private List<LowActivityAlerts> alerts;
        private double clickThruRatio;
        private int numSearches;
        private double previousClickThruRatio;
        private int previousNumSearches;

        private int marketAveragePrice;
        private int totalActions;
        private int dayRecordsInAggregate;
        private int previousDayRecordsInAggregate;
        public double averageDailySearches
        {
         
            get
            {
                return ((double)numSearches)/dayRecordsInAggregate;
            }
        }

        public double previousAverageDailySearches
        {
            get
            {
                return ((double)previousNumSearches) / previousDayRecordsInAggregate;
            }
        }
        public MerchandisingAlert()
        {
            alerts = new List<LowActivityAlerts>();
        }

        public bool HasAlerts()
        {
            return alerts.Count > 0;
        }

        public List<LowActivityAlerts> Alerts
        {
            get { return alerts; }
            set { alerts = value; }
        }

        
        public string GetAlertText()
        {
            return GetAlertText("\n");
        }
        public string GetAlertText(string separator)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("Total Internet Actions: ");
            sb.Append(totalActions);
            sb.Append(separator);

            if (alerts.Contains(LowActivityAlerts.LowClickthruAlert))
            {
                sb.Append("Low Clickthru Rate: " + Math.Round(1000*clickThruRatio)/10 + "% (avg. 2.6%)");
                sb.Append(separator);
            }

            if (alerts.Contains(LowActivityAlerts.LowSearchAlert))
            {
                sb.Append("Low Search Rate: " + numSearches + " (avg. 75 per day)");
                sb.Append(separator);
            }

            if (alerts.Contains(LowActivityAlerts.UnderpricingAlert))
            {
                sb.Append("Potentially underpriced, market average: " + String.Format("{0:c0}",marketAveragePrice));
                sb.Append(separator);
            }

            if (alerts.Contains(LowActivityAlerts.OverpricingAlert))
            {
                sb.Append("Potentially overpriced, market average: " + String.Format("{0:c0}", marketAveragePrice));
                sb.Append(separator);
            }

            return sb.ToString().TrimEnd(separator.ToCharArray());
        }
        public string GetAlertHTML()
        {
            return GetAlertHTML("<br />", true, false);
        }
        public string GetAlertHTML(string separator, bool useHR, bool concise)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("<h4>Total Internet Actions: ");
            sb.Append(totalActions);
            sb.Append("</h4>");
            if (useHR)
            {
                sb.Append("<hr />");
            }else
            {
                sb.Append(separator);
            }

            if (alerts.Contains(LowActivityAlerts.NegativeTrending))
            {
                if (!concise)
                {
                    sb.Append("<span>Negative trend in vehicle activity:</span>");
                }

                if (averageDailySearches <= negativeTrendingThreshold*previousAverageDailySearches)
                {
                    //sb.Append(string.Format("Daily Searches declined from {0:n0} to {1:n0}<br />", previousAverageDailySearches, averageDailySearches));
                    sb.Append(string.Format("Daily Searches declined {0:n0}%", 100*(previousAverageDailySearches - averageDailySearches) / previousAverageDailySearches));
                    sb.Append(separator);
                    
                }
                if (clickThruRatio <= negativeTrendingThreshold*previousClickThruRatio)
                {
                    //sb.Append(string.Format("Detail Page Views declined from {0:n1}% to {1:n1}%<br />", previousClickThruRatio*100, clickThruRatio*100));
                    sb.Append(string.Format("Detail Page Views declined {0:n1}%", 100*(previousClickThruRatio - clickThruRatio) / previousClickThruRatio));
                    sb.Append(separator);
                }
                
            }
            if (!concise && (alerts.Contains(LowActivityAlerts.LowSearchAlert) || alerts.Contains(LowActivityAlerts.LowClickthruAlert)))
            {
                sb.Append("<span>Poor online activity:</span>");                
            }
            if (alerts.Contains(LowActivityAlerts.LowClickthruAlert))
            {
                sb.Append(string.Format("Low Clickthru Rate: {0:n0}% of average", Math.Round(1000 * clickThruRatio/clickThruAverage) / 10));
                sb.Append(separator);
            }

            if (alerts.Contains(LowActivityAlerts.LowSearchAlert))
            {
                sb.Append(string.Format("Low Search Rate: {0:n0}% of average", Math.Round((double)1000*numSearches/dayRecordsInAggregate/75)/10));
                sb.Append(separator);
            }

            /*if (alerts.Contains(LowActivityAlerts.UnderpricingAlert))
            {
                sb.Append("Potentially underpriced, market average: " + String.Format("{0:c0}", marketAveragePrice));
                sb.Append(separator);
            }

            if (alerts.Contains(LowActivityAlerts.OverpricingAlert))
            {
                sb.Append("Potentially overpriced, market average: " + String.Format("{0:c0}", marketAveragePrice));
                sb.Append(separator);
            }*/

            return sb.ToString().TrimEnd(separator.ToCharArray());
        }

        
        public static MerchandisingAlert GetLowActivityAlerts(int businessUnitID, int inventoryId)
        {
            MerchandisingAlert retAlert = new MerchandisingAlert();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.getLowActivityAlerts";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "NegativeTrendingThreshold", negativeTrendingThreshold, DbType.Double);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        
                        if (reader != null && reader.Read())
                        {
                            string search = reader.GetString(reader.GetOrdinal("SearchAlert"));
                            if (!string.IsNullOrEmpty(search))
                            {
                                retAlert.alerts.Add(LowActivityAlerts.LowSearchAlert);
                            }

                            string clickthru = reader.GetString(reader.GetOrdinal("ClickthruAlert"));
                            if (!string.IsNullOrEmpty(clickthru))
                            {
                                retAlert.alerts.Add(LowActivityAlerts.LowClickthruAlert);
                            }

                            string price = reader.GetString(reader.GetOrdinal("PricingAlert"));
                            if (!string.IsNullOrEmpty(price))
                            {
                                if (price.Contains("under"))
                                {
                                    retAlert.alerts.Add(LowActivityAlerts.UnderpricingAlert);
                                }
                                else
                                {
                                    retAlert.alerts.Add(LowActivityAlerts.OverpricingAlert);
                                }
                            }
                            if (reader.GetBoolean(reader.GetOrdinal("hasNegativeTrend")))
                            {
                                retAlert.alerts.Add(LowActivityAlerts.NegativeTrending);
                            }

                            retAlert.clickThruRatio = Database.GetNullableDouble(reader, "clickThruRatio").GetValueOrDefault(0);
                            retAlert.numSearches = Database.GetNullableInt(reader, "sumSearchViewed").GetValueOrDefault(0);
                            retAlert.marketAveragePrice = Database.GetNullableInt(reader,"maxAvgPrice").GetValueOrDefault(0);
                            retAlert.totalActions = Database.GetNullableInt(reader, "totalActions").GetValueOrDefault(0);
                            retAlert.previousClickThruRatio = Database.GetNullableDouble(reader, "previousClickThruRatio").GetValueOrDefault(0);
                            retAlert.previousNumSearches = Database.GetNullableInt(reader, "previousSumSearchViewed").GetValueOrDefault(0);
                            retAlert.dayRecordsInAggregate = Database.GetNullableInt(reader, "dayRecordsInAggregate").GetValueOrDefault(0);
                            retAlert.previousDayRecordsInAggregate = Database.GetNullableInt(reader, "previousDayRecordsInAggregate").GetValueOrDefault(0);
                        }
                        return retAlert;
                    }
                }
            }
        }
    }
}