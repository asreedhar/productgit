﻿namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class TemplateData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public VehicleProfile VehicleProfile { get; set; }
    }
}