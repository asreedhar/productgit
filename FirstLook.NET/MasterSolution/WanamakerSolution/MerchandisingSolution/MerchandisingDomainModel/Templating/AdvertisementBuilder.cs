﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Core.TemplatingModel;
using log4net;
using MerchandisingLibrary;
using MvcMiniProfiler;
using VehicleTemplatingDomainModel;
using FirstLook.Merchandising.DomainModel.Core;
using BlurbTemplate = FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.BlurbTemplate;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    //Would like to make AdTemplate implement the visitor so this could visit the items
    //but its model has too much logic on it.
    public class AdvertisementBuilder
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(AdvertisementBuilder).FullName);
        #endregion

        private Advertisement _editedModel;
        private Advertisement _model;
        private AdTemplate _adTemplate;
        private List<int> _sampleTextIds;

        public AdvertisementBuilder(AdTemplate adTemplate, Advertisement editedModel)
        {
            _adTemplate = adTemplate;
            _model = new Advertisement();
            _sampleTextIds = new List<int>();
            _editedModel = editedModel;
        }

        public List<int> SampleTextIds
        {
            get
            {
                return _sampleTextIds;
            }
        }

        private void MarkModulesEdited()
        {
            var editedBlurbs = new List<Item>();
            foreach(var module in _editedModel.Modules)
            {
                if(module.Header.Item.Edited && module.Header.Item.Children.Count > 0)
                    editedBlurbs.Add(module.Header.Item);
                
                var paragraphItems = module.Paragraph.Items.Where(x => x.Edited && x.Children.Count > 0);
                editedBlurbs.AddRange(paragraphItems);
            }

            foreach (var item in editedBlurbs)
            {
                var templateItem = _adTemplate.TemplateItems.Where(x => x.BlurbID == item.BlurbTemplateID).SingleOrDefault();
                if(templateItem == null && item.BlurbTemplateID == -1) //mark the preblurb as edited. Corner case where a blurb is a header and a paragraph
                {
                    //peak at the first item in the paragraph and set the Sample Text to edited
                    var module = _editedModel.Modules.Where(x =>
                                                                {
                                                                    if (x.Header.Item.PreBlurb == null || item.PreBlurb == null) //There can be empty headers
                                                                        return false;
                                                                    return x.Header.Item.PreBlurb.ID == item.PreBlurb.ID;
                                                                }
                        ).FirstOrDefault();

                    if (module != null && module.Paragraph.Items.Length > 0)
                    {
                        var blurbid = module.Paragraph.Items[0].BlurbTemplateID;
                        var template = _adTemplate.TemplateItems.Where(x => x.BlurbID == blurbid).FirstOrDefault();
                        if(template != null)
                            template.SetEditedSampleText(item.PreBlurb.ID, item.Text);
                    }
                }

                if (templateItem != null) //mark the blurb as edited
                {
                    templateItem.Edited = true;
                    templateItem.EditedText = item.Text;
                }
            }
        }

        private void CreateTemplateItems()
        {
            //if Items have no children they were created by the user. Custom added blurbs can only be added to the bottom of the ad in a new module for now. So
            //adding new TemplateItems to end of the TemplateItems list.
            int blurbID = -1;
            foreach (var module in _editedModel.Modules)
            {
                if (module.Header.Item.Edited && module.Header.Item.Children.Count == 0)
                {
                    var templateItem = new TemplateItem();
                    templateItem.Edited = true;
                    templateItem.EditedText = module.Header.Item.Text;
                    templateItem.BlurbID = blurbID;
                    blurbID--;

                    templateItem.SampleTextList.Add(new BlurbText(-1, "", "") {IsHeader = true});
                    _adTemplate.TemplateItems.Add(templateItem);
                }

                foreach (var item in module.Paragraph.Items.Where(x => x.Edited && x.Children.Count == 0))
                {
                    var templateItem = new TemplateItem();
                    templateItem.Edited = true;
                    templateItem.EditedText = item.Text;
                    templateItem.BlurbID = blurbID;
                    blurbID--;

                    templateItem.SampleTextList.Add(new BlurbText(-1, "", "") {IsHeader = false});
                    _adTemplate.TemplateItems.Add(templateItem);
                }
            }
        }

        private void RemoveEmptyParagraphModules()
        {
            var modules = _model.Modules.Where(x => x.Paragraph.Children.Count == 0).ToList();
            foreach (Module module in modules)
                _model.Children.Remove(module);
        }

        public Advertisement Build(TemplateMediator mediator, bool mergeData, int characterLimit)
        {
            if(_editedModel.Preview != null)
                mediator.RegisterPreviewItems(_editedModel.Preview.Paragraph.Items);
            else
                mediator.RegisterPreviewItems(new List<PreviewItem>());
                
            
            MarkModulesEdited();
            CreateTemplateItems();

            //get prioritization order, and process strings while we're at it...
            List<TemplateItem> prioritizedList = new List<TemplateItem>();

            List<int> themedItems = new List<int>();
            using (MiniProfiler.Current.Step("GetThemedItems"))
            {
                themedItems = GetThemedItems(mediator);
            }

            List<Canidate> renderCandidates;
            using (MiniProfiler.Current.Step("GetCanidates"))
            {
                renderCandidates = GetCanidates(mediator, themedItems, prioritizedList, mergeData);
                RemoveDuplicateOptions(renderCandidates);
            }
            prioritizedList.Sort();

            characterLimit = ReduceCharacterlimitByFooterLength(mediator, characterLimit);

            Trim(renderCandidates, prioritizedList, characterLimit);

            _model.Name = _adTemplate.Name;
            _model.AdTemplateID = _adTemplate.TemplateID;

            using (MiniProfiler.Current.Step("Build Model"))
            {
                BuildModel(renderCandidates, mediator.GetPreviewItems());
            }

            RemoveEmptyParagraphModules();
            return _model;
        }

        private static void RemoveDuplicateOptions(List<Canidate> renderCandidates)
        {
            var usedOptions = new List<string>();

            foreach (var candidate in renderCandidates)
            {
                if (candidate.IsPreview)
                    continue;

                var head = candidate.OrignalText.Substring(0, candidate.OrignalText.IndexOf(":", StringComparison.Ordinal)+1);
                var tail = candidate.OrignalText.Substring(candidate.OrignalText.IndexOf(":", StringComparison.Ordinal) + 1);

                var candidateOptions = Regex.Split(tail, @"(?<!\d),|,(?!\d)").ToList().ConvertAll(x => x.Trim()); // split on commas not preceeded by a digit or split on commas not succeeded by a digit (1,a a,1 but not 1,1)
                var keepOptions = new List<string>();

                foreach (var co in candidateOptions)
                {
                    var keepIt = usedOptions.All(uo => !StringHelper.AreSimilar(co, uo));

                    if(keepIt)
                    {
                        keepOptions.Add(co);
                    }
                }

                usedOptions.AddRange(keepOptions.Distinct(StringComparer.InvariantCultureIgnoreCase));
                candidate.ModifiedText = head +" "+ string.Join(", ", keepOptions);
            }
        }

        private static int ReduceCharacterlimitByFooterLength (TemplateMediator mediator, int characterLimit)
        {
            var footerLength = mediator.CalulateFooterLength();
            return characterLimit - footerLength;
        }

        private static bool ContainedWithinLargeString(string larger, string candidate)
        {
            var contained = larger.Contains(candidate);
            if (contained)
            {
                Log.DebugFormat("The existing ad text already contains {0}.", candidate);
            }
            return contained;
        }

        private static bool IsSimilarToAnyElementIn(IEnumerable<string> used, string candidate)
        {
            var similars = from instance in used
                           where StringHelper.AreSimilar(instance, candidate)
                           select instance;

            bool similar = similars.Any();

            if (similar)
            {
                Log.DebugFormat("'{0}' is too similar to '{1}' and therefore won't be used.", candidate, similars.First());
            }

            return similar;
        }

        private static bool IsDuplicateCandidate (TemplateItem item, List<Canidate> candidates)
        {
            var templateText = item.TemplateText;
            if ( !string.IsNullOrWhiteSpace( templateText ) )
            {
                if ( candidates.Exists( x => x.Template.TemplateText == templateText ) )
                {
                    return true;
                }
            }
            return false;
        }

        private static bool HasBlurb(TemplateItem template)
        {
            return template.BlurbID > 0;
        }

        private static bool HasPreBlurb(BlurbText blurbText)
        {
            return (blurbText != null && blurbText.BlurbTextID > 0 && blurbText.PreGenerateText != String.Empty);
        }

        private static bool HasPostBlurb(BlurbText blurbText)
        {
            return (blurbText != null && blurbText.BlurbTextID > 0 && blurbText.PostGenerateText != String.Empty);
        }
        
        private void BuildModel(List<Canidate> renderCandidates, PreviewItem[] previewItems)
        {
            var fragmentFormatter = new TextFragmentFormatter();

            Module currentModule = new Module();
            currentModule.Children.Add(new Header());
            currentModule.Children.First().Children.Add(new Item());

            var rawTextUsed = new List<string>();
            foreach (var candidate in renderCandidates)
            {
                bool use = candidate.Selected;
                string rawText = candidate.ModifiedText;

                if (use) // it was tagged for inclusion
                {
                    // Put together the current raw text.
                    var currentRawText = rawTextUsed.ToDelimitedString(" ");

                    // Look for "similar" used text.
                    if (IsSimilarToAnyElementIn(rawTextUsed, rawText) || ContainedWithinLargeString(currentRawText, rawText))
                    {
                        Log.Debug("Similar or same text has already been used in the Ad.  Skipping.");
                        continue;
                    }

                    rawTextUsed.Add(rawText);

                    if(!candidate.IsPreview)
                    {
                        if (candidate.IsHeader)
                        {
                            currentModule = new Module();
                            _model.Children.Add(currentModule);

                            Header header = new Header();
                            currentModule.Children.Add(header);

                            Item item = new Item();
                            header.Children.Add(item);

                            Paragraph paragraph = new Paragraph();
                            currentModule.Children.Add(paragraph);

                            //Weird corner case where a blurb has a template and is a header. Example: blurbID="90" (Key Features HEADER)
                            if (candidate.Template.TemplateText != string.Empty && HasPreBlurb(candidate.BlurbText)) 
                            {
                                item.Text = candidate.BlurbText.PreGenerateText.CleanUpHeader();
                                item.Children.Add(new PreBlurb() { ID = candidate.BlurbText.BlurbTextID, Text = candidate.BlurbText.PreGenerateText });
                                item.Edited = candidate.BlurbText.PreBlurbEdited;

                                string paragraphText = Regex.Replace( candidate.ModifiedText, Regex.Escape( candidate.BlurbText.PreGenerateText ), "", RegexOptions.IgnoreCase );

                                Item paragraphItem = new Item();
                                paragraph.Children.Add(paragraphItem);

                                paragraphItem.Text = candidate.Template.Edited ? paragraphText : fragmentFormatter.Format(paragraphText);
                                paragraphItem.Edited = candidate.Template.Edited;

                                if(HasBlurb(candidate.Template))
                                    paragraphItem.Children.Add(new BlurbTemplate() { ID = candidate.Template.BlurbID, Name = candidate.Template.BlurbName, Text = paragraphText });

                                if (HasPostBlurb(candidate.BlurbText))
                                    paragraphItem.Children.Add(new PostBlurb() { ID = candidate.BlurbText.BlurbTextID, Text = candidate.BlurbText.PostGenerateText });
                                
                            }
                            else
                            {
                                item.Text = candidate.Template.Edited ? candidate.ModifiedText : candidate.ModifiedText.CleanUpHeader();
                                item.Edited = candidate.Template.Edited;

                                if (HasBlurb(candidate.Template))
                                    item.Children.Add(new BlurbTemplate() { ID = candidate.Template.BlurbID, Name = candidate.Template.BlurbName, Text = candidate.ModifiedText });
                               
                                if (HasPreBlurb(candidate.BlurbText))
                                    item.Children.Add(new PreBlurb() { ID = candidate.BlurbText.BlurbTextID, Text = candidate.BlurbText.PreGenerateText });

                                if (HasPostBlurb(candidate.BlurbText))
                                    item.Children.Add(new PostBlurb() { ID = candidate.BlurbText.BlurbTextID, Text = candidate.BlurbText.PostGenerateText });
                                
                                
                            }
                        }
                        else
                        {
                            if (!_model.Children.Contains(currentModule))
                                _model.Children.Add(currentModule);

                            if (currentModule.Paragraph == null)
                                currentModule.Children.Add(new Paragraph());

                            Paragraph paragraph = currentModule.Paragraph;

                            Item item = new Item();
                            if (candidate.Template.BlurbCategory == BlurbCategory.Price)
                                item = new PriceItem();

                            paragraph.Children.Add(item);
                            item.Text = candidate.Template.Edited ? candidate.ModifiedText : fragmentFormatter.Format(candidate.ModifiedText);
                            item.Edited = candidate.Template.Edited;

                            if (HasBlurb(candidate.Template))
                                item.Children.Add(new BlurbTemplate() { ID = candidate.Template.BlurbID, Name = candidate.Template.BlurbName, Text = candidate.ModifiedText });
                           
                            if (HasPreBlurb(candidate.BlurbText))
                                item.Children.Add(new PreBlurb(){ID = candidate.BlurbText.BlurbTextID, Text = candidate.BlurbText.PreGenerateText});

                            if (HasPostBlurb(candidate.BlurbText))
                                item.Children.Add(new PostBlurb(){ID = candidate.BlurbText.BlurbTextID, Text = candidate.BlurbText.PostGenerateText});
                            
                        }
                    }
                    else
                    {
                        Preview preview = new Preview();
                        _model.Children.Add(preview);

                        PreviewHeader header = new PreviewHeader(){};
                        preview.Children.Add(header);
                        header.Children.Add(new PreviewItem(){Text = "PREVIEW"});

                        PreviewParagraph paragraph = new PreviewParagraph();
                        preview.Children.Add(paragraph);
                        
                        foreach(var item in previewItems)
                            paragraph.Children.Add(item);
                    }
                }
            }
        }

        private void Trim(List<Canidate> renderCandidates, List<TemplateItem> prioritizedList, int characterLimit)
        {
            int charCount = 0;
            int ndx = 0;
            TextFragmentFormatter fragmentFormatter = new TextFragmentFormatter();

            while (ndx < prioritizedList.Count)
            {
                //get the index of the next item (in terms of priority) so we can access it
                //in the textSnippets array
                TemplateItem templateItem = prioritizedList[ndx];
                //Log.DebugFormat("Looking at prioritizedList[{0}], which is '{1}' to see if including it would cause to to exceed the character limit.",
                  //  ndx, PrintItem(templateItem));

                // Will including the formatted text cause us to exceed the character count?
                var foundTuple = renderCandidates.Find(t => t.Template.Equals(templateItem));
                if (foundTuple == null)
                {
                    Log.Warn("Skipping to the next iteration of the loop.");
                    ndx++;
                    continue;
                }

                int tupleIndex = renderCandidates.IndexOf(foundTuple);

                Log.DebugFormat("Formatting text for evaluation '{0}'", foundTuple.OrignalText);
                string evaluationText = fragmentFormatter.Format(foundTuple.OrignalText);
                Log.DebugFormat("Formatted evaluation text is '{0}'", evaluationText);

                // If the text is emtpy, skip.
                if (string.IsNullOrEmpty(evaluationText))
                {
                    Log.Info("The formatted text is empty. Skipping to the next iteration of the loop.");
                    ndx++;
                    continue;
                }

                Log.DebugFormat("The Ad's current character count is {0}", charCount);
                Log.DebugFormat("The Ad's character count would be {0} if we include text '{1}'", charCount + evaluationText.Length, evaluationText);

                //We only trim on commas for non price items. Slitting on commas was causing slitting on price items causing half rendered price blurbs.
                //Ex: Priced $2,600, below NADA Retail => Priced $2
                if (foundTuple.Template.BlurbCategory != BlurbCategory.Price) 
                {
                    string originalEvaluationText = evaluationText;

                    // Trim until it fits.
                    int commaCount = 0;
                    while ((charCount + evaluationText.Length > characterLimit)
                           && evaluationText.LastIndexOf(',') > 0)
                    {
                        evaluationText = evaluationText.Substring(0, evaluationText.LastIndexOf(','));
                        ++commaCount;
                    }

                    // Update the corresponding raw text (which will be used for classification in factory.Create() in the following loop).
                    if (originalEvaluationText.Length != evaluationText.Length)
                    {
                        // The tuple's raw text has not been scrubbed and may not match the scrubbed and modified text used for evaluation of length.
                        // We need to make sure that we perform the same edits to the raw text as we did to the evaluation text.
                        string rawText = renderCandidates[tupleIndex].OrignalText;
                        // Remove same amount from rawText as was done for evaluationText.
                        for (int i = 0; i < commaCount && rawText.LastIndexOf(',') > 0; ++i)
                        {
                            rawText = rawText.Substring(0, rawText.LastIndexOf(','));
                        }
                        renderCandidates[tupleIndex].ModifiedText = rawText;
                        Log.InfoFormat("{0} characters have been removed from render candidate.",
                                       originalEvaluationText.Length - evaluationText.Length);
                    }
                }

                // Last check for length.
                if (charCount + evaluationText.Length > characterLimit)
                {
                    Log.InfoFormat("We can't include text '{0}' as it would cause us to exceed the Ad's character limit of {1}.", evaluationText, characterLimit);

                    ndx++;
                    continue;
                }

                charCount += evaluationText.Length;
                Log.DebugFormat("The Ad's character count has been increased to {0}", charCount);

                Log.DebugFormat("We will use text '{0}' in the Ad.", evaluationText);
                renderCandidates[tupleIndex].Selected = true;

                ndx++;
            }
        }

        private List<Canidate> GetCanidates(TemplateMediator mediator, List<int> themedItems, List<TemplateItem> prioritizedList, bool mergeData)
        {
            List<Canidate> renderCandidates = new List<Canidate>();

            foreach (TemplateItem item in _adTemplate.TemplateItems)
            {
                //Log.DebugFormat("Evaluating conditions loop 2 for TemplateItem '{0}'", PrintItem(item));

                // If the mediator says this blurb is "blocked", move on.
                if (mediator.BlockedItems.Contains(item.BlurbID))
                {
                    continue;
                }

                //FB 20181 if template has mutliple candidates with same property (blurbTemplate) don't add twice 
                if ( IsDuplicateCandidate( item, renderCandidates ) ) continue;

                prioritizedList.Add(item);

                string text = string.Empty;
                using (MiniProfiler.Current.Step("ProcessItem--" + item.BlurbName))
                {
                    // Generate the text for this template item.
                    text = _adTemplate.ProcessItem(item,
                                                          mergeData,
                                                          themedItems.Contains(item.BlurbID), mediator)
                        .Trim(" ".ToCharArray());
                }

                // Make sure to end with a space
                if (!string.IsNullOrEmpty(text))
                {
                    text = text + " ";
                }

                // assume the text wont be used for now.  
                var canidate = new Canidate() { Template = item, OrignalText = text, ModifiedText = text, Selected = false };
                renderCandidates.Add(canidate);

                //store the sample text id used to create the advert so we can recreate or inspect later
                int? sampleTextId = item.ActiveSampleTextId;
                if (sampleTextId.HasValue)
                {
                    canidate.BlurbText = item.ActiveBlurbText;
                    _sampleTextIds.Add(sampleTextId.Value);
                }
            }

            return renderCandidates;
        }


        private List<int> GetThemedItems(TemplateMediator mediator)
        {
            List<int> themedItems = new List<int>();

            foreach (TemplateItem templateItem in _adTemplate.TemplateItems)
            {
                //Log.DebugFormat("Evaluating conditions loop 1 for TemplateItem '{0}'", PrintItem(templateItem));

                // Are the conditions satisifed for using this blurb?
                TemplateItem item = templateItem;

                if (!item.HasThemeMatch(mediator.ThemeId))
                {
                    //Log.InfoFormat("The TemplateItem {0} is not part of the specified theme {1}. Continuing to next TemplateItem.",
                    //   PrintItem(item), mediator.ThemeId);
                    continue;
                }

                if (!mediator.EvaluateCondition(templateItem.TemplateCondition))
                {
                    //Log.InfoFormat("The mediator evaluated the TemplateItem.TemplateCondition {0} to false. Continuing to next TemplateItem.",
                    // templateItem.TemplateCondition);
                    continue;
                }

                // Use the blurb.
                //Log.DebugFormat("We will try to use the TemplateItem {0}", PrintItem(templateItem));
                themedItems.Add(templateItem.BlurbID);
            }

            return themedItems;
        }

        private class Canidate
        {
            public bool Selected { get; set; }

            public TemplateItem Template { get; set; }
            public BlurbText BlurbText { get; set; }
            public string OrignalText { get; set; }
            public string ModifiedText { get; set; }

            public bool IsPreview
            {
                get
                {
                    return Template.BlurbID == 62;
                }
            }

            public bool IsHeader
            {
                get
                {
                    if (BlurbText != null)
                        return BlurbText.IsHeader;

                    return false;
                }
            }
        }
    }

}
