using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Merchandising.DomainModel.Vehicles;

namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class MostSearchedTerm : ICategorizedEquipment
    {
        internal MostSearchedTerm(IDataRecord record)
        {
            ValidDescriptors = new List<string>(record.GetString(record.GetOrdinal("searchKeywords")).Split(",".ToCharArray()));
            Ranking = record.GetInt32(record.GetOrdinal("mostSearchedRanking"));
            CategoryId = record.GetInt32(record.GetOrdinal("categoryId"));
        }
        public MostSearchedTerm(int categoryId, int ranking, List<string> validDescriptors)
        {
            CategoryId = categoryId;
            Ranking = ranking;
            ValidDescriptors = validDescriptors;
        }

        public int CategoryId { get; set; }

        public int Ranking { get; set; }

        public List<string> ValidDescriptors { get; set; }

        public override string ToString()
        {
            if (ValidDescriptors.Count > 0)
            {
                Random rand = new Random();
                return ValidDescriptors[rand.Next(ValidDescriptors.Count)];
            }
            return string.Empty;
        }

        public int GetCategoryId()
        {
            return CategoryId;
        }
    }
}
