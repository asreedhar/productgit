namespace FirstLook.Merchandising.DomainModel.Templating
{
    public class GasMileagePreference
    {
        public GasMileagePreference(int segmentId, string segmentName, int cityMileage, int highwayMileage)
        {
            CityMileage = cityMileage;
            HighwayMileage = highwayMileage;
            SegmentId = segmentId;
            SegmentName = segmentName;
        }

        public int SegmentId { get; set; }

        public string SegmentName { get; set; }

        public int CityMileage { get; set; }

        public int HighwayMileage { get; set; }
    }
}