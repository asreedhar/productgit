namespace FirstLook.Merchandising.DomainModel.MapVehicles
{
    public class VehicleMake
    {
        public VehicleMake(int makeId, string name)
        {
            MakeId = makeId;
            Name = name;
        }

        public int MakeId { get; set; }

        public string Name { get; set; }
    }
}
