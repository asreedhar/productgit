using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using FirstLook.Common.Data;


namespace FirstLook.Merchandising.DomainModel.MapVehicles
{
    [DataObject(true)]
    
    public class GroupGridViewSource
    {
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static DataTable GroupGetStyleID(int divisionID, int? marketClassId, string modelName, string trim, int? modelYear)
        {                        
            using (IDataConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase, false))
            {
                conn.Open();
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText =
                        @"SELECT 
                            StyleID, 
                            div.divisionId,                     
                            div.divisionName,
                            mdl.ModelYear, 
                            mdl.ModelName,
                            Trim,
                            StyleName 
                            from VehicleCatalog.Chrome.Styles sty
                            JOIN VehicleCatalog.Chrome.Models mdl
	                            ON sty.ModelID = mdl.ModelID
	                        JOIN VehicleCatalog.Chrome.Divisions div
		                        ON div.DivisionID=mdl.DivisionID
                            WHERE 
                                (@divisionId IS NULL OR div.DivisionID=@divisionID)
                                AND (@modelName IS NULL OR mdl.ModelName=@modelName)
                                AND (@modelYear IS NULL OR mdl.ModelYear = @modelYear)
                                AND (@trim IS NULL OR sty.Trim=@trim) 
                                AND (@mktClassID IS NULL or sty.mktClassId = @mktClassID)
                                AND sty.CountryCode = 1
                                AND div.CountryCode = 1
                                AND mdl.CountryCode = 1
                            Order by StyleID";
                    cmd.CommandType = CommandType.Text;                 

                    Database.AddRequiredParameter(cmd, "divisionId", divisionID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "modelName", modelName, DbType.String);
                    Database.AddRequiredParameter(cmd, "trim", trim, DbType.String);
                    Database.AddRequiredParameter(cmd, "modelYear", modelYear, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "mktClassId", marketClassId, DbType.Int32);

                    SqlDataAdapter sda = new SqlDataAdapter((SqlCommand)cmd);
                    DataSet ds = new DataSet();
                    sda.Fill(ds);

                    return ds.Tables.Count > 0 ? ds.Tables[0] : new DataTable();
                }                                
            }
        }
    }
}