﻿using System.Collections.Generic;
using System.ComponentModel;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.MapVehicles
{
    [DataObject(true)]
    public class VehicleTypeDataSource
    {
        public List<VehicleType> GetVehicleTypes()
        {
            return new List<VehicleType> { VehicleType.Both, VehicleType.New, VehicleType.Used };
        }
    }
}