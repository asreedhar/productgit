using System.Collections.Generic;
using System.Data;
using System.ComponentModel;


namespace FirstLook.Merchandising.DomainModel.MapVehicles
{
    [DataObject(true)]
    public class DataSourceYear
    {
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<int> GetYear(int divisionId, string modelName, string trim)
        {
            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                conn.Open();
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText =
                            @"select DISTINCT mdl.ModelYear 
                        From Chrome.Models as mdl, Chrome.Divisions as div, Chrome.Styles as sty
                        Where div.DivisionID = div.DivisionID
                        AND mdl.ModelID = sty.ModelID
                        AND div.DivisionId = @divisionId
                        AND ModelName = @modelName
                        AND (@trim IS NULL OR sty.trim = @trim)
                        AND sty.CountryCode=1
                        AND mdl.CountryCode = 1
                        AND div.CountryCode = 1
                        ORDER BY ModelYear";
                    cmd.CommandType = CommandType.Text;
                    Database.AddRequiredParameter(cmd, "divisionId", divisionId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "modelName", modelName, DbType.String);
                    Database.AddRequiredParameter(cmd, "trim", trim, DbType.String);
                    
                    List<int> years = new List<int>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            years.Add((int) reader["modelYear"]);
                        }
                    }
                    return years;
                }
            }
          
                 
        }
    }
}