using System.Collections.Generic;
using System.Data;
using System.ComponentModel;

namespace FirstLook.Merchandising.DomainModel.MapVehicles
{
    [DataObject(true)]
    public class DataSourceMake
    {
        [DataObjectMethod(DataObjectMethodType.Select, true)]
// ReSharper disable UnusedMember.Global -- Used by VehicleSetCreator.ascx zb
        public static List<VehicleMake> GetMake()
// ReSharper restore UnusedMember.Global
        {
            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                conn.Open();
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText =
                        @"select DISTINCT DivisionName, divisionid
                            From Chrome.Divisions div, Chrome.Models mdl, Chrome.Styles sty
                            Where mdl.DivisionID = div.DivisionID
                            AND mdl.ModelID = sty.ModelID
                            AND sty.CountryCode=1
                            AND div.CountryCode=1
                            AND mdl.CountryCode=1
                            ORDER BY DivisionName";
                    cmd.CommandType = CommandType.Text;

                    List<VehicleMake> makes = new List<VehicleMake>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            makes.Add(new VehicleMake((int) reader["divisionId"], (string) reader["divisionName"]));
                        }
                    }
                    return makes;
                }
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<VehicleMake> GetMake(int? mktClassID)
        {
            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                conn.Open();
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText =
                            @"select DISTINCT div.DivisionName, div.DivisionID 
                                From VehicleCatalog.Chrome.Divisions as div, 
                                    VehicleCatalog.Chrome.Models mdl, 
                                    VehicleCatalog.Chrome.Styles sty
                                Where mdl.DivisionID = div.DivisionID
                                AND mdl.ModelID = sty.ModelID
                                AND (@MktClassID IS NULL OR sty.MktClassID = @MktClassID)
                                AND sty.CountryCode=1

                                ORDER BY DivisionName";
                    cmd.CommandType = CommandType.Text;
                    Database.AddRequiredParameter(cmd, "MktClassID", mktClassID, DbType.Int32);
                    
                    List<VehicleMake> makes = new List<VehicleMake>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            makes.Add(new VehicleMake((int) reader["divisionId"], (string) reader["divisionName"]));
                        }
                    }
                    return makes;
                }
            }
        }
    }
}