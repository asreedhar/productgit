using System;
using System.Collections.Generic;
using System.Data;
using System.ComponentModel;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.MapVehicles
{
    [DataObject(true)]
    public class DataSourceModel
    {
        private class ByName : IComparer<VehicleModel>
        {
            public int Compare(VehicleModel first, VehicleModel second)
            {
                return StringComparer.InvariantCulture.Compare(first.Name, second.Name);
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<VehicleModel> GetModelNameByID(int? divisionID, int? mktClassID)
        {
            if (!divisionID.HasValue && !mktClassID.HasValue)
            {
                return new List<VehicleModel>();
            }
            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                conn.Open();
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    //only use the modelName b/c this allows us to group by name
                    cmd.CommandText =
                        @"select DISTINCT ModelName
                                FROM Chrome.Models, Chrome.Divisions, Chrome.Styles
                                Where Chrome.Models.DivisionID = Chrome.Divisions.DivisionID
                                AND Chrome.Models.ModelID = Chrome.Styles.ModelID
                                AND (@DivisionID IS NULL OR Chrome.Divisions.DivisionID = @DivisionID)
                                AND (@MktClassID IS NULL OR Chrome.Styles.MktClassID = @MktClassID)
                                AND Chrome.Styles.CountryCode=1";

                    Database.AddRequiredParameter(cmd, "DivisionID", divisionID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "MktClassID", mktClassID, DbType.Int32);
                    
                    List<VehicleModel> models = new List<VehicleModel>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            models.Add(new VehicleModel((string) reader["modelName"]));
                        }
                    }
                    
                    models.Sort(new ByName());
                    
                    return models;
                }
            }
        }

    }
}