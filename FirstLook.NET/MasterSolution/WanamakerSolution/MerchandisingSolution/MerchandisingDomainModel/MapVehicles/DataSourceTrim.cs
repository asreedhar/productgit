using System.Collections.Generic;
using System.Data;
using System.ComponentModel;


namespace FirstLook.Merchandising.DomainModel.MapVehicles
{
    [DataObject(true)]
    public class DataSourceTrim
    {
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public static List<string> GetTrimByID(int divisionId, string modelName, int? mktClassId)
        {

            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                conn.Open();
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText =
                        @"Select Distinct Trim
                            FROM VehicleCatalog.Chrome.Styles sty
                            JOIN VehicleCatalog.Chrome.Models mdl
	                            ON sty.ModelID = mdl.ModelID
	                        JOIN VehicleCatalog.Chrome.Divisions div
	                        ON mdl.DivisionID = div.DivisionID
                            Where div.DivisionID=@divisionID
                            AND mdl.ModelName=@modelName
                            AND (@mktClassId IS NULL OR sty.mktClassId = @mktClassId)
                            AND sty.CountryCode=1
                            AND div.CountryCode=1
                            AND mdl.CountryCode=1";
                    Database.AddRequiredParameter(cmd, "divisionId", divisionId, DbType.Int32);
                    if (string.IsNullOrEmpty(modelName))
                    {
                        modelName = string.Empty;
                    }
                    Database.AddRequiredParameter(cmd, "modelName", modelName, DbType.String);
                    Database.AddRequiredParameter(cmd, "mktClassID", mktClassId, DbType.Int32);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<string> returnTrims = new List<string>();

                        while (reader.Read())
                        {
                            returnTrims.Add((string)reader["Trim"]);
                        }
                        return returnTrims;
                    }
                }
            }
        }
    }
}