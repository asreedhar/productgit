﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Merchandising.DomainModel
{
    public class MerchandisingContext
    {
        public int BusinessUnitId { get; private set; }
        public int InventoryId { get; private set; }
        public String Vin { get; private set; }

        private MerchandisingContext()
        {
        }

        public static MerchandisingContext FromPricing(PricingContext context)
        {
            return FromPricing(context.OwnerHandle, context.VehicleHandle);
        }

        public static MerchandisingContext FromPricing(string ownerHandle, string vehicleHandle)
        {
            MerchandisingContext context = new MerchandisingContext();

            var inventory = Inventory.GetInventory(ownerHandle, vehicleHandle);
            Owner owner = Owner.GetOwner(ownerHandle);

            context.BusinessUnitId = owner.OwnerEntityId;
            context.InventoryId = inventory.Id;
            context.Vin = inventory.Vin;

            return context;
        }

    }
}
