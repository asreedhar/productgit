﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GetReportSettings : Common.Core.Command.AbstractCommand
    {
        private const int CacheTimeoutPolicy = 5 * 60;

        public int BusinessUnitId { get; private set; }

        public bool HasSettings { get; private set; }
        
        public Single LowActivityThreshold { get; private set; }
        public Single HighActivityThreshold { get; private set; }
        public int MinSearchCountThreshold { get; private set; }
        public int TimeToMarketGoalDays { get; private set; }

        public int VehicleOnlineStatusReport_MinAgeDays { get; private set; }
        public int AdCompleteFlags { get; private set; }

        public MerchandisingGraphSettings MerchandisingGraphDefault { get; private set; }

        public GetReportSettings(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
            HasSettings = false;
        }

        protected override void Run()
        {
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "settings.ReportSettings#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("businessUnitID", BusinessUnitId, DbType.Int32);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            HasSettings = true;

                            if (reader.ColumnExists("lowActivityThreshold") && (reader["lowActivityThreshold"] != DBNull.Value))
                                LowActivityThreshold = Convert.ToSingle(reader["lowActivityThreshold"]);
                            if (reader.ColumnExists("highActivityThreshold") && reader["highActivityThreshold"] != DBNull.Value)
                                HighActivityThreshold = Convert.ToSingle(reader["highActivityThreshold"]);
                            if (reader.ColumnExists("minSearchCountThreshold") && (reader["minSearchCountThreshold"] != DBNull.Value))
                                MinSearchCountThreshold = reader.GetInt32(reader.GetOrdinal("minSearchCountThreshold"));
                            if (reader.ColumnExists("VehicleOnlineStatusReportMinAgeDays") && (reader["VehicleOnlineStatusReportMinAgeDays"] != DBNull.Value))
                                VehicleOnlineStatusReport_MinAgeDays = reader.GetInt32(reader.GetOrdinal("VehicleOnlineStatusReportMinAgeDays"));
                            if (reader.ColumnExists("AdCompleteFlags") && (reader["AdCompleteFlags"] != DBNull.Value))
                                AdCompleteFlags = reader.GetInt32(reader.GetOrdinal("AdCompleteFlags"));
                            else
                                AdCompleteFlags = 7; // The default is all boxes checked.
                            if (reader.ColumnExists("TimeToMarketGoalDays") && reader["TimeToMarketGoalDays"] != DBNull.Value)
                                TimeToMarketGoalDays = reader.GetInt32(reader.GetOrdinal("TimeToMarketGoalDays"));

                            if (reader.ColumnExists("MerchandisingGraphDefault") && (reader["MerchandisingGraphDefault"] != DBNull.Value))
                                MerchandisingGraphDefault = (MerchandisingGraphSettings)reader.GetByte(reader.GetOrdinal("MerchandisingGraphDefault"));
                        }
                    }
                }
            }
        }

        public static GetReportSettings GetSettings(int businessUnitId)
        {
            return LoadFromCache(businessUnitId) ?? LoadFromDatabase(businessUnitId);
        }

        private static GetReportSettings LoadFromDatabase(int businessUnitId)
        {
            var settings = new GetReportSettings(businessUnitId);
            settings.Run();
            SaveToCache(settings);
            return settings;
        }

        private static string GetCacheKey(int businessUnitId)
        {
            return typeof (GetReportSettings).FullName + "_" + businessUnitId;
        }

        private static ICache GetCache()
        {
            return Registry.Resolve<ICache>();
        }

        private static void SaveToCache(GetReportSettings settings)
        {
            GetCache().Set(GetCacheKey(settings.BusinessUnitId), settings, CacheTimeoutPolicy);
        }

        private static GetReportSettings LoadFromCache(int businessUnitId)
        {
            return (GetReportSettings) GetCache().Get(GetCacheKey(businessUnitId));
        }

        public static void InvalidateCache(int businessUnitId)
        {
            GetCache().Delete(GetCacheKey(businessUnitId));
        }
    }
}

