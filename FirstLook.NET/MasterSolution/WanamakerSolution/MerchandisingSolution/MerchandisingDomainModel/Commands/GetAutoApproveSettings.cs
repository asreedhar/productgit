﻿using System;
using System.Data;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GetAutoApproveSettings : Common.Core.Command.AbstractCommand
    {
        private static readonly string CacheKey = typeof(GetAutoApproveSettings).FullName;

        public int BusinessUnitId { get; private set; }
        public bool HasSettings { get; private set; }

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public AutoApproveStatus Status { get; private set; }

        public DateTime? CreatedOn { get; private set; }
        public string CreatedBy { get; private set; }
        public DateTime? UpdatedOn { get; private set; }
        public string UpdatedBy { get; private set; }
        public bool EmailBounced { get; private set; }

        public GetAutoApproveSettings ( int businessUnitId )
        {
            BusinessUnitId = businessUnitId;
            HasSettings = false;
        }

        public static GetAutoApproveSettings GetSettings(int businessUnitId)
        {
            return LoadFromCache(businessUnitId) ?? LoadFromDatabase(businessUnitId);
        }

        private static GetAutoApproveSettings LoadFromDatabase(int businessUnitId)
        {
            var settings = new GetAutoApproveSettings(businessUnitId);
            DoRun(settings);
            SaveToCache(settings);
            return settings;
        }

        private static GetAutoApproveSettings LoadFromCache(int businessUnitId)
        {
            return RequestCache.Fetch<GetAutoApproveSettings>(CacheKey + businessUnitId);
        }

        private static void SaveToCache(GetAutoApproveSettings settings)
        {
            RequestCache.Save(CacheKey + settings.BusinessUnitId, settings);
        }

        protected override void Run ()
        {
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "settings.AutoApprove#Fetch";
                command.CommandType = CommandType.StoredProcedure;
                Database.AddRequiredParameter(command, "businessUnitID", BusinessUnitId, DbType.Int32);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        HasSettings = true;

                        if (reader.ColumnExists("FirstName") && (reader["FirstName"] != DBNull.Value))
                            FirstName = reader["FirstName"].ToString();

                        if (reader.ColumnExists("LastName") && (reader["LastName"] != DBNull.Value))
                            LastName = reader["LastName"].ToString();

                        if (reader.ColumnExists("Email") && reader["Email"] != DBNull.Value)
                            Email = reader["Email"].ToString();

                        if (reader.ColumnExists("Status") && (reader["Status"] != DBNull.Value))
                            Status = (AutoApproveStatus) reader.GetInt32(reader.GetOrdinal("Status"));

                        if (reader.ColumnExists("CreatedOn") && (reader["CreatedOn"] != DBNull.Value))
                            CreatedOn = (DateTime) reader["CreatedOn"];

                        if (reader.ColumnExists("CreatedBy") && (reader["CreatedBy"] != DBNull.Value))
                            CreatedBy = reader["CreatedBy"].ToString();

                        if (reader.ColumnExists("UpdatedOn") && reader["UpdatedOn"] != DBNull.Value)
                            UpdatedOn = (DateTime) reader["UpdatedOn"];

                        if (reader.ColumnExists("UpdatedBy") && (reader["UpdatedBy"] != DBNull.Value))
                            UpdatedBy = reader["UpdatedBy"].ToString();

                        EmailBounced = reader.GetBoolean(reader.GetOrdinal("EmailBounced"));
                    }
                }
            }
        }

    }
}
