﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.DomainModel.Oltp;
using MAX.Entities.Reports.Dashboard;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GoogleAnalyticsGroupTrafficReport
    {
        #region Properties
        private readonly int _groupId;

        private DateTime _beginDate = DateTime.MinValue;
        private DateTime _endDate = DateTime.MaxValue;
        private Boolean _fetchMonth = false;

        public GroupTraffic GroupTrafficDto
        {
            get { return _groupTrafficDto ?? (_groupTrafficDto = new GroupTraffic(_groupId)); }
        }
        private GroupTraffic _groupTrafficDto;

        public Boolean HasData { get; private set; }
        #endregion

        #region Methods
        #region Construction
        public GoogleAnalyticsGroupTrafficReport(int groupId)
        {
            _groupId = groupId;
        }

        public GoogleAnalyticsGroupTrafficReport(int groupId, DateTime beginDate, DateTime endDate)
            : this(groupId)
        {
            _beginDate = beginDate;
            _endDate = endDate;
        }
        #endregion

        public Boolean FetchWebTraffic()
        {
            var beginDate = DateTime.UtcNow.ToString("yyyy-MM");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM");

            if (_beginDate != DateTime.MinValue)
                beginDate = _beginDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");
            if (_endDate != DateTime.MaxValue)
                endDate = _endDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");

            var getObjectReport = new GetGoogleAnalyticsGroupWebTrafficReport(_groupId, beginDate, endDate);
            AbstractCommand.DoRun(getObjectReport);

            HasData = getObjectReport.HasData;
            if(HasData)
                GroupTrafficDto.DealershipTrafficList.AddRange(getObjectReport.WebTrafficDtoList);

            return HasData;
        }

        public Boolean FetchWebTraffic(DateTime beginDate, DateTime endDate)
        {
            _beginDate = beginDate;
            _endDate = endDate;

            return FetchWebTraffic();
        }

        public Boolean FetchWebTrafficByMonths(DateTime beginDate, DateTime endDate)
        {
            _fetchMonth = true;

            return FetchWebTraffic(beginDate, endDate);
        }

        public Boolean FetchShowroomTraffic()
        {
            var beginDate = DateTime.UtcNow.ToString("yyyy-MM");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM");

            if (_beginDate != DateTime.MinValue)
                beginDate = _beginDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");
            if (_endDate != DateTime.MaxValue)
                endDate = _endDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");

            var getObjectReport = new GetGoogleAnalyticsGroupShowroomTrafficReport(_groupId, beginDate, endDate);
            AbstractCommand.DoRun(getObjectReport);

            HasData = getObjectReport.HasData;
            if (HasData)
                GroupTrafficDto.DealershipTrafficList.AddRange(getObjectReport.ShowroomTrafficDtoList);

            return HasData;
        }

        public Boolean FetchShowroomTraffic(DateTime beginDate, DateTime endDate)
        {
            _beginDate = beginDate;
            _endDate = endDate;

            return FetchShowroomTraffic();
        }

        public Boolean FetchShowroomTrafficByMonths(DateTime beginDate, DateTime endDate)
        {
            _fetchMonth = true;

            return FetchShowroomTraffic(beginDate, endDate);
        }
        #endregion
    }

    public class GetGoogleAnalyticsGroupWebTrafficReport : AbstractCommand
    {
        #region Properties
        private readonly int _groupId;

        private readonly String _beginDate;
        private readonly String _endDate;

        public List<WebTraffic> WebTrafficDtoList
        {
            get { return _webTrafficDtoList ?? (_webTrafficDtoList = new List<WebTraffic>()); }
        }
        private List<WebTraffic> _webTrafficDtoList;

        public Boolean HasData { get { return WebTrafficDtoList.Any(); } }
        #endregion

        #region Methods
        #region Construction
        public GetGoogleAnalyticsGroupWebTrafficReport(int businessUnitId)
        {
            _groupId = businessUnitId;

            _beginDate = String.Empty;
            _endDate = String.Empty;
        }

        public GetGoogleAnalyticsGroupWebTrafficReport(int businessUnitId, String beginDate, String endDate)
            : this(businessUnitId)
        {
            _beginDate = beginDate;
            _endDate = endDate;
        }
        #endregion

        #region Overrides
        protected override void Run()
        {
            var groupBusinessUnit = BusinessUnitFinder.Instance().Find(_groupId);
            var dealerList = groupBusinessUnit.Dealerships();

            foreach (var webTrafficReport in dealerList.Select(dealer => new GetGoogleAnalyticsWebTrafficReport(dealer.Id, _beginDate, _endDate)))
            {
                DoRun(webTrafficReport);

                if(webTrafficReport.HasData)
                    WebTrafficDtoList.Add(webTrafficReport.WebTrafficDto);
            }
        }
        #endregion
        #endregion
    }

    public class GetGoogleAnalyticsGroupShowroomTrafficReport : AbstractCommand
    {
        #region Properties
        private readonly int _groupId;

        private readonly String _beginDate;
        private readonly String _endDate;

        public List<ShowroomTraffic> ShowroomTrafficDtoList
        {
            get { return _showroomTrafficDtoList ?? (_showroomTrafficDtoList = new List<ShowroomTraffic>()); }
        }
        private List<ShowroomTraffic> _showroomTrafficDtoList;

        public Boolean HasData { get { return ShowroomTrafficDtoList.Any(); } }
        #endregion

        #region Methods
        #region Construction
        public GetGoogleAnalyticsGroupShowroomTrafficReport(int businessUnitId)
        {
            _groupId = businessUnitId;

            _beginDate = String.Empty;
            _endDate = String.Empty;
        }

        public GetGoogleAnalyticsGroupShowroomTrafficReport(int businessUnitId, String beginDate, String endDate)
            : this(businessUnitId)
        {
            _beginDate = beginDate;
            _endDate = endDate;
        }
        #endregion

        #region Overrides
        protected override void Run()
        {
            var groupBusinessUnit = BusinessUnitFinder.Instance().Find(_groupId);
            var dealerList = groupBusinessUnit.Dealerships();

            foreach (var showroomTrafficReport in dealerList.Select(dealer => new GetGoogleAnalyticsShowroomTrafficReport(dealer.Id, _beginDate, _endDate)))
            {
                DoRun(showroomTrafficReport);

                if (showroomTrafficReport.HasData)
                    ShowroomTrafficDtoList.Add(showroomTrafficReport.ShowroomTrafficDto);
            }
        }
        #endregion
        #endregion
    }
}
