﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects;
using Twilio;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    [Serializable]
    public class DealerPhoneMapping
    {
        #region Properties
        public const String ErrorAlreadyPurchased = "A number has already been purchased.";
        public const String ErrorAreaCodeIsNullOrWhiteSpace = "The area code is empty.";
        public const String ErrorAreaCodeIsTollFree = "The area code cannot be a toll free area code.";
        public const String ErrorAreaCodeIsNotNumeric = "The area code entered is not numeric.";
        public const String ErrorNoNumbersAvailable = "There were no numbers available for the area code entered. Do you want to generate a toll-free number?";
        public const String ErrorMappedPhoneIsNullOrWhiteSpace = "The mapped phone number is empty.";
        public const String ErrorNoTollFreeNumbersAvailable = "There were no toll-free numbers available.";

        private const String Pattern = @".?[\+]? ?[\d]? ?[-.,]? ?[\(]? ?([2-9]\d\d) ?[\)]? ?[-.,]? ?([2-9]\d\d) ?[-.,]? ?(\d{4})";

        private readonly String _accountSid = Properties.Twilio.Default.AccountSid;
        private readonly String _authToken = Properties.Twilio.Default.AuthToken;
        private readonly String[] _tollNumbers = { "800", "844", "855", "866", "877", "888" };

        public int BusinessUnitId { get; private set; }
        public Boolean HasSettings { get; private set; }
        public virtual IDataConnection Connection
        {
            get { return Database.GetOpenConnection(Database.MerchandisingDatabase); }
        }
        private DealerGeneralPreferenceTO _dealerPreference;
        public virtual DealerGeneralPreferenceTO DealerPreference
        {
            get
            {
                try
                {
                    if(_dealerPreference == null)
                        _dealerPreference = DealerGeneralPreference.GetDealerGeneralPreference(Owner.GetOwner(BusinessUnitId).Handle).ToTransferObject();
                }
                catch (Exception) // catch when the dealer does not have an entry in DealerGeneralPreferences
                {
                    _dealerPreference = new DealerGeneralPreferenceTO(Owner.GetOwner(BusinessUnitId).Handle);
                }

                return _dealerPreference;
            }
        }
        private String _mappedPhone;
        public String MappedPhone
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_mappedPhone))
                    _mappedPhone = PhoneToFriendlyName(DealerPreference.SalesPhoneNumber);

                return _mappedPhone;
            }
            private set { _mappedPhone = value; }
        }
        public String MappedPhoneAreaCode
        {
            get
            {
                Int32 areaCode = 0;
                if (MappedPhone.Length > 4)
                {
                    Int32.TryParse(MappedPhone.Substring(1, 3), out areaCode);
                }

                return areaCode > 0 ? areaCode.ToString(CultureInfo.InvariantCulture) : String.Empty;
            }
        }
        public virtual String ProvisionedPhone { get; private set; }
        public virtual Boolean Purchased { get; private set; }
        public virtual Boolean IsTollFree { get; private set; }
        #endregion

        public DealerPhoneMapping(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;

            HasSettings = false;
            ProvisionedPhone = String.Empty;
            Purchased = false;
            IsTollFree = false;
        }

        public void Fetch()
        {
            using (var command = Connection.CreateCommand())
            {
                command.CommandText = "settings.DealerPhoneMapping#Fetch";
                command.CommandType = CommandType.StoredProcedure;
                command.AddRequiredParameter("BusinessUnitID", BusinessUnitId, DbType.Int32);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        HasSettings = true;
                        MappedPhone = reader.GetString(reader.GetOrdinal("MappedPhone"));
                        ProvisionedPhone = reader.GetString(reader.GetOrdinal("ProvisionedPhone"));
                        Purchased = reader.GetBoolean(reader.GetOrdinal("Purchased"));
                    }
                }
            }
        }

        public void Upsert(String mappedPhone)
        {
            if(String.IsNullOrWhiteSpace(mappedPhone))
                throw new ApplicationException(ErrorMappedPhoneIsNullOrWhiteSpace);

            MappedPhone = PhoneToFriendlyName(mappedPhone);

            if (!Purchased)
                PurchaseNumber();

            if (!Purchased && !HasSettings) return;

            using (var command = Connection.CreateCommand())
            {
                command.CommandText = "settings.DealerPhoneMapping#Upsert";
                command.CommandType = CommandType.StoredProcedure;

                command.AddRequiredParameter("BusinessUnitID", BusinessUnitId, DbType.Int32);
                command.AddRequiredParameter("MappedPhone", MappedPhone, DbType.String);
                command.AddRequiredParameter("ProvisionedPhone", ProvisionedPhone, DbType.String);
                command.AddRequiredParameter("Purchased", Purchased, DbType.Boolean);

                command.ExecuteNonQuery();

                HasSettings = true;
            }
        }

        public Boolean GetNewTollFreeNumber()
        {
            if (Purchased)
                throw new ApplicationException(ErrorAlreadyPurchased);

            var twilio = new TwilioRestClient(_accountSid, _authToken);

            var availableNumbers = twilio.ListAvailableTollFreePhoneNumbers("US");

            if (availableNumbers.RestException != null)
                throw new ApplicationException(availableNumbers.RestException.Message);

            if (availableNumbers == null || availableNumbers.AvailablePhoneNumbers == null || availableNumbers.AvailablePhoneNumbers.Count < 1)
                throw new ApplicationException(ErrorNoTollFreeNumbersAvailable);

            ProvisionedPhone = PhoneToFriendlyName(availableNumbers.AvailablePhoneNumbers[0].PhoneNumber);
            IsTollFree = true;
            return true;
        }

        public Boolean GetNewNumber(String areaCode)
        {
            if (Purchased)
                throw new ApplicationException(ErrorAlreadyPurchased);

            if (String.IsNullOrWhiteSpace(areaCode))
                throw new ApplicationException(ErrorAreaCodeIsNullOrWhiteSpace);

            if (_tollNumbers.Contains(areaCode))
                throw new ApplicationException(ErrorAreaCodeIsTollFree);

            Int32 code;
            if(!Int32.TryParse(areaCode, out code))
                throw new ApplicationException(ErrorAreaCodeIsNotNumeric);

            var twilio = new TwilioRestClient(_accountSid, _authToken);
            
            var options = new AvailablePhoneNumberListRequest {AreaCode = areaCode};
            var availableNumbers = twilio.ListAvailableLocalPhoneNumbers("US", options);

            if (availableNumbers.RestException != null)
                throw new ApplicationException(availableNumbers.RestException.Message);

            if (availableNumbers == null || availableNumbers.AvailablePhoneNumbers == null ||
                availableNumbers.AvailablePhoneNumbers.Count < 1)
                return false;
                
            ProvisionedPhone = PhoneToFriendlyName(availableNumbers.AvailablePhoneNumbers[0].PhoneNumber);
            IsTollFree = false;
            return true;
        }

        public void PurchaseNumber()
        {
            var twilio = new TwilioRestClient(_accountSid, _authToken);
            var options = new PhoneNumberOptions
            {
                FriendlyName = DealerPreference.Name,
                PhoneNumber = PhoneToE164(ProvisionedPhone),
                VoiceUrl = String.Format("http://twimlets.com/forward?PhoneNumber={0}", Regex.Replace(MappedPhone, Pattern, @"$1$2$3"))
            };
            var number = IsTollFree ? twilio.AddIncomingTollFreePhoneNumber(options) : twilio.AddIncomingLocalPhoneNumber(options);
            
            if (number.RestException != null)
                throw new ApplicationException(number.RestException.Message);
            
            Purchased = true;
        }

        public String PhoneToE164(String phoneNumber)
        {
            return Regex.Replace(phoneNumber, Pattern, @"+1$1$2$3");
        }

        public String PhoneToFriendlyName(String phoneNumber)
        {
            return Regex.Replace(phoneNumber, Pattern, @"($1) $2-$3");
        }
    }
}
