using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class SetLastWindowStickerPrintDate : Common.Core.Command.AbstractCommand
    {
        private readonly int _inventoryId;
        public SetLastWindowStickerPrintDate(int inventoryId)
        {
            _inventoryId = inventoryId;
        }

        protected override void Run()
        {
            using (IDataConnection connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.Transaction = transaction;
                        command.CommandText = "WindowSticker.LastPrintDate#Save";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter(command, "InventoryId", _inventoryId, DbType.Int32);
                        Database.AddRequiredParameter(command, "TemplateTypeId", 1, DbType.Byte);
                        Database.AddRequiredParameter(command, "LatestPrintDate", DateTime.Now, DbType.DateTime);

                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }
        }
    }
}