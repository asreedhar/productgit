﻿using System.Data;
using FirstLook.Common.Data;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class SetBucketAlerts : Common.Core.Command.AbstractCommand
    {
        private readonly int _businessUnitId;
        private readonly bool _active;
        private readonly string _emails;
        private readonly string _weeklyEmails;
        private readonly string _bionicHealthReportEmails;
        private readonly string _groupBionicHealthReportEmails;
        private readonly int _newUsed;
        private readonly string _user;
        private readonly bool _emailBounced;
        private readonly bool _weeklyEmailBounced;
        private readonly bool _bionicHealthReportEmailBounced;
        private readonly bool _groupBionicHealthReportEmailBounced;
        private readonly BucketAlertThreshold[] _thresholds;

        public SetBucketAlerts(int businessUnitId, bool active, string emails, string weeklyEmails, string bionicHealthReportEmails, string groupBionicHealthReportEmails, int newUsed, string user, bool emailBounced, bool weeklyEmailBounced, bool bionicHealthReportEmailBounced, bool groupBionicHealthReportEmailBounced, BucketAlertThreshold[] thresholds)
        {
            _businessUnitId = businessUnitId;
            _active = active;
            _emails = emails;
            _weeklyEmails = weeklyEmails;
            _bionicHealthReportEmails = bionicHealthReportEmails;
            _groupBionicHealthReportEmails = groupBionicHealthReportEmails;
            _newUsed = newUsed;
            _user = user;
            _thresholds = thresholds;
            _emailBounced = emailBounced;
            _weeklyEmailBounced = weeklyEmailBounced;
            _bionicHealthReportEmailBounced = bionicHealthReportEmailBounced;
            _groupBionicHealthReportEmailBounced = groupBionicHealthReportEmailBounced;
        }

        protected override void Run ()
        {
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.Transaction = transaction;
                        command.CommandText = "settings.BucketAlerts#Upsert";
                        command.CommandType = CommandType.StoredProcedure;

                        command.AddRequiredParameter("BusinessUnitId", _businessUnitId, DbType.Int32);
                        command.AddRequiredParameter("Active", _active, DbType.Boolean);
                        command.AddRequiredParameter("Email", _emails, DbType.String);
                        command.AddRequiredParameter("WeeklyEmail", _weeklyEmails, DbType.String);
                        command.AddRequiredParameter("BionicHealthReportEmail", _bionicHealthReportEmails, DbType.String);
                        command.AddRequiredParameter("GroupBionicHealthReportEmail", _groupBionicHealthReportEmails, DbType.String);
                        command.AddRequiredParameter("NewUsed", _newUsed, DbType.Int32);
                        command.AddRequiredParameter("User", _user, DbType.String);
                        command.AddRequiredParameter("Bounced", _emailBounced, DbType.Boolean);
                        command.AddRequiredParameter("WeeklyBounced", _weeklyEmailBounced, DbType.Boolean);
                        command.AddRequiredParameter("BionicHealthReportEmailBounced", _bionicHealthReportEmailBounced, DbType.Boolean);
                        command.AddRequiredParameter("GroupBionicHealthReportEmailBounced", _groupBionicHealthReportEmailBounced, DbType.Boolean);

                        command.ExecuteNonQuery();
                    }

                    foreach (var threshold in _thresholds)
                    {
                        using (IDbCommand command = connection.CreateCommand())
                        {
                            command.Transaction = transaction;
                            command.CommandText = "settings.BucketAlertThreshold#Upsert";
                            command.CommandType = CommandType.StoredProcedure;

                            command.AddRequiredParameter("Id", threshold.Id, DbType.Int32);
                            command.AddRequiredParameter("BusinessUnitId", threshold.BusinessUnitId, DbType.Int32);
                            command.AddRequiredParameter("Bucket", threshold.Bucket, DbType.Int32);
                            command.AddRequiredParameter("Active", threshold.Active, DbType.Boolean);
                            command.AddRequiredParameter("Limit", threshold.Limit, DbType.Int32);
                            command.AddRequiredParameter("EmailActive", threshold.EmailActive, DbType.Int32); // FB: 27993

                            command.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                }
            }

        }
    }
}
