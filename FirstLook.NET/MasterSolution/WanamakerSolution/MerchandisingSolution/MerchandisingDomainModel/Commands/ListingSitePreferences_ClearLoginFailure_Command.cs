using System.Data;
using FirstLook.Common.Core.Command;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class ListingSitePreferences_ClearLoginFailure_Command : AbstractCommand
    {
        public int BusinessUnitId { get; private set; }
        public int DestinationId { get; private set; }

        public ListingSitePreferences_ClearLoginFailure_Command(int businessUnitId, int destinationId)
        {
            BusinessUnitId = businessUnitId;
            DestinationId = destinationId;
        }

        protected override void Run()
        {
            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandText = "Release.ListingSitePreferences#ClearLoginFailure";
                cmd.CommandType = CommandType.StoredProcedure;

                Database.AddRequiredParameter(cmd, "@businessUnitID", BusinessUnitId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "@destinationID", DestinationId, DbType.Int32);

                cmd.ExecuteNonQuery();
            }
        }

        public static void Execute(int businessUnitId, int destinationId)
        {
            var cmd = new ListingSitePreferences_ClearLoginFailure_Command(businessUnitId, destinationId);
            DoRun(cmd);
        }
    }
}