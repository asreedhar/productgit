using System;
using System.Data;
using FirstLook.Common.Core.Command;
using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class SetMiscSettings : AbstractCommand
    {
        private readonly int _businessUnitId;
        private readonly bool _sendOptimalFormat;
        private readonly Franchise _franchise;
        private readonly bool _priceNewCars;
        private readonly bool _showDashboard;
        private readonly bool _showGroupLevelDashboard;
        private readonly bool _analyticsSuite;
        private readonly byte _maxVersion;
        private readonly bool _webLoaderEnabled;
        private readonly bool _modelLevelFrameworksEnabled;
        private readonly bool _autoOfflineWholesalePlanTrigger;
        private readonly bool _showCtrGraph;
        ///
        public bool BatchAutoload;
        public DealershipSegment DealershipSegment;
        private readonly bool _bulkPriceNewCars;
        private readonly bool _showOnlineClassifiedOverview;
        private readonly bool _showTimeToMarket;

        private readonly GidProvider _newGidProvider;
        private readonly GidProvider _usedGidProvider;

        private readonly String _dmsName;
        private readonly String _dmsEmail;

        private readonly ReportDataSource _reportDataSourceNew;
        private readonly ReportDataSource _reportDataSourceUsed;

        public SetMiscSettings(int businessUnitId, bool sendOptimalFormat, Franchise franchise, bool priceNewCars, bool showDashboard, bool showGroupLevelDashboard, bool analyticsSuite, bool batchAutoload, byte maxVersion, bool webLoaderEnabled, bool modelLevelFrameworksEnabled, bool autoOfflineWholesalePlanTrigger, bool showCtrGraph, DealershipSegment dealershipSegment, bool bulkPriceNewCars, bool showOnlineClassifiedOverview, bool showTimeToMarket, GidProvider newGidProvider, GidProvider usedGidProvider, string dmsName, string dmsEmail, ReportDataSource reportDataSourceNew, ReportDataSource reportDataSourceUsed)
        {
            _businessUnitId = businessUnitId;
            _sendOptimalFormat = sendOptimalFormat;
            _franchise = franchise;
            _priceNewCars = priceNewCars;
            _showDashboard = showDashboard;
            _showGroupLevelDashboard = showGroupLevelDashboard;
            _analyticsSuite = analyticsSuite;
            BatchAutoload = batchAutoload;
            _maxVersion = maxVersion;
            _webLoaderEnabled = webLoaderEnabled;
            _modelLevelFrameworksEnabled = modelLevelFrameworksEnabled;
            _autoOfflineWholesalePlanTrigger = autoOfflineWholesalePlanTrigger;
            _showCtrGraph = showCtrGraph;
            DealershipSegment = dealershipSegment;
            _bulkPriceNewCars = bulkPriceNewCars;
            _showOnlineClassifiedOverview = showOnlineClassifiedOverview;
            _showTimeToMarket = showTimeToMarket;
            _newGidProvider = newGidProvider;
            _usedGidProvider = usedGidProvider;
            _dmsName = dmsName;
            _dmsEmail = dmsEmail;
            _reportDataSourceNew = reportDataSourceNew;
            _reportDataSourceUsed = reportDataSourceUsed;
        }
        
        protected override void Run()
        {
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var transaction = connection.BeginTransaction())
            using (var command = connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = "settings.MiscSettings#Update";
                command.CommandType = CommandType.StoredProcedure;
             
                command.AddRequiredParameter("BusinessUnitId", _businessUnitId, DbType.Int32);
                command.AddRequiredParameter("sendOptimalFormat", _sendOptimalFormat, DbType.Boolean);
                command.AddRequiredParameter("franchiseId", _franchise, DbType.Int32);
                command.AddRequiredParameter("priceNewCars", _priceNewCars, DbType.Boolean);
                command.AddRequiredParameter("showDashboard", _showDashboard, DbType.Boolean);
                command.AddRequiredParameter("showGroupLevelDashboard", _showGroupLevelDashboard, DbType.Boolean);
                command.AddRequiredParameter("analyticsSuite", _analyticsSuite, DbType.Boolean);
                command.AddRequiredParameter("batchAutoload", BatchAutoload, DbType.Boolean);
                command.AddRequiredParameter("MaxVersion", _maxVersion, DbType.Byte);
                command.AddRequiredParameter("WebLoaderEnabled", _webLoaderEnabled, DbType.Boolean);
                command.AddRequiredParameter("ModelLevelFrameworksEnabled", _modelLevelFrameworksEnabled, DbType.Boolean);
                command.AddRequiredParameter("AutoOffline_WholesalePlanTrigger", _autoOfflineWholesalePlanTrigger, DbType.Boolean);
                command.AddRequiredParameter("ShowCtrGraph", _showCtrGraph, DbType.Boolean);
                if( DealershipSegment != DealershipSegment.Undefined )
                    command.AddRequiredParameter("DealershipSegment", DealershipSegment, DbType.Int32);

                command.AddRequiredParameter("BulkPriceNewCars", _bulkPriceNewCars, DbType.Boolean);
                command.AddRequiredParameter("showOnlineClassifiedOverview", _showOnlineClassifiedOverview, DbType.Boolean);
                command.AddRequiredParameter("ShowTimeToMarket", _showTimeToMarket, DbType.Boolean);
                command.AddParameterWithValue("GIDProviderNew", (byte)_newGidProvider);
                command.AddParameterWithValue("GIDProviderUsed", (byte)_usedGidProvider);
                command.AddRequiredParameter("DMSName", _dmsName, DbType.String);
                command.AddRequiredParameter("DMSEmail", _dmsEmail, DbType.String);
                command.AddParameterWithValue("ReportDataSourceNew", (byte)_reportDataSourceNew);
                command.AddParameterWithValue("ReportDataSourceUsed", (byte)_reportDataSourceUsed);

                command.ExecuteNonQuery();

                GetMiscSettings.InvalidateCache(_businessUnitId);

                transaction.Commit();
            }

            // group-wide settings
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var transaction = connection.BeginTransaction())
            using (var command = connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = "settings.Merchandising_ShowGroupLevelDashboard#Update";
                command.CommandType = CommandType.StoredProcedure;
             
                command.AddRequiredParameter("businessUnitID", _businessUnitId, DbType.Int32);
                command.AddRequiredParameter("value", _showGroupLevelDashboard, DbType.Boolean);

                command.ExecuteNonQuery();
                transaction.Commit();
            }

        }
    }
}