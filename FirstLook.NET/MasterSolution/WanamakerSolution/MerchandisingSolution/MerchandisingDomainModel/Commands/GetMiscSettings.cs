﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using MAX.Entities;
using MAX.Entities.Enumerations;
using MAX.Entities.Extensions;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    [Serializable]
    public class GetMiscSettings
    {
        private const int CacheTimeoutPolicy = 30*60;

        private readonly int _businessUnitId;
        private readonly MiscSetting _miscSetting;

        // wrap entity for legacy calls
        public int BusinessUnitId { get { return _miscSetting.BusinessUnitId; } }
        public bool HasSettings { get { return _miscSetting.HasSettings; } }
        public bool OptimalFormat { get { return _miscSetting.OptimalFormat; } }
        public Franchise Franchise { get { return _miscSetting.Franchise; } }
        public bool PriceNewCars { get { return _miscSetting.PriceNewCars; } }
        public bool ShowDashboard { get { return _miscSetting.ShowDashboard; } }
        public bool ShowGroupLevelDashboard { get { return _miscSetting.ShowGroupLevelDashboard; } }
        public bool AnalyticsSuite { get { return _miscSetting.AnalyticsSuite; } }
        public bool BatchAutoload { get { return _miscSetting.BatchAutoload; } }
        public byte MaxVersion { get { return _miscSetting.MaxVersion; } }
        public bool WebLoaderEnabled { get { return _miscSetting.WebLoaderEnabled; } }
        public bool ModelLevelFrameworksEnabled { get { return _miscSetting.ModelLevelFrameworksEnabled; } }
        public bool AutoOffline_WholesalePlanTrigger { get { return _miscSetting.AutoOffline_WholesalePlanTrigger; } }
        public bool ShowCtrGraph { get { return _miscSetting.ShowCtrGraph; } }
        public DealershipSegment DealershipSegment { get { return _miscSetting.DealershipSegment; } }
        public bool BulkPriceNewCars { get { return _miscSetting.BulkPriceNewCars; } }
        public bool showOnlineClassifiedOverview { get { return _miscSetting.showOnlineClassifiedOverview; } }
        public bool ShowTimeToMarket { get { return _miscSetting.ShowTimeToMarket; } }
        public virtual GidProvider NewGidProvider { get { return _miscSetting.NewGidProvider; } }
        public virtual GidProvider UsedGidProvider { get { return _miscSetting.UsedGidProvider; } }
        public virtual ReportDataSource ReportDataSourceUsed { get { return _miscSetting.ReportDataSourceUsed; } }
        public virtual ReportDataSource ReportDataSourceNew { get { return _miscSetting.ReportDataSourceNew; } }
        public String DMSName { get { return _miscSetting.DMSName; } }
        public String DMSEmail { get { return _miscSetting.DMSEmail; } }
        // --

        public GetMiscSettings(int businessUnitId)
        {
            _businessUnitId = businessUnitId;
            _miscSetting = new MiscSetting(_businessUnitId);
        }

        public static GetMiscSettings GetSettings(int businessUnitId)
        {
            return LoadFromCache(businessUnitId) ?? LoadFromDatabase(businessUnitId);
        }

        private static GetMiscSettings LoadFromDatabase(int businessUnitId)
        {
            var settings = new GetMiscSettings(businessUnitId);
            settings.Run();
            SaveToCache(settings);
            return settings;
        }

        private static string GetCacheKey(int businessUnitId)
        {
            return typeof (GetMiscSettings).FullName + "_" + businessUnitId;
        }

        private static ICache GetCache()
        {
            return Registry.Resolve<ICache>();
        }

        private static void SaveToCache(GetMiscSettings settings)
        {
            GetCache().Set(GetCacheKey(settings.BusinessUnitId), settings, CacheTimeoutPolicy);
        }

        private static GetMiscSettings LoadFromCache(int businessUnitId)
        {
            return (GetMiscSettings) GetCache().Get(GetCacheKey(businessUnitId));
        }

        public static void InvalidateCache(int businessUnitId)
        {
            GetCache().Delete(GetCacheKey(businessUnitId));
        }

        protected void Run()
        {
			using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "settings.MiscSettings#Fetch";
                command.CommandType = CommandType.StoredProcedure;
                command.AddRequiredParameter("businessUnitID", BusinessUnitId, DbType.Int32);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        _miscSetting.HasSettings = true;
                        _miscSetting.OptimalFormat = reader.GetBoolean(reader.GetOrdinal("sendOptimalFormat"));
                        _miscSetting.Franchise = (Franchise)reader.GetInt32(reader.GetOrdinal("franchiseId"));
                        _miscSetting.PriceNewCars = reader.GetBoolean(reader.GetOrdinal("priceNewCars"));
                        _miscSetting.ShowDashboard = reader.GetBoolean(reader.GetOrdinal("showDashboard"));
                        _miscSetting.ShowGroupLevelDashboard = reader.GetBoolean(reader.GetOrdinal("ShowGroupLevelDashboard"));
                        _miscSetting.AnalyticsSuite = reader.GetBoolean(reader.GetOrdinal("analyticsSuite"));
                        _miscSetting.BatchAutoload = reader.GetBoolean(reader.GetOrdinal("batchAutoload"));
                        _miscSetting.MaxVersion = reader.GetByte(reader.GetOrdinal("MaxVersion"));
                        _miscSetting.WebLoaderEnabled = reader.GetBoolean(reader.GetOrdinal("WebLoaderEnabled"));
                        _miscSetting.ModelLevelFrameworksEnabled = reader.GetBoolean(reader.GetOrdinal("ModelLevelFrameworksEnabled"));
                        _miscSetting.AutoOffline_WholesalePlanTrigger = reader.GetBoolean(reader.GetOrdinal("AutoOffline_WholesalePlanTrigger"));
                        _miscSetting.ShowCtrGraph = reader.GetBoolean(reader.GetOrdinal("ShowCtrGraph"));
                        _miscSetting.DealershipSegment = reader.IsDBNull(reader.GetOrdinal("DealershipSegment")) ? DealershipSegment.Undefined : (DealershipSegment)reader.GetInt32(reader.GetOrdinal("DealershipSegment"));
                        _miscSetting.BulkPriceNewCars = reader.GetBoolean(reader.GetOrdinal("BulkPriceNewCars"));
                        _miscSetting.showOnlineClassifiedOverview = reader.GetBoolean(reader.GetOrdinal("showOnlineClassifiedOverview"));
                        _miscSetting.ShowTimeToMarket = reader.GetBoolean(reader.GetOrdinal("ShowTimeToMarket"));
                        _miscSetting.NewGidProvider = (GidProvider)reader.GetByte("GIDProviderNew");
                        _miscSetting.UsedGidProvider = (GidProvider)reader.GetByte("GIDProviderUsed");
                        if (reader.ColumnExists("DMSName") && reader["DMSName"] != DBNull.Value)
                            _miscSetting.DMSName = reader.GetString(reader.GetOrdinal("DMSName"));
                        if (reader.ColumnExists("DMSEmail") && reader["DMSEmail"] != DBNull.Value)
                            _miscSetting.DMSEmail = reader.GetString(reader.GetOrdinal("DMSEmail"));
                        _miscSetting.ReportDataSourceNew = (ReportDataSource) reader.GetByte("ReportDataSourceNew");
                        _miscSetting.ReportDataSourceUsed = (ReportDataSource)reader.GetByte("ReportDataSourceUsed");
                    }
                }
            }
        }

        public IList<MiscSetting> GetAllMiscSettings()
        {
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "settings.MiscSettings#FetchAll";
                command.CommandType = CommandType.StoredProcedure;

                using (var reader = command.ExecuteReader())
                {
                    return (
                        from dr in reader.Records()
                        select new MiscSetting(dr.GetInt32("BusinessUnitId"))
                        {
                            HasSettings = true,
                            OptimalFormat = dr.GetBoolean(dr.GetOrdinal("sendOptimalFormat")),
                            Franchise = (Franchise)dr.GetInt32(dr.GetOrdinal("franchiseId")),
                            PriceNewCars = dr.GetBoolean(dr.GetOrdinal("priceNewCars")),
                            ShowDashboard = dr.GetBoolean(dr.GetOrdinal("showDashboard")),
                            ShowGroupLevelDashboard = dr.GetBoolean(dr.GetOrdinal("ShowGroupLevelDashboard")),
                            AnalyticsSuite = dr.GetBoolean(dr.GetOrdinal("analyticsSuite")),
                            BatchAutoload = dr.GetBoolean(dr.GetOrdinal("batchAutoload")),
                            MaxVersion = dr.GetByte(dr.GetOrdinal("MaxVersion")),
                            WebLoaderEnabled = dr.GetBoolean(dr.GetOrdinal("WebLoaderEnabled")),
                            ModelLevelFrameworksEnabled = dr.GetBoolean(dr.GetOrdinal("ModelLevelFrameworksEnabled")),
                            AutoOffline_WholesalePlanTrigger = dr.GetBoolean(dr.GetOrdinal("AutoOffline_WholesalePlanTrigger")),
                            ShowCtrGraph = dr.GetBoolean(dr.GetOrdinal("ShowCtrGraph")),
                            DealershipSegment = dr.IsDBNull(dr.GetOrdinal("DealershipSegment")) ? DealershipSegment.Undefined : (DealershipSegment)dr.GetInt32(dr.GetOrdinal("DealershipSegment")),
                            BulkPriceNewCars = dr.GetBoolean(dr.GetOrdinal("BulkPriceNewCars")),
                            showOnlineClassifiedOverview = dr.GetBoolean(dr.GetOrdinal("showOnlineClassifiedOverview")),
                            ShowTimeToMarket = dr.GetBoolean(dr.GetOrdinal("ShowTimeToMarket")),
                            NewGidProvider = (GidProvider)dr.GetByte("GIDProviderNew"),
                            UsedGidProvider = (GidProvider)dr.GetByte("GIDProviderUsed"),
                            DMSName = dr.IsDBNull(dr.GetOrdinal("DMSName")) ? String.Empty : dr.GetString(dr.GetOrdinal("DMSName")),
                            DMSEmail = dr.IsDBNull(dr.GetOrdinal("DMSEmail")) ? String.Empty : dr.GetString(dr.GetOrdinal("DMSEmail")),
                            ReportDataSourceNew = (ReportDataSource) dr.GetByte("ReportDataSourceNew"),
                            ReportDataSourceUsed = (ReportDataSource)dr.GetByte("ReportDataSourceUsed")
                        }).ToList();
                }
            }
        }
    }
}

