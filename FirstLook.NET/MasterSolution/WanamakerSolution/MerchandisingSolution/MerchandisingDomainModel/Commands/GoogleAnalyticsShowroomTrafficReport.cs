﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using MAX.Entities.Reports.Dashboard;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GoogleAnalyticsShowroomTrafficReport
    {
        #region Properties
        private readonly int _businessUnitId;

        private DateTime _beginDate = DateTime.MinValue;
        private DateTime _endDate = DateTime.MaxValue;
        private Boolean _fetchMonth = false;

        public ShowroomTraffic ShowroomTrafficDto
        {
            get { return _showroomTrafficDto ?? (_showroomTrafficDto = new ShowroomTraffic(_businessUnitId)); }
        }
        private ShowroomTraffic _showroomTrafficDto;

        public Boolean HasData { get; private set; }
        #endregion

        #region Methods
        #region Construction
        public GoogleAnalyticsShowroomTrafficReport(int businessUnitId)
        {
            _businessUnitId = businessUnitId;
        }

        public GoogleAnalyticsShowroomTrafficReport(int businessUnitId, DateTime beginDate, DateTime endDate)
            : this(businessUnitId)
        {
            _beginDate = beginDate;
            _endDate = endDate;
        }
        #endregion

        public Boolean Fetch()
        {
            var beginDate = DateTime.UtcNow.ToString("yyyy-MM");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM");

            if (_beginDate != DateTime.MinValue)
                beginDate = _beginDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");
            if (_endDate != DateTime.MaxValue)
                endDate = _endDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");

            var getObjectReport = new GetGoogleAnalyticsShowroomTrafficReport(_businessUnitId, beginDate, endDate);
            AbstractCommand.DoRun(getObjectReport);

            HasData = getObjectReport.HasData;
            ShowroomTrafficDto.Clone(getObjectReport.ShowroomTrafficDto);

            return HasData;
        }

        public Boolean Fetch(DateTime beginDate, DateTime endDate)
        {
            _beginDate = beginDate;
            _endDate = endDate;

            return Fetch();
        }

        public Boolean FetchMonths(DateTime beginDate, DateTime endDate)
        {
            _fetchMonth = true;

            return Fetch(beginDate, endDate);
        }
        #endregion
    }

    class GetGoogleAnalyticsShowroomTrafficReport: AbstractCommand
    {
        #region Properties
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly int _businessUnitId;

        private readonly String _beginDate;
        private readonly String _endDate;

        private ISimpleDbFactory _simpleDbFactory
        {
            get { return Registry.Resolve<ISimpleDbFactory>(); }
        }

        public ShowroomTraffic ShowroomTrafficDto
        {
            get { return _showroomTrafficDto ?? (_showroomTrafficDto = new ShowroomTraffic(_businessUnitId)); }
        }
        private ShowroomTraffic _showroomTrafficDto;

        public Boolean HasData { get; private set; }
        #endregion

        #region Methods
        #region Construction
        public GetGoogleAnalyticsShowroomTrafficReport(int businessUnitId)
        {
            _businessUnitId = businessUnitId;

            _beginDate = String.Empty;
            _endDate = String.Empty;
        }

        public GetGoogleAnalyticsShowroomTrafficReport(int businessUnitId, String beginDate, String endDate)
            : this(businessUnitId)
        {
            _beginDate = beginDate;
            _endDate = endDate;
        }
        #endregion

        #region Overrides
        protected override void Run()
        {
            var gaReportDb = _simpleDbFactory.GetGoogleAnalyticsReportDb();

            var query =
                String.Format("SELECT * FROM `{0}` WHERE config_business_unit_id = '{1}'",
                    gaReportDb.ContainerName(),
                    _businessUnitId);

            if (!String.IsNullOrWhiteSpace(_beginDate) && String.IsNullOrWhiteSpace(_endDate))
                query += String.Format(" AND config_start_date >= '{0}'", _beginDate);
            if (!String.IsNullOrWhiteSpace(_endDate) && String.IsNullOrWhiteSpace(_beginDate))
                query += String.Format(" AND config_end_date <= '{0}'", _endDate);
            if (!String.IsNullOrWhiteSpace(_beginDate) && !String.IsNullOrWhiteSpace(_endDate))
                query += String.Format(" AND (config_start_yearmonth between '{0}' AND '{1}')",
                    _beginDate, _endDate);

            try
            {
                var results = gaReportDb.Select(query);
                if (results.Count <= 0) return;

                HasData = true;

                var businessUnit = BusinessUnitFinder.Instance().Find(_businessUnitId);
                ShowroomTrafficDto.DealershipName = businessUnit.Name;

                foreach (var row in results)
                {
                    var dsrVal = new ShowroomTraffic.DateValue
                    {
                        Time = DateTime.Parse(row["config_start_date"]),
                        Count = row.ContainsKey("dsr_pageviews") ? Double.Parse(row["dsr_pageviews"]) : 0
                    };
                    ShowroomTrafficDto.DigitalShowroom.Add(dsrVal);

                    var msrVal = new ShowroomTraffic.DateValue
                    {
                        Time = DateTime.Parse(row["config_start_date"]),
                        Count = row.ContainsKey("msr_pageviews") ? Double.Parse(row["msr_pageviews"]) : 0
                    };
                    ShowroomTrafficDto.MobileShowroom.Add(msrVal);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failure in the GetGoogleAnalyticsShowroomTrafficReport.", ex);
            }
        }
        #endregion
        #endregion
    }
}
