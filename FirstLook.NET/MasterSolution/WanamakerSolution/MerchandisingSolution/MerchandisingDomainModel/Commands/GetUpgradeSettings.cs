﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    [Serializable]
    public class GetUpgradeSettings
    {
        private const int CacheTimeoutPolicy = 30*60;

        public int BusinessUnitId { get; private set; }
        public bool HasSettings { get; private set; }
        public bool MAXForSmartphone { get; private set; }
        public int? AutoWindowStickerTemplateId { get; private set; }
        public bool MAXForWebsite20 { get; private set; }
		public bool MAXDigitalShowroom { get; private set; }
        public bool UsePhoneMapping { get; private set; }
        public bool UseDealerRater { get; private set; }
        public bool EnableBookValuations { get; private set; }
        public VehicleType VehiclesInShowroom { get; private set; }
        public bool EnableShowroomReports { get; private set; }

        private GetUpgradeSettings(){}

        public GetUpgradeSettings(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
            HasSettings = false;
        }

        public static List<GetUpgradeSettings> GetGroupUpgradeSettings(int groupId, bool allowCache = true)
        {
            var groupSettings = new List<GetUpgradeSettings>();
	        if (allowCache)
                groupSettings = LoadGroupFromCache(groupId);
	        if(groupSettings == null || !groupSettings.Any())
                groupSettings = LoadGroupFromDatabase(groupId);

            return groupSettings;
        }

        public static GetUpgradeSettings GetSettings(int businessUnitId, bool allowCache = true)
        {
	        if (allowCache)
                return LoadFromCache(businessUnitId) ?? LoadFromDatabase(businessUnitId);
	        return LoadFromDatabase(businessUnitId);
        }

	    private static GetUpgradeSettings LoadFromDatabase(int businessUnitId)
        {
            var settings = new GetUpgradeSettings(businessUnitId);
            settings.Run();
            SaveToCache(settings);
            return settings;
        }

        private static string GetCacheKey(int businessUnitId)
        {
            return typeof (GetUpgradeSettings).FullName + "_" + businessUnitId;
        }

        private static ICache GetCache()
        {
            return Registry.Resolve<ICache>();
        }

        private static void SaveToCache(GetUpgradeSettings settings)
        {
            GetCache().Set(GetCacheKey(settings.BusinessUnitId), settings, CacheTimeoutPolicy);
        }

        private static GetUpgradeSettings LoadFromCache(int businessUnitId)
        {
            return (GetUpgradeSettings) GetCache().Get(GetCacheKey(businessUnitId));
        }

        public static void InvalidateCache(int businessUnitId)
        {
            GetCache().Delete(GetCacheKey(businessUnitId));
        }

        protected void Run()
        {
			using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "settings.upgradeSettings#Fetch";
                command.CommandType = CommandType.StoredProcedure;
                command.AddRequiredParameter("businessUnitID", BusinessUnitId, DbType.Int32);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        HasSettings = true;
                        MAXForSmartphone = reader.GetBoolean(reader.GetOrdinal("maxForSmartphone"));
                        if (reader[reader.GetOrdinal("autoWindowStickerTemplateId")] != Convert.DBNull)
                            AutoWindowStickerTemplateId = reader.GetInt32(reader.GetOrdinal("autoWindowStickerTemplateId"));
                        else
                            AutoWindowStickerTemplateId = null;
                        MAXForWebsite20 = reader.GetBoolean(reader.GetOrdinal("MAXForWebsite20"));
						MAXDigitalShowroom = reader.GetBoolean(reader.GetOrdinal("MaxDigitalShowroom"));
                        UsePhoneMapping = reader.GetBoolean(reader.GetOrdinal("UsePhoneMapping"));
                        UseDealerRater = reader.GetBoolean(reader.GetOrdinal("UseDealerRater"));
                        EnableBookValuations = reader.GetBoolean(reader.GetOrdinal("EnableShowroomBookValuations"));
                        VehiclesInShowroom = reader.GetByte(reader.GetOrdinal("vehiclesInShowroom"));
                        EnableShowroomReports = reader.GetBoolean(reader.GetOrdinal("EnableShowroomReports"));
                    }
                }
            }
        }

        private static List<GetUpgradeSettings> LoadGroupFromDatabase(int groupId)
        {
            var groupSettings = new List<GetUpgradeSettings>();
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "settings.groupUpgradeSettings#Fetch";
                command.CommandType = CommandType.StoredProcedure;
                command.AddRequiredParameter("groupId", groupId, DbType.Int32);

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var settings = new GetUpgradeSettings
                        {
                            HasSettings = true,
                            MAXForSmartphone = reader.GetBoolean(reader.GetOrdinal("maxForSmartphone")),
                            MAXForWebsite20 = reader.GetBoolean(reader.GetOrdinal("MAXForWebsite20")),
                            MAXDigitalShowroom = reader.GetBoolean(reader.GetOrdinal("MaxDigitalShowroom")),
                            UsePhoneMapping = reader.GetBoolean(reader.GetOrdinal("UsePhoneMapping")),
                            UseDealerRater = reader.GetBoolean(reader.GetOrdinal("UseDealerRater")),
                            EnableBookValuations = reader.GetBoolean(reader.GetOrdinal("EnableShowroomBookValuations")),
                            VehiclesInShowroom = reader.GetByte(reader.GetOrdinal("vehiclesInShowroom")),
                            EnableShowroomReports = reader.GetBoolean(reader.GetOrdinal("EnableShowroomReports")),
                            BusinessUnitId = reader.GetInt32(reader.GetOrdinal("businessUnitID"))
                        };

                        if (reader[reader.GetOrdinal("autoWindowStickerTemplateId")] != Convert.DBNull)
                            settings.AutoWindowStickerTemplateId = reader.GetInt32(reader.GetOrdinal("autoWindowStickerTemplateId"));
                        else
                            settings.AutoWindowStickerTemplateId = null;
                        groupSettings.Add(settings);
                    }
                }
            }
            return groupSettings;
        }

        private static List<GetUpgradeSettings> LoadGroupFromCache(int groupId)
        {
            return (List<GetUpgradeSettings>)GetCache().Get(GetCacheKey(groupId));
        }
    }
}

