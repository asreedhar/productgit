﻿using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    /// <summary>
    /// Determines whether photo transfers are enabled for a given business unit. Can pass in either dealer or dealer-group business unit ID.
    /// </summary>
    public class GetPhotoTransfersEnabled : Common.Core.Command.AbstractCommand
    {
        public int BusinessUnitId { get; private set; }
        public bool PreferenceValue { get; private set; }

        public GetPhotoTransfersEnabled(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
        }

        protected override void Run()
        {
            using (IDataConnection connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "PhotoTransfers#GetGroupParticipation";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(command, "BusinessUnitId", BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(command, "enabled", null, DbType.Int32);
                    var enabledValueParam = ((IDataParameter)command.Parameters["enabled"]);
                    enabledValueParam.Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    if (enabledValueParam.Value != DBNull.Value)
                        PreferenceValue = ((int) enabledValueParam.Value) == 1;
                }
            }

        }
    }
}
