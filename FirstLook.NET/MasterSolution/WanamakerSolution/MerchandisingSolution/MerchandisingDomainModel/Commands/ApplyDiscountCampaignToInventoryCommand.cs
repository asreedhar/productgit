﻿using System.Linq;
using System.Reflection;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using log4net;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class ApplyDiscountCampaignToInventoryCommand : AbstractCommand
    {
        private readonly IDiscountPricingCampaignManager _campaignManager;
        private readonly IInventorySearch _inventorySearch;
        private readonly int _businessUnitId;
        private readonly int _inventoryId;
        private readonly string _activatedBy;
        private readonly bool _force;

        #region Logging


        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


        #endregion Logging

        public ApplyDiscountCampaignToInventoryCommand(IDiscountPricingCampaignManager campaignManager, IInventorySearch inventorySearch, int businessUnitId, int inventoryId, string activatedBy, bool force = false)
        {
            _campaignManager = campaignManager;
            _inventorySearch = inventorySearch;
            _businessUnitId = businessUnitId;
            _inventoryId = inventoryId;
            _activatedBy = activatedBy;
            _force = force;
        }

        protected override void Run()
        {
            // Find all the campaigns and apply it to the given inventory.
			var campaigns = _campaignManager.FetchActiveCampaignsNoInventory(_businessUnitId);
			Log.DebugFormat("Found {0} active campaigns for BU.", campaigns.Count);
			Log.Debug("Comparing active inventory data to campaign filter.");
            // Campaigns are ordered by campaign id. we want to sort by date descending.
            campaigns.Sort((c1, c2) => (c1.CreationDate < c2.CreationDate) ? 0 : 1);

            _inventorySearch.Initialize(_businessUnitId, VehicleType.New);

            var foundCampaign = false;
			
            foreach (var campaign in 
				campaigns
                .Where(campaign =>
                    _inventorySearch
						.ByCampaignFilter(campaign.CampaignFilter) // Set of currently active inventory that should be in a campaign
						.Select(inventoryList => inventoryList.InventoryID) // The list of inventory ids from that set
						.Contains(_inventoryId) // Contains the inventory id from this message/command?
                ))
            {
                Log.DebugFormat("Found a match for InventoryId {0} in CampaignId {1}", _inventoryId, campaign.CampaignId);
				foundCampaign = true;
				
				_campaignManager.LoadInventoryForCampaign(new [] {campaign}); // Load up the InventoryList for this campaign from the database/cache
                _campaignManager.LoadExclusionsForCampaign(new[] { campaign }); // Load up the Exclusion for this campaign from the database/cache
                
				if (_force || !campaign.InventoryList.Select(x => x.InventoryId).Contains(_inventoryId))
                {
					if(_force)
						Log.InfoFormat("Forced to reprocess this InventoryId {0} into campaign {1}.", _inventoryId, campaign.CampaignId);
					else
					{
					    // check for exclusion list (manual re-price) and don't add this motherF*)(*_( back in FB: 30572
                        if(campaign.InventoryExclusionList.Select(x => x.InventoryId).Contains(_inventoryId))
                        {
                            Log.WarnFormat("{0} triggered task found vehicle (inv id: {1}) in BU {2} that belongs in a campaign (ID: {3}) but currently isn't.  Not adding because it is in exclusion list.",
                                    _activatedBy, _inventoryId, _businessUnitId, campaign.CampaignId);
                            break;
                        }
					    
                        Log.WarnFormat("{0} triggered task found vehicle (inv id: {1}) in BU {2} that belongs in a campaign (ID: {3}) but currently isn't.  Adding it to the campaign.",
					                   _activatedBy, _inventoryId, _businessUnitId, campaign.CampaignId);
					}

                    _campaignManager.AddInventoryToCampaign(campaign, new InventoryDiscountPricingModel
                    {
                        ActivatedBy = _activatedBy,
                        CampaignId = campaign.CampaignId,
                        InventoryId = _inventoryId
                    });
                }
                // Stop at the first found campaign for this inventory.
                break;
            }

            // A vehicle got sent back through the system
            // If we don't find a valid discount campaign for it, make sure it has none applied.
            if (!foundCampaign)
            {
                // Now search to see if this vehicle belongs in a campaign
                var campaign = _campaignManager.FetchCampaignForInventory(_businessUnitId, _inventoryId);
                if (campaign == null) return;

                // If we found a campaign for this inventory, something went wrong.  Log it and remove the inventory from the campaign.
                Log.WarnFormat("Found vehicle in campaign (ID: {0}) that it shouldn't be in. Expiring all campaigns for inventory {1}, BU {2}", campaign.CampaignId, _inventoryId, _businessUnitId);
                _campaignManager.ExpireAllCampaignsForInventory(_inventoryId, _activatedBy, true);
            }
        }
    }
}
