﻿using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using MAX.Entities.Reports.Dashboard;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GoogleAnalyticsWebTrafficReport
    {
        #region Properties
        private readonly int _businessUnitId;

        private DateTime _beginDate = DateTime.MinValue;
        private DateTime _endDate = DateTime.MaxValue;
        private Boolean _fetchMonth = false;

        public WebTraffic WebTrafficDto
        {
            get { return _webTrafficDto ?? (_webTrafficDto = new WebTraffic(_businessUnitId)); }
        }
        private WebTraffic _webTrafficDto;

        public Boolean HasData { get; private set; }
        #endregion

        #region Methods
        #region Construction
        public GoogleAnalyticsWebTrafficReport(int businessUnitId)
        {
            _businessUnitId = businessUnitId;
        }

        public GoogleAnalyticsWebTrafficReport(int businessUnitId, DateTime beginDate, DateTime endDate)
            : this(businessUnitId)
        {
            _beginDate = beginDate;
            _endDate = endDate;
        }
        #endregion

        public Boolean Fetch()
        {
            var beginDate = DateTime.UtcNow.ToString("yyyy-MM");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM");

            if (_beginDate != DateTime.MinValue)
                beginDate = _beginDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");
            if (_endDate != DateTime.MaxValue)
                endDate = _endDate.ToString(_fetchMonth ? "yyyy-MM" : "yyyy-MM-dd");

            var getObjectReport = new GetGoogleAnalyticsWebTrafficReport(_businessUnitId, beginDate, endDate);
            AbstractCommand.DoRun(getObjectReport);

            HasData = getObjectReport.HasData;
            WebTrafficDto.Clone(getObjectReport.WebTrafficDto);

            return HasData;
        }

        public Boolean Fetch(DateTime beginDate, DateTime endDate)
        {
            _beginDate = beginDate;
            _endDate = endDate;

            return Fetch();
        }

        public Boolean FetchMonths(DateTime beginDate, DateTime endDate)
        {
            _fetchMonth = true;

            return Fetch(beginDate, endDate);
        }
        #endregion
    }

    public class GetGoogleAnalyticsWebTrafficReport : AbstractCommand
    {
        #region Properties
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly int _businessUnitId;

        private readonly String _beginDate;
        private readonly String _endDate;

        private ISimpleDbFactory _simpleDbFactory
        {
            get { return Registry.Resolve<ISimpleDbFactory>(); }
        }

        public Boolean HasData { get; private set; }
        public WebTraffic WebTrafficDto
        {
            get { return _webTrafficDto ?? (_webTrafficDto = new WebTraffic(_businessUnitId)); }
        }
        private WebTraffic _webTrafficDto;
        #endregion

        #region Methods
        #region Construction
        public GetGoogleAnalyticsWebTrafficReport(int businessUnitId)
        {
            _businessUnitId = businessUnitId;

            _beginDate = String.Empty;
            _endDate = String.Empty;
        }

        public GetGoogleAnalyticsWebTrafficReport(int businessUnitId, String beginDate, String endDate)
            : this(businessUnitId)
        {
            _beginDate = beginDate;
            _endDate = endDate;
        }
        #endregion

        #region Overrides
        protected override void Run()
        {
            var gaReportDb = _simpleDbFactory.GetGoogleAnalyticsReportDb();

            var query =
                String.Format("SELECT * FROM `{0}` WHERE config_business_unit_id = '{1}'",
                    gaReportDb.ContainerName(),
                    _businessUnitId);

            if (!String.IsNullOrWhiteSpace(_beginDate) && String.IsNullOrWhiteSpace(_endDate))
                query += String.Format(" AND config_start_date >= '{0}'", _beginDate);
            if (!String.IsNullOrWhiteSpace(_endDate) && String.IsNullOrWhiteSpace(_beginDate))
                query += String.Format(" AND config_end_date <= '{0}'", _endDate);
            if (!String.IsNullOrWhiteSpace(_beginDate) && !String.IsNullOrWhiteSpace(_endDate))
                query += String.Format(" AND (config_start_yearmonth between '{0}' AND '{1}')",
                    _beginDate, _endDate);

            try
            {
                var results = gaReportDb.Select(query);
                if (results.Count <= 0) return;

                HasData = true;

                var businessUnit = BusinessUnitFinder.Instance().Find(_businessUnitId);
                WebTrafficDto.DealershipName = businessUnit.Name;

                var timeOnPageSum = 0D;
                foreach (var row in results)
                {
                    // get the time on page information
                    timeOnPageSum += row.ContainsKey("web_avg_time_on_page_s")
                        ? Double.Parse(row["web_avg_time_on_page_s"])
                        : 0;


                    // get the list of keys for each row (because it could be different row to row in SimpleDB)
                    var directLeadKeys =
                        row.Keys.Where(x => x.StartsWith("web_") && x.EndsWith("_leads_direct")).ToList();
                    var indirectLeadKeys =
                        row.Keys.Where(x => x.StartsWith("web_") && x.EndsWith("_leads_indirect")).ToList();


                    // page metrics
                    WebTrafficDto.PageViews += row.ContainsKey("web_pageviews") ? Double.Parse(row["web_pageviews"]) : 0;

                    // direct leads
                    WebTrafficDto.TotalDirectLeads += row.Where(kvp => directLeadKeys.Contains(kvp.Key)).Sum(v =>
                    {
                        Double val;
                        Double.TryParse(v.Value, out val);
                        return val;
                    });
                    // indirect leads
                    WebTrafficDto.TotalActions += row.Where(kvp => indirectLeadKeys.Contains(kvp.Key)).Sum(v =>
                    {
                        Double val;
                        Double.TryParse(v.Value, out val);
                        return val;
                    });
                }

                // average the time on page info into a time span
                var timeOnPageAvg = timeOnPageSum/results.Count;
                var timeOnPageAvgSeconds = (Int32) Math.Abs(timeOnPageAvg);
                var timeonPageAvgMilliseconds = (Int32) Math.Round((timeOnPageAvg - timeOnPageAvgSeconds)*1000, 0);

                WebTrafficDto.AvgTimeOnPage = new TimeSpan(0, 0, 0, timeOnPageAvgSeconds, timeonPageAvgMilliseconds);
            }
            catch (Exception ex)
            {
                Log.Error("Failure in the GetGoogleAnalyticsWebTrafficReport.", ex);
            }
        }
        #endregion
        #endregion
    }
}
