﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using MAX.Entities;
using MAX.Entities.Extensions;
using Merchandising.Messages.BucketAlerts;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GetBucketAlerts : Common.Core.Command.AbstractCommand
    {
        private const Int32 MinimumMaxVersion = 3;

        private readonly int _businessUnitId;
        private readonly BucketAlert _bucketAlert;

        // wrap entity for legacy calls
        public virtual bool Active { get { return _bucketAlert.Active; } }
        public int BusinessUnitId { get { return _bucketAlert.BusinessUnitId; } }

        public virtual string[] Emails { get { return _bucketAlert.Emails; } }
        public virtual string[] WeeklyEmails { get { return _bucketAlert.WeeklyEmails; } }
        public virtual string[] BionicHealthReportEmails { get { return _bucketAlert.BionicHealthReportEmails; } }
        public virtual string[] GroupBionicHealthReportEmails { get { return _bucketAlert.GroupBionicHealthReportEmails; } }
        public virtual int NewUsed { get { return _bucketAlert.NewUsed; } }

        public virtual bool EmailBounced { get { return _bucketAlert.EmailBounced; } }
        public virtual bool WeeklyEmailBounced { get { return _bucketAlert.WeeklyEmailBounced; } }
        public virtual bool BionicHealthReportEmailBounced { get { return _bucketAlert.BionicHealthReportEmailBounced; } }
        public virtual bool GroupBionicHealthReportEmailBounced { get { return _bucketAlert.GroupBionicHealthReportEmailBounced; } }

        public virtual BucketAlertThreshold[] Thresholds { get { return _bucketAlert.Thresholds; } }
        // --

        public GetBucketAlerts(int businessUnitId)
        {
            _businessUnitId = businessUnitId;
            _bucketAlert = new BucketAlert(_businessUnitId);
        }

        public static BucketAlertMessage[] GetActiveBusinessUnitAlerts()
        {
            var list = new List<BucketAlertMessage>();

            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "settings.BucketAlerts#Active";

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));

                            list.Add(new BucketAlertMessage(businessUnitId));
                        }
                    }
                }
            }

            return list.ToArray();
        }

        public static BucketAlertMessage[] GetActiveBusinessUnits()
        {
            return MerchandisingBusinessUnits.Instance().FindAllMerchandisingDealershipsAboveVersion(MinimumMaxVersion)
                .Select(dlr => new BucketAlertMessage(dlr.Id))
                .ToArray();
        }

        public static int GetWeeklyEmailSendDay()
        {
            int dayOfWeek = 2; //default to Tuesday

            // -1 is off
            // 0 through 6 is Sun - Sat

            string value = ConfigurationManager.AppSettings["WeeklyEmailSendDay"];
            if (!String.IsNullOrEmpty(value))
                if (int.TryParse(value, out dayOfWeek))
                    if (dayOfWeek < -1 || dayOfWeek > 6) 
                        dayOfWeek = 0;

            return dayOfWeek;
        }

        public static void SetBounced(int businessUnitId, bool? emailBounced = null, bool? weeklyBounced = null, bool? bionicHealthReportEmailBounced = null, bool? groupBionicHealthReportEmailBounced = null)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "release.SetBucketAlertsEmailBounced";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);
                    
                    // if null is passed in these parameters, it will use whatever is existing in the DB
                    command.AddRequiredParameter("EmailBounced", emailBounced, DbType.Boolean);
                    command.AddRequiredParameter("WeeklyEmailBounced", weeklyBounced, DbType.Boolean);
                    command.AddRequiredParameter("BionicHealthReportEmailBounced", bionicHealthReportEmailBounced, DbType.Boolean);
                    command.AddRequiredParameter("GroupBionicHealthReportEmailBounced", groupBionicHealthReportEmailBounced, DbType.Boolean);

                    command.ExecuteNonQuery();
                }
            }
        }

        protected override void Run()
        {
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "settings.BucketAlerts#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("businessUnitID", _businessUnitId, DbType.Int32);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _bucketAlert.Active = reader.GetBoolean(reader.GetOrdinal("Active"));
                            var emails = reader.GetString(reader.GetOrdinal("Email"));
                            var weeklyEmails = Database.GetNullableString(reader,"WeeklyEmail");
                            var bionicHealthReportEmails = Database.GetNullableString(reader, "BionicHealthReportEmail");
                            var groupBionicHealthReportEmails = Database.GetNullableString(reader, "GroupBionicHealthReportEmail");
                            _bucketAlert.Emails = emails.Split(',');
                            _bucketAlert.WeeklyEmails = weeklyEmails.Split(',');
                            _bucketAlert.BionicHealthReportEmails = bionicHealthReportEmails.Split(',');
                            _bucketAlert.GroupBionicHealthReportEmails = groupBionicHealthReportEmails.Split(',');
                            _bucketAlert.NewUsed = reader.GetInt32(reader.GetOrdinal("NewUsed"));
                            _bucketAlert.EmailBounced = reader.GetBoolean(reader.GetOrdinal("EmailBounced"));
                            _bucketAlert.WeeklyEmailBounced = reader.GetBoolean(reader.GetOrdinal("WeeklyEmailBounced"));
                            _bucketAlert.BionicHealthReportEmailBounced = reader.GetBoolean(reader.GetOrdinal("BionicHealthReportEmailBounced"));
                            _bucketAlert.GroupBionicHealthReportEmailBounced = reader.GetBoolean(reader.GetOrdinal("GroupBionicHealthReportEmailBounced"));
                        }
                    }
                }

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "settings.BucketAlertThreshold#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("businessUnitID", _businessUnitId, DbType.Int32);

                    var list = new List<BucketAlertThreshold>();
                    using (var reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            var id = reader.GetInt32(reader.GetOrdinal("Id"));
                            var businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));
                            var bucket = (WorkflowType)reader.GetInt32(reader.GetOrdinal("Bucket"));
                            var active = reader.GetBoolean(reader.GetOrdinal("Active"));
                            var limit = reader.GetInt32(reader.GetOrdinal("Limit"));
                            var emailActive = reader.GetBoolean(reader.GetOrdinal("EmailActive")); // FB: 27993 - separate email for alerts

                            list.Add(new BucketAlertThreshold() { Active = active, Bucket = bucket, BusinessUnitId = businessUnitId, Id = id, Limit = limit, EmailActive = emailActive });
                        }
                    }

                    _bucketAlert.Thresholds = list.ToArray();
                }
            }
        }

        public IList<BucketAlert> GetAllBucketAlertSettings()
        {
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "settings.BucketAlerts#FetchAll";
                command.CommandType = CommandType.StoredProcedure;

                using (var reader = command.ExecuteReader())
                {
                    return (
                        from dr in reader.Records()
                        select new BucketAlert(dr.GetInt32("BusinessUnitId"))
                        {
                            Active = dr.GetBoolean("Active"),
                            Emails = Database.GetNullableString(dr, "Email").Replace(" ", "").Split(','),
                            NewUsed = dr.GetInt32("NewUsed"),
                            EmailBounced = dr.GetBoolean("EmailBounced"),
                            WeeklyEmails = Database.GetNullableString(dr, "WeeklyEmail").Replace(" ", "").Split(','),
                            WeeklyEmailBounced = dr.GetBoolean("WeeklyEmailBounced"),
                            BionicHealthReportEmails = Database.GetNullableString(dr, "BionicHealthReportEmail").Replace(" ", "").Split(','),
                            BionicHealthReportEmailBounced = dr.GetBoolean("BionicHealthReportEmailBounced"),
                            GroupBionicHealthReportEmails = Database.GetNullableString(dr, "GroupBionicHealthReportEmail").Replace(" ", "").Split(','),
                            GroupBionicHealthReportEmailBounced = dr.GetBoolean("GroupBionicHealthReportEmailBounced")
                        }).ToList();
                }
            }
        }
    }
}
