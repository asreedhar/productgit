using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class SetReportSettings : Common.Core.Command.AbstractCommand
    {
        private readonly int _businessUnitId;
        private readonly Single _lowActivityThreshold;
        public readonly Single _highActivityThreshold;
        public readonly int _minSearchCountThreshold;
        public readonly int _vehicleOnlineStatusReportMinAgeDays;
        public readonly int _adCompleteFlags;
        public readonly int _merchandisingGraphDefault;
        private readonly int _timeToMarketGoalDays;

        private const int MaxMinSearchCountThreshold = 100000;

        public List<string> ValidationErrors { get; private set; }

        public SetReportSettings(int businessUnitId, Single lowActivityThreshold, Single highActivityThreshold, int minSearchCountThreshold, int vehicleOnlineStatusReportMinAgeDays, int adCompleteFlags, int merchandisingGraphDefault, int timeToMarketGoalDays)
        {
            _businessUnitId = businessUnitId;
            _lowActivityThreshold = lowActivityThreshold;
            _highActivityThreshold = highActivityThreshold;
            _minSearchCountThreshold = minSearchCountThreshold;
            _vehicleOnlineStatusReportMinAgeDays = vehicleOnlineStatusReportMinAgeDays;
            _adCompleteFlags = adCompleteFlags;
            _merchandisingGraphDefault = merchandisingGraphDefault;
            _timeToMarketGoalDays = timeToMarketGoalDays;
        }

        private bool IsValid()
        {
            ValidationErrors = new List<string>();

            if (_lowActivityThreshold < 0 || _lowActivityThreshold > 1) ValidationErrors.Add("Low Activity Threshold is out of range.");
            if (_highActivityThreshold < 0 || _highActivityThreshold > 1) ValidationErrors.Add("High Activity Threshold is out of range.");
            if (_minSearchCountThreshold < 0 || _minSearchCountThreshold > MaxMinSearchCountThreshold) ValidationErrors.Add("Min search count threshold should to be between 1 and 100000");
            if (_vehicleOnlineStatusReportMinAgeDays < 0) ValidationErrors.Add("Vehicle Online Status Report parameter 'Minimum Age' must be greater than zero");
            if (_adCompleteFlags <= 0) ValidationErrors.Add("Time to Market Ad Complete: You must select at least one Ad Complete metric");

            return ValidationErrors.Count == 0;
        }

        protected override void Run()
        {
            if (!IsValid()) throw new Exception(string.Join(",", ValidationErrors));
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.Transaction = transaction;
                        command.CommandText = "settings.ReportSettings#Update";
                        command.CommandType = CommandType.StoredProcedure;
                        command.AddRequiredParameter("BusinessUnitId", _businessUnitId, DbType.Int32);
                        command.AddRequiredParameter("lowActivityThreshold", _lowActivityThreshold, DbType.Single);
                        command.AddRequiredParameter("highActivityThreshold", _highActivityThreshold, DbType.Single);
                        command.AddRequiredParameter("minSearchCountThreshold", _minSearchCountThreshold, DbType.Single);
                        command.AddRequiredParameter("vehicleOnlineStatusReportMinAgeDays", _vehicleOnlineStatusReportMinAgeDays, DbType.Single);
                        command.AddRequiredParameter("adCompleteFlags", _adCompleteFlags, DbType.Int32);
                        command.AddRequiredParameter("MerchandisingGraphDefault", _merchandisingGraphDefault, DbType.Byte);
                        command.AddRequiredParameter("TimeToMarketGoalDays", _timeToMarketGoalDays, DbType.Int32);

                        command.ExecuteNonQuery();
                    }

                    GetReportSettings.InvalidateCache(_businessUnitId);

                    transaction.Commit();
                }
            }
        }
    }
}