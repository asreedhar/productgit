﻿using System.Data;
using FirstLook.Common.Core.Command;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class RemoveSpecialPrice : AbstractCommand
    {
        private readonly int _inventoryId;
        private readonly decimal _specialPrice;

        public RemoveSpecialPrice(int inventoryId, decimal specialPrice)
        {
            _inventoryId = inventoryId;
            _specialPrice = specialPrice;
        }

        protected override void Run()
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "builder.NewCarPricing#Delete";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("InventoryID", _inventoryId, DbType.Int32);
                    command.AddRequiredParameter("SpecialPrice", _specialPrice, DbType.Decimal);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}