﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using Merchandising.Messages.AutoApprove;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class AutoApproveActivity : AbstractCommand
    {
        public DailyEmailMessage[] Messages { get; private set; }

        protected override void Run()
        {
            var messages = new List<DailyEmailMessage>();

            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "release.AutoApproveActivity#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var businessUnit = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            var email = reader.GetString(reader.GetOrdinal("Email"));
                            var bounced = reader.GetBoolean(reader.GetOrdinal("EmailBounced"));

                            if(!bounced)
                                messages.Add(new DailyEmailMessage(businessUnit, email));
                        }
                    }
                }
            }

            Messages = messages.ToArray();
        }


        public static void SetBounced(int businessUnitId, bool bounced)
        {
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "release.SetAutoApproveEmailBounced";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(command, "Bounced", bounced, DbType.Boolean);

                    command.ExecuteNonQuery();
                }
            }
            
        }
    }
}
