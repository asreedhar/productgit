using System.Data;
using FirstLook.DomainModel.Oltp;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class SetUpgradeSettings : Common.Core.Command.AbstractCommand
    {
        private readonly int _businessUnitId;
        private readonly bool _maxForSmartphone;
        private readonly int? _autoWindowStickerTemplateId;
        private readonly bool _maxForWebsite20;
	    private readonly bool _maxDigitalShowroom;
        private readonly bool _usePhoneMapping;
        private readonly bool _useDealerRater;
        private readonly bool _enableBookValuations;
        private readonly bool _enableShowroomReports;
        private readonly VehicleType _vehiclesInShowroom;

        public SetUpgradeSettings(
            int businessUnitId,
            bool maxForSmartphone,
            int? autoWindowStickerTemplateId,
            bool maxForWebsite20 = false,
			bool maxDigitalShowroom = false,
            bool usePhoneMapping = false,
            bool useDealerRater = false,
            bool enableBookValuations = false,
            VehicleType vehiclesInShowroom = null,
            bool enableShowroomReports = false)
        {
            _businessUnitId = businessUnitId;
            _maxForSmartphone = maxForSmartphone;
            _autoWindowStickerTemplateId = autoWindowStickerTemplateId;
            _maxForWebsite20 = maxForWebsite20;
	        _maxDigitalShowroom = maxDigitalShowroom;
            _usePhoneMapping = usePhoneMapping;
            _useDealerRater = useDealerRater;
            _enableBookValuations = enableBookValuations;
            _vehiclesInShowroom = vehiclesInShowroom;
            _enableShowroomReports = enableShowroomReports;
        }

        protected override void Run()
        {
            using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                command.CommandText = "settings.UpgradeSettings#Update";
                command.CommandType = CommandType.StoredProcedure;
             
                command.AddRequiredParameter("BusinessUnitId", _businessUnitId, DbType.Int32);
                command.AddRequiredParameter("maxForSmartphone", _maxForSmartphone, DbType.Boolean);
                if(_autoWindowStickerTemplateId != null)
                    command.AddRequiredParameter("autoWindowStickerTemplateId", _autoWindowStickerTemplateId, DbType.Int32);
                command.AddRequiredParameter("MAXForWebsite20", _maxForWebsite20, DbType.Boolean);
				command.AddRequiredParameter("MaxDigitalShowroom", _maxDigitalShowroom, DbType.Boolean);
                command.AddRequiredParameter("UsePhoneMapping", _usePhoneMapping, DbType.Boolean);
                command.AddRequiredParameter("UseDealerRater", _useDealerRater, DbType.Boolean);
                command.AddRequiredParameter("EnableShowroomBookValuations", _enableBookValuations, DbType.Boolean);
                command.AddRequiredParameter("VehiclesInShowroom", _vehiclesInShowroom.IntValue, DbType.Int16);
                command.AddRequiredParameter("EnableShowroomReports", _enableShowroomReports, DbType.Boolean);

                command.ExecuteNonQuery();

                GetUpgradeSettings.InvalidateCache(_businessUnitId);
                // invalidate group cache
                GetUpgradeSettings.InvalidateCache(BusinessUnitFinder.Instance().FindDealerGroupByDealership(_businessUnitId).Id);
            }

        }
    }
}