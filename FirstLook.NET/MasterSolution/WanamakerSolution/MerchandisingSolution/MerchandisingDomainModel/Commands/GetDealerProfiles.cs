using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Command;

namespace FirstLook.Merchandising.DomainModel.Commands
{
	public class GetDealerProfiles : AbstractCommand
	{
		protected override void Run()
		{
			ChangedDealerProfiles = new List<DealerProfile>();
			AllDealerProfiles = new List<DealerProfile>();
			DeleteOwnerHandleList = new List<string>();

			using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
			using (var command = connection.CreateCommand())
			{
				command.CommandText = "settings.GetDealerProfilePublishingUpdates";
				command.CommandType = CommandType.StoredProcedure;
				using (var reader = command.ExecuteReader())
				{
					while (reader.Read())
					{	// changed dealer profiles are first
						ChangedDealerProfiles.Add(new DealerProfile(reader));
					}

					reader.NextResult();

					while (reader.Read())
					{	// deleted dealers are next
						DeleteOwnerHandleList.Add(reader.GetGuid(reader.GetOrdinal("DeleteOwnerHandle")).ToString());
					}
					
					reader.NextResult();

					while (reader.Read())
					{	// all dealer profiles are last
						AllDealerProfiles.Add(new DealerProfile(reader));
					}

				}
			}
		}

		public List<DealerProfile> ChangedDealerProfiles { get; private set; }
		public List<DealerProfile> AllDealerProfiles { get; private set; }
		public List<string> DeleteOwnerHandleList { get; private set; }
	}

	public class DealerProfile
	{
		public DealerProfile(IDataRecord record)
		{
			BusinessUnitID = record.GetInt32(record.GetOrdinal("BusinessUnitID"));
			BusinessUnitName = record.GetString(record.GetOrdinal("BusinessUnitName"));
			BusinessUnitCode = record.GetString(record.GetOrdinal("BusinessUnitCode"));
			OwnerHandle = record.GetGuid(record.GetOrdinal("OwnerHandle")).ToString().ToUpperInvariant();
			GroupId = record.GetInt32(record.GetOrdinal("GroupId"));
			Address = record.GetString(record.GetOrdinal("Address"));
			City = record.GetString(record.GetOrdinal("City"));
			Description = record.GetString(record.GetOrdinal("Description"));
			Email = record.GetString(record.GetOrdinal("Email"));
			Phone = record.GetString(record.GetOrdinal("Phone"));
			Url = record.GetString(record.GetOrdinal("Url"));
		    LogoUrl = record.GetString(record.GetOrdinal("LogoUrl"));
			State = record.GetString(record.GetOrdinal("State"));
			ZipCode = record.GetString(record.GetOrdinal("ZipCode"));
			HasMaxForWebsite1 = record.GetBoolean(record.GetOrdinal("HasMaxForWebsite1"));
			HasMaxForWebsite2 = record.GetBoolean(record.GetOrdinal("HasMaxForWebsite2"));
			HasMobileShowroom = record.GetBoolean(record.GetOrdinal("HasMobileShowroom"));
			HasShowroom = record.GetBoolean(record.GetOrdinal("HasShowroom"));
		    HasShowroomBookValuations = record.GetBoolean(record.GetOrdinal("HasShowroomBookValuations"));
		    HasNewVehiclesEnabled = record.GetBoolean(record.GetOrdinal("HasNewVehiclesEnabled"));
            HasUsedVehiclesEnabled = record.GetBoolean(record.GetOrdinal("HasUsedVehiclesEnabled"));
            HasProfitMAX = record.GetBoolean(record.GetOrdinal("HasProfitMAX"));
		}

	    public int BusinessUnitID { get; set; }
		public string BusinessUnitName { get; set; }
		public string BusinessUnitCode { get; set; }
		public string OwnerHandle { get; set; }
		public int GroupId { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string Description { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Url { get; set; }
        public string LogoUrl { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public bool HasMaxForWebsite1 { get; set; }
		public bool HasMaxForWebsite2 { get; set; }
		public bool HasMobileShowroom { get; set; }
		public bool HasShowroom { get; set; }
        public bool HasShowroomBookValuations { get; set; }
        public bool HasNewVehiclesEnabled { get; set; }
        public bool HasUsedVehiclesEnabled { get; set; }
        public bool HasProfitMAX { get; set; }

	}

}