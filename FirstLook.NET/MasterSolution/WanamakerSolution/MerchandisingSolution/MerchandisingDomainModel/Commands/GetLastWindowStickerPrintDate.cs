﻿using System;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GetLastWindowStickerPrintDate : AbstractCommand
    {
        private readonly int _inventoryId;
        public GetLastWindowStickerPrintDate(int inventoryId)
        {
            _inventoryId = inventoryId;
        }

        public DateTime LastPrintDate { get; private set; }

        protected override void Run()
        {
            using (IDataConnection connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "WindowSticker.LastPrintDate#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(command, "InventoryId", _inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(command, "TemplateTypeId", 1, DbType.Byte);
                    Database.AddRequiredParameter(command, "LatestPrintDate", null, DbType.DateTime);
                    var lastPrintDateOutput = ((IDataParameter) command.Parameters["LatestPrintDate"]);
                    lastPrintDateOutput.Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    if (lastPrintDateOutput.Value != DBNull.Value)
                        LastPrintDate = (DateTime) lastPrintDateOutput.Value;
                }
            }
        }
    }
}
