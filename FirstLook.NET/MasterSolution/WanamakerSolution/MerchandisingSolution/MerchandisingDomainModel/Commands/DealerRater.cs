﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Pricing;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    [Serializable]
    public class DealerRater
    {
        #region Properties
        public int BusinessUnitId { get; private set; }
        public Boolean HasSettings { get; private set; }
        public String DealerRaterId { get; private set; }

        public String OwnerHandle
        {
            get
            {
                return PricingData.GetOwnerHandle(BusinessUnitId);
            }
        }
        #endregion

        #region Methods
        #region Construction

        public DealerRater(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;

            HasSettings = false;
            DealerRaterId = String.Empty;
        }
        #endregion

        public void Fetch()
        {
            var simpleDbFactory = new SimpleDbFactory();
            var dealerRaterDb = simpleDbFactory.GetDealerRaterDb();

            var results = dealerRaterDb.Select(new Dictionary<string, string> { { "itemName", OwnerHandle } });
            if (results.Count <= 0) return;

            HasSettings = true;
            DealerRaterId = results[0]["dealer_rater_id"];
        }

        public void Upsert(String dealerRaterId)
        {
            DealerRaterId = dealerRaterId;

            var simpleDbFactory = new SimpleDbFactory();
            var dealerRaterDb = simpleDbFactory.GetDealerRaterDb();

            var attributes = new Dictionary<String, String>();
            attributes["dealer_rater_id"] = DealerRaterId;

            dealerRaterDb.Write(OwnerHandle, attributes);
            HasSettings = true;
        }
        #endregion
    }
}
