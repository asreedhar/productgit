﻿using System;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.DTO;
using FirstLook.Merchandising.DomainModel.Marketing;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class GetOfflineStatus : CachedCommand, ISurrogateKey
    {
        private int BusinessUnitId { get; set; }
        private string Vin { get; set; }

        private bool? _isOffline;

        public bool IsOffline
        {
            get
            {
                if (_isOffline.HasValue)
                {
                    return _isOffline.Value;
                }

                throw new ApplicationException("The command was not ran before an attempt was made to access the value.");
            }

            set { _isOffline = value; }
        }

        public GetOfflineStatus(int businessUnitId, string vin)
        {
            BusinessUnitId = businessUnitId;
            Vin = vin;
        }

        protected override void Run()
        {

            // Check cache for existing data.
            var data = Cache.Get(Key);
            if (data != null)
            {
                // return from cache
                _isOffline = Convert.ToBoolean(data);
                return;
            }

            // hit the db
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "builder.GetVehicleOfflineStatus";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddRequiredParameter("BusinessUnitId", BusinessUnitId, DbType.Int32);
                    command.AddRequiredParameter("Vin", Vin, DbType.String);
                    command.AddRequiredParameter("IsOffline", null, DbType.Boolean );
                    var isOfflineParam = ((IDataParameter)command.Parameters["IsOffline"]);
                    isOfflineParam.Direction = ParameterDirection.Output;

                    command.ExecuteNonQuery();

                    if (isOfflineParam.Value != DBNull.Value)
                        _isOffline = (bool) isOfflineParam.Value;
                    else
                        _isOffline = false;
                }
                
                // cache it
                Cache.Set(Key, _isOffline, DefaultCacheExpirationSeconds);
            }
        }

        public string Key
        {
            get { return string.Format("{1}{0}{2}{0}{3}", CommonKeys.DELIM, GetType(), BusinessUnitId, Vin); }
        }
    }
}