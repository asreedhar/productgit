﻿using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    /// <summary>
    /// Enables/disables photo transfers for a given business unit. Can pass in either dealer or dealer-group business unit ID.
    /// </summary>
    public class SetPhotoTransfersEnabled : Common.Core.Command.AbstractCommand
    {
        public int BusinessUnitId { get; private set; }
        public bool PreferenceValue { get; private set; }

        public SetPhotoTransfersEnabled(int businessUnitId, bool value)
        {
            BusinessUnitId = businessUnitId;
            PreferenceValue = value;
        }

        protected override void Run()
        {
            using (IDataConnection connection = Database.GetConnection(Database.IMTDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "PhotoTransfers#SetGroupParticipation";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(command, "BusinessUnitId", BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(command, "enabled", PreferenceValue ? 1 : 0, DbType.Int32);
                                        
                    command.ExecuteNonQuery();
                                        
                }
            }

        }
    }
}
