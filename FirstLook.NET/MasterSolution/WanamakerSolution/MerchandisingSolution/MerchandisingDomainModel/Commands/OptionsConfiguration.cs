﻿using System.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class OptionsConfiguration
    {
        public static void InsertMissingOptionsConfigurationRecords(int businessUnitId, int inventoryId, string memberLogin = null)
        {
            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "builder.InsertMissingOptionsConfigurationRecords";

                cmd.AddRequiredParameter("@BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("@InventoryID", inventoryId, DbType.Int32);
                
                if(memberLogin != null)
                    cmd.AddRequiredParameter("@MemberLogin", memberLogin, DbType.AnsiString);

                cmd.ExecuteNonQuery();
            }
        }
        
        public static void UpdateOptionsConfigurationChromeStyleIdField(int businessUnitId, int inventoryId, string memberLogin = null)
        {
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "workflow.UpdateOptionsConfigurationChromeStyleIdField";

                cmd.AddRequiredParameter("@BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("@InventoryID", inventoryId, DbType.Int32);

                if (memberLogin != null)
                    cmd.AddRequiredParameter("@MemberLogin", memberLogin, DbType.AnsiString);

                cmd.ExecuteNonQuery();
            }
            
        }
    }
}