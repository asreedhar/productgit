﻿using System.Data;
using FirstLook.Common.Data;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class SetAutoApproveSettings : Common.Core.Command.AbstractCommand
    {
        private readonly int _businessUnitId;

        private readonly string _firstName;
        private readonly string _lastName;
        private readonly string _email;
        private readonly AutoApproveStatus _status;
        private readonly string _userName;
        private bool _emailBounced;

        public SetAutoApproveSettings(int businessUnitId, string firstName, string lastName, string email, AutoApproveStatus status, string userName, bool emailBounced)
        {
            _businessUnitId = businessUnitId;
            _firstName = firstName;
            _lastName = lastName;
            _email = email;
            _status = status;
            _userName = userName;
        }

        protected override void Run ()
        {
            using ( IDataConnection connection = Database.GetConnection( Database.MerchandisingDatabase ) )
            {
                connection.Open();

                using ( IDbTransaction transaction = connection.BeginTransaction() )
                {
                    using ( IDbCommand command = connection.CreateCommand() )
                    {
                        command.Transaction = transaction;
                        command.CommandText = "settings.AutoApprove#Upsert";
                        command.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter( command, "BusinessUnitId", _businessUnitId, DbType.Int32 );
                        Database.AddRequiredParameter( command, "FirstName", _firstName, DbType.String );
                        Database.AddRequiredParameter( command, "LastName", _lastName, DbType.String );
                        Database.AddRequiredParameter( command, "Email", _email, DbType.String );
                        Database.AddRequiredParameter( command, "Status", _status, DbType.Int16 );
                        Database.AddRequiredParameter( command, "UserName", _userName, DbType.String );
                        Database.AddRequiredParameter(command, "Bounced", _emailBounced, DbType.Boolean);

                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }
        }

    }
}
