using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core.Command;

namespace FirstLook.Merchandising.DomainModel.Commands
{
	public class SaveDealerProfilesAsPublished : AbstractCommand
	{
		private readonly List<DealerProfile> _dealerProfiles;


		public SaveDealerProfilesAsPublished(List<DealerProfile> dealerProfiles)
		{
			_dealerProfiles = dealerProfiles;
		}

		protected override void Run()
		{
			var dealerProfileTable = GetDealerProfilesDataTable(_dealerProfiles);

			using (var connection = Database.GetOpenConnection(Database.MerchandisingDatabase))
			using (var command = connection.CreateCommand())
			{
				command.CommandText = "settings.SavePublishedDealerProfiles";
				command.CommandType = CommandType.StoredProcedure;
				var param = new SqlParameter("DealerProfilesTable", dealerProfileTable) {SqlDbType = SqlDbType.Structured};
				command.Parameters.Add(param);

				command.ExecuteNonQuery();
			}
		}
		
		
		private static DataTable GetDealerProfilesDataTable(IEnumerable<DealerProfile> dealerProfiles)
		{
			var dt = new DataTable();
			dt.Columns.Add("BusinessUnitID", typeof (int));
			dt.Columns.Add("BusinessUnitName", typeof (string));
			dt.Columns.Add("BusinessUnitCode", typeof (string));
			dt.Columns.Add("OwnerHandle", typeof (Guid));
			dt.Columns.Add("GroupId", typeof (int));
			dt.Columns.Add("Address", typeof (string));
			dt.Columns.Add("City", typeof (string));
			dt.Columns.Add("Description", typeof (string));
			dt.Columns.Add("Email", typeof (string));
			dt.Columns.Add("Phone", typeof (string));
			dt.Columns.Add("Url", typeof (string));
            dt.Columns.Add("LogoUrl", typeof(string));
			dt.Columns.Add("State", typeof(string));
			dt.Columns.Add("ZipCode", typeof (string));
			dt.Columns.Add("HasMaxForWebsite1", typeof (bool));
			dt.Columns.Add("HasMaxForWebsite2", typeof (bool));
			dt.Columns.Add("HasMobileShowroom", typeof (bool));
			dt.Columns.Add("HasShowroom", typeof (bool));
		    dt.Columns.Add("HasShowroomBookValuations", typeof (bool));
		    dt.Columns.Add("HasNewVehiclesEnabled", typeof (bool));
            dt.Columns.Add("HasUsedVehiclesEnabled", typeof(bool));
            dt.Columns.Add("HasProfitMAX", typeof(bool));


			foreach (var dp in dealerProfiles)
			{
				var row = dt.NewRow();
				row["BusinessUnitID"] = dp.BusinessUnitID;
				row["BusinessUnitName"] = dp.BusinessUnitName;
				row["BusinessUnitCode"] = dp.BusinessUnitCode;
				row["OwnerHandle"] = dp.OwnerHandle;
				row["GroupId"] = dp.GroupId;
				row["Address"] = dp.Address;
				row["City"] = dp.City;
				row["Description"] = dp.Description;
				row["Email"] = dp.Email;
				row["Phone"] = dp.Phone;
				row["Url"] = dp.Url;
			    row["LogoUrl"] = dp.LogoUrl;
				row["State"] = dp.State;
				row["ZipCode"] = dp.ZipCode;
				row["HasMaxForWebsite1"] = dp.HasMaxForWebsite1;
				row["HasMaxForWebsite2"] = dp.HasMaxForWebsite2;
				row["HasMobileShowroom"] = dp.HasMobileShowroom;
				row["HasShowroom"] = dp.HasShowroom;
			    row["HasShowroomBookValuations"] = dp.HasShowroomBookValuations;
			    row["HasNewVehiclesEnabled"] = dp.HasNewVehiclesEnabled;
			    row["HasUsedVehiclesEnabled"] = dp.HasUsedVehiclesEnabled;
                row["HasProfitMAX"] = dp.HasProfitMAX;
				dt.Rows.Add(row);
			}

			return dt;
		}
	}
}