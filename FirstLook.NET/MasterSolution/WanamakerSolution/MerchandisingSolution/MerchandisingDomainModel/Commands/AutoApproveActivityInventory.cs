﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    internal class AutoApproveActivityInventory : AbstractCommand
    {
        private int _businessUnitID;

        public AutoApproveActivityInventory(int businessUnitID)
        {
            _businessUnitID = businessUnitID;
        }

        public int[] InventoryIDs { get; private set; }

        protected override void Run()
        {
            var inventoryIds = new List<int>();

            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Release.AutoApproveInventory#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(command, "businessUnitID", _businessUnitID, DbType.Int32);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            inventoryIds.Add(reader.GetInt32(reader.GetOrdinal("inventoryID")));
                    }
                }
            }

            InventoryIDs = inventoryIds.ToArray();
        }
    }
}
