﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Commands
{
    public class SetSpecialPrice : AbstractCommand
    {
        private int _inventoryID;
        private decimal _specialPrice;
        private string _userName;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inventoryId">The inventoryId to set a special price on.</param>
        /// <param name="specialPrice">The special price to set on the inventory.</param>
        /// <param name="userName">The user making the special price setting. Used for history audits.</param>

        public SetSpecialPrice(int inventoryId, decimal specialPrice, string userName)
        {
            _inventoryID = inventoryId;
            _specialPrice = specialPrice;
            _userName = userName;
        }

        protected override void Run()
        {
            using (IDataConnection connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "builder.NewCarPricing#Upsert";
                    command.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(command, "InventoryID", _inventoryID, DbType.Int32);
                    Database.AddRequiredParameter(command, "SpecialPrice", _specialPrice, DbType.Decimal);
                    Database.AddRequiredParameter(command, "UserName", _userName, DbType.AnsiString);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
