﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Admin
{
    public interface IAutoLoadAuditRepository
    {
        AutoLoadBusinessAudit[] FetchAudit();
        AutoLoadDetails[] FetchBusinessUnitDetails(int businesssUnitId);
        string[] GetWhiteList(int manufactureId, int divisionId);
    }
}
