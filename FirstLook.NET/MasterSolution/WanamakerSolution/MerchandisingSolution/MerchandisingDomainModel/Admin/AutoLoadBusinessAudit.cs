﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Admin
{
    public class AutoLoadBusinessAudit
    {
        public AutoLoadBusinessAudit()
        {
            StatusCounts = new Dictionary<string, int>();
        }

        public int BusinessUnitId { get; set; }
        public string BusinessUnit { get; set; }

        public Dictionary<string, int> StatusCounts { get; set; }

        public string PercentSuccess
        {
            get
            {
                double total = StatusCounts.Values.Sum();
                double successs = StatusCounts.ContainsKey("Success") ? StatusCounts["Success"] : 0;
                double percentage = (successs / total) * 100;
                return String.Format("{0:f0}", percentage);
            }
        }


    }
}
