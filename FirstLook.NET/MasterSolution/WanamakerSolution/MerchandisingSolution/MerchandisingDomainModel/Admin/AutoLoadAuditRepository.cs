﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Admin
{
    public class AutoLoadAuditRepository : IAutoLoadAuditRepository
    {

        public string[] GetWhiteList(int manufactureId, int divisionId)
        {
            var list = new List<string>();
            using (IDbConnection con = Common.Core.Data.Database.Connection(Database.MerchandisingDatabase))
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                            SELECT OptionCode 
                                FROM [dbo].[ManufacturerVehicleOptionCodeWhiteList] 
                                WHERE VehicleManufacturerID = @ManufacturerID 
                                AND VehicleMakeID = @MakeID
                                ORDER BY OptionCode ";

                    cmd.CommandType = CommandType.Text;
                    cmd.AddParameterWithValue("@ManufacturerID", manufactureId);
                    cmd.AddParameterWithValue("@MakeID", divisionId);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string code = reader.GetString(0);
                            list.Add(code);
                        }
                    }
                }
                con.Close();
            }

            return list.ToArray();
        }

        public AutoLoadBusinessAudit[] FetchAudit()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();             
                using (IDbCommand cmd = con.CreateCommand())
                {
                    //change to stored proc.
                    cmd.CommandText = "builder.AutoLoadAudit#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    var businessAudits = new List<AutoLoadBusinessAudit>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        int lastBusinessUnitId = -1;
                        AutoLoadBusinessAudit businessAudit = null;
                        while (reader.Read())
                        {
                            var businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            var businessUnit = reader.GetString(reader.GetOrdinal("BusinessUnit"));

                            if (lastBusinessUnitId != businessUnitId)
                            {
                                if (businessAudit != null)
                                    businessAudits.Add(businessAudit);

                                businessAudit = new AutoLoadBusinessAudit{ BusinessUnitId = businessUnitId, BusinessUnit = businessUnit};
                            }

                            var statusType = reader.GetString(reader.GetOrdinal("StatusType")).HumanizeString();
                            var statusCount = reader.GetInt32(reader.GetOrdinal("StatusCount"));
                            businessAudit.StatusCounts.Add(statusType, statusCount);

                            lastBusinessUnitId = businessUnitId;
                        }

                        if (businessAudit != null)
                            businessAudits.Add(businessAudit);
                    }

                    return businessAudits.ToArray();
                }
            }
        }

        public AutoLoadDetails[] FetchBusinessUnitDetails(int businesssUnitId)
        {
            var details = new List<AutoLoadDetails>();
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    //change to stored proc.
                    cmd.CommandText = "builder.AutoLoadAuditDetails#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("BusinessUnitID", businesssUnitId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var detail = new AutoLoadDetails
                                {
                                    Vin = reader.GetString(reader.GetOrdinal("Vin")),
                                    Start = reader.GetDateTime(reader.GetOrdinal("StartTime")),
                                    End = DateTime.MinValue
                                };

                            if(!reader.IsDBNull(reader.GetOrdinal("EndTime")))
                                detail.End = reader.GetDateTime(reader.GetOrdinal("EndTime"));
                            
                            detail.Status = reader.GetString(reader.GetOrdinal("StatusType"));
                            detail.Make = reader.GetString(reader.GetOrdinal("Make"));
                            detail.InventoryType = reader.GetByte(reader.GetOrdinal("InventoryType"));

                            detail.InventoryId = Convert.ToInt32(reader["InventoryID"]);
                            if (reader["ManufacturerName"] != Convert.DBNull)
                                detail.Manufacturer = reader["ManufacturerName"].ToString();

                            detail.ManufacturerId = Convert.ToInt32(reader["ManufacturerId"]);
                            detail.DivisionId = Convert.ToInt32(reader["DivisionId"]);

                            details.Add(detail);
                        }
                        
                    }
                }
            }

            return details.ToArray();
        }
    }
}
