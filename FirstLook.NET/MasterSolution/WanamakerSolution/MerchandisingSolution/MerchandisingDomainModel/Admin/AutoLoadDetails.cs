﻿using System;

namespace FirstLook.Merchandising.DomainModel.Admin
{
    public class AutoLoadDetails
    {
        public string Vin { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Status { get; set; }
        public string Make { get; set; }
        public int InventoryType { get; set; }
        public int InventoryId { get; set; }
        public string Manufacturer { get; set; }
        public int ManufacturerId { get; set; }
        public int DivisionId { get; set; }
    }
}
