﻿using System;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Admin;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types;
using FirstLook.Merchandising.DomainModel.Marketing.AdPreview;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.Documents;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Merchandising.DomainModel.Marketing.Vehicle;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.AdStatus;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Reports.InventoryPerformace;
using FirstLook.Merchandising.DomainModel.Reports.SitePerformance;
using FirstLook.Merchandising.DomainModel.Reports.TimeToMarket;
using FirstLook.Merchandising.DomainModel.Synonyms;
using FirstLook.Merchandising.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel.Templating.Previews.DAL;
using FirstLook.Merchandising.DomainModel.VehicleHistory;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;
using FirstLook.Merchandising.DomainModel.Workflow.AutoOffline;
using FirstLook.Merchandising.DomainModel.Workflow.PriceComparisonsThrehold;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl.V_2_0;
using Merchandising.Messages;
using PublishingDomain;
using PublishingDomain.FileSystem;
using ICache = FirstLook.Common.Core.ICache;

namespace FirstLook.Merchandising.DomainModel
{
    public class MerchandisingDomainRegistry : IRegistryModule
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<PreviewPreferencesDao>().As<IPreviewPreferencesDao>();

            //Group level cache
            builder.RegisterType<GroupLevelS3DataCache>().As<IGroupLevelDataCache>();

            //MaxAnalytics
            builder.RegisterType<AnalyticsWrapper>().As<IMaxAnalytics>();
            builder.RegisterType<DataOnlineRepository>().As<IDataOnlineRepository>();
	        builder.RegisterType<InventoryCountWebservice>().As<IInventoryCountWebService>();
	        builder.RegisterType<InventoryCountDao>().As<InventoryCountDao>();
			builder.RegisterType<InventoryCountLegacyDao>().As<InventoryCountLegacyDao>();

            //Analytics
            builder.RegisterType<GoogleAnalyticsRepository>().As<IGoogleAnalyticsRepository>();
            builder.RegisterType<GoogleAnalyticsQuery>().As<IGoogleAnalyticsQuery>();
            builder.RegisterType<GoogleAnalyticsWebAuthorization>().As<IGoogleAnalyticsWebAuthorization>();
            builder.RegisterType<GoogleAnalyticsResultCache>().As<IGoogleAnalyticsResultCache>();

            //Merchandising
            builder.Register(c => new InventoryDataAccess()).Named<IInventoryDataAccess>("DbAccess");
            builder.Register(c => new InventoryDataAccessCache(c.ResolveNamed<IInventoryDataAccess>("DbAccess")))
                .As<IInventoryDataAccessCache>()
                .As<IInventoryDataAccess>();
            builder.Register(c => new MappedThirdPartyOptionCache(new MappedThirdPartyOptionDataAccess()))
                .As<IMappedThirdPartyOptionDataAccess>();
            builder.Register(
                c => new VehicleConfigurationCache(new VehicleConfigurationDataAccess(c.Resolve<IChromeMapper>(), c.Resolve<IAdMessageSender>())))
                .As<IVehicleConfigurationDataAccess>();

            builder.RegisterType<TimeProvider>().As<ITimeProvider>().SingleInstance();
            builder.RegisterType<WebLoaderPhotoRepository>().As<IWebLoaderPhotoRepository>();

            builder.RegisterType<WebsiteProviderMetricsModel>().As<IWebsiteProviderMetricsModel>();
            builder.RegisterType<DealerListingSiteCredentialRepository>().As<IDealerListingSiteCredentialRepository>();
            builder.RegisterType<DealershipSegmentModel>().As<IDealershipSegmentModel>();

            // VehicleHistoryReportDomain
            builder.RegisterType<CarFaxUrlBuilder>().As<ICarFaxUrlBuilder>();

            //
            //Release Tasks
            //
            builder.RegisterType<MerchandisingTaskThreadStarter>().As<IMerchandisingTaskThreadStarter>();

            //
            // Market Analysis
            //
            builder.RegisterType<MarketAnalysisDealerPreferenceDataAccess>()
                .As<IMarketAnalysisDealerPreferenceDataAccess>();

            builder.RegisterType<MarketAnalysisPricingDataAccess>().As<IMarketAnalysisPricingDataAccess>();

            builder.RegisterType<MarketAnalysisVehiclePreferenceDataAccess>()
                .As<IMarketAnalysisVehiclePreferenceDataAccess>();

            // Consumer Highlights
            builder.RegisterType<ConsumerHighlightsDealerPreferenceDataAccess>()
                .As<IConsumerHighlightsDealerPreferenceDataAccess>();
            builder.RegisterType<ConsumerHighlightsVehiclePreferenceDataAccess>()
                .As<IConsumerHighlightsVehiclePreferenceDataAccess>();

            // Photos
            builder.RegisterType<PhotoLocatorDataAccess>().As<IPhotoLocatorDataAccess>();
            builder.RegisterType<PhotoVehiclePreferenceDataAccess>().As<IPhotoVehiclePreferenceDataAccess>();


            // Dealer general pref
            builder.RegisterType<DealerGeneralPreferenceDataAccess>().As<IDealerGeneralPreferenceDataAccess>();

            // Certified
            builder.RegisterType<CertifiedVehiclePreferenceDataAccess>().As<ICertifiedVehiclePreferenceDataAccess>();

            // Value analyzer
            builder.RegisterType<ValueAnalyzerVehiclePreferenceDataAccess>()
                .As<IValueAnalyzerVehiclePreferenceDataAccess>();

            // Vehicle summary
            builder.RegisterType<VehicleSummaryDataAccess>().As<IVehicleSummaryDataAccess>();

            // Equipment
            builder.RegisterType<EquipmentProviderAssignmentDataAccess>().As<IEquipmentProviderAssignmentDataAccess>();
            builder.RegisterType<EquipmentDataAccess>().As<IEquipmentDataAccess>();
            builder.RegisterType<EquipmentProviderDataAccess>().As<IEquipmentProviderDataAccess>();

            //AdPreview
            builder.RegisterType<AdPreviewVehiclePreferenceDataAccess>().As<IAdPreviewVehiclePreferenceDataAccess>();

            // Time to market reports
            builder.RegisterType<ReportStartDates>().As<IReportStartDates>();
			builder.RegisterType<AdStatusRepository>().As<IAdStatusRepository>();
	        builder.RegisterType<TimeToMarketRepository>().As<ITimeToMarketRepository>();

            //
            // Market Listings
            //

            // A cache decorator around the DAL.
            builder.Register(c =>
                             new MarketListingVehiclePreferenceDataAccessCache(
                                 new MarketListingVehiclePreferenceDataAccess(),
                                 c.Resolve<ICache>()))
                .As<IMarketListingVehiclePreferenceDataAccess>();

            //builder.Register(c => new MarketListingSearchAdapter(c.Resolve<IMarketListingV1VehiclePreferenceDataAccess>()))
            //    .As<IMarketListingSearchAdapter>();
            builder.Register(c => new MarketListingSearchAdapter())
                .As<IMarketListingSearchAdapter>();

            // Old Ping access
            builder.Register(c => new MarketListingsV1DataAccess()).As<IMarketListingsV1DataAccess>();

            // New Ping (ProfitMAX)
            builder.Register(c => new MarketListingsV2DataAccess()).As<IMarketListingsV2DataAccess>();

            // Publishing
            builder.RegisterType<ValueAnalyzerVehiclePreferenceDataAccess>()
                .As<IValueAnalyzerVehiclePreferenceDataAccess>();

            builder.RegisterType<FileStoreFactory>().As<IFileStoreFactory>();
            builder.RegisterType<AmazonInventoryDocumentRepository>().As<IInventoryDocumentRepository>();

            // 
            builder.RegisterType<NoSqlDbFactory>().As<INoSqlDbFactory>();
            builder.RegisterType<SimpleDbFactory>().As<ISimpleDbFactory>();

            //Public API Documents with version decorator
            builder.Register(
                c =>
                    new InventoryDocumentVersionAdapter(
                        new InventoryDocumentDataAccess(), 
                        new Version(1, 2), 
                        c.Resolve<IQueueFactory>() 
                )
            ).As<IInventoryDocumentDataAccess>();

            //Dashboard
            builder.RegisterType<DashboardBusinessUnitRepository>().As<IDashboardBusinessUnitRepository>();
            builder.RegisterType<GroupDashboardManager>().As<IGroupDashboardManager>();
            builder.RegisterType<DashboardManager>().As<IDashboardManager>();
            builder.RegisterType<DashboardRepository>().As<IDashboardRepository>();
            builder.RegisterType<Clock>().As<IClock>();

            //AutoLoad Admin Audit
            builder.RegisterType<AutoLoadAuditRepository>().As<IAutoLoadAuditRepository>();
            builder.RegisterType<VehicleReferenceDataRepository>().As<IVehicleReferenceDataRepository>();   // Used to post-process the autoload xml
                                                                                                            // called by the AutoLoadAudit task

            //Inventory Performace
            builder.RegisterType<PerformanceRepository>().As<IPerformanceRepository>();

            builder.RegisterType<SiteBudgetRepository>().As<ISiteBudgetRepository>();

            //Site performance
            builder.RegisterType<SitePerformanceRepository>().As<ISitePerformanceRepository>();

            // Workflows
            builder.RegisterType<WorkflowRepository>().As<IWorkflowRepository>().SingleInstance();
            builder.Register(c => new AgeBucketRepository()).Named<IAgeBucketRepository>("DbAccess");
            builder.RegisterType<PriceComparisonsRepository>().As<IPriceComparisonsRepository>();
            //builder.Register(c => new PriceComparisonsRepository()).Named<IPriceComparisonsRepository>("DbAccess");
            builder.Register(c => new AgeBucketCachedRepository(
                                      c.Resolve<ICache>(),
                                      c.ResolveNamed<IAgeBucketRepository>("DbAccess")))
                .SingleInstance().As<IAgeBucketRepository>();

            // Inventory
            builder.Register(c => new InventoryDataRepository()).SingleInstance().As<IInventoryDataRepository>();
            builder.Register(c => new InventoryDataSessionCachedRepository())
                .As<IInventoryDataSessionCachedRepository>().SingleInstance();

            builder.Register(c => new InventoryQueryBuilder(c.Resolve<IWorkflowRepository>()))
                .As<IInventoryQueryBuilder>();

            // Inventory Search / New Car Pricing
            builder.RegisterType<InventorySearch>().As<IInventorySearch>();
            builder.RegisterType<DiscountPricingRepository>().As<IDiscountPricingRepository>();
            builder.RegisterType<DiscountPricingCampaignManager>().As<IDiscountPricingCampaignManager>();

            // Chrome
            builder.RegisterType<DotnetMemoryCacheWrapper>().As<IMemoryCache>();
            builder.Register(c => new ChromeMapRepository()).Named<IChromeMapRepository>("DbAccess");
            builder.Register(c => new ChromeMapCachedRepository(
                                      c.Resolve<IMemoryCache>(),
                                      c.ResolveNamed<IChromeMapRepository>("DbAccess")
                                      )).SingleInstance().As<IChromeMapRepository>();
            builder.RegisterType<ChromeMapper>().SingleInstance().As<IChromeMapper>();



            builder.Register(c => new VinPatternRepository()).Named<IVinPatternRepository>("DbAccess");
            builder.Register(
                c => new CachedVinPatternRepository(
                    c.Resolve<ICache>(), 
                    c.ResolveNamed<IVinPatternRepository>("DbAccess")))
                .SingleInstance().As<IVinPatternRepository>();

            builder.Register(c => new StyleRepository()).Named<IStyleRepository>("DbAccess");
            builder.Register(c => new CachedStyleRepository(
                                      c.Resolve<ICache>(),
                                      c.ResolveNamed<IStyleRepository>("DbAccess")))
                .SingleInstance().As<IStyleRepository>();

            builder.Register(c => new SharedChromeDataRepository()).Named<ISharedChromeDataRepository>("DbAccess");
            builder.Register(c => new CachedSharedChromeDataRepository(
                                      c.Resolve<ICache>(),
                                      c.ResolveNamed<ISharedChromeDataRepository>("DbAccess")))
                .SingleInstance().As<ISharedChromeDataRepository>();

            // Messaging
            builder.RegisterType<AdMessageSender>().As<IAdMessageSender>();

            // attempt register new objects to get task processor to run as a fix to FB: 31580
            builder.RegisterType<AdMessageSender>().As<IDocumentPublishMessager>();
            builder.RegisterType<AdMessageSender>().As<IPricingMessager>();
            builder.RegisterType<AdMessageSender>().As<IWebloaderMessager>();


            // Auto Offline
            builder.RegisterType<AutoOfflineProcessor>().As<IAutoOfflineProcessor>().SingleInstance();
            builder.RegisterType<AutoOfflineProcessorTask>().AsSelf();

            //Option packages
            builder.Register(c => new OptionPackageRulesCache(new OptionPackageRulesRepository()))
                .As<IOptionPackageRulesRepository>();

            // Authoriation
            builder.Register(c => new DealerRepository()).Named<IDealerRepository>("DbAccess");
            builder.Register(c => new DealerCachedRepository(
                                      c.ResolveNamed<IDealerRepository>("DbAccess"),
                                      c.Resolve<IMemoryCache>())).SingleInstance().As<IDealerRepository>();
            builder.Register(c => new DealerServices(c.Resolve<IDealerRepository>()))
                .SingleInstance().As<IDealerServices>();
            builder.Register(c => new UserRepository(c.Resolve<IDealerServices>())).Named<IUserRepository>("DbAccess");
            builder.Register(
                c => new UserCachedRepository(c.Resolve<ICache>(), c.ResolveNamed<IUserRepository>("DbAccess")))
                .SingleInstance().As<IUserCachedRepository>().As<IUserRepository>();
            builder.Register(c => new UserServices(c.Resolve<IUserRepository>(), c.Resolve<IAuthenticateUser>(), c.Resolve<ICurrentPrincipal>()))
                .SingleInstance().As<IUserServices>();
            builder.Register(c => new AuthorizeUser(c.Resolve<IUserServices>())).SingleInstance().As<IAuthorizeUser>();
            builder.Register(c => new AuthorizeDealer(c.Resolve<IUserServices>(), c.Resolve<IDealerServices>()))
                .SingleInstance().As<IAuthorizeDealer>();
            builder.Register(c => new UnauthorizedResultFactory()).SingleInstance().As<IUnauthorizedResultFactory>();

            // Authentication
            builder.Register(c => new AuthenticateUserWithCasClient()).Named<IAuthenticateUser>("WebsvcAccess");
            builder.Register(c => new CachedAuthenticateUser(
                                      c.Resolve<ICache>(),
                                      c.ResolveNamed<IAuthenticateUser>("WebsvcAccess"),
                                      c.Resolve<ITimeProvider>()))
                .SingleInstance().As<IAuthenticateUser>();
            builder.Register(c => new CurrentAspNetPrincipal()).SingleInstance().As<ICurrentPrincipal>();

            // Vehicle History
            builder.Register(c => new CarfaxServices(c.Resolve<IUserServices>())).Named<ICarfaxServices>("WebsvcAccess");
            builder.Register(c => new CachedCarfaxServices(
                                      c.Resolve<ICache>(), c.ResolveNamed<ICarfaxServices>("WebsvcAccess")))
                .SingleInstance().As<ICarfaxServices>();


            // New Car Pricing
            builder.RegisterType<NewCarPricingRepository>().As<INewCarPricingRepository>();

            // Vehicle Photo Transfer
            builder.Register(c => new PhotoTransferDataAccess()).SingleInstance().As<IPhotoTransferDataAccess>();

            // Vehicle Templating Data Factory
            builder.RegisterType<VehicleTemplatingDataFactory>().As<IVehicleTemplatingFactory>();

            // Document publishing
            builder.RegisterType<EquipmentRepository>().As<IEquipmentRepository>();
            builder.RegisterType<OptionsRepository>().As<IOptionsRepository>();
            builder.RegisterType<ReferenceDataRepository>().As<IReferenceDataRepository>();
	        builder.RegisterType<MarketingDocumentRepository>().As<IMarketingDocumentRepository>();

            //Synonyms
            builder.RegisterType<SynonymDbFetcher>().As<ISynonymFetcher>();
            builder.RegisterType<CachedSynonymRepository>().As<ISynonymRepository>();
            builder.RegisterType<SynonymEvaluator>().As<ISynonymEvaluator>();

        }
    }
}
