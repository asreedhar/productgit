﻿using System;
using System.Web;
using FirstLook.DomainModel.Oltp;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public static class WorkflowState
    {
        public const string InventorySessionKey = "Inventory";

        public const string InventoryKey = "inv";
        public const string FilterKey = "filter";
        public const string UsedNewKey = "usednew";
        public const string AgeBucketKey = "agebucket";

        public static string CreateUrl()
        {
            return CreateUrl(HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, InventoryId, ActiveListType, UsedOrNewFilter, AgeBucketId);
        }

        public static string CreateUrl(int inventoryId, WorkflowType filter, int usedNew, int? ageBucketId)
        {
            return CreateUrl(HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath, inventoryId, filter, usedNew, ageBucketId);
        }

        public static string CreateUrl(string baseurl)
        {
            return CreateUrl(baseurl, InventoryId, ActiveListType, UsedOrNewFilter, AgeBucketId);
        }

        public static string CreateUrl(string baseurl, int inventoryId, WorkflowType filter, int usedNew, int? ageBucketId)
        {
            return String.Format("{0}?{1}={2}&{3}={4}&{5}={6}&{7}={8}", baseurl, InventoryKey, inventoryId, FilterKey, filter, UsedNewKey, usedNew, AgeBucketKey, ageBucketId);
        }
        public static string CreatePricingPingUrl(string url, int inventoryId, int businessUnitId,WorkflowType filter= WorkflowType.None, int? usedNew=null, int? ageBucketId=null)
        {
            return String.Format(url + "?inv={0}&businessUnitId={1}&{2}={3}&{4}={5}&{6}={7}",  inventoryId, businessUnitId, FilterKey, filter, UsedNewKey, usedNew, AgeBucketKey, ageBucketId);
        }
        private static void SetBusinessUnitItem()
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(
                HttpContext.Current.User.Identity.Name,
                SoftwareSystemComponentStateFacade.DealerComponentToken);

            if (state != null)
            {
                BusinessUnit dealer = state.Dealer.GetValue();
                if (dealer != null)
                    HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);
            }
        }

        public static int BusinessUnitID
        {
            get
            {
                return BusinessUnit.Id;
            }
        }

        public static BusinessUnit BusinessUnit
        {
            get
            {
                var bu = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
                if (bu == null)
                    SetBusinessUnitItem();

                bu = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
                if (bu == null)
                    throw new ApplicationException("Invalid business unit");
                return bu;
            }
        }

        public static int InventoryId
        {
            get
            {
                var inventoryStringValue = HttpContext.Current.Request.QueryString[InventoryKey];
                int inventoryValue;
                if(!int.TryParse(inventoryStringValue, out inventoryValue))
                    throw new InvalidOperationException("Cannot parse InventoryID from Querystring");
                
                return inventoryValue;
            }
        }

        public static WorkflowType ActiveListType
        {
            get
            {
                var workFlowStringValue = HttpContext.Current.Request.QueryString[FilterKey];
                WorkflowType workflowTypeValue;
                if (!Enum.TryParse(workFlowStringValue, true, out workflowTypeValue))
                    throw new InvalidOperationException("Cannot parse WorkflowType");

                return workflowTypeValue;
            }
        }

        public static int? AgeBucketId
        {
            get
            {
                var ageBucketstringValue = HttpContext.Current.Request.QueryString[AgeBucketKey];
                if (string.IsNullOrWhiteSpace(ageBucketstringValue))
                    return null;

                int ageBucketId;
                if(!int.TryParse(ageBucketstringValue, out ageBucketId))
                    throw new InvalidOperationException("Cannot parse ageBucket from Querystring");

                return ageBucketId;
            }
        }

        public static int UsedOrNewFilter
        {
            get
            {
                var usedNewStringValue = HttpContext.Current.Request.QueryString[UsedNewKey];
                int usedNewValue;
                if (!int.TryParse(usedNewStringValue, out usedNewValue))
                    throw new InvalidOperationException("Cannot parse usedNew from Querystring");

                return usedNewValue;
            }
        }

    }
}
