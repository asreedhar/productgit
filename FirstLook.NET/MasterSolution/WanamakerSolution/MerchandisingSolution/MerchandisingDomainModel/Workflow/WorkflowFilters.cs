﻿using System;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public static class WorkflowFilters
    {
        public static bool DoNotPost(IInventoryData inv)
        {
            return inv.StatusBucket == MerchandisingBucket.DoNotPost;
        }

        public static bool NotPosted(IInventoryData inv)
        {
            return inv.StatusBucket == null
                   || inv.StatusBucket == MerchandisingBucket.PendingApproval
                   || inv.StatusBucket == MerchandisingBucket.NotPostedNeedsAttention;
        }

        public static bool Online(IInventoryData inv)
        {
            return inv.StatusBucket == MerchandisingBucket.Online
                   || inv.StatusBucket == MerchandisingBucket.OnlineNeedsAttention;
        }

        public static bool NotOnline(IInventoryData inv)
        {
            return inv.OnlineState == 2/*Not Online*/;
        }

        public static bool AllInventory(IInventoryData obj)
        {
            return true;
        }

        /* Used or New */
        public static bool IsUsed(IInventoryData inv)
        {
            return !inv.IsNew();
        }

        /* Equipment */
        public static bool IsPostableAndEquipmentNeeded(IInventoryData inv)
        {
            return !DoNotPost(inv) && EquipmentNeeded(inv);
        }

        public static bool OnlineAndNotOutdatedPriceAndNeedsEquipment(IInventoryData inv)
        {
            return Online(inv) && !HasOutdatedPrice(inv) && EquipmentNeeded(inv);
        }

        public static bool EquipmentNeeded(IInventoryData inv)
        {
            return inv.NeedsExterior || inv.NeedsInterior;
        }

        /* Photos Counts */
        public static bool IsPostableAndNoPhotos(IInventoryData inv)
        {
            return !DoNotPost(inv) && NoPhotos(inv);
        }

        public static bool IsPostableAndLowPhotos(IInventoryData inv)
        {
            return !DoNotPost(inv) && LowPhotos(inv);
        }

        public static bool NoOrLowPhotos(IInventoryData inv)
        {
            return NoPhotos(inv) || LowPhotos(inv);
        }


        public static bool NoPhotos(IInventoryData inv)
        {
            return inv.PhotoCount == 0 && inv.NeedsPhotos;
        }

        public static bool LowPhotos(IInventoryData inv)
        {
            return inv.PhotoCount > 0 && inv.PhotoCount < inv.DesiredPhotoCount && inv.NeedsPhotos;
        }


        public static bool OnlineAndNotOutdatedPriceAndNeedsPhotos(IInventoryData inv)
        {
            return Online(inv) && !HasOutdatedPrice(inv) && NoOrLowPhotos(inv);
        }

        /* Pricing */
        public static bool IsPostableAndNoListPrice(IInventoryData inv)
        {
            return !DoNotPost(inv) && NoListPrice(inv);
        }

        public static bool NoListPrice(IInventoryData inv)
        {
            return inv.ListPrice <= 0;
        }

        public static bool OnlineAndOutdatedPrice(IInventoryData inv)
        {
            return Online(inv) && HasOutdatedPrice(inv);
        }

        public static bool HasOutdatedPrice(IInventoryData inv)
        {
            return inv.OutdatedPrice;
        }

        public static bool HasOutdatedPrice(decimal? lastUpdateListPrice, decimal listPrice)
        {
            return lastUpdateListPrice.HasValue 
                && lastUpdateListPrice > 0m
                && Math.Round(lastUpdateListPrice.Value, MidpointRounding.AwayFromZero) !=
                    Math.Round(listPrice, MidpointRounding.AwayFromZero);
        }

        public static bool OnlineAndNotOutdatedPriceAndNeedsPricing(IInventoryData inv)
        {
            return Online(inv) && !HasOutdatedPrice(inv) && NoListPrice(inv);
        }

        public static bool IsPostableAndUsedAndDueForRepricing(IInventoryData inv)
        {
            return !DoNotPost(inv) && IsUsed(inv) && DueForRepricing(inv);
        }

        public static bool DueForRepricing(IInventoryData inv)
        {
            return inv.IsDueForRepricing == DueForRepricingMode.DueForRepricing;
        }

        /* Ad Review */
        public static bool IsPostableAndAdReviewNeeded(IInventoryData inv)
        {
            return !DoNotPost(inv) && AdReviewNeeded(inv);
        }

        public static bool AdReviewNeeded(IInventoryData inv)
        {
            return inv.NeedsAdReview;
        }

        /* Ad Quality */
        public static bool IsPostableAndLowActivity(IInventoryData inv)
        {
            return !DoNotPost(inv) && IsLowActivity(inv);
        }

        private static bool IsLowActivity(IInventoryData inv)
        {
            return inv.IsLowActivity;
        }

        public static bool IsPostableAndNoTrim(IInventoryData inv)
        {
            return !DoNotPost(inv) && NoTrim(inv);
        }

        public static bool NoTrim(IInventoryData inv)
        {
            return (inv.ChromeStyleId ?? -1) == -1;
        }

        public static bool IsPostableAndUsedAndNoBookValue(IInventoryData inv)
        {
            return !DoNotPost(inv) && IsUsed(inv) && NoBookValue(inv);
        }

        public static bool NoBookValue(IInventoryData inv)
        {
            return !inv.HasCurrentBookValue;
        }

        public static bool IsPostableAndNoPackages(IInventoryData inv)
        {
            return !DoNotPost(inv) && NoPackages(inv) && EquipmentNeeded(inv);
        }

        private static bool NoPackages(IInventoryData inv)
        {
            return inv.NoPackages;
        }

        public static bool IsPostableAndUsedAndNoCarfax(IInventoryData inv)
        {
            return !DoNotPost(inv) && IsUsed(inv) && NoCarfax(inv);
        }

        private static bool NoCarfax(IInventoryData inv)
        {
            return !inv.HasCarfax;
        }

        /* High Level Filters */
        public static bool NeedsApprovalAll(IInventoryData inv)
        {
            return NotPosted(inv) || IsPostableAndNoTrim(inv) || OnlineAndOutdatedPrice(inv);
        }

        public static bool NeedsAdReviewAll(IInventoryData inv)
        {
            return IsPostableAndNoTrim(inv) || IsPostableAndAdReviewNeeded(inv) || NotPosted(inv);
        }

        public static bool NeedsPricingAll(IInventoryData inv)
        {
            return IsPostableAndNoListPrice(inv) || IsPostableAndUsedAndDueForRepricing(inv);
        }

        public static bool LowAdQualityAll(IInventoryData inv)
        {
            return IsPostableAndNoPackages(inv)
                   || IsPostableAndUsedAndNoBookValue(inv)
                   || IsPostableAndUsedAndNoCarfax(inv)
                   || IsPostableAndLowActivity(inv)
                   || IsPostableAndNoPriceComparison(inv);
        }

        public static bool NeedsPhotosEquipmentAll(IInventoryData inv)
        {
            return IsPostableAndEquipmentNeeded(inv)
                   || IsPostableAndNoPhotos(inv)
                   || IsPostableAndLowPhotos(inv);
        }

        public static bool NoPriceComparison(IInventoryData inv)
        {
            return  Online(inv) && inv.isFavourable;
        }

        public static bool IsPostableAndNoPriceComparison(IInventoryData inv)
        {
            return !DoNotPost(inv) && NoPriceComparison(inv);
        }
    }
}