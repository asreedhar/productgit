﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using MAX.Entities;
using MAX.Entities.Filters;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public static class Extensions
    {
        public static IEnumerable<IInventoryData> WorkflowFilter(
            this IEnumerable<IInventoryData> inventory, WorkflowType type,
            Predicate<IInventoryData> conditionFilter = null,
            Predicate<IInventoryData> ageBucketFilter = null,
            string searchText = null)
        {
            var predicate = Bin.CreatePredicate(type, conditionFilter, ageBucketFilter, searchText);
            return inventory.Where(inv => predicate(inv));
        }

        public static IEnumerable<IInventoryData> WorkflowFilter(
            this IEnumerable<IInventoryData> inventory, IInventoryQueryBuilder queryBuilder)
        {
            var predicate = queryBuilder.ToPredicate();
            return predicate == null
                       ? inventory
                       : inventory.Where(predicate);
        }

        public static string GetNavigationUrl(this IWorkflow workflow)
        {
            return
                workflow.IsPricingWorkflow()
                    ? "Pricing.aspx"
                    : "ApprovalSummary.aspx";
        }

        public static bool IsPricingWorkflow(this IWorkflow workflow)
        {
            var t = workflow.Type;
            return t == WorkflowType.NoPrice
                   || t == WorkflowType.OptimizedNoPrice
                   || t == WorkflowType.DueForRepricing;
        }

        public static Predicate<IInventoryData> CreatePredicate(this IWorkflow workflow,
            Predicate<IInventoryData> conditionFilter = null, 
            Predicate<IInventoryData> ageBucketFilter = null, 
            string searchText = null)
        {
            return Bin.CreatePredicate(workflow.Type, conditionFilter, ageBucketFilter, searchText);
        }

        public static Bin CreateBin(this IWorkflow workflow, 
            UsedOrNewFilter usedOrNew = UsedOrNewFilter.Used_and_New, 
            int? ageBucketId = null, 
            string searchText = null)
        {
            return Bin.Create(workflow.Type, (int) usedOrNew, ageBucketId, searchText);
        }
    }
}