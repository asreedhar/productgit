﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public interface IWorkflowRepository
    {
        IWorkflow GetWorkflow(WorkflowType type);
        IEnumerable<IWorkflow> Workflows { get; }
        IWorkflowCounts CalculateCounts(IEnumerable<IInventoryData> inventory, ISet<WorkflowType> workflowsToCount = null);
        int CalculateTotal( IEnumerable<IInventoryData> inventory, ISet<WorkflowType> workflowsToCount );
    }
}