namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public interface IPresenter
    {
        void Initialize();
        void Load();
        void PreRender();
    }
}