﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    internal class WorkflowRepository : IWorkflowRepository
    {
        private static readonly Lazy<IDictionary<WorkflowType, IWorkflow>> WorkflowLookup = 
            new Lazy<IDictionary<WorkflowType, IWorkflow>>(BuildList, LazyThreadSafetyMode.PublicationOnly);

        private static IDictionary<WorkflowType, IWorkflow> BuildList()
        {
            var list = new []
            {
                new WorkflowInfo("All Inventory", WorkflowType.AllInventory, MerchandisingBucket.All, WorkflowFilters.AllInventory),
                new WorkflowInfo("Approved", WorkflowType.OptimizedAds, MerchandisingBucket.AllOnline, WorkflowFilters.Online),
                new WorkflowInfo("Marked Offline", WorkflowType.Offline, MerchandisingBucket.DoNotPost, WorkflowFilters.DoNotPost),
                new WorkflowInfo("Not Online", WorkflowType.NotOnline, MerchandisingBucket.All, WorkflowFilters.NotOnline),

                new WorkflowInfo("Needs Approval > All", WorkflowType.NeedsApprovalAll, MerchandisingBucket.All, WorkflowFilters.NeedsApprovalAll),
                new WorkflowInfo("No Description", WorkflowType.CreateInitialAd, MerchandisingBucket.NotPosted, WorkflowFilters.NotPosted),
                new WorkflowInfo("No Trim", WorkflowType.TrimNeeded, MerchandisingBucket.All, WorkflowFilters.IsPostableAndNoTrim),
                new WorkflowInfo("Outdated Price in Ad", WorkflowType.OutdatedPrice, MerchandisingBucket.AllOnline, WorkflowFilters.OnlineAndOutdatedPrice),

                new WorkflowInfo("Needs Ad Review > All", WorkflowType.NeedsAdReviewAll, MerchandisingBucket.AllOnline, WorkflowFilters.NeedsAdReviewAll),
                // And also No Trim
                new WorkflowInfo("Ad Review Needed", WorkflowType.AdReviewNeeded, MerchandisingBucket.NotPosted, WorkflowFilters.IsPostableAndAdReviewNeeded),

                new WorkflowInfo("Needs Pricing > All", WorkflowType.NeedsPricingAll, MerchandisingBucket.AllOnline, WorkflowFilters.NeedsPricingAll),
                new WorkflowInfo("No Price", WorkflowType.NoPrice, MerchandisingBucket.AllOnline, WorkflowFilters.IsPostableAndNoListPrice),
                new WorkflowInfo("Needs Re-pricing", WorkflowType.DueForRepricing, MerchandisingBucket.All, WorkflowFilters.IsPostableAndUsedAndDueForRepricing),

                new WorkflowInfo("Low Ad Quality > All", WorkflowType.LowAdQualityAll, MerchandisingBucket.AllOnline, WorkflowFilters.LowAdQualityAll),
                new WorkflowInfo("No Packages", WorkflowType.NoPackages, MerchandisingBucket.All, WorkflowFilters.IsPostableAndNoPackages),
                new WorkflowInfo("No Book Value", WorkflowType.BookValueNeeded, MerchandisingBucket.All, WorkflowFilters.IsPostableAndUsedAndNoBookValue),
                new WorkflowInfo("No Carfax", WorkflowType.NoCarfax, MerchandisingBucket.All, WorkflowFilters.IsPostableAndUsedAndNoCarfax),
                new WorkflowInfo("Low Online Activity", WorkflowType.LowActivity, MerchandisingBucket.AllOnline, WorkflowFilters.IsPostableAndLowActivity),

                new WorkflowInfo("Needs Photos/Equipment > All", WorkflowType.NeedsPhotosEquipmentAll, MerchandisingBucket.All, WorkflowFilters.NeedsPhotosEquipmentAll),
                new WorkflowInfo("Equipment Review", WorkflowType.EquipmentNeeded, MerchandisingBucket.All, WorkflowFilters.IsPostableAndEquipmentNeeded),
                new WorkflowInfo("Low Photos", WorkflowType.LowPhotos, MerchandisingBucket.All, WorkflowFilters.IsPostableAndLowPhotos),
                new WorkflowInfo("No Photos", WorkflowType.NoPhotos, MerchandisingBucket.All, WorkflowFilters.IsPostableAndNoPhotos),
                 new WorkflowInfo("No/Low Price Comparisons",WorkflowType.NoFavourablePriceComparisons,MerchandisingBucket.All,WorkflowFilters.IsPostableAndNoPriceComparison), 
                               
                // Maybe delete these? Someday?
                new WorkflowInfo("Equipment Review", WorkflowType.OptimizedEquipmentNeeded, MerchandisingBucket.AllOnline, WorkflowFilters.OnlineAndNotOutdatedPriceAndNeedsEquipment),
                new WorkflowInfo("No/Low Photos", WorkflowType.OptimizedNoLowPhotos, MerchandisingBucket.AllOnline, WorkflowFilters.OnlineAndNotOutdatedPriceAndNeedsPhotos),
                new WorkflowInfo("Zero/No Price", WorkflowType.OptimizedNoPrice, MerchandisingBucket.AllOnline, WorkflowFilters.OnlineAndNotOutdatedPriceAndNeedsPricing),

                new WorkflowInfo("New Inventory", WorkflowType.NewInventory, MerchandisingBucket.NotPosted, WorkflowFilters.NotPosted)

               
            };
            return list.ToDictionary(x => x.Type, x => (IWorkflow) x);
        }

        public IWorkflow GetWorkflow(WorkflowType type)
        {
            return WorkflowLookup.Value[type];
        }

        public IEnumerable<IWorkflow> Workflows
        {
            get { return WorkflowLookup.Value.Values; }
        }

        public IWorkflowCounts CalculateCounts(IEnumerable<IInventoryData> inventory, ISet<WorkflowType> workflowsToCount)
        {
            return new WorkflowCounts(this, inventory, workflowsToCount);
        }

        public int CalculateTotal( IEnumerable<IInventoryData> inventory, ISet<WorkflowType> workflowsToCount )
        {
            return WorkflowCounts.CalculateTotal( this, workflowsToCount, inventory );
        }
    }
}