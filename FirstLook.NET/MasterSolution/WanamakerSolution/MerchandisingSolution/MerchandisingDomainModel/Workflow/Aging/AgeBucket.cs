using System;
using System.Globalization;

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    [Serializable]
    internal class AgeBucket : IAgeBucket
    {
        public AgeBucket(int? id, int low, int? high)
        {
            if(low > high)
                throw new ArgumentOutOfRangeException("low", "Age Bucket low range must be less then high range.");
            if(low < 0)
                throw new ArgumentOutOfRangeException("low", "Age Bucket low range must be positive.");

            Id = id;
            Low = low;
            High = high;
        }

        public int? Id { get; private set; }

        public int Low { get; private set; }

        public int? High{ get; private set; }
        
        public string Description
        {
            get
            {
                return string.Format(
                    "{0}{1}{2}{3} Days",
                    Low,
                    Low != High && High.HasValue ? "-" : "",
                    High.HasValue && High != Low ? High : null,
                    !Id.HasValue || !High.HasValue ? "+" : "");
            }
        }

        public override string ToString()
        {
            return Description;
        }
    }
}