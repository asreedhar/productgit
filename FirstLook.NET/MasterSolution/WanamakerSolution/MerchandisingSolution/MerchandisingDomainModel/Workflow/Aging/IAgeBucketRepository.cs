﻿namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public interface IAgeBucketRepository
    {
        IAgeBuckets GetBucketsForBusinessUnit(int businessUnitId);
    }
}