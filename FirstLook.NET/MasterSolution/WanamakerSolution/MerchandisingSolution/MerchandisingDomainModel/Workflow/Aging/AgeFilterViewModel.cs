using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    internal class AgeFilterViewModel : IAgeFilterViewModel
    {
        public IList<IAgeBucket> AgeBuckets { get; private set; }
        public int? CurrentAgeBucketId { get; private set; }

        public AgeFilterViewModel(IList<IAgeBucket> ageBuckets, int? currentAgeBucketId)
        {
            AgeBuckets = ageBuckets;
            CurrentAgeBucketId = currentAgeBucketId;
        }
    }
}