using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public interface IAgeFilterViewModel
    {
        IList<IAgeBucket> AgeBuckets { get; }
        int? CurrentAgeBucketId { get; }
    }
}