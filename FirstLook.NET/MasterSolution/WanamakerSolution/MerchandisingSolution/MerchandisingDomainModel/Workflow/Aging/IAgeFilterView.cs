using System;

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public interface IAgeFilterView
    {
        Action<int?> AgeBucketClicked { get; set; }
    }
}