using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    internal class AgeBucketCachedRepository : IAgeBucketRepository
    {
        private ICache Cache { get; set; }
        private IAgeBucketRepository Inner { get; set; }

        private static readonly string CacheKeyPrefix = typeof (AgeBucketCachedRepository).FullName + "_AgeBucketsFor_";
        private const int SecondsToExpire = 60*5;

        public AgeBucketCachedRepository(ICache cache, IAgeBucketRepository inner)
        {
            Cache = cache;
            Inner = inner;
        }

        public IAgeBuckets GetBucketsForBusinessUnit(int businessUnitId)
        {
            var key = CacheKeyPrefix + businessUnitId;

            var item = (IAgeBuckets) Cache.Get(key);
            if(item == null)
            {
                item = Inner.GetBucketsForBusinessUnit(businessUnitId);
                Cache.Set(key, item, SecondsToExpire);
            }

            return item;
        }
    }
}