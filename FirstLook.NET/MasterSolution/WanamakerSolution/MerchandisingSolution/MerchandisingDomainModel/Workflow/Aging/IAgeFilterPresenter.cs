namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public interface IAgeFilterPresenter : IPresenter
    {
        int? CurrentAgeBucketId { get; set; }
        IAgeFilterViewModel GetViewModel();
    }
}