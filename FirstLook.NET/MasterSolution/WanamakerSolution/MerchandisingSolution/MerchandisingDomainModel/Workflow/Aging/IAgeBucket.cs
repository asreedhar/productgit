namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public interface IAgeBucket
    {
        int? Id { get; }
        int Low { get; }
        int? High { get; }
        string Description { get; }
    }
}