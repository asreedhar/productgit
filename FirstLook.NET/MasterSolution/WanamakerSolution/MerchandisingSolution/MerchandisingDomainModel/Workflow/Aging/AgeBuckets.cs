using System;
using System.Collections.Generic;
using System.Linq;

// NOTE: We want to use For(...) for performance reasons.
// ReSharper disable ForCanBeConvertedToForeach

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    [Serializable]
    internal class AgeBuckets : IAgeBuckets
    {
        private IList<IAgeBucket> Buckets { get; set; }

        public AgeBuckets(IList<IAgeBucket> buckets)
        {
            var firstLow = 0;
            var appendAdditionalItem = false;
            for(var i = 0; i < buckets.Count; ++i)
            {
                var bucket = buckets[i];
                if(bucket.Low != firstLow)
                    throw new ArgumentException("Age buckets must be contiguous and the first bucket must have a low value of 0.");
                if(i == buckets.Count - 1)
                {
                    if(bucket.High.HasValue)
                        appendAdditionalItem = true;
                }
                else
                {
                    if (!bucket.High.HasValue)
                        throw new ArgumentException("Only the last age bucket can have a null high value.");
                    firstLow = bucket.High.Value + 1;
                }
            }

            var bucketSequence = BuildBucketSequence(buckets, appendAdditionalItem);

            Buckets = bucketSequence.AsReadOnly();
        }

        private List<IAgeBucket> BuildBucketSequence(IList<IAgeBucket> buckets, bool appendAdditionalItem)
        {
            var bucketSequence = new List<IAgeBucket>();
            bucketSequence.Add(CreateAllAgesBucket(buckets, appendAdditionalItem));
            bucketSequence.AddRange(buckets);
            if (appendAdditionalItem)
                bucketSequence.Add(CreateFinalAgeBucket(buckets));
            return bucketSequence;
        }

        private static IAgeBucket CreateAllAgesBucket(IList<IAgeBucket> buckets, bool appendAdditionalBucket)
        {
            if(buckets.Count <= 0)
                return new AgeBucket(null, 0, 0);

            return appendAdditionalBucket 
                ? new AgeBucket(null, 0, (buckets[buckets.Count - 1].High ?? 0) + 1) 
                : new AgeBucket(null, 0, buckets[buckets.Count - 1].Low);
        }

        private static IAgeBucket CreateFinalAgeBucket(IList<IAgeBucket> buckets)
        {
            return new AgeBucket(buckets.Where(b => b.Id.HasValue).Max(b => b.Id ?? 0) + 1,
                                 (buckets[buckets.Count - 1].High ?? 0) + 1, null);
        }

        public int? GetBucketIdForAge(int age)
        {
            if (Buckets.Count <= 1)
                return null;
            if (Buckets.Count == 2 || age < 0)
                return Buckets[1].Id;

            var last = Buckets.Count - 1;
            for(var i = 1; i < last; ++i)
            {
                var b = Buckets[i];
                if (age >= b.Low && age <= b.High)
                    return b.Id;
            }
            return Buckets[last].Id;
        }

        public IAgeBucket GetBucket(int? id)
        {
            return Buckets.FirstOrDefault(b => b.Id == id);
        }

        public IList<IAgeBucket> GetBuckets()
        {
            return Buckets;
        }
    }
}