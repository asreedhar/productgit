namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public interface IAgeFilterModel
    {
        IAgeBuckets GetAgeBuckets();
        int? LoadAgeBucket();
        void SaveAgeBucket(int? id);
    }
}