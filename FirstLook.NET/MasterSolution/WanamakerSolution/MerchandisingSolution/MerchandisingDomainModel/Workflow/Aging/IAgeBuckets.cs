using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public interface IAgeBuckets
    {
        int? GetBucketIdForAge(int age);
        IAgeBucket GetBucket(int? id);
        IList<IAgeBucket> GetBuckets();
    }
}