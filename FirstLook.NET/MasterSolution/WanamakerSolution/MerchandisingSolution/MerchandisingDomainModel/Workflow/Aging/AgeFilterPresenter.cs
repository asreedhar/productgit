﻿namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    public class AgeFilterPresenter : IAgeFilterPresenter
    {
        private IAgeFilterView View { get; set; }
        private IAgeFilterModel Model { get; set; }

        public AgeFilterPresenter(IAgeFilterView view, IAgeFilterModel model)
        {
            View = view;
            Model = model;

            View.AgeBucketClicked = OnAgeBucketClicked;
        }

        public void Initialize()
        {
        }

        public void Load()
        {
        }

        public void PreRender()
        {
        }

        private void OnAgeBucketClicked(int? ageBucketId)
        {
            SetCurrentAgeBucket(ageBucketId, true);
        }

        public IAgeFilterViewModel CalcViewModel()
        {
            return new AgeFilterViewModel(
                Model.GetAgeBuckets().GetBuckets(),
                CurrentAgeBucketId);
        }

        private IAgeFilterViewModel UpdateViewModel(IAgeFilterViewModel originalViewModel)
        {
            return originalViewModel.CurrentAgeBucketId != CurrentAgeBucketId
                ? CalcViewModel()
                : originalViewModel;
        }

        private bool CurrentAgeBucketInitialized { get; set; }
        private int? CurrentAgeBucketIdInner { get; set; }
        public int? CurrentAgeBucketId
        {
            get
            {
                if (!CurrentAgeBucketInitialized)
                    SetCurrentAgeBucket(Model.LoadAgeBucket(), false);
                return CurrentAgeBucketIdInner;
            }
            set { SetCurrentAgeBucket(value, false); }
        }

        private void SetCurrentAgeBucket(int? bucketId, bool shouldSave)
        {
            var ageBuckets = Model.GetAgeBuckets();
            var validatedBucketId = ageBuckets.GetBucket(bucketId);
            CurrentAgeBucketIdInner = validatedBucketId != null ? validatedBucketId.Id : null;
            CurrentAgeBucketInitialized = true;
            if (shouldSave)
                Model.SaveAgeBucket(CurrentAgeBucketIdInner);
        }

        private IAgeFilterViewModel CachedViewModel { get; set; }
        public IAgeFilterViewModel GetViewModel()
        {
            CachedViewModel = CachedViewModel == null ? CalcViewModel() : UpdateViewModel(CachedViewModel);
            return CachedViewModel;
        }
    }
}