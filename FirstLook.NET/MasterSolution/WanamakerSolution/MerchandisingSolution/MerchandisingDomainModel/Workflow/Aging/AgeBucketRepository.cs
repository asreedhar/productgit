using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Logging;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Workflow.Aging
{
    internal class AgeBucketRepository : IAgeBucketRepository
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AgeBucketRepository>();

        public IAgeBuckets GetBucketsForBusinessUnit(int businessUnitId)
        {
            using(MiniProfiler.Current.Step("GetBucketsForBusinessUnit"))
            {
                if (Log.IsInfoEnabled)
                    Log.Info("Starting to execute workflow.GetInventoryBucketRanges");

                var t = DateTimeOffset.UtcNow;

                var buckets = ReadBucketRanges(businessUnitId);

                if(Log.IsInfoEnabled)
                    Log.InfoFormat("Retrieved buckets. ElapsedMs={0:0.0} AgeBucketCount={1}",
                        (DateTimeOffset.UtcNow - t).TotalMilliseconds, buckets.GetBuckets().Count);

                return buckets;
            }
        }

        private IAgeBuckets ReadBucketRanges(int businessUnitId)
        {
            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Merchandising.workflow.GetInventoryBucketRanges";

                cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);

                using(var r = cmd.ExecuteReader())
                {
                    var rangeIdColumn = r.GetOrdinal("RangeID");
                    var lowColumn = r.GetOrdinal("Low");
                    var highColumn = r.GetOrdinal("High");
                    
                    var list = new List<IAgeBucket>();
                    while(r.Read())
                    {
                        list.Add(new AgeBucket(
                                     r.GetByte(rangeIdColumn),
                                     r.GetInt32(lowColumn),
                                     r.IsDBNull(highColumn) ? (int?) null : r.GetInt32(highColumn)));
                    }

                    return new AgeBuckets(list);
                }
            }
        }
    }
}