using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public interface IWorkflowCounts
    {
        /// <summary>
        /// Gets the count for the specified Workflow List.
        /// </summary>
        /// <param name="wfType"></param>
        /// <returns></returns>
        int GetCount(WorkflowType wfType);
    }
}