namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public interface IView
    {
        void Bind();
    }
}