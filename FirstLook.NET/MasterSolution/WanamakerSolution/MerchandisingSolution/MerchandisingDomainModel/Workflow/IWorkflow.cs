using System;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public interface IWorkflow
    {
        string Label { get; }
        WorkflowType Type { get; }
        MerchandisingBucket MerchandisingBucket { get; }
        Predicate<IInventoryData> Filter { get; }
    }
}