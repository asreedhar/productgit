﻿using System;
using System.Collections.Generic;
using System.Linq;
using MAX.Entities;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public class FiltersPresenter : IFiltersPresenter
    {
        internal static IFilterTemplate CreateTemplate()
        {
            return new FilterTemplate(
                defaultWorkflow: 
                    model => model.GetAutoApproveStatus() == AutoApproveStatus.Off 
                        ? WorkflowType.CreateInitialAd 
                        : WorkflowType.AdReviewNeeded,
                auxFilters: new[] { WorkflowType.AllInventory, WorkflowType.OptimizedAds, WorkflowType.Offline },
                highLevelFilters: new[] { WorkflowType.NeedsApprovalAll, WorkflowType.NeedsAdReviewAll, WorkflowType.NeedsPricingAll, 
                                          WorkflowType.LowAdQualityAll, WorkflowType.NeedsPhotosEquipmentAll },
                itemTemplates: new[]
                    {
                        /* Auxiliary Filters */
                        new FilterItemTemplate(WorkflowType.AllInventory, "All Inventory"), 
                        new FilterItemTemplate(WorkflowType.OptimizedAds, "Approved"), 
                        new FilterItemTemplate(WorkflowType.Offline, "Offline"),

                        /* Need Approval */
                        new FilterItemTemplate(WorkflowType.NeedsApprovalAll, "All", 
                                               children: new[] { WorkflowType.NeedsApprovalAll, WorkflowType.CreateInitialAd, WorkflowType.TrimNeeded, WorkflowType.OutdatedPrice },
                                               highLevelParent: WorkflowType.NeedsApprovalAll,
                                               highLevelLabel: "Need Approval",
                                               isValid: model => model.GetAutoApproveStatus() == AutoApproveStatus.Off),
                        new FilterItemTemplate(WorkflowType.CreateInitialAd, "No Description",
                                               highLevelParents: new[] {WorkflowType.NeedsApprovalAll, WorkflowType.NeedsAdReviewAll},
                                               isVisible: model => model.GetAutoApproveStatus() == AutoApproveStatus.Off ||
                                                                   model.GetWorkflowCounts(null).GetCount(WorkflowType.CreateInitialAd) > 0),
                        new FilterItemTemplate(WorkflowType.TrimNeeded, "No Trim", // NoTrim can appear under both "Needs Ad Approval" or "Needs Ad Review"
                                               highLevelParents: new[] { WorkflowType.NeedsApprovalAll, WorkflowType.NeedsAdReviewAll }), 
                        new FilterItemTemplate(WorkflowType.OutdatedPrice, "Outdated Price in Ad", highLevelParent: WorkflowType.NeedsApprovalAll),

                        /* Need Ad Review */
                        new FilterItemTemplate(WorkflowType.NeedsAdReviewAll, "All",
                                               children: new[] { WorkflowType.NeedsAdReviewAll, WorkflowType.TrimNeeded, WorkflowType.CreateInitialAd, WorkflowType.AdReviewNeeded },
                                               highLevelParent: WorkflowType.NeedsAdReviewAll,
                                               highLevelLabel: "Need Review",
                                               isValid: model => model.GetAutoApproveStatus() != AutoApproveStatus.Off), // NoTrim specified above
                        new FilterItemTemplate(WorkflowType.AdReviewNeeded, "Ad Review Needed", highLevelParent: WorkflowType.NeedsAdReviewAll), 

                        /* Need Pricing */
                        new FilterItemTemplate(WorkflowType.NeedsPricingAll, "All",
                                               children: new[] { WorkflowType.NeedsPricingAll, WorkflowType.NoPrice, WorkflowType.DueForRepricing },
                                               highLevelParent: WorkflowType.NeedsPricingAll,
                                               highLevelLabel: "Need Pricing"), 
                        new FilterItemTemplate(WorkflowType.NoPrice, "No Price", highLevelParent: WorkflowType.NeedsPricingAll), 
                        new FilterItemTemplate(WorkflowType.DueForRepricing, "Needs Re-pricing", highLevelParent: WorkflowType.NeedsPricingAll),
                        
                        /* Merchandising Alerts (formally: Low Ad Quality) */
                        new FilterItemTemplate(WorkflowType.LowAdQualityAll, "All",
                                               children: new[] { WorkflowType.LowAdQualityAll, WorkflowType.NoPackages, WorkflowType.BookValueNeeded,
                                                                 WorkflowType.NoCarfax, WorkflowType.LowActivity,WorkflowType.NoFavourablePriceComparisons},
                                               highLevelParent: WorkflowType.LowAdQualityAll,
                                               highLevelLabel: "Merchandising Alerts"),  // changed from "Low Ad Quality" FB: 27963, tureen 11/26/2013 // FB 28657 changed again!
                        new FilterItemTemplate(WorkflowType.NoPackages, "No Packages", highLevelParent: WorkflowType.LowAdQualityAll),
                        new FilterItemTemplate(WorkflowType.BookValueNeeded, "No Book Value", highLevelParent: WorkflowType.LowAdQualityAll), 
                        new FilterItemTemplate(WorkflowType.NoCarfax, "No Carfax", highLevelParent: WorkflowType.LowAdQualityAll), 
                        new FilterItemTemplate(WorkflowType.LowActivity, "Low Online Activity", highLevelParent: WorkflowType.LowAdQualityAll),
                        new FilterItemTemplate(WorkflowType.NoFavourablePriceComparisons,"No/Low Price Comparisons",highLevelParent: WorkflowType.LowAdQualityAll),

                        /* Need Photos/Equipment */
                        new FilterItemTemplate(WorkflowType.NeedsPhotosEquipmentAll, "All",
                                               children: new[] { WorkflowType.NeedsPhotosEquipmentAll, WorkflowType.EquipmentNeeded, WorkflowType.NoPhotos, WorkflowType.LowPhotos},
                                               highLevelParent: WorkflowType.NeedsPhotosEquipmentAll,
                                               highLevelLabel: "Need Photos/Equipment"),
                        new FilterItemTemplate(WorkflowType.EquipmentNeeded, "Equipment Review", highLevelParent: WorkflowType.NeedsPhotosEquipmentAll), 
                        new FilterItemTemplate(WorkflowType.NoPhotos, "No Photos", highLevelParent: WorkflowType.NeedsPhotosEquipmentAll),
                        new FilterItemTemplate(WorkflowType.LowPhotos, "Low Photos", highLevelParent: WorkflowType.NeedsPhotosEquipmentAll)
                    }
                );
        }

        private IFiltersView View { get; set; }
        private IFiltersModel Model { get; set; }
        private IFilterTemplate Template { get; set; }

        private static readonly Lazy<IFilterTemplate> DefaultTemplate = new Lazy<IFilterTemplate>(CreateTemplate);

        public FiltersPresenter(IFiltersView view, IFiltersModel model, IFilterTemplate template = null)
        {
            View = view;
            Model = model;
            Template = template ?? DefaultTemplate.Value;

            View.WorkflowClicked = OnWorkflowClicked;
        }

        public void Initialize()
        {
        }

        public void Load()
        {
        }

        public void PreRender()
        {
        }

        public IFiltersViewModel CalcViewModel()
        {
            var counts = CalcWorkflowCounts();
            var currentHighLevelWorkflow = CalcCurrentHighLevelWorkflow();
            var currentWorkflow = CurrentWorkflow.Type;
            return new FiltersViewModel(
                GetFilterItemList(Template.AuxiliaryFilters, counts),
                GetHighLevelFilterItemList(Template.HighLevelFilters, counts),
                currentWorkflow, currentHighLevelWorkflow, CalcIsAuxiliaryFilterSelected(),
                CalcWorkflowLabels(currentHighLevelWorkflow, currentWorkflow),
                counts.GetCount(currentWorkflow));
        }

        private IList<string> CalcWorkflowLabels(WorkflowType? currentHighLevelWorkflow, WorkflowType currentWorkflow)
        {
            return currentHighLevelWorkflow.HasValue
                       ? new[]
                             {
                                 Template.AllItems[currentHighLevelWorkflow.Value].HighLevelLabel,
                                 Template.AllItems[currentWorkflow].Label
                             }
                       : new[] {Template.AllItems[currentWorkflow].Label};
        }

        private bool CalcIsAuxiliaryFilterSelected()
        {
            return Template.AuxiliaryFilters.Any(filter => filter == CurrentWorkflow.Type);
        }

        private IFiltersViewModel UpdateViewModel(IFiltersViewModel originalViewModel)
        {
            return originalViewModel.CurrentWorkflow != CurrentWorkflow.Type
                ? CalcViewModel()
                : originalViewModel;
        }

        private IWorkflowCounts CalcWorkflowCounts()
        {
            var workflowsToCount =
                new HashSet<WorkflowType>(
                    Template.AllItems
                        .Where(kvp => kvp.Value.Valid(Model))
                        .Select(kvp => kvp.Key));

            return Model.GetWorkflowCounts(workflowsToCount);
        }

        private IList<IFilterItem> GetFilterItemList(IEnumerable<WorkflowType> workflows, IWorkflowCounts counts)
        {
            return workflows
                .Select(workflow => Template.AllItems[workflow])
                .Where(template => template.Valid(Model) && template.Visible(Model))
                .Select(template => new FilterItem(template.Label, counts.GetCount(template.WorkflowType), template.WorkflowType))
                .ToArray();
        }

        private IList<IHighLevelFilterItem> GetHighLevelFilterItemList(IEnumerable<WorkflowType> workflows, IWorkflowCounts counts)
        {
            return workflows
                .Select(workflow => Template.AllItems[workflow])
                .Where(template => template.Valid(Model) && template.Visible(Model))
                .Select(template => new HighLevelFilterItem(template.HighLevelLabel, 
                    counts.GetCount(template.WorkflowType), template.WorkflowType, 
                    GetFilterItemList(template.Children, counts)))
                .ToArray();
        }

        private WorkflowType? CalcCurrentHighLevelWorkflow()
        {
            var currentTemplateItem = Template.AllItems[CurrentWorkflow.Type];
            return currentTemplateItem.HighLevelParents
                .Select(workflow => Template.AllItems[workflow])
                .Where(template => template.Valid(Model) && template.Visible(Model))
                .Select(template => (WorkflowType?) template.WorkflowType)
                .FirstOrDefault();
        }

        private void OnWorkflowClicked(WorkflowType wf)
        {
            SetCurrentWorkflow(wf, true);
        }

        private WorkflowType? CurrentWorkflowInner { get; set; }
        public IWorkflow CurrentWorkflow 
        {
            get
            {
                var workflowValue = CurrentWorkflowInner.HasValue
                                        ? CurrentWorkflowInner.Value
                                        : SetCurrentWorkflow(LoadWorkflowFromModel(), false);

                return Model.GetWorkflowRepository().GetWorkflow(workflowValue);
            }
            set
            {
                SetCurrentWorkflow(value == null ? (WorkflowType?)null : value.Type, false);
            }
        }

        private IFiltersViewModel CachedViewModel { get; set; }
        public IFiltersViewModel GetViewModel()
        {
            CachedViewModel = CachedViewModel == null ? CalcViewModel() : UpdateViewModel(CachedViewModel);
            return CachedViewModel;
        }

        private WorkflowType SetCurrentWorkflow(WorkflowType? workflow, bool clicked)
        {
            var workflowValue =
                (workflow == null || !IsValidWorkflow(workflow.Value))
                    ? GetDefaultWorkflow()
                    : workflow.Value;

            CurrentWorkflowInner = workflowValue;

            Model.SaveWorkflow(workflowValue, clicked);

            return workflowValue;
        }

        private WorkflowType LoadWorkflowFromModel()
        {
            return Model.LoadWorkflow() ?? Template.DefaultWorkflow(Model);
        }

        private WorkflowType GetDefaultWorkflow()
        {
            return Template.DefaultWorkflow(Model);
        }

        private bool IsValidWorkflow(WorkflowType type)
        {
            IFilterItemTemplate filterItem;
            return Template.AllItems.TryGetValue(type, out filterItem) 
                && filterItem.Valid(Model);
        }
    }
}