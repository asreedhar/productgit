using System;
using System.Collections.Generic;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IFilterItemTemplate
    {
        WorkflowType WorkflowType { get; }
        IList<WorkflowType> HighLevelParents { get; }
        IList<WorkflowType> Children { get; }
        Predicate<IFiltersModel> Visible { get; }
        Predicate<IFiltersModel> Valid { get; }
        string Label { get; }
        string HighLevelLabel { get; }
    }
}