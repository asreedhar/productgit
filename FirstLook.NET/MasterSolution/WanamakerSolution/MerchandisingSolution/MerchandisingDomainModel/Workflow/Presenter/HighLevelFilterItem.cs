using System.Collections.Generic;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    internal class HighLevelFilterItem : FilterItem, IHighLevelFilterItem
    {
        public HighLevelFilterItem(string label, int count, WorkflowType workflow, IList<IFilterItem> detailedFilters)
            : base(label, count, workflow)
        {
            DetailedFilters = detailedFilters;
        }

        public IList<IFilterItem> DetailedFilters { get; private set; }
    }
}