using System;
using System.Collections.Generic;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public class FilterItemTemplate : IFilterItemTemplate
    {
        private static readonly WorkflowType[] EmptyWorkflowList = new WorkflowType[0];

        public WorkflowType WorkflowType { get; private set; }
        public IList<WorkflowType> HighLevelParents { get; private set; }
        public IList<WorkflowType> Children { get; private set; }
        public Predicate<IFiltersModel> Visible { get; private set; }
        public Predicate<IFiltersModel> Valid { get; private set; }
        public string Label { get; private set; }
        public string HighLevelLabel { get; private set; }

        public FilterItemTemplate(WorkflowType workflowType, string label, 
                                  string highLevelLabel = null,
                                  IList<WorkflowType> highLevelParents = null, WorkflowType? highLevelParent = null, 
                                  IList<WorkflowType> children = null, 
                                  Predicate<IFiltersModel> isVisible = null, Predicate<IFiltersModel> isValid = null)
        {
            WorkflowType = workflowType;
            if (highLevelParent != null)
                HighLevelParents = new[] {highLevelParent.Value};
            else if (highLevelParents != null)
                HighLevelParents = highLevelParents;
            else
                HighLevelParents = EmptyWorkflowList;

            Children = children ?? EmptyWorkflowList;
            Visible = isVisible ?? (model => true);
            Valid = isValid ?? (model => true);
            Label = label;
            HighLevelLabel = highLevelLabel;
        }
    }
}