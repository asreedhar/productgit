using System.Collections.Generic;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IFiltersViewModel
    {
        IList<IFilterItem> AuxilaryFilters { get; }
        IList<IHighLevelFilterItem> HighLevelFilters { get; }

        bool IsAuxiliaryFilterSelected { get; }
        
        WorkflowType CurrentWorkflow { get; }
        WorkflowType? CurrentHighLevelWorkflow { get; }
        IList<string> CurrentWorkflowLabels { get; }
        int CurrentWorkflowCount { get; }
    }
}