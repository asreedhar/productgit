using System;
using System.Collections.Generic;
using System.Linq;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public class FilterTemplate : IFilterTemplate
    {
        private static readonly IList<WorkflowType> EmptyWorkflowList = new WorkflowType[0];

        public IList<WorkflowType> AuxiliaryFilters { get; private set; }
        public IList<WorkflowType> HighLevelFilters { get; private set; }
        public IDictionary<WorkflowType, IFilterItemTemplate> AllItems { get; private set; }
        public Func<IFiltersModel, WorkflowType> DefaultWorkflow { get; private set; }

        public FilterTemplate(IList<WorkflowType> auxFilters, IList<WorkflowType> highLevelFilters, 
                              IEnumerable<IFilterItemTemplate> itemTemplates,
                              Func<IFiltersModel, WorkflowType> defaultWorkflow)
        {
            AuxiliaryFilters = auxFilters ?? EmptyWorkflowList;
            HighLevelFilters = highLevelFilters ?? HighLevelFilters;
            AllItems = itemTemplates.ToDictionary(t => t.WorkflowType, t => t);
            DefaultWorkflow = defaultWorkflow;
        }
    }
}