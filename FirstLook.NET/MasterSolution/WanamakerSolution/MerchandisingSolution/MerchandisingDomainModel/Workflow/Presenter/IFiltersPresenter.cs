﻿namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IFiltersPresenter : IPresenter
    {
        IWorkflow CurrentWorkflow { get; set; }
        IFiltersViewModel GetViewModel();
    }
}