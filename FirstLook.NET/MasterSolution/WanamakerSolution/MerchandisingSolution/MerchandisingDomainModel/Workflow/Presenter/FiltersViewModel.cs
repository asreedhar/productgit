﻿using System.Collections.Generic;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    internal class FiltersViewModel : IFiltersViewModel
    {
        public IList<IFilterItem> AuxilaryFilters { get; private set; }

        public IList<IHighLevelFilterItem> HighLevelFilters { get; private set; }

        public bool IsAuxiliaryFilterSelected { get; private set; }

        public WorkflowType CurrentWorkflow { get; private set; }

        public WorkflowType? CurrentHighLevelWorkflow { get; private set; }

        public IList<string> CurrentWorkflowLabels { get; private set; }

        public int CurrentWorkflowCount { get; private set; }

        public FiltersViewModel(IList<IFilterItem> auxilaryFilters, IList<IHighLevelFilterItem> highLevelFilters, 
            WorkflowType currentWorkflow, WorkflowType? currentHighLevelWorkflow, bool isAuxiliaryFilterSelected, 
            IList<string> currentWorkflowLabels, int currentWorkflowCount)
        {
            AuxilaryFilters = auxilaryFilters;
            HighLevelFilters = highLevelFilters;
            CurrentWorkflow = currentWorkflow;
            CurrentHighLevelWorkflow = currentHighLevelWorkflow;
            IsAuxiliaryFilterSelected = isAuxiliaryFilterSelected;
            CurrentWorkflowLabels = currentWorkflowLabels;
            CurrentWorkflowCount = currentWorkflowCount;
        }
    }
}