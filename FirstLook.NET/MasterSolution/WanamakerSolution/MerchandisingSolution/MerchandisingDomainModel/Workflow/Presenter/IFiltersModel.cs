﻿using System.Collections.Generic;
using MAX.Entities;
using MAX.Entities.Filters;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IFiltersModel
    {
        WorkflowType? LoadWorkflow();
        void SaveWorkflow(WorkflowType? workflow, bool clicked);

        AutoApproveStatus GetAutoApproveStatus();
        UsedOrNewFilter GetUsedOrNewFilter();

        IWorkflowCounts GetWorkflowCounts(ISet<WorkflowType> workflowsToCount);
        IWorkflowRepository GetWorkflowRepository();
    }
}