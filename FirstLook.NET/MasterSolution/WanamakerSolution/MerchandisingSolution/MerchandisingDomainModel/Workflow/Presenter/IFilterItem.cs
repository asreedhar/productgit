using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IFilterItem
    {
        string Label { get; }
        int Count { get; }
        WorkflowType WorkflowType { get; }
    }
}