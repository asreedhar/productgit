using System;
using System.Collections.Generic;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IFilterTemplate
    {
        IList<WorkflowType> AuxiliaryFilters { get; }
        IList<WorkflowType> HighLevelFilters { get; }
        IDictionary<WorkflowType, IFilterItemTemplate> AllItems { get; }
        Func<IFiltersModel, WorkflowType> DefaultWorkflow { get; }
    }
}