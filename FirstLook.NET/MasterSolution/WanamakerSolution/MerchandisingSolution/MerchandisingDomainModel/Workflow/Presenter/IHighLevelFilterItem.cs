using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IHighLevelFilterItem : IFilterItem
    {
        IList<IFilterItem> DetailedFilters { get; }
    }
}