﻿using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    internal class FilterItem : IFilterItem
    {
        public string Label { get; private set; }
        public int Count { get; private set; }
        public WorkflowType WorkflowType { get; private set; }

        public FilterItem(string label, int count, WorkflowType wf)
        {
            Label = label;
            Count = count;
            WorkflowType = wf;
        }
    }
}