﻿using System;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow.Presenter
{
    public interface IFiltersView
    {
        Action<WorkflowType> WorkflowClicked { get; set; }
    }
}