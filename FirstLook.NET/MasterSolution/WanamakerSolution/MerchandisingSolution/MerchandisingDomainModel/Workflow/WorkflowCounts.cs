using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public class WorkflowCounts : IWorkflowCounts
    {
        private readonly IDictionary<WorkflowType, int> counts;

        public WorkflowCounts(
            IWorkflowRepository repository,
            IEnumerable<IInventoryData> inventory, 
            ISet<WorkflowType> workflowsToCount = null)
        {
            counts = Calculate(repository, workflowsToCount, inventory);
        }

        public int GetCount(WorkflowType wfType)
        {
            int item;
            counts.TryGetValue(wfType, out item);
            return item;
        }
        
        

        private class Accumulator
        {
            public WorkflowType Type;
            public Predicate<IInventoryData> Filter;
            public int Count;
        }

        public static IDictionary<WorkflowType, int> Calculate(
            IWorkflowRepository repository, 
            ISet<WorkflowType> workflowsToCount,
            IEnumerable<IInventoryData> inventoryList)
        {
            var accumulators = repository.Workflows
                .Where(wf => workflowsToCount == null || workflowsToCount.Contains(wf.Type))
                .Select(wf => new Accumulator {Filter = wf.Filter, Type = wf.Type})
                .ToArray();

            foreach (var accum in 
                from inventory in inventoryList
                from accumulator in accumulators
                where accumulator.Filter(inventory)
                select accumulator)
            {
                accum.Count++;
            }

            return accumulators.ToDictionary(acc => acc.Type, acc => acc.Count);
        }

        public static int CalculateTotal(
            IWorkflowRepository repository, 
            ISet<WorkflowType> workflowsToCount,
            IEnumerable<IInventoryData> inventoryList)
        {
            var workflows = repository.Workflows.Where( wf => workflowsToCount.Contains( wf.Type )).ToList();
            return inventoryList.Count( inventory => workflows.Any( workflow => workflow.Filter( inventory )));
        }
    }
}