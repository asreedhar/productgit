namespace FirstLook.Merchandising.DomainModel.Workflow.AutoOffline
{
    public interface IAutoOfflineProcessor
    {
        void ProcessForAllBusinessUnits(string userName = "AutoOffline");
        void ProcessForBusinessUnit(int businessUnitId, string userName = "AutoOffline");
        void SendProcessAutoOfflineMessage(int? businessUnitId = null);
    }
}