﻿using System.Globalization;
using Core.Messaging;
using FirstLook.Common.Core.Logging;
using Merchandising.Messages;
using Merchandising.Messages.AutoOffline;

namespace FirstLook.Merchandising.DomainModel.Workflow.AutoOffline
{
    internal class AutoOfflineProcessorTask : TaskRunner<ProcessAutoOfflineMessage>
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<AutoOfflineProcessorTask>();

        private IAutoOfflineProcessor Processor { get; set; }

        public AutoOfflineProcessorTask(IQueueFactory queueFactory, IAutoOfflineProcessor processor) 
            : base(queueFactory.CreateProcessAutoOffline())
        {
            Processor = processor;
        }

        public override void Process(ProcessAutoOfflineMessage message)
        {
            Log.InfoFormat("Processing AutoOffline. BusinessUnitId={0}",
                           message.BusinessUnitId.HasValue ? message.BusinessUnitId.Value.ToString(CultureInfo.InvariantCulture) : "\"all eligable\"");
            if (message.BusinessUnitId.HasValue)
                Processor.ProcessForBusinessUnit(message.BusinessUnitId.Value);
            else
                Processor.ProcessForAllBusinessUnits();
        }
    }
}