﻿using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages;
using Merchandising.Messages.AutoOffline;

namespace FirstLook.Merchandising.DomainModel.Workflow.AutoOffline
{
    public class AutoOfflineProcessor : IAutoOfflineProcessor
    {
        private IAdMessageSender AdMessageSender { get; set; }
        private IQueueFactory QueueFactory { get; set; }
        private IQueue<ProcessAutoOfflineMessage> Queue { get; set; }

        public AutoOfflineProcessor(IAdMessageSender adMessageSender, IQueueFactory queueFactory)
        {
            AdMessageSender = adMessageSender;
            QueueFactory = queueFactory;
            Queue = QueueFactory.CreateProcessAutoOffline();
        }

        public void ProcessForAllBusinessUnits(string userName = "AutoOffline")
        {
            var businessUnitIds = GetEligibleBusinessUnits();
            foreach (var businessUnitId in businessUnitIds)
            {
                ProcessForBusinessUnit(businessUnitId, userName);
            }
        }

        private static IEnumerable<int> GetEligibleBusinessUnits()
        {
            var ids = new List<int>();
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Merchandising.workflow.GetEligibleAutoOfflineBusinessUnits";

                using (var r = cmd.ExecuteReader())
                {
                    var businessUnitIdField = r.GetOrdinal("BusinessUnitID");
                    while (r.Read())
                        ids.Add(r.GetInt32(businessUnitIdField));
                }
            }

            return ids;
        }

        public void ProcessForBusinessUnit(int businessUnitId, string userName = "AutoOffline")
        {
            var inventoryItemsToChange = GetAutoOfflineActionItems(businessUnitId);
            foreach (var inventoryItem in inventoryItemsToChange)
            {
                if (inventoryItem.ShouldBeOffline)
                    InventoryDataSource.MoveOffline(businessUnitId, inventoryItem.InventoryId, userName, false);
                else
                    InventoryDataSource.MoveToReleasable(businessUnitId, inventoryItem.InventoryId, userName, false, AdMessageSender);
            }
        }

        private static IEnumerable<ActionItem> GetAutoOfflineActionItems(int businessUnitId)
        {
            var list = new List<ActionItem>();

            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Merchandising.workflow.GetAutoOfflineStatus";
                cmd.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);

                using (var r = cmd.ExecuteReader())
                {
                    var inventoryIdField = r.GetOrdinal("InventoryID");
                    var shouldBeOfflineField = r.GetOrdinal("ShouldBeOffline");
                    while (r.Read())
                    {
                        list.Add(
                            new ActionItem(
                                r.GetInt32(inventoryIdField),
                                r.GetBoolean(shouldBeOfflineField)
                                ));
                    }
                }
            }

            return list;
        }

        private class ActionItem
        {
            public int InventoryId { get; private set; }
            public bool ShouldBeOffline { get; private set; }

            public ActionItem(int inventoryId, bool shouldBeOffline)
            {
                InventoryId = inventoryId;
                ShouldBeOffline = shouldBeOffline;
            }
        }

        public void SendProcessAutoOfflineMessage(int? businessUnitId)
        {
            AdMessageSender.SendProcessAutoOfflineMessage(businessUnitId, GetType().FullName);
        }
    }
}