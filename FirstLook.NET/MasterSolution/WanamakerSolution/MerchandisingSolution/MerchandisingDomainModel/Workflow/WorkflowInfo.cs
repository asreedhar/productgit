using System;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Workflow
{
    public class WorkflowInfo : IWorkflow
    {
        public string Label { get; private set; }
        public WorkflowType Type { get; private set; }
        public MerchandisingBucket MerchandisingBucket { get; private set; }
        public Predicate<IInventoryData> Filter { get; private set; }

        public WorkflowInfo(string label, WorkflowType type, MerchandisingBucket merchandisingBucket, Predicate<IInventoryData> filter)
        {
            Label = label;
            Type = type;
            MerchandisingBucket = merchandisingBucket;
            Filter = filter;
        }
    }
}