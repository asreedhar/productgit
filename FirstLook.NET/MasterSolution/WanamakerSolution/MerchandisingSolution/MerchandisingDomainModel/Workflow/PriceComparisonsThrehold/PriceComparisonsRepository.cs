﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Logging;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Workflow.PriceComparisonsThrehold
{
    public class PriceComparisonsRepository : IPriceComparisonsRepository
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<PriceComparisonsRepository>();

        public IPriceComparisonThresholds GetPriceComparisonsAndDealerThresholds(int businessUnitId, int inventoryType)
        {

            using (MiniProfiler.Current.Step("GetPriceComparisonThresholdsForBusinessUnit"))
            {
                if (Log.IsInfoEnabled)
                    Log.Info("Starting to execute GetPriceComparisonThresholdsForBusinessUnit");

                var t = DateTimeOffset.UtcNow;

                var priceComparisonsThreshold = ReadPriceComparisonThrehoslds(businessUnitId, inventoryType);

                if (Log.IsInfoEnabled)
                    Log.InfoFormat("Retrieved Thresholds for . ElapsedMs={0:0.0} TotalInventoryCount={1}",
                        (DateTimeOffset.UtcNow - t).TotalMilliseconds, priceComparisonsThreshold.GetCount().Count);

                return priceComparisonsThreshold;
            }
        }

        private IPriceComparisonThresholds ReadPriceComparisonThrehoslds(int businessUnitId, int inventoryType)
        {
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Merchandising.builder.getPriceComparisonsAndDealerThresholds";

                cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("@InventoryType", inventoryType, DbType.Int32);

                using (var r = cmd.ExecuteReader())
                {
                    var inventoryIdColumn = r.GetOrdinal("InventoryID");
                    var listpriceColumn = r.GetOrdinal("ListPrice");
                    var msrpColumn = r.GetOrdinal("MSRP");
                    var kbbColumn = r.GetOrdinal("KBB");
                    var nadaColumn = r.GetOrdinal("NADA");
                    var edmundsColumn = r.GetOrdinal("EdmundsTMV");
                    var marketAvgColumn = r.GetOrdinal("PingMarketAvg");
                    var msrpDollarColumn = r.GetOrdinal("MSRPDollarT");
                    var kbbDollarColumn = r.GetOrdinal("kbbDollarT");
                    var nadaDollarColumn = r.GetOrdinal("NadaDollarT");
                    var edmundsDollarColumn = r.GetOrdinal("EdmundsDollarT");
                    var marketAvgDollarColumn = r.GetOrdinal("MIPDollarT");
                    var comparisonColor = r.GetOrdinal("ComparisonColor");
                    var thresholdProofPoint = r.GetOrdinal("ThresholdProofPoint");
                    var kbbConsumerValueColumn = r.GetOrdinal("KbbConsumerValue");


                    var list = new List<IPriceComparisonThreshold>();
                    while (r.Read())
                    {
                        list.Add(new PriceComparisonThreshold(
                                                                    r.GetInt32(inventoryIdColumn),
                                                                    r.GetInt32(listpriceColumn),
                                                                    r.GetInt32(msrpColumn),
                                                                    r.GetInt32(kbbColumn),
                                                                    r.GetInt32(nadaColumn),
                                                                    r.GetInt32(edmundsColumn),
                                                                    r.GetInt32(marketAvgColumn),
                                                                    r.GetInt32(msrpDollarColumn),
                                                                    r.GetInt32(kbbDollarColumn),
                                                                    r.GetInt32(nadaDollarColumn),
                                                                    r.GetInt32(edmundsDollarColumn),
                                                                    r.GetInt32(marketAvgDollarColumn),
                                                                    r.GetInt32(comparisonColor),
                                                                    r.GetInt32(thresholdProofPoint),
                                                                    r.GetInt32(kbbConsumerValueColumn)

                                                            ));
                    }

                    return new PriceComparisonThrehsolds(list);
                }
            }
        }

    }



}
