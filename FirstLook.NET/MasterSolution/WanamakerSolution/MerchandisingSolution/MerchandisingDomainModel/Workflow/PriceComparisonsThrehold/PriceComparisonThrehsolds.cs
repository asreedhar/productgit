﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.Workflow.PriceComparisonsThrehold
{
    public class PriceComparisonThrehsolds:IPriceComparisonThresholds
    {
        private IList<IPriceComparisonThreshold> priceComparisonThreshold { get; set; }
        public PriceComparisonThrehsolds(IList<IPriceComparisonThreshold> pricecomparisonT)
        {
            priceComparisonThreshold = pricecomparisonT;
        }

        public void SetFavourableOrNot(IEnumerable<InventoryData> retList)
        {
             foreach(InventoryData inv in retList)
             {
                 int favourableCount=0;
                 IPriceComparisonThreshold IP=priceComparisonThreshold.Where(pt=>pt.inventoryId==inv.InventoryID).FirstOrDefault();
                 if(IP==null){
                     inv.isFavourable=false;
                 }
                 else
                 {
                    // New logic for Price Comparisons based on Threshold Color setting for the dealer
                     if (IP.ThresholdProofPoint == 0)
                     {
                         if (IP.ComparisonColor == 0)
                         {
                             inv.isFavourable = true;
                         }

                     }
                     else if (IP.ThresholdProofPoint == 1)
                     {
                         if (IP.ComparisonColor == 0 || IP.ComparisonColor == 2)
                         {
                             inv.isFavourable = true;
                         }

                     }
                 }
             }
        }

        public IList<IPriceComparisonThreshold> GetCount()
        {
            return priceComparisonThreshold;
        }
 
    }
}
