﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Workflow.PriceComparisonsThrehold
{
    public interface IPriceComparisonThreshold
    {
        int inventoryId { get; }
        int listPrice { get; }
        int Kbb { get; }
        int Nada { get; }
        int EdmundsTmv { get; }
        int PingMarketAvg { get; }
        int msrp { get; }
        int msrpDollarT { get; }
        int kbbDollarT { get; }
        int nadaDollarT { get; }
        int edmundsTmvDollarT { get; }
        int pingMarketAvgT { get; }
        int ComparisonColor { get; }
        int ThresholdProofPoint { get; }
        int KbbConsumerValue { get; }


    }
}
