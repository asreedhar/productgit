﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;

namespace FirstLook.Merchandising.DomainModel.Workflow.PriceComparisonsThrehold
{
    public interface IPriceComparisonThresholds
    {
        void SetFavourableOrNot(IEnumerable<InventoryData> retList);
        IList<IPriceComparisonThreshold> GetCount();

    }
}
