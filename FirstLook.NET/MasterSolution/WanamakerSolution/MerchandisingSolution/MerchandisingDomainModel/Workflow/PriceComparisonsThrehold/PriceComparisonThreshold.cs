﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Workflow.PriceComparisonsThrehold
{
    [Serializable]
    public class PriceComparisonThreshold : IPriceComparisonThreshold
    {


        public PriceComparisonThreshold(int iid, int listprice, int msrp, int kbb, int nada, int edmunds, int mktavg, int msrpd, int kbbd, int nadad, int edmundsd, int mktavgd, int comparisonClr, int thresholdPp, int kbbconsumervalue)
        {
            inventoryId = iid;
            listPrice = listprice;
            this.msrp = msrp;
            this.Kbb = kbb;
            this.Nada = nada;
            EdmundsTmv = edmunds;
            PingMarketAvg = mktavg;
            msrpDollarT = msrpd;
            kbbDollarT = kbbd;
            nadaDollarT = nadad;
            edmundsTmvDollarT = edmundsd;
            pingMarketAvgT = mktavgd;
            ThresholdProofPoint = thresholdPp;
            ComparisonColor = comparisonClr;
            KbbConsumerValue = kbbconsumervalue;

        }

        public int ThresholdProofPoint { get; set; }

        public int ComparisonColor { get; set; }

        public int inventoryId
        {
            get;
            set;
        }

        public int listPrice
        {
            get;
            set;
        }

        public int Kbb
        {
            get;
            set;
        }

        public int Nada
        {
            get;
            set;
        }

        public int EdmundsTmv
        {
            get;
            set;
        }

        public int PingMarketAvg
        {
            get;
            set;
        }

        public int msrp
        {
            get;
            set;
        }

        public int msrpDollarT
        {
            get;
            set;
        }

        public int kbbDollarT
        {
            get;
            set;
        }

        public int nadaDollarT
        {
            get;
            set;
        }

        public int edmundsTmvDollarT
        {
            get;
            set;
        }

        public int pingMarketAvgT
        {
            get;
            set;
        }

        public int minFavourableT
        {
            get;
            set;
        }


        public int KbbConsumerValue
        {
            get;
            set;
        }
    }
}
