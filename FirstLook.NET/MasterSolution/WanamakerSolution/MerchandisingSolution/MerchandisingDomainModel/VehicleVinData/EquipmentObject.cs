using System;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    // An Object that will hold an equipment type. i.e. Engine Type (V6, V8) and its availability
    // i.e. Standard or Optional

    // Object made Serializable so that I can store it in ViewState
    [Serializable]
    public class EquipmentObject : ISerializable
    {
        public static readonly EquipmentObject NotSpecified = new EquipmentObject(-1); 

        public string AttributeType { get; private set; }

        public string Category { get; private set; }

        public string Status { get; private set; }

        public bool IsStandard { get; private set; }

        public bool IsOptional { get; private set; }

        public bool IsNeither { get; private set; }

        public int TypeID { get; private set; }

        // Making my EquipmentObject serializable so that I can store it in ViewState
        public EquipmentObject(SerializationInfo info, StreamingContext ctxt)
        {
            TypeID = (int) info.GetValue("TypeID", typeof (int));
            AttributeType = (String) info.GetValue("attributeType", typeof (string));
            Category = (String) info.GetValue("category", typeof (string));
            Status = (String) info.GetValue("status", typeof (string));
            IsOptional = (Boolean) info.GetValue("optional", typeof (bool));
            IsStandard = (Boolean) info.GetValue("standard", typeof (bool));
            IsNeither = (Boolean) info.GetValue("neither", typeof (bool));
        }

        // Making my EquipmentObject Deserializable so I can retrieve it from ViewState.

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("TypeID", TypeID);
            info.AddValue("attributeType", AttributeType);
            info.AddValue("category", Category);
            info.AddValue("status", Status);
            info.AddValue("optional", IsOptional);
            info.AddValue("standard", IsStandard);
            info.AddValue("neither", IsNeither);
        }

        #endregion

        #region Constructors

        public EquipmentObject(int typeID)
        {
            TypeID = typeID;
        }

        // Constructor that stores the typeID and availability and category
        public EquipmentObject(int typeID, string availability, string category)
        {
            TypeID = typeID;
            Category = category;
            SetStyleAvailability(availability);
        }

        #endregion

        // Constructor that stores the type of attribute, category, and availability

        private void SetStyleAvailability(string availability)
        {
            if (availability.Equals("Standard"))
            {
                IsStandard = true;
                IsOptional = false;
                IsNeither = false;
                Status = "Standard";
            }
            else if (availability.Equals("Optional"))
            {
                IsOptional = true;
                IsStandard = false;
                IsNeither = false;
                Status = "Optional";
            }
            else
            {
                IsNeither = true;
                IsStandard = false;
                IsOptional = false;
                Status = "Neither";
            }
        }

        public override string ToString()
        {
            return AttributeType;
        }
    }
}