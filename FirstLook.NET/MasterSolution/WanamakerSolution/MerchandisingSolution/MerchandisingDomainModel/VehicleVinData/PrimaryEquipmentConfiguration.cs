using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    [Serializable]
    public class PrimaryEquipmentConfiguration
    {
        public EquipmentObject Engine { get; private set; }
        public EquipmentObject Transmission { get; private set; }
        public EquipmentObject Drivetrain { get; private set; }
        public EquipmentObject Fuel { get; private set; }

        public PrimaryEquipmentConfiguration(EquipmentObject engine, EquipmentObject transmission, 
            EquipmentObject driveTrain, EquipmentObject fuel)
        {
            Engine = engine ?? EquipmentObject.NotSpecified;
            Transmission = transmission ?? EquipmentObject.NotSpecified;
            Drivetrain = driveTrain ?? EquipmentObject.NotSpecified;
            Fuel = fuel ?? EquipmentObject.NotSpecified;
        }

        public static readonly PrimaryEquipmentConfiguration Default = 
            new PrimaryEquipmentConfiguration(
                EquipmentObject.NotSpecified, 
                EquipmentObject.NotSpecified,
                EquipmentObject.NotSpecified,
                EquipmentObject.NotSpecified);

        public static PrimaryEquipmentConfiguration FetchVehicleConfig(int businessUnitId, int inventoryId)
        {
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandText = "builder.NewCheckMapping";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.AddRequiredParameter("@inventoryID", inventoryId, DbType.Int32);
                cmd.AddRequiredParameter("@businessUnitID", businessUnitId, DbType.Int32);

                using(var r = cmd.ExecuteReader())
                {
                    var engineField = r.GetOrdinal("EngineTypeCategoryID");
                    var transmissionField = r.GetOrdinal("TransmissionTypeCategoryID");
                    var driveTrainField = r.GetOrdinal("DriveTrainCategoryID");
                    var fuelField = r.GetOrdinal("FuelCategoryID");

                    PrimaryEquipmentConfiguration data = null;

                    if(r.Read())
                    {
                        data = new PrimaryEquipmentConfiguration(
                            new EquipmentObject(r.GetInt32(engineField)),
                            new EquipmentObject(r.GetInt32(transmissionField)),
                            new EquipmentObject(r.GetInt32(driveTrainField)),
                            new EquipmentObject(r.GetInt32(fuelField)));
                    }

                    if(r.Read())
                        throw new InvalidOperationException("Too many rows returned.");

                    return data;
                }
            }
        }
    }
}