using System.Data;

namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    // My Upsert Object. This Object will contain the data for a record to be either Updated
    // or inserted into a database. 
    public class UpsertVehicleConfiguration
    {
        #region instance variables

        private readonly int _driveTrainCategoryID;
        private readonly int _engineTypeCategoryID;
        private readonly int _fuelCategoryID;
        private readonly int _transmissionTypeCategoryID;
        private readonly int _businessUnitID;
        private readonly int _inventoryID;
        private readonly string _memberLogin;
        private int _chromeStyleID;

        #endregion

        // the fields of my object that will be stored in the database

        // Default Constructor
        public UpsertVehicleConfiguration()
        {
        }

        // Intializing all fields of my UpsertObject
        public UpsertVehicleConfiguration(int csid, int invID, int bID, int eID, int tID, int dtID, int fID,
                                          string member)
        {
            _chromeStyleID = csid;
            _inventoryID = invID;
            _businessUnitID = bID;
            _engineTypeCategoryID = eID;
            _transmissionTypeCategoryID = tID;
            _driveTrainCategoryID = dtID;
            _fuelCategoryID = fID;
            _memberLogin = member;
        }

        // Sending my object to the database by way of a stored procedure.
        public void MapEquipment()
        {
            // This stored procedure will either update a record if it already exists in the
            // builder.NewEquipmentMapping table, if not then it will insert it.
            const string STORED_PROC = "builder.NewEquipmentMappingUpsert";

            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = STORED_PROC;
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "@inventoryID", _inventoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@businessUnitID", _businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@EngineTypeCategoryID", _engineTypeCategoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@TransmissionTypeCategoryID", _transmissionTypeCategoryID,
                                                  DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@DriveTrainCategoryID", _driveTrainCategoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@FuelCategoryID", _fuelCategoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@editedBy", _memberLogin, DbType.String);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateEngine(int inventoryID, int businessUnitID, int engineId)
        {
            const string UPDATE = "builder.UpdateEngineMapUpsert";

            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = UPDATE;
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "@inventoryID", inventoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@businessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@EngineTypeCategoryID", engineId, DbType.Int32);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateTrans(int inventoryID, int businessUnitID, int transId)
        {
            const string UPDATETRANS = "builder.UpdateTransMapUpsert";

            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = UPDATETRANS;
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "@inventoryID", inventoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@businessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@TransmissionTypeCategoryID", transId, DbType.Int32);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateDrivetrain(int inventoryID, int businessUnitID, int drivetrainId)
        {
            const string UPDATEDRIVETRAIN = "builder.UpdateDriveMapUpsert";

            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = UPDATEDRIVETRAIN;
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "@inventoryID", inventoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@businessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@DriveTrainCategoryID", drivetrainId, DbType.Int32);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateFuelsystem(int inventoryID, int businessUnitID, int fuelsystemId)
        {
            const string UPDATEFUEL = "builder.UpdateFuelMapUpsert";

            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = UPDATEFUEL;
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "@inventoryID", inventoryID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@businessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@FuelCategoryID", fuelsystemId, DbType.Int32);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateEquipmentOption(string type, int inventoryID, int businessUnitID, int categoryID)
        {
            switch (type)
            {
                case "Engine":
                    UpdateEngine(inventoryID, businessUnitID, categoryID);
                    return;
                case "Transmission":
                    UpdateTrans(inventoryID, businessUnitID, categoryID);
                    return;
                case "Drivetrain":
                    UpdateDrivetrain(inventoryID, businessUnitID, categoryID);
                    return;
                case "Fuel":
                    UpdateFuelsystem(inventoryID, businessUnitID, categoryID);
                    return;
                default:
                    return;
            }
        }
    }
}