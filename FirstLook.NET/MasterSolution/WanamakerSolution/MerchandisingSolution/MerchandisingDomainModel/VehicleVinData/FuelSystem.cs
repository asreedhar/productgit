using System;
using System.Data;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    [Serializable]
    public class FuelSystem : IEquipmentOption, ISerializable
    {
        public FuelSystem(IDataReader reader)
        {
            CategoryId = reader.GetInt32(reader.GetOrdinal("CategoryID"));
            CategoryTypeFilter = reader.GetString(reader.GetOrdinal("CategoryTypeFilter"));
            Description = reader.GetString(reader.GetOrdinal("UserFriendlyName"));
            IsStandard = reader.GetBoolean(reader.GetOrdinal("IsStandard"));
            IsOptional = reader.GetBoolean(reader.GetOrdinal("IsOptional"));

            //check either one of isStandard or isOptional but not both
        }

        public FuelSystem()
        {
            CategoryId = -1;
            CategoryTypeFilter = "Fuel System";
            Description = "Fuel System Not Selected";
            IsOptional = false;
            IsStandard = false;
        }

        public FuelSystem(SerializationInfo info, StreamingContext ctxt)
        {
            CategoryId = (int) info.GetValue("_categoryId", typeof (int));
            CategoryTypeFilter = (String) info.GetValue("_categoryTypeFilter", typeof (string));
            Description = (String) info.GetValue("_description", typeof (string));
            IsStandard = (Boolean) info.GetValue("_isStandard", typeof (bool));
            IsOptional = (Boolean) info.GetValue("_isOptional", typeof (bool));
        }

        #region IEquipmentOption Members

        public int CategoryId { get; private set; }

        public string CategoryTypeFilter { get; private set; }

        public string Description { get; set; }

        public bool IsStandard { get; private set; }

        public bool IsOptional { get; private set; }

        #endregion

        // Making my EquipmentObject Deserializable so I can retrieve it from ViewState.

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("_categoryId", CategoryId);
            info.AddValue("_categoryTypeFilter", CategoryTypeFilter);
            info.AddValue("_description", Description);
            info.AddValue("_isStandard", IsStandard);
            info.AddValue("_isOptional", IsOptional);
        }

        #endregion

        public override string ToString()
        {
            return Description;
        }
    }
}