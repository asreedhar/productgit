using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    [Serializable]
    public class FuelSystemCollection : EquipmentCollectionBaseClass, IList<IEquipmentOption>, ISerializable
    {
        /// <summary>
        /// List that contains the FuelSystems.
        /// </summary>
        private readonly IList<IEquipmentOption> _list = new List<IEquipmentOption>();

        #region ICollection<T> methods

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return _list.IsReadOnly; }
        }

        public void Add(IEquipmentOption item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(IEquipmentOption item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(IEquipmentOption[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool Remove(IEquipmentOption item)
        {
            return _list.Remove(item);
        }

        #endregion

        #region IList<T> methods

        public IEquipmentOption this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }

        public int IndexOf(IEquipmentOption item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, IEquipmentOption item)
        {
            _list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        #endregion

        #region IEnumerable<T> methods

        public IEnumerator<IEquipmentOption> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        #region IEnumerable methods

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        public FuelSystemCollection()
        {
        }

        public FuelSystemCollection(SerializationInfo info, StreamingContext ctxt)
        {
            _list = (List<IEquipmentOption>)info.GetValue("_list", typeof(List<IEquipmentOption>));
        }

        // Making my EquipmentObject Deserializable so I can retrieve it from ViewState.

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("_list", _list);
        }

        #endregion

        public int ContainsFuelSystem(int categoryID)
        {
            int found = -1;
            for (int i = 0; i < Count; i++)
            {
                if (this[i].CategoryId == categoryID)
                {
                    found = i;
                }
            }
            return found;
        }

        public override IEquipmentOption GetStandard()
        {
            return this.LastOrDefault(fs => fs.IsStandard);
        }

        public override string GetDefaultCallToAction()
        {
            return "Select Fuel System...";
        }

        public IEquipmentOption GetFuelSystem(int categoryID)
        {
            return this.LastOrDefault(fs => fs.CategoryId == categoryID);
        }

        public static FuelSystemCollection GetFuelSystems(int chromeStyleId)
        {
            const string QUERY = @"SELECT DISTINCT 
                    CS.CategoryTypeFilter, 
                    CS.UserFriendlyName, 
                    CS.CategoryID, 
                    CAST(CASE SGE.StyleAvailability
                        WHEN 'Standard' THEN 1
                        ELSE 0
                    END AS BIT) IsStandard,
                    CAST(CASE SGE.StyleAvailability
                        WHEN 'Optional' THEN 1
                        ELSE 0
                    END AS BIT) IsOptional
                FROM VehicleCatalog.Chrome.Categories CS
                LEFT JOIN VehicleCatalog.Chrome.StyleGenericEquipment SGE
                    ON SGE.CountryCode = CS.CountryCode
                    AND SGE.ChromeStyleID = @ChromeStyleID
                    AND SGE.CategoryID = CS.CategoryID
                WHERE CS.CountryCode = 1
                AND CS.CategoryTypeFilter = 'Fuel system'
                ORDER BY CS.UserFriendlyName";


            using (IDbConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = QUERY;
                    cmd.CommandType = CommandType.Text;
                    cmd.AddRequiredParameter("ChromeStyleID", chromeStyleId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var fuelSystemCollection = new FuelSystemCollection();
                        while (reader.Read())
                        {
                            fuelSystemCollection.Add(new FuelSystem(reader));
                        }
                        return fuelSystemCollection;
                    }
                }
            }
        }
    }
}