using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;

namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    [Serializable]
    public class DrivetrainCollection : EquipmentCollectionBaseClass, IList<IEquipmentOption>, ISerializable
    {
        /// <summary>
        /// List that contains the Drivetrains.
        /// </summary>
        private readonly IList<IEquipmentOption> _list = new List<IEquipmentOption>();

        #region ICollection<T> methods

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return _list.IsReadOnly; }
        }

        public void Add(IEquipmentOption item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(IEquipmentOption item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(IEquipmentOption[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool Remove(IEquipmentOption item)
        {
            return _list.Remove(item);
        }

        #endregion

        #region IList<T> methods

        public IEquipmentOption this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }

        public int IndexOf(IEquipmentOption item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, IEquipmentOption item)
        {
            _list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        #endregion

        #region IEnumerable<T> methods

        public IEnumerator<IEquipmentOption> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        #region IEnumerable methods

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        public DrivetrainCollection()
        {
        }

        public DrivetrainCollection(SerializationInfo info, StreamingContext ctxt)
        {
            _list = (List<IEquipmentOption>)info.GetValue("_list", typeof(List<IEquipmentOption>));
        }

        // Making my EquipmentObject Deserializable so I can retrieve it from ViewState.

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("_list", _list);
        }

        #endregion

        public int ContainsDrivetrain(int categoryID)
        {
            int found = -1;
            for (int i = 0; i < Count; i++)
            {
                if (this[i].CategoryId == categoryID)
                {
                    found = i;
                }
            }
            return found;
        }

        public IEquipmentOption GetDrivetrain(int categoryID)
        {
            return this.LastOrDefault(dt => dt.CategoryId == categoryID);
        }

        public static DrivetrainCollection GetDrivetrains(int chromeStyleId)
        {
            const string storedProc = "chrome.getDrivetrainsByStyleId";

            using (var conn = Database.GetConnection(Database.MerchandisingDatabase, false))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    {
                        cmd.CommandText = storedProc;
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.AddRequiredParameter("ChromeStyleID", chromeStyleId, DbType.Int32);

                        using (var reader = cmd.ExecuteReader())
                        {
                            var drivetrainCollection = new DrivetrainCollection();
                            while (reader.Read())
                            {
                                drivetrainCollection.Add(new Drivetrain(reader));
                            }
                            return drivetrainCollection;
                        }
                    }
                }
            }
        }

        public override IEquipmentOption GetStandard()
        {
            return this.LastOrDefault(dt => dt.IsStandard);
        }

        public override string GetDefaultCallToAction()
        {
            return "Select Drivetrain...";
        }
    }
}