namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    public abstract class EquipmentCollectionBaseClass
    {
        public abstract IEquipmentOption GetStandard();
        public abstract string GetDefaultCallToAction();
    }
}
