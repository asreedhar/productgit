using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    public class EquipmentCategoryIds
    {
        public EquipmentCategoryIds(int chromeStyle)
        {
            AllEquipmentObjects = new List<EquipmentObject>();
            ChromeStyleID = chromeStyle;
            Engines = EngineCollection.GetEngines(ChromeStyleID);
            Transmissions = TransmissionCollection.GetTransmissions(ChromeStyleID);
            Drivetrains = DrivetrainCollection.GetDrivetrains(ChromeStyleID);
            Fuelsystems = FuelSystemCollection.GetFuelSystems(ChromeStyleID);
            PopulateListWithEquipmentObjects();
        }

        public EngineCollection Engines { get; private set; }

        public TransmissionCollection Transmissions { get; private set; }

        public DrivetrainCollection Drivetrains { get; private set; }

        public FuelSystemCollection Fuelsystems { get; private set; }

        public int ChromeStyleID { get; private set; }

        public List<EquipmentObject> AllEquipmentObjects { get; private set; }

        public void PopulateListWithEquipmentObjects()
        {
            foreach (Engine obj in Engines)
            {
                AllEquipmentObjects.Add(new EquipmentObject(obj.CategoryId, "", obj.CategoryTypeFilter));
            }

            foreach (Transmission obj in Transmissions)
            {
                AllEquipmentObjects.Add(new EquipmentObject(obj.CategoryId, "", obj.CategoryTypeFilter));
            }

            foreach (Drivetrain obj in Drivetrains)
            {
                AllEquipmentObjects.Add(new EquipmentObject(obj.CategoryId, "", obj.CategoryTypeFilter));
            }

            foreach (FuelSystem obj in Fuelsystems)
            {
                AllEquipmentObjects.Add(new EquipmentObject(obj.CategoryId, "", obj.CategoryTypeFilter));
            }
        }

        public static string FindTypeByCategoryID(int categoryId)
        {
            using (IDataConnection conn = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                conn.Open();

                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText =
                        "SELECT ct.UserFriendlyName as description, ch.sequence, '' as categoryList, ch.categoryheader as header " +
                        "FROM chrome.categories as ct INNER JOIN chrome.categoryHeaders as ch ON ch.categoryHeaderID = ct.categoryHeaderID " +
                        "WHERE ct.CountryCode = 1 AND ch.CountryCode = 1 AND ct.CategoryID IN (@categoryId) " +
                        "ORDER BY ch.sequence, description";
                    cmd.CommandType = CommandType.Text;
                    Database.AddRequiredParameter(cmd, "@categoryId", categoryId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return reader.GetString(reader.GetOrdinal("header"));
                        }
                    }
                }
            }
            return "";
        }

        public bool ContainsCategoryId(int categoryId)
        {
            bool contains = false;
            foreach (EquipmentObject equipment in AllEquipmentObjects)
            {
                if (equipment.TypeID == categoryId)
                {
                    contains = true;
                }                
            }
            return contains;
        }

        public EquipmentObject GetByCategoryId(int categoryId)
        {
            return AllEquipmentObjects.Select(equipment => equipment.TypeID == categoryId ? equipment : null).FirstOrDefault();
        }

        public EquipmentCollectionBaseClass GetCollectionByType(string type)
        {
            switch (type)
            {
                case "Engine":
                    return Engines;
                case "Transmission":
                    return Transmissions;
                case "Drivetrain":
                    return Drivetrains;
                case "Fuel":
                    return Fuelsystems;
                default:
                    return null;
            }
        }
    }
}