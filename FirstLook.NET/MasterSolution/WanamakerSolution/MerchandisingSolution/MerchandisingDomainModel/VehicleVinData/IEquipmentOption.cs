namespace FirstLook.Merchandising.DomainModel.VehicleVinData
{
    public interface IEquipmentOption
    {
        int CategoryId{ get; }
        string CategoryTypeFilter{ get;  }
        string Description{ get;  }
        bool IsStandard{ get;  }
        bool IsOptional { get; }
    }
}
