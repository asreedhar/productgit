using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel
{
    public class DealerHelper
    {
        public static bool IsDealerSetup(int businessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                        IF EXISTS(select 1 from settings.Merchandising Where businessUnitId = @BusinessUnitId)
                        BEGIN
                            SELECT 1
                        END
                        ELSE
                        BEGIN
                            SELECT 0
                        END
                        ";
                    cmd.CommandType = CommandType.Text;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    return Convert.ToBoolean((int)cmd.ExecuteScalar());
                }
            }
        }
    }
}
