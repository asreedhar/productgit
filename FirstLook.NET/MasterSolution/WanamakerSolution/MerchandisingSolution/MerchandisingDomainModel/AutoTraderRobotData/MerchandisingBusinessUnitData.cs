using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.AutoTraderRobotData
{
    public class MerchandisingBusinessUnitData
    {
        public int BusinessUnitId { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }

        private MerchandisingBusinessUnitData(int buid, string username, string password)
        {
            BusinessUnitId = buid;
            Username = username;
            Password = password;
        }

        #region Data Access Factory Methods

        /// <summary>
        /// Gets a list of all Business Units that are set up in the MAX Merchandising application.
        /// </summary>
        /// <returns>List of all Business Units that are set up in the MAX Merchandising application.</returns>
        public static List<Int32> GetAllMerchandisingBusinessUnits()
        {
            List<Int32> businessUnitCollection = new List<int>();

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString);
            con.Open();

            SqlCommand select = con.CreateCommand();
            select.CommandText = "SELECT businessUnitID FROM settings.Merchandising";
            
            SqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                businessUnitCollection.Add(Convert.ToInt32(reader["businessUnitID"]));
            }
            reader.Close();
            con.Close();

            return businessUnitCollection;
        }

        /// <summary>
        /// Gets the specified Business Unit's username and password for AutoTrader.
        /// </summary>
        /// <returns></returns>
        public static MerchandisingBusinessUnitData GetAutoTraderLogin(int businessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Release.ThirdPartyLogin#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "DestinationId", 2, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new MerchandisingBusinessUnitData(
                                businessUnitId,
                                reader.GetString(reader.GetOrdinal("userName")),
                                reader.GetString(reader.GetOrdinal("password"))
                                );
                        }

                        throw new ApplicationException("Login information invalid for BusinessUnitID (" +
                                                       businessUnitId + ") to PerformanceSource (AutoTrader)");
                    }
                }
            }
        }

        /// <summary>
        /// Gets a list of business units whose AutoTrader credentials are currently marked invalid (failed login).
        /// </summary>
        /// <returns></returns>
        public static List<Int32> GetAutoTraderFailedLogins()
        {
			throw new NotSupportedException("This data has moved to settings.DealerListingSites. Please update this method.");
		
            /*List<Int32> businessUnitCollection = new List<int>();

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString);
            con.Open();

            SqlCommand select = con.CreateCommand();
            select.CommandText = "SELECT businessUnitID FROM merchandising.AutoTraderFailedLogins";
            
            SqlDataReader reader = select.ExecuteReader();
            
            while (reader.Read())
            {
                businessUnitCollection.Add(Convert.ToInt32(reader["businessUnitID"]));
            }
            reader.Close();
            con.Close();

            return businessUnitCollection;*/
        }

        #endregion

        /// <summary>
        /// Adds a log to the database that the specified Business Unit's AutoTrader.com credentials are invalid (failed login).
        /// </summary>
        /// <param name="buid"></param>
        public static void LogInFailed(int buid)
        {
            const string STORED_PROC = "merchandising.AutoTraderFailed#Upsert";
            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = STORED_PROC;
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "@businessUnitID", buid, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@dateFailed", DateTime.Now, DbType.DateTime);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
