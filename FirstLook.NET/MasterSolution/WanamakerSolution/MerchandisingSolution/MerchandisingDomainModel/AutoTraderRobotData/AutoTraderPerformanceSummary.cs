using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.AutoTraderRobotData
{
    public class AutoTraderPerformanceSummary : AutoTraderRobot
    {
        #region Instance Variables

        #endregion

        #region Properties

        public DateTime Month { get; set; }

        public int? CallsToDealership { get; set; }

        public int? ViewedWebsiteOrInventory { get; set; }

        public int? SecureCreditApplications { get; set; }

        public int? UsedVehiclesSearched { get; set; }

        public int? UsedDetailPagesViewed { get; set; }

        public int? UsedViewedMaps { get; set; }

        public int? UsedEmailsToDealership { get; set; }

        public int? UsedPrintableAds { get; set; }

        public int? NewVehiclesSearched { get; set; }

        public int? NewDetailPagesViewed { get; set; }

        public int? NewViewedMaps { get; set; }

        public int? NewEmailsToDealership { get; set; }

        public int? NewPrintableAds { get; set; }

        public int? SearchImpressions { get; set; }

        public int? ClickThrus { get; set; }

        public int? ViewedMaps { get; set; }

        public int? EmailsToDealership { get; set; }

        public int? AskedForMoreDetails { get; set; }

        public int? UsedAvgDailyVehicles { get; set; }

        public decimal? UsedPctWithPrice { get; set; }

        public decimal? UsedPctWithComments { get; set; }

        public decimal? UsedPctWithPhotos { get; set; }

        public decimal? UsedPctMultiplePhotos { get; set; }

        public decimal? UsedPctSinglePhoto { get; set; }

        public string UsedPrimarySource { get; set; }

        public int? NewAvgDailyVehicles { get; set; }

        public decimal? NewPctWithPrice { get; set; }

        public decimal? NewPctWithComments { get; set; }

        public decimal? NewPctWithPhotos { get; set; }

        public decimal? NewPctMultiplePhotos { get; set; }

        public decimal? NewPctSinglePhoto { get; set; }

        public string NewPrimarySource { get; set; }

        public string PartnerSelectedMarkets { get; set; }

        public int? PartnerBannerAdImpressions1 { get; set; }

        public int? PartnerBannerAdImpressions2 { get; set; }

        public int? PartnerBannerAdClickThrus1 { get; set; }

        public int? PartnerBannerAdClickThrus2 { get; set; }

        #endregion

        #region Constructor

        public AutoTraderPerformanceSummary()
        {
            CallsToDealership = null;
            ViewedWebsiteOrInventory = null;
            SecureCreditApplications = null;
            UsedVehiclesSearched = null;
            UsedDetailPagesViewed = null;
            UsedViewedMaps = null;
            UsedEmailsToDealership = null;
            UsedPrintableAds = null;
            NewVehiclesSearched = null;
            NewDetailPagesViewed = null;
            NewViewedMaps = null;
            NewEmailsToDealership = null;
            NewPrintableAds = null;
            SearchImpressions = null;
            ClickThrus = null;
            ViewedMaps = null;
            EmailsToDealership = null;
            AskedForMoreDetails = null;
            UsedAvgDailyVehicles = null;
            UsedPctWithPrice = null;
            UsedPctWithComments = null;
            UsedPctWithPhotos = null;
            UsedPctMultiplePhotos = null;
            UsedPctSinglePhoto = null;
            UsedPrimarySource = null;
            NewAvgDailyVehicles = null;
            NewPctWithPrice = null;
            NewPctWithComments = null;
            NewPctWithPhotos = null;
            NewPctMultiplePhotos = null;
            NewPctSinglePhoto = null;
            NewPrimarySource = null;
            PartnerSelectedMarkets = null;
            PartnerBannerAdImpressions1 = null;
            PartnerBannerAdImpressions2 = null;
            PartnerBannerAdClickThrus1 = null;
            PartnerBannerAdClickThrus2 = null;
        }

        #endregion

        #region Public Methods

        public void Save()
        {
            const string STORED_PROC = "merchandising.AutoTraderPerfUpsert";
            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = STORED_PROC;
                    cmd.CommandType = CommandType.StoredProcedure;

                    CheckDateTime(Month, "Month", cmd);
                    CheckInt(BusinessUnitId, "BusinessUnitId", cmd);
                    CheckInt(CallsToDealership, "CallsToDealership", cmd);
                    CheckInt(ViewedWebsiteOrInventory, "ViewedWebsiteOrInventory", cmd);
                    CheckInt(SecureCreditApplications, "SecureCreditApplications", cmd);
                    CheckInt(UsedVehiclesSearched, "UsedVehiclesSearched", cmd);
                    CheckInt(UsedDetailPagesViewed, "UsedDetailPagesViewed", cmd);
                    CheckInt(UsedViewedMaps, "UsedViewedMaps", cmd);
                    CheckInt(UsedPrintableAds, "UsedPrintableAds", cmd);
                    CheckInt(UsedEmailsToDealership, "UsedEmailsToDealership", cmd);
                    CheckInt(NewVehiclesSearched, "NewVehiclesSearched", cmd);
                    CheckInt(NewViewedMaps, "NewViewedMaps", cmd);
                    CheckInt(NewDetailPagesViewed, "NewDetailPagesViewed", cmd);
                    CheckInt(NewEmailsToDealership, "NewEmailsToDealership", cmd);
                    CheckInt(NewPrintableAds, "NewPrintableAds", cmd);
                    CheckInt(SearchImpressions, "SearchImpressions", cmd);
                    CheckInt(ClickThrus, "ClickThrus", cmd);
                    CheckInt(ViewedMaps, "ViewedMaps", cmd);
                    CheckInt(EmailsToDealership, "EmailsToDealership", cmd);
                    CheckInt(AskedForMoreDetails, "AskedForMoreDetails", cmd);
                    CheckInt(UsedAvgDailyVehicles, "UsedAvgDailyVehicles", cmd);
                    CheckDecimal(UsedPctWithPrice, "UsedPctWithPrice", cmd);
                    CheckDecimal(UsedPctWithComments, "UsedPctWithComments", cmd);
                    CheckDecimal(UsedPctWithPhotos, "UsedPctWithPhotos", cmd);
                    CheckDecimal(UsedPctMultiplePhotos, "UsedPctMultiplePhotos", cmd);
                    CheckDecimal(UsedPctSinglePhoto, "UsedPctSinglePhoto", cmd);
                    CheckString(UsedPrimarySource, "UsedPrimarySource", cmd);
                    CheckInt(NewAvgDailyVehicles, "NewAvgDailyVehicles", cmd);
                    CheckDecimal(NewPctWithPrice, "NewPctWithPrice", cmd);
                    CheckDecimal(NewPctWithComments, "NewPctWithComments", cmd);
                    CheckDecimal(NewPctWithPhotos, "NewPctWithPhotos", cmd);
                    CheckDecimal(NewPctMultiplePhotos, "NewPctMultiplePhotos", cmd);
                    CheckDecimal(NewPctSinglePhoto, "NewPctSinglePhoto", cmd);
                    CheckString(NewPrimarySource, "NewPrimarySource", cmd);
                    CheckString(PartnerSelectedMarkets, "PartnerSelectedMarkets", cmd);
                    CheckInt(PartnerBannerAdImpressions1, "PartnerBannerAdImpressions1", cmd);
                    CheckInt(PartnerBannerAdImpressions2, "PartnerBannerAdImpressions2", cmd);
                    CheckInt(PartnerBannerAdClickThrus1, "PartnerBannerAdClickThrus1", cmd);
                    CheckInt(PartnerBannerAdClickThrus2, "PartnerBannerAdClickThrus2", cmd);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}