using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.AutoTraderRobotData
{

    public abstract class AutoTraderRobot
    {
        public int BusinessUnitId { get; set; }

        /// <summary>
        /// If the datetime is not null, adds the datetime value to the database parameter.  Otherwise, adds a null value.
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="field"></param>
        /// <param name="cmd"></param>
        public void CheckDateTime(DateTime? datetime, string field, IDbCommand cmd)
        {
            Database.AddRequiredParameter(cmd, "@" + field, datetime, DbType.DateTime);
        }
        /// <summary>
        /// If the integer is not null, adds the integer value to the database parameter.  Otherwise, adds a null value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="field"></param>
        /// <param name="cmd"></param>
        public void CheckInt(int? property, string field, IDbCommand cmd)
        {
            Database.AddRequiredParameter(cmd, "@" + field, property, DbType.Int32);
        }
        /// <summary>
        /// If the decimal is not null, adds the decimal value to the database parameter.  Otherwise, adds a null value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="field"></param>
        /// <param name="cmd"></param>
        public void CheckDecimal(decimal? property, string field,  IDbCommand cmd)
        {
            Database.AddRequiredParameter(cmd, "@" + field, property, DbType.Decimal);
        }
        /// <summary>
        /// If the string is not null, adds the string value to the database parameter.  Otherwise, adds a null value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="field"></param>
        /// <param name="cmd"></param>
        public void CheckString(string property, string field, IDbCommand cmd)
        {
            Database.AddRequiredParameter(cmd, "@" + field, property, DbType.String);
        }
        /// <summary>
        /// If the boolean is not null, adds the boolean value to the database parameter.  Otherwise, adds a null value.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="field"></param>
        /// <param name="cmd"></param>
        public void CheckBool(bool? property, string field, IDbCommand cmd)
        {
            if (property.HasValue)
            {
                Database.AddRequiredParameter(cmd, "@" + field, property.Value ? 1 : 0, DbType.Boolean);
            }
            else
            {
                Database.AddRequiredParameter(cmd, "@" + field, null, DbType.Boolean);
            }

        }
    }
}
