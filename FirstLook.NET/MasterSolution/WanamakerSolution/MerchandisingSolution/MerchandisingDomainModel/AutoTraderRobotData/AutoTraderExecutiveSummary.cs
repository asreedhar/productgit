using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.AutoTraderRobotData
{
    public class AutoTraderExecutiveSummary : AutoTraderRobot
    {
        #region Instance Variables

        #endregion

        #region Properties

        public DateTime Month { get; set; }

        public int? AvgDayNew { get; set; }

        public int? AvgDayUsed { get; set; }

        public int? AvgDayTotal { get; set; }

        public int? TimesSeenInSearch { get; set; }

        public int? BannerAdImpressions { get; set; }

        public int? FindDealerSearchImpressions { get; set; }

        public int? TotalExposure { get; set; }

        public int? DetailPagesViewed { get; set; }

        public int? ViewedMaps { get; set; }

        public int? PrintableAdsRequested { get; set; }

        public int? ViewedWebsiteOrInventory { get; set; }

        public int? FindDealerClickThrus { get; set; }

        public int? BannerAdClickThrus { get; set; }

        public int? TotalActivity { get; set; }

        public int? CallsToDealership { get; set; }

        public int? EmailsToDealership { get; set; }

        public int? SecureCreditApplications { get; set; }

        public int? TotalTrackableProspects { get; set; }

        public int? TotalShoppersInArea { get; set; }

        #endregion

        #region Constructor
        public AutoTraderExecutiveSummary()
        {
            AvgDayNew = null;
            AvgDayUsed = null;
            AvgDayTotal = null;
            TimesSeenInSearch = null;
            BannerAdImpressions = null;
            FindDealerSearchImpressions = null;
            TotalExposure = null;
            DetailPagesViewed = null;
            ViewedMaps = null;
            PrintableAdsRequested = null;
            ViewedWebsiteOrInventory = null;
            FindDealerClickThrus = null;
            BannerAdClickThrus = null;
            TotalActivity = null;
            CallsToDealership = null;
            EmailsToDealership = null;
            SecureCreditApplications = null;
            TotalTrackableProspects = null;
            TotalShoppersInArea = null;
        }
        #endregion

        #region Public Methods
        public void Save()
        {
            const string STORED_PROC = "merchandising.AutoTraderExUpsert";
            using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = STORED_PROC;
                    cmd.CommandType = CommandType.StoredProcedure;
                    CheckDateTime(Month, "Month", cmd);
                    CheckInt(BusinessUnitId, "BusinessUnitId", cmd);
                    CheckInt(AvgDayNew, "AvgDayNew", cmd);
                    CheckInt(AvgDayUsed, "AvgDayUsed", cmd);
                    CheckInt(AvgDayTotal, "AvgDayTotal", cmd);
                    CheckInt(TimesSeenInSearch, "TimesSeenInSearch", cmd);
                    CheckInt(BannerAdImpressions, "BannerAdImpressions", cmd);
                    CheckInt(FindDealerSearchImpressions, "FindDealerSearchImpressions", cmd);
                    CheckInt(TotalExposure, "TotalExposure", cmd);
                    CheckInt(DetailPagesViewed, "DetailPagesViewed", cmd);
                    CheckInt(ViewedMaps, "ViewedMaps", cmd);
                    CheckInt(PrintableAdsRequested, "PrintableAdsRequested", cmd);
                    CheckInt(ViewedWebsiteOrInventory, "ViewedWebsiteOrInventory", cmd);
                    CheckInt(FindDealerClickThrus, "FindDealerClickThrus", cmd);
                    CheckInt(BannerAdClickThrus, "BannerAdClickThrus", cmd);
                    CheckInt(TotalActivity, "TotalActivity", cmd);
                    CheckInt(CallsToDealership, "CallsToDealership", cmd);
                    CheckInt(EmailsToDealership, "EmailsToDealership", cmd);
                    CheckInt(SecureCreditApplications, "SecureCreditApplications", cmd);
                    CheckInt(TotalTrackableProspects, "TotalTrackableProspects", cmd);
                    CheckInt(TotalShoppersInArea, "TotalShoppersInArea", cmd);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}
