using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.AutoTraderRobotData
{
    /// <summary>
    /// Holds all fields that are scraped by the AutoTraderExecutiveSummary robot.
    /// </summary>
    public class AutoTraderVehicles : AutoTraderRobot
    {
        #region Instance Variables

        #endregion

        #region Properties

        public DateTime Month { get; set; }

        public string VIN { get; set; }

        public string Type { get; set; }

        public string YearMakeModel { get; set; }

        public string StockNumber { get; set; }

        public decimal? Price { get; set; }

        public int? AppearedInSearch { get; set; }

        public int? DetailsPageViews { get; set; }

        public int? MapViews { get; set; }

        public int? EmailsSent { get; set; }

        public int? DetailsPagePrinted { get; set; }

        public bool? HasSpotlightAds { get; set; }

        #endregion

        #region Constructor

        public AutoTraderVehicles()
        {
            VIN = null;
            Type = null;
            YearMakeModel = null;
            StockNumber = null;
            Price = null;
            AppearedInSearch = null;
            DetailsPageViews = null;
            MapViews = null;
            EmailsSent = null;
            DetailsPagePrinted = null;
            HasSpotlightAds = null;
        }

        #endregion

        #region Public Methods

        public void Save()
        {
            if (!String.IsNullOrEmpty(VIN))
            {
                const string STORED_PROC = "merchandising.AutoTraderVehUpsert";
                using (IDbConnection conn = Database.GetConnection(Database.MerchandisingDatabase))
                {
                    using (IDbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = STORED_PROC;
                        cmd.CommandType = CommandType.StoredProcedure;
                        CheckDateTime(Month, "Month", cmd);
                        CheckInt(BusinessUnitId, "BusinessUnitId", cmd);
                        CheckString(VIN, "Vin", cmd);
                        CheckString(Type, "Type", cmd);
                        CheckString(YearMakeModel, "YearMakeModel", cmd);
                        CheckString(StockNumber, "StockNumber", cmd);
                        CheckDecimal(Price, "Price", cmd);
                        CheckInt(AppearedInSearch, "AppearedInSearch", cmd);
                        CheckInt(DetailsPageViews, "DetailsPageViews", cmd);
                        CheckInt(MapViews, "MapViews", cmd);
                        CheckInt(EmailsSent, "EmailsSent", cmd);
                        CheckInt(DetailsPagePrinted, "DetailsPagePrinted", cmd);
                        CheckBool(HasSpotlightAds, "HasSpotlightAds", cmd);
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion
    }
}