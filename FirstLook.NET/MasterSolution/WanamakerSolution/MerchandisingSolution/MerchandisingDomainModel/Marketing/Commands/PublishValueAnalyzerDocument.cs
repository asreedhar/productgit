﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using AmazonServices.SNS;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Extensions;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using PublishingDomain;
using Inventory = FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle.Inventory;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class PublishValueAnalyzerDocument : AbstractCommand 
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;
        private readonly byte[] _document;
        private readonly string _userName;
        private readonly ILogger _logger;

        // Injected dependencies
        public IValueAnalyzerVehiclePreferenceDataAccess DataAccess { get; set; }
        public IInventoryDocumentRepository DocumentRepository { get; set; }

        public string PublishedUrl { get; private set; }

        public PublishValueAnalyzerDocument(string ownerHandle, string vehicleHandle, byte[] document, string userName, ILogger logger)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _document = document;
            _userName = userName;
            _logger = logger;
        }

        protected override void Run()
        {
            try
            {
                // Construct inventory document key.
                Inventory inventory = Inventory.GetInventory(_ownerHandle, _vehicleHandle);

                if (inventory == null)
                {
                    throw new ApplicationException("The specified vehicle cannot be found in inventory.");
                }

                // Get the business unit code.
                var businessUnitCode = GetDealerCodeFromOwnerHandle(_ownerHandle);

                // Invoke the publishing command.
                var command = new PublishValueAnalyzerDocumentCommand(businessUnitCode,
                                                                      inventory.Vin,
                                                                      inventory.StockNumber,
                                                                      _document, _logger, DocumentRepository);
                DoRun(command);


                // Save urls
                var dto = DataAccess.Get(_ownerHandle, _vehicleHandle);

                PublishedUrl = command.PublishedUrl;
                dto.LongUrl = command.PublishedUrl;
                dto.ShortUrl = command.ShortUrl;

                DataAccess.Save(dto, _userName);

                // Notify subscribers that a new document has been published, if we published to S3.
                if (command.PublicationDetails != null &&
                    command.PublicationDetails["bucket"] != null &&
                    command.PublicationDetails["key"] != null)
                {
                    NotifySubscribers(command.PublicationDetails["bucket"], command.PublicationDetails["key"], inventory.Vin);                    
                }
            }
            catch (Exception ex)
            {
				Log.Error("Error publishing ValueAnalyzer document.", ex);
                _logger.Log(ex);
            }
        }

        private void NotifySubscribers(string bucketName, string key, string vin)
        {
            // create the message
            // TODO: Refactor to use MaxDocumentPublishTask
            var message = new
            {
                bucket = bucketName,
                key = key,
                url = string.Format("https://s3.amazonaws.com/{0}/{1}", bucketName, key),
                generated_at = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                sender = "PublishValueAnalyzerDocument",
                message_type = "marketing", // don't change, downstream subscribers switch on it
                content_type = "application/pdf",
                vin = vin
            };

            string messageJson = message.ToJson();

            // push a message to a topic.
            string topicArn = GenericTopic.CreateTopic("max-marketing-pdf-changes", true);
            string messageId = GenericTopic.Publish(messageJson, topicArn);

            Log.DebugFormat("Marketing PDF document update message with id {0} published to SNS.", messageId);
        }

        private static string GetDealerCodeFromOwnerHandle(string ownerHandle)
        {
            // Get the owner, so we can get the business unit, so we can get the business unit code.
            var owner = Owner.GetOwner(ownerHandle);

            // we currently only support dealers and inventory
            if (owner.OwnerEntityType != OwnerEntityType.Dealer)
                throw new NotSupportedException("The passed owner entity type is not supported.");

            // get the BusinessUnit from the owner
            var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);

            return businessUnit.BusinessUnitCode;
        }
    }
}
