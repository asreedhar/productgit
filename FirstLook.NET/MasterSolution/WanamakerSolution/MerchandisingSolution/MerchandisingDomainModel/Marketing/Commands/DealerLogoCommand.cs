using System;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class DealerLogoCommand : CachedCommand
    {
        private readonly int _maxHeight;
        private readonly int _maxWidth;
        private readonly string _ownerHandle;
        private string _url, _displayUrl;
        private readonly string _defaultImageFileName;
        private int _displayWidth, _displayHeight;

        /// <summary>
        /// The caller may already know the url or the owner handle.  If the url is not empty, it will
        /// be used.  Otherwise, the owner handle will be used to find the correct url.  If both are
        /// passed, the url is given preference.  If neither is passed, it is an error.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="defaultImageFileName"></param>
        /// <param name="ownerHandle"></param>
        /// <param name="maxWidth"></param>
        /// <param name="maxHeight"></param>
        public DealerLogoCommand(string url, string defaultImageFileName, string ownerHandle, int maxWidth, int maxHeight)
        {
            _url = url;
            _defaultImageFileName = defaultImageFileName;
            _ownerHandle = ownerHandle;
            _maxHeight = maxHeight;
            _maxWidth = maxWidth;
        }

        protected override void Run()
        {
            if( _url.Length == 0 && _ownerHandle.Length == 0 )
            {
                throw new ApplicationException("A url or owner handle must be supplied.");
            }

            if( _url.Length == 0 )
            {
                // Get the url using the handle.
                _url = new PhotoLocator().LocateDealerLogoUrl(_ownerHandle, _defaultImageFileName);
            }

            // Invoke the safe image scale command.
            SafeImageScaleCommand command = new SafeImageScaleCommand(_url, _defaultImageFileName, _maxHeight, _maxWidth); 
            DoRun( command );

            // Set output.
            _displayUrl = command.DisplayUrl;
            _displayHeight = command.DisplayHeight;
            _displayWidth = command.DisplayWidth;
        }

        // Results
        public string DisplayUrl
        {
            get { return _displayUrl; }
        }

        public int DisplayWidth
        {
            get { return _displayWidth; }
        }

        public int DisplayHeight
        {
            get { return _displayHeight; }
        }
    }
}
