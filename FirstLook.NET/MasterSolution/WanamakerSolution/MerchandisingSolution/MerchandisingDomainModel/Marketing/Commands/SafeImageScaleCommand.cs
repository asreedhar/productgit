using System;
using FirstLook.Common.Core.Command;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class SafeImageScaleCommand : CachedCommand
    {
        private readonly string _url, _notFoundUrl;        
        private readonly int _maxHeight, _maxWidth;

        private int _displayHeight, _displayWidth;
        private string _displayUrl, _absoluteUri;

        /// <summary>
        /// The command will retrieve the image at the specified url, inspect its native dimensions, and if needed,
        /// calculate scaled dimensions which do not exceed the max width/height.  If the specified image cannot be
        /// loaded for any reason, the notFoundUrl image will be used instead.  In this case, it will be displayed
        /// at max height/width.  Image scale calculations are cached.
        /// 
        /// The command is safe, in that exceptions are suppressed and logged.
        /// 
        /// added absolute URI to get the resolved URL
        /// </summary>
        /// <param name="url">The url to the image.</param>
        /// <param name="notFoundPath">The local path to the image to be used in case the supplied image cannot be located.</param>
        /// <param name="maxHeight">The maximum image height.</param>
        /// <param name="maxWidth">The maximum image width.</param>
        public SafeImageScaleCommand(string url, string notFoundPath, int maxHeight, int maxWidth) 
        {
            _url = url;
            _notFoundUrl = notFoundPath;
            _maxHeight = maxHeight;
            _maxWidth = maxWidth;
        }

        protected override void Run()
        {
            // Assume that we will display at the max dimensions.
            _displayHeight = _maxHeight;
            _displayWidth = _maxWidth;
            _displayUrl = _url;

            // Try to calculate the remote image's scaled dimensions.
            var calculator = new RemoteImageScaleCalculator();
            try
            {
                calculator.RescaleImage(_url, _maxHeight, _maxWidth, out _displayHeight, out _displayWidth, out _absoluteUri);
            }
            catch (Exception ex)
            {
                Log.Log(ex);
                _displayUrl = _notFoundUrl;
            }

            // Zero-sized images should not be displayed - the not found image should be displayed instead.
            // Similarly, if the not-found image is used, it should have non-zero size.
            if (_displayHeight == 0 || _displayWidth == 0 || _displayUrl == _notFoundUrl)
            {
                _displayUrl = _notFoundUrl;
                _displayHeight = _maxHeight;
                _displayWidth = _maxWidth;                
            }

        }

        // Results
        public string DisplayUrl
        {
            get { return _displayUrl; }
        }

        public int DisplayWidth
        {
            get { return _displayWidth; }
        }

        public int DisplayHeight
        {
            get { return _displayHeight; }
        }

        public string AbsoluteUri
        {
            get { return _absoluteUri; }
        }
        
    }
}
