using System;
using System.Data;
using System.Threading;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    /// <summary>
    /// Command for auditing generated print content for Sales Accelerator.
    /// </summary>
    public class AuditPrintCommand : CachedCommand
    {

        /// <summary>
        /// The xml that is fed into princeXml to generate a pdf.
        /// </summary>
        private readonly string _printXml;

        /// <summary>
        /// The handle of the owner who requested the printing.
        /// </summary>
        private readonly string _ownerHandle;

        /// <summary>
        /// The handle of the vehicle this printing covers.
        /// </summary>
        private readonly string _vehicleHandle;

        /// <summary>
        /// Populate this command with the parameters that will be saved to the database.
        /// </summary>
        /// <param name="printXml">Xml that is used to generate the pdf byte stream.</param>L
        /// <param name="ownerHandle">Handle of the owner who has requested the printing.</param>
        /// <param name="vehicleHandle">Handle of the vehicle this printing covers.</param>
        public AuditPrintCommand(string printXml, string ownerHandle, string vehicleHandle)             
        {
            _printXml = printXml;
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
        }

        /// <summary>
        /// Execute this command. Will save the print details to the database.
        /// </summary>
        protected override void Run()
        {
            try
            {
                using (var connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.PrintAudit#Insert";

                        var insertUser = Thread.CurrentPrincipal.Identity.Name;

                        command.AddParameterWithValue("PrintXML", DbType.String, true, _printXml);
                        command.AddParameterWithValue("OwnerHandle", DbType.String, true, _ownerHandle);
                        command.AddParameterWithValue("VehicleHandle", DbType.String, true, _vehicleHandle);
                        command.AddParameterWithValue("InsertUser", DbType.String, true, insertUser);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exception)
            {
                Log.Log(exception);
            }
        }
    }
}