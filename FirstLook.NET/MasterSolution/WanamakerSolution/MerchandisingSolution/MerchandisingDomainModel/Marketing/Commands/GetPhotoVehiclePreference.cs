using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.DTO;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using FirstLook.Merchandising.DomainModel.Marketing.Photos.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class GetPhotoVehiclePreference : CachedCommand, ISurrogateKey
    {

        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;

        // Injected dependencies
        public IPhotoVehiclePreferenceDataAccess DataAccess { get; set; }

        public GetPhotoVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
        }

        protected override void Run()
        {
            Preference = DataAccess.GetPhotoVehiclePreference(_ownerHandle, _vehicleHandle);
        }

        // Results
        public PhotoVehiclePreferenceTO Preference { get; private set; }

        public string Key
        {
            get { return _ownerHandle + ":" + _vehicleHandle; }
        }
    }
}
