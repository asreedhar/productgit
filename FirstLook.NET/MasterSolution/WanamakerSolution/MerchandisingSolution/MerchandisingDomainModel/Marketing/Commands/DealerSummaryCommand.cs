using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.DTO;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class DealerSummaryCommand : CachedCommand, ISurrogateKey 
    {
        private readonly string _ownerHandle;
        private string _dealerSummaryXml;

        public DealerSummaryCommand(string ownerHandle)
        {
            _ownerHandle = ownerHandle;
            _dealerSummaryXml = string.Empty;
        }


        protected override void Run()
        {
            // Check cache for existing data.
            string data = Cache.Get(Key) as string;
            if (data != null)
            {
                _dealerSummaryXml = data;
                return;
            }

            // The data is not in the cache - go get it.
            try
            {
                DealerGeneralPreference preference = DealerGeneralPreference.GetOrCreateDealerGeneralPreference(_ownerHandle);
                _dealerSummaryXml = preference.AsXml();

                // Store in cache.
                Cache.Set(Key, _dealerSummaryXml, DefaultCacheExpirationSeconds);

            }
            catch (Exception e)
            {
                Log.Log(e);
                _dealerSummaryXml = string.Empty;
            }
           
        }

        public string DealerSummary
        {
            get { return _dealerSummaryXml; }
        }

        public string Key
        {
            get
            {
                return GetType() + CommonKeys.DELIM + _ownerHandle;
            }
        }
    }
}
