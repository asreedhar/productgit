using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using FirstLook.Merchandising.DomainModel.Pricing;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
	public class CertifiedProgamsForDealerMakeCommand : CachedCommand
	{
		public int BusinessUnitId { get; private set; }
		public string Make { get; private set; }
		public IEnumerable<CertifiedProgramInfoTO> Programs { get; private set; }

		public CertifiedProgamsForDealerMakeCommand(int businessUnitId, string make)
		{
			BusinessUnitId = businessUnitId;
			Make = make;
		}

		protected override void Run()
		{
			var ownerHandle = PricingData.GetOwnerHandle(BusinessUnitId);

			var programsList = new List<CertifiedProgramInfoTO>();

			using (var connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Certified.GetCertifiedProgramsForOwnerMake";

					CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
					CommonMethods.AddWithValue(command, "Make", Make, DbType.String);

					using (var rdr = command.ExecuteReader())
					{
						while (rdr.Read())
						{
							var type = rdr.GetString(rdr.GetOrdinal("ProgramType"));
							var id = rdr.GetInt32(rdr.GetOrdinal("ProgramId"));
							var name = rdr.GetString(rdr.GetOrdinal("Name"));
							var requiresCertifiedId = rdr.GetBoolean(rdr.GetOrdinal("RequiresCertifiedId"));
							var mfrReportsCerts = rdr.GetBoolean(rdr.GetOrdinal("ManfacturerReportsCertifications"));
							programsList.Add(new CertifiedProgramInfoTO(id, type, name, requiresCertifiedId, mfrReportsCerts));
						}
					}
				}
			}

			Programs = programsList;
		}
		
	}
}