﻿using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class SetValueAnalyzerVehiclePreference : AbstractCommand
    {
        private readonly ValueAnalyzerVehiclePreferenceTO _preference;
        private readonly string _userName;

        // Injected dependencies
        public IValueAnalyzerVehiclePreferenceDataAccess DataAccess { get; set; }

        public SetValueAnalyzerVehiclePreference(string ownerHandle, string vehicleHandle, bool displayVehicleDescription, bool useLongMarketingListingLayout, string userName)
        {
            _preference = new ValueAnalyzerVehiclePreferenceTO(ownerHandle, vehicleHandle, displayVehicleDescription, useLongMarketingListingLayout);
            _userName = userName;
        }

        public SetValueAnalyzerVehiclePreference(ValueAnalyzerVehiclePreferenceTO preference, string userName)
        {
            _preference = preference;
            _userName = userName;
        }

        protected override void Run()
        {
            DataAccess.Save(_preference, _userName);
        }
    }
}
