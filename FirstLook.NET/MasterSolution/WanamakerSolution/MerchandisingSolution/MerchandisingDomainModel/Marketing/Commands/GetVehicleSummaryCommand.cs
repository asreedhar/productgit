using System;
using FirstLook.Common.Core.Command;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class GetVehicleSummaryCommand : CachedCommand
    {
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;
        private string _vehicleSummaryXml;

        public GetVehicleSummaryCommand(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _vehicleSummaryXml = string.Empty;
        }

        protected override void Run()
        {
            // The data is not in the cache - go get it.
            try
            {
                Vehicle.VehicleSummary summary =
                   Vehicle.VehicleSummary.GetVehicleSummary(_ownerHandle, _vehicleHandle);

                _vehicleSummaryXml = summary.AsXml();
            }
            catch (Exception e)
            {
                Log.Log(e);
                _vehicleSummaryXml = string.Empty;
            }
        }

        public string VehicleSummary
        {
            get{ return _vehicleSummaryXml; }
        }

    }
}
