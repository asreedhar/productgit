using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using FirstLook.Merchandising.DomainModel.Marketing.Photos.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class SetPhotoVehiclePreference : AbstractCommand
    {
        private readonly PhotoVehiclePreferenceTO _preference;
        private readonly string _userName;

        // Injected dependencies
        public IPhotoVehiclePreferenceDataAccess DataAccess { get; set; }

        public SetPhotoVehiclePreference(string ownerHandle, string vehicleHandle, bool isDisplayed, string userName)
        {
            _preference = new PhotoVehiclePreferenceTO(ownerHandle, vehicleHandle, isDisplayed);
            _userName = userName;
        }

        protected override void Run()
        {
            DataAccess.SetPhotoVehiclePreference(_preference, _userName );
        }
    }
}
