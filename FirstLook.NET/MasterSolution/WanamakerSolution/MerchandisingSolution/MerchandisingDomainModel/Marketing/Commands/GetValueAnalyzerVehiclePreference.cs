﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.DTO;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    /// <summary>
    /// A command that gets a vehicle-specific preference for WebPDF - formerly called "value analyzer" document.
    /// </summary>
    public class GetValueAnalyzerVehiclePreference : CachedCommand, ISurrogateKey
    {
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;
        private ValueAnalyzerVehiclePreferenceTO _preference;

        // Injected dependencies
        public IValueAnalyzerVehiclePreferenceDataAccess DataAccess { get; set; }

        public GetValueAnalyzerVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
        }

        protected override void Run()
        {
            _preference = DataAccess.Get(_ownerHandle, _vehicleHandle);
        }

        public string Key
        {
            get { return _ownerHandle + ":" + _vehicleHandle; }
        }

        // Results
        public ValueAnalyzerVehiclePreferenceTO Preference
        {
            get { return _preference; }
        }
    }
}
