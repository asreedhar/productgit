using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class VehicleImageCommand : CachedCommand
    {
        private const int MAX_HEIGHT = 104;
        private const int MAX_WIDTH = 138;

        private const string NOT_FOUND_FILE_PATH = "/pricing/Public/Images/";
        private const string NOT_FOUND_FILE_NAME = "PhotoNotAvailable.png";
        private const string NOT_FOUND_FILE = NOT_FOUND_FILE_PATH + NOT_FOUND_FILE_NAME;

        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;

        private string _displayUrl;
        private int _displayHeight, _displayWidth;

        public VehicleImageCommand(string ownerHandle, string vehicleHandle) 
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
        }
        /// <summary>
        /// changed to return the forwarded URL
        /// </summary>
        protected override void Run()
        {
            // get the url
            string url = new PhotoLocator().LocateVehiclePhotoUrl(_ownerHandle, _vehicleHandle, NOT_FOUND_FILE_NAME);

            // Rescale image
            var command = new SafeImageScaleCommand(url, NOT_FOUND_FILE, MAX_HEIGHT, MAX_WIDTH);
            DoRun(command);

            // Set results.
            // Commented out to hotfix 32646 - Travis Huber 2015-03-09
            //_displayUrl = !string.IsNullOrWhiteSpace(command.AbsoluteUri) ? command.AbsoluteUri : command.DisplayUrl;
            _displayUrl = command.DisplayUrl;
            _displayHeight = command.DisplayHeight;
            _displayWidth = command.DisplayWidth;
        }

        // Results
        public string DisplayUrl
        {
            get { return _displayUrl; }
        }

        public int DisplayWidth
        {
            get { return _displayWidth; }
        }

        public int DisplayHeight
        {
            get { return _displayHeight; }
        }
    }
}
