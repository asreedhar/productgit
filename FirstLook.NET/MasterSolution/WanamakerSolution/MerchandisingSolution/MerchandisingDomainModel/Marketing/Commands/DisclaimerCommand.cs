using System;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.Commands
{
    public class DisclaimerCommand : CachedCommand
    {
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;
        private readonly string _searchHandle;
        
        private JDPower _jdPower;
        private KelleyBlueBook _kellyBlueBook;
        private Ping _ping;
        private Nada _nada;

        public DisclaimerCommand(string ownerHandle, string vehicleHandle, string searchHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _searchHandle = searchHandle;
        }

        protected override void Run()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.GetPricingDisclaimer";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, _vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, _ownerHandle);
                    command.AddParameterWithValue("searchHandle", DbType.String, false, _searchHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        int? pingDistance = null;
                        string kbbPublicationDate = null;
                        DateTime? jdPowerPeriodBeginDate = null;
                        DateTime? jdPowerPeriodEndDate = null;
                        string jdPowerRegion = null;
                        string nadaPublicationDate = null;

                        if (reader.Read())
                        {
                            pingDistance = GetNullable<int>(reader, "InternetDistance");
                            kbbPublicationDate = GetValue<string>(reader, "KbbPublicationDate");

                            jdPowerPeriodBeginDate = GetNullable<DateTime>(reader, "JDPowerPeriodBeginDate");
                            jdPowerPeriodEndDate = GetNullable<DateTime>(reader, "JDPowerPeriodEndDate");
                            jdPowerRegion = GetValue<string>(reader, "JDPowerRegion");
                            nadaPublicationDate = GetValue<string>( reader, "NadaPublicationDate" );
                        }

                        _ping = new Ping(pingDistance);
                        _kellyBlueBook = new KelleyBlueBook(kbbPublicationDate);
                        _jdPower = new JDPower(jdPowerPeriodBeginDate, jdPowerPeriodEndDate, jdPowerRegion);
                        _nada = new Nada( nadaPublicationDate );
                    }
                }
            }
        }

        private static TValue GetValue<TValue>(IDataRecord record, string columnName) where TValue : class
        {
            TValue value = null;
            int ordinal = record.GetOrdinal(columnName);
            if (!record.IsDBNull(ordinal))
                value = (TValue)record.GetValue(ordinal);

            return value;
        }

        private static TValue? GetNullable<TValue>(IDataRecord record, string columnName) where TValue : struct 
        {
            TValue? value = null;
            int ordinal = record.GetOrdinal(columnName);
            if (!record.IsDBNull(ordinal))
                value = (TValue)record.GetValue(ordinal);

            return value;
        }

        public string Xml
        {
            get
            {
                /*Need to TagBuilder.Wrap for pdf web service*/
                StringBuilder xmlStringBuilder = new StringBuilder();
                XmlWriterSettings settings = new XmlWriterSettings() { OmitXmlDeclaration = true };
                using (XmlWriter xmlWriter = XmlWriter.Create(xmlStringBuilder, settings))
                {
                    xmlWriter.WriteStartElement("disclaimer", string.Empty);

                    xmlWriter.WriteStartElement("jdpower", string.Empty);
                    xmlWriter.WriteAttributeString("isValid", _jdPower.IsValid.ToString().ToLower());
                    xmlWriter.WriteElementString("region", _jdPower.Region);
                    xmlWriter.WriteElementString("begindate", _jdPower.BeginDate.ToString());
                    xmlWriter.WriteElementString("enddate", _jdPower.EndDate.ToString());
                    xmlWriter.WriteElementString("periodweeks", _jdPower.PeriodWeeks);
                    xmlWriter.WriteEndElement();

                    xmlWriter.WriteStartElement("kelleybluebook", string.Empty);
                    xmlWriter.WriteAttributeString( "isValid", _kellyBlueBook.IsValid.ToString().ToLower() );
                    xmlWriter.WriteElementString("pubdate", _kellyBlueBook.PubDate);
                    xmlWriter.WriteEndElement();

                    xmlWriter.WriteStartElement("ping", string.Empty);
                    xmlWriter.WriteAttributeString( "isValid", _ping.IsValid.ToString().ToLower() );
                    xmlWriter.WriteElementString("radiusmiles", _ping.RadiusMiles.ToString());
                    xmlWriter.WriteEndElement();

                    xmlWriter.WriteStartElement( "nada", string.Empty );
                    xmlWriter.WriteAttributeString( "isValid", _nada.IsValid.ToString().ToLower() );
                    xmlWriter.WriteElementString( "pubmonth", _nada.PubMonth );
                    xmlWriter.WriteEndElement();

                    xmlWriter.WriteEndElement();
                }

                return xmlStringBuilder.ToString();
            }
        }

        private class JDPower
        {
            public JDPower(DateTime? beginDate, DateTime? endDate, string region)
            {
                BeginDate = beginDate ?? DateTime.MinValue;
                EndDate = endDate ?? DateTime.MinValue;
                Region = region ?? string.Empty;
            }

            public String Region {get; private set;}
            public DateTime BeginDate { get; private set; }
            public DateTime EndDate { get; private set; }

            public string PeriodWeeks
            {
                get
                {
                    TimeSpan span = EndDate - BeginDate;
                    return Math.Round(span.Days / 7f).ToString();
                }
            }

            public bool IsValid
            {
                get
                {
                    return !string.IsNullOrEmpty(Region) && BeginDate != DateTime.MinValue && EndDate != DateTime.MinValue;
                }
            }
        }

        private class KelleyBlueBook
        {
            public KelleyBlueBook(string pubDate)
            {
                PubDate = pubDate ?? string.Empty;
            }

            public string PubDate { get; private set; }

            public bool IsValid
            {
                get
                {
                    return !string.IsNullOrEmpty(PubDate);
                }
            }
        }

        private class Ping
        {
            public Ping(int? radiusMiles)
            {
                RadiusMiles = radiusMiles ?? -1;
            }

            public int RadiusMiles { get; private set; }

            public bool IsValid
            {
                get
                {
                    return RadiusMiles >= 0;
                }
            }
        }

        private class Nada
        {
            public Nada( string pubDate )
            {
                PubDate = pubDate ?? string.Empty;
            }

            public string PubDate { get; private set; }

            public string PubMonth
            {
                get
                {
                    int parse;
                    int.TryParse( PubDate, out parse );
                    if( parse > 0 )
                    {
                        if( PubDate.StartsWith( DateTime.Now.Year.ToString().Substring(0,2) ) )
                        {
                            int.TryParse( PubDate.Substring( 4, 2 ), out parse );
                            switch (parse)
                            {
                            case 1: return "January";
                            case 2: return "February";
                            case 3: return "March";
                            case 4: return "April";
                            case 5: return "May";
                            case 6: return "June";
                            case 7: return "July";
                            case 8: return "August";
                            case 9: return "September";
                            case 10: return "October";
                            case 11: return "November";
                            case 12: return "December";
                            }
                        }
                    }
                    return PubDate;
                }
            }

            public bool IsValid
            {
                get
                {
                    return !string.IsNullOrEmpty( PubDate );
                }
            }
        }
    }
}
