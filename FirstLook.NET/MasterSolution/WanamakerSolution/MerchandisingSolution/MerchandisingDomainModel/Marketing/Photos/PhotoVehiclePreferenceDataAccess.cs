using System;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Photos.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Photos
{
    class PhotoVehiclePreferenceDataAccess : IPhotoVehiclePreferenceDataAccess
    {
        public PhotoVehiclePreferenceTO GetPhotoVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            PhotoVehiclePreferenceTO preference = null;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.PhotoVehiclePreference#Fetch";

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    IDataParameter isDisplayed = SimpleQuery.AddOutParameter(command, "IsDisplayed", false, DbType.Boolean);

                    command.ExecuteNonQuery();
                    preference = new PhotoVehiclePreferenceTO(ownerHandle, vehicleHandle, (bool)isDisplayed.Value);
                }
            }

            if (preference == null) throw new ApplicationException("A preference or a suitable default was not found.");

            return preference;
        }

        public void SetPhotoVehiclePreference(PhotoVehiclePreferenceTO preference, string userName)
        {
            // Update the preference.
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.PhotoVehiclePreference#Set";

                    SimpleQuery.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", preference.VehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "User", userName, DbType.String);
                    SimpleQuery.AddWithValue(command, "IsDisplayed", preference.IsDisplayed, DbType.Boolean);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

                // Commit
                txn.Commit();
            }

        }
    }
}
