using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Photos
{
    /// <summary>
    /// Used to perform math on image rescaling.
    /// </summary>
    public class ImageScaler
    {
        /// <summary>
        /// Given maximum input dimensions and actual dimensions, calcualte output dimensions which do not exceed the maxiumums
        /// but which preserve the scale of the input dimensions.
        /// </summary>
        /// <param name="maxHeight">The max height.</param>
        /// <param name="maxWidth">The max width.</param>
        /// <param name="nativeHeight">The native height.</param>
        /// <param name="nativeWidth">The native width.</param>
        /// <param name="height">The calculated height.</param>
        /// <param name="width">The calculated width.</param>
        public void Scale(int maxHeight, int maxWidth, int nativeHeight, int nativeWidth, out int height, out int width )
        {
            // assume initially that the native dimensions are fine.
            height = nativeHeight;
            width = nativeWidth;

            // if either is zero, both should be 0
            if( nativeHeight == 0 || nativeWidth == 0 )
            {
              height = 0;
              width = 0;
              return;
            }

            // If neither exceed the max, we have nothing to do.
            if( nativeHeight <= maxHeight && nativeWidth <= maxWidth )
            {
                return;
            }


            // Watch out for 1 pixel images - we cannot scale these down.
            if (nativeWidth == 1 || nativeHeight == 1)
            {
                // we cannot scale this image down further.
                height = 0;
                width = 0;
                return;
            }

            // down-scale if needed
            if (nativeHeight > maxHeight && nativeWidth > maxWidth)
            {
                if (nativeHeight > 0)
                {
                    double scale = (double) nativeWidth / nativeHeight;
                    double maxScale = (double) maxWidth/maxHeight;

                    if( scale < maxScale )
                    {
                        // image should be constrained by height - it is taller than wide, compared to maximums
                        // try to maximize the dimensions when we scale down
                        height = maxHeight;
                        width = Round( height * scale );
                    }
                    else
                    {
                        // image should be constrained by width - it is wider than tall, compared to maximums
                        // try to maximize the dimensions when we scale down
                        width = maxWidth;
                        height = Round( width / scale );
                    }
                }
            }

            // if only one dimension is to large, we don't want to up-scale the image to the maximums.
            else if( nativeHeight > maxHeight && nativeWidth <= maxWidth )
            {
              if (nativeHeight > 0)
              {
                  // scale by height
                  double scale = (double) nativeWidth/nativeHeight;
                  height = maxHeight;
                  width = Round( height * scale );
              }
            }
            else if( nativeWidth > maxWidth && nativeHeight <= maxHeight )
            {
              if (nativeHeight > 0)
              {
                  // scale by width.
                  double scale = (double) nativeWidth/nativeHeight;
                  width = maxWidth;
                  height = Round( width / scale );
              }
            }
            
            // If, by the above calculations, either dimension is zero, both should be zero.
            if (height == 0 || width == 0)
            {
                height = 0;
                width = 0;
                return;
            }
        }


        private int Round(double d)
        {
            return (int) Math.Round(d, 0, MidpointRounding.AwayFromZero);
        }
          
    }
}
