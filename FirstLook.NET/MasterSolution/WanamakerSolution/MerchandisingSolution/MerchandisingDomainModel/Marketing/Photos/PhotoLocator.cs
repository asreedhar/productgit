using System;
using System.Configuration;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.PhotoServices;

namespace FirstLook.Merchandising.DomainModel.Marketing.Photos
{
    public class PhotoLocator
    {
        private readonly string _localRoot;
        private readonly string _remoteRoot;
        private readonly ILogger _logger;
        private readonly IPhotoServices _photoServices;
        private readonly IPhotoLocatorDataAccess _dataAccess;

        public enum BrandLogo
        {
            EdmundsTrueMarketValue,
            JdPowerPinAverageInternetPrice,
            KelleyBlueBookRetailValue,
            NadaRetailValue,
            PingMarketAverageInternetPrice,
            BlackBook,
            Galves
        }

        internal PhotoLocator(ILogger logger, IPhotoLocatorDataAccess dataAccess, IPhotoServices photoServices, 
            string localImageUrlRoot, string remoteImageUrlRoot)
        {
            _localRoot = localImageUrlRoot;
            _remoteRoot = remoteImageUrlRoot;
            _logger = logger;
            _dataAccess = dataAccess;
            _photoServices = photoServices;
        }

        // Use default services from service registry.
        public PhotoLocator(string localImageUrlRoot, string remoteImageUrlRoot)
            : this(Registry.Resolve<ILogger>(), Registry.Resolve<IPhotoLocatorDataAccess>(), 
                Registry.Resolve<IPhotoServices>(), localImageUrlRoot, remoteImageUrlRoot)
        {
        }

        // Use values from config.
        public PhotoLocator()
            : this(ConfigurationManager.AppSettings["photo_local_root_url"], ConfigurationManager.AppSettings["photo_remote_root_url"])
        {
        }

        public string LocalImageRootUrl
        {
            get { return _localRoot; }
        }

        public string RemoteImageRootUrl
        {
            get { return _remoteRoot; }
        }

        public string BrandLogoFileName( BrandLogo logo )
        {
            string fileName = string.Empty;

            switch(logo)
            {
                case BrandLogo.EdmundsTrueMarketValue:
                    fileName = "logo_Edmunds.gif";
                    break;
                case BrandLogo.JdPowerPinAverageInternetPrice:
                    fileName = "logo_JDPower.gif";
                    break;
                case BrandLogo.KelleyBlueBookRetailValue:
                    fileName = "logo_KBB2.gif";
                    break;
                case BrandLogo.NadaRetailValue:
                    fileName = "logo_NADA.gif";
                    break;
                case BrandLogo.PingMarketAverageInternetPrice:
                    fileName = "logo_MktAvgInternetPrice.png";
                    break;
                case BrandLogo.BlackBook:
                    fileName = "logo_BlackBook.gif";
                    break;
                case BrandLogo.Galves:
                    fileName = "logo_Galves.gif";
                    break;
            }

            return fileName;
        }

        public string BrandLogoUrl( BrandLogo logo )
        {
            // Get the file name
            string fileName = BrandLogoFileName(logo);

            // Brand logos are local.
            return LocalImageRootUrl + fileName;
        }

        /// <summary>
        /// Locate the url for the dealer logo photo.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="defaultImageFileName"></param>
        /// <returns></returns>
        public string LocateDealerLogoUrl(string ownerHandle, string defaultImageFileName)
        {
            string url = LocalImageRootUrl + defaultImageFileName;

            try
            {
                string tempUrl = DataAccess.FetchDealerLogoUrl(ownerHandle);

                if (IsUrl(tempUrl))
                {
                    // it's already a url
                    return tempUrl;
                }

                // It's a path
                if( tempUrl.Length > 0 )
                {
                    url = RemoteImageRootUrl + tempUrl;    
                }                
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
            }

            return url;
        }

        /// <summary>
        /// Locate the url for the vehicle photo.  Currently, only inventory photos are supported.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <param name="defaultImageFileName"></param>
        /// <returns></returns>
        public string LocateVehiclePhotoUrl(string ownerHandle, string vehicleHandle, string defaultImageFileName)
        {
            string url = LocalImageRootUrl + defaultImageFileName;

            try
            {
                string tempUrl = _photoServices.GetMainPhotoUrlByOwnerAndVehicleHandle(ownerHandle, vehicleHandle);

                if( IsUrl( tempUrl ))
                {
                    // it's already a url
                    return tempUrl;
                }

                // It's a path
                if (tempUrl.Length > 0)
                {
                    url = RemoteImageRootUrl + tempUrl;
                }            
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
            }

            return url;
        }

      
        private static bool IsUrl(string candidate)
        {
            return candidate.StartsWith("http") || candidate.StartsWith("https");
        }

        private IPhotoLocatorDataAccess DataAccess
        {
            get { return _dataAccess; }
        }
    }
}