﻿
namespace FirstLook.Merchandising.DomainModel.Marketing.Photos.Types
{
    public class PhotoVehiclePreferenceTO
    {
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;

        internal PhotoVehiclePreferenceTO(string ownerHandle, string vehicleHandle, bool isDisplayed)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            IsDisplayed = isDisplayed;
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public bool IsDisplayed { get; set; }
    }
}
