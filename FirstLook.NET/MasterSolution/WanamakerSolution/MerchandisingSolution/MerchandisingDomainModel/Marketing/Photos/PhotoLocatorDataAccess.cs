using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.Photos
{
    internal class PhotoLocatorDataAccess : IPhotoLocatorDataAccess
    {
        #region Implementation of IPhotoLocatorDataAccess

        public string FetchDealerLogoUrl(string ownerHandle)
        {
            string url = string.Empty;
                     
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.GetDealerLogoUrl";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr != null)
                    {
                        if (rdr.Read())
                        {
                            url = rdr.GetString(rdr.GetOrdinal("Url"));
                        }
                    }
                    else
                    {
                        throw new ApplicationException("The datareader is null.");
                    }
                    if (!rdr.IsClosed) rdr.Close();
                }
            }

            return url;
        }

        #endregion
    }
}
