namespace FirstLook.Merchandising.DomainModel.Marketing.Photos
{
    internal interface IPhotoLocatorDataAccess
    {
        string FetchDealerLogoUrl(string ownerHandle);
    }
}
