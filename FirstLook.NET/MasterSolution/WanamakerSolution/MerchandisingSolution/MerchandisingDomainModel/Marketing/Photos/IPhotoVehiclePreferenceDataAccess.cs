﻿
using FirstLook.Merchandising.DomainModel.Marketing.Photos.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Photos
{
    public interface IPhotoVehiclePreferenceDataAccess
    {
        PhotoVehiclePreferenceTO GetPhotoVehiclePreference(string ownerHandle, string vehicleHandle);

        void SetPhotoVehiclePreference(PhotoVehiclePreferenceTO preference, string userName);
    }
}