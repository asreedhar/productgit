﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    internal class InventoryDocumentWorkItemsCommand : AbstractCommand
    {
        private IList<WorkItemsDTO> _list;

        public InventoryDocumentWorkItemsCommand()
        {
            _list = new List<WorkItemsDTO>();
        }

        protected override void Run()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.InventoryDocuments#FetchActive";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            WorkItemsDTO workItem = new WorkItemsDTO();
                            workItem.BusinessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitID"));
                            workItem.InventoryId = reader.GetInt32(reader.GetOrdinal("InventoryID"));

                            _list.Add(workItem);
                        }
                    }
                }
            }
        }

        public WorkItemsDTO[] GetWorkItems()
        {
            return _list.ToArray();
        }

    }
}
