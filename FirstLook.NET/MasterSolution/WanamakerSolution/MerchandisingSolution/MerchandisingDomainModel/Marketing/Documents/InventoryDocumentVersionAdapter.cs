﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.IO;
using System.Xml.XPath;
using AmazonServices.SNS;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using Merchandising.Messages;
using Merchandising.Messages.InventoryDocuments;
using Microsoft.JScript;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    internal class InventoryDocumentVersionAdapter : IInventoryDocumentDataAccess
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private Version _version;
        private readonly IQueueFactory _queueFactory;
	    private IInventoryDocumentDataAccess _dataAccess;        
        private string _schemaLocation;

        public InventoryDocumentVersionAdapter(IInventoryDocumentDataAccess dataAccess, Version version, IQueueFactory queueFactory)
        {
            _dataAccess = dataAccess;            
            _version = version;
            _queueFactory = queueFactory;
	        _schemaLocation = string.Format("{0}/api.aspx/Inventory/{1}/InventoryDocument.xsd", ConfigurationManager.AppSettings["DocumentSchemaEndpoint"], _version.ToString());
        }

        private string EmbeddedSchemaResource
        {
            get
            {
                return string.Format("FirstLook.Merchandising.DomainModel.Marketing.Documents.Schema._{0}._{1}.InventoryDocument.xsd",
                                  _version.Major, _version.Minor);
            }
        }

        private void ScrubCustomerData(XmlDocument doc)
        {
            XmlNode customerNode = doc.SelectSingleNode("//salesaccelerator/offerFooter/customer");
            if(customerNode != null)
                customerNode.RemoveAll();

            XmlNode salesPersonNode = doc.SelectSingleNode("//salesaccelerator/offerFooter/salesperson");
            if(salesPersonNode != null)
                salesPersonNode.RemoveAll();
        }

        private void SetVersion(XmlDocument doc)
        {
            XmlNode rootNode = doc.SelectSingleNode("//salesaccelerator");
            XmlNode attribute = doc.CreateAttribute("version");
            attribute.Value = _version.ToString();
            rootNode.Attributes.SetNamedItem(attribute);
        }

        private void SetSchemaLocation(XmlDocument doc)
        {
            XmlNode rootNode = doc.SelectSingleNode("//salesaccelerator");
            
            XmlAttribute attribute = doc.CreateAttribute("xsi", "noNamespaceSchemaLocation", "http://www.w3.org/2001/XMLSchema-instance");
            attribute.Value = _schemaLocation;
            rootNode.Attributes.SetNamedItem(attribute);
        }

        internal string ScrubForPublic(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            ScrubCustomerData(doc);
            SetVersion(doc);
            SetSchemaLocation(doc);

            return doc.InnerXml;
        }

        internal bool ValidateDocument(string xml)
        {
            using (XmlReader schemaReader = XmlReader.Create(typeof(InventoryDocumentVersionAdapter).Assembly.GetManifestResourceStream(EmbeddedSchemaResource)))
            {
                XmlSchemaSet set = new XmlSchemaSet();
                set.Add(null, schemaReader);

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas = set;
                settings.ValidationType = ValidationType.Schema;

                using(TextReader reader = new StringReader(xml))
                using(XmlReader xmlReader = XmlReader.Create(reader, settings))
                {
                    XmlDocument doc = new XmlDocument();

                    doc.Load(xmlReader);
                    doc.Validate((sender, args) =>
                                             {
                                                 if(args.Severity == XmlSeverityType.Error)
                                                     throw args.Exception;
                                             });
                }
            }

	        return true;
        }
      
        public InventoryDocumentT0 Create(string ownerHandle, string vehicleHandle)
        {
            return _dataAccess.Create(ownerHandle, vehicleHandle);
        }

        public InventoryDocumentT0 Fetch(string ownerHandle, string vehicleHandle)
        {
            return _dataAccess.Fetch(ownerHandle, vehicleHandle);
        }

        // TODO: Pull this out to an event handler. I don't really want the publishing to all happen here.
        //       Instead, simply raise an event and let the event handler deal with it.  ZB 1/17/14   
        internal void PublishInventoryXmlMessage(InventoryDocumentT0 dto)
        {
			// Send publish docs message
			Log.InfoFormat("Sending publish docs message for OwnerHandle: {0}, VehicleHandle: {1}", dto.OwnerHandle, dto.VehicleHandle);
	        try
	        {
		        var ctxt = MerchandisingContext.FromPricing(dto.OwnerHandle, dto.VehicleHandle);
		        BusFactory.CreatePublishDocumentsBus<MaxDocumentPublishMessage, MaxDocumentPublishMessage>()
			        .Publish(new MaxDocumentPublishMessage(ctxt.BusinessUnitId, ctxt.InventoryId, ctxt.Vin), "InventoryDocumentVersionAdapter");
	        }
	        catch (Exception ex)
	        {
		        Log.Error("Unable to send Publish Message.", ex);
	        }
        }

        public void Insert(InventoryDocumentT0 dto, string userName, bool validateXml)
        {
            dto.Xml = ScrubForPublic(dto.Xml);
            if(validateXml)
                ValidateDocument(dto.Xml);

            _dataAccess.Insert(dto, userName, validateXml);
			PublishInventoryXmlMessage(dto);

        }

        public void Update(InventoryDocumentT0 dto, string userName, bool validateXml)
        {
            dto.Xml = ScrubForPublic(dto.Xml);
            if(validateXml)
                ValidateDocument(dto.Xml);

            _dataAccess.Update(dto, userName, validateXml);
			PublishInventoryXmlMessage(dto);
        }

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            return _dataAccess.Exists(ownerHandle, vehicleHandle);
        }

        public void Delete(int id, string userName)
        {
            _dataAccess.Delete(id, userName);
        }
    }
}
