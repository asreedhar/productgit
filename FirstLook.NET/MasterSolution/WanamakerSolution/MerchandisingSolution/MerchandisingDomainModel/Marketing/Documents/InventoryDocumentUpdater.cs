﻿using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    public class InventoryDocumentUpdater
    {
        private PricingContext _context;
        private IMarketListingSearchAdapter _searchAdapter;
        private ILogger _logger;
        private string _user;
        
        public InventoryDocumentUpdater(PricingContext context, IMarketListingSearchAdapter searchAdapter, ILogger logger, string user)
        {
            _context = context;
            _searchAdapter = searchAdapter;
            _logger = logger;
            _user = user;
        }

        public static WorkItemsDTO[] GetWorkItems()
        {
            InventoryDocumentWorkItemsCommand workItemsCommand = new InventoryDocumentWorkItemsCommand();
            AbstractCommand.DoRun(workItemsCommand);
            return workItemsCommand.GetWorkItems();
        }

        public void Update()
        {
            InventoryDocument inventoryDoc = InventoryDocument.GetOrCreate(_context.OwnerHandle, _context.VehicleHandle);
            inventoryDoc.UserName = _user;
            //not going to validate the xml here because schemas not backwards compatible
            //EX: we are updating a 1.0 doc with a current 1.2 schema
            //should be ok because we are just updating market listings and they haven't changed.
            inventoryDoc.ValidateXml = false;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(inventoryDoc.Xml);
            var node = xmlDoc.SelectSingleNode("//salesaccelerator/marketlistings");
            var price = xmlDoc.SelectSingleNode("//salesaccelerator/pricing/gaugeprices/price[@isOfferPrice='true']/amount");

            var deal = new Deal.Deal(_logger, _searchAdapter, _context.OwnerHandle, _context.VehicleHandle, _context.SearchHandle);
            deal.OfferPrice = int.Parse(price.InnerText);
            deal.SaveXml(deal.GetMarketListings());
            var newMarketListingsDoc = deal.GetXml(NodeType.marketlistings);
            var newNode = newMarketListingsDoc.SelectSingleNode("marketlistings");

            node.InnerXml = newNode.InnerXml;
            inventoryDoc.Xml = xmlDoc.InnerXml;
            inventoryDoc.Save();
        }

    }
}
