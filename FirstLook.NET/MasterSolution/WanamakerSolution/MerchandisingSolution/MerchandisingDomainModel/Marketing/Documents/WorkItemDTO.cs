﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    public class WorkItemsDTO
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
    }
}
