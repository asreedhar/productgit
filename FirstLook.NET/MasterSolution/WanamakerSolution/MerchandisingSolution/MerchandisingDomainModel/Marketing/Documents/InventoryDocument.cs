﻿using System;
using Csla;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    [Serializable]
    public class InventoryDocument : InjectableBusinessBase<InventoryDocument>
    {
        private int _documentID;
        private string _xmlDoc;
        private string _ownerHandle;
        private string _vehicleHandle;
        private string _userName;

        //Injectable Property
        public IInventoryDocumentDataAccess DataAccess { get; set; }

        public bool ValidateXml { get; set; }

        public string UserName
        {
            get
            {
                if (_userName == null)
                    return ApplicationContext.User.Identity.Name;
                
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public string Xml
        {
            get
            {
                return _xmlDoc;
            }
            set
            {
                _xmlDoc = value;
                PropertyHasChanged("Xml");
            }
        }

        protected override object GetIdValue()
        {
            return _vehicleHandle;
        }

        public static InventoryDocument GetOrCreate(string ownerHandle, string vehicleHandle)
        {
            ExistsCommand command = new ExistsCommand(ownerHandle, vehicleHandle);
            DataPortal.Execute(command);

            InventoryDocument doc = command.Exists ? DataPortal.Fetch<InventoryDocument>(new Criteria(ownerHandle, vehicleHandle)) :
                                    DataPortal.Create<InventoryDocument>(new Criteria(ownerHandle, vehicleHandle));

            doc.ValidateXml = true;
            return doc;
        }

        private void DataPortal_Create(Criteria createCriteria)
        {
            var dto = DataAccess.Create(createCriteria.OwnerHandle, createCriteria.VehicleHandle);
            FromTransferObject(dto);

            MarkNew(); 
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            var dto = DataAccess.Fetch(criteria.OwnerHandle, criteria.VehicleHandle);

            FromTransferObject(dto);

            MarkOld();
        }

        protected override void DataPortal_Update()
        {
            if (!IsDirty)
                return;

            var dto = ToTransferObject();
            DataAccess.Update(dto, UserName, ValidateXml);

            MarkOld();
        }

        protected override void DataPortal_Insert()
        {
            var dto = ToTransferObject();

            DataAccess.Insert(dto, UserName, ValidateXml);
            
            FromTransferObject(dto);
            MarkOld();
        }

        private void FromTransferObject(InventoryDocumentT0 dto)
        {
            _documentID = dto.DocumentID;
            _xmlDoc = dto.Xml;
            _ownerHandle = dto.OwnerHandle;
            _vehicleHandle = dto.VehicleHandle;
        }

        private InventoryDocumentT0 ToTransferObject()
        {
            InventoryDocumentT0 transfer = new InventoryDocumentT0();
            transfer.DocumentID = _documentID;
            transfer.Xml = _xmlDoc;
            transfer.OwnerHandle = _ownerHandle;
            transfer.VehicleHandle = _vehicleHandle;

            return transfer;
        }

        [Serializable]
        private class Criteria
        {
            public string VehicleHandle { get; private set; }
            public string OwnerHandle { get; private set; }

            public Criteria(String ownerHandle, String vehicleHandle)
            {
                VehicleHandle = vehicleHandle;
                OwnerHandle = ownerHandle;
            }
        }

        [Serializable]
        private class ExistsCommand : InjectableCommandBase
        {
            private readonly String _vehicleHandle = String.Empty;
            private readonly String _ownerHandle = String.Empty;
            private Boolean _exists;

            //Injectable Property
            public IInventoryDocumentDataAccess DataAccess { get; set; }

            public bool Exists
            {
                get { return _exists; }
            }


            public ExistsCommand(String ownerHandle, String vehicleHandle)
            {
                _vehicleHandle = vehicleHandle;
                _ownerHandle = ownerHandle;
            }

            protected override void DataPortal_Execute()
            {
                _exists = DataAccess.Exists(_ownerHandle, _vehicleHandle);
            }
        }
    }
}
