﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    public class InventoryDocumentT0
    {
        public int DocumentID { get; set; }
        public String VehicleHandle { get; set; }
        public String OwnerHandle { get; set; }

        public string Xml { get; set; }
    }
}
