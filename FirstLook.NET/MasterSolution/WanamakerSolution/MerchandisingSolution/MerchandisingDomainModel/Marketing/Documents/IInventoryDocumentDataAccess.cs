﻿namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    public interface IInventoryDocumentDataAccess
    {
        InventoryDocumentT0 Create(string ownerHandle, string vehicleHandle);
        InventoryDocumentT0 Fetch(string ownerHandle, string vehicleHandle);
        void Insert(InventoryDocumentT0 dto, string userName, bool validateXml);
        void Update(InventoryDocumentT0 dto, string userName, bool validateXml);
        bool Exists(string ownerHandle, string vehicleHandle);
        void Delete(int id, string userName);
    }
}
