﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.Pdf;
using log4net.Repository.Hierarchy;
using Quartz;
using FirstLook.Common.Core.IOC;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    public class InventoryDocumentUpdaterJob
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string UpdateUser = "firstlook";

        private ILogger _logger;
        private IMarketListingSearchAdapter _searchAdapter;

        public InventoryDocumentUpdaterJob(IMarketListingSearchAdapter searchAdapter, ILogger logger)
        {
            _logger = logger;
            _searchAdapter = searchAdapter;
        }

        public void DoWork(int businessUnit, int inventoryID)
        {
            Log.DebugFormat("Updating Listings for BusinessUnitID = {0}, InventoryID = {1}", businessUnit, inventoryID);
            var pricingContext = PricingContext.FromMerchandising(businessUnit, inventoryID);
            var updater = new InventoryDocumentUpdater(pricingContext, _searchAdapter, _logger, UpdateUser);
            updater.Update();

            InventoryDocument inventoryDoc = InventoryDocument.GetOrCreate(pricingContext.OwnerHandle, pricingContext.VehicleHandle);
            var pdfManager = PdfManager.FromWebSitePdfXml(inventoryDoc.Xml, _logger);
            byte[] pdf = pdfManager.ToPdf();

            var command = new PublishValueAnalyzerDocument(pricingContext.OwnerHandle, pricingContext.VehicleHandle, pdf, UpdateUser, _logger);
            AbstractCommand.DoRun(command);
           
        }
    }
}
