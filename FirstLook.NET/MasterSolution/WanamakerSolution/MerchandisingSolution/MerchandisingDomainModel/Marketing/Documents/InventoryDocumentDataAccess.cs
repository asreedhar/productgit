﻿using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.Documents
{
    internal class InventoryDocumentDataAccess : IInventoryDocumentDataAccess
    {
        public InventoryDocumentT0 Create(string ownerHandle, string vehicleHandle)
        {
            //In the future, will want to create a document from the domain objects for this vehicle.
            InventoryDocumentT0 transfer = new InventoryDocumentT0();
            transfer.OwnerHandle = ownerHandle;
            transfer.VehicleHandle = vehicleHandle;
            transfer.Xml = string.Empty;
            transfer.DocumentID = -1;

            return transfer;
        }

        public InventoryDocumentT0 Fetch(string ownerHandle, string vehicleHandle)
        {
            var dto = new InventoryDocumentT0();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.InventoryDocuments#Fetch";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, ownerHandle);

                    dto.OwnerHandle = ownerHandle;
                    dto.VehicleHandle = vehicleHandle;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new DataException("Missing inventory document information");

                        dto.DocumentID = reader.GetInt32(reader.GetOrdinal("DocumentID"));
                        dto.Xml = reader.GetString(reader.GetOrdinal("DocumentXml"));
                    }
                }
            }

            return dto;
        }

        public void Insert(InventoryDocumentT0 dto, string userName, bool validateXml)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.InventoryDocuments#Insert";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("vehicleHandle", DbType.String, false, dto.VehicleHandle);
                        command.AddParameterWithValue("ownerHandle", DbType.String, false, dto.OwnerHandle);
                        command.AddParameterWithValue("XmlDoc", DbType.String, false, dto.Xml);
                        command.AddParameterWithValue("InsertUser", DbType.String, false, userName);

                        //Add the out param
                        IDataParameter newId = command.AddOutParameter("DocumentID", DbType.Int32);

                        // Execute and capture the ID
                        command.ExecuteNonQuery();
                        dto.DocumentID = (Int32)newId.Value;
                    }
                    transaction.Commit();
                }
            }
        }

        public void Update(InventoryDocumentT0 dto, string userName, bool validateXml)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.InventoryDocuments#Update";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("DocumentID", DbType.Int32, false, dto.DocumentID);
                        command.AddParameterWithValue("DocumentXml", DbType.String, false, dto.Xml);
                        command.AddParameterWithValue("UpdateUser", DbType.String, false, userName);

                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                connection.Open();
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.InventoryDocuments#Exists";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, ownerHandle);

                    // create return value parameter
                    IDataParameter parameter = command.CreateParameter();
                    parameter.ParameterName = "ReturnValue";
                    parameter.DbType = DbType.Int32;
                    parameter.Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add(parameter);

                    int count = 0;
                    command.ExecuteNonQuery();

                    IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }

                    return (count > 0);
                }
            }
        }

        public void Delete(int id, string userName)
        {
            throw new NotImplementedException();
        }
    }
}
