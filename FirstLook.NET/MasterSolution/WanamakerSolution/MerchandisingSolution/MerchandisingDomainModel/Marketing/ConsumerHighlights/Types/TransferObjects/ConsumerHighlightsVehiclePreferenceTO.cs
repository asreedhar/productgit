using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects
{
    /// <summary>
    /// Transfer object for consumer highlights vehicle preferences.
    /// </summary>
    [Serializable]
    public class ConsumerHighlightsVehiclePreferenceTO : IConsumerHighlightsVehiclePreference
    {
        #region Properties

        /// <summary>
        /// Vehicle preference identifier.
        /// </summary>
        public int ConsumerHighlightVehiclePreferenceId
        {
            get { return _consumerHighlightVehiclePreferenceId; }
            internal set { _consumerHighlightVehiclePreferenceId = value; }
        }
        private int _consumerHighlightVehiclePreferenceId;

        /// <summary>
        /// Owner handle.
        /// </summary>
        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }
        private readonly string _ownerHandle;

        /// <summary>
        /// Vehicle handle.
        /// </summary>
        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }
        private readonly string _vehicleHandle;

        /// <summary>
        /// Are these vehicle preferences displayed?
        /// </summary>
        public bool IsDisplayed
        {
            get { return _isDisplayed; }
            set { _isDisplayed = value; }
        }
        private bool _isDisplayed;

        /// <summary>
        /// Is the Carfax One Owner icon displayed?
        /// </summary>
        public bool IncludeCarfaxOneOwnerIcon
        {
            get { return _includeCarfaxOneOwnerIcon; }
            set { _includeCarfaxOneOwnerIcon = value; }
        }
        private bool _includeCarfaxOneOwnerIcon;

        /// <summary>
        /// Date at which these preferences were either inserted or most recently updated.
        /// </summary>
        public DateTime ChangedDate
        {
            get { return _changedDate; }
            set { _changedDate = value; }
        }
        private DateTime _changedDate;

        /// <summary>
        /// Highlight section ordering.
        /// </summary>
        public List<HighlightSectionType> SectionOrdering
        {
            get; set;
        }

        /// <summary>
        /// Free text highlights.
        /// </summary>
        public List<FreeTextHighlightTO> FreeTextHighlights
        {
            get; set;
        }

        /// <summary>
        /// Vehicle history highlights.
        /// </summary>
        public List<VehicleHistoryHighlightTO> VehicleHistoryHighlights
        {
            get; set;
        }

        /// <summary>
        /// Snippet highlights.
        /// </summary>
        public List<SnippetHighlightTO> SnippetHighlights
        {
            get; set;
        }

        /// <summary>
        /// Circle rating highlights.
        /// </summary>
        public List<CircleRatingHighlightTO> CircleRatingHighlights
        {
            get; set;
        }

        #endregion        

        #region Construction

        /// <summary>
        /// Construct a new vehicle preference transfer object.
        /// </summary>
        /// <param name="consumerHighlightsVehiclePreferenceId">Vehicle preference identifier.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="isDisplayed">Should these vehicle preferences be displayed?</param>
        /// <param name="includeCarfaxOneOwnerIcon">Should the Carfax 1-Owner icon be included?</param>
        /// <param name="changedDate">Date at which these preferences were either inserted or most recently updated.</param>
        /// <param name="sectionOrdering">Highlight section type ordering.</param>
        /// <param name="circleRatingHighlights">Circle rating highlights.</param>
        /// <param name="freeTextHighlights">Free text highlights.</param>
        /// <param name="snippetHighlights">Snippet highlights.</param>
        /// <param name="vehicleHistoryHighlights">Vehicle history highlights.</param>
        internal ConsumerHighlightsVehiclePreferenceTO(int? consumerHighlightsVehiclePreferenceId, string ownerHandle,
                                                       string vehicleHandle, bool isDisplayed, bool includeCarfaxOneOwnerIcon, 
                                                       DateTime changedDate,
                                                       List<HighlightSectionType> sectionOrdering,
                                                       List<CircleRatingHighlightTO> circleRatingHighlights,
                                                       List<FreeTextHighlightTO> freeTextHighlights,
                                                       List<SnippetHighlightTO> snippetHighlights,
                                                       List<VehicleHistoryHighlightTO> vehicleHistoryHighlights)
        {
            if (consumerHighlightsVehiclePreferenceId.HasValue)
            {
                _consumerHighlightVehiclePreferenceId = consumerHighlightsVehiclePreferenceId.Value;
            }

            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _isDisplayed = isDisplayed;
            _includeCarfaxOneOwnerIcon = includeCarfaxOneOwnerIcon;
            _changedDate = changedDate;

            SectionOrdering = sectionOrdering;
            CircleRatingHighlights = circleRatingHighlights;
            FreeTextHighlights = freeTextHighlights;
            SnippetHighlights = snippetHighlights;
            VehicleHistoryHighlights = vehicleHistoryHighlights;
        }

        #endregion        

        #region Highlight Retrieval

        /// <summary>
        /// Get all consumer highlights. All circle rating, free text, snippet and vehicle history highlights will be
        /// added in order of section ordering preferences.
        /// </summary>
        /// <remarks>        
        /// </remarks>
        /// <returns>Enumeration of all highlights.</returns>
        public IEnumerable<IConsumerHighlight> GetHighlights()
        {
            foreach (HighlightSectionType sectionType in SectionOrdering)
            {
                switch (sectionType)
                {
                    case HighlightSectionType.CircleRatings:
                        foreach (CircleRatingHighlightTO to in CircleRatingHighlights)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.FreeText:
                        foreach (FreeTextHighlightTO to in FreeTextHighlights)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.Snippets:
                        foreach (SnippetHighlightTO to in SnippetHighlights)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.VehicleHistory:
                        foreach (VehicleHistoryHighlightTO to in VehicleHistoryHighlights)
                        {
                            yield return to;
                        }
                        break;
                }
            }
        }

        #endregion
    }
}
