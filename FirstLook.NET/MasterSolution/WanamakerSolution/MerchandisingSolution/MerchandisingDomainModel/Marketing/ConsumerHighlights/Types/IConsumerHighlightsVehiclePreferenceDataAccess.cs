using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types
{
    /// <summary>
    /// Interface for consumer highlights vehicle preference data operations.
    /// </summary>
    public interface IConsumerHighlightsVehiclePreferenceDataAccess
    {
        #region Vehicle Preference Methods

        /// <summary>
        /// Do vehicle preferences exist for the owner and vehicle with the given handles?
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>True if preferences exist, false otherwise.</returns>
        bool Exists(string ownerHandle, string vehicleHandle);

        /// <summary>
        /// Fetch the vehicle preferences for the owner and vehicle with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Vehicle preference transfer object.</returns>
        ConsumerHighlightsVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle);

        /// <summary>
        /// Insert vehicle preferences to the database.
        /// </summary>
        /// <param name="preference">Vehicle preferences to save.</param>
        /// <param name="userName">Name of the user performing this operation.</param>
        void Insert(ConsumerHighlightsVehiclePreferenceTO preference, string userName);

        /// <summary>
        /// Update the given vehicle preferences to the database.
        /// </summary>
        /// <param name="preference">Vehicle preference.</param>
        /// <param name="userName">Name of the user performing this operation.</param>
        void Update(ConsumerHighlightsVehiclePreferenceTO preference, string userName);

        /// <summary>
        /// Delete the given vehicle preferences from the database.
        /// </summary>
        /// <param name="preference">Vehicle preferences to delete.</param>
        /// <param name="userName">Name of the user performing this operation.</param>
        void Delete(ConsumerHighlightsVehiclePreferenceTO preference, string userName);

        #endregion

        #region Vehicle History Report Methods

        /// <summary>
        /// Get all vehicle history report highlights for the owner and vehicle with the given handles. Will get the 
        /// highlights from both Carfax and AutoCheck.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Vehicle history report highlights.</returns>
        List<VehicleHistoryHighlightTO> GetVehicleHistoryHighlights(string ownerHandle, string vehicleHandle);

        /// <summary>
        /// Get the Carfax report highlights for the owner and vehicle with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="startRank">Starting rank that should be applied to retrieved highlights.</param>
        /// <returns>List of Carfax report items as consumer highlights transfer objects.</returns>
        List<VehicleHistoryHighlightTO> GetCarfaxHighlights(string ownerHandle, string vehicleHandle, int startRank);

        /// <summary>
        /// Get the Autocheck report highlights for the owner and vehicle with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="startRank">Starting rank that should be applied to retrieved highlights.</param>
        /// <returns>List of AutoCheck report items as consumer highlight transfer objects.</returns>
        List<VehicleHistoryHighlightTO> GetAutoCheckHighlights(string ownerHandle, string vehicleHandle, int startRank);

        #endregion

        #region Snippet Methods

        /// <summary>
        /// Get the marketing text snippets (aka 'Expert Reviews & Awards') for the owner and vehicle with the given
        /// handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>        
        /// <returns>List of snippets as consumer highlight transfer objects.</returns>
        List<SnippetHighlightTO> FetchSnippets(string ownerHandle, string vehicleHandle);

        #endregion

        #region Circle Rating Methods

        /// <summary>
        /// Get the JD Power circle rating highlights for the owner and vehicle with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>        
        /// <returns>List of JD Power circle rating items as consumer highlight transfer objects.</returns>
        List<CircleRatingHighlightTO> FetchCircleRatings(string ownerHandle, string vehicleHandle);

        #endregion
    }
}
