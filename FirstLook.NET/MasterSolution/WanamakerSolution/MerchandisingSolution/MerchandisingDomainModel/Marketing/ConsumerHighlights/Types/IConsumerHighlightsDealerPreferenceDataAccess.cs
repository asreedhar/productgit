﻿using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types
{
    /// <summary>
    /// Interface that defines the data access of consumer highlights dealer preferences.
    /// </summary>
    public interface IConsumerHighlightsDealerPreferenceDataAccess
    {
        /// <summary>
        /// Create a new set of preferences for the owner with the given handle.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>Dealer preferences object.</returns>
        ConsumerHighlightsDealerPreferenceTO Create(string ownerHandle);

        /// <summary>
        /// Do preferences exist for the owner with the given handle?
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>True if preferences exist, false otherwise.</returns>
        bool Exists(string ownerHandle);
        
        /// <summary>
        /// Fetch the consumer highglights dealer preferences for the owner with the given handle.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>Dealer preferences object.</returns>
        ConsumerHighlightsDealerPreferenceTO Fetch(string ownerHandle);

        /// <summary>
        /// Save the dealer preferences.
        /// </summary>        
        /// <param name="preference">Consumer highlights dealer preferences.</param>
        void Save(ConsumerHighlightsDealerPreferenceTO preference);       
    }
}
