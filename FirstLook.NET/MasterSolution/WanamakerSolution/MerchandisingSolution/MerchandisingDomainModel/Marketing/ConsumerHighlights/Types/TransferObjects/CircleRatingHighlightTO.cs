﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects
{
    /// <summary>
    /// JD Power circle rating highlight.
    /// </summary>
    [Serializable]
    public class CircleRatingHighlightTO : ConsumerHighlightTO
    {        
        #region Consumer Highlight Properties

        /// <summary>
        /// The section this highlight will be displayed in.
        /// </summary>
        public override HighlightSectionType HighlightSection
        {
            get { return HighlightSectionType.CircleRatings; }
        }

        /// <summary>
        /// Provider type of this highlight.
        /// </summary>
        public override HighlightType Provider
        {
            get { return HighlightType.CircleRating; }
        }

        /// <summary>
        /// The identifier of the row in the provider's source table.
        /// </summary>
        public override int? ProviderSourceRowId
        {
            get { return _providerSourceRowId; }
        }
        private readonly int? _providerSourceRowId;

        /// <summary>
        /// Deletion status of this highlight. Not supported.
        /// </summary>
        public override bool IsDeleted
        {
            get { return false; }
            set { throw new NotSupportedException("JD Power circle rating highlights may not be deleted.");}
        }

        /// <summary>
        /// Can this highlight be deleted?
        /// </summary>
        public override bool CanDelete
        {
            get { return false; }
        }

        /// <summary>
        /// Can this highlight be edited?
        /// </summary>
        public override bool CanEdit
        {
            get { return false; }
        }

        #endregion

        #region Circle Rating Highlight Properties

        /// <summary>
        /// JD Power circle rating value.
        /// </summary>
        public double RatingValue { get; set; }

        /// <summary>
        /// The type of the JD Power circle rating value.
        /// </summary>
        public string RatingDescription { get; set; }

        /// <summary>
        /// Date at which this circle rating was created in its source table.
        /// </summary>
        public DateTime CreateDate { get; set; }

        #endregion

        #region Construction

        /// <summary>
        /// Parameter-less constructor for serialization.
        /// </summary>
        public CircleRatingHighlightTO()
        {            
        }

        /// <summary>
        /// Set the base class values of a consumer highlight transfer object.
        /// </summary>
        /// <param name="highlightId">Consumer highlight identifier.</param>
        /// <param name="preferenceId">Vehicle preference identifier.</param>
        /// <param name="rank">Ordering rank.</param>        
        /// <param name="isDisplayed">Is the highlight displayed.</param>
        /// <param name="ratingValue">Circle rating value.</param>
        /// <param name="ratingDescription">Type of the rating value.</param>
        /// <param name="sourceRowId">Identifier into the provider source table.</param>        
        /// <param name="createDate">Date at which this circle rating was created in its source table.</param>
        public CircleRatingHighlightTO(int highlightId, int preferenceId, int rank, bool isDisplayed, 
                                       double ratingValue, string ratingDescription, int sourceRowId, 
                                       DateTime createDate)
            : base(highlightId, preferenceId, rank, isDisplayed, (int)HighlightType.CircleRating + ":" + sourceRowId)
        {
            RatingValue          = ratingValue;
            RatingDescription    = ratingDescription;
            CreateDate           = createDate;
            _providerSourceRowId = sourceRowId;
        }

        #endregion        
    }
}