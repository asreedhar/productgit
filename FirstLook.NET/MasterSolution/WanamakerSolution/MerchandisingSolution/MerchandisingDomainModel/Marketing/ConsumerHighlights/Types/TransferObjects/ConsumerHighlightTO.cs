using System;
using System.Collections;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects
{
    /// <summary>
    /// Consumer highlight abstract base class.
    /// </summary>
    [Serializable]
    public abstract class ConsumerHighlightTO : IConsumerHighlight, IComparer, IComparable
    {
        #region Abstract Properties

        /// <summary>
        /// The section this highlight will be displayed in.
        /// </summary>
        public abstract HighlightSectionType HighlightSection { get; }

        /// <summary>
        /// Provider type of this highlight.
        /// </summary>
        public abstract HighlightType Provider { get; }

        /// <summary>
        /// The identifier of the row in the provider's source table.
        /// </summary>
        public abstract int? ProviderSourceRowId { get; }

        /// <summary>
        /// Can this highlight be edited?
        /// </summary>
        public abstract bool CanEdit { get; }

        /// <summary>
        /// Can this highlight be deleted?
        /// </summary>
        public abstract bool CanDelete { get; }

        /// <summary>
        /// Has this highlight been marked for deletion?
        /// </summary>
        public abstract bool IsDeleted { get; set; }        

        #endregion

        #region Properties

        /// <summary>
        /// Identifier for a highlight - best for when a proper identifier hasn't been assigned.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Identifier of this highlight.
        /// </summary>
        public int ConsumerHighlightId { get; set; }        

        /// <summary>
        /// Identifier of the vehicle preference associated with this highlight.
        /// </summary>
        public int VehiclePreferenceId { get; set; }

        /// <summary>
        /// Ordered rank of this highlight with respect to others of the same type.
        /// </summary>
        public int Rank { get; set; }        

        /// <summary>
        /// Is this highlight displayed?
        /// </summary>
        public bool IsDisplayed { get; set; }                                    

        #endregion

        #region Construction

        /// <summary>
        /// Parameter-less constructor for serialization.
        /// </summary>
        protected ConsumerHighlightTO()
        {            
        }

        /// <summary>
        /// Set the base class values of a consumer highlight transfer object. Key is assigned to the same value as
        /// the consumer highlight identifier.
        /// </summary>
        /// <param name="highlightId">Consumer highlight identifier.</param>
        /// <param name="preferenceId">Vehicle preference identifier.</param>
        /// <param name="rank">Ordering rank.</param>        
        /// <param name="isDisplayed">Is the highlight displayed?</param>
        /// <param name="key">Identifier to be used when highlightId has not yet been assigned.</param>
        protected ConsumerHighlightTO(int highlightId, int preferenceId, int rank, bool isDisplayed, string key)            
        {
            ConsumerHighlightId = highlightId;
            VehiclePreferenceId = preferenceId;
            Rank                = rank;            
            IsDisplayed         = isDisplayed;
            Key                 = key;
        }

        #endregion

        #region Comparison

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than 
        /// the other.
        /// </summary>
        /// <returns>
        /// Value Condition Less than zero <paramref name="x"/> is less than <paramref name="y"/>. 
        /// Zero <paramref name="x"/> equals <paramref name="y"/>. Greater than zero <paramref name="x"/> is greater 
        /// than <paramref name="y"/>. 
        /// </returns>
        /// <param name="x">The first object to compare. </param>
        /// <param name="y">The second object to compare.</param>
        /// <exception cref="T:System.ArgumentException">
        /// Neither <paramref name="x"/> nor <paramref name="y"/> 
        /// implements the <see cref="T:System.IComparable"/> interface.-or- <paramref name="x"/> and 
        /// <paramref name="y"/> are of different types and neither one can handle comparisons with the other.
        /// </exception>
        /// <filterpriority>2</filterpriority>
        public int Compare(object x, object y)
        {
            ConsumerHighlightTO xTo = x as ConsumerHighlightTO;
            ConsumerHighlightTO yTo = y as ConsumerHighlightTO;

            if (xTo == null || yTo == null)
            {
                throw new ArgumentException("Compared objects must be of type ConsumerHighlightTO");
            }

            if (xTo.Rank < yTo.Rank)
            {
                return -1;
            }
            if (xTo.Rank > yTo.Rank)
            {
                return 1;
            }
            return 0;
        }        

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than <paramref name="obj"/>. Zero This instance is equal to <paramref name="obj"/>. Greater than zero This instance is greater than <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. </param><exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. </exception><filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            return Compare(this, obj);
        }

        #endregion
    }
}
