﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects
{
    /// <summary>
    /// Snippet (a.k.a. 'Expert Reviews & Awards') consumer highlight.
    /// </summary>
    [Serializable]
    public class SnippetHighlightTO : ConsumerHighlightTO
    {
        #region Consumer Highlight Properties

        /// <summary>
        /// The section this highlight will be displayed in.
        /// </summary>
        public override HighlightSectionType HighlightSection
        {
            get { return HighlightSectionType.Snippets; }
        }

        /// <summary>
        /// Provider type of this highlight.
        /// </summary>
        public override HighlightType Provider
        {
            get { return HighlightType.Snippet; }
        }

        /// <summary>
        /// The identifier of the row in the provider's source table.
        /// </summary>
        public override int? ProviderSourceRowId
        {
            get { return _providerSourceRowId; }
        }
        private readonly int? _providerSourceRowId;

        /// <summary>
        /// Deletion status of this highlight. Not supported.
        /// </summary>
        public override bool IsDeleted
        {
            get { return false; }
            set { throw new NotSupportedException("Snippet highlights may not be deleted."); }
        }

        /// <summary>
        /// Can this highlight be deleted?
        /// </summary>
        public override bool CanDelete
        {
            get { return false; }
        }

        /// <summary>
        /// Can this highlight be edited?
        /// </summary>
        public override bool CanEdit
        {
            get { return false; }
        }

        #endregion

        #region Snippet Highlight Properties

        /// <summary>
        /// Text of the snippet.
        /// </summary>
        public string Snippet { get; set; }

        /// <summary>
        /// Identifier of the source of this snippet.
        /// </summary>
        public int SnippetSourceId { get; set; }

        /// <summary>
        /// Name of the source of this snippet.
        /// </summary>
        public string SnippetSourceName { get; set; }        

        /// <summary>
        /// Date at which this snippet was created in its source table.
        /// </summary>
        public DateTime CreateDate { get; set; }

        #endregion   
     
        #region Construction

        /// <summary>
        /// Parameter-less constructor for serialization.
        /// </summary>
        public SnippetHighlightTO()
        {            
        }

        /// <summary>
        /// Set the base class values of a consumer highlight transfer object.
        /// </summary>
        /// <param name="highlightId">Consumer highlight identifier.</param>
        /// <param name="preferenceId">Vehicle preference identifier.</param>
        /// <param name="rank">Ordering rank.</param>        
        /// <param name="isDisplayed">Is the highlight displayed.</param>
        /// <param name="providerSourceRowId">Identifier into the provider source table.</param>
        /// <param name="sourceId">Identifier of the source of the snippet.</param>
        /// <param name="sourceName">Name of the source of the snippet.</param>
        /// <param name="snippet">Snippet text.</param>
        /// <param name="createDate">Date at which this snippet was created in its source table.</param>
        public SnippetHighlightTO(int highlightId, int preferenceId, int rank, bool isDisplayed,
                                  int providerSourceRowId, int sourceId, string sourceName, string snippet,
                                  DateTime createDate)
            : base(highlightId, preferenceId, rank, isDisplayed, (int)HighlightType.Snippet + ":" + providerSourceRowId)
        {
            _providerSourceRowId = providerSourceRowId;
            SnippetSourceId      = sourceId;
            SnippetSourceName    = sourceName;
            Snippet              = snippet;
            CreateDate           = createDate;
        }

        #endregion
    }
}
