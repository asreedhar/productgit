using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types
{
    /// <summary>
    /// Interface for consumer highlights vehicle preferences.
    /// </summary>
    public interface IConsumerHighlightsVehiclePreference
    {
        /// <summary>
        /// Vehicle preference identifier.
        /// </summary>
        int ConsumerHighlightVehiclePreferenceId { get;}

        /// <summary>
        /// Owner handle.
        /// </summary>
        string OwnerHandle { get;}

        /// <summary>
        /// Vehicle handle.
        /// </summary>
        string VehicleHandle { get;}

        /// <summary>
        /// Are these vehicle preferences displayed?
        /// </summary>
        bool IsDisplayed { get; set;}

        /// <summary>
        /// Should the "Carfax 1-Owner" icon be included?
        /// </summary>
        bool IncludeCarfaxOneOwnerIcon { get; set;}

        /// <summary>
        /// Most recent date and time this 
        /// </summary>
        DateTime ChangedDate { get; set; }

        /// <summary>
        /// Ordering of consumer highlight sections.
        /// </summary>
        List<HighlightSectionType> SectionOrdering { get; }

        /// <summary>
        /// Get all consumer highlights of every type. The order in which each type is added to the enumeration is
        /// defined by the section ordering preferences.
        /// </summary>
        /// <returns>Enumeration of all highlights.</returns>
        IEnumerable<IConsumerHighlight> GetHighlights();

        /// <summary>
        /// Free text highlights.
        /// </summary>
        List<FreeTextHighlightTO> FreeTextHighlights { get; }

        /// <summary>
        /// Vehicle history highlights.
        /// </summary>
        List<VehicleHistoryHighlightTO> VehicleHistoryHighlights { get; }

        /// <summary>
        /// Snippet highlights.
        /// </summary>
        List<SnippetHighlightTO> SnippetHighlights { get; }

        /// <summary>
        /// JDPower Circle rating highlights.
        /// </summary>
        List<CircleRatingHighlightTO> CircleRatingHighlights { get; }
    }
}
