﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects
{
    /// <summary>
    /// Transfer object for consumer highlights dealer preferences.
    /// </summary>
    public class ConsumerHighlightsDealerPreferenceTO
    {       
        /// <summary>
        /// The identifier of the owner to whom these preferences apply.
        /// </summary>
        public string OwnerHandle { get; set; }

        /// <summary>
        /// Minimum number of circles required to display the JD Power circle rating for a highlight.
        /// </summary>
        public double MinimumJdPowerCircleRating { get; set; }

        public int MaxVHRItemsInitalDisplay { get; set; }

        /// <summary>
        /// Ordering of displayed marketing text sources.
        /// </summary>
        public List<SnippetSource> DisplayedSnippetSources { get; set; }

        /// <summary>
        /// Ordering of hidden marketing text sources.
        /// </summary>
        public List<SnippetSource> HiddenSnippetSources { get; set; }

        /// <summary>
        /// Ordering of highlight sections.
        /// </summary>
        public List<HighlightSection> HighlightSections { get; set; }

        /// <summary>
        /// Ordering of displayed marketing text sources.
        /// </summary>
        public List<VehicleHistoryReportSource> DisplayedVehicleHistoryReportSources { get; set; }

        /// <summary>
        /// Ordering of hidden marketing text sources.
        /// </summary>
        public List<VehicleHistoryReportSource> HiddenVehicleHistoryReportSources { get; set; }

    }
}
