using FirstLook.Common.Core.DTO;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types
{
    /// <summary>
    /// Interface for consumer highlights.
    /// </summary>
    public interface IConsumerHighlight : ISurrogateKey
    {        
        /// <summary>
        /// Identifier of this highlight.
        /// </summary>
        int ConsumerHighlightId { get; }

        /// <summary>
        /// Identifier of the parent vehicle preferences.
        /// </summary>
        int VehiclePreferenceId { get; }

        /// <summary>
        /// The section this highlight will be displayed in.
        /// </summary>
        HighlightSectionType HighlightSection { get; }

        /// <summary>
        /// Provider type of this highlight.
        /// </summary>
        HighlightType Provider { get; }        

        /// <summary>
        /// The identifier of the row in the provider's source table.
        /// </summary>
        int? ProviderSourceRowId { get; }        

        /// <summary>
        /// Ordering of this rank.
        /// </summary>
        int Rank { get; }

        /// <summary>
        /// Is this highlight displayed?
        /// </summary>
        bool IsDisplayed { get; }        

        /// <summary>
        /// Has this highlight been marked for deletion?
        /// </summary>
        bool IsDeleted { get; }

        /// <summary>
        /// Can this highlight be deleted?
        /// </summary>
        bool CanDelete { get; }

        /// <summary>
        /// Can this highlight be edited?
        /// </summary>
        bool CanEdit { get; }
    }
}
