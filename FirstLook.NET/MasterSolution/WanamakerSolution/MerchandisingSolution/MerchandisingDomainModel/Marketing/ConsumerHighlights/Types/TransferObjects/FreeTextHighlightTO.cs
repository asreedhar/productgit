﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects
{
    /// <summary>
    /// Text entered by the dealer to highlight a vehicle.
    /// </summary>
    [Serializable]
    public class FreeTextHighlightTO : ConsumerHighlightTO
    {
        #region Consumer Highlight Properties

        /// <summary>
        /// The section this highlight will be displayed in.
        /// </summary>
        public override HighlightSectionType HighlightSection
        {
            get { return HighlightSectionType.FreeText; }
        }

        /// <summary>
        /// Provider type of this highlight.
        /// </summary>
        public override HighlightType Provider
        {
            get { return HighlightType.FreeText; }
        }

        /// <summary>
        /// The identifier of the row in the provider's source table.
        /// </summary>
        public override int? ProviderSourceRowId
        {
            get { return null; }
        }

        /// <summary>
        /// Deletion status of this highlight.
        /// </summary>
        public override bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }
        private bool _isDeleted;

        /// <summary>
        /// Can this highlight be deleted?
        /// </summary>
        public override bool CanDelete 
        { 
            get { return true;}
        }

        /// <summary>
        /// Can this highlight be edited?
        /// </summary>
        public override bool CanEdit
        {
            get { return true; }
        }

        #endregion

        #region Free Text Highlight Properties

        /// <summary>
        /// Text entered by the dealer.
        /// </summary>
        public string Text { get; set; }

        #endregion

        #region Construction

        /// <summary>
        /// Parameter-less constructor for serialization.
        /// </summary>
        public FreeTextHighlightTO()
        {            
        }

        /// <summary>
        /// Create a new free-text highlight transfer object.
        /// </summary>
        /// <param name="highlightId">Consumer highlight identifier.</param>
        /// <param name="preferenceId">Vehicle preference identifier.</param>
        /// <param name="rank">Ordering rank.</param>        
        /// <param name="isDisplayed">Is this highlight displayed?</param>
        /// <param name="text">Dealer-entered free text.</param>
        public FreeTextHighlightTO(int highlightId, int preferenceId, int rank, bool isDisplayed, string text) :
            base(highlightId, preferenceId, rank, isDisplayed, (int)HighlightType.FreeText + ":" + highlightId)
        {
            Text = text;
        }

        #endregion
    }
}
