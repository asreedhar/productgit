﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects
{
    /// <summary>
    /// Vehicle history highlight. Possible types are Carfax, AutoCheck.
    /// </summary>
    [Serializable]
    public class VehicleHistoryHighlightTO : ConsumerHighlightTO
    {
        #region Consumer Highlight Properties

        /// <summary>
        /// The section this highlight will be displayed in.
        /// </summary>
        public override HighlightSectionType HighlightSection
        {
            get { return HighlightSectionType.VehicleHistory; }
        }

        /// <summary>
        /// Provider type of this highlight.
        /// </summary>
        public override HighlightType Provider
        {
            get { return _provider; }            
        }
        private readonly HighlightType _provider;

        /// <summary>
        /// The identifier of the row in the provider's source table.
        /// </summary>
        public override int? ProviderSourceRowId
        {
            get { return null; }
        }

        /// <summary>
        /// Deletion status of this highlight. Not supported.
        /// </summary>
        public override bool IsDeleted
        {
            get { return false; }
            set { throw new NotSupportedException("Vehicle history highlights may not be deleted."); }
        }

        /// <summary>
        /// Can this highlight be deleted?
        /// </summary>
        public override bool CanDelete
        {
            get { return false; }
        }

        /// <summary>
        /// Can this highlight be edited?
        /// </summary>
        public override bool CanEdit
        {
            get { return false; }
        }

        #endregion

        #region Vehicle History Highlight Properties

        /// <summary>
        /// Highlight text from the vehicle history report.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Expiration date of this vehicle history report.
        /// </summary>
        public DateTime? ExpirationDate { get; set; }

        #endregion     
  
        #region Construction

        /// <summary>
        /// Parameter-less constructor for serialization.
        /// </summary>
        public VehicleHistoryHighlightTO()
        {            
        }

        /// <summary>
        /// Set the base class values of a consumer highlight transfer object. Key is not set.
        /// </summary>
        /// <param name="highlightId">Consumer highlight identifier.</param>
        /// <param name="preferenceId">Vehicle preference identifier.</param>
        /// <param name="rank">Ordering rank.</param>
        /// /// <param name="isDisplayed">Is the highlight displayed.</param>
        /// <param name="expirationDate">Expiration date, if applicable.</param>        
        /// <param name="provider">Type of vehicle history report highlight this is.</param>
        /// <param name="text">Vehicle hilight text.</param>
        /// <param name="key">Identifier used when highlightId has not been assigned.</param>
        public VehicleHistoryHighlightTO(int highlightId, int preferenceId, int rank, bool isDisplayed,
                                         DateTime? expirationDate, HighlightType provider, string text, string key)
            : base(highlightId, preferenceId, rank, isDisplayed, key)
        {
            if (provider != HighlightType.AutoCheck && provider != HighlightType.CarFax)
            {
                throw new ArgumentException("Vehicle history report highlights may only be from AutoCheck or CarFax");
            }

            Text           = text;
            ExpirationDate = expirationDate;
            _provider      = provider;            
        }

        #endregion
    }
}
