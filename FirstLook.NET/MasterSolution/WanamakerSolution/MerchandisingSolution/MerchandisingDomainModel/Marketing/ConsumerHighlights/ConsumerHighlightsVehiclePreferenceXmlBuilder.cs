using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Renders xml from consumer highlights. Can process each highlight type individually or all highlight types at
    /// the same time.
    /// </summary>
    public class ConsumerHighlightsVehiclePreferenceXmlBuilder
    {
        // Constants.
        public static readonly string LocalImageRoot = ConfigurationManager.AppSettings["photo_local_root_url"];
        public static readonly string CarfaxLogoUrl = LocalImageRoot + "logo_CarFaxOneOwner.gif";

        /// <summary>
        /// Text of the Carfax 1-Owner highlight to find when rendering vehicle highlights.
        /// </summary>
        private const string ONE_OWNER = "CARFAX 1-Owner";

        #region Example XML

        /*
        <consumerhighlights display="True">

            <circleratinghighlights rank="0">
                <highlight>
                    <description>Initial Quality Survey</description>
                    <value>3.5</value>
                </highlight>
                <highlight>
                    <description>Final Quality Survey</description>
                    <value>2.0</value>
                </highlight>
            </circleratinghighlights>
            
            <freetexthighlights rank="1">
                <highlight>
                    <text>Spontaneously Combusts!</text>
                </highlight>
                <highlight>
                    <text>Flame Retardant!</text>
                </highlight>
            </freetexthighlights>

            <snippethighlights rank="2">
                <highlight>
                    <source>MotorTrend.com</source>
                    <snippet>Car Of The Year 2010</snippet>
                </highlight>
                <highlight>
                    <source>CarAndDriver.com</source>
                    <snippet>Cures World Hunger</snippet>
                </highlight>
            </snippethighlights>
            
            <vehiclehistoryhighlights showCarFaxLogo="True" carFaxLogoUrl="http://www.firstlook.biz/logo.gif" rank="3">
                <highlight>
                    <text>CARFAX 1-Owner</text>
                    <source>Carfax</source>
                </highlight>
                <highlight>
                    <text>Original Chasis</text>
                    <source>AutoCheck</source>
                </highlight>
            </vehiclehistoryhighlights>

        </consumerhighlights>        
        */

        #endregion

        #region Consumer Highlights Redenering

        /// <summary>
        /// Build xml of all the consumer highlights in the given vehicle preferences.
        /// </summary>
        /// <param name="preference">Vehicle preferences.</param>        
        /// <param name="showOnlyDisplayed">Should only displayed highlights be included in the xml?</param>
        /// <returns>Consumer highlights as an xml string.</returns>
        public static string AsXml(IConsumerHighlightsVehiclePreference preference, bool showOnlyDisplayed)
        {
            return AsXml(preference, showOnlyDisplayed, String.Empty);
        }
        
        /// <summary>
        /// Build xml of all the consumer highlights in the given vehicle preferences.  The caller may specify the url 
        /// of the Carfax logo.
        /// </summary>
        /// <param name="preference">Vehicle preferences.</param>        
        /// <param name="showOnlyDisplayed">Should only displayed highlights be included in the xml?</param>
        /// <param name="carFaxLogoUrl">Url of the Carfax logo.</param>
        /// <returns>Consumer highlights as an xml string.</returns>
        public static string AsXml(IConsumerHighlightsVehiclePreference preference, bool showOnlyDisplayed, 
                                   string carFaxLogoUrl)
        {
            if (preference == null || preference.SectionOrdering == null || preference.SectionOrdering.Count == 0)
            {
                return string.Empty;
            }

            StringBuilder highlightsStringBuilder = new StringBuilder();

            int i = 0;
            foreach (HighlightSectionType sectionType in preference.SectionOrdering)
            {
                switch (sectionType)
                {
                    case HighlightSectionType.CircleRatings:
                        highlightsStringBuilder.Append(CircleRatingHighlightsAsXml(preference.CircleRatingHighlights, 
                                                                                   showOnlyDisplayed, i));
                        break;

                    case HighlightSectionType.FreeText:
                        highlightsStringBuilder.Append(FreeTextHighlightsAsXml(preference.FreeTextHighlights, 
                                                                               showOnlyDisplayed, i));
                        break;

                    case HighlightSectionType.Snippets:
                        highlightsStringBuilder.Append(SnippetHighlightsAsXml(preference.SnippetHighlights, 
                                                                              showOnlyDisplayed, i));
                        break;

                    case HighlightSectionType.VehicleHistory:
                        highlightsStringBuilder.Append(VehicleHistoryHighlightsAsXml(preference.VehicleHistoryHighlights,
                                                preference.IncludeCarfaxOneOwnerIcon, showOnlyDisplayed, carFaxLogoUrl, i));
                        break;
                }
                i++;
            }

            // Add the 'displayed' attribute.
            IDictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("display", preference.IsDisplayed.ToString().ToLower());            

            string consumerHighlights = TagBuilder.Wrap("consumerhighlights", attributes, highlightsStringBuilder.ToString());
            return consumerHighlights;
        }

        #endregion

        #region Highlight Section Rendering

        /// <summary>
        /// Build circle rating highlights as xml.
        /// </summary>
        /// <param name="highlights">Circle rating highlights.</param>
        /// <param name="showOnlyDisplayed">Should only displayed highlights be added to the xml?</param>
        /// <param name="rank">Ordering rank of the circle rating highlight section.</param>
        /// <returns>Circle rating highlights as an xml string.</returns>
        public static string CircleRatingHighlightsAsXml(IList<CircleRatingHighlightTO> highlights, 
                                                         bool showOnlyDisplayed, int rank)
        {            
            if (highlights == null || highlights.Count == 0)
            {
                return string.Empty;
            }

            bool anyHighlights = false;

            StringBuilder highlightsStringBuilder = new StringBuilder();
            
            foreach (CircleRatingHighlightTO highlight in highlights)
            {
                if ((showOnlyDisplayed && highlight.IsDisplayed) || !showOnlyDisplayed)
                {
                    string description = TagBuilder.Wrap("description", highlight.RatingDescription);
                    string value       = TagBuilder.Wrap("value",       highlight.RatingValue.ToString());

                    highlightsStringBuilder.Append(TagBuilder.Wrap("highlight", description + value));

                    anyHighlights = true;
                }
            }

            // If no highlights were added to the xml, return an empty string instead of a child-less node.
            if (!anyHighlights)
            {
                return string.Empty;
            }

            // Add the 'rank' attribute.
            IDictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("rank", rank.ToString());

            return TagBuilder.Wrap("circleratinghighlights", attributes, highlightsStringBuilder.ToString());   
        }        

        /// <summary>
        /// Build free text highlights as xml.
        /// </summary>
        /// <param name="highlights">Free text highlights.</param>
        /// <param name="showOnlyDisplayed">Should only displayed highlights be added to the xml?</param>
        /// <param name="rank">Ordering rank of the free text highlight section.</param>
        /// <returns>Free text highlights as an xml string.</returns>
        public static string FreeTextHighlightsAsXml(IList<FreeTextHighlightTO> highlights, bool showOnlyDisplayed,
                                                     int rank)
        {
            if (highlights == null || highlights.Count == 0)
            {
                return string.Empty;
            }

            bool anyHighlights = false;

            StringBuilder highlightsStringBuilder = new StringBuilder();            
            
            foreach (FreeTextHighlightTO highlight in highlights)
            {
                if ((showOnlyDisplayed && highlight.IsDisplayed) || !showOnlyDisplayed)
                {
                    string text = TagBuilder.Wrap("text", highlight.Text);

                    highlightsStringBuilder.Append(TagBuilder.Wrap("highlight", text));

                    anyHighlights = true;
                }
            }

            // If no highlights were added to the xml, return an empty string instead of a child-less node.
            if (!anyHighlights)
            {
                return string.Empty;
            }

            // Add the 'rank' attribute.
            IDictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("rank", rank.ToString());

            return TagBuilder.Wrap("freetexthighlights", attributes, highlightsStringBuilder.ToString());            
        }

        /// <summary>
        /// Build snippet highlights as xml.
        /// </summary>
        /// <param name="highlights">Snippet highlights.</param>
        /// <param name="showOnlyDisplayed">Should only displayed highlights be added to the xml?</param>
        /// <param name="rank">Ordering rank of the snippet highlight section.</param>
        /// <returns>Snippet highlights as an xml string.</returns>
        public static string SnippetHighlightsAsXml(IList<SnippetHighlightTO> highlights, bool showOnlyDisplayed,
                                                    int rank)
        {
            if (highlights == null || highlights.Count == 0)
            {
                return string.Empty;
            }

            bool anyHighlights = false;

            StringBuilder highlightsStringBuilder = new StringBuilder();
            
            foreach (SnippetHighlightTO highlight in highlights)
            {
                if ((showOnlyDisplayed && highlight.IsDisplayed) || !showOnlyDisplayed)
                {
                    string snippet    = TagBuilder.Wrap("snippet", highlight.Snippet);
                    string sourceName = TagBuilder.Wrap("source", highlight.SnippetSourceName);

                    highlightsStringBuilder.Append(TagBuilder.Wrap("highlight", snippet + sourceName));

                    anyHighlights = true;
                }
            }

            // If no highlights were added to the xml, return an empty string instead of a child-less node.
            if (!anyHighlights)
            {
                return string.Empty;
            }

            // Add the 'rank' attribute.
            IDictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("rank", rank.ToString());

            return TagBuilder.Wrap("snippethighlights", attributes, highlightsStringBuilder.ToString());
        }

        /// <summary>
        /// Build vehicle history highlights as xml.
        /// </summary>
        /// <param name="highlights">Vehicle history highlights.</param>
        /// <param name="includeCarfaxOneOwnerIcon">Should the Carfax One Owner icon be included?</param>
        /// <param name="showOnlyDisplayed">Should only displayed highlights be added to the xml?</param>
        /// <param name="carFaxLogoUrl">The url to the Carfax logo.</param>
        /// <param name="rank">Ordering rank of the vehicle history highlight section.</param>
        /// <returns>Vehicle history highlights as an xml string.</returns>
        public static string VehicleHistoryHighlightsAsXml(IList<VehicleHistoryHighlightTO> highlights, 
                                                           bool includeCarfaxOneOwnerIcon, 
                                                           bool showOnlyDisplayed, string carFaxLogoUrl,
                                                           int rank)
        {
            if (highlights == null || highlights.Count == 0)
            {
                return string.Empty;
            }

            bool anyHighlights = false;

            StringBuilder highlightsStringBuilder = new StringBuilder();

            bool oneOwner = false;
            bool showCarFaxLogo = false;
                        
            foreach (VehicleHistoryHighlightTO highlight in highlights)
            {                
                if ((showOnlyDisplayed && highlight.IsDisplayed) || !showOnlyDisplayed)
                {
                    string text = TagBuilder.Wrap("text", highlight.Text);

                    highlightsStringBuilder.Append(TagBuilder.Wrap("highlight", text));

                    anyHighlights = true;
                }

                // Did we just see the "one owner" highlight?
                if (highlight.Text.Trim().ToLower() == ONE_OWNER.ToLower())
                {
                    oneOwner = true;
                }
            }            

            // If no highlights were added to the xml, return an empty string instead of a child-less node.
            if (!anyHighlights)
            {
                return string.Empty;
            }

            // Is this a carfax 1-owner vehicle and does the preference indicate they wish to show the logo?
            if (includeCarfaxOneOwnerIcon && oneOwner)
            {
                showCarFaxLogo = true;
            }

            // Build the "showCarFaxLogo" attribute.
            IDictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("showCarFaxLogo", showCarFaxLogo.ToString().ToLower());
            attributes.Add("carFaxLogoUrl", carFaxLogoUrl);
            attributes.Add("rank", rank.ToString());

            return TagBuilder.Wrap("vehiclehistoryhighlights", attributes, highlightsStringBuilder.ToString());
        }

        #endregion
    }
}
