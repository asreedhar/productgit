using System;
using System.Collections.Generic;
using Csla;
using FirstLook.Common.Core.DTO;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    [Serializable]
    public sealed class ConsumerHighlightsVehiclePreference : InjectableBusinessBase<ConsumerHighlightsVehiclePreference>, 
        IConsumerHighlightsVehiclePreference, IRenderXml, ITransferObject<ConsumerHighlightsVehiclePreferenceTO>
    {
        #region Constants

        private const int  NOT_ASSIGNED = 0;
        private const bool DEFAULT_IS_DISPLAYED = true;
        private const bool DEFAULT_INCLUDE_CARFAX_ONEOWNER = true;

        #endregion

        #region Injected dependencies

        public IConsumerHighlightsVehiclePreferenceDataAccess DataAccess { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Vehicle preference identifier.
        /// </summary>
        public int ConsumerHighlightVehiclePreferenceId
        {
            get
            {
                CanReadProperty("ConsumerHighlightVehiclePreferenceId", true);

                return _consumerHighlightsVehiclePreferenceId;
            }
        }
        private int _consumerHighlightsVehiclePreferenceId;

        /// <summary>
        /// Owner handle.
        /// </summary>
        public string OwnerHandle
        {
            get
            {
                CanReadProperty("OwnerHandle", true);

                return _ownerHandle;
            }
            set
            {
                CanWriteProperty("OwnerHandle", true);

                _ownerHandle = value;

                PropertyHasChanged("OwnerHandle");
            }
        }
        private string _ownerHandle;

        /// <summary>
        /// Vehicle handle.
        /// </summary>
        public string VehicleHandle
        {
            get
            {
                CanReadProperty("VehicleHandle", true);

                return _vehicleHandle;
            }
            set
            {
                CanWriteProperty("VehicleHandle", true);

                _vehicleHandle = value;

                PropertyHasChanged("VehicleHandle");
            }
        }
        private string _vehicleHandle;

        /// <summary>
        /// Are these preferences to be shown?
        /// </summary>
        public bool IsDisplayed
        {
            get
            {
                CanReadProperty("IsDisplayed", true);

                return _isDisplayed;
            }
            set
            {
                CanWriteProperty("IsDisplayed", true);

                _isDisplayed = value;

                PropertyHasChanged("IsDisplayed");
            }
        }
        private bool _isDisplayed;

        /// <summary>
        /// Show the Carfax 1 Owner icon be included?
        /// </summary>
        public bool IncludeCarfaxOneOwnerIcon
        {
            get
            {
                CanReadProperty("IncludeCarfaxOneOwnerIcon", true);

                return _includeCarfaxOneOwnerIcon;
            }
            set
            {
                CanWriteProperty("IncludeCarfaxOneOwnerIcon", true);

                _includeCarfaxOneOwnerIcon = value;

                PropertyHasChanged("IncludeCarfaxOneOwnerIcon");
            }
        }
        private bool _includeCarfaxOneOwnerIcon;

        /// <summary>
        /// Date at which these preferences were either inserted or most recently updated.
        /// </summary>
        public DateTime ChangedDate
        {
            get
            {
                CanReadProperty("ChangedDate", true);

                return _changedDate;
            }
            set
            {
                CanWriteProperty("ChangedDate", true);

                _changedDate = value;

                PropertyHasChanged("ChangedDate");
            }
        }
        private DateTime _changedDate;

        /// <summary>
        /// Ordering of the highlights sections. Defined by dealer preferences.
        /// </summary>
        public List<HighlightSectionType> SectionOrdering
        {
            get
            {
                CanReadProperty("SectionOrdering", true);
                return _sectionOrdering;
            }
        }
        private List<HighlightSectionType> _sectionOrdering;

        /// <summary>
        /// Circle rating highlights.
        /// </summary>
        public List<CircleRatingHighlightTO> CircleRatingHighlights
        {
            get
            {
                CanReadProperty("CircleRatingHighlights", true);
                return _circleRatings;
            }
            set
            {
                CanWriteProperty("CircleRatingHighlights", true);
                _circleRatings = value;
                PropertyHasChanged("CircleRatingHighlights");
            }
        }
        private List<CircleRatingHighlightTO> _circleRatings;  

        /// <summary>
        /// Free text consumer highlights.
        /// </summary>
        public List<FreeTextHighlightTO> FreeTextHighlights
        {
            get
            {
                CanReadProperty("FreeTextHighlights", true); 
                return _freeText;
            }
            set
            {
                CanWriteProperty("FreeTextHighlights", true); 
                _freeText = value;
                PropertyHasChanged("FreeTextHighlights");
            }
        }
        private List<FreeTextHighlightTO> _freeText;        

        /// <summary>
        /// Snippet highlights.
        /// </summary>
        public List<SnippetHighlightTO> SnippetHighlights
        {
            get
            {
                CanReadProperty("SnippetHighlights", true); 
                return _snippets;
            }
            set
            {
                CanWriteProperty("SnippetHighlights", true); 
                _snippets = value;
                PropertyHasChanged("SnippetHighlights");
            }
        }
        private List<SnippetHighlightTO> _snippets;

        /// <summary>
        /// Vehicle history report highlights.
        /// </summary>
        public List<VehicleHistoryHighlightTO> VehicleHistoryHighlights
        {
            get
            {
                CanReadProperty("VehicleHistoryHighlights", true); 
                return _vehicleHistory;
            }
            set 
            {
                CanWriteProperty("VehicleHistoryHighlights", true); 
                _vehicleHistory = value;
                PropertyHasChanged("VehicleHistoryHighlights");
            }
        }
        private List<VehicleHistoryHighlightTO> _vehicleHistory;                             

        #endregion

        #region Factory Methods

        /// <summary>
        /// Parameter-less constructor, used by DataPortal.Create.
        /// </summary>
        internal ConsumerHighlightsVehiclePreference()
        {            
        }

        /// <summary>
        /// Create a vehicle preference from the given parameters.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="isDisplayed">Are the preferences displayed?</param>
        /// <param name="includeCarfaxOneOwnerIcon">Shopuld the Carfax 1 Owner icon be shown?</param>
        private ConsumerHighlightsVehiclePreference(IConsumerHighlightsVehiclePreferenceDataAccess dataAccess, string ownerHandle, 
                                                    string vehicleHandle, bool isDisplayed, 
                                                    bool includeCarfaxOneOwnerIcon)
        {
            _consumerHighlightsVehiclePreferenceId = NOT_ASSIGNED;
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _isDisplayed = isDisplayed;
            _includeCarfaxOneOwnerIcon = includeCarfaxOneOwnerIcon;      
            _changedDate = new DateTime();

            // Get the highlights data.
            _circleRatings  = dataAccess.FetchCircleRatings(_ownerHandle, _vehicleHandle);
            _freeText       = new List<FreeTextHighlightTO>();
            _snippets       = dataAccess.FetchSnippets(_ownerHandle, _vehicleHandle);
            _vehicleHistory = dataAccess.GetVehicleHistoryHighlights(ownerHandle, vehicleHandle);                       
            
            MarkNew();
        }

        /// <summary>
        /// Gets an existing ConsumerHighlightsVehiclePreference or returns a new unpersisted ConsumerHighlightsVehiclePreference if one does not exist.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>New or existing vehicle preferences.</returns>
        public static ConsumerHighlightsVehiclePreference GetOrCreateConsumerHighlightsPreference(string ownerHandle, 
                                                                                                  string vehicleHandle)
        {
            var existsCmd = new ExistsCommand(ownerHandle, vehicleHandle);
            DataPortal.Execute(existsCmd);
            return existsCmd.Exists
                       ? GetConsumerHighlightsVehiclePreference(ownerHandle, vehicleHandle)
                       : CreateConsumerHighlightsVehiclePreference(ownerHandle, vehicleHandle);
        }

        /// <summary>
        /// Get consumer highlight vehicle preferences that are expected to exist. Also, get highlight section ordering
        /// preferences from the dealer preferences.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Existing vehicle preferences.</returns>
        public static ConsumerHighlightsVehiclePreference GetConsumerHighlightsVehiclePreference(string ownerHandle, 
                                                                                                 string vehicleHandle)
        {
            var preferences = 
                DataPortal.Fetch<ConsumerHighlightsVehiclePreference>(new Criteria(ownerHandle, vehicleHandle));

            var dealerPreferences =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);
            preferences._sectionOrdering = dealerPreferences.HighlightSectionTypes;

            return preferences;
        }        

        /// <summary>
        /// Create consumer highlight vehicle preferences. Also, get highlight section ordering preferences from the
        /// dealer preferences.        
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>New vehicle preferences.</returns>
        public static ConsumerHighlightsVehiclePreference CreateConsumerHighlightsVehiclePreference(string ownerHandle, 
                                                                                                    string vehicleHandle)
        {
            var preferences = 
                DataPortal.Create<ConsumerHighlightsVehiclePreference>(new Criteria(ownerHandle, vehicleHandle));

            var dealerPreferences =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);
            preferences._sectionOrdering = dealerPreferences.HighlightSectionTypes;

            return preferences;
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Criteria for data operations on vehicle preferences. Consists of owner and vehicle handles.
        /// </summary>
        [Serializable]
        private class Criteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }
        }

        /// <summary>
        /// Create a new vehicle preference.
        /// </summary>
        /// <param name="criteria">Owner and vehicle handles.</param>
        private void DataPortal_Create(Criteria criteria)
        {            
            var preference = new ConsumerHighlightsVehiclePreference(DataAccess, criteria.OwnerHandle, criteria.VehicleHandle,
                DEFAULT_IS_DISPLAYED, DEFAULT_INCLUDE_CARFAX_ONEOWNER);

            var to = preference.ToTransferObject();
            FromTransferObject(to);
        }

        /// <summary>
        /// Fetch vehicle preferences for the given owner and vehicle.
        /// </summary>
        /// <param name="criteria">Owner and vehicle handles.</param>
        private void DataPortal_Fetch(Criteria criteria)
        {            
            var preferenceTO = DataAccess.Fetch(criteria.OwnerHandle, criteria.VehicleHandle);                       
            FromTransferObject(preferenceTO);
        }

        /// <summary>
        /// Insert vehicle preferences to the database.
        /// </summary>
        protected override void DataPortal_Insert()
        {            
            var preferenceTO = ToTransferObject();                        
            DataAccess.Insert(preferenceTO, ApplicationContext.User.Identity.Name);
                        
            _consumerHighlightsVehiclePreferenceId = preferenceTO.ConsumerHighlightVehiclePreferenceId;
        }

        /// <summary>
        /// Update the vehicle preferences in the database.
        /// </summary>
        protected override void DataPortal_Update()
        {            
            DataAccess.Update(ToTransferObject(), ApplicationContext.User.Identity.Name);
        }

        /// <summary>
        /// Delete these vehicle preferences.
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {            
            DataAccess.Delete(ToTransferObject(), ApplicationContext.User.Identity.Name);
        }


        #endregion

        #region Exists Command

        /// <summary>
        /// Determines whether a preference exists for the specified owner and vehicle.
        /// </summary>
        [Serializable]
        class ExistsCommand : InjectableCommandBase
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private bool _exists;

            #region Injected dependencies

// ReSharper disable MemberCanBePrivate.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
            public IConsumerHighlightsVehiclePreferenceDataAccess DataAccess { get; set; }
// ReSharper restore UnusedAutoPropertyAccessor.Local
// ReSharper restore MemberCanBePrivate.Local

            #endregion

            public ExistsCommand(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public bool Exists
            {
                get { return _exists; }
            }

            protected override void DataPortal_Execute()
            {
                _exists = DataAccess.Exists(_ownerHandle, _vehicleHandle);
            }
        }

        #endregion

        #region Highlights Methods

        /// <summary>
        /// Get all consumer highlights. All circle rating, free text, snippet and vehicle history highlights will be
        /// added in order of section ordering preferences.
        /// </summary>
        /// <remarks>
        /// I don't know that this function should exist. -christian
        /// </remarks>
        /// <returns>Enumeration of all highlights.</returns>
        public IEnumerable<IConsumerHighlight> GetHighlights()
        {
            foreach (HighlightSectionType sectionType in _sectionOrdering)
            {
                switch (sectionType)
                {
                    case HighlightSectionType.CircleRatings:
                        foreach (CircleRatingHighlightTO to in _circleRatings)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.FreeText:
                        foreach (FreeTextHighlightTO to in _freeText)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.Snippets:
                        foreach (SnippetHighlightTO to in _snippets)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.VehicleHistory:
                        foreach (VehicleHistoryHighlightTO to in _vehicleHistory)
                        {
                            yield return to;
                        }
                        break;                        
                }
            }
        }

        /// <summary>
        /// Get the total number of consumer highlights in this preference.
        /// </summary>
        /// <returns></returns>
        public int HighlightsCount()
        {
            return _circleRatings.Count + _freeText.Count + _snippets.Count + _vehicleHistory.Count;
        }

        /// <summary>
        /// Get circle rating highlights.
        /// </summary>
        /// <returns>Enumeration of circle rating highlights.</returns>
        public IEnumerable<CircleRatingHighlightTO> GetCircleRatingHighlights()
        {
            foreach (CircleRatingHighlightTO to in _circleRatings)
            {
                yield return to;
            }
        }

        /// <summary>
        /// Get free text highlights.
        /// </summary>
        /// <returns>Enumeration of free text highlights.</returns>
        public IEnumerable<FreeTextHighlightTO> GetFreeTextHighlights()
        {
            foreach (FreeTextHighlightTO to in _freeText)
            {
                yield return to;
            }
        }

        /// <summary>
        /// Get snippet highlights.
        /// </summary>
        /// <returns>Enumeration of snippet highlights.</returns>
        public IEnumerable<SnippetHighlightTO> GetSnippetHighlights()
        {
            foreach (SnippetHighlightTO to in _snippets)
            {
                yield return to;
            }
        }

        /// <summary>
        /// Update (or delete) a Consumer Highlight in this preference's Consumer Highlight enumeration.
        /// </summary>
        /// <param name="key">Unique identifier of this highlight - used when a proper id hasn't been set.</param>        
        /// Can be used to identify new Consumer Highlights (that do not yet have a unique ConsumerHighlightId) .</param>
        /// <param name="isDisplayed">Indicates whether or not the Consumer Highlight should be displayed.</param>
        /// <param name="text">The text of the Consumer Highlight.</param>
        /// <param name="rank">The value used to order the Consumer Highlights.</param>
        /// <param name="isDeleted">Use this to mark a Consumer Highlight for deletion.</param>
        public void UpdateHighlight(string key, bool isDisplayed, string text, int rank, bool isDeleted)
        {
            var highlight = Find(key);

            if (highlight == null)
            {
                throw new ApplicationException("Consumer Highlight not found.");
            }
            
            // An update is being attempted on a vehicle history highlight - this has a few additional restrictions 
            // that need to be enforced.
            var vehicleHistoryHighlight = highlight as VehicleHistoryHighlightTO;
            if( vehicleHistoryHighlight != null )
            {   
                if (isDeleted)
                {
                    throw new ApplicationException("Vehicle History Consumer Highlights cannot be deleted.");
                }                
                if (vehicleHistoryHighlight.Text != text)
                {
                    throw new ApplicationException("The text values of Vehicle History Consumer Highlights cannot be updated.");
                }
            }

            // Properties that can only change with a free text highlight.
            var freeTextHighlight = highlight as FreeTextHighlightTO;
            if (freeTextHighlight != null)
            {
                freeTextHighlight.Text = text;
                freeTextHighlight.IsDeleted = isDeleted;    
            }

            // Properties that can change no matter what type of highlight.
            highlight.IsDisplayed = isDisplayed;            
            highlight.Rank = rank;
            
            MarkDirty();
        }

        /// <summary>
        /// Creates a new Consumer Highlight in the enumeration.
        /// </summary>
        /// <param name="isDisplayed">Indicates whether or not the highlight should be displayed.</param>
        /// <param name="text">The text of the highlight.</param>
        /// <param name="rank">The value used to order the highlight.</param>
        public void CreateFreeTextHighlight(bool isDisplayed, string text, int rank)
        {
            _freeText.Add(new FreeTextHighlightTO(NOT_ASSIGNED, NOT_ASSIGNED, rank, isDisplayed, text));           
            
            MarkDirty();
        }

        #endregion

        #region Transfer Object Conversion

        /// <summary>
        /// Assigns the values of the current preference to the values of a transfer object.
        /// </summary>
        /// <param name="preference">The transfer object to use during assignment.</param>
        internal void FromTransferObject(ConsumerHighlightsVehiclePreferenceTO preference)
        {
            _consumerHighlightsVehiclePreferenceId = preference.ConsumerHighlightVehiclePreferenceId;
            _includeCarfaxOneOwnerIcon = preference.IncludeCarfaxOneOwnerIcon;
            _ownerHandle   = preference.OwnerHandle;
            _vehicleHandle = preference.VehicleHandle;
            _isDisplayed   = preference.IsDisplayed;
            _changedDate   = preference.ChangedDate;

            _sectionOrdering = preference.SectionOrdering;
            _circleRatings   = preference.CircleRatingHighlights;
            _freeText        = preference.FreeTextHighlights;
            _snippets        = preference.SnippetHighlights;
            _vehicleHistory  = preference.VehicleHistoryHighlights;
        }

        /// <summary>
        /// Converts this preference to a transfer object.
        /// </summary>
        /// <returns>The transfer object that represents this preference.</returns>
        public ConsumerHighlightsVehiclePreferenceTO ToTransferObject()
        {
            return new ConsumerHighlightsVehiclePreferenceTO(_consumerHighlightsVehiclePreferenceId, _ownerHandle,
                                                             _vehicleHandle, _isDisplayed, _includeCarfaxOneOwnerIcon,
                                                             _changedDate, _sectionOrdering, _circleRatings, _freeText, 
                                                             _snippets, _vehicleHistory);
        }

        #endregion

        #region BusinessBase Overrides

        /// <summary>
        /// Get the identifier of this business object.
        /// </summary>
        /// <returns>Identifier.</returns>
        protected override object GetIdValue()
        {
            return _consumerHighlightsVehiclePreferenceId;
        }

        #endregion

        #region Private Utility Methods

        /// <summary>
        /// Get the max rank for the highlights of the given type.
        /// </summary>
        /// <param name="highlightType">Highlight type to find the max rank for.</param>
        /// <returns>Maximum rank for the given type of highlight.</returns>
        public int GetMaxRank(HighlightType highlightType)
        {
            int maxRank = 0;

            if (highlightType == HighlightType.CircleRating)
            {
                foreach (CircleRatingHighlightTO to in _circleRatings)
                {
                    if (to.Rank > maxRank)
                    {
                        maxRank = to.Rank;
                    }
                }
            }
            else if (highlightType == HighlightType.FreeText)
            {
                foreach (FreeTextHighlightTO to in _freeText)
                {
                    if (to.Rank > maxRank)
                    {
                        maxRank = to.Rank;
                    }
                }
            }
            else if (highlightType == HighlightType.Snippet)
            {
                foreach (SnippetHighlightTO to in _snippets)
                {
                    if (to.Rank > maxRank)
                    {
                        maxRank = to.Rank;
                    }
                }
            }
            else if (highlightType == HighlightType.CarFax ||
                     highlightType == HighlightType.AutoCheck)
            {
                foreach (VehicleHistoryHighlightTO to in _vehicleHistory)
                {
                    if (to.Rank > maxRank)
                    {
                        maxRank = to.Rank;
                    }
                }
            }

            return maxRank;
        }

        /// <summary>
        /// Finds the consumer highlight with the given key if it exists.
        /// </summary>
        /// <param name="key">Key to find a highlight by.</param>
        /// <returns>Consumer highlight for the given key, or null if not found.</returns>
        private ConsumerHighlightTO Find(string key)
        {
            // Look in circle ratings.
            foreach (CircleRatingHighlightTO dto in _circleRatings)
            {
                if (dto.Key == key)
                {
                    return dto;
                }
            }
            // Look in free text.
            foreach (FreeTextHighlightTO dto in _freeText)
            {
                if (dto.Key == key)
                {
                    return dto;
                }
            }
            // Look in snippets.
            foreach (SnippetHighlightTO dto in _snippets)
            {
                if (dto.Key == key)
                {
                    return dto;
                }
            }
            // Look in vehicle history.
            foreach (VehicleHistoryHighlightTO dto in _vehicleHistory)
            {
                if (dto.Key == key)
                {
                    return dto;
                }
            }

            return null;
        }        

        #endregion

        #region Delete Behavior

        /// <summary>
        /// Mark these preferences for deletion.
        /// </summary>
        public void MarkForDeletion()
        {
            MarkDeleted();
        }

        #endregion

        #region IRenderXml

        /// <summary>
        /// Transform these vehicle preferences into an xml-formatted string.
        /// </summary>
        /// <returns>Xml of these vehicle preferences.</returns>
        public string AsXml()
        {            
            return ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(this, false);
        }

        #endregion
    }
}
