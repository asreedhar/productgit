﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Data access of consumer highlights dealer preferences.
    /// </summary>
    public class ConsumerHighlightsDealerPreferenceDataAccess : IConsumerHighlightsDealerPreferenceDataAccess
    {
        #region Create Methods

        /// <summary>
        /// Create a new preference object for the owner with the given handle. Minimum JD Power circle rating is 
        /// based on the default defined in <see cref="ConsumerHighlightsDealerPreference"/>. The highlight sections
        /// and displayed snippet sources are fetched from the database. Hidden snippet sources are left empty for new 
        /// preference objects (i.e. snippet sources default to viewed).
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>Consumer highlights dealer preference object.</returns>
        public ConsumerHighlightsDealerPreferenceTO Create(string ownerHandle)
        {
            var preferenceTO = new ConsumerHighlightsDealerPreferenceTO();
            
            preferenceTO.OwnerHandle                = ownerHandle;            
            preferenceTO.DisplayedSnippetSources    = FetchSnippetSources();
            preferenceTO.HiddenSnippetSources       = new List<SnippetSource>();
            preferenceTO.HighlightSections          = FetchHighlightSections();
            preferenceTO.MinimumJdPowerCircleRating = ConsumerHighlightsDealerPreference.DefaultMinimumJdPowerCircleRating;
            preferenceTO.MaxVHRItemsInitalDisplay   = ConsumerHighlightsDealerPreference.DefaultMaxVHRItemsInitalDisplay;
            FetchVehicleHistoryReportSources(ownerHandle, preferenceTO);
            preferenceTO.HiddenVehicleHistoryReportSources = new List<VehicleHistoryReportSource>();
            
            return preferenceTO;
        }

        #endregion

        #region Exist Methods

        /// <summary>
        /// Do preferences exist for the dealer with the given handle?
        /// </summary>
        /// <param name="ownerHandle">Dealer identifier.</param>
        /// <returns>True if preferences exist, false otherwise.</returns>
        public bool Exists(string ownerHandle)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightPreference#Exists";

                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        return reader.Read();   
                    }                    
                }
            }
        }        

        /// <summary>
        /// Do highlight section ordering preferences already exist for the given owner?
        /// </summary>
        /// <remarks>
        /// The provided connection and transaction are present since this will be called in the middle of an open
        /// transaction used when inserting/updating a section preference.
        /// </remarks>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>        
        /// <returns>True if preferences exist, false otherwise.</returns>
        private static bool SectionPreferenceExists(IDbConnection connection, IDbTransaction transaction, 
                                                    string ownerHandle)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.ConsumerHighlightSectionPreference#Exists";
                command.Transaction = transaction;
                
                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return reader.Read();
                }                
            }            
        }
        
        /// <summary>
        /// Do snippet source ordering preferences already exist for the given owner?
        /// </summary>
        /// <remarks>
        /// The provided connection and transaction are present since this will be called in the middle of an open
        /// transaction used when inserting/updating a section preference.
        /// </remarks>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>        
        /// <returns>True if preferences exist, false otherwise.</returns>
        private static bool SnippetPreferenceExists(IDbConnection connection, IDbTransaction transaction, 
                                                    string ownerHandle)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.SnippetSourcePreference#Exists";
                command.Transaction = transaction;

                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);                

                using (IDataReader reader = command.ExecuteReader())
                {
                    return reader.Read();
                }                    
            }            
        }

        private static bool VehicleHistoryPreferenceExists(IDbConnection connection, IDbTransaction transaction,
                                                    string ownerHandle)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.VehicleHistorySourcePreference#Exists";
                command.Transaction = transaction;
                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return reader.Read();
                }
            }      
        }

        #endregion

        #region Fetch Methods

        /// <summary>
        /// Fetch the consumer highglights dealer preferences for the owner with the given handle.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>Dealer preferences object.</returns>
        public ConsumerHighlightsDealerPreferenceTO Fetch(string ownerHandle)
        {            
            ConsumerHighlightsDealerPreferenceTO preferenceTO = new ConsumerHighlightsDealerPreferenceTO();
            preferenceTO.OwnerHandle = ownerHandle;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightPreference#Fetch";                    

                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                    
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new ApplicationException(
                                "The consumer highlights dealer preferences could not be found.");
                        }

                        // Fetch the general dealer preferences (including the minimum JD Power circle rating).                    
                        preferenceTO.MinimumJdPowerCircleRating = reader.GetDouble(reader.GetOrdinal("MinimumJDPowerCircleRating"));
                        preferenceTO.MaxVHRItemsInitalDisplay = reader.GetByte(reader.GetOrdinal("MaxVHRItemsInitialDisplay"));

                        if (!reader.NextResult())
                        {
                            throw new ApplicationException(
                                "The consumer highlights dealer preferences could not be found.");
                        }

                        // Fetch the section ordering dealer preferences.
                        preferenceTO.HighlightSections = FetchHighlightSectionPreferences(reader);

                        if (!reader.NextResult())
                        {
                            throw new ApplicationException(
                                "The consumer highlights dealer preferences could not be found.");
                        }

                        // Fetch the displayed snippet source ordering dealer preferences.
                        preferenceTO.DisplayedSnippetSources = FetchSnippetSourcePreferences(reader);

                        if (!reader.NextResult())
                        {
                            throw new ApplicationException(
                                "The consumer highlights dealer preferences could not be found.");
                        }

                        // Fetch the hidden snippet source ordering dealer preferences.
                        preferenceTO.HiddenSnippetSources = FetchSnippetSourcePreferences(reader);

                        if (!reader.NextResult())
                        {
                            throw new ApplicationException(
                                "The consumer highlights dealer preferences could not be found.");
                        }

                        FetchVehicleHistoryReportSources(ownerHandle, preferenceTO);
                    }

                    return preferenceTO;
                }
            }            
        }

        /// <summary>
        /// Fetch all highlight sections from the database.
        /// </summary>
        /// <returns>List of highlight sections.</returns>
        private static List<HighlightSection> FetchHighlightSections()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightSection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        List<HighlightSection> highlightSections = new List<HighlightSection>();

                        for (int i = 0; reader.Read(); i++)
                        {
                            HighlightSection section = new HighlightSection();
                            section.Type =
                                (HighlightSectionType) reader.GetInt32(reader.GetOrdinal("ConsumerHighlightSectionId"));
                            section.Name = reader.GetString(reader.GetOrdinal("Name"));
                            section.Rank = reader.GetInt32(reader.GetOrdinal("DefaultRank"));

                            highlightSections.Add(section);
                        }

                        return highlightSections;
                    }
                }
            }  
        }

        /// <summary>
        /// Fetch the section ordering preferences for a dealer.
        /// </summary>
        /// <param name="reader">Data reader.</param>
        /// <returns>List of highlight sections in order.</returns>
        private static List<HighlightSection> FetchHighlightSectionPreferences(IDataReader reader)
        {
            List<HighlightSection> highlightSections = new List<HighlightSection>();

            while (reader.Read())
            {
                HighlightSection section = new HighlightSection();
                
                section.Type = (HighlightSectionType)reader.GetInt32(reader.GetOrdinal("ConsumerHighlightSectionId"));
                section.Name = reader.GetString(reader.GetOrdinal("Name"));
                section.Rank = reader.GetInt32(reader.GetOrdinal("Rank"));

                highlightSections.Add(section);
            }

            return highlightSections;
        }

        /// <summary>
        /// Fetch all the snippet sources from the database. All are set to be displayed, by default.
        /// </summary>
        /// <returns>List of all snippet sources from the database.</returns>
        private static List<SnippetSource> FetchSnippetSources()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.VehicleCatalogDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "FirstLook.MarketingTextSource#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        List<SnippetSource> sources = new List<SnippetSource>();

                        while( reader.Read() )
                        {
                            SnippetSource source = new SnippetSource();

                            source.Type         = (SnippetSourceType)reader.GetByte( reader.GetOrdinal( "MarketingTextSourceId" ) );
                            source.Name         = reader.GetString( reader.GetOrdinal( "Name" ) );
                            source.HasCopyright = reader.GetBoolean( reader.GetOrdinal( "HasCopyright" ) );
                            source.Rank         = reader.GetInt32( reader.GetOrdinal( "DefaultRank" ) );
                            source.IsDisplayed  = true;

                            sources.Add( source );
                        }

                        return sources;
                    }
                }
            }   
        }

        /// <summary>
        /// Fetch the snippet source ordering preferences for a dealer from the given result set.
        /// </summary>
        /// <param name="reader">Data reader.</param>
        /// <returns>List of snippet sources in order.</returns>
        private static List<SnippetSource> FetchSnippetSourcePreferences(IDataReader reader)
        {            
            List<SnippetSource> sources = new List<SnippetSource>();

            while (reader.Read())
            {
                SnippetSource source = new SnippetSource();

                source.Type         = (SnippetSourceType)reader.GetInt32(reader.GetOrdinal("SnippetSourceId"));
                source.Name         = reader.GetString(reader.GetOrdinal("Name"));
                source.Rank         = reader.GetInt32(reader.GetOrdinal("Rank"));
                source.IsDisplayed  = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                source.HasCopyright = reader.GetBoolean(reader.GetOrdinal("HasCopyright"));

                sources.Add(source);
            }

            return sources;
        }




        /// <summary>
        /// Fetch all the Vehicle History Report sources from the database. All are set to be displayed, by default.
        /// </summary>
        private static void FetchVehicleHistoryReportSources(string ownerHandle, ConsumerHighlightsDealerPreferenceTO preferenceTO)
        {
            using( IDataConnection connection = SimpleQuery.ConfigurationManagerConnection( FirstLook.Pricing.DomainModel.Database.InventoryDatabase ) )
            {
                if( connection.State == ConnectionState.Closed )
                {
                    connection.Open();
                }

                using( IDataCommand command = new DataCommand( connection.CreateCommand() ) )
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.VehicleHistorySourcePreference#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);

                    using( IDataReader reader = command.ExecuteReader() )
                    {
                        preferenceTO.DisplayedVehicleHistoryReportSources = FetchVehicleHistoryReportSources(reader);

                        if (!reader.NextResult())
                        {
                            throw new ApplicationException(
                                "The result set of hidden VHR items could not be found.");
                        }

                        preferenceTO.HiddenVehicleHistoryReportSources = FetchVehicleHistoryReportSources(reader);
                    }
                }
            }  
        }

        private static List<VehicleHistoryReportSource> FetchVehicleHistoryReportSources(IDataReader reader)
        {
            var sources = new List<VehicleHistoryReportSource>();

            while( reader.Read() )
            {
                var source = new VehicleHistoryReportSource();
                source.VehicleHistoryReportPreferenceId = reader.GetInt32(reader.GetOrdinal("VehicleHistoryReportPreferenceId"));
                source.VehicleHistoryReportInspectionId = reader.GetByte(reader.GetOrdinal("VehicleHistoryReportInspectionId"));
                source.VehicleHistoryProviderId = reader.GetByte( reader.GetOrdinal( "VehicleHistoryProviderId" ) );
                //source.ReportInspectionId = reader.GetByte( reader.GetOrdinal( "ReportInspectionId" ) );
                source.ReportInspectionId = ((source.VehicleHistoryReportInspectionId - 1) % 8) + 1;
                source.Description = reader.GetString(reader.GetOrdinal("Description"));
                source.Rank = 0;
                source.IsDisplayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                sources.Add( source );
            }

            return sources;
        }


        #endregion

        #region Insert Methods

        /// <summary>
        /// Insert the given dealer preferences for consumer highlights to the database.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>        
        /// <param name="preference">Consumer highlights dealer preference.</param>        
        public void Insert(IDataConnection connection, IDbTransaction transaction, 
                           ConsumerHighlightsDealerPreferenceTO preference)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.ConsumerHighlightPreference#Insert";
                command.Transaction = transaction;

                string insertUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("OwnerHandle", DbType.String, false, preference.OwnerHandle);
                command.AddParameterWithValue("MinimumJDPowerCircleRating", DbType.Double, false, preference.MinimumJdPowerCircleRating);
                command.AddParameterWithValue("MaxVHRItemsInitialDisplay", DbType.Byte, false, preference.MaxVHRItemsInitalDisplay);
                command.AddParameterWithValue("InsertUser", DbType.String, false, insertUser);

                command.ExecuteNonQuery();                
            }            
        }            
 
        /// <summary>
        /// Insert the consumer highlights section preferences for a dealer.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="highlightSection">Highlight section.</param>        
        private static void InsertSectionPreference(IDbConnection connection, IDbTransaction transaction,
                                                    string ownerHandle, HighlightSection highlightSection)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.ConsumerHighlightSectionPreference#Insert";
                command.Transaction = transaction;

                string insertUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("ConsumerHighlightSectionId", DbType.Int32,  false, highlightSection.Type);
                command.AddParameterWithValue("OwnerHandle",                DbType.String, false, ownerHandle);
                command.AddParameterWithValue("Rank",                       DbType.Int32,  false, highlightSection.Rank);
                command.AddParameterWithValue("InsertUser",                 DbType.String, false, insertUser);

                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Insert the dealer preferences on a snippet source's ordering to the database.
        /// </summary>        
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="source">Snippet source ordering dealer preference.</param>        
        private static void InsertSnippetPreference(IDbConnection connection, IDbTransaction transaction,
                                                    string ownerHandle, SnippetSource source)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.SnippetSourcePreference#Insert";
                command.Transaction = transaction;

                string insertUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("SnippetSourceId", DbType.Int32, false, (int)source.Type);
                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                command.AddParameterWithValue("IsDisplayed", DbType.Boolean, false, source.IsDisplayed);
                command.AddParameterWithValue("Rank", DbType.Int32, false, source.Rank);
                command.AddParameterWithValue("InsertUser", DbType.String, false, insertUser);

                command.ExecuteNonQuery();
            }
        }


        private static int InsertVehicleReportPreference(IDbConnection connection, IDbTransaction transaction,
                                                    string ownerHandle)
        {
            int vehicleHistoryReportPreferenceId = 0;
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.VehicleHistoryReportPreference#Insert";
                command.Transaction = transaction;

                string insertUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                command.AddParameterWithValue("InsertUser", DbType.String, false, insertUser);

                //Add the out param
                IDataParameter newId = command.AddOutParameter("VehicleHistoryReportPreferenceId", DbType.Int32);

                // Execute and capture the ID
                command.ExecuteNonQuery();
                vehicleHistoryReportPreferenceId = (Int32)newId.Value;
            }

            return vehicleHistoryReportPreferenceId;
        }


        private static void InsertVehicleHistoryPreference(IDbConnection connection, IDbTransaction transaction,
                                                    string ownerHandle, VehicleHistoryReportSource source, int vehicleHistoryReportPreferenceId)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.VehicleHistorySourcePreference#Insert";
                command.Transaction = transaction;

                string insertUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("VehicleHistoryReportPreferenceId", DbType.Int32, false, vehicleHistoryReportPreferenceId);
                command.AddParameterWithValue("VehicleHistoryReportInspectionId", DbType.Int32, false, source.VehicleHistoryReportInspectionId);
                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                command.AddParameterWithValue("IsDisplayed", DbType.Boolean, false, source.IsDisplayed);
                command.AddParameterWithValue("Rank", DbType.Int32, false, source.Rank);
                command.AddParameterWithValue("InsertUser", DbType.String, false, insertUser);

                command.ExecuteNonQuery();
            }
        }


        #endregion

        #region Save Methods

        /// <summary>
        /// Upsert the dealer preferences.
        /// </summary>        
        /// <param name="preference">Consumer highlights dealer preferences.</param>
        public void Save(ConsumerHighlightsDealerPreferenceTO preference)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                
                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    // Save general dealer preferences first.
                    if (!Exists(preference.OwnerHandle))
                    {
                        Insert(connection, transaction, preference);
                    }
                    else
                    {
                        Update(connection, transaction, preference);
                    }

                    // Save section ordering preferences.
                    SaveSectionPreferences(connection, transaction, preference.OwnerHandle, preference.HighlightSections);

                    // Save snippet source ordering preferences.
                    SaveSnippetPreferences(connection, transaction, preference.OwnerHandle, preference.DisplayedSnippetSources);
                    SaveSnippetPreferences(connection, transaction, preference.OwnerHandle, preference.HiddenSnippetSources);

                    SaveVehicleHistoryPreference(connection, transaction, preference.OwnerHandle, preference.DisplayedVehicleHistoryReportSources, preference.HiddenVehicleHistoryReportSources);

                    transaction.Commit();
                }
            }
        }

        /// <summary>
        /// Upsert the snippet source ordering preferences.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="sources">Snippet sources in order.</param>        
        private static void SaveSnippetPreferences(IDbConnection connection, IDbTransaction transaction, 
                                                   string ownerHandle, IEnumerable<SnippetSource> sources)
        {
            // Update existing snippet source ordering preferences.
            if (SnippetPreferenceExists(connection, transaction, ownerHandle))
            {
                foreach (SnippetSource source in sources)
                {
                    UpdateSnippetPreference(connection, transaction, ownerHandle, source);
                }
            }
            // Insert new snippet source ordering preferences.
            else
            {
                foreach (SnippetSource source in sources)
                {
                    InsertSnippetPreference(connection, transaction, ownerHandle, source);
                }
            }
        }

        /// <summary>
        /// Upsert the section ordering preferences.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="highlightSections">Highlight sections in order.</param>        
        private static void SaveSectionPreferences(IDbConnection connection, IDbTransaction transaction, 
                                                   string ownerHandle, IList<HighlightSection> highlightSections)
        {       
            // Update existing section preferences.
            if (SectionPreferenceExists(connection, transaction, ownerHandle))
            {
                for (int i = 0; i < highlightSections.Count; i++)
                {
                    UpdateSectionPreference(connection, transaction, ownerHandle, highlightSections[i]);
                }
            }
            // Insert new section preferences.
            else
            {
                for (int i = 0; i < highlightSections.Count; i++)             
                {
                    InsertSectionPreference(connection, transaction, ownerHandle, highlightSections[i]);
                }
            }
        }

        private static void SaveVehicleHistoryPreference(IDbConnection connection, IDbTransaction transaction,
                                                   string ownerHandle, IList<VehicleHistoryReportSource> displayedVehicleHistorySource, IList<VehicleHistoryReportSource> hiddenVehicleHistorySource)
        {
            if (VehicleHistoryPreferenceExists(connection, transaction, ownerHandle))
            {
                foreach (var source in displayedVehicleHistorySource)
                    UpdateVehicleHistoryPreference(connection, transaction, ownerHandle, source);

                foreach (var source in hiddenVehicleHistorySource)
                    UpdateVehicleHistoryPreference(connection, transaction, ownerHandle, source);
            }
            // Insert new section preferences.
            else
            {
                int vehicleHistoryReportPreferenceId = InsertVehicleReportPreference(connection, transaction, ownerHandle);

                foreach (var source in displayedVehicleHistorySource)
                    InsertVehicleHistoryPreference(connection, transaction, ownerHandle, source, vehicleHistoryReportPreferenceId);

                foreach (var source in hiddenVehicleHistorySource)
                    InsertVehicleHistoryPreference(connection, transaction, ownerHandle, source, vehicleHistoryReportPreferenceId);
            }
            
        }

        #endregion

        #region Update Methods

        /// <summary>
        /// Update the consumer highlights dealer preferences with the given object.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>        
        /// <param name="preference">Consumer highlights dealer preferences.</param>        
        private static void Update(IDbConnection connection, IDbTransaction transaction,
                                   ConsumerHighlightsDealerPreferenceTO preference)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.ConsumerHighlightPreference#Update";
                command.Transaction = transaction;

                string updateUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("OwnerHandle",                DbType.String, false, preference.OwnerHandle);
                command.AddParameterWithValue("MinimumJDPowerCircleRating", DbType.Double, false, preference.MinimumJdPowerCircleRating);
                command.AddParameterWithValue("UpdateUser",                 DbType.String, false, updateUser);
                command.AddParameterWithValue("MaxVHRItemsInitialDisplay", DbType.Byte, false, preference.MaxVHRItemsInitalDisplay);

                command.ExecuteNonQuery();
            }            
        }        

        /// <summary>
        /// Update the consumer highlights section preferences for a dealer.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="highlightSection">Highlight section.</param>        
        private static void UpdateSectionPreference(IDbConnection connection, IDbTransaction transaction, 
                                                    string ownerHandle, HighlightSection highlightSection)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.ConsumerHighlightSectionPreference#Update";
                command.Transaction = transaction;

                string updateUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("ConsumerHighlightSectionId", DbType.Int32,  false, highlightSection.Type);
                command.AddParameterWithValue("OwnerHandle",                DbType.String, false, ownerHandle);
                command.AddParameterWithValue("Rank",                       DbType.Int32,  false, highlightSection.Rank);
                command.AddParameterWithValue("UpdateUser",                 DbType.String, false, updateUser);

                command.ExecuteNonQuery();                
            }
        }      
  
        /// <summary>
        /// Update the snippet source ordering preferences for a dealer.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>        
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="source">Snippet source ordering dealer preference.</param>
        private static void UpdateSnippetPreference(IDbConnection connection, IDbTransaction transaction, 
                                                    string ownerHandle, SnippetSource source)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.SnippetSourcePreference#Update";
                command.Transaction = transaction;

                string updateUser = Thread.CurrentPrincipal.Identity.Name;

                command.AddParameterWithValue("SnippetSourceId", DbType.Int32,   false, (int)source.Type);
                command.AddParameterWithValue("OwnerHandle",     DbType.String,  false, ownerHandle);
                command.AddParameterWithValue("IsDisplayed",     DbType.Boolean, false, source.IsDisplayed);
                command.AddParameterWithValue("Rank",            DbType.Int32,   false, source.Rank);
                command.AddParameterWithValue("UpdateUser",      DbType.String,  false, updateUser);

                command.ExecuteNonQuery();                
            }
        }

         private static void UpdateVehicleHistoryPreference(IDbConnection connection, IDbTransaction transaction, string ownerHandle, VehicleHistoryReportSource source)
         {
             using (IDataCommand command = new DataCommand(connection.CreateCommand()))
             {
                 command.CommandType = CommandType.StoredProcedure;
                 command.CommandText = "Marketing.VehicleHistorySourcePreference#Update";
                 command.Transaction = transaction;

                 string updateUser = Thread.CurrentPrincipal.Identity.Name;

                 command.AddParameterWithValue("VehicleHistoryReportPreferenceId", DbType.Int32, false, source.VehicleHistoryReportPreferenceId);
                 command.AddParameterWithValue("VehicleHistoryReportInspectionId", DbType.Int32, false, source.VehicleHistoryReportInspectionId);
                 command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                 command.AddParameterWithValue("IsDisplayed", DbType.Boolean, false, source.IsDisplayed);
                 command.AddParameterWithValue("Rank", DbType.Int32, false, source.Rank);
                 command.AddParameterWithValue("UpdateUser", DbType.String, false, updateUser);

                 command.ExecuteNonQuery();
             }
             
         }

        #endregion
    }
}
