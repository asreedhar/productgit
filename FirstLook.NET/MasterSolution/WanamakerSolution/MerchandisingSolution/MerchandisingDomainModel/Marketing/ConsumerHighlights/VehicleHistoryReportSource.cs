﻿
namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    public class VehicleHistoryReportSource
    {
        public int VehicleHistoryReportPreferenceId { get; set; }
        /// <summary>
        /// identifies a unique report item returned by a provider
        /// </summary>
        public int VehicleHistoryReportInspectionId { get; set; }

        /// <summary>
        /// VHR Report Inspection Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ordering rank for this inspection report
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Is this item displayed
        /// </summary>
        public bool IsDisplayed { get; set; }

        /// <summary>
        /// ID of the provider
        /// </summary>
        public int VehicleHistoryProviderId { get; set; }

        /// <summary>
        /// ID matching the specific table (CarfaxReportInspection or AutoCheckReportInspection)
        /// </summary>
        public int ReportInspectionId { get; set; }
    }
}
