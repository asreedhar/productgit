﻿namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Source behind a marketing snippet for a vehicle. Examples are ConsumerReports.org, MotorTrend.com, 
    /// CarAndDriver.com, etc.
    /// </summary>
    public class SnippetSource
    {
        /// <summary>
        /// Snippet source identifier.
        /// </summary>
        public SnippetSourceType Type { get; set; }

        /// <summary>
        /// Snippet source name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Does the snippet source have copyright over its text?
        /// </summary>
        public bool HasCopyright { get; set; }

        /// <summary>
        /// Ordering rank for a dealer of this snippet source.
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Is this snippet source displayed for a given dealer?
        /// </summary>
        public bool IsDisplayed { get; set; }
    }
}
