﻿namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Sections that are displayed in Consumer Highlights.
    /// </summary>
    public enum HighlightSectionType
    {
        /// <summary>
        /// Unrecognized highlight section type.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// JD Power circle ratings.
        /// </summary>
        CircleRatings = 1, 

        /// <summary>
        /// Snippets - a.k.a. Expert Reviews & Awards.
        /// </summary>
        Snippets = 2,
        
        /// <summary>
        /// Vehicle history report highlights.
        /// </summary>
        VehicleHistory = 3,

        /// <summary>
        /// Dealer-entered, free-text highlights.
        /// </summary>
        FreeText = 4
    }
}
