namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Types of consumer highlight data.
    /// </summary>
    public enum HighlightType
    {
        /// <summary>
        /// Free-text entered by the dealer.
        /// </summary>
        FreeText = 1,

        /// <summary>
        /// AutoCheck vehicle history report.
        /// </summary>
        AutoCheck = 2,

        /// <summary>
        /// Carfax vehicle history report.
        /// </summary>
        CarFax = 3,        

        /// <summary>
        /// Marketing text snippet - aka Expert Reviews & Awards.
        /// </summary>
        Snippet = 4,

        /// <summary>
        /// JD Power circle rating.
        /// </summary>
        CircleRating = 5
    }
}
