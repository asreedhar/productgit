﻿namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// A highlight section - consists of a name, a type and a rank.
    /// </summary>
    public class HighlightSection
    {
        /// <summary>
        /// Type of the highlight section.
        /// </summary>
        public HighlightSectionType Type { get; set; }

        /// <summary>
        /// Name of the highlight section.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Rank of the section.
        /// </summary>
        public int Rank { get; set; }
    }
}
