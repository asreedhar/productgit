﻿namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Source of expert review snippets.
    /// </summary>
    public enum SnippetSourceType
    {
        /// <summary>
        /// Unrecognized snippet source.
        /// </summary>
        Unrecognized = 0,

        /// <summary>
        /// JDPower.com.
        /// </summary>
        JdPower = 1,

        /// <summary>
        /// Edmunds.com.
        /// </summary>
        Edmunds = 2, 

        /// <summary>
        /// newCarTestDrive.com.
        /// </summary>
        NewCarTestDrive = 3,

        /// <summary>
        /// KBB.com.
        /// </summary>
        KBB = 4,

        /// <summary>
        /// TheCarConnection.com.
        /// </summary>
        TheCarConnection = 5,

        /// <summary>
        /// ConsumerReports.org.
        /// </summary>
        ConsumerReports = 6,

        /// <summary>
        /// RoadAndTrack.com.
        /// </summary>
        RoadAndTrack = 7,

        /// <summary>
        /// MotorTrend.com.
        /// </summary>
        MotorTrend = 8,

        /// <summary>
        /// Automedia.com.
        /// </summary>
        Automedia = 9,

        /// <summary>
        /// AutomobileMagazine.com.
        /// </summary>
        AutomobileMagazine = 10,

        /// <summary>
        /// Medley.
        /// </summary>
        Medley = 11,

        /// <summary>
        /// CarAndDriver.com.
        /// </summary>
        CarAndDriver = 12,

        /// <summary>
        /// bmwUsa.com.
        /// </summary>
        BmwUsa = 13
    }
}
