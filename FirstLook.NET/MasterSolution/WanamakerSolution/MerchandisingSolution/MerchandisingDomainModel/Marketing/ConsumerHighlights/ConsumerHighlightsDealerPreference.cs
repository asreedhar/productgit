using System;
using System.Collections.Generic;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Dealer preferences for the display of consumer highlight data.
    /// </summary>
    [Serializable]
    public class ConsumerHighlightsDealerPreference
    {
        #region Constants

        /// <summary>
        /// Default minimum value for JD Power Circle Ratings.
        /// </summary>
        internal const double DefaultMinimumJdPowerCircleRating = 3;

        internal const int DefaultMaxVHRItemsInitalDisplay = 6;

        #endregion

        #region Properties

        /// <summary>
        /// The identifier of the owner to whom these preferences apply.
        /// </summary>
        public string OwnerHandle { get; set; }

        /// <summary>
        /// Minimum number of circles required to display the JD Power circle rating for a highlight.
        /// </summary>
        public double MinimumJdPowerCircleRating { get; set; }
        public int MaxVHRItemsInitalDisplay { get; set; }

        /// <summary>
        /// Ordering of displayed marketing text sources.
        /// </summary>
        public List<SnippetSource> DisplayedSnippetSources { get; set; }

        /// <summary>
        /// Ordering of hidden marketing text sources.
        /// </summary>
        public List<SnippetSource> HiddenSnippetSources { get; set; }

        /// <summary>
        /// Ordering of displayed marketing text sources.
        /// </summary>
        public List<VehicleHistoryReportSource> DisplayedVehicleHistoryReportSources { get; set; }

        /// <summary>
        /// Ordering of displayed marketing text sources.
        /// </summary>
        public List<VehicleHistoryReportSource> HiddenVehicleHistoryReportSources { get; set; }
        
        /// <summary>
        /// Ordering of highlight sections.
        /// </summary>
        public List<HighlightSection> HighlightSections { get; set; }

        /// <summary>
        /// Ordering of highlight sections, as only highlight section types.
        /// </summary>
        public List<HighlightSectionType> HighlightSectionTypes
        {
            get
            {
                List<HighlightSectionType> highlightSectionTypes = new List<HighlightSectionType>();
                foreach (HighlightSection highlightSection in HighlightSections)
                {
                    highlightSectionTypes.Add(highlightSection.Type);
                }
                return highlightSectionTypes;
            }
        }

        #endregion

        #region Construction

        /// <summary>        
        /// Create an empty preference.
        /// </summary>
        private ConsumerHighlightsDealerPreference(ConsumerHighlightsDealerPreferenceTO transferObject)
        {
            FromTransferObject(transferObject);
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Get the dealer preferences for the given owner if they already exist. Otherwise, create them.
        /// </summary>
        /// <param name="ownerHandle">Owner identifier.</param>
        /// <returns>Consumer highlights dealer preference.</returns>
        public static ConsumerHighlightsDealerPreference GetOrCreateDealerPreference(string ownerHandle)
        {
            IConsumerHighlightsDealerPreferenceDataAccess dataAccess = 
                Registry.Resolve<IConsumerHighlightsDealerPreferenceDataAccess>();

            ConsumerHighlightsDealerPreferenceTO transferObject;

            if (dataAccess.Exists(ownerHandle))
            {
                transferObject = dataAccess.Fetch(ownerHandle);                
            }
            else
            {
                transferObject = dataAccess.Create(ownerHandle);
            }

            return new ConsumerHighlightsDealerPreference(transferObject);
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Save this consumer highlights dealer preference to the database.
        /// </summary>
        public void Save()
        {
            IConsumerHighlightsDealerPreferenceDataAccess dataAccess =
                Registry.Resolve<IConsumerHighlightsDealerPreferenceDataAccess>();

            ConsumerHighlightsDealerPreferenceTO transferObject = ToTransferObject();
            dataAccess.Save(transferObject);
        }

        #endregion

        #region Transfer Object Parsing

        /// <summary>
        /// Populate this dealer preference object with values from the transfer object.
        /// </summary>
        /// <param name="transferObject">Dealer preference transfer object.</param>
        internal void FromTransferObject(ConsumerHighlightsDealerPreferenceTO transferObject)
        {
            OwnerHandle                = transferObject.OwnerHandle;
            MinimumJdPowerCircleRating = transferObject.MinimumJdPowerCircleRating;
            MaxVHRItemsInitalDisplay   = transferObject.MaxVHRItemsInitalDisplay;
            HighlightSections          = transferObject.HighlightSections;
            DisplayedSnippetSources    = transferObject.DisplayedSnippetSources;
            HiddenSnippetSources       = transferObject.HiddenSnippetSources;
            DisplayedVehicleHistoryReportSources = transferObject.DisplayedVehicleHistoryReportSources;
            HiddenVehicleHistoryReportSources = transferObject.HiddenVehicleHistoryReportSources;
        }

        /// <summary>
        /// Populate a dealer preference transfer object with the values of this preference object.
        /// </summary>
        /// <returns>Dealer preference transfer object.</returns>
        internal ConsumerHighlightsDealerPreferenceTO ToTransferObject()
        {
            ConsumerHighlightsDealerPreferenceTO transferObject = new ConsumerHighlightsDealerPreferenceTO();

            transferObject.OwnerHandle                = OwnerHandle;
            transferObject.MinimumJdPowerCircleRating = MinimumJdPowerCircleRating;
            transferObject.MaxVHRItemsInitalDisplay   = MaxVHRItemsInitalDisplay;
            transferObject.HighlightSections          = HighlightSections;
            transferObject.DisplayedSnippetSources    = DisplayedSnippetSources;
            transferObject.HiddenSnippetSources       = HiddenSnippetSources;
            transferObject.DisplayedVehicleHistoryReportSources = DisplayedVehicleHistoryReportSources;
            transferObject.HiddenVehicleHistoryReportSources = HiddenVehicleHistoryReportSources;
                 
            return transferObject;
        }

        #endregion
    }
}
