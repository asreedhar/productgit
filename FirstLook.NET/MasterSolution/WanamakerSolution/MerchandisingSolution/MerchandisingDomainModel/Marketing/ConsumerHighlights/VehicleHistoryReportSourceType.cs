﻿namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    public enum VehicleHistoryReportSourceType
    {
        Unrecognized = 0,
        Custom = 1,
        AutoCheck = 2,
        CarFax = 3,
        ExpertReviews = 4,
        JDPowerDotComRatings = 5
    }
}
