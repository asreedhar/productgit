using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Linq;
using Csla.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.VehicleHistoryReport.WebService.Client.Carfax;
using FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck;
using UserIdentity = FirstLook.VehicleHistoryReport.WebService.Client.Carfax.UserIdentity;

namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Access for data operations on consumer highlights vehicle preferences.
    /// </summary>
    internal class ConsumerHighlightsVehiclePreferenceDataAccess : IConsumerHighlightsVehiclePreferenceDataAccess
    {
        private readonly ILogger _logger;

        #region Constants

        private const int NOT_ASSIGNED = 0;
        private const string KEY_DELIM = ":";

        #endregion


        #region 

        /// <summary>
        /// Default ctor.
        /// </summary>
        public ConsumerHighlightsVehiclePreferenceDataAccess()
        {
            _logger = new NullLogger();
        }

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="logger">A logger.</param>
        public ConsumerHighlightsVehiclePreferenceDataAccess(ILogger logger)
        {
            _logger = logger;
        }

        #endregion

        #region Vehicle Preference Methods

        /// <summary>
        /// Do vehicle preferences exist for the owner and vehicle with the given handles?
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>True if preferences exist, false otherwise.</returns>
        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            int count = 0;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightVehiclePreference#Exists";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "ReturnValue", count, DbType.Int32, ParameterDirection.ReturnValue, false);

                    command.ExecuteNonQuery();

                    IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }
                }
            }
            return (count > 0);
        }

        /// <summary>
        /// Fetch the vehicle preferences - including all highlights - for the owner and vehicle with the given 
        /// handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Vehicle preference transfer object.</returns>
        public ConsumerHighlightsVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle)
        {
            int vehicleCatalogId = VehicleHelper.GetVehicle(ownerHandle, vehicleHandle).DecodingClassification.Id;

            int consumerHighlightVehiclePreferenceId;
            bool isDisplayed;
            bool includeCarfaxOneOwnerIcon;
            DateTime changedDate;

            List<HighlightSectionType> sectionOrdering = new List<HighlightSectionType>();

            List<CircleRatingHighlightTO>   circleRatings  = new List<CircleRatingHighlightTO>();
            List<FreeTextHighlightTO>       freeText       = new List<FreeTextHighlightTO>();
            List<SnippetHighlightTO>        snippets       = new List<SnippetHighlightTO>();
            List<VehicleHistoryHighlightTO> vehicleHistory = new List<VehicleHistoryHighlightTO>();            
            
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightVehiclePreference#Fetch";

                    CommonMethods.AddWithValue(command, "OwnerHandle",      ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle",    vehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleCatalogId", vehicleCatalogId, DbType.Int32);

                    SafeDataReader reader = new SafeDataReader(command.ExecuteReader());

                    if (reader.Read())
                    {
                        consumerHighlightVehiclePreferenceId = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightVehiclePreferenceId"));
                        isDisplayed               = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        includeCarfaxOneOwnerIcon = reader.GetBoolean(reader.GetOrdinal("IncludeCarfaxOneOwnerIcon"));
                        changedDate               = reader.GetDateTime(reader.GetOrdinal("ChangedDate"));
                    }
                    else
                    {
                        throw new ApplicationException("The consumer highlights vehicle preference could not be found.");
                    }

                    // Get the free text highlights.
                    if (!reader.NextResult())
                    {
                        throw new ApplicationException("Free text consumer highlights could not be found.");
                    }
                    while (reader.Read())
                    {
                        int id         = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightId"));
                        int prefId     = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightVehiclePreferenceId"));
                        int rank       = reader.GetInt32(reader.GetOrdinal("Rank"));
                        bool displayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        string text    = reader.GetString(reader.GetOrdinal("Text"));

                        freeText.Add(new FreeTextHighlightTO(id, prefId, rank, displayed, text));                        
                    }

                    // Get the vehicle history higlights.
                    if (!reader.NextResult())
                    {
                        throw new ApplicationException("Vehicle history highlights could not be found.");
                    }

                    while (reader.Read())
                    {
                        int id                 = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightId"));
                        int prefId             = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightVehiclePreferenceId"));
                        int rank               = reader.GetInt32(reader.GetOrdinal("Rank"));
                        bool displayed         = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        string text            = reader.GetString(reader.GetOrdinal("Text"));
                        DateTime expiration    = reader.GetDateTime(reader.GetOrdinal("ExpirationDate"));
                        HighlightType provider = (HighlightType)reader.GetByte(reader.GetOrdinal("HighlightProviderId"));

                        vehicleHistory.Add(new VehicleHistoryHighlightTO(id, prefId, rank, displayed, expiration, provider, text, id.ToString()));  
                    }
                    //if they ordered a report after the vehicle preference was saved try to get the items from the web service
                    if (vehicleHistory.Count == 0)
                    {
                        List<VehicleHistoryHighlightTO> vehicleReport = GetVehicleHistoryHighlights(ownerHandle, vehicleHandle);
                        vehicleHistory.AddRange(vehicleReport);
                    }

                    // Get the circle rating highlights.
                    if (!reader.NextResult())
                    {
                        throw new ApplicationException("Circle rating highlights could not be found.");
                    }
                    while (reader.Read())
                    {
                        int id              = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightId"));
                        int prefId          = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightVehiclePreferenceId"));
                        int rank            = reader.GetInt32(reader.GetOrdinal("Rank"));
                        bool displayed      = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        double value        = reader.GetDouble(reader.GetOrdinal("CircleRating"));
                        string name         = reader.GetString(reader.GetOrdinal("Description"));
                        int sourceRowId     = reader.GetInt32(reader.GetOrdinal("ProviderSourceRowId"));
                        DateTime createDate = reader.GetDateTime(reader.GetOrdinal("DateCreated"));

                        circleRatings.Add(new CircleRatingHighlightTO(id, prefId, rank, displayed, value, name, 
                                                                      sourceRowId, createDate));
                    }

                    // Get the snippet highlights.
                    if (!reader.NextResult())
                    {
                        throw new ApplicationException("Snippet highlights could not be found.");
                    }
                    while (reader.Read())
                    {
                        int id              = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightId"));
                        int prefId          = reader.GetInt32(reader.GetOrdinal("ConsumerHighlightVehiclePreferenceId"));
                        int rank            = reader.GetInt32(reader.GetOrdinal("Rank"));
                        bool displayed      = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        int providerRowId   = reader.GetInt32(reader.GetOrdinal("ProviderSourceRowId"));
                        int sourceId        = reader.GetByte(reader.GetOrdinal("SourceId"));
                        string source       = reader.GetString(reader.GetOrdinal("Name"));
                        string snippet      = reader.GetString(reader.GetOrdinal("MarketingText"));
                        DateTime createDate = reader.GetDateTime(reader.GetOrdinal("DateCreated"));

                        snippets.Add(new SnippetHighlightTO(id, prefId, rank, displayed, providerRowId, sourceId, 
                                                            source, snippet, createDate));
                    }                                        

                    if (!reader.IsClosed)
                    {
                        reader.Close();
                    }
                }
            }

            // Check that all snippet and circle rating highlights are valid, and delete the ones that are not.
            ValidateHighlights(ownerHandle, vehicleHandle, snippets, circleRatings);

            // Add any new snippet or circle ratings to our collections as hidden at the highest rank.
            ImportCircleRatingsFromSource(ownerHandle, vehicleHandle, circleRatings);
            ImportSnippetsFromSource(ownerHandle, vehicleHandle, snippets);                       

            // Create the new vehicle preference!
            return new ConsumerHighlightsVehiclePreferenceTO(consumerHighlightVehiclePreferenceId, ownerHandle,
                                                             vehicleHandle, isDisplayed, includeCarfaxOneOwnerIcon,
                                                             changedDate, sectionOrdering, circleRatings, freeText, 
                                                             snippets, vehicleHistory);
        }       

        /// <summary>
        /// Insert vehicle preferences to the database.
        /// </summary>
        /// <param name="preference">Vehicle preferences to save.</param>
        /// <param name="userName">Name of the user performing this operation.</param>
        public void Insert(ConsumerHighlightsVehiclePreferenceTO preference, string userName)
        {
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightVehiclePreference#Insert";

                    SimpleQuery.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", preference.VehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "IsDisplayed", preference.IsDisplayed, DbType.Boolean);
                    SimpleQuery.AddWithValue(command, "IncludeCarfaxOneOwnerIcon", preference.IncludeCarfaxOneOwnerIcon, DbType.Boolean);
                    SimpleQuery.AddWithValue(command, "InsertUser", userName, DbType.String);
                    IDataParameter newId = SimpleQuery.AddOutParameter(command, "ConsumerHighlightVehiclePreferenceId", false, DbType.Int32);

                    try
                    {
                        command.ExecuteNonQuery();
                        preference.ConsumerHighlightVehiclePreferenceId = (int)newId.Value;
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

                // Insert circle rating highlights.
                foreach (CircleRatingHighlightTO highlight in preference.CircleRatingHighlights)
                {
                    highlight.VehiclePreferenceId = preference.ConsumerHighlightVehiclePreferenceId;
                    InsertHighlight(txn, preference, highlight, userName);
                }

                // Insert free text highlights.
                foreach (FreeTextHighlightTO highlight in preference.FreeTextHighlights)
                {
                    highlight.VehiclePreferenceId = preference.ConsumerHighlightVehiclePreferenceId;
                    InsertHighlight(txn, preference, highlight, userName);
                }

                // Insert snippet highlights.
                foreach (SnippetHighlightTO highlight in preference.SnippetHighlights)
                {
                    highlight.VehiclePreferenceId = preference.ConsumerHighlightVehiclePreferenceId;
                    InsertHighlight(txn, preference, highlight, userName);
                }

                // Insert vehicle history highlights.
                foreach (VehicleHistoryHighlightTO highlight in preference.VehicleHistoryHighlights)
                {
                    highlight.VehiclePreferenceId = preference.ConsumerHighlightVehiclePreferenceId;
                    InsertHighlight(txn, preference, highlight, userName);
                }

                // Commit the batch of commands.
                txn.Commit();
            }
        }

        /// <summary>
        /// Update the given vehicle preferences to the database.
        /// </summary>
        /// <param name="preference">Vehicle preference.</param>
        /// <param name="userName">Name of the user performing this operation.</param>
        public void Update(ConsumerHighlightsVehiclePreferenceTO preference, string userName)
        {
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightVehiclePreference#Update";

                    CommonMethods.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", preference.VehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "IsDisplayed", preference.IsDisplayed, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "IncludeCarfaxOneOwnerIcon", preference.IncludeCarfaxOneOwnerIcon, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "UpdateUser", userName, DbType.String);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

                // Process every consumer highlight.
                foreach (ConsumerHighlightTO highlight in preference.GetHighlights())
                {
                    if (highlight.ConsumerHighlightId == NOT_ASSIGNED)
                    {
                        InsertHighlight(txn, preference, highlight, userName);
                    }
                    else if (highlight.IsDeleted)
                    {
                        DeleteHighlight(txn, highlight);
                    }
                    else
                    {
                        UpdateHighlight(txn, highlight, userName);
                    }
                }                

                // Commit the batch of commands.
                txn.Commit();
            }
        }

        /// <summary>
        /// Delete the given vehicle preferences from the database.
        /// </summary>
        /// <remarks>
        /// The deleted preference's highlights are deleted by a cascading delete.
        /// </remarks>
        /// <param name="preference">Vehicle preferences to delete.</param>
        /// <param name="userName">Name of the user performing this operation.</param>
        public void Delete(ConsumerHighlightsVehiclePreferenceTO preference, string userName)
        {
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ConsumerHighlightVehiclePreference#Delete";

                    CommonMethods.AddWithValue(command, "ConsumerHighlightVehiclePreferenceId",
                                               preference.ConsumerHighlightVehiclePreferenceId,
                                               DbType.Int32, ParameterDirection.Input, false);

                    try
                    {
                        command.ExecuteNonQuery();
                        txn.Commit();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }                
            }
        }

        #endregion        

        #region Vehicle History Report Methods

        /// <summary>
        /// Get the Carfax web service.
        /// </summary>
        /// <returns>Carfax web service.</returns>
        private static CarfaxWebService GetCarfaxWebService()
        {
            UserIdentity identity = new UserIdentity();

            // Assigning the the identity.username to the thread principal makes this much less useful.  
            // Down deep in the service, it looks for a member with the same name and decides whether to 
            // treat us as a dealer, seller, etc... Reports are generally not available unless it thinks we are
            // a dealer.
            identity.UserName = Thread.CurrentPrincipal.Identity.Name;

            CarfaxWebService service = new CarfaxWebService();

            service.UserIdentityValue = identity;

            return service;
        }

        /// <summary>
        /// Get the AutoCheck web service.
        /// </summary>
        /// <returns>AutoCheck web service.</returns>
        private static AutoCheckWebService GetAutoCheckWebService()
        {
            VehicleHistoryReport.WebService.Client.AutoCheck.UserIdentity identity = new VehicleHistoryReport.WebService.Client.AutoCheck.UserIdentity();

            identity.UserName = Thread.CurrentPrincipal.Identity.Name;

            AutoCheckWebService service = new AutoCheckWebService();

            service.UserIdentityValue = identity;

            return service;
        }

        /// <summary>
        /// Get the Carfax report for the given dealer and vehicle.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vin">VIN.</param>
        /// <returns>Carfax report transfer object.</returns>
        internal CarfaxReportTO GetCarfaxReport(int dealerId, string vin)
        {
            try
            {
                CarfaxWebService service = GetCarfaxWebService();

                CarfaxReportTO report = service.GetReport(dealerId, vin);

                return report;

            }
            catch (Exception ex)
            {
                _logger.Log(ex);
                return null;
            }
        }

        /// <summary>
        /// Get the AutoCheck report for the given dealer and vehicle.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="vin">VIN.</param>
        /// <returns>AutoCheck report transfer object.</returns>
        internal AutoCheckReportTO GetAutoCheckReport(int dealerId, string vin)
        {
            try
            {
                AutoCheckWebService service = GetAutoCheckWebService();

                AutoCheckReportTO report = service.GetReport(dealerId, vin);

                return report;

            }
            catch (Exception ex)
            {
                _logger.Log(ex);
                return null;
            }
        }

        /// <summary>
        /// Get the Carfax highlights for the owner and vehicle with the given handles. Add any highlights to a list
        /// of highlights with ranks starting from the given parameter.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="startRank">Starting rank that retrieved highlights should be assigned.</param>
        /// <returns>List of Carfax highlights.</returns>
        public List<VehicleHistoryHighlightTO> GetCarfaxHighlights(string ownerHandle, string vehicleHandle, int startRank)
        {
            const string PREFIX_CARFAX = "CARFAX ";

            List<VehicleHistoryHighlightTO> highlights = new List<VehicleHistoryHighlightTO>();

            string vin = GetVehicleVin(ownerHandle, vehicleHandle);
            int dealerId = GetDealerId(ownerHandle);
            CarfaxReportTO reportTO = GetCarfaxReport(dealerId, vin);

            if (reportTO != null)
            {
                foreach (CarfaxReportInspectionTO to in reportTO.Inspections)
                {
                    if (to.Selected)
                    {
                        string key = ((int)HighlightType.CarFax) + KEY_DELIM + to.Id;

                        highlights.Add(new VehicleHistoryHighlightTO(NOT_ASSIGNED, NOT_ASSIGNED, startRank++, true,
                                                                     reportTO.ExpirationDate, HighlightType.CarFax, 
                                                                     PREFIX_CARFAX + to.Name, key));
                    }
                }
            }

            highlights.Sort();
            return highlights;
        }

        /// <summary>
        /// Get the Autocheck highlights for the owner and vehicle with the given handles. Add any highlights to a list
        /// of highlights with ranks starting from the given parameter.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="startRank">Starting rank that retrieved highlights should be assigned.</param>
        /// <returns>List of AutoCheck highlights.</returns>
        public List<VehicleHistoryHighlightTO> GetAutoCheckHighlights(string ownerHandle, string vehicleHandle, int startRank)
        {
            const string PREFIX_AUTOCHECK = "AutoCheck ";

            List<VehicleHistoryHighlightTO> highlights = new List<VehicleHistoryHighlightTO>();

            string vin = GetVehicleVin(ownerHandle, vehicleHandle);
            int dealerId = GetDealerId(ownerHandle);
            AutoCheckReportTO reportTO = GetAutoCheckReport(dealerId, vin);

            if (reportTO != null)
            {
                foreach (AutoCheckReportInspectionTO to in reportTO.Inspections)
                {
                    if (to.Selected)
                    {                        
                        string key = ((int)HighlightType.AutoCheck) + KEY_DELIM + to.Id;

                        highlights.Add(new VehicleHistoryHighlightTO(NOT_ASSIGNED, NOT_ASSIGNED, startRank++, true,
                                                                     reportTO.ExpirationDate, HighlightType.AutoCheck,
                                                                     PREFIX_AUTOCHECK + to.Name, key));
                    }
                }
            }

            highlights.Sort();
            return highlights;
        }

        /// <summary>
        /// Get all vehicle history report highlights for the owner and vehicle with the given handles. Will get the 
        /// highlights from both Carfax and AutoCheck.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Vehicle history report highlights.</returns>
        public List<VehicleHistoryHighlightTO> GetVehicleHistoryHighlights(string ownerHandle, string vehicleHandle)
        {
            List<VehicleHistoryHighlightTO> highlights = new List<VehicleHistoryHighlightTO>();

            IList<VehicleHistoryHighlightTO> autoCheck = GetAutoCheckHighlights(ownerHandle, vehicleHandle, 0);
            IList<VehicleHistoryHighlightTO> carfax = GetCarfaxHighlights(ownerHandle, vehicleHandle, autoCheck.Count);

            foreach (VehicleHistoryHighlightTO to in autoCheck)
            {
                highlights.Add(to);
            }
            foreach (VehicleHistoryHighlightTO to in carfax)
            {
                highlights.Add(to);
            }

            highlights.Sort();


            var dealerPreference = ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);

            var vehicleHistoryHighlights = new List<VehicleHistoryHighlightTO>();
            var vehicleHistoryHighlightsMap = highlights.ToDictionary(highlight => highlight.Key);
            int itemsDisplayedCount = 0;

            foreach (var vehicleHistoryReportSource in dealerPreference.DisplayedVehicleHistoryReportSources)
            {
                var key = vehicleHistoryReportSource.VehicleHistoryProviderId + ":" + vehicleHistoryReportSource.ReportInspectionId;
                if (vehicleHistoryHighlightsMap.ContainsKey(key))
                {
                    var vehicleHistoryHighlight = vehicleHistoryHighlightsMap[key];
                    vehicleHistoryHighlight.IsDisplayed = vehicleHistoryReportSource.IsDisplayed;
                    vehicleHistoryHighlights.Add(vehicleHistoryHighlight);
                    if (vehicleHistoryHighlight.IsDisplayed)
                    {
                        itemsDisplayedCount++;
                    }
                    if (dealerPreference.MaxVHRItemsInitalDisplay < itemsDisplayedCount)
                    {
                        vehicleHistoryHighlight.IsDisplayed = false;
                    }
                }
            }

            highlights = vehicleHistoryHighlights;
            return highlights;
        }

        #endregion                       

        #region Snippet Methods

        /// <summary>
        /// Get the marketing text snippets (aka 'Expert Reviews & Awards') for the owner and vehicle with the given
        /// handles. Used when initializing vehicle preferences.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>        
        /// <returns>List of snippets highlights.</returns>
        public List<SnippetHighlightTO> FetchSnippets(string ownerHandle, string vehicleHandle)
        {            
            int vehicleCatalogId = VehicleHelper.GetVehicle(ownerHandle, vehicleHandle).DecodingClassification.Id;

            List<SnippetHighlightTO> snippets = new List<SnippetHighlightTO>();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.VehicleCatalogDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Firstlook.SnippetText";

                    command.AddParameterWithValue("VehicleCatalogID", DbType.Int32, false, vehicleCatalogId);                    

                    using (IDataReader reader = command.ExecuteReader())
                    {                        
                        for (int i = 0; reader.Read(); i++ )
                        {
                            int sourceRowId       = reader.GetInt32(reader.GetOrdinal("MarketingTextID"));
                            int sourceId          = reader.GetByte(reader.GetOrdinal("MarketingTextSourceId"));
                            string sourceName     = reader.GetString(reader.GetOrdinal("Name"));
                            string snippet        = reader.GetString(reader.GetOrdinal("MarketingText"));
                            DateTime createDate   = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
                            bool defaultDisplayed = reader.GetBoolean(reader.GetOrdinal("DefaultDisplay"));

                            snippets.Add(new SnippetHighlightTO(NOT_ASSIGNED, NOT_ASSIGNED, i, defaultDisplayed, sourceRowId, 
                                                                sourceId, sourceName, snippet, createDate));
                        }                                                
                    }
                }
            }
            
            // Apply the dealer's preferences on the snippets.            
            ApplySnippetDealerPreferences(ownerHandle, snippets);
            return snippets;
        }


        /// <summary>
        /// Apply a dealer's preferences on the given snippets. Means, for each snippet, checking that the dealer has
        /// set that snippet source to be displayed. Additionally, set the ordering rank of these displayed snippets.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="snippets">Snippets to apply dealer preferences to.</param>
        private static void ApplySnippetDealerPreferences(string ownerHandle, List<SnippetHighlightTO> snippets)
        {                        
            ConsumerHighlightsDealerPreference dealerPreferences =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);

            // Get the list of displayed snippet sources from the dealer preferences.
            List<SnippetSource> snippetPreferences = dealerPreferences.DisplayedSnippetSources;

            List<SnippetHighlightTO> deleteSnippets = new List<SnippetHighlightTO>();

            // Iterate through the snippets and compare them against the dealer preference's list of displayed snippet
            // sources. If the snippet is ok, set its rank and displayed status.            
            foreach (SnippetHighlightTO snippet in snippets)
            {
                bool foundSnippet = false;
                foreach (SnippetSource snippetSource in snippetPreferences)
                {
                    if ((SnippetSourceType)snippet.SnippetSourceId == snippetSource.Type)
                    {
                        foundSnippet = true;
                        snippet.Rank = snippetSource.Rank;
                    }
                }
                if (!foundSnippet)
                {
                    deleteSnippets.Add(snippet);
                }
            }

            // Remove snippets that according to the dealer's preferences are not displayed.
            foreach (SnippetHighlightTO deleteSnippet in deleteSnippets)
            {
                snippets.Remove(deleteSnippet);
            }
            
            // Sort the snippets based on their rank.
            snippets.Sort();
        }

        /// <summary>
        /// Does a snippet exist for the given snippet and vehicle catalog identifiers?
        /// </summary>
        /// <param name="transaction">Database transaction, since this will be executed amidst deletes.</param>
        /// <param name="snippetId">Snippet identifier.</param>
        /// <param name="vehicleCatalogId">Vehicle catalog identifier.</param>
        /// <returns>True if the snippet exists, false otherwise.</returns>
        private static bool SnippetExists(DbTransaction transaction, int snippetId, int vehicleCatalogId)
        {
            using (IDbCommand command = transaction.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "VehicleCatalog.FirstLook.MarketingTextVehicleCatalog#Exists";

                CommonMethods.AddWithValue(command, "MarketingTextID", snippetId, DbType.Int32);
                CommonMethods.AddWithValue(command, "VehicleCatalogID", vehicleCatalogId, DbType.Int32);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return reader.Read();
                }
            }
        }

        /// <summary>
        /// Add to the given list of snippet highlights all snippets in the source table (as opposed to the vehicle 
        /// preference table) that are not already in the given list. All of these imported highlights are added as 
        /// hidden highlights.        
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="snippets">Collection of snippetsto import into.</param>
        private void ImportSnippetsFromSource(string ownerHandle, string vehicleHandle, 
                                              ICollection<SnippetHighlightTO> snippets)
        {
            // Get the max snippet ordering rank.
            int maxRank = 0;
            foreach (SnippetHighlightTO snippet in snippets)
            {
                if (snippet.Rank > maxRank)
                {
                    maxRank = snippet.Rank;
                }
            }

            IList<SnippetHighlightTO> allSnippets = FetchSnippets(ownerHandle, vehicleHandle);

            // Go through all the newly retrieved snippets.
            foreach (SnippetHighlightTO newSnippet in allSnippets)
            {
                bool hasSnippet = false;

                foreach (SnippetHighlightTO snippet in snippets)
                {
                    if (snippet.ProviderSourceRowId == newSnippet.ProviderSourceRowId)
                    {
                        hasSnippet = true;
                    }
                }

                // If the new snippet isn't in our list then add it at the highest rank with 'displayed' set to false.
                if (!hasSnippet)
                {
                    newSnippet.Rank = maxRank++;
                    newSnippet.IsDisplayed = false;
                    snippets.Add(newSnippet);
                }
            }
        }

        #endregion

        #region Circle Rating Methods

        /// <summary>
        /// Get the JD Power circle rating highlights for the owner and vehicle with the given handles. Used when 
        /// initializing vehicle preferences.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>        
        /// <returns>List of JD Power circle rating highlights.</returns>
        public List<CircleRatingHighlightTO> FetchCircleRatings(string ownerHandle, string vehicleHandle)
        {            
            int vehicleCatalogId = VehicleHelper.GetVehicle(ownerHandle, vehicleHandle).DecodingClassification.Id;

            // Get the minimum rating to return, as defined by the dealer preferences.
            double minimumRating = ConsumerHighlightsDealerPreference.
                                    GetOrCreateDealerPreference(ownerHandle).MinimumJdPowerCircleRating;                

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.VehicleCatalogDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Firstlook.JDPowerRating";                    

                    command.AddParameterWithValue("VehicleCatalogID", DbType.Int32, false, vehicleCatalogId);
                    command.AddParameterWithValue("OwnerHandle",      DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("DefaultMinRating", DbType.Double, false, minimumRating);

                    using (IDataReader reader = command.ExecuteReader())
                    {                        
                        List<CircleRatingHighlightTO> circleRatings = new List<CircleRatingHighlightTO>();

                        for (int i = 0; reader.Read(); i++)
                        {
                            double value = reader.GetDouble(reader.GetOrdinal("CircleRating"));
                            string text  = reader.GetString(reader.GetOrdinal("Description"));
                            int sourceRowId = reader.GetInt32(reader.GetOrdinal("JoinID"));
                            DateTime createDate = reader.GetDateTime(reader.GetOrdinal("DateCreated"));

                            circleRatings.Add(new CircleRatingHighlightTO(NOT_ASSIGNED, NOT_ASSIGNED, i, true, value, 
                                                                          text, sourceRowId, createDate));                            
                        }

                        circleRatings.Sort();
                        return circleRatings;
                    }
                }
            }
        }

        /// <summary>
        /// Does a circle rating exist for the given circle rating and vehicle catalog identifiers?
        /// </summary>
        /// <param name="transaction">Database transaction, since this will be executed amidst deletes.</param>
        /// <param name="circleRatingId">Circle rating identifier.</param>
        /// <param name="vehicleCatalogId">Vehicle catalog identifier.</param>
        /// <returns>True if the circle rating exists, false otherwise.</returns>
        private static bool CircleRatingExists(DbTransaction transaction, int circleRatingId, int vehicleCatalogId)
        {
            using (IDbCommand command = transaction.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "VehicleCatalog.FirstLook.VehicleCatalog_JDPowerRating#Exists";

                CommonMethods.AddWithValue(command, "JoinID", circleRatingId, DbType.Int32);
                CommonMethods.AddWithValue(command, "VehicleCatalogID", vehicleCatalogId, DbType.Int32);

                using (IDataReader reader = command.ExecuteReader())
                {
                    return reader.Read();
                }
            }
        }

        /// <summary>
        /// Add to the given list of circle rating highlights all circle ratings in the source table (as opposed to
        /// the vehicle preference table) that are not already in the given list. All of these imported highlights are 
        /// added as hidden highlights.        
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="circleRatings">Collection of circle ratings to import into.</param>
        private void ImportCircleRatingsFromSource(string ownerHandle, string vehicleHandle,
                                                   ICollection<CircleRatingHighlightTO> circleRatings)
        {
            // Get the max circle rating ordering rank.
            int maxRank = 0;
            foreach (CircleRatingHighlightTO circleRating in circleRatings)
            {
                if (circleRating.Rank > maxRank)
                {
                    maxRank = circleRating.Rank;
                }
            }

            IList<CircleRatingHighlightTO> allCircleRatings = FetchCircleRatings(ownerHandle, vehicleHandle);

            // Go through all the newly retrieved circle ratings.
            foreach (CircleRatingHighlightTO newCircleRating in allCircleRatings)
            {
                bool hasCircleRating = false;

                foreach (CircleRatingHighlightTO circleRating in circleRatings)
                {
                    if (circleRating.ProviderSourceRowId == newCircleRating.ProviderSourceRowId)
                    {
                        hasCircleRating = true;
                    }
                }

                // If the new highlight isn't in our list, add it at the highest rank with 'displayed' set to false.
                if (!hasCircleRating)
                {
                    newCircleRating.Rank = maxRank++;
                    newCircleRating.IsDisplayed = false;
                    circleRatings.Add(newCircleRating);
                }
            }
        }

        #endregion

        #region Highlight Methods

        /// <summary>
        /// Validate the snippet and circle rating highlights. Checks that each higlight in those lists still exists
        /// in its source table (which is in a different database). If a highlight is not valid, it is deleted from
        /// the consumer highlights table and removed from our lists.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="snippets">Snippet highlights.</param>
        /// <param name="circleRatings">Circle rating highlights.</param>
        private static void ValidateHighlights(string ownerHandle, string vehicleHandle,
                                             ICollection<SnippetHighlightTO> snippets,
                                             ICollection<CircleRatingHighlightTO> circleRatings)
        {
            List<int> deleteHighlights = new List<int>();

            using (DbTransaction transaction = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                // Check snippet validity.
                foreach (SnippetHighlightTO snippet in snippets)
                {
                    if (!ValidateHighlight(transaction, ownerHandle, vehicleHandle, snippet))
                    {
                        deleteHighlights.Add(snippet.ConsumerHighlightId);
                    }
                }

                // Check circle rating validity.
                foreach (CircleRatingHighlightTO circleRating in circleRatings)
                {
                    if (!ValidateHighlight(transaction, ownerHandle, vehicleHandle, circleRating))
                    {
                        deleteHighlights.Add(circleRating.ConsumerHighlightId);
                    }
                }

                transaction.Commit();
            }

            // Remove invalid highlights.
            foreach (int highlightId in deleteHighlights)
            {
                foreach (SnippetHighlightTO highlight in snippets)
                {
                    if (highlight.ConsumerHighlightId == highlightId)
                    {
                        snippets.Remove(highlight);
                        break;
                    }
                }

                foreach (CircleRatingHighlightTO highlight in circleRatings)
                {
                    if (highlight.ConsumerHighlightId == highlightId)
                    {
                        circleRatings.Remove(highlight);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Validate the given highlight. Currently this only applies to snippets and circle ratings, and means 
        /// checking that a given highlight still exists in its source table (which is, in the case of snippets and 
        /// circle ratings, in another database), and also that the highlight still applies to the right vehicle.
        /// If a highlight is no valid, it is deleted from the consumer highlights table.
        /// </summary>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="highlight">Highlight to validate.</param>
        private static bool ValidateHighlight(DbTransaction transaction, string ownerHandle, string vehicleHandle,
                                               IConsumerHighlight highlight)
        {
            int vehicleCatalogId = VehicleHelper.GetVehicle(ownerHandle, vehicleHandle).DecodingClassification.Id;

            // Validate snippet.
            if (highlight.Provider == HighlightType.Snippet)
            {
                SnippetHighlightTO snippet = highlight as SnippetHighlightTO;
                if (snippet != null && !SnippetExists(transaction, snippet.ProviderSourceRowId.Value, vehicleCatalogId))
                {
                    DeleteHighlight(transaction, snippet);
                    return false;
                }
            }
            // Validate circle rating.
            else if (highlight.Provider == HighlightType.CircleRating)
            {
                CircleRatingHighlightTO circleRating = highlight as CircleRatingHighlightTO;
                if (circleRating != null &&
                    !CircleRatingExists(transaction, circleRating.ProviderSourceRowId.Value, vehicleCatalogId))
                {
                    DeleteHighlight(transaction, circleRating);
                    return false;
                }
            }

            return true;
        }           

        /// <summary>
        /// Insert a highlight into the database.
        /// </summary>
        /// <param name="txn">Database transaction.</param>
        /// <param name="preference">Vehicle preferences that the highlight is tied to.</param>
        /// <param name="highlight">Consumer highlight to insert.</param>
        /// <param name="userName">Name of the user performing the operation.</param>
        private static void InsertHighlight(DbTransaction txn, IConsumerHighlightsVehiclePreference preference, 
                                            ConsumerHighlightTO highlight, string userName)
        {            
            FreeTextHighlightTO freeTextHighlight = highlight as FreeTextHighlightTO;
            VehicleHistoryHighlightTO vehicleHistoryHighlight = highlight as VehicleHistoryHighlightTO;

            using (IDbCommand command = txn.CreateCommand())
            {
                command.CommandText = "Marketing.ConsumerHighlight#Insert";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "ConsumerHighlightVehiclePreferenceId", preference.ConsumerHighlightVehiclePreferenceId, DbType.Int32);                
                SimpleQuery.AddWithValue(command, "HighlightProviderId", highlight.Provider, DbType.Int32);

                // Text field only applies to free text and vehicle history highlight types.
                if (freeTextHighlight != null)
                {
                    SimpleQuery.AddWithValue(command, "Text", freeTextHighlight.Text, DbType.String);
                }
                else if (vehicleHistoryHighlight != null)
                {
                    SimpleQuery.AddWithValue(command, "Text", vehicleHistoryHighlight.Text, DbType.String);
                }

                // Provider source row id is only used by some highlight types.
                if (highlight.ProviderSourceRowId != null && highlight.ProviderSourceRowId.HasValue)
                {
                    SimpleQuery.AddWithValue(command, "ProviderSourceRowId", highlight.ProviderSourceRowId, DbType.Int32);
                }

                // Rank and IsDisplayed are common to all highlight types.
                SimpleQuery.AddWithValue(command, "Rank", highlight.Rank, DbType.Int32);
                SimpleQuery.AddWithValue(command, "IsDisplayed", highlight.IsDisplayed, DbType.Int32);

                // Only vehicle history uses expiration date.
                if (vehicleHistoryHighlight != null && vehicleHistoryHighlight.ExpirationDate.HasValue)
                {
                    SimpleQuery.AddWithValue(command, "ExpirationDate", vehicleHistoryHighlight.ExpirationDate, DbType.DateTime);
                }

                SimpleQuery.AddWithValue(command, "InsertUser", userName, DbType.String);

                IDataParameter newId = SimpleQuery.AddOutParameter(command, "ConsumerHighlightId", false, DbType.Int32);

                try
                {
                    command.ExecuteNonQuery();
                    highlight.ConsumerHighlightId = (int)newId.Value;
                    highlight.VehiclePreferenceId = preference.ConsumerHighlightVehiclePreferenceId;
                }
                catch (Exception)
                {
                    txn.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Delete a highlight from the database.
        /// </summary>
        /// <param name="txn">Database transaction.</param>
        /// <param name="highlight">Consumer highlight to delete.</param>
        private static void DeleteHighlight(DbTransaction txn, IConsumerHighlight highlight)
        {
            using (IDbCommand command = txn.CreateCommand())
            {
                command.CommandText = "Marketing.ConsumerHighlight#Delete";
                command.CommandType = CommandType.StoredProcedure;
                SimpleQuery.AddWithValue(command, "ConsumerHighlightId", highlight.ConsumerHighlightId, DbType.Int32);

                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    txn.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Update a given highlight in the database.
        /// </summary>
        /// <param name="txn">Database transaction.</param>
        /// <param name="highlight">Consumer highlight to update.</param>
        /// <param name="userName">Name of the user performing this operation.</param>
        private static void UpdateHighlight(DbTransaction txn, IConsumerHighlight highlight, string userName)
        {            
            FreeTextHighlightTO       freeTextHighlight       = highlight as FreeTextHighlightTO;
            VehicleHistoryHighlightTO vehicleHistoryHighlight = highlight as VehicleHistoryHighlightTO;
            
            using (IDbCommand command = txn.CreateCommand())
            {
                command.CommandText = "Marketing.ConsumerHighlight#Update";
                command.CommandType = CommandType.StoredProcedure;
                
                SimpleQuery.AddWithValue(command, "ConsumerHighlightId", highlight.ConsumerHighlightId, DbType.Int32);

                // Text field only applies to free text and vehicle history highlight types.
                if (freeTextHighlight != null)
                {
                    SimpleQuery.AddWithValue(command, "Text", freeTextHighlight.Text, DbType.String);
                }
                else if (vehicleHistoryHighlight != null)
                {
                    SimpleQuery.AddWithValue(command, "Text", vehicleHistoryHighlight.Text, DbType.String);
                }

                SimpleQuery.AddWithValue(command, "Rank", highlight.Rank, DbType.Int32);

                // Free text and vehicle history highlight types will have a null provider source row id.
                if (highlight.ProviderSourceRowId != null && highlight.ProviderSourceRowId.HasValue)
                {                 
                    SimpleQuery.AddWithValue(command, "ProviderSourceRowId", highlight.ProviderSourceRowId.Value, DbType.Int32);    
                }
                
                SimpleQuery.AddWithValue(command, "IsDisplayed", highlight.IsDisplayed, DbType.Boolean);
                SimpleQuery.AddWithValue(command, "UpdateUser", userName, DbType.String);

                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    txn.Rollback();
                    throw;
                }
            }
        }

        #endregion

        #region Utility Methods

      

        /// <summary>
        /// Get the VIN for the vehicle with the given handle.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>VIN.</returns>
        private static string GetVehicleVin(string ownerHandle, string vehicleHandle)
        {
            Inventory inv = Inventory.GetInventory(ownerHandle, vehicleHandle);
            return inv.Vin;
        }

        /// <summary>
        /// Get the dealer identifier for a given owner handle.
        /// </summary>
        /// <param name="ownerHandle">Handle to the owner to find the dealer id for.</param>
        /// <returns>Dealer id if the owner handle describes a dealer, -1 otherwise.</returns>
        private static int GetDealerId(string ownerHandle)
        {
            if (!string.IsNullOrEmpty(ownerHandle))
            {
                DataTable table = GetOwnerTable(ownerHandle);
                foreach (DataRow row in table.Rows)
                {
                    return Int32Helper.ToInt32(row["OwnerKey"]);
                }
            }
            return -1;
        }

        /// <summary>
        /// Get owner data for a handle.
        /// </summary>
        /// <param name="ownerHandle">Handle to the owner to return data for.</param>
        /// <returns>Owner data.</returns>
        private static DataTable GetOwnerTable(string ownerHandle)
        {
            OwnerNameDataSource source = new OwnerNameDataSource();
            return source.FindByOwnerHandle(ownerHandle);
        }

        #endregion
    }
}
