﻿namespace FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights
{
    /// <summary>
    /// Marketing snippet from an expert review.
    /// </summary>
    public class Snippet
    {
        /// <summary>
        /// Source of the expert review.
        /// </summary>
        public SnippetSource Source { get; set; }

        /// <summary>
        /// Snippet of text from the expert review.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Is this snippet verbatim from the source?
        /// </summary>
        public bool IsVerbatim { get; set; }
    }
}
