﻿using System.Collections.Generic;
using FirstLook.Common.Core.Xml;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class PaymentInformationXmlBuilder
    {
        #region Sample Output
        /*
            <paymentInformation>
	            <lease isDisplayed="true">
                    <paymentAmount>$100</paymentAmount>
                    <paymentTerms>36 months</paymentTerms>
		            <downPaymentAmount>$500</downPaymentAmount>
                    <interestRate>5%</interestRate>
                    <showInterestRate>true<showInterestRate>
	            </lease>
	            <own isDisplayed="false">
                    <paymentAmount>$200</paymentAmount>
                    <paymentTerms>48 months</paymentTerms>
		            <downPaymentAmount>$5000</downPaymentAmount>
                    <interestRate>4%</interestRate>
                    <showInterestRate>false<showInterestRate>
	            </own>
            </paymentInformation>
*/
        #endregion Sample Output

        public static string AsXml(PaymentInformation lease, PaymentInformation own)
        {
 
            return TagBuilder.Wrap( "paymentInformation", 
                                    GetPaymentElement("lease", lease),
                                    GetPaymentElement("own", own));
        }

        private static string GetPaymentElement(string name, PaymentInformation pay)
        {

            if( pay == null )
            {
                pay = new PaymentInformation();
            }

            var payAmount = TagBuilder.Wrap("paymentAmount", pay.PaymentAmount);
            var terms = TagBuilder.Wrap("paymentTerms", pay.PaymentTerms);
            var down = TagBuilder.Wrap("downPaymentAmount", pay.DownPaymentAmount);
            var rate = TagBuilder.Wrap("interestRate", pay.InterestRate);
            var showRate = TagBuilder.Wrap("showInterestRate", pay.ShowInterestRate.ToString().ToLower());

            var attributes = new Dictionary<string, string> {{"isDisplayed", pay.IsDisplayed.ToString().ToLower()}};

            return TagBuilder.Wrap( name, 
                                    attributes, 
                                    payAmount, terms, down, rate, showRate);
        }
    }
}
