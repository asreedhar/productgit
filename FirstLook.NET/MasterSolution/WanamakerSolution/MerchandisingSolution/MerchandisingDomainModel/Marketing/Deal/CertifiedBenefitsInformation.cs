﻿using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class CertifiedBenefitsInformation
    {
        private string _ownerHandle;
        private string _vehicleHandle;

        private CertifiedVehiclePreference _preference;
        private bool _certifiedExists;

        public CertifiedBenefitsInformation(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;

            _certifiedExists = CertifiedVehiclePreference.CertifiedProgramExists(_ownerHandle, _vehicleHandle);
            
            if(_certifiedExists)
                _preference = CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(_ownerHandle, _vehicleHandle);
        }

        public bool CertifiedExists
        {
            get
            {
                return _certifiedExists;
            }
        }

        public CertifiedVehiclePreference Preference
        {
            get
            {
                return _preference;
            }
        }
    }
}
