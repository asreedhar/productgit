using System;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public interface IDeal
    {
        //ILogger Logger { get; set; }
        String OwnerHandle { get; set; }
        string VehicleHandle { get; set; }
        string SearchHandle { get; set; }

        CustomerPacketPublishingOptions CustomerPacketOptions { get; set; }
        ValueAnalyzerPacketPublishingOptions ValueAnalyzerPacketOptions { get; set; }
        
        int OfferPrice { get; set; }
        string ValidUntilDate { get; set; }
        string DealerDisclaimer { get; set; }
        Packets PacketType { get; set; }

        IMarketAnalysisData MarketAnalysis { get; set; }

        // payment information for lease and own
        PaymentInformation LeaseInformation { get; set; }
        PaymentInformation OwnInformation { get; set; }

        DealerSummaryInformation GetDealerSummary();
        void SaveXml(DealerSummaryInformation info);

        VehicleYearModelInformation GetVehicleYearModel();
        void SaveXml(VehicleYearModelInformation info);

        VehicleImageInformation GetVehicleImage();
        void SaveXml(VehicleImageInformation info);

        VehicleFeaturesInformation GetVehicleFeatures();
        void SaveXml(VehicleFeaturesInformation info);

        ConsumerHighlightsInformation GetConsumerHighlights();
        void SaveXml(ConsumerHighlightsInformation info);

        CertifiedBenefitsInformation GetCertifiedBenefits();
        void SaveXml(CertifiedBenefitsInformation info);

        PricingAnalysisInformation GetPricingAnalysis();
        void SaveXml(PricingAnalysisInformation info);

        MarketListingsInformation GetMarketListings();
        void SaveXml(MarketListingsInformation info);

        /// <summary>
        /// Get the specified node.  
        /// </summary>
        /// <param name="nodeType"></param>
        /// <returns></returns>
        SerializableXmlDocument GetXml(NodeType nodeType);

        SerializableXmlDocument GetXml();

        void Reset();
        void ResetMarketAnalysis();

        void ShowEquipmentOnMarketComparison(bool value);
        void ShowAdPreviews(bool value);
        bool ShowAdPreviews();
    }

    public enum NodeType
    {
        certifiedbenefits,
        dealer,
        vehiclephoto,
        vehicleequipment,
        consumerhighlights,
        pricing,
        marketlistings,
        vehicleyearmodel
    }

    public interface IMarketAnalysisData
    {
        IMarketAnalysisVehiclePreference VehiclePreference { get; set; }
        IMarketAnalysisDealerPreference DealerPreference { get; set; }
        IMarketAnalysisPricingInformation Pricing { get; set; }
    }

    [Serializable]
    internal class MarketAnalysisData : IMarketAnalysisData
    {
        #region Implementation of IMarketAnalysisData

        public IMarketAnalysisVehiclePreference VehiclePreference { get; set; }
        public IMarketAnalysisDealerPreference DealerPreference { get; set; }
        public IMarketAnalysisPricingInformation Pricing { get; set; }

        #endregion
    }
}