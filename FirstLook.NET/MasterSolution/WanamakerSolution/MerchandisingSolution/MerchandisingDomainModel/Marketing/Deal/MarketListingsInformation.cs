﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class MarketListingsInformation
    {
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;

        private readonly MarketListingVehiclePreference _preference;
        private readonly IMarketListingSearchAdapter _searchAdapter;
        private IEnumerable<IMarketListing> _listings;

        public MarketListingsInformation(string ownerHandle, string vehicleHandle, int offerPrice, IMarketListingSearchAdapter searchAdapter)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;

            _searchAdapter = searchAdapter;
            _preference = MarketListingVehiclePreference.GetOrCreate(vehicleHandle, ownerHandle, offerPrice);
        }

        public MarketListingVehiclePreference Preference
        {
            get
            {
                return _preference;
            }
        }

        public IEnumerable<IMarketListing> Listings
        {
            get
            {
                if (_listings == null)
                    LoadMarketListings();

                return _listings;
            }
        }

        public void LoadMarketListings()
        {
            LoadMarketListings(Preference);
        }

        public void LoadMarketListings(IMarketListingSortSpecification sortSpecification)
        {
            _listings = GetDisplayedListings(sortSpecification, Preference);
        }

        private IEnumerable<IMarketListing> GetDisplayedListings(IMarketListingSortSpecification sortSpecification, MarketListingVehiclePreference preference)
        {
            var marketListings = _searchAdapter.Search(_vehicleHandle, _ownerHandle, preference.UserName);

            marketListings = marketListings.Where(x => preference.DisplayListingsIDs.IndexOf(x.VehicleId) >= 0);

            return Sort(marketListings, sortSpecification);
        }

        private static IEnumerable<IMarketListing> Sort(IEnumerable<IMarketListing> marketListings, IMarketListingSortSpecification sortSpecification)
        {
            return marketListings.OrderBySortCode(sortSpecification.Sortcode, sortSpecification.SortAscending);
        }
    }
}
