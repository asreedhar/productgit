﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    [Serializable]
    public class PrintOverridesDTO
    {
        public PrintOverridesDTO()
        {
            ShowAdPreviews = true;
            MarketComparisonEquipment = false;
        }

        public bool MarketComparisonEquipment { get; set; }
        public bool ShowAdPreviews { get; set; }
    }
}