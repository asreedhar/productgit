﻿using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class ConsumerHighlightsInformation
    {
        private string _ownerHandle;
        private string _vehicleHandle;

        private ConsumerHighlightsVehiclePreference _vehiclePreference;

        public ConsumerHighlightsInformation(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;

            _vehiclePreference = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(_ownerHandle, _vehicleHandle);        
        }


        public ConsumerHighlightsVehiclePreference Preference
        {
            get
            {
                return _vehiclePreference;
            }
        }
    }
}
