﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    [Serializable]
    public enum Packets
    {
        SalesPacket,
        ValueAnalyzer
    }
}