﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class VehicleFeaturesInformation
    {
        private string _ownerHandle;
        private string _vehicleHandle;

        private EquipmentFacade _facade;
        private int? _equipmentProviderId;
        private IList<Equipment.Equipment> _equipment;

        public VehicleFeaturesInformation(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;

            _equipment = new List<Equipment.Equipment>();
            _facade = new EquipmentFacade(_ownerHandle, _vehicleHandle);
            _equipmentProviderId = GetEquipmentProvider();
            InitEquipment();
        }

        protected virtual int? GetEquipmentProvider()
        {
            int? equipmentProviderId = null;

            if (VehicleEquipmentProviderAssignment.Exists(_ownerHandle, _vehicleHandle))
            {
                VehicleEquipmentProviderAssignment assignment =
                    VehicleEquipmentProviderAssignment.GetVehicleProviderAssignment(_ownerHandle, _vehicleHandle);

                equipmentProviderId = assignment.EquipmentProviderId;
            }
            else
            {
                EquipmentProviderAssignmentList assignments =
                    EquipmentProviderAssignmentList.ReconcileEquipmentProviders(_ownerHandle);

                foreach (EquipmentProviderAssignment item in assignments)
                {
                    if (item.IsDefault)
                    {
                        equipmentProviderId = item.EquipmentProviderId;
                        break;
                    }
                }
            }

            return equipmentProviderId;
        }

        private void InitEquipment()
        {
            if (_equipmentProviderId.HasValue)
            {
                EquipmentList equipmentList = EquipmentList.ReconcileEquipmentList(
                    _ownerHandle,
                    _vehicleHandle,
                    _equipmentProviderId.Value);

                if (equipmentList != null)
                {
                    foreach (Equipment.Equipment equipment in equipmentList)
                    {
                        if (equipment.Selected)
                        {
                            _equipment.Add(equipment);
                        }
                    }
                }
            }
        }

        public EquipmentFacade EquipmentFacade
        {
            get
            {
                return _facade;
            }
        }

        public IList<Equipment.Equipment> Equipment
        {
            get
            {
                return _equipment;
            }
        }
    }
}
