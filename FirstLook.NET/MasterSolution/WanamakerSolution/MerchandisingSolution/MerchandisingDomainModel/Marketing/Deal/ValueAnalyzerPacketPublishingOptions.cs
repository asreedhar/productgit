﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    [Serializable]
    public class ValueAnalyzerPacketPublishingOptions
    {
        public ValueAnalyzerPacketPublishingOptions()
        {
            CarFaxReport = false;
            AutoCheckReport = false;

            Print = false;
            PostToWebsite = false;
            DisplayMarketListingsPermission = false;
        }

        public bool CarFaxReport { get; set; }
        public bool AutoCheckReport { get; set; }
        public bool Print { get; set; }
        public bool PostToWebsite { get; set; }
        public bool DisplayMarketListingsPermission { get; set; }
    }

}
