﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class VehicleYearModelInformation
    {
        private string _ownerHandle;
        private string _vehicleHandle;

        private DealerSummaryCommand _dealerSummaryrCommand;
        private GetVehicleSummaryCommand _vehicleSummaryCommand;

        public VehicleYearModelInformation(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;

            // Get the dealer summary
            _dealerSummaryrCommand = new DealerSummaryCommand(_ownerHandle);
            AbstractCommand.DoRun(_dealerSummaryrCommand);

            // Get the vehicle summary.
            _vehicleSummaryCommand = new GetVehicleSummaryCommand(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(_vehicleSummaryCommand);
        }

        public string Xml
        {
            get
            {
                string dealerSummary = _dealerSummaryrCommand.DealerSummary;
                string vehicleSummary = _vehicleSummaryCommand.VehicleSummary;
                return TagBuilder.Wrap("vehicleyearmodel", dealerSummary, vehicleSummary);
            }
        }
    }
}
