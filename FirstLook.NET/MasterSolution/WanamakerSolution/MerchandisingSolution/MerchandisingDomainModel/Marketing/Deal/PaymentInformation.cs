﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    [Serializable]
    public class PaymentInformation
    {
        public PaymentInformation()
        {
            // reasonable default
            DownPaymentAmount = string.Empty;
            InterestRate = string.Empty;
            IsDisplayed = false;
            PaymentAmount = string.Empty;
            PaymentTerms = string.Empty;
            ShowInterestRate = false;
        }

        public bool IsDisplayed { get; set; }

        public string PaymentAmount { get; set; }
        public string PaymentTerms { get; set; }
        public string DownPaymentAmount { get; set; }
        public string InterestRate { get; set; }
        public bool ShowInterestRate { get; set; }
    }    
}
