﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    [Serializable]
    public class Deal : IDeal
    {
	    #region Logging

	    protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

	    #endregion Logging

        // Ctor
        public Deal(ILogger logger, IMarketListingSearchAdapter searchAdapter, string ownerHandle, string vehicleHandle, string searchHandle)
        {
            OwnerHandle = ownerHandle;
            VehicleHandle = vehicleHandle;
            SearchHandle = searchHandle;
            _searchAdapter = searchAdapter;

            _printOverrides = new PrintOverridesDTO();
            _xmlWrapper = new DealXmlWrapper(this);

            _leaseInformation = new PaymentInformation();
            _ownInformation = new PaymentInformation();

            _xmlWrapper.UpdatePrintOverrides(_printOverrides);
            Logger = logger;
        }

        // Private member variables
        private CustomerPacketPublishingOptions _customerPacketOptions;
        private ValueAnalyzerPacketPublishingOptions _valueAnalyzerPacketOptions;
        private int? _offerPrice;
        private string _validUntilDate;
        private string _dealerDisclaimer;
        private Packets? _packetType;
        private IMarketAnalysisData _marketAnalysisData;
        private PrintOverridesDTO _printOverrides;
        private PaymentInformation _leaseInformation;
        private PaymentInformation _ownInformation;

        private DealXmlWrapper _xmlWrapper;
        private IMarketListingSearchAdapter _searchAdapter;

        // Injected dependencies
        public ILogger Logger { get; set; }

        // Properties
        public String OwnerHandle { get; set; }
        public string VehicleHandle { get; set; }
        public string SearchHandle { get; set; }

        

        public CustomerPacketPublishingOptions CustomerPacketOptions
        {
            get
            {
                if( _customerPacketOptions == null )
                {
                    _customerPacketOptions = new CustomerPacketPublishingOptions { SalesPacketOptions = SalesPackOptions.SellingSheet };
                }

                return _customerPacketOptions;

            }
            set { _customerPacketOptions = value; }
        }

        public ValueAnalyzerPacketPublishingOptions ValueAnalyzerPacketOptions
        {
            get
            {
                if( _valueAnalyzerPacketOptions == null )
                {
                    _valueAnalyzerPacketOptions = new ValueAnalyzerPacketPublishingOptions();
                }
                return _valueAnalyzerPacketOptions;

            }
            set { _valueAnalyzerPacketOptions = value; }
        }

        public int OfferPrice
        {
            get
            {
                if( !_offerPrice.HasValue )
                {
                    try
                    {
                        // Get the list price (aka "current internet price")
                        MarketAnalysisPricingInformation pricingInformation =
                            MarketAnalysisPricingInformation.GetMarketAnalysisPricingInformation(OwnerHandle, VehicleHandle,
                                                                                                 SearchHandle);

                        int? listPrice = pricingInformation.ListPrice;

                        // If null was encountered, use 0.
                        _offerPrice = listPrice.GetValueOrDefault(0);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex);
                        _offerPrice = 0;
                    }
                }
                return _offerPrice.Value;

            }
            set { _offerPrice = value; }
        }

        public string ValidUntilDate
        {
            get
            {
                if ( String.IsNullOrEmpty(_validUntilDate) )
                {
                    try
                    {

                        // Get the dealer "valid until" setting
                        int daysValid = DealerGeneralPreference.Exists(OwnerHandle) ?
                                        DealerGeneralPreference.GetDealerGeneralPreference(OwnerHandle).DaysValidFor : 0;

                        // Valid until date is today + daysValid
                        DateTime validUntilDate = DateTime.Now.AddDays(daysValid);

                        // Append values.
                        _validUntilDate = validUntilDate.ToShortDateString();

                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex);
                        _validUntilDate =  DateTime.Now.ToShortDateString();
                    }
                }

                return _validUntilDate;
            }
            set { _validUntilDate = value; }
        }

        public string DealerDisclaimer
        {
            get
            {
                if (  _dealerDisclaimer == null )
                {
                    try
                    {
                        // Get the dealer disclaimer setting
                        _dealerDisclaimer = DealerGeneralPreference.Exists(OwnerHandle) ?
                            DealerGeneralPreference.GetDealerDisclaimer(OwnerHandle) : string.Empty;

                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex);
                        _dealerDisclaimer = string.Empty;
                    }
                }

                return _dealerDisclaimer;
            }
            set { _dealerDisclaimer = value; }
        }

        public Packets PacketType
        {
            get
            {
                if (!_packetType.HasValue)
                {
                    throw new ApplicationException("Could not determine packet type.");
                }

                return _packetType.Value;
            }
            set { _packetType = value; }
        }

        public IMarketAnalysisData MarketAnalysis
        {
            get
            {
                if( _marketAnalysisData == null )
                {
                    _marketAnalysisData = new MarketAnalysisData();
                }

                if (_marketAnalysisData.DealerPreference == default(IMarketAnalysisDealerPreference))
                {
                    _marketAnalysisData.DealerPreference = MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(OwnerHandle);                    
                }

                if (_marketAnalysisData.VehiclePreference == default(IMarketAnalysisVehiclePreference))
                {
                    _marketAnalysisData.VehiclePreference = MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(OwnerHandle, VehicleHandle);                    
                }

                if (_marketAnalysisData.Pricing == default(IMarketAnalysisPricingInformation))
                {
                    // Get pricing information, passing in the vehicle preference for overriding various price points.
                    _marketAnalysisData.Pricing =
                        MarketAnalysisPricingInformation.GetMarketAnalysisPricingInformation(_marketAnalysisData.VehiclePreference, SearchHandle);                    
                }


                return _marketAnalysisData;
            }
            set { _marketAnalysisData = value; }
        }

        public void ShowEquipmentOnMarketComparison(bool value)
        {
            _printOverrides.MarketComparisonEquipment = value;
            _xmlWrapper.UpdatePrintOverrides(_printOverrides);
        }

        public void ShowAdPreviews(bool value)
        {
            _printOverrides.ShowAdPreviews = value;
            _xmlWrapper.UpdatePrintOverrides(_printOverrides);
        }

        public bool ShowAdPreviews()
        {
            return _printOverrides.ShowAdPreviews;
        }


        public PaymentInformation LeaseInformation
        {
            get { return _leaseInformation; }
            set
            {
                _leaseInformation = value;
                _xmlWrapper.UpdatePaymentInformation(_leaseInformation, _ownInformation);
            }
        }

        public PaymentInformation OwnInformation
        {
            get { return _ownInformation; }
            set
            {
                _ownInformation = value;
                _xmlWrapper.UpdatePaymentInformation(_leaseInformation, _ownInformation);
            }
        }

        public DealerSummaryInformation GetDealerSummary()
        {
            return new DealerSummaryInformation(OwnerHandle);
        }

        public void SaveXml(DealerSummaryInformation info)
        {
            SerializableXmlDocument xmlDoc = new SerializableXmlDocument();
            xmlDoc.LoadXml(info.Xml);

            XmlNode logoNode = xmlDoc.CreateElement("logo");
            XmlAttribute width = xmlDoc.CreateAttribute("width");
            XmlAttribute height = xmlDoc.CreateAttribute("height");
            width.Value = info.DisplayWidth.ToString();
            height.Value = info.DisplayHeight.ToString();
            logoNode.Attributes.Append(height);
            logoNode.Attributes.Append(width);
            logoNode.InnerText = info.DisplayUrl;

            xmlDoc.SelectSingleNode("dealer").AppendChild(logoNode);
            _xmlWrapper.SaveXml(NodeType.dealer, xmlDoc);
        }

        public VehicleYearModelInformation GetVehicleYearModel()
        {
            return new VehicleYearModelInformation(OwnerHandle, VehicleHandle);
        }

        public void SaveXml(VehicleYearModelInformation info)
        {
            SerializableXmlDocument xmlDoc = new SerializableXmlDocument();
            xmlDoc.LoadXml(info.Xml);

            _xmlWrapper.SaveXml(NodeType.vehicleyearmodel, xmlDoc);
        }

        public VehicleImageInformation GetVehicleImage()
        {
           return new VehicleImageInformation(OwnerHandle, VehicleHandle); 
        }

        public void SaveXml(VehicleImageInformation info)
        {
            SerializableXmlDocument xmlDoc = new SerializableXmlDocument();

            // Build the xml.
            IDictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("imagePath", info.DisplayUrl);
            attributes.Add("height", info.DisplayHeight.ToString());
            attributes.Add("width", info.DisplayWidth.ToString());
            attributes.Add("display", info.PhotoPreference.IsDisplayed.ToString().ToLower());
            string xml = TagBuilder.Wrap("vehiclephoto", attributes);

            xmlDoc.LoadXml(xml);
            _xmlWrapper.SaveXml(NodeType.vehiclephoto, xmlDoc);
        }

        public virtual VehicleFeaturesInformation GetVehicleFeatures()
        {
            return new VehicleFeaturesInformation(OwnerHandle, VehicleHandle);
        }

        public void SaveXml(VehicleFeaturesInformation info)
        {
            SerializableXmlDocument xmlDoc = new SerializableXmlDocument();

            XmlNode list = xmlDoc.AppendChild(xmlDoc.CreateElement("vehicleequipment"));

            XmlAttribute displayAttr = xmlDoc.CreateAttribute("display");
            displayAttr.Value = info.EquipmentFacade.IsDisplayed.ToString().ToLower();
            list.Attributes.Append(displayAttr);

            foreach (Equipment.Equipment equipment in info.Equipment)
            {
                XmlNode item = list.AppendChild(xmlDoc.CreateElement("item"));
                item.AppendChild(xmlDoc.CreateTextNode(equipment.Name));
            }

            _xmlWrapper.SaveXml(NodeType.vehicleequipment, xmlDoc);
        }


        public ConsumerHighlightsInformation GetConsumerHighlights()
        {
            return new ConsumerHighlightsInformation(OwnerHandle, VehicleHandle);
        }

        public void SaveXml(ConsumerHighlightsInformation info)
        {
            SerializableXmlDocument xmlDoc = new SerializableXmlDocument();

            string consumerHighlightsXml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(info.Preference, true, ConsumerHighlightsVehiclePreferenceXmlBuilder.CarfaxLogoUrl);
            xmlDoc.LoadXml(consumerHighlightsXml);

            _xmlWrapper.SaveXml(NodeType.consumerhighlights, xmlDoc);
        }


        public CertifiedBenefitsInformation GetCertifiedBenefits()
        {
            return new CertifiedBenefitsInformation(OwnerHandle, VehicleHandle);
        }

        public void SaveXml(CertifiedBenefitsInformation info)
        {
            if (info.CertifiedExists)
            {
                SerializableXmlDocument xmlDoc = new SerializableXmlDocument();
                string xml = info.Preference.AsXml();

                if (xml.Length > 0)
                    xmlDoc.LoadXml(xml);

                _xmlWrapper.SaveXml(NodeType.certifiedbenefits, xmlDoc);
            }
        }

        public PricingAnalysisInformation GetPricingAnalysis()
        {
            return new PricingAnalysisInformation(OwnerHandle);
        }

        public void SaveXml(PricingAnalysisInformation info)
        {
            SerializableXmlDocument xmlDoc = new SerializableXmlDocument();

            MarketAnalysisXmlBuilder xmlBuilder = new MarketAnalysisXmlBuilder(
                MarketAnalysis.DealerPreference,
                MarketAnalysis.VehiclePreference,
                MarketAnalysis.Pricing, OfferPrice);

            xmlDoc.LoadXml(xmlBuilder.AsXml());

            // Adjust the logo dimensions
            XmlNodeList logoNodes = xmlDoc.SelectNodes("//logo");

            if (logoNodes != null)
            {
                foreach (XmlNode logoNode in logoNodes)
                {
                    // Get the logo image path, height and width.
                    string url = logoNode.InnerText;
                    DealerLogoCommand command = new DealerLogoCommand(url, PricingAnalysisInformation.DEFAULT_IMAGE, OwnerHandle, PricingAnalysisInformation.MAX_WIDTH, PricingAnalysisInformation.MAX_HEIGHT);
                    AbstractCommand.DoRun(command);

                    // Set the logo url attribute.
                    logoNode.InnerText = command.DisplayUrl;

                    // Add height/width attributes.               
                    XmlAttribute widthAttribute = xmlDoc.CreateAttribute("width");
                    XmlAttribute heightAttribute = xmlDoc.CreateAttribute("height");

                    widthAttribute.Value = command.DisplayWidth.ToString();
                    heightAttribute.Value = command.DisplayHeight.ToString();

                    logoNode.Attributes.Append(widthAttribute);
                    logoNode.Attributes.Append(heightAttribute);
                }
            }

            _xmlWrapper.SaveXml(NodeType.pricing, xmlDoc);
        }


        public MarketListingsInformation GetMarketListings()
        {
            // decide on which version of market listings here (PING or ProfitMAX)
            return new MarketListingsInformation(OwnerHandle, VehicleHandle, OfferPrice, _searchAdapter);
        }

        public void SaveXml(MarketListingsInformation info)
        {
            SerializableXmlDocument xmlDoc = new SerializableXmlDocument();
            
            string xml = SearchAdapterXmlBuilder.AsXml(info.Listings, OfferPrice);
            xmlDoc.LoadXml(xml);

            XmlAttribute isDisplyedAttr = xmlDoc.CreateAttribute("isDisplayed");
            XmlAttribute showCertifiedColumnAttr = xmlDoc.CreateAttribute("showCertifiedColumn");
            XmlAttribute showDealerColumnAttr = xmlDoc.CreateAttribute("showDealerColumn");
            XmlNode rootNode = xmlDoc.SelectSingleNode("marketlistings");

            showCertifiedColumnAttr.Value = bool.Parse("true").ToString().ToLower();
            showDealerColumnAttr.Value = info.Preference.ShowDealerName.ToString().ToLower();
            isDisplyedAttr.Value = info.Preference.IsDisplayed.ToString().ToLower();

            rootNode.Attributes.Append(isDisplyedAttr);
            rootNode.Attributes.Append(showCertifiedColumnAttr);
            rootNode.Attributes.Append(showDealerColumnAttr);

            _xmlWrapper.SaveXml(NodeType.marketlistings, xmlDoc);
        }

        private void GenerateNodeXmlIfNotExists()
        {
			Log.Debug("Generating Xml for dealer node.");
            GenerateNodeXmlIfNotExists(NodeType.dealer);
			Log.Debug("Generating Xml for vehicleyearmodel node.");
            GenerateNodeXmlIfNotExists(NodeType.vehicleyearmodel);
			Log.Debug("Generating Xml for vehiclephoto node.");
            GenerateNodeXmlIfNotExists(NodeType.vehiclephoto);
			Log.Debug("Generating Xml for vehicleequipment node.");
			GenerateNodeXmlIfNotExists(NodeType.vehicleequipment);
			Log.Debug("Generating Xml for consumerhighlights node.");
			GenerateNodeXmlIfNotExists(NodeType.consumerhighlights);
			Log.Debug("Generating Xml for certifiedbenefits node.");
			GenerateNodeXmlIfNotExists(NodeType.certifiedbenefits);
			Log.Debug("Generating Xml for pricing node.");
			GenerateNodeXmlIfNotExists(NodeType.pricing);
			Log.Debug("Generating Xml for marketlistings node.");
			GenerateNodeXmlIfNotExists(NodeType.marketlistings);
        }

        private void GenerateNodeXmlIfNotExists(NodeType nodeType)
        {
            if (!_xmlWrapper.XmlExists(nodeType))
                GenerateNodeXml(nodeType);
        }
        
        private void GenerateNodeXml(NodeType nodetype)
        {
            if(nodetype == NodeType.dealer)
                SaveXml(GetDealerSummary());
            if(nodetype == NodeType.vehicleyearmodel)
                SaveXml(GetVehicleYearModel());
            if(nodetype == NodeType.vehiclephoto)
                SaveXml(GetVehicleImage());
            if(nodetype == NodeType.vehicleequipment)
                SaveXml(GetVehicleFeatures());
            if(nodetype == NodeType.consumerhighlights)
                SaveXml(GetConsumerHighlights());
            if(nodetype == NodeType.certifiedbenefits)
                SaveXml(GetCertifiedBenefits());
            if(nodetype == NodeType.pricing)
                SaveXml(GetPricingAnalysis());
            if(nodetype == NodeType.marketlistings)
                SaveXml(GetMarketListings());
        }

        public SerializableXmlDocument GetXml(NodeType nodeType)
        {
            GenerateNodeXmlIfNotExists(nodeType);
            return _xmlWrapper.GetXml(nodeType);
        }

        public SerializableXmlDocument GetXml()
        {
            GenerateNodeXmlIfNotExists();
            return _xmlWrapper.GetXml();
        }

        public ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference()
        {
            var command = new GetValueAnalyzerVehiclePreference(OwnerHandle, VehicleHandle);
            AbstractCommand.DoRun(command);
            return command.Preference;
        }


        public void Reset()
        {
            _customerPacketOptions = null;
            _valueAnalyzerPacketOptions = null;
            _offerPrice = null;
            _validUntilDate = null;
            _dealerDisclaimer = null;
            _packetType = Packets.SalesPacket;
            ResetMarketAnalysis();

            _printOverrides = new PrintOverridesDTO(); ;
            _leaseInformation = new PaymentInformation();
            _ownInformation = new PaymentInformation();

            _xmlWrapper = new DealXmlWrapper(this);
            _xmlWrapper.UpdatePrintOverrides(_printOverrides);
        }

        public void ResetMarketAnalysis()
        {
            _marketAnalysisData = null;
        }
    }

    [Serializable]
    internal class DealXmlWrapper
    {
        private XDocument _xmlDoc;

        // XPath
        private const string OFFER_FOOTER = "offerFooter";
        private const string ROOT_NODE = "salesaccelerator";
        private const string OVER = "overrides";
        private const string MARKET = "marketcomparisonequipment";
        private const string ADPREVIEWS = "showadpreviews";
        private const string DISCLAIMER = "disclaimer";
        private const string VA_OPTIONS = "valueAnalyzerOptions";
        private const string PAY = "paymentInformation";
        
        private readonly Deal _deal;

        internal DealXmlWrapper(Deal deal)
        {
            _deal = deal;
            _xmlDoc = CreateDocument();                        
        }
        
        internal void UpdatePaymentInformation(PaymentInformation lease, PaymentInformation own)
        {
            
            // Delete the existing node.
            var paymentNode = Root().Element(PAY);
            if( paymentNode != null )
            {
                paymentNode.Remove();
            }

            // Create a new payment information node
            var xml = PaymentInformationXmlBuilder.AsXml(lease, own);
            Root().Add(XElement.Parse(xml));

        }

        internal void UpdatePrintOverrides(PrintOverridesDTO overrides)
        {
            // Make sure we have an overrides node.
            var overridesNode = Root().Element(OVER);
            if (overridesNode == null)
            {
                Root().Add(new XElement(XName.Get(OVER)));
            }

            // Delete the market comparison nodes.
            Root().Descendants(MARKET).Remove();

            // Delete the ad previews node
            Root().Descendants(ADPREVIEWS).Remove();

            // Create and add the market node.
            XElement marketElement = new XElement(MARKET, overrides.MarketComparisonEquipment.ToString().ToLower());
            Root().Element(OVER).Add(marketElement);

            // Create and add the preview node
            XElement adPreviewElement = new XElement(ADPREVIEWS, overrides.ShowAdPreviews.ToString().ToLower());
            Root().Element(OVER).Add(adPreviewElement);
        }
       
        private static XDocument CreateDocument()
        {
            var doc = new XDocument();

            var element = new XElement(ROOT_NODE);                
            var attr = new XAttribute("date", DateTime.Today.ToShortDateString());
            element.Add( attr );
            doc.Add( element );
            return doc;
        }

        private XElement Root()
        {
            return _xmlDoc.Root;
        }

        /// <summary>
        /// Save the xml to the document, replacing whatever is there.
        /// </summary>
        /// <param name="nodeType"></param>
        /// <param name="node"></param>
        public void SaveXml(NodeType nodeType, SerializableXmlDocument node)
        {
            Trace.WriteLine("Saving node: " + GetName(nodeType) + " value " + node.InnerXml);

            // Get an XElement
            XElement element = XElement.Parse(node.InnerXml);
  
            // Are we adding or replacing the node?
            var existingNode = GetNode(nodeType);

            if (existingNode == null)
            {
                // Add the element.
                Root().Add( element );
            }
            else
            {
                // Replace the element.
                existingNode.ReplaceWith( element );
            }
        }

        public bool XmlExists(NodeType nodeType)
        {
            return GetNode(nodeType) != null;
        }
        
        /// <summary>
        /// Get the specified node.  If no node is supplied, returns the entire document.
        /// </summary>
        /// <param name="nodeType"></param>
        /// <returns></returns>
        public SerializableXmlDocument GetXml(NodeType nodeType)
        {
            var node = GetNode(nodeType);
            var doc = new SerializableXmlDocument {InnerXml = node.ToString()};
            return doc;
        }

        /// <summary>
        /// Returns the entire document.
        /// </summary>
        /// <returns></returns>
        public SerializableXmlDocument GetXml()
        {
            // Remove existing nodes.
            Root().Descendants(OFFER_FOOTER).Remove();
            Root().Descendants(DISCLAIMER).Remove();
            Root().Descendants(VA_OPTIONS).Remove();

            // Create and Add new nodes.
            var offerNode = CreateOfferFooter();
            var valueAnalyzerOptionsNode = CreateValueAnalyzerOptions();
            var disclaimerNode = CreateDisclaimer();

            Root().Add( offerNode );
            Root().Add( valueAnalyzerOptionsNode );
            Root().Add( disclaimerNode );

            // Create a new xml doc and return it.
            var doc = new SerializableXmlDocument();
            doc.LoadXml(_xmlDoc.ToString());

            return doc;
        }

        private static string GetName(NodeType nodeType)
        {
            return Enum.GetName(typeof(NodeType), nodeType);
        }

        private XElement GetNode(NodeType nodeType)
        {
            var nodeName = GetName(nodeType);
            return Root().Element(XName.Get(nodeName));
        }



        private XElement CreateOfferFooter()
        {
            var customer = _deal.CustomerPacketOptions.Customer;
            var footer =
                new XElement(OFFER_FOOTER,
                             new XAttribute("expiredate", _deal.ValidUntilDate),
                             new XElement("customer",
                                          new XElement("name", customer.FirstName + " " + customer.LastName),
                                          new XElement("phone", customer.PhoneNumber),
                                          new XElement("email", customer.Email)),
                             new XElement("salesperson",
                                          new XElement("name", _deal.CustomerPacketOptions.SalesPerson.Name)),
                             new XElement("payments",
                                          new XElement("down", _deal.CustomerPacketOptions.DownPayment.ToString()),
                                          new XElement("monthly", _deal.CustomerPacketOptions.MonthlyPayment.ToString()))
                    );


            return footer;

        }

        private XElement CreateDisclaimer()
        {
            var disclaimerCommand = new DisclaimerCommand(_deal.OwnerHandle, _deal.VehicleHandle, _deal.SearchHandle);
            AbstractCommand.DoRun(disclaimerCommand);
            return XElement.Parse(disclaimerCommand.Xml);
        }

        private XElement CreateValueAnalyzerOptions()
        {
            var preference = _deal.GetValueAnalyzerPreference();

            var valueAnalyzerOptionsElement =
                new XElement(VA_OPTIONS,
                             new XElement("displayVehicleDescription", preference.DisplayVehicleDescription.ToString().ToLower()),
                             new XElement("useLongMarketListings", preference.UseLongMarketingListingLayout.ToString().ToLower()));

            return valueAnalyzerOptionsElement;

        }


    }     
    
}
