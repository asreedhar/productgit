﻿using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.Photos.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class VehicleImageInformation
    {
        private string _ownerHandle;
        private string _vehicleHandle;

        private VehicleImageCommand _vehicleCommand;
        private GetPhotoVehiclePreference _preferenceCommand;

        public VehicleImageInformation(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;

            _vehicleCommand = new VehicleImageCommand(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(_vehicleCommand);

            _preferenceCommand = new GetPhotoVehiclePreference(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(_preferenceCommand);
        }

        public string DisplayUrl
        {
            get
            {
                return _vehicleCommand.DisplayUrl;
            }
        }

        public int DisplayWidth
        {
            get
            {
                return _vehicleCommand.DisplayWidth;
            }
        }

        public int DisplayHeight
        {
            get
            {
                return _vehicleCommand.DisplayHeight;
            }
        }

        public PhotoVehiclePreferenceTO PhotoPreference
        {
            get
            {
                return _preferenceCommand.Preference;
            }
        }
    }
}
