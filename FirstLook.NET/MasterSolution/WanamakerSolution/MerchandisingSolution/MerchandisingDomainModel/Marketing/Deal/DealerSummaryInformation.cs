﻿using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;

namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class DealerSummaryInformation
    {
        private const int MAX_HEIGHT = 99;
        private const int MAX_WIDTH = 132;

        private string _ownerHandle;

        private DealerSummaryCommand _dealerSummaryCommand;
        private DealerLogoCommand _dealerLogoCommand;

        public DealerSummaryInformation(string ownerHandle)
        {
            _ownerHandle = ownerHandle;

            _dealerSummaryCommand = new DealerSummaryCommand(_ownerHandle);
            AbstractCommand.DoRun(_dealerSummaryCommand);

            _dealerLogoCommand = new DealerLogoCommand(string.Empty, null, _ownerHandle, MAX_WIDTH, MAX_HEIGHT);
            AbstractCommand.DoRun(_dealerLogoCommand);
        }

        public string DisplayUrl
        {
            get
            {
                return _dealerLogoCommand.DisplayUrl;
            }
        }

        public int DisplayWidth
        {
            get
            {
                return _dealerLogoCommand.DisplayWidth;
            }
        }

        public int DisplayHeight
        {
            get
            {
                return _dealerLogoCommand.DisplayHeight;
            }
        }

        public string Xml
        {
            get
            {
                return _dealerSummaryCommand.DealerSummary;
            }
        }
        
        
    }

}
