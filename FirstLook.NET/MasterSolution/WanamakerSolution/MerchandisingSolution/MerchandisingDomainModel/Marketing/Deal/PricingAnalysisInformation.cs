﻿namespace FirstLook.Merchandising.DomainModel.Marketing.Deal
{
    public class PricingAnalysisInformation
    {
        public const string DEFAULT_IMAGE = "/pricing/Public/Images/logoFirstLook1.png";
        public const int MAX_HEIGHT = 32;
        public const int MAX_WIDTH = 112;

        private string _ownerHandle;

        public PricingAnalysisInformation(string ownerHandle)
        {
            _ownerHandle = ownerHandle;

        }
    }
}
