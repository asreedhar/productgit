﻿using System.Collections.Generic;
using System.Text;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    /// <summary>
    /// A cache decorator around an IMarketListingVehiclePreferenceDataAccess.  Only the Search() method is
    /// cached currently.
    /// </summary>
    public class MarketListingVehiclePreferenceDataAccessCache : IMarketListingVehiclePreferenceDataAccess
    {
        private const string DELIM = ";";
        private const int CACHE_EXPIRATION_SECONDS = 300; // 5 minutes

        private readonly IMarketListingVehiclePreferenceDataAccess _dataAccess;
        private readonly ICache _cache;

        public MarketListingVehiclePreferenceDataAccessCache(IMarketListingVehiclePreferenceDataAccess dataAccess, ICache cache)
        {
            _dataAccess = dataAccess;
            _cache = cache;
        }

        public MarketListingVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle, int offerPrice)
        {
            return _dataAccess.Create(ownerHandle, vehicleHandle, offerPrice);
        }

        public MarketListingVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle, int offerPrice)
        {
            return _dataAccess.Fetch(ownerHandle, vehicleHandle, offerPrice);
        }

        public void Insert(MarketListingVehiclePreferenceTO dto, string userName)
        {
            _dataAccess.Insert(dto, userName);
        }

        public void Update(MarketListingVehiclePreferenceTO dto, string userName)
        {
            _dataAccess.Update(dto, userName);
        }

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            return _dataAccess.Exists(ownerHandle, vehicleHandle);
        }

        public void Delete(int preferenceId, string userName)
        {
            _dataAccess.Delete(preferenceId, userName);
        }
        public int GetVehicleID( string vehicleID )
        {
            return _dataAccess.GetVehicleID( vehicleID );
        }

        //public IEnumerable<IMarketListing> Search(string ownerHandle, string vehicleHandle, string userName)
        //{
        //    // try to get it from the cache.
        //    string key = CacheKey("MarketListingVehiclePreferenceDataAccessCache.Search", ownerHandle, vehicleHandle);

        //    var results = _cache.Get(key) as IEnumerable<IMarketListing>;

        //    if( results != null )
        //    {
        //        return results;
        //    }

        //    // Get results from the db and cache them.
        //    results = _dataAccess.Search(ownerHandle, vehicleHandle, userName);
        //    _cache.Set(key, results, CACHE_EXPIRATION_SECONDS);

        //    return results;
        //}

        //public IEnumerable<IMarketListing> Filter(IEnumerable<IMarketListing> marketListings, IMarketListingFilterSpecification searchSpecification)
        //{
        //    return _dataAccess.Filter(marketListings, searchSpecification);
        //}

        private static string CacheKey(string methodName, string ownerHandle, string vehicleHandle)
        {
            var sb = new StringBuilder();
            sb.Append(methodName);
            sb.Append(DELIM);
            sb.Append("oh:");
            sb.Append(ownerHandle);
            sb.Append(DELIM);
            sb.Append("vh:");
            sb.Append(vehicleHandle);
            sb.Append(DELIM);
            return sb.ToString();
        }
    }
}
