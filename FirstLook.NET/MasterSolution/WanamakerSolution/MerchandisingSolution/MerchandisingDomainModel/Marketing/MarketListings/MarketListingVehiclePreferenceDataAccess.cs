﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    public class MarketListingVehiclePreferenceDataAccess : IMarketListingVehiclePreferenceDataAccess
    {
        public MarketListingVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle, int offerPrice)
        {
            var dto = new MarketListingVehiclePreferenceTO();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketListingVehiclePreference#Create";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("offerPrice", DbType.Int32, false, offerPrice);

                    dto.OwnerHandle = ownerHandle;
                    dto.VehicleHandle = vehicleHandle;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        reader.Read();
                        dto.HasMarketListingPreference = reader.GetBoolean(reader.GetOrdinal("HasMarketListingPreference"));
                        dto.IsDisplayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));

                        dto.MileageLow = reader.GetInt32(reader.GetOrdinal("MileageLow"));
                        if (dto.MileageLow < 0)
                            dto.MileageLow = 0; //Make sure the value makes sense (above 0)

                        dto.MileageHigh = reader.GetInt32(reader.GetOrdinal("MileageHigh"));
                        dto.PriceLow = reader.GetInt32(reader.GetOrdinal("PriceLow"));
                        if (dto.PriceLow < 0)
                            dto.PriceLow = 0; //Make sure the value makes sense (above 0)

                        dto.PriceHigh = reader.GetInt32(reader.GetOrdinal("PriceHigh"));
                    }
                }
            }


            dto.Sortcode = SortCode.ListPrice;
            dto.SortAscending = false;
            dto.ShowDealerName = false;
            dto.DisplayListingsIDs = new List<int>();
            dto.RemovedListingsIDs = new List<int>();

            return dto;
        }

        public MarketListingVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle, int offerPrice)
        {
            /*need to join against displayed items*/

            var dto = new MarketListingVehiclePreferenceTO();
            var displayListingsIDs = new List<int>();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketListingVehiclePreference#Fetch";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("offerPrice", DbType.Int32, false, offerPrice);

                    dto.OwnerHandle = ownerHandle;
                    dto.VehicleHandle = vehicleHandle;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new DataException("Missing Market Listing Vehicle Preference information");

                        dto.ID = reader.GetInt32(reader.GetOrdinal("MarketListingVehiclePreferenceId"));
                        dto.HasMarketListingPreference = reader.GetBoolean(reader.GetOrdinal("HasMarketListingPreference"));
                        dto.IsDisplayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        dto.ShowDealerName = reader.GetBoolean(reader.GetOrdinal("ShowDealerName"));
                        dto.MileageLow = reader.GetInt32(reader.GetOrdinal("MileageLow"));
                        if (dto.MileageLow < 0)
                            dto.MileageLow = 0; //Make sure the value makes sense (above 0)

                        dto.MileageHigh = reader.GetInt32(reader.GetOrdinal("MileageHigh"));
                        dto.PriceLow = reader.GetInt32(reader.GetOrdinal("PriceLow"));
                        if (dto.PriceLow < 0)
                            dto.PriceLow = 0; //Make sure the value makes sense (above 0)

                        dto.PriceHigh = reader.GetInt32(reader.GetOrdinal("PriceHigh"));

                        dto.Sortcode = (SortCode)reader.GetInt32(reader.GetOrdinal("SortCode"));
                        dto.SortAscending = reader.GetBoolean(reader.GetOrdinal("SortAscending"));
                    }

                    //
                    // Run a second query to get search overrides - these are listings that the user wants to show.
                    //
                    command.Parameters.Clear();
                    command.CommandText = "Marketing.MarketListingVehiclePreferenceSearchOverrides#Fetch";
                    command.AddParameterWithValue("MarketListingVehiclePreferenceId", DbType.Int32, false, dto.ID);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int vehicleId = reader.GetInt32(reader.GetOrdinal("VehicleId"));
                            displayListingsIDs.Add(vehicleId);
                        }
                    }

                }
            }

            dto.DisplayListingsIDs = displayListingsIDs;
            dto.RemovedListingsIDs = new List<int>();

            return dto;
        }

        public void Insert(MarketListingVehiclePreferenceTO dto, string userName)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.MarketListingVehiclePreference#Insert";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("vehicleHandle", DbType.String, false, dto.VehicleHandle);
                        command.AddParameterWithValue("ownerHandle", DbType.String, false, dto.OwnerHandle);
                        command.AddParameterWithValue("IsDisplayed", DbType.Boolean, false, dto.IsDisplayed);
                        command.AddParameterWithValue("ShowDealerName", DbType.Boolean, false, dto.ShowDealerName);
                        command.AddParameterWithValue("SortCode", DbType.Int32, false, (int)dto.Sortcode);
                        command.AddParameterWithValue("SortAscending", DbType.Boolean, false, dto.SortAscending);

                        command.AddParameterWithValue("InsertUser", DbType.String, false, userName);

                        //Add the out param
                        IDataParameter newId = command.AddOutParameter("MarketListingVehiclePreferenceId", DbType.Int32);

                        // Execute and capture the ID
                        command.ExecuteNonQuery();
                        dto.ID = (Int32)newId.Value;

                        //
                        // Insert search overrides.
                        //
                        command.Parameters.Clear();
                        command.CommandText = "Marketing.MarketListingVehiclePreferenceSearchOverrides#Save";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("MarketListingVehiclePreferenceId", DbType.Int32, false, dto.ID);
                        command.AddParameterWithValue("DisplayedIds", DbType.String, false, dto.DisplayListingsIDs.ToDelimitedString(","));
                        command.AddParameterWithValue("RemovedIds", DbType.String, false, dto.RemovedListingsIDs.ToDelimitedString(","));
                        command.AddParameterWithValue("InsertUser", DbType.String, false, userName);

                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        public void Update(MarketListingVehiclePreferenceTO dto, string userName)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.MarketListingVehiclePreference#Update";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("MarketListingVehiclePreferenceId", DbType.Int32, false, dto.ID);
                        command.AddParameterWithValue("IsDisplayed", DbType.Boolean, false, dto.IsDisplayed);
                        command.AddParameterWithValue("ShowDealerName", DbType.Boolean, false, dto.ShowDealerName);
                        command.AddParameterWithValue("UpdateUser", DbType.String, false, userName);
                        command.AddParameterWithValue("SortCode", DbType.Int32, false, (int)dto.Sortcode);
                        command.AddParameterWithValue("SortAscending", DbType.Boolean, false, dto.SortAscending);

                        command.ExecuteNonQuery();

                        //
                        // Update search overrides.
                        //
                        command.Parameters.Clear();
                        command.CommandText = "Marketing.MarketListingVehiclePreferenceSearchOverrides#Save";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("MarketListingVehiclePreferenceId", DbType.Int32, false, dto.ID);
                        command.AddParameterWithValue("DisplayedIds", DbType.String, false, dto.DisplayListingsIDs.ToDelimitedString(","));
                        command.AddParameterWithValue("RemovedIds", DbType.String, false, dto.RemovedListingsIDs.ToDelimitedString(","));
                        command.AddParameterWithValue("InsertUser", DbType.String, false, userName);

                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                connection.Open();
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketListingVehiclePreference#Exists";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, ownerHandle);

                    // create return value parameter
                    IDataParameter parameter = command.CreateParameter();
                    parameter.ParameterName = "ReturnValue";
                    parameter.DbType = DbType.Int32;
                    parameter.Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add(parameter);

                    int count = 0;
                    command.ExecuteNonQuery();

                    IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }

                    return (count > 0);
                }
            }
        }

        public void Delete(int preferenceId, string userName)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                connection.Open();
                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.MarketListingVehiclePreference#Delete";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("MarketListingVehiclePreferenceId", DbType.Int32, false, preferenceId);
                        command.AddParameterWithValue("DeleteUser", DbType.String, false, userName);
                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        public int GetVehicleID(string vehicleHandle)
        {
            int vehicleId;
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed) { connection.Open(); }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.MarketListing#VehicleID#Fetch";
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, vehicleHandle);
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read()) { throw new DataException("Unable to get MarketListing VehicleID for Vehicle Handle " + vehicleHandle); }
                        vehicleId = reader.GetInt32(reader.GetOrdinal("VehicleId"));
                    }
                }
            }
            return vehicleId;
        }
    }
}
