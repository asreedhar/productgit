using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    [Serializable]
    public class MarketListingPreference : BusinessBase<MarketListingPreference>
    {
        public const int Unlimited = 1000000;
        #region Business Methods
        
        private Int32 _id;
        private String _ownerHandle;
        private Boolean _isDisplayed ;
        private Int32 _mileageDeltaAbove;
        private Int32 _mileageDeltaBelow;
        private Int32 _priceDeltaAbove;
        private Int32 _priceDeltaBelow;
        
        public Int32 id
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("id", true);
                return _id;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("id", true);
                if (_id != value)
                {
                    _id = value;
                    PropertyHasChanged("id");
                }
            }
        }

        public string OwnerHandle
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("OwnerHandle", true);
                return _ownerHandle;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("OwnerHandle", true);
                if (value == null) value = String.Empty;
                _ownerHandle = value;
                PropertyHasChanged("OwnerHandle");
            }
        }


        public bool IsDisplayed
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("IsDisplayed", true);
                return _isDisplayed;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("IsDisplayed", true);
                _isDisplayed = value;
                PropertyHasChanged("IsDisplayed");
            }
        }


        public int MileageDeltaAbove
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("MileageDeltaAbove", true);
                return _mileageDeltaAbove;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("MileageDeltaAbove", true);
                _mileageDeltaAbove = value;
                PropertyHasChanged("MileageDeltaAbove");
            }
        }

        public int MileageDeltaBelow
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("MileageDeltaBelow", true);
                return _mileageDeltaBelow;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("MileageDeltaBelow", true);
                _mileageDeltaBelow = value;
                PropertyHasChanged("MileageDeltaBelow");
            }
        }

        public int PriceDeltaAbove
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("PriceDeltaAbove", true);
                return _priceDeltaAbove;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("PriceDeltaAbove", true);
                _priceDeltaAbove = value;
                PropertyHasChanged("PriceDeltaAbove");
            }
        }

        public int PriceDeltaBelow
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("PriceDeltaBelow", true);
                return _priceDeltaBelow;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("PriceDeltaBelow", true);
                _priceDeltaBelow = value;
                PropertyHasChanged("PriceDeltaBelow");
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
        }

        #endregion

        #region Authorization Rules

        protected override void AddAuthorizationRules()
        {
        }

        #endregion

        #region Factory Methods

        //This returns a new default MarketListingPreference for the user
        public static MarketListingPreference NewMarketListingPreference(String ownerHandle)
        {
            MarketListingPreference marketListingPreference = DataPortal.Create<MarketListingPreference>();
            marketListingPreference.OwnerHandle = ownerHandle;
            return marketListingPreference;
        }

        public static MarketListingPreference GetMarketListingPreference(String ownerHandle)
        {
            return DataPortal.Fetch<MarketListingPreference>(new Criteria(ownerHandle));
        }

        public static void DeleteMarketListingPreference(Int32 id)
        {
            new InvalidOperationException("MarketListingPreferences are not supposed to be deleted");
        }

        private MarketListingPreference()
        { /* Require use of factory methods */ }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly string _ownerHandle;
            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }
            public Criteria(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

        }

        [RunLocal]
        protected override void DataPortal_Create()
        {
            ValidationRules.CheckRules();
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketListingPreference#Create";
                    
                    using (IDataReader reader = command.ExecuteReader())
                    {

                        if (!reader.Read()) throw new DataException("Missing default Market Listing Preference information");

                        _id = 0; // set this to 0 as we are getting the defauts from the db
                        IsDisplayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        MileageDeltaBelow = reader.GetInt32(reader.GetOrdinal("MileageDeltaBelow"));
                        MileageDeltaAbove = reader.GetInt32(reader.GetOrdinal("MileageDeltaAbove"));
                        PriceDeltaBelow = reader.GetInt32(reader.GetOrdinal("PriceDeltaBelow"));
                        PriceDeltaAbove = reader.GetInt32(reader.GetOrdinal("PriceDeltaAbove"));
                    }
                }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketListingPreference#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);

                    _ownerHandle = criteria.OwnerHandle;

                    using (IDataReader reader = command.ExecuteReader())
                    {

                        if (!reader.Read())
                            throw new DataException("Missing Market Listing Preference information"); 
                        
                        _id = reader.GetInt32(reader.GetOrdinal("MarketListingPreferenceId"));
                        IsDisplayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                        MileageDeltaBelow = reader.GetInt32(reader.GetOrdinal("MileageDeltaBelow"));
                        MileageDeltaAbove = reader.GetInt32(reader.GetOrdinal("MileageDeltaAbove"));
                        PriceDeltaBelow = reader.GetInt32(reader.GetOrdinal("PriceDeltaBelow"));
                        PriceDeltaAbove = reader.GetInt32(reader.GetOrdinal("PriceDeltaAbove"));
                    }
                }
            }
            MarkOld();
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Insert()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using(IDbTransaction transaction = connection.BeginTransaction())
                {
                     using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                     {
                         command.CommandType = CommandType.StoredProcedure;
                         command.CommandText = "Marketing.MarketListingPreference#Insert";
                         command.Transaction = transaction;

                         command.AddParameterWithValue("OwnerHandle", DbType.String, false,OwnerHandle);
                         command.AddParameterWithValue("IsDisplayed", DbType.Boolean, false, IsDisplayed);
                         command.AddParameterWithValue("MileageDeltaBelow", DbType.Int32, false, MileageDeltaBelow);
                         command.AddParameterWithValue("MileageDeltaAbove", DbType.Int32, false, MileageDeltaAbove);
                         command.AddParameterWithValue("PriceDeltaBelow", DbType.Int32, false, PriceDeltaBelow);
                         command.AddParameterWithValue("PriceDeltaAbove", DbType.Int32, false, PriceDeltaAbove);

                         command.AddParameterWithValue("InsertUser", DbType.String, false, ApplicationContext.User.Identity.Name);

                         //Add the out param
                         IDataParameter newId = command.AddOutParameter("MarketListingPreferenceId", DbType.Int32);
                         
                         //Execute
                         command.ExecuteNonQuery();
                         _id = (Int32)newId.Value;


                     }
                     transaction.Commit();
                }
                MarkOld();
            }
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            if (!IsDirty) return;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.MarketListingPreference#Update";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("MarketListingPreferenceId", DbType.Int32, false, _id);
                        command.AddParameterWithValue("IsDisplayed", DbType.Boolean, false, IsDisplayed);
                        command.AddParameterWithValue("MileageDeltaBelow", DbType.Int32, false, MileageDeltaBelow);
                        command.AddParameterWithValue("MileageDeltaAbove", DbType.Int32, false, MileageDeltaAbove);
                        command.AddParameterWithValue("PriceDeltaBelow", DbType.Int32, false, PriceDeltaBelow);
                        command.AddParameterWithValue("PriceDeltaAbove", DbType.Int32, false, PriceDeltaAbove);
                        command.AddParameterWithValue("UpdateUser", DbType.String, false, ApplicationContext.User.Identity.Name);

                        
                        command.ExecuteNonQuery();

                    }
                    transaction.Commit();
                }
                MarkOld();

                
            }
        }

        protected override void DataPortal_DeleteSelf()
        {
            new InvalidOperationException("MarketListingPreferences are not supposed to be deleted");
        }

        #endregion

        #region DataSource

        # region DataSource
        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public MarketListingPreference Select(String ownerHandle)
            {
                return Exists(ownerHandle) ?
                    GetMarketListingPreference(ownerHandle) : NewMarketListingPreference(ownerHandle);
            }
            [DataObjectMethod(DataObjectMethodType.Update)]
            public void Update
                (
                    String ownerHandle,
                    bool isDisplayed,
                    int mileageDeltaAbove,
                    int mileageDeltaBelow,
                    int priceDeltaAbove,
                    int priceDeltaBelow
                )
            {
                MarketListingPreference marketListingPreference = (Exists(ownerHandle))
                                                                      ? GetMarketListingPreference(ownerHandle)
                                                                      : NewMarketListingPreference(ownerHandle);
                
                marketListingPreference.IsDisplayed = isDisplayed;
                marketListingPreference.MileageDeltaAbove = mileageDeltaAbove;
                marketListingPreference.MileageDeltaBelow = mileageDeltaBelow;
                marketListingPreference.PriceDeltaAbove = priceDeltaAbove;
                marketListingPreference.PriceDeltaBelow = priceDeltaBelow;
                marketListingPreference.Save();

            }
        }
        
        [DataObject]
        public class DataSourceRange
        {
            public class RangeVal
            {
                private readonly String _name;
                private readonly Int32 _val;

                public RangeVal(String name, Int32 val)
                {
                    _name = name;
                    _val = val;
                }

                public string Name
                {
                    get { return _name; }
                }

                public int Val
                {
                    get { return _val; }
                }
            }
            
            [DataObjectMethod(DataObjectMethodType.Select)]
            public RangeVal[] SelectRange()
            {
                RangeVal[] raneval = new RangeVal[]
                 {
                     new RangeVal("0",0),
                     new RangeVal("1000",1000),
                     new RangeVal("2000",2000),
                     new RangeVal("3000",3000),
                     new RangeVal("4000",4000),
                     new RangeVal("5000",5000),
                     new RangeVal("6000",6000),
                     new RangeVal("7000",7000),
                     new RangeVal("8000",8000),
                     new RangeVal("9000",9000),
                     new RangeVal("10000",10000),
                     new RangeVal("15000",15000),
                     new RangeVal("20000",20000),
                     new RangeVal("25000",25000),
                     new RangeVal("30000",30000),
                     new RangeVal("35000",35000),
                     new RangeVal("40000",40000),
                     new RangeVal("45000",45000),
                     new RangeVal("50000",50000),
                     new RangeVal("Unlimited", Unlimited)
                 }
                ;
                return raneval;
            }
        }
        # endregion 
        

        #endregion DataSource

        #region Exists

        public static bool Exists(String ownerHandle)
        {
            ExistsCommand result =
                DataPortal.Execute(new ExistsCommand(ownerHandle));
            return result.Exists;
        }

        [Serializable]
        private class ExistsCommand : CommandBase
        {
            private readonly String _ownerHandle = String.Empty;
            private Boolean _exists;

            public bool Exists
            {
                get { return _exists; }
            }


            public ExistsCommand(String ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            protected override void DataPortal_Execute()
            {

                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
                {
                    connection.Open();
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.MarketListingPreference#Exists";
                        command.AddParameterWithValue("OwnerHandle", DbType.String, false, _ownerHandle);

                        // create return value parameter
                        IDataParameter parameter = command.CreateParameter();
                        parameter.ParameterName = "ReturnValue";
                        parameter.DbType = DbType.Int32;
                        parameter.Direction = ParameterDirection.ReturnValue;
                        command.Parameters.Add(parameter);

                        int count = 0;
                        command.ExecuteNonQuery();
                        IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                        if (retParem != null)
                        {
                            count = Convert.ToInt32(retParem.Value);
                        }
                        _exists = (count > 0);
                    }
                }
            }
        }

        #endregion Exists


    }
}
