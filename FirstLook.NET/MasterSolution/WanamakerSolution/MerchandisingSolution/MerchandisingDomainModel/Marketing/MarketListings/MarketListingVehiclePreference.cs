using System;
using System.Collections.Generic;
using Csla;
using System.Text.RegularExpressions;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.Vehicle;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    [Serializable]
    public class MarketListingVehiclePreference : InjectableBusinessBase<MarketListingVehiclePreference>, IMarketListingSortSpecification
    {
        private const int InitialMarketListingCount = 10;
        public const int PriceIncrements = 10000;
        public const int MileageIncrements = 10000;


        #region Business Methods

        public string UserName
        {
            get { return ApplicationContext.User.Identity.Name; }
        }

        private int _id;
        public int ID
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("ID", true);
                return _id;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("ID", true);
                if (_id != value)
                {
                    _id = value;
                    PropertyHasChanged("ID");
                }
            }
        }


        private String _vehicleHandle = String.Empty;
        public String VehicleHandle
        {
            get
            {
                CanReadProperty("VehicleHandle", true);
                return _vehicleHandle;
            }
        }

        protected override object GetIdValue()
        {
            return VehicleHandle;
        }


        private String _ownerHandle = String.Empty;
        public String OwnerHandle
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("OwnerHandle", true);
                return _ownerHandle;
            }
        }

        private Boolean _isDisplayed;
        public bool IsDisplayed
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("IsDisplayed", true);
                return _isDisplayed;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("IsDisplayed", true);
                _isDisplayed = value;
                PropertyHasChanged("IsDisplayed");
            }
        }

        private bool _showDealerName;
        public bool ShowDealerName
        {
            get
            {
                CanReadProperty("ShowDealerName", true);
                return _showDealerName;
            }
            set
            {
                CanWriteProperty("ShowDealerName", true);
                _showDealerName = value;
                PropertyHasChanged("ShowDealerName");
            }
        }

        private IList<int> _displayListingsIDs;
        public IList<int> DisplayListingsIDs
        {
            get
            {
                CanReadProperty("DisplayListingsIDs", true);
                return _displayListingsIDs;

            }
            set
            {
                CanWriteProperty("DisplayListingsIDs", true);
                _displayListingsIDs = value;
                PropertyHasChanged("DisplayListingsIDs");
            }
        }

        private IList<int> _removedListingsIDs;
        public IList<int> RemovedListingsIDs
        {
            get
            {
                CanReadProperty("RemovedListingsIDs", true);
                return _removedListingsIDs;

            }
            set
            {
                CanWriteProperty("RemovedListingsIDs", true);
                _removedListingsIDs = value;
                PropertyHasChanged("RemovedListingsIDs");
            }
        }

        //
        // Sort behavior.
        //
        public SortCode Sortcode { get; set;}
        public bool SortAscending { get; set; }


        private Int32 _mileageHigh;
        public Int32 MileageHigh
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                return _mileageHigh;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("MileageHigh", true);
                _mileageHigh = value;
                PropertyHasChanged("MileageHigh");
            }
        }


        private Int32 _mileageLow;
        public Int32 MileageLow
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                return _mileageLow;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("MileageLow", true);
                _mileageLow = value;
                PropertyHasChanged("MileageLow");
            }
        }

        private Int32 _priceHigh;
        public Int32 PriceHigh
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                return _priceHigh;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("PriceHigh", true);
                _priceHigh = value;
                PropertyHasChanged("PriceHigh");
            }
        }


        private Int32 _priceLow;
        public Int32 PriceLow
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                return _priceLow;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            set
            {
                CanWriteProperty("PriceLow", true);
                _priceLow = value;
                PropertyHasChanged("PriceLow");
            }
        }

        private bool _hasMarketListingPreference;
        public bool HasMarketListingPreference
        {
            get
            {
                return _hasMarketListingPreference;
            }
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            // TODO: add validation rules
            //ValidationRules.AddRule(null, "");
        }

        #endregion

        #region Authorization Rules

        protected override void AddAuthorizationRules()
        {
        }


        #endregion

        #region Injected dependencies

        public IMarketListingSearchAdapter SearchAdapter { get; set; }
        public IMarketListingVehiclePreferenceDataAccess DataAccess { get; set; }

        #endregion

        #region Factory Methods

        public static MarketListingVehiclePreference NewMarketListingVehiclePreference(String vehicleHandle, String ownerHandle, Int32 offerPrice)
        {
            return DataPortal.Create<MarketListingVehiclePreference>(new Criteria(vehicleHandle, ownerHandle, offerPrice));
        }

        public static MarketListingVehiclePreference GetMarketListingVehiclePreference(String vehicleHandle, String ownerHandle, Int32 offerPrice)
        {
            return DataPortal.Fetch<MarketListingVehiclePreference>(new Criteria(vehicleHandle, ownerHandle, offerPrice));
        }

        public static void DeleteMarketListingVehiclePreference(String vehicleHandle, String ownerHandle)
        {
            DataPortal.Delete(new DeleteCriteria(vehicleHandle, ownerHandle));
        }

        public static MarketListingVehiclePreference GetOrCreate(string vehicleHandle, string ownerHandle, int offerPrice)
        {
            ExistsCommand command = new ExistsCommand(vehicleHandle, ownerHandle );
            DataPortal.Execute(command);
            
            return command.Exists ? GetMarketListingVehiclePreference(vehicleHandle, ownerHandle, offerPrice) : 
                                    NewMarketListingVehiclePreference(vehicleHandle, ownerHandle, offerPrice);
        }

        /// <summary>
        /// Private ctor.  Callers interact with factory methods.
        /// </summary>
        private MarketListingVehiclePreference()
        {                         
        }

        #endregion



        private int[] GetRemovedIDs(string currentSelectedItems, string orignalSelectedItems)
        {
            var removedIDS = new List<int>();
            int[] orignalSelectedValues = GetOrignalSelectedValues(orignalSelectedItems);
            int[] currentSelectedValues = GetCurrentSelectedValues(currentSelectedItems);
            Array.ForEach(orignalSelectedValues, x => { if (!currentSelectedValues.Contains(x)) removedIDS.Add(x); });

            return removedIDS.ToArray();
        }

        private static int[] GetAddedIDs(string currentSelectedItems, string orignalSelectedItems)
        {
            var addedIDs = new List<int>();
            int[] orignalSelectedValues = GetOrignalSelectedValues(orignalSelectedItems);
            int[] currentSelectedValues = GetCurrentSelectedValues(currentSelectedItems);
            Array.ForEach(currentSelectedValues, x => { if (!orignalSelectedValues.Contains(x)) addedIDs.Add(x); });

            return addedIDs.ToArray();
        }

        private static int[] GetOrignalSelectedValues(string orignalSelectedItems)
        {
            var selectedValues = new int[0];
            if (!String.IsNullOrEmpty(orignalSelectedItems))
            {
                var values = Regex.Split(orignalSelectedItems, @",\s*");
                selectedValues = values.Select(x => int.Parse(x)).ToArray();
            }

            return selectedValues;
        }

        private static int[] GetCurrentSelectedValues(string currentSelectedItems)
        {
            var selectedValues = new int[0];
            if (!String.IsNullOrEmpty(currentSelectedItems))
            {
                var values = Regex.Split(currentSelectedItems, @",\s*");
                selectedValues = values.Select(x => int.Parse(x)).ToArray();
            }

            return selectedValues;
        }

        public void RemoveAddSelectedItems(string currentSelectedItems, string orignalSelectedItems)
        {
            var removedIDs = GetRemovedIDs(currentSelectedItems, orignalSelectedItems);
            var addedIDs = GetAddedIDs(currentSelectedItems, orignalSelectedItems);
            if(removedIDs.Length > 0 || addedIDs.Length > 0)
                MarkDirty();

            Array.ForEach(removedIDs, x =>
            {
                DisplayListingsIDs.Remove(x);
                RemovedListingsIDs.Add(x);
            });
            Array.ForEach(addedIDs, x => DisplayListingsIDs.Add(x));
        }


        #region Data Access

        [Serializable]
        private class DeleteCriteria
        {
            private readonly String _vehicleHandle;
            private readonly String _ownerHandle;
            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }
            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public DeleteCriteria(String vehicleHandle, String ownerHandle)
            {
                _vehicleHandle = vehicleHandle;
                _ownerHandle = ownerHandle;
            }
        }

        [Serializable]
        private class Criteria
        {
            private readonly String _vehicleHandle;
            private readonly String _ownerHandle;
            private readonly Int32 _offerPrice;
            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }
            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public Int32 OfferPrice
            {
                get { return _offerPrice; }
            }

            public Criteria(String vehicleHandle, String ownerHandle, Int32 offerPrice)
            {
                _vehicleHandle = vehicleHandle;
                _ownerHandle = ownerHandle;
                _offerPrice = offerPrice;
            }
        }

        public static int GetMileageLowFilter(string ownerHandle, string vehicleHandle)
        {
            VehicleSummary summary = VehicleSummary.GetVehicleSummary(ownerHandle, vehicleHandle);
            int low = summary.Mileage - 12000;
            if (low < 0)
                low = 0;

            return ((int)Math.Round(low / (double)MileageIncrements)) * MileageIncrements;
        }

        private IList<int> GetInitialMarketListings(string ownerHandle, string vehicleHandle, int offerPrice)
        {
            bool useTrimNameFilter = false;
            string trimFilter = string.Empty;
            
            var marketlistings = SearchAdapter.Search(vehicleHandle, ownerHandle, UserName);

            int lowFilter = GetMileageLowFilter(ownerHandle, vehicleHandle);

            try
            {
                int vehicleId = SearchAdapter.GetVehicleID(vehicleHandle);
                trimFilter = marketlistings.First(m => m.VehicleId == vehicleId).VehicleName;
                useTrimNameFilter = true;
            }
            catch (Exception)
            {
                // We can't use the Trim Name filter; leave the boolean false.  This code will have to be changed in 6.2
                // The fix is likely to pull up the IsAnalyzedVehicle into the MarketListing class/interface (already in resultset from proc), then 
                // use this value to select the single market listing for which this is 'true' and use the vehicleName 
                // from that marketlisting for the trimName filter.
                // 2011-02-15 - dh
            }

            if(useTrimNameFilter && marketlistings.Count( x => x.VehicleName == trimFilter && x.Mileage > lowFilter && ((x.ListPrice - offerPrice) > 0) ) > 0 )
            {
                marketlistings = marketlistings.Where( x => x.VehicleName == trimFilter && x.Mileage > lowFilter && ((x.ListPrice - offerPrice) > 0) );
            }
            else
            {
                marketlistings = marketlistings.Where(x => x.Mileage > lowFilter && ((x.ListPrice - offerPrice) > 0));
            }

            return marketlistings.OrderByDescending( x => (x.ListPrice - offerPrice) )
                .Select( x => x.VehicleId )
                .Take( InitialMarketListingCount )
                .ToList();
        }

        private void DataPortal_Create(Criteria createCriteria)
        {
            ValidationRules.CheckRules();

            InitializeHiddenAndDisplayedLists();

            var dto = DataAccess.Create(createCriteria.OwnerHandle, createCriteria.VehicleHandle,
                                        createCriteria.OfferPrice);

            dto.DisplayListingsIDs = GetInitialMarketListings(createCriteria.OwnerHandle, createCriteria.VehicleHandle,
                                                              createCriteria.OfferPrice);

            FromTransferObject(dto);            

            // We have created nothing in the db. This has actually been a fetch
            MarkNew(); 
        }
       
 
        private void InitializeHiddenAndDisplayedLists()
        {
            // Initialize lists of market listings.
            if (DisplayListingsIDs == null)
                DisplayListingsIDs = new List<int>();
            if(RemovedListingsIDs == null)
                RemovedListingsIDs = new List<int>();
        }


        private void DataPortal_Fetch(Criteria criteria)
        {
            InitializeHiddenAndDisplayedLists();

            var dto = DataAccess.Fetch(criteria.OwnerHandle, criteria.VehicleHandle, criteria.OfferPrice);

            FromTransferObject(dto);
             
            MarkOld();
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Insert()
        {
            var dto = ToTransferObject();
             
            DataAccess.Insert(dto, UserName);

            FromTransferObject(dto);

            MarkOld();
        }
       

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            if (!IsDirty) return;

            var dto = ToTransferObject();
            DataAccess.Update(dto, UserName);

            MarkOld();
        }

        protected override void DataPortal_DeleteSelf()
        {
            DataPortal_Delete(new DeleteCriteria(_vehicleHandle, _ownerHandle));
        }

        private void DataPortal_Delete(DeleteCriteria criteria)
        {
            if (!Exists(criteria.VehicleHandle, criteria.OwnerHandle))
            {
                throw new ArgumentException(
                    "No Market listing Vehicle Preference for the specified VehicleHandle and OwnerHandle");
            }

            DataAccess.Delete(ID, UserName );
        }

        #endregion


        #region Exists

        public static bool Exists(String vehicleHandle, String ownerHandle)
        {
            ExistsCommand result =
                DataPortal.Execute(new ExistsCommand(vehicleHandle, ownerHandle));
            return result.Exists;
        }

        [Serializable]
        private class ExistsCommand : InjectableCommandBase
        {
            private readonly String _vehicleHandle = String.Empty;
            private readonly String _ownerHandle = String.Empty;
            private Boolean _exists;

            #region Injected dependencies
            public IMarketListingVehiclePreferenceDataAccess DataAccess { get; set; }
            #endregion

            public bool Exists
            {
                get { return _exists; }
            }


            public ExistsCommand(String vehicleHandle, String ownerHandle)
            {
                _vehicleHandle = vehicleHandle;
                _ownerHandle = ownerHandle;
            }

            protected override void DataPortal_Execute()
            {               
                _exists = DataAccess.Exists(_ownerHandle, _vehicleHandle);
            }
        }

        #endregion Exists


        private void FromTransferObject(MarketListingVehiclePreferenceTO dto)
        {
            _id = dto.ID;
            _hasMarketListingPreference = dto.HasMarketListingPreference;
            _isDisplayed = dto.IsDisplayed;
            _showDealerName = dto.ShowDealerName;
            _mileageHigh = dto.MileageHigh;
            _mileageLow = dto.MileageLow;
            _ownerHandle = dto.OwnerHandle;
            _priceHigh = dto.PriceHigh;
            _priceLow = dto.PriceLow;
           
            _vehicleHandle = dto.VehicleHandle;
            
            Sortcode = dto.Sortcode;
            SortAscending = dto.SortAscending;
            DisplayListingsIDs = new List<int>(dto.DisplayListingsIDs);
            RemovedListingsIDs = new List<int>(dto.RemovedListingsIDs);
        }

        private MarketListingVehiclePreferenceTO ToTransferObject()
        {
            var dto = new MarketListingVehiclePreferenceTO();
            dto.ID = _id;
            dto.HasMarketListingPreference = _hasMarketListingPreference;
            dto.IsDisplayed = _isDisplayed;
            dto.ShowDealerName = _showDealerName;
            dto.MileageHigh = _mileageHigh;
            dto.MileageLow = _mileageLow;
            dto.OwnerHandle = _ownerHandle;
            dto.PriceHigh = _priceHigh;
            dto.PriceLow = _priceLow;
            dto.VehicleHandle = _vehicleHandle;

            dto.Sortcode = Sortcode;
            dto.SortAscending = SortAscending;
            dto.DisplayListingsIDs = DisplayListingsIDs;
            dto.RemovedListingsIDs = RemovedListingsIDs;

            return dto;
        }

    }
}
