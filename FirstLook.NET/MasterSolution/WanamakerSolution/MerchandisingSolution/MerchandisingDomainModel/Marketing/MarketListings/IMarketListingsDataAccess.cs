﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    public interface IMarketListingsDataAccess
    {
        IEnumerable<IMarketListing> Search(string ownerHandle, string vehicleHandle, string userName);
        IEnumerable<IMarketListing> Filter(IEnumerable<IMarketListing> marketListings, IMarketListingFilterSpecification searchSpecification);
    }

    public interface IMarketListingsV1DataAccess : IMarketListingsDataAccess
    {
        // Old Ping - need this for AutoFac registry resolution
    }

    public interface IMarketListingsV2DataAccess : IMarketListingsDataAccess
    {
        // New Ping (ProfitMAX) - need this for AutoFac registry resolution
    }
}