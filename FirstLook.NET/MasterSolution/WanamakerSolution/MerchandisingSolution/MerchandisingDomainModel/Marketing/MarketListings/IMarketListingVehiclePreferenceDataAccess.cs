﻿namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    public interface IMarketListingVehiclePreferenceDataAccess
    {
        MarketListingVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle, int offerPrice);
        MarketListingVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle, int offerPrice);
        void Insert(MarketListingVehiclePreferenceTO dto, string userName);
        void Update(MarketListingVehiclePreferenceTO dto, string userName);
        bool Exists(string ownerHandle, string vehicleHandle);
        void Delete(int preferenceId, string userName);

        int GetVehicleID(string vehicleID);
    }
}
