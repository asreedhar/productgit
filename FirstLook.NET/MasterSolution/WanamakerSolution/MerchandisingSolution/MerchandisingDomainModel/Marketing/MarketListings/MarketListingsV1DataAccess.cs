using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Csla.Data;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    public class MarketListingsV1DataAccess : MarketListingsDataAccess, IMarketListingsV1DataAccess
    {
        public override IEnumerable<IMarketListing> Search(string ownerHandle, string vehicleHandle, string userName)
        {
            // We will return a list of market listings.
            var marketListings = new List<IMarketListing>();
            var dealerName = GetDealerName(ownerHandle);

            using (var connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                // Run these queries with dirty reads. They often timeout due to contention during PING loads.  ZB 6/17/2014
                using (var transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    // 
                    // First, let's get the SearchHandle
                    //
                    String searchHandle;
                    using (var command = new DataCommand(connection.CreateCommand()))
                    {
                        command.Transaction = transaction;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Pricing.HandleLookup_Search";
                        command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                        command.AddParameterWithValue("VehicleHandle", DbType.String, false, vehicleHandle);
                        command.AddParameterWithValue("InsertUser", DbType.String, false, userName);

                        try
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                if (!reader.Read())
                                {
                                    throw new DataException(
                                        "Missing Market Listing for the specified vehicle and owner handles");
                                }
                                searchHandle = reader.GetString(reader.GetOrdinal("SearchHandle"));
                            }
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }

                    if (String.IsNullOrEmpty(searchHandle))
                    {
                        transaction.Rollback();
                        throw new DataException("Can't look for Marketlisting with an empty search handle");
                    }

                    //
                    // Now get the listings.
                    //
                    using (var command = new DataCommand((connection.CreateCommand())))
                    {
                        command.Transaction = transaction;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Pricing.MarketListing";
                        command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                        command.AddParameterWithValue("SearchHandle", DbType.String, false, searchHandle);
                        // for now, SearchTypeID is hard coded to the value of 1 "overall percision": 
                        command.AddParameterWithValue("SearchTypeID", DbType.Int32, false, 1);
                        // for now, SortColumns is hard coded to the value of "ListPrice"
                        command.AddParameterWithValue("SortColumns", DbType.String, false, "ListPrice");
                        command.AddParameterWithValue("MaximumRows", DbType.Int32, false, Int32.MaxValue - 1);
                        command.AddParameterWithValue("StartRowIndex", DbType.Int32, false, 0);

                        try
                        {
                            using (var reader = new SafeDataReader(command.ExecuteReader()))
                            {
                                while (reader.Read())
                                {
                                    var marketListing = new MarketListing
                                    {
                                        DealerName = reader.GetString(reader.GetOrdinal("Seller")),
                                        VehicleName = reader.GetString(reader.GetOrdinal("VehicleDescription")),
                                        Color = reader.GetString(reader.GetOrdinal("VehicleColor")),
                                        Mileage = reader.GetInt32(reader.GetOrdinal("VehicleMileage")),
                                        IsCertified = reader.GetBoolean(reader.GetOrdinal("ListingCertified")),
                                        ListPrice = reader.GetInt32(reader.GetOrdinal("ListPrice")),
                                        VehicleDescription = StringHelper.StripHtmlTags(
                                            reader.GetString(reader.GetOrdinal("ListingSellerDescription"))),
                                        Options = reader.GetString(reader.GetOrdinal("ListingOptions")),
                                        VehicleId = reader.GetInt32(reader.GetOrdinal("VehicleId"))
                                    };


                                    var charLimit = (String.Compare(marketListing.DealerName, dealerName, true, CultureInfo.InvariantCulture) == 0)
                                        ? 250
                                        : 150;
                                    marketListing.VehicleDescription =
                                        new String(marketListing.VehicleDescription.Take(charLimit).ToArray());

                                    marketListings.Add(marketListing);
                                }
                            }
                        }
                        catch (SqlException e)
                        {
                            transaction.Rollback();
                            // no results is ok.                           
                            if (e.Number != 50200) throw;
                        }
                    }

                    // we're done with the txn, release resources.
                    transaction.Commit();
                }
            }

            // Return the market listings.
            return marketListings;
        }
    }
}