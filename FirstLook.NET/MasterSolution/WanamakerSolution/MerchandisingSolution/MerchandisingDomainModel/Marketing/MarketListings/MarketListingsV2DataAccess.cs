using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.BusinessUnitModel;
using FirstLook.Merchandising.ModelBuilder.BusinessUnit.Dto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.InventoryHeader;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.PricingAnalysis;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SearchCriteriaDto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Dto.SearchCriteriaDto;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.InventoryHeader;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.MarketListings;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SearchCriteria;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Model.SearchCriteria;
using FirstLook.Merchandising.ModelBuilder.MaxPricingTool.Repository;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using CertifiedProgamsForDealerMakeCommand = FirstLook.Merchandising.DomainModel.Marketing.Commands.CertifiedProgamsForDealerMakeCommand;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    public class MarketListingsV2DataAccess : MarketListingsDataAccess, IMarketListingsV2DataAccess
    {
        #region Properties
        public const string ListingsApi = "/v1/marketListing/search";
        public const string VhrApi = "/v1/dealerInfo/dealerId/{0}";
        public const string InventoryHeaderApi = "/v1/pinginventory/{0}/{1}";
        public const string SeachCriteriaUrl = "/v1/amazonSearchCriteria/dealerId/{0}/inventoryId/{1}";

        public string BusinessUnitApiUrl
        {
            get
            {
                var vhrapi = ConfigurationManager.AppSettings["BusinessUnitApi"];
                return vhrapi == null ? VhrApi : vhrapi.TrimEnd(new[] { '/', '\\' });
            }
        }

        public string SeachCriteriaApiurl
        {
            get
            {
                string apiUrl = ConfigurationManager.AppSettings["SearchCriteriaApiUrl"];
                return apiUrl == null ? SeachCriteriaUrl : apiUrl.TrimEnd(new[] { '/', '\\' });
            }
        }

        private readonly ILogger _logger;
        private readonly IRepository<MarketListingsModel, MarketListingsArgumentDto> _marketListingsRepository;

        private String _dealerName;
        #endregion

        #region Methods
        #region Construction
        internal MarketListingsV2DataAccess(IRepository<MarketListingsModel, MarketListingsArgumentDto> repository, ILogger logger)
        {
            _marketListingsRepository = repository;
            _logger = logger;
        }

        public MarketListingsV2DataAccess()
            : this(Registry.Resolve<IRepository<MarketListingsModel, MarketListingsArgumentDto>>(), Registry.Resolve<ILogger>())
        {
        }
        #endregion

        public override IEnumerable<IMarketListing> Search(string ownerHandle, string vehicleHandle, string userName)
        {
            List<IMarketListing> marketListings;

            try
            {
                // Get the owner, so we can get the business unit, so we can get the business unit id.
                var owner = Owner.GetOwner(ownerHandle);

                // we currently only support dealers and inventory
                if (owner.OwnerEntityType != OwnerEntityType.Dealer)
                    throw new NotSupportedException("The passed owner entity type is not supported.");

                _dealerName = owner.Name;

                // get the BusinessUnit from the owner
                var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);
                var inventory = Inventory.GetInventory(ownerHandle, vehicleHandle);
                var dealer = GetDealer(businessUnit.Id);
                var inventoryDetails = GetInventoryDetails(businessUnit.Id, inventory.Id);

                // build the search criteria to use for the market listing search
                var searchMarketListings = new SearchMarketListings
                {
                    BusinessUnitId = dealer.DealerId,
                    InventoryId = inventoryDetails.InventoryId,
                    Year = inventoryDetails.Year,
                    Make = inventoryDetails.Make,
                    Model = inventoryDetails.Model,
                    Latitude = Convert.ToString(dealer.Latitude),
                    Longitude = Convert.ToString(dealer.Longitude),
                    GetRanks = true,
                    Size = PricingAnalysisEnums.DefaultPageSize,
                    Aggregations = new List<string> {
                        PricingAnalysisEnums.Aggregations.equipment_terms.ToString(),
                        PricingAnalysisEnums.Aggregations.price_stats.ToString(),
                        PricingAnalysisEnums.Aggregations.trim_terms.ToString(),
                        PricingAnalysisEnums.Aggregations.transmission_terms.ToString(),
                        PricingAnalysisEnums.Aggregations.drive_train_terms.ToString(),
                        PricingAnalysisEnums.Aggregations.oem_certified_terms.ToString(),
                        PricingAnalysisEnums.Aggregations.engine_terms.ToString(),
                        PricingAnalysisEnums.Aggregations.fuel_type_terms.ToString(),
                        PricingAnalysisEnums.Aggregations.odometer_stats.ToString()
                    }
                };

                var searchCriteria = GetSearchCriteria(businessUnit.Id, inventory.Id);
                if (searchCriteria != null && searchCriteria.Criteria != null)
                {
                    searchMarketListings.Distance = searchCriteria.Criteria.Distance;
                    if(searchCriteria.Criteria.Listings != null)
                        searchMarketListings.RecentActive = searchCriteria.Criteria.Listings.Contains("RecentActive");
                    searchMarketListings.ActiveOnly = !searchMarketListings.RecentActive;
                    searchMarketListings.OemCertified = searchCriteria.Criteria.Certified;

                    searchMarketListings.DriveTrains = searchCriteria.Criteria.DriveTrian ?? new List<string>();
                    searchMarketListings.Engines = searchCriteria.Criteria.Engine ?? new List<string>();
                    searchMarketListings.FuelTypes = searchCriteria.Criteria.Fuel ?? new List<string>();
                    searchMarketListings.Transmissions = searchCriteria.Criteria.Transmission ?? new List<string>();
                    searchMarketListings.Trims = searchCriteria.Criteria.Trim ?? new List<string>();
                }
                else
                {
                    searchMarketListings.Distance = dealer.PingIIDefaultSearchRadius;
                    if (!String.IsNullOrWhiteSpace(inventoryDetails.Trim))
                    {
                        searchMarketListings.Trims = new List<string>
                        {
                            inventoryDetails.Trim
                        };
                    }
                }

                var searchMarketListingsList = new List<SearchMarketListings>
                {
                    searchMarketListings
                };

                // load MarketListingRequestDto with criteria
                var marketListingRequestDto = new MarketListingRequestDto()
                {
                    SearchMarketListings = searchMarketListingsList
                };

                //
                // Now get the listings.
                //
                var marketListingsModel = _marketListingsRepository.Fetch(new MarketListingsArgumentDto()
                {
                    ApiUser = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    ApiPass = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    Method = "POST",
                    Url = ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + ListingsApi,
                    RequestData = marketListingRequestDto
                });

                // convert model to marketListings
                marketListings = ModelToMarketListings(marketListingsModel);

            }
            catch (Exception ex)
            {
                _logger.Log(ex);
                throw;
            }

            // Return the market listings.
            return marketListings;
        }

        #region Utilities
        public ModelBuilder.BusinessUnit.Dealer GetDealer(int businessUnitId)
        {
            var businessUnit = new ModelBuilder.BusinessUnit.Dealer();

            var repository = Registry.Resolve<IRepository<BusinessUnitModel, BusinessUnitDto>>();

            var model = repository.Fetch(new BusinessUnitDto()
            {
                ApiPass = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                ApiUser = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                Method = "GET",
                Url = ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl + string.Format(BusinessUnitApiUrl, businessUnitId)
            });

            businessUnit.BookOut = model.DealerInfo.BookOut;
            businessUnit.DealerId = model.DealerInfo.DealerId;
            businessUnit.DistanceList = model.DealerInfo.DistanceList;
            businessUnit.EdmundsTrueMarketValue = model.DealerInfo.EdmundsTrueMarketValue;
            businessUnit.FavourableThreshold = model.DealerInfo.FavourableThreshold;
            businessUnit.GuideBook2Id = model.DealerInfo.GuideBook2Id;
            businessUnit.GuideBookID = model.DealerInfo.GuideBookID;
            businessUnit.IsNewPing = model.DealerInfo.IsNewPing;
            businessUnit.KBBRetailValue = model.DealerInfo.KBBRetailValue;
            businessUnit.Latitude = model.DealerInfo.Latitude;
            businessUnit.Longitude = model.DealerInfo.Longitude;
            businessUnit.MarketAvgInternetPrice = model.DealerInfo.MarketAvgInternetPrice;
            businessUnit.NADARetailValue = model.DealerInfo.NADARetailValue;
            businessUnit.OriginalMSRP = model.DealerInfo.OriginalMSRP;
            businessUnit.OwnerName = model.DealerInfo.OwnerName;
            businessUnit.PingIIDefaultSearchRadius = model.DealerInfo.PingIIDefaultSearchRadius;
            businessUnit.PingIIGreenRange1Value1 = model.DealerInfo.PingIIGreenRange1Value1;
            businessUnit.PingIIGreenRange1Value2 = model.DealerInfo.PingIIGreenRange1Value2;
            businessUnit.PingIIGuideBookId = model.DealerInfo.PingIIGuideBookId;
            businessUnit.PingIIMarketListing = model.DealerInfo.PingIIMarketListing;
            businessUnit.PingIIMaxSearchRadius = model.DealerInfo.PingIIMaxSearchRadius;
            businessUnit.PingIIRedRange1Value1 = model.DealerInfo.PingIIRedRange1Value1;
            businessUnit.PingIIRedRange1Value2 = model.DealerInfo.PingIIRedRange1Value2;
            businessUnit.PingIIYellowRange1Value1 = model.DealerInfo.PingIIYellowRange1Value1;
            businessUnit.PingIIYellowRange1Value2 = model.DealerInfo.PingIIYellowRange1Value2;
            businessUnit.PingIIYellowRange2Value1 = model.DealerInfo.PingIIYellowRange2Value1;
            businessUnit.PingIIYellowRange2Value2 = model.DealerInfo.PingIIYellowRange2Value2;
            businessUnit.UnitCostThreshold = model.DealerInfo.UnitCostThreshold;
            businessUnit.UnitCostThresholdLower = model.DealerInfo.UnitCostThresholdLower;
            businessUnit.UnitCostThresholdUpper = model.DealerInfo.UnitCostThresholdUpper;
            businessUnit.ZipCode = model.DealerInfo.ZipCode;

            return businessUnit;
        }

        public InventoryDetails GetInventoryDetails(int businessUnitId, int inventoryId)
        {
            var inventoryDetails = new InventoryDetails();

            var repository = Registry.Resolve<IRepository<InventoryHeaderModel, InventoryHeaderArgumentDto>>();

            var model = repository.Fetch(new InventoryHeaderArgumentDto()
            {
                ApiPass = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                ApiUser = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                Method = "GET",
                Url =
                    ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl +
                    string.Format(InventoryHeaderApi, businessUnitId, inventoryId),
            });

            //Set certification 
            CertifiedProgramInfoTO cpt = null;

            var certification = CpoPrograms(businessUnitId, model.InventoryInfo.Make);
            if(certification != null)
                cpt = certification.FirstOrDefault(x => x.RequiresCertifiedId);

            if (cpt != null)
            {
                inventoryDetails.CertificationName = "Certified";
                inventoryDetails.CertifiedProgramId = cpt.Id;
            }
            else if (model.InventoryInfo.Certified == 1)
            {
                inventoryDetails.CertificationName = "Certified";
            }
            else
            {
                inventoryDetails.CertificationName = "";
            }


            inventoryDetails.Certified = model.InventoryInfo.Certified;
            inventoryDetails.Mileage = model.InventoryInfo.Mileage;
            inventoryDetails.Make = model.InventoryInfo.Make;
            inventoryDetails.Model = model.InventoryInfo.Model;
            inventoryDetails.StockNumber = model.InventoryInfo.StockNumber;
            inventoryDetails.ChromeStyleId = model.InventoryInfo.ChromeStyleId;
            inventoryDetails.UnitCost = model.InventoryInfo.UnitCost;
            inventoryDetails.Year = model.InventoryInfo.Year;
            inventoryDetails.Trim = model.InventoryInfo.Trim;
            inventoryDetails.Vin = model.InventoryInfo.Vin;
            inventoryDetails.ListPrice = model.InventoryInfo.ListPrice;

            return inventoryDetails;
        }

        public SearchCriteriaModel GetSearchCriteria(int businessUnitId, int inventoryId)
        {
            SearchCriteriaModel searchCriteriaModel = null;

            try
            {
                var repository = Registry.Resolve<IRepository<SearchCriteriaModel, SearchCriteriaArgumentDto>>();

                searchCriteriaModel = repository.Fetch(new SearchCriteriaArgumentDto()
                {
                    ApiPass = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiPass,
                    ApiUser = ModelBuilder.MaxPricingTool.Helpers.FlServiceApiUser,
                    Method = "GET",
                    Url =
                        ModelBuilder.MaxPricingTool.Helpers.FlServiceBaseUrl +
                        string.Format(SeachCriteriaApiurl, businessUnitId, inventoryId)
                });
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
            }

            // Return the search criteria (if any)
            return searchCriteriaModel;
        }

        private IEnumerable<CertifiedProgramInfoTO> CpoPrograms(int businessUnitId, string make)
        {
            IEnumerable<CertifiedProgramInfoTO> cpoPrograms = null;

            var certifiedProgamCommand = new CertifiedProgamsForDealerMakeCommand(businessUnitId, make);
            AbstractCommand.DoRun(certifiedProgamCommand);
            cpoPrograms = certifiedProgamCommand.Programs;

            return cpoPrograms;
        }

        private List<IMarketListing> ModelToMarketListings(MarketListingsModel marketListingsModel)
        {
            if (marketListingsModel == null)
                throw new ArgumentNullException("marketListingsModel", @"The MarketListingsModel cannot be null");

            var marketListings = new List<IMarketListing>();

            if (marketListingsModel.MarketListingsResponse != null)
            {
                marketListings.AddRange(
                    from marketListingsResponse in marketListingsModel.MarketListingsResponse
                    from dealerMarketListing in marketListingsResponse.DealerMarketListings
                    select new MarketListing()
                    {
                        DealerName = dealerMarketListing.Seller,
                        VehicleName =
                            String.Format("{0} {1} {2} {3}", dealerMarketListing.Year, dealerMarketListing.Make,
                                dealerMarketListing.Model, dealerMarketListing.Trim),
                        Color = dealerMarketListing.Color,
                        Mileage = dealerMarketListing.Mileage,
                        IsCertified = dealerMarketListing.Certified,
                        ListPrice = (int?) dealerMarketListing.InternetPrice,
                        VehicleDescription =
                            String.Format("{0} {1} {2} {3}", dealerMarketListing.Year, dealerMarketListing.Make,
                                dealerMarketListing.Model, dealerMarketListing.Trim)
                    });

                marketListings.ForEach(marketListing =>
                {
                    var charLimit =
                        String.Compare(marketListing.DealerName, _dealerName, true, CultureInfo.InvariantCulture) == 0
                            ? 250
                            : 150;
                    marketListing.VehicleDescription =
                        new string(marketListing.VehicleDescription.Take(charLimit).ToArray());
                });
            }

            return marketListings;
        }
        #endregion
        #endregion
    }
}