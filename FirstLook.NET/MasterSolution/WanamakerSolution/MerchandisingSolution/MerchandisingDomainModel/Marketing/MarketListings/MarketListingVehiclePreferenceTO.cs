﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    public class MarketListingVehiclePreferenceTO
    {
        public int ID { get; set;}
        public String VehicleHandle { get; set; }
        public String OwnerHandle { get; set; }

        public bool HasMarketListingPreference { get; set; }
        public bool IsDisplayed { get; set; }
        public bool ShowDealerName { get; set; }
        public SortCode Sortcode { get; set; }
        public bool SortAscending { get; set; }

        public Int32 MileageHigh { get; set; }
        public Int32 MileageLow { get; set; }

        public Int32 PriceHigh { get; set; }
        public Int32 PriceLow { get; set; }

        public IEnumerable<int> DisplayListingsIDs { get; set; }
        public IEnumerable<int> RemovedListingsIDs { get; set;}
    }
}