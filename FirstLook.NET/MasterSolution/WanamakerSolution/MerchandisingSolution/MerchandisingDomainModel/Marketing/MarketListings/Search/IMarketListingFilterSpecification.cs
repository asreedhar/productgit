namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    public interface IMarketListingFilterSpecification
    {
        int MileageHigh { get; }
        int MileageLow { get; }
        int PriceHigh { get; }
        int PriceLow { get; }
        string DealerName { get; }
        string[] Colors { get; }
        string TrimLevel { get; }
    }
}
