using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    internal class MarketListingFilterSpecification : IMarketListingFilterSpecification
    {
        public MarketListingFilterSpecification()
        {
            MileageHigh = Int32.MaxValue;
            MileageLow = 0;
            PriceHigh = Int32.MaxValue;
            PriceLow = 0;
            DealerName = string.Empty;
            Colors = new string[] { };
            TrimLevel = string.Empty;
        }

        // ReSharper disable UnusedAutoPropertyAccessor.Local
        public int MileageHigh { get; set; }
        public int MileageLow { get; set; }
        public int PriceHigh { get; set; }
        public int PriceLow { get; set; }
        public string DealerName { get; set; }
        public string[] Colors { get; set; }
        public string TrimLevel { get; set; }
        // ReSharper restore UnusedAutoPropertyAccessor.Local
    }
}