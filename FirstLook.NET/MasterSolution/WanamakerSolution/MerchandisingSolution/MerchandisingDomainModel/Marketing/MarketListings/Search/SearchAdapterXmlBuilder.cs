using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Xml;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    public class SearchAdapterXmlBuilder
    {
        #region Sample Output
        /*
            <marketlistings>
	            <marketlisting>
		            <dealer>
			            <name>Shepard Chevrolet</name>
			            <logo></logo>
		            </dealer>
		            <vehicle>
			            <name>2006 Chevrolet Suburban SUV LS</name>
			            <color>Blue</color>
			            <mileage>27179</mileage>
			            <certified>False</certified>
			            <price>24999</price>
			            <savings>-1</savings>
			            <description>2006 CHEVROLET SUBURBAN LS 1500</description>
                        <vehicleId>1</vehicleId>
		            </vehicle>
	            </marketlisting>
	            <marketlisting>
		            <dealer>
			            <name>BILL KAY CHEVROLET</name>
			            <logo></logo>
		            </dealer>
		            <vehicle>
			            <name>2006 Chevrolet Suburban SUV Z71</name>
			            <color>White</color>
			            <mileage>30768</mileage>
			            <certified>False</certified>
			            <price>25147</price>
			            <savings>147</savings>
			            <description>2006 CHEVROLET SUBURBAN 4X4</description>
                        <vehicleId>2</vehicleId>
		            </vehicle>
	            </marketlisting>
	            <marketlisting>
		            <dealer>
			            <name>JERRY HAGGERTY CHEVROLET</name>
			            <logo></logo>
		            </dealer>
		            <vehicle>
			            <name>2006 Chevrolet Suburban SUV LTZ</name>
			            <color>White</color>
			            <mileage>39481</mileage>
			            <certified>True</certified>
			            <price>26886</price>
			            <savings>1886</savings>
			            <description>2006 CHEVROLET SUBURBAN 1500 LTZ</description>
                        <vehicleId>3</vehicleId>
		            </vehicle>
	            </marketlisting>
            </marketlistings>
*/
        #endregion Sample Output

        public static string AsXml(IEnumerable<IMarketListing> marketListings, int offerPrice)
        {
            return TagBuilder.Wrap("marketlistings",
                                   marketListings.Select(marketListing => BuildMarketListingElement(marketListing, offerPrice)).ToArray());
        }

        private static String BuildMarketListingElement(IMarketListing marketListing, Int32 offerPrice)
        {
            var dealerName = TagBuilder.Wrap("name", marketListing.DealerName);
            var logo = TagBuilder.Wrap("logo", String.Empty); //empty for now till we find what to bring.
            var vehicleName = TagBuilder.Wrap("name", marketListing.VehicleName);
            var color = TagBuilder.Wrap("color", marketListing.Color);
            var mileage = TagBuilder.Wrap("mileage", Convert.ToString(marketListing.Mileage));
            var certified = TagBuilder.Wrap("certified", Convert.ToString(marketListing.IsCertified).ToLower());
            var price = TagBuilder.Wrap("price", Convert.ToString(marketListing.ListPrice));
            var savings = TagBuilder.Wrap("savings", Convert.ToString((marketListing.ListPrice - offerPrice)));
            var description = TagBuilder.Wrap("description", marketListing.VehicleDescription);
            var vehicleId = TagBuilder.Wrap("vehicleId", Convert.ToString(marketListing.VehicleId));

            var dealer = TagBuilder.Wrap("dealer", dealerName, logo);
            var vehicle = TagBuilder.Wrap("vehicle", vehicleName, color, mileage, certified, price, savings,
                                             description, vehicleId);

            return TagBuilder.Wrap("marketlisting", dealer, vehicle);
        }
    }
}
