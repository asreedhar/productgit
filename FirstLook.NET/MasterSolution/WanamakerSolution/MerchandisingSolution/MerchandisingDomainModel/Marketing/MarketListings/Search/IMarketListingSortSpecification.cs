﻿namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    public interface IMarketListingSortSpecification
    {
        /// <summary>
        /// How should the search results be sorted?
        /// </summary>
        SortCode Sortcode { get; set; }

        /// <summary>
        /// Should the market listings be sorted in ascending order?
        /// </summary>
        bool SortAscending { get; set; }
    }

    /// <summary>
    /// Sort codes - exposed to clients to encapsulate sort behavior.
    /// </summary>
    public enum SortCode
    {
        ListPrice = 1,
        Color,
        DealerName,
        IsCertified,
        Mileage,
        VehicleDescription,
        VehicleName,
        VehicleId
    }
}
