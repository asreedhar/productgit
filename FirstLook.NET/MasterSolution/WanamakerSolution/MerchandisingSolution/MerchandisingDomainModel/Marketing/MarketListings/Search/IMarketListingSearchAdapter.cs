using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    // base interface for Market Listings search
    public interface IMarketListingSearchAdapter
    {
        IEnumerable<IMarketListing> Search(String vehicleHandle, String ownerHandle, string userName);
        IEnumerable<IMarketListing> Search(String vehicleHandle, String ownerHandle, IMarketListingFilterSpecification search, string userName);
        int GetVehicleID(String vehicleHandle);
    }
}