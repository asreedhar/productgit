using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    public class SearchAdapter : ISearchAdapter
    {
        private readonly IMarketListingVehiclePreferenceDataAccess _dataAccess;

        public SearchAdapter(IMarketListingVehiclePreferenceDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public int GetVehicleID( String vehicleHandle )
        {
            return _dataAccess.GetVehicleID( vehicleHandle );
        }

        public IEnumerable<IMarketListing> Search(String vehicleHandle, String ownerHandle, string userName)
        {
            // exclude vehicles with under 1k miles, since they are almost always new cars.
            var filterSpec = new MarketListingFilterSpecification {MileageLow = 1000};
            return Search(vehicleHandle, ownerHandle, filterSpec, userName);
        }

        public IEnumerable<IMarketListing> Search(String vehicleHandle, String ownerHandle, IMarketListingFilterSpecification search, string userName)
        {
            var allListings = _dataAccess.Search(ownerHandle, vehicleHandle, userName);
            return _dataAccess.Filter(allListings, search);
        }

    }

    internal class MarketListingFilterSpecification : IMarketListingFilterSpecification
    {
        public MarketListingFilterSpecification()
        {
            MileageHigh = Int32.MaxValue;
            MileageLow = 0;
            PriceHigh = Int32.MaxValue;
            PriceLow = 0;
            DealerName = string.Empty;
            Colors = new string[] { };
            TrimLevel = string.Empty;
        }

        // ReSharper disable UnusedAutoPropertyAccessor.Local
        public int MileageHigh { get; set; }
        public int MileageLow { get; set; }
        public int PriceHigh { get; set; }
        public int PriceLow { get; set; }
        public string DealerName { get; set; }
        public string[] Colors { get; set; }
        public string TrimLevel { get; set; }
        // ReSharper restore UnusedAutoPropertyAccessor.Local
    }



}
