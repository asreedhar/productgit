using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    public interface IMarketListing
    {
        String Color { get; set; }
        String DealerName { get; set; }
        Boolean? IsCertified { get; set; }
        Int32? ListPrice { get; set; }
        Int32? Mileage { get; set; }
        String VehicleDescription { get; set; }
        String VehicleName { get; set; }

        int VehicleId { get; set; }
    }

    public static class IMarketListingExtensions
    {
        public static IEnumerable<IMarketListing> OrderBySortCode(this IEnumerable<IMarketListing> marketListings, SortCode code, bool sortAscending)
        {
            IEnumerable<IMarketListing> sorted = new List<IMarketListing>();

            // I would love to do this another way, where the SortCode was passed to a function and I don't have to repeat the same sort operation.
            if (code == SortCode.Color) sorted = marketListings.OrderByDescending(l => l.Color);
            if (code == SortCode.DealerName) sorted = marketListings.OrderByDescending(l => l.DealerName);
            if (code == SortCode.IsCertified) sorted = marketListings.OrderByDescending(l => l.IsCertified);
            if (code == SortCode.ListPrice) sorted = marketListings.OrderByDescending(l => l.ListPrice);
            if (code == SortCode.Mileage) sorted = marketListings.OrderByDescending(l => l.Mileage);
            if (code == SortCode.VehicleDescription) sorted = marketListings.OrderByDescending(l => l.VehicleDescription);
            if (code == SortCode.VehicleId) sorted = marketListings.OrderByDescending(l => l.VehicleId);
            if (code == SortCode.VehicleName) sorted = marketListings.OrderByDescending(l => l.VehicleName);

            if (sortAscending)
                sorted = sorted.Reverse();

            return sorted;
        }
    }
}
