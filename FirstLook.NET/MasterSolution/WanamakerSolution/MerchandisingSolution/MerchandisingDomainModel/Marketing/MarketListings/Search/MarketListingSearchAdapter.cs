using System;
using System.Collections.Generic;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.DataAccess;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    public class MarketListingSearchAdapter : IMarketListingSearchAdapter
    {
        #region Properties
        private readonly IMarketListingVehiclePreferenceDataAccess _vehiclePreferenceDataAccess;
        private readonly IMarketListingsV1DataAccess _marketListingsV1DataAccess;
        private readonly IMarketListingsV2DataAccess _marketListingsV2DataAccess;
        #endregion

        #region Methods
        #region Construction

        internal MarketListingSearchAdapter(IMarketListingVehiclePreferenceDataAccess vehiclePreferenceDataAccess,
            IMarketListingsV1DataAccess marketListingsV1DataAccess,
            IMarketListingsV2DataAccess marketListingsV2DataAccess)
        {
            _vehiclePreferenceDataAccess = vehiclePreferenceDataAccess;
            _marketListingsV1DataAccess = marketListingsV1DataAccess;
            _marketListingsV2DataAccess = marketListingsV2DataAccess;
        }

        public MarketListingSearchAdapter() : this(Registry.Resolve<IMarketListingVehiclePreferenceDataAccess>(),
            Registry.Resolve<IMarketListingsV1DataAccess>(), Registry.Resolve<IMarketListingsV2DataAccess>())
        {
        }

        #endregion

        public int GetVehicleID(String vehicleHandle)
        {
            return _vehiclePreferenceDataAccess.GetVehicleID(vehicleHandle);
        }

        public IEnumerable<IMarketListing> Search(String vehicleHandle, String ownerHandle, string userName)
        {
            // exclude vehicles with under 1k miles, since they are almost always new cars.
            var filterSpec = new MarketListingFilterSpecification {MileageLow = 1000};
            return Search(vehicleHandle, ownerHandle, filterSpec, userName);
        }

        public IEnumerable<IMarketListing> Search(String vehicleHandle, String ownerHandle, IMarketListingFilterSpecification search, string userName)
        {
            // get the dealer's ping access (Ping II or ProfitMAX)
            var marketListingsDataAccess = GetDealerMarketListingsDataAccess(ownerHandle);

            var allListings = marketListingsDataAccess.Search(ownerHandle, vehicleHandle, userName);
            return marketListingsDataAccess.Filter(allListings, search);
        }

        // virtual method so that NUnit can replace the returned value for tests
        public virtual IMarketListingsDataAccess GetDealerMarketListingsDataAccess(String ownerHandle)
        {
            // Get the owner, so we can get the business unit, so we can get the business unit id.
            var owner = Owner.GetOwner(ownerHandle);

            // we currently only support dealers and inventory
            if (owner.OwnerEntityType != OwnerEntityType.Dealer)
                throw new NotSupportedException("The passed owner entity type is not supported.");

            // get the BusinessUnit from the owner
            var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);

            if (DealerPingAccess.CheckDealerPingAccess(businessUnit.Id))
                return _marketListingsV2DataAccess;

            return _marketListingsV1DataAccess;
        }
        #endregion
    }
}
