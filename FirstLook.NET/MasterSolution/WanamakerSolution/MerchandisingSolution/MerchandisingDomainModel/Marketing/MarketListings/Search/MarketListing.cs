using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search
{
    internal class MarketListing : IMarketListing
    {
        public MarketListing()
        {
            Color = String.Empty;
            DealerName = String.Empty;
            IsCertified = false;
            ListPrice = null;
            Mileage = null;
            VehicleDescription = String.Empty;
            VehicleName = String.Empty;
            Options = String.Empty;
            VehicleId = 0;
        }

        public string Color { get; set; }
        public string DealerName { get; set; }
        public bool? IsCertified { get; set; }
        public int? ListPrice { get; set; }
        public int? Mileage { get; set; }
        public string VehicleDescription { get; set; }
        public string Options { get; set; }
        public string VehicleName { get; set; }

        public int VehicleId { get; set; }
    }
}
