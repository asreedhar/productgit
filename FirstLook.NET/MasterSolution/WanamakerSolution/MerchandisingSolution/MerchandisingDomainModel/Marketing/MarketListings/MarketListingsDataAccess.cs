using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketListings
{
    public abstract class MarketListingsDataAccess : IMarketListingsDataAccess
    {
        public abstract IEnumerable<IMarketListing> Search(String ownerHandle, String vehicleHandle, String userName);

        public IEnumerable<IMarketListing> Filter(IEnumerable<IMarketListing> marketListings, IMarketListingFilterSpecification searchSpecification)
        {
            var priceHigh = searchSpecification.PriceHigh;
            var priceLow = searchSpecification.PriceLow;
            var mileageHigh = searchSpecification.MileageHigh;
            var mileageLow = searchSpecification.MileageLow;
            var dealerName = searchSpecification.DealerName;
            var trimLevel = searchSpecification.TrimLevel;
            var colors = searchSpecification.Colors;

            var filtered = marketListings
                .Where(m =>
                    (m.ListPrice <= priceHigh && m.ListPrice >= priceLow)
                    && (m.Mileage <= mileageHigh && m.Mileage >= mileageLow)
                    && (String.IsNullOrWhiteSpace(dealerName) || dealerName == m.DealerName)
                    && (String.IsNullOrWhiteSpace(trimLevel) || trimLevel == m.VehicleName)
                    && (colors == null || colors.Length == 0 || colors.Contains(m.Color)))
                .ToList();

            return filtered;
        }

        protected static String GetDealerName(String ownerHandle)
        {
            var ownerNameDataSource = new OwnerNameDataSource();
            var table = ownerNameDataSource.FindByOwnerHandle(ownerHandle);
            return (from DataRow row in table.Rows select row["OwnerName"]).FirstOrDefault() as String ?? String.Empty;
        }
    }
}