using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects
{
    [Serializable]
    public class DealerGeneralPreferenceTO
    {
        private const int NOT_ASSIGNED = 0;

        private string _addressLine1;
        private string _addressLine2;
        private string _city;
        private string _state;
        private string _zipCode;
        private string _url;
        private string _salesEmailAddress;
        private string _salesPhoneNumber;
        private string _extendedTagline;
        private int _daysValidFor;
        private string _disclaimer;
        private string _name;
        private readonly string _ownerHandle;
        private bool _displayContactInfoOnWebPdf;

        internal DealerGeneralPreferenceTO(string addressLine1, string addressLine2, string city, string state, string zipCode, string url, 
                                           string salesEmailAddress, string salesPhoneNumber, string extendedTagLine, int daysValidFor, 
                                           string disclaimer, string name, string ownerHandle, bool displayContactInfoOnWebPdf)
        {

             
            _addressLine1 = addressLine1;
            _addressLine2 = addressLine2;
            _city = city;
            _state = state;
            _zipCode = zipCode;
            _url = url;
            _salesEmailAddress = salesEmailAddress;
            _salesPhoneNumber = salesPhoneNumber;
            _extendedTagline = extendedTagLine;
            _daysValidFor = daysValidFor;
            _disclaimer = disclaimer;
            _name = name;
            _ownerHandle = ownerHandle;
            _displayContactInfoOnWebPdf = displayContactInfoOnWebPdf;
        }

        internal DealerGeneralPreferenceTO(string ownerHandle) : this( String.Empty, String.Empty, String.Empty, String.Empty,
            String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, NOT_ASSIGNED, String.Empty, String.Empty, ownerHandle, true)
        {
        }

        public string AddressLine1
        {
            get { return _addressLine1; }
            set { _addressLine1 = value; }
        }

        public string AddressLine2
        {
            get { return _addressLine2; }
            set { _addressLine2 = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public bool DisplayContactInfoOnWebPdf
        {
            get { return _displayContactInfoOnWebPdf; }
            set { _displayContactInfoOnWebPdf = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        public string Url
        { 
            get { return _url; }
            set { _url = value;}
        }

        public string SalesEmailAddress
        {
            get { return _salesEmailAddress; }
            set { _salesEmailAddress = value; }
        }

        public string SalesPhoneNumber
        {
            get { return _salesPhoneNumber; }
            set { _salesPhoneNumber = value; }
        }

        public string ExtendedTagline
        {
            get { return _extendedTagline; }
            set { _extendedTagline = value; }
        }

        public int DaysValidFor
        {
            get { return _daysValidFor; }
            set { _daysValidFor = value; }
        }

        public string Disclaimer
        {
            get { return _disclaimer; }
            set { _disclaimer = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }
    }
}
