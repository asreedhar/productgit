using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types
{
    public interface IDealerGeneralPreferenceDataAccess
    {
        /// <summary>
        /// Does a general dealer preference exist for the specified owner?
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <returns></returns>
        bool Exists(string ownerHandle);


        /// <summary>
        /// Creates the dealer's general preference.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <returns></returns>
        DealerGeneralPreferenceTO Create(string ownerHandle);

        /// <summary>
        /// Fetch the dealer's general preference.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <returns></returns>
        DealerGeneralPreferenceTO Fetch(string ownerHandle);

        /// <summary>
        /// Insert a new preference.
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        void Insert(DealerGeneralPreferenceTO preference, string userName);

        /// <summary>
        /// Update an existing preference
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        void Update(DealerGeneralPreferenceTO preference, string userName);  
    }
}