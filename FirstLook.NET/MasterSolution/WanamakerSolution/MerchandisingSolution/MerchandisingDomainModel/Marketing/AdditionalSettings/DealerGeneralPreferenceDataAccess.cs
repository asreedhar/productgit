using System;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects;
using IDataConnection = FirstLook.Common.Data.IDataConnection;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings
{
    internal class DealerGeneralPreferenceDataAccess : IDealerGeneralPreferenceDataAccess 
    {

        private static String GetStringOrEmpty(IDataRecord record, string columnName)
        {
            String value = String.Empty;
            int ordinal = record.GetOrdinal(columnName);
            if (!record.IsDBNull(ordinal))
                value = record.GetString(ordinal);

            return value;
        }

        public bool Exists(string ownerHandle)
        {            
            int count = 0;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.DealerGeneralPreference#Exists";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "ReturnValue", count, DbType.Int32, ParameterDirection.ReturnValue, false);

                    command.ExecuteNonQuery();

                    IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }
                }

            }         
        
            return (count > 0);
        }

        public DealerGeneralPreferenceTO Create(string ownerHandle)
        {
            DealerGeneralPreferenceTO preference = null;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.DealerGeneralPreference#Create";
                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    IDataReader rdr = command.ExecuteReader();

                    if (rdr.Read())
                    {
                        string name = GetStringOrEmpty(rdr, "Name");
                        string addressLine1 = GetStringOrEmpty(rdr, "Address1");
                        string addressLine2 = GetStringOrEmpty(rdr, "Address2");
                        string city = GetStringOrEmpty(rdr, "City");
                        string state = GetStringOrEmpty(rdr, "State");
                        string zip = GetStringOrEmpty(rdr, "ZipCode");
                        string phone = GetStringOrEmpty(rdr, "PhoneNumber");

                        const bool DISPLAY_CONTACT_INFO_ON_WEB_PDF = false;

                        preference = new DealerGeneralPreferenceTO(addressLine1, addressLine2,
                                                                   city, state, zip, String.Empty, String.Empty, phone,
                                                                   String.Empty, 0,
                                                                   String.Empty, name, ownerHandle, DISPLAY_CONTACT_INFO_ON_WEB_PDF);
                    }
                }
            }

            return preference;
        }

        public DealerGeneralPreferenceTO Fetch(string ownerHandle)
        {
           
            DealerGeneralPreferenceTO preference = null;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.DealerGeneralPreference#Fetch";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr != null)
                    {
                        if (rdr.Read())
                        {
                            string handle = rdr.GetString(rdr.GetOrdinal("OwnerHandle"));
                            string addressLine1 = rdr.GetString(rdr.GetOrdinal("AddressLine1"));
                            string addressLine2 = rdr.GetString(rdr.GetOrdinal("AddressLine2"));
                            string city = rdr.GetString(rdr.GetOrdinal("City"));
                            string state = rdr.GetString(rdr.GetOrdinal("StateCode"));
                            string zip = rdr.GetString(rdr.GetOrdinal("ZipCode"));
                            string url = rdr.GetString(rdr.GetOrdinal("Url"));
                            string email = rdr.GetString(rdr.GetOrdinal("SalesEmailAddress"));
                            string phone = rdr.GetString(rdr.GetOrdinal("SalesPhoneNumber"));
                            string tag = rdr.GetString(rdr.GetOrdinal("ExtendedTagLine"));
                            int daysValid = rdr.GetInt32(rdr.GetOrdinal("DaysValidFor"));
                            string disclaimer = DataRecord.GetString(rdr, "Disclaimer");
                            string name = rdr.GetString(rdr.GetOrdinal("Name"));

                            bool displayContactInfoOnWebPdf =
                                rdr.GetBoolean(rdr.GetOrdinal("DisplayContactInfoOnWebPDF"));

                            preference = new DealerGeneralPreferenceTO(addressLine1, addressLine2,
                                                                       city, state, zip, url, email, phone, tag, daysValid,
                                                                       disclaimer, name, handle, displayContactInfoOnWebPdf);

                        }
                    }
                    else
                    {
                        throw new ApplicationException("The datareader is null.");
                    }
                    if (!rdr.IsClosed) rdr.Close();
                }
            }
            return preference;
        }

        public void Insert( DealerGeneralPreferenceTO preference, string userName )
        {             
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.DealerGeneralPreference#Insert";

                    CommonMethods.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "AddressLine1", preference.AddressLine1, DbType.String);
                    CommonMethods.AddWithValue(command, "AddressLine2", preference.AddressLine2, DbType.String);
                    CommonMethods.AddWithValue(command, "City", preference.City, DbType.String);
                    CommonMethods.AddWithValue(command, "StateCode", preference.State, DbType.String);
                    CommonMethods.AddWithValue(command, "ZipCode", preference.ZipCode, DbType.String);
                    CommonMethods.AddWithValue(command, "Url", preference.Url, DbType.String);
                    CommonMethods.AddWithValue(command, "SalesEmailAddress", preference.SalesEmailAddress, DbType.String);
                    CommonMethods.AddWithValue(command, "SalesPhoneNumber", preference.SalesPhoneNumber, DbType.String);
                    CommonMethods.AddWithValue(command, "ExtendedTagLine", preference.ExtendedTagline, DbType.String);
                    CommonMethods.AddWithValue(command, "DaysValidFor", preference.DaysValidFor, DbType.Int32);
                    CommonMethods.AddWithValue(command, "Disclaimer", preference.Disclaimer, DbType.String);
                    CommonMethods.AddWithValue(command, "DisplayContactInfoOnWebPDF", preference.DisplayContactInfoOnWebPdf, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "InsertUser", userName, DbType.String);

                    try
                    {
                        command.ExecuteNonQuery();
                        txn.Commit();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }
            }
        }

        public void Update( DealerGeneralPreferenceTO preference, string userName )
        {
            
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.DealerGeneralPreference#Update";

                    CommonMethods.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "AddressLine1", preference.AddressLine1, DbType.String);
                    CommonMethods.AddWithValue(command, "AddressLine2", preference.AddressLine2, DbType.String);
                    CommonMethods.AddWithValue(command, "City", preference.City, DbType.String);
                    CommonMethods.AddWithValue(command, "StateCode", preference.State, DbType.String);
                    CommonMethods.AddWithValue(command, "ZipCode", preference.ZipCode, DbType.String);
                    CommonMethods.AddWithValue(command, "Url", preference.Url, DbType.String);
                    CommonMethods.AddWithValue(command, "SalesEmailAddress", preference.SalesEmailAddress, DbType.String);
                    CommonMethods.AddWithValue(command, "SalesPhoneNumber", preference.SalesPhoneNumber, DbType.String);
                    CommonMethods.AddWithValue(command, "ExtendedTagLine", preference.ExtendedTagline, DbType.String);
                    CommonMethods.AddWithValue(command, "DaysValidFor", preference.DaysValidFor, DbType.Int32);
                    CommonMethods.AddWithValue(command, "Disclaimer", preference.Disclaimer, DbType.String);
                    CommonMethods.AddWithValue(command, "DisplayContactInfoOnWebPDF", preference.DisplayContactInfoOnWebPdf, DbType.Boolean); 
                    CommonMethods.AddWithValue(command, "UpdateUser", userName, DbType.String);

                    try
                    {
                        command.ExecuteNonQuery();
                        txn.Commit();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}
