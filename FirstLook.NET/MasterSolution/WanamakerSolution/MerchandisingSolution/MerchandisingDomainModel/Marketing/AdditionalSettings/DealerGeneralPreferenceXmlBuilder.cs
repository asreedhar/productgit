using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Xml;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings
{
    /*
    <dealer ownerHandle="abc">
		<contact>
			<address>5901 South La Grange Road</address>
			<city>Countryside</city>
			<email>sales@valleyhonda.com</email>
			<phone>877-203-0685</phone>
			<state>IL</state>
			<zip>60525</zip>
		</contact>
		<description>Valley Honda - Aliquip, nibh saepius antehabeo luptatum enim quidem minim, molior turpis oppeto. Macto ingenium voco, feugait appellatio, turpis torqueo adipiscing, praesent persto iustum persto, feugiat.</description>
		<logo>/Public/Images/logo_ValleyHonda_Lrg.png</logo>
		<name>Valley Honda</name>
        <offerValidUntil>12/16/2009</offerValidUntil>
        <disclamer>abc</disclaimer>
        <url>http://pgatour.com</url>
        <displayContactInfoOnWebPdf>True</displayContactInfoOnWebPdf>
	</dealer>
    */
    internal class DealerGeneralPreferenceXmlBuilder  
    {
        public static string AsXml(DealerGeneralPreference dealerGeneralPreference)
        {

            string contact = Contact(dealerGeneralPreference);
            string description = TagBuilder.Wrap( "description", dealerGeneralPreference.ExtendedTagline);
            string name = TagBuilder.Wrap( "name", dealerGeneralPreference.Name);
            string valid = TagBuilder.Wrap("offerValidUntil", OfferValidUntil( dealerGeneralPreference ));
            string url = TagBuilder.Wrap("url", dealerGeneralPreference.Url);
            string disclaimer = TagBuilder.Wrap("disclaimer", dealerGeneralPreference.Disclaimer);
            string displayContactInfoOnWebPdf = TagBuilder.Wrap("displayContactInfoOnWebPdf",
                                                                dealerGeneralPreference.DisplayContactInfoOnWebPdf.
                                                                    ToString().ToLower());

            IDictionary<string,string> dealerAttributes = new Dictionary<string, string>();
            dealerAttributes.Add("ownerHandle", dealerGeneralPreference.OwnerHandle );

            string dealer = TagBuilder.Wrap("dealer", dealerAttributes, contact, description, name, valid, disclaimer, url, displayContactInfoOnWebPdf);
            return dealer;
        }   

        private static string OfferValidUntil(DealerGeneralPreference dealerGeneralPreference)
        {            
            return DateTime.Now.AddDays(dealerGeneralPreference.DaysValidFor).ToShortDateString();
        }

        private static string Contact(DealerGeneralPreference dealerGeneralPreference)
        {
            string address = TagBuilder.Wrap("address", CombineAddress(dealerGeneralPreference.AddressLine1, dealerGeneralPreference.AddressLine2));
            string city = TagBuilder.Wrap("city", dealerGeneralPreference.City);
            string email = TagBuilder.Wrap("email", dealerGeneralPreference.SalesEmailAddress);
            string phone = TagBuilder.Wrap("phone", dealerGeneralPreference.SalesPhoneNumber);
            string state = TagBuilder.Wrap("state", dealerGeneralPreference.StateCode);
            string zip = TagBuilder.Wrap("zip", dealerGeneralPreference.ZipCode);

            string contact = TagBuilder.Wrap("contact", address, city, email, phone, state, zip);
            return contact;
        }

        private static string CombineAddress(string addressLine1, string addressLine2)
        {
            string address = addressLine1 + " " + addressLine2;
            return address.Trim();
        }
    }
}
