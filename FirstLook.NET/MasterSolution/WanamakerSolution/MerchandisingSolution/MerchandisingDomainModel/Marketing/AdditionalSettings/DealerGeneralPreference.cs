using System;
using Csla;
using Csla.Validation;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings
{
    public class DealerGeneralPreference : InjectableBusinessBase<DealerGeneralPreference>, IRenderXml 
    {
        #region Private member variables

        
        private string _addressLine1;
        private string _addressLine2;
        private string _city;
        private string _stateCode;
        private string _zipCode;
        private string _url;
        private string _salesEmailAddress;
        private string _salesPhoneNumber;
        private string _extendedTagline;
        private int _daysValidFor;
        private string _disclaimer;
        private string _name;
        private bool _displayContactInfoOnWebPdf;

        private string _ownerHandle;

        #endregion

        #region Properties

        

        public string AddressLine1
        {
            get
            {
                CanReadProperty("AddressLine1", true);
                return _addressLine1;
            }
            set
            {
                CanWriteProperty("AddressLine1", true); 
                _addressLine1 = value;
                PropertyHasChanged("AddressLine1");
            }
        }

        public string AddressLine2
        {
            get
            {
                CanReadProperty("AddressLine2", true);
                return _addressLine2;
            }
            set
            {
                CanWriteProperty("AddressLine2", true); 
                _addressLine2 = value;
                PropertyHasChanged("AddressLine2");
            }
        }

        public string City
        {
            get
            {
                CanReadProperty("City", true);
                return _city;
            }
            set
            {
                CanWriteProperty("City", true); 
                _city = value;
                PropertyHasChanged("City");
            }
        }

        public bool DisplayContactInfoOnWebPdf
        {
            get
            {
                CanReadProperty("DisplayContactInfoOnWebPdf", true);
                return _displayContactInfoOnWebPdf;
            }
            set 
            { 
                CanWriteProperty("DisplayContactInfoOnWebPdf", true);
                _displayContactInfoOnWebPdf = value;
                PropertyHasChanged("DisplayContactInfoOnWebPdf");
            }
        }

        public string StateCode
        {
            get
            {
                CanReadProperty("StateCode", true);
                return _stateCode;
            }
            set
            {
                CanWriteProperty("StateCode", true); 
                _stateCode = value;
                PropertyHasChanged("StateCode");
            }
        }

        public string ZipCode
        {
            get
            {
                CanReadProperty("ZipCode", true);
                return _zipCode;
            }
            set
            {
                CanWriteProperty("ZipCode", true); 
                _zipCode = value;
                PropertyHasChanged("ZipCode");
            }
        }

        public string Url
        {
            get
            {
                CanReadProperty("Url", true);
                return _url;
            }
            set
            {
                CanWriteProperty("Url", true); 
                _url = value;
                PropertyHasChanged("Url");
            }

        }

        public string SalesEmailAddress
        {
            get
            {
                CanReadProperty("SalesEmailAddress", true);
                return _salesEmailAddress;
            }
            set
            {
                CanWriteProperty("SalesEmailAddress", true); 
                _salesEmailAddress = value;
                PropertyHasChanged("SalesEmailAddress");
            }
        }

        public string SalesPhoneNumber
        {
            get
            {
                CanReadProperty("SalesPhoneNumber", true);
                return _salesPhoneNumber;
            }
            set
            {
                CanWriteProperty("SalesPhoneNumber", true); 
                _salesPhoneNumber = value;
                PropertyHasChanged("SalesPhoneNumber");
            }
        }

        public string ExtendedTagline
        {
            get
            {
                CanReadProperty("ExtendedTagline", true);                
                return _extendedTagline;
            }
            set
            {
                CanWriteProperty("ExtendedTagline", true); 
                _extendedTagline = value;
                PropertyHasChanged("ExtendedTagline");
            }
        }

        public int DaysValidFor
        {
            get
            {
                CanReadProperty("DaysValidFor", true);
                return _daysValidFor;
            }
            set
            {
                CanWriteProperty("DaysValidFor", true); 
                _daysValidFor = value;
                PropertyHasChanged("DaysValidFor");
            }
        }

        public string Disclaimer
        {
            get
            {
                CanReadProperty("Disclaimer", true);
                return _disclaimer;
            }
            set
            {
                CanWriteProperty("Disclaimer", true); 
                _disclaimer = value;
                PropertyHasChanged("Disclaimer");
            }
        }


        public string Name
        {
            get
            {
                CanReadProperty("Name", true); 
                return _name;
            }
        }

        public string OwnerHandle
        {
            get
            {
                CanReadProperty("OwnerHandle", true);
                return _ownerHandle;
            }
        }


        #endregion

        #region Injected dependencies
        
        public IDealerGeneralPreferenceDataAccess DataAccess { get; set; }

        #endregion

        protected override object GetIdValue()
        {
            return _ownerHandle;
        }

        #region Factory methods

        /// <summary>
        /// Private default ctor. Force factory method usage.
        /// </summary>
        private DealerGeneralPreference() {}

        /// <summary>
        /// Create a new dealer general preference.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <returns></returns>
        public static DealerGeneralPreference CreateDealerGeneralPreference(string ownerHandle)
        {
            return DataPortal.Create<DealerGeneralPreference>(ownerHandle);
        }

        /// <summary>
        /// Get an existing dealer general preference.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <returns></returns>
        public static DealerGeneralPreference GetDealerGeneralPreference(string ownerHandle)
        {
            return DataPortal.Fetch<DealerGeneralPreference>(ownerHandle);
        }

        public static string GetDealerDisclaimer(string ownerHandle)
        {
            return DataPortal.Fetch<DealerGeneralPreference>(ownerHandle).Disclaimer;
        }

        public static DealerGeneralPreference GetOrCreateDealerGeneralPreference(string ownerHandle)
        {
            ExistsCommand command = new ExistsCommand(ownerHandle);
            DataPortal.Execute(command);
            return command.Exists ? GetDealerGeneralPreference(ownerHandle) : CreateDealerGeneralPreference(ownerHandle);
        }


        #endregion

        #region Data Access

        private void DataPortal_Fetch(string ownerHandle)
        {
            DealerGeneralPreferenceTO preference = DataAccess.Fetch( ownerHandle );

            // Initialize from the fetched transfer object.
            FromTransferObject( preference );
        }

        private void DataPortal_Create(string ownerHandle)
        {
            DealerGeneralPreferenceTO preference = DataAccess.Create(ownerHandle);

            FromTransferObject(preference);
            MarkNew();
        }

        protected override void DataPortal_Insert()
        {
            DataAccess.Insert( ToTransferObject(), ApplicationContext.User.Identity.Name);
        }

        protected override void DataPortal_Update()
        {
            DataAccess.Update( ToTransferObject(), ApplicationContext.User.Identity.Name);
        }
        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("ExtendedTagline", 2000));
            ValidationRules.AddRule(CommonRules.IntegerMinValue, new CommonRules.IntegerMinValueRuleArgs("DaysValidFor", 0));
            ValidationRules.AddRule(CommonRules.IntegerMaxValue, new CommonRules.IntegerMaxValueRuleArgs("DaysValidFor", 14));
            ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("Disclaimer", 500));
        }

        #endregion

        #region Map to/from CertifiedVehiclePreferenceTO transfer objects

        internal DealerGeneralPreferenceTO ToTransferObject()
        {
            return new DealerGeneralPreferenceTO(_addressLine1, _addressLine2, _city, _stateCode, _zipCode, _url,
                                                 _salesEmailAddress,
                                                 _salesPhoneNumber, _extendedTagline, _daysValidFor, _disclaimer, _name,
                                                 OwnerHandle, _displayContactInfoOnWebPdf);
        }

        internal void FromTransferObject(DealerGeneralPreferenceTO transferObject)
        {             
            _addressLine1 = transferObject.AddressLine1;
            _addressLine2 = transferObject.AddressLine2;
            _city = transferObject.City;
            _daysValidFor = transferObject.DaysValidFor;
            _disclaimer = transferObject.Disclaimer;
            _displayContactInfoOnWebPdf = transferObject.DisplayContactInfoOnWebPdf;
            _extendedTagline = transferObject.ExtendedTagline;
            _name = transferObject.Name;
            _ownerHandle = transferObject.OwnerHandle;
            _salesEmailAddress = transferObject.SalesEmailAddress;
            _salesPhoneNumber = transferObject.SalesPhoneNumber;
            _stateCode = transferObject.State;
            _zipCode = transferObject.ZipCode;
            _url = transferObject.Url;
        }

        #endregion


        #region Exists  

        public static bool Exists(string ownerHandle)
        {
            ExistsCommand command = new ExistsCommand(ownerHandle);
            DataPortal.Execute(command);
            return command.Exists;
        }

        [Serializable]
        private class ExistsCommand : InjectableCommandBase
        {
            private readonly string _ownerHandle;
            private bool _exists;

            #region Injected dependencies

            public IDealerGeneralPreferenceDataAccess DataAccess { get; set; }

            #endregion

            public ExistsCommand(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            public bool Exists
            {
                get { return _exists; }
            }

            protected override void DataPortal_Execute()
            {
                _exists = DataAccess.Exists(_ownerHandle);
            }
        }

        #endregion

        public string AsXml()
        {
            return DealerGeneralPreferenceXmlBuilder.AsXml(this);
        }
        
    }
}
