﻿using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer
{
    class ValueAnalyzerVehiclePreferenceDataAccess : IValueAnalyzerVehiclePreferenceDataAccess
    {
        public ValueAnalyzerVehiclePreferenceTO Get(string ownerHandle, string vehicleHandle)
        {
            ValueAnalyzerVehiclePreferenceTO preference = null;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ValueAnalyzerVehiclePreference#Fetch";

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);

                    IDataReader reader =  command.ExecuteReader();

                    if (reader.Read())  // we expect exactly one row back
                    {
                        bool displayVehicleDescription = reader.GetBoolean(reader.GetOrdinal("DisplayVehicleDescription"));
                        bool useLongMarketingListingLayout = reader.GetBoolean(reader.GetOrdinal("UseLongMarketingListingLayout"));
                        string shortUrl = reader[reader.GetOrdinal("ShortUrl")] != DBNull.Value
                                             ? reader.GetString(reader.GetOrdinal("ShortUrl"))
                                             : null;
                        string longUrl = reader[reader.GetOrdinal("LongUrl")] != DBNull.Value
                                             ? reader.GetString(reader.GetOrdinal("LongUrl"))
                                             : null;
                        preference = new ValueAnalyzerVehiclePreferenceTO(ownerHandle, vehicleHandle,
                                                                          displayVehicleDescription,
                                                                          useLongMarketingListingLayout, shortUrl,
                                                                          longUrl);
                    }
                }
            }

            if (preference == null) throw new ApplicationException("A preference or a suitable default was not found.");

            return preference;
        }

        public void Save(ValueAnalyzerVehiclePreferenceTO preference, string userName)
        {
            // Update the preference.
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.ValueAnalyzerVehiclePreference#Set";

                    SimpleQuery.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", preference.VehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "User", userName, DbType.String);
                    SimpleQuery.AddWithValue(command, "DisplayVehicleDescription", preference.DisplayVehicleDescription, DbType.Boolean);
                    SimpleQuery.AddWithValue(command, "UseLongMarketingListingLayout", preference.UseLongMarketingListingLayout, DbType.Boolean);
                    
                    if (string.IsNullOrEmpty(preference.ShortUrl))
                    {
                        SimpleQuery.AddNullValue(command, "ShortUrl", DbType.String);
                    }else
                    {
                        SimpleQuery.AddWithValue(command, "ShortUrl", preference.ShortUrl, DbType.String);
                    }

                    if (string.IsNullOrEmpty(preference.LongUrl))
                    {
                        SimpleQuery.AddNullValue(command, "LongUrl", DbType.String);
                    }
                    else
                    {
                        SimpleQuery.AddWithValue(command, "LongUrl", preference.LongUrl, DbType.String);
                    }


                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

                // Commit
                txn.Commit();
            }            

        }
    }
}
