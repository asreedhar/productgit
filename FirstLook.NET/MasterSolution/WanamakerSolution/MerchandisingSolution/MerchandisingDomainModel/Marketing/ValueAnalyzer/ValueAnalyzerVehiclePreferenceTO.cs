﻿
namespace FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer
{
    /// <summary>
    /// A vehicle-specific preference for WebPDF - formerly called "value analyzer" document.
    /// </summary>
    public class ValueAnalyzerVehiclePreferenceTO
    {
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;

        internal ValueAnalyzerVehiclePreferenceTO(string ownerHandle, string vehicleHandle, bool displayVehicleDescription, bool useLongMarketingListingLayout)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            DisplayVehicleDescription = displayVehicleDescription;
            UseLongMarketingListingLayout = useLongMarketingListingLayout;
        }

        internal ValueAnalyzerVehiclePreferenceTO(string ownerHandle, string vehicleHandle, bool displayVehicleDescription, bool useLongMarketingListingLayout, string shortUrl, string longUrl)
            : this(ownerHandle, vehicleHandle, displayVehicleDescription, useLongMarketingListingLayout)
        {
            ShortUrl = shortUrl;
            LongUrl = longUrl;
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public bool DisplayVehicleDescription { get; set; }

        public bool UseLongMarketingListingLayout { get; set; }
        
        public string ShortUrl { get; set; }
        
        public string LongUrl { get; set; }
    }
}
