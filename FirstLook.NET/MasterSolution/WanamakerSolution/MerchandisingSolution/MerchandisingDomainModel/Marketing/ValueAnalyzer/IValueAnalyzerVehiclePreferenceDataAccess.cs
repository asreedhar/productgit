
namespace FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer
{
    public interface IValueAnalyzerVehiclePreferenceDataAccess
    {
        ValueAnalyzerVehiclePreferenceTO Get(string ownerHandle, string vehicleHandle);
        void Save(ValueAnalyzerVehiclePreferenceTO preference, string user);
    }
}
