 namespace FirstLook.Merchandising.DomainModel.Marketing
{
    /// <summary>
    /// Common Cache/Request keys.
    /// </summary>
    public static class CommonKeys
    {
        public const string DELIM = ";";
        public const string OWNER_HANDLE = "oh";
        public const string VEHICLE_HANDLE = "vh";
        public const string SEARCH_HANDLE = "sh";
    }
}
