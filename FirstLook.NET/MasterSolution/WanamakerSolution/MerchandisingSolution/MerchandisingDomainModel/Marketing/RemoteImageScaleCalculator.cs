using System;
using System.IO;
using System.Net;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;

namespace FirstLook.Merchandising.DomainModel.Marketing
{
    /// <summary>
    /// Consider making this a command.
    /// </summary>
    internal class RemoteImageScaleCalculator
    {

        internal void RescaleImage(string url, int maxHeight, int maxWidth, out int height, out int width, out string absoluteUri)
        {
            // If the url is empty, or if it doesn't refer to a file, we cannot proceed.
            if (url.Trim().Length == 0 || url.EndsWith("/"))
            {
                height = 0;
                width = 0;
                absoluteUri = null;
                return;
            }            
            
            // Note:  I would prefer to cache the dimensions, however this can lead to problems as the image
            // may not be accessible over time.  We must actually load the image to confirm whether
            // it is accessible.  Having done this, little is saved by caching dimensions.  Caching was
            // initally implemented, but has been removed.  Look thru the file history for this code.
            // ZB: 10 Jan 2010

            // Get the image.
            WebRequest request = WebRequest.Create(url);
            request.Timeout = 5000; // 5s
            WebResponse response;
            System.Drawing.Image image;

            

            try
            {
                response = request.GetResponse();
                absoluteUri = response.ResponseUri.AbsoluteUri;  // save the URL in case it is redirected
                Stream s = response.GetResponseStream();
                image = System.Drawing.Image.FromStream(s);
            }
            catch (Exception e)            
            {                
                throw new ApplicationException("Could not load the image at url: " + url, e);
            }

            // Use the scaler to do the math.
            ImageScaler scaler = new ImageScaler();
            scaler.Scale( maxHeight, maxWidth, image.Height, image.Width, out height, out width );
        }

    }
}
