using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Vehicle
{
    [Serializable]
    public class VehicleSummaryTO : IVehicleSummary
    {
        private int _mileage;
        private string _genericExteriorColor;
        private string _exteriorColor;
        private string _engine;
        private string _transmission;
        private string _driveTrain;
        private string _stockNumber;
        private string _vin;
        private string _description;
        private string _advertisement;

        private string _ownerHandle;
        private string _vehicleHandle;
        private string _inventoryType;

        public VehicleSummaryTO(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
        }

        public VehicleSummaryTO(string ownerHandle, string vehicleHandle, int mileage, string color, string genericExteriorColor,
            string engine, string transmission, string driveTrain, string stockNumber, string vin, string description)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _mileage = mileage;
            _exteriorColor = color;
            _genericExteriorColor = genericExteriorColor;
            _engine = engine;
            _transmission = transmission;
            _driveTrain = driveTrain;
            _stockNumber = stockNumber;
            _vin = vin;
            _description = description;
        }

        public VehicleSummaryTO(string ownerHandle, string vehicleHandle, int mileage, string color, string genericExteriorColor,
            string engine, string transmission, string driveTrain, string stockNumber, string vin, string description, string inventoryType)
            : this(ownerHandle, vehicleHandle, mileage, color, genericExteriorColor, engine, transmission, driveTrain, stockNumber, vin, description)
        {
            _inventoryType = inventoryType;
        }

        public int Year { get; internal set; }
        public string Make { get; internal set; }
        public string Line { get; internal set; }
        public string Series { get; internal set; }
        public string InteriorColor { get; internal set; }

        public int Mileage
        {
            get { return _mileage; }
            internal set { _mileage = value; }
        }

        public string ExteriorColor
        {
            get { return _exteriorColor; }
            internal set { _exteriorColor = value; }
        }

        public string GenericExteriorColor
        {
            get { return _genericExteriorColor; }
            internal set { _genericExteriorColor = value; }
        }

        public string Engine
        {
            get { return _engine; }
            internal set { _engine = value; }
        }

        public string Transmission
        {
            get { return _transmission; }
            internal set { _transmission = value; }
        }

        public string DriveTrain
        {
            get { return _driveTrain; }
            internal set { _driveTrain = value; }
        }

        public string StockNumber
        {
            get { return _stockNumber; }
            internal set { _stockNumber = value; }
        }

        public string VIN
        {
            get { return _vin; }
            internal set { _vin = value; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
            internal set { _vehicleHandle = value; }
        }
       
        public string OwnerHandle
        {
            get { return _ownerHandle; }
            internal set { _ownerHandle = value; }
        }

        public string Description
        {
            get { return _description; }
            internal set { _description = value; }
        }

        public string Advertisement
        {
            get { return _advertisement; }
            internal set { _advertisement = value; }
        }

        public string InventoryType
        {
            get { return _inventoryType; }
            internal set { _inventoryType = value; }
        }
    }
}
