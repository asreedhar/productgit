namespace FirstLook.Merchandising.DomainModel.Marketing.Vehicle
{
    public interface IVehicleSummary
    {
        int Mileage { get; }
        string GenericExteriorColor { get; }
        string ExteriorColor { get; }
        string Engine { get; }
        string Transmission { get; }
        string DriveTrain { get; }
        string StockNumber { get; }
        string VIN { get; }
        string VehicleHandle { get; }
        string OwnerHandle { get; }
        string Description { get;}
        string Advertisement { get; }
        int Year { get; }
        string Make { get; }
        string Line { get; }
        string Series { get; }
        string InteriorColor { get; }
        string InventoryType { get; }
    }
}