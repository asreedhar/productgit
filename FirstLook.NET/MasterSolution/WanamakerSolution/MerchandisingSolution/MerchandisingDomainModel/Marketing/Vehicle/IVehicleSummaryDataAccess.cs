namespace FirstLook.Merchandising.DomainModel.Marketing.Vehicle
{
    public interface IVehicleSummaryDataAccess
    {
        /// <summary>
        /// Fetch the vehicle summary.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        VehicleSummaryTO Fetch(string ownerHandle, string vehicleHandle);
    }
}