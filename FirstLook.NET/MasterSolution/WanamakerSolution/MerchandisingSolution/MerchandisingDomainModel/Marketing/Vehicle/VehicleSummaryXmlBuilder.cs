using System.Globalization;
using FirstLook.Common.Core.Xml;

namespace FirstLook.Merchandising.DomainModel.Marketing.Vehicle
{
    internal class VehicleSummaryXmlBuilder
    {
        /*
        <vehiclesummary>
		    <color>Silver</color>
		    <drivetrain>2WD</drivetrain>
		    <engine>V6 3.5 Liter VTEC</engine>
		    <mileage>32,848</mileage>
		    <stocknumber>ACG123</stocknumber>
		    <transmission>5-speed Auto</transmission>
		    <vin>1FTPW14V77FA04367</vin>
		    <yearmodel>2008 Honda Odyssey EX-L</yearmodel>
            <advertisement>This is a great car.</advertisement>
	    </vehiclesummary>
        */
        public static string AsXml(IVehicleSummary summary)
        {
            string color = TagBuilder.Wrap("color", summary.ExteriorColor);
            string interiorColor = TagBuilder.Wrap("interiorcolor", summary.InteriorColor);
            string genericExteriorColor = (summary.GenericExteriorColor != null ? TagBuilder.Wrap("genericexteriorcolor", summary.GenericExteriorColor) : "");
            string drive = TagBuilder.Wrap("drivetrain", summary.DriveTrain);
            string engine = TagBuilder.Wrap("engine", summary.Engine);
            string mileage = TagBuilder.Wrap("mileage", summary.Mileage.ToString(CultureInfo.InvariantCulture));
            string stockNumber = TagBuilder.Wrap("stocknumber", summary.StockNumber);
            string trans = TagBuilder.Wrap("transmission", summary.Transmission);
            string vin = TagBuilder.Wrap("vin", summary.VIN);
            string year = TagBuilder.Wrap("year", summary.Year.ToString(CultureInfo.InvariantCulture));
            string make = TagBuilder.Wrap("make", summary.Make);
            string model = TagBuilder.Wrap("model", summary.Line);
            string trim = TagBuilder.Wrap("trim", summary.Series);

            string yearModel = TagBuilder.Wrap("yearmodel", summary.Description);
            string advertisement = TagBuilder.Wrap("advertisement", summary.Advertisement);
            string inventoryType = TagBuilder.Wrap("inventorytype", summary.InventoryType.ToLower(CultureInfo.InvariantCulture));

            // The sequence of items is extremely important and must match the XSD. If it is out of order, the validation will throw an error! - TSH
            string xml = TagBuilder.Wrap("vehiclesummary", color, interiorColor, genericExteriorColor, drive, engine, mileage, stockNumber, trans, vin,
                                         year, make, model, trim, yearModel, advertisement, inventoryType);

            return xml;
        }   

    }
}
