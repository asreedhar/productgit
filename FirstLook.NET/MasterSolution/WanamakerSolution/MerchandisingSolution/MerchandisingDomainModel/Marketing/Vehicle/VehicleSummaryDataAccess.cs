using System;
using System.Data;
using System.Data.SqlClient;
using Csla;
using Csla.Data;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using VehicleType = MAX.Entities.VehicleType;

namespace FirstLook.Merchandising.DomainModel.Marketing.Vehicle
{
    internal class VehicleSummaryDataAccess : IVehicleSummaryDataAccess
    {
        public VehicleSummaryTO Fetch(string ownerHandle, string vehicleHandle)
        {
            // Create the new dto and build it up.
            VehicleSummaryTO dto = new VehicleSummaryTO(ownerHandle, vehicleHandle);

            BuildUpFromDomain( dto );
            BuildUpFromDatabase( dto );
            BuildUpFromMerchandisingDatabase(dto);

            return dto;           
        }

        /// <summary>
        /// Basically want to override any values for engine, trans, drivetrain, milage or exterior color with value from merchandising
        /// </summary>
        /// <param name="dto"></param>
        private static void BuildUpFromMerchandisingDatabase(VehicleSummaryTO dto)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.Merchandising))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                // Get the vehicle catalog id.
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "builder.VehicleMerchandisingSummary#Fetch";

                    CommonMethods.AddWithValue(command, "OwnerHandle", dto.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", dto.VehicleHandle, DbType.String);

                    using (IDataReader summaryReader = command.ExecuteReader())
                    {
                        if (summaryReader != null)
                        {
                            if (summaryReader.Read())
                            {
                                if (summaryReader["Transmission"] != Convert.DBNull && !string.IsNullOrWhiteSpace(summaryReader["Transmission"].ToString()))
                                    dto.Transmission = summaryReader["Transmission"].ToString();

                                if (summaryReader["Engine"] != Convert.DBNull && !string.IsNullOrWhiteSpace(summaryReader["Engine"].ToString()))
                                    dto.Engine = summaryReader["Engine"].ToString();

                                if (summaryReader["DriveTrain"] != Convert.DBNull && !string.IsNullOrWhiteSpace(summaryReader["DriveTrain"].ToString()))
                                    dto.DriveTrain = summaryReader["DriveTrain"].ToString();
                                
                                if (summaryReader["BaseColor"] != Convert.DBNull && !string.IsNullOrWhiteSpace(summaryReader["BaseColor"].ToString()))
                                    dto.ExteriorColor = summaryReader["BaseColor"].ToString();

                                if (summaryReader["GenericExtColor"] != Convert.DBNull && !string.IsNullOrWhiteSpace(summaryReader["GenericExtColor"].ToString()))
                                    dto.GenericExteriorColor = summaryReader["GenericExtColor"].ToString();

                                if (summaryReader["IntColor"] != Convert.DBNull && !string.IsNullOrWhiteSpace(summaryReader["IntColor"].ToString()))
                                    dto.InteriorColor = summaryReader["IntColor"].ToString();
                                
                                if (summaryReader["MileageReceived"] != Convert.DBNull && Convert.ToInt32(summaryReader["MileageReceived"] ) != 0  )
                                    dto.Mileage = Convert.ToInt32(summaryReader["MileageReceived"]);

                                if (summaryReader["InventoryType"] != Convert.DBNull)
                                    dto.InventoryType = (VehicleType) Convert.ToInt32(summaryReader["InventoryType"]);

                            }
                            else
                            {
                                throw new ApplicationException("The vehicle catalog record could not be identified.");
                            }
                        }
                        else
                        {
                            throw new ApplicationException("The datareader is null.");
                        }
                        if (!summaryReader.IsClosed) summaryReader.Close();
                    }

                }

            }
            
        }

        /// <summary>
        /// Build up the transfer object with data readily accessible from the domain.
        /// </summary>
        /// <param name="dto"></param>
        private static void BuildUpFromDatabase(VehicleSummaryTO dto)
        {  
            string engine, driveTrain, transmission;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                int vehicleCatalogId;

                // Get the vehicle catalog id.
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.GetVehicleIdentifiers";

                    CommonMethods.AddWithValue(command, "OwnerHandle", dto.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", dto.VehicleHandle, DbType.String);
                    
                    using(IDataReader summaryReader = command.ExecuteReader() )
                    {
                        if (summaryReader != null)
                        {
                            if (summaryReader.Read())
                            {
                                vehicleCatalogId = summaryReader.GetInt32(summaryReader.GetOrdinal("VehicleCatalogID"));
                            }
                            else
                            {
                                throw new ApplicationException("The vehicle catalog record could not be identified.");
                            }
                        }
                        else
                        {
                            throw new ApplicationException("The datareader is null.");
                        }
                        if (!summaryReader.IsClosed) summaryReader.Close();
                    }
                   
                }

                // Get the engine, drive train, transmission data from vc.
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.GetVehicleSummary";

                    CommonMethods.AddWithValue(command, "VehicleCatalogID", vehicleCatalogId, DbType.Int32);

                    using (IDataReader summaryReader = command.ExecuteReader())
                    {
                        if (summaryReader != null)
                        {
                            SafeDataReader sdr = new SafeDataReader(summaryReader);

                            if (sdr.Read())
                            {
                                engine = sdr.GetString(summaryReader.GetOrdinal("Engine"));
                                driveTrain = sdr.GetString(summaryReader.GetOrdinal("DriveTypeCode"));
                                transmission = sdr.GetString(summaryReader.GetOrdinal("Transmission"));
                            }
                            else
                            {
                                throw new ApplicationException("The vehicle summary could not be found.");
                            }
                        }
                        else
                        {
                            throw new ApplicationException("The datareader is null.");
                        }
                        
                        if (!summaryReader.IsClosed) summaryReader.Close();
                    }

                }

            }

            // build up the dto.
            dto.Engine = engine;
            dto.DriveTrain = driveTrain;
            dto.Transmission = transmission;
        }

        /// <summary>
        /// Build up the transfer object with data from the database.
        /// </summary>        
        private static void BuildUpFromDomain(VehicleSummaryTO dto)
        {
            const int NOT_FOUND = 50200;

            string color = string.Empty;
            string vin = string.Empty;
            string stockNumber = string.Empty;
            int? mileage = null;
            string description = string.Empty;
            bool found = false;

            string interiorColor = String.Empty;
            int year = 0;
            string make = string.Empty;
            string line = string.Empty;
            string series = string.Empty;

            // Use existing pricing domain to determine the make.
            try
            {
                Inventory inventory = Inventory.GetInventory(dto.OwnerHandle, dto.VehicleHandle);
                color = inventory.ExteriorColor.Name;
                vin = inventory.Vin;
                mileage = inventory.Mileage;
                stockNumber = inventory.StockNumber;
                description = inventory.Description;
                year = inventory.Year;
                make = inventory.Make;
                line = inventory.Line;
                series = inventory.Series;
                interiorColor = inventory.InteriorColor.Name;
                found = true;             
            }
            catch (DataPortalException ex)
            {
                // Try to cast the BusinessException to a SqlException.
                SqlException sqlException = ex.BusinessException as SqlException;

                // Suppress not found exceptions.
                if (sqlException == null || sqlException.Number != NOT_FOUND)
                    throw;
            }

            // Look for market listings if we couldn't find it in inventory.
            if( !found )
            {
                try
                {
                    InternetListing listing = InternetListing.GetInternetListing(dto.OwnerHandle, dto.VehicleHandle);
                    color = listing.ExteriorColor.Name;
                    vin = listing.Vin;
                    mileage = listing.Mileage;
                    stockNumber = listing.StockNumber;
                    description = listing.Description;
                    found = true;
                }
                catch (DataPortalException ex)
                {
                    // Try to cast the BusinessException to a SqlException.
                    SqlException sqlException = ex.BusinessException as SqlException;

                    // Suppress not found exceptions.
                    if (sqlException == null || sqlException.Number != NOT_FOUND)
                        throw;
                }
            }


            if ( !found )
            {
                throw new ApplicationException("The specified vehicle is neither in inventory nor in market listings.");    
            }
            
            // Build up the transfer object.
            dto.ExteriorColor = color;
            dto.VIN = vin;

            if( mileage != null && mileage.HasValue )
            {
                dto.Mileage = mileage.Value;                
            }

            dto.StockNumber = stockNumber;
            dto.Description = description;
            dto.Year = year;
            dto.Make = make;
            dto.Line = line;
            dto.Series = series;
            dto.InteriorColor = interiorColor;

            // The extended description is obtained from the "Advertisement" class.
            string advertisementBody = string.Empty;
            try
            {
                Advertisement advertisement = new Advertisement.DataSource().Select(dto.OwnerHandle, dto.VehicleHandle);
                advertisementBody = advertisement.Body;

            }
            catch (Exception)
            {
                // If an advertisement could not be found, we don't really care.                
            }

            dto.Advertisement = advertisementBody;
        }


    }
}
