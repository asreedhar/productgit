using Csla;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.AdPreview;

namespace FirstLook.Merchandising.DomainModel.Marketing.Vehicle
{
    public class VehicleSummary : InjectableReadOnlyBase<VehicleSummary>, IRenderXml, IVehicleSummary
    {
        #region Private member variables

        private int _mileage;
        private string _color;
        private string _genericExteriorColor;
        private string _engine;
        private string _transmission;
        private string _driveTrain;
        private string _stockNumber;
        private string _vin;
        private string _description;
        private string _advertisement;

        private string _ownerHandle;
        private string _vehicleHandle;

        private int _year;
        private string _make;
        private string _line;
        private string _series;
        private string _interiorColor;
        private string _inventoryType;
        #endregion

        #region Properties

        public int Mileage
        {
            get
            {
                CanReadProperty("Mileage", true); 
                return _mileage;
            }
        }

        public string GenericExteriorColor
        {
            get
            {
                CanReadProperty("GenericExteriorColor", true);
                return _genericExteriorColor;
            }
        }


        public string ExteriorColor
        {
            get
            {
                CanReadProperty("ExteriorColor", true);
                return _color;
            }
        }

        public string InteriorColor
        {
            get
            {
                CanReadProperty("InteriorColor", true);
                return _interiorColor;
            }

        }

        public string Engine
        {
            get 
            { 
                CanReadProperty("Engine", true);
                return _engine; 
            }
        }

        public string Transmission
        {
            get
            {
                CanReadProperty("Transmission", true);
                return _transmission;
            }
        }

        public string DriveTrain
        {
            get
            {
                CanReadProperty("DriveTrain", true);
                return _driveTrain;
            }
        }

        public string StockNumber
        {
            get
            {
                CanReadProperty("StockNumber", true);
                return _stockNumber;
            }
        }

        public string VIN
        {
            get
            {
                CanReadProperty("VIN", true);
                return _vin;
            }
        }

        public string VehicleHandle
        {
            get
            {
                CanReadProperty("VehicleHandle", true);
                return _vehicleHandle;
            }
        }
       
        public string OwnerHandle
        {
            get
            {
                CanReadProperty("OwnerHandle", true);
                return _ownerHandle;
            }
        }

        public string Description
        {
            get
            {
                CanReadProperty("Description", true);
                return _description;
            }
        }

        public string Advertisement
        {
            get
            {
                CanReadProperty("Advertisement", true);
                return _advertisement;
            }
        }

        public int Year
        {
            get
            {
                CanReadProperty("Year", true);
                return _year;
            }
            
        }

        public string Make
        {
            get
            {
                CanReadProperty("Make", true);
                return _make;
            }
        }

        public string Line
        {
            get
            {
                CanReadProperty("Line", true);
                return _line;
            }
        }

        public string Series
        {
            get
            {
                CanReadProperty("Series", true);
                return _series;
            }
        }

        public string InventoryType
        {
            get
            {
                CanReadProperty("InventoryType", true);
                return _inventoryType;
            }

        }

        #endregion

        protected override object GetIdValue()
        {
            return _ownerHandle + _vehicleHandle;
        }

        #region Injected dependencies

        public IVehicleSummaryDataAccess DataAccess { get; set; }

        #endregion

        #region Factory methods

        /// <summary>
        /// Private default ctor. Force factory method usage.
        /// </summary>
        private VehicleSummary() { }

        private VehicleSummary(string ownerHandle, string vehicleHandle, int mileage, string color, string genericExteriorColor,
                               string engine, string transmission, string driveTrain, string stockNumber, string vin, string description, string advertisement)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _mileage = mileage;
            _color = color;
            _genericExteriorColor = genericExteriorColor;
            _engine = engine;
            _transmission = transmission;
            _driveTrain = driveTrain;
            _stockNumber = stockNumber;
            _vin = vin;
            _description = description;
            _advertisement = advertisement;
        }

        private VehicleSummary(string ownerHandle, string vehicleHandle, int mileage, string color, string genericExteriorColor,
            string engine, string transmission, string driveTrain, string stockNumber, string vin, string description,
            string advertisement, string inventoryType)
            : this(
                ownerHandle, vehicleHandle, mileage, color, genericExteriorColor, engine, transmission, driveTrain, stockNumber, vin,
                description, advertisement)
        {
            _inventoryType = inventoryType;
        }

        /// <summary>
        /// Get an existing dealer general preference.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        public static VehicleSummary GetVehicleSummary(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<VehicleSummary>(new VehicleSummaryTO(ownerHandle, vehicleHandle));
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(IVehicleSummary dto)
        {
            VehicleSummaryTO summary = DataAccess.Fetch(dto.OwnerHandle, dto.VehicleHandle);

            // Initialize from the fetched transfer object.
            FromTransferObject(summary);
        }

        #endregion

        #region Map to/from CertifiedVehiclePreferenceTO transfer objects

        internal void FromTransferObject(VehicleSummaryTO transferObject)
        {
            _genericExteriorColor = transferObject.GenericExteriorColor;
            _ownerHandle = transferObject.OwnerHandle;
            _vehicleHandle = transferObject.VehicleHandle;
            _mileage = transferObject.Mileage;
            _color = transferObject.ExteriorColor;
            _engine = transferObject.Engine;
            _transmission = transferObject.Transmission;
            _driveTrain = transferObject.DriveTrain;
            _stockNumber = transferObject.StockNumber;
            _vin = transferObject.VIN;
            _description = transferObject.Description;
            _advertisement = transferObject.Advertisement;
            _year = transferObject.Year;
            _make = transferObject.Make;
            _line = transferObject.Line;
            _series = transferObject.Series;
            _interiorColor = transferObject.InteriorColor;
            _inventoryType = transferObject.InventoryType;
        }

        #endregion

        public string AsXml()
        {
            return VehicleSummaryXmlBuilder.AsXml(new TruncatedAdWrapper(this));
        }


        private class TruncatedAdWrapper : IVehicleSummary
        {
            private VehicleSummary _summary;

            public TruncatedAdWrapper(VehicleSummary summary)
            {
                _summary = summary;
            }

            public string Advertisement
            {
                get
                {
                    var preference = AdPreviewVehiclePreference.GetOrCreate(_summary.VehicleHandle, _summary.OwnerHandle);
                    return preference.GetTruncatedAdvertisment();
                }
            }

            public int Mileage
            {
                get { return _summary.Mileage; }
            }

            public string GenericExteriorColor
            {
                get { return _summary.GenericExteriorColor;  }
            }

            public string ExteriorColor
            {
                get { return _summary.ExteriorColor; }
            }

            public string Engine
            {
                get { return _summary.Engine; }
            }

            public string Transmission
            {
                get { return _summary.Transmission; }
            }

            public string DriveTrain
            {
                get { return _summary.DriveTrain; }
            }

            public string StockNumber
            {
                get { return _summary.StockNumber; }
            }

            public string VIN
            {
                get { return _summary.VIN; }
            }

            public string VehicleHandle
            {
                get { return _summary.VehicleHandle; }
            }

            public string OwnerHandle
            {
                get { return _summary.OwnerHandle; }
            }

            public string Description
            {
                get { return _summary.Description; }
            }

            public int Year
            {
                get { return _summary.Year;  }
            }

            public string Make
            {
                get { return _summary.Make; }
            }

            public string Line
            {
                get { return _summary.Line; }
            }

            public string Series
            {
                get { return _summary.Series; }
            }

            public string InteriorColor
            {
                get { return _summary.InteriorColor; }
            }

            public string InventoryType
            {
                get { return _summary.InventoryType; }
            }

        }

    }
}