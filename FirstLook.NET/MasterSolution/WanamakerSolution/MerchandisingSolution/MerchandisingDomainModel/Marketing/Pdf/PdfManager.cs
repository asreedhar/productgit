﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.Core.Xml;
using System.IO;
using System.Diagnostics;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.Documents;

namespace FirstLook.Merchandising.DomainModel.Marketing.Pdf
{
    public abstract class PdfManager
    {
        // private string _sheets;
        readonly string _docOpen = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\r\n<head>\r\n\t<title>Print Sheet</title>\r\n\t<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" /><base href=\"" + ConfigurationManager.AppSettings["prince_xml_assests_host"] + "\" />\r\n\t<link rel=\"stylesheet\" type=\"text/css\" href=\"App_Themes/Lion/MAXSE.521.print.deploy.css\" />\r\n</head>\r\n<body>";
        const string DOC_CLOSE = "</body>\r\n</html>";

        private ILogger _log;
        private SerializableXmlDocument _xmlDoc;

        public static PdfManager FromWebSitePdfXml(string xml, ILogger log)
        {
            var xmlImpl = new XmlImpl(xml, log);
            xmlImpl._xmlDoc = xmlImpl.CreateXmlDoc();
            return xmlImpl;
        }

        public static PdfManager FromDeal(IDeal deal, ILogger log)
        {
            var dealImpl = new DealImpl(deal, log);
            dealImpl._xmlDoc = dealImpl.CreateXmlDoc();
            return dealImpl;
        }

        protected PdfManager(ILogger log)
        {
            Debug.Assert(log != null);

            _log = log;
        }

        public string GetXml()
        {
            return _xmlDoc.InnerXml;
        }

        public string ToHtml()
        {
            return TransformXmlToHtml(_xmlDoc);
        }

        public Collection<byte[]> ToPdfParts()
        {
            string html = ToHtml();
            var princeSvc = new PdfService();
            return new Collection<byte[]> { princeSvc.GeneratePdf(html) };
        }

        public static byte[] MergePdf(Collection<byte[]> parts)
        {
            return PdfDocumentWrapper.Merge(parts);
        }

        public byte[] ToPdf()
        {
            var pdfParts = ToPdfParts();
            return MergePdf(pdfParts);
        }

        protected abstract SerializableXmlDocument CreateXmlDoc();
        protected abstract void AddTransforms(XmlNode xmlDoc, List<Transforms> transformList);

        private string TransformXmlToHtml(XmlNode xmlDoc)
        {
            var transformList = new List<Transforms>();
            AddTransforms(xmlDoc, transformList);
            
            // Always add the disclaimer to the end of our content.
            transformList.Add(Transforms.Disclaimer);

            // Apply the transforms and return.
            var transformed = ApplyTransform(xmlDoc, transformList.ToArray());

            // TODO: Look into putting the copyright escape sequence into the xslt (Disclaimer.xslt) and make sure xslt doesn't transform it back to a '©'.
            transformed = transformed.Replace("©", "&#xA9;").Replace("’", "'");

            // Wrap with open and closing blocks.
            var sb = new StringBuilder();
            sb.Append(_docOpen);
            sb.Append(transformed);
            sb.Append(DOC_CLOSE);

            return sb.ToString();
        }

        private string ApplyTransform(XmlNode doc, params Transforms[] transforms)
        {
            var pdfStringBuilder = new StringBuilder();

            foreach (var transform in transforms)
            {
                var stringBuilder = new StringBuilder();

                string embeddedResourceXslPath;

                switch (transform)
                {
                    case Transforms.ConsumerPacket:
                        embeddedResourceXslPath = "FirstLook.Merchandising.DomainModel.Marketing.Pdf.Xsl.SellingSheet.xslt";
                        break;
                    case Transforms.MarketComparison:
                        embeddedResourceXslPath = "FirstLook.Merchandising.DomainModel.Marketing.Pdf.Xsl.MarketComparison.xslt";
                        break;
                    case Transforms.ValueAnalyzer:
                        embeddedResourceXslPath = "FirstLook.Merchandising.DomainModel.Marketing.Pdf.Xsl.ValueAnalyzer.xslt";
                        break;
                    case Transforms.Disclaimer:
                        embeddedResourceXslPath = "FirstLook.Merchandising.DomainModel.Marketing.Pdf.Xsl.Disclaimer.xslt";
                        break;
                    default:
                        throw new ApplicationException("Invalid sheet specified.");
                }

                var xml = new SerializableXmlDocument();
                xml.LoadXml(doc.OuterXml);

                try
                {
                    using (var writer = new StringWriter(stringBuilder))
                    using (XmlReader xsltReader = XmlReader.Create(typeof(PdfManager).Assembly.GetManifestResourceStream(embeddedResourceXslPath)))
                    {
                        var xslt = new XslCompiledTransform();
                        xslt.Load(xsltReader);
                        xslt.Transform(xml, null, writer);

                        pdfStringBuilder.Append(stringBuilder.ToString(), 0, stringBuilder.Length);
                    }

                }
                catch (Exception exception)
                {
                    _log.Log(exception);
                    return string.Empty;
                }
            }

            return pdfStringBuilder.ToString();
        }

        protected enum Transforms
        {
            ConsumerPacket,
            MarketComparison,
            ValueAnalyzer,
            Disclaimer
        }

        private class DealImpl : PdfManager
        {
            private IDeal _deal;

            public DealImpl(IDeal deal, ILogger log) : base(log)
            {
                Debug.Assert(deal != null);
                _deal = deal;
            }

            protected override SerializableXmlDocument CreateXmlDoc()
            {
                return _deal.GetXml();
            }

            protected override void AddTransforms(XmlNode xmlDoc, List<Transforms> transformList)
            {
                // Build the transform list.
                switch (_deal.PacketType)
                {
                    case Packets.SalesPacket:
                        if ((_deal.CustomerPacketOptions.SalesPacketOptions & SalesPackOptions.SellingSheet) == SalesPackOptions.SellingSheet)
                            transformList.Add(Transforms.ConsumerPacket);

                        if ((_deal.CustomerPacketOptions.SalesPacketOptions & SalesPackOptions.MarketComparison) == SalesPackOptions.MarketComparison)
                            transformList.Add(Transforms.MarketComparison);

                        break;

                    case Packets.ValueAnalyzer:
                        if (!_deal.ValueAnalyzerPacketOptions.DisplayMarketListingsPermission)
                        {
                            var node = xmlDoc.SelectSingleNode("/salesaccelerator/marketlistings");
                            if (node != null)
                            {
                                var attribute = node.Attributes["isDisplayed"];
                                attribute.Value = _deal.ValueAnalyzerPacketOptions.DisplayMarketListingsPermission.ToString().ToLower();
                            }
                        }
                        transformList.Add(Transforms.ValueAnalyzer);
                        break;

                    default:
                        throw new ApplicationException("Invalid packet type.");

                }
            }
        }

        private class XmlImpl : PdfManager
        {
            private string _xml;

            public XmlImpl(string xml, ILogger log) : base(log)
            {
                _xml = xml;
            }

            protected override SerializableXmlDocument CreateXmlDoc()
            {
                SerializableXmlDocument doc = new SerializableXmlDocument();
                doc.LoadXml(_xml);
                return doc;
            }

            protected override void AddTransforms(XmlNode xmlDoc, List<Transforms> transformList)
            {
                transformList.Add(Transforms.ValueAnalyzer);
            }
        }


    }
}
