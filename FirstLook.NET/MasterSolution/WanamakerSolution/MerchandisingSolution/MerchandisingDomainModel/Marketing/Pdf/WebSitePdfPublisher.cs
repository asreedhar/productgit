﻿using System;
using System.Threading;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.Documents;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Marketing.Pdf
{
    public class WebSitePdfPublisher
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(WebSitePdfPublisher).FullName);

        private PricingContext _context;
        private IMarketListingSearchAdapter _searchAdapter;
        private ILogger _logger;
        private string _user;
        private bool _displayMarketListings;

        public WebSitePdfPublisher(PricingContext context, ILogger logger, IMarketListingSearchAdapter searchAdapter, bool displayMarketListings, string user)
        {
            _context = context;
            _searchAdapter = searchAdapter;
            _logger = logger;
            _displayMarketListings = displayMarketListings;
            _user = user;
        }

        public void PublishAsyc()
        {
            ThreadPool.QueueUserWorkItem(x => Publish());
        }

        public string Publish()
        {
            string publishedUrl = string.Empty;
            try
            {
                var pdfManager = GetPdfManager();
	            var xml = pdfManager.GetXml();
                byte[] pdf = pdfManager.ToPdf();

				Log.DebugFormat("Saving Audit XML for marketing document.");
                Audit(xml);
				Log.DebugFormat("Saving Document XML for marketing document.");
				SaveDocumentXml(xml);
				Log.DebugFormat("Publishing PDF marketing document.");
                publishedUrl = PublishDocument(pdf);
            }
            catch (Exception e)
            {
                Log.Error("Failed to Publish PDF", e);
            }

            return publishedUrl;
        }

	    public string GetXml()
	    {
		    var pdfmanager = GetPdfManager();
		    return pdfmanager.GetXml();
	    }

	    public string GetHtml()
	    {
		    var pdfmanager = GetPdfManager();
		    return pdfmanager.ToHtml();
	    }

	    private PdfManager GetPdfManager()
	    {
		    Deal.Deal deal = new PublishDeal(_logger, _searchAdapter, _context.OwnerHandle, _context.VehicleHandle, _context.SearchHandle);
		    deal.PacketType = Packets.ValueAnalyzer;
		    deal.ValueAnalyzerPacketOptions.DisplayMarketListingsPermission = _displayMarketListings;

		    var pdfManager = PdfManager.FromDeal(deal, _logger);
		    return pdfManager;
	    }

	    private void Audit(string xml)
        {
            var auditCommand = new AuditPrintCommand(xml, _context.OwnerHandle, _context.VehicleHandle);
            AbstractCommand.DoRun(auditCommand);
        }

        private void SaveDocumentXml(string xml)
        {
            InventoryDocument doc = InventoryDocument.GetOrCreate(_context.OwnerHandle, _context.VehicleHandle);
            doc.UserName = _user;
            doc.Xml = xml;
            doc.Save();
        }

        private string PublishDocument(byte[] document)
        {
            var command = new PublishValueAnalyzerDocument(_context.OwnerHandle, _context.VehicleHandle, document, _user, _logger);
            AbstractCommand.DoRun(command);

            return command.PublishedUrl;
        }


        private class PublishDeal : Deal.Deal
        {
            public PublishDeal(ILogger logger, IMarketListingSearchAdapter searchAdapter, string ownerHandle, string vehicleHandle, string searchHandle)
                : base(logger, searchAdapter, ownerHandle, vehicleHandle, searchHandle)
            {
            }

            public override VehicleFeaturesInformation GetVehicleFeatures()
            {
                return new MaxAdVehicleFeaturesInformation(OwnerHandle, VehicleHandle);
            }
            
            private class MaxAdVehicleFeaturesInformation : VehicleFeaturesInformation
            {
                public MaxAdVehicleFeaturesInformation(string ownerHandle, string vehicleHandle)
                    : base(ownerHandle, vehicleHandle)
                {   
                }

                protected override int? GetEquipmentProvider()
                {
                    return (int) EquipmentProvider.Code.MaxAd;
                }
            }
        }
    }
}
