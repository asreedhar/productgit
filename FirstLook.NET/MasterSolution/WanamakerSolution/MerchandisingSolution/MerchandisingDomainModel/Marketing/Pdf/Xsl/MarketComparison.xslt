<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes" encoding="iso-8859-1" omit-xml-declaration="yes" media-type="text/html" />

	<xsl:template match="salesaccelerator">
		<div id="content">
			<div id="marketcomparisoncontent" class="page">
				<div id="market-analysis-head">
					<h1>MARKET COMPARISON ANALYSIS</h1>
				</div>
				<div id="vehicle-year-model" class="clearfix">
					<h2>
						<xsl:value-of select="//salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					</h2>
					<div id="vehicleLogos">
						<xsl:if test="//salesaccelerator/consumerhighlights/vehiclehistoryhighlights/@showCarFaxLogo = 'true'">
							<img alt="CarFax One Owner" src="{//salesaccelerator/consumerhighlights/vehiclehistoryhighlights/@carFaxLogoUrl}" class="carFaxOneOwnerLogo" />
						</xsl:if>
						<xsl:if test="//salesaccelerator/certifiedbenefits/@logoPath != '' and ( //salesaccelerator/certifiedbenefits/@display = 'true' or //salesaccelerator/certifiedbenefits/@vehicleIsCertified = 'true')">
							<img alt="Certified Pre-Owned vehicle" src="{//salesaccelerator/certifiedbenefits/@logoPath}" class="certifiedPreOwnedLogo" />
						</xsl:if>
					</div>
				</div>
				<div id="market-comparison-wrap">
					<div id="vehiclespecs">
						<xsl:apply-templates select="vehicleyearmodel/vehiclesummary" />
						<xsl:choose>
							<xsl:when test="//salesaccelerator/overrides/marketcomparisonequipment">
								<xsl:if test="//salesaccelerator/overrides/marketcomparisonequipment = 'true'">
									<xsl:if test="vehicleequipment/*">
										<xsl:apply-templates select="vehicleequipment" />
									</xsl:if>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="vehicleequipment/*">
									<xsl:apply-templates select="vehicleequipment[@display!='false']" />
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:apply-templates select="vehicleyearmodel/vehiclesummary/advertisement" />
						<div id="vehiclespecs-bottom"><![CDATA[ ]]></div>
					</div>
					<ul id="pricing-list">
						<li class="first-child">
							<label><xsl:value-of select="//salesaccelerator/pricing/gaugeprices/price[@isOfferPrice='true']/text" /></label> $<xsl:value-of select="format-number(//salesaccelerator/pricing/gaugeprices/price[@isOfferPrice='true']/amount, '###,##0')" />
						</li>
						<xsl:for-each select="//salesaccelerator/pricing/gaugeprices/price[@displayHead='true' and @isOfferPrice!='true']">
							<li>
								<label>
									<xsl:value-of select="text" />:
								</label> <xsl:value-of select="format-number(amount, '$###,##0')" />
							</li>
						</xsl:for-each>
					</ul>
					<xsl:if test="marketlistings/*">
						<xsl:apply-templates select="marketlistings" />
					</xsl:if>
					<xsl:apply-templates select="offerFooter" />
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/vehicleyearmodel/vehiclesummary">
		<xsl:attribute name="class">
			<xsl:choose>
				<xsl:when test="//salesaccelerator/vehiclephoto/@display = 'false'">noVehiclePhoto</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<div id="VehicleSummary">
			<div class="columnA">
				<xsl:if test="//salesaccelerator/vehiclephoto/@display != 'false'">
          <xsl:variable name="imagePath" select="//salesaccelerator/vehiclephoto/@imagePath" />
          <xsl:choose>
            <xsl:when test="starts-with($imagePath, 'https')">
              <xsl:variable name="newImagePath" select="concat('http', substring-after($imagePath,'https'))" />
              <img src="{$newImagePath}" width="{//salesaccelerator/vehiclephoto/@width}" height="{//salesaccelerator/vehiclephoto/@height}" />
            </xsl:when>
            <xsl:otherwise>
              <img src="{$imagePath}" width="{//salesaccelerator/vehiclephoto/@width}" height="{//salesaccelerator/vehiclephoto/@height}" />
            </xsl:otherwise>
          </xsl:choose>
				</xsl:if>
			</div>
			<div class="columnB clearfix">
				<div id="vehicle-summary">
					<div class="columnBA">
						<ul>
							<li class="first-child" style="width:275px;">
								<label>Exterior Color:</label>
								<span>
									<xsl:value-of select="color" />
								</span>
							</li>
							<li class="first-child">
								<label>Mileage:</label>
								<xsl:value-of select="format-number(mileage, '###,##0')" />
							</li>
							<li id="EngineInfo">
								<label>Engine:</label>
								<span>
									<xsl:value-of select="engine" />
								</span>
							</li>
							<li>
								<label>Transmission:</label>
								<span>
									<xsl:value-of select="transmission" />
								</span>
							</li>
							<li>
								<label>Drive Train:</label>
								<span>
									<xsl:value-of select="drivetrain" />
								</span>
							</li>
						</ul>
					</div>
					<div class="columnBB">
						<ul>
							<li>
								Stock #: <xsl:value-of select="stocknumber" />
							</li>
							<li>
								VIN: <xsl:value-of select="vin" />
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/vehicleequipment">
		<div id="vehicle-features" class="clearfix">
			<h4>Equipment:</h4>
			<ul>
				<xsl:for-each select="item">
					<li>
						<xsl:value-of select="."/>
					</li>
				</xsl:for-each>
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/vehicleyearmodel/vehiclesummary/advertisement">
		<xsl:if test="./text() != ''">
			<div id="vehicle-description">
				<h4>Ad Preview:</h4>
				<p>
					<xsl:value-of select="."/>
				</p>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="salesaccelerator/marketlistings">
		<div id="marketcomparisoncontent">
			<div id="market-comparison">
				<h3>Compare With:</h3>
				<table>
					<tbody>
						<xsl:for-each select="marketlisting">
							<tr>
								<td class="mcItemHeader">
									<ul>
										<li class="vehicleMakeModel">
											<span class="highlight">
												<xsl:value-of select="vehicle/name" />
											</span>
											<ul>
												<li>
													Color: <xsl:value-of select="vehicle/color" />
												</li>
												<li>
													Mileage: <xsl:value-of select="format-number(vehicle/mileage, '###,##0')" />
												</li>
												<xsl:if test="../@showCertifiedColumn='true' and vehicle/certified = 'true'">
													<li>
														<strong>Certified</strong>
													</li>
												</xsl:if>
											</ul>
										</li>
										<li class="vehicleDealer">
											<xsl:if test="../@showDealerColumn='true'">
												Dealership: <span class="highlight">
													<xsl:value-of select="dealer/name" />
												</span>
											</xsl:if>
											<![CDATA[ ]]>
										</li>
										<li class="vehiclePrice">
											Their Price: <xsl:value-of select="format-number(vehicle/price, '$###,##0')" /><br/>
											
											<span class="savings">
												<xsl:if test="number(vehicle/savings) &lt; 0">
													<strong class="red">
														You save
														<xsl:value-of select="format-number(vehicle/savings, '$###,##0;($###,##0)')" />
													</strong>
												</xsl:if>
												<xsl:if test="number(vehicle/savings) &gt; -1">
													<strong>
														You save
														<xsl:value-of select="format-number(vehicle/savings, '$###,##0')" />
													</strong>
												</xsl:if>
											</span>
											
										</li>
									</ul>
								</td>
							</tr>
							<xsl:if test="../../overrides/showadpreviews = 'true'">
								<tr>
									<td class="mcItemDesc">
										<p>
											<xsl:if test="vehicle/description != ''">
												<xsl:value-of select="vehicle/description" />
											</xsl:if>
											<xsl:if test="vehicle/description = ''">
												No ad preview found.
											</xsl:if>
										</p>
									</td>
								</tr>
							</xsl:if>
						</xsl:for-each>
					</tbody>
				</table>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/offerFooter">
		<div id="preview-footer">
			<div id="customer-info">
				<xsl:if test="customer">
					<xsl:if test="@expiredate != '' and customer/name != ''">
						<h3>
							Offer valid until <xsl:value-of select="@expiredate" /> for:
						</h3>
					</xsl:if>
					<xsl:if test="customer/name != ''">
						<ul>
							<li>
								<xsl:value-of select="customer/name" />
							</li>
							<xsl:if test="customer/email != ''">
								<li>
									Email: <xsl:value-of select="customer/email" />
								</li>
							</xsl:if>
							<xsl:if test="customer/phone != ''">
								<li>
									Phone: <xsl:value-of select="customer/phone" />
								</li>
							</xsl:if>
						</ul>
					</xsl:if>
				</xsl:if>
			</div>
			<div id="salesperson-info">
				<xsl:if test="salesperson">
					<xsl:if test="salesperson/name != ''">
						<h3>Prepared by:</h3>
						<ul>
							<li>
								<xsl:value-of select="salesperson/name" />
							</li>
						</ul>
					</xsl:if>
				</xsl:if>
			</div>
			<xsl:if test="//salesaccelerator/dealer/disclaimer != ''">
				<p class="disclaimer">
					<xsl:value-of select="//salesaccelerator/dealer/disclaimer" />
				</p>
			</xsl:if>
		</div>
	</xsl:template>


</xsl:stylesheet>

