﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes" encoding="iso-8859-1" omit-xml-declaration="yes" media-type="text/html" />

	<xsl:template match="salesaccelerator">
		<div id="content">
			<div id="sellingsheetcontent" class="page">
				<xsl:apply-templates select="dealer" />
				<div id="vehicle-year-model" class="clearfix">
					<h2>
						<xsl:value-of select="//salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					</h2>
					<div id="vehicleLogos">
						<xsl:if test="//salesaccelerator/consumerhighlights/vehiclehistoryhighlights/@showCarFaxLogo = 'true'">
							<img alt="CarFax One Owner" src="{//salesaccelerator/consumerhighlights/vehiclehistoryhighlights/@carFaxLogoUrl}" class="carFaxOneOwnerLogo" />
						</xsl:if>
						<xsl:if test="//salesaccelerator/certifiedbenefits/@logoPath != '' and //salesaccelerator/certifiedbenefits/@vehicleIsCertified = 'true'">
							<img alt="Certified Pre-Owned vehicle" src="{//salesaccelerator/certifiedbenefits/@logoPath}" class="certifiedPreOwnedLogo" />
						</xsl:if>
					</div>
				</div>
				<div id="sellingsheet">
					<div id="vehiclespecs">
						<xsl:apply-templates select="vehicleyearmodel/vehiclesummary" />
						<xsl:if test="vehicleequipment/*">
							<xsl:apply-templates select="vehicleequipment[@display!='false']" />
						</xsl:if>
						<xsl:if test="consumerhighlights/*">
							<xsl:apply-templates select="consumerhighlights[@display!='false']" />
						</xsl:if>
						<xsl:apply-templates select="vehicleyearmodel/vehiclesummary/advertisement" />
						<div id="vehiclespecs-bottom"><![CDATA[ ]]></div>
					</div>
					<xsl:apply-templates select="certifiedbenefits[@display='true' and @vehicleIsCertified='true']" />
					<ul id="pricing-list">
						<li class="first-child">
							<label>
								<xsl:value-of select="//salesaccelerator/pricing/gaugeprices/price[@isOfferPrice='true']/text" />
							</label> $<xsl:value-of select="format-number(//salesaccelerator/pricing/gaugeprices/price[@isOfferPrice='true']/amount, '###,##0')" />
						</li>
						<xsl:for-each select="//salesaccelerator/pricing/gaugeprices/price[@displayHead='true' and @isOfferPrice!='true']">
							<li>
								<label>
									<xsl:value-of select="text" />:
								</label> $<xsl:value-of select="format-number(amount, '###,##0')" />
							</li>
						</xsl:for-each>
					</ul>
					<xsl:apply-templates select="pricing[@isDisplayed!='false']" />
					<xsl:if test="marketlistings/*">
						<xsl:apply-templates select="marketlistings[@isDisplayed!='false']" />
					</xsl:if>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/dealer">
		<div id="dealersummary">
			<xsl:if test="logo = ''">
				<xsl:attribute name="class">noDealerLogo</xsl:attribute>
			</xsl:if>
			<div class="logo">
				<img src="{logo}" alt="{name}" width="{logo/@width}" height="{logo/@height}" />
			</div>
			<div class="copy">
				<p>
					<xsl:value-of select="description" />
				</p>
				<p class="address">
					<xsl:if test="normalize-space(contact/address) != ''">
						<xsl:value-of select="contact/address" />
						<span class="middot">
							<img src="/resources/App_Themes/Lion/Images/bullet_LI2.png" />
						</span>
					</xsl:if>
					<xsl:if test="normalize-space(contact/city) != ''">
						<xsl:value-of select="contact/city" />,
					</xsl:if>
					<xsl:if test="normalize-space(contact/state) != ''">
						<span class="nbsp"></span>
						<xsl:value-of select="contact/state" />
						<span class="nbsp"></span>
					</xsl:if>
					<xsl:if test="normalize-space(contact/zip) != ''">
						<span class="nbsp"></span>
						<xsl:value-of select="contact/zip" />
					</xsl:if>
					<xsl:if test="normalize-space(contact/city) != '' or normalize-space(contact/state) != '' or normalize-space(contact/zip) != ''">
						<span class="middot">
							<img src="/resources/App_Themes/Lion/Images/bullet_LI2.png" />
						</span>
					</xsl:if>
					<xsl:if test="displayContactInfoOnWebPdf = 'true' and normalize-space(contact/email) != ''">
						<xsl:value-of select="contact/email" />
						<span class="middot">
							<img src="/resources/App_Themes/Lion/Images/bullet_LI2.png" />
						</span>
					</xsl:if>
					<xsl:if test="normalize-space(url) != ''">
						<a href="{url}" target="_blank">
							<xsl:value-of select="url" />
						</a>
						<span class="middot">
							<img src="/resources/App_Themes/Lion/Images/bullet_LI2.png" />
						</span>
					</xsl:if>
					<xsl:if test="displayContactInfoOnWebPdf = 'true' and normalize-space(contact/phone) != ''">
						<xsl:value-of select="contact/phone" />
					</xsl:if>
				</p>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/vehicleyearmodel/vehiclesummary">
		<xsl:attribute name="class">
			<xsl:choose>
				<xsl:when test="//salesaccelerator/vehiclephoto/@display = 'false'">noVehiclePhoto</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<div id="VehicleSummary">
			<div class="columnA">
				<xsl:if test="//salesaccelerator/vehiclephoto/@display != 'false'">
          <xsl:variable name="imagePath" select="//salesaccelerator/vehiclephoto/@imagePath" />
          <xsl:choose>
            <xsl:when test="starts-with($imagePath, 'https')">
              <xsl:variable name="newImagePath" select="concat('http', substring-after($imagePath,'https'))" />
              <img src="{$newImagePath}" width="{//salesaccelerator/vehiclephoto/@width}" height="{//salesaccelerator/vehiclephoto/@height}" />
            </xsl:when>
            <xsl:otherwise>
              <img src="{$imagePath}" width="{//salesaccelerator/vehiclephoto/@width}" height="{//salesaccelerator/vehiclephoto/@height}" />
            </xsl:otherwise>
          </xsl:choose>
				</xsl:if>
			</div>
			<div class="columnB clearfix">
				<div id="vehicle-summary">
					<div class="columnBA">
						<ul>
							<li class="first-child" style="width:275px;">
								<label>Exterior Color:</label>
								<span>
									<xsl:value-of select="color" />
								</span>
							</li>
							<li class="first-child">
								<label>Mileage:</label>
								<xsl:value-of select="format-number(mileage, '###,##0')" />
							</li>
							<li id="EngineInfo">
								<label>Engine:</label>
								<span>
									<xsl:value-of select="engine" />
								</span>
							</li>
							<li>
								<label>Transmission:</label>
								<span>
									<xsl:value-of select="transmission" />
								</span>
							</li>
							<li>
								<label>Drive Train:</label>
								<span>
									<xsl:value-of select="drivetrain" />
								</span>
							</li>
						</ul>
					</div>
					<div class="columnBB">
						<ul>
							<li>
								Stock #: <xsl:value-of select="stocknumber" />
							</li>
							<li>
								VIN: <xsl:value-of select="vin" />
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/vehicleequipment">
		<div id="vehicle-features" class="clearfix">
			<h4>Equipment:</h4>
			<ul>
				<xsl:for-each select="item">
					<li>
						<xsl:value-of select="."/>
					</li>
				</xsl:for-each>
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/consumerhighlights">
		<div style="border-top:solid transparent 0.001cm">
			<hr class="highlights"/>
			<xsl:apply-templates>
				<xsl:sort select="@rank" data-type="number" />
			</xsl:apply-templates>
		</div>
		<br style="line-height:1.7em" />
	</xsl:template>

	<xsl:template match="salesaccelerator/consumerhighlights/freetexthighlights">
		<div id="consumer-highlights">
			<h4>Additional Highlights:</h4>
			<ul>
				<xsl:for-each select="highlight">
					<li>
						<xsl:value-of select="."/>
					</li>
				</xsl:for-each>
			</ul>
			<br style="line-height:1em" />
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/consumerhighlights/circleratinghighlights">
		<div id="jdpower-highlights">
			<h4>JDPower.com Ratings:</h4>
			<ul>
				<xsl:for-each select="highlight">
					<li>
						<xsl:value-of select="description" />
						<xsl:choose>
							<xsl:when test="contains(value, '1.5')">
								<span class="stars15">
									<xsl:value-of select="value" />
								</span>
							</xsl:when>
							<xsl:when test="contains(value, '2.5')">
								<span class="stars25">
									<xsl:value-of select="value" />
								</span>
							</xsl:when>
							<xsl:when test="contains(value, '3.5')">
								<span class="stars35">
									<xsl:value-of select="value" />
								</span>
							</xsl:when>
							<xsl:when test="contains(value, '4.5')">
								<span class="stars45">
									<xsl:value-of select="value" />
								</span>
							</xsl:when>
							<xsl:otherwise>
								<span class="stars{value}">
									<xsl:value-of select="value" />
								</span>
							</xsl:otherwise>
						</xsl:choose>
					</li>
				</xsl:for-each>
			</ul>
			<br style="line-height:1em" />
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/consumerhighlights/vehiclehistoryhighlights">
		<div id="vhr-highlights">
			<h4>
				Vehicle History Highlights:
				<xsl:if test="//text[contains(., 'CARFAX')]">
					<img align="absmiddle" style="border-width: 0px; margin-left: 0.5em;" src="/resources/pricing/Public/Images/logo_Carfax1.gif" />
				</xsl:if>
				<xsl:if test="//text[contains(., 'AutoCheck')]">
					<img align="absmiddle" style="border-width: 0px; margin-left: 0.5em;" src="/resources/pricing/Public/Images/logo_Autocheck1.gif" />
				</xsl:if>
			</h4>
			<ul>
				<xsl:for-each select="highlight">
					<li>
						<xsl:value-of select="."/>
					</li>
				</xsl:for-each>
			</ul>
			<br style="line-height:1em" />
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/consumerhighlights/snippethighlights">
		<div id="reviewsawards-highlights">
			<h4>Expert Reviews:</h4>
			<ul>
				<xsl:for-each select="highlight">
					<li>
						<span class="source">
							<xsl:value-of select="source" />:
						</span>
						<xsl:value-of select="snippet" />
					</li>
				</xsl:for-each>
			</ul>
			<br style="line-height:1em" />
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/vehicleyearmodel/vehiclesummary/advertisement">
		<xsl:if test="//salesaccelerator/valueAnalyzerOptions/displayVehicleDescription = 'true' and ./text() != ''">
			<div id="vehicle-description">
				<h4>Ad Preview:</h4>
				<p>
					<xsl:value-of select="."/>
				</p>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="salesaccelerator/certifiedbenefits">
		<div id="certified-features" class="clearfix">
			<div class="clearfix">
				<h4>
					<xsl:value-of select="@name" /> FEATURES
				</h4>
				<ul class="clearfix">
					<xsl:for-each select="benefit">
						<li class="clearfix">
							<xsl:value-of select="."/>
						</li>
					</xsl:for-each>
				</ul>
			</div>
			<div id="certified-features-bottom"><![CDATA[ ]]></div>
		</div>
	</xsl:template>

	<xsl:template match="salesaccelerator/pricing">
		<xsl:if test="gaugeprices/price/@isOfferPrice='false'">
			<div id="pricing-mkt-analysis">
				<h3>
					360<sup>o</sup> Pricing Analysis
				</h3>
				<xsl:if test="gaugeprices/@showGauge = 'true'">
					<div id="Gauge">
						<div id="GaugeItems">
							<ul>
								<xsl:for-each select="gaugeprices/price">
									<xsl:if test="@isOfferPrice = 'true'">
										<li class='offerPrice' style='width:{gauge/@width}px;left:{gauge/@position}px'>
											<span>
												<xsl:value-of select="text" />
												<strong>
													$<xsl:value-of select="format-number(amount, '###,##0')" />
												</strong>
											</span>
										</li>
									</xsl:if>
									<xsl:if test="@isOfferPrice != 'true'">
										<li style="width:{gauge/@width}px;left:{gauge/@position}px">
											<span>
												<xsl:value-of select="text" />
												<strong>
													$<xsl:value-of select="format-number(amount, '###,##0')" />
												</strong>
											</span>
										</li>
									</xsl:if>
								</xsl:for-each>
							</ul>
							<xsl:if test="gaugeprices > 0">
								<div id="GaugeBreakDiv" class="gaugeBreakDiv"><![CDATA[ ]]></div>
							</xsl:if>
							<div class="clearBoth"><![CDATA[ ]]></div>
						</div>
					</div>
				</xsl:if>
				<table>
					<tr>
						<th style="width:20%">
							<label class="date">
								(AS OF <xsl:value-of select="//salesaccelerator/@date" />)
							</label>
						</th>
						<th style="width:40%"></th>
						<th style="width:10%"></th>
						<th style="width:30%">
							<label class="price">YOUR PRICE IS:</label>
						</th>
					</tr>
					<xsl:for-each select="comparisonprices/price[@selected = 'true']">
						<tr class="comparisonRow">
							<td class="logo">
								<img src="{logo}" alt="{text}" width="logo/@width" height="{logo/@height}" />
							</td>
							<td class="label">
								<label>
									<xsl:value-of select="text" />
								</label>
							</td>
							<td class="price">
								<xsl:value-of select="amount" />
							</td>
							<td class="delta">
								<xsl:if test="delta &lt; 0">
									<span class="grey">
										<xsl:value-of select="format-number(delta, '$###,##0;$###,##0')" />
									</span>
									<span class="small">
										Above <xsl:value-of select="text" />
									</span>
								</xsl:if>
								<xsl:if test="delta &gt; -1">
									<xsl:value-of select="format-number(delta, '$###,##0')" />
									<span class="small">
										Below <xsl:value-of select="text" />
									</span>
								</xsl:if>
							</td>
						</tr>
					</xsl:for-each>
				</table>
				<div style="clear:both;height:0px;overflow:hidden;"><![CDATA[ ]]></div>
			</div>
			<div class="comparisonBtm"><![CDATA[  ]]></div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="salesaccelerator/marketlistings">

		<xsl:if test="//salesaccelerator/valueAnalyzerOptions/useLongMarketListings = 'false'">
			<div id="market-comparison">
				<div>
					<h3>Comparison Cars in Market:</h3>
					<table>
						<tbody>
							<tr>
								<xsl:if test="@showDealerColumn='true'">
									<th align="left">Dealership</th>
								</xsl:if>
								<th align="left">Vehicle</th>
								<th>Color</th>
								<th>Mileage</th>
								<xsl:if test="@showCertifiedColumn='true'">
									<th style="width:1.59cm">Certified?</th>
								</xsl:if>
								<th style="width:2.27cm">Internet Price</th>
								<th style="width:2.86cm">Your Price Savings:</th>
							</tr>
							<xsl:for-each select="marketlisting">
								<xsl:choose>
									<xsl:when test="position() mod 2 != 1">
										<tr class="highlightRow">
											<xsl:if test="../@showDealerColumn='true'">
												<td>
													<xsl:value-of select="dealer/name" />
												</td>
											</xsl:if>
											<td>
												<xsl:value-of select="vehicle/name" />
											</td>
											<td align="center">
												<xsl:value-of select="vehicle/color" />
											</td>
											<td align="center">
												<xsl:value-of select="format-number(vehicle/mileage, '###,##0')" />
											</td>
											<xsl:if test="../@showCertifiedColumn='true'">
												<td align="center" class="certified">
													<xsl:if test="vehicle/certified = 'true'">
														<div class="checkmark">
															<span>Yes</span>
														</div>
													</xsl:if>
												</td>
											</xsl:if>
											<td align="center">
												<xsl:value-of select="format-number(vehicle/price, '$###,##0')" />
											</td>
											<td align="center" class="savings">
												<xsl:if test="number(vehicle/savings) &lt; 0">
													<strong class="red">
														You save
														<xsl:value-of select="format-number(vehicle/savings, '$###,##0;($###,##0)')" />
													</strong>
												</xsl:if>
												<xsl:if test="number(vehicle/savings) &gt; -1">
													<strong>
														You save
														<xsl:value-of select="format-number(vehicle/savings, '$###,##0')" />
													</strong>
												</xsl:if>
											</td>
										</tr>
										<xsl:if test="../../overrides/showadpreviews = 'true'">
											<tr class="highlightRow">
												<td class="mcItemDesc" colspan="8">
													<p>
														<xsl:if test="vehicle/description != ''">
															<xsl:value-of select="vehicle/description" />
														</xsl:if>
														<xsl:if test="vehicle/description = ''">
															No ad preview found.
														</xsl:if>
													</p>
												</td>
											</tr>
										</xsl:if>
									</xsl:when>
									<xsl:otherwise>
										<tr>
											<xsl:if test="../@showDealerColumn='true'">
												<td>
													<xsl:value-of select="dealer/name" />
												</td>
											</xsl:if>
											<td>
												<xsl:value-of select="vehicle/name" />
											</td>
											<td align="center">
												<xsl:value-of select="vehicle/color" />
											</td>
											<td align="center">
												<xsl:value-of select="format-number(vehicle/mileage, '###,##0')" />
											</td>
											<xsl:if test="../@showCertifiedColumn='true'">
												<td align="center" class="certified">
													<xsl:if test="vehicle/certified = 'true'">
														<div class="checkmark">
															<span>Yes</span>
														</div>
													</xsl:if>
												</td>
											</xsl:if>
											<td align="center">
												<xsl:value-of select="format-number(vehicle/price, '$###,##0')" />
											</td>
											<td align="center" class="savings">
												<xsl:if test="number(vehicle/savings) &lt; 0">
													<strong class="red">
														You save
														<xsl:value-of select="format-number(vehicle/savings, '$###,##0;($###,##0)')" />
													</strong>
												</xsl:if>
												<xsl:if test="number(vehicle/savings) &gt; -1">
													<strong>
														You save
														<xsl:value-of select="format-number(vehicle/savings, '$###,##0')" />
													</strong>
												</xsl:if>
											</td>
										</tr>
										<xsl:if test="../../overrides/showadpreviews = 'true'">
											<tr>
												<td class="mcItemDesc" colspan="8">
													<p>
														<xsl:if test="vehicle/description != ''">
															<xsl:value-of select="vehicle/description" />
														</xsl:if>
														<xsl:if test="vehicle/description = ''">
															No ad preview found.
														</xsl:if>
													</p>
												</td>
											</tr>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tbody>
					</table>
				</div>
			</div>
		</xsl:if>

		<xsl:if test="//salesaccelerator/valueAnalyzerOptions/useLongMarketListings = 'true'">

			<div id="marketcomparisoncontent">
				<div id="market-comparison">
					<h3>Compare With:</h3>
					<table>
						<tbody>
							<xsl:for-each select="marketlisting">
								<tr>
									<td class="mcItemHeader">
										<ul>
											<li class="vehicleMakeModel">
												<span class="highlight">
													<xsl:value-of select="vehicle/name" />
												</span>
												<ul>
													<li>
														Color: <xsl:value-of select="vehicle/color" />
													</li>
													<li>
														Mileage: <xsl:value-of select="format-number(vehicle/mileage, '###,##0')" />
													</li>
													<xsl:if test="../@showCertifiedColumn='true' and vehicle/certified = 'true'">
														<li>
															<strong>Certified</strong>
														</li>
													</xsl:if>
												</ul>
											</li>
											<li class="vehicleDealer">
												<xsl:if test="../@showDealerColumn='true'">
													Dealership: <span class="highlight">
														<xsl:value-of select="dealer/name" />
													</span>
												</xsl:if>
												<![CDATA[ ]]>
											</li>
											<li class="vehiclePrice">
												Their Price: <xsl:value-of select="format-number(vehicle/price, '$###,##0')" /><br/>
												<span class="savings">
													<xsl:if test="number(vehicle/savings) &lt; 0">
														<strong class="red">
															You save
															<xsl:value-of select="format-number(vehicle/savings, '$###,##0;($###,##0)')" />
														</strong>
													</xsl:if>
													<xsl:if test="number(vehicle/savings) &gt; -1">
														<strong>
															You save
															<xsl:value-of select="format-number(vehicle/savings, '$###,##0')" />
														</strong>
													</xsl:if>
												</span>
											</li>
										</ul>
									</td>
								</tr>
								<xsl:if test="../../overrides/showadpreviews = 'true'">
									<tr>
										<td class="mcItemDesc">
											<p>
												<xsl:if test="vehicle/description != ''">
													<xsl:value-of select="vehicle/description" />
												</xsl:if>
												<xsl:if test="vehicle/description = ''">
													No ad preview found.
												</xsl:if>
											</p>
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>
						</tbody>
					</table>
				</div>
			</div>

		</xsl:if>

	</xsl:template>

	<xsl:template match="salesaccelerator/offerFooter">
		<div id="preview-footer">
			<div id="customer-info">
				<xsl:if test="customer">
					<xsl:if test="@expiredate != '' and customer/name != ''">
						<h3>
							Offer valid until <xsl:value-of select="@expiredate" /> for:
						</h3>
					</xsl:if>
					<xsl:if test="customer/name != ''">
						<ul>
							<li>
								<xsl:value-of select="customer/name" />
							</li>
							<xsl:if test="customer/email != ''">
								<li>
									Email: <xsl:value-of select="customer/email" />
								</li>
							</xsl:if>
							<xsl:if test="customer/phone != ''">
								<li>
									Phone: <xsl:value-of select="customer/phone" />
								</li>
							</xsl:if>
						</ul>
					</xsl:if>
				</xsl:if>
			</div>
			<div id="salesperson-info">
				<xsl:if test="salesperson">
					<xsl:if test="salesperson/name != ''">
						<h3>Prepared by:</h3>
						<ul>
							<li>
								<xsl:value-of select="salesperson/name" />
							</li>
						</ul>
					</xsl:if>
				</xsl:if>
			</div>
			<xsl:if test="//salesaccelerator/dealer/disclaimer != ''">
				<p class="disclaimer">
					<xsl:value-of select="//salesaccelerator/dealer/disclaimer" />
				</p>
			</xsl:if>
		</div>
	</xsl:template>


</xsl:stylesheet>

