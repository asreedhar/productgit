﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions"
		xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
	<xsl:output method="html" indent="yes" encoding="iso-8859-1" omit-xml-declaration="yes" media-type="text/html" />

	<xsl:template match="/">
		<div id="content">
			<div id="disclaimercontent" class="page">
				<div id="disclaimer-head">	
					<h1>Glossary of Price Calculations</h1>
				</div>
				
				<p>
					<i>JDPower.com Power Circle Ratings</i>
				</p>
				<p>
					Power Circle Ratings (the Ratings) are developed by J.D. Power and Associates for JDPower.com. All Ratings are based on the opinions of consumers 
					who have actually used or owned the product or service being rated. Since the Ratings are based on J.D. Power and Associates research studies that 
					survey a representative sample of owners, they are indicative of what typical buyers may experience.<br /><br />

					High ratings for a particular product/service/company do not necessarily mean that every customer will have a positive experience. It simply 
					indicates that, on average, consumer perceptions of the product/service/company indicate that it stands out when compared with competitive 
					products/services/companies.
				</p>
				
				<p>
					<i>Expert Reviews</i>
				</p>
				<p>
					Expert Reviews are excerpts from reviews and automotive awards from sites across the internet.  The sites include newCarTestDrive.com, 
					Edmunds.com and other similar sites.<br /><br />

					The positive Expert Reviews are only excerpts of the full article or review.  The full article may contain additional comments, both 
					positive and negative, regarding the vehicle.  For a more complete understanding of the scope of the review, you should read the entire review or 
					award, which can be obtained from the referenced site.
				</p>
				
				<xsl:if test="salesaccelerator/disclaimer/jdpower[@isValid='true']" >
				<p>
					<i>
						JD Power PIN Selling Price
					</i>
				</p>
				<p>
					Actual values for 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					will vary based upon selected optional equipment, market conditions, specifications, vehicle condition or other particular circumstances pertinent 
					to this vehicle. JD Power PIN (Power Information Network) Average Selling Price is calculated using a sampling of PIN enrolled dealer transactions for 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					in your dealer’s geographic region
					(<xsl:value-of select="salesaccelerator/disclaimer/jdpower/region" />)
					over the last
					<xsl:value-of select="salesaccelerator/disclaimer/jdpower/periodweeks" />
					weeks.  JD Power PIN is a representative sample of the market transactions for preowned automotive sales but JD Power PIN does not account for the
					particular condition, equipment, or circumstances of this vehicle which could cause the value of the vehicle to vary significantly from the JD Power 
					PIN Selling Price.  The JD Power PIN data does not include every transaction in a market and transaction coverage is limited by the number of dealers 
					participating in the PIN program.
				</p>
				</xsl:if>
				
				<p>
					<i>NADA Used Car Guide - Retail Value</i>
				</p>
				<p>
					Actual valuations for 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					with <xsl:value-of select="format-number(salesaccelerator/vehicleyearmodel/vehiclesummary/mileage, '###,##0')" /> miles
					will vary based upon optional equipment selected by the dealer, market conditions, specifications, vehicle condition or other particular circumstances 
					pertinent to this vehicle.  The specific information required to determine the value for this vehicle is based on your dealer’s location, state - 
					<xsl:value-of select="salesaccelerator/dealer/contact/state" />
					, for 
					<xsl:value-of select="salesaccelerator/disclaimer/nada/pubmonth"/>
					and NADA Used Car Guide.  NADA Used Car Guide vehicle valuations may vary from vehicle to vehicle.  While the NADA Used Car Guide does take into
					account many variables related to the vehicle beyond make, model and geographic location, it does not account for the particular condition or
					circumstances of this vehicle which could cause the value of the vehicle to vary significantly from the NADA Used Car Guide - Retail Value.
				</p>
				
				<xsl:if test="salesaccelerator/disclaimer/kelleybluebook[@isValid='true']" >
				<p>
					<i>
						Kelley Blue Book Retail Value
					</i>
				</p>
				<p>
					Actual valuations for 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					with <xsl:value-of select="format-number(salesaccelerator/vehicleyearmodel/vehiclesummary/mileage, '###,##0')" /> miles
					will vary based upon optional equipment selected by the dealer, market conditions, specifications, vehicle condition or other particular 
					circumstances pertinent to this vehicle.  The specific information required to determine the value for this vehicle is based on your dealer’s 
					location, zipcode - 
					<xsl:value-of select="salesaccelerator/dealer/contact/zip" />
					, and the Kelley Blue Book Edition, 
					<xsl:value-of select="salesaccelerator/disclaimer/kelleybluebook/pubdate"/>
					Edition.  Kelley Blue Book vehicle valuations may vary from vehicle to vehicle.  While the Kelley Blue Book does take into account many variables 
					related to the vehicle beyond make, model and geographic location, it does not account for the particular condition or circumstances of this 
					vehicle which could cause the value of the vehicle to vary significantly from the Kelley Blue Book Retail Value.
				</p>
				</xsl:if>
				
				<p>
					<i>
						Edmunds True Market Value (TMV)
					</i>
				</p>
				<p>
					Actual valuations for 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					with <xsl:value-of select="format-number(salesaccelerator/vehicleyearmodel/vehiclesummary/mileage, '###,##0')" /> miles
					will vary based upon optional equipment selected by the dealer, market conditions, specifications, vehicle condition or other particular 
					circumstances pertinent to this vehicle.  Edmunds TMV price is an estimate of the current average selling price transactions for 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					with <xsl:value-of select="format-number(salesaccelerator/vehicleyearmodel/vehiclesummary/mileage, '###,##0')" /> miles
					based on optional equipment and vehicle color based on your dealer’s location, zipcode - 
					<xsl:value-of select="salesaccelerator/dealer/contact/zip" />.  
					Edmunds TMV vehicle valuations may vary from vehicle to vehicle.  While the Edmunds TMV does take into account many variables related to the 
					vehicle beyond make, model and geographic location, it does not account for the particular condition or circumstances of this vehicle which could 
					cause the value of the vehicle to vary significantly from the Edmunds True Market Value.
				</p>
				
				<xsl:if test="salesaccelerator/disclaimer/ping[@isValid='true']" >
				<p>
					<i>
						Average Market Price
					</i>
				</p>

				<p>
					Average Market Price is calculated based on sampling of listings based on your dealer’s zipcode, 
					<xsl:value-of select="salesaccelerator/dealer/contact/zip" />.  
					The source of the listings is third-party sites and dealer websites containing preowned and new vehicles.  Average Market Price values may vary 
					from vehicle to vehicle.  Actual valuations for 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					with <xsl:value-of select="format-number(salesaccelerator/vehicleyearmodel/vehiclesummary/mileage, '###,##0')" /> miles
					will vary based upon selected optional equipment, market conditions, specifications, vehicle condition or other particular circumstances pertinent 
					to this vehicle.  While the average market price is an average of similar 
					<xsl:value-of select="salesaccelerator/vehicleyearmodel/vehiclesummary/yearmodel" />
					and <xsl:value-of select="salesaccelerator/disclaimer/ping/radiusmiles" />
					miles radius from dealership, based on your dealer’s zipcode
					(<xsl:value-of select="salesaccelerator/dealer/contact/zip" />), 
					the average can include vehicles with different trims, mileage ranges and dissimilar vehicle equipment.  The sample of market listings may not 
					represent all relevant vehicles available in the area searched.
				</p>

				<p>
					<i>Comparison Cars in Market</i>
				</p>
				<p>
					Comparison Cars in Market is created based on a select set of dealer classified listings from over forty thousand dealers nationally.  The source of 
					the listings is third-party sites and dealer websites containing preowned and new vehicles.  While comparison cars are from similar vehicles based on 
					year, manufacturer, model and 
					<xsl:value-of select="salesaccelerator/disclaimer/ping/radiusmiles" />
					miles radius from dealership, based on your dealer’s zipcode
					(<xsl:value-of select="salesaccelerator/dealer/contact/zip" />), 
					the comparison can include different trims, mileage ranges and dissimilar vehicle equipment.  The dealer has full discretion in selecting the 
					comparison cars, and may choose vehicles with different levels of trim and additional optional equipment.  As a consequence, the comparison cars may 
					not be comparable to the vehicle you are considering.  The Comparison Cars in Market are based on the sample market listings available from various 
					Internet data sources and may not represent all relevant vehicles available in the area searched.
				</p>
				</xsl:if>
				
				<div class="underline">
				</div>
				<p>
					CONSUMER IS RESPONSIBLE FOR VERIFYING ALL EQUIPMENT.
				</p>

				<p>
					INCISENT TECHNOLOGIES DEPENDS ON ITS SOURCES FOR THE ACCURACY AND RELIABILITY OF ITS INFORMATION. THEREFORE, NO RESPONSIBILITY IS ASSUMED BY INCISENT
					TECHNOLOGIES OR ITS AGENTS FOR ERRORS OR OMISSIONS IN THIS REPORT. INCISENT TECHNOLOGIES FURTHER EXPRESSLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED,
					INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
				</p>

				<p>
					ALL INFORMATION RELATED TO THE SOURCES ABOVE WAS ASSEMBLED BY
					<xsl:value-of select="translate(salesaccelerator/dealer/name,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
					AND NO RESPONSIBILITY IS ASSUMED BY INCISENT TECHNOLOGIES OR ITS AGENTS FOR ERRORS OR OMISSIONS OF THE ASSEMBLED DATA.
				</p>

				<p>
					© 2010 INCISENT Technologies, LLC. All rights reserved.
					<xsl:value-of select="salesaccelerator/@date" />
				</p>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
