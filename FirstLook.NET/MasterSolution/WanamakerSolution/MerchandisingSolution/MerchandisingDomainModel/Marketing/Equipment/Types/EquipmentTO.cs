using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    [Serializable]
    public class EquipmentTO
    {
        private int _id;
        private string _name;
        private int _rank;
        private int? _value;
        private bool _selected;
        private byte[] _version;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Rank
        {
            get { return _rank; }
            set { _rank = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        public int? Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public byte[] Version
        {
            get { return _version; }
            set { _version = value; }
        }
    }
}