using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    public class EquipmentInfoListTO : List<EquipmentInfoTO>
    {
        private EquipmentSourceTO _source;

        public EquipmentSourceTO Source
        {
            get { return _source; }
            set { _source = value; }
        }
    }
}