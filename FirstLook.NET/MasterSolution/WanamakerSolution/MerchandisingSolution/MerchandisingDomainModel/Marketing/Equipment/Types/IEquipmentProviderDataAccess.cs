namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    public interface IEquipmentProviderDataAccess
    {
        #region Abstract Book Provider

        EquipmentSourceTO GetCurrentSource(string ownerHandle, string vehicleHandle, int equipmentProviderId);

        EquipmentInfoListTO GetEquipmentList(string ownerHandle, string vehicleHandle, int equipmentProviderId);

        #endregion
    }
}