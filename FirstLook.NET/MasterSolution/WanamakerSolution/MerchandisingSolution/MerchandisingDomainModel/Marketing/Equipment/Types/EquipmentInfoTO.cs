using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    [Serializable]
    public class EquipmentInfoTO
    {
        private string _name;
        private int? _value;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int? Value
        {
            get { return _value; }
            set { this._value = value; }
        }
    }
}