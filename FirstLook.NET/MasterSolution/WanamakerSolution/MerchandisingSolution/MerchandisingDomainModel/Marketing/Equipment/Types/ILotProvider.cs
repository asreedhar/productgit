﻿
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    public interface ILotProvider
    {
       LotProviderCode  LotProviderCode { get; }
    }
}
