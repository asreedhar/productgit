using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    [Serializable]
    public class VehicleEquipmentProviderAssignmentTO
    {
        private int _equipmentProviderId;
        private bool _visible;
        private byte[] _version;

        public int EquipmentProviderId
        {
            get { return _equipmentProviderId; }
            set { _equipmentProviderId = value; }
        }

        public byte[] Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }
    }
}