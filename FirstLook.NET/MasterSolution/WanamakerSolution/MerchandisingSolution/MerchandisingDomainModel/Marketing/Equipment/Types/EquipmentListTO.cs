using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    [Serializable]
    public class EquipmentListTO : List<EquipmentTO>
    {
        private int _id;
        private EquipmentSourceTO _source;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public EquipmentSourceTO Source
        {
            get { return _source; }
            set { _source = value; }
        }
    }
}