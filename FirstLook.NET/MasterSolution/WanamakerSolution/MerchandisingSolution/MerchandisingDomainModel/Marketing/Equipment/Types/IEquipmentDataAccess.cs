using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    public interface IEquipmentDataAccess
    {
        ITransaction BeginTransaction();

        bool Exists(string ownerHandle, string vehicleHandle, int equipmentProviderId);

        int Create(ITransaction transaction, string ownerHandle, string vehicleHandle, EquipmentSourceTO source);

        EquipmentListTO Fetch(string ownerHandle, string vehicleHandle, int equipmentProviderId);

        void Delete(ITransaction transaction, int equipmentListId);

        void Insert(ITransaction transaction, int equipmentListId, EquipmentTO equipment);

        void Update(ITransaction transaction, int equipmentListId, EquipmentTO equipment);

        void Delete(ITransaction transaction, int equipmentListId, EquipmentTO equipment);
    }
}