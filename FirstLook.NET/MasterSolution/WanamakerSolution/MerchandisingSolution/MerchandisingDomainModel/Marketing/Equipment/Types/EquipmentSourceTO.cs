using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    [Serializable]
    public class EquipmentSourceTO
    {
        private int _equipmentProviderId;
        private long _identity;

        public int EquipmentProviderId
        {
            get { return _equipmentProviderId; }
            set { _equipmentProviderId = value; }
        }

        public long Identity
        {
            get { return _identity; }
            set { _identity = value; }
        }
    }
}