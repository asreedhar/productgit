using System.Collections.Generic;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    public interface IEquipmentProviderAssignmentDataAccess
    {
        ITransaction BeginTransaction();

        IList<EquipmentProviderInfoTO> FetchEquipmentProviderInfo();

        IList<EquipmentProviderInfoTO> FetchEquipmentProviderInfo(string ownerHandle);

        bool ExistsEquipmentProviderAssignmentList(string ownerHandle);

        int CreateEquipmentProviderAssignmentList(string ownerHandle);

        EquipmentProviderAssignmentListTO FetchEquipmentProviderAssignmentList(string ownerHandle);

        bool ExistsVehicleEquipmentProviderAssignment(string ownerHandle, string vehicleHandle);

        VehicleEquipmentProviderAssignmentTO FetchVehicleEquipmentProviderAssignment(string ownerHandle, string vehicleHandle);

        bool HasAssignment(string ownerHandle, int equipmentProviderId);

        void Insert(ITransaction transaction, int id, int rank, EquipmentProviderAssignmentTO equipmentProviderAssignment);

        void Update(ITransaction transaction, int id, int rank, EquipmentProviderAssignmentTO equipmentProviderAssignment);

        void Delete(ITransaction transaction, int id, EquipmentProviderAssignmentTO equipmentProviderAssignment);

        void Insert(string ownerHandle, string vehicleHandle, VehicleEquipmentProviderAssignmentTO assignment);

        void Update(string ownerHandle, string vehicleHandle, VehicleEquipmentProviderAssignmentTO assignment);

        void Delete(string ownerHandle, string vehicleHandle, VehicleEquipmentProviderAssignmentTO assignment);
    }
}
