using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    [Serializable]
    public class EquipmentProviderAssignmentTO
    {
        private int _equipmentProviderId;
        private bool _isDefault;
        private byte[] _version;

        public int EquipmentProviderId
        {
            get { return _equipmentProviderId; }
            set { _equipmentProviderId = value; }
        }

        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        public byte[] Version
        {
            get { return _version; }
            set { _version = value; }
        }
    }
}