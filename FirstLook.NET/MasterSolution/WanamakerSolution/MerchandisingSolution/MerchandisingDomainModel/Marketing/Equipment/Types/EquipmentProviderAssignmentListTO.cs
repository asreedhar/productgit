using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types
{
    public class EquipmentProviderAssignmentListTO : List<EquipmentProviderAssignmentTO>
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }
}