using System;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class Equipment : BusinessBase<Equipment>
    {
        #region Business Methods

        private int _id;
        private string _name;
        private bool _selected;
        private int? _value;
        private byte[] _version;

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return _name;
            }
            set
            {
                CanWriteProperty("Name", true);

                if (!Equals(Name, value))
                {
                    _name = value;

                    PropertyHasChanged("Name");
                }
            }
        }

        public bool Selected
        {
            get
            {
                CanReadProperty("Selected", true);

                return _selected;
            }
            set
            {
                CanWriteProperty("Selected", true);

                if (Selected != value)
                {
                    _selected = value;

                    PropertyHasChanged("Selected");
                }
            }
        }

        public int? Value
        {
            get
            {
                CanReadProperty("Value", true);

                return _value;
            }
            set
            {
                CanWriteProperty("Value", true);

                if (!Nullable.Equals(Value, value))
                {
                    _value = value;

                    PropertyHasChanged("Value");
                }
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Factory Methods

        private Equipment()
        {
            MarkAsChild();
        }

        private Equipment(EquipmentTO equipment, bool create) : this()
        {
            if (create)
            {
                Create(equipment);
            }
            else
            {
                Fetch(equipment);
            }
        }

        private Equipment(string name, int? value, bool selected) : this()
        {
            _name = name;
            _value = value;
            _selected = selected;
        }

        internal static Equipment NewEquipment(string name, int? value, bool selected)
        {
            return new Equipment(name, value, selected);
        }

        internal static Equipment NewEquipment(EquipmentTO equipment)
        {
            return new Equipment(equipment, true);
        }

        internal static Equipment GetEquipment(EquipmentTO equipment)
        {
            return new Equipment(equipment, false);
        }

        #endregion

        #region Data Access

        private void Create(EquipmentTO equipment)
        {
            FromTransferObject(equipment);
        }

        private void Fetch(EquipmentTO equipment)
        {
            FromTransferObject(equipment);

            MarkOld();
        }

        internal void Insert(IEquipmentDataAccess accessor, ITransaction transaction, int id, int rank)
        {
            EquipmentTO transferObject = ToTransferObject(rank);

            accessor.Insert(transaction, id, transferObject);

            _id = transferObject.Id;

            _version = transferObject.Version;

            MarkOld();
        }

        internal void Update(IEquipmentDataAccess accessor, ITransaction transaction, int id, int rank)
        {
            EquipmentTO transferObject = ToTransferObject(rank);

            accessor.Update(transaction, id, transferObject);

            _version = transferObject.Version;

            MarkOld();
        }

        internal void DeleteSelf(IEquipmentDataAccess accessor, ITransaction transaction, int id)
        {
            accessor.Delete(transaction, id, ToTransferObject(-1));

            MarkNew();
        }

        private void FromTransferObject(EquipmentTO equipment)
        {
            _id = equipment.Id;
            _name = equipment.Name;
            _selected = equipment.Selected;
            _value = equipment.Value;
            _version = equipment.Version;
        }

        private EquipmentTO ToTransferObject(int rank)
        {
            EquipmentTO equipment = new EquipmentTO();
            equipment.Id = _id;
            equipment.Name = _name;
            equipment.Rank = rank;
            equipment.Selected = _selected;
            equipment.Value = _value;
            equipment.Version = _version;
            return equipment;
        }

        #endregion
    }
}