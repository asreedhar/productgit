using System;
using Csla;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class EquipmentProviderInfo : ReadOnlyBase<EquipmentProviderInfo>
    {
        #region Business Methods

        private int _id;
        private string _name;

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        public bool IsAvailable(string ownerHandle)
        {
            return AvailableCommand.Execute(ownerHandle, Id);
        }

        public static bool IsAvailable(string ownerHandle, int providerId)
        {
            return AvailableCommand.Execute(ownerHandle, providerId);
        }

        #endregion

        #region Factory Methods

        internal static EquipmentProviderInfo GetProviderInfo(EquipmentProviderInfoTO values)
        {
            return new EquipmentProviderInfo(values);
        }

        private EquipmentProviderInfo(EquipmentProviderInfoTO values)
        {
            Fetch(values);
        }

        #endregion

        #region Data Access

        [Serializable]
        private class AvailableCommand : InjectableCommandBase
        {
            #region Injected dependencies

// ReSharper disable MemberCanBePrivate.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
            public IEquipmentProviderAssignmentDataAccess DataAccess { get; set; }
// ReSharper restore UnusedAutoPropertyAccessor.Local
// ReSharper restore MemberCanBePrivate.Local

            #endregion

            public static bool Execute(string ownerHandle, int providerId)
            {
                var command = new AvailableCommand(ownerHandle, providerId);
                DataPortal.Execute(command);
                return command._available;
            }

            private readonly string _ownerHandle;
            private readonly int _providerId;
            private bool _available;

            private AvailableCommand(string ownerHandle, int provider)
            {
                _ownerHandle = ownerHandle;
                _providerId = provider;
            }

            protected override void DataPortal_Execute()
            {
                _available = DataAccess.HasAssignment(_ownerHandle, _providerId);
            }
        }

        private void Fetch(EquipmentProviderInfoTO values)
        {
            _id = values.Id;
            _name = values.Name;
        }

        #endregion
    }
}