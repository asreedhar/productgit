using System;
using System.Collections.Generic;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class EquipmentProviderAssignmentList : InjectableBusinessListBase<EquipmentProviderAssignmentList, EquipmentProviderAssignment>
    {
        #region Business Methods

        private string _ownerHandle;

        private int _id;

        public int Id
        {
            get { return _id; }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public bool Contains(int providerId)
        {
            foreach (EquipmentProviderAssignment provider in this)
            {
                if (provider.EquipmentProviderId == providerId)
                {
                    return true;
                }
            }

            return false;
        }

        public bool Remove(int providerId)
        {
            foreach (EquipmentProviderAssignment provider in this)
            {
                if (provider.EquipmentProviderId == providerId)
                {
                    return Remove(provider);
                }
            }

            return false;
        }

        public bool Assign(int providerId)
        {
            if (Contains(providerId))
            {
                return false;
            }

            Add(EquipmentProviderAssignment.NewProvider(providerId));

            return true;
        }

        public void Reconcile(EquipmentProviderInfoList providers)
        {
            foreach (EquipmentProviderInfo provider in providers)
            {
                bool contains = Contains(provider.Id);

                bool available = provider.IsAvailable(OwnerHandle);

                if (available && !contains)
                {
                    Assign(provider.Id);
                }
                else if (!available && contains)
                {
                    Remove(provider.Id);
                }
            }

            bool hasDefault = false;

            foreach (EquipmentProviderAssignment assignment in this)
            {
                hasDefault |= assignment.IsDefault;
            }

            if (!hasDefault && Count > 0)
            {
                //Want MaxAd as default if there
                var providerAssignment = this.Where(x => ((EquipmentProvider.Code) x.EquipmentProviderId) == EquipmentProvider.Code.MaxAd).Select(x => x).SingleOrDefault();
                providerAssignment = providerAssignment ?? this[0];

                providerAssignment.IsDefault = true;
            }
        }

        #endregion

        #region Factory Methods

        public static bool Exists(string ownerHandle)
        {
            return ExistsCommand.Execute(ownerHandle);
        }

        public static EquipmentProviderAssignmentList GetEquipmentProviders(string ownerHandle)
        {
            return DataPortal.Fetch<EquipmentProviderAssignmentList>(new Criteria(ownerHandle));
        }

        public static EquipmentProviderAssignmentList NewEquipmentProviders(string ownerHandle)
        {
            return DataPortal.Create<EquipmentProviderAssignmentList>(new Criteria(ownerHandle));
        }

        public static EquipmentProviderAssignmentList ReconcileEquipmentProviders(string ownerHandle)
        {
            EquipmentProviderInfoList providers = EquipmentProviderInfoList.GetEquipmentProviders();

            return ReconcileEquipmentProviders(ownerHandle, providers);
        }

        public static EquipmentProviderAssignmentList ReconcileEquipmentProviders(string ownerHandle, EquipmentProviderInfoList providers)
        {
            EquipmentProviderAssignmentList assignmnents;

            if (Exists(ownerHandle))
            {
                assignmnents = GetEquipmentProviders(ownerHandle);
            }
            else
            {
                assignmnents = NewEquipmentProviders(ownerHandle);
            }

            assignmnents.Reconcile(providers);

            assignmnents.Save();

            return assignmnents;
        }

        public class DataSource
        {
            public IList<EquipmentProviderInfo> Select(string ownerHandle)
            {
                EquipmentProviderInfoList providers = EquipmentProviderInfoList.GetEquipmentProviders();

                EquipmentProviderAssignmentList assignmnents = ReconcileEquipmentProviders(ownerHandle, providers);

                List<EquipmentProviderInfo> result = new List<EquipmentProviderInfo>();

                foreach (EquipmentProviderInfo provider in providers)
                {
                    foreach (EquipmentProviderAssignment assignment in assignmnents)
                    {
                        if (assignment.EquipmentProviderId == provider.Id)
                        {
                            result.Add(provider);
                        }
                    }
                }

                return result;
            }
        }

        #endregion

        #region Injected dependencies

        public IEquipmentProviderAssignmentDataAccess EquipmentProviderAssignmentDataAccess { get; set;}

        #endregion

        [Serializable]
        protected class Criteria
        {
            private readonly string _ownerHandle;

            public Criteria(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }
        }

        [Serializable]
        private class ExistsCommand : InjectableCommandBase
        {
            #region Injected dependencies

// ReSharper disable MemberCanBePrivate.Local
            public IEquipmentProviderAssignmentDataAccess EquipmentProviderAssignmentDataAccess { get; set; }
// ReSharper restore MemberCanBePrivate.Local

            #endregion

            #region Data Access

            public static bool Execute(string ownerHandle)
            {
                ExistsCommand command = new ExistsCommand(ownerHandle);
                DataPortal.Execute(command);
                return command._exists;
            }

            private readonly string _ownerHandle;
            private bool _exists;

            private ExistsCommand(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            protected override void DataPortal_Execute()
            {
                _exists = EquipmentProviderAssignmentDataAccess.ExistsEquipmentProviderAssignmentList(_ownerHandle);
            }
        }

        protected void DataPortal_Create(Criteria criteria)
        {
            _ownerHandle = criteria.OwnerHandle;

            _id = EquipmentProviderAssignmentDataAccess.CreateEquipmentProviderAssignmentList(criteria.OwnerHandle);
        }

        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            _ownerHandle = criteria.OwnerHandle;

            Fetch(EquipmentProviderAssignmentDataAccess.FetchEquipmentProviderAssignmentList(criteria.OwnerHandle));
        }

        protected override void DataPortal_Update()
        {
            RaiseListChangedEvents = false;

            try
            {
                IEquipmentProviderAssignmentDataAccess accessor = EquipmentProviderAssignmentDataAccess;

                using (ITransaction transaction = accessor.BeginTransaction())
                {
                    foreach (EquipmentProviderAssignment provider in DeletedList)
                    {
                        provider.DeleteSelf(accessor, transaction, Id);
                    }

                    DeletedList.Clear();

                    for (int i = 0, l = Count; i < l; i++)
                    {
                        EquipmentProviderAssignment provider = this[i];

                        if (provider.IsNew)
                        {
                            provider.Insert(accessor, transaction, Id, i);
                        }
                        else
                        {
                            provider.Update(accessor, transaction, Id, i);
                        }
                    }

                    transaction.Commit();
                }
            }
            finally
            {
                RaiseListChangedEvents = true;
            }
        }

        private void Fetch(EquipmentProviderAssignmentListTO assignments)
        {
            _id = assignments.Id;

            RaiseListChangedEvents = false;

            foreach (EquipmentProviderAssignmentTO values in assignments)
            {
                Add(EquipmentProviderAssignment.GetProvider(values));
            }

            RaiseListChangedEvents = true;
        }

       

        #endregion
    }
}