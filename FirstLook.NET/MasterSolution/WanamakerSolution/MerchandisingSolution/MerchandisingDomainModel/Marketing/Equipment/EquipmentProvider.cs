using System;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    public abstract class EquipmentProvider
    {
        public static EquipmentProvider GetEquipmentProvider(int id)
        {
            if (!Enum.IsDefined(typeof (Code), id))
            {
                throw new Exception("EquipmentProvider Not Defined!");
            }

            Code code = (Code) Enum.ToObject(typeof (Code), id);

            switch (code)
            {
                case Code.BlackBook:
                    return new BlackBookEquipmentProvider();
                case Code.Nada:
                    return new NadaEquipmentProvider();
                case Code.KelleyBlueBook:
                    return new KelleyBlueBookEquipmentProvider();
                case Code.Galves:
                    return new GalvesEquipmentProvider();
                case Code.Edmunds:
                    return new EdmundsEquipmentProvider();
                case Code.GetAuto:
                    return new GetAutoEquipmentProvider();
                case Code.DealerPeak:
                    return new DealerPeakEquipmentProvider();
                case Code.eBizAutos:
                    return new eBizAutosEquipmentProvider();
                case Code.HomeNet:
                    return new HomeNetEquipmentProvider();
                case Code.CDMData:
                    return new CDMDataEquipmentProvider();
                case Code.AULTec:
                    return new AULTecEquipmentProvider();
                case Code.MaxAd:
                    return new MaxAdEquipmentProvider();

                default:
                    throw new Exception("EquipmentProvider Not Implemented!");
            }
        }

        public abstract Code Id { get; }

        protected abstract EquipmentSourceTO GetCurrentSource(string ownerHandle, string vehicleHandle);

        protected abstract EquipmentInfoListTO GetEquipmentList(string ownerHandle, string vehicleHandle);

        public bool HasChanged(EquipmentList values)
        {
            EquipmentSourceTO source = GetCurrentSource(values.OwnerHandle, values.VehicleHandle);

            return ((source == null) || (source.Identity != values.Source.Identity));
        }

        public EquipmentList Fetch(string ownerHandle, string vehicleHandle)
        {
            EquipmentInfoListTO list = GetEquipmentList(ownerHandle, vehicleHandle);
            
            if (list == null) return null;

            return EquipmentList.NewEquipmentList(ownerHandle, vehicleHandle, list);
        }

        [Serializable]
        public enum Code
        {
            NotDefined = 0,
            BlackBook = 1,
            Nada = 2,
            KelleyBlueBook = 3,
            Galves = 4,
            Edmunds = 5,
            AutoTrader = 6,
            CarsDotCom = 7,
            GetAuto = 8,
            DealerPeak = 9,
// ReSharper disable InconsistentNaming
            eBizAutos = 10,
// ReSharper restore InconsistentNaming
            HomeNet = 11,
            CDMData = 12,
            AULTec = 13,
            MaxAd = 14
        }
    }
}