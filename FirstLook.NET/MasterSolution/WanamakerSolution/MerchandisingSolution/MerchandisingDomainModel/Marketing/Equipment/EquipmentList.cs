using System;
using System.Collections.Generic;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;
using FirstLook.Pricing.DomainModel.Presentation;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class EquipmentList : InjectableBusinessListBase<EquipmentList, Equipment>
    {
        #region Business Methods

        private string _ownerHandle;
        private string _vehicleHandle;
        private int _id;
        private EquipmentSource _source;

        public int Id
        {
            get { return _id; }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public EquipmentSource Source
        {
            get { return _source; }
        }

        public void MergeSelections(EquipmentList original)
        {
            foreach (Equipment value in original)
            {
                foreach (Equipment item in this)
                {
                    if (string.Equals(value.Name, item.Name, StringComparison.InvariantCulture))
                    {
                        item.Selected = value.Selected;
                    }
                }
            }
        }

        public void Update(IList<ITableRow> values)
        {
            foreach (Equipment item in this)
            {
                bool exists = false;

                foreach (ITableRow value in values)
                {
                    if (value.Id == item.Id)
                    {
                        item.Selected = value.Selected;
                        item.Name = value.Name;
                        exists = true;
                        break;
                    }
                }

                if (!exists)
                {
                    throw new Exception("GAH! Missing Equipment!");
                }
            }

            // rank

            for (int i = 0, ic = values.Count; i < ic; i++)
            {
                int id = values[i].Id;

                int x = -1;

                for (int j = 0, jc = Count; j < jc; j++)
                {
                    int jd = this[j].Id;

                    if (jd == id)
                    {
                        x = j;

                        break;
                    }
                }

                if (x == -1)
                {
                    throw new Exception("AAAARGH! Not Found!");
                }

                if (i != x)
                {
                    Equipment tmp = this[x];

                    RemoveAt(x);

                    Insert(i, tmp);
                }
            }
        }

        protected override void InsertItem(int index, Equipment item)
        {
            if (DeletedList.Contains(item))
            {
                DeletedList.Remove(item);
            }

            base.InsertItem(index, item);
        }
        
        public override EquipmentList Save()
        {
            if (_id == 0)
            {
                return DataPortal.Update(this);
            }

            return base.Save();
        }
        

        #endregion

        #region Factory Methods

        private EquipmentList()
        {
            /* force use of factory methods */
        }

        internal static EquipmentList NewEquipmentList(string ownerHandle, string vehicleHandle,
                                                                   EquipmentInfoListTO items)
        {
            return DataPortal.Create<EquipmentList>(new CreateCriteria(ownerHandle, vehicleHandle, items));
        }

        public static bool Exists(string ownerHandle, string vehicleHandle, int providerId)
        {
            return ExistsCommand.Execute(ownerHandle, vehicleHandle, providerId);
        }

        public static EquipmentList GetEquipmentList(string ownerHandle, string vehicleHandle,
                                                                 int providerId)
        {
            return DataPortal.Fetch<EquipmentList>(new Criteria(ownerHandle, vehicleHandle, providerId));
        }

        public static EquipmentList ReconcileEquipmentList(string ownerHandle, string vehicleHandle,
                                                                 int providerId)
        {
            EquipmentList lastKnownEquipment = null;

            if (Exists(ownerHandle, vehicleHandle, providerId))
            {
                lastKnownEquipment = GetEquipmentList(ownerHandle, vehicleHandle, providerId);
            }

            EquipmentProvider provider = EquipmentProvider.GetEquipmentProvider(providerId);

            if (lastKnownEquipment == null)
            {
                EquipmentList currentEquipment = provider.Fetch(ownerHandle, vehicleHandle);
                
                if (currentEquipment != null) 
                    currentEquipment.Save();

                return currentEquipment;
            }

            if (provider.HasChanged(lastKnownEquipment))
            {
                EquipmentList currentEquipment = provider.Fetch(ownerHandle, vehicleHandle);

                if (currentEquipment != null)
                {
                    currentEquipment.MergeSelections(lastKnownEquipment);

                    Replace(lastKnownEquipment, currentEquipment);

                    return currentEquipment;
                }

                // we had equipment at one point (lastKnownEquipment != null), 
                // but the provider has no equipment for the same vehicle now (currentEquipment == null).
                // The original list can be considered void; delete it.
                Delete(lastKnownEquipment);
                return null;

            }

            return lastKnownEquipment;
        }

        public static void Replace(EquipmentList original, EquipmentList replacement)
        {
            ReplaceCommand.Execute(original, replacement);
        }

        public static void Delete(EquipmentList list)
        {
            DeleteCommand.Execute(list);
        }

        public class DataSource
        {
            public EquipmentList Select(string ownerHandle, string vehicleHandle, int providerId)
            {
                return ReconcileEquipmentList(ownerHandle, vehicleHandle, providerId);
            }

            public void Update(string ownerHandle, string vehicleHandle, int providerId, IList<ITableRow> values)
            {
                // if there is no list, then we won't try to update it.
                // this would happen if there is an empty list for a given owner,vehicle & provider
                if (Exists(ownerHandle, vehicleHandle, providerId))
                {
                    EquipmentList list = GetEquipmentList(ownerHandle, vehicleHandle, providerId);

                    list.Update(values);

                    list.Save();
                }
            }
        }

        #endregion

        #region Injected dependencies

        public IEquipmentDataAccess EquipmentDataAccess { get; set; }

        #endregion

        #region Data Access

        [Serializable]
        protected class Criteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private readonly int _providerId;

            public Criteria(string ownerHandle, string vehicleHandle, int providerId)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
                _providerId = providerId;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }

            public int ProviderId
            {
                get { return _providerId; }
            }
        }
        [Serializable]
        protected class CreateCriteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private readonly EquipmentInfoListTO _items;

            public CreateCriteria(string ownerHandle, string vehicleHandle, EquipmentInfoListTO items)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
                _items = items;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }

            public EquipmentInfoListTO Items
            {
                get { return _items; }
            }
        }

        [Serializable]
        private class ExistsCommand : InjectableCommandBase
        {
            #region Injected dependencies

// ReSharper disable MemberCanBePrivate.Local
            public IEquipmentDataAccess EquipmentDataAccess { get; set; }
// ReSharper restore MemberCanBePrivate.Local

            #endregion

            public static bool Execute(string ownerHandle, string vehicleHandle, int providerId)
            {
                ExistsCommand command = new ExistsCommand(ownerHandle, vehicleHandle, providerId);
                DataPortal.Execute(command);
                return command._exists;
            }

            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private readonly int _providerId;
            private bool _exists;

            private ExistsCommand(string ownerHandle, string vehicleHandle, int providerId)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
                _providerId = providerId;
            }

            protected override void DataPortal_Execute()
            {
                _exists = EquipmentDataAccess.Exists(_ownerHandle, _vehicleHandle, _providerId);
            }
        }

        [Serializable]
        private class DeleteCommand : InjectableCommandBase
        {
            #region Injected dependencies

            // ReSharper disable MemberCanBePrivate.Local
            public IEquipmentDataAccess EquipmentDataAccess { get; set; }
            // ReSharper restore MemberCanBePrivate.Local

            #endregion

            public static void Execute(EquipmentList listToDelete)
            {
                DataPortal.Execute(new DeleteCommand(listToDelete));
            }

            private readonly EquipmentList _listToDelete;

            private DeleteCommand(EquipmentList listToDelete)
            {
                _listToDelete = listToDelete;
            }

            protected override void DataPortal_Execute()
            {
                IEquipmentDataAccess accessor = EquipmentDataAccess;

                using (ITransaction transaction = accessor.BeginTransaction())
                {
                    _listToDelete.Delete(accessor, transaction);

                    transaction.Commit();
                }
            }
        }

        [Serializable]
        private class ReplaceCommand : InjectableCommandBase
        {
            #region Injected dependencies

// ReSharper disable MemberCanBePrivate.Local
            public IEquipmentDataAccess EquipmentDataAccess { get; set; }
// ReSharper restore MemberCanBePrivate.Local

            #endregion

            public static void Execute(EquipmentList original, EquipmentList replacement)
            {
                DataPortal.Execute(new ReplaceCommand(original, replacement));
            }

            private readonly EquipmentList _original;
            private readonly EquipmentList _replacement;

            private ReplaceCommand(EquipmentList original, EquipmentList replacement)
            {
                _original = original;
                _replacement = replacement;
            }

            protected override void DataPortal_Execute()
            {
                IEquipmentDataAccess accessor = EquipmentDataAccess;

                using (ITransaction transaction = accessor.BeginTransaction())
                {
                    _original.Delete(accessor, transaction);

                    _replacement.Update(accessor, transaction);

                    transaction.Commit();
                }
            }
        }

        protected virtual void DataPortal_Create(CreateCriteria criteria)
        {
            _ownerHandle = criteria.OwnerHandle;

            _vehicleHandle = criteria.VehicleHandle;

            Create(criteria.Items);
        }

        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            EquipmentListTO list = EquipmentDataAccess.Fetch(
                criteria.OwnerHandle,
                criteria.VehicleHandle,
                criteria.ProviderId);

            _ownerHandle = criteria.OwnerHandle;

            _vehicleHandle = criteria.VehicleHandle;

            _id = list.Id;

            Fetch(list);
        }

        protected override void DataPortal_Update()
        {
            IEquipmentDataAccess accessor = EquipmentDataAccess;

            using (ITransaction transaction = accessor.BeginTransaction())
            {
                Update(accessor, transaction);

                transaction.Commit();
            }
        }

        private void Update(IEquipmentDataAccess accessor, ITransaction transaction)
        {
            RaiseListChangedEvents = false;

            try
            {
                if (_id == 0)
                {
                    EquipmentSourceTO source = new EquipmentSourceTO();
                    source.Identity = _source.Identity;
                    source.EquipmentProviderId = _source.ProviderId;                    

                    _id = EquipmentDataAccess.Create(
                        transaction,
                        OwnerHandle,
                        VehicleHandle,
                        source);
                }

                foreach (Equipment item in DeletedList)
                {
                    item.DeleteSelf(accessor, transaction, _id);
                }

                DeletedList.Clear();

                for (int i = 0, l = Count; i < l; i++)
                {
                    Equipment item = this[i];

                    if (item.IsNew)
                    {
                        item.Insert(accessor, transaction, _id, i);
                    }
                    else
                    {
                        item.Update(accessor, transaction, _id, i);
                    }
                }
            }
            finally
            {
                RaiseListChangedEvents = true;
            }
        }

        private void Delete(IEquipmentDataAccess accessor, ITransaction transaction)
        {
            Clear();

            Update(accessor, transaction);

            accessor.Delete(transaction, _id);
        }

        private void Create(EquipmentInfoListTO items)
        {
            _source = EquipmentSource.GetEquipmentSource(items.Source);

            RaiseListChangedEvents = false;

            foreach (EquipmentInfoTO item in items)
            {
                Add(Equipment.NewEquipment(item.Name, item.Value, true));
            }

            RaiseListChangedEvents = true;
        }

        private void Fetch(EquipmentListTO list)
        {
            _source = EquipmentSource.GetEquipmentSource(list.Source);

            RaiseListChangedEvents = false;

            foreach (EquipmentTO item in list)
            {
                Add(Equipment.GetEquipment(item));
            }

            RaiseListChangedEvents = true;
        }
        #endregion
    }
}