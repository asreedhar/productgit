using System;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class EquipmentProviderAssignment : BusinessBase<EquipmentProviderAssignment>
    {
        #region Business Methods

        private int _equipmentProviderId;
        private bool _isDefault;
        private byte[] _version;

        public int EquipmentProviderId
        {
            get { return _equipmentProviderId; }
        }

        public bool IsDefault
        {
            get
            {
                CanReadProperty("IsDefault", true);

                return _isDefault;
            }
            set
            {
                CanWriteProperty("IsDefault", true);

                if (IsDefault != value)
                {
                    _isDefault = value;

                    PropertyHasChanged("IsDefault");
                }
            }
        }

        protected override object GetIdValue()
        {
            return _equipmentProviderId;
        }

        #endregion

        #region Factory Methods

        private EquipmentProviderAssignment()
        {
            MarkAsChild();
        }

        private EquipmentProviderAssignment(int providerId, bool isDefault) : this()
        {
            _equipmentProviderId = providerId;

            _isDefault = isDefault;
        }

        private EquipmentProviderAssignment(EquipmentProviderAssignmentTO values) : this()
        {
            Fetch(values);
        }

        internal static EquipmentProviderAssignment NewProvider(int providerId)
        {
            return new EquipmentProviderAssignment(providerId, false);
        }

        internal static EquipmentProviderAssignment GetProvider(EquipmentProviderAssignmentTO values)
        {
            return new EquipmentProviderAssignment(values);
        }

        #endregion

        #region Data Access

        private void Fetch(EquipmentProviderAssignmentTO values)
        {
            FromTransferObject(values);

            MarkOld();
        }

        internal void Insert(IEquipmentProviderAssignmentDataAccess accessor, ITransaction transaction, int id, int rank)
        {
            EquipmentProviderAssignmentTO transferObject = ToTransferObject();

            accessor.Insert(transaction, id, rank, transferObject);

            _version = transferObject.Version;

            MarkOld();
        }

        internal void Update(IEquipmentProviderAssignmentDataAccess accessor, ITransaction transaction, int id, int rank)
        {
            EquipmentProviderAssignmentTO transferObject = ToTransferObject();

            accessor.Update(transaction, id, rank, transferObject);

            _version = transferObject.Version;

            MarkOld();
        }

        internal void DeleteSelf(IEquipmentProviderAssignmentDataAccess accessor, ITransaction transaction, int id)
        {
            accessor.Delete(transaction, id, ToTransferObject());

            MarkNew();
        }

        private void FromTransferObject(EquipmentProviderAssignmentTO values)
        {
            _equipmentProviderId = values.EquipmentProviderId;

            _isDefault = values.IsDefault;

            _version = values.Version;
        }

        private EquipmentProviderAssignmentTO ToTransferObject()
        {
            EquipmentProviderAssignmentTO equipmentProviderAssignment = new EquipmentProviderAssignmentTO();
            equipmentProviderAssignment.EquipmentProviderId = _equipmentProviderId;
            equipmentProviderAssignment.IsDefault = _isDefault;
            equipmentProviderAssignment.Version = _version;
            return equipmentProviderAssignment;
        }

        #endregion
    }
}