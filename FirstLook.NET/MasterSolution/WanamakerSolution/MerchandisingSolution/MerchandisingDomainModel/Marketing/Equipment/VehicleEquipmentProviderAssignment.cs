using System;
using Csla;
using Csla.Validation;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class VehicleEquipmentProviderAssignment : InjectableBusinessBase<VehicleEquipmentProviderAssignment>
    {
        #region Business Methods

        private string _ownerHandle;
        private string _vehicleHandle;
        private int _equipmentProviderId;
        private bool _visible;
        private byte[] _version;

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public int EquipmentProviderId
        {
            get
            {
                CanReadProperty("EquipmentProviderId", true);

                return _equipmentProviderId;
            }
            set
            {
                CanWriteProperty("EquipmentProviderId", true);

                if (EquipmentProviderId != value)
                {
                    _equipmentProviderId = value;

                    PropertyHasChanged("EquipmentProviderId");
                }
            }
        }

        public bool Visible
        {
            get
            {
                CanReadProperty("Visible", true);

                return _visible;
            }
            set
            {
                CanWriteProperty("Visible", true);

                if (Visible != value)
                {
                    _visible = value;

                    PropertyHasChanged("Visible");
                }
            }
        }

        protected override object GetIdValue()
        {
            return VehicleHandle;
        }

        #endregion

        #region Validation

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(
                CommonRules.IntegerMinValue,
                new CommonRules.IntegerMinValueRuleArgs("EquipmentProviderId", 1));
        }

        #endregion

        #region Injected dependencies

        public IEquipmentProviderAssignmentDataAccess EquipmentProviderAssignmentDataAccess { get; set; }

        #endregion

        #region Factory Methods

        private VehicleEquipmentProviderAssignment()
        {
            /* force use of factory methods */
        }

        private VehicleEquipmentProviderAssignment(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;

            _vehicleHandle = vehicleHandle;

            _visible = true;
        }

        public static bool Exists(string ownerHandle, string vehicleHandle)
        {
            return ExistsCommand.Execute(ownerHandle, vehicleHandle);
        }

        public static VehicleEquipmentProviderAssignment GetVehicleProviderAssignment(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<VehicleEquipmentProviderAssignment>(new Criteria(ownerHandle, vehicleHandle));
        }

        public static VehicleEquipmentProviderAssignment CreateVehicleProviderAssignment(string ownerHandle, string vehicleHandle)
        {
            return new VehicleEquipmentProviderAssignment(ownerHandle, vehicleHandle);
        }

        #endregion

        #region Data Access

        [Serializable]
        protected class Criteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }
        }

        protected class ExistsCommand : InjectableCommandBase
        {
            #region Injected dependencies

            public IEquipmentProviderAssignmentDataAccess EquipmentProviderAssignmentDataAccess { get; set; }

            #endregion

            public static bool Execute(string ownerHandle, string vehicleHandle)
            {
                ExistsCommand command = new ExistsCommand(ownerHandle, vehicleHandle);
                DataPortal.Execute(command);
                return command._exists;
            }

            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private bool _exists;

            private ExistsCommand(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            protected override void DataPortal_Execute()
            {
                _exists = EquipmentProviderAssignmentDataAccess.ExistsVehicleEquipmentProviderAssignment(_ownerHandle, _vehicleHandle);
            }
        }

        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            _ownerHandle = criteria.OwnerHandle;

            _vehicleHandle = criteria.VehicleHandle;

            Fetch(EquipmentProviderAssignmentDataAccess.FetchVehicleEquipmentProviderAssignment(criteria.OwnerHandle, criteria.VehicleHandle));
        }

        protected override void DataPortal_Insert()
        {
            VehicleEquipmentProviderAssignmentTO transferObject = ToTransferObject();

            EquipmentProviderAssignmentDataAccess.Insert(OwnerHandle, VehicleHandle, transferObject);

            _version = transferObject.Version;

            MarkOld();
        }

        protected override void DataPortal_Update()
        {
            VehicleEquipmentProviderAssignmentTO transferObject = ToTransferObject();

            EquipmentProviderAssignmentDataAccess.Update(OwnerHandle, VehicleHandle, transferObject);

            _version = transferObject.Version;

            MarkOld();
        }

        protected override void DataPortal_DeleteSelf()
        {
            EquipmentProviderAssignmentDataAccess.Delete(OwnerHandle, VehicleHandle, ToTransferObject());

            MarkNew();
        }

        private void Fetch(VehicleEquipmentProviderAssignmentTO assignment)
        {
            FromTransferObject(assignment);
        }

        private void FromTransferObject(VehicleEquipmentProviderAssignmentTO assignment)
        {
            _equipmentProviderId = assignment.EquipmentProviderId;
            _visible = assignment.Visible;
            _version = assignment.Version;
        }

        private VehicleEquipmentProviderAssignmentTO ToTransferObject()
        {
            VehicleEquipmentProviderAssignmentTO assignment = new VehicleEquipmentProviderAssignmentTO();
            assignment.EquipmentProviderId = _equipmentProviderId;
            assignment.Visible = _visible;
            assignment.Version = _version;
            return assignment;
        }
        #endregion
    }
}