﻿
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    class DealerPeakEquipmentProvider : AbstractBookEquipmentProvider, ILotProvider
    {
        public override Code Id
        {
            get { return Code.DealerPeak; }
        }

        public LotProviderCode LotProviderCode
        {
            get { return LotProviderCode.DealerPeak; }
        }
    }
}
