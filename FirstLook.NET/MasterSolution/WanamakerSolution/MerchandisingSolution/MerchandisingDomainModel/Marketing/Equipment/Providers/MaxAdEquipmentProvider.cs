﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    public class MaxAdEquipmentProvider : AbstractBookEquipmentProvider
    {
        public override Code Id
        {
            get { return Code.MaxAd; }
        }

        public static bool UseMaxAd(string ownerHandle)
        {
            Owner owner = Owner.GetOwner(ownerHandle);

            var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);
            return businessUnit.HasDealerUpgrade(Upgrade.Merchandising);
        }

        protected override EquipmentSourceTO GetCurrentSource(string ownerHandle, string vehicleHandle)
        {
            ValidateVehicleHandle(vehicleHandle);

            MerchandisingContext context = MerchandisingContext.FromPricing(ownerHandle, vehicleHandle);
            EquipmentCommand equipmentCommand = new EquipmentCommand(context.BusinessUnitId, context.InventoryId);
            AbstractCommand.DoRun(equipmentCommand);

            EquipmentSourceTO source = new EquipmentSourceTO();
            source.EquipmentProviderId = (int)Id;
            source.Identity = -1;
            if (equipmentCommand.HasEquipment)
                source.Identity = equipmentCommand.ReleaseTime.Ticks;
            
            return source;
        }

        protected override EquipmentInfoListTO GetEquipmentList(string ownerHandle, string vehicleHandle)
        {
            ValidateVehicleHandle(vehicleHandle);

            MerchandisingContext context = MerchandisingContext.FromPricing(ownerHandle, vehicleHandle);
            EquipmentCommand equipmentCommand = new EquipmentCommand(context.BusinessUnitId, context.InventoryId);
            AbstractCommand.DoRun(equipmentCommand);

            EquipmentSourceTO source = new EquipmentSourceTO();
            source.EquipmentProviderId = (int)Id;
            source.Identity = -1;
            if (equipmentCommand.HasEquipment)
                source.Identity = equipmentCommand.ReleaseTime.Ticks;
            
            EquipmentInfoListTO list = new EquipmentInfoListTO();
            list.Source = source;

            foreach (var equipmentName in equipmentCommand.Equipment)
                list.Add(new EquipmentInfoTO() { Name = equipmentName, Value = null });
            
            return list;
        }

        private class EquipmentCommand : AbstractCommand
        {
            private const int EquipmentLimit = 24;
            private static Regex CommaSplitRegex = new Regex(@",\s*");

            private int _businessUnitID;
            private int _inventoryID;

            public string[] Equipment { get; private set; }
            public DateTime ReleaseTime { get; private set; }

            public EquipmentCommand(int businessUnitID, int inventoryID)
            {
                _businessUnitID = businessUnitID;
                _inventoryID = inventoryID;

                Equipment = new string[0];
                ReleaseTime = DateTime.MinValue;
            }

            public bool HasEquipment
            {
                get
                {
                    return Equipment.Length > 0;
                }
            }

            protected override void Run()
            {
                using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.Merchandising))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "merchandising.EquipmentItems#Fetch";
                        command.CommandType = CommandType.StoredProcedure;

                        SimpleQuery.AddWithValue(command, "BusinessUnitID", _businessUnitID, DbType.Int32);
                        SimpleQuery.AddWithValue(command, "InventoryID", _inventoryID, DbType.Int32);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                string equipment = reader.GetString(reader.GetOrdinal("EquipmentList"));
                                Equipment = CommaSplitRegex.Split(equipment).Take(EquipmentLimit).ToArray();
                                ReleaseTime = reader.GetDateTime(reader.GetOrdinal("ScheduledReleaseTime"));
                            }
                        }
                    }
                }
            }
        }

    }
}
