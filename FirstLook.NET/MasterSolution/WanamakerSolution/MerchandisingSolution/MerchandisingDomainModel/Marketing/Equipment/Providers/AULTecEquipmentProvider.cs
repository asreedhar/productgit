﻿
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    class AULTecEquipmentProvider : AbstractBookEquipmentProvider, ILotProvider
    {
        public override Code Id
        {
            get { return Code.AULTec; }
        }

        public LotProviderCode LotProviderCode
        {
            get { return LotProviderCode.AULTec; }
        }
    }
}
