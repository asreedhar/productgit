﻿
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    class CDMDataEquipmentProvider : AbstractBookEquipmentProvider, ILotProvider
    {
        public override Code Id
        {
            get { return Code.CDMData; }
        }

        public LotProviderCode LotProviderCode
        {
            get { return LotProviderCode.CDMData; }
        }
    }
}
