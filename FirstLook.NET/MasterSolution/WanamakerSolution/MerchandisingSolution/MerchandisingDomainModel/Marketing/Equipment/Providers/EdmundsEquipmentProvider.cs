﻿
namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    class EdmundsEquipmentProvider : AbstractBookEquipmentProvider
    {
        public override Code Id
        {
            get { return Code.Edmunds; }
        }
    }
}
