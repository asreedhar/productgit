namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    public class BlackBookEquipmentProvider : AbstractBookEquipmentProvider
    {
        public override Code Id
        {
            get { return Code.BlackBook; }
        }
    }
}