namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    public class KelleyBlueBookEquipmentProvider : AbstractBookEquipmentProvider
    {
        public override Code Id
        {
            get { return Code.KelleyBlueBook; }
        }
    }
}