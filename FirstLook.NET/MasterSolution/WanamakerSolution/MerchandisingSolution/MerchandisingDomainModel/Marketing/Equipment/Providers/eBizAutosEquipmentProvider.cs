﻿
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
// ReSharper disable InconsistentNaming
    class eBizAutosEquipmentProvider : AbstractBookEquipmentProvider, ILotProvider
// ReSharper restore InconsistentNaming
    {
        public override Code Id
        {
            get { return Code.eBizAutos; }
        }

        public LotProviderCode LotProviderCode
        {
            get { return LotProviderCode.eBizAutos; }
        }
    }
}
