using System;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    public abstract class AbstractBookEquipmentProvider : EquipmentProvider
    {
        private readonly EquipmentInfoListTO _list = new EquipmentInfoListTO();

        protected virtual EquipmentInfoListTO List
        {
            get { return _list; }
        }

        protected override EquipmentSourceTO GetCurrentSource(string ownerHandle, string vehicleHandle)
        {
            ValidateVehicleHandle(vehicleHandle);

            return EquipmentProviderDataAccess.GetCurrentSource(ownerHandle, vehicleHandle, (int) Id);
        }

        protected override EquipmentInfoListTO GetEquipmentList(string ownerHandle, string vehicleHandle)
        {
            ValidateVehicleHandle(vehicleHandle);

            return EquipmentProviderDataAccess.GetEquipmentList(ownerHandle, vehicleHandle, (int) Id);
        }

        protected virtual void ValidateVehicleHandle(string vehicleHandle)
        {
            VehicleType type = VehicleTypeCommand.Execute(vehicleHandle);

            if (type != VehicleType.Inventory && type != VehicleType.Appraisal)
            {
                throw new Exception("Invalid Vehicle Type");
            }
        }

        protected static IEquipmentProviderDataAccess EquipmentProviderDataAccess
        {
            get { return Registry.Resolve<IEquipmentProviderDataAccess>(); }
        }
    }
}