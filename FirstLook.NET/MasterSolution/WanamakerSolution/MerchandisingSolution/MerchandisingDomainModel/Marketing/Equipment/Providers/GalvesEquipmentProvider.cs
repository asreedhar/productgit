namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    public class GalvesEquipmentProvider : AbstractBookEquipmentProvider
    {
        public override Code Id
        {
            get { return Code.Galves; }
        }
    }
}