﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{

    [Serializable]
    public enum LotProviderCode
    {
        GetAuto = 2,
        // ReSharper disable InconsistentNaming
        eBizAutos = 5,
        // ReSharper restore InconsistentNaming
        HomeNet = 6,
        CDMData = 7,
        AULTec = 8,
        DealerPeak = 9
    }
}