namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    public class NadaEquipmentProvider : AbstractBookEquipmentProvider
    {
        public override Code Id
        {
            get { return Code.Nada; }
        }
    }
}