﻿
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    class GetAutoEquipmentProvider : AbstractBookEquipmentProvider, ILotProvider
    {
        public override Code Id
        {
            get { return Code.GetAuto; }
        }

        public LotProviderCode LotProviderCode
        {
            get { return LotProviderCode.GetAuto; }
        }
    }
}
