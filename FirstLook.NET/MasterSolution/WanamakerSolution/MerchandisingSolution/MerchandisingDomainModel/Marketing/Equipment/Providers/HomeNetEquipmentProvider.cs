﻿
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers
{
    class HomeNetEquipmentProvider : AbstractBookEquipmentProvider, ILotProvider
    {
        public override Code Id
        {
            get { return Code.HomeNet; }
        }

        public LotProviderCode LotProviderCode
        {
            get { return LotProviderCode.HomeNet; }
        }
    }
}
