using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FirstLook.Common.Core.DTO;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.DomainModel.Presentation;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    /// <summary>
    /// The Equipment subsystem exposes a fine-grained, decentralized API.  This facade centralizes the API and employs
    /// some data transfer objects for clients to interact with.
    /// </summary>
    public class EquipmentFacade
    {
        string OwnerHandle { get; set; }
        string VehicleHandle { get; set; }

        private int? _dealerId;
        private bool? _hasKbbAsBook;
        private bool? _hasKbbConsumerTool;
        private bool? _isValidDealer;

        public EquipmentFacade(string ownerHandle, string vehicleHandle)
        {
            OwnerHandle = ownerHandle;
            VehicleHandle = vehicleHandle;
        }

        /// <summary>
        /// Which providers are associated with a dealer?
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EquipmentProviderDTO> GetProviders()
        {
            var dataSource = new EquipmentProviderAssignmentList.DataSource();
            var providerInfo = dataSource.Select(OwnerHandle);

            IList<EquipmentProviderDTO> dtos = new List<EquipmentProviderDTO>();

            foreach(var info in providerInfo )
            {
                var dto = new EquipmentProviderDTO {Name = info.Name, EquipmentProviderID = info.Id};
                dtos.Add(dto);
            }

            return dtos;
        }
    
        /// <summary>
        ///  Get equipment dtos for an owner/vehicle from a specified provider.
        /// </summary>
        /// <param name="equipmentProviderId"></param>
        /// <returns></returns>
        public IEnumerable<EquipmentDTO> GetEquipment( int equipmentProviderId )
        {
            var dataSource = new EquipmentList.DataSource();
            var equipmentList = dataSource.Select(OwnerHandle, VehicleHandle, equipmentProviderId);

            if (equipmentList != null)
            {
                IList<EquipmentDTO> dtos = new List<EquipmentDTO>();

                for( int i=0; i < equipmentList.Count; i++ )
                {
                    Equipment eq = equipmentList[i];
                    var dto = new EquipmentDTO {EquipmentID = eq.Id, Name = eq.Name, Position = i, Selected = eq.Selected};
                    dtos.Add( dto );
                }
                return dtos;
            }

            return new List<EquipmentDTO>();
        }

        /// <summary>
        /// Return the provider id of the 'selected' provider.
        /// </summary>
        /// <returns></returns>
        public int? GetSelectedProviderId()
        {
            // if a selection has been made for this vehicle, use it.  Otherwise use the default provider.
            if (VehicleEquipmentProviderAssignment.Exists(OwnerHandle, VehicleHandle))
            {
                VehicleEquipmentProviderAssignment assignment = VehicleEquipmentProviderAssignment.
                    GetVehicleProviderAssignment(
                        OwnerHandle, VehicleHandle);

                return assignment.EquipmentProviderId;
            }

            return GetDefaultProviderId();
        }

        private int? GetDefaultProviderId()
        {
            var providers = EquipmentProviderAssignmentList.ReconcileEquipmentProviders(OwnerHandle);

            if (providers != null)
            {
                // Get the default provider.
                var provider = providers.Where(p => p.IsDefault).FirstOrDefault();

                if( provider != null )
                {
                    return provider.EquipmentProviderId;
                }                 
            }

            // A provider id cannot be determined.
            return null;
        }

        /// <summary>
        /// Update the position (aka rank) and selected/available property of individual equipment items.
        /// 
        /// Mark equipment (specified by id) as "selected" or "available".  The "position" of selected equipment is 
        /// implied by the position in the ""selected" enumeration. 
        /// 
        /// All "available" equipment (e.g. not selected) will have an unspecified position.
        /// 
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="selected"></param>
        /// <param name="available"></param>
        public void SaveSelected(int providerId, IEnumerable<int> selected, IEnumerable<int> available)
        {
            // Get the equipment for this provider.
            var equipmentList = GetEquipment(providerId);

            // We should have the same counts.
            if( selected.Count() + available.Count() != equipmentList.Count())
            {
                throw new ApplicationException("Count mismatch");
            }

            // build some "table rows" 
            var tableRows = new List<ITableRow>();
            
            // Process the "selected" equipment.
            foreach( var id in selected )
            {
                // Find the equipment.
                var equipment = GetEquipment(equipmentList, id);
                var row = CreateTableRow(equipment.EquipmentID, equipment.Name, true);
                tableRows.Add( row );
            }
            
            // Now process the "available" equipment.
            foreach (var id in available)
            {
                // Find the equipment.
                var equipment = GetEquipment(equipmentList, id);
                var row = CreateTableRow(equipment.EquipmentID, equipment.Name, false);
                tableRows.Add(row);
            }            

            // Save.
            var dataSource = new EquipmentList.DataSource();
            dataSource.Update(OwnerHandle, VehicleHandle, providerId, tableRows );

            var assignment = GetAssignment(providerId);
            assignment.Save();

            
        }

        /// <summary>
        /// Get/Set whether the equipment module is displayed
        /// </summary>
        public bool IsDisplayed
        {
            get
            {
                // The module is really only hidden if the vehicle assignment specifies it's not visible.
                int? providerId = GetSelectedProviderId();
                return providerId.HasValue && GetAssignment(providerId.Value).Visible;
            }
            set
            {
                int? providerId = GetSelectedProviderId();
                if (!providerId.HasValue) return;

                var assignment = GetAssignment(providerId.Value);
                assignment.Visible = value;
                assignment.Save();
            }
        }

        /// <summary>
        /// Lose all changes.  Reset to defaults.  
        /// </summary>
        public void ResetToDefaults()
        {
            if (!VehicleEquipmentProviderAssignment.Exists(OwnerHandle, VehicleHandle)) return;
            
            // remove vehicle specific assignment
            VehicleEquipmentProviderAssignment assignment = VehicleEquipmentProviderAssignment.GetVehicleProviderAssignment(
                OwnerHandle, VehicleHandle);

            assignment.Delete();
            assignment.Save();

            int? defaultProviderId = GetDefaultProviderId();

            if (defaultProviderId.HasValue)
            {
                // select all items
                EquipmentList list = EquipmentList.ReconcileEquipmentList(OwnerHandle, VehicleHandle, defaultProviderId.Value);

                if (list != null && list.Count > 0)
                {
                    foreach (Equipment item in list)
                    {
                        item.Selected = true;
                    }

                    list.Save();
                }
            }
        }


        #region Private

        /// <summary>
        /// Get the provider assignment for the specified provider id for this owner and vehicle.
        /// If no assignment exists (and only one can), a new one is created and is assigned to the 
        /// specified provider id.
        /// </summary>
        /// <param name="providerId"></param>
        /// <returns></returns>
        private VehicleEquipmentProviderAssignment GetAssignment( int providerId )
        {
            VehicleEquipmentProviderAssignment assignment;

            if (VehicleEquipmentProviderAssignment.Exists(OwnerHandle, VehicleHandle))
            {
                assignment = VehicleEquipmentProviderAssignment.GetVehicleProviderAssignment(
                                                                OwnerHandle, VehicleHandle);
            }
            else
            {
                assignment = VehicleEquipmentProviderAssignment.CreateVehicleProviderAssignment(
                    OwnerHandle, VehicleHandle);    
            }
            
            assignment.EquipmentProviderId = providerId;

            return assignment;
        }

        private static TableRow CreateTableRow(int id, string name, bool selected)
        {
            return new TableRow
                       {
                           Enabled = true,
                           HasSelectedChanged = true,
                           Id = id,
                           Name = name,
                           Selected = selected
                       };

        }

        private EquipmentDTO GetEquipment(IEnumerable<EquipmentDTO> equipmentList, int id)
        {
            return equipmentList.Where(eq => eq.EquipmentID == id).Single();
        }

        /// <summary>
        /// A private class used to interact with the equipment API, which expects to be passed objects implementing ITableRow
        /// </summary>
        private class TableRow : ITableRow 
        {
            public int Id { get; set;}
            public string Name { get; set; }
            public bool Enabled { get; set; }
            public bool Selected { get; set; }
            public bool HasSelectedChanged { get; set; }
        }

        /// <summary>
        /// Get the 'bookout' url for the selected equipment provider.
        /// </summary>
        /// <returns></returns>
        public string GetProviderUrl(EquipmentProvider.Code provider)
        {
            switch (provider)
            {
                case EquipmentProvider.Code.BlackBook:
                    return EStockCardUrl;
                case EquipmentProvider.Code.Nada:
                    return EStockCardUrl;
                case EquipmentProvider.Code.KelleyBlueBook:
                    return HasKbbAsBook ? EStockCardUrl : KbbUrl;
                case EquipmentProvider.Code.Galves:
                    return EStockCardUrl;
                case EquipmentProvider.Code.Edmunds:
                    return EdmundsTmvUrl;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets a value indicating whether or not the dealer has Kbb as a guidebook.
        /// </summary>
        public bool HasKbbAsBook
        {
            get
            {
                if (!_hasKbbAsBook.HasValue && IsValidDealer)
                {
                        _hasKbbAsBook = KelleyBlueBookClient.HasKbbAsBook(DealerId);

                    return _hasKbbAsBook.GetValueOrDefault();

                }
                return _hasKbbAsBook.GetValueOrDefault(false);
            }
        }

        /// <summary>
        /// Gets the estockcard url
        /// </summary>
        public string EStockCardUrl
        {
            get
            {
                var inventory = Inventory.GetInventory(OwnerHandle, VehicleHandle);

                if (!string.IsNullOrEmpty(inventory.StockNumber))
                {
                    return string.Format(CultureInfo.InvariantCulture,
                                         "/IMT/EStock.go?stockNumberOrVin={0}&isPopup=true",
                                         inventory.StockNumber);
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Kbb Url
        /// </summary>
        public string KbbUrl
        {
            get
            {
                string kbbUrl = null;

                VehicleType vehType = VehicleTypeCommand.Execute(VehicleHandle);

                //PER VINCE MEASE:
                //
                //if dealer does not have KBB as a Guide Book and does not have KBB Stand Alone -> link to KBB.com
                //else
                //      if Inventory:
                //          if dealer has KBB as a Guide Book and has KBB Stand Alone -> link to estock card
                //          else if dealer has KBB Stand Alone -> link to the KBB Consumer Tool

                if (!HasKbbAsBook && !HasKbbConsumerTool)
                {
                    kbbUrl = "http://www.kbb.com/";
                }
                else
                {
                    if (vehType == VehicleType.Inventory)
                    {
                        if (HasKbbAsBook)
                        {
                            kbbUrl = EStockCardUrl;
                        }
                        else
                        {
                            string vin = VehicleHelper.GetVin(OwnerHandle, VehicleHandle);
                            int? mileage = null;

                            if (vehType == VehicleType.Inventory)
                            {
                                Inventory inv = Inventory.GetInventory(OwnerHandle, VehicleHandle);
                                mileage = inv.Mileage;
                            }
                            if (mileage.HasValue)
                            {
                                kbbUrl = String.Format(CultureInfo.InvariantCulture,
                                                       "/IMT/KbbConsumerValueLandingAction.go?dealerId={0}&vin={1}&mileage={2}",
                                                       DealerId, vin, mileage);
                            }
                        }
                    }
                }

                return kbbUrl;
            }
        }

        /// <summary>
        /// Gets the JDPower Url
        /// </summary>
        public string JDPowerUrl
        {
            get
            {
                var inventory = Inventory.GetInventory(OwnerHandle, VehicleHandle);
                var vehicleCatalogId = inventory.DecodingClassification.Id;
                var modelYear = inventory.DecodingClassification.ModelYear;
                return string.Format(
                    CultureInfo.InvariantCulture,
                    "/pricing/Pages/JDPower/JDPower.aspx?token=DEALER_SYSTEM_COMPONENT&vehicleCatalogId={0}&modelYear={1}",
                    vehicleCatalogId, modelYear);
            }
        }

        /// <summary>
        /// Gets the Edmunds TMV url
        /// </summary>
        public string EdmundsTmvUrl
        {
            get
            {
                return string.Format("/pricing/Pages/Edmunds/TmvCalculator.aspx?oh={0}&vh={1}", OwnerHandle,
                                     VehicleHandle);
            }
        }


        /// <summary>
        /// Gets a value indicating whether or not the dealer has the Kbb stand alone tool.
        /// </summary>
        public bool HasKbbConsumerTool
        {
            get
            {
                if (!_hasKbbConsumerTool.HasValue && IsValidDealer)
                {
                    _hasKbbConsumerTool = KelleyBlueBookClient.HasKbbConsumerTool(DealerId);
                }

                return _hasKbbConsumerTool.GetValueOrDefault(false);
            }
        }

        private int DealerId
        {
            get
            {
                if (!_dealerId.HasValue)
                {
                    _dealerId = Identity.GetDealerId(OwnerHandle);
                }

                return _dealerId.GetValueOrDefault();
            }
        }

        private bool IsValidDealer
        {
            get
            {
                if (! _isValidDealer.HasValue)
                    _isValidDealer = Identity.IsDealer(OwnerHandle);

                return _isValidDealer.GetValueOrDefault(false);
            }
        }


        #endregion
    }

   
    
    //
    // Transfer objects to be used by clients.
    //

    [Serializable]
    public class EquipmentDTO : ISurrogateKey 
    {
        public int EquipmentID { get; internal set; }
        public string Name { get; internal set; }
        public bool Selected { get; set; }
        public int Position { get; set; }

        public string Key
        {
            get { return EquipmentID.ToString(); }
        }
    }

    [Serializable]
    public class EquipmentProviderDTO
    {
        public int EquipmentProviderID { get; internal set; }
        public string Name { get; internal set; }
    }


}
