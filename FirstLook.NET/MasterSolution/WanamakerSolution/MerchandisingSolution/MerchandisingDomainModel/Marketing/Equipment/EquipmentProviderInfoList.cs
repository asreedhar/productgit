using System;
using System.Collections.Generic;
using Csla;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class EquipmentProviderInfoList : InjectableReadOnlyListBase<EquipmentProviderInfoList, EquipmentProviderInfo>
    {
        #region Factory Methods

        public static EquipmentProviderInfoList GetEquipmentProviders()
        {
            return DataPortal.Fetch<EquipmentProviderInfoList>(new EmptyCriteria());
        }

        public static EquipmentProviderInfoList GetEquipmentProviders(string ownerHandle)
        {
            return DataPortal.Fetch<EquipmentProviderInfoList>(new OwnerCriteria(ownerHandle));
        }

        #endregion

        #region Injected dependencies

        public IEquipmentProviderAssignmentDataAccess EquipmentProviderAssignmentAccessor { get; set; }

        #endregion

        #region Data Access

        [Serializable]
        protected class EmptyCriteria
        {
        }

        [Serializable]
        protected class OwnerCriteria
        {
            private readonly string _ownerHandle;

            public OwnerCriteria(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }
        }

        protected virtual void DataPortal_Fetch(EmptyCriteria criteria)
        {
            IEquipmentProviderAssignmentDataAccess accessor = EquipmentProviderAssignmentAccessor;

            Fetch(accessor.FetchEquipmentProviderInfo());
        }

        protected virtual void DataPortal_Fetch(OwnerCriteria criteria)
        {
            IEquipmentProviderAssignmentDataAccess accessor = EquipmentProviderAssignmentAccessor;

            Fetch(accessor.FetchEquipmentProviderInfo(criteria.OwnerHandle));
        }

        private void Fetch(IEnumerable<EquipmentProviderInfoTO> items)
        {
            IsReadOnly = false;

            foreach (EquipmentProviderInfoTO item in items)
            {
                Add(EquipmentProviderInfo.GetProviderInfo(item));
            }

            IsReadOnly = true;
        }
        #endregion
    }
}