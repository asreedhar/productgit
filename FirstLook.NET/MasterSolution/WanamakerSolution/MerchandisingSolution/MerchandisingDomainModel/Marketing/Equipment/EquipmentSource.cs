using System;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment
{
    [Serializable]
    public class EquipmentSource
    {
        #region Business Methods

        private readonly int _providerId;
        private readonly long _identity;

        public int ProviderId
        {
            get { return _providerId; }
        }

        public long Identity
        {
            get { return _identity; }
        }

        #endregion

        #region Factory Methods

        private EquipmentSource(EquipmentSourceTO item)
        {
            _providerId = item.EquipmentProviderId;
            _identity = item.Identity;
        }

        internal static EquipmentSource GetEquipmentSource(EquipmentSourceTO item)
        {
            return new EquipmentSource(item);
        }

        #endregion
    }
}