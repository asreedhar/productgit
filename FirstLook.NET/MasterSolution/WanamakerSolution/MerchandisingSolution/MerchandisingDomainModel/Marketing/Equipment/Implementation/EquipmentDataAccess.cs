using System.Data;
using System.Threading;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Implementation
{
    public class EquipmentDataAccess : IEquipmentDataAccess
    {
        private static string CurrentUser
        {
            get { return Thread.CurrentPrincipal.Identity.Name; }
        }

        public ITransaction BeginTransaction()
        {
            return new DbTransaction(FirstLook.Pricing.DomainModel.Database.MarketingDatabase);
        }

        public int Create(ITransaction transaction, string ownerHandle, string vehicleHandle, EquipmentSourceTO source)
        {
            DbTransaction trxn = (DbTransaction) transaction;
            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.EquipmentList#Create";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                SimpleQuery.AddWithValue(command, "EquipmentProviderID", source.EquipmentProviderId, DbType.Int32);
                SimpleQuery.AddWithValue(command, "IdentityValue", source.Identity, DbType.Int64);
                SimpleQuery.AddWithValue(command, "InsertUser", CurrentUser, DbType.String);

                IDataParameter newId = SimpleQuery.AddOutParameter(command, "Id", false, DbType.Int32);

                command.ExecuteNonQuery();

                return (int) newId.Value;
            }
        }

        public bool Exists(string ownerHandle, string vehicleHandle, int equipmentProviderId)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentList#Exists";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "EquipmentProviderID", equipmentProviderId, DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("Exists"));
                        }

                        return false;
                    }
                }
            }
        }

        public EquipmentListTO Fetch(string ownerHandle, string vehicleHandle, int equipmentProviderId)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentList#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "EquipmentProviderID", equipmentProviderId, DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        EquipmentListTO list = new EquipmentListTO();

                        if (reader.Read())
                        {
                            list.Id = reader.GetInt32(reader.GetOrdinal("Id"));

                            EquipmentSourceTO source = new EquipmentSourceTO();

                            source.Identity = reader.GetInt64(reader.GetOrdinal("IdentityValue"));

                            source.EquipmentProviderId = reader.GetInt32(reader.GetOrdinal("EquipmentProviderId"));

                            list.Source = source;
                        }
                        else
                        {
                            throw new DataException("Expected List result set!");
                        }

                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                EquipmentTO item = new EquipmentTO();

                                item.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                                item.Name = reader.GetString(reader.GetOrdinal("Name"));
                                item.Rank = reader.GetInt32(reader.GetOrdinal("Position"));
                                item.Selected = reader.GetBoolean(reader.GetOrdinal("Selected"));

                                if (!reader.IsDBNull(reader.GetOrdinal("Value")))
                                {
                                    item.Value = reader.GetInt32(reader.GetOrdinal("Value"));
                                }

                                item.Version = (byte[]) reader.GetValue(reader.GetOrdinal("Version"));

                                list.Add(item);
                            }
                        }
                        else
                        {
                            throw new DataException("Expected a second result set!");
                        }

                        return list;
                    }
                }
            }
        }

        public void Insert(ITransaction transaction, int equipmentListId, EquipmentTO equipment)
        {
            DbTransaction trxn = (DbTransaction) transaction;

            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.Equipment#Insert";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "EquipmentListID", equipmentListId, DbType.Int32);
                SimpleQuery.AddWithValue(command, "Name", equipment.Name, DbType.String);

                if (equipment.Value.HasValue)
                {
                    SimpleQuery.AddWithValue(command, "Value", equipment.Value, DbType.Int32);
                }
                else
                {
                    SimpleQuery.AddNullValue(command, "Value", DbType.Int32);
                }

                SimpleQuery.AddWithValue(command, "Selected", equipment.Selected, DbType.Boolean);
                SimpleQuery.AddWithValue(command, "Position", equipment.Rank, DbType.Int32);
                SimpleQuery.AddWithValue(command, "InsertUser", CurrentUser, DbType.String);

                IDataParameter newId = SimpleQuery.AddOutParameter(command, "Id", false, DbType.Int32);

                IDataParameter newVersion = SimpleQuery.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                command.ExecuteNonQuery();

                equipment.Id = (int) newId.Value;

                equipment.Version = (byte[]) newVersion.Value;
            }
        }

        public void Update(ITransaction transaction, int equipmentListId, EquipmentTO equipment)
        {
            DbTransaction trxn = (DbTransaction) transaction;

            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.Equipment#Update";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "EquipmentListID", equipmentListId, DbType.Int32);
                SimpleQuery.AddWithValue(command, "EquipmentID", equipment.Id, DbType.Int32);
                SimpleQuery.AddWithValue(command, "Name", equipment.Name, DbType.String);

                if (equipment.Value.HasValue)
                {
                    SimpleQuery.AddWithValue(command, "Value", equipment.Value, DbType.Int32);
                }
                else
                {
                    SimpleQuery.AddNullValue(command, "Value", DbType.Int32);
                }

                SimpleQuery.AddWithValue(command, "Selected", equipment.Selected, DbType.Boolean);
                SimpleQuery.AddWithValue(command, "Position", equipment.Rank, DbType.Int32);
                SimpleQuery.AddWithValue(command, "UpdateUser", CurrentUser, DbType.String);
                SimpleQuery.AddArrayParameter(command, "Version", DbType.Binary, false, equipment.Version);

                IDataParameter newVersion = SimpleQuery.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                command.ExecuteNonQuery();

                equipment.Version = (byte[]) newVersion.Value;
            }
        }

        public void Delete(ITransaction transaction, int equipmentListId, EquipmentTO equipment)
        {
            DbTransaction trxn = (DbTransaction) transaction;

            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.Equipment#DeleteSelf";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "EquipmentListID", equipmentListId, DbType.Int32);
                SimpleQuery.AddWithValue(command, "EquipmentID", equipment.Id, DbType.Int32);
                SimpleQuery.AddArrayParameter(command, "Version", DbType.Binary, false, equipment.Version);

                command.ExecuteNonQuery();
            }
        }

        public void Delete(ITransaction transaction, int equipmentListId)
        {
            DbTransaction trxn = (DbTransaction) transaction;

            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.EquipmentList#DeleteSelf";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "EquipmentListID", equipmentListId, DbType.Int32);

                command.ExecuteNonQuery();
            }
        }
    }
}