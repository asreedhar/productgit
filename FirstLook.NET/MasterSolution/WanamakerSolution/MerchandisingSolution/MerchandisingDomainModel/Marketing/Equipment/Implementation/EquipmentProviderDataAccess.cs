using System;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Implementation
{
    public class EquipmentProviderDataAccess : IEquipmentProviderDataAccess
    {
        public EquipmentSourceTO GetCurrentSource(string ownerHandle, string vehicleHandle, int equipmentProviderId)
        {
            EquipmentProvider provider = EquipmentProvider.GetEquipmentProvider(equipmentProviderId);

            var lotProvider = provider as ILotProvider;

            /* TODO:
             * The lot provider data access can be broken out into a separate dataaccess class.
             */

            if (lotProvider != null)
            {
                #region Lot Provider
                // get the inventory item
                var inventory = Inventory.GetInventory(ownerHandle, vehicleHandle);

                string dealerCode = GetDealerCodeFromOwnerHandle(ownerHandle);

                using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.LotDatabase))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "Listing.getOptionListVersion";
                        command.CommandType = CommandType.StoredProcedure;

                        SimpleQuery.AddWithValue(command, "DealerCode", dealerCode, DbType.String);
                        SimpleQuery.AddWithValue(command, "VIN", inventory.Vin, DbType.String);
                        SimpleQuery.AddWithValue(command, "StockNumber", inventory.StockNumber, DbType.String);
                        SimpleQuery.AddWithValue(command, "ProviderID", lotProvider.LotProviderCode, DbType.Int32);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                EquipmentSourceTO source = new EquipmentSourceTO();

                                source.EquipmentProviderId = equipmentProviderId;

                                source.Identity = reader.GetInt16(reader.GetOrdinal("OptionVersion"));

                                return source;
                            }

                            // if we don't have a list, return null.
                            return null;
                        }
                    }
                }
                #endregion
            }

            // third party
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderList#CurrentSource";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "EquipmentProviderID", equipmentProviderId, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            EquipmentSourceTO source = new EquipmentSourceTO();

                            source.EquipmentProviderId = reader.GetInt32(reader.GetOrdinal("EquipmentProviderId"));

                            source.Identity = reader.GetInt64(reader.GetOrdinal("IdentityValue"));

                            return source;
                        }

                        // if we don't have a list, return null.
                        return null;
                    }
                }
            }
        }

        public EquipmentInfoListTO GetEquipmentList(string ownerHandle, string vehicleHandle, int equipmentProviderId)
        {
            EquipmentProvider provider = EquipmentProvider.GetEquipmentProvider(equipmentProviderId);

            var lotProvider = provider as ILotProvider;

            /* TODO:
             * The lot provider data access can be broken out into a separate dataaccess class.
             */
            if (lotProvider != null)
            {
                #region Lot Provider
                // get the inventory item
                var inventory = Inventory.GetInventory(ownerHandle, vehicleHandle);

                string dealerCode = GetDealerCodeFromOwnerHandle(ownerHandle);

                using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.LotDatabase))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "Listing.GetOptionList";
                        command.CommandType = CommandType.StoredProcedure;

                        SimpleQuery.AddWithValue(command, "DealerCode", dealerCode, DbType.String);
                        SimpleQuery.AddWithValue(command, "VIN", inventory.Vin, DbType.String);
                        SimpleQuery.AddWithValue(command, "StockNumber", inventory.StockNumber, DbType.String);
                        SimpleQuery.AddWithValue(command, "ProviderID", lotProvider.LotProviderCode, DbType.Int32);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                EquipmentSourceTO source = new EquipmentSourceTO();

                                source.EquipmentProviderId = equipmentProviderId;

                                source.Identity = reader.GetInt16(reader.GetOrdinal("OptionVersion"));

                                EquipmentInfoListTO list = new EquipmentInfoListTO();
                                list.Source = source;

                                EquipmentInfoTO item = new EquipmentInfoTO();
                                item.Name = reader.GetString(reader.GetOrdinal("OptionDescription"));
                                list.Add(item);

                                while (reader.Read())
                                {
                                    item = new EquipmentInfoTO();
                                    item.Name = reader.GetString(reader.GetOrdinal("OptionDescription"));
                                    list.Add(item);
                                }

                                return list;
                            }

                            return null;
                        }
                    }
                }
                #endregion
            }

            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderList#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "EquipmentProviderID", equipmentProviderId, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            EquipmentSourceTO source = new EquipmentSourceTO();

                            source.EquipmentProviderId = reader.GetInt32(reader.GetOrdinal("EquipmentProviderId"));

                            source.Identity = reader.GetInt64(reader.GetOrdinal("IdentityValue"));

                            if (reader.NextResult())
                            {
                                EquipmentInfoListTO list = new EquipmentInfoListTO();

                                list.Source = source;

                                while (reader.Read())
                                {
                                    EquipmentInfoTO item = new EquipmentInfoTO();

                                    item.Name = reader.GetString(reader.GetOrdinal("Name"));

                                    if (!reader.IsDBNull(reader.GetOrdinal("Value")))
                                    {
                                        item.Value = reader.GetInt32(reader.GetOrdinal("Value"));
                                    }

                                    list.Add(item);
                                }

                                return list;
                            }
                        }

                        return null;
                    }
                }
            }
        }

        private static string GetDealerCodeFromOwnerHandle(string ownerHandle)
        {
            // Get the owner, so we can get the business unit, so we can get the business unit code.
            var owner = Owner.GetOwner(ownerHandle);

            // we currently only support dealers and inventory
            if (owner.OwnerEntityType != OwnerEntityType.Dealer)
                throw new NotSupportedException("The passed owner entity type is not supported.");

            // get the BusinessUnit from the owner
            var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);

            return businessUnit.BusinessUnitCode;
        }
    }
}