using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Providers;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.Equipment.Implementation
{
    public class EquipmentProviderAssignmentDataAccess : IEquipmentProviderAssignmentDataAccess
    {
        private static string CurrentUser
        {
            get { return Thread.CurrentPrincipal.Identity.Name; }
        }

        public ITransaction BeginTransaction()
        {
            return new DbTransaction(FirstLook.Pricing.DomainModel.Database.MarketingDatabase);
        }

        public IList<EquipmentProviderInfoTO> FetchEquipmentProviderInfo()
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderInfoList#Fetch_All";
                    command.CommandType = CommandType.StoredProcedure;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        List<EquipmentProviderInfoTO> list = new List<EquipmentProviderInfoTO>();

                        while (reader.Read())
                        {
                            EquipmentProviderInfoTO item = new EquipmentProviderInfoTO();

                            item.Id = reader.GetInt32(reader.GetOrdinal("Id"));

                            item.Name = reader.GetString(reader.GetOrdinal("Name"));

                            list.Add(item);
                        }

                        return list;
                    }
                }
            }
        }

        public IList<EquipmentProviderInfoTO> FetchEquipmentProviderInfo(string ownerHandle)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderInfoList#Fetch_Owner";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        List<EquipmentProviderInfoTO> list = new List<EquipmentProviderInfoTO>();

                        while (reader.Read())
                        {
                            EquipmentProviderInfoTO item = new EquipmentProviderInfoTO();

                            item.Id = reader.GetInt32(reader.GetOrdinal("Id"));

                            item.Name = reader.GetString(reader.GetOrdinal("Name"));

                            list.Add(item);
                        }

                        return list;
                    }
                }
            }
        }

        public bool ExistsEquipmentProviderAssignmentList(string ownerHandle)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderAssignmentList#Exists";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("Exists"));
                        }

                        return false;
                    }
                }
            }
        }

        public int CreateEquipmentProviderAssignmentList(string ownerHandle)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderAssignmentList#Create";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "InsertUser", CurrentUser, DbType.String);

                    IDataParameter newId = SimpleQuery.AddOutParameter(command, "Id", false, DbType.Int32);

                    command.ExecuteNonQuery();

                    return (int) newId.Value;
                }
            }
        }

        public EquipmentProviderAssignmentListTO FetchEquipmentProviderAssignmentList(string ownerHandle)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderAssignmentList#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        EquipmentProviderAssignmentListTO list = new EquipmentProviderAssignmentListTO();

                        if (reader.Read())
                        {
                            list.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        }
                        else
                        {
                            throw new DataException("Expected first result-set!");
                        }

                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                EquipmentProviderAssignmentTO item = new EquipmentProviderAssignmentTO();

                                item.EquipmentProviderId = reader.GetInt32(reader.GetOrdinal("EquipmentProviderId"));

                                item.IsDefault = reader.GetBoolean(reader.GetOrdinal("IsDefault"));

                                item.Version = (byte[]) reader.GetValue(reader.GetOrdinal("Version"));

                                list.Add(item);
                            }
                        }
                        else
                        {
                            throw new DataException("Expected second result-set!");
                        }
                        
                        return list;
                    }
                }
            }
        }

        public bool ExistsVehicleEquipmentProviderAssignment(string ownerHandle, string vehicleHandle)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.VehicleEquipmentProvider#Exists";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("Exists"));
                        }

                        return false;
                    }
                }
            }
        }

        public VehicleEquipmentProviderAssignmentTO FetchVehicleEquipmentProviderAssignment(string ownerHandle, string vehicleHandle)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.VehicleEquipmentProvider#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            VehicleEquipmentProviderAssignmentTO item = new VehicleEquipmentProviderAssignmentTO();

                            item.EquipmentProviderId = reader.GetInt32(reader.GetOrdinal("EquipmentProviderId"));

                            item.Visible = reader.GetBoolean(reader.GetOrdinal("Visible"));

                            item.Version = (byte[]) reader.GetValue(reader.GetOrdinal("Version"));

                            return item;
                        }

                        throw new DataException("Expected a result set!");
                    }
                }
            }
        }

        public bool HasAssignment(string ownerHandle, int equipmentProviderId)
        {
            EquipmentProvider provider = EquipmentProvider.GetEquipmentProvider(equipmentProviderId);

            if(provider is MaxAdEquipmentProvider)
                return MaxAdEquipmentProvider.UseMaxAd(ownerHandle);

            var lotProvider = provider as ILotProvider;

            /* TODO:
             * The lot provider data access can be broken out into a separate dataaccess class.
             */

            if (lotProvider != null)
            {

                var owner = Owner.GetOwner(ownerHandle);

                // we currently only support dealers
                if (owner.OwnerEntityType != OwnerEntityType.Dealer)
                    throw new NotSupportedException("The passed owner entity type is not supported.");

                // get the BusinessUnit from the owner
                var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);

                using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.LotDatabase))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "Listing.ProviderBusinessUnit#Exists";
                        command.CommandType = CommandType.StoredProcedure;

                        SimpleQuery.AddWithValue(command, "BusinessUnitCode", businessUnit.BusinessUnitCode, DbType.String);
                        SimpleQuery.AddWithValue(command, "ProviderID", lotProvider.LotProviderCode, DbType.Byte);
                        var existsParam = SimpleQuery.AddOutParameter(command, "Exists", false, DbType.Boolean);

                        command.ExecuteNonQuery();

                        bool exists;

                        if (bool.TryParse(existsParam.Value.ToString(), out exists))
                            return exists;

                        return false;
                    }
                }
            }

            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.EquipmentProviderInfo#Available";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "EquipmentProviderId", equipmentProviderId, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("Available"));
                        }

                        return false;
                    }
                }
            }
        }

        public void Insert(ITransaction transaction, int id, int rank, EquipmentProviderAssignmentTO equipmentProviderAssignment)
        {
            DbTransaction trxn = (DbTransaction) transaction;

            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.EquipmentProviderAssignment#Insert";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "EquipmentProviderAssignmentListID", id, DbType.Int32);
                SimpleQuery.AddWithValue(command, "EquipmentProviderID", equipmentProviderAssignment.EquipmentProviderId, DbType.Int32);
                SimpleQuery.AddWithValue(command, "IsDefault", equipmentProviderAssignment.IsDefault, DbType.Boolean);
                SimpleQuery.AddWithValue(command, "Position", rank, DbType.Int32);
                SimpleQuery.AddWithValue(command, "InsertUser", CurrentUser, DbType.String);

                IDataParameter newVersion = SimpleQuery.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                command.ExecuteNonQuery();

                equipmentProviderAssignment.Version = (byte[]) newVersion.Value;
            }
        }

        public void Update(ITransaction transaction, int id, int rank, EquipmentProviderAssignmentTO equipmentProviderAssignment)
        {
            DbTransaction trxn = (DbTransaction)transaction;

            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.EquipmentProviderAssignment#Update";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "EquipmentProviderAssignmentListID", id, DbType.Int32);
                SimpleQuery.AddWithValue(command, "EquipmentProviderID", equipmentProviderAssignment.EquipmentProviderId, DbType.Int32);
                SimpleQuery.AddWithValue(command, "IsDefault", equipmentProviderAssignment.IsDefault, DbType.Boolean);
                SimpleQuery.AddWithValue(command, "Position", rank, DbType.Int32);
                SimpleQuery.AddWithValue(command, "UpdateUser", CurrentUser, DbType.String);
                SimpleQuery.AddArrayParameter(command, "Version", DbType.Binary, false, equipmentProviderAssignment.Version);

                IDataParameter newVersion = SimpleQuery.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                command.ExecuteNonQuery();

                equipmentProviderAssignment.Version = (byte[])newVersion.Value;
            }
        }

        public void Delete(ITransaction transaction, int id, EquipmentProviderAssignmentTO equipmentProviderAssignment)
        {
            DbTransaction trxn = (DbTransaction)transaction;

            using (IDbCommand command = trxn.CreateCommand())
            {
                command.CommandText = "Marketing.EquipmentProviderAssignment#DeleteSelf";
                command.CommandType = CommandType.StoredProcedure;

                SimpleQuery.AddWithValue(command, "EquipmentProviderAssignmentListID", id, DbType.Int32);
                SimpleQuery.AddWithValue(command, "EquipmentProviderID", equipmentProviderAssignment.EquipmentProviderId, DbType.Int32);
                SimpleQuery.AddArrayParameter(command, "Version", DbType.Binary, false, equipmentProviderAssignment.Version);

                command.ExecuteNonQuery();
            }
        }

        public void Insert(string ownerHandle, string vehicleHandle, VehicleEquipmentProviderAssignmentTO assignment)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.VehicleEquipmentProvider#Insert";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "EquipmentProviderID", assignment.EquipmentProviderId, DbType.Int32);
                    SimpleQuery.AddWithValue(command, "Visible", assignment.Visible, DbType.Boolean);
                    SimpleQuery.AddWithValue(command, "InsertUser", CurrentUser, DbType.String);

                    IDataParameter newVersion = SimpleQuery.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                    command.ExecuteNonQuery();

                    assignment.Version = (byte[])newVersion.Value;
                }
            }
        }

        public void Update(string ownerHandle, string vehicleHandle, VehicleEquipmentProviderAssignmentTO assignment)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.VehicleEquipmentProvider#Update";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "EquipmentProviderID", assignment.EquipmentProviderId, DbType.Int32);
                    SimpleQuery.AddWithValue(command, "Visible", assignment.Visible, DbType.Boolean);
                    SimpleQuery.AddWithValue(command, "UpdateUser", CurrentUser, DbType.String);
                    SimpleQuery.AddArrayParameter(command, "Version", DbType.Binary, false, assignment.Version);

                    IDataParameter newVersion = SimpleQuery.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                    command.ExecuteNonQuery();

                    assignment.Version = (byte[])newVersion.Value;
                }
            }
        }

        public void Delete(string ownerHandle, string vehicleHandle, VehicleEquipmentProviderAssignmentTO assignment)
        {
            using (IDbConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.MarketingDatabase))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "Marketing.VehicleEquipmentProvider#DeleteSelf";
                    command.CommandType = CommandType.StoredProcedure;

                    SimpleQuery.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SimpleQuery.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    SimpleQuery.AddArrayParameter(command, "Version", DbType.Binary, false, assignment.Version);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}