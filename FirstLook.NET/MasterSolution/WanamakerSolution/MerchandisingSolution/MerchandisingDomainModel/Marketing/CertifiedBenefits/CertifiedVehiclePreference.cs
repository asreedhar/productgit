using System;
using System.Collections.Generic;
using Csla;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits
{
    public sealed class CertifiedVehiclePreference : InjectableBusinessBase<CertifiedVehiclePreference>, IRenderXml, ICertifiedVehiclePreference
    {
        private const int NOT_ASSIGNED = 0;

        private int? _certifiedVehiclePreferenceId;
        private string _ownerHandle;
        private string _vehicleHandle;
        private bool _vehicleIsCertified;
        private bool _isDisplayed;

        private IList<CertifiedVehicleBenefitLineItemTO> _benefits;
        private OwnerCertifiedProgramTO _program;

        #region Injected dependencies

        public ICertifiedVehiclePreferenceDataAccess DataAccess { get; set;}

        #endregion

        #region Internal Properties

        internal IList<CertifiedVehicleBenefitLineItemTO> Benefits
        {
            get { return _benefits; }
        }

        #endregion

        public void SetBenefitLineItemDisplayProperties(string benefitLineItemKey, bool isHighlight, int rank)
        {
            // find the vehicleBenefitLineItem and set the highlight value
            CertifiedVehicleBenefitLineItemTO vehicleBenefitLineItem = Find( benefitLineItemKey );  
            if( vehicleBenefitLineItem == null ) throw new ApplicationException("Benefit not found.");
            
            vehicleBenefitLineItem.IsHighlighted = isHighlight;
            vehicleBenefitLineItem.Rank = rank;

            MarkDirty();                
        }

        private CertifiedVehicleBenefitLineItemTO Find(string key)
        {
            foreach (CertifiedVehicleBenefitLineItemTO benefitDto in _benefits)
            {
                if (benefitDto.Key == key) return benefitDto;
            }
            return null;
        }


        #region Factory Methods

        // Ctor needed for CSLA
        private CertifiedVehiclePreference(){}

        private CertifiedVehiclePreference(ICertifiedVehiclePreferenceDataAccess dataAccess, string ownerHandle, string vehicleHandle)
        {
			_certifiedVehiclePreferenceId = NOT_ASSIGNED;

			_program = dataAccess.FetchProgram(ownerHandle, vehicleHandle);

            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _vehicleIsCertified = dataAccess.IsCertifiedInventory(ownerHandle, vehicleHandle);
            // if the vehicle is certified, default the display to true, otherwise default it to not display
            _isDisplayed = _vehicleIsCertified;

            _benefits = new List<CertifiedVehicleBenefitLineItemTO>();

	        if (_vehicleIsCertified && _program != null)
	        {
		        // Create benefit preferences using the owner benefits as prototypes.
		        foreach (OwnerCertifiedProgramBenefitTO ownerBenefit in _program.Benefits)
		        {
			        var vehicleBenefitLineItem =
				        new CertifiedVehicleBenefitLineItemTO(CertifiedVehiclePreferenceID, _program.ProgamId,
					        ownerBenefit.ProgramBenefitId, ownerBenefit.Name, ownerBenefit.Text, ownerBenefit.IsHighlighted,
					        ownerBenefit.Rank);

			        _benefits.Add(vehicleBenefitLineItem);
		        }
	        }



	        MarkNew();
        }

        /// <summary>
        /// Locate a certified vehicle preference using the specified handles.  If a preference doesn't exist,
        /// create one.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        public static CertifiedVehiclePreference GetOrCreateCertifiedVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            // Does a preference exist?
            var command = new ExistsCommand( ownerHandle, vehicleHandle );
            DataPortal.Execute(command);

            // If the preference exists, return it.  Otherwise create a new one.
            return command.Exists ? GetCertifiedVehiclePreference( ownerHandle, vehicleHandle ) : 
                                    CreateCertifiedVehiclePreference( ownerHandle, vehicleHandle );
        }

        public static CertifiedVehiclePreference GetCertifiedVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<CertifiedVehiclePreference>(new Criteria(ownerHandle, vehicleHandle));
        }

        public static CertifiedVehiclePreference CreateCertifiedVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Create<CertifiedVehiclePreference>(new Criteria(ownerHandle, vehicleHandle));   
        }

        public static bool CertifiedProgramExists(string ownerHandle, string vehicleHandle)
        {
            var command = new OwnerCertifiedProgramExistsCommand(ownerHandle, vehicleHandle);
            DataPortal.Execute(command);

            return command.Exists;
        }

        #endregion

        #region Delete Behavior

        public void MarkForDeletion()
        {
            MarkDeleted();
        }

        #endregion

        #region Base Class Overrides

        protected override object GetIdValue()
        {
            return _certifiedVehiclePreferenceId;
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            CertifiedVehiclePreferenceTO preference = DataAccess.Fetch(
                criteria.OwnerHandle,
                criteria.VehicleHandle);

            // Initialize from the transfer object.
            FromTransferObject(preference);
        }

        private void DataPortal_Create(Criteria criteria)
        {
            // We have to construct a new object, get a corresponding transfer object, and then initialize ourselves from the transfer
            // object because of CSLA's odd create semantics.
            CertifiedVehiclePreferenceTO preference =
                    new CertifiedVehiclePreference( DataAccess, criteria.OwnerHandle, criteria.VehicleHandle)
                    .ToTransferObject();

            // Initialize from the transfer object.
            FromTransferObject(preference);   
        }

        protected override void DataPortal_Insert()
        {
            CertifiedVehiclePreferenceTO preference = ToTransferObject();
            DataAccess.Insert( preference, ApplicationContext.User.Identity.Name );

            _certifiedVehiclePreferenceId = preference.CertifiedVehiclePreferenceID;
        }

        protected override void DataPortal_Update()
        {             
            DataAccess.Update( ToTransferObject(), ApplicationContext.User.Identity.Name );
        }

        protected override void DataPortal_DeleteSelf()
        {             
            DataAccess.Delete( ToTransferObject(), ApplicationContext.User.Identity.Name);
        }

        

        #endregion

        #region Exists Command

        [Serializable]
        class ExistsCommand : InjectableCommandBase
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private bool _exists;

            #region Injected dependencies

// ReSharper disable MemberCanBePrivate.Local
            public ICertifiedVehiclePreferenceDataAccess DataAccess { get; set; }
// ReSharper restore MemberCanBePrivate.Local

            #endregion

            public ExistsCommand(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public bool Exists
            {
                get{ return _exists; }
            }

            protected override void DataPortal_Execute()
            {
                _exists = DataAccess.Exists(_ownerHandle, _vehicleHandle);
            }
        }

        #endregion

        #region ICertifiedVehiclePreference

        public int CertifiedVehiclePreferenceID
        {
            get
            {
                if (_certifiedVehiclePreferenceId.HasValue) return _certifiedVehiclePreferenceId.Value;
                return NOT_ASSIGNED;
            }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public bool IsDisplayed
        {
            get
            {
                CanReadProperty("IsDisplayed", true);

                return _isDisplayed;
            }
            set
            {
                CanWriteProperty("IsDisplayed", true);

                if (IsDisplayed != value)
                {
                    _isDisplayed = value;

                    PropertyHasChanged("IsDisplayed");
                }
            }
        }

        public IEnumerable<ICertifiedVehicleBenefit> GetBenefits()
        {
            foreach(CertifiedVehicleBenefitLineItemTO dto in _benefits)
            {
                yield return dto;
            }
        }

        /*
         * Max Marketing is being changed to allow the presentation of certified benefits 
         * for inventory items that are not certified.  Thus, we can't assume that preferences
         * are created for certfied inventory items only.
         */
        public bool VehicleIsCertified
        {
            get
            {
                CanReadProperty("VehicleIsCertified", true);

                return _vehicleIsCertified ;
            }
        }

        public string ProgramName
        {
            get { return _program.ProgramName; }
        }

        public string ProgramLogoPath
        {
            get { return _program.ProgramLogoPath; }
        }



        #endregion

        #region Map to/from CertifiedVehiclePreferenceTO transfer objects

        internal CertifiedVehiclePreferenceTO ToTransferObject()
        {
            return new CertifiedVehiclePreferenceTO(_certifiedVehiclePreferenceId, _ownerHandle,_vehicleHandle,
                _isDisplayed, _benefits, _program, _vehicleIsCertified );
        }

        internal void FromTransferObject(CertifiedVehiclePreferenceTO transferObject)
        {
            _certifiedVehiclePreferenceId = transferObject.CertifiedVehiclePreferenceID;
            _ownerHandle = transferObject.OwnerHandle;
            _vehicleHandle = transferObject.VehicleHandle;
            _isDisplayed = transferObject.IsDisplayed;
            _vehicleIsCertified = transferObject.VehicleIsCertified;

            _program = transferObject.OwnerCertifiedProgram;
            _benefits = transferObject.Benefits;
        }

        #endregion

        #region IRenderXml

        public string AsXml()
        {
            return CertifiedVehiclePreferenceXmlBuilder.AsXml(this);
        }

        #endregion

        #region Owner Certified Program Exists Command

        [Serializable]
        class OwnerCertifiedProgramExistsCommand : InjectableCommandBase
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private bool _exists;

            #region Injected dependencies

            public ICertifiedVehiclePreferenceDataAccess DataAccess { get; set; }

            #endregion

            public OwnerCertifiedProgramExistsCommand(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public bool Exists
            {
                get { return _exists; }
            }

            protected override void DataPortal_Execute()
            {
                _exists = DataAccess.OwnerCertifiedProgramExists(_ownerHandle, _vehicleHandle);
            }
        }

        #endregion


        
    }
}
