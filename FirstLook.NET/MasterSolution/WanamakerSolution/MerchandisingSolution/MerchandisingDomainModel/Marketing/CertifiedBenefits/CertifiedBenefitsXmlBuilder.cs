using System.Collections.Generic;
using System.Text;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits
{
    public class CertifiedVehiclePreferenceXmlBuilder
    {
        /*
	        <certifiedbenefits name="Chevrolet Certified Pre-Owned Vehicle (CPOV)" logoPath="/resources/certified/Public/Images/CPOLogos/gmc.jpg" display="True" vehicleIsCertified="true">
		        <benefit key="1:1" rank="1">
			        117-Point Inspection and Reconditioning
		        </benefit>
		        <benefit key="1:2" rank="2">
			        12-Month/12,000-Mile Bumper-to-Bumper Warranty
		        </benefit>
		        <benefit key="1:3" rank="3">
			        24/7 roadside assistance for 5 Years/100,000 miles
		        </benefit>
		        <benefit key="1:5" rank="5">
			        NO Deductible, transferrable to subsequent owners
		        </benefit>
		        <benefit key="1:6" rank="6">Trip-Interruption Services	</benefit>
		        <benefit key="1:7" rank="7">Vehicle History Report	</benefit>
		        <benefit key="1:8" rank="8">
			        5-Year/100,000-Mile Powertrain Limited Warranty from Original In-Service Date
		        </benefit>
	        </certifiedbenefits>
        */
        public static string AsXml(ICertifiedVehiclePreference preference)
        {
            if (preference == null ) 
                return string.Empty;

            // build benefit elements
            IEnumerable<ICertifiedVehicleBenefit> benefits = preference.GetBenefits();
            string benefitElements = BuildBenefitElements( benefits );

            // build certified element.
            IDictionary<string,string> attributes = new Dictionary<string, string>();
            attributes.Add("name", preference.ProgramName);
            attributes.Add("logoPath", preference.ProgramLogoPath);
            attributes.Add("display", preference.IsDisplayed.ToString().ToLower() );
            attributes.Add("vehicleIsCertified", preference.VehicleIsCertified.ToString().ToLower() );

            string element = TagBuilder.Wrap("certifiedbenefits", attributes, benefitElements);
            return element;
        }
        
        private static string BuildBenefitElements(IEnumerable<ICertifiedVehicleBenefit> benefits)
        {
            var sb = new StringBuilder();
            foreach(var benefit in benefits)
            {
                if( benefit.IsHighlighted )
                {
                    IDictionary<string,string> attributes = new Dictionary<string, string>();
                    attributes.Add("key", benefit.Key);
                    attributes.Add("rank", benefit.Rank.ToString());
                    sb.Append( TagBuilder.Wrap("benefit", attributes, benefit.Text));
                }
            }
            string elements = sb.ToString();
            return elements;
        }
    }

    
}
