﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Exceptions
{
	public class ProgramNoLongerAvailableException : Exception
	{
		public ProgramNoLongerAvailableException() 
		{
		}

		public ProgramNoLongerAvailableException(string message) : base(message)
		{
		}

		public ProgramNoLongerAvailableException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
