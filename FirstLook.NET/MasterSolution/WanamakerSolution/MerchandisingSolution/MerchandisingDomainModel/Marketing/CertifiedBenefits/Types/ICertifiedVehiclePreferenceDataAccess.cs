using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types
{
    public interface ICertifiedVehiclePreferenceDataAccess
    {
       
        bool Exists(string ownerHandle, string vehicleHandle);

        CertifiedVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle);

	    CertifiedVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle, int? certifiedVehicleBenefitPreferenceId);


        /// <summary>
        /// Get the correct certified program given owner and vehicle handles.
        /// </summary>
        /// <returns></returns>
        OwnerCertifiedProgramTO FetchProgram(string ownerHandle, string vehicleHandle);

        /// <summary>
        /// Delete the preference.
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        void Delete(CertifiedVehiclePreferenceTO preference, string userName);


        bool IsCertifiedInventory(string ownerHandle, string vehicleHandle);

        /// <summary>
        /// Returns a value indicating whether or not an owner participates in a certified program for a particular vehicle.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        bool OwnerCertifiedProgramExists(string ownerHandle, string vehicleHandle);

	    void Insert(CertifiedVehiclePreferenceTO preference, string name);
	    void Update(CertifiedVehiclePreferenceTO toTransferObject, string name);
    }
}
