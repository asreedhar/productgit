using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects
{
    [Serializable]
    public class CertifiedVehiclePreferenceTO : ICertifiedVehiclePreference
    {
        private int _certifiedVehiclePreferenceId;
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;
        private bool _isDisplayed;
        private readonly bool _vehicleIsCertified;
        private readonly IList<CertifiedVehicleBenefitLineItemTO> _benefits;
        private readonly OwnerCertifiedProgramTO _program;

        internal CertifiedVehiclePreferenceTO(int? certifiedVehiclePreferenceId, string ownerHandle, string vehicleHandle,
                                               bool isDisplayed, IList<CertifiedVehicleBenefitLineItemTO> benefits, OwnerCertifiedProgramTO program, bool vehicleIsCertified)
        {
            if (certifiedVehiclePreferenceId.HasValue) _certifiedVehiclePreferenceId = certifiedVehiclePreferenceId.Value;
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _isDisplayed = isDisplayed;
            _benefits = benefits;
            _program = program;
            _vehicleIsCertified = vehicleIsCertified;
        }

        #region Implementation of ICertifiedVehiclePreference

        public int CertifiedVehiclePreferenceID
        {
            get { return _certifiedVehiclePreferenceId; }
            internal set { _certifiedVehiclePreferenceId = value; }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public bool IsDisplayed
        {
            get { return _isDisplayed; }
            set { _isDisplayed = value; }
        }

        public bool VehicleIsCertified
        {
            get { return _vehicleIsCertified; }
        }

        public string ProgramName
        {
            get { return _program.ProgramName; }
        }

        public string ProgramLogoPath
        {
            get { return _program.ProgramLogoPath; }
        }

        public IEnumerable<ICertifiedVehicleBenefit> GetBenefits()
        {
            foreach( CertifiedVehicleBenefitLineItemTO dto in _benefits)
            {
                yield return dto;
            }             
        }

        #endregion

        internal IList<CertifiedVehicleBenefitLineItemTO> Benefits
        {
            get { return _benefits; }
        }

        internal OwnerCertifiedProgramTO OwnerCertifiedProgram
        {
            get { return _program; }
        }
    }
}