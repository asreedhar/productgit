using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects
{
    [Serializable]
    public class OwnerCertifiedProgramBenefitTO : IOwnerCertifiedProgramBenefit
    {
	    internal OwnerCertifiedProgramBenefitTO(int programBenefitId, int? ownerBenefitId, string name, string text, bool isHighlighted, int rank)
        {
			OwnerBenefitID = ownerBenefitId;
		    ProgramBenefitId = programBenefitId;
            Name = name;
            Text = text;
            IsHighlighted = isHighlighted;
            Rank = rank;
        }


	    #region Implementation of IOwnerCertifiedProgramBenefit
		public int ProgramBenefitId { get; private set; }

	    public int? OwnerBenefitID { get; private set; }

	    public string Name { get; private set; }

	    public string Text { get; private set; }

	    public bool IsHighlighted { get; private set; }

	    public int Rank { get; private set; }

	    #endregion


    }
}