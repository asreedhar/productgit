using FirstLook.Common.Core.DTO;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types
{
    public interface ICertifiedVehicleBenefit : ISurrogateKey
    {
        int? CertifiedVehiclePreferenceID { get;}
	    int CertifiedProgramId { get; }
	    bool IsHighlighted { get; set; }
        int Rank { get; set; }
	    string Text { get; set; }
	    string Name { get; set; }
    }
}
