using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects
{
    [Serializable]
    public class OwnerCertifiedProgramTO
    {
		internal OwnerCertifiedProgramTO(int programId, int? ownerProgramId, string programName, string logoPath, List<OwnerCertifiedProgramBenefitTO> benefits)
        {
            ProgamId = programId;
		    OwnerProgramId = ownerProgramId;
            ProgramName = programName;
            ProgramLogoPath = logoPath;
			Benefits = benefits;
        }


	    internal int ProgamId { get; private set; }

	    internal int? OwnerProgramId { get; private set; }

	    internal string ProgramName { get; private set; }

	    internal string ProgramLogoPath { get; private set; }

	    internal List<OwnerCertifiedProgramBenefitTO> Benefits { get; private set; }
    }
}