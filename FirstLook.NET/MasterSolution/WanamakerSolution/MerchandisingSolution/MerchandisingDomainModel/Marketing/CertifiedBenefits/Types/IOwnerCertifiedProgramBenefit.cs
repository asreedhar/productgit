namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types
{
     public interface IOwnerCertifiedProgramBenefit
     {
		 int ProgramBenefitId { get; }
		 int? OwnerBenefitID { get; }
         string Name { get; }
         string Text { get; }
         bool IsHighlighted { get; }
         int Rank { get; }
     }
}
