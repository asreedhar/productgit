using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects
{
    [Serializable]
    internal class CertifiedVehicleBenefitLineItemTO : ICertifiedVehicleBenefit
    {
        private const string KeyDelim = ":";

	    /// <summary>
	    /// Construct an existing benefit dto.
	    /// </summary>
	    /// <param name="certifiedVehiclePreferenceId"></param>
	    /// <param name="benefitId"></param>
	    /// <param name="text"></param>
	    /// <param name="isHighlighted"></param>
	    /// <param name="rank"></param>
	    /// <param name="certifiedProgramId"></param>
	    /// <param name="name"></param>
	    internal CertifiedVehicleBenefitLineItemTO( int? certifiedVehiclePreferenceId, int certifiedProgramId, int benefitId,string name, string text, bool isHighlighted, int rank)
        {
            Rank = rank;
            CertifiedVehiclePreferenceID = certifiedVehiclePreferenceId;
            IsHighlighted = isHighlighted;
	        CertifiedProgramId = certifiedProgramId;
	        BenefitId = benefitId;
		    Name = name;
		    Text = text;

        }

        #region Implementation of ICertifiedVehicleBenefit

        public int? CertifiedVehiclePreferenceID { get; internal set; }
        public bool IsHighlighted { get; set; }
        public int Rank { get; set; }
	    public string Text { get; set; }
	    public string Name { get; set; }
	    public int CertifiedProgramId { get; private set; }
		public int BenefitId { get; private set; }

        #endregion

        #region Implementation of ISurrogateKey

        /// <summary>
        /// The Key is the programId:OwnerCertifiedProgramBenefitId.  It is returned as a string.  E.G.  "10:32"
        /// </summary>
        public string Key
        {
			get { return CertifiedProgramId + KeyDelim + BenefitId; }
        }

        #endregion

        
    }
}