namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects
{
	public class CertifiedProgramInfoTO
	{
		public CertifiedProgramInfoTO(int id, string programType, string name, bool requiresCertifiedId, bool manufacturerReportsCertifications)
		{
			Id = id;
			ProgramType = programType;
			Name = name;
			RequiresCertifiedId = requiresCertifiedId;
			ManufacturerReportsCertifications = manufacturerReportsCertifications;
		}

		public int Id { get; private set; }
		public string ProgramType { get; private set; }
		public string Name { get; private set; }
		public bool RequiresCertifiedId { get; private set; }
		public bool ManufacturerReportsCertifications { get; private set; }

		public string Key
		{
			get { return string.Format("{0}|{1}", Id, RequiresCertifiedId); }
		}
	}
}