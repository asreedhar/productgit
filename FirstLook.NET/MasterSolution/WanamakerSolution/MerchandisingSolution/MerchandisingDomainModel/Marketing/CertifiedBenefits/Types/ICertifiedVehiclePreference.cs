using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types
{
    public interface ICertifiedVehiclePreference  
    {
        int CertifiedVehiclePreferenceID { get;}
        string OwnerHandle { get;}
        string VehicleHandle { get; }
        bool IsDisplayed { get; }
        bool VehicleIsCertified { get; }
        string ProgramName { get; }
        string ProgramLogoPath { get; }

        IEnumerable<ICertifiedVehicleBenefit> GetBenefits();

    }

    
}
