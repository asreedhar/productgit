using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using Csla;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using IDataConnection = FirstLook.Common.Data.IDataConnection;

namespace FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits
{
    /// <summary>
    /// Data access layer.
    /// </summary>
    class CertifiedVehiclePreferenceDataAccess : ICertifiedVehiclePreferenceDataAccess
    {
        private const string CERTIFIED_PROGRAM_LOGO_PATH = "/resources/certified/Public/Images/CPOLogos/";

        /// <summary>
        /// Check to see if a preference exists.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        /// TESTED
        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            int count = 0;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.CertifiedVehicleBenefitPreference#Exists";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    CommonMethods.AddWithValue( command, "ReturnValue", count, DbType.Int32, ParameterDirection.ReturnValue, false );

                    command.ExecuteNonQuery();

                    IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }
                }
            }
            return (count > 0);
        }

	    public CertifiedVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle, int? certifiedVehicleBenefitPreferenceId)
	    {
			// get the program
			var program = GetOwnerCertifiedProgram(ownerHandle, vehicleHandle);

			// get the benefit preferences
			IList<CertifiedVehicleBenefitLineItemTO> benefitLineItems = program.Benefits.Select(benefit => new CertifiedVehicleBenefitLineItemTO(certifiedVehicleBenefitPreferenceId, program.ProgamId, benefit.ProgramBenefitId, benefit.Name, benefit.Text, benefit.IsHighlighted, benefit.Rank)).ToList();

			// get the preference
			return GetPreference(ownerHandle, vehicleHandle, benefitLineItems, program);
	    }

		public CertifiedVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle)
		{
			var preferenceId = GetCertifiedVehicleBenefitPreferenceID(ownerHandle, vehicleHandle);
			return Fetch(ownerHandle, vehicleHandle, preferenceId );
		}

        /// <summary>
        /// Insert a preference
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        /// TESTED
        public void Insert(CertifiedVehiclePreferenceTO preference, string userName)
        {
            // Verify make is assigned to program?
            int certifiedVehiclePreferenceID = 0;

            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {               
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.CertifiedVehicleBenefitPreference#Insert";

                    CommonMethods.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", preference.VehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "IsDisplayed", preference.IsDisplayed, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "OwnerCertifiedProgramID", preference.OwnerCertifiedProgram.ProgamId, DbType.Int32);
                    CommonMethods.AddWithValue(command, "InsertUser", userName, DbType.String);
                    CommonMethods.AddWithValue(command, "CertifiedVehiclePreferenceID", certifiedVehiclePreferenceID, DbType.Int32, ParameterDirection.Output, false);

                    try
                    {
                        command.ExecuteNonQuery();
                        certifiedVehiclePreferenceID = (int)((SqlParameter)command.Parameters["CertifiedVehiclePreferenceID"]).Value;
                    }
                    catch (Exception)
                    {
                        txn.Rollback();                        
                        throw;
                    }
                }

                // Commit the batch of commands.
                txn.Commit();
            }

            preference.CertifiedVehiclePreferenceID = certifiedVehiclePreferenceID;
        }

        /// <summary>
        /// Update the preference and the line items.
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        public void Update(CertifiedVehiclePreferenceTO preference, string userName)
        {
            // Update the preference.
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.CertifiedVehicleBenefitPreference#Update";

                    CommonMethods.AddWithValue(command, "CertifiedVehicleBenefitPreferenceID",
                                               preference.CertifiedVehiclePreferenceID, DbType.Int32);
                    CommonMethods.AddWithValue(command, "IsDisplayed", preference.IsDisplayed, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "UpdateUser", userName, DbType.String);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

				// Commit the batch of commands.
                txn.Commit();
            }
        }



        public OwnerCertifiedProgramTO FetchProgram(string ownerHandle, string vehicleHandle)
        {
            return GetOwnerCertifiedProgram(ownerHandle, vehicleHandle);
        }

        /// <summary>
        /// Delete the preference.
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        /// TESTED
        public void Delete(CertifiedVehiclePreferenceTO preference, string userName)
        {
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {                
                using (IDbCommand command = txn.CreateCommand())
                {

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.CertifiedVehicleBenefitPreference#Delete";

                    CommonMethods.AddWithValue(command, "CertifiedVehicleBenefitPreferenceID",
                                               preference.CertifiedVehiclePreferenceID,
                                               DbType.Int32, ParameterDirection.Input, false);

                    try
                    {
                        command.ExecuteNonQuery();
                        txn.Commit();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }                    
                }
            }
        }

       

        #region Internal Methods

        internal CertifiedVehiclePreferenceTO GetPreference(string ownerHandle, string vehicleHandle, 
                        IList<CertifiedVehicleBenefitLineItemTO> benefitPreferences, OwnerCertifiedProgramTO program )
        {
            CertifiedVehiclePreferenceTO preference;

            bool isCertified = IsCertifiedInventory(ownerHandle, vehicleHandle);

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.CertifiedVehicleBenefitPreference#Fetch";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);

	                using (var rdr = command.ExecuteReader())
	                {
		                if (rdr.Read())
		                {
			                int certifiedVehicleBenefitPreferenceId =
				                rdr.GetInt32(rdr.GetOrdinal("CertifiedVehicleBenefitPreferenceID"));
			                bool isDisplayed = rdr.GetBoolean(rdr.GetOrdinal("IsDisplayed"));

			                preference =
				                new CertifiedVehiclePreferenceTO(certifiedVehicleBenefitPreferenceId, ownerHandle, vehicleHandle,
					                isDisplayed, benefitPreferences, program, isCertified);
		                }
		                else
		                {
			                throw new ApplicationException("The preference could not be found.");
		                }
	                }
                }
            }

            if( preference == null )
            {
                // If nothing found, throw.
                throw new ApplicationException("No certified vehicle preference could be found with the specified handles.");                
            }

            return preference;
        }

        /// <summary>
        /// Get the CertifiedVehicleBenefitPreferenceID given an owner and vehicle handle.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        internal int GetCertifiedVehicleBenefitPreferenceID(string ownerHandle, string vehicleHandle)
        {
            int preferenceId = Int32.MinValue;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.CertifiedVehicleBenefitPreference#Fetch";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr != null)
                    {
                        if (rdr.Read())
                        {
                            preferenceId = rdr.GetInt32(rdr.GetOrdinal("CertifiedVehicleBenefitPreferenceID"));
                        }
                        else
                        {
                            throw new ApplicationException("The preference could not be found.");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("The datareader is null.");
                    }
                    if (!rdr.IsClosed) rdr.Close();
                }
            }

            if (preferenceId == Int32.MinValue)
            {
                // If nothing found, throw.
                throw new ApplicationException("No certified vehicle preference could be found with the specified handles.");
            }

            return preferenceId;
        }

		///// <summary>
		///// Get the benefit line items.
		///// </summary>
		///// <param name="ownerHandle"></param>
		///// <param name="vehicleHandle"></param>
		///// <returns></returns>
		//internal IList<CertifiedVehicleBenefitLineItemTO> GetCertifiedVehicleBenefitLineItems(string ownerHandle, string vehicleHandle)
		//{
		//    // Get the preference id.
		//    int certifiedVehicleBenefitPreferenceId = GetCertifiedVehicleBenefitPreferenceID(ownerHandle, vehicleHandle);

		//    // Get the owner benefits
		//    IList<OwnerCertifiedProgramBenefitTO> ownerBenefits = FetchProgram(ownerHandle, vehicleHandle);

		//    // A collection of line items which we will return.
		//    IList<CertifiedVehicleBenefitLineItemTO> lineItems = new List<CertifiedVehicleBenefitLineItemTO>();

		//    // Get the line items.
		//    using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
		//    {
		//        if (connection.State == ConnectionState.Closed)
		//            connection.Open();

		//        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
		//        {
		//            command.CommandType = CommandType.StoredProcedure;
		//            command.CommandText = "Marketing.CertifiedVehicleBenefitLineItem#Fetch";

		//            CommonMethods.AddWithValue(command, "CertifiedVehicleBenefitPreferenceID", certifiedVehicleBenefitPreferenceId, DbType.Int32);

		//            IDataReader rdr = command.ExecuteReader();

		//            if (rdr != null)
		//            {
		//                while (rdr.Read())
		//                {                           
                            
		//                    int ownerCertifiedProgramBenefitId = rdr.GetInt32(rdr.GetOrdinal("OwnerCertifiedProgramBenefitID"));
		//                    bool isHighlight = rdr.GetBoolean(rdr.GetOrdinal("IsHighlight"));
		//                    int rank = rdr.GetInt32(rdr.GetOrdinal("Rank"));
                            
		//                    // Find the matching owner benefit.
		//                    OwnerCertifiedProgramBenefitTO ownerCertifiedProgramBenefit = FindOwnerBenefit(
		//                        ownerBenefits, ownerCertifiedProgramBenefitId);

		//                    // Create the line item.
		//                    CertifiedVehicleBenefitLineItemTO
		//                        lineItem =
		//                            new CertifiedVehicleBenefitLineItemTO(certifiedVehicleBenefitPreferenceId, ownerCertifiedProgramBenefit, isHighlight, rank);

		//                    lineItems.Add( lineItem );
		//                }
                        
		//            }
		//            else
		//            {
		//                throw new ApplicationException("The datareader is null.");
		//            }
		//            if (!rdr.IsClosed) rdr.Close();
		//        }
		//    }

		//    // Sort the line items by the rank.
		//    return SortByRank( lineItems );

		//}

        private static IList<CertifiedVehicleBenefitLineItemTO> SortByRank(IEnumerable<CertifiedVehicleBenefitLineItemTO> lineItems)
        {
            // Create comparer and sort list by rank.
            CertifiedVehicleBenefitLineItemTORankComparer comparer = new CertifiedVehicleBenefitLineItemTORankComparer();
            List<CertifiedVehicleBenefitLineItemTO> list = new List<CertifiedVehicleBenefitLineItemTO>(lineItems);

            list.Sort(comparer);
            
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ownerBenefits"></param>
        /// <param name="ownerCertifiedProgramBenefitID"></param>
        /// <returns></returns>
        /// TESTED
        internal static OwnerCertifiedProgramBenefitTO FindOwnerBenefit(IEnumerable<OwnerCertifiedProgramBenefitTO> ownerBenefits, int ownerCertifiedProgramBenefitID)
        {
            foreach( OwnerCertifiedProgramBenefitTO benefit in ownerBenefits )
            {
                if( benefit.OwnerBenefitID == ownerCertifiedProgramBenefitID) return benefit;
            }
            return null;
        }

        /// <summary>
        /// Get the owner certified program for this owner/vehicle.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        internal OwnerCertifiedProgramTO GetOwnerCertifiedProgram( string ownerHandle, string vehicleHandle )
        {
            int? certifiedProgramId;
			GetApplicableCertfiedProgramId(ownerHandle, vehicleHandle, out certifiedProgramId);

            // Logo path is currently not used/set.
            string programName;
			int rootProgramId;
			int? ownerCertifiedProgramId;
            string programIcon;

	        if (!certifiedProgramId.HasValue)
		        return null;

			var benefits = new List<OwnerCertifiedProgramBenefitTO>();

	        using (var connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
	        {
		        if (connection.State == ConnectionState.Closed)
			        connection.Open();

		        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
		        {
			        command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Certified.ManufacturerOrDealerCertifiedProgram#Fetch";

			        CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
			        CommonMethods.AddWithValue(command, "CertifiedProgramID", certifiedProgramId.Value, DbType.Int32);

			        using (var rdr = command.ExecuteReader())
			        {
				        if (rdr.Read())
				        {
					        programName = rdr.GetString(rdr.GetOrdinal("Name"));
							rootProgramId = rdr.GetInt32(rdr.GetOrdinal("CertifiedProgramID"));
							ownerCertifiedProgramId = rdr.GetNullableInt32("OwnerCertifiedProgramID");

					        var iconOrdinal = rdr.GetOrdinal("Icon");
							programIcon = rdr.IsDBNull(iconOrdinal) ? string.Empty : CERTIFIED_PROGRAM_LOGO_PATH + rdr.GetString(rdr.GetOrdinal("Icon"));
				        }
				        else
				        {
					        throw new ApplicationException("The owner certified program could not be found.");
				        }

				        rdr.NextResult();

						while (rdr.Read())
						{
							var name = rdr.GetString(rdr.GetOrdinal("Name"));
							var text = rdr.GetString(rdr.GetOrdinal("Text"));
							var programBenefitId = rdr.GetInt32(rdr.GetOrdinal("CertifiedProgramBenefitID"));
							var ownerBenefitId = rdr.GetNullableInt32("OwnerCertifiedProgramBenefitID");

							var isHighlight = rdr.GetBoolean(rdr.GetOrdinal("IsProgramHighlight"));
							var rank = rdr.GetInt16(rdr.GetOrdinal("Rank"));

							var benefit = new OwnerCertifiedProgramBenefitTO(
								programBenefitId,
								ownerBenefitId,
								name,
								text,
								isHighlight,
								rank);

							benefits.Add(benefit);
						}
			        }

		        }
	        }

			return new OwnerCertifiedProgramTO(rootProgramId, ownerCertifiedProgramId, programName, programIcon, benefits);
        }

        /// <summary>
        /// Look for the vehicle in inventory and in market listings.  Return the makeId.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        /// TESTED
        internal int GetMakeId(string ownerHandle, string vehicleHandle)
        {
            const int NOT_FOUND = 50200;

            // Use existing pricing domain to determine the make.
            try
            {
                Inventory inventory = Inventory.GetInventory(ownerHandle, vehicleHandle);
                return inventory.DecodingClassification.MakeId;
            }
            catch (DataPortalException ex)            
            {   
                // Try to cast the BusinessException to a SqlException.
                SqlException sqlException = ex.BusinessException as SqlException;

                // Suppress not found exceptions.
                if( sqlException == null || sqlException.Number != NOT_FOUND ) 
                    throw;               
            }


            try
            {
                InternetListing listing = InternetListing.GetInternetListing(ownerHandle, vehicleHandle);
                return listing.DecodingClassification.MakeId;
            }
            catch (DataPortalException ex)
            {
                // Try to cast the BusinessException to a SqlException.
                SqlException sqlException = ex.BusinessException as SqlException;

                // Suppress not found exceptions.
                if (sqlException == null || sqlException.Number != NOT_FOUND)
                    throw;
            }

            throw new ApplicationException("The specified vehicle is neither in inventory nor in market listings.");
        }

        /// <summary>
        /// Returns a value indicating whether or not the vehicle specified by the owner and vehicle handles is certified inventory.
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="vehicleHandle"></param>
        /// <returns></returns>
        public bool IsCertifiedInventory(string ownerHandle, string vehicleHandle)
        {
	        bool isInventory = false, isCertified = false;

            // Is this vehicle certified?
            try
            {
                var inventory = Inventory.GetInventory(ownerHandle, vehicleHandle);
	            if (inventory != null)
	            {
		            isInventory = true;
		            isCertified = inventory.Certified || inventory.CertifiedProgramId.HasValue;
	            }
            }
            catch (Exception)
            {
                // this is not an inventory item.
                isInventory = false;
                isCertified = false;
            }

	        return isInventory && isCertified;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="makeId"></param>
        /// <param name="certifiedProgramId"></param>
        /// TESTED
		//internal void GetCertifiedProgramId(int makeId, out int? certifiedProgramId)
		//{
		//    using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
		//    {
		//        if (connection.State == ConnectionState.Closed)
		//            connection.Open();

		//        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
		//        {
		//            command.CommandType = CommandType.StoredProcedure;
		//            command.CommandText = "Certified.CertifiedProgram#FetchByMake";

		//            CommonMethods.AddWithValue(command, "MakeId", makeId, DbType.Int32);

		//            IDataReader rdr = command.ExecuteReader();

		//            if (rdr != null)
		//            {
		//                if (rdr.Read())
		//                {
		//                    certifiedProgramId = rdr.GetInt32(rdr.GetOrdinal("CertifiedProgramID"));
		//                }
		//                else
		//                {
		//                    certifiedProgramId = null;
		//                }
		//            }
		//            else
		//            {
		//                throw new ApplicationException("The datareader is null.");
		//            }
		//            if (!rdr.IsClosed) rdr.Close();
		//        }
		//    }

		//}


	
		internal void GetApplicableCertfiedProgramId(string ownerHandle, string vehicleHandle, out int? certifiedProgramId)
		{
			using (var connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandText = "Certified.GetApplicableCertfiedProgramId";

					CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
					CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
					var outputParam = command.AddOutParameter("ProgramId", DbType.Int32);

					command.ExecuteNonQuery();

					certifiedProgramId = outputParam.Value as int?;
				}
			}

		}


        public bool OwnerCertifiedProgramExists(string ownerHandle, string vehicleHandle)
        {
            // Get the certified program mapped to this make.
            int? certifiedProgramId;

			GetApplicableCertfiedProgramId(ownerHandle, vehicleHandle, out certifiedProgramId);

            return certifiedProgramId.HasValue; // && OwnerCertifiedProgramExists(ownerHandle, certifiedProgramId.Value);
        }




	    /// <summary>
        /// 
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <param name="certifiedProgramId"></param>
        /// <returns></returns>
        /// TESTED
        internal bool OwnerCertifiedProgramExists(string ownerHandle, int certifiedProgramId)
        {
            int count = 0;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Certified.OwnerCertifiedProgram#Exists";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "CertifiedProgramID", certifiedProgramId, DbType.Int32);
                    CommonMethods.AddWithValue( command, "ReturnValue", count, DbType.Int32, ParameterDirection.ReturnValue, false );
                    command.ExecuteNonQuery();

                    IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }
                }
            }
            return (count > 0);
        }

        #endregion

        /// <summary>
        /// A custom sorter to sort line items by rank.
        /// </summary>
        private class CertifiedVehicleBenefitLineItemTORankComparer : IComparer<CertifiedVehicleBenefitLineItemTO>
        {
            public int Compare(CertifiedVehicleBenefitLineItemTO x, CertifiedVehicleBenefitLineItemTO y)
            {
                int comparison = 0;

                if (x.Rank < y.Rank) comparison -= 1; 
                else if( x.Rank > y.Rank ) comparison += 1;

                return comparison;                
            }
        }
    }
}