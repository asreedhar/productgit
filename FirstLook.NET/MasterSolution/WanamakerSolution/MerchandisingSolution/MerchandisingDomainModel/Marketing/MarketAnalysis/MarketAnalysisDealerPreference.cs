using System;
using System.Collections.Generic;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    [Serializable]
    public class MarketAnalysisDealerPreference : InjectableBusinessBase<MarketAnalysisDealerPreference>, IMarketAnalysisDealerPreference
    {
        #region Private Member Variables

        private int _id;
        private string _ownerHandle;
        private int _numberOfPricesRequiredForPricingGauge;
        private bool _useCustomCurrentInternetPrice;
        private string _currentInternetPriceDisplayName;
        private bool _useCustomOfferPrice;
        private string _offerPriceDisplayName;
        private bool _useCustomComparisonPricePriority;
        private List<PriceProvider> _priceProviders;
        private string _logoUrl;
        private bool _originalListPriceCalculationOverride;
        
        #endregion

        #region Construction

        private MarketAnalysisDealerPreference()
        {
            /* force use of factory methods */
        }

        #endregion

        #region required By BusinessBase
        protected override object GetIdValue()
        {
            return _id;
        }
        #endregion

        #region Properties
        public int Id
        {
            get
            {
                CanReadProperty("Id", true);
                return _id;
            }
        }

        public string OwnerHandle
        {
            get
            {
                CanReadProperty("OwnerHandle", true);

                return _ownerHandle;
            }
            set
            {
                CanWriteProperty("OwnerHandle", true);

                _ownerHandle = value;

                PropertyHasChanged("OwnerHandle");
            }
        }

        public int NumberOfPricesRequiredForPricingGauge
        {
            get
            {
                CanReadProperty("NumberOfPricesRequiredForPricingGauge", true);

                return _numberOfPricesRequiredForPricingGauge;
            }
            set
            {
                CanWriteProperty("NumberOfPricesRequiredForPricingGauge", true);

                if (NumberOfPricesRequiredForPricingGauge != value)
                {
                    _numberOfPricesRequiredForPricingGauge = value;

                    PropertyHasChanged("NumberOfPricesRequiredForPricingGauge");
                }
            }
        }

        public bool UseCustomCurrentInternetPrice
        {
            get
            {
                CanReadProperty("UseCustomCurrentInternetPrice", true);

                return _useCustomCurrentInternetPrice;
            }
            set
            {
                CanWriteProperty("UseCustomCurrentInternetPrice", true);

                if (UseCustomCurrentInternetPrice != value)
                {
                    _useCustomCurrentInternetPrice = value;

                    PropertyHasChanged("UseCustomCurrentInternetPrice");
                }
            }
        }

        public string CurrentInternetPriceDisplayName
        {
            get
            {
                CanReadProperty("CurrentInternetPriceDisplayName", true);

                return _currentInternetPriceDisplayName;
            }
            set
            {
                CanWriteProperty("CurrentInternetPriceDisplayName", true);

                _currentInternetPriceDisplayName = value;

                PropertyHasChanged("CurrentInternetPriceDisplayName");
            }
        }

        public bool UseCustomOfferPrice
        {
            get
            {
                CanReadProperty("UseCustomOfferPrice", true);

                return _useCustomOfferPrice;
            }
            set
            {
                CanWriteProperty("UseCustomOfferPrice", true);

                if (UseCustomOfferPrice != value)
                {
                    _useCustomOfferPrice = value;

                    PropertyHasChanged("UseCustomOfferPrice");
                }
            }
        }

        public string OfferPriceDisplayName
        {
            get
            {
                CanReadProperty("OfferPriceDisplayName", true);

                return _offerPriceDisplayName;
            }
            set
            {
                CanWriteProperty("OfferPriceDisplayName", true);

                if (!Equals(OfferPriceDisplayName, value))
                {
                    _offerPriceDisplayName = value;

                    PropertyHasChanged("OfferPriceDisplayName");
                }
            }
        }

        public bool UseCustomComparisonPricePriority
        {
            get
            {
                CanReadProperty("UseCustomComparisonPricePriority", true);

                return _useCustomComparisonPricePriority;
                
            }
            set
            {
                CanWriteProperty("UseCustomComparisonPricePriority", true);

                if (!Equals(OfferPriceDisplayName, value))
                {
                    _useCustomComparisonPricePriority = value;

                    PropertyHasChanged("UseCustomComparisonPricePriority");
                }
            }

        }

        public List<PriceProvider> PriceProviders
        {
            get
            {
                CanReadProperty("PriceProviders", true);

                return _priceProviders;
            }

            set
            {
                CanWriteProperty("PriceProviders", true);
                if (!Equals(PriceProviders, value))
                {
                    _priceProviders = value;

                    PropertyHasChanged("PriceProviders");
                }                
            }
        }

        public string LogoUrl
        {
            get
            {
                CanReadProperty("LogoUrl", true);

                return _logoUrl;
            }
        }


        #endregion

        #region Injected dependencies

        public IMarketAnalysisDealerPreferenceDataAccess MarketAnalysisDataAccess { get; set; }

        #endregion

        #region Data Access

        private void Fetch(MarketAnalysisDealerPreferenceTO preference)
        {
            FromTransferObject(preference);
            MarkOld();
        }

        private void FromTransferObject(MarketAnalysisDealerPreferenceTO preferenceTO)
        {
            _id = preferenceTO.Id;
            _ownerHandle = preferenceTO.OwnerHandle;
            _numberOfPricesRequiredForPricingGauge = preferenceTO.NumberOfPricesRequiredForPricingGauge;
            _useCustomCurrentInternetPrice = preferenceTO.UseCustomCurrentInternetPrice;
            _currentInternetPriceDisplayName = preferenceTO.CurrentInternetPriceDisplayName;
            _useCustomOfferPrice = preferenceTO.UseCustomOfferPrice;
            _offerPriceDisplayName = preferenceTO.OfferPriceDisplayName;
            _useCustomComparisonPricePriority = preferenceTO.UseCustomComparisonPricePriority;
            _priceProviders = preferenceTO.PriceProviders;
            _logoUrl = preferenceTO.LogoUrl;

        }

        public MarketAnalysisDealerPreferenceTO ToTransferObject()
        {
            MarketAnalysisDealerPreferenceTO preferenceTO = new MarketAnalysisDealerPreferenceTO();

            preferenceTO.Id = _id;
            preferenceTO.OwnerHandle = _ownerHandle;
            preferenceTO.NumberOfPricesRequiredForPricingGauge = _numberOfPricesRequiredForPricingGauge;
            preferenceTO.UseCustomCurrentInternetPrice = _useCustomCurrentInternetPrice;
            preferenceTO.CurrentInternetPriceDisplayName = _currentInternetPriceDisplayName;
            preferenceTO.UseCustomOfferPrice = _useCustomOfferPrice;
            preferenceTO.OfferPriceDisplayName = _offerPriceDisplayName;
            preferenceTO.UseCustomComparisonPricePriority = _useCustomComparisonPricePriority;
            preferenceTO.PriceProviders = _priceProviders;
            preferenceTO.LogoUrl = _logoUrl;
            

            return preferenceTO;
        }

        public MarketAnalysisDealerPreferenceTO GetTransferObject()
        {
            return ToTransferObject();
        }

        public static bool Exists(string ownerHandle)
        {
            return ExistsCommand.Execute(ownerHandle);
        }

        #endregion

        #region Factory Methods

        public static MarketAnalysisDealerPreference GetOrCreateMarketAnalysisDealerPreference(string ownerHandle)
        {
            if (Exists(ownerHandle))
            {
                return DataPortal.Fetch<MarketAnalysisDealerPreference>(new Criteria(ownerHandle));
            }
            return DataPortal.Create<MarketAnalysisDealerPreference>(new Criteria(ownerHandle));
        }
        #endregion

        #region DataPortal methods

        protected void DataPortal_Create(Criteria criteria)
        {
            // this is used to create a new preference for an owner, based off the sytem preference.
            MarketAnalysisDealerPreferenceTO dto = MarketAnalysisDataAccess.Create(criteria.OwnerHandle);
            Fetch(dto);
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (ITransaction transaction = MarketAnalysisDataAccess.BeginTransaction())
            {
                MarketAnalysisDataAccess.Update(transaction, ToTransferObject());
                transaction.Commit();
            }
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Insert()
        {
            using (ITransaction transaction = MarketAnalysisDataAccess.BeginTransaction())
            {
                MarketAnalysisDataAccess.Insert(transaction, OwnerHandle, ToTransferObject());
                transaction.Commit();
            }
        }
        
        [Transactional(TransactionalTypes.Manual)]
        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            MarketAnalysisDealerPreferenceTO dto = MarketAnalysisDataAccess.Fetch(criteria.OwnerHandle);
            Fetch(dto);
        }

        #endregion

        #region Public Static Methods
        public static string GetDealerPreferredPriceProviderDescription(PriceProvider provider, IMarketAnalysisDealerPreference preference)
        {
            // use the dealer override text for the current internet price (based on the dealer preference)
            if ((provider.PriceProviderId == MarketAnalysisPriceProviders.CurrentInternetPrice) && preference.UseCustomCurrentInternetPrice)
            {
                return preference.CurrentInternetPriceDisplayName;
            }

            // get the default text description for the price provider (KBB, NADA, etc.)
            return provider.Description;
        }
        #endregion

        [Serializable]
        private class ExistsCommand : InjectableCommandBase
        {
            #region Injected dependencies

            public IMarketAnalysisDealerPreferenceDataAccess MarketAnalysisDataAccess { get; set; }

            #endregion

            public static bool Execute(string ownerHandle)
            {
                ExistsCommand command = new ExistsCommand(ownerHandle);
                DataPortal.Execute(command);
                return command._exists;
            }

            private readonly string _ownerHandle;
            private bool _exists;

            private ExistsCommand(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            protected override void DataPortal_Execute()
            {
                _exists = MarketAnalysisDataAccess.Exists(_ownerHandle);
            }
        }

        internal class InsertPriceProviderRankCommand : InjectableCommandBase
        {
            #region Injected dependencies
            
            public IMarketAnalysisDealerPreferenceDataAccess MarketAnalysisDataAccess { get; set; }
            
            #endregion

            public static void Execute(string ownerHandle, MarketAnalysisPriceProviders provider, int rank, int varianceAmount, int varianceDirection)
            {
                InsertPriceProviderRankCommand command = new InsertPriceProviderRankCommand(ownerHandle, provider, rank, varianceAmount, varianceDirection);
                DataPortal.Execute(command);
            }

            private readonly MarketAnalysisPriceProviders _provider;
            private readonly string _ownerHandle;
            private readonly int _rank;
            private readonly int _varianceAmount;
            private readonly int _varianceDirection;

            private InsertPriceProviderRankCommand(string ownerHandle, MarketAnalysisPriceProviders provider, int rank, int varianceAmount, int varianceDirection)
            {
                _ownerHandle = ownerHandle;
                _provider = provider;
                _rank = rank;
                _varianceAmount = varianceAmount;
                _varianceDirection = varianceDirection;
            }

            protected override void DataPortal_Execute()
            {
                using (ITransaction transaction = MarketAnalysisDataAccess.BeginTransaction())
                {
                    MarketAnalysisDataAccess.InsertPriceProviderRank(transaction, _ownerHandle, _provider, _rank, _varianceAmount, _varianceDirection);
                    transaction.Commit();
                }
            } 
        }


        internal class ChangePriceProviderRankCommand : InjectableCommandBase
        {
            #region Injected dependencies

            public IMarketAnalysisDealerPreferenceDataAccess MarketAnalysisDataAccess { get; set; }

            #endregion

            public static void Execute(int ownerPriceProviderId, int newRank, int varianceAmount, int varianceDirection)
            {
                ChangePriceProviderRankCommand command = new ChangePriceProviderRankCommand(ownerPriceProviderId, newRank, varianceAmount, varianceDirection);
                DataPortal.Execute(command);
            }

            private readonly int _ownerPriceProviderId;
            private readonly int _newRank;
            private readonly int _varianceAmount;
            private readonly int _varianceDirection;

            private ChangePriceProviderRankCommand(int ownerPriceProviderId, int newRank, int varianceAmount, int varianceDirection)
            {
                _ownerPriceProviderId = ownerPriceProviderId;
                _newRank = newRank;
                _varianceAmount = varianceAmount;
                _varianceDirection = varianceDirection;
            }

            protected override void DataPortal_Execute()
            {
                using (ITransaction transaction = MarketAnalysisDataAccess.BeginTransaction())
                {
                    MarketAnalysisDataAccess.ChangePriceProviderRank(transaction, _ownerPriceProviderId, _newRank, _varianceAmount, _varianceDirection);
                    transaction.Commit();
                }

            }            
        }

        [Serializable]
        protected class Criteria
        {
            private string _ownerHandle;

            public Criteria(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            public string OwnerHandle
            {
                get
                {
                    return _ownerHandle;
                }

                set
                {
                    _ownerHandle = value;
                }
            }
        }
    }


}