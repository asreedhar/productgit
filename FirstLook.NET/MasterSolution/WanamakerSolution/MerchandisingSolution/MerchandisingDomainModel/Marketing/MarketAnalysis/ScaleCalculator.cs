using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    /// <summary>
    /// Perform scale calculations for the "worm".
    /// </summary>
    class ScaleCalculator
    {
        // The outlier determination is largely based on what you plan to do with the numbers if an outlier is identified.
        // Based on careful study of the provided screenshots and "worm" layout discussions, these thresholds seem to
        // make sense.  If you have 4 points, 0.5 is the threshold.  If you have 3 points, 1/3 is the threshold.
        private const double THREE_OUTLIER_THRESHOLD = 1d/3d;
        private const double FOUR_OUTLIER_THRESHOLD = 0.5;

        private readonly double _pixelRange;
        private readonly MathUtilities _math;
        private double? _breakPositionInPixels;

        internal ScaleCalculator(int pixelRange)
        {
            if( pixelRange == 0 )
            {
                throw new ApplicationException("Pixels cannot be zero.");
            }
                       
            _math = new MathUtilities();

            _pixelRange = pixelRange;

            _breakPositionInPixels = null;
        }

        internal int? BreakPositionInPixels
        {
            get
            {
                return (int?) _breakPositionInPixels;
            }
        }

        internal IList<double> CalculateScaledPixelPostions(IList<double> dollarAmounts, int maxPixels, bool ignoreLastAmount)
        {
            IList<double> scaledPrices = new List<double>();
            IList<double> pixelPositions = new List<double>();

            double min = dollarAmounts[0];
            

            if( ignoreLastAmount && dollarAmounts.Count < 2)
            {
                throw new ApplicationException("At least two prices are needed to perform this calculation.");
            }


            foreach( double price in dollarAmounts )
            {
                // We need to subtract the min price amount from every price, thus allowing us to scale
                // the prices over a new range from 0 thru maxPixels.
                double scaledPrice = ScaleToMin(price, min);
                scaledPrices.Add(scaledPrice);
            }

            // Calculate scaled max price based on scaled prices.  We might need to overlook the last amount.
            int scaledMaxIndex = ignoreLastAmount ? scaledPrices.Count - 2 : scaledPrices.Count - 1;
            double scaledMaxPrice = scaledPrices[scaledMaxIndex];

            for (int i = 0; i <= scaledMaxIndex; i++ )
            {
                // watch out for divide-by-zero scenarios
                if( scaledMaxPrice == 0 )
                {
                    pixelPositions.Add(0);
                    continue;
                }

                double scaledPrice = scaledPrices[i];
                double pixelPosition = (scaledPrice / scaledMaxPrice) * maxPixels;
                pixelPositions.Add(pixelPosition);
            }

            return pixelPositions;
        }

        private static double ScaleToMin(double price, double min)
        {
            return price - min;
        }
        
        internal IList<int> CalculatePixelPostions(IList<int> dollarAmounts)
        {
            VerifyNonNegative(dollarAmounts);
            VerifyMoreThanOnePrice(dollarAmounts);             
            VerifyNonDescending(dollarAmounts);
            VerifyNotAllSameNumber(dollarAmounts);

            IList<double> pixelPositions = new List<double>();
            if (dollarAmounts.Count == 0)
            {
                return GetInts(pixelPositions);
            }

            // Copy our input to a list of doubles to avoid lots of casting.
            IList<double> doubles = GetDoubles(dollarAmounts);

            double? breakPosition = null;
            double ceiling = _pixelRange;

            // Check for breaks.
            switch (doubles.Count)
            {
                case 3:
                    if (OutlierExists(doubles))
                    {
                        ceiling = Thirds(1);
                        breakPosition = Thirds(2);
                    }
                    break;
                case 4:
                    if (OutlierExists(doubles))
                    {
                        // position the break at 3/4 full scale
                        breakPosition = Fourths(3);

                        // The ceiling is 50% of full scale.
                        ceiling = Fourths(2);
                    }
                    break;
            }

            // Scale everything now.
            bool ignoreLastPrice = breakPosition.HasValue;

            IList<double> scaledPixelPositions = CalculateScaledPixelPostions(doubles, (int) ceiling, ignoreLastPrice );

            // If we asked the scale calculation to ignore the last price, we need to manually add it.  It will be placed at full scale.
            if( ignoreLastPrice )
            {
                scaledPixelPositions.Add(_pixelRange);
            }

            // Assign the break position, if one exists.
            _breakPositionInPixels = breakPosition;

            return GetInts(scaledPixelPositions);

        }

       


        private double Thirds(int multiplier)
        {
            if( multiplier > 3)
            {
                throw new ApplicationException("Only thirds less than one are supported.");
            }

            return (_pixelRange * multiplier) / 3;
        }

        private double Fourths(int multiplier)
        {
            if (multiplier > 4)
            {
                throw new ApplicationException("Only fourths less than one are supported.");
            }

            return (_pixelRange * multiplier) / 4;
        }

        private bool OutlierExists(IList<double> numbers)
        {
            
            if(numbers.Count == 3)
            {
                return _math.IsLargestNumberAnOutlier(numbers, THREE_OUTLIER_THRESHOLD);
            }
            
            if( numbers.Count == 4)
            {
                return _math.IsLargestNumberAnOutlier(numbers, FOUR_OUTLIER_THRESHOLD);                
            }
            
            throw new ApplicationException("Only lists of length 3 and 4 are supported.");                        
        }

        private static void VerifyNotAllSameNumber(IList<int> amounts)
        {
            if( amounts.Count == 0) return;

            int first = amounts[0];

            foreach( int num in amounts )
            {
                // If the number changed, we're done.
                if( num != first ) return;
            }

            throw new ApplicationException("It is impossible to plot prices on the gauge when they are all equal.");
        }

        private static void VerifyNonDescending(IEnumerable<int> numbers)
        {
            int last = 0;
            foreach(int num in numbers)
            {
                if( num < last )
                {
                    throw new ApplicationException("The numbers must be in non-descending order.");
                }
                last = num;
            }
        }

        private static void VerifyNonNegative(IEnumerable<int> numbers)
        {
            foreach(int num in numbers)
            {
                if (num < 0)
                    throw new ApplicationException("All prices must be non-negative.");
            }
        }

        private static void VerifyMoreThanOnePrice(ICollection<int> amounts)
        {
            if( amounts.Count == 1 )
            {
                throw new ApplicationException("At least two prices are needed to perform gauge scale calculations.");
            }
        }
      

        private static IList<double> GetDoubles(IEnumerable<int> ints)
        {
            IList<double> doubles = new List<double>();
            foreach (int num in ints)
            {
                doubles.Add( num );
            }
            return doubles;
        }

        private static IList<int> GetInts(IEnumerable<double> doubles)
        {
            IList<int> ints = new List<int>();
            foreach (int num in doubles)
            {
                ints.Add(num);
            }
            return ints;
        }

    }
}
