﻿using System;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    [Serializable]
    public class PriceProvider
    {
        private int _id;
        private readonly MarketAnalysisPriceProviders _priceProviderId;
        private string _description;
        private int _rank;
        private string _logoUrl;
        private MarketAnalysisVarianceDirection _varianceDirection;
        private int _varianceDollarAmount;

        public PriceProvider(int id, MarketAnalysisPriceProviders priceProviderId, string description, int rank, string logoUrl) :
            this(id, priceProviderId, description, rank, logoUrl, 1, MarketAnalysisVarianceDirection.Below)
        {
            
        }

        public PriceProvider(int id, MarketAnalysisPriceProviders priceProviderId, string description, int rank, string logoUrl, 
            int varianceDollarAmount, MarketAnalysisVarianceDirection varianceDirection)
        {
            _id = id;
            _priceProviderId = priceProviderId;
            _description = description;
            _rank = rank;
            _logoUrl = logoUrl;
            _varianceDollarAmount = varianceDollarAmount;
            _varianceDirection = varianceDirection;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public MarketAnalysisPriceProviders PriceProviderId
        {
            get { return _priceProviderId; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public int Rank
        {
            get { return _rank; }
            set { _rank = value; }
        }

        public string LogoUrl
        {
            get { return _logoUrl; }
            set { _logoUrl = value; }
        }

        public int VarianceDollarAmount
        {
            get { return _varianceDollarAmount; }
            set { _varianceDollarAmount = value; }
        }

        public MarketAnalysisVarianceDirection VarianceDirection
        {
            get { return _varianceDirection; }
            set { _varianceDirection = value; }
        }


        /// <summary>
        /// Returns the url to use for a given price provider.
        /// </summary>
        /// <param name="provider">provider type</param>
        /// <param name="dealerUrl">Dealer logo url for market analysis</param>
        /// <returns></returns>
        internal static string GetLogoUrl(MarketAnalysisPriceProviders provider, string dealerUrl)
        {
            PhotoLocator photoLocator = new PhotoLocator();
            string fullyQualifiedDealerUrl = String.Empty;

            if (dealerUrl.Length > 0)
            {
                fullyQualifiedDealerUrl = photoLocator.RemoteImageRootUrl + dealerUrl;
            }

            switch (provider)
            {
                // TODO: Remove pricing prefixes
                case MarketAnalysisPriceProviders.CurrentInternetPrice:
                    return fullyQualifiedDealerUrl;

                case MarketAnalysisPriceProviders.EdmundsTrueMarketValue:
                    return photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.EdmundsTrueMarketValue);

                case MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice:
                    return photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.JdPowerPinAverageInternetPrice);

                case MarketAnalysisPriceProviders.KelleyBlueBookRetailValue:
                    return photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.KelleyBlueBookRetailValue);

                case MarketAnalysisPriceProviders.NADARetailValue:
                    return photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.NadaRetailValue);

                case MarketAnalysisPriceProviders.OfferPrice:
                    return fullyQualifiedDealerUrl;

                case MarketAnalysisPriceProviders.OriginalMSRP:
                    return fullyQualifiedDealerUrl;

                case MarketAnalysisPriceProviders.PINGMarketAverageInternetPrice:
                    return photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.PingMarketAverageInternetPrice);

                case MarketAnalysisPriceProviders.BlackBook:
                    return photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.BlackBook);

                case MarketAnalysisPriceProviders.Galves:
                    return photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.Galves);
            }

            return String.Empty;
        }
    }
}
