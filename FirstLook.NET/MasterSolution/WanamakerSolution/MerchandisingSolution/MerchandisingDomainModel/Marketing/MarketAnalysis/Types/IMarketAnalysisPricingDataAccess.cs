using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types
{
    public interface IMarketAnalysisPricingDataAccess
    {
        MarketAnalysisPricingInformationTO GetPricingInformation(string ownerHandle, string vehicleHandle,
                                                                  string searchHandle);
    }
}
