namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types
{
    interface IMarketAnalysisDataAccess
    {
        string OwnerHandle
        { get; set;}

        string VehicleHandle
        { get; set;}

        string SearchHandle
        { get; set;}

        int? EdmundsTMVPrice{ get; }
        int? NADAPrice{ get; }
        int? JDPowerPrice { get; }
        int? PINGPrice { get; }
        int? KBBPrice { get; }
    }
}
