namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types
{
    public interface IMarketAnalysisPricingInformation
    {
        string OwnerHandle { get;}
        string VehicleHandle { get; }
        string SearchHandle { get; }
        int? ListPrice { get;}
        bool? HasMarketPrice { get;}
        int? MarketPrice { get;}
        bool? HasKbbPrice { get;}
        int? KbbPrice { get;}
        bool? KbbPriceIsAccurate { get;}
        bool? HasNadaPrice { get;}
        int? NadaPrice { get;}
        bool? NadaPriceIsAccurate { get;}
        bool? HasEdmundsPrice { get;}
        int? EdmundsPrice { get;}
        bool? HasJdPowerPrice { get;}
        int? JdPowerPrice { get;}
        int? OriginalMSRP { get; set; }
        bool? HasBlackBookPrice { get; }
        int? BlackBookPrice { get; }
        bool? BlackBookPriceIsAccurate { get; }
        bool? HasGalvesPrice { get; }
        int? GalvesPrice { get; }
        bool? GalvesPriceIsAccurate { get; }
        int DealerId { get; }
        bool HasKbbAsBook { get; }
        bool HasKbbConsumerTool { get; }
        string StockNumber { get; }
        int VehicleCatalogId { get; }
        int ModelYear { get; }
    }
}
