using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types
{
    public interface IMarketAnalysisDealerPreferenceDataAccess
    {
        ITransaction BeginTransaction();

        bool Exists(string ownerHandle);

        MarketAnalysisDealerPreferenceTO Create(string ownerHandle);

        MarketAnalysisDealerPreferenceTO Fetch(string ownerHandle);

        void Insert(ITransaction transaction, string ownerHandle, MarketAnalysisDealerPreferenceTO dealerPreference);

        void Update(ITransaction transaction, MarketAnalysisDealerPreferenceTO dealerPreference);

        void ChangePriceProviderRank(ITransaction transaction, int OwnerPriceProviderId, int newRank, int varianceAmount, int varianceDirection);

        void InsertPriceProviderRank(ITransaction transaction, string ownerHandle, MarketAnalysisPriceProviders provider,
                                     int rank, int varianceAmount, int varianceDirection);
    }
}