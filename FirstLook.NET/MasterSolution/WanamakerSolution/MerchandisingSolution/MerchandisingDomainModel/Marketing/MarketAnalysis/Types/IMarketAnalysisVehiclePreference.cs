using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types
{
    public interface IMarketAnalysisVehiclePreference
    {
        int MarketAnalysisVehiclePreferenceId { get;}
        string OwnerHandle { get;}
        string VehicleHandle { get; }
        bool IsDisplayed { get; set;}
        bool IsGaugeDisplayed { get; set;}
        bool HasOverriddenPriceProviders { get; set; }
        IEnumerable<MarketAnalysisPriceProviders> SelectedPriceProviders { get; set; }
    }
}
