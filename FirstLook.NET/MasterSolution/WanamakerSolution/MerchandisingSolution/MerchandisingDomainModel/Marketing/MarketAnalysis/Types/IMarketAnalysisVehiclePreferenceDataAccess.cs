
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types
{
    public interface IMarketAnalysisVehiclePreferenceDataAccess
    {

        bool Exists(string ownerHandle, string vehicleHandle);

        MarketAnalysisVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle);

        MarketAnalysisVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle);

        /// <summary>
        /// Insert a new preference.
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        void Insert(MarketAnalysisVehiclePreferenceTO preference, string userName);

        /// <summary>
        /// Update an existing preference
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        void Update(MarketAnalysisVehiclePreferenceTO preference, string userName);


        /// <summary>
        /// Delete the preference.
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="userName"></param>
        void Delete(MarketAnalysisVehiclePreferenceTO preference, string userName);

    }

}
