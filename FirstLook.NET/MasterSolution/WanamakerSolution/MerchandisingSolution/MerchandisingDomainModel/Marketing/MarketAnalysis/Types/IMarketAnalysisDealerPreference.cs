﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types
{
    public interface IMarketAnalysisDealerPreference
    {
        int Id { get; }
        string OwnerHandle { get; set; }
        int NumberOfPricesRequiredForPricingGauge { get; set; }
        bool UseCustomCurrentInternetPrice { get; set; }
        string CurrentInternetPriceDisplayName { get; set; }
        bool UseCustomOfferPrice { get; set; }
        string OfferPriceDisplayName { get; set; }
        bool UseCustomComparisonPricePriority { get; set; }
        List<PriceProvider> PriceProviders { get; set; }
        string LogoUrl { get; }
    }
}
