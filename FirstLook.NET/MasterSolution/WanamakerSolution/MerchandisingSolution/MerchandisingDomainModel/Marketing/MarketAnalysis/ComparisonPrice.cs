﻿
namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    public class ComparisonPrice
    {
        private readonly PriceProvider _priceProvider;
        private readonly int _dollars;
        private readonly int _delta;
        private readonly string _text;
        private readonly bool _selected;
        private readonly bool _isAccurate;

        public ComparisonPrice(PriceProvider priceProvider, int dollars, int delta, string text, bool selected, bool isAccurate)
        {

            _priceProvider = priceProvider;
            _dollars = dollars;
            _delta = delta;
            _text = text;
            _selected = selected;
            _isAccurate = isAccurate;
        }

        internal int Dollars
        {
            get { return _dollars; }
        }

        internal string Text
        {
            get { return _text; }
        }

        internal int Delta
        {
            get { return _delta; }
        }

        internal string LogoUrl
        {
            get { return _priceProvider.LogoUrl; }
        }

        internal int Rank
        {
            get
            {
                return _priceProvider.Rank;
            }
        }

        internal MarketAnalysisPriceProviders PriceProviderId
        {
            get { return _priceProvider.PriceProviderId; }
        }

        public bool Selected { 
            get { return _selected; }
        }

        internal bool IsAccurate
        {
            get { return _isAccurate; }
        }

    }
}
