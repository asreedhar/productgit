using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    public class PriceProcessor
    {
        private const int DEFAULT_PRICE = 0;

        #region Private Member Variables
        private readonly IMarketAnalysisDealerPreference _dealerPreference;
        private readonly IMarketAnalysisVehiclePreference _vehiclePreference;
        private readonly IMarketAnalysisPricingInformation _pricingInformation;
        private readonly int _offerPrice;
        private int? _breakPosition = 0;
        private bool _showPricingGauge = true;
        #endregion

        #region Constants
        internal const int MaxNumberOfPrices = 4; // 1 offer price and 3 comparison prices.
        internal const int GaugeItemPadding = 10;
        internal const int GaugeMaxWidth = 530;
        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new PriceProcessor based on a preference, pricing data, and an offer price.
        /// </summary>
        /// <param name="preference">The dealer preference</param>
        /// <param name="vehiclePreference">The vehicle preference</param>
        /// <param name="pricingInformation">The pricing information for a vehicle.</param>
        /// <param name="offerPrice">The offer price to use for the gauge.</param>
        public PriceProcessor(IMarketAnalysisDealerPreference preference, IMarketAnalysisVehiclePreference vehiclePreference,  IMarketAnalysisPricingInformation pricingInformation, int offerPrice)
        {
            _dealerPreference = preference;
            _vehiclePreference = vehiclePreference;
            _pricingInformation = pricingInformation;
            _offerPrice = offerPrice;
            ComparisonPrices = new List<ComparisonPrice>();
            GaugePrices = new List<GaugePrice>();
        }


        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a list of prices for the gauge.
        /// </summary>
        public List<GaugePrice> GaugePrices { get; private set; }

        /// <summary>
        /// Gets a list of comparison prices (not just those that will appear on the gauge)
        /// </summary>
        public List<ComparisonPrice> ComparisonPrices { get; private set; }

        /// <summary>
        /// Gets a value that indicates where a scale-break should be placed on the gauge.  If null, no scale-break is needed.
        /// </summary>
        public int? BreakPosition
        {
            get
            {
                return _breakPosition;
            }
        }

        /// <summary>
        /// Gets a value that indicates whether or not the gauge can be displayed.
        /// </summary>
        public bool ShowGauge
        {
            get
            {
                return _showPricingGauge;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Prepares the processors GaugePrices.  Builds a filtered list of prices (offer price and those that exceed the offer price); 
        /// removes excess prices based on the gauge capacity; if enough prices are present, assigns gauge positions for prices, and sorts 
        /// the prices according to the dealer or system preference.
        /// </summary>
        public void ProcessPrices()
        {
            if ((_dealerPreference != null) && (_pricingInformation != null))
            {
                LoadComparisonPrices();
                LoadGaugePrices();
                RemoveExcessPrices();

                int comparisonPriceCount = GaugePrices.Count - 1;   // we always have offer price, all others are "comparison" prices

                if (
                    comparisonPriceCount > 0 &&
                    _vehiclePreference.IsGaugeDisplayed &&
                    comparisonPriceCount >= _dealerPreference.NumberOfPricesRequiredForPricingGauge
                    )
                {
                    _showPricingGauge = true;

                    // all of this only matters if we're going to display the gauge.
                    CheckOverallWidth(GaugePrices);

                    // sort the prices by ascending dollar value for the sake of gauge positioning
                    GaugePrices.Sort(GaugePrice.ComparePricesByAscendingDollarValue);

                    AssignGaugePositions();

                    // re-sort the prices according to the system default or preference.
                    SortPricesAccordingToPreference();
                }
                else
                {
                    _showPricingGauge = false;
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Assigns the gauge positions for any price in GaugePrices and corrects overlapping prices.
        /// </summary>
        private void AssignGaugePositions()
        {

            // create our list of prices
            var prices = new List<int>();
            foreach (GaugePrice price in GaugePrices)
            {
                prices.Add(price.Dollars);
            }

            // use the scale calculator to calculate the position (in pixels) of each price in the list.
            var calculator = new ScaleCalculator(GaugeMaxWidth);
            var positions = calculator.CalculatePixelPostions(prices);
            _breakPosition = calculator.BreakPositionInPixels;

            // if we get a break position, make sure it doesn't equal any other position
            if (_breakPosition.HasValue)
            {
                foreach (int i in positions)
                {
                    if (i == _breakPosition.Value) throw new ApplicationException("The break position matched another position returned.");
                }
            }

            // set the gauge positions for our prices from the values returned by the calculator
            for (int i = 0; i < positions.Count; i++)
            {
                //set the position of the price such that the halfway point of the price aligns with the ideal position obtained from the calculator.
                GaugePrices[i].GaugePosition = positions[i] - GaugePrices[i].Width / 2;
            }

            // adjust the gauge prices for overlap
            if (GaugePrices.Count == 3) AdjustForMiddlePriceOverlaps3Prices();
            if (GaugePrices.Count == 4) AdjustForMiddlePriceOverlaps4Prices();
        }



        /// <summary>
        /// Fixes price overlaps when there are 3 prices
        /// </summary>
        private void AdjustForMiddlePriceOverlaps3Prices()
        {
            GaugePrice first = GaugePrices[0];
            GaugePrice middle = GaugePrices[1];
            GaugePrice last = GaugePrices[2];

            // move the middle price if it overlaps the first price.
            if (PricesOverlap(first, middle))
            {
                MovePriceToRightofOtherPrice(middle, first);
            }

            // move the middle price if it overlaps the last price.
            if (PricesOverlap(middle, last))
            {
                MovePriceToLeftofOtherPrice(middle, last);
            }
        }

        /// <summary>
        /// Fixes price overlaps when there are 4 prices
        /// </summary>
        private void AdjustForMiddlePriceOverlaps4Prices()
        {
            GaugePrice first = GaugePrices[0];
            GaugePrice second = GaugePrices[1];
            GaugePrice third = GaugePrices[2];
            GaugePrice last = GaugePrices[3];

            // move the second price if it overlaps the first price.
            if (PricesOverlap(first, second))
            {
                MovePriceToRightofOtherPrice(second, first);
            }

            // move the third price if it overlaps the last price.
            if (PricesOverlap(third, last))
            {
                MovePriceToLeftofOtherPrice(third, last);
            }

            //
            // now, all of the prices are in the middle (perhaps overlapping each other)
            //

            if (PricesOverlap(second, third))
            {
                // yep, they overlap

                // determine how much they overlap
                int overlapAmount =  second.RightEdge + GaugeItemPadding - third.LeftEdge;

                // determine how much space we have to the left of the second price
                int leftSpace = second.LeftEdge - first.RightEdge - GaugeItemPadding;

                // determine how much space we have to the right of the third price
                int rightSpace = last.LeftEdge - third.RightEdge - GaugeItemPadding;

                // if the combined amount of space is not at least as much as the overlap, we have a big problem...
                if (leftSpace + rightSpace < overlapAmount)
                {
                    throw new ApplicationException("we can't show the gauge because there isn't enough room to display all of the prices without overlapping.");
                }

                // we can fix the overlap.  Let's try to move each price half the overlap amount
                // determine how much we will move the second price
                int moveSecond = overlapAmount/2;
                // determine how much we will move the third price
                int moveThird = overlapAmount - moveSecond; // hedge against rounding issues

                if ((leftSpace < moveSecond) || (rightSpace < moveThird))
                {   // we don't have enough room on one or the other sides

                    if (leftSpace < moveSecond)
                    {   // we don't have enough room on the left, take as much as we can
                        moveSecond = leftSpace;
                        moveThird = overlapAmount - moveSecond;
                    }
                    else
                    {   // we have enough room on the left, so we must be short on the right.
                        moveThird = rightSpace;
                        moveSecond = overlapAmount - moveThird;
                    }
                }
                
                // adjust the positions of the middle prices
                second.GaugePosition -= moveSecond;
                third.GaugePosition += moveThird;
            }
        }

        /// <summary>
        /// Lookup the price provider's description by PriceProviderId
        /// </summary>
        /// <param name="priceProviderId">The Price Provider's PriceProviderId</param>
        /// <returns>The Price Provider's Description</returns>
        private string GetPriceDescription(MarketAnalysisPriceProviders priceProviderId)
        {
            // This is a total hack...the offer price and other price-providers could benefit from some refactoring.
            if (priceProviderId == MarketAnalysisPriceProviders.OfferPrice) return "Your Price";

            foreach (PriceProvider provider in _dealerPreference.PriceProviders)
            {
                if (provider.PriceProviderId == priceProviderId)
                    return provider.Description;
            }

            throw new ApplicationException("The priceProviderId supplied was not found.");
        }

        /// <summary>
        /// Lookup the price provider's price by PriceProviderId
        /// </summary>
        /// <param name="priceProviderId"></param>
        /// <returns>Price Provider's price</returns>
        private int? GetPrice(MarketAnalysisPriceProviders priceProviderId)
        {
            switch (priceProviderId)
            {
                case MarketAnalysisPriceProviders.OfferPrice:
                    {
                        return _offerPrice;
                    }
                case MarketAnalysisPriceProviders.CurrentInternetPrice:
                    {
                        return _pricingInformation.ListPrice.GetValueOrDefault(DEFAULT_PRICE);
                    }
                case MarketAnalysisPriceProviders.OriginalMSRP:
                    {
                        return _pricingInformation.OriginalMSRP.GetValueOrDefault(DEFAULT_PRICE);
                    }
                case MarketAnalysisPriceProviders.PINGMarketAverageInternetPrice:
                    {
                        return _pricingInformation.MarketPrice.GetValueOrDefault(DEFAULT_PRICE);
                    }
                case MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice:
                    {
                        if (!_pricingInformation.HasJdPowerPrice.GetValueOrDefault(false)) return null;
                        return _pricingInformation.JdPowerPrice.GetValueOrDefault(DEFAULT_PRICE);
                    }
                case MarketAnalysisPriceProviders.EdmundsTrueMarketValue:
                    {
                        if (!_pricingInformation.HasEdmundsPrice.GetValueOrDefault(false)) return null;
                        return _pricingInformation.EdmundsPrice.GetValueOrDefault(DEFAULT_PRICE);
                    }
                case MarketAnalysisPriceProviders.NADARetailValue:
                    {
                        // If the price is not accurate or null, return 0.  Otherwise return the price.
                        if (!_pricingInformation.HasNadaPrice.GetValueOrDefault(false)) return null;
                        bool accurate = _pricingInformation.NadaPriceIsAccurate.GetValueOrDefault(false);
                        return (accurate ? _pricingInformation.NadaPrice.GetValueOrDefault(DEFAULT_PRICE) : 0 );
                    }
                case MarketAnalysisPriceProviders.KelleyBlueBookRetailValue:
                    {
                        // If the price is not accurate, return 0.  If this property is null or true, return the price.
                        if (!_pricingInformation.HasKbbPrice.GetValueOrDefault(false)) return null;
                        bool accurate = _pricingInformation.KbbPriceIsAccurate.GetValueOrDefault(true);
                        return accurate ? _pricingInformation.KbbPrice.GetValueOrDefault(DEFAULT_PRICE) : 0;
                    }
                case MarketAnalysisPriceProviders.BlackBook:
                    {
                        // If the price is not accurate, return 0.  If this property is null or true, return the price.
                        if (!_pricingInformation.HasBlackBookPrice.GetValueOrDefault(false)) return null;
                        bool accurate = _pricingInformation.BlackBookPriceIsAccurate.GetValueOrDefault(true);
                        return accurate ? _pricingInformation.BlackBookPrice.GetValueOrDefault(DEFAULT_PRICE) : 0;
                    }
                case MarketAnalysisPriceProviders.Galves:
                    {
                        // If the price is not accurate, return 0.  If this property is null or true, return the price.
                        if (!_pricingInformation.HasGalvesPrice.GetValueOrDefault(false)) return null;
                        bool accurate = _pricingInformation.GalvesPriceIsAccurate.GetValueOrDefault(true);
                        return accurate ? _pricingInformation.GalvesPrice.GetValueOrDefault(DEFAULT_PRICE) : 0;
                    }
                default:
                    {
                        throw new ApplicationException("An invalid PriceProviderId was obtained.  PriceProviderId of " + priceProviderId + " is not supported.");
                    }

            }

        }
        #endregion

        #region Internal Methods
        /// <summary>
        /// Load the list of comparison prices
        /// </summary>
        internal void LoadComparisonPrices()
        {
            foreach (PriceProvider provider in _dealerPreference.PriceProviders)
            {
                int? priceInDollars = GetPrice(provider.PriceProviderId);
                
                if (priceInDollars.HasValue)
                {
                    string descriptionText = MarketAnalysisDealerPreference.GetDealerPreferredPriceProviderDescription(provider, _dealerPreference);

                    bool selected = false;

                    if (_vehiclePreference.HasOverriddenPriceProviders)
                    {   // get the overrides from the preference
                        foreach (var selectedProviderId in _vehiclePreference.SelectedPriceProviders)
                        {
                            if (selectedProviderId == provider.PriceProviderId)
                            {
                                selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        selected = (priceInDollars > _offerPrice);
                    }


                    bool isAccurate = true;

                    // set isAccurate to false if we have an inaccurate price (only KBB and NADA support this)
                    if ((provider.PriceProviderId == MarketAnalysisPriceProviders.KelleyBlueBookRetailValue && !_pricingInformation.KbbPriceIsAccurate.GetValueOrDefault(true))
                        || (provider.PriceProviderId == MarketAnalysisPriceProviders.NADARetailValue && !_pricingInformation.NadaPriceIsAccurate.GetValueOrDefault(true)))
                    {
                        isAccurate = false;
                    }

					// instantiate the price
					var comparisonPrice = new ComparisonPrice(provider, priceInDollars.Value,
															  priceInDollars.Value - _offerPrice, descriptionText,
															  selected, isAccurate);

					// add the price to the price list
					ComparisonPrices.Add(comparisonPrice);


                }
            }
        }

        /// <summary>
        /// Loads the GaugePrices with prices that can be used on the gauge (the offer price and those comparison prices that exceed the offer price).
        /// </summary>
        internal void LoadGaugePrices()
        {
/*
            string text = _dealerPreference.UseCustomOfferPrice
                              ? _dealerPreference.OfferPriceDisplayName
                              : GetPriceDescription(MarketAnalysisPriceProviders.OfferPrice);
            GaugePrices.Add(new GaugePrice(MarketAnalysisPriceProviders.OfferPrice, _offerPrice, 0, text, 0, _dealerPreference.LogoUrl));
 */
            // Add the offer price first
            // use the dealer override text for the offer price (based on the dealer preference)
            string text = _dealerPreference.UseCustomOfferPrice
                              ? _dealerPreference.OfferPriceDisplayName
                              : GetPriceDescription(MarketAnalysisPriceProviders.OfferPrice);
            GaugePrices.Add(new GaugePrice(MarketAnalysisPriceProviders.OfferPrice, text, 0, _offerPrice, 0));

            // Next, add the other prices
            foreach (ComparisonPrice comparisonPrice in ComparisonPrices)
            {
                if (comparisonPrice.Dollars > _offerPrice) // it must support our cause (be > the offer price)
                {
                    // add the price to the price list
                    GaugePrices.Add(new GaugePrice(comparisonPrice.PriceProviderId, comparisonPrice.Text,
                                                   comparisonPrice.Rank, comparisonPrice.Dollars,
                                                   comparisonPrice.Dollars - _offerPrice));
                }
            }
        }

        internal void SortPricesAccordingToPreference()
        {
            if (_dealerPreference.UseCustomComparisonPricePriority)
            {
                GaugePrices.Sort(GaugePrice.ComparePricesByRank);
            }
            else
            {
                GaugePrices.Sort(GaugePrice.ComparePricesByOfferThenDescendingDollarValue);
            }

        }

        /// <summary>
        /// Removes any prices that exceed the max number of prices for the gauge.
        /// It removes those with the lowest priority (according to the preference).
        /// </summary>
        internal void RemoveExcessPrices()
        {
            SortPricesAccordingToPreference();

            for (int i = GaugePrices.Count - 1; i > MaxNumberOfPrices - 1; i--)
            {
                GaugePrices.RemoveAt(i);
            }
        }

        /// <summary>
        /// Checks the width of all of the prices in GaugePrices to ensure they will all fit on the gauge.
        /// This is a static method so unit tests can easily be written to test this feature.
        /// </summary>
        internal static void CheckOverallWidth(List<GaugePrice> gaugePrices)
        {
            int overallWidth = 0;

            // sum the item widths
            foreach (GaugePrice price in gaugePrices)
            {
                overallWidth += price.Width;
            }

            // account for padding
            overallWidth += ((gaugePrices.Count - 1) * GaugeItemPadding);

            // check against the max size of the gauge
            if (overallWidth > GaugeMaxWidth)
                throw new ApplicationException("The overall width of the price items exceeds the width of the pricing gauge.");
        }

        internal bool PricesOverlap(GaugePrice leftGaugePrice, GaugePrice rightGaugePrice)
        {
            return leftGaugePrice.RightEdge + GaugeItemPadding > rightGaugePrice.LeftEdge;
        }

        internal void MovePriceToLeftofOtherPrice(GaugePrice gaugePrice, GaugePrice otherGaugePrice)
        {
            gaugePrice.GaugePosition = otherGaugePrice.LeftEdge - GaugeItemPadding - gaugePrice.Width - 1;
        }

        internal void MovePriceToRightofOtherPrice(GaugePrice gaugePrice, GaugePrice otherGaugePrice)
        {
            gaugePrice.GaugePosition = otherGaugePrice.RightEdge + GaugeItemPadding + 1;
        }
        #endregion
    }
}
