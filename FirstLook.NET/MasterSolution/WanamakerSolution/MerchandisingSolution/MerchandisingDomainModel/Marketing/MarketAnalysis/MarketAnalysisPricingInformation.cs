using System;
using System.Linq;
using Csla;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    [Serializable]
    public class MarketAnalysisPricingInformation : InjectableReadOnlyBase<MarketAnalysisPricingInformation>, IMarketAnalysisPricingInformation
    {
        #region Overrides of ReadOnlyBase<MarketAnalysisPricingInformation>
        
        private Criteria _id;
        private bool? _hasKbbAsBook;
        private bool? _hasKbbConsumerTool;
        private int? _dealerId;

        public int DealerId
        {
            get
            {
                return _dealerId.GetValueOrDefault();
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }
        #endregion

        #region Injected dependencies

        public IMarketAnalysisPricingDataAccess PricingDataAccess { get; set;}

        #endregion

        private void FromTransferObject(MarketAnalysisPricingInformationTO pricing)
        {
            ListPrice = pricing.ListPrice;
            HasOriginalListPrice = pricing.HasOriginalListPrice;
            OriginalMSRP = pricing.OriginalMSRP;
            HasMarketPrice = pricing.HasMarketPrice;
            MarketPrice = pricing.MarketPrice;
            HasKbbPrice = pricing.HasKbbPrice;
            KbbPrice = pricing.KbbPrice;
            KbbPriceIsAccurate = pricing.KbbPriceIsAccurate;
            HasNadaPrice = pricing.HasNadaPrice;
            NadaPrice = pricing.NadaPrice;
            NadaPriceIsAccurate = pricing.NadaPriceIsAccurate;
            HasEdmundsPrice = pricing.HasEdmundsPrice;
            EdmundsPrice = pricing.EdmundsPrice;
            HasJdPowerPrice = pricing.HasJdPowerPrice;
            JdPowerPrice = pricing.JdPowerPrice;
            HasBlackBookPrice = pricing.HasBlackBookPrice;
            BlackBookPrice = pricing.BlackBookPrice;
            BlackBookPriceIsAccurate = pricing.BlackBookPriceIsAccurate;
            HasGalvesPrice = pricing.HasGalvesPrice;
            GalvesPrice = pricing.GalvesPrice;
            GalvesPriceIsAccurate = pricing.GalvesPriceIsAccurate;
            StockNumber = pricing.StockNumber;
            VehicleCatalogId = pricing.VehicleCatalogId;
            ModelYear = pricing.ModelYear;
            _hasKbbAsBook = pricing.HasKbbAsBook;
            _hasKbbConsumerTool = pricing.HasKbbConsumerTool;

            _dealerId = pricing.DealerId;

            if (!HasKbbPrice.GetValueOrDefault() && (HasKbbAsBook || HasKbbConsumerTool))
            {
                KelleyBlueBookReport kbbReport = KelleyBlueBookReportCommand.TryFetch(_id.OwnerHandle, _id.VehicleHandle);
                if (kbbReport != null && kbbReport.Valuations.Count > 0)
                {
                    HasKbbPrice = true;
                    KbbPrice =
                        kbbReport.Valuations.GetValue(KelleyBlueBookCategory.Retail,
                                                      KelleyBlueBookCondition.NotApplicable).Value;
                }
            }
        }

        public MarketAnalysisPricingInformationTO ToTransferObject()
        {
            var transerObject = new MarketAnalysisPricingInformationTO(
                                                                      OwnerHandle, VehicleHandle, SearchHandle,
                                                                      ListPrice,
                                                                      HasOriginalListPrice,
                                                                      OriginalMSRP,
                                                                      HasMarketPrice,
                                                                      MarketPrice,
                                                                      HasKbbPrice,
                                                                      KbbPrice,
                                                                      KbbPriceIsAccurate,
                                                                      HasNadaPrice,
                                                                      NadaPrice,
                                                                      NadaPriceIsAccurate,
                                                                      HasEdmundsPrice,
                                                                      EdmundsPrice,
                                                                      HasJdPowerPrice,
                                                                      JdPowerPrice,
                                                                      HasBlackBookPrice,
                                                                      BlackBookPrice,
                                                                      BlackBookPriceIsAccurate,
                                                                      HasGalvesPrice,
                                                                      GalvesPrice,
                                                                      GalvesPriceIsAccurate,
                                                                      DealerId,
                                                                      HasKbbAsBook,
                                                                      HasKbbConsumerTool,
                                                                      StockNumber,
                                                                      VehicleCatalogId,
                                                                      ModelYear);

            return transerObject;
        }

// ReSharper disable InconsistentNaming
        protected void DataPortal_Fetch(Criteria criteria)
// ReSharper restore InconsistentNaming
        {
            MarketAnalysisPricingInformationTO pricingInformation = 
                PricingDataAccess.GetPricingInformation(criteria.OwnerHandle, criteria.VehicleHandle, criteria.SearchHandle);

            _id = criteria;
            FromTransferObject(pricingInformation);
        }

        #region Factory Methods

        public static MarketAnalysisPricingInformation GetMarketAnalysisPricingInformation(string ownerHandle, string vehicleHandle, string searchHandle)
        {
            return DataPortal.Fetch<MarketAnalysisPricingInformation>(new Criteria(ownerHandle, vehicleHandle, searchHandle));
        }

        /// <summary>
        /// Supply a vehicle preference to override certain pricing information.
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="searchHandle"></param>
        /// <returns></returns>
        public static MarketAnalysisPricingInformation GetMarketAnalysisPricingInformation(IMarketAnalysisVehiclePreference preference, string searchHandle)
        {
            // Get pricing information.
            var pricingInformation = GetMarketAnalysisPricingInformation(preference.OwnerHandle,
                                                                         preference.VehicleHandle, searchHandle);

            return pricingInformation;
        }

        #endregion



        [Serializable]
        protected internal class Criteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private readonly string _searchHandle;

            public Criteria(string ownerHandle, string vehicleHandle, string searchHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
                _searchHandle = searchHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }

            public string SearchHandle
            {
                get { return _searchHandle; }
            }

        }

        #region Implementation of IMarketAnalysisPricingInformation

        public string OwnerHandle
        {
            get { return _id.OwnerHandle; }
        }

        public string VehicleHandle
        {
            get { return _id.VehicleHandle; }
        }

        public string SearchHandle
        {
            get { return _id.SearchHandle; }
        }

        public int? ListPrice { get; private set; }

        public bool? HasOriginalListPrice { get; private set; }

        public int? OriginalMSRP { get; set; }

        public int? BlackBookPrice { get; private set; }

        public bool? BlackBookPriceIsAccurate { get; private set; }

        public int? GalvesPrice { get; private set; }

        public bool? GalvesPriceIsAccurate { get; private set; }

        public bool? HasMarketPrice { get; private set; }

        public int? MarketPrice { get; private set; }

        public bool? HasKbbPrice { get; private set; }

        public int? KbbPrice { get; private set; }

        public bool? KbbPriceIsAccurate { get; private set; }

        public bool? HasNadaPrice { get; private set; }

        public int? NadaPrice { get; private set; }

        public bool? NadaPriceIsAccurate { get; private set; }

        public bool? HasEdmundsPrice { get; private set; }

        public int? EdmundsPrice { get; private set; }

        public bool? HasJdPowerPrice { get; private set; }

        public int? JdPowerPrice { get; private set; }

        public bool? HasBlackBookPrice { get; private set; }

        public bool? HasGalvesPrice { get; private set; }

        public bool HasKbbAsBook
        {
            get
            {
                return _hasKbbAsBook.GetValueOrDefault();
            }
        }

        public bool HasKbbConsumerTool
        {
            get
            {
                return _hasKbbConsumerTool.GetValueOrDefault();
            }
        }

        public string StockNumber { get; private set; }

        public int VehicleCatalogId { get; private set; }

        public int ModelYear { get; private set; }

        #endregion
    }
}
