using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    // With the exception of "OfferPrice", this should match the table IMT.Marketing.MarketAnalysisPriceProvider.
    // Offer price was initially added to the database, but subsequently removed.
    // The Market Analysis Price Providers should probably be refactored later.
    //
    // Travis 12/05/2014: I've removed JDPowerPINAverageInternetPrice, BlackBook, and Galves from the database, but
    //                    I'm leaving this Enum unchanged so the ids will still match the existing values.
    [Serializable]
    public enum MarketAnalysisPriceProviders
    {
        OfferPrice = 1,
        CurrentInternetPrice,
        OriginalMSRP,
        PINGMarketAverageInternetPrice,
        JDPowerPINAverageInternetPrice,
        EdmundsTrueMarketValue,
        NADARetailValue,
        KelleyBlueBookRetailValue,
        BlackBook,
        Galves
   }
}
