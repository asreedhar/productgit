using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    [Serializable]
    public sealed class MarketAnalysisVehiclePreference : InjectableBusinessBase<MarketAnalysisVehiclePreference>, IMarketAnalysisVehiclePreference
    {
        private const int NOT_ASSIGNED = 0;

        #region Overrides of BusinessBase<MarketAnalysisVehiclePreference>
        private int? _marketAnalysisVehiclePreferenceId;
        private string _ownerHandle;
        private string _vehicleHandle;
        private bool _isDisplayed;
        private bool _isGaugeDisplayed;
        private bool _hasOverriddenPriceProviders;
        private IEnumerable<MarketAnalysisPriceProviders> _selectedPriceProviders;

        protected override object GetIdValue()
        {
            return _marketAnalysisVehiclePreferenceId;
        }

        #endregion

        #region Injected dependencies
        
        public IMarketAnalysisVehiclePreferenceDataAccess DataAccess { get; set; }

        #endregion


        #region Factory Methods

        // ReSharper disable UnusedMember.Local
        private MarketAnalysisVehiclePreference() {}
// ReSharper restore UnusedMember.Local

        private MarketAnalysisVehiclePreference(string ownerHandle, string vehicleHandle, bool isDisplayed, bool isGaugeDisplayed)
        {
            _marketAnalysisVehiclePreferenceId = NOT_ASSIGNED;

            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _isDisplayed = isDisplayed;
            _isGaugeDisplayed = isGaugeDisplayed;
            _hasOverriddenPriceProviders = false;
            _selectedPriceProviders = null;
            MarkNew();
        }

        public static MarketAnalysisVehiclePreference GetOrCreateMarketAnalysisVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            // Check for existing preference
            ExistsCommand command = new ExistsCommand( ownerHandle, vehicleHandle );
            DataPortal.Execute(command);

            // If the preference exists, return it.  Otherwise create a new one.
            return command.Exists ? GetMarketAnalysisVehiclePreference( ownerHandle, vehicleHandle ) : 
                                    CreateMarketAnalysisVehiclePreference( ownerHandle, vehicleHandle );
        }

        public static MarketAnalysisVehiclePreference GetMarketAnalysisVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<MarketAnalysisVehiclePreference>(new Criteria(ownerHandle, vehicleHandle));
        }

        public static MarketAnalysisVehiclePreference CreateMarketAnalysisVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Create<MarketAnalysisVehiclePreference>(new Criteria(ownerHandle, vehicleHandle));   
        }

        #endregion


        #region Implementation of IMarketAnalysisVehiclePreference

        public int MarketAnalysisVehiclePreferenceId
        {
            get
            {
                if (_marketAnalysisVehiclePreferenceId.HasValue) return _marketAnalysisVehiclePreferenceId.Value;
                return NOT_ASSIGNED;
            }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public bool IsDisplayed
        {
            get
            {
                CanReadProperty("IsDisplayed", true);

                return _isDisplayed;
            }
            set
            {
                CanWriteProperty("IsDisplayed", true);

                if (IsDisplayed != value)
                {
                    _isDisplayed = value;

                    PropertyHasChanged("IsDisplayed");
                }
            }
        }

        public bool IsGaugeDisplayed
        {
            get
            {
                CanReadProperty("IsGaugeDisplayed", true);

                return _isGaugeDisplayed;
            }
            set
            {
                CanWriteProperty("IsGaugeDisplayed", true);

                if (IsGaugeDisplayed != value)
                {
                    _isGaugeDisplayed = value;

                    PropertyHasChanged("IsGaugeDisplayed");
                }
            }
        }

        public bool HasOverriddenPriceProviders
        {
            get
            {
                CanReadProperty("HasOverriddenPriceProviders", true);

                return _hasOverriddenPriceProviders;
            }
            set
            {
                CanWriteProperty("HasOverriddenPriceProviders", true);

                if (HasOverriddenPriceProviders != value)
                {
                    _hasOverriddenPriceProviders = value;

                    PropertyHasChanged("HasOverriddenPriceProviders");
                }
            }
        }

        public IEnumerable<MarketAnalysisPriceProviders> SelectedPriceProviders
        {
            get
            {
                CanReadProperty("SelectedPriceProviders", true);

                return _selectedPriceProviders;
            }
            set
            {
                CanWriteProperty("SelectedPriceProviders", true);

                if (SelectedPriceProviders != value)
                {
                    if ((SelectedPriceProviders != null)
                        && (value != null))
                    {
                        // if we have two non-null objects, compare the contents
                        if (!SelectedPriceProviders.SequenceEqual(value))
                        {
                            _selectedPriceProviders = value;

                            PropertyHasChanged("SelectedPriceProviders");
                        }
                    }
                    else
                    {
                        _selectedPriceProviders = value;

                        PropertyHasChanged("SelectedPriceProviders");                        
                    }
                }                
            }

        }


        #endregion


        #region Data Access


        [Serializable]
        private class Criteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }
        }



// ReSharper disable UnusedMember.Local
        private void DataPortal_Fetch(Criteria criteria)
// ReSharper restore UnusedMember.Local
        {
            MarketAnalysisVehiclePreferenceTO preference = DataAccess.Fetch(
                criteria.OwnerHandle,
                criteria.VehicleHandle);

            // Initialize from the transfer object.
            FromTransferObject(preference);
        }

// ReSharper disable UnusedMember.Local
        private void DataPortal_Create(Criteria criteria)
// ReSharper restore UnusedMember.Local
        {
            // We have to construct a new object, get a corresponding transfer object, and then initialize ourselves from the transfer
            // object because of CSLA's odd create semantics.

            MarketAnalysisVehiclePreferenceTO preference = DataAccess.Create(criteria.OwnerHandle,
                                                                             criteria.VehicleHandle);
            // Initialize from the transfer object.
            FromTransferObject(preference);
        }

        private void FromTransferObject(IMarketAnalysisVehiclePreference transferObject)
        {
            _marketAnalysisVehiclePreferenceId = transferObject.MarketAnalysisVehiclePreferenceId;
            _ownerHandle = transferObject.OwnerHandle;
            _vehicleHandle = transferObject.VehicleHandle;
            _isDisplayed = transferObject.IsDisplayed;
            _isGaugeDisplayed = transferObject.IsGaugeDisplayed;
            _hasOverriddenPriceProviders = transferObject.HasOverriddenPriceProviders;
            _selectedPriceProviders = transferObject.SelectedPriceProviders;
        }

        /// <summary>
        /// Callers may update the business object in bulk.
        /// </summary>
        /// <param name="preference"></param>
        public void UpdateFrom(IMarketAnalysisVehiclePreference preference)
        {
            if( _marketAnalysisVehiclePreferenceId == preference.MarketAnalysisVehiclePreferenceId &&
                _ownerHandle.Equals( preference.OwnerHandle ) &&
                _vehicleHandle.Equals( preference.VehicleHandle ) 
                )
            {
                IsDisplayed = preference.IsDisplayed;
                IsGaugeDisplayed = preference.IsGaugeDisplayed;
                HasOverriddenPriceProviders = preference.HasOverriddenPriceProviders;
                SelectedPriceProviders = preference.SelectedPriceProviders;
            }
            else
            {
                throw new ApplicationException("The keys do not match.");
            }
        }

        private MarketAnalysisVehiclePreferenceTO ToTransferObject()
        {
            return new MarketAnalysisVehiclePreferenceTO(_marketAnalysisVehiclePreferenceId, _ownerHandle, _vehicleHandle, _isDisplayed, 
                _isGaugeDisplayed, _hasOverriddenPriceProviders, _selectedPriceProviders);
        }

        protected override void DataPortal_Insert()
        {
            MarketAnalysisVehiclePreferenceTO preference = ToTransferObject();
            DataAccess.Insert(preference, ApplicationContext.User.Identity.Name);

            _marketAnalysisVehiclePreferenceId = preference.MarketAnalysisVehiclePreferenceId;
        }

        protected override void DataPortal_Update()
        {
            DataAccess.Update(ToTransferObject(), ApplicationContext.User.Identity.Name);
        }

        protected override void DataPortal_DeleteSelf()
        {
            DataAccess.Delete(ToTransferObject(), ApplicationContext.User.Identity.Name);
        }

        #endregion


        #region Exists Command

        [Serializable]
        class ExistsCommand : InjectableCommandBase
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;
            private bool _exists;

            #region Injected dependencies

            public IMarketAnalysisVehiclePreferenceDataAccess DataAccess { get; set; }

            #endregion

            public ExistsCommand(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public bool Exists
            {
                get { return _exists; }
            }

            protected override void DataPortal_Execute()
            {
                _exists = DataAccess.Exists(_ownerHandle, _vehicleHandle);
            }
        }

        #endregion

       
    }

}
