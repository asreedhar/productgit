using System.Collections.Generic;
using System.Text;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    public class MarketAnalysisXmlBuilder :IRenderXml
    {
        private readonly IMarketAnalysisDealerPreference _dealerPreference;
        private readonly IMarketAnalysisPricingInformation _pricingInformation;
        private readonly IMarketAnalysisVehiclePreference _vehiclePreference;

        private readonly int _offerPrice;

        public MarketAnalysisXmlBuilder(IMarketAnalysisDealerPreference dealerPreference, IMarketAnalysisVehiclePreference vehiclePreference, 
            IMarketAnalysisPricingInformation pricingInformation, int offerPrice)
        {
            _dealerPreference = dealerPreference;
            _vehiclePreference = vehiclePreference;
            _pricingInformation = pricingInformation;
            _offerPrice = offerPrice;
        }


        #region Implementation of IRenderXml

        #region Sample XML
        /*  XML example:
          <pricing isDisplayed="True">
              <gaugeprices breakPosition="" showGauge="True">
                  <price displayHead="False" isOfferPrice="True">
                      <delta>0</delta> 
                      <amount>6990</amount> 
                      <text>Your Price</text> 
                      <providerId>1</providerId> 
                      <gauge position="-22" width="45" /> 
                  </price>
                  <price displayHead="False" isOfferPrice="False">
                      <delta>10305</delta> 
                      <amount>17295</amount> 
                      <text>Original List Price</text> 
                      <providerId>3</providerId> 
                      <gauge position="499" width="63" /> 
                  </price>
                  <price displayHead="False" isOfferPrice="False">
                      <delta>10009</delta> 
                      <amount>16999</amount> 
                      <text>Current Internet Price</text> 
                      <providerId>2</providerId> 
                      <gauge position="415" width="73" /> 
                  </price>
                  <price displayHead="False" isOfferPrice="False">
                      <delta>1899</delta> 
                      <amount>8889</amount> 
                      <text>PING Market Average Internet Price</text> 
                      <providerId>4</providerId> 
                      <gauge position="41" width="113" /> 
                  </price>
              </gaugeprices>
              <comparisonprices>
                  <price selected="True">
                      <providerId>2</providerId> 
                      <delta>10009</delta> 
                      <logo>http://ub9aweb-phto01x.int.firstlook.biz:8080/digitalimages/101619/Logo/Logo_101619_honda.jpg</logo> 
                      <text>Current Internet Price</text> 
                      <isaccurate>True</isaccurate> 
                      <amount>$16,999</amount> 
                      <savingsValue>$10,009</savingsValue> 
                      <savingsText>Below Current Internet Price</savingsText> 
                  </price>
                  <price selected="True">
                      <providerId>3</providerId> 
                      <delta>10305</delta> 
                      <logo>http://ub9aweb-phto01x.int.firstlook.biz:8080/digitalimages/101619/Logo/Logo_101619_honda.jpg</logo> 
                      <text>Original List Price</text> 
                      <isaccurate>True</isaccurate> 
                      <amount>$17,295</amount> 
                      <savingsValue>$10,305</savingsValue> 
                      <savingsText>Below Original List Price</savingsText> 
                  </price>
                  <price selected="True">
                      <providerId>4</providerId> 
                      <delta>1899</delta> 
                      <logo>http://kboucher01:2476/Pricing/Public/Images/logo_PingII.gif</logo> 
                      <text>PING Market Average Internet Price</text> 
                      <isaccurate>True</isaccurate> 
                      <amount>$8,889</amount> 
                      <savingsValue>$1,899</savingsValue> 
                      <savingsText>Below PING Market Average Internet Price</savingsText> 
                  </price>
                  <price selected="True">
                      <providerId>5</providerId> 
                      <delta>10</delta> 
                      <logo>http://kboucher01:2476/Pricing/Public/Images/logo_JDPower.gif</logo> 
                      <text>JD Power PIN Average Internet Price</text> 
                      <isaccurate>True</isaccurate> 
                      <amount>$7,000</amount> 
                      <savingsValue>$10</savingsValue> 
                      <savingsText>Below JD Power PIN Average Internet Price</savingsText> 
                  </price>
                  <price selected="False">
                      <providerId>6</providerId> 
                      <delta>-6990</delta> 
                      <logo>http://kboucher01:2476/Pricing/Public/Images/logo_Edmunds.gif</logo> 
                      <text>Edmunds True Market Value</text> 
                      <isaccurate>True</isaccurate> 
                      <amount>-</amount> 
                      <savingsValue>?</savingsValue> 
                      <savingsText>Above Edmunds True Market Value</savingsText> 
                  </price>
                  <price selected="False">
                      <providerId>7</providerId> 
                      <delta>-6990</delta> 
                      <logo>http://kboucher01:2476/Pricing/Public/Images/logo_NADA.gif</logo> 
                      <text>NADA Retail Value</text> 
                      <isaccurate>False</isaccurate> 
                      <amount>-</amount> 
                      <savingsValue>?</savingsValue> 
                      <savingsText>Above NADA Retail Value</savingsText> 
                  </price>
                  <price selected="False">
                      <providerId>8</providerId> 
                      <delta>-6990</delta> 
                      <logo>http://kboucher01:2476/Pricing/Public/Images/logo_KBB2.gif</logo> 
                      <text>Kelley Blue Book Retail Value</text> 
                      <isaccurate>False</isaccurate> 
                      <amount>-</amount> 
                      <savingsValue>?</savingsValue> 
                      <savingsText>Above Kelley Blue Book Retail Value</savingsText> 
                  </price>
                  <price selected="False">
                      <providerId>9</providerId> 
                      <delta>-6990</delta> 
                      <logo>http://ub9aweb-phto01x.int.firstlook.biz:8080/digitalimages/101619/Logo/Logo_101619_honda.jpg</logo> 
                      <text>Black Book</text> 
                      <isaccurate>True</isaccurate> 
                      <amount>-</amount> 
                      <savingsValue>?</savingsValue> 
                      <savingsText>Above Black Book</savingsText> 
                  </price>
                      <price selected="False">
                      <providerId>10</providerId> 
                      <delta>-6990</delta> 
                      <logo>http://ub9aweb-phto01x.int.firstlook.biz:8080/digitalimages/101619/Logo/Logo_101619_honda.jpg</logo> 
                      <text>Galves</text> 
                      <isaccurate>True</isaccurate> 
                      <amount>-</amount> 
                      <savingsValue>?</savingsValue> 
                      <savingsText>Above Galves</savingsText> 
                  </price>
              </comparisonprices>
              <listprices>
                  <listprice isOriginal="False">16495</listprice> 
                  <listprice isOriginal="False">16995</listprice> 
                  <listprice isOriginal="False">17295</listprice> 
              </listprices>
          </pricing>
             */
        #endregion


        public string AsXml()
        {
            // Create a price processor
            var priceProcessor = new PriceProcessor(_dealerPreference, _vehiclePreference, _pricingInformation, _offerPrice);

            priceProcessor.ProcessPrices();

            string pricesNode = GetGaugePricesNode(priceProcessor);

            string comparisonPricesNode = GetComparisonPricesNode(priceProcessor);

            var attributes = new Dictionary<string, string>();
            attributes.Add("isDisplayed", _vehiclePreference.IsDisplayed.ToString().ToLower());

            return TagBuilder.Wrap("pricing", attributes, pricesNode + comparisonPricesNode);
        }

        private static string GetComparisonPricesNode(PriceProcessor processor)
        {
            var sb = new StringBuilder();

            // use the processed prices from our price processor to build the xml
            foreach (ComparisonPrice price in processor.ComparisonPrices)
            {
                sb.Append(BuildPriceElement(price));
            }

            return TagBuilder.Wrap("comparisonprices", sb.ToString());

        }

        private static string GetGaugePricesNode(PriceProcessor priceProcessor)
        {
            var sb = new StringBuilder();

            // use the processed prices from our price processor to build the xml
            foreach (GaugePrice price in priceProcessor.GaugePrices)
            {
                sb.Append(BuildPriceElement(price));
            }

            IDictionary<string, string> pricesAttributes = new Dictionary<string, string>
                                                               {
                                                                   {   "breakPosition",priceProcessor.BreakPosition.ToString()  },
                                                                   {    "showGauge", priceProcessor.ShowGauge.ToString().ToLower()    }
                                                               };

            return TagBuilder.Wrap("gaugeprices", pricesAttributes, sb.ToString());
        }

        private static string BuildPriceElement(GaugePrice gaugePrice)
        {
            var sb = new StringBuilder();

            // add nodes for this price
            sb.Append(TagBuilder.Wrap("delta", gaugePrice.Delta.ToString()));
            sb.Append(TagBuilder.Wrap("amount", gaugePrice.Dollars.ToString()));
            sb.Append(TagBuilder.Wrap("text", gaugePrice.Text));
            sb.Append(TagBuilder.Wrap("providerId", ((int) gaugePrice.PriceProviderId).ToString()));

            // create a dictionary of attributes for the gauge node
            IDictionary<string, string> gaugeAttributes = new Dictionary<string, string>
                                                              {
                                                                  {"position", gaugePrice.GaugePosition.ToString()},
                                                                  {"width", gaugePrice.Width.ToString()}
                                                              };

            // create the gauge node, using the attributes created above
            sb.Append(TagBuilder.Wrap("gauge", gaugeAttributes, string.Empty));

            // create a dictionary of attributes for the price node
            IDictionary<string,string> priceAttributes = new Dictionary<string, string>();
            priceAttributes.Add("displayHead", gaugePrice.DisplayHead.ToString().ToLower());
            priceAttributes.Add("isOfferPrice", gaugePrice.IsOfferPrice.ToString().ToLower());

            // create the price node, using the attributes created above
            return TagBuilder.Wrap("price", priceAttributes, sb.ToString());
        }

        private static string BuildPriceElement(ComparisonPrice price)
        {
            var sb = new StringBuilder();

            // add nodes for this price
            sb.Append(TagBuilder.Wrap("providerId", ((int)price.PriceProviderId).ToString()));
            sb.Append(TagBuilder.Wrap("delta", price.Delta.ToString()));
            sb.Append(TagBuilder.Wrap("logo", price.LogoUrl));
            sb.Append(TagBuilder.Wrap("text", price.Text));
            sb.Append(TagBuilder.Wrap("isaccurate", price.IsAccurate.ToString().ToLower()));
            sb.Append(TagBuilder.Wrap("amount", string.Format("{0:$#,##0;$#,##0;'-'}", price.Dollars)));
            
            var savingsText = new StringBuilder();
            savingsText.Append(price.Delta > 0 ? "Below " : "Above ").Append(price.Text);
            
            if (price.Dollars == 0)
                sb.Append(TagBuilder.Wrap("savingsValue", "?"));
            else
                sb.Append(TagBuilder.Wrap("savingsValue", string.Format("{0:$#,##0;$#,##0;$0}", price.Delta)));

            sb.Append(TagBuilder.Wrap("savingsText", savingsText.ToString()));

            // create a dictionary of attributes for the price node
            IDictionary<string, string> priceAttributes = new Dictionary<string, string>
                                                              {{"selected", price.Selected.ToString().ToLower()}};

            // create the price node, using the attributes created above
            return TagBuilder.Wrap("price", priceAttributes, sb.ToString());
        }



        #endregion


    }
}
