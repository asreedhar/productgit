using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    internal class MathUtilities
    {         
        internal bool IsLargestNumberAnOutlier(IList<double> numbers, double outlierThreshold)
        {
            // This is only valid for 2 or more numbers.
            if( numbers.Count < 2 )
            {
                return false;
            }

            // Normalize the numbers.
            IList<double> normalized = Normalize(numbers);

            // If the 2nd to last number is < the threshold, the last number is consider to be an outlier.
            return normalized[normalized.Count - 2] < outlierThreshold;
        }

        internal IList<double> Normalize(IList<double> numbers)
        {
            // Normalize the numbers.
            IList<double> normalized = new List<double>(numbers.Count);
            double max = numbers[numbers.Count - 1];
            
            foreach (double num in numbers)
            {
                normalized.Add(num / max);
            }

            return normalized;
        }
    }
}
