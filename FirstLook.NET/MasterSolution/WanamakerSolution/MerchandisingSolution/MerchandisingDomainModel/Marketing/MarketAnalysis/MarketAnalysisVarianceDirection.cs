using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    // Used to determine if the VarianceDollarAmount is above or below the comparison price.
    [Serializable]
    public enum MarketAnalysisVarianceDirection
    {
        Above = 1,
        Below = 2
    }
}