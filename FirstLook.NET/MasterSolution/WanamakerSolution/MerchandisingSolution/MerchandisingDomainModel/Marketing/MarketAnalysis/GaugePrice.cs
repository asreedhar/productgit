using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis
{
    public class GaugePrice
    {
        internal static decimal WidthRatio = 10 / (decimal)3;
        //private readonly PriceProvider _priceProvider;
        private readonly MarketAnalysisPriceProviders _priceProviderId;
        private readonly int _dollars;
        private readonly int _width;
        private readonly int _delta;
        private readonly string _text;
        private readonly int _rank;

        internal static int MinWidth = 45;

        public GaugePrice(MarketAnalysisPriceProviders providerType, string text, int rank, int dollars, int delta)
        {
            _priceProviderId = providerType;
            _text = text;
            _rank = rank;
            _dollars = dollars;
            _delta = delta;

            /* calculate the width based off the text length 
             * and a width ratio that was derived from testing 
             * different values given our current: 
             *      gauge width, 
             *      max length of a price description, 
             *      max number of lines we wanted the text to wrap (3).
            */

            decimal width = Text.Length * WidthRatio;
            _width = (int)Math.Round(width);

            // if the calculated width is less than the minimum width, use the minimum width.
            if (_width < MinWidth) _width = MinWidth;            
        }

        internal int Dollars
        {
            get { return _dollars; }
        }

        internal string Text
        {
            get { return _text; }
        }

        internal int Delta
        {
            get { return _delta; }
        }

        internal int GaugePosition { get; set; }

        internal int Width
        {
            get { return _width; }
        }

        internal int Rank
        {
            get
            {
                return _rank;
            }
        }

        internal int RightEdge
        {
            get 
            { 
                return LeftEdge + Width;
            }
        }

        internal int LeftEdge
        {
            get
            {
                return GaugePosition;
            }
        }

        internal MarketAnalysisPriceProviders PriceProviderId
        {
            get { return _priceProviderId; }
        }

        public bool IsOfferPrice
        {
            get { return PriceProviderId == MarketAnalysisPriceProviders.OfferPrice; }
        }

        public object DisplayHead
        {
            // TODO: Remove this as part of the refactoring.  It may not be needed any longer.
            get { return false; }
        }

        #region Sort Methods
        internal static int ComparePricesByRank(GaugePrice x, GaugePrice y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    // If x is null and y is null, they're equal. 
                    return 0;
                }
                // If x is null and y is not null, y is greater. 
                return -1;
            }

            // If x is not null...

            if (y == null)  // ...and y is null, x is greater.
            {
                return 1;
            }

            // if they are the same object, return zero.
            if (x.Equals(y)) return 0;

            // Offer Price is always the first price in our list
            if (x.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice) return -1;
            if (y.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice) return 1;

            // ...and y is not null, compare the ranks of each price.
            int retval = x.Rank.CompareTo(y.Rank);

            if (retval != 0)
            {
                // If the ranks are not equal, the larger rank is greater.
                return retval;
            }

            // If the ranks are equal, sort them with ordinary int comparison.
            return x.Rank.CompareTo(y.Rank);
        }
        internal static int ComparePricesByOfferThenDescendingDollarValue(GaugePrice x, GaugePrice y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    // If x is null and y is null, they're equal. 
                    return 0;
                }
                // If x is null and y is not null, y is greater. 
                return -1;
            }

            // If x is not null...

            if (y == null)  // ...and y is null, x is greater.
            {
                return 1;
            }

            // if they are the same object, return zero.
            if (x.Equals(y)) return 0;

            // Offer Price is always the first price in our list
            if (x.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice) return -1;
            if (y.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice) return 1;

            // ...and y is not null, compare the ranks of each price.
            int retval = y.Dollars.CompareTo(x.Dollars);

            if (retval != 0)
            {
                // If the dollar values are not equal, the larger dollar value is greater.
                return retval;
            }

            // If the dollar values are equal, compare the ranks (lower rank = higher priority)
            if (x.Rank < y.Rank)
                return -1;

            if (x.Rank > y.Rank)
                return 1;

            return 0;
        }
        internal static int ComparePricesByAscendingDollarValue(GaugePrice x, GaugePrice y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    // If x is null and y is null, they're equal. 
                    return 0;
                }
                // If x is null and y is not null, y is greater. 
                return -1;
            }

            // If x is not null...

            if (y == null)  // ...and y is null, x is greater.
            {
                return 1;
            }
            
            // if they are the same object, return zero.
            if (x.Equals(y)) return 0;

            // ...and y is not null, compare the ranks of each price.
            int retval = x.Dollars.CompareTo(y.Dollars);

            if (retval != 0)
            {
                // If the dollar values are not equal, the larger dollar value is greater.
                return retval;
            }

            // If the dollar values are equal, compare the ranks (lower rank = higher priority)
            if (x.Rank < y.Rank)
                return -1;

            if (x.Rank > y.Rank)
                return 1;

            return 0;
        }
        #endregion
    }

}
