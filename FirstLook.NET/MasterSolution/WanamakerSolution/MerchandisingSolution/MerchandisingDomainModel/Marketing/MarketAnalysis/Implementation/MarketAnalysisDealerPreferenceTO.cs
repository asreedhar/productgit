using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation
{
    [Serializable]
    public class MarketAnalysisDealerPreferenceTO : IMarketAnalysisDealerPreference
    {

        private int _id;
        private string _ownerHandle;
        private int _numberOfPricesRequiredForPricingGauge;
        private bool _useCustomCurrentInternetPrice;
        private string _currentInternetPriceDisplayName;
        private bool _useCustomOfferPrice;
        private string _offerPriceDisplayName;
        private bool _useCustomComparisonPricePriority;
        private List<PriceProvider> _priceProviders;
        private string _logoUrl;
        private bool _originalListPriceCalculationOverride;
        
        public MarketAnalysisDealerPreferenceTO()
        {
            _priceProviders = new List<PriceProvider>();
        }

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string OwnerHandle
        {
            get 
            { 
                return _ownerHandle; 
            }
            set
            {
                _ownerHandle = value;
            }
        }

        public int NumberOfPricesRequiredForPricingGauge
        {
            get
            {
                return _numberOfPricesRequiredForPricingGauge;
            }
            set
            {
                _numberOfPricesRequiredForPricingGauge = value;
            }
        }

        public bool UseCustomCurrentInternetPrice
        {
            get
            {
                return _useCustomCurrentInternetPrice;
            }
            set
            {
                _useCustomCurrentInternetPrice = value;
            }
        }

        public string CurrentInternetPriceDisplayName
        {
            get
            {
                return _currentInternetPriceDisplayName;
            }
            set
            {
                _currentInternetPriceDisplayName = value;
            }
        }

        public bool UseCustomOfferPrice
        {
            get
            {
                return _useCustomOfferPrice;
            }
            set
            {
                _useCustomOfferPrice = value;
            }
        }

        public string OfferPriceDisplayName
        {
            get
            {
                return _offerPriceDisplayName;
            }
            set
            {
                _offerPriceDisplayName = value;
            }
        }

        public bool UseCustomComparisonPricePriority
        {
            get
            {
                return _useCustomComparisonPricePriority;
            }
            set
            {
                _useCustomComparisonPricePriority = value;
            }
        }

        public List<PriceProvider> PriceProviders
        {
            get { return _priceProviders; }
            set { _priceProviders = value; }            
        }

        public string LogoUrl
        {
            get { return _logoUrl; }
            set { _logoUrl = value; }
        }

    }
}