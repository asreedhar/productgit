using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Csla;
using Csla.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation
{
    public class MarketAnalysisDealerPreferenceDataAccess : IMarketAnalysisDealerPreferenceDataAccess
    {
        public ITransaction BeginTransaction()
        {
            return new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase);
        }


        #region Public Data Access Methods
        public bool Exists(string ownerHandle)
        {
            bool exists;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Marketing.MarketAnalysisPreference#Exists";

                    CommonMethods.AddWithValue(command, "OwnerHandle", String.IsNullOrEmpty(ownerHandle)?null:ownerHandle, DbType.String);
                    SqlParameter existsParam = new SqlParameter("Exists", SqlDbType.Bit);
                    existsParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(existsParam);

                    command.ExecuteNonQuery();

                    exists = Boolean.Parse(existsParam.Value.ToString());
                }
            }

            return exists;
        }

        public MarketAnalysisDealerPreferenceTO Fetch(string ownerHandle)
        {
            return DoCreateOrFetch(ownerHandle, false);
        }

        public MarketAnalysisDealerPreferenceTO Create(string ownerHandle)
        {
            return DoCreateOrFetch(ownerHandle, true);
        }

        public void Insert(ITransaction transaction, string ownerHandle, MarketAnalysisDealerPreferenceTO dealerPreferenceTO)
        {
            DoInsert((DbTransaction) transaction, ownerHandle, dealerPreferenceTO);
        }

        public void Update(ITransaction transaction, MarketAnalysisDealerPreferenceTO dealerPreferenceTO)
        {
            DoUpdate((DbTransaction) transaction, dealerPreferenceTO);
        }

        public void ChangePriceProviderRank(ITransaction transaction, int ownerPriceProviderId, int newRank, int varianceAmount, int varianceDirection )
        {
            DoChangePriceProviderRank((DbTransaction) transaction, ownerPriceProviderId, newRank, varianceAmount, varianceDirection);
        }

        public void InsertPriceProviderRank(ITransaction transaction, string ownerHandle, MarketAnalysisPriceProviders provider, int rank, int varianceAmount, int varianceDirection)
        {
            DoInsertPriceProviderRank((DbTransaction) transaction, ownerHandle, provider, rank, varianceAmount, varianceDirection);
        }

        #endregion

        private static MarketAnalysisDealerPreferenceTO DoCreateOrFetch(string ownerHandle, bool isNew)
        {
            MarketAnalysisDealerPreferenceTO dto = new MarketAnalysisDealerPreferenceTO();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = isNew ? "Marketing.MarketAnalysisPreference#Create" : "Marketing.MarketAnalysisPreference#Fetch";
                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    SafeDataReader reader = new SafeDataReader(command.ExecuteReader());

                    if (reader.Read())
                    {
                        dto.Id = isNew ? 0 : reader.GetInt32(reader.GetOrdinal("MarketAnalysisPreferenceId"));
                        dto.OwnerHandle = ownerHandle;
                        dto.NumberOfPricesRequiredForPricingGauge = reader.GetByte(reader.GetOrdinal("NumberOfPrices"));
                        dto.UseCustomOfferPrice = reader.GetBoolean(reader.GetOrdinal("UseCustomOfferPrice"));
                        dto.OfferPriceDisplayName = reader.IsDBNull(reader.GetOrdinal("OfferPriceDisplayName")) ? String.Empty : reader.GetString(reader.GetOrdinal("OfferPriceDisplayName"));
                        dto.UseCustomCurrentInternetPrice = reader.GetBoolean(reader.GetOrdinal("UseCustomCurrentInternetPrice"));
                        dto.CurrentInternetPriceDisplayName = reader.IsDBNull(reader.GetOrdinal("CurrentInternetPriceDisplayName")) ? String.Empty : reader.GetString(reader.GetOrdinal("CurrentInternetPriceDisplayName"));
                        dto.UseCustomComparisonPricePriority =
                            Boolean.Parse(reader[reader.GetOrdinal("UseCustomComparisonPricePriority")].ToString());
                        dto.LogoUrl = reader.GetString(reader.GetOrdinal("LogoUrl"));

                        if (reader.NextResult())
                        {
                            List<PriceProvider> providers = new List<PriceProvider>();

                            while (reader.Read())
                            {
                                int ownerPriceProviderId = isNew ? 0 : reader.GetInt32(reader.GetOrdinal("OwnerPriceProviderID"));
                                MarketAnalysisPriceProviders priceProviderId =  
                                    (MarketAnalysisPriceProviders)  reader.GetByte( reader.GetOrdinal("PriceProviderID"));
                                
                                string providerDescription = reader.GetString(reader.GetOrdinal("ProviderDescription"));
                                int providerRank = reader.GetInt32(reader.GetOrdinal("Rank"));

                                string logoUrl = PriceProvider.GetLogoUrl(
                                    priceProviderId, dto.LogoUrl);

                                int varianceDollarAmount = reader.GetInt32(reader.GetOrdinal("MinimumDollarVariance"));
                                MarketAnalysisVarianceDirection varianceDirection = (MarketAnalysisVarianceDirection)reader.GetByte(reader.GetOrdinal("VarianceDirection"));

                                PriceProvider provider = new PriceProvider(ownerPriceProviderId, priceProviderId, providerDescription,
                                                                                 providerRank, logoUrl, varianceDollarAmount, varianceDirection);
                                providers.Add(provider);
                            }

                            dto.PriceProviders = providers;
                        }
                    }                    
                    
                    if (!reader.IsClosed) reader.Close();
                }
            }

            return dto;
        }



        private static void DoInsert(DbTransaction transaction, string ownerHandle, MarketAnalysisDealerPreferenceTO dealerPreferenceTO)
        {
            using (IDbCommand command = transaction.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;

                command.CommandText = "Marketing.MarketAnalysisPreference#Insert";

                CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                CommonMethods.AddWithValue(command, "NumberOfPrices", dealerPreferenceTO.NumberOfPricesRequiredForPricingGauge, DbType.Byte);
                CommonMethods.AddWithValue(command, "UseCustomCurrentInternetPrice", dealerPreferenceTO.UseCustomCurrentInternetPrice, DbType.Boolean, true);
                CommonMethods.AddWithValue(command, "CurrentInternetPriceDisplayName", dealerPreferenceTO.CurrentInternetPriceDisplayName, DbType.String, true);
                CommonMethods.AddWithValue(command, "UseCustomOfferPrice", dealerPreferenceTO.UseCustomOfferPrice, DbType.Boolean, true);
                CommonMethods.AddWithValue(command, "OfferPriceDisplayName", dealerPreferenceTO.OfferPriceDisplayName, DbType.String, true);
                CommonMethods.AddWithValue(command, "UseCustomComparisonPricePriority", dealerPreferenceTO.UseCustomComparisonPricePriority, DbType.Boolean);
                CommonMethods.AddWithValue(command, "InsertUser", ApplicationContext.User.Identity.Name, DbType.String);
                SqlParameter prefId = new SqlParameter("MarketAnalysisPreferenceId", SqlDbType.Int);
                prefId.Direction = ParameterDirection.Output;

                command.Parameters.Add(prefId);

                command.ExecuteNonQuery();
            }
        }

        private static void DoUpdate(DbTransaction transaction, MarketAnalysisDealerPreferenceTO dealerPreferenceTO)
        {
            using (IDbCommand command = transaction.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;

                command.CommandText = "Marketing.MarketAnalysisPreference#Update";

                CommonMethods.AddWithValue(command, "MarketAnalysisPreferenceId", dealerPreferenceTO.Id, DbType.Int32);
                CommonMethods.AddWithValue(command, "NumberOfPrices", dealerPreferenceTO.NumberOfPricesRequiredForPricingGauge, DbType.Byte);
                CommonMethods.AddWithValue(command, "UseCustomCurrentInternetPrice", dealerPreferenceTO.UseCustomCurrentInternetPrice, DbType.Boolean, true);
                CommonMethods.AddWithValue(command, "CurrentInternetPriceDisplayName", dealerPreferenceTO.CurrentInternetPriceDisplayName, DbType.String);
                CommonMethods.AddWithValue(command, "UseCustomOfferPrice", dealerPreferenceTO.UseCustomOfferPrice, DbType.Boolean, true);
                CommonMethods.AddWithValue(command, "OfferPriceDisplayName", dealerPreferenceTO.OfferPriceDisplayName, DbType.String);
                CommonMethods.AddWithValue(command, "UseCustomComparisonPricePriority", dealerPreferenceTO.UseCustomComparisonPricePriority, DbType.Boolean);
                CommonMethods.AddWithValue(command, "UpdateUser", ApplicationContext.User.Identity.Name, DbType.String);

                command.ExecuteNonQuery();

                foreach (PriceProvider priceProvider in dealerPreferenceTO.PriceProviders)
                {
                    if (priceProvider.Id == 0)
                    {
                        // insert it
                        MarketAnalysisDealerPreference.InsertPriceProviderRankCommand.Execute(dealerPreferenceTO.OwnerHandle, priceProvider.PriceProviderId, priceProvider.Rank, priceProvider.VarianceDollarAmount, (int)priceProvider.VarianceDirection);
                    }
                    else
                    {
                        // update it
                        MarketAnalysisDealerPreference.ChangePriceProviderRankCommand.Execute(priceProvider.Id, priceProvider.Rank, priceProvider.VarianceDollarAmount, (int)priceProvider.VarianceDirection);
                    }
                }
            }
        }


        private static void DoInsertPriceProviderRank(DbTransaction transaction, string ownerHandle, MarketAnalysisPriceProviders provider, int rank, int varianceAmount, int varianceDirection)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = transaction.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Marketing.MarketAnalysisOwnerPriceProvider#Insert";
                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "PriceProviderId", (int) provider, DbType.Byte);
                    CommonMethods.AddWithValue(command, "Rank", rank, DbType.Byte);
                    CommonMethods.AddWithValue(command, "InsertUser", ApplicationContext.User.Identity.Name, DbType.String);
                    CommonMethods.AddWithValue(command, "VarianceAmount", varianceAmount, DbType.Int32);
                    CommonMethods.AddWithValue(command, "VarianceDirection", varianceDirection, DbType.Byte);

                    SqlParameter id = new SqlParameter("OwnerPriceProviderId", SqlDbType.Int);
                    id.Direction = ParameterDirection.Output;

                    command.Parameters.Add(id);

                    command.ExecuteNonQuery();
                }
            }
        }

        private static void DoChangePriceProviderRank(DbTransaction transaction, int ownerPriceProviderId, int newRank, int varianceAmount, int varianceDirection)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbCommand command = transaction.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Marketing.MarketAnalysisOwnerPriceProvider#Update";

                    CommonMethods.AddWithValue(command, "OwnerPriceProviderID", ownerPriceProviderId, DbType.Int32);
                    CommonMethods.AddWithValue(command, "Rank", newRank, DbType.Byte);
                    CommonMethods.AddWithValue(command, "UpdateUser", ApplicationContext.User.Identity.Name, DbType.String);
                    CommonMethods.AddWithValue(command, "VarianceAmount", varianceAmount, DbType.Int32);
                    CommonMethods.AddWithValue(command, "VarianceDirection", varianceDirection, DbType.Byte);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}