using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation
{
    internal class MarketAnalysisVehiclePreferenceDataAccess : IMarketAnalysisVehiclePreferenceDataAccess
    {
        #region Implementation of IMarketAnalysisVehiclePreferenceDataAccess

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketAnalysisVehiclePreference#Exists";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "Exists", false, DbType.Boolean, ParameterDirection.InputOutput, false);

                    command.ExecuteNonQuery();

                    IDataParameter retParam = command.Parameters["Exists"] as IDataParameter;
                    if (retParam != null)
                    {
                        return Convert.ToBoolean(retParam.Value);
                    }
                }
            }
            return false;
        }

        public MarketAnalysisVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle)
        {
            MarketAnalysisVehiclePreferenceTO preferenceTO;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketAnalysisVehiclePreference#Create";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    
                    IDataReader reader = command.ExecuteReader();
                    if (reader != null)
                    {
                        if (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("MarketAnalysisVehiclePreferenceId"));
                            string oh = reader.GetString(reader.GetOrdinal("OwnerHandle"));
                            string vh = reader.GetString(reader.GetOrdinal("VehicleHandle"));
                            bool isDisplayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                            bool showGauge = reader.GetBoolean(reader.GetOrdinal("ShowPricingGauge"));
                            bool hasOverriddenPriceProviders =
                                reader.GetBoolean(reader.GetOrdinal("HasOverriddenPriceProviders"));

                            var priceProviders = new List<MarketAnalysisPriceProviders>();

                            preferenceTO = new MarketAnalysisVehiclePreferenceTO(id, oh, vh, isDisplayed,
                                                                                 showGauge,
                                                                                 hasOverriddenPriceProviders,
                                                                                 priceProviders);

                        }
                        else
                        {
                            throw new ApplicationException("The preference could not be found.");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("The data reader is null.");
                    }
                    if (!reader.IsClosed) reader.Close();
                }
            }

            if (preferenceTO == null)
            {
                // If nothing found, throw.
                throw new ApplicationException("No market analysis vehicle preference could be found with the specified handles.");
            }

            return preferenceTO;
        }



        public MarketAnalysisVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle)
        {
            MarketAnalysisVehiclePreferenceTO preferenceTO;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketAnalysisVehiclePreference#Fetch";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    
                    IDataReader reader = command.ExecuteReader();
                    if (reader != null)
                    {
                        if (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("MarketAnalysisVehiclePreferenceId"));
                            string oh = reader.GetString(reader.GetOrdinal("OwnerHandle"));
                            string vh = reader.GetString(reader.GetOrdinal("VehicleHandle"));
                            bool isDisplayed = reader.GetBoolean(reader.GetOrdinal("IsDisplayed"));
                            bool showGauge = reader.GetBoolean(reader.GetOrdinal("ShowPricingGauge"));
                            bool hasOverriddenPriceProviders =
                                reader.GetBoolean(reader.GetOrdinal("HasOverriddenPriceProviders"));

                            List<MarketAnalysisPriceProviders> priceProviders = new List<MarketAnalysisPriceProviders>();
                            if (reader.NextResult())
                            {   // we have some price providers to load
                                while (reader.Read())
                                {
                                    int priceProviderId =
                                        //reader.GetInt32(reader.GetOrdinal("MarketAnalysisPriceProviderId"));
                                    Convert.ToInt32(reader["MarketAnalysisPriceProviderId"]);

                                    if (! Enum.IsDefined(typeof(MarketAnalysisPriceProviders), priceProviderId))
                                    {
                                        throw new ApplicationException(
                                            @"The price provider id obtained from
                                        Marketing.MarketAnalysisVehiclePreference#Fetch, for ownerHandle" + ownerHandle
                                            + " and vehicle handle " + vehicleHandle + @" was not a valid value 
                                        for the MarketAnalysisPriceProviders enumeration.");
                                    }
                                    
                                    priceProviders.Add(
                                        (MarketAnalysisPriceProviders)
                                        Enum.ToObject(typeof (MarketAnalysisPriceProviders), priceProviderId));
                                }
                            }

                            preferenceTO = 
                                new MarketAnalysisVehiclePreferenceTO(id, oh, vh, isDisplayed, showGauge, 
                                                                      hasOverriddenPriceProviders, priceProviders);

                        }
                        else
                        {
                            throw new ApplicationException("The preference could not be found.");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("The data reader is null.");
                    }
                    if (!reader.IsClosed) reader.Close();
                }
            }

            if (preferenceTO == null)
            {
                // If nothing found, throw.
                throw new ApplicationException("No market analysis vehicle preference could be found with the specified handles.");
            }

            return preferenceTO;
        }

        public void Insert(MarketAnalysisVehiclePreferenceTO preference, string userName)
        {
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketAnalysisVehiclePreference#Insert";

                    CommonMethods.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", preference.VehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "IsDisplayed", preference.IsDisplayed, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "ShowPricingGauge", preference.IsGaugeDisplayed, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "InsertUser", userName, DbType.String);
                    CommonMethods.AddWithValue(command, "HasOverriddenPriceProviders", preference.HasOverriddenPriceProviders, DbType.Boolean);

                    
                    CommonMethods.AddWithValue(command, "MarketAnalysisVehiclePreferenceID", 0, DbType.Int32, ParameterDirection.Output, false);

                    try
                    {
                        command.ExecuteNonQuery();
                        preference.MarketAnalysisVehiclePreferenceId = (int)((SqlParameter)command.Parameters["MarketAnalysisVehiclePreferenceID"]).Value;


                        if (preference.HasOverriddenPriceProviders)
                        {
                            // Add each selected price provider
                            foreach (var marketAnalysisPriceProvider in preference.SelectedPriceProviders)
                            {
                                AddPriceProviderToPreference(preference.MarketAnalysisVehiclePreferenceId,
                                                             (int)marketAnalysisPriceProvider, userName, txn.CreateCommand());
                            }
                        }
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

                // Commit the batch of commands.
                txn.Commit();
            }
        }

        public void Update(MarketAnalysisVehiclePreferenceTO preference, string userName)
        {
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketAnalysisVehiclePreference#Update";

                    CommonMethods.AddWithValue(command, "OwnerHandle", preference.OwnerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", preference.VehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "IsDisplayed", preference.IsDisplayed, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "ShowPricingGauge", preference.IsGaugeDisplayed, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "HasOverriddenPriceProviders", preference.HasOverriddenPriceProviders, DbType.Boolean);

                    CommonMethods.AddWithValue(command, "UpdateUser", userName, DbType.String);

                    try
                    {
                        // update the preference
                        command.ExecuteNonQuery();


                        // clear the price providers associated with this preference
                        ClearPriceProvidersFromPreference(preference.MarketAnalysisVehiclePreferenceId,
                                                          txn.CreateCommand());

                        // Add each selected price provider
                        foreach (var marketAnalysisPriceProvider in preference.SelectedPriceProviders)
                        {
                            AddPriceProviderToPreference(preference.MarketAnalysisVehiclePreferenceId,
                                                         (int) marketAnalysisPriceProvider, "zbrown", txn.CreateCommand());
                        }
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

                // Commit the batch of commands.
                txn.Commit();
            }
            
        }

        public void Delete(MarketAnalysisVehiclePreferenceTO preference, string userName)
        {
            using (DbTransaction txn = new DbTransaction(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                using (IDbCommand command = txn.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketAnalysisVehiclePreference#Delete";

                    CommonMethods.AddWithValue(command, "MarketAnalysisVehiclePreferenceId", preference.MarketAnalysisVehiclePreferenceId, DbType.Int32);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        txn.Rollback();
                        throw;
                    }
                }

                // Commit the batch of commands.
                txn.Commit();
            }
            
            
        }

        #endregion


        private static void ClearPriceProvidersFromPreference(int marketAnalysisVehiclePreferenceId, IDbCommand command)
        {
            using (command)
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.MarketAnalysisVehiclePreferencePriceProvider#DeleteAll";
                CommonMethods.AddWithValue(command, "MarketAnalysisVehiclePreferenceID", marketAnalysisVehiclePreferenceId, DbType.Int32);
                command.ExecuteNonQuery();
                return;
            }
        }

        private static void AddPriceProviderToPreference(int marketAnalysisVehiclePreferenceId, int marketAnalysisPriceProviderId, string userName, IDbCommand command)
        {
            using (command)
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Marketing.MarketAnalysisVehiclePreferencePriceProvider#Insert";
                CommonMethods.AddWithValue(command, "MarketAnalysisVehiclePreferenceID", marketAnalysisVehiclePreferenceId, DbType.Int32);
                CommonMethods.AddWithValue(command, "MarketAnalysisPriceProviderId", marketAnalysisPriceProviderId, DbType.Int32);
                CommonMethods.AddWithValue(command, "InsertUser", userName, DbType.String);
                command.ExecuteNonQuery();
                return;
            }
        }
    }
}
