﻿using System.Text;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation
{
    /// <summary>
    /// A cache decorator around an IMarketAnalysisPricingDataAccess.
    /// </summary>
    public class MarketAnalysisPricingDataAccessCache : IMarketAnalysisPricingDataAccess
    {
        private const string DELIM = ";";
        private const int CACHE_EXPIRATION_SECONDS = 10;

        private readonly IMarketAnalysisPricingDataAccess _dataAccess;
        private readonly ICache _cache;


        public MarketAnalysisPricingDataAccessCache( IMarketAnalysisPricingDataAccess dataAccess, ICache cache)
        {
            _dataAccess = dataAccess;
            _cache = cache;
        }

        public MarketAnalysisPricingInformationTO GetPricingInformation(string ownerHandle, string vehicleHandle, string searchHandle)
        {
            // get the cache key
            var key = CacheKey("MarketAnalysisPricingDataAccessCache.GetPricingInformation", ownerHandle, vehicleHandle,
                               searchHandle);

            // are the results in the cache?
            var results = _cache.Get(key) as MarketAnalysisPricingInformationTO;

            if (results != null)
            {
                return results;
            }

            // Get results from the db and cache them.
            results = _dataAccess.GetPricingInformation(ownerHandle, vehicleHandle, searchHandle);
            _cache.Set(key, results, CACHE_EXPIRATION_SECONDS);

            return results;
        }

        private static string CacheKey(string methodName, string ownerHandle, string vehicleHandle, string searchHandle)
        {
            var sb = new StringBuilder();
            sb.Append(methodName);
            sb.Append(DELIM);
            
            sb.Append("oh:");
            sb.Append(ownerHandle);
            sb.Append(DELIM);
            
            sb.Append("vh:");
            sb.Append(vehicleHandle);
            sb.Append(DELIM);
            
            sb.Append("sh:");
            sb.Append(DELIM);
            sb.Append(searchHandle);

            return sb.ToString();
        }
    }
}
