using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation
{
    [Serializable]
    public class MarketAnalysisVehiclePreferenceTO : IMarketAnalysisVehiclePreference
    {
        private int _marketAnalysisVehiclePreferenceId;
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;
        private bool _isDisplayed;
        private bool _isGaugeDisplayed;
        private bool _hasOverriddenPriceProviders;
        private IEnumerable<MarketAnalysisPriceProviders> _selectedPriceProviders;
        private int? _originalListPriceOverride;
        
        internal MarketAnalysisVehiclePreferenceTO(int? marketAnalysisVehiclePreferenceId, string ownerHandle, string vehicleHandle,
                                               bool isDisplayed, bool isGaugeDisplayed)
        {
            if (marketAnalysisVehiclePreferenceId.HasValue) _marketAnalysisVehiclePreferenceId = marketAnalysisVehiclePreferenceId.Value;
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
            _isDisplayed = isDisplayed;
            _isGaugeDisplayed = isGaugeDisplayed;
            _hasOverriddenPriceProviders = false;
            _selectedPriceProviders = null;
            _originalListPriceOverride = null;
        }

        internal  MarketAnalysisVehiclePreferenceTO(int? marketAnalysisVehiclePreferenceId, string ownerHandle, string vehicleHandle,
                                               bool isDisplayed, bool isGaugeDisplayed, bool hasOverriddenPriceProviders, IEnumerable<MarketAnalysisPriceProviders> selectedPriceProviders) 
            : this(marketAnalysisVehiclePreferenceId, ownerHandle, vehicleHandle, isDisplayed, isGaugeDisplayed)
        {
            _hasOverriddenPriceProviders = hasOverriddenPriceProviders;
            _selectedPriceProviders = selectedPriceProviders;
        }

        #region IMarketAnalysisVehiclePreference Members

        public int MarketAnalysisVehiclePreferenceId
        {
            get { return _marketAnalysisVehiclePreferenceId; }
            internal set { _marketAnalysisVehiclePreferenceId = value;}
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return _vehicleHandle; }
        }

        public bool IsDisplayed
        {
            get { return _isDisplayed; }
            set { _isDisplayed = value; }
        }

        public bool IsGaugeDisplayed
        {
            get { return _isGaugeDisplayed; }
            set { _isGaugeDisplayed = value; }
        }

        public bool HasOverriddenPriceProviders
        {
            get { return _hasOverriddenPriceProviders;}
            set { _hasOverriddenPriceProviders = value; }
        }

        public IEnumerable<MarketAnalysisPriceProviders> SelectedPriceProviders
        {
            get { return _selectedPriceProviders;}
            set { _selectedPriceProviders = value; }            
        }

        #endregion
    }
}
