using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation
{
    internal class MarketAnalysisPricingDataAccess : IMarketAnalysisPricingDataAccess
    {

        #region Implementation of IMarketAnalysisPricingDataAccess

        public MarketAnalysisPricingInformationTO GetPricingInformation(string ownerHandle, string vehicleHandle, string searchHandle)
        {
            return DoGet(ownerHandle, vehicleHandle, searchHandle);
        }

        private static MarketAnalysisPricingInformationTO DoGet(string ownerHandle, string vehicleHandle, string searchHandle)
        {

            var pricingInfo = DataPortal.Fetch<PricingInformation>(
                new PricingInformation.Criteria(ownerHandle, vehicleHandle,searchHandle, 1));
            
            
            int? listPrice = null;
            bool? hasOriginalMSRP = false;
            int? originalMsrp = null;
            bool? hasMarketPrice = false;
            int? marketPrice = null;
            bool? hasKbbPrice = false;
            int? kbbPrice = null;
            bool? kbbPriceIsAccurate = null;
            bool? hasNadaPrice = false;
            int? nadaPrice = null;
            bool? nadaPriceIsAccurate = false;
            bool? hasEdmundsPrice = false;
            int? edmundsPrice = null;
            bool? hasJdPowerPrice = false;
            int? jdPowerPrice = null;
            bool? hasBlackBookPrice = false;
            int? blackBookPrice= null;
            bool? blackBookPriceIsAccurate= null;
            bool? hasGalvesPrice = false;
            int? galvesPrice= null;
            bool? galvesPriceIsAccurate= null;


            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.MarketAnalysisPricingInformation";
                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "VehicleHandle", vehicleHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "SearchHandle", searchHandle, DbType.String);
                    IDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        int priceProviderId = reader.GetByte(reader.GetOrdinal("PriceProviderId"));

                        switch (priceProviderId)
                        {
                            case (int) MarketAnalysisPriceProviders.OfferPrice:
                                {
                                    throw new ApplicationException("The offer price should not be returned from the Marketing.MarketAnalysisPricingInformation stored procedure.");
                                }
                            case (int) MarketAnalysisPriceProviders.OriginalMSRP:
                                {
                                    hasOriginalMSRP = true;
                                    originalMsrp = GetNullableInt32(reader, "Price");
                                    break;
                                }
                            case (int) MarketAnalysisPriceProviders.CurrentInternetPrice:
                                {
                                    listPrice = GetNullableInt32(reader, "Price");
                                    break;
                                }
                            case (int) MarketAnalysisPriceProviders.PINGMarketAverageInternetPrice:
                                {
                                    hasMarketPrice = true;
                                    marketPrice = GetNullableInt32(reader, "Price");
                                    break;
                                }
                            case (int) MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice:
                                {
                                    hasJdPowerPrice = true;
                                    jdPowerPrice = GetNullableInt32(reader, "Price");
                                    break;
                                }
                            case (int) MarketAnalysisPriceProviders.EdmundsTrueMarketValue:
                                {
                                    hasEdmundsPrice = true;
                                    edmundsPrice = GetNullableInt32(reader, "Price");
                                    break;
                                }
                            case (int) MarketAnalysisPriceProviders.NADARetailValue:
                                {
                                    hasNadaPrice = true;
                                    nadaPrice = GetNullableInt32(reader, "Price");
                                    nadaPriceIsAccurate = GetPriceIsAccurate(reader);
                                    break;
                                }
                            case (int) MarketAnalysisPriceProviders.KelleyBlueBookRetailValue:
                                {
                                    hasKbbPrice = true;
                                    kbbPrice = GetNullableInt32(reader, "Price");
                                    kbbPriceIsAccurate = GetPriceIsAccurate(reader);
                                    break;
                                }
                            case (int)MarketAnalysisPriceProviders.BlackBook:
                                {
                                    hasBlackBookPrice = true;
                                    blackBookPrice = GetNullableInt32(reader, "Price");
                                    blackBookPriceIsAccurate = GetPriceIsAccurate(reader);
                                    break;
                                }
                            case (int)MarketAnalysisPriceProviders.Galves:
                                {
                                    hasGalvesPrice = true;
                                    galvesPrice = GetNullableInt32(reader, "Price");
                                    galvesPriceIsAccurate = GetPriceIsAccurate(reader);
                                    break;
                                }
                            default:
                                {
                                    throw new ApplicationException("An invalid PriceProviderId was obtained.  PriceProviderId of " + priceProviderId + " is not supported.");
                                }
                        }
                    }
                }
            }



            var transferObject = new MarketAnalysisPricingInformationTO(   ownerHandle, vehicleHandle, searchHandle,
                                                                           listPrice, hasOriginalMSRP, originalMsrp, 
                                                                           hasMarketPrice, marketPrice,
                                                                           hasKbbPrice, kbbPrice,
                                                                           kbbPriceIsAccurate, hasNadaPrice,
                                                                           nadaPrice, nadaPriceIsAccurate,
                                                                           hasEdmundsPrice, edmundsPrice,
                                                                           hasJdPowerPrice, jdPowerPrice, hasBlackBookPrice,
                                                                           blackBookPrice, blackBookPriceIsAccurate,
                                                                           hasGalvesPrice,galvesPrice, galvesPriceIsAccurate, 
                                                                           pricingInfo.DealerId, pricingInfo.HasKbbAsBook, 
                                                                           pricingInfo.HasKbbConsumerTool, pricingInfo.StockNumber, 
                                                                           pricingInfo.VehicleCatalogId, pricingInfo.ModelYear);
            return transferObject;
        }

        private static bool? GetPriceIsAccurate(IDataRecord reader)
        {

            if (reader.IsDBNull(reader.GetOrdinal("IsAccurate")))
            {
                // if it is not set to false (marked as inaccurate), it is considered accurate
                return true;
            }

            return reader.GetBoolean(reader.GetOrdinal("IsAccurate"));
        }

        private static int? GetNullableInt32(IDataRecord record, string columnName)
        {
            int? value = null;

            int ordinal = record.GetOrdinal(columnName);

            if (!record.IsDBNull(ordinal))
            {
                value = record.GetInt32(ordinal);
            }

            return value;
        }


        #endregion
    }
}
