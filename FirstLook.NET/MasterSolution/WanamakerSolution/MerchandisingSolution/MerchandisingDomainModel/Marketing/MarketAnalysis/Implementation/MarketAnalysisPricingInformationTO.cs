using System;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation
{
    [Serializable]
    public class MarketAnalysisPricingInformationTO : IMarketAnalysisPricingInformation
    {
        public MarketAnalysisPricingInformationTO(string ownerHandle, string vehicleHandle, string searchHandle,
                                                  int? listPrice,
                                                  bool? hasOriginalListPrice, int? originalMsrp,
                                                  bool? hasMarketPrice, int? marketPrice,
                                                  bool? hasKbbPrice, int? kbbPrice, bool? kbbPriceIsAccurate,
                                                  bool? hasNadaPrice, int? nadaPrice, bool? nadaPriceIsAccurate,
                                                  bool? hasEdmundsPrice, int? edmundsPrice,
                                                  bool? hasJdPowerPrice, int? jdPowerPrice,
                                                  bool? hasBlackBookPrice, int? blackBookPrice,
                                                  bool? blackBookPriceIsAccurate,
                                                  bool? hasGalvesPrice, int? galvesPrice, bool? galvesPriceIsAccurate,
                                                  int dealerId, bool hasKbbAsBook, bool hasKbbConsumerTool,
                                                  string stockNumber, int vehicleCatalogId, int modelYear
            )
        {
            OwnerHandle = ownerHandle;
            VehicleHandle = vehicleHandle;
            SearchHandle = searchHandle;
            ListPrice = listPrice;
            HasOriginalListPrice = hasOriginalListPrice;
            OriginalMSRP = originalMsrp;
            HasMarketPrice = hasMarketPrice;
            MarketPrice = marketPrice;
            HasKbbPrice = hasKbbPrice;
            KbbPrice = kbbPrice;
            KbbPriceIsAccurate = kbbPriceIsAccurate;
            HasNadaPrice = hasNadaPrice;
            NadaPrice = nadaPrice;
            NadaPriceIsAccurate = nadaPriceIsAccurate;
            HasEdmundsPrice = hasEdmundsPrice;
            EdmundsPrice = edmundsPrice;
            HasJdPowerPrice = hasJdPowerPrice;
            JdPowerPrice = jdPowerPrice;
            HasBlackBookPrice = hasBlackBookPrice;
            BlackBookPrice = blackBookPrice;
            BlackBookPriceIsAccurate = blackBookPriceIsAccurate;
            HasGalvesPrice = hasGalvesPrice;
            GalvesPrice = galvesPrice;
            GalvesPriceIsAccurate = galvesPriceIsAccurate;
            DealerId = dealerId;
            HasKbbAsBook = hasKbbAsBook;
            HasKbbConsumerTool = hasKbbConsumerTool;
            StockNumber = stockNumber;
            VehicleCatalogId = vehicleCatalogId;
            ModelYear = modelYear;
        }

        #region Implementation of IMarketAnalysisPricingInformationTO

        public string OwnerHandle { get; private set; }

        public string VehicleHandle { get; private set; }

        public string SearchHandle { get; private set; }

        public int? ListPrice { get; private set; }

        public bool? HasOriginalListPrice { get; private set; }

        public int? OriginalMSRP { get; set; }

        public bool? HasBlackBookPrice { get; private set; }

        public int? BlackBookPrice { get; private set; }

        public bool? BlackBookPriceIsAccurate { get; private set; }

        public bool? HasGalvesPrice { get; private set; }

        public int? GalvesPrice { get; private set; }

        public bool? GalvesPriceIsAccurate { get; private set; }

        public int DealerId { get; private set; }

        public bool HasKbbAsBook { get; private set; }

        public bool HasKbbConsumerTool { get; private set; }

        public string StockNumber { get; private set; }

        public int VehicleCatalogId { get; private set; }

        public int ModelYear { get; private set; }

        public bool? HasMarketPrice { get; private set; }

        public int? MarketPrice { get; private set; }

        public bool? HasKbbPrice { get; private set; }

        public int? KbbPrice { get; private set; }

        public bool? KbbPriceIsAccurate { get; private set; }

        public bool? HasNadaPrice { get; private set; }

        public int? NadaPrice { get; private set; }

        public bool? NadaPriceIsAccurate { get; private set; }

        public bool? HasEdmundsPrice { get; private set; }

        public int? EdmundsPrice { get; private set; }

        public bool? HasJdPowerPrice { get; private set; }

        public int? JdPowerPrice { get; private set; }

        #endregion
    }
}
