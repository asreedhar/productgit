﻿using System;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdPreview
{
    public class AdPreviewVehiclePreferenceTO
    {
        public int PreferenceID { get; set; }
        public String VehicleHandle { get; set; }
        public String OwnerHandle { get; set; }
        public int CharacterLimit { get; set; }
    }
}
