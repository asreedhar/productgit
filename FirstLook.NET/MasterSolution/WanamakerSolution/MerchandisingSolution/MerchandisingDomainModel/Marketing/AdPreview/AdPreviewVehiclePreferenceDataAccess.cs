﻿using System;
using FirstLook.Common.Data;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdPreview
{
    public class AdPreviewVehiclePreferenceDataAccess : IAdPreviewVehiclePreferenceDataAccess
    {
        private const int DefaultCharacterLimit = 250;

        public AdPreviewVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle)
        {
            var dto = new AdPreviewVehiclePreferenceTO();

            dto.OwnerHandle = ownerHandle;
            dto.VehicleHandle = vehicleHandle;
            dto.CharacterLimit = DefaultCharacterLimit;
            return dto;
        }

        public AdPreviewVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle)
        {
            var dto = new AdPreviewVehiclePreferenceTO();
            dto.OwnerHandle = ownerHandle;
            dto.VehicleHandle = vehicleHandle;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.AdPreviewVehiclePreference#Fetch";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, ownerHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new DataException("Missing information");

                        dto.PreferenceID = reader.GetInt32(reader.GetOrdinal("AdPreviewVehiclePreferenceId"));
                        dto.CharacterLimit = reader.GetInt32(reader.GetOrdinal("CharacterLimit"));
                    }
                }
            }

            return dto;
        }

        public void Insert(AdPreviewVehiclePreferenceTO dto, string userName)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.AdPreviewVehiclePreference#Insert";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("vehicleHandle", DbType.String, false, dto.VehicleHandle);
                        command.AddParameterWithValue("ownerHandle", DbType.String, false, dto.OwnerHandle);
                        command.AddParameterWithValue("CharacterLimit", DbType.Int32, false, dto.CharacterLimit);
                        command.AddParameterWithValue("InsertUser", DbType.String, false, userName);

                        //Add the out param
                        IDataParameter newId = command.AddOutParameter("AdPreviewVehiclePreferenceId", DbType.Int32);

                        // Execute and capture the ID
                        command.ExecuteNonQuery();
                        dto.PreferenceID = (Int32)newId.Value;
                    }
                    transaction.Commit();
                }
            }
        }

        public void Update(AdPreviewVehiclePreferenceTO dto, string userName)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.AdPreviewVehiclePreference#Update";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("AdPreviewVehiclePreferenceId", DbType.Int32, false, dto.PreferenceID);
                        command.AddParameterWithValue("CharacterLimit", DbType.Int32, false, dto.CharacterLimit);
                        command.AddParameterWithValue("UpdateUser", DbType.String, false, userName);

                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                connection.Open();
                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.AdPreviewVehiclePreference#Exists";
                    command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, ownerHandle);

                    // create return value parameter
                    IDataParameter parameter = command.CreateParameter();
                    parameter.ParameterName = "ReturnValue";
                    parameter.DbType = DbType.Int32;
                    parameter.Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add(parameter);

                    int count = 0;
                    command.ExecuteNonQuery();

                    IDataParameter retParem = command.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }

                    return (count > 0);
                }
            }
        }

        public void Delete(int preferenceId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(FirstLook.Pricing.DomainModel.Database.InventoryDatabase))
            {
                connection.Open();
                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.AdPreviewVehiclePreference#Delete";
                        command.Transaction = transaction;

                        command.AddParameterWithValue("MarketListingVehiclePreferenceId", DbType.Int32, false, preferenceId);
                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }
    }
}
