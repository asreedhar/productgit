﻿namespace FirstLook.Merchandising.DomainModel.Marketing.AdPreview
{
    public interface IAdPreviewVehiclePreferenceDataAccess
    {
        AdPreviewVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle);
        AdPreviewVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle);
        void Insert(AdPreviewVehiclePreferenceTO dto, string userName);
        void Update(AdPreviewVehiclePreferenceTO dto, string userName);
        bool Exists(string ownerHandle, string vehicleHandle);
        void Delete(int preferenceId);
    }
}
