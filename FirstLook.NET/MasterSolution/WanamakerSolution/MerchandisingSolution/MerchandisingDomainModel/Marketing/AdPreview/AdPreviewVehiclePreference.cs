﻿using System;
using System.Text.RegularExpressions;
using Csla;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Marketing.Vehicle;

namespace FirstLook.Merchandising.DomainModel.Marketing.AdPreview
{
    [Serializable]
    public class AdPreviewVehiclePreference : InjectableBusinessBase<AdPreviewVehiclePreference>
    {
        private int _preferenceID;
        private string _ownerHandle;
        private string _vehicleHandle;
        private int _characterLimit;
        private string _advertisement;
        
        public IAdPreviewVehiclePreferenceDataAccess DataAccess { get; set; }

        public int CharacterLimit
        {
            get
            {
                return _characterLimit;
            }
            set
            {
                _characterLimit = value;
                PropertyHasChanged("CharacterLimit");
            }
        }

        public string Advertisement
        {
            get
            {
                return _advertisement;
            }
        }


        private string UserName
        {
            get { return ApplicationContext.User.Identity.Name; }
        }


        protected override object GetIdValue()
        {
            return _vehicleHandle;
        }

        private void FromTransferObject(AdPreviewVehiclePreferenceTO preferenceTO)
        {
            _preferenceID = preferenceTO.PreferenceID;
            _ownerHandle = preferenceTO.OwnerHandle;
            _vehicleHandle = preferenceTO.VehicleHandle;
            _characterLimit = preferenceTO.CharacterLimit;

            var summary = VehicleSummary.GetVehicleSummary(_ownerHandle, _vehicleHandle);
            _advertisement = summary.Advertisement ?? String.Empty;
            _advertisement = Regex.Replace(_advertisement, @"\s+", " ");
        }

        private AdPreviewVehiclePreferenceTO ToTransferObject()
        {
            AdPreviewVehiclePreferenceTO preferenceTO = new AdPreviewVehiclePreferenceTO();

            preferenceTO.PreferenceID = _preferenceID;
            preferenceTO.OwnerHandle = _ownerHandle;
            preferenceTO.VehicleHandle = _vehicleHandle;
            preferenceTO.CharacterLimit = _characterLimit;

            return preferenceTO;
        }

        private static AdPreviewVehiclePreference NewAdPreviewVehiclePreference(String vehicleHandle, String ownerHandle)
        {
            return DataPortal.Create<AdPreviewVehiclePreference>(new Criteria(vehicleHandle, ownerHandle));
        }

        private static AdPreviewVehiclePreference GetAdPreviewVehiclePreference(String vehicleHandle, String ownerHandle)
        {
            return DataPortal.Fetch<AdPreviewVehiclePreference>(new Criteria(vehicleHandle, ownerHandle));
        }

        public static AdPreviewVehiclePreference GetOrCreate(string vehicleHandle, string ownerHandle)
        {
            ExistsCommand command = new ExistsCommand(vehicleHandle, ownerHandle);
            DataPortal.Execute(command);

            return command.Exists ? GetAdPreviewVehiclePreference(vehicleHandle, ownerHandle) :
                                    NewAdPreviewVehiclePreference(vehicleHandle, ownerHandle);
        }

        public string GetTruncatedAdvertisment()
        {
            var words = Regex.Split(_advertisement, @"\s+");
            bool isTruncated = false;

            int charCount = 0;
            int wordCount = 0;
            foreach (string word in words)
            {
                charCount += word.Length + 1; //add one for whitespace
                wordCount++;
                if(charCount >= _characterLimit)
                {
                    isTruncated = true;
                    break;
                }
            }

            string ad = String.Join(" ", words.Take(wordCount).ToArray());
            if(isTruncated)
                ad = Regex.Replace(ad, @"[,]$", String.Empty) + " ...";

            return ad;
        }

        private void DataPortal_Create(Criteria createCriteria)
        {
            var dto = DataAccess.Create(createCriteria.OwnerHandle, createCriteria.VehicleHandle);
            FromTransferObject(dto);   
          
            MarkNew();
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            var dto = DataAccess.Fetch(criteria.OwnerHandle, criteria.VehicleHandle);
            FromTransferObject(dto);

            MarkOld();
        }

        protected override void DataPortal_Insert()
        {
            var dto = ToTransferObject();
            DataAccess.Insert(dto, UserName);
            FromTransferObject(dto);

            MarkOld();
        }

        protected override void DataPortal_Update()
        {
            if (!IsDirty)
                return;

            var dto = ToTransferObject();
            DataAccess.Update(dto, UserName);

            MarkOld();
        }

        protected override void DataPortal_DeleteSelf()
        {
            DataAccess.Delete(_preferenceID);
        }

        [Serializable]
        private class Criteria
        {
            public string VehicleHandle { get; private set; }
            public string OwnerHandle { get; private set; }

            public Criteria(String vehicleHandle, String ownerHandle)
            {
                VehicleHandle = vehicleHandle;
                OwnerHandle = ownerHandle;
            }
        }

        [Serializable]
        private class ExistsCommand : InjectableCommandBase
        {
            private readonly String _vehicleHandle = String.Empty;
            private readonly String _ownerHandle = String.Empty;
            private Boolean _exists;

            public IAdPreviewVehiclePreferenceDataAccess DataAccess { get; set; }
            
            public bool Exists
            {
                get { return _exists; }
            }

            public ExistsCommand(String vehicleHandle, String ownerHandle)
            {
                _vehicleHandle = vehicleHandle;
                _ownerHandle = ownerHandle;
            }

            protected override void DataPortal_Execute()
            {
                _exists = DataAccess.Exists(_ownerHandle, _vehicleHandle);
            }
        }

    }
}
