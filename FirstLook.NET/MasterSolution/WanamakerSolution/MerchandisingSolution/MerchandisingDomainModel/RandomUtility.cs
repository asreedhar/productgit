using System;
using System.Collections.Generic;
using System.Text;
using FirstLook.Merchandising.DomainModel.Templating;
using log4net;

namespace FirstLook.Merchandising.DomainModel
{
    [Serializable]
    public class RandomUtility : Random
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(RandomUtility).FullName);
        #endregion


        public RandomUtility() : base(DateTime.Now.Millisecond)
        {
            
        }
        public RandomUtility(int seed) : base(seed)
        {
            
        }

        public T GetRandomValue<T>(T[] array)
        {
            int ndx = Next(array.Length);
            Log.DebugFormat("Index {0} selected.", ndx);

            var val = array[ndx];
            Log.DebugFormat("Returning {0}", val);
            return val;
        }

        public T[] RandomPermutation<T>(List<T> array)
        {
            Log.Debug("RandomPermutation() called.");

            // null check
            if( array == null )
            {
                Log.Warn("Null array passed in, returning empty array.");
                return new T[0];
            }

            int len = array.Count;
            T[] retArray = new T[len];
            array.CopyTo(retArray, 0);

            for (int i = 0; i < len; i += 1)
            {
                Log.DebugFormat("Inspeecting index {0}", i);

                int swapIndex = Next(i, len);
                Log.DebugFormat("The swapIndex is {0}", swapIndex);

                if (swapIndex != i)
                {
                    Log.DebugFormat("Prior to swap, retArray[{0}] has value {1} and retArray[{2}] has value {3}.", i, retArray[i], swapIndex, retArray[swapIndex]);

                    T temp = retArray[i];
                    retArray[i] = retArray[swapIndex];
                    retArray[swapIndex] = temp;

                    Log.DebugFormat("After the swap, retArray[{0}] has value {1} and retArray[{2}] has value {3}.", i, retArray[i], swapIndex, retArray[swapIndex]);

                }
            }

            return retArray;
        }

        public List<IWeightedItem> RandomWeightedPermutation(List<IWeightedItem> list)
        {
            Log.DebugFormat("RandomWeightedPermutation() called with list {0}", PrintItemList(list));

            list.Sort(new StochasticWeightedItemComparer());
            return list;
        }

        private static string PrintItemList(IList<IWeightedItem> list)
        {
            if(list == null)
            {
                return "List is null.";
            }

            if( list.Count == 0 )
            {
                return "List is empty.";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("IList<IWeightedItem> contains: ");

            foreach(var item in list)
            {
                sb.Append(";Tier: ");
                sb.Append(item.GetTierNumber());
                sb.Append(", Weight: ");
                sb.Append(item.GetWeight());
            }

            sb.Append("End of list.");
            return sb.ToString();
        }

    }
}
