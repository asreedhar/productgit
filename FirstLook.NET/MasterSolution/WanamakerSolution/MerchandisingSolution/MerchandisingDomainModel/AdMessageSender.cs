﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using AmazonServices.SNS;
using Core.Messages;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.DocumentPublishing;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using log4net;
using MAX.Entities;
using MAX.Entities.Messages;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.AutoLoad;
using Merchandising.Messages.AutoOffline;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.GroupLevel;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;
using Merchandising.Messages.WebLoader;

namespace FirstLook.Merchandising.DomainModel
{
    public class AdMessageSender : IAdMessageSender, IDocumentPublishMessager, IPricingMessager, IWebloaderMessager
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IQueueFactory _factory;
        private IDiscountCampaignQueueFactory _discountQueueFactory;
        private IEmailQueueFactory _emailQueueFactory;
        private IGroupLevelDataCache _groupLevelCache;

        public bool SendMessageAsync { get; set; }

        public AdMessageSender(IQueueFactory factory, IDiscountCampaignQueueFactory discountQueueFactory, IEmailQueueFactory emailQueueFactory, IGroupLevelDataCache groupLevelDataCache)
        {
            _factory = factory;
            _groupLevelCache = groupLevelDataCache;
	        _discountQueueFactory = discountQueueFactory;
	        _emailQueueFactory = emailQueueFactory;
        }

	    public void SendApproval(ApproveMessage message, string sentFrom)
        {
            var queue = _factory.CreateApprove();
            //queue.SendMessage(message, sentFrom);
            SendMessage<ApproveMessage>(queue, message, sentFrom);
        }

        public void BootStrapGroupLevelIfNecessary(GroupLevelAggregationMessage message, string sentFrom)
        {
            var status = _groupLevelCache.GetCachedStatus(message.GroupId);
            if (status.State != State.Empty) //If we have some data don't regenerate it
                return;

            var queue = _factory.CreateGroupLevelAggregation();
            queue.SendMessage(message, sentFrom);
        }

        /// <summary>
        /// Generates ad automatically. When certain vehicle attributes change
        /// </summary>
        /// <param name="message"></param>
        /// <param name="isNew">Is this a new car?</param>
        public void SendAutoApproval(AutoApproveMessage message, bool isNew, string sentFrom)
        {

            // ZB: I'd really prefer that AdMessageSender figure out if the car is new or not.  Why should every
            // message publisher have to figure this out?  Kind of leaky.
            var settings = GetAutoApproveSettings.GetSettings(message.BusinessUnitId);

            var queue = _factory.CreateAutoApprove();
            if (settings.HasSettings && settings.Status == AutoApproveStatus.OnForAll)
                //queue.SendMessage(message, sentFrom);
                SendMessage<AutoApproveMessage>(queue, message, sentFrom);
            else if(settings.HasSettings && settings.Status == AutoApproveStatus.OnForNew && isNew)
                //queue.SendMessage(message, sentFrom);
                SendMessage<AutoApproveMessage>(queue, message, sentFrom);
            else if(settings.HasSettings && settings.Status == AutoApproveStatus.OnForUsed && !isNew)
                //queue.SendMessage(message, sentFrom);
                SendMessage<AutoApproveMessage>(queue, message, sentFrom);
            
        }

        /// <summary>
        /// This will work for 9.2.  AutoApprove is either on or off.  It doesn't vary by new/used.
        /// </summary>
        /// <param name="message"></param>
        public void SendAutoApproval(AutoApproveMessage message, string sentFrom)
        {
            // TODO: In 10.0 we think we will start distinguishing between new/used, etc...
            var settings = GetAutoApproveSettings.GetSettings(message.BusinessUnitId);
            if (!settings.HasSettings) return;

            var queue = _factory.CreateAutoApprove();
            var status = settings.Status;

            if (status != AutoApproveStatus.Off)
            {
                //queue.SendMessage(message, sentFrom);
                SendMessage<AutoApproveMessage>(queue, message, sentFrom);
            }
        }

        public void SendAutoApproval(IEnumerable<int> inventoryIds, int businessUnitId, string user, string sentFrom)
        {
            var queue = _factory.CreateAutoApprove();
            foreach (var inventoryId in inventoryIds)
            {
                //queue.SendMessage(new AutoApproveMessage(businessUnitId, inventoryId, user), sentFrom);
                SendMessage<AutoApproveMessage>(queue, new AutoApproveMessage(businessUnitId, inventoryId, user), sentFrom);
            }
        }

        //Autoload all vehicles in business unit
        public void SendBatchAutoLoadInventory(AutoLoadInventoryMessage message, string sentFrom)
        {
            var queue = _factory.CreateAutoLoadInventory();
            //queue.SendMessage(message, sentFrom);
            SendMessage<AutoLoadInventoryMessage>(queue, message, sentFrom);
        }

        public void SendBatchPriceChange(BatchPriceChangeMessage message, string sentFrom)
        {
            var queue = _discountQueueFactory.CreateBatchPriceChangeQueue();
            //queue.SendMessage(message, sentFrom);
            SendMessage<BatchPriceChangeMessage>(queue, message, sentFrom);
        }

        public void SendPricingCampaignMessage(int businessUnitId, int inventoryId, string userName, string sentFrom)
        {
            var queue = _discountQueueFactory.CreateApplyInventoryDiscountCampaignQueue();
            queue.SendMessage(new ApplyInventoryDiscountCampaignMessage(businessUnitId, inventoryId, userName), sentFrom);
        }

	    public void SendPublishDocumentsNotification(int businessUnitId, int inventoryId, string vin, string sentFrom)
	    {
			BusFactory.CreatePublishDocumentsBus<MaxDocumentPublishMessage, MaxDocumentPublishMessage>()
				.Publish(new MaxDocumentPublishMessage(businessUnitId, inventoryId, vin), sentFrom);
	    }

	    public void SendPublishAllDocumentNotifications(int businessUnitId, string sentFrom)
	    {
		    var inventoryList = InventoryDataFilter.FetchListFromSession(businessUnitId);
		    foreach (var result in inventoryList){
				SendPublishDocumentsNotification(result.BusinessUnitID, result.InventoryID, result.VIN, sentFrom);
		    }
	    }


	    public void SendStyleSet(int businessUnitId, int inventoryId, string user, string sentFrom)
        {
            var styleSetBus = BusFactory.CreateStyleSet<StyleSetMessage, StyleSetMessage>();
            styleSetBus.Publish(new StyleSetMessage(businessUnitId, inventoryId, user), sentFrom);
        }

        //Approve all vehicles with no advertisement (Ad Approval Needed)
        public void SendAutoApprovalInventory(AutoApproveInventoryMessage message, string sentFrom)
        {
			var queue = _factory.CreateAutoApproveInventory();
            //queue.SendMessage(message, sentFrom);
            SendMessage<AutoApproveInventoryMessage>(queue, message, sentFrom);
        }

		public void SendAutoApproveAllInventory(int businessUnitId, string sentFrom)
		{
			var settings = GetAutoApproveSettings.GetSettings(businessUnitId);
			SendAutoApprovalInventory(new AutoApproveInventoryMessage(businessUnitId, settings.Status, WorkflowType.AllInventory), sentFrom);
		}

        public void SendCreatePdfMessage(int businessUnitId, int inventoryId, string approvalUser)
        {
            var writeQueue = _factory.CreatePdf();
            //writeQueue.SendMessage(new CreatePdfMessage(businessUnitId, inventoryId, approvalUser), GetType().FullName);
            SendMessage<CreatePdfMessage>(writeQueue, new CreatePdfMessage(businessUnitId, inventoryId, approvalUser), GetType().FullName);
        }

        public void PublishMaxDocumentPublishMessage(string vin, int businessUnitId, int inventoryId, string sentFrom)
        {
            var bus = BusFactory.CreatePublishDocumentsBus<MaxDocumentPublishMessage, MaxDocumentPublishMessage>();
            bus.Publish(
                new MaxDocumentPublishMessage
                {
                    Vin = vin,
                    BusinessUnitId = businessUnitId,
                    InventoryId = inventoryId
                },
                sentFrom);
        }

        public void SendBucketValueMessage(BucketValueMessage message, string sentFrom)
        {
            var bucketValueQueue = _emailQueueFactory.CreateBucketValueQueue();
            SendMessage<BucketValueMessage>(bucketValueQueue, message, sentFrom);
            //bucketValueQueue.SendMessage(message, sentFrom);
        }

        public void SendProcessAutoOfflineMessage(int? businessUnitId, string sentFrom)
        {
            var queue = _factory.CreateProcessAutoOffline();
            SendMessage<ProcessAutoOfflineMessage>(queue, new ProcessAutoOfflineMessage(businessUnitId), sentFrom);
            //queue.SendMessage(new ProcessAutoOfflineMessage(businessUnitId), sentFrom);
        }

        private void SendMessage<T>(IQueue<T> sender, T message,string sentFrom)
        {
            if (SendMessageAsync)
            {
                sender.SendMessageAsync(message,sentFrom);
            }
            else
            {
                sender.SendMessage(message, sentFrom);
            }
        }
        // IDocumentPublishMessager implementation
        public void PublishInactiveInventoryMessage(int businessUnitId, string ownerHandle, DocumentPublishing.Repositories.InventorySummary summary, Pair<Pricing.VehicleHandle, Pricing.SearchHandle> handles)
        {
            var activityBus = BusFactory.CreateInventoryActivityBus<InventoryActivityMessage, InventoryActivityMessage>();

            var receivedDate = summary.InventoryReceivedDate.ToUniversalTime().ToString(CultureInfo.InvariantCulture);

            if (summary.InventoryActive)
            {
                Log.WarnFormat("ProcessInActiveInventorySummary called on an active inventory item with inventoryId {0}.", summary.InventoryId);
                return;
            }

            // it's not active            
            var publishDeleteDate = string.Empty;
            if (summary.DeleteDt.HasValue)
            {
                publishDeleteDate = summary.DeleteDt.Value.ToUniversalTime().ToString(CultureInfo.InvariantCulture);
            }

            // Notify topic that the inventory is no longer active.
            activityBus.Publish(new InventoryActivityMessage
            {
                Active = false,
                InventoryReceivedDateUtc = receivedDate,
                InventoryDeletedDateUtc = publishDeleteDate,
                BusinessUnitId = businessUnitId,
                InventoryId = summary.InventoryId,
                OwnerHandle = ownerHandle,
                VehicleHandle = handles.First.Value,
                Vin = summary.Vin
            }, "InactiveInventoryTask");
            Log.DebugFormat("InventoryActivityMessage (inactive) sent for inventory {0}, VIN {1}", summary.InventoryId, summary.Vin);
        }

        public void SendDocumentPublishNotification(PublicationInfo publicationInfo)
        {
            var bucketName = publicationInfo.FileStore.ContainerName();
            var key = publicationInfo.Key;

            var topicMessage = new
            {
                bucket = bucketName,
                key,
                url = String.Format("https://s3.amazonaws.com/{0}/{1}", bucketName, key),
                generated_at = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                sender = publicationInfo.Sender,
                message_type = publicationInfo.MessageType,
                content_type = publicationInfo.ContentType,
                vin = publicationInfo.Vin
            };

            var messageJson = topicMessage.ToJson();

            var topicArn = GenericTopic.CreateTopic(publicationInfo.NotificationTopic, true, true, '-');
            var messageId = GenericTopic.Publish(messageJson, topicArn);

            Log.DebugFormat("MAX document update message with id {0} published to SNS.", messageId);
        }

        public void SendPhotoPublishMessage(int businessUnitId, string vin, string xmlToPublish)
        {
            var bus = BusFactory.CreatePublishPhotoDocumentsBus<MaxPhotoPublishMessage, MaxPhotoPublishMessage>();
            bus.Publish(new MaxPhotoPublishMessage(businessUnitId, vin, xmlToPublish), "PhotoDocumentGenerationTask");
        }


        // IPricingMessager implementation
        public void SendBatchPriceMessages(int businessUnitId, IEnumerable<int> inventoryIDs)
        {
            var bus = BusFactory.CreatePriceChange<PriceChangeMessage, PriceChangeMessage>();

            // Can't find anywhere that actually uses the EventId or Price, so I'm not going to fake it up in the sender.
            inventoryIDs
                .ToList()
                .ForEach(x => bus.Publish(new PriceChangeMessage(0, businessUnitId, x), GetType().FullName));
        }

        public void SendApplyInventoryDiscountCampaignMessage(StyleSetMessage message)
        {
            var writeQueue = CreateApplyInventoryDiscountCampaignQueue();
            writeQueue.SendMessage(new ApplyInventoryDiscountCampaignMessage(message.BusinessUnitId, message.InventoryId, message.User), GetType().FullName);
        }

        public void SendApplyInventoryDiscountCampaignMessage(int businessUnitId, IEnumerable<int> inventoryIds,
            string activatedBy, bool force)
        {
            var writeQueue = CreateApplyInventoryDiscountCampaignQueue();
            foreach (var inventoryId in inventoryIds)
            {
                writeQueue.SendMessage(
                    new ApplyInventoryDiscountCampaignMessage(businessUnitId, inventoryId, activatedBy, force), GetType().Name);
            }
        }

        public void SendApplyDiscountMessage(int businessUnitId, int inventoryId, string discountActivatedBy)
        {
            var discountQueue = _discountQueueFactory.CreateApplyInventoryDiscountCampaignQueue();

            var message = new ApplyInventoryDiscountCampaignMessage
            {
                ActivatedBy = discountActivatedBy,
                BusinessUnitId = businessUnitId,
                InventoryId = inventoryId
            };

            discountQueue.SendMessage(message, GetType().FullName);
        }

        public void SendInventoryChangedMessage(int businessUnitId, int inventoryId)
        {
            var writeQueue = _factory.CreateInventoryChanged();
            writeQueue.SendMessage(new InventoryMessage(businessUnitId, inventoryId), GetType().FullName);
        }

        
        //IWebLoaderMessager implementation
        public void SendPhotoUploadMessage(PhotoUploadMessage[] photoMessages, int photoCount)
        {
            var writeQueue = _factory.CreateWebLoaderPhotoUpload();
            foreach (var photo in photoMessages)
            {
                photo.Position = photo.Position + photoCount;
                writeQueue.SendMessage(photo, GetType().FullName);
            }
        }

        public void SendPhotoUploadMessage(PhotoUploadMessage message)
        {
            var writeQueue = _factory.CreateWebLoaderPhotoUpload();
            writeQueue.SendMessage(message, GetType().FullName);
        }


        // Queue Creation
        public IQueue<StyleSetMessage> CreateStyleSetQueue()
        {
            return _discountQueueFactory.CreateCampaignStyleSet();
        }

        public IQueue<ApplyInventoryDiscountCampaignMessage> CreateApplyInventoryDiscountCampaignQueue()
        {
            return _discountQueueFactory.CreateApplyInventoryDiscountCampaignQueue();
        }


        // Bus Creation
        public IBus<StyleSetMessage, StyleSetMessage> CreateStyleSetBus()
        {
            return BusFactory.CreateStyleSet<StyleSetMessage, StyleSetMessage>();
        }

    }
}
