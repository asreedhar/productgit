using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.MarketingData
{
    public class MarketingDataPoint
    {
        public MarketingDataPoint(int id, string marketingText, bool isVerbatim, MarketingSource source, List<MarketingTag> tags)
        {
            Id = id;
            MarketingText = marketingText;
            IsVerbatim = isVerbatim;
            Source = source;
            Tags = tags;
        }

        public int Id { get; set; }

        public string MarketingText { get; set; }

        public bool IsVerbatim { get; set; }

        public MarketingSource Source { get; set; }

        public List<MarketingTag> Tags { get; set; }

        private readonly string[] _verbatimFormatStrings = new[] { "\"{0}\" -{1}", "{1} explains \"{0}\"", "{1}'s review says \"{0}\""};
        private readonly string[] _nonVerbatimFormatStrings = new[] { "{1} says - {0}", "From {1}: {0}"};

        public string ToString(RandomUtility randomizer)
        {
            if (!Source.IsMedley)
            {
                if (IsVerbatim)
                {
                    return string.Format(randomizer.GetRandomValue(_verbatimFormatStrings), MarketingText,
                                         Source.SourceName);
                }
                
                if (!string.IsNullOrEmpty(Source.SourceName))
                {
                    return string.Format(randomizer.GetRandomValue(_nonVerbatimFormatStrings), MarketingText,
                                         Source.SourceName);
                }
            }
            return MarketingText;
            
        }
    }
}
