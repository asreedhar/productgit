using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.MarketingData
{
    public class MarketingDataCollection : CollectionBase
    {
        public int Add(MarketingDataPoint point)
        {
            return InnerList.Add(point);
        }

        public static MarketingDataCollection Fetch(int chromeStyleId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "snippets.marketing#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ChromeStyleId", chromeStyleId, DbType.Int32);

                    MarketingDataCollection mdc = new MarketingDataCollection();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            mdc.Add(new MarketingDataPoint(
                                    reader.GetInt32(reader.GetOrdinal("marketingId")),
                                    reader.GetString(reader.GetOrdinal("marketingText")).Trim(),
                                    reader.GetBoolean(reader.GetOrdinal("isVerbatim")),
                                    new MarketingSource(
                                        reader.GetInt32(reader.GetOrdinal("sourceId")),
                                        reader.GetString(reader.GetOrdinal("sourceName")),
                                        reader.GetBoolean(reader.GetOrdinal("hasCopyright"))),
                                    new List<MarketingTag>()
                                    ));         
                        }
                        Dictionary<int, List<MarketingTag>> map = new Dictionary<int, List<MarketingTag>>();
                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                int mktId = reader.GetInt32(reader.GetOrdinal("marketingId"));
                                if (!map.ContainsKey(mktId))
                                {
                                    map.Add(mktId, new List<MarketingTag>());
                                }
                                map[mktId].Add((MarketingTag)reader.GetInt32(reader.GetOrdinal("tagId")));
                            }
                        }

                        foreach (MarketingDataPoint mdp in mdc)
                        {
                            if (map.ContainsKey(mdp.Id))
                            {
                                mdp.Tags = map[mdp.Id];
                            }
                        }

                        return mdc;
                    }
                }
            }
        }

        public MarketingDataCollection FindByTag(MarketingTag tag)
        {
            MarketingDataCollection retList = new MarketingDataCollection();

            foreach (MarketingDataPoint mdp in InnerList)
            {
                if (mdp.Tags.Contains(tag))
                {
                    retList.Add(mdp);
                }
            }
            return retList;
        }
        public MarketingDataPoint Get(int index)
        {
            return (MarketingDataPoint)InnerList[index];
        }

        public MarketingDataPoint this[int index]
        {
            get { return Get(index); }
        }

        public override string ToString()
        {
            return ToString(1,false, new RandomUtility(DateTime.Now.Millisecond));
        }

        public string ToString(int maxSentenceCount, bool allowDuplicateSource, RandomUtility randomizer)
        {
            int ct = 0;
            List<MarketingSource> sourcesUsed = new List<MarketingSource>();
            StringBuilder sb = new StringBuilder();
            foreach (MarketingDataPoint mdp in InnerList)
            {
                MarketingSource source = mdp.Source;
                if (allowDuplicateSource || !sourcesUsed.Exists(delegate(MarketingSource cmp) { return cmp.Id == source.Id;}))
                {
                    if (ct >= maxSentenceCount)
                    {
                        break;
                    }

                    sb.Append(mdp.ToString(randomizer)).Append("  ");
                    sourcesUsed.Add(source);

                    ct++;
                }

            }
            return sb.ToString().Trim();
        }
        
    }
}
