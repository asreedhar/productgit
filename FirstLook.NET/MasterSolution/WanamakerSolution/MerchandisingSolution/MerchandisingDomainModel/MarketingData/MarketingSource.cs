namespace FirstLook.Merchandising.DomainModel.MarketingData
{
    public class MarketingSource
    {
        public MarketingSource(int id, string sourceName, bool hasCopyright)
        {
            SourceName = sourceName;
            Id = id;
            HasCopyright = hasCopyright;
        }

        public int Id { get; set; }

        public string SourceName { get; set; }

        public bool HasCopyright { get; set; }

        public bool IsMedley
        {
            get { return SourceName.ToLower() == "medley"; }
        }
    }
}
