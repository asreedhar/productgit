namespace FirstLook.Merchandising.DomainModel.MarketingData
{
    public enum MarketingTag
    {
        ManufacturerMarketing = 3,
        GenerationChanges = 4,
        ModelComparisons = 5,
        ModelAward = 6,
        Interior = 8,
        Introduction = 17,
        Exterior = 18,
        Performance = 19,
        Safety = 20,
        StandardFeatures = 21,
        CompetitorComparison = 22,
        Space = 23,
        Cargo = 24,
        Hauling = 25,
        Design = 26,
        FuelEconomy = 27,
        Price = 28,
        Sound = 29,
        Family = 30,
        Sporty = 32,
        CityDwelling = 33,
        Superlative = 35,
        Preview = 36,
        Comfort = 37,
        Luxury = 38,
        Accolades = 39,
        Assemblage = 40,
        Technology = 41,
        Reliability = 42,
        Refinement = 43,
        Attractive = 44,
        Appearance = 45,
        Value = 46,
        Versatilit = 47

    }
}
