namespace FirstLook.Merchandising.DomainModel.Postings
{
    public enum AdvertisementPostingStatus
    {
        Error,
        NeedsAction,
        PendingApproval,
        CurrentlyReleasing,
        NoActionRequired
    }
}
