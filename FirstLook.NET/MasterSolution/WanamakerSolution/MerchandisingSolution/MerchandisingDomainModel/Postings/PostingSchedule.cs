using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Release;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class PostingSchedule
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(PostingSchedule).FullName);

        #endregion


        public AdvertisementStatus StatusCode { get; set; }

        public int InventoryId { get; set; }

        public PostingSchedule(int businessUnitID, int inventoryId, List<int> destinationIDs, DateTime postTime,
                               string approvedByMemberLogin, DateTime approvalTimestamp, AdvertisementStatus statusCode)
        {
            DestinationIDs = destinationIDs;
            InventoryId = inventoryId;
            BusinessUnitID = businessUnitID;
            PostTime = postTime;
            ApprovedByMemberLogin = approvedByMemberLogin;
            ApprovalTimestamp = approvalTimestamp;
            StatusCode = statusCode;
        }

        public List<int> DestinationIDs { get; set; }


        public int BusinessUnitID { get; set; }

        public DateTime PostTime { get; set; }

        public string ApprovedByMemberLogin { get; set; }

        public DateTime ApprovalTimestamp { get; set; }


        public bool PostToDestination(int destinationID)
        {
            return DestinationIDs.Contains(destinationID);
        }

        public static void CreateOrUpdate(int businessUnitID, int inventoryId, 
                                            DateTime releaseDate, string approverLogin, string merchandisingDescription,
                                            string footer, string equipmentList, decimal listPrice, decimal? specialPrice, DateTime? expirationDate,
                                            string exteriorColor, string interiorColor, string interiorType,
                                            string mappedOptionIds, string optionCodes, string highlightCalloutText, string seoKeywords, bool autoRegenerate, int msrp, int? manufacturerRebate, int? dealerDiscount)
        {
            Log.DebugFormat("Saving posting schedule for BusinessUnitID:{0}, InventoryId:{1}.", businessUnitID, inventoryId);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "postings.schedule#Create";
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ReleaseTime", releaseDate, DbType.DateTime);
                    Database.AddRequiredParameter(cmd, "ApproverLogin", approverLogin, DbType.String);
                    Database.AddRequiredParameter(cmd, "MerchandisingDescription", merchandisingDescription, DbType.String);
                    Database.AddRequiredParameter(cmd, "Footer", footer, DbType.String);
                    Database.AddRequiredParameter(cmd, "EquipmentList", equipmentList, DbType.String);
                    Database.AddRequiredParameter(cmd, "ListPrice", listPrice, DbType.Decimal);
                    Database.AddRequiredParameter(cmd, "SpecialPrice", specialPrice, DbType.Decimal);
                    Database.AddRequiredParameter(cmd, "AutoReGenerate", autoRegenerate, DbType.Boolean);

                    if (expirationDate.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "ExpiresOn", expirationDate, DbType.DateTime);
                    }
                    Database.AddRequiredParameter(cmd, "ExteriorColor", exteriorColor, DbType.String);
                    Database.AddRequiredParameter(cmd, "InteriorColor", interiorColor, DbType.String);
                    Database.AddRequiredParameter(cmd, "InteriorType", interiorType, DbType.String);
                    Database.AddRequiredParameter(cmd, "MappedOptionIds", mappedOptionIds, DbType.String);
                    Database.AddRequiredParameter(cmd, "OptionCodes", optionCodes, DbType.String);
                    Database.AddRequiredParameter(cmd, "HighlightCalloutText", highlightCalloutText, DbType.String);
                    Database.AddRequiredParameter(cmd, "SeoKeywords", seoKeywords, DbType.String);

                    // sometimes we don't have an msrp
                    if(msrp > 0)
                        cmd.AddRequiredParameter("MSRP", msrp, DbType.Int32);

                    if (manufacturerRebate.HasValue)
                    {
                        cmd.AddRequiredParameter("ManufacturerRebate", manufacturerRebate.Value, DbType.Int32);
                    }
                    if (dealerDiscount.HasValue)
                    {
                        cmd.AddRequiredParameter("DealerDiscount", dealerDiscount, DbType.Int32);
                    }

                    cmd.ExecuteNonQuery();

                }
            }
        }

        private static void UpdateAdvertisementStatus(int businessUnitID, int inventoryId, AdvertisementPostingStatus postingStatus)
        {
            Log.DebugFormat("Setting posting status to {0} for BusinessUnitId:{1} and InventoryId:{2}", postingStatus, businessUnitID, inventoryId);

            //should we restrict when releaseTime IS NULL (e.g. when the posting is actually a manual override)
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Postings.Status#Update";
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ReleaseStatus", (int) postingStatus, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        
        public static void CancelPostingAndReturnToPendingStatus(int businessUnitID, int inventoryId, string memberLogin)
        {
            StepStatusCollection
                .Fetch(businessUnitID, inventoryId)
                .SetPostingStatus(AdvertisementPostingStatus.PendingApproval)
                .Save(memberLogin);
            
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Postings.Cancel";
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void MarkAsPosted()
        {
            UpdateAdvertisementStatus(BusinessUnitID, InventoryId, AdvertisementPostingStatus.NoActionRequired);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="busUnitID">Business Unit ID</param>
        /// <param name="checkedIDs">input is a dictionary w/ key = destinationID, value is a dictionary with stockNumber and whether dest is enabled</param>
        /// <param name="releaseTime"></param>
        /// <param name="approverLogin">member id of user who approved the entries</param>
        /// <param name="approvalTime">Time of approval</param>
        /// <returns></returns>
        public static List<PostingSchedule> ConvertToPostingList(int busUnitID,
                                                                 Dictionary<int, Dictionary<int, bool>> checkedIDs,
                                                                 DateTime releaseTime, string approverLogin,
                                                                 DateTime approvalTime)
        {
            List<PostingSchedule> retList = new List<PostingSchedule>();

            Dictionary<int, List<int>> invIdListDestinations = new Dictionary<int, List<int>>();
            foreach (KeyValuePair<int, Dictionary<int, bool>> kvp in checkedIDs)
            {
                foreach (KeyValuePair<int, bool> kvp2 in kvp.Value)
                {
                    //value determines whether the destination will be used for this vehicle
                    if (kvp2.Value)
                    {
                        int invId = kvp2.Key;
                        if (!invIdListDestinations.ContainsKey(invId))
                        {
                            invIdListDestinations.Add(invId, new List<int>());
                        }
                        invIdListDestinations[invId].Add(kvp.Key); //add the destination
                    }
                }
            }

            //now we have a dictionary of stocknumbers that describe what destinations should be used...create
            //the postingSchedules now
            foreach (KeyValuePair<int, List<int>> kvp in invIdListDestinations)
            {
                retList.Add(new PostingSchedule(busUnitID, kvp.Key, kvp.Value, releaseTime, approverLogin, approvalTime, AdvertisementStatus.ApprovedPendingRelease));
            }
            return retList;
        }
    }
}