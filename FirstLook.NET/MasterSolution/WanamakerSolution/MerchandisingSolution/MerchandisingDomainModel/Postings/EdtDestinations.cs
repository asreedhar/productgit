using System.ComponentModel;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public enum EdtDestinations
    {
        [Description("Other")]
        Undefined = 0,
        [Description("Cars.com")]
        CarsDotCom = 1,
        [Description("AutoTrader")]
        AutoTrader = 2,
        [Description("Dealer Specialties")]
        DealerSpecialties = 3,
        [Description("Dealer.com")]
        DealerDotCom = 4,
        [Description("eBizAutos")]
        eBizAutos = 5,
        [Description("Auto Uplink USA")]
        autoUplinkUSA = 7,
        [Description("CDMData")]
        CDMData = 8,
        [Description("Cobalt")]
        Cobalt = 9,
        [Description("ClickMotive")]
        ClickMotive = 10,
        [Description("drivedominion")]
        DriveDomain = 11,
        [Description("SOKAL")]
        Sokal = 12,
        [Description("Stpventures")]
        StpVentures = 13,
        [Description("VinSolutions")]
        VinSolutions = 14,
        [Description("worlddealer.net")]
        WorldDealerDotNet = 15,
        [Description("Dealer Car Search")]
        DealerCarSearch = 16,
        [Description("Dealer eProcess")]
        DealerEProcess = 17,
        [Description("Dealer Fire")]
        DealerFire = 18,
        [Description("DealerOn")]
        DealerOn,
        [Description("DealerPeak")]
        DealerPeak,
        [Description("iPublishers")]
        IPublishers
    }

    public static class EdtDestinationHelper
    {
        public static IEnumerable<EdtDestinations> GetGoogleVdpSites()
        {
            var cache = Registry.Resolve<ICache>();
            List<EdtDestinations> googleableSites = (List<EdtDestinations>)cache.Get("GoogleVdpSites");
            if((googleableSites == null) || (googleableSites.Count == 0))
            {
                googleableSites = new List<EdtDestinations>();
                using( var conn = Database.GetConnection(Database.MerchandisingDatabase))
                {
                    conn.Open();
                    using(var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "[settings].[EdtDestinations#FetchGoogleVdpWebsites]";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        using(var reader = cmd.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                googleableSites.Add((EdtDestinations)reader.GetInt32(reader.GetOrdinal("destinationID")));
                            }
                        }
                    }
                }
            }

            // Cache for 15 minutes.
            if(googleableSites.Count > 0)
                cache.Set("GoogleVdpSites", googleableSites, 900);

            return googleableSites;
        }

        public static IEnumerable<EdtDestinations> GetAvailableWebsiteDestinationIds()
        {
            List<Postings.EdtDestinations> destinationList = new List<Postings.EdtDestinations>();

            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "[settings].[EdtDestinations#FetchDealerWebsites]";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int destId = reader.GetInt32(reader.GetOrdinal("destinationId"));
                            destinationList.Add((Postings.EdtDestinations)destId);
                        }
                    }
                }
            }

            return destinationList;
        }
    }
}
