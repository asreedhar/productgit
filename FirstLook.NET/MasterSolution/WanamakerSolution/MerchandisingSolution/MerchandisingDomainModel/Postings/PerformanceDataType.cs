namespace FirstLook.Merchandising.DomainModel.Postings
{
    public enum PerformanceDataType
    {
        Undefined = 0,
        SearchCount = 1,
        ClickthruRatio = 2,
        ActivityCount = 3,
        ActivityRatio = 4,
        DetailViews = 5

    }
    
}
