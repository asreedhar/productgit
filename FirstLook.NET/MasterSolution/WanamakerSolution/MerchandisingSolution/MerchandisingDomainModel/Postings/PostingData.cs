using FirstLook.Merchandising.DomainModel.Pricing;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class PostingData
    {
        public PostingData()
        {
            ThumbnailUrl = string.Empty;
            SellerDescription = new OnlineDescription();
            OptionsList = string.Empty;
        }

        
        public PostingData(string thumbnailUrl, OnlineDescription sellerDescription, string optionsList)
        {
            ThumbnailUrl = thumbnailUrl;
            SellerDescription = sellerDescription;
            OptionsList = optionsList;
        }

        public string ThumbnailUrl { get; set; }

        public OnlineDescription SellerDescription { get; set; }

        public string OptionsList { get; set; }
    }
}
