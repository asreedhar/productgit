using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Release;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class Advertisement
    {
        //private readonly List<Option> options;
        private readonly List<string> photoUrls;
        private readonly decimal price;
        private readonly int inventoryId;
        private readonly DateTime? expiresOn;
        private readonly string approvedByMemberLogin;
        private string exteriorColor;
        private string interiorColor;
        private string interiorType;
        private DateTime releaseDate;
        private DateTime approvalTime;
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public DateTime ApprovalTime
        {
            get { return approvalTime; }
            set { approvalTime = value; }
        }

        private AdvertisementStatus releaseStatus;

        public AdvertisementStatus ReleaseStatus
        {
            get { return releaseStatus; }
            set { releaseStatus = value; }
        }

        public DateTime ReleaseDate
        {
            get { return releaseDate; }
            set { releaseDate = value; }
        }

        public string ExteriorColor
        {
            get { return exteriorColor; }
            set { exteriorColor = value; }
        }

        public string InteriorColor
        {
            get { return interiorColor; }
            set { interiorColor = value; }
        }

        public string InteriorType
        {
            get { return interiorType; }
            set { interiorType = value; }
        }

        public string MappedOptionIds
        {
            get { return mappedOptionIds; }
            set { mappedOptionIds = value; }
        }

        private string mappedOptionIds;
        
        public string ApprovedByMemberLogin
        {
            get { return approvedByMemberLogin; }
        }

        public DateTime? ExpiresOn
        {
            get { return expiresOn; }
        }

        public int InventoryId
        {
            get { return inventoryId; }
        }

        private readonly string text;
        private readonly string footer;

        public string Footer
        {
            get { return footer; }
        }

        private readonly string equipmentList;

        public string EquipmentList
        {
            get { return equipmentList; }
        }

        internal Advertisement(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("schedulingId"));
            price = reader.GetDecimal(reader.GetOrdinal("listPrice"));
            text = reader.GetString(reader.GetOrdinal("merchandisingDescription"));
            footer = reader.GetString(reader.GetOrdinal("footer"));
            equipmentList = reader.GetString(reader.GetOrdinal("equipmentList"));
            //videoUrl = reader.GetString(reader.GetOrdinal("videoUrl"));
            //stockNumber = reader.GetString(reader.GetOrdinal("stockNumber"));
            inventoryId = reader.GetInt32(reader.GetOrdinal("inventoryId"));
            //vin = reader.GetString(reader.GetOrdinal("vin"));
            releaseDate = reader.GetDateTime(reader.GetOrdinal("scheduledReleaseTime"));
            approvedByMemberLogin = reader.GetString(reader.GetOrdinal("approvedByMemberLogin"));
            approvalTime = reader.GetDateTime(reader.GetOrdinal("approvedTimeStamp"));
            
            object val = reader.GetValue(reader.GetOrdinal("expiresOn"));
            if (val != DBNull.Value)
                expiresOn = (DateTime) val;

            photoUrls = new List<string>();
            //options = new List<Option>();

            exteriorColor = Database.GetNullableString(reader, "exteriorColor");
            interiorColor = Database.GetNullableString(reader, "interiorColor");
            interiorType = Database.GetNullableString(reader, "interiorType");
            mappedOptionIds = Database.GetNullableString(reader, "mappedOptionIds");
            releaseStatus = (AdvertisementStatus)reader.GetInt32(reader.GetOrdinal("advertisementStatus"));
        }

        public string Text
        {
            get { return text; }
        }
        public int PhotoCount
        {
            get
            {
                return photoUrls.Count;
            }
        }
        public IEnumerable<string> PhotoUrls
        {
            get { return photoUrls; }
        }

        public decimal Price
        {
            get { return price; }
        }

        /*public void AddOption(Option option)
        {
            options.Add(option);
        }*/

        public void AddPhoto(string photoUrl)
        {
            photoUrls.Add(photoUrl);
        }

        public static void ReturnPostingToApproved(int advertisementId, AdvertisementStatus status)
        {
            
            //log the posting error
            //do NOT delete the posting...
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "release.Advertisement#UpdateStatus";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "AdvertisementId", advertisementId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "AdvertisementStatus", (int)status, DbType.Int32);

                    //set this ad status to error
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static IEnumerable<Advertisement> Fetch(int businessUnitID)
        {
            //Get all advertisements for the dealer to a given destination
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                List<Advertisement> adsList = new List<Advertisement>();
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "postings.scheduledAds#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", null, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            adsList.Add(new Advertisement(reader));
                        }


                        return adsList;
                    }
                }
            }
        }
    }
}