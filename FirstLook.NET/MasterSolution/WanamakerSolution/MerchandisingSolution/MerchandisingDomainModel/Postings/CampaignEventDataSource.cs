using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public static class CampaignEventDataSource
    {
        public static List<CampaignEvent> FetchList(int businessUnitId)
        {
            return CampaignEvent.FetchList(businessUnitId);
        }
        public static void Delete(int businessUnitId, int campaignEventId)
        {
            CampaignEvent.Delete(businessUnitId, campaignEventId);
        }
// ReSharper disable UnusedMember.Global
        // used by CampaignSettings.ascx
        public static void Delete(CampaignEvent campaignEvent)
// ReSharper restore UnusedMember.Global
        {
            campaignEvent.Delete();            
        }

        public static void Upsert(CampaignEvent campaignEvent)
        {
            campaignEvent.Upsert();
        }



        
    }
}
