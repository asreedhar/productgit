namespace FirstLook.Merchandising.DomainModel.Postings
{
    public enum CampaignRepriceType
    {
        NoReprice = 0,
        PercentageOfCurrentPrice = 1,
        PercentageOfMarketAverage = 2
    }
}
