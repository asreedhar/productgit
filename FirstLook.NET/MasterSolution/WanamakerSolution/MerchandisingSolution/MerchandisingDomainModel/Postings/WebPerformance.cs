using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class WebPerformance
    {
        public WebPerformance()
        {
            MarketAveragePrice = -1;
            CurrentPrice = -1;
            AdPrints = -1;
            MapPrints = -1;
            WebsiteViews = -1;
            Emails = -1;
            DetailViews = -1;
            Searches = -1;
        }

        public WebPerformance(int searches, int detailViews, int emails, int websiteViews, int mapPrints, int adPrints, int currentPrice, int marketAveragePrice)
        {
            Searches = searches;
            DetailViews = detailViews;
            Emails = emails;
            WebsiteViews = websiteViews;
            MapPrints = mapPrints;
            AdPrints = adPrints;
            CurrentPrice = currentPrice;
            MarketAveragePrice = marketAveragePrice;
        }

        internal WebPerformance(IDataRecord reader) {
            Searches = reader.GetInt32(reader.GetOrdinal("searchViewed"));
            DetailViews = reader.GetInt32(reader.GetOrdinal("detailsViewed"));
            Emails = reader.GetInt32(reader.GetOrdinal("emails"));
            WebsiteViews = reader.GetInt32(reader.GetOrdinal("websiteClickthrus"));
            AdPrints = reader.GetInt32(reader.GetOrdinal("adsPrinted"));
            MapPrints = reader.GetInt32(reader.GetOrdinal("mapsViewed"));
            CurrentPrice = reader.GetInt32(reader.GetOrdinal("currentPrice"));
            MarketAveragePrice = reader.GetInt32(reader.GetOrdinal("mktAvgPrice"));
                
        }

        public double GetData(PerformanceDataType dataType)
        {
            switch (dataType)
            {
                case PerformanceDataType.ActivityCount:
                    return ActivityCount;
                case PerformanceDataType.SearchCount:
                    return Searches;
                case PerformanceDataType.ClickthruRatio:
                    if (DetailViews == -1 && Searches == -1)
                    {
                        return 0;
                    }
                    return 100*((double) DetailViews)/Searches;
                case PerformanceDataType.ActivityRatio:
                    if (ActivityCount == -1 && DetailViews == -1)
                    {
                        return -1;
                    }
                    return 100*((double)ActivityCount) / DetailViews;
                case PerformanceDataType.DetailViews:
                    return DetailViews;
                default:
                    throw new ApplicationException("Not a known performance data type.");
            }
        }
        public static WebPerformance operator + (WebPerformance a, WebPerformance b)
        {
            return new WebPerformance(a.Searches + b.Searches,
                               a.DetailViews + b.DetailViews,
                               a.Emails + b.Emails,
                               a.WebsiteViews + b.WebsiteViews,
                               a.MapPrints + b.MapPrints,
                               a.AdPrints + b.AdPrints,
                               a.CurrentPrice + b.CurrentPrice,
                               a.MarketAveragePrice + b.MarketAveragePrice);
        }
        public static WebPerformance operator - (WebPerformance a, WebPerformance b)
        {
            return new WebPerformance(a.Searches - b.Searches,
                               a.DetailViews - b.DetailViews,
                               a.Emails - b.Emails,
                               a.WebsiteViews - b.WebsiteViews,
                               a.MapPrints - b.MapPrints,
                               a.AdPrints - b.AdPrints,
                               a.CurrentPrice - b.CurrentPrice,
                               a.MarketAveragePrice - b.MarketAveragePrice);
        }
        public static WebPerformance operator / (WebPerformance a, int divisor)
        {
            return new WebPerformance(a.Searches/divisor,
                a.DetailViews/divisor,
                a.Emails/divisor,
                a.WebsiteViews/divisor,
                a.MapPrints/divisor,
                a.AdPrints/divisor,
                a.CurrentPrice/divisor,
                a.MarketAveragePrice/divisor);
        }

        public int ActivityCount
        {
            get { return Emails + WebsiteViews + AdPrints + MapPrints; }
        }

        public int Searches { get; set; }

        public int DetailViews { get; set; }

        public int Emails { get; set; }

        public int WebsiteViews { get; set; }

        public int MapPrints { get; set; }

        public int AdPrints { get; set; }

        public int CurrentPrice { get; set; }

        public int MarketAveragePrice { get; set; }

        public static DataSet FetchWebReport(int businessUnitId, DateTime startDate, DateTime endDate)
        {
            return FetchWebReport(businessUnitId, startDate, endDate);
        }

        public static DataSet FetchWebReport(int businessUnitId, DateTime startDate, DateTime endDate, int sourceId)
        {
            
        
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "postings.WebMetricReport#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "StartDate", startDate, DbType.DateTime);
                    Database.AddRequiredParameter(cmd, "EndDate", endDate, DbType.DateTime);
                    Database.AddRequiredParameter(cmd, "MetricSourceId", sourceId, DbType.Int32);

                    using(SqlDataAdapter sda = new SqlDataAdapter((SqlCommand)cmd))
                    {
                        DataSet ds = new DataSet();
                        sda.Fill(ds);
                        ds.Tables[0].TableName = "Make";
                        ds.Tables[1].TableName = "Model";
                        ds.Tables[2].TableName = "Year";
                        return ds;
                    }
                    
                }
            }
    
        }
        public static List<string> FetchWebReportFilters(int businessUnitId, DateTime startDate, DateTime endDate, int groupNumber)
        {
            return FetchWebReportFilters(businessUnitId, startDate, endDate, 0);
        }

        public static List<string> FetchWebReportFilters(int businessUnitId, DateTime startDate, DateTime endDate, int groupNumber, int sourceId)
        {


            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "postings.WebMetricReportFilters#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "StartDate", startDate, DbType.DateTime);
                    Database.AddRequiredParameter(cmd, "EndDate", endDate, DbType.DateTime);
                    

                    using (SqlDataAdapter sda = new SqlDataAdapter((SqlCommand)cmd))
                    {
                        DataSet ds = new DataSet();
                        sda.Fill(ds);

                        List<string> retList = new List<string>();
                        foreach (DataRow dr in ds.Tables[groupNumber].Rows)
                        {
                            retList.Add(dr[0].ToString());
                        }
                        return retList;
                        
                    }

                }
            }

        }

    }
}
