using System.Data;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class VehicleDestinationSetting : DestinationSetting
    {
        public int InventoryId { get; set; }


        public VehicleDestinationSetting(IDataRecord reader) : base(reader)
        {
            InventoryId = reader.GetInt32(reader.GetOrdinal("inventoryId"));
            
        }

        public VehicleDestinationSetting(IDataRecord reader, int inventoryId)
            : base(reader)
        {
            InventoryId = inventoryId;
        }

        
        public VehicleDestinationSetting(bool isActive, int destinationId, string description, int inventoryId) : base(isActive, destinationId, description)
        {
            InventoryId = inventoryId;
        }
    }
}
