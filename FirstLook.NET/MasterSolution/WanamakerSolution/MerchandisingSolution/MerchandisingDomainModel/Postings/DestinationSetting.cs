using System.Data;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class DestinationSetting
    {
        public bool IsActive { get; set; }

        public int DestinationId { get; set; }

        public string Description { get; set; }

        public DestinationSetting(bool isActive, int destinationId, string description)
        {
            IsActive = isActive;
            DestinationId = destinationId;
            Description = description;
        }

        public DestinationSetting(IDataRecord reader)
        {
            IsActive = reader.GetBoolean(reader.GetOrdinal("activeFlag"));
            DestinationId = reader.GetInt32(reader.GetOrdinal("destinationId"));
            Description= reader.GetString(reader.GetOrdinal("description"));
        }
    }
}
