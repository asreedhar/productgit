using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class DealerContact
    {
        public DealerContact()
        {
            LeadEmailAddress = string.Empty;
            LeadPhoneNumber = string.Empty;
            LeadManagerFirstName= string.Empty;
            LeadManagerLastName = string.Empty;
        }

        public string LeadEmailAddress { get; set; }

        public string LeadPhoneNumber { get; set; }

        public string LeadManagerFirstName { get; set; }

        public string LeadManagerLastName { get; set; }

        public static DealerContact Fetch(int businessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "select * from settings.dealerContact where businessUnitId = @BusinessUnitId";
                    cmd.CommandType = CommandType.Text;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            DealerContact dc = new DealerContact();
                            dc.LeadEmailAddress = reader.GetString(reader.GetOrdinal("leadEmailAddress"));
                            dc.LeadPhoneNumber = reader.GetString(reader.GetOrdinal("leadPhoneNumber"));
                            dc.LeadManagerFirstName = reader.GetString(reader.GetOrdinal("leadManagerFirstName"));
                            dc.LeadManagerLastName = reader.GetString(reader.GetOrdinal("leadManagerLastName"));
                            return dc;
                        }
                        
                        return new DealerContact();                            
                    }

                }
            }
        }

        public static void UpdateOrCreate(int businessUnitId, string leadEmailAddress, string leadPhoneNumber, string leadManagerFirstName, string leadManagerLastName)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                        IF EXISTS (SELECT 1 FROM settings.dealerContact WHERE businessUnitId = @BusinessUnitID)
                        BEGIN
                            UPDATE settings.dealerContact
                            SET leadManagerFirstName = @leadManagerFirstName,
                            leadManagerLastName = @leadManagerLastName,
                            leadPhoneNumber = @leadPhoneNumber,
                            leadEmailAddress = @leadEmailAddress
                            WHERE
                            businessUnitId = @BusinessUnitId
                        END                        
                        ELSE
                        BEGIN
                            INSERT INTO settings.dealerContact
                            (businessUnitId, leadManagerFirstName, leadManagerLastName, leadPhoneNumber, leadEmailAddress)
                            VALUES (@BusinessUnitId, @leadManagerFirstName, @leadManagerLastName, @leadPhoneNumber, @leadEmailAddress)    
                        END
                    ";
                    cmd.CommandType = CommandType.Text;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "leadManagerFirstName", leadManagerFirstName, DbType.String);
                    Database.AddRequiredParameter(cmd, "leadManagerLastName", leadManagerLastName, DbType.String);
                    Database.AddRequiredParameter(cmd, "leadPhoneNumber", leadPhoneNumber, DbType.String);
                    Database.AddRequiredParameter(cmd, "leadEmailAddress", leadEmailAddress, DbType.String);
                    cmd.ExecuteNonQuery();

                }
            }
        }
    }
}
