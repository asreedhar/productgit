using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Postings
{
    public class CampaignEvent
    {
        private const int defaultAgeInDaysForAction = 30;

        private readonly int businessUnitId;
        private int campaignEventId;
        private bool regenerateDescription;
        private int? repricePercentage;
        private CampaignRepriceType repriceType;
        private int? templateId;
        private string templateName;
        private int? themeId;

        private string themeName;
        private int vehicleAgeInDaysForAction;

        internal CampaignEvent(IDataRecord record)
        {
            campaignEventId = record.GetInt32(record.GetOrdinal("campaignEventId"));
            businessUnitId = record.GetInt32(record.GetOrdinal("businessUnitId"));
            vehicleAgeInDaysForAction = record.GetInt32(record.GetOrdinal("vehicleAgeInDaysForAction"));
            templateId = Database.GetNullableInt(record, "templateId");
            themeId = Database.GetNullableInt(record, "themeId");
            regenerateDescription = record.GetBoolean(record.GetOrdinal("regenerateDescription"));
            repriceType = (CampaignRepriceType) record.GetInt32(record.GetOrdinal("repriceTypeId"));
            repricePercentage = Database.GetNullableInt(record, "repricePercentage");
            themeName = Database.GetNullableString(record, "themeName");
            templateName = Database.GetNullableString(record, "templateName");
        }

        public CampaignEvent()
        {
        }

        public CampaignEvent(int businessUnitId)
            : this(
                businessUnitId, defaultAgeInDaysForAction, null, string.Empty, null, string.Empty, false,
                CampaignRepriceType.NoReprice, null)
        {
        }

        public CampaignEvent(int businessUnitId, int vehicleAgeInDaysForAction, int? templateId, string templateName,
                             int? themeId, string themeName, bool regenerateDescription, CampaignRepriceType repriceType,
                             int? repricePercentage)
        {
            this.businessUnitId = businessUnitId;
            this.vehicleAgeInDaysForAction = vehicleAgeInDaysForAction;
            this.templateId = templateId;
            this.templateName = templateName;
            this.themeId = themeId;
            this.themeName = themeName;
            this.regenerateDescription = regenerateDescription;
            this.repriceType = repriceType;
            this.repricePercentage = repricePercentage;
        }

        public int CampaignEventId
        {
            get { return campaignEventId; }
            set { campaignEventId = value; }
        }

        public string ThemeName
        {
            get { return themeName; }
            set { themeName = value; }
        }

        public string TemplateName
        {
            get { return templateName; }
            set { templateName = value; }
        }

        public int VehicleAgeInDaysForAction
        {
            get { return vehicleAgeInDaysForAction; }
            set { vehicleAgeInDaysForAction = value; }
        }

        public int? TemplateId
        {
            get { return templateId; }
            set { templateId = value; }
        }

        public int? ThemeId
        {
            get { return themeId; }
            set { themeId = value; }
        }

        public bool RegenerateDescription
        {
            get { return regenerateDescription; }
            set { regenerateDescription = value; }
        }

        public CampaignRepriceType RepriceType
        {
            get { return repriceType; }
            set { repriceType = value; }
        }

        public int? RepricePercentage
        {
            get { return repricePercentage; }
            set { repricePercentage = value; }
        }

        public void Upsert()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.campaignEvent#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "vehicleAgeInDaysForAction", vehicleAgeInDaysForAction, DbType.Int32);
                    if (templateId.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "templateId", templateId.Value, DbType.Int32);
                    }
                    if (themeId.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "themeId", themeId, DbType.Int32);
                    }

                    Database.AddRequiredParameter(cmd, "regenerateDescription", regenerateDescription, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "repriceTypeId", (int) repriceType, DbType.Int32);

                    if (repricePercentage.HasValue)
                    {
                        Database.AddRequiredParameter(cmd, "repricePercentage", repricePercentage.Value, DbType.Int32);
                    }

                    if (campaignEventId != 0)
                    {
                        Database.AddRequiredParameter(cmd, "campaignEventId", campaignEventId, DbType.Int32);
                    }

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Delete()
        {
            Delete(businessUnitId, campaignEventId);
        }

        public static void Delete(int businessUnitId, int campaignEventId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.campaignEvent#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "campaignEventId", campaignEventId, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static CampaignEvent Fetch(int businessUnitId, int campaignEventId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.campaignEvent#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "campaignEventId", campaignEventId, DbType.Int32);


                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new CampaignEvent(reader);
                        }

                        throw new ApplicationException("Campaign Event not found");
                    }
                }
            }
        }

        public static List<CampaignEvent> FetchList(int businessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            { 
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.campaignEventList#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<CampaignEvent> evtList = new List<CampaignEvent>();
                        while (reader.Read())
                        {
                            evtList.Add(new CampaignEvent(reader));
                        }
                        return evtList;
                    }
                }
            }
        }
    }
}