﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public class ReferralResult
    {
        private Dictionary<string, ReferralRow> _rows = new Dictionary<string, ReferralRow>();

        private Google.Apis.Analytics.v3.Data.GaData gdata;
        public ReferralRow TotalsForAllResults { get; set; }

        public Dictionary<string, ReferralRow> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }

        public ReferralResult() { }

        public ReferralResult(Google.Apis.Analytics.v3.Data.GaData data)
        {
            this.gdata = data;
            List<Google.Apis.Analytics.v3.Data.GaData.ColumnHeadersData> headers = new List<Google.Apis.Analytics.v3.Data.GaData.ColumnHeadersData>(data.ColumnHeaders);

            // Be a little defensive...
            if (this.gdata.Query.Metrics.Contains("ga:visits"))
            {
                TotalsForAllResults = ReferralRow.TotalsFromGData(headers, this.gdata.TotalsForAllResults);

                if (( TotalsForAllResults.Visits > 0) && (this.gdata.Rows != null))
                {
                    int sourceIdx = headers.FindIndex(gh => gh.Name.ToUpperInvariant().Contains("SOURCE"));
                    int mediumIdx = headers.FindIndex(gh => gh.Name.ToUpperInvariant().Contains("MEDIUM"));
                    int mobileIdx = headers.FindIndex(gh => gh.Name.ToUpperInvariant().Contains("MOBILEDEVICEINFO"));
                    int keywordIdx = headers.FindIndex(gh => gh.Name.ToUpperInvariant().Contains("KEYWORD"));
                    int socialIdx = headers.FindIndex(gh => gh.Name.ToUpperInvariant().Contains("SOCIALNETWORK"));

                    foreach (var row in this.gdata.Rows)
                    {
                        if (keywordIdx > -1)
                            Rows.Add(row[keywordIdx], ReferralRow.FromGDataRow(headers, row));
                        else if (mobileIdx > -1)
                            Rows.Add(row[mobileIdx], ReferralRow.FromGDataRow(headers, row));
                        else if(sourceIdx > -1)
                        {
                            string sourceMediumKey = row[sourceIdx];
                            if(mediumIdx > -1)
                                sourceMediumKey += "|" + row[mediumIdx];

                            Rows.Add(sourceMediumKey, ReferralRow.FromGDataRow(headers, row));
                        }
                        else if(socialIdx != -1) // FB# 27478; Traffic Trends
                        {
                            Rows.Add((row[mediumIdx]+"|"+row[socialIdx]), ReferralRow.FromGDataRow(headers, row));
                        }
                    }
                }
            }
        }
    }
}
