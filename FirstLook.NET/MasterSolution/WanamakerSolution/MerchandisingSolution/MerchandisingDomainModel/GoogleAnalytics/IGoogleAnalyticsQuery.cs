﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Enumerations;
using System;
using Google.Apis.Analytics.v3.Data;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Common.Core;
namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public interface IGoogleAnalyticsQuery
    {
        void Init(int businessUnitId);

        GaData DoQuery(GoogleAnalyticsProfile profile, Internal.GoogleQueryParameters queryParameters, DateRange dateRange, int limit);
        GoogleAnalyticsResult DoAllQueries(GoogleAnalyticsProfile profile, DateRange dateRange, int limit = 0);
        Dictionary<DateTime, VDPResult> GetVdpTrend(GoogleAnalyticsProfile profile, DateRange dateRange, int limit = 50);

        ProfileResult[] GetProfiles();
    }
}
