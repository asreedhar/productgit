﻿
using FirstLook.Merchandising.DomainModel.Postings;
namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public class GoogleAnalyticsProfile
    {
        public GoogleAnalyticsProfile(int businessUnitId, GoogleAnalyticsType type, int profileId, string profileName, int destination) :
            this (businessUnitId, type, profileId, profileName, (EdtDestinations) destination)
        { }

        public GoogleAnalyticsProfile(int businessUnitId, GoogleAnalyticsType type, int profileId, string profileName, EdtDestinations destination)
        {
            BusinessUnitId = businessUnitId;
            Type = type;
            ProfileId = profileId;
            ProfileName = profileName;
            SiteProvider = destination;
        }

        public int BusinessUnitId { get; private set; }
        public GoogleAnalyticsType Type { get; private set; }
        public int ProfileId { get; private set; }
        public string ProfileName { get; private set; }
        public EdtDestinations SiteProvider { get; set; }
    }
}
