﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public interface IGoogleAnalyticsWebAuthorization
    {
        bool IsAuthorized(int businessunitId);
        bool Authorize(int businessunitId);
        void SendRedirect();
    }
}
