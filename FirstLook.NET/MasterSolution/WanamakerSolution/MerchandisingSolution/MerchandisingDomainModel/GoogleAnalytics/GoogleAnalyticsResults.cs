﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Dashboard.Concrete;
using FirstLook.Merchandising.DomainModel.Reports;
using MAX.Entities.Interfaces;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public class GoogleAnalyticsResult : IBusinessUnitAggregation
    {
        private Dictionary<string, ReferralResult> _referrals = new Dictionary<string, ReferralResult>();
        private VDPResult _vdp = new VDPResult();

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [NonAggregate]
        public int BusinessUnitId { get; set; }
        public string BusinessUnit { get; set; }

        public Dictionary<string, ReferralResult> Referrals
        {
            get { return _referrals; }
            set { _referrals = value; }
        }

        public VDPResult VDP
        {
            get { return _vdp; }
            set { _vdp = value; }
        }

        public GoogleAnalyticsResult() { }

        public GoogleAnalyticsResult(GoogleAnalyticsProfile profile, DateRange dateRange)
        {
            BusinessUnitId = profile.BusinessUnitId;
            var businessunitRepositry = new DashboardBusinessUnitRepository();
            BusinessUnit = businessunitRepositry.GetBusinessUnit(BusinessUnitId).Name;

            StartDate = dateRange.StartDate;

            // Get the last second of the last day of the end date provided.
            EndDate = dateRange.EndDate;
        }
    }
}
