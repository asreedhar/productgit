﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public enum GoogleQueryType
    {
        Undefined = 0,
        Referrals = 1,
        VDPNew = 2,
        VDPUsed = 3,
        VDPBoth = 4
    }
}
