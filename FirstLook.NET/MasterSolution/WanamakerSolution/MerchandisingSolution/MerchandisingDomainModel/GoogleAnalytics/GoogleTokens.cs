﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public class GoogleTokens
    {
        public GoogleTokens(string clientId, string clientSecret, string refreshToken)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
            RefreshToken = refreshToken;
        }

        public string ClientId { get; private set; }
        public string ClientSecret { get; private set; }
        public string RefreshToken { get; private set; }
    }
}
