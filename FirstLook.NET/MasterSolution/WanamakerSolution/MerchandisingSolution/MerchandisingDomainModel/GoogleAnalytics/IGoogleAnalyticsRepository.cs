﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public interface IGoogleAnalyticsRepository
    {
        bool IsActiveRefreshToken(int businessUnitId);
        void SetRefreshTokenInactive(int businessUnitId);

        void SetTokens(int businessUnitId, GoogleTokens tokens);
        GoogleTokens FetchTokens(int businessUnitId);

        void CreateUpdateProfileAccount(GoogleAnalyticsProfile profile);
        GoogleAnalyticsProfile FetchProfile(int businessUnitId, GoogleAnalyticsType type);

        List<GoogleQuery> GetQueries(GoogleAnalyticsProfile profile, GoogleQueryType googleQueryType);

        List<GoogleAnalyticsProfile> GetAllActiveProfiles();
        void DeleteProfile(int businessUnitId, GoogleAnalyticsType type);

        void Write_AdminLock( int BU_id, bool locked );
        bool Read_AdminLock( int BU_id );

    }
}
