﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Common.Core;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public interface IGoogleAnalyticsResultCache
    {
        /** Store Level **/
        GoogleAnalyticsResult FetchRangedGoogleData(int buid, GoogleAnalyticsType profiletype, DateRange fetchMonths);
        List<GoogleAnalyticsResult> FetchRangedGoogleDataUngrouped(int buid, GoogleAnalyticsType profiletype, DateRange fetchMonths);
        GoogleAnalyticsResult GenerateRangedGoogleData(int buid, GoogleAnalyticsType profiletype, DateRange month);
        Dictionary<DateTime, VDPResult> FetchVDPTrend(int buid, DateRange dateRange);

        /** Groups **/
        IEnumerable<GoogleAnalyticsResult> FetchGroupRangedGoogleData(int groupid, GoogleAnalyticsType profiletype, DateRange dateRange);
        IEnumerable<GoogleAnalyticsResult> GenerateGroupRangedGoogleData(int groupid, GoogleAnalyticsType profiletype, DateRange dateRange);
        Dictionary<DateTime, VDPResult> FetchGroupVDPTrend(int groupid, DateRange dateRange);
    }
}
