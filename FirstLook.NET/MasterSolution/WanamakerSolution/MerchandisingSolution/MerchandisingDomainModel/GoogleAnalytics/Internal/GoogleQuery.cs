﻿
namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal
{
    public class GoogleQuery
    {
        public string QueryName { get; set; }
        public GoogleQueryType QueryType { get; set; }
        public GoogleQueryParameters Parameters { get; set; }

        public GoogleQuery(string qname, GoogleQueryType qtype, GoogleQueryParameters qparam)
        {
            QueryName = qname;
            QueryType = qtype;
            Parameters = qparam;
        }
    }
}
