﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Web;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Analytics.v3;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Util;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal
{
    internal class GoogleAnalyticsWebAuthorization : IGoogleAnalyticsWebAuthorization
    {
        private IGoogleAnalyticsRepository _repository;
        private WebServerClient _client;

        public GoogleAnalyticsWebAuthorization(IGoogleAnalyticsRepository repository)
        {
            _repository = repository;

            var authEndPoint = new Uri(GoogleAuthenticationServer.Description.AuthorizationEndpoint.AbsoluteUri + "?access_type=offline&approval_prompt=force", UriKind.Absolute);

            var authDescription = new AuthorizationServerDescription()
                {
                    AuthorizationEndpoint = authEndPoint,
                    ProtocolVersion = GoogleAuthenticationServer.Description.ProtocolVersion,
                    TokenEndpoint = GoogleAuthenticationServer.Description.TokenEndpoint
                };

            _client = new WebServerClient(authDescription);
            _client.ClientIdentifier = GoogleAnalytics.Default.ClientId;
            _client.ClientSecret = GoogleAnalytics.Default.ClientSecret;
        }

        public bool IsAuthorized(int businessunitId)
        {
            return _repository.IsActiveRefreshToken(businessunitId);
        }

        public bool Authorize(int businessunitId)
        {
            try
            {
                var state = _client.ProcessUserAuthorization(new HttpRequestInfo(HttpContext.Current.Request));
                if (state != null && !string.IsNullOrEmpty(state.RefreshToken))
                {
                    _repository.SetTokens(businessunitId, new GoogleTokens(_client.ClientIdentifier, _client.ClientSecret, state.RefreshToken));
                    return true;
                }

                return false;
            }
            catch (ProtocolException)
            {
                return false;
            }
        }

        public void SendRedirect()
        {
            string scope = AnalyticsService.Scopes.AnalyticsReadonly.GetStringValue();
            OutgoingWebResponse response = _client.PrepareRequestUserAuthorization(new[] { scope });
            response.Send();
        }
    }
}
