﻿using System;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal
{
    [Serializable]
    public class GoogleQueryParameters
    {
        public string Dimensions { get; set; }
        public string Metrics { get; set; }
        public string Sort { get; set; }
        public string Segment { get; set; }
        public int Limit { get; set; }
        public string Filter { get; set; }
    }
}
