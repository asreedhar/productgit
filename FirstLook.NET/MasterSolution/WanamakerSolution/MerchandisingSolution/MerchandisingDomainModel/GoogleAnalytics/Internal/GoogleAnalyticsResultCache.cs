﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Merchandising.DomainModel.Commands;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal
{
    internal class GoogleAnalyticsResultCache : IGoogleAnalyticsResultCache
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<GoogleAnalyticsResultCache>();

        private IGoogleAnalyticsQuery _query;
        private IGoogleAnalyticsRepository _repo;
        private IGoogleAnalyticsWebAuthorization _webAuth;
        private IFileStoreFactory _factory;
        private IFileStorage _fileStore;
        private IDashboardBusinessUnitRepository _dealerRespository;

        private const string FOLDERNAME = "ExecutiveLevelCache";
        private const string GROUPFOLDERNAME = "GroupLevelCache";
        private const string GOOGLEDATA = "GoogleData";
        private const string VDPTRENDS = "VDPTrends";
        private const string ANALYTICS = "Analytics";

        public GoogleAnalyticsResultCache(IGoogleAnalyticsQuery query, IGoogleAnalyticsRepository repository, IGoogleAnalyticsWebAuthorization webAuth, IFileStoreFactory fileStoreFactory, IDashboardBusinessUnitRepository dealerRepository)
        {
            _query = query;
            _repo = repository;
            _webAuth = webAuth;
            _factory = fileStoreFactory;
            _fileStore = _factory.CreateFileStore();
            _dealerRespository = dealerRepository;
        }


        #region BusinessUnit Methods

        /// <summary>
        /// Get an aggregated GoogleAnalyticsResult for a business unit, across a given date range
        /// </summary>
        /// <param name="buid"></param>
        /// <param name="profiletype"></param>
        /// <param name="fetchMonths"></param>
        /// <returns></returns>
        public GoogleAnalyticsResult FetchRangedGoogleData(int buid, GoogleAnalyticsType profiletype, DateRange fetchMonths)
        {
            GoogleAnalyticsResult targetResult = null;
            List<GoogleAnalyticsResult> AggregateResults = new List<GoogleAnalyticsResult>();

            GoogleAnalyticsProfile profile;

            List<Task<GoogleAnalyticsResult>> MonthDataTasks = new List<Task<GoogleAnalyticsResult>>();
            List<DateRange> missingMonths = new List<DateRange>();

            if (Log.IsDebugEnabled)
                Log.DebugFormat("FetchRangedGoogleData: {0}, {1}, {2}", buid, profiletype, fetchMonths);

            AggregateResults = FetchRangedGoogleDataUngrouped(buid, profiletype, fetchMonths);

            if (Log.IsDebugEnabled)
                Log.Debug("Fetch collection complete. Aggregating.");

            
            if(AggregateResults.Count > 0)
                targetResult = SumAggregator.Summation<GoogleAnalyticsResult>(AggregateResults);

            if (Log.IsDebugEnabled)
                Log.DebugFormat("EndFetchRangedGoogleData: {0}, {1}, {2}", buid, profiletype, fetchMonths);

            return targetResult;
        }
        
        /// <summary>
        /// Get a NON-aggregated GoogleAnalyticsResult for a business unit, across a given date range
        /// </summary>
        /// <param name="buid"></param>
        /// <param name="profiletype"></param>
        /// <param name="fetchMonths"></param>
        /// <returns></returns>
        public List<GoogleAnalyticsResult> FetchRangedGoogleDataUngrouped(int buid, GoogleAnalyticsType profiletype, DateRange fetchMonths)
        {
            List<GoogleAnalyticsResult> results = new List<GoogleAnalyticsResult>();

            GoogleAnalyticsProfile profile;

            List<Task<GoogleAnalyticsResult>> MonthDataTasks = new List<Task<GoogleAnalyticsResult>>();
            List<DateRange> missingMonths = new List<DateRange>();

            if (Log.IsDebugEnabled)
                Log.DebugFormat("FetchRangedGoogleData: {0}, {1}, {2}", buid, profiletype, fetchMonths);

            if (TryGetProfile(buid, profiletype, out profile))
            {
                fetchMonths.AsMonths().ToList().ForEach(month =>
                {
                    MonthDataTasks.Add(GetMonthDataAsync(buid, profile, month));
                });

                // Spin through tasks to read from S3
                Task.WaitAll( MonthDataTasks.ToArray());
                for (int x = 0; x < MonthDataTasks.Count; x++)
                {
                    Task<GoogleAnalyticsResult> task = MonthDataTasks[x];
                    if (task.IsCompleted)
                    {
                        if (task.Result.Referrals.Count > 0)
                        {
                            results.Add(task.Result);
                        }
                        else
                        {
                            // if it failed add it to a list so we can call GA consecutively in order to avoid their rate limiter.
                            missingMonths.Add(new DateRange(task.Result.StartDate, task.Result.EndDate));
                        }
                    }
                }

                if (missingMonths.Count > 0)
                {
                    missingMonths.ForEach(missingMonth =>
                    {
                        GoogleAnalyticsResult mr = GenerateRangedGoogleData(buid, profiletype, missingMonth);
                        if(mr.Referrals.Count > 0)
                            results.Add(mr);
                    });
                }
            }

            if (Log.IsDebugEnabled)
                Log.DebugFormat("EndFetchRangedGoogleData: {0}, {1}, {2}", buid, profiletype, fetchMonths);

            return results;
        }

        /// <summary>
        /// This will always get fresh data from google and create the Aggregated file in S3.
        /// Make sure you only call this method with the date ranges you want combined.
        /// </summary>
        /// <param name="buid"></param>
        /// <param name="profiletype"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public GoogleAnalyticsResult GenerateRangedGoogleData(int buid, GoogleAnalyticsType profiletype, DateRange month)
        {
            GoogleAnalyticsResult analyticsResult = null;
            GoogleAnalyticsProfile profile;

            if (Log.IsDebugEnabled)
                Log.DebugFormat("GenerateRangedGoogleData: {0}, {1}, {2}", buid, profiletype, month);

            if (TryGetProfile(buid, profiletype, out profile))
            {
                analyticsResult = _query.DoAllQueries(profile, month);
            }

            if (analyticsResult != null)
            {
                byte[] bytes = Encoding.ASCII.GetBytes(analyticsResult.ToJson());
                MemoryStream stream = new MemoryStream(bytes);
                _fileStore.Write(CreateDateRangeExecutiveGoogleDataKey(profile, month), stream);
            }

            if (Log.IsDebugEnabled)
                Log.DebugFormat("End GenerateRangedGoogleData: {0}, {1}, {2}", buid, profiletype, month);

            return analyticsResult;
        }

        public Dictionary<DateTime, VDPResult> FetchVDPTrend(int buid, DateRange dateRange)
        {
            Dictionary<DateTime, VDPResult> vdpResults = new Dictionary<DateTime, VDPResult>();

            GetVDPByTypeAndMonth(buid, dateRange).AsParallel().ForAll( vdpTask => {
                if (vdpTask.IsFaulted)
                    Log.ErrorFormat("FetchVDPTrend Fail: {0}, {1}", buid, dateRange);
                else if(vdpTask.Result != null)
                {
                    VDPResult resultByMonth = new VDPResult();
                    if (vdpResults.TryGetValue(vdpTask.Result.StartDate, out resultByMonth))
                        vdpResults[vdpTask.Result.StartDate] = resultByMonth + vdpTask.Result.VDP;
                    else
                    {
                        vdpResults.Add(vdpTask.Result.StartDate, vdpTask.Result.VDP);
                    }
                }
            });

            return vdpResults;
        }
        

        #region BusinessUnit Private Methods
        /// <summary>
        /// Task wrapper to speed up multiple reads.
        /// </summary>
        /// <param name="buid"></param>
        /// <param name="profiletype"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private Task<GoogleAnalyticsResult> GetMonthDataAsync(int buid, GoogleAnalyticsProfile profile, DateRange month)
        {
            return Task.Factory.StartNew(() => GetCachedAnalyticsResult(buid, profile, month));
        }

        /// <summary>
        /// Get a GoogleAnalyticsResult for one month.
        /// </summary>
        /// <param name="buid"></param>
        /// <param name="profiletype"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private GoogleAnalyticsResult GetCachedAnalyticsResult(int buid, GoogleAnalyticsProfile profile, DateRange month)
        {
            GoogleAnalyticsResult analyticsResult = new GoogleAnalyticsResult(profile, month);

            if (Log.IsDebugEnabled)
                Log.DebugFormat("GetCachedAnalyticsResult: {0}, {1}, {2}", buid, profile.ProfileId, month);

            // check repo
            string key = CreateDateRangeExecutiveGoogleDataKey(profile, month);
            bool exists;
            string txt = null;
            DateTime modifiedTime = DateTime.MinValue;
            
            // For *this actual current month* if we have refreshed the data today, just return that.
            if(month.StartDate.Equals(DateTime.Now.FirstSecondOfMonth()))
            {
                modifiedTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            }

            using (var stream = _fileStore.ReadIfModified(key, modifiedTime, out exists))
            {
                if (exists)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        txt = reader.ReadToEnd();
                    }

                    if (!String.IsNullOrWhiteSpace(txt))
                        analyticsResult = txt.FromJson<GoogleAnalyticsResult>();
                }
                else if (Log.IsDebugEnabled)
                {
                    Log.Debug("GetCachedAnalyticsResult Cache Miss: " + key);
                }
            }

            if (Log.IsDebugEnabled)
                Log.DebugFormat("End GetCachedAnalyticsResult: {0}, {1}, {2}", buid, profile.ProfileId, month);

            return analyticsResult;
        }

        private bool TryGetProfile(int buid, GoogleAnalyticsType profiletype, out GoogleAnalyticsProfile profile)
        {
            profile = null;
            
            try
            {
                if (_webAuth.IsAuthorized(buid))
                {
                    profile = _repo.FetchProfile(buid, profiletype);
                    _query.Init(buid);
                }
            }
            catch (Exception) { }

            return profile != null;
        }

        /// <summary>
        /// Get all the available cached results of any profile type spanning a given date range
        /// </summary>
        /// <param name="buid"></param>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        private List<Task<GoogleAnalyticsResult>> GetVDPByTypeAndMonth(int buid, DateRange dateRange)
        {
            List<Task<GoogleAnalyticsResult>> typeAndMonthVdps = new List<Task<GoogleAnalyticsResult>>();
            foreach (GoogleAnalyticsType gt in EnumHelper.GetValues<GoogleAnalyticsType>().ToArray())
            {
                GoogleAnalyticsProfile profile;
                if (TryGetProfile(buid, gt, out profile))
                {
                    dateRange.AsMonths().ToList().ForEach(month =>
                    {
                        typeAndMonthVdps.Add(GetMonthDataAsync(buid, profile, month));
                    });
                }
            }
            return typeAndMonthVdps;
        }

        #endregion BusinessUnit Private Methods


        #endregion BusinessUnit Methods

        #region Group Methods

        /// <summary>
        /// Get an aggregated GoogleAnalyticsResult for a Dealership Group, across a given date range
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="profiletype"></param>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public IEnumerable<GoogleAnalyticsResult> FetchGroupRangedGoogleData(int groupid, GoogleAnalyticsType profiletype, DateRange dateRange)
        {
            Dictionary<DateRange, IEnumerable<GoogleAnalyticsResult>> groupDataByMonth = new Dictionary<DateRange, IEnumerable<GoogleAnalyticsResult>>();
            List<DateRange> missingMonths = FetchCachedMonths(groupid, profiletype, dateRange, groupDataByMonth);

            // Generate when not found.
            if (missingMonths.Count > 0)
            {
                FetchGenerateCachedMonths(groupid, profiletype, groupDataByMonth, missingMonths);
            }

            if (Log.IsDebugEnabled)
                Log.Debug("Aggregating Group Results for date range");

            List<GoogleAnalyticsResult> groupTotalForRange = AggregateGroupByMonths(dateRange, groupDataByMonth);

            if (Log.IsDebugEnabled)
                Log.Debug("Aggregating the TOTAL object for Group/DateRange");
            
            if (groupTotalForRange.Count > 0)
            {
                var sum = groupTotalForRange.Summation();
                sum.BusinessUnitId = groupid;
                sum.BusinessUnit = "TOTAL";
                sum.StartDate = dateRange.StartDate;
                sum.EndDate = dateRange.EndDate;

                groupTotalForRange.Add(sum);
            }

            if (Log.IsDebugEnabled)
                Log.DebugFormat(" End FetchGroupRangedGoogleData: {0}, {1}, {2}", groupid, profiletype, dateRange);

            return groupTotalForRange;
        }
        
        /// <summary>
        /// This will always get fresh data from google and create the Aggregated file in S3.
        /// Make sure you only call this method with the date ranges you want combined.
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="profiletype"></param>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public IEnumerable<GoogleAnalyticsResult> GenerateGroupRangedGoogleData(int groupid, GoogleAnalyticsType profiletype, DateRange dateRange)
        {
            var dealerships = _dealerRespository.GetBusinessUnitsfromGroup(groupid);
            List<GoogleAnalyticsResult> generatedResults = new List<GoogleAnalyticsResult>();

            if (Log.IsDebugEnabled)
                Log.DebugFormat("GeneratingGroupRangedGoogleData for: {0}, {1}, {2}", groupid, profiletype, dateRange);

            dealerships.ToList().ForEach(dealerShip =>
            {
                GoogleAnalyticsResult dealerAnalytics = FetchRangedGoogleData(dealerShip.BusinessUnitId, profiletype, dateRange);
                if (dealerAnalytics != null)
                    generatedResults.Add(dealerAnalytics);
            });

            if (Log.IsDebugEnabled)
                Log.DebugFormat("Results generated, creating total.");

            if (generatedResults.Count > 0)
            {
                byte[] bytes = Encoding.ASCII.GetBytes(generatedResults.ToJson());
                MemoryStream stream = new MemoryStream(bytes);
                _fileStore.Write(CreateDateRangeGroupGoogleDataKey(groupid, profiletype, dateRange), stream);
                // write it a second time
                stream = new MemoryStream(bytes);
                _fileStore.Write(CreateCurrentRangeGroupGoogleDataKey(groupid, profiletype, dateRange), stream);
            }

            if (Log.IsDebugEnabled)
                Log.DebugFormat("End GeneratingGroupRangedGoogleData: {0}, {1}, {2}", groupid, profiletype, dateRange);

            return generatedResults;
        }

        /// <summary>
        /// Get the vdps for all the businessunits in the group and return that mess!
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public Dictionary<DateTime, VDPResult> FetchGroupVDPTrend(int groupid, DateRange dateRange)
        {
            Dictionary<DateTime, VDPResult> groupVdps = new Dictionary<DateTime, VDPResult>();
            List<Task<Dictionary<DateTime, VDPResult>>> groupVdpTasks = new List<Task<Dictionary<DateTime, VDPResult>>>();
            foreach (var bu in _dealerRespository.GetBusinessUnitsfromGroup(groupid))
            {
                groupVdpTasks.Add(Task.Factory.StartNew(() => FetchVDPTrend(bu.BusinessUnitId, dateRange)));
            }

            Task.WaitAll(groupVdpTasks.ToArray());
            groupVdpTasks.ForEach(taskResult =>
            {
                if (taskResult.IsFaulted)
                {
                    Log.ErrorFormat("FetchGroupVDPTrend Fail: {0}, {1}", groupid, dateRange);
                }
                else if(taskResult.Result != null)
                {
                    VDPResult groupVdp = new VDPResult();
                    foreach (KeyValuePair<DateTime, VDPResult> businessUnitVdps in taskResult.Result)
                    {
                        if (groupVdps.TryGetValue(businessUnitVdps.Key, out groupVdp))
                            groupVdps[businessUnitVdps.Key] = groupVdp + businessUnitVdps.Value;
                        else
                            groupVdps.Add(businessUnitVdps.Key, businessUnitVdps.Value);
                    }
                }
            });

            return groupVdps;
        }

        #region Group Private Methods

        private static List<GoogleAnalyticsResult> AggregateGroupByMonths(DateRange dateRange, Dictionary<DateRange, IEnumerable<GoogleAnalyticsResult>> groupDataByMonth)
        {
            List<GoogleAnalyticsResult> groupTotalForRange = new List<GoogleAnalyticsResult>();
            // Aggregate the ByMonth data to include just this range.
            groupDataByMonth.Values.ToList().ForEach(monthList =>
            {
                monthList.ToList().ForEach(businessUnitResult =>
                {
                    var settings = GetMiscSettings.GetSettings(businessUnitResult.BusinessUnitId); // already being cached, no worries
                    if (!settings.AnalyticsSuite)
                        return; // skip this business unit entirely; it's not allowed to see this information

                    int fIdx = groupTotalForRange.FindIndex(gtfr => gtfr.BusinessUnitId == businessUnitResult.BusinessUnitId);

                    if (fIdx >= 0)
                    {
                        // This businessUnit already has data in our totals array, so update that data in place.
                        GoogleAnalyticsResult summedResult = new[] { businessUnitResult, groupTotalForRange[fIdx] }.Summation();
                        summedResult.StartDate = dateRange.StartDate;
                        summedResult.EndDate = dateRange.EndDate;
                        groupTotalForRange[fIdx] = summedResult;
                    }
                    else
                    {
                        // This businessUnit does not exist yet, add them to the totals array.
                        groupTotalForRange.Add(SumAggregator.Summation<GoogleAnalyticsResult>(
                            new[] { 
                                new GoogleAnalyticsResult() { 
                                    BusinessUnitId = businessUnitResult.BusinessUnitId, 
                                    BusinessUnit = businessUnitResult.BusinessUnit,
                                    StartDate = dateRange.StartDate,
                                    EndDate = dateRange.EndDate
                                }, 
                                businessUnitResult 
                            }
                        ));
                    }
                });
            });
            return groupTotalForRange;
        }

        private void FetchGenerateCachedMonths(int groupid, GoogleAnalyticsType profiletype, Dictionary<DateRange, IEnumerable<GoogleAnalyticsResult>> groupDataByMonth, List<DateRange> missingMonths)
        {
            if (Log.IsDebugEnabled)
                Log.Debug("Generating new files for missed months: ");

            missingMonths.ForEach(missingMonth =>
            {
                IEnumerable<GoogleAnalyticsResult> mr = GenerateGroupRangedGoogleData(groupid, profiletype, missingMonth);
                if (mr != null)
                {
                    if (groupDataByMonth.Keys.Contains(missingMonth))
                    {
                        List<GoogleAnalyticsResult> md = groupDataByMonth[missingMonth].ToList();
                        md.AddRange(mr);
                    }
                    else
                        groupDataByMonth.Add(missingMonth, mr);
                }
            });
        }

        private List<DateRange> FetchCachedMonths(int groupid, GoogleAnalyticsType profiletype, DateRange dateRange, Dictionary<DateRange, IEnumerable<GoogleAnalyticsResult>> groupDataByMonth)
        {
            List<DateRange> missingMonths = new List<DateRange>();
            List<Task<IEnumerable<GoogleAnalyticsResult>>> MonthDataTasks = new List<Task<IEnumerable<GoogleAnalyticsResult>>>();

            if (Log.IsDebugEnabled)
                Log.DebugFormat("FetchGroupRangedGoogleData: {0}, {1}, {2}", groupid, profiletype, dateRange);

            dateRange.AsMonths().ToList().ForEach(month =>
            {
                MonthDataTasks.Add(GetGroupMonthDataAsync(groupid, profiletype, month));
            });

            // Spin through tasks to read from S3
            // check for existing group level file.
            Task.WaitAll(MonthDataTasks.ToArray());
            for (int x = 0; x < MonthDataTasks.Count; x++)
            {
                Task<IEnumerable<GoogleAnalyticsResult>> task = MonthDataTasks[x];
                if (task.IsCompleted)
                {
                    if (task.Result != null && task.Result.Count() > 0)
                    {
                        groupDataByMonth.Add(
                            new DateRange(task.Result.First().StartDate, task.Result.First().EndDate),
                            task.Result
                        );
                    }
                    else
                    {
                        // if it failed add it to a list so we can call GA consecutively in order to avoid their rate limiter.
                        missingMonths.Add(dateRange.AsMonths().ToArray()[x]);
                    }
                }
            }

            return missingMonths;
        }

        private Task<IEnumerable<GoogleAnalyticsResult>> GetGroupMonthDataAsync(int groupid, GoogleAnalyticsType profiletype, DateRange month)
        {
            return Task.Factory.StartNew(() => GetCachedGroupAnalyticsResult(groupid, profiletype, month));
        }

        private IEnumerable<GoogleAnalyticsResult> GetCachedGroupAnalyticsResult(int groupid, GoogleAnalyticsType profiletype, DateRange month)
        {
            List<GoogleAnalyticsResult> analyticsResult = new List<GoogleAnalyticsResult>();

            if (Log.IsDebugEnabled)
                Log.DebugFormat("GetCachedAnalyticsResult: {0}, {1}, {2}", groupid, profiletype, month);

            // check repo
            string key = CreateCurrentRangeGroupGoogleDataKey(groupid, profiletype, month);
            bool exists;
            string txt = null;
            DateTime? isThisMonth = null;

            // If the month we're checking for is *the current* month, then see if we've already checked for data today.
            if (month.StartDate.Equals(DateTime.Now.FirstSecondOfMonth()))
            {
                isThisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            }

            using (var stream = _fileStore.Exists(key, out exists, isThisMonth))
            {
                if (exists)
                {
                    if (Log.IsDebugEnabled)
                        Log.DebugFormat("Found CachedAnalyticsResult: {0}, {1}, {2}", groupid, profiletype, month);

                    using (var reader = new StreamReader(stream))
                    {
                        txt = reader.ReadToEnd();
                    }

                    if (!String.IsNullOrWhiteSpace(txt))
                        analyticsResult = txt.FromJson<List<GoogleAnalyticsResult>>();
                }
                else if (Log.IsDebugEnabled)
                {
                    Log.Debug("GetCachedGroupAnalyticsResult Cache Miss: " + key);
                }
            }

            if (Log.IsDebugEnabled)
                Log.DebugFormat("End GetCachedGroupAnalyticsResult: {0}, {1}, {2}", groupid, profiletype, month);

            return analyticsResult;
        }

        #endregion Group Private Methods

        #endregion Group Methods


        private static string CreateDateRangeExecutiveGoogleDataKey(GoogleAnalyticsProfile profile, DateRange month)
        {
            return string.Format(@"{0}/{1}/{2}/{3}/{4}/{5}-{6}-{7}", FOLDERNAME, profile.BusinessUnitId, GOOGLEDATA, profile.ProfileId, profile.SiteProvider, ANALYTICS, month.StartDate.ToString("yyyy-MM-dd"), month.EndDate.ToString("yyyy-MM-dd"));
        }

        private string CreateCurrentRangeGroupGoogleDataKey(int groupid, GoogleAnalyticsType profiletype, DateRange month)
        {
            return string.Format(@"{0}/{1}/Current/{2}/{3}/{4}-{5}-{6}", GROUPFOLDERNAME, groupid, GOOGLEDATA, profiletype, ANALYTICS, month.StartDate.ToString("yyyy-MM-dd"), month.EndDate.ToString("yyyy-MM-dd"));
        }
        private string CreateDateRangeGroupGoogleDataKey(int groupid, GoogleAnalyticsType profiletype, DateRange month)
        {
            return string.Format(@"{0}/{1}/{2}/{3}/{4}/{5}-{6}-{7}", GROUPFOLDERNAME, groupid,DateTime.Now.ToString("yyyy-MM-dd"), GOOGLEDATA, profiletype, ANALYTICS, month.StartDate.ToString("yyyy-MM-dd"), month.EndDate.ToString("yyyy-MM-dd"));
        }

    }
}
