﻿using System;
using System.Data;
using FirstLook.Common.Core.Utilities;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Reports;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal
{
    internal class GoogleAnalyticsRepository : IGoogleAnalyticsRepository
    {
        public bool IsActiveRefreshToken(int businessUnitId)
        {
            bool exists = false;
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "settings.GoogleAnalyticsRefreshToken#Active";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "businessUnitId", businessUnitId, DbType.Int32);

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException("result expected");

                        exists = reader.GetInt32(reader.GetOrdinal("Exist")) == 0 ? false : true;
                    }
                }
            }

            return exists;
        }

        public void SetRefreshTokenInactive(int businessUnitId)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            {
                connection.Open();
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = "settings.GoogleAnalyticsRefreshToken#Inactive";
                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(command, "businessUnitId", businessUnitId, DbType.Int32);

                    command.ExecuteNonQuery();
                }
            }
        }

        public void SetTokens(int businessUnitId, GoogleTokens tokens)
        {
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GoogleAnalyticsRefreshToken#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "refreshToken", tokens.RefreshToken, DbType.String);
                    Database.AddRequiredParameter(cmd, "clientSecret", tokens.ClientSecret, DbType.String);
                    Database.AddRequiredParameter(cmd, "clientId", tokens.ClientId, DbType.String);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public GoogleTokens FetchTokens(int businessUnitId)
        {
            GoogleTokens tokens = null;

            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GoogleAnalyticsRefreshToken#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                            throw new InvalidOperationException("expected refresh token");

                        string refreshToken = reader.GetString(reader.GetOrdinal("RefreshToken"));
                        string clientId = reader.GetString(reader.GetOrdinal("ClientId"));
                        string clientSecret = reader.GetString(reader.GetOrdinal("ClientSecret"));

                        tokens = new GoogleTokens(clientId, clientSecret, refreshToken);
                    }
                }
            }

            return tokens;
        }

        public void CreateUpdateProfileAccount(GoogleAnalyticsProfile profile)
        {
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GoogleAnalyticsProfile#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", profile.BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "type", profile.Type, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "profileId", profile.ProfileId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "profileName", profile.ProfileName, DbType.String);
                    Database.AddRequiredParameter(cmd, "destinationId", profile.SiteProvider, DbType.Byte);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public GoogleAnalyticsProfile FetchProfile(int businessUnitId, GoogleAnalyticsType type)
        {
            GoogleAnalyticsProfile profile = null;
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GoogleAnalyticsProfile#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "type", type, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                            return null; //no object

                        int profileId = reader.GetInt32(reader.GetOrdinal("ProfileId"));
                        string profileName = reader.GetString(reader.GetOrdinal("ProfileName"));
                        int destinationId = reader.GetInt32(reader.GetOrdinal("DestinationId"));

                        profile = new GoogleAnalyticsProfile(businessUnitId, type, profileId, profileName, destinationId);
                    }
                }
            }

            return profile;
        }


        public List<GoogleQuery> GetQueries(GoogleAnalyticsProfile profile, GoogleQueryType googleQueryType)
        {
            List<GoogleQuery> results = new List<GoogleQuery>();
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GoogleAnalyticsQueries#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ProfileId", profile.ProfileId.ToString(), DbType.String);
                    Database.AddRequiredParameter(cmd, "QueryType", googleQueryType, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", profile.BusinessUnitId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsClosed)
                                results.Add( new GoogleQuery(reader["QueryName"].ToString(), googleQueryType, new GoogleQueryParameters()
                                {
                                    Dimensions = reader.IsDBNull(reader.GetOrdinal("Dimensions")) ? String.Empty : reader.GetString(reader.GetOrdinal("Dimensions")),
                                    Metrics = reader.IsDBNull(reader.GetOrdinal("Metrics")) ? String.Empty : reader.GetString(reader.GetOrdinal("Metrics")),
                                    Limit = reader.IsDBNull(reader.GetOrdinal("Limit")) ? 0 : reader.GetInt32(reader.GetOrdinal("Limit")),
                                    Segment = reader.IsDBNull(reader.GetOrdinal("Segment")) ? String.Empty : reader.GetString(reader.GetOrdinal("Segment")),
                                    Sort = reader.IsDBNull(reader.GetOrdinal("Sort")) ? String.Empty : reader.GetString(reader.GetOrdinal("Sort")),
                                    Filter = reader.IsDBNull(reader.GetOrdinal("Filter")) ? String.Empty : reader.GetString(reader.GetOrdinal("Filter"))
                                }));
                        }
                    }
                }
            }
            return results;
        }

        public List<GoogleAnalyticsProfile> GetAllActiveProfiles()
        {
            List<GoogleAnalyticsProfile> profiles = new List<GoogleAnalyticsProfile>();
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GoogleAnalyticsProfile#FetchAllActive";
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int businessUnitId = reader.GetInt32(reader.GetOrdinal("BusinessUnitId"));
                            GoogleAnalyticsType type = (GoogleAnalyticsType)reader.GetInt32(reader.GetOrdinal("Type")); 
                            int profileId = reader.GetInt32(reader.GetOrdinal("ProfileId"));
                            string profileName = reader.GetString(reader.GetOrdinal("ProfileName"));
                            int destinationId = reader.GetInt32(reader.GetOrdinal("DestinationId"));

                            profiles.Add(new GoogleAnalyticsProfile(businessUnitId, type, profileId, profileName, destinationId));
                        }
                    }
                }
            }

            return profiles;
        }


        public void DeleteProfile(int businessUnitId, GoogleAnalyticsType type)
        {
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "settings.GoogleAnalyticsProfile#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("businessUnitId", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter("type", type, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Write_AdminLock( int BU_id, bool locked )
        {
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "UPDATE settings.Merchandising SET LockSelectedGoogleAnalyticsProfile=@LockSelectedGoogleAnalyticsProfile WHERE businessUnitID=@businessUnitID";
                    cmd.AddRequiredParameter("businessUnitID", BU_id, DbType.Int32);
                    cmd.AddRequiredParameter("LockSelectedGoogleAnalyticsProfile", locked, DbType.Boolean);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public bool Read_AdminLock( int BU_id )
        {
            using (var con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT TOP 1 LockSelectedGoogleAnalyticsProfile FROM settings.Merchandising WHERE businessUnitID=@businessUnitID";
                    cmd.AddRequiredParameter("businessUnitID", BU_id, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean( 0 ); // assumed to be first and only field of result set
                        }
                    }
                }
            }
            return false; // default value, should not be needed
        }


    }
}
