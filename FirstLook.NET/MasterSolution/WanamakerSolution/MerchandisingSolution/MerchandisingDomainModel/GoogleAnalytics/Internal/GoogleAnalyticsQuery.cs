﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetOpenAuth.OAuth2;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.Utilities;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Util;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal
{
    internal class GoogleAnalyticsQuery : IGoogleAnalyticsQuery
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<GoogleAnalyticsQuery>();

        private IGoogleAnalyticsRepository _repository;
        private AnalyticsService _service;

        public GoogleAnalyticsQuery(IGoogleAnalyticsRepository repository)
        {
            _repository = repository;
        }


        #region Implement Interface


        public void Init(int businessUnitId)
        {
            var client = new NativeApplicationClient(GoogleAuthenticationServer.Description);

            var tokens = _repository.FetchTokens(businessUnitId);
            client.ClientIdentifier = tokens.ClientId;
            client.ClientSecret = tokens.ClientSecret;

            var auth = new OAuth2Authenticator<NativeApplicationClient>(client, (nativeClient) => GetAuthorization(nativeClient, tokens.RefreshToken));
            _service = new AnalyticsService(auth);
        }
        
        /// <summary>
        /// Return GaData object - direct result from Google Analytics
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="gqParams"></param>
        /// <param name="dateRange"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData DoQuery(GoogleAnalyticsProfile profile, GoogleQueryParameters gqParams, DateRange dateRange, int limit = 25)
        {
            GaData data = new GaData();

            string profileid = "ga:" + profile.ProfileId;
            string startDate = dateRange.StartDate.ToString("yyyy-MM-dd");
            string endDate = dateRange.EndDate.ToString("yyyy-MM-dd");
            DataResource.GaResource.GetRequest request = _service.Data.Ga.Get(profileid, startDate, endDate, gqParams.Metrics);

            // 1000 is the google default max value.
            if (limit == 0)
                limit = gqParams.Limit;

            request.MaxResults = limit;
            request.Dimensions = gqParams.Dimensions;
            request.Sort = gqParams.Sort;
            request.Segment = gqParams.Segment;
            request.Filters = gqParams.Filter;

            // Try to avoid most of the GA limits.
            request.QuotaUser = profile.BusinessUnitId.ToString();
            if (System.Web.HttpContext.Current != null && !String.IsNullOrEmpty(System.Web.HttpContext.Current.Request.UserHostAddress))
                request.UserIp = System.Web.HttpContext.Current.Request.UserHostAddress;

            try
            {
                if (Log.IsDebugEnabled)
                    Log.Debug("Starting call to Google Analytics GetReferrals");

                var t = DateTimeOffset.UtcNow;

                data = request.Fetch();

                if (Log.IsDebugEnabled)
                    Log.DebugFormat("Retrieved referrals. ElapsedMs={0:0.0} ",
                        (DateTimeOffset.UtcNow - t).TotalMilliseconds);
            }
            catch (DotNetOpenAuth.Messaging.ProtocolException prex)
            {
                Log.Error("GetAuth Failed", prex);
                _repository.SetRefreshTokenInactive(profile.BusinessUnitId);
                Exception baseException = prex.GetBaseException();

                throw new System.AccessViolationException("GetAuth Failed", prex.GetBaseException());
            }
            catch (Google.GoogleApiRequestException gare)
            {
                Log.Error("GoogleApiRequestException: ", gare);
                if(gare.RequestError != null && gare.RequestError.Code.Equals(403) && gare.RequestError.Errors.FirstOrDefault(singleError => singleError.Reason.ToUpperInvariant().Contains("QUOTA")) != null)
                {
                    // We have exceeded our rate limit. back of and try again.
                    Log.Error(gare.ToJson());
                }
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("Google Query Failed unexpectedly", ex);
                throw;
            }

            return data;
        }

        /// <summary>
        /// Return the GaData for all available queries given a profile, query type and date range.
        /// </summary>
        /// <param name="profile">GoogleAnalyticsProfile</param>
        /// <param name="queryType"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public GoogleAnalyticsResult DoAllQueries(GoogleAnalyticsProfile profile, DateRange dateRange, int limit = 25)
        {
            List<GoogleQuery> allTheQueries = new List<GoogleQuery>();

            foreach (GoogleQueryType queryType in EnumHelper.GetValues<GoogleQueryType>())
            {
                // Do not attempt vdp queries.
                if (Enum.GetName(typeof(GoogleQueryType), queryType).ToUpperInvariant().Contains("VDP"))
                    continue;

                allTheQueries.AddRange(_repository.GetQueries(profile, queryType) );
            }

            GoogleAnalyticsResult result = new GoogleAnalyticsResult(profile, dateRange);

            if ( allTheQueries.IsNotNullOrEmpty())
            {
                /** Async methods
                // Spin up all the requests
                Dictionary<GoogleQuery, Task<GaData>> queryTaskresults = StartQueriesAsync(profile, dateRange, limit, allTheQueries);
                // Wait on the data to return
                Task.WaitAll(queryTaskresults.Values.ToArray());
                
                foreach (KeyValuePair<GoogleQuery, Task<GaData>> kvp in queryTaskresults)
                {
                    if (kvp.Key.QueryType.Equals(GoogleQueryType.Referrals))
                    {
                        result.Referrals.Add(kvp.Key.QueryName, new ReferralResult(kvp.Value.Result));
                    }
                }
                ***/
                foreach (GoogleQuery gq in allTheQueries)
                {
                    GaData gdat = DoQuery(profile, gq.Parameters, dateRange);
                    result.Referrals.Add(gq.QueryName, new ReferralResult(gdat));
                }
            }

            // This is where we get the VDP results
            Dictionary<DateTime, VDPResult> vdps = GetVdpTrend(profile, dateRange);
            if (vdps.Count > 0)
            {
                foreach (VDPResult trendvdp in vdps.Values)
                {
                    result.VDP += trendvdp;
                }
            }

            return result;
        }

        /// <summary>
        /// A month indexed dictionary of VDPResults based on Google Analytics Data
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="dateRange"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public Dictionary<DateTime, VDPResult> GetVdpTrend(GoogleAnalyticsProfile profile, DateRange dateRange, int limit = 50)
        {
            Dictionary<DateTime, VDPResult> vdpTrends = new Dictionary<DateTime, VDPResult>();

            List<GoogleQuery> allTheQueries = new List<GoogleQuery>();

            foreach (GoogleQueryType queryType in EnumHelper.GetValues<GoogleQueryType>())
            {
                // Do not attempt vdp queries.
                if (Enum.GetName(typeof(GoogleQueryType), queryType).ToUpperInvariant().Contains("VDP"))
                    allTheQueries.AddRange(_repository.GetQueries(profile, queryType));
            }

            if (allTheQueries.IsNotNullOrEmpty())
            {
                /**** Async method
                 *  Removed (for now) until we can handle the rate limiting
                 *  Check out our Exponential Backoff Calculator and handle Google.GoogleApiRequestException
                // Spin up all the requests
                Dictionary<GoogleQuery, Task<GaData>> queryTaskresults = StartQueriesAsync(profile, dateRange, limit, allTheQueries);

                // Wait on the data to return
                Task.WaitAll(queryTaskresults.Values.ToArray());
                
                foreach (KeyValuePair<GoogleQuery, Task<GaData>> kvp in queryTaskresults)
                {
                    if (kvp.Value.IsFaulted)
                    {
                        AggregateException agEx = kvp.Value.Exception;
                        Google.GoogleApiRequestException rex = (Google.GoogleApiRequestException)agEx.InnerExceptions.FirstOrDefault(ex => ex.GetType().Equals(typeof(Google.GoogleApiRequestException)));

                    }
                    else
                        AddVdpTrend(kvp.Key.QueryType, kvp.Value.Result, ref vdpTrends);
                }
                ****/

                // Synchronous calls.
                foreach (GoogleQuery gq in allTheQueries)
                {
                    GaData gdat = DoQuery(profile, gq.Parameters, dateRange);
                    AddVdpTrend(gq.QueryType, gdat, ref vdpTrends);
                }
            }

            return vdpTrends;
        }

        public ProfileResult[] GetProfiles()
        {
            var list = new List<ProfileResult>();

            try
            {
                Profiles googleProfile = _service.Management.Profiles.List("~all", "~all").Fetch();
                if (googleProfile.TotalResults > 0)
                {
                    foreach (var profile in googleProfile.Items)
                    {
                        var profileName = String.Format("{0} - {1} ({2})", profile.Name, profile.WebsiteUrl, profile.WebPropertyId);
                        list.Add(new ProfileResult(int.Parse(profile.Id), profileName));
                    }
                }
            }
            catch (DotNetOpenAuth.Messaging.ProtocolException prex)
            {
                Log.Error("GetAuth Failed", prex);

                Exception baseException = prex.GetBaseException();

                throw new System.AccessViolationException("GetAuth Failed", prex.GetBaseException());
            }
            catch (Google.GoogleApiException gax)
            {
                Exception baseException = gax.GetBaseException();
                Log.Error("User does not have Analytics API configured", baseException);
                throw new System.AccessViolationException("User does not have Analytics API configured", baseException);
            }
            return list.ToArray();
        }
        
        
        #endregion Implement Interface

        #region private methods

        /// <summary>
        /// Start up a task for each query in the list. Returns a Dictionary where key = query and value = the System.Threading.Task
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="dateRange"></param>
        /// <param name="limit"></param>
        /// <param name="allTheQueries"></param>
        /// <returns></returns>
        private Dictionary<GoogleQuery, Task<GaData>> StartQueriesAsync(GoogleAnalyticsProfile profile, DateRange dateRange, int limit, List<GoogleQuery> allTheQueries)
        {
            Dictionary<GoogleQuery, Task<GaData>> queryTaskresults = new Dictionary<GoogleQuery, Task<GaData>>();
            // Queue up all the query tasks
            foreach (GoogleQuery gq in allTheQueries)
            {
                queryTaskresults.Add(gq, Task.Factory.StartNew(() => DoQuery(profile, gq.Parameters, dateRange, limit)));
            }

            return queryTaskresults;
        }
        
        private void AddVdpTrend(GoogleQueryType queryType, GaData gaData, ref Dictionary<DateTime, VDPResult> vdpTrends)
        {
            foreach (List<string> rows in gaData.Rows)
            {
                DateTime vdpMonth = new DateTime(int.Parse(rows[0]), int.Parse(rows[1]), 1);
                VDPResult vdpResult;
                if (!vdpTrends.TryGetValue(vdpMonth, out vdpResult))
                    vdpResult = new VDPResult();

                vdpResult.SetVdp(queryType, gaData);

                if (!vdpTrends.ContainsKey(vdpMonth))
                    vdpTrends.Add(vdpMonth, vdpResult);
            }
        }

        private static IAuthorizationState GetAuthorization(NativeApplicationClient client, string refreshToken)
        {
            IAuthorizationState state = new AuthorizationState(new[] { AnalyticsService.Scopes.AnalyticsReadonly.GetStringValue() }) { RefreshToken = refreshToken };

            if (Log.IsDebugEnabled)
                Log.Debug("Starting call to Google Analytics RefreshToken");

            var t = DateTimeOffset.UtcNow;

            client.RefreshToken(state);

            if (Log.IsDebugEnabled)
                Log.DebugFormat("Token refreshed. ElapsedMs={0:0.0} ",
                    (DateTimeOffset.UtcNow - t).TotalMilliseconds);

            return state;
        }

        #endregion private methods
    }   
}
