﻿using System.Collections.Generic;
using System;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public class VDPResult 
    {
        public int New { get; set; }
        public int Used { get; set; }
        public int Both { get; set; }

        public static VDPResult operator +(VDPResult VDP1, VDPResult VDP2)
        {
            return new VDPResult()
            {
                New = VDP1.New + VDP2.New,
                Used = VDP1.Used + VDP2.Used,
                Both = VDP1.Both + VDP2.Both
            };
        }
    }

    internal static class VDPResultExtensions
    {
        internal static void SetVdp( this VDPResult VDP, GoogleQueryType queryType, Google.Apis.Analytics.v3.Data.GaData gaData)
        {
            foreach (List<string> iRow in gaData.Rows)
            {
                int vdpval = int.Parse(iRow[2]);

                switch (queryType)
                {
                    case GoogleQueryType.VDPNew:
                        VDP.New += vdpval;
                        break;
                    case GoogleQueryType.VDPUsed:
                        VDP.Used += vdpval;
                        break;
                    default:
                        VDP.Both += vdpval;
                        break;
                }
            }
        }
    }
}

