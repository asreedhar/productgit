﻿
namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public class ProfileResult
    {
        public ProfileResult(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
    }
}
