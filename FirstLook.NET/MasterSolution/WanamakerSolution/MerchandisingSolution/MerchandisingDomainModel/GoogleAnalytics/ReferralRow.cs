﻿using System;
using System.Collections.Generic;
using System.Reflection;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Logging;

namespace FirstLook.Merchandising.DomainModel.GoogleAnalytics
{
    public class ReferralRow
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<ReferralRow>();

        // Returned values
        public int Visits { get; set; }
        public int NewVisits { get; set; }
        public int Bounces { get; set; }
        public double TimeOnSite { get; set; }
        public int Visitors { get; set; }
        public int Pageviews { get; set; }
        public double TimeOnPage { get; set; }
        public int UniquePageviews { get; set; }
        public int Exits { get; set; }

        // Calculated Values
        // Always return a number (ie default to 0)
        public double PageviewsPerVisit
        {
            get
            {
                if (Visits > 0)
                    return (double)Pageviews / (double)Visits;
                else
                    return 0;
            }
        }
        public double AverageVisitDuration
        {
            get
            {
                if (Visits > 0)
                    return (double)TimeOnSite / (double)Visits;
                else
                    return 0;
            }
        }
        public double PercentageNew
        {
            get
            {
                if (Visits > 0)
                    return ((double)NewVisits / (double)Visits) * 100;
                else
                    return 0;
            }
        }
        public double VisitBounceRate
        {
            get
            {
                if (Visits > 0)
                    return ((double)Bounces / (double)Visits) * 100;
                else
                    return 0;
            }
        }

        public ReferralRow() { }

        internal static ReferralRow FromGDataRow(List<Google.Apis.Analytics.v3.Data.GaData.ColumnHeadersData> headers, IList<string> row)
        {
            ReferralRow referral = new ReferralRow();
            Type ttype = referral.GetType();
            foreach (PropertyInfo pinfo in ttype.GetProperties())
            {
                try
                {
                    string propertyName = "ga:" + pinfo.Name.FirstCharacterToLower();
                    int headIdx = headers.FindIndex(hIdx => hIdx.Name.Equals(propertyName));
                    if (headIdx > -1)
                    {
                        double doubleValue;

                        if (Double.TryParse(row[headIdx], out doubleValue))
                        {
                            pinfo.SetValue(referral, Convert.ChangeType(doubleValue, pinfo.PropertyType), null);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Problem setting data FromGDataRow", ex);
                }
            }
            
            return referral;
        }

        internal static ReferralRow TotalsFromGData(List<Google.Apis.Analytics.v3.Data.GaData.ColumnHeadersData> headers, Google.Apis.Analytics.v3.Data.GaData.TotalsForAllResultsData totalsForAllResultsData)
        {
            ReferralRow totals = new ReferralRow();
            Type ttype = totals.GetType();
            
            foreach (PropertyInfo pinfo in ttype.GetProperties())
            {

                string totalName = "ga:" + pinfo.Name.FirstCharacterToLower();
                if (totalsForAllResultsData.ContainsKey(totalName))
                {

                    if (pinfo.PropertyType == typeof(int))
                    {
                        pinfo.SetValue(totals, int.Parse(totalsForAllResultsData[totalName]), null);
                    }
                    else if (pinfo.PropertyType == typeof(decimal))
                    {
                        pinfo.SetValue(totals, decimal.Parse(totalsForAllResultsData[totalName]), null);
                    }
                }
            }
            return totals;
        }
    }
}
