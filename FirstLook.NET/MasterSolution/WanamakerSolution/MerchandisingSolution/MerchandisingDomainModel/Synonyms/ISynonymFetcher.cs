﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public interface ISynonymFetcher
    {
        List<Synonym> Fetch();
    }
}