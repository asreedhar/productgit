﻿using System;
using System.Collections.Generic;
using System.Reflection;
using MAX.Caching;

namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public class CachedSynonymRepository : ISynonymRepository
    {
        private ICacheKeyBuilder KeyBuilder { get; set; }
        private ICacheWrapper Cache { get; set; }
        private ISynonymFetcher Fetcher { get; set; }
        private string _key;
        
        public CachedSynonymRepository(ICacheKeyBuilder keyBuilder, ICacheWrapper cache, ISynonymFetcher fetcher)
        {
            KeyBuilder = keyBuilder;
            Cache = cache;
            Fetcher = fetcher;
        }
        
        internal List<Synonym> Synonyms
        {
            get
            {
                var synonyms = Cache.ReadThroughCache(SynonymsCacheKey, Fetcher.Fetch, DateTime.Now.AddHours(2));
                return synonyms;
            }
        }

        public bool SynonymExists(string val1, string val2)
        {
            var synonym = new Synonym { Value = val1, SynonymousValue = val2 };
            var index = Synonyms.BinarySearch(synonym, new SynonymComparer());
            return index >= 0;            
        }

        internal string SynonymsCacheKey
        {
            get
            {
                if (_key == null)
                {
                    _key = KeyBuilder.CacheKey(new[] { "SynonymRepository", MethodBase.GetCurrentMethod().Name });
                }
                return _key;
            }
        }

        public void ClearCache()
        {
            Cache.Set(SynonymsCacheKey, null);
        }
    }
}