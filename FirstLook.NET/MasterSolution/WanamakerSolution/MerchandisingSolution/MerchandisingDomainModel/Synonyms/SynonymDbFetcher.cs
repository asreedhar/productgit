﻿using System.Collections.Generic;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public class SynonymDbFetcher : ISynonymFetcher
    {
        public List<Synonym> Fetch()
        {
            var list = new List<Synonym>();

            using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "settings.TextSynonyms#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var value = reader.GetString(reader.GetOrdinal("TextValue"));
                            var othervalue = reader.GetString(reader.GetOrdinal("TextSynonym"));
                            list.Add(new Synonym { Value = value, SynonymousValue = othervalue });
                        }
                    }
                }
            }
            list.Sort(new SynonymComparer());
            return list;
        }

    }
}