﻿using System;

namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public static class Extensions
    {
        public static bool IsSynonymous(this String s, string other, ISynonymEvaluator synonymEvaluator)
        {
            return synonymEvaluator.AreSynonymous(s, other);
        }
    }
}