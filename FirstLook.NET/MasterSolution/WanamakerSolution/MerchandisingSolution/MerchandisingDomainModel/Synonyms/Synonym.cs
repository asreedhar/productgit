﻿using System;

namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    [Serializable]
    public class Synonym
    {
        public string Value { get; set; }
        public string SynonymousValue { get; set; }
    }
}