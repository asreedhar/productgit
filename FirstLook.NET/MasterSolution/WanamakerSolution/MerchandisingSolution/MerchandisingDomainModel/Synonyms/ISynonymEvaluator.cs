﻿namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public interface ISynonymEvaluator
    {
        bool AreSynonymous(string val1, string val2);
    }
}