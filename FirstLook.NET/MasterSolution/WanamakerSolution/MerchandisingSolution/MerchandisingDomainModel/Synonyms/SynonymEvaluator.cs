﻿using System;

namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public class SynonymEvaluator : ISynonymEvaluator
    {
        private ISynonymRepository Repository { get; set; }

        public SynonymEvaluator(ISynonymRepository repository)
        {
            Repository = repository;
        }

        public bool AreSynonymous(string value, string other)
        {
            var sameExactThing = String.Compare(value, other, StringComparison.OrdinalIgnoreCase) == 0;

            return sameExactThing || Repository.SynonymExists(value, other);
        }
    }
}