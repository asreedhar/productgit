﻿
namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public interface ISynonymRepository
    {
        bool SynonymExists(string val1, string val2);
    }
}
