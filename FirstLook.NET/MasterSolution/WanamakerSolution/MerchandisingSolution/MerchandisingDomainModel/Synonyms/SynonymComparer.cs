﻿using System;
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Synonyms
{
    public class SynonymComparer : IComparer<Synonym>
    {
        /// <summary>
        /// Compares the two Synonyms by "Value" first, then "SynonymousValue" if the "Value" properties match.
        /// Can be used to sort the Synonyms in "Value","SynonymousValue" order.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(Synonym x, Synonym y)
        {
            if (ReferenceEquals(x, y))
                return 0;
            if (x == null)
                return -1;
            if (y == null)
                return 1;

            var valuesDiff = String.Compare(x.Value, y.Value, StringComparison.OrdinalIgnoreCase);

            return valuesDiff == 0 ?
                String.Compare(x.SynonymousValue, y.SynonymousValue, StringComparison.OrdinalIgnoreCase) :
                valuesDiff;
        }
    }
}