using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml;
using FirstLook.Common.Data;
using log4net;
using log4net.Core;
using MvcMiniProfiler.Data;

namespace FirstLook.Merchandising.DomainModel
{
    internal static partial class Database
    {
        private const string MERCHANDISING_DATABASE = "Merchandising";
        private const string CHROME_DATABASE = "Merchandising";
        private const string VEHICLE_CATALOG_DATABASE = "VehicleCatalog";
        private const string MARKET_DATABASE = "Market";
        private const string IMT_DATABASE = "IMT";
        public const string PricingDatabase = "Market";

        public static string MarketDatabase
        {
            get { return MARKET_DATABASE; }
        }

        public static string MerchandisingDatabase
        {
            get { return MERCHANDISING_DATABASE; }
        }

        public static string ChromeDatabase
        {
            get { return CHROME_DATABASE; }
        }

        public static string VehicleCatalogDatabase
        {
            get { return VEHICLE_CATALOG_DATABASE; }
        }

        public static string IMTDatabase
        {
            get { return IMT_DATABASE; }
        }

        private static IDataConnection GetMiniProfilerConnection(string databaseName)
        {
            //Not so greate downcasting but need to wrap the in profiler
            var wrapper = SimpleQuery.ConfigurationManagerConnection(databaseName) as DbConnectionWrapper;
            DbConnection connection = wrapper.Connection() as DbConnection;
            var miniProfiler = new ProfiledDbConnection(connection, MvcMiniProfiler.MiniProfiler.Current);
            var dataConnection = new DataConnection(miniProfiler);
            return dataConnection;
        }

        public static IDataConnection GetConnection(string databaseName)
        {
            return GetConnection(databaseName, true);
        }

        public static IDataConnection GetConnection(string databaseName, bool profile)
        {
            var conn = (profile && (MvcMiniProfiler.MiniProfiler.Current != null))
                       ? GetMiniProfilerConnection(databaseName)
                       : SimpleQuery.ConfigurationManagerConnection(databaseName);

            var timedWrapper = new DataConnectionWithTimedComamnds(conn);
            return new DataConnection(timedWrapper);
        }

        public static IDataConnection GetOpenConnection(string databaseName)
        {
            var cn = GetConnection(databaseName);
            var success = false;
            try
            {
                cn.Open();
                success = true;
            }
            finally
            {
                if(!success)
                    cn.Dispose();
            }
            return cn;
        }

        public static void AddRequiredParameter(this IDbCommand command, string parameterName, object parameterValue,
                                                DbType dbType)
        {
            if (parameterValue == null)
            {
                SimpleQuery.AddNullValue(command, parameterName, dbType);
            }
            else
            {
                SimpleQuery.AddWithValue(command, parameterName, parameterValue, dbType);
            }
        }

        public static decimal? GetNullableDecimal(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetDecimal(ord);
        }

        public static DateTime? GetNullableDateTime(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetDateTime(ord);
        }

        public static float? GetNullableFloat(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetFloat(ord);
        }

        public static double? GetNullableDouble(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetDouble(ord);
        }

        public static bool? GetNullableBoolean(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetBoolean(ord);
        }

        public static decimal? GetNullableDecimal(object o)
        {
            if (o == DBNull.Value)
            {
                return null;
            }
            return (decimal) o;
        }

        public static int? GetNullableInt(IDataRecord reader, string column)
        {
            int ord = reader.GetOrdinal(column);
            if (reader.IsDBNull(ord))
            {
                return null;
            }

            return reader.GetInt32(ord);
        }

        public static int? GetNullableInt(XmlNode node, string column)
        {
            XmlAttribute xa = node.Attributes[column];
            if (xa == null || string.IsNullOrEmpty(xa.Value))
            {
                return null;
            }

            return Convert.ToInt32(node.Attributes[column].Value);
        }

        public static decimal? GetNullableDecimal(XmlNode node, string column)
        {
            XmlAttribute xa = node.Attributes[column];
            if (xa == null || string.IsNullOrEmpty(xa.Value))
            {
                return null;
            }

            return Convert.ToDecimal(node.Attributes[column].Value);
        }

        public static bool? GetNullableBoolean(XmlNode node, string column)
        {
            XmlAttribute xa = node.Attributes[column];
            if (xa == null || string.IsNullOrEmpty(xa.Value))
            {
                return null;
            }
            string val = node.Attributes[column].Value;
            if (val.Length == 1)
            {
                return Convert.ToBoolean(Convert.ToInt32(val));
            }
            return Convert.ToBoolean(val);
        }

        public static string GetNullableString(IDataRecord reader, string column)
        {
            if (reader.IsDBNull(reader.GetOrdinal(column)))
            {
                return string.Empty;
            }
            return reader.GetString(reader.GetOrdinal(column));
        }

        public static bool ColumnExists(this IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i) == columnName)
                {
                    return true;
                }
            }

            return false;
        }

        public static void AddParameterWithValue(this IDbCommand cmd, string paramName, object value)
        {
            var parameter = cmd.CreateParameter();
            parameter.ParameterName = paramName;
            parameter.Value = value;
            parameter.DbType = GetDbType(value.GetType());

            cmd.Parameters.Add(parameter);
        }

        private static DbType GetDbType(Type theType)
        {
            var param = new SqlParameter();
            var tc = TypeDescriptor.GetConverter(param.DbType);
            if (tc.CanConvertFrom(theType))
            {
                var convertFrom = tc.ConvertFrom(theType.Name);
                if (convertFrom != null)
                {
                    param.DbType = (DbType)convertFrom;
                }
            }
            else
            {
                // try to forcefully convert
                try
                {
                    var convertFrom = tc.ConvertFrom(theType.Name);
                    if (convertFrom != null)
                    {
                        param.DbType = (DbType)convertFrom;
                    }
                }
                catch
                {
                    // ignore the exception
                }
            }
            return param.DbType;
        }
    }



    public class DataConnectionWithTimedComamnds : DbConnectionWrapper 
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataConnectionWithTimedComamnds).FullName);

        public DataConnectionWithTimedComamnds(IDbConnection connection) : base(connection)
        {
        }

        public override IDbCommand CreateCommand()
        {
            const string KEY = "CommandTimeoutSeconds";
            var cmd = base.CreateCommand();

            try
            {
                var settings = ConfigurationManager.AppSettings;
                if (settings[KEY] != null)
                {
                    cmd.CommandTimeout = Int32.Parse(settings[KEY]);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

            return cmd;
        }
    }
}