using System.Collections;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class SpecialsCollection : CollectionBase
    {
        public SpecialsCollection()
        {
            
        }
        internal SpecialsCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                List.Add(new FinancingSpecial(reader));
            }
        }
        public int Add(FinancingSpecial special)
        {
            return List.Add(special);
        }
        public static SpecialsCollection FetchValidSpecials(int ownerEntityTypeId, int ownerEntityId, InventoryData inventoryData)
        {
            SpecialsCollection collection = FetchCurrentSpecials(ownerEntityTypeId, ownerEntityId,
                                                                 inventoryData.CertificationTypeId);
            
            SpecialsCollection returnCollection = new SpecialsCollection();
            foreach (FinancingSpecial special in collection)
            {
                if (special.IsMatch(inventoryData))
                    returnCollection.Add(special);
            }
            return returnCollection;
        }
        public static SpecialsCollection FetchCurrentSpecials(int ownerEntityTypeId, int ownerEntityId, CertificationType certificationTypeId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.financingSpecials#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "OwnerEntityTypeId", ownerEntityTypeId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OwnerEntityId", ownerEntityId, DbType.Int32);
                    if (certificationTypeId > 0)
                    {
                        Database.AddRequiredParameter(cmd, "CertificationTypeId", ((int)certificationTypeId), DbType.Int32);
                    }

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new SpecialsCollection(reader);
                    }

                }
            }

        }
        
        public static bool HasSpecialFinancing(int businessUnitId, int? specialFinancingId, CertificationType? certificationTypeId)
        {

            if (specialFinancingId ==  null && certificationTypeId == null)
            {
                return false;
            }

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.financingSpecials#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "OwnerEntityTypeId", 1, DbType.Int32); //dealer
                    Database.AddRequiredParameter(cmd, "OwnerEntityId", businessUnitId, DbType.Int32);
                    if (specialFinancingId != null && specialFinancingId.Value > 0)
                    {
                        Database.AddRequiredParameter(cmd, "SpecialID", specialFinancingId, DbType.Int32);
                    }
                    if (certificationTypeId != null && certificationTypeId != CertificationType.Undefined)
                    {
                        Database.AddRequiredParameter(cmd, "CertificationTypeID", ((int)certificationTypeId), DbType.Int32);
                    }

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return true;
                        }

                        return false;
                    }

                }
            }

        }
    }
}
