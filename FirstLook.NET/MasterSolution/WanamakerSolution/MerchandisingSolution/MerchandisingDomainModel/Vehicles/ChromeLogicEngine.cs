using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for ChromeLogicEngine
    /// </summary>
    public static class ChromeLogicEngine
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(ChromeLogicEngine).FullName);
        #endregion


        public static decimal GetHighestOptionPrice(Equipment eq)
        {
            decimal retVal = 0.0M;
            foreach (PricingCondition pc in eq.PricingStates)
            {
                retVal = Math.Max(pc.Msrp, retVal);
            }
            return retVal;
        }

        public static decimal GetSingleOptionPrice(Equipment op, EquipmentCollection eqColl)
        {
            foreach (PricingCondition pc in op.PricingStates)
            {
                //process in order, b/c they are already sorted by addpricingcondition
                if (EvalLogic(pc.Condition, eqColl))                    
                {
                    return pc.Msrp;
                }
            }

            Log.Warn("debug me...should always have evaluated to true...");

            //throw new ApplicationException("debug me...should always have evaluated to true..."););
            return 0.0M;
        }
        public static decimal ComputeOptionsPrice(EquipmentCollection optionCollection)
        {
            decimal price = 0.0M;
            foreach (Equipment op in optionCollection)
            {
                if (op.Selected)
                {
                    price += GetSingleOptionPrice(op, optionCollection);
                }
            }
            return price;
        }

        /// <summary>
        /// From a list of optioncode/conditions/msrps return the best or highest priced options.
        /// 
        /// This method will prune the list of duplicate option codes by matching the Condition.
        /// In the event of a tie we keep only the highest price.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public static IEnumerable<ChromeMsrpCondition> GetBestOptionsByMsrp(IEnumerable<ChromeMsrpCondition> options)
        {
            var finalOptions = new List<ChromeMsrpCondition>();
            var chromeMsrpConditions = options as ChromeMsrpCondition[] ?? options.ToArray();
            var allOptionCodes = chromeMsrpConditions.Select(x => x.OptionCode).ToArray();
            
            foreach (var optionDto in chromeMsrpConditions)
            {
                ChromeMsrpCondition addOption;

                if (chromeMsrpConditions.Count(dto => dto.OptionCode == optionDto.OptionCode) > 1)
                {
                    if (!EvalLogic(optionDto.Condition,
                        allOptionCodes))
                    {
                        Log.InfoFormat("{0} - {1} lost for condition: {2}", optionDto.OptionCode, optionDto.Msrp, optionDto.Condition);
                        continue;
                    }

                    // If we do not already have a match for this option code, add it
                    if (finalOptions.All(opt => opt.OptionCode != optionDto.OptionCode))
                    {
                        addOption = optionDto;
                        Log.InfoFormat("{0} - {1} is the first of multiple matches. Condition: {2}", addOption.OptionCode, addOption.Msrp,
                            (string.IsNullOrWhiteSpace(optionDto.Condition)) ? "DEFAULT" : addOption.Condition);
                    }
                    else // We do already have a match
                    {
                        var originalMatch = finalOptions.First(fo => fo.OptionCode == optionDto.OptionCode);
                        finalOptions.Remove(originalMatch); // because we're going to be picking the best one.

                        var matchComparisons = new[] {originalMatch, optionDto};

                        // if any of these multiple matches is the default match
                        if (matchComparisons.Any(x => string.IsNullOrWhiteSpace(x.Condition)))  
                        {
                            // take the one that is not the default
                            addOption = matchComparisons.First(x => !string.IsNullOrWhiteSpace(x.Condition));
                            Log.InfoFormat("{0} - {1} is a better match for condition: {2}", addOption.OptionCode, addOption.Msrp, addOption.Condition);
                        }
                        else
                        {
                            //take the match with the highest price.
                            addOption = matchComparisons.Aggregate((condition, msrpCondition) => condition.Msrp >= msrpCondition.Msrp ? condition : msrpCondition);
                            Log.InfoFormat("{0} - {1} is the highest price match for condition: {2}", addOption.OptionCode, addOption.Msrp, addOption.Condition);
                        }
                    }
                }
                else
                {
                    addOption = optionDto;
                    Log.InfoFormat("{0} - {1} is the only match. Condition: {2}", addOption.OptionCode, addOption.Msrp, addOption.Condition);
                }

                finalOptions.Add(addOption);
            }

            return finalOptions;
        }


        public delegate bool ConditionProcessor(string TemplateCondition);
        
        public static bool EvalLogic(string logicStr, EquipmentCollection eqColl)  //change to bool when finished
        {
            return EvalLogic(logicStr, eqColl.TOSelectedOptionCodeList());
        }
        public static bool EvalLogic(string logicStr, ICollection<string> optionCodes)  //change to bool when finished
        {
            return EvalLogic(logicStr, opCode => optionCodes.Contains(opCode));
        }

        public static bool EvalLogic(string logicStr, ConditionProcessor conditionProcessor)
        {
            Log.DebugFormat("evalLogic() called with logicStr '{0}'",  logicStr);

            int ndxPos = 0;
            int ndxOpen = -1;
            int ndxClose = -1;
            string workingLogic = "";
            ndxOpen = logicStr.IndexOf('(', 0);

            while (ndxOpen != -1)
            {
                //outStr += "" + ndx;
                ndxPos = ndxOpen;
                ndxOpen = logicStr.IndexOf('(', ndxPos + 1);
                ndxClose = logicStr.IndexOf(')', ndxPos + 1);
                
                if (ndxClose == -1)
                {
                    Log.Warn("ndxClose == -1.  This indicates bad logic, missing close. Returning false.");
                    return false;// = "bad logic...missing close";
                }
                else if (ndxOpen == -1 || ndxClose < ndxOpen)
                { 
                    //we're closing up the open paren
                    workingLogic = logicStr.Substring(ndxPos + 1, ndxClose - ndxPos - 1);
                    Log.DebugFormat("workingLogic = {0}", workingLogic);

                    logicStr = logicStr.Remove(ndxPos, ndxClose - ndxPos + 1);
                    Log.DebugFormat("logicStr = {0}", logicStr);

                    if (ndxPos == 0)
                    {
                        logicStr = EvalInnerLogic(workingLogic, conditionProcessor) + logicStr;
                    }
                    else
                    {
                        logicStr = logicStr.Insert(ndxPos, EvalInnerLogic(workingLogic, conditionProcessor).ToString());
                    }
                    Log.DebugFormat("logicStr = {0}", logicStr);

                    //outStr = logicStr;

                    //need to reset the ndx vars to make loop look for next set of parens
                    ndxOpen = logicStr.IndexOf('(', 0);
                }
                else
                { //inner opening of parens
                    continue;
                }
            }

            // no open parens, proc and return the last logic (check for stray close?)
            bool result = EvalInnerLogic(logicStr, conditionProcessor);

            Log.DebugFormat("Result is {0}", result);
            return result;
        }

        //used to evaluate logic inside of the innermost parenthesis
        private static bool EvalInnerLogic(string logicStr, ConditionProcessor conditionProcessor)
        {
            Log.DebugFormat("evalInnerLogic() called with logicStr '{0}'", logicStr);

            int len = logicStr.Length;
            int ndx = 0;
            int opNdx = 0;
            string op1;

            bool logic1;

            char[] delim = { '&', ',' };
            bool logicState = true;  //logic state and prevOp preloaded to make first round run smoothly
            char prevOp = '&';
            char currOp = '0';

            while (ndx < len)
            {
                //get first operand and operator
                opNdx = logicStr.IndexOfAny(delim, ndx);
                Log.DebugFormat("First operator found at index {0}", opNdx);

                //each loop, we find next boolean operator (if one exists)
                //then parse the operand
                //then evaluate: (logicState [prev_operator} currOperand)
                //set logic state to this value
                if (opNdx == -1)
                {
                    Log.Debug("No more operators, so operand is the rest of string");

                    op1 = logicStr.Substring(ndx, len - ndx);
                    Log.DebugFormat("Operand op1 set to '{0}'", op1);

                    ndx = len;
                    Log.DebugFormat("ndx set to {0}", len);
                }
                else
                {
                    Log.Debug("another operator exists, so parse operand before it");
                    op1 = logicStr.Substring(ndx, opNdx - ndx);
                    Log.DebugFormat("Operand is '{0}'", op1);

                    //set ndx and prevOp for next step
                    ndx = opNdx + 1;
                    Log.DebugFormat("ndx set to {0}", ndx);

                    currOp = logicStr[opNdx]; //just take one character
                    Log.DebugFormat("currOp set to '{0}'", currOp);
                }


                //check for "not" logic
                //could clean up a bit if xor function exists for "not"-ing
                if (op1[0].CompareTo('!') == 0)
                {
                    Log.DebugFormat("op1[0] == '!'. Negative logic detected.");
                    op1 = op1.Remove(0, 1);

                    logic1 = !((conditionProcessor(op1)) || (op1.ToLower() == "true"));
                    Log.DebugFormat("logic1 set to '{0}'", logic1);
                }
                else
                {
                    logic1 = conditionProcessor(op1) || op1.ToLower() == "true";
                    Log.DebugFormat("logic1 set to '{0}'", logic1);
                }

                Log.Debug("evaluate logic and store in logicState");
                if (prevOp == '&')
                {
                    Log.DebugFormat("logicState is currently '{0}'", logicState);
                    Log.Debug("evaluating 'and' operation");
                    logicState = logicState && logic1;
                    Log.DebugFormat("logicState is now '{0}'", logicState);

                    prevOp = currOp; //pass the torch to prevOp
                }
                else if (prevOp == ',')
                {
                    Log.DebugFormat("logicState is currently '{0}'", logicState);
                    Log.Debug("evaluating 'or' operation");
                    logicState = logicState || logic1;
                    Log.DebugFormat("logicState is now '{0}'", logicState);

                    prevOp = currOp; //pass the torch to prevOp
                }
            }

            Log.DebugFormat("Returning value '{0}'", logicState);
            return logicState;
        }
    }
}