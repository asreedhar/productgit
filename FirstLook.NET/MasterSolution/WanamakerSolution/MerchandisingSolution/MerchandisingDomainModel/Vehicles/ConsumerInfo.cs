using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using FirstLook.Common.Data;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for ConsumerInfo
    /// </summary>
    public enum ConsumerInfoMap
    {
        DriverFrontCrash = 1,
        DriverSideCrash = 2,
        PassengerFrontCrash = 3,
        PassengerSideCrash = 4,
        RolloverRating = 5,
        BasicWarranty = 6,
        DrivetrainWarranty = 7,
        ExtendedWarranty = 8,
        FuelHwy = 9,
        FuelCity = 10,
        CorrosionWarranty = 11,
        RoadsideAssistance = 12
    }

    [Serializable]
    public class ConsumerInfo
    {
        private readonly string _fullText;
        private readonly Hashtable _attribHash;
        private readonly WarrantyCollection _warranties = new WarrantyCollection();

        private readonly List<WarrantyTypes> _warrantiesOfInterest =
            new List<WarrantyTypes>(new[]
                                        {
                                            WarrantyTypes.BasicWarranty, WarrantyTypes.ExtendedWarranty,
                                            WarrantyTypes.DrivetrainWarranty
                                        });

        public ConsumerInfoMap Category;

        private const string LONG_RATING_FORMAT_STRING = "{0} Star {1} Rating";

        public ConsumerInfo(IDataReader reader)
        {
            _attribHash = new Hashtable();
            while (reader.Read())
            {
                _fullText = (string) reader["Text"];

                switch (Convert.ToInt32(reader["TypeID"]))
                {
                    case 1: //Rebate
                        break;

                    case 2: //Warranty
                        _warranties = new WarrantyCollection(_fullText);
                        break;

                    case 4: //News: Crash Test Results
                        ParseTestRatings(_fullText);
                        break;
                    case 5: //News: JD Powers Ratings
                        break;
                    case 8: //News: Reliability Ratings
                        break;
                    case 11: //News: Reviews
                        break;
                    case 12: //Highlights
                        break;
                }
            }
        }

        public ConsumerInfo(string fullText)
        {
            _fullText = fullText;
        }

        public int DriverFrontCrashRating
        {
            get { return GetValue(ConsumerInfoMap.DriverFrontCrash); }
        }

        public string DriverFrontCrash
        {
            get
            {
                int val = DriverFrontCrashRating;
                return val > 0 ? string.Format(LONG_RATING_FORMAT_STRING, val, "Driver Front Crash") : string.Empty;
            }
        }

        public int DriverSideCrashRating
        {
            get { return GetValue(ConsumerInfoMap.DriverSideCrash); }
        }

        public string DriverSideCrash
        {
            get
            {
                int val = DriverSideCrashRating;
                return val > 0 ? string.Format(LONG_RATING_FORMAT_STRING, val, "Driver Side Crash") : string.Empty;
            }
        }

        public int PassengerFrontCrashRating
        {
            get { return GetValue(ConsumerInfoMap.PassengerFrontCrash); }
        }

        public string PassengerFrontCrash
        {
            get
            {
                int val = PassengerFrontCrashRating;
                return val > 0 ? string.Format(LONG_RATING_FORMAT_STRING, val, "Passenger Front Crash") : string.Empty;
            }
        }

        public int PassengerSideCrashRating
        {
            get { return GetValue(ConsumerInfoMap.PassengerSideCrash); }
        }

        public string PassengerSideCrash
        {
            get
            {
                int val = PassengerSideCrashRating;
                return val > 0 ? string.Format(LONG_RATING_FORMAT_STRING, val, "Passenger Side Crash") : string.Empty;
            }
        }

        public int RolloverRating
        {
            get { return GetValue(ConsumerInfoMap.RolloverRating); }
        }

        public string RolloverRatingText
        {
            get
            {
                int val = RolloverRating;
                return val > 0 ? string.Format(LONG_RATING_FORMAT_STRING, val, "Rollover") : string.Empty;
            }
        }


        public int FuelHwyInt { get; private set; }

        public string FuelHwy
        {
            get
            {
                int val = GetValue(ConsumerInfoMap.FuelHwy);
                return val > 0 ? val + " MPG Hwy" : string.Empty;
            }
        }

        public int FuelCityInt { get; private set; }

        public string FuelCity
        {
            get
            {
                int val = GetValue(ConsumerInfoMap.FuelCity);
                if (val > 0)
                {
                    return val + " MPG City";
                }
                return string.Empty;
            }
        }

        public int GetCrashOrRolloverRating(ConsumerInfoMap desiredType)
        {
            if (!_attribHash.Contains(desiredType))
            {
                return -1;
            }

            //we already know if contains the field at this point...now check to see if a rating
            if (desiredType == ConsumerInfoMap.DriverFrontCrash || 
                desiredType == ConsumerInfoMap.DriverSideCrash ||
                desiredType == ConsumerInfoMap.PassengerFrontCrash ||
                desiredType == ConsumerInfoMap.PassengerSideCrash ||
                desiredType == ConsumerInfoMap.RolloverRating)
            {
                return (int) _attribHash[desiredType];
            }
            
            throw new ArgumentException(
                @"Must be a rollover or crash test rating type in ConsumerInfoMap enumeration", "desiredType");
        }

        public int GetFuelEconomy(ConsumerInfoMap desiredType)
        {
            switch (desiredType)
            {
                case ConsumerInfoMap.FuelHwy:
                    return FuelHwyInt;
                case ConsumerInfoMap.FuelCity:
                    return FuelCityInt;
            }

            throw (new ArgumentException(@"Must be a fuel economy type in ConsumerInfoMap enumeration", "desiredType"));
        }

        public Pair<int, int> ParseSingleTestRatingSet(string testRating)
        {
            int ndxPs = testRating.IndexOf("Passenger");
            int firstRating; //could be driver or rollover
            int passengerRating = -1;

            if (ndxPs == -1)
            {
                //only has driver ratings
                firstRating = StringHelper.CountOccurencesOfChar(testRating, '*');
            }
            else
            {
                firstRating = StringHelper.CountOccurencesOfChar(testRating.Substring(0, ndxPs), '*');


                passengerRating = StringHelper.CountOccurencesOfChar(testRating.Substring(ndxPs), '*');
            }
            return new Pair<int, int>(firstRating, passengerRating);
        }

        public void ParseTestRatings(string testRating)
        {
            //get all the indices based on format of consumer info text string...
            int ndxFr = _fullText.ToLower().IndexOf("frontal crash ratings:");
            int ndxSi = _fullText.ToLower().IndexOf("side crash ratings:");
            int ndxRo = _fullText.ToLower().IndexOf("nhtsa rollover resistance rating:");
            int ndxEnd = _fullText.ToLower().IndexOf("the rollover resistance rating is an estimate ");

            //parse string from frontal crash rating through to the beginning of side crash
            if (ndxFr >= 0 && ndxSi > ndxFr)
            {
                string frontRatingStr = _fullText.Substring(ndxFr, ndxSi - ndxFr);
                Pair<int, int> frontRatings = ParseSingleTestRatingSet(frontRatingStr);
                if (frontRatings.First > -1 && !_attribHash.ContainsKey(ConsumerInfoMap.DriverFrontCrash))
                {
                    _attribHash.Add(ConsumerInfoMap.DriverFrontCrash, frontRatings.First);
                }
                if (frontRatings.Second > -1 && !_attribHash.ContainsKey(ConsumerInfoMap.PassengerFrontCrash))
                {
                    _attribHash.Add(ConsumerInfoMap.PassengerFrontCrash, frontRatings.Second);
                }
            }

            if (ndxSi >= 0 && ndxRo > ndxSi)
            {
                string sideRatingStr = _fullText.Substring(ndxSi, ndxRo - ndxSi);
                Pair<int, int> sideRatings = ParseSingleTestRatingSet(sideRatingStr);
                if (sideRatings.First > -1 && !_attribHash.ContainsKey(ConsumerInfoMap.DriverSideCrash))
                {
                    _attribHash.Add(ConsumerInfoMap.DriverSideCrash, sideRatings.First);
                }
                if (sideRatings.Second > -1 && !_attribHash.ContainsKey(ConsumerInfoMap.PassengerSideCrash))
                {
                    _attribHash.Add(ConsumerInfoMap.PassengerSideCrash, sideRatings.Second);
                }
            }

            if (ndxRo >= 0 && ndxEnd > ndxRo)
            {
                string rollRatingStr = _fullText.Substring(ndxRo, ndxEnd - ndxRo);
                Pair<int, int> rollRating = ParseSingleTestRatingSet(rollRatingStr);
                if (rollRating.First > -1 && !_attribHash.ContainsKey(ConsumerInfoMap.RolloverRating))
                {
                    _attribHash.Add(ConsumerInfoMap.RolloverRating, rollRating.First);
                }
            }
        }

        //test rating should be the string containing the driver and passenger ratings
        //if no passenger rating exists, only one will be returned
        //titleText is the heading text to be applied before the Passenger/Driver subtext
        public ListDictionary ParseTestRatings(string testRating, string titleText)
        {
            int ndxDri = testRating.IndexOf("Passenger");
            var ratings = new ListDictionary();

            if (ndxDri == -1)
            {
                //no passenger text, pull header from string?
                if (testRating.IndexOf("Driver") > -1)
                {
                    titleText += " Driver";
                }

                int singleRating = StringHelper.CountOccurencesOfChar(testRating, '*');
                ratings.Add(titleText + ": ", singleRating);
            }
            else
            {
                int driverRating = StringHelper.CountOccurencesOfChar(testRating.Substring(0, ndxDri), '*');
                ratings.Add(titleText + " Driver: ", driverRating);

                int passengerRating = StringHelper.CountOccurencesOfChar(testRating.Substring(ndxDri), '*');
                ratings.Add(titleText + " Passenger: ", passengerRating);
            }

            return ratings;
        }


        public void LoadFuelData(int chromeStyleID, EquipmentCollection selectedEquipment)
        {
            //now get fuel economy
            FuelEconomySelect(chromeStyleID, selectedEquipment);


            if (FuelCityInt <= 0 && FuelHwyInt <= 0)
            {
                //try the equipment now...
                var equipmentCollection = EquipmentCollection.FuelEconomyEquipmentSelect(chromeStyleID);
                
                //example formats
                //1) City 20/hwy 27 (3.0L engine/4-speed auto trans) (Estimated)
                //2) City 19/hwy 24
                //3) EPA mileage - city 17/hwy 26 (5.0L STD engine/5-spd manual trans)
                //4) 16 city/21 hwy
                //5) EPA mileage - 16 city/21 hwy (5-spd manual 4-spd auto trans)
                
                foreach (Equipment equipment in equipmentCollection)
                {
                    var gasMileage = GasMileageParser.Parse(equipment.Description);

                    FuelCityInt = gasMileage.City;
                    FuelHwyInt = gasMileage.Highway;

                    // Case : 18661 - Error screen when attempting click in to make-model in MaxAd

                    //take worst for now? <-- original comment before refactoring

                    //The original code had the above comment, however, it did not actually do anything to try and figure out what the worst gas mileage was.
                    // It just iterated through the list and ended up picking the last value.  That behavior was maintained, but if necessary the logic can be added to pick the worst.
                }
            }
        }


        //returns pair (CITY, HIGHWAY) of ints that represent the miles per gallon of the vehicle
        //selected equipment should contain the equipment with the selected property set to true for all selected equipment
        public void FuelEconomySelect(int chromeStyleID, EquipmentCollection selectedEquipment)
        {
            if (selectedEquipment == null)
            {
                FuelCityInt = -1;
                FuelHwyInt = -1;
            }

            using (IDataConnection con = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText =
                        @"SELECT TitleID, Text, Condition
                            FROM VehicleCatalog.Chrome.TechSpecs
                            WHERE StyleID = @ChromeStyleID AND (TitleID IN (26, 27)) AND CountryCode = 1
                            ORDER BY sequence";
                    cmd.CommandType = CommandType.Text;

                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        bool cityFound = false;
                        bool hwyFound = false;

                        //while we have data and we haven't found both values
                        while (reader.Read() && !(hwyFound && cityFound))
                        {
                            if (!cityFound && Convert.ToInt32(reader["TitleID"]) == 26)
                            {
                                //test the logic to see if this one applies to our vehicle's equipment
                                if (ChromeLogicEngine.EvalLogic(Convert.ToString(reader["condition"]),
                                                                selectedEquipment))
                                {
                                    cityFound = true;
                                    try
                                    {
                                        FuelCityInt = Convert.ToInt32(reader["Text"]);
                                    }
                                    catch
                                    {
                                        FuelCityInt = -1;
                                        //invalid value, TBD, or empty because chrome doesn't know yet
                                    }
                                }
                            }
                            else if (!hwyFound && Convert.ToInt32(reader["TitleID"]) == 27) 
                            {
                                //test the logic to see if this one applies to our vehicle's equipment
                                if (ChromeLogicEngine.EvalLogic(Convert.ToString(reader["condition"]),
                                                                selectedEquipment))
                                {
                                    hwyFound = true;
                                    try
                                    {
                                        FuelHwyInt = Convert.ToInt32(reader["Text"]);
                                    }
                                    catch
                                    {
                                        FuelHwyInt = -1;
                                        //invalid value, TBD, or empty because chrome doesn't know yet
                                    }
                                }
                            }
                        }

                        return;
                    }
                }
            }
        }

        public static ConsumerInfo Fetch(int chromeStyleID, List<int> selectedCategoryIds, DetailedEquipmentCollection packages)
        {
            ConsumerInfo cinfo;
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getConsumerInfo";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        cinfo = new ConsumerInfo(reader);
                    }
                }
            }

            EquipmentCollection availableOptEquipment = EquipmentCollection.FetchOptional(chromeStyleID, false, -1.0M);
            availableOptEquipment.SetSelected(selectedCategoryIds);

            //If they have the package it should be selected.
            foreach(Equipment eq in availableOptEquipment)
            {
                if (packages.ContainsOption(eq.OptionCode))
                    eq.Selected = true;
            }

            cinfo.LoadFuelData(chromeStyleID, availableOptEquipment);
            return cinfo;
        }

        public bool HasGoodCrashRatings(int minGoodCrashRating)
        {
            if (GetCrashOrRolloverRating(ConsumerInfoMap.DriverFrontCrash) >= minGoodCrashRating ||
                GetCrashOrRolloverRating(ConsumerInfoMap.DriverSideCrash) >= minGoodCrashRating ||
                GetCrashOrRolloverRating(ConsumerInfoMap.RolloverRating) >= minGoodCrashRating ||
                GetCrashOrRolloverRating(ConsumerInfoMap.PassengerFrontCrash) >= minGoodCrashRating ||
                GetCrashOrRolloverRating(ConsumerInfoMap.PassengerSideCrash) >= minGoodCrashRating)
            {
                return true;
            }
            return false;
        }

        public bool AreBothSideCrashTestsGood(int minGoodCrashRating)
        {
            return GetCrashOrRolloverRating(ConsumerInfoMap.DriverSideCrash) >= minGoodCrashRating
                   && GetCrashOrRolloverRating(ConsumerInfoMap.PassengerSideCrash) >= minGoodCrashRating;
        }

        public bool AreBothFrontCrashTestsGood(int minGoodCrashRating)
        {
            return GetCrashOrRolloverRating(ConsumerInfoMap.DriverFrontCrash) >= minGoodCrashRating
                   && GetCrashOrRolloverRating(ConsumerInfoMap.PassengerFrontCrash) >= minGoodCrashRating;
        }

        public bool AreBothDriverCrashTestsGood(int minGoodCrashRating)
        {
            return GetCrashOrRolloverRating(ConsumerInfoMap.DriverFrontCrash) >= minGoodCrashRating
                   && GetCrashOrRolloverRating(ConsumerInfoMap.DriverSideCrash) >= minGoodCrashRating;
        }

        public bool AreBothPassengerCrashTestsGood(int minGoodCrashRating)
        {
            return GetCrashOrRolloverRating(ConsumerInfoMap.PassengerFrontCrash) >= minGoodCrashRating
                   && GetCrashOrRolloverRating(ConsumerInfoMap.PassengerSideCrash) >= minGoodCrashRating;
        }

        public string GetGoodCrashRatingText(int minGoodCrashRating, int maxCountToDisplay)
        {
            return GetGoodCrashRatingText(minGoodCrashRating, maxCountToDisplay, false, true);
        }

        public string GetGoodCrashRatingText(int minGoodCrashRating, int maxCountToDisplay, bool showSummaries,
                                             bool showStarRatings)
        {
            var st = new StringBuilder();

            int ct = 0;
            bool bothFronts = AreBothFrontCrashTestsGood(minGoodCrashRating);
            bool bothSides = AreBothSideCrashTestsGood(minGoodCrashRating);
            if (showSummaries && bothFronts && bothSides)
            {
                st.Append("Great front and side crash test ratings.  ");
                ct++;
            }
            else if (showSummaries && bothFronts)
            {
                st.Append("Great front crash test ratings.  ");
                ct++;
            }
            else if (showSummaries && bothSides)
            {
                st.Append("Great side crash test ratings.  ");
                ct++;
            }
            else if (showStarRatings)
            {
                if (ct < maxCountToDisplay &&
                    GetCrashOrRolloverRating(ConsumerInfoMap.DriverFrontCrash) >= minGoodCrashRating)
                {
                    st.Append(DriverFrontCrash + ".  ");
                    ct++;
                }
                if (ct < maxCountToDisplay &&
                    GetCrashOrRolloverRating(ConsumerInfoMap.DriverSideCrash) >= minGoodCrashRating)
                {
                    st.Append(DriverSideCrash + ".  ");
                    ct++;
                }

                if (ct < maxCountToDisplay &&
                    GetCrashOrRolloverRating(ConsumerInfoMap.PassengerFrontCrash) >= minGoodCrashRating)
                {
                    st.Append(PassengerFrontCrash + ".  ");
                    ct++;
                }

                if (ct < maxCountToDisplay &&
                    GetCrashOrRolloverRating(ConsumerInfoMap.PassengerSideCrash) >= minGoodCrashRating)
                {
                    st.Append(PassengerSideCrash + ".  ");
                    ct++;
                }
            }

            if (showStarRatings && ct < maxCountToDisplay &&
                GetCrashOrRolloverRating(ConsumerInfoMap.RolloverRating) >= minGoodCrashRating)
            {
                st.Append(RolloverRatingText + ".  ");                
            }

            return st.ToString();
        }


        public int GetValue(ConsumerInfoMap ruleParameter)
        {
            switch (ruleParameter)
            {
                case ConsumerInfoMap.DriverFrontCrash:
                    return GetCrashOrRolloverRating(ConsumerInfoMap.DriverFrontCrash);

                case ConsumerInfoMap.PassengerFrontCrash:
                    return GetCrashOrRolloverRating(ConsumerInfoMap.PassengerFrontCrash);

                case ConsumerInfoMap.DriverSideCrash:
                    return GetCrashOrRolloverRating(ConsumerInfoMap.DriverSideCrash);

                case ConsumerInfoMap.PassengerSideCrash:
                    return GetCrashOrRolloverRating(ConsumerInfoMap.PassengerSideCrash);

                case ConsumerInfoMap.FuelCity:
                    return FuelCityInt;
                case ConsumerInfoMap.FuelHwy:
                    return FuelHwyInt;

                case ConsumerInfoMap.RolloverRating:

                    return GetCrashOrRolloverRating(ConsumerInfoMap.RolloverRating);
                default:
                    throw new ApplicationException("Not implemented");
            } 
        }

        #region Warranty

        public string BasicWarranty
        {
            get { return GetWarrantyInfo(WarrantyTypes.BasicWarranty); }
        }

        public string DrivetrainWarranty
        {
            get { return GetWarrantyInfo(WarrantyTypes.DrivetrainWarranty); }
        }

        public string ExtendedWarranty
        {
            get { return GetWarrantyInfo(WarrantyTypes.ExtendedWarranty); }
        }

        public string CorrosionWarranty
        {
            get { return GetWarrantyInfo(WarrantyTypes.CorrosionWarranty); }
        }

        public string RoadsideAssistance
        {
            get { return GetWarrantyInfo(WarrantyTypes.RoadsideAssistance); }
        }

        public string GetWarrantyInfo(WarrantyTypes desiredType)
        {
            return _warranties.Contains(desiredType) ? _warranties.GetByType(desiredType).ToString(0) : string.Empty;
        }

        public bool HasGoodWarrantyRemaining(int minMileageLimit, int currentMileage, DateTime inServiceDate)
        {
            return _warranties.HasGoodWarrantyRemaining(minMileageLimit, currentMileage, inServiceDate,
                                                       _warrantiesOfInterest);
        }

        public bool HasRoadsideAssistanceRemaining(int minMileageLimit, int currentMileage, DateTime inServiceDate)
        {
            var warrantyTypes = new List<WarrantyTypes>();
            warrantyTypes.Add(WarrantyTypes.RoadsideAssistance);
            return _warranties.HasGoodWarrantyRemaining(minMileageLimit, currentMileage, inServiceDate, warrantyTypes);
        }

        public string GetGoodWarrantyDescriptions(int minMileageLimit, int currentMileage, DateTime inServiceDate,
                                                  int maxCt)
        {
            return _warranties.GetGoodWarrantyDescriptions(minMileageLimit, currentMileage, inServiceDate, maxCt,
                                                          _warrantiesOfInterest);
        }

        #endregion
    }

    public class StringHelper
    {
        public static int CountOccurencesOfChar(string instance, char c)
        {
            int result = 0;
            foreach (char curChar in instance)
            {
                if (c == curChar)
                {
                    ++result;
                }
            }
            return result;
        }
    }
}