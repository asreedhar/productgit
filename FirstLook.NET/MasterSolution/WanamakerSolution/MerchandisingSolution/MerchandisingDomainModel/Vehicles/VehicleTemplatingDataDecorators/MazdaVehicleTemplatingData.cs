﻿using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews;

namespace FirstLook.Merchandising.DomainModel.Vehicles.VehicleTemplatingDataDecorators
{
    public class MazdaVehicleTemplatingData : VehicleTemplatingDataDecorator
    {
        /** AS - 2015-04-1
         *  The following comment was copied over from LincolnVehicleTemplatingData because it is !IMPORTANT! 
         **/

            /* TH - 2013-01-1-29
             *  The following comment was copied over from VehicleTemplatingData because it is !IMPORTANT! 
             */

            /* This class is used through reflection to read property values when the ad is constructed. Any property with
             * a comment above it like $base.EngineDetails$ means that property is called through reflection. The MerchandisingPoints table
             * contains these strings. Do not change the property names. Even if it doesn't have a comment above the property think 
             * twice before changing the name.
         */
        public MazdaVehicleTemplatingData(IVehicleTemplatingData vehicleTemplatingData)
            : base(vehicleTemplatingData)
        {
        }

        public override string Trim
        {
            get
            {
                var customTrim = VehicleConfig.AfterMarketTrim;
                var invTrim = Model + ' ' + InventoryItem.Trim;
                var retTrim = string.IsNullOrWhiteSpace(customTrim) ? invTrim.Trim() : customTrim;
                if (!string.IsNullOrEmpty(retTrim))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Trim);
                }

                return retTrim;
            }
        }
    }
}