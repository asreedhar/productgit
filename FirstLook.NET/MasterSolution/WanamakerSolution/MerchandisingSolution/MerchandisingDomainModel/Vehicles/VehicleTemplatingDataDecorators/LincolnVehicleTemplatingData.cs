﻿using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews;

namespace FirstLook.Merchandising.DomainModel.Vehicles.VehicleTemplatingDataDecorators
{
    public class LincolnVehicleTemplatingData : VehicleTemplatingDataDecorator
    {
        /** TH - 2013-01-1-29
         *  The following comment was copied over from VehicleTemplatingData because it is !IMPORTANT! 
         **/

        /* This class is used through reflection to read property values when the ad is constructed. Any property with
         * a comment above it like $base.EngineDetails$ means that property is called through reflection. The MerchandisingPoints table
         * contains these strings. Do not change the property names. Even if it doesn't have a comment above the property think 
         * twice before changing the name.
         */
        public LincolnVehicleTemplatingData(IVehicleTemplatingData vehicleTemplatingData) 
            : base(vehicleTemplatingData)
        {
        }

        // need to use "new" to override existing properties. Instead of using "override" as you would on a method
        public override bool HasTrim
        {
            get
            {
                return (InventoryItem.Trim.Trim().Length +
                        VehicleConfig.AfterMarketTrim.Trim().Length) > 0;
            }
        }

        public override string Trim
        {
            get
            {
                var customTrim = VehicleConfig.AfterMarketTrim;
                var invTrim = InventoryItem.Trim;
                var retTrim = string.IsNullOrWhiteSpace(customTrim) ? invTrim.Trim() : customTrim;
                if (!string.IsNullOrEmpty(retTrim))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Trim);
                }

                return retTrim;
            }
        }
    }
}