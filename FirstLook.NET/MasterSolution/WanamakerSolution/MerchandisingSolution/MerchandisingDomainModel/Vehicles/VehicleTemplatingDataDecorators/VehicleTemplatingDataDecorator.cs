﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.MarketingData;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews;

namespace FirstLook.Merchandising.DomainModel.Vehicles.VehicleTemplatingDataDecorators
{
    /** TH - 2013-01-1-29
     *  The following comment was copied over from VehicleTemplatingData because it is !IMPORTANT! 
     **/

    /* This class is used through reflection to read property values when the ad is constructed. Any property with
     * a comment above it like $base.EngineDetails$ means that property is called through reflection. The MerchandisingPoints table
     * contains these strings. Do not change the property names. Even if it doesn't have a comment above the property think 
     * twice before changing the name.
     */
    public class VehicleTemplatingDataDecorator : IVehicleTemplatingData
    {
        private readonly IVehicleTemplatingData _vehicleTemplatingData;

        public VehicleTemplatingDataDecorator(IVehicleTemplatingData vehicleTemplatingData)
        {
            _vehicleTemplatingData = vehicleTemplatingData;
        }

        /**
         * Re-Sharper implemented the wrapped calls to the IVehicleData interface
         * I love you Re-Sharper, you just saved me tons of tedious typing.
         **/

        public virtual string HighInternetPrice
        {
            get { return _vehicleTemplatingData.HighInternetPrice; }
        }

        public virtual bool IsPriceReduced
        {
            get { return _vehicleTemplatingData.IsPriceReduced; }
        }

        public virtual bool IsPriceReducedEnough
        {
            get { return _vehicleTemplatingData.IsPriceReducedEnough; }
        }

        public virtual bool HasDisplayablePrice
        {
            get { return _vehicleTemplatingData.HasDisplayablePrice; }
        }

        public virtual string ListPrice
        {
            get { return _vehicleTemplatingData.ListPrice; }
        }

        public virtual bool HasGoodPriceBelowMSRP
        {
            get { return _vehicleTemplatingData.HasGoodPriceBelowMSRP; }
        }

        public virtual string OriginalMsrp
        {
            get { return _vehicleTemplatingData.OriginalMsrp; }
        }

        public virtual bool HasFinancingAvailable
        {
            get { return _vehicleTemplatingData.HasFinancingAvailable; }
        }

        public virtual string SpecialFinancing
        {
            get { return _vehicleTemplatingData.SpecialFinancing; }
        }

        public virtual decimal PreferredBookValue
        {
            get { return _vehicleTemplatingData.PreferredBookValue; }
        }

        public virtual bool HasGoodPriceBelowBook
        {
            get { return _vehicleTemplatingData.HasGoodPriceBelowBook; }
        }

        public virtual BookoutCollection Bookouts
        {
            get { return _vehicleTemplatingData.Bookouts; }
        }

        public virtual string PreferredBook
        {
            get { return _vehicleTemplatingData.PreferredBook; }
        }

        public virtual string TargetPriceDisplay
        {
            get { return _vehicleTemplatingData.TargetPriceDisplay; }
        }

        public virtual decimal TargetPrice
        {
            get { return _vehicleTemplatingData.TargetPrice; }
        }

        public virtual int CountBelowBook
        {
            get { return _vehicleTemplatingData.CountBelowBook; }
        }

        public virtual int CountInPriceRange
        {
            get { return _vehicleTemplatingData.CountInPriceRange; }
        }

        public virtual int CountBelowTwentyThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowTwentyThousandDollars; }
        }

        public virtual int CountBelowFifteenThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowFifteenThousandDollars; }
        }

        public virtual int CountBelowTenThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowTenThousandDollars; }
        }

        public virtual int CountBelowNineThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowNineThousandDollars; }
        }

        public virtual int CountBelowEightThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowEightThousandDollars; }
        }

        public virtual int CountBelowSevenThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowSevenThousandDollars; }
        }

        public virtual string DisclaimerDate
        {
            get { return _vehicleTemplatingData.DisclaimerDate; }
        }

        public virtual DateTime? ExpirationDate
        {
            get { return _vehicleTemplatingData.ExpirationDate; }
            set { _vehicleTemplatingData.ExpirationDate = value; }
        }

        public virtual VehicleAttributes VehicleAttributes
        {
            get { return _vehicleTemplatingData.VehicleAttributes; }
            set { _vehicleTemplatingData.VehicleAttributes = value; }
        }

        public virtual IVehicleConfiguration VehicleConfig
        {
            get { return _vehicleTemplatingData.VehicleConfig; }
            set { _vehicleTemplatingData.VehicleConfig = value; }
        }

        public virtual ITemplateMediator Mediator
        {
            get { return _vehicleTemplatingData.Mediator; }
            set { _vehicleTemplatingData.Mediator = value; }
        }

        public virtual IInventoryData InventoryItem
        {
            get { return _vehicleTemplatingData.InventoryItem; }
            set { _vehicleTemplatingData.InventoryItem = value; }
        }

        public virtual DealerAdvertisementPreferences Preferences
        {
            get { return _vehicleTemplatingData.Preferences; }
            set { _vehicleTemplatingData.Preferences = value; }
        }

        public virtual bool IsLowMilesPerYear
        {
            get { return _vehicleTemplatingData.IsLowMilesPerYear; }
        }

        public virtual bool IsNewVehicle
        {
            get { return _vehicleTemplatingData.IsNewVehicle; }
        }

        public virtual bool HasInteriorType
        {
            get { return _vehicleTemplatingData.HasInteriorType; }
        }

        public virtual string MilesPerYearCategory
        {
            get { return _vehicleTemplatingData.MilesPerYearCategory; }
        }

        public virtual string Make
        {
            get { return _vehicleTemplatingData.Make; }
        }

        public virtual string Model
        {
            get { return _vehicleTemplatingData.Model; }
        }

        public virtual string Year
        {
            get { return _vehicleTemplatingData.Year; }
        }

        public virtual bool HasTrim
        {
            get { return _vehicleTemplatingData.HasTrim; }
        }

        public virtual string Trim
        {
            get { return _vehicleTemplatingData.Trim; }
        }

        public virtual string Mileage
        {
            get { return _vehicleTemplatingData.Mileage; }
        }

        public virtual bool HasColors
        {
            get { return _vehicleTemplatingData.HasColors; }
        }

        public virtual string InteriorType
        {
            get { return _vehicleTemplatingData.InteriorType; }
        }

        public virtual string Colors
        {
            get { return _vehicleTemplatingData.Colors; }
        }

        public virtual string InteriorColor
        {
            get { return _vehicleTemplatingData.InteriorColor; }
        }

        public virtual string ExteriorColor
        {
            get { return _vehicleTemplatingData.ExteriorColor; }
        }

        public virtual string StockNumber
        {
            get { return _vehicleTemplatingData.StockNumber; }
        }

        public virtual bool HasCertificationPreview
        {
            get { return _vehicleTemplatingData.HasCertificationPreview; }
        }

        public virtual string CertificationPreview
        {
            get { return _vehicleTemplatingData.CertificationPreview; }
        }

        public virtual bool HasCertificationDetails
        {
            get { return _vehicleTemplatingData.HasCertificationDetails; }
        }

        public virtual string CertificationDetails
        {
            get { return _vehicleTemplatingData.CertificationDetails; }
        }

        public virtual bool IsCertified
        {
            get { return _vehicleTemplatingData.IsCertified; }
        }

        public virtual string Certified
        {
            get { return _vehicleTemplatingData.Certified; }
        }

        public virtual string CertificationTitle
        {
            get { return _vehicleTemplatingData.CertificationTitle; }
        }

        public virtual string CertifiedID
        {
            get { return _vehicleTemplatingData.CertifiedID; }
        }

        public virtual string FuelCity
        {
            get { return _vehicleTemplatingData.FuelCity; }
        }

        public virtual string FuelHwy
        {
            get { return _vehicleTemplatingData.FuelHwy; }
        }

        public virtual bool GetsGoodGasMileage
        {
            get { return _vehicleTemplatingData.GetsGoodGasMileage; }
        }

        public virtual string BestGasMileage
        {
            get { return _vehicleTemplatingData.BestGasMileage; }
        }

        public virtual bool HasRoadsideAssistance
        {
            get { return _vehicleTemplatingData.HasRoadsideAssistance; }
        }

        public virtual string RoadsideAssistance
        {
            get { return _vehicleTemplatingData.RoadsideAssistance; }
        }

        public virtual ConsumerInfo ConsInfo
        {
            get { return _vehicleTemplatingData.ConsInfo; }
            set { _vehicleTemplatingData.ConsInfo = value; }
        }

        public virtual bool HasGoodCrashTestRatings
        {
            get { return _vehicleTemplatingData.HasGoodCrashTestRatings; }
        }

        public virtual string DriverFrontCrash
        {
            get { return _vehicleTemplatingData.DriverFrontCrash; }
        }

        public virtual string DriverSideCrash
        {
            get { return _vehicleTemplatingData.DriverSideCrash; }
        }

        public virtual string RolloverRating
        {
            get { return _vehicleTemplatingData.RolloverRating; }
        }

        public virtual string BasicWarranty
        {
            get { return _vehicleTemplatingData.BasicWarranty; }
        }

        public virtual string DrivetrainWarranty
        {
            get { return _vehicleTemplatingData.DrivetrainWarranty; }
        }

        public virtual CarfaxInterface CarfaxInfo
        {
            get { return _vehicleTemplatingData.CarfaxInfo; }
            set { _vehicleTemplatingData.CarfaxInfo = value; }
        }

        public virtual bool IsOneOwner
        {
            get { return _vehicleTemplatingData.IsOneOwner; }
        }

        public virtual bool IsCleanCarfaxReport
        {
            get { return _vehicleTemplatingData.IsCleanCarfaxReport; }
        }

        public virtual string CleanCarfaxReport
        {
            get { return _vehicleTemplatingData.CleanCarfaxReport; }
        }

        public virtual string Carfax1Owner
        {
            get { return _vehicleTemplatingData.Carfax1Owner; }
        }

        public virtual bool HasBuybackGuarantee
        {
            get { return _vehicleTemplatingData.HasBuybackGuarantee; }
        }

        public virtual string BuybackGuarantee
        {
            get { return _vehicleTemplatingData.BuybackGuarantee; }
        }

        public virtual AutoCheckInterface AutoCheckInfo
        {
            get { return _vehicleTemplatingData.AutoCheckInfo; }
        }

        public virtual bool IsAutoCheckOneOwner
        {
            get { return _vehicleTemplatingData.IsAutoCheckOneOwner; }
        }

        public virtual string AutoCheckOneOwner
        {
            get { return _vehicleTemplatingData.AutoCheckOneOwner; }
        }

        public virtual bool IsAutoCheckAssured
        {
            get { return _vehicleTemplatingData.IsAutoCheckAssured; }
        }

        public virtual string AutoCheckAssured
        {
            get { return _vehicleTemplatingData.AutoCheckAssured; }
        }

        public virtual bool IsCleanAutoCheckReport
        {
            get { return _vehicleTemplatingData.IsCleanAutoCheckReport; }
        }

        public virtual string CleanAutoCheckReport
        {
            get { return _vehicleTemplatingData.CleanAutoCheckReport; }
        }

        public virtual bool HasGoodAutoCheckScore
        {
            get { return _vehicleTemplatingData.HasGoodAutoCheckScore; }
        }

        public virtual string AutoCheckScore
        {
            get { return _vehicleTemplatingData.AutoCheckScore; }
        }

        public virtual bool HasHighlightedPackages
        {
            get { return _vehicleTemplatingData.HasHighlightedPackages; }
        }

        public virtual List<PreviewEquipmentItem> PreviewEquipment
        {
            get { return _vehicleTemplatingData.PreviewEquipment; }
        }

        public virtual bool HasMostSearchedEquipmentAvailable
        {
            get { return _vehicleTemplatingData.HasMostSearchedEquipmentAvailable; }
        }

        public virtual bool HasTier1EquipmentAvailable
        {
            get { return _vehicleTemplatingData.HasTier1EquipmentAvailable; }
        }

        public virtual bool HasEnoughTier1EquipmentAvailable
        {
            get { return _vehicleTemplatingData.HasEnoughTier1EquipmentAvailable; }
        }

        public virtual bool HasTier2EquipmentAvailable
        {
            get { return _vehicleTemplatingData.HasTier2EquipmentAvailable; }
        }

        public virtual bool HasEnoughTier2EquipmentAvailable
        {
            get { return _vehicleTemplatingData.HasEnoughTier2EquipmentAvailable; }
        }

        public virtual string AfterMarketEquipment
        {
            get { return _vehicleTemplatingData.AfterMarketEquipment; }
        }

        public virtual string Loaded
        {
            get { return _vehicleTemplatingData.Loaded; }
        }

        public virtual List<string> MajorEquipmentOfInterest
        {
            get { return _vehicleTemplatingData.MajorEquipmentOfInterest; }
        }

        public virtual bool HasValuedOptions
        {
            get { return _vehicleTemplatingData.HasValuedOptions; }
        }

        public virtual List<string> ValuedOptions
        {
            get { return _vehicleTemplatingData.ValuedOptions; }
        }

        public virtual bool HasTruckOptions
        {
            get { return _vehicleTemplatingData.HasTruckOptions; }
        }

        public virtual List<string> TruckOptions
        {
            get { return _vehicleTemplatingData.TruckOptions; }
        }

        public virtual bool HasSafetyOptions
        {
            get { return _vehicleTemplatingData.HasSafetyOptions; }
        }

        public virtual List<string> SafetyOptions
        {
            get { return _vehicleTemplatingData.SafetyOptions; }
        }

        public virtual bool HasConvenienceOptions
        {
            get { return _vehicleTemplatingData.HasConvenienceOptions; }
        }

        public virtual List<string> ConvenienceOptions
        {
            get { return _vehicleTemplatingData.ConvenienceOptions; }
        }

        public virtual bool HasLuxuryOptions
        {
            get { return _vehicleTemplatingData.HasLuxuryOptions; }
        }

        public virtual List<string> LuxuryOptions
        {
            get { return _vehicleTemplatingData.LuxuryOptions; }
        }

        public virtual bool HasSportyOptions
        {
            get { return _vehicleTemplatingData.HasSportyOptions; }
        }

        public virtual List<string> SportyOptions
        {
            get { return _vehicleTemplatingData.SportyOptions; }
        }

        public virtual bool HasThemedEquipment
        {
            get { return _vehicleTemplatingData.HasThemedEquipment; }
        }

        public virtual List<string> ThemedEquipment
        {
            get { return _vehicleTemplatingData.ThemedEquipment; }
        }

        public virtual List<string> HighlightedPackages
        {
            get { return _vehicleTemplatingData.HighlightedPackages; }
        }

        public virtual List<string> Tier1Equipment
        {
            get { return _vehicleTemplatingData.Tier1Equipment; }
        }

        public virtual List<string> Tier2Equipment
        {
            get { return _vehicleTemplatingData.Tier2Equipment; }
        }

        public virtual List<string> Tier3Equipment
        {
            get { return _vehicleTemplatingData.Tier3Equipment; }
        }

        public virtual string Engine
        {
            get { return _vehicleTemplatingData.Engine; }
        }

        public virtual string EngineOfInterest
        {
            get { return _vehicleTemplatingData.EngineOfInterest; }
        }

        public virtual string EngineDetails
        {
            get { return _vehicleTemplatingData.EngineDetails; }
        }

        public virtual string Horsepower
        {
            get { return _vehicleTemplatingData.Horsepower; }
        }

        public virtual string Drivetrain
        {
            get { return _vehicleTemplatingData.Drivetrain; }
        }

        public virtual string DrivetrainOfInterest
        {
            get { return _vehicleTemplatingData.DrivetrainOfInterest; }
        }

        public virtual string Transmission
        {
            get { return _vehicleTemplatingData.Transmission; }
        }

        public virtual string TransmissionOfInterest
        {
            get { return _vehicleTemplatingData.TransmissionOfInterest; }
        }

        public virtual string FuelType
        {
            get { return _vehicleTemplatingData.FuelType; }
        }

        public virtual string FuelTypeOfInterest
        {
            get { return _vehicleTemplatingData.FuelTypeOfInterest; }
        }

        public virtual ModelMarketingCollection MarketingCollection
        {
            get { return _vehicleTemplatingData.MarketingCollection; }
        }

        public virtual string BestModelAward
        {
            get { return _vehicleTemplatingData.BestModelAward; }
        }

        public virtual bool HasAwards
        {
            get { return _vehicleTemplatingData.HasAwards; }
        }

        public virtual string ModelAwards
        {
            get { return _vehicleTemplatingData.ModelAwards; }
        }

        public virtual string MarketingPreview
        {
            get { return _vehicleTemplatingData.MarketingPreview; }
        }

        public virtual string MarketingSuperlative
        {
            get { return _vehicleTemplatingData.MarketingSuperlative; }
        }

        public virtual string MarketingSentence
        {
            get { return _vehicleTemplatingData.MarketingSentence; }
        }

        public virtual string MarketingSummary
        {
            get { return _vehicleTemplatingData.MarketingSummary; }
        }

        public virtual MarketingDataCollection MarketingList
        {
            get { return _vehicleTemplatingData.MarketingList; }
        }

        public virtual bool HasMarketingPreviewSuperlative
        {
            get { return _vehicleTemplatingData.HasMarketingPreviewSuperlative; }
        }

        public virtual bool HasMarketingSuperlative
        {
            get { return _vehicleTemplatingData.HasMarketingSuperlative; }
        }

        public virtual bool HasMarketingData
        {
            get { return _vehicleTemplatingData.HasMarketingData; }
        }

        public virtual JdPowerRatingCollection JdPowerRatings
        {
            get { return _vehicleTemplatingData.JdPowerRatings; }
        }

        public virtual bool HasGoodJdPowerRatings
        {
            get { return _vehicleTemplatingData.HasGoodJdPowerRatings; }
        }

        public virtual string GoodJdPowerRatings
        {
            get { return _vehicleTemplatingData.GoodJdPowerRatings; }
        }

        public virtual string JdPowerRatingPreview
        {
            get { return _vehicleTemplatingData.JdPowerRatingPreview; }
        }

        public virtual bool HasConditionHighlights
        {
            get { return _vehicleTemplatingData.HasConditionHighlights; }
        }

        public virtual bool HasCondition
        {
            get { return _vehicleTemplatingData.HasCondition; }
        }

        public virtual string ConditionSentence
        {
            get { return _vehicleTemplatingData.ConditionSentence; }
        }

        public virtual string ConditionSummary
        {
            get { return _vehicleTemplatingData.ConditionSummary; }
        }

        public virtual bool HasConditionDetails
        {
            get { return _vehicleTemplatingData.HasConditionDetails; }
        }

        public virtual string PreviewHighlights
        {
            get { return _vehicleTemplatingData.PreviewHighlights; }
        }

        public virtual decimal ReconditioningValue
        {
            get { return _vehicleTemplatingData.ReconditioningValue; }
        }

        public virtual string ReconditioningText
        {
            get { return _vehicleTemplatingData.ReconditioningText; }
        }

        public virtual bool HasKeyInformation
        {
            get { return _vehicleTemplatingData.HasKeyInformation; }
        }

        public virtual string AdditionalInformation
        {
            get { return _vehicleTemplatingData.AdditionalInformation; }
        }

        public virtual bool HasMissionPreviewStatment
        {
            get { return _vehicleTemplatingData.HasMissionPreviewStatment; }
        }

        public virtual string MissionPreviewStatement
        {
            get { return _vehicleTemplatingData.MissionPreviewStatement; }
        }

        public virtual IPreviewPreferences UsedPreviewPreferences
        {
            get { return _vehicleTemplatingData.UsedPreviewPreferences; }
        }

        public virtual IPreviewPreferences NewPreviewPreferences
        {
            get { return _vehicleTemplatingData.NewPreviewPreferences; }
        }

        public virtual IPreviewPreferences PreviewPrefs
        {
            get { return _vehicleTemplatingData.PreviewPrefs; }
        }

        public virtual bool HasAvailableBookout(BookoutCategory category)
        {
            return _vehicleTemplatingData.HasAvailableBookout(category);
        }

        public virtual string PriceBelowBook
        {
            get { return _vehicleTemplatingData.PriceBelowBook; }
        }

        public virtual string RoughPriceBelowBook
        {
            get { return _vehicleTemplatingData.RoughPriceBelowBook; }
        }

        public virtual string RoughPriceBelowMSRP
        {
            get { return _vehicleTemplatingData.RoughPriceBelowMSRP; }
        }

        public virtual string PriceBelowMSRP
        {
            get { return _vehicleTemplatingData.PriceBelowMSRP; }
        }

        public virtual void SetFinancingSpecial(int specialId, string memberLogin)
        {
            _vehicleTemplatingData.SetFinancingSpecial(specialId, memberLogin);
        }

        public virtual string YearMakeModel
        {
            get { return _vehicleTemplatingData.YearMakeModel; }
        }

        public virtual bool HasHighPassengerCountForClass()
        {
            return _vehicleTemplatingData.HasHighPassengerCountForClass();
        }

        public virtual string GoodWarranties
        {
            get { return _vehicleTemplatingData.GoodWarranties; }
        }

        public virtual string GoodCrashTests
        {
            get { return _vehicleTemplatingData.GoodCrashTests; }
        }

        public virtual bool HasEquipment(string equipmentDescription)
        {
            return _vehicleTemplatingData.HasEquipment(equipmentDescription);
        }

        public virtual string GetCondition()
        {
            return _vehicleTemplatingData.GetCondition();
        }

        public virtual List<string> LimitCount(List<string> values)
        {
            return _vehicleTemplatingData.LimitCount(values);
        }

        public virtual bool EvaluateCondition(string templateCondition)
        {
            return _vehicleTemplatingData.EvaluateCondition(templateCondition);
        }

        public virtual bool EvaluateSingleCondition(string condition)
        {
            return _vehicleTemplatingData.EvaluateSingleCondition(condition);
        }

        public virtual bool EvaluateSingleCondition(TemplateCondition condition)
        {
            return _vehicleTemplatingData.EvaluateSingleCondition(condition);
        }

        public virtual T GetRandomValue<T>(T[] array)
        {
            return _vehicleTemplatingData.GetRandomValue(array);
        }

        public virtual T[] RandomPermutation<T>(List<T> array)
        {
            return _vehicleTemplatingData.RandomPermutation(array);
        }

        public virtual List<IWeightedItem> RandomWeightedPermutation(List<IWeightedItem> array)
        {
            return _vehicleTemplatingData.RandomWeightedPermutation(array);
        }

        public virtual int Next()
        {
            return _vehicleTemplatingData.Next();
        }

        public virtual int Next(int minValue, int maxValue)
        {
            return _vehicleTemplatingData.Next(minValue, maxValue);
        }

        public virtual int Next(int maxValue)
        {
            return _vehicleTemplatingData.Next(maxValue);
        }

        public virtual double NextDouble()
        {
            return _vehicleTemplatingData.NextDouble();
        }

        public virtual void NextBytes(byte[] buffer)
        {
            _vehicleTemplatingData.NextBytes(buffer);
        }

        public virtual List<string> HighlightedPackageSummaries
        {
            get { return _vehicleTemplatingData.HighlightedPackageSummaries; }
        }

        public virtual decimal KBBBookValue
        {
            get { return _vehicleTemplatingData.KBBBookValue; }
        }

        public virtual decimal NADABookValue
        {
            get { return _vehicleTemplatingData.NADABookValue; }
        }

        public virtual void AddedDescriptionContent(MerchandisingDescriptionItemType itemType)
        {
            _vehicleTemplatingData.AddedDescriptionContent(itemType);
        }
    }
}
