namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum GenericEquipmentReplacementType
    {
        System = 1,
        Dealer = 2
    }
}
