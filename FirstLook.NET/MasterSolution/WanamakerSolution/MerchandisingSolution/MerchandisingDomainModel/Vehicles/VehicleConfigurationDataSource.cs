using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Data;
using MvcMiniProfiler;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class VehicleConfigurationDataSource
    {
        public VehicleConfiguration Select(int businessUnitID, int inventoryId, string memberLogin)
        {
            return VehicleConfiguration.FetchOrCreateDetailed(businessUnitID, inventoryId, memberLogin);
        }

        public void Update(int businessUnitID, string memberLogin, VehicleConfiguration vehicleConfiguration)
        {
            vehicleConfiguration.Save(businessUnitID, memberLogin);
        }

        public List<AdditionalVehicleInfo> GetAdditionalInfoItems(int businessUnitID)
        {
            using (MiniProfiler.Current.Step("VehicleConfigurationDataSource.GetAdditionalInfoItems"))
            {
                using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
                using (IDbCommand cmd = con.CreateCommand())
                {
                    con.Open();

                    cmd.CommandText = "builder.getAdditionalInfoItems";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var retList = new List<AdditionalVehicleInfo>();
                        while (reader.Read())
                        {
                            retList.Add(new AdditionalVehicleInfo(reader));
                        }
                        return retList.Where(info => info.Enabled).ToList();
                    }
                }

            }


        }


    }
}