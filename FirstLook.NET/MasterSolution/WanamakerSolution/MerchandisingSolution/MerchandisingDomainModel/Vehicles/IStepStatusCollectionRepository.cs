﻿using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface IStepStatusCollectionRepository
    {
        IEnumerable<IStepStatus> Fetch(int businessUnitId, int inventoryId);
        void Save(IEnumerable<IStepStatus> stepsToSave, string memberLogin);
    }
}