namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum GenericEquipmentHeaders
    {
        Undefined = 0,
        SeatConfiguration = 44,
        SeatTrim = 43
    }
}
