using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Data;
using log4net;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class GenericEquipmentCollection : CollectionBase, IEnumerable<ITieredEquipment>
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(GenericEquipmentCollection).FullName);
        #endregion

        private const int MAX_COUNT_PER_CATEGORY = 20; //2;

        private bool _isDirty = false;

        public GenericEquipmentCollection()
        {
            
        }
        internal GenericEquipmentCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                if (reader.GetValue(reader.GetOrdinal("categoryId")) != DBNull.Value)
                {
                    Add(new GenericEquipment(reader));
                }
            }
        }
        public static GenericEquipmentCollection FetchSelections(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.selectedEquipment#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "inventoryId", inventoryId, DbType.Int32);

                    Log.Info("FetchSelections - builder.selectedEquipment#Fetch -- get the selected equipment");
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        GenericEquipmentCollection dec = new GenericEquipmentCollection();

                        while (reader != null && reader.Read())
                        {
                            //not first, so add to retList
                            CategoryLink cl = new CategoryLink();
                            cl.CategoryID = reader.GetInt32(reader.GetOrdinal("categoryId"));
                            cl.Description = reader.GetString(reader.GetOrdinal("userFriendlyName"));
                            cl.Header = reader.GetString(reader.GetOrdinal("CategoryHeader"));
                            cl.HeaderID = reader.GetInt32(reader.GetOrdinal("CategoryHeaderID"));

                            if (reader.GetString(reader.GetOrdinal("CategoryHeader")).Equals("Fuel"))
                            {
                                cl.TypeFilter = "Fuel system";
                            }
                            else
                            {
                                cl.TypeFilter = reader.GetString(reader.GetOrdinal("CategoryHeader"));
                            }
                            dec.Add(new GenericEquipment(cl, reader.GetBoolean(reader.GetOrdinal("isStandardEquipment"))));
                        }


                        Log.InfoFormat("FetchSelections returning {0}", StandardObjectDumper.Write(dec, 1));

                        return dec;

                    }
                }
            }
        }
        public static GenericEquipmentCollection StandardEquipmentFetch(int businessUnitID, int chromeStyleID)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getStandardEquipmentCategoriesForDisplay";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", 0, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        GenericEquipmentCollection dec = new GenericEquipmentCollection();
                        Log.Info("StandardEquipmentFetch - Chrome.getStandardEquipmentCategoriesForDisplay -- get the standard equipment");
                        while (reader != null && reader.Read())
                        {
                            //not first, so add to retList
                            dec.Add(new GenericEquipment(reader));
                        }

                        Log.InfoFormat("StandardEquipmentFetch returning {0}", StandardObjectDumper.Write(dec, 1));

                        return dec;

                    }
                }
            }
        }
        public void Save(int businessUnitId, int inventoryId)
        {
            Log.InfoFormat("Save VehicleOptions for businessUnitId = {0}, inventoryId = {1}", businessUnitId, inventoryId);

            // create table to pass to stored proc
            DataTable vehicleOption = new DataTable();
            vehicleOption.Columns.Add("OptionID", typeof(long));
            vehicleOption.Columns.Add("OptionsCode", typeof(string));
            vehicleOption.Columns.Add("OptionText", typeof(string));
            vehicleOption.Columns.Add("DetailText", typeof(string));
            vehicleOption.Columns.Add("IsStandardEquipment", typeof(bool));

            // populate with collection
            foreach (GenericEquipment ge in this)
            {
                DataRow vehicleOptionRow = vehicleOption.NewRow();
                vehicleOptionRow["OptionID"] = ge.Category.CategoryID;
                vehicleOptionRow["OptionsCode"] = ge.OptionCode;
                vehicleOptionRow["OptionText"] = ge.OptionText;
                vehicleOptionRow["DetailText"] = ge.GetDetailText();
                vehicleOptionRow["IsStandardEquipment"] = ge.IsStandard;
                vehicleOption.Rows.Add(vehicleOptionRow);

            }

            // call proc (don't wait for ten minutes!!!
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[Database.MerchandisingDatabase].ConnectionString))
            {

                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[builder].[VehicleOption#BulkSave]";
                    cmd.Connection = con;

                    cmd.Parameters.AddWithValue("@businessUnitID", businessUnitId);
                    cmd.Parameters.AddWithValue("@inventoryId", inventoryId);
                    var tableParm = cmd.Parameters.AddWithValue("@vehicleOptions", vehicleOption);
                    tableParm.SqlDbType = SqlDbType.Structured;
                    tableParm.TypeName = "[builder].[VehicleOption]";

                    cmd.ExecuteNonQuery();

                }

            }
            
            _isDirty = false;
        }
        public void DeleteAll(int businessUnitId, int inventoryId)
        {
            Log.InfoFormat("Deleting all VehicleOptions for businessUnitId = {0} inventoryID = {1}", businessUnitId, inventoryId);
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                //delete generic options
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "builder.vehicleOptions#Delete";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryID", inventoryId, DbType.Int32);
                    cmd.ExecuteNonQuery();

                    _isDirty = false;
                }

              
            }
            Log.DebugFormat("Finished deleting all VehicleOptions for businessUnitId = {0} inventoryID = {1}", businessUnitId, inventoryId);
        }

        public int Add(GenericEquipment equip)
        {
            Log.DebugFormat("Adding generic equipment {0}", StandardObjectDumper.Write(equip,2));

            if (!Contains(equip.Category.CategoryID))
            {
                _isDirty = true;
                var idx = FilterByType(equip);
                Log.DebugFormat("Index is {0}", idx);
                return idx;
            }
            Log.Debug("Returning 0");
            return 0;
        }

        /// <summary>
        /// AddRange does not duplicate values
        /// </summary>
        /// <param name="collection"></param>
        public void AddRange(GenericEquipmentCollection collection)
        {
            foreach (GenericEquipment equip in collection)
            {
                Add(equip);
            }
        }

        private int FilterByType(GenericEquipment equip)
        {
            string typeFilter = equip.Category.TypeFilter;
            Log.DebugFormat("RemoveByTypeFilter() called with typeFilter '{0}'", typeFilter);
            bool addEquipment = true;

            if (ChromeCatFilter.DRIVETRAIN.Equals(typeFilter)
                || ChromeCatFilter.ENGINE.Equals(typeFilter)
                || ChromeCatFilter.TRANSMISSION.Equals(typeFilter)
                || ChromeCatFilter.FUEL.Equals(typeFilter))
            {
                List<GenericEquipment> dupList = new List<GenericEquipment>();
                foreach (GenericEquipment equipment in List)
                {
                    if (equipment.MatchesTypeFilter(typeFilter))
                    {
                        dupList.Add(equipment);
                    }
                }

                foreach (GenericEquipment equipment in dupList)
                {
                    if (!equipment.IsStandard) //favor optionals (don't remove)
                    {
                        addEquipment = false;
                        break;
                    }

                    List.Remove(equipment);
                    Log.DebugFormat("Removing equipment '{0}' from List.", equipment.Description);
                }
            }

            if (addEquipment)
                return List.Add(equip);
            
            return -1;
        }

        public int OptionalEquipmentCount()
        {
            return Count - StandardEquipmentCount();
        }
        public int StandardEquipmentCount()
        {
            int ct = 0;
            foreach (GenericEquipment equipment in List)
            {
                if (equipment.IsStandard)
                {
                    ct++;
                }
            }
            return ct;
        }

        public GenericEquipment Item(int i)
        {
            return (GenericEquipment)List[i];
        }

        public bool Contains(int categoryId)
        {
            foreach (GenericEquipment de in List)
            {
                if (de.Category.CategoryID == categoryId)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Contains(string description)
        {
            foreach (GenericEquipment de in List)
            {
                if (de.Category.Description == description)
                {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsTypeFilter(string typeFilter)
        {
            foreach (GenericEquipment de in List)
            {
                if (de.Category.TypeFilter == typeFilter)
                {
                    return true;
                }
            }
            return false;
        }

        public void Remove(int categoryId)
        {
            Log.DebugFormat("Remove() called with cateogoryId {0}", categoryId);

            foreach (GenericEquipment de in List)
            {
                if (de.Category.CategoryID == categoryId)
                {
                    _isDirty = true;
                    List.Remove(de);
                    if( Log.IsInfoEnabled )
                    {
                        Log.InfoFormat("Removed generic equipment {0}", StandardObjectDumper.Write(de, 1));                        
                    }
                    return;
                }
            }
        }

        public void RemoveOptionsOnly(int categoryId)
        {
            Log.DebugFormat("RemoveOptionsOnly() called with categoryId {0}", categoryId);

            foreach (GenericEquipment de in List)
            {
                if (de.Category.CategoryID == categoryId && !de.IsStandard)
                {
                    _isDirty = true;
                    List.Remove(de);
                    Log.InfoFormat("Removed GenericEquipment {0}", StandardObjectDumper.Write(de, 1));
                    return;
                }
            }
        }

        public List<int> GetCategoryIdList()
        {
            List<int> retList = new List<int>();

            foreach (GenericEquipment de in List)
            {
                retList.Add(de.Category.CategoryID);
            }

            Log.DebugFormat("CategoryIdList is: {0}", retList.ToDelimitedString(","));

            return retList;
        }

        public GenericEquipmentCollection GetByHeaderID(int headerID, bool includeStandardEquipment)
        {
            Log.DebugFormat("getByHeaderID called with headerID {0} and includeStandardEquipment {1}",
                headerID, includeStandardEquipment);
            GenericEquipmentCollection dec = new GenericEquipmentCollection();
            foreach (GenericEquipment equipment in List)
            {
                if (equipment.Category.HeaderID == headerID && (includeStandardEquipment || !equipment.IsStandard))  //if including standard or this isn't standard it's eligible
                {
                    dec.Add(equipment);
                }
            }

            Log.DebugFormat("Returning {0}", StandardObjectDumper.Write(dec,1));
            return dec;
        }

        public GenericEquipment GetByCategoryId(int categoryId)
        {
            Log.DebugFormat("getByCategoryId() called with categoryId {0}", categoryId);

            Log.DebugFormat("List is {0}", StandardObjectDumper.Write(List,1));
            foreach (GenericEquipment de in List)
            {
                if (de.Category.CategoryID == categoryId)
                {
                    Log.DebugFormat("Rreturning {0}", StandardObjectDumper.Write(de,1));
                    return de;
                }
            }
            const string ERROR = "No display equipment found for category id";
            Log.Error(ERROR);
            throw new ApplicationException(ERROR);
        }

        public GenericEquipmentCollection GetByCategoryIdList(List<int> categoryIDs)
        {
            Log.DebugFormat("getByCategoryIdList() called with categoryIDs {0}", categoryIDs.ToDelimitedString(","));

            GenericEquipmentCollection retCollection = new GenericEquipmentCollection();
            foreach (GenericEquipment equipment in List)
            {
                if (categoryIDs.Contains(equipment.Category.CategoryID))
                {
                    retCollection.Add(equipment);
                }
            }

            Log.DebugFormat("Returning {0}", StandardObjectDumper.Write(retCollection,1));

            return retCollection;
        }

        public GenericEquipmentCollection GetOptionsFromHeaderList(List<int> headerIDList, 
            List<int> excludeCategoryIds)
        {
            Log.DebugFormat("getOptionsFromHeaderList() called with headerIdList {0} and excludeCategoryIds {1}",
                headerIDList.ToDelimitedString(","), excludeCategoryIds.ToDelimitedString(","));

            List<int> validCatIDs = CategoryLink.GetCatIDsFromHeaderList(headerIDList);
            GenericEquipmentCollection retColl = new GenericEquipmentCollection();
            
            InnerList.Sort();
            
            foreach (GenericEquipment de in List)
            {
                int catId = de.Category.CategoryID;
                if (validCatIDs.Contains(catId) && !excludeCategoryIds.Contains(catId) && de.Tier <= 2)  //only take tier 0,1,2
                {
                    retColl.Add(de);
                }
            }
            
            Log.DebugFormat("Returning collection {0}", StandardObjectDumper.Write(retColl,1));
            return retColl;
        }

        public GenericEquipmentCollection GetAllByTypeFilter(string typeFilter)
        {
            Log.DebugFormat("getAllByTypeFilter() called with typeFilter '{0}'", typeFilter);
            GenericEquipmentCollection genericEquipmentCollection = new GenericEquipmentCollection();
            foreach (GenericEquipment de in List)
            {
                if (de.Category.TypeFilter == typeFilter)
                {
                    genericEquipmentCollection.Add(de);
                }
            }
            Log.DebugFormat("Returning collection {0}", StandardObjectDumper.Write(genericEquipmentCollection, 1));
            return genericEquipmentCollection;
        }

        public void RemoveAllWithHeader(int removeHeaderID)
        {
            Log.DebugFormat("RemoveAllWithHeader() called with headerID {0}", removeHeaderID);
            for(int i = List.Count-1; i >= 0; i--)
            {
                if (Item(i).Category.HeaderID == removeHeaderID)
                {
                    Log.DebugFormat("RemoveAt({0}) called to remove GenericEquipment {1}.", 
                        i, StandardObjectDumper.Write(Item(i), 1));
                    RemoveAt(i);
                }
            }
        }

        public List<string> GetDescriptions()
        {
            Log.DebugFormat("GetDescriptions() called.");
            List<string> retList = new List<string>();
            foreach (GenericEquipment de in List)
            {
                retList.Add(de.GetDescription());
            }
            Log.DebugFormat("Returning generic equipment list: {0}", retList.ToDelimitedString(","));
            return retList;
        }

        public List<CategoryCollection> GetCategoryCollections()
        {
            Dictionary<string, CategoryCollection> workingList = new Dictionary<string, CategoryCollection>();
            foreach (GenericEquipment de in List)
            {
                string header = de.Category.Header;
                if (!workingList.ContainsKey(header))
                {
                    Log.DebugFormat("Adding header {0}", header);
                    workingList.Add(header, new CategoryCollection(header));
                }

                Log.DebugFormat("Calling workingList[{0}].Add() with category {1}", header, de.Category.CategoryID);
                workingList[header].Add(de.Category);
            }

            var retVal = new List<CategoryCollection>(workingList.Values);
            Log.DebugFormat("Returning {0}", StandardObjectDumper.Write(retVal,1));
            return retVal;
        }

        public void FilterHeadersAndExclusiveCategories()
        {
            Log.Debug("FilterHeadersAndExclusiveCategories() called.");

            //first sort in the prefilter order for removing standard equipment eliminated by options
            //this.InnerList.Sort((IComparer)new GenericEquipmentPreFilterOrderer());

            //first eliminate categories based on type filters
            Log.Debug("Executing type filters.");
            ExecuteTypeFilters();

            //then remove any by header...
            Log.Debug("Processing exclusion map");
            IDictionary<int, List<int>> exclusionMap = GetExclusionMap();
            ProcessExclusionFilters(exclusionMap);

            Log.InfoFormat("List Contents: {0}", StandardObjectDumper.Write(List, 2));
        }

        public void Sort()
        {
            Log.Debug("Sort() called.");
            InnerList.Sort();
        }

        private void ProcessExclusionFilters(IDictionary<int, List<int>> exclusionMap)
        {
            Log.Debug("ProcessExclusionFilteres() called.");

            //order for filtering of exclusion
            InnerList.Sort(new GenericEquipmentPreFilterOrderer());

            GenericEquipmentCollection retCol = new GenericEquipmentCollection();
            //first copy all over...
            foreach (GenericEquipment de in List)
            {
                retCol.Add(de);
            }

            //now use the hashtable to remove categories that should not appear for 
            foreach (GenericEquipment de in retCol)
            {
                Log.DebugFormat("Processing GenericEquipment '{0}'", de.Description);

                int currentCatID = de.Category.CategoryID;
                Log.DebugFormat("CategoryID is {0}", currentCatID);

                if (Contains(currentCatID))
                {
                    Log.DebugFormat("Contains({0}) is true.", currentCatID);
                    if (exclusionMap.ContainsKey(currentCatID)) //remove all others if this category eliminates them
                    {
                        Log.DebugFormat("exclusionMap.ContainsKey({0}) is true. remove all others if this category eliminates them", currentCatID);

                        List<int> elimCatIDs = exclusionMap[currentCatID];
                        Log.DebugFormat("ExclusionMap[{0}] contains the following elimination category ids: {0}", elimCatIDs.ToDelimitedString(","));

                        foreach (int catID in elimCatIDs)
                        {
                            if (retCol.Contains(catID))
                            {
                                GenericEquipment ge = retCol.GetByCategoryId(catID);
                                string typefilter = ge.Category.TypeFilter;

                                if (typefilter.Equals("Engine") || typefilter.Equals("Drivetrain") || typefilter.Equals("Transmission") || typefilter.Equals("Fuel system"))
                                {
                                    continue;
                                }
                                if (Contains(catID))
                                {
                                    Remove(catID);
                                    Log.DebugFormat("Removed category {0}", catID);
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }
            }
        }

        private static IDictionary<int, List<int>> GetExclusionMap()
        {
            Log.Debug("getExclusionMap() called.");

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {

                    cmd.CommandText = "templates.getCategoryExclusionMap";
                    cmd.CommandType = CommandType.StoredProcedure;

                    IDictionary<int, List<int>> exclusionMap = new Dictionary<int, List<int>>();

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int dispID = reader.GetInt32(reader.GetOrdinal("displayCategoryID"));
                            int elimID = reader.GetInt32(reader.GetOrdinal("eliminateCategoryID"));
                            if (!exclusionMap.ContainsKey(dispID))
                            {
                                exclusionMap.Add(dispID, new List<int>());
                            }
                            exclusionMap[dispID].Add(elimID);
                        }

                        Log.DebugFormat("Returning map {0}", StandardObjectDumper.Write(exclusionMap,1));
                        return exclusionMap;
                    }
                }
            }

        }

        private void ExecuteTypeFilters()
        {
            Log.Debug("ExecuteTypeFilters() called.");

            //sort for the filtering...
            InnerList.Sort(new GenericEquipmentPreFilterOrderer());

            GenericEquipmentCollection retCol = new GenericEquipmentCollection();

            //first copy all over...
            foreach (GenericEquipment de in List)
            {
                retCol.Add(de);
            }

            //
            // NOTE: This code is suspect.
            //
            foreach (GenericEquipment de in retCol)
            {
                if (String.IsNullOrEmpty(de.Category.TypeFilter))
                {
                    continue;
                }

                //if it has a typeFilter, though, we want to remove all other following categories with same type filter
                if (Contains(de.Category.CategoryID))
                {
                    GenericEquipmentCollection cc = GetAllByTypeFilter(de.Category.TypeFilter);

                    
                    // NOTE: I believe this is the filter that prevents you from having 2 equipment items
                    // in the mirrors group.
                    if (cc.Count >= MAX_COUNT_PER_CATEGORY)
                    {
                        Log.DebugFormat("There are more than {0} generic equipment items.  We will now filter some out.");
                        for (int i = 1; i < cc.Count; i++)
                        {
                            var id = cc.Item(i).Category.CategoryID; 
                            Remove(id);
                            Log.DebugFormat("Removed category {0}", id);
                        }
                    }
                }
            }
        }

        public void SetSelectionState(int categoryId, bool isSelected)
        {
            Log.DebugFormat("SetSelectionState() called with categoryId {0}, isSelected {1}",
                categoryId, isSelected);

            if (!Contains(categoryId))
            {
                Log.DebugFormat("Adding categoryId {0}", categoryId);
                GenericEquipment ge = new GenericEquipment();
                ge.Category.CategoryID = categoryId;
                ge.Category.IsSelected = isSelected;
                Add(ge);
            }else
            {
                foreach (GenericEquipment ge in this)
                {
                    if (ge.Category.CategoryID == categoryId)
                    {
                        Log.DebugFormat("CategoryId already in this.  Setting IsSelected to {0}", isSelected);
                        ge.Category.IsSelected = isSelected;

                        // NOTE: This may be a problem.  Why return now?  Can't we have more than
                        // one genericEquipment with the same categoryId?
                        return;
                    }
                }
            }
        }

        public void DeselectStandardEquipment(List<int> stdCategoryIdsToRemove)
        {
            Log.DebugFormat("DeselectStandardEquipment() called with categoryIds {0}", stdCategoryIdsToRemove.ToDelimitedString(","));

            foreach (int catId in stdCategoryIdsToRemove)
            {
                Remove(catId);
            }
        }

        public void ReprocessStandardEquipment(int businessUnitId, int chromeStyleId, List<int> stdCategoryIdsToRemove)
        {
            Log.DebugFormat(
                "ReprocessStandardEquipment() called with businessUnitId {0}, chromeStyleId {1}, stdCategoryIdsToRemove {2}",
                businessUnitId, chromeStyleId, stdCategoryIdsToRemove.ToDelimitedString(","));

            LoadChromeStyle(businessUnitId, chromeStyleId, true);
            DeselectStandardEquipment(stdCategoryIdsToRemove);

        }
        public void LoadChromeStyle(int businessUnitId, int chromeStyleId, bool forceReload)
        {
            Log.DebugFormat("LoadChromeStyle() called with businesUnitId {0}, chromeStyleId {1}, forceReload {2}",
                businessUnitId, chromeStyleId, forceReload);

            if (chromeStyleId > 0)
            {
                if (forceReload)
                {
                    Log.Debug("Calling ClearStandardEquipment()");
                    ClearStandardEquipment();
                }

                if (StandardEquipmentCount() == 0)
                {
                    Log.DebugFormat("Calling StandardEquipmentFetch() with businesssUnitId {0}, chromeStyleId {1}",
                        businessUnitId, chromeStyleId);

                    GenericEquipmentCollection std = StandardEquipmentFetch(businessUnitId, chromeStyleId);
                    AddRange(std); //                    
                }

                _isDirty = true;
            }
        }

        public void ClearStandardEquipment()
        {
            Log.Debug("ClearStandardEquipment() called.");
            GenericEquipmentCollection dec = new GenericEquipmentCollection();
            dec.AddRange(this);

            Log.Debug("Clearing current contents.");
            List.Clear(); //clear current contents...
            foreach (GenericEquipment de in dec)
            {
                if (!de.IsStandard)
                {
                    Log.DebugFormat("Adding non-standard generic equipment {0}", StandardObjectDumper.Write(de, 1));
                    Add(de); //add to this
                }
            }

            _isDirty = true;
        }

        public bool IsDirty
        {
            get { return _isDirty; }
        }


        public new IEnumerator<ITieredEquipment> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                // Iterator
                yield return Item(i);
            }
        }

        public void LoadViewState(object state)
        {
            Log.DebugFormat("LoadViewState() called with state {0}", StandardObjectDumper.Write(state,1));

            Log.Debug("Clearing list.");
            List.Clear();
            
            if (state is ArrayList)
            {
                ArrayList al = state as ArrayList;
                foreach (object eqState in al)
                {
                    Log.DebugFormat("Loading equipment state into Generic Equipment and adding to List. State is {0}",
                        StandardObjectDumper.Write(eqState,1));

                    GenericEquipment ge = new GenericEquipment();
                    ge.LoadViewState(eqState);
                    Add(ge);
                }
            }

        }

        public object SaveViewState()
        {
            Log.Debug("SaveViewState() called.");
            ArrayList al = new ArrayList();
            foreach (GenericEquipment ge in List)
            {
                al.Add(ge.SaveViewState());
            }
            Log.DebugFormat("Returning ArrayList {0}", StandardObjectDumper.Write(al,1));
            return al;
        }  

        public List<ITieredEquipment> RemoveDuplicates(GenericEquipmentReplacementCollection replacementRules)
        {
            return new GenericEquipmentCollectionDuplicateRemover(replacementRules, this).RemoveDuplicates();
        }
    }
}
