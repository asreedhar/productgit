namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum DrivetrainType
    {
        Unknown = 0,
        FrontWheelDrive = 1,
        RearWheelDrive = 2,
        FourWheelDrive = 3,
        AllWheelDrive = 4,
        TwoWheelDrive = 5
    }
}
