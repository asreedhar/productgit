namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum CertificationFeatureType
    {
        Undefined = 0,
        CpoSuggestedText,       //1
        DealerCustomText,
        BasicWarranty,
        DrivetrainWarranty,
        AdditionalWarranty,     //5
        ExtendedWarranty,
        CorrosionProtection,
        WarrantyUpgrades,
        CustomerService,
        Deductible,             //10
        Financing,
        Return,
        History,
        Inspection,
        Loaner,                 //15
        Other,
        RoadsideAssistance,
        Transferrable,
        Trials,
        TripInterruption,       //20
        TripPlanning,
        DealerCustomPreviewText //22
    }
}
