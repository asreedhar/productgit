namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// this enum is used as reference values for generic option Id lookups
    /// </summary>
    public enum ChromeCategory
    {
        Undefined = 0,
        FourWheelDrive = 1040,
        AllWheelDrive = 1041
    }
}
