﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{

    public interface IChromeOptionRepository
    {
        EquipmentCollection FetchAllOptional(int styleId);
        AvailableChromeColors FetchColors(int styleId);
        List<ChromeStyle> StylesFor(string vin);
    }

    public class ChromeOptionRepository : IChromeOptionRepository
    {
        public EquipmentCollection FetchAllOptional(int styleId)
        {
            return EquipmentCollection.FetchOptional(styleId, false);
        }

        public AvailableChromeColors FetchColors(int styleId)
        {
            return AvailableChromeColors.Fetch(styleId);
        }

        public List<ChromeStyle> StylesFor(string vin)
        {
            return ChromeStyle.FetchList(vin);
        }
    }

    public interface ILotRepository
    {
        LotInfo GetInfo(int businessUnitId, int inventoryId);
    }

    public class LotInfo
    {
        public string Codes { get; set; }
        public int StyleId { get; set; }
        public string Vin { get; set; }
        public string Color { get; set; }

        public static LotInfo Empty = new LotInfo {Color = "", Codes = "", StyleId = -1, Vin = "" };
    }

    public class LotRepository : ILotRepository
    {

        public LotInfo GetInfo(int businessUnitId, int inventoryId)
        {
            using (var connection = Database.GetConnection(Database.MerchandisingDatabase))
            using (var command = connection.CreateCommand())
            {
                connection.Open();

                command.CommandText = "builder.GetLotInfo#Fetch";
                command.CommandType = CommandType.StoredProcedure;

                command.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);
                command.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var lotInfo = new LotInfo();

                        var styleIndex = reader.GetOrdinal("ChromeStyleId");
                        var vinIndex = reader.GetOrdinal("vin");
                        var codesIndex = reader.GetOrdinal("optioncodes");
                        var colorIndex = reader.GetOrdinal("Color");

                        lotInfo.StyleId = reader.IsDBNull(styleIndex) ? -1 : reader.GetInt32(styleIndex);
                        lotInfo.Vin = reader.IsDBNull(vinIndex) ? "" : reader.GetString(vinIndex);
                        lotInfo.Codes = reader.IsDBNull(codesIndex) ? "" : reader.GetString(codesIndex);
                        lotInfo.Color = reader.IsDBNull(colorIndex) ? "" : reader.GetString(colorIndex);

                        return lotInfo;
                    }
                }

                return LotInfo.Empty;
            }
        }

    }


    public class LotVehicle
    {
        public int ChromeStyleId { get; set; }
        public string InteriorCode { get; set; }
        public string ExteriorCode1 { get; set; }
        public string ExteriorCode2 { get; set; }
        public string ExteriorDescription { get; set; }
        public string ExteriorDescription2 { get; set; }
        public string InteriorDescription { get; set; }

        public List<DetailedEquipment> Options { get; set; }

        public LotVehicle()
        {
            Options = new List<DetailedEquipment>();
        }

        public static LotVehicle Empty = new LotVehicle { ExteriorCode1 = "", ExteriorCode2 = "", InteriorCode = "", Options = new List<DetailedEquipment>(), ChromeStyleId = -1 };
    }

    public class LotLoader
    {
        private readonly IChromeOptionRepository _chromeRepository;
        private readonly ILotRepository _lotRepository;

        public LotLoader() : this(new ChromeOptionRepository(), new LotRepository()) { }

        internal LotLoader(IChromeOptionRepository chromeRepository, ILotRepository lotRepository)
        {
            _chromeRepository = chromeRepository;
            _lotRepository = lotRepository;
        }


        public bool Load(int businessUnitId, VehicleConfiguration configuration)
        {
            var info = _lotRepository.GetInfo(businessUnitId, configuration.InventoryId);
            
            if (info == LotInfo.Empty) //the row is empty nothing to do.
                return false;

            var lotOptions = info.Codes.Split(new [] {'|', ','});

            bool loaded = false;

            if (configuration.ChromeStyleID <= 0 && info.StyleId >= 0)
            {
                if (Validate(info.StyleId, info.Vin))
                {
                    configuration.SetChromeStyleID( businessUnitId, configuration.InventoryId, info.StyleId);
                    loaded = true;
                }
            }

            if (configuration.ChromeStyleID <= 0) return false;

            var availableOptions = _chromeRepository.FetchAllOptional(configuration.ChromeStyleID);
            var availableColors = _chromeRepository.FetchColors(configuration.ChromeStyleID);

            foreach (Equipment equipment in availableOptions)
            {
                if (!lotOptions.Contains(equipment.OptionCode)) continue;

                if (availableColors.ExteriorColors.Any(c => c.First.ColorCode == equipment.OptionCode))
                {
                    if (string.IsNullOrEmpty(configuration.ExteriorColorCode))
                    {
                        configuration.ExteriorColorCode = equipment.OptionCode;
                        loaded = true;

                        var pairs = availableColors.ExteriorColors.Where(p => p.First.ColorCode == equipment.OptionCode).ToArray();
                        if (pairs.Any()) configuration.ExteriorColor1 = pairs.First().First.Description;
                    }

                    continue;
                }

                if (availableColors.ExteriorColors.Any(c => c.Second.ColorCode == equipment.OptionCode))
                {
                    if (string.IsNullOrEmpty(configuration.ExteriorColorCode2))
                    {
                        configuration.ExteriorColorCode2 = equipment.OptionCode;
                        loaded = true;

                        var pairs = availableColors.ExteriorColors.Where(p => p.Second.ColorCode == equipment.OptionCode).ToArray();
                        if (pairs.Any()) configuration.ExteriorColor2 = pairs.First().Second.Description;
                    }

                    continue;
                }

                if (availableColors.InteriorColors.Any(c => c.ColorCode == equipment.OptionCode))
                {
                    if (string.IsNullOrEmpty(configuration.InteriorColorCode))
                    {
                        configuration.InteriorColorCode = equipment.OptionCode;
                        loaded = true;

                        var color = availableColors.InteriorColors.Where(c => c.ColorCode == equipment.OptionCode).ToArray();
                        if (color.Any()) configuration.InteriorColor = color.First().Description;

                    }

                    continue;
                }

                if (configuration.VehicleOptions.options.ContainsOption(equipment.OptionCode)) continue;

                var normalizedDescription = equipment.NormalizedDescription.Trim().ToUpper();

                var detailText = string.IsNullOrEmpty(equipment.ExtDescription) || string.IsNullOrEmpty(equipment.ExtDescription.Trim())
                                     ? normalizedDescription
                                     : normalizedDescription + ": " + equipment.ExtDescription.Trim();

                configuration.VehicleOptions.AddOption(new DetailedEquipment(
                    equipment.OptionCode, 
                    normalizedDescription,
                    equipment.CategoryList, 
                    detailText, 
                    false
                ));

                loaded = true;

            }

            if (string.IsNullOrEmpty(configuration.ExteriorColorCode) && !string.IsNullOrEmpty(info.Color))
            {
                var matchingDescriptions = availableColors.ExteriorColors.Where(exteriorColor => exteriorColor.First.Description.ToLower() == info.Color.ToLower()).ToList();
                
                if (matchingDescriptions.Any())
                {
                    configuration.ExteriorColorCode = matchingDescriptions.First().First.ColorCode;
                    configuration.ExteriorColor1 = matchingDescriptions.First().First.Description;
                    loaded = true;
                }
            }

            return loaded;
        }

        private bool Validate(int lotStyleId, string vin)
        {
            return lotStyleId > 0 && _chromeRepository.StylesFor(vin).Any(s => s.ChromeStyleID == lotStyleId);
        }
    }

}