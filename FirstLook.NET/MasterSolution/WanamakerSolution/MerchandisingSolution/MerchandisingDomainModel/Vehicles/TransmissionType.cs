namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum TransmissionType
    {
        Unknown = 0,
        Automatic = 1,
        Manual = 2
    }
}
