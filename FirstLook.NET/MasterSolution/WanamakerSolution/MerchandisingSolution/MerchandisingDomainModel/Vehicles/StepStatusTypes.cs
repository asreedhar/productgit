namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum StepStatusTypes
    {
        Undefined = 0,
        ExteriorEquipment,
        InteriorEquipment,
        Photos,
        Pricing,
        Posting,
        AdReview
    }
}
