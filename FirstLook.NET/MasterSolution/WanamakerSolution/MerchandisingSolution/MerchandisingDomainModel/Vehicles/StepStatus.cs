namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class StepStatus : IStepStatus
    {
        internal StepStatus(int businessUnitId, int inventoryId,
            StepStatusTypes statusTypeId, int? statusLevel, int? defaultStatusLevel, byte[] version)
        {
            BusinessUnitId = businessUnitId;
            InventoryId = inventoryId;
            StatusTypeId = statusTypeId;
            StatusLevelInner = statusLevel.GetValueOrDefault(defaultStatusLevel.HasValue ? defaultStatusLevel.Value : (int) ActivityStatusCodes.Untouched);
            Version = version;
            IsNew = !statusLevel.HasValue;
        }

        public int BusinessUnitId { get; private set; }

        public int InventoryId { get; private set; }

        public StepStatusTypes StatusTypeId { get; private set; }

        private int StatusLevelInner { get; set; }
        public int StatusLevel
        {
            get { return StatusLevelInner; }
            set
            {
                if (StatusLevelInner == value) return;
                StatusLevelInner = value;
                IsDirty = true;
            }
        }

        public byte[] Version { get; private set; }

        public bool IsDirty { get; private set; }

        public bool IsNew { get; private set; }
    }
}