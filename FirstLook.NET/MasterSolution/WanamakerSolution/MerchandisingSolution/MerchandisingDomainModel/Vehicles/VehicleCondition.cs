using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class VehicleCondition
    {
        public const int CONDITION_LEVEL_EXCELLENT = 3;
        public const int CONDITION_LEVEL_GOOD = 2;
        public const int CONDITION_LEVEL_FAIR = 1;
        public const int CONDITION_LEVEL_POOR = 0;


        public VehicleCondition(int conditionTypeId, int conditionLevel, string description)
        {
            ConditionTypeId = conditionTypeId;
            Description = description;
            ConditionLevel = conditionLevel;
        }

        internal VehicleCondition(IDataRecord reader)
        {
            ConditionTypeId = reader.GetInt32(reader.GetOrdinal("vehicleConditionTypeId"));
            Description = reader.GetString(reader.GetOrdinal("title"));
            Description = reader.GetString(reader.GetOrdinal("description"));
            ConditionLevel = reader.GetInt32(reader.GetOrdinal("conditionLevel"));
        }

        public string ConditionType { get; set; }

        public int ConditionLevel { get; set; }

        public int ConditionTypeId { get; set; }

        public string Description { get; set; }

        public void Save(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.VehicleCondition#Save";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Description", string.Empty, DbType.String);
                    Database.AddRequiredParameter(cmd, "ConditionTypeId", ConditionTypeId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ConditionLevel", ConditionLevel, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}