using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Workflow;
using Google.Apis.Util;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    /// <summary>
    /// This class needs to be refactored, badly.  It's all over the place.  The sheer # of using statements should be a 
    /// big red flag.  It calls web services, it does data access via sprocs, it has embedded workflow logic, it 
    /// sorts, it tries to use the session as a cache, and at some point, it apparantly needed to use javascript.  
    /// FAIL. ZB 5/22/2014
    /// </summary>
    [Serializable]
    public class InventoryData : IInventoryData
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof(InventoryData).FullName);

        #endregion

        public const int NewVehicle = 1;
        public const int UsedVehicle = 2;

        public DueForRepricingMode IsDueForRepricing { get; private set; }

        public int? AgeBucketId { get; set; }

        public bool IsPreOwned()
        {
            return InventoryType == UsedVehicle;
        }
        public bool IsNew()
        {
            return InventoryType == NewVehicle;
        }
        
        private const int CriticalStatusLevel = (int)ActivityStatusCodes.Complete;

        private readonly DateTime _receiveDate;
        public DateTime ReceivedDate
        {
            get { return _receiveDate; }
        }

        private string _thumbnailUrl;
        public string ThumbnailUrl
        {
            get { return _thumbnailUrl ?? (_thumbnailUrl = PhotoServices.Value.GetMainThumbnailUrl(BusinessUnitID, VIN).Url); }
        }

        public CertificationType CertificationTypeId
        {
            get { return ManufacturerCertification.GetCertificationType(Make); }
        }

        public int BusinessUnitID { get; private set; }

        private static readonly Lazy<IPhotoServices> PhotoServices =
            new Lazy<IPhotoServices>(Registry.Resolve<IPhotoServices>,
                                     true);

        private int? _photoCount;
        public int PhotoCount
        {
            get
            {
                if(!_photoCount.HasValue)
                    _photoCount = PhotoServices.Value.GetPhotoUrlsByVin(BusinessUnitID, VIN, TimeSpan.FromSeconds(10)).PhotoUrls.Length;

                return _photoCount.Value;
            }
            internal set { _photoCount = value; }
        }

        public int DesiredPhotoCount { get; set; }

        private bool DealerCertified
	    {
		    get { return ! Certified && CertifiedProgramId.HasValue && CertifiedProgramId.Value > 0; }

	    }

	    public string CertificationTitle
        {
            get
            {
				if (Certified && CertifiedProgramId.HasValue && CertifiedProgramId.Value > 0)
                {
					if (String.IsNullOrEmpty(Make))
					{
						return "Certified";
					}
					return Make + " Certified";
				}

				return DealerCertified ? 
                            CertifiedProgramMarketingName.IsNullOrEmpty() 
                                ? "Dealer Certified" // dealer certified without MarketingName
                                : CertifiedProgramMarketingName // dealer certified with Marketing Name
                            : string.Empty; // not dealer certified
            }
        }

        public int InventoryType { get; set; }

        public decimal? LastUpdateListPrice { get; set; }

        public MerchandisingBucket? StatusBucket { get; set; }

        public string VehicleLocation { get; set; }

        public string BaseColor { get; set; }
        public string Ext1Desc { get; private set; }
        public string Ext1Code { get; private set; }
        public string Ext2Desc { get; private set; }
        public string Ext2Code { get; private set; }
        public string IntDesc { get; private set; }
        public string IntCode { get; private set; }

        public int SegmentId { get; set; }

        public bool NeedsInterior { get; set; }

        public DateTime? LastDescriptionPublicationDate { get; set; }

        public string LastDescriptionPublishedDateText()
        {
            return LastDescriptionPublicationDate.HasValue 
                ? String.Format("{0:d}", LastDescriptionPublicationDate) 
                : "--";
        }

        public bool HasBeenPublished
        {
            get { return LastDescriptionPublicationDate.HasValue; }
        }

        public string PercentComplete
        {
            get { return CalculatePercentComplete(); }
        }

        private string CalculatePercentComplete()
        {
            const int TOTAL_CT = 5;
            var val = TOTAL_CT
                      - (NeedsPhotos ? 1 : 0)
                      - (NeedsPricing ? 1 : 0)
                      - (NeedsExterior ? 1 : 0)
                      - (NeedsInterior ? 1 : 0)
                      - (NeedsAdReview ? 1 : 0);

            var percentage = val*(100/TOTAL_CT);

            return percentage > 0
                       ? percentage + " % Done"
                       : "";
        }

        public bool IsLowActivity { get; set; }

        public DateTime InServiceDate
        {
            get
            {
                //default to aug. 1 of year prior to model year -- need to get in service date from database
                return new DateTime(Int32.Parse(Year) - 1, 8, 1);
            }
        }

        public bool isFavourable { get; set; }
        

        public InventoryData()
        {
            NeedsInterior = true;
            PendingApproval = true;
            NeedsExterior = true;
            NeedsPhotos = true;
            NeedsPricing = true;
            NeedsAdReview = true;
            Certified = false;
            ListPrice = -1.0M;
            MileageReceived = -1;
            _receiveDate = DateTime.MinValue;
            StockNumber = "";
            _tradeOrPurchase = 1;
            UnitCost = -1.0M;
            VIN = "";
            Year = "";
            Make = "";
            Model = "";
            Trim = "";
            AfterMarketTrim = "";
            MarketClass = "";
            //_inventoryWorkflowSearch = new InventoryDataFilter();
        }

        internal InventoryData(string stockNumber, int businessUnitId, string vin, decimal unitCost,
                              bool certified, string certifiedId, int? certifiedProgramId, int mileageReceived,
                              int tradeOrPurchase, decimal listPrice, DateTime receiveDate, string year, string make,
                              string model, string marketClass, int segmentId, string trim, string afterMarketTrim, int inventoryId,
                              bool isLowActivity, int exteriorStatus, int pricingStatus,
                              int interiorStatus, int photoStatus, int adReviewStatus, int postingStatus,
                              string baseColor, string ext1Code, string ext1Desc, string ext2Code, string ext2Desc, string intCode, string intDesc, 
                              int? chromeStyleId,
                              int? vehicleCatalogId, string vehicleLocation, int inventoryType,
                              DateTime? lastDescriptionPublicationDate, int? statusbucket,
                              decimal? lastUpdateListPrice, int desiredPhotoCount, decimal lotPrice, decimal msrp, bool isDueForRepricing,
                              decimal? specialPrice, bool hasCurrentBookValue, bool hasCarfax, int? autoLoadStatusTypeId, DateTime? autoloadStatusEndTime,
                              bool noPackages, string bodyStyleName = null, string engineDescription = null, string engineOptionCode = null, bool isStandardEngine = false,
                              bool autoAutoWindowStickerPrinted = false
                              )
        {
            Certified = certified;
            CertifiedID = certifiedId;
	        CertifiedProgramId = certifiedProgramId;
            MileageReceived = mileageReceived;
            _receiveDate = receiveDate;
            StockNumber = stockNumber;
            _tradeOrPurchase = tradeOrPurchase;
            UnitCost = unitCost;
            BusinessUnitID = businessUnitId;
            VIN = (vin ?? "").ToUpperInvariant();
            Year = year;
            Make = make;
            Model = model;
            MarketClass = marketClass;
            SegmentId = segmentId;
            Trim = trim;
            AfterMarketTrim = afterMarketTrim;
            InventoryID = inventoryId;
            IsLowActivity = isLowActivity;
            BaseColor = baseColor;
            Ext1Code = ext1Code;
            Ext1Desc = ext1Desc;
            Ext2Code = ext2Code;
            Ext2Desc = ext2Desc;
            IntCode = intCode;
            IntDesc = intDesc;

            NeedsExterior = (exteriorStatus < CriticalStatusLevel);
            NeedsInterior = (interiorStatus < CriticalStatusLevel);
            PendingApproval = postingStatus == (int) AdvertisementPostingStatus.PendingApproval;
            NeedsPricing = (pricingStatus < CriticalStatusLevel);
            NeedsPhotos = (photoStatus < CriticalStatusLevel);
            NeedsAdReview = (adReviewStatus < CriticalStatusLevel);
            ChromeStyleId = chromeStyleId;
            VehicleCatalogId = vehicleCatalogId;
            VehicleLocation = vehicleLocation;
            InventoryType = inventoryType;
            StatusBucket = (MerchandisingBucket?)statusbucket;
            LastDescriptionPublicationDate = lastDescriptionPublicationDate;
            LastUpdateListPrice = lastUpdateListPrice;
            DesiredPhotoCount = desiredPhotoCount;
            FlListPrice = listPrice;
            ListPrice = (inventoryType == 1 && specialPrice.HasValue) ? specialPrice.Value : listPrice; //if its a new car and we have a special price (they repriced the car) we show that
            LotPrice = lotPrice;
            MSRP = msrp;
            IsDueForRepricing = isDueForRepricing
                                    ? DueForRepricingMode.DueForRepricing
                                    : DueForRepricingMode.NotDueForRepricing;
            SpecialPrice = specialPrice;
            HasCurrentBookValue = hasCurrentBookValue;
            AutoloadStatusTypeId = autoLoadStatusTypeId;
            AutoloadEndTime = autoloadStatusEndTime;
            NoPackages = noPackages;
            HasCarfax = hasCarfax;
            OnlineState = null;
            BodyStyleName = bodyStyleName;
            EngineDescription = engineDescription;
            EngineOptionCode = engineOptionCode;
            IsStandardEngine = isStandardEngine;
            AutoWindowStickerPrinted = autoAutoWindowStickerPrinted;
        }

        public int? ChromeStyleId { get; set; }

        public int? VehicleCatalogId { get; set; }

        public int InventoryID { get; private set; }

        public decimal UnitCost { get; set; }

        public bool Certified { get; set; }

        public string CertifiedID { get; set; }

        private readonly int _tradeOrPurchase;
        public string TradeOrPurchase
        {
            get
            {
                switch(_tradeOrPurchase)
                {
                    case 1:
                        return "Purchased";
                    case 2:
                        return "Trade-in";
                    default:
                        return "?";
                }
            }
        }

        public decimal ListPrice { get; set; }

        public decimal MSRP { get; private set; }

        public decimal? SpecialPrice { get; private set; }

        public decimal LotPrice { get; private set; }

        public decimal FlListPrice { get; private set; }

        public int MileageReceived { get; set; }

        public string DisplayMileage
        {
            get
            {
                if (MileageReceived < 0)
                {
                    return "--";
                }
                return String.Format("{0:n0}", Convert.ToDouble(MileageReceived));
            }
        }

        private string _stockNumber;
        public string StockNumber
        {
            get { return _stockNumber ?? ""; }
            set { _stockNumber = value; }
        }

        private string _vin;
        public string VIN
        {
            get { return _vin ?? ""; }
            set { _vin = value; }
        }

        public int Age
        {
            get
            {
                var ts = DateTime.Today.Subtract(_receiveDate);
                return Math.Max(0, ts.Days);
            }
        }

        private string _year;
        public string Year
        {
            get { return _year ?? ""; }
            set { _year = value; }
        }

        private string _make;
        public string Make
        {
            get { return _make ?? ""; }
            set { _make = value; }
        }

        private string _model;
        public string Model
        {
            get { return _model ?? ""; }
            set { _model = value; }
        }

        public string YearMakeModel
        {
            get { return Year + " " + Make + " " + Model; }
        }

        private string _trim;
        public string Trim
        {
            get { return _trim ?? ""; }
            set { _trim = value; }
        }

        private string _afterMarketTrim;
        public string AfterMarketTrim
        {
            get { return _afterMarketTrim ?? ""; }
            set { _afterMarketTrim = value; }
        }

        private string _marketClass;
        public string MarketClass
        {
            get { return _marketClass ?? ""; }
            set { _marketClass = value; }
        }

        public string GetMilesPerYearDescription
        {
            get { return CalculateMilesPerYearDescription(); }
        }

        private string CalculateMilesPerYearDescription()
        {
            var temp = "";
            switch (GetMilesPerYearCategory())
            {
                case MilesPerYearCategories.VeryLow:
                    temp = "Very Low";
                    break;
                case MilesPerYearCategories.Low:
                    temp = "Low";
                    break;
                case MilesPerYearCategories.Average:
                    temp = "Average";
                    break;
                case MilesPerYearCategories.High:
                    temp = "High";
                    break;
                case MilesPerYearCategories.Excesssive:
                    temp = "Excessive";
                    break;
            }
            return temp + " Mileage";
        }


        public string IdentifierText
        {
            get { return StockNumber + " " + Year + " " + Make + " " + Model; }
        }

        public string FullHeaderText
        {
            get { return Year + " " + Make + " " + Model; }
        }

        public string HeaderText
        {
            get
            {
                string str = FullHeaderText;
                if (str.Length > 22)
                {
                    str = str.Substring(0, 22) + "...";
                }
                return str;
            }
        }

        public bool NeedsAdReview { get; set; }

        public bool HasCurrentBookValue { get; private set; }

        public bool NoPackages { get; private set; }

        public bool NeedsPricing { get; set; }

        public bool NeedsPhotos { get; set; }

        public bool NeedsExterior { get; set; }

        public bool PendingApproval { get; set; }

        public bool AutoWindowStickerPrinted { get; private set; }

        public bool OutdatedPrice { get { return WorkflowFilters.HasOutdatedPrice(LastUpdateListPrice, ListPrice); } }

        public int GetMilesPerYear()
        {
            var day = DateTime.Today;
            var milesPerYear =
                (int) (MileageReceived/((day.Subtract(new DateTime(Int32.Parse(_year) - 1, 9, 1)).Days)/365.0f));
            return milesPerYear;
        }

        public MilesPerYearCategories GetMilesPerYearCategory()
        {
            int milesPerYear = GetMilesPerYear();
            if (milesPerYear < 10000)
            {
                return MilesPerYearCategories.VeryLow;
            }
            if (milesPerYear < 12000)
            {
                return MilesPerYearCategories.Low;
            }
            if (milesPerYear < 15000)
            {
                return MilesPerYearCategories.Average;
            }
            if (milesPerYear < 20000)
            {
                return MilesPerYearCategories.High;
            }
            return MilesPerYearCategories.Excesssive;
        }

        // NOTE: Most (all?) of the methods below need to be factored out to different classes. They
        // deal primarily with data access.


        public static void Refresh(int businessUnitId, int inventoryId)
        {
            var dataAccess = Registry.Resolve<IInventoryDataAccessCache>();
            dataAccess.Clear(businessUnitId, inventoryId);
        }

        public static InventoryData Fetch(int businessUnitId, int inventoryId)
        {
            var dataAccess = Registry.Resolve<IInventoryDataAccessCache>();
            return dataAccess.Fetch(businessUnitId, inventoryId);
        }


        /// <summary>
        /// Returns the number of records found in the search.
        /// </summary>
        public static Int32 WorkflowSearchCount(int businessUnitId, List<InventoryData> entireList, Bin bin)
        {
            if (entireList == null)
            {
                entireList = InventoryDataSource.FetchList(businessUnitId);
            }

            return entireList.Where(bin.Filter).Count();
        }

        // Fantastic, not all of the methods are public :)

        #region TODO: Move these to InventoryOnlineStatusModifier, or something with a better name

        #endregion

        #region TODO: Move these to InventoryDataLinkBuilder

        public string GetEStockLink()
        {
            return GetEStockLink(ConfigurationManager.AppSettings["estock_host_name"]);
        }

        private string GetEStockLink(string estockHost)
        {
            return GetEStockLink(estockHost, _stockNumber, InventoryType == UsedVehicle);
        }

        public static string GetEStockLink(string stkNum, bool isUsed)
        {
            return GetEStockLink(ConfigurationManager.AppSettings["estock_host_name"], stkNum, isUsed);
        }

        public static string GetEStockLink(string estockHost, string stkNum, bool isUsed)
        {
            return string.Format("{0}/IMT/EStock.go?stockNumberOrVin={1}&productMode=edge&inventoryType={2}&isPopup=true",
                                 estockHost, stkNum, isUsed ? "used" : "new");
        }

        public string GetGoogleLink()
        {
            return
                "http://www.google.com/base/s2?a_n0=vehicles&a_y0=9&hl=en&gl=us#/base/s2/ajax?a_n0=vehicles&a_y0=9&start=0&q=" +
                _vin;
        }

        private static PricingContext GetCachedContext(int requestingBusinessUnitId, int inventoryId)
        {
            //bugzid:14554 - Caching due to performance issues with this being called for every vehicle displayed.

            string pricingContextKey = "PricingContext-" + requestingBusinessUnitId + inventoryId;

            //bugzid:14554 - Caching due to performance issues with this being called for every vehicle displayed.
            PricingContext context;
            var cache = Registry.Resolve<ICache>(); // TODO: rip out all of this registry access and use a proper cache.
            var value = cache.Get(pricingContextKey);
            if (value != null)
            {
                context = (PricingContext)value;
            }
            else
            {
                context = PricingContext.FromMerchandising(requestingBusinessUnitId, inventoryId);
                cache.Set(pricingContextKey, context, TimeSpan.FromDays(1));
            }

            return context;
        }

        public static string GetMaxSellingLink(int requestingBusinessUnitId, int inventoryId,
                                    string rootPathForPricingTool)
        {
            return GetPricingLink(requestingBusinessUnitId, inventoryId, rootPathForPricingTool);
        }

        public static string GetPricingLink(int requestingBusinessUnitId, int inventoryId,
                                            string rootPathForPricingTool)
        {
            var retStr = rootPathForPricingTool;
            if (retStr != String.Empty)
            {
                try
                {
                    PricingContext context = GetCachedContext(requestingBusinessUnitId, inventoryId);

                    retStr += "?oh=" + context.OwnerHandle + "&vh=" + context.VehicleHandle + "&sh=" + context.SearchHandle;
                    return retStr;
                }
                catch
                {
                    return string.Empty;
                }
            }

            return string.Empty;
        }


        #endregion

        public bool IsPriceReduced
        {
            get
            {
                return ListPrice > 0 && HighInternetPrice.HasValue && HighInternetPrice > ListPrice;
            }
        }
        public bool IsPriceReducedEnough(int minGoodPriceReduction)
        {            
            return HighInternetPrice.HasValue && 
                    (ListPrice > 0) && 
                    (HighInternetPrice - ListPrice) > minGoodPriceReduction;
        }
        private int? _highInternetPrice;
	    private int? _certifiedProgramId;

	    public int? HighInternetPrice
        {
            get
            {
                if (!_highInternetPrice.HasValue)
                {
                    // TODO: Extract this to IInventoryDataSource / Lazy
                    using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
                    {

                        con.Open();
                        
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "builder.HighInternetPrice#Select";
                            cmd.AddRequiredParameter("InventoryId", InventoryID, DbType.Int32);
                            try
                            {
                                _highInternetPrice = (int?) Database.GetNullableDecimal(cmd.ExecuteScalar());
                            }catch
                            {
                                _highInternetPrice = null;
                            }
                        }                    
                    }
                }
                return _highInternetPrice;
            }
        }

        public bool HasCarfax { get; private set; }
        
        public int? AutoloadStatusTypeId { get; private set; }

        public DateTime? AutoloadEndTime { get; private set; }

        public int? OnlineState { get; set; }

        public string BodyStyleName { get; set; }
        public string EngineDescription { get; set; }
        public string EngineOptionCode { get; set; }
        public bool IsStandardEngine { get; set; }


	    private ManufacturerCertification _certification;
        private string _certifiedProgramMarketingName;

        private ManufacturerCertification CertificationInfo
	    {
            get
            {
                return _certification ?? (_certification = ManufacturerCertification.Fetch(BusinessUnitID, InventoryID, Make));
            }
	    }

	    public int? CertifiedProgramId
	    {
		    get
		    {
				return _certifiedProgramId ?? (CertificationInfo != null? CertificationInfo.ProgramId : default (int?));
		    }
		    set { _certifiedProgramId = value; }
	    }

        public string CertifiedProgramMarketingName
        {
            get { return _certifiedProgramMarketingName ?? (CertificationInfo != null ? CertificationInfo.MarketingName : string.Empty); }
            set { _certifiedProgramMarketingName = value; }
        }

        public bool CertifiedProgramIsPersisted
	    {
		    get
		    {
			    return CertificationInfo == null || CertificationInfo.ProgramIsPersisted;
		    }
	    }
    }
}