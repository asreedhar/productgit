using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
//using FirstLook.Common.Core.PhotoServices.WebService.V1.Model;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.Activity.InventoryCount;
using FirstLook.Merchandising.DomainModel.MaxAnalytics.OnlineModel;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;
using log4net;
using MAX.Caching;
using Merchandising.Messages.AutoApprove;
using MvcMiniProfiler;
using FirstLook.Merchandising.DomainModel.Workflow.PriceComparisonsThrehold;

namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    public enum MerchandisingBucket
    {
        Invalid = -1,
        DoNotPost = 8,
        NotPostedNeedsAttention = 16,
        PendingApproval = 32,
        NotPosted = NotPostedNeedsAttention | PendingApproval,
        OnlineNeedsAttention = 64,
        Online = 128,
        AllOnline = OnlineNeedsAttention | Online,
        All = NotPostedNeedsAttention | PendingApproval | OnlineNeedsAttention | Online
    }

    public enum InventoryFilterMode
    {
        NeedsNone = 0,
        NeedsPricing = 1,
        NeedsDescriptionAttention = 2,
        NeedsPhotos = 4,
        ShowAllActivityFilters = NeedsPricing | NeedsDescriptionAttention | NeedsPhotos,
        LowActivity = 16
    }

    public enum PricingRiskFilterMode
    {
        OverPriced = 0,
        UnderPriced,
        AtMarket,
        None,
        Any
    }

    public enum DueForRepricingMode
    {
        DueForRepricing = 0,
        NotDueForRepricing,
        Any,
        None
    }

    /// <summary>
    /// A list of the possible Outdated Price filters for inventory lists.
    /// </summary>
    public enum OutdatedPriceFilterMode
    {
        None,
        Outdated,
        NotOutdated
    }

    public enum InventoryCountItem
    {
        Undefined = 0,
        Total,
        Online,
        OnlineNeedAction,
        WebLoadingIncomplete,
        NeedsPhotos,
        NeedsEquipment,
        NeedsPricing,
        NoPrice,
        NoDescription,
        PendingApproval,
        LowActivity,
        DoNotPost,
        UnderMerchandisedDescription,
        UnderMerchandisedPhotos,
        UnderMerchandisedPricing,
        OverdueEquipment,
        OverduePhotos,
        OverduePricing,
        OverdueToPostOnline,
        OverdueStillInPendingApproval
    }

    public class InventoryDataSource
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public List<InventoryData> WorkflowInventorySubsetSelect(int businessUnitID, int startIndex, int pageSize, string sortExpression, Bin bin)
        {
            var allInventory = InventoryDataFilter.FetchListFromSession(businessUnitID);
            return InventoryDataFilter.WorkflowSearch(businessUnitID, allInventory, startIndex, pageSize, sortExpression, bin);
        }

        public List<DestinationSetting> EdtDestinationsSelect(int businessUnitID)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {

                    cmd.CommandText = "Release.ListingSitePreferences#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OnlyActiveReleaseSites", true, DbType.Boolean);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<DestinationSetting> retList = new List<DestinationSetting>();
                        while (reader.Read())
                        {
                            retList.Add(new DestinationSetting(reader));
                        }
                        return retList;
                    }
                }
            }
        }
        public DataTable TrimHelpFetch(string vin)
        {
            int vinPatternId = ChromeStyle.GetVinPatternIdForVin(vin);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {

                    cmd.CommandText = "Chrome.getDifferentiatedCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "VinPatternId", vinPatternId, DbType.Int32);

                    var dr = cmd.ExecuteReader();

                    var dt = new DataTable();

                    dt.Load(dr);

                    return dt;
                }
            }
        }
        
        /// <summary>
        /// returns string for stockNumber, int for destination IDs
        /// </summary>
        /// <param name="businessUnitID"></param>
        /// <returns></returns>
        public List<VehicleDestinationSetting> GetVehicleDisabledEdts(int businessUnitID)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {

                    cmd.CommandText = @"select ed.destinationID, inventoryId, cast(destinationEnabled as bit) as activeFlag, description 
                                        from postings.VehicleAdScheduling vas
                                        inner join settings.edtDestinations ed on ed.destinationID = vas.destinationid

                                        WHERE businessUnitID = @BusinessUnitID AND scheduledReleaseTime IS NULL
                                        ORDER BY inventoryId
                                        ";
                    cmd.CommandType = CommandType.Text;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    List<VehicleDestinationSetting> retList = new List<VehicleDestinationSetting>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        
                        while (reader != null && reader.Read())
                        {
                            retList.Add(new VehicleDestinationSetting(reader));
                        }
                        return retList;
                    }
                    
                }
            }
        }

        public int GetCountBetterThanBookDifference(int businessUnitID, decimal goodBookDifferential)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getVehicleCountBelowBook";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "@minAmountBelowBookToInclude", goodBookDifferential, DbType.Decimal);
                    
                    int ret = Convert.ToInt32(cmd.ExecuteScalar());
                    return ret;
                
                }
            }
        }
        public int GetCountBelowTargetPrice(int businessUnitID, decimal targetPrice)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getVehicleCountBelowPrice";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "TargetPrice", targetPrice, DbType.Decimal);
                    Database.AddRequiredParameter(cmd, "MinPrice", 100.0M, DbType.Decimal);

                    int ret = Convert.ToInt32(cmd.ExecuteScalar());
                    return ret;
                
                }
            }
        }


        public static int CreateStaging(int businessUnitId, string vin, string stockNumber)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "staging.vehicle#Create";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter("VIN", vin, DbType.String);
                    cmd.AddRequiredParameter("StockNumber", stockNumber, DbType.String);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        //move to second result set (first has vin pattern id in it)
                        if (reader.NextResult() && reader.Read())
                        {
                            return (int) reader[0];
                        }
                        throw new ApplicationException("Invalid VIN.  Please enter another.");
                        
                    }
                    
                }
            }
            
        }

        public static List<InventoryData> FetchList( int businessUnitId, int usedNewFilter = 0)
        {
            var photoServices = Registry.Resolve<IPhotoServices>();

            using (MiniProfiler.Current.Step("InventoryData.FetchList(...)"))
            {
                var photoCountsTask = GetVinPhotoCountsForBusinessUnitAsync(businessUnitId, photoServices);

                List<InventoryData> retList;

                using (var con = Database.GetOpenConnection(Database.MerchandisingDatabase))
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "builder.Inventory#Fetch";

                    cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter("inventoryType", usedNewFilter, DbType.Int32);
					 
                    retList = ReadInventoryDataRecords(businessUnitId, cmd);
                }

                // Please don't ever make all of these queries (including remote web service calls!) while keeping
                // your db connection open - as in the previous version of this file. ZB 5/22/14
                var applyPhotoCountsList = retList;

                var photoCountsMergeTask = photoCountsTask.ContinueWith(task => ApplyPhotoCountsToInventoryList(task.Result, applyPhotoCountsList));

                ApplyChromeStyleIdCalculations(retList);

                ApplyAgeBucketToInventoryList(retList, businessUnitId);

                // call max analytics and get the low activity vehicles here
				// NOTE: Makes a remote web service call, this needs to be cached!
				ApplyLowActivityFlagFromMaxAnalytics(retList, businessUnitId, usedNewFilter);

                ApplyIsFavourableFlagToInventoryList(retList,businessUnitId,usedNewFilter);

                using (MiniProfiler.Current.Step("Waiting for GetPhotoUrlsByBusinessUnit(...)"))
                {
                    photoCountsMergeTask.Wait();
                }

                // We need constant order for when we page through cars in the workflow selector and for the home page initial state
                using (MiniProfiler.Current.Step("Sorting InventoryData"))
                {
                    retList = retList.OrderByDescending(x => x.Age).ThenBy(x => x.InventoryID).ToList();
                }

                return retList;
            }
        }

        private static void ApplyLowActivityFlagFromMaxAnalytics(IEnumerable<InventoryData> retList, int businessUnitId, int usedNewFilter)
        {
            Log.DebugFormat("ApplyLowActivityFlagFromMaxAnalytics starting...");
            using (MiniProfiler.Current.Step("ApplyLowActivityFlagFromMaxAnalytics"))
            {
                var memberContext = Registry.Resolve<IMemberContext>();
                var dashboardBusinessUnitRepository = Registry.Resolve<IDashboardBusinessUnitRepository>();
                var dataOnlineRepository = Registry.Resolve<IDataOnlineRepository>();
                var inventoryCountWebService = Registry.Resolve<IInventoryCountWebService>();

                // Needs to be injected so we can cache.
                var analyticsWrapper = new AnalyticsWrapper(memberContext, dashboardBusinessUnitRepository,
                    dataOnlineRepository, inventoryCountWebService);

                var inventoryActivityStats = analyticsWrapper.GetLowActivity(businessUnitId, usedNewFilter, 0);

                foreach (var inv in retList)
                {
                    InventoryData inv1 = inv; // avoid access to foreach variable in closure
                    inv.IsLowActivity = inventoryActivityStats.Any(x => x.VIN == inv1.VIN && x.LowActivity);
                }
            }
            Log.DebugFormat("ApplyLowActivityFlagFromMaxAnalytics finished.");

        }

        private static List<InventoryData> ReadInventoryDataRecords(int businessUnitId, IDbCommand cmd)
        {
            var retList = new List<InventoryData>();

            using (MiniProfiler.Current.Step("builder.Inventory#Fetch"))
            using (var reader = cmd.ExecuteReader())
            {
                var inventoryReader = new InventoryDataReader(reader, businessUnitId);

                while (reader.Read())
                    retList.Add(inventoryReader.ReadRecord());
            }
            return retList;
        }

        private static void ApplyAgeBucketToInventoryList(IEnumerable<InventoryData> retList, int businessUnitId)
        {
            var ageBucketRepository = Registry.Resolve<IAgeBucketRepository>();
            var ageBuckets = ageBucketRepository.GetBucketsForBusinessUnit(businessUnitId);

            foreach(var inv in retList)
            {
                inv.AgeBucketId = ageBuckets.GetBucketIdForAge(inv.Age);
            }
        }

        private static void ApplyChromeStyleIdCalculations(IEnumerable<InventoryData> retList)
        {
            using (MiniProfiler.Current.Step("ApplyChromeStyleIdCalculations(...)"))
            {
                var mapper = Registry.Resolve<IChromeMapper>();

                foreach (var inv in retList)
                {
                    inv.ChromeStyleId =
                        (inv.ChromeStyleId == -1 ? null : inv.ChromeStyleId)
                        ?? mapper.LookupChromeStyleIdByVehicleCatalogId(inv.VehicleCatalogId)
                        ?? mapper.LookupChromeStyleIdByVin(inv.VIN)
                        ?? -1;
                }
            }
        }

        private static void ApplyPhotoCountsToInventoryList(IDictionary<String, int> photoCounts, IEnumerable<InventoryData> inventoryList)
        {
            foreach (var inventory in inventoryList)
                inventory.PhotoCount = photoCounts.ContainsKey(inventory.VIN) ? photoCounts[inventory.VIN] : 0;
        }

        private static void ApplyIsFavourableFlagToInventoryList(IEnumerable<InventoryData> retList,int businessUnitId,int inventoryType)
        {
            var PriceComparisonsRepository = Registry.Resolve<IPriceComparisonsRepository>();
            try
            {
                var priceComparisonThresholds = PriceComparisonsRepository.GetPriceComparisonsAndDealerThresholds(businessUnitId, inventoryType);
                priceComparisonThresholds.SetFavourableOrNot(retList);
            }
            catch (Exception e)
            {
                // BugzId: 33947 - Some dealers do not have an owner handle.
                Log.Error("Unable to apply price comparison thresholds", e);
            }
        }

        private static Task<IDictionary<String, int>> GetVinPhotoCountsForBusinessUnitAsync(int businessUnitId, IPhotoServices photoServices)
        {
            try
            {
                return Task.Factory.StartNew(() => photoServices.GetVinPhotoCountsForBusinessUnit(businessUnitId, TimeSpan.FromSeconds(30)));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new Task<IDictionary<String, int>>(() => new Dictionary<String, int>());
            }
        }

        public static void MoveToReleasable(int businessUnitId, int inventoryId, string memberLogin,
            bool userInitiated = true, IAdMessageSender messageSender = null)
        {
            using (var con = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "builder.VehicleConfiguration#Releasable";
                cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);
                cmd.AddRequiredParameter("MemberLogin", memberLogin, DbType.String);
                cmd.AddRequiredParameter("UserInitiated", userInitiated, DbType.Boolean);

                cmd.ExecuteNonQuery();
            }

            // This really doesn't belong here.  This is workflow logic, inside what should be part of a repository or DAO. 
            messageSender = messageSender ?? Registry.Resolve<IAdMessageSender>();
            messageSender.SendAutoApproval(
                new AutoApproveMessage(businessUnitId, inventoryId, memberLogin), MethodBase.GetCurrentMethod().DeclaringType.FullName);
        }

        public static void MoveOffline(int businessUnitId, int inventoryId, string memberLogin,
            bool userInitiated = true)
        {
            using (var con = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "postings.moveOffline";
                cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);
                cmd.AddRequiredParameter("MemberLogin", memberLogin, DbType.String);
                cmd.AddRequiredParameter("UserInitiated", userInitiated, DbType.Boolean);

                cmd.ExecuteNonQuery();
            }
        }
    }

}