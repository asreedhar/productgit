using System;
using System.Data;
using System.Globalization;
using System.Web;

namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    internal class InventoryDataReader
    {
        private readonly int stockNumberField;
        private readonly int vinField;
        private readonly int unitCostField;
        private readonly int certifiedField;
        private readonly int certifiedIdField;
		private readonly int certifiedProgramIdField;
		private readonly int mileageReceivedField;
        private readonly int tradeOrPurchaseField;
        private readonly int listPriceField;
        private readonly int inventoryReceivedDateField;
        private readonly int vehicleYearField;
        private readonly int makeField;
        private readonly int modelField;
        private readonly int marketClassField;
        private readonly int segmentIdField;
        private readonly int trimField;
        private readonly int afterMarketTrimField;
        private readonly int inventoryIdField;
        private readonly int lowActivityFlagField;
        private readonly int exteriorStatusField;
        private readonly int pricingStatusField;
        private readonly int interiorStatusField;
        private readonly int photoStatusField;
        private readonly int adReviewStatusField;
        private readonly int postingStatusField;
        
        private readonly int baseColorField;
        private readonly int extColor1Field;
        private readonly int exteriorColorCodeField;
        private readonly int extColor2Field;
        private readonly int exteriorColorCode2Field;
        private readonly int intColorField;
        private readonly int interiorColorCodeField;

        private readonly int chromeStyleIdField;
        private readonly int vehicleCatalogIdField;
        private readonly int vehicleLocationField;
        private readonly int inventoryTypeField;
        private readonly int datePostedField;
        private readonly int statusBucketField;
        private readonly int lastUpdateListPriceField;
        private readonly int desiredPhotoCountField;
        private readonly int lotPriceField;
        private readonly int msrpField;
        private readonly int dueForRepricingField;
        private readonly int specialPriceField;
        private readonly int hasCurrentBookValueField;
        private readonly int hasCarfaxField;
        private readonly int hasCarFaxAsAdminField;
        private readonly int autoloadStatusTypeIdField;
        private readonly int autoloadEndTimeField;
        private readonly int noPackagesField;
        private readonly int autoWindowStickerPrintedField;


        private readonly IDataReader r;
        private readonly int businessUnitId;
        private readonly bool isUserAdmin;

        public InventoryDataReader(IDataReader r, int businessUnitId)
            : this(r, businessUnitId, IsCurrentHttpContextUserAdmin())
        {
        }

        private static bool IsCurrentHttpContextUserAdmin()
        {
            return HttpContext.Current != null && HttpContext.Current.User != null &&
                   HttpContext.Current.User.IsInRole("Administrator");
        }

        public InventoryDataReader(IDataReader r, int businessUnitId, bool isUserAdmin)
        {
            stockNumberField = r.GetOrdinal("StockNumber");
            vinField = r.GetOrdinal("VIN");
            unitCostField = r.GetOrdinal("UnitCost");
            certifiedField = r.GetOrdinal("Certified");
            certifiedIdField = r.GetOrdinal("CertifiedID");
	        certifiedProgramIdField = r.GetOrdinal("CertifiedProgramId");
            mileageReceivedField = r.GetOrdinal("MileageReceived");
            tradeOrPurchaseField = r.GetOrdinal("TradeOrPurchase");
            listPriceField = r.GetOrdinal("ListPrice");
            inventoryReceivedDateField = r.GetOrdinal("InventoryReceivedDate");
            vehicleYearField = r.GetOrdinal("VehicleYear");
            makeField = r.GetOrdinal("Make");
            modelField = r.GetOrdinal("Model");
            marketClassField = r.GetOrdinal("marketClass");
            segmentIdField = r.GetOrdinal("segmentId");
            trimField = r.GetOrdinal("trim");
            afterMarketTrimField = r.GetOrdinal( "afterMarketTrim" );
            inventoryIdField = r.GetOrdinal("inventoryID");
            lowActivityFlagField = r.GetOrdinal("lowActivityFlag");
            exteriorStatusField = r.GetOrdinal("exteriorStatus");
            pricingStatusField = r.GetOrdinal("pricingStatus");
            interiorStatusField = r.GetOrdinal("interiorStatus");
            photoStatusField = r.GetOrdinal("photoStatus");
            adReviewStatusField = r.GetOrdinal("adReviewStatus");
            postingStatusField = r.GetOrdinal("postingStatus");
            
            baseColorField = r.GetOrdinal("baseColor");
            extColor1Field = r.GetOrdinal("ExtColor1");
            exteriorColorCodeField = r.GetOrdinal("ExteriorColorCode");
            extColor2Field = r.GetOrdinal("ExtColor2");
            exteriorColorCode2Field = r.GetOrdinal("ExteriorColorCode2");
            intColorField = r.GetOrdinal("IntColor");
            interiorColorCodeField = r.GetOrdinal("InteriorColorCode");

            chromeStyleIdField = r.GetOrdinal("chromeStyleId");
            vehicleCatalogIdField = r.GetOrdinal("vehicleCatalogId");
            vehicleLocationField = r.GetOrdinal("vehicleLocation");
            inventoryTypeField = r.GetOrdinal("inventoryType");
            datePostedField = r.GetOrdinal("datePosted");
            statusBucketField = r.GetOrdinal("statusBucket");
            lastUpdateListPriceField = r.GetOrdinal("lastUpdateListPrice");
            desiredPhotoCountField = r.GetOrdinal("desiredPhotoCount");
            lotPriceField = r.GetOrdinal("LotPrice");
            msrpField = r.GetOrdinal("MSRP");
            dueForRepricingField = r.GetOrdinal("DueForRepricing");
            specialPriceField = r.GetOrdinal("SpecialPrice");
            hasCurrentBookValueField = r.GetOrdinal("HasCurrentBookValue");
            autoloadStatusTypeIdField = r.GetOrdinal( "autoloadStatusTypeId");
            autoloadEndTimeField = r.GetOrdinal("autoloadEndTime");
            noPackagesField = r.GetOrdinal("NoPackages");
            hasCarfaxField = r.GetOrdinal("HasCarfax");
            hasCarFaxAsAdminField = r.GetOrdinal("HasCarfaxAsAdmin");
            autoWindowStickerPrintedField = r.GetOrdinal("AutoWindowStickerPrinted");

            this.r = r;
            this.businessUnitId = businessUnitId;
            this.isUserAdmin = isUserAdmin;
        }

        public InventoryData ReadRecord()
        {
            return new InventoryData(
                r.GetString(stockNumberField),
                businessUnitId,
                r.IsDBNull(vinField) ? "" : r.GetString(vinField),
                r.GetDecimal(unitCostField),
                r.GetByte(certifiedField) != 0,
                r.IsDBNull(certifiedIdField) ? "" : r.GetString(certifiedIdField),
				r.IsDBNull(certifiedProgramIdField) ? default(int?) : r.GetInt32(certifiedProgramIdField),
				r.IsDBNull(mileageReceivedField) ? 0 : r.GetInt32(mileageReceivedField),
                r.GetByte(tradeOrPurchaseField),
                r.IsDBNull(listPriceField) ? 0.0m : r.GetDecimal(listPriceField),
                r.GetDateTime(inventoryReceivedDateField),
                r.IsDBNull(vehicleYearField) ? "" : r.GetInt32(vehicleYearField).ToString(CultureInfo.InvariantCulture),
                r.IsDBNull(makeField) ? "" : r.GetString(makeField),
                r.IsDBNull(modelField) ? "" : r.GetString(modelField),
                r.IsDBNull(marketClassField) ? "" : r.GetString(marketClassField),
                r.IsDBNull(segmentIdField) ? (int)Segments.Unknown : r.GetByte(segmentIdField),
                r.IsDBNull(trimField) ? "" : r.GetString(trimField),
                r.IsDBNull( afterMarketTrimField ) ? "" : r.GetString( afterMarketTrimField ),
                r.GetInt32(inventoryIdField),
                !r.IsDBNull(lowActivityFlagField) && r.GetBoolean(lowActivityFlagField),
                r.GetInt32(exteriorStatusField),
                r.GetInt32(pricingStatusField),
                r.GetInt32(interiorStatusField),
                r.GetInt32(photoStatusField),
                r.GetInt32(adReviewStatusField),
                r.GetInt32(postingStatusField),

                r.IsDBNull(baseColorField) ? "" : r.GetString(baseColorField),
                r.IsDBNull(exteriorColorCodeField) ? "" : r.GetString(exteriorColorCodeField),
                r.IsDBNull(extColor1Field) ? "" : r.GetString(extColor1Field),
                r.IsDBNull(exteriorColorCode2Field) ? "" : r.GetString(exteriorColorCode2Field),
                r.IsDBNull(extColor2Field) ? "" : r.GetString(extColor2Field),
                r.IsDBNull(interiorColorCodeField) ? "" : r.GetString(interiorColorCodeField),
                r.IsDBNull(intColorField) ? "" : r.GetString(intColorField),

                r.IsDBNull(chromeStyleIdField) ? (int?) null : r.GetInt32(chromeStyleIdField),
                r.IsDBNull(vehicleCatalogIdField) ? (int?) null : r.GetInt32(vehicleCatalogIdField),
                r.IsDBNull(vehicleLocationField) ? "" : r.GetString(vehicleLocationField),
                r.GetByte(inventoryTypeField),
                r.IsDBNull(datePostedField) ? (DateTime?) null : r.GetDateTime(datePostedField),
                r.IsDBNull(statusBucketField) ? (int?) null : r.GetInt32(statusBucketField),
                r.IsDBNull(lastUpdateListPriceField) ? (decimal?) null : r.GetDecimal(lastUpdateListPriceField),
                r.IsDBNull(desiredPhotoCountField) ? 0 : r.GetInt32(desiredPhotoCountField),
                r.IsDBNull(lotPriceField) ? 0.0m : r.GetDecimal(lotPriceField),
                r.IsDBNull(msrpField) ? 0.0m : r.GetDecimal(msrpField),
                r.GetBoolean(dueForRepricingField),
                r.IsDBNull(specialPriceField) ? (decimal?) null : r.GetDecimal(specialPriceField),
                !r.IsDBNull(hasCurrentBookValueField) && r.GetBoolean(hasCurrentBookValueField),
                (!r.IsDBNull(hasCarfaxField) && r.GetBoolean(hasCarfaxField)) 
                    || (isUserAdmin && !r.IsDBNull(hasCarFaxAsAdminField) && r.GetBoolean(hasCarFaxAsAdminField)),
                r.IsDBNull(autoloadStatusTypeIdField) ? (int?) null : r.GetInt32(autoloadStatusTypeIdField),
                r.IsDBNull(autoloadEndTimeField) ? (DateTime?) null : r.GetDateTime(autoloadEndTimeField),
                !r.IsDBNull(noPackagesField) && r.GetBoolean(noPackagesField),
                null, // bodyStyleName
                null, // engineDescription
                null, // engineOptionCode
                false, // isStandardEngine
                !r.IsDBNull(autoWindowStickerPrintedField) && r.GetBoolean(autoWindowStickerPrintedField)
                );
        }
    }
}