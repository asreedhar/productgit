using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    /// <summary>
    /// This is effectively a filter. It can find specific InventoryData instances that meet different 
    /// criteria. It also has some methods for explicitly caching the filtered list of InventoryData.
    /// </summary>
    public class InventoryDataFilter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string AllInventoryCacheKey = "AllInventory";
        [Serializable]
        private class InventorySessionData
        {
            public int BusinessUnitId { get; set; }
            public List<InventoryData> Data { get; set; }
        }

        public static void UpdateSessionData(int businessUnitId)
        {
            ClearListFromSession();
            FetchListFromSession(businessUnitId);
        }

        public static List<InventoryData> FetchListFromSession(int businessUnitId)
        {
            Log.Debug("Fetching Inventory List from session.");
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["memcache"] = businessUnitId.ToString();
            InventorySessionData data = null;


            if (HttpContext.Current != null)
                data = (InventorySessionData)HttpContext.Current.Session[AllInventoryCacheKey];
            else
            {
                BinaryFormatter binaryFormatter = null;
                MemoryStream sdata = null;

                binaryFormatter = new BinaryFormatter();
                try
                {
                    using (sdata)
                    {
                        sdata =
                            MerchandisingAnalyticsClient.GetReportData<MemoryStream>(
                                MerchandisingAnalyticsClient.GetParameterList(parameterValues));

                        if (sdata != null)
                        {
                            sdata.Position = 0;
                            data = (InventorySessionData)binaryFormatter.Deserialize(sdata);
                            sdata.Close();
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.Error("An error occured while implementing Memcached Client", e);
                }
            }
            if (data == null || data.BusinessUnitId != businessUnitId)
            {
                Log.Debug("Inventory List not found in session. Fetching fresh data.");
                data = new InventorySessionData
                {
                    BusinessUnitId = businessUnitId,
                    Data = InventoryDataSource.FetchList(businessUnitId)
                };

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    BinaryFormatter binaryFormatter2 = new BinaryFormatter();
                    binaryFormatter2.Serialize(memoryStream, data);
                    try
                    {
                        MerchandisingAnalyticsClient.SetReportData(MerchandisingAnalyticsClient.GetParameterList(parameterValues), memoryStream);
                        memoryStream.Close();
                        if (HttpContext.Current != null)
                            HttpContext.Current.Session[AllInventoryCacheKey] = data;
                    }
                    catch (Exception e)
                    {
                        Log.Error("An error occured while implementing Memcached Client", e);
                    }

                }
            }

            return data.Data;
        }

        public static void ClearListFromSession(int businessUnitId = 0)
        {
            Log.Debug("Clearing Inventory List from session.");

            if (HttpContext.Current != null)
                HttpContext.Current.Session.Remove(AllInventoryCacheKey);
            else
            {
                var cacheKey = string.Format("BusinessUnitInventoryKey={0}", businessUnitId);
                var cache = Registry.Resolve<ICache>();

                if (cache != null)
                    cache.Delete(cacheKey);
            }
        }

        public static List<IInventoryData> WorkflowSearch(int businessUnitId, List<InventoryData> entireList, int startIndex, int pageSize, SortType type, SortDirection direction, Bin bin)
        {
            if (entireList == null)
            {
                entireList = InventoryDataSource.FetchList(businessUnitId);
            }

            var filteredResults = entireList.Where(bin.Filter).ToList();

            // TODO: Inject this.
            IInventoryDataSorter inventoryDataSorter = new InventoryDataSorter();

            var sortedResults = inventoryDataSorter.SortInventory(filteredResults, type, direction);

            //Limit number of cars returned to the pageSize
            var finalList = new List<IInventoryData>();
            for (var i = startIndex; i < startIndex + Math.Min(pageSize, sortedResults.Count - startIndex); i++)
            {
                finalList.Add(sortedResults[i]);
            }

            return finalList;
        }

        [Obsolete("Use the new enum based sort type & expression method instead of this", false)]
        public static List<InventoryData> WorkflowSearch(int businessUnitId, List<InventoryData> entireList, int startIndex, int pageSize, string sortExpression, Bin bin)
        {
            // TODO: update dependencies to use the new enum based WorkflowSearch method instead of this one
            // the following is straight from the old InventoryDataSorter and is being used as an adapter to invoke the new explicit WorkflowSearch. D:
            string[] splits = sortExpression.Split(' ');
            var sortDirection = (splits.Length > 1 ? SortDirection.Ascending : SortDirection.Descending); // TODO: this is so bad and wrong and it was like this before I got here
            SortType sortType;
            try
            {
                sortType = EnumHelper.GetEnumFromDescription<SortType>(splits[0]);
            }
            catch (ArgumentException ex)
            {
                sortType = SortType.None;
            }

            var result =
                WorkflowSearch(businessUnitId, entireList, startIndex, pageSize, sortType, sortDirection, bin)
                    .Select(res => res as InventoryData)
                    .ToList();
            return result;
        }

    }

}