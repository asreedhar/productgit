using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    internal class InventoryDataSessionCachedRepository : IInventoryDataSessionCachedRepository
    {
        public IEnumerable<IInventoryData> GetAllInventory(int businessUnitId)
        {
            return InventoryDataFilter.FetchListFromSession(businessUnitId);
        }

        public void ClearSessionData()
        {
            InventoryDataFilter.ClearListFromSession();
        }
    }
}