using System;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    public class ApproveForRelease
    {
        public static void Run(int businessUnitId, int inventoryId,
            DateTime releaseDate, string approverLogin, string merchandisingDescription,string footer,
            string equipmentList, decimal listPrice, decimal? specialPrice, DateTime? expirationDate,string exteriorColor, string interiorColor,
            string interiorType, string mappedOptionIds, string optionCodes, bool autoRegenerate, int msrp, int? manufacturerRebate, int? dealerDiscount)
        {
            Run(businessUnitId, inventoryId, releaseDate, approverLogin, merchandisingDescription,
                footer, equipmentList, listPrice, specialPrice, expirationDate, exteriorColor, interiorColor, interiorType, mappedOptionIds, optionCodes, String.Empty, String.Empty, 
                autoRegenerate, msrp, manufacturerRebate, dealerDiscount);
            
        }

        private static void Run(int businessUnitId, int inventoryId, 
            DateTime releaseDate, string approverLogin, string merchandisingDescription,string footer,
            string equipmentList, decimal listPrice, decimal? specialPrice, DateTime? expirationDate,string exteriorColor, string interiorColor,
            string interiorType, string mappedOptionIds, string optionCodes, string highlightCalloutText, string seoKeywords, bool autoRegenerate, 
            int msrp, int? manufacturerRebate, int? dealerDiscount)
        {
            if (releaseDate < DateTime.Now )
            {
                releaseDate = DateTime.Now;
            }
            StepStatusCollection.Fetch(businessUnitId, inventoryId)
                .SetPostingStatus(AdvertisementPostingStatus.NoActionRequired)
                .Save(approverLogin);
            
            PostingSchedule.CreateOrUpdate(businessUnitId, inventoryId, releaseDate, 
                approverLogin, merchandisingDescription, footer, equipmentList, listPrice, specialPrice, expirationDate,
                exteriorColor, interiorColor, interiorType, mappedOptionIds, optionCodes, highlightCalloutText, seoKeywords,
                autoRegenerate, msrp, manufacturerRebate, dealerDiscount);
            
        }
    }
}