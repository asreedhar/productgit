using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    internal class InventoryDataRepository : IInventoryDataRepository
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<InventoryDataRepository>();

        public IEnumerable<IInventoryData> GetAllInventory(int businessUnitId)
        {
            if(Log.IsInfoEnabled)
                Log.Info("Started InventoryDataRepository.GetAllInventory()");

            var t = DateTimeOffset.UtcNow;

            var data = InventoryDataSource.FetchList(businessUnitId);

            if (Log.IsInfoEnabled)
                Log.InfoFormat("Completed InventoryDataRepository.GetAllInventory(). ElapsedMs={0:0.0} FetchCount={1}",
                               (DateTimeOffset.UtcNow - t).TotalMilliseconds, data.Count);

            return data;
        }
    }
}