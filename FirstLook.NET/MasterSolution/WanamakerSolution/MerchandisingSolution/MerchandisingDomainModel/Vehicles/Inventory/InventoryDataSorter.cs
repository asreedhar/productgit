using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Helpers;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.Entities.Enumerations;

namespace FirstLook.Merchandising.DomainModel.Vehicles.Inventory
{
    public interface IInventoryDataSorter
    {
        /// <summary>
        /// Sorts the passed-in list of Inventory Data.
        /// </summary>
        /// <param name="inventoryList">The list of Inventory Data to be sorted.</param>
        /// <param name="sortExpression">The column and direction to base the sort on.</param>
        /// <returns>The sorted list of Inventory Data.</returns>
        ///
        /// 
        /// <summary>
        /// Sorts the passed-in list of Inventory Data.
        /// </summary>
        /// <param name="inventoryList"></param>
        /// <param name="type"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        List<IInventoryData> SortInventory(IEnumerable<IInventoryData> inventoryList, SortType type, SortDirection direction);
    }
    [Description("Inventory sort types")]
    public enum SortType
    {
        None = 0,
        Certified,
        InventoryReceivedDate,
        ListPrice,
        [Description("make")] // used for conversion from legacy strings to our enum via EnumHelper
        Make,
        [Description("marketClassID")]
        MarketClassId,
        [Description("model")]
        Model,
        MileageReceived,
        StepsComplete,
        StockNumber,
        Trim,
        TradeOrPurchase,
        UnitCost,
        [Description("vehicleYear")]
        VehicleYear
    }
    public class InventoryDataSorter : IInventoryDataSorter
    {
        /// <summary>
        /// Sorts the passed-in list of Inventory Data.
        /// </summary>
        /// <param name="id">The list of Inventory Data to be sorted.</param>
        /// <param name="type">The data field to sort on.</param>
        /// <param name="direction">The direction to sort in.</param>
        /// <returns>The sorted list of Inventory Data.</returns>
        public List<IInventoryData> SortInventory(IEnumerable<IInventoryData> id, SortType type, SortDirection direction = SortDirection.Ascending)
        {
            if (SortType.None.Equals(type))
                return id.ToList();

            switch (direction)
            {
                case SortDirection.Ascending:
                    return SortAscending(id, type).ToList();
                case SortDirection.Descending:
                    return SortDescending(id, type).ToList();
            }
            return id.ToList();
        }

        private static IOrderedEnumerable<IInventoryData> SortAscending(IEnumerable<IInventoryData> id, SortType type)
        {
            switch (type)
            {
                case SortType.InventoryReceivedDate:
                    var ordered = id.OrderBy(d => d.ReceivedDate);
                    return ordered;
                case SortType.VehicleYear:
                    ordered = id.OrderBy(d => d.Year);
                    return ordered;
                case SortType.Make:
                    ordered = id.OrderBy(d => d.Make);
                    return ordered;
                case SortType.Model:
                    ordered = id.OrderBy(d => d.Model);
                    return ordered;
                case SortType.Trim:
                    ordered = id.OrderBy(d => d.Trim);
                    return ordered;
                case SortType.MileageReceived:
                    ordered = id.OrderBy(d => d.MileageReceived);
                    return ordered;
                case SortType.StockNumber:
                    ordered = id.OrderBy(d => d.StockNumber);
                    return ordered;
                case SortType.Certified:
                    ordered = id.OrderBy(d => d.Certified);
                    return ordered;
                case SortType.ListPrice:
                    ordered = id.OrderBy(d => d.ListPrice);
                    return ordered;
                case SortType.UnitCost:
                    ordered = id.OrderBy(d => d.UnitCost);
                    return ordered;
                case SortType.TradeOrPurchase:
                    ordered = id.OrderBy(d => d.TradeOrPurchase);
                    return ordered;
                case SortType.MarketClassId:
                    ordered = id.OrderBy(d => d.MarketClass);
                    return ordered;
                case SortType.StepsComplete:
                    ordered = id.OrderBy(d => d.PercentComplete);
                    return ordered;
                default:
                    return id.OrderBy(d => 1);
            }
        }
        private static IOrderedEnumerable<IInventoryData> SortDescending(IEnumerable<IInventoryData> id, SortType type)
        {
            switch (type)
            {
                case SortType.InventoryReceivedDate:
                    var ordered = id.OrderByDescending(d => d.ReceivedDate);
                    return ordered;
                case SortType.VehicleYear:
                    ordered = id.OrderByDescending(d => d.Year);
                    return ordered;
                case SortType.Make:
                    ordered = id.OrderByDescending(d => d.Make);
                    return ordered;
                case SortType.Model:
                    ordered = id.OrderByDescending(d => d.Model);
                    return ordered;
                case SortType.Trim:
                    ordered = id.OrderByDescending(d => d.Trim);
                    return ordered;
                case SortType.MileageReceived:
                    ordered = id.OrderByDescending(d => d.MileageReceived);
                    return ordered;
                case SortType.StockNumber:
                    ordered = id.OrderByDescending(d => d.StockNumber);
                    return ordered;
                case SortType.Certified:
                    ordered = id.OrderByDescending(d => d.Certified);
                    return ordered;
                case SortType.ListPrice:
                    ordered = id.OrderByDescending(d => d.ListPrice);
                    return ordered;
                case SortType.UnitCost:
                    ordered = id.OrderByDescending(d => d.UnitCost);
                    return ordered;
                case SortType.TradeOrPurchase:
                    ordered = id.OrderByDescending(d => d.TradeOrPurchase);
                    return ordered;
                case SortType.MarketClassId:
                    ordered = id.OrderByDescending(d => d.MarketClass);
                    return ordered;
                case SortType.StepsComplete:
                    ordered = id.OrderByDescending(d => d.PercentComplete);
                    return ordered;
                default:
                    return id.OrderBy(d => 1);
            }
        }
    }
}