using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class TechSpec : IComparable
    {
        internal TechSpec(IDataRecord reader)
        {
            Titleheader = reader.GetString(reader.GetOrdinal("TechTitleHeaderText"));
            Title = reader.GetString(reader.GetOrdinal("Title"));
            TitleId = reader.GetInt32(reader.GetOrdinal("TitleId"));
            Text = reader.GetString(reader.GetOrdinal("Text"));
            Sequence = reader.GetInt32(reader.GetOrdinal("Sequence"));
            Condition = reader.GetString(reader.GetOrdinal("Condition"));
        }

        public int TitleId { get; set; }

        public string Titleheader { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public int Sequence { get; set; }

        public string Condition { get; set; }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            var ts2 = obj as TechSpec;
            if (ts2 == null) return 1;

            return Sequence.CompareTo(ts2.Sequence);
        }

        #endregion

        public string GetDescription(TechSpecType specType)
        {
            var sb = new StringBuilder();
            switch (specType)
            {
                case TechSpecType.Horsepower:

                    //narsty regex to cover different conditions for titleId 48
                    var hpRegex =
                        new Regex(
                            @"\s*(?<horsepower>(?:\d+|\d+\s+[\(]\d+\s+CA[\)]))?\s*[\@]\s*(?:(?<rpm>\d+)|[\w\-]*)\D*.*");
                    GroupCollection grps = hpRegex.Match(Text).Groups;
                    if (grps.Count >= 2)
                    {
                        if (grps["horsepower"].Success)
                        {
                            sb.Append(grps["horsepower"].Captures[0].Value);
                            sb.Append(" HP");
                        }

                        if (grps["rpm"].Success)
                        {
                            sb.Append(" at ");
                            sb.Append(grps["rpm"].Captures[0].Value);
                            sb.Append(" RPM");
                        }
                    }
                    return sb.ToString();
                case TechSpecType.HorsepowerSecondary:
                    return "";
                default:
                    return GetDescription();
            }
        }

        public string GetDescription()
        {
            //We could add a switch here to do some custom formatting for different types of tech specs...

            return Title + " " + Text;
        }
    }
}