using System;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Templating;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class FinancingSpecial
    {
        public static int AutomatedFinancingSpecialSelectionId = -1;
        public static int DoNotUseFinancingSpecialId = -2;

        private int id;
        private string title;
        private string specialDescription;
        private DateTime? expirationDate;

        private VehicleProfile profile;
       
        public DateTime? ExpirationDate
        {
            get { return expirationDate; }
            set { expirationDate = value; }
        }

        public FinancingSpecial(int id, string title, string specialDescription, DateTime expirationDate)
        {
            this.id = id;
            this.title = title;
            this.specialDescription = specialDescription;
            this.expirationDate = expirationDate;
            
        }
        internal FinancingSpecial(IDataRecord reader)
        {
            this.id = reader.GetInt32(reader.GetOrdinal("specialId"));
            this.title = reader.GetString(reader.GetOrdinal("specialTitle"));
            this.specialDescription = reader.GetString(reader.GetOrdinal("description"));

            var expiresOnOrdinal = reader.GetOrdinal("expiresOn");
            expirationDate = reader.IsDBNull(expiresOnOrdinal) ? null : (DateTime?) reader.GetDateTime(expiresOnOrdinal);

            profile = new VehicleProfile(reader);
        }
        
        public static void Create(int businessUnitId, string title, string description, DateTime expiresOn, int? certificationTypeId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.financingSpecial#Create";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Title", title, DbType.String);
                    Database.AddRequiredParameter(cmd, "Description", description, DbType.String);
                    Database.AddRequiredParameter(cmd, "ExiresOn",expiresOn, DbType.DateTime);
                    if (certificationTypeId != null)
                    {
                        Database.AddRequiredParameter(cmd, "CertificationTypeID", certificationTypeId, DbType.Int32);
                    }
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public static FinancingSpecial Fetch(int ownerEntityTypeId, int ownerEntityId, int specialId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.financingSpecial#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "SpecialID", specialId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        
                        if (reader.Read())
                        {
                           return new FinancingSpecial(reader);
                        }
                        throw new ApplicationException("Special not found id=" + specialId + ", owner entity=" + ownerEntityId + " entity type=" + ownerEntityTypeId);

                    }

                }
            }

        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string SpecialDescription
        {
            get { return specialDescription; }
            set { specialDescription = value; }
        }

        public string getSpecialDescription(IInventoryData vehicleData)
        {
            //only return a financing special if it matches the vehicle data
            if (profile.IsMatch(vehicleData))
            {
                return SpecialDescription;
            }
            return string.Empty;
        }

        public bool IsMatch(IInventoryData vehicleData)
        {
            return profile.IsMatch(vehicleData);
        }


        
    }
}
