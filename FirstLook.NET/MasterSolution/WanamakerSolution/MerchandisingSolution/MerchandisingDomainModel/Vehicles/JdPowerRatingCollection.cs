using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class JdPowerRatingCollection : CollectionBase
    {
        private const string _ratingCollectionStartText = "JDPower.com Power Circle Ratings - ";
        private const string _previewRatingCollectionStartText = "JDPower.com - ";
        private const string _ratingReferenceUrlFormatString = "{0} ({1})"; //used to show the ratings and the associated reference url
        private int year;
        private string make;
        private string model;
        private string ratingReferenceUrl;
        public JdPowerRatingCollection()
        {
            
        }
        internal JdPowerRatingCollection(IDataRecord reader)
        {
            year = reader.GetInt32(reader.GetOrdinal("year"));
            make = reader.GetString(reader.GetOrdinal("make"));
            model = reader.GetString(reader.GetOrdinal("model"));

            // hard-code the jdpower url for contractual reasons ... FB 12714
            ratingReferenceUrl = "www.jdpower.com";

            //start with APEAL awards
            List<JdPowerRating> apealSubRatings = new List<JdPowerRating>();
            apealSubRatings.Add(new JdPowerRating("APEAL Comfort", "Comfort", Database.GetNullableDouble(reader, "Comfort"), JdPowerRatingType.ApealSubRatingComfort));
            apealSubRatings.Add(new JdPowerRating("APEAL Features and Instruments Panel", "Features and Instruments Panel", Database.GetNullableDouble(reader, "FeaturesAndInstrumentPanel"), JdPowerRatingType.ApealSubRatingFeaturesAndInstrumentPanel));
            apealSubRatings.Add(new JdPowerRating("APEAL Performance", "Performance", Database.GetNullableDouble(reader, "Performance"), JdPowerRatingType.ApealSubRatingPerformance));
            apealSubRatings.Add(new JdPowerRating("APEAL Style", "Style", Database.GetNullableDouble(reader, "Style"), JdPowerRatingType.ApealSubRatingStyle));

            List.Add(new JdPowerRatingGroup("Overall Performance and Design", "Performance and Design", Database.GetNullableDouble(reader, "OverallPerformanceAndDesign"),JdPowerRatingType.OverallPerformanceAndDesign,apealSubRatings));

            //add predicted reliability
            List.Add(new JdPowerRating("Predicted Reliability", "Predicted Reliability", Database.GetNullableDouble(reader, "PredictedReliability"), JdPowerRatingType.PredictedReliability));

            //add iqs
            List.Add(new JdPowerRating("Overall Initial Quality Study", "IQS", Database.GetNullableDouble(reader, "OverallIQS"),
                                       JdPowerRatingType.OverallInitialQualityStudy));
            //add mechanical
            List<JdPowerRating>  mechSubRatings = new List<JdPowerRating>();
            mechSubRatings.Add(new JdPowerRating("Powertrain Mechanical Quality", "Powertrain", Database.GetNullableDouble(reader, "PowertrainQualityMechanical"),JdPowerRatingType.QualitySubRatingPowertrain));
            mechSubRatings.Add(new JdPowerRating("Body and Interior Mechanical Quality", "Body and Interior", Database.GetNullableDouble(reader, "BodyAndInteriorQualityMechanical"), JdPowerRatingType.QualitySubRatingBodyAndInterior));
            mechSubRatings.Add(new JdPowerRating("Features and Accessories Mechanical Quality", "Features and Accessories", Database.GetNullableDouble(reader, "FeaturesAndAccessoriesQualityMechanical"), JdPowerRatingType.QualitySubRatingFeaturesAndAccessories));
            List.Add(new JdPowerRatingGroup("Overall Mechanical Quality", "Mechanical Quality", Database.GetNullableDouble(reader, "OverallQualityMechanical"), JdPowerRatingType.OverallQualityMechanical, mechSubRatings));

            //add design
            List<JdPowerRating> designSubRatings = new List<JdPowerRating>();
            designSubRatings.Add(new JdPowerRating("Powertrain Design Quality", "Powertrain", Database.GetNullableDouble(reader, "PowertrainQualityDesign"), JdPowerRatingType.QualitySubRatingPowertrain));
            designSubRatings.Add(new JdPowerRating("Body and Interior Design Quality", "Body and Interior", Database.GetNullableDouble(reader, "BodyAndInteriorQualityDesign"), JdPowerRatingType.QualitySubRatingBodyAndInterior));
            designSubRatings.Add(new JdPowerRating("Features and Accessories Design Quality", "Features and Accessories", Database.GetNullableDouble(reader, "FeaturesAndAccessoriesQualityDesign"), JdPowerRatingType.QualitySubRatingFeaturesAndAccessories));
            List.Add(new JdPowerRatingGroup("Overall Design Quality", "Design Quality", Database.GetNullableDouble(reader, "OverallQualityDesign"), JdPowerRatingType.OverallQualityDesign, designSubRatings));

            //add design
            List<JdPowerRating> depSubRatings = new List<JdPowerRating>();
            depSubRatings.Add(new JdPowerRating("Powertrain Dependability", "Powertrain", Database.GetNullableDouble(reader, "PowertrainDependability"), JdPowerRatingType.QualitySubRatingPowertrain));
            depSubRatings.Add(new JdPowerRating("Body and Interior Dependability", "Body and Interior", Database.GetNullableDouble(reader, "BodyAndInteriorDependability"), JdPowerRatingType.QualitySubRatingBodyAndInterior));
            depSubRatings.Add(new JdPowerRating("Features and Accessories Dependability", "Features and Accessories", Database.GetNullableDouble(reader, "FeatureAndAccessoryDependability"), JdPowerRatingType.QualitySubRatingFeaturesAndAccessories));
            List.Add(new JdPowerRatingGroup("Overall Dependability", "Dependability", Database.GetNullableDouble(reader, "OverallDependability"), JdPowerRatingType.OverallDependability, depSubRatings));            


        }
        public bool HasDisplayableRatings(int count, float minValidScore)
        {
            return GetTopRatings(count, minValidScore).Count > 0;
        }
        public string GetRatingPreviewText(float minValidScore)
        {
            List<JdPowerRating> ratings = GetTopRatings(1, minValidScore);
            if (ratings.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(_previewRatingCollectionStartText);
                sb.Append(ratings[0].ToPreviewString());
                return sb.ToString();
            }
            return string.Empty;            
            
        }
        public string GetRatingText(int count, float minValidScore)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_ratingCollectionStartText);
            foreach (JdPowerRating rating in GetTopRatings(count, minValidScore))
            {
                sb.Append(rating.ToString(minValidScore));
                sb.Append(", ");
            }

            //if the reference url doesn't exist, just return the ratings
            if (string.IsNullOrEmpty(ratingReferenceUrl))
            {
                return sb.ToString(0, sb.Length - 2);
            }
            
            //else include the ref url per the format string
            return string.Format(_ratingReferenceUrlFormatString, sb.ToString(0, sb.Length - 2), ratingReferenceUrl);
            
            
        }
        
        public List<JdPowerRating> GetTopRatings(int count, float minValidScore)
        {
            minValidScore -= 0.01f;//i'm paranoid about floats

            List<JdPowerRating> validRatings = new List<JdPowerRating>();
            foreach (JdPowerRating rating in List)
            {
                if (rating is JdPowerRatingGroup)
                {

                    JdPowerRatingGroup jg = (JdPowerRatingGroup) rating;
                    if (jg.IsDisplayable(minValidScore))
                    {
                        validRatings.Add(jg);
                    }
                    
                }else
                {
                    if (rating.Rating >= minValidScore)
                    {
                        validRatings.Add(rating);
                    }
                }
            }

            //sort to bubble up the groups and high value ratings
            validRatings.Sort();
            return validRatings.GetRange(0, Math.Min(count, validRatings.Count));
        }



        public static JdPowerRatingCollection Fetch(int year, string make, string model, int segmentid)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "merchandising.jdPowerRatings#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ModelYear", year, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Make", make, DbType.String);
                    Database.AddRequiredParameter(cmd, "Model", model, DbType.String);
                    Database.AddRequiredParameter(cmd, "SegmentID", segmentid, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        //move to second result set (first has vin pattern id in it)
                        if (reader.Read())
                        {
                            return new JdPowerRatingCollection(reader);
                        }
                        return new JdPowerRatingCollection();

                    }

                }
            }
        }
        public int Add(JdPowerRating rating)
        {
            return List.Add(rating);
        }

        public JdPowerRating Get(int i)
        {
            return (JdPowerRating)List[i];
        }

        public int IndexOf(JdPowerRating rating)
        {
            return List.IndexOf(rating);
        }

        public void Insert(int i, JdPowerRating rating)
        {
            List.Insert(i, rating);
        }

        public void Remove(JdPowerRating rating)
        {
            List.Remove(rating);
        }

        public bool Contains(JdPowerRating rating)
        {
            return List.Contains(rating);
        }

        public string RatingReferenceUrl
        {
            get { return ratingReferenceUrl; }
            set { ratingReferenceUrl = value; }
        }
    }
}
