using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{

    public enum ModelMarketingType
    {
        Undefined = 0,
        IntroText,
        Taglines,
        Awards

    }
    public class ModelMarketing : IComparable
    {
        public ModelMarketing(string marketingText, ModelMarketingType marketingType, int importanceRanking)
        {
            MarketingText = marketingText;
            MarketingType = marketingType;
            ImportanceRanking = importanceRanking;
        }

        public string MarketingText { get; set; }

        public int ImportanceRanking { get; set; }

        public ModelMarketingType MarketingType { get; set; }

        internal ModelMarketing(IDataRecord reader)
        {
            MarketingText = reader.GetString(reader.GetOrdinal("text"));
            MarketingType = (ModelMarketingType)reader.GetInt32(reader.GetOrdinal("marketingType"));
            ImportanceRanking = reader.GetInt32(reader.GetOrdinal("importanceRanking"));
        }

        public int CompareTo(object obj)
        {
            ModelMarketing y = obj as ModelMarketing;
            if (y == null) throw new ApplicationException("Can only compare model marketing to model marketing");

            return ImportanceRanking.CompareTo(y.ImportanceRanking);
        }
    }
}
