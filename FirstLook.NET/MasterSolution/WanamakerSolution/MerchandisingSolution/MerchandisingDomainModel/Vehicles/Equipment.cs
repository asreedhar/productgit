using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Vehicles;
using log4net;

namespace VehicleDataAccess
{
    /// <summary>
    /// Summary description for Class1
    /// </summary>
    [Serializable]
    public class Equipment
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(Equipment).FullName);
        #endregion

        public Equipment()
        {
            CategoryList = "";
        }

        #region EquipmentKinds enum

        public enum EquipmentKinds : byte
        {
            EquipOpt,
            EquipStd
        } ;

        #endregion

        private readonly ArrayList pricingStates = new ArrayList();

        //option pricing
        //private string condition = "";
        //private decimal invoice = 0.0M;
        //private decimal msrp = 0.0M;
        //private string priceState = "";


        //for optional equipment
        private string extDescription = "";

        private bool _isCustomized;
        public bool IsCustomized
        {
            get { return _isCustomized; }
            set
            {
                Log.DebugFormat("IsCustomized set to {0}", value);
                _isCustomized = value;
            }
        }

        private bool _hasCustomDescription;
        public bool HasCustomDescription
        {
            get { return _hasCustomDescription; }
            set
            {
                Log.DebugFormat("HasCustomDescription set to {0}", EquipmentKind);
                _hasCustomDescription = value;
            }
        }

        /// <summary>
        /// Creates a new instance of Standard
        /// </summary>
        public Equipment(IDataRecord reader)
        {
            Log.Debug("Ctor called with IDataRecord");

            OptionKind = "";
            TypeFilter = "";
            OptionCode = "";
            Sequence = reader.GetInt32(reader.GetOrdinal("Sequence"));
            Description = reader.GetString(reader.GetOrdinal("description")); //could be std desc or opt desc
            CategoryList = reader.GetString(reader.GetOrdinal("categoryList"));
            Header = reader.GetString(reader.GetOrdinal("header"));
            IsCustomized = reader.GetBoolean(reader.GetOrdinal("isCustomized"));

            if (reader.FieldCount <= 5)
            {
                EquipmentKind = EquipmentKinds.EquipStd;
            }
            else
            {
                //this.condition = (string)reader["condition"];
                //this.invoice = (decimal)reader["invoice"];
                //this.msrp = (decimal)reader["msrp"];
                //this.priceState = (string)reader["priceState"];

                var condition = reader.GetString(reader.GetOrdinal("condition"));
                var msrp = reader.GetDecimal(reader.GetOrdinal("msrp"));
                var invoice = reader.GetDecimal(reader.GetOrdinal("invoice"));
                var priceState = reader.GetString(reader.GetOrdinal("priceState"));
                var sequence = reader.GetInt32(reader.GetOrdinal("sequence"));

                pricingStates = new ArrayList();
                pricingStates.Add(new PricingCondition(
                                      condition,
                                      msrp,
                                      invoice,
                                      priceState,
                                      sequence));

                Log.Debug("New PricingCondition added to pricingStates");

                //for optional equipment
                extDescription = reader.GetString(reader.GetOrdinal("extDescription"));
                Log.DebugFormat("extDescription set to '{0}'", extDescription);

                OptionCode = reader.GetString(reader.GetOrdinal("optionCode"));
                TypeFilter = Database.GetNullableString(reader, "typeFilter");
                OptionKindID = reader.GetInt32(reader.GetOrdinal("optionKindID"));                
                OptionKind = reader.GetString(reader.GetOrdinal("optionKind"));
                EquipmentKind = EquipmentKinds.EquipOpt;
                
            }
        }

        private EquipmentKinds _equipmentKind;
        public EquipmentKinds EquipmentKind
        {
            get { return _equipmentKind; }
            set
            {
                Log.DebugFormat("EquipmentKind set to {0}", value);
                _equipmentKind = value;
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                Log.DebugFormat("Description set to '{0}'", value);
                _description = value;
            }
        }

        public string NormalizedDescription
        {
            get
            {
                var normalized = DetailedEquipment.processExceptions(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Description.ToLower()));
                Log.DebugFormat("Normalied description is '{0}'", normalized);
                return normalized;
            }
        }

        public string NormalizedDescriptionAbbreviated
        {
            get
            {
                if (NormalizedDescription.Length > 90)
                {
                    return OptionCode + ": " + NormalizedDescription.Substring(0, 90) + "...";                    
                }
                return OptionCode + ": " + NormalizedDescription;
            }
        }

        public decimal MaxMSRP
        {
            get { return getHighestMSRP(); }
        }

        private int _sequence;
        public int Sequence
        {
            get { return _sequence; }
            set
            {
                Log.DebugFormat("Sequence set to {0}", value);
                _sequence = value;
            }
        }

        private string _header;
        public string Header
        {
            get { return _header; }
            set
            {
                Log.DebugFormat("Header set to {0}", value); 
                _header = value;
            }
        }

        private string _categoryList;
        public string CategoryList
        {
            get { return _categoryList; }
            set
            {
                Log.DebugFormat("CategoryList set to {0}", EquipmentKind);
                _categoryList = value;
            }
        }

        //option pricing

        public ArrayList PricingStates
        {
            get { return pricingStates; }
        }

        //for optional equipment --//could we add a list of extDescs?
        public string ExtDescription
        {
            get
            {
                string extStr = extDescription;
                Regex rg = new Regex("[\\-][iI]nc[\\:]");
                var result = rg.Replace(extStr, "");

                Log.DebugFormat("The ExtDescription is '{0}'", result);
                return result;
            }
            set
            {
                Log.DebugFormat("ExtDescription set to '{0}'", value);
                extDescription = value;
            }
        }

        public List<string> ExtDescriptions
        {
            get
            {                
                string[] list = ExtDescription.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                List<string> retList = new List<string>();
                foreach (string str in list)
                {
                    Log.DebugFormat("Description found: '{0}'", str);

                    string strTrimmed = str.Trim(" ".ToCharArray());
                    if (!String.IsNullOrEmpty(strTrimmed))
                    {
                        Log.DebugFormat("Adding '{0}'", strTrimmed);
                        retList.Add(strTrimmed);
                    }
                }

                Log.DebugFormat("Return list is: '{0}'", retList.ToDelimitedString(","));
                return retList;
            }
        }

        /// <summary>
        /// Returns the ExtDescription shortened to 25 characters, plus "..."
        /// </summary>
        public string ExtDescriptionPreview
        {
            get
            {
                if (ExtDescription.Length > 25)
                {
                    return ExtDescription.Substring(0, 25) + "...";                    
                }
                return ExtDescription;
            }
        }

        private string _optionCode;
        public string OptionCode
        {
            get { return _optionCode; }
            set
            {
                Log.DebugFormat("OptionCode set to {0}", value);
                _optionCode = value;
            }
        }

        private string _typeFilter;
        public string TypeFilter
        {
            get { return _typeFilter; }
            set
            {
                Log.DebugFormat("TypeFilter set to {0}", value);
                _typeFilter = value;
            }
        }

        private int _optionKindID;
        public int OptionKindID
        {
            get { return _optionKindID; }
            set
            {
                Log.DebugFormat("OptionKindID set to {0}", value);
                _optionKindID = value;
            }
        }

        private string _optionKind;
        public string OptionKind
        {
            get { return _optionKind; }
            set
            {
                Log.DebugFormat("OptionKind set to {0}", value);
                _optionKind = value;
            }
        }

        public string OptionCodePlusName
        {
            get { return OptionCode + " -- " + NormalizedDescription; }
        }

        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set
            {
                Log.DebugFormat("Selected set to {0}", value);
                _selected = value;
            }
        }

        public void AddPricingState(PricingCondition pc)
        {
            //insert in properly ordered sequence
            for (int i = 0; i < pricingStates.Count; i++)
            {
                if (pc.Sequence < ((PricingCondition) pricingStates[i]).Sequence)
                {
                    pricingStates.Insert(i, pc);
                    break;
                }
            }
        }

        public virtual string getUniqueCode()
        {
            var code = "" + Sequence;
            Log.DebugFormat("Unique code is '{0}'", code);
            return code;
        }

        public ArrayList parseCategoryList()
        {
            int len = CategoryList.Length;
            
            ArrayList retAL = new ArrayList();
            for (int i = 0; i < len; i += 5)
            {
                int id = Int32.Parse(CategoryList.Substring(i, 4));
                string catClass = CategoryList.Substring(i + 4, 1);
                retAL.Add(new CategoryLink(id, catClass));

                Log.DebugFormat("Added new CategoryLink with id {0} and catClass '{1}'", id, catClass);
            }
            return retAL;
        }

        public static CategoryCollection parseCategoryList(string chromeCategoryList)
        {
            int len = chromeCategoryList.Length;

            CategoryCollection retCC = new CategoryCollection("");
            for (int i = 0; i < len; i += 5)
            {
                int id = Int32.Parse(chromeCategoryList.Substring(i, 4));
                string catClass = chromeCategoryList.Substring(i + 4, 1);
                retCC.Add(new CategoryLink(id, catClass));

                Log.DebugFormat("Added new CategoryLink with id {0} and catClass '{1}'", id, catClass);
            }
            return retCC;
        }


        public bool isPackage()
        {
            //may be additional logic to add in here...
            bool result = CategoryList.ToLower().Contains("p");
            Log.DebugFormat("Returning {0}", result);
            return result;
        }

        //returns a string of category descriptions for a package
        //takes collection of CategoryLink's
        public string[] GetPackageContents(ICollection validCategories)
        {
            ArrayList retList = new ArrayList();

            ArrayList cats = parseCategoryList();
            foreach (CategoryLink cl in cats)
            {
                foreach (CategoryLink cl2 in (validCategories))
                {
                    if (cl.CategoryID == cl2.CategoryID)
                    {
                        retList.Add(cl2.Description);
                        Log.DebugFormat("Adding description '{0}'", cl2.Description);
                        break;
                    }
                }
            }
            if (retList.Count > 0)
            {
                var result = (string[]) retList.ToArray(typeof (string));

                return result;
            }
            
            Log.Debug("Returning new string[0]");
            return new string[0];
            
        }

        public decimal getHighestMSRP()
        {
            decimal highPrice = 0.0M;
            foreach (PricingCondition pc in PricingStates)
            {
                if (pc.Msrp > highPrice)
                {
                    highPrice = pc.Msrp;
                }
            }

            Log.DebugFormat("Returning {0}", highPrice);
            return highPrice;
        }

        public bool IncludesCategoryID(int categoryID)
        {
            bool result = false;

            foreach (CategoryLink cl in parseCategoryList())
            {
                if (cl.CategoryID == categoryID && cl.CategoryAction.ToLower() != "d")
                {
                    result = true;
                    break;
                }
            }

            Log.DebugFormat("Result is {0}", result);
            return result;
        }
    }
}