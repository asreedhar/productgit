using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class CertificationFeature
    {
        public int FeatureTypeId { get; set; }

        public CertificationFeature(string description, bool featureDisplayFlag)
        {
            Description = description;
            FeatureDisplayFlag = featureDisplayFlag;
        }

		public CertificationFeature(IDataRecord reader)
		{
			Title = reader.GetString(reader.GetOrdinal("BenefitName"));
            Description = reader.GetString(reader.GetOrdinal("BenefitText"));
            FeatureDisplayFlag = reader.GetBoolean(reader.GetOrdinal("BenefitDisplay"));
        }

        public override string ToString()
        {
            return !String.IsNullOrEmpty(Description) ? Description : Title;
        }

	    public string Title { get; private set; }
	    public string Description { get; private set; }
		public bool FeatureDisplayFlag { get; set; }

    }
}
