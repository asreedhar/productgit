using System;
using System.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class AdditionalVehicleInfo
    {
        public AdditionalVehicleInfo(int infoId, string questionText, string adText)
        {
            InfoId = infoId;
            QuestionText = questionText;
            AdText = adText;
        }

        internal AdditionalVehicleInfo(IDataRecord reader)
        {
            InfoId = reader.GetInt32(reader.GetOrdinal("infoId"));
            QuestionText = reader.GetString(reader.GetOrdinal("infoBuilderDisplay"));
            AdText = reader.GetString(reader.GetOrdinal("infoAdDisplay"));
            Enabled = Convert.ToBoolean(reader["enabled"]);
        }

        public bool Enabled { get; set; }

        public int InfoId { get; set; }

        public string QuestionText { get; set; }

        public string AdText { get; set; }

        public string IdAndAdText
        {
            get { return string.Format("{0}|{1}", InfoId, AdText); }
        }

        public static int GetId(string idAndAdText)
        {
            return int.Parse(idAndAdText.Substring(0, idAndAdText.IndexOf("|")));
        }

        public static string GetAdText(string idAndAdText)
        {
            int stndx = idAndAdText.IndexOf("|") + 1;
            return idAndAdText.Length > stndx ? idAndAdText.Substring(stndx) : string.Empty;
        }
    }
}