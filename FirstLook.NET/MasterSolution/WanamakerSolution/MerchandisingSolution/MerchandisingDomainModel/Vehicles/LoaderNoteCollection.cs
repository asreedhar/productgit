using System.Collections;
using System.Data;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class LoaderNoteCollection : CollectionBase
    {
        public LoaderNoteCollection()
        {
        }

        internal LoaderNoteCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                InnerList.Add(new LoaderNote(reader));    
            }
        }
        
        public void Save(int businessUnitId, int inventoryId, string memberLogin)
        {
            foreach (var note in InnerList.Cast<LoaderNote>().Where(note => note.IsNew))
            {
                note.Save(businessUnitId, inventoryId, memberLogin);
            }
        }
    }
}
