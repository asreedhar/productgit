using System;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class ImageMetadata
    {
        public DateTime Captured { get; set; }

        public string TextTag { get; set; }

        public ImageMetadata(DateTime captured, string textTag)
        {
            Captured = captured;
            TextTag = textTag;
        }
    }
}
