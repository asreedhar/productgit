﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.DataAccess;
using log4net;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    internal static class Lot
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(Lot).FullName);
        #endregion

        public static void MarkAsLoaded(int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.LotLoaded#Save";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }


        public static bool IsLoaded(int inventoryId)
        {
            int count = 0;

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.LotLoaded#Exists";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);

                    // create return value parameter
                    IDataParameter parameter = cmd.CreateParameter();
                    parameter.ParameterName = "ReturnValue";
                    parameter.DbType = DbType.Int32;
                    parameter.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(parameter);

                    cmd.ExecuteNonQuery();

                    IDataParameter retParem = cmd.Parameters["ReturnValue"] as IDataParameter;
                    if (retParem != null)
                    {
                        count = Convert.ToInt32(retParem.Value);
                    }
                }
            }

            return (count > 0);
        }

        public static GenericEquipmentCollection FetchMappedThirdPartyOptions(int businessUnitId, int inventoryId)
        {
            Log.DebugFormat("FetchMappedThirdPartyOptions() called with businessUnitId {0}, inventoryId {1}",
                businessUnitId, inventoryId);

            //if it's a staging inventory, return empty mapped options
            if (inventoryId < 0)
            {
                Log.Debug("InventoryId < 0");
                return new GenericEquipmentCollection();
            }

            var dataAccess = Registry.Resolve<IMappedThirdPartyOptionDataAccess>();
            var transfer = dataAccess.Fetch(businessUnitId, inventoryId);
            return transfer.Equipment;
        }


    }
}
