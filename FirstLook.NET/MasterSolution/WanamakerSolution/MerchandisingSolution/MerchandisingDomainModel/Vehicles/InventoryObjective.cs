namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum InventoryObjective
    {
        Undefined = 0,
        Retail = 1,
        Wholesale = 2,
        Other = 3
    }
}
