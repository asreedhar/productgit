using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum WarrantyTypes
    {
        BasicWarranty = 1,
        DrivetrainWarranty = 2,
        ExtendedWarranty = 3,
        CorrosionWarranty = 4,
        RoadsideAssistance = 5
    }

    [Serializable]
    public class Warranty : IComparable
    {
        private readonly string[] _milesFormatString = new[] {"{0} Miles - {1} Warranty", "{1} Warranty: {0} Miles"};

        private readonly string[] _yearsAndMilesFormatString = new[]
                                                                   {
                                                                       "{0} yrs/{1} Miles - {2} Warranty",
                                                                       "{2} Warranty: {0} Years/{1} Miles"
                                                                   };

        private readonly string[] _yearsFormatString = new[] {"{0} yrs - {1} Warranty", "{1} Warranty: {0} Years"};

        public Warranty(string descriptionName, int years, int miles, WarrantyTypes warrantyType)
        {
            DescriptionName = descriptionName;
            Years = years;
            Miles = miles;
            WarrantyType = warrantyType;
        }

        public Warranty(string descriptionName, WarrantyTypes warrantyType, string preppedWarrantyText)
        {
            DescriptionName = descriptionName;
            WarrantyType = warrantyType;
            Years = ParseWarrantyYears(preppedWarrantyText);
            Miles = ParseWarrantyMiles(preppedWarrantyText);
        }

        public int Years { get; set; }

        public int Miles { get; set; }

        public string DescriptionName { get; set; }

        public WarrantyTypes WarrantyType { get; set; }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            var warObj = obj as Warranty;
            if (warObj == null || obj.GetType() != typeof (Warranty))
            {
                throw new ApplicationException("Can only compare warranties to warranties");
            }

            if (Miles > warObj.Miles)
            {
                return 1;
            }

            return Miles == warObj.Miles ? (Years > warObj.Years ? 1 : 0) : -1;
        }

        #endregion

        public string ToString(int formatId)
        {
            string milesVal;
            if (Miles == Int32.MaxValue)
            {
                milesVal = " Unlimited";
            }
            else
            {
                milesVal = Miles/1000 + "k";
            }

            //select the proper format Id for the string        
            if (formatId > _yearsAndMilesFormatString.Length)
            {
                formatId = 0;
            }


            //grab suitable format string based on warranty years/miles
            if (Years > 0 && Miles > 0)
            {
                return string.Format(_yearsAndMilesFormatString[formatId], Years, milesVal, DescriptionName);
            }
            if (Years > 0)
            {
                return string.Format(_yearsFormatString[formatId], Years, DescriptionName);
            }
            if (Miles > 0)
            {
                return string.Format(_milesFormatString[formatId], milesVal, DescriptionName);
            }
            return string.Empty;
        }

        private static int ParseWarrantyYears(string preppedWarrantyString)
        {
            var rg = new Regex("\\d[\\d]?[\\s]+[Yy]ears[/]");

            if (rg.IsMatch(preppedWarrantyString))
            {
                Match mt = rg.Match(preppedWarrantyString);

                //take from first char (first digit) to pre first space (should be after first/second digit in years)
                string years = mt.Value.Substring(0, mt.Value.IndexOf(" "));
                return Int32.Parse(years);
            }
            return -1;
        }

        private static int ParseWarrantyMiles(string preppedWarrantyString)
        {
            var rg = new Regex("[/]\\d+?[,][\\d]{3}");

            if (rg.IsMatch(preppedWarrantyString))
            {
                Match mt = rg.Match(preppedWarrantyString);
                string miles = mt.Value.Replace(",", String.Empty).TrimStart("/".ToCharArray());
                return Int32.Parse(miles);
            }

            if (rg.IsMatch(preppedWarrantyString))
            {
                return Int32.MaxValue;
            }
            return -1;
        }

        public bool IsStillGood(int currentMileage, int minMileageLimit, DateTime inServiceDate, int minDaysRemaining)
        {
            TimeSpan minTimeRemaining = TimeSpan.FromDays(Years*365 - minDaysRemaining);
            if ((Miles - currentMileage) > minMileageLimit && DateTime.Today.Subtract(inServiceDate) < minTimeRemaining)
            {
                return true;
            }
            return false;
        }
    }

    public class WarrantyOrderer : IComparer
    {
        // Calls with the parameters reversed.

        #region IComparer Members

        public int Compare(object x, object y)
        {
            var wx = x as Warranty;
            var wy = y as Warranty;
            if (wx == null || wy == null)
            {
                throw new ApplicationException("WarrantyOrderer only accepts warranty objects");
            }
            return wy.CompareTo(wx);
        }

        #endregion
    }
}