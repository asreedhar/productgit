using System;
using System.Collections;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public struct ChromeMsrpCondition
    {
        public string OptionCode;
        public string Condition;
        public decimal Msrp;
    }
}