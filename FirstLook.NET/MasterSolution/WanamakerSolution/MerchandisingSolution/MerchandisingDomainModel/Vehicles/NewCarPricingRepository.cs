﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using log4net;
using MAX.Caching;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
	public class NewCarPricingRepository : INewCarPricingRepository
	{
		#region Logging

		protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		#endregion Logging

		private readonly ICacheKeyBuilder _cacheKeyBuilder;
		private readonly ICacheWrapper _cacheWrapper;
		private const double CacheMinutes = 20;

		public NewCarPricingRepository(ICacheKeyBuilder cacheKeyBuilder, ICacheWrapper cacheWrapper)
		{
			_cacheKeyBuilder = cacheKeyBuilder;
			_cacheWrapper = cacheWrapper;
		}

		public IEnumerable<IInventoryData> GetNewCarPricingInventory(int businessUnitId)
		{
			var cacheKey = _cacheKeyBuilder.CacheKey(new[] { MethodBase.GetCurrentMethod().Name, businessUnitId.ToString(CultureInfo.InvariantCulture) });
            var cacheData = _cacheWrapper.Get<List<InventoryData>>(cacheKey);
			var iDataList = new List<InventoryData>();
			if (cacheData != null)
			{
				iDataList = cacheData;
				if (iDataList.Any())
				{
					Log.Debug("NewCarPricingInventoryDataRepository cache.hit");
					return iDataList;
				}
			}
			Log.DebugFormat("NewCarPricingInventoryDataRepository cache.miss");

			iDataList = InventoryDataFilter.FetchListFromSession(businessUnitId);
			if (!iDataList.Any())
			{
				return iDataList;
			}

			using (var conn = Database.GetConnection(Database.MerchandisingDatabase))
			{
				conn.Open();
				using (var cmd = conn.CreateCommand())
				{
					cmd.CommandText = "workflow.InventoryNewCarPricing#Fetch";
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.AddParameterWithValue("BusinessUnitId", businessUnitId);

					using (var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							var data = iDataList.FirstOrDefault(idata => idata.InventoryID.Equals(reader.GetInt32("InventoryId")));
							if (data == null)
							{
								Log.ErrorFormat(
									"workflow.InventoryNewCarPricing#Fetch returned an inventory id that is not found in InventoryDataFilter.FetchListFromSession. InventoryID: {0}, BusinessUnitId: {1}",
									reader.GetInt32("InventoryId"), businessUnitId);
								continue;
							}
							data.BodyStyleName = reader.IsDBNull("BodyStyleName") ? null : reader.GetString("BodyStyleName");
							data.EngineDescription = reader.IsDBNull("EngineDescription") ? null : reader.GetString("EngineDescription");
							data.IsStandardEngine = !reader.IsDBNull("IsStandard") && reader.GetBoolean("IsStandard");
							data.EngineOptionCode = reader.IsDBNull("OptionCode") ? null : reader.GetString("OptionCode");
						}
					}
				}
			}

			Log.DebugFormat("NewCarPricingInventoryDataRepository cache expires: {0}", DateTime.Now.AddMinutes(CacheMinutes));
			if (iDataList.Any())
				_cacheWrapper.Set(cacheKey, iDataList, new DateTimeOffset(DateTime.Now.AddMinutes(CacheMinutes)));
			return iDataList;
		}
	}
}