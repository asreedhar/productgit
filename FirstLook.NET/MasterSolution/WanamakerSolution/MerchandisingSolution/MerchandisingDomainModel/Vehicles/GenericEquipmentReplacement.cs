using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Data;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class GenericEquipmentReplacement : ITieredEquipment
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(GenericEquipmentReplacement).FullName);
        #endregion


        public int Id { get; set; }

        public GenericEquipmentReplacementType ReplacementType { get; set; }

        public CategoryCollection IncludesCategories { get; set; }

        public CategoryCollection ExcludesCategories { get; set; }

        public List<int> IncludesGenericEquipmentIds
        {
            get
            {
                List<int> ids = new List<int>();
                foreach (CategoryLink category in IncludesCategories)
                {
                    ids.Add(category.CategoryID);
                }
                return ids;
                
            }
            
        }


        public List<int> ExcludesGenericEquipmentIds
        {
            get
            {
                List<int> ids = new List<int>();
                foreach (CategoryLink category in ExcludesCategories)
                {
                    ids.Add(category.CategoryID);
                }
                return ids;                

            }            
        }

        private readonly List<string> _possibleDescriptions;

        public int Tier { get; set; }

        public int DisplayPriority { get; set; }

        public GenericEquipmentReplacement()
        {
            IncludesCategories = new CategoryCollection("included");
            ExcludesCategories = new CategoryCollection("excluded");
            _possibleDescriptions = new List<string>();
        }
        public GenericEquipmentReplacement(int id, CategoryCollection includesCategories, CategoryCollection excludesCategories, string description, GenericEquipmentReplacementType replacementType)
        {
            Id = id;
            IncludesCategories= includesCategories;
            ExcludesCategories = excludesCategories;
            _possibleDescriptions = new List<string>();
            _possibleDescriptions.Add(description);
            ReplacementType = replacementType;
        }
        public GenericEquipmentReplacement(int id, CategoryCollection includesCategories, CategoryCollection excludesCategories, List<string> possibleDescriptions, GenericEquipmentReplacementType replacementType)
        {
            Id = id;
            IncludesCategories= includesCategories;
            ExcludesCategories = excludesCategories;
            _possibleDescriptions = possibleDescriptions;
            ReplacementType = replacementType;
        }

        public bool IsWhollyContained(List<int> setOfCategoryIds)
        {
            foreach (CategoryLink category in IncludesCategories)
            {

                if (!setOfCategoryIds.Contains(category.CategoryID))
                {
                    return false;                    
                }
            }

            //none of the categoryIds were missing from the proposed set, so wholly contained
            return true;

        }
        public bool IsIncluded(GenericEquipmentCollection genericEquipmentCollection)
        {
            bool retVal = true;

            //if the generic equipment does not include any of our required "included" categories
            foreach (CategoryLink category in IncludesCategories)
            {
                if (!genericEquipmentCollection.Contains(category.CategoryID))
                {
                    retVal = false;
                    break;
                }
            }

            if (retVal)  //this only matters if we are still including based on the above
            {
                //OR if it contains any of hte excluded categories, we should not include it
                foreach (CategoryLink category in ExcludesCategories)
                {
                    if (genericEquipmentCollection.Contains(category.CategoryID))
                    {
                        retVal = false;
                        break;
                    }
                }
            }
            return retVal;

        }
        public bool Contains(int categoryId)
        {
            return IncludesCategories.Contains(categoryId);
        }
        public int CompareTo(object obj)
        {

            ITieredEquipment x = obj as ITieredEquipment;
            if (x == null) throw new ApplicationException("Can only compare Tiered to TieredEquipment");

            int val = Tier.CompareTo(x.GetTier());
            if (val == 0)
            {
                val = DisplayPriority.CompareTo(x.GetDisplayPriority());
            }

            //rank detailed equipment first
            if (val == 0)
            {
                 if (x is DetailedEquipment) val = 1;    
                 else if (x is GenericEquipment) val = -1;
                 else if (x is GenericEquipmentReplacement)
                 {
                     val =
                         ((GenericEquipmentReplacement) x).IncludesCategories.Count.CompareTo(
                             IncludesCategories.Count); //take larger set
                 }
            }
            return val;
        
        }
        public string Description
        {
            get
            {
                return GetDescription();
            }
            set
            {
                if (_possibleDescriptions.Count > 0)
                {
                    _possibleDescriptions[0] = value;
                }else
                {
                    _possibleDescriptions.Add(value);
                }

            }
        }
        public string GetDescription()
        {
            Random rand = new Random();
            int ct = _possibleDescriptions.Count;
            return ct > 0 ? _possibleDescriptions[rand.Next(0, ct)] : string.Empty;
        }

        public int GetTier()
        {
            return Tier;
        }

        public int GetDisplayPriority()
        {
            return DisplayPriority;
        }

        public void SetTierAndPriority(GenericEquipmentCollection generics)
        {
            int tmpDisplayPriority = 999999;
            int tmpTier = 100;
            
            foreach (GenericEquipment equipment in generics)
            {
                if (IncludesCategories.Contains(equipment.Category.CategoryID))
                {
                    tmpTier = Math.Min(equipment.GetTier(), tmpTier);
                    tmpDisplayPriority = Math.Min(equipment.GetDisplayPriority(), tmpDisplayPriority);
                }
            }
            Tier = tmpTier;
            DisplayPriority = tmpDisplayPriority;
        }

        public void Prioritize()
        {
            // set tier and priority
            Tier = 1;
            DisplayPriority = 0;
        }

        public string GetSummary()
        {
            return string.Empty;
        }

        public static void Delete(int replacementId, int businessUnitId, string memberLogin)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                
                //delete the categoryReplacement
                using (IDbCommand cmd = con.CreateCommand())
                {
                    
                    cmd.CommandText = "settings.categoryReplacement#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ReplacementId", replacementId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);
                    cmd.ExecuteNonQuery();
                }

                
            }
        }

        public static void Create(int businessUnitId, string description, List<int> includedCategoryIds, GenericEquipmentReplacementType replacementType, string memberLogin)
        {
            //the zero works here, but we really need to make this a nullable
            GenericEquipmentReplacement ger = new GenericEquipmentReplacement();
            ger.Id = 0;

            ger.IncludesCategories = new CategoryCollection("included");
            foreach (int id in includedCategoryIds)
            {
                ger.IncludesCategories.Add(new CategoryLink(id, string.Empty));
            }
            ger.ExcludesCategories = new CategoryCollection("excluded");
            ger.Description = description;
            ger.ReplacementType = replacementType;
            
            ger.UpdateOrOverride(businessUnitId, memberLogin);
            return;
        }
        public void UpdateOrOverride(int businessUnitId, string memberLogin)
        {
            //save the grouping information here!
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbTransaction trans = con.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {

                        //create the categoryReplacement
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trans;
                            cmd.CommandText = "settings.categoryReplacement#Insert";
                            cmd.CommandType = CommandType.StoredProcedure;

                            Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                            Database.AddRequiredParameter(cmd, "Name", Description, DbType.String);
                            
                            //this function always creates dealer replacements
                            Database.AddRequiredParameter(cmd, "ReplacementType", (int) GenericEquipmentReplacementType.Dealer, DbType.Int32);

                            if (ReplacementType == GenericEquipmentReplacementType.System)
                            {
                                Database.AddRequiredParameter(cmd, "OverridesReplacementId", Id, DbType.Int32);
                            }
                            Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);

                            SqlParameter param = new SqlParameter("ReplacementId", DbType.Int32);
                            param.Direction = ParameterDirection.InputOutput;
                            param.Value = Id;
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();

                            Id = (int) param.Value;
                        }

                        //blow away old categories
                        using (IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trans;
                            cmd.CommandText = "settings.categoryReplacementItems#Delete";
                            cmd.CommandType = CommandType.StoredProcedure;

                            Database.AddRequiredParameter(cmd, "ReplacementId", Id, DbType.Int32);

                            cmd.ExecuteNonQuery();

                        }

                        //add each newly mapped category Id in turn
                        foreach (CategoryLink category in IncludesCategories)
                        {
                            using (IDbCommand cmd = con.CreateCommand())
                            {
                                cmd.Transaction = trans;
                                cmd.CommandText = "settings.categoryReplacementItem#Insert";
                                cmd.CommandType = CommandType.StoredProcedure;

                                Database.AddRequiredParameter(cmd, "ReplacementId", Id, DbType.Int32);
                                Database.AddRequiredParameter(cmd, "CategoryId", category.CategoryID, DbType.Int32);
                                Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);

                                cmd.ExecuteNonQuery();

                            }
                        }
                        trans.Commit();
                    }
                    catch
                    {
                        trans.Rollback();
                    }
                }
            }
                        
        }


 
    }
}
