﻿namespace FirstLook.Merchandising.DomainModel.Vehicles.ManufacturerSpecificVehicleTemplatingData
{
    public class LincolnVehicleTemplatingData : VehicleTemplatingData
    {
        public new bool HasTrim
        {
            get
            {
                return (InventoryItem.Trim.Trim().Length +
                        VehicleConfig.AfterMarketTrim.Trim().Length) > 0;
            }
        }


        /*$base.Trim$*/
        public new string Trim
        {
            get
            {
                var customTrim = VehicleConfig.AfterMarketTrim;
                var invTrim = InventoryItem.Trim;
                var retTrim = string.IsNullOrWhiteSpace(customTrim) ? invTrim.Trim() : customTrim;
                if (!string.IsNullOrEmpty(retTrim))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Trim);
                
                }

                return retTrim;
            }
        }
    }
}