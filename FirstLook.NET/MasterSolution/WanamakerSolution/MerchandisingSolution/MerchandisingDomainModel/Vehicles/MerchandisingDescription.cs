using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using FirstLook.Merchandising.DomainModel.Pricing;
using FirstLook.Merchandising.DomainModel.Templating;
using log4net;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class MerchandisingDescription
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(MerchandisingDescription).FullName);
        #endregion


        #region Fields

        private const string EXPIRES_DATE_FORMAT = "[\\d]{1,2}/[\\d]{1,2}/[\\d]{2,4}";
        private const string EXPIRES_DISCLAIMER = "Offer valid until {0}. ";

        private const string EQUIPMENT_DISCLAIMER =
            "Please confirm the accuracy of the included equipment by calling us prior to purchase. ";

        private const string GAS_DISCLAIMER =
            "Fuel economy calculations based on original manufacturer data for trim engine configuration. ";

        private const string HORSEPOWER_DISCLAIMER = "Horsepower calculations based on trim engine configuration. ";
        private readonly List<MerchandisingDescriptionItemType> _contents = new List<MerchandisingDescriptionItemType>();

        private readonly string _priceDisclaimer = "Pricing analysis performed on " + DateTime.Today.ToShortDateString() +
                                                  ". ";

        private string _description = string.Empty;

        private string _footer = string.Empty;
        private bool _includesExpiresDisclaimer;

        private bool _isDirty;

        #endregion

        #region Properties

        /// <summary>
        /// This describes the crude structure of the Ad.  Initially it is quite limited.
        /// </summary>
        public Advertisement AdvertisementModel { get; set; }
        public Advertisement OldAdvertisementModel { get; set; }

        internal IEnumerable<TemplateItem> TemplateItems { get; set; }

        public int? ThemeId { get; set; }

        public int? TemplateId { get; set; }

        public bool IsAutoPilot { get; set; }

        public bool IsLongForm { get; set; }

        public decimal? LastUpdateListPrice { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        public byte[] Version { get; set; }

        public List<int> SampleTextIds { get; set; }

        public DescriptionProperties BasicProperties { get; set; }

        public DescriptionExtendedProperties ExtendedProperties { get; set; }

        public string Footer
        {
            get
            {
                if (string.IsNullOrEmpty(_footer))
                {
                    ResetDisclaimer();
                }

                return _footer;
            }
            set
            {
                if (_footer != value)
                {
                    _footer = value;
                    _isDirty = true;
                }
            }
        }

        public int? AdvertisementId { get; set; }

        public DateTime? ExpiresOn { get; set; }

        public string Description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    _isDirty = true;
                }
            }
        }

        public bool IsNew { get; private set; }

        public int BusinessUnitId { get; set; }

        #endregion

        #region Constructors

        public MerchandisingDescription()
        {
            ExpiresOn = DateTime.MaxValue;
            ExtendedProperties = new DescriptionExtendedProperties();
            BasicProperties = new DescriptionProperties();
            SampleTextIds = new List<int>();
            Version = new byte[8];
            IsNew = true;
            //advertisementId is null...
        }

        public MerchandisingDescription(int businessUnitId)
            : this()
        {
            BusinessUnitId = businessUnitId;
            //advertisementId is null...
        }

        internal MerchandisingDescription(IDataReader reader, bool isFromAccelerator, int businessUnitId)
        {
            ExpiresOn = DateTime.MaxValue;
            ExtendedProperties = new DescriptionExtendedProperties();
            BasicProperties = new DescriptionProperties();
            SampleTextIds = new List<int>();
            Version = new byte[8];
            if (isFromAccelerator)
            {
                if (reader.Read())
                {
                    _description = reader.GetString(reader.GetOrdinal("Body"));
                    _footer = reader.GetString(reader.GetOrdinal("Footer"));
                    ExpiresOn = DateTime.Today; ///////////////NOTE FIX
                }
                if (reader.NextResult() && reader.Read())
                {
                    BasicProperties = new DescriptionProperties(reader);
                }
                if (reader.NextResult() && reader.Read())
                {
                    //not sure why the below is commented out July 22, 2010 -bf
                    //extendedProperties = new DescriptionExtendedProperties(reader);
                }
                IsNew = false;
            }
            else
            {
                if (reader.Read())
                {
                    AdvertisementId = Database.GetNullableInt(reader, "acceleratorAdvertisementId");
                    ExpiresOn = Database.GetNullableDateTime(reader, "expiresOn");
                    _description = Database.GetNullableString(reader, "merchandisingDescription");
                    _footer = Database.GetNullableString(reader, "footer");
                    LastUpdatedOn = Database.GetNullableDateTime(reader, "lastUpdatedOn");
                    LastUpdateListPrice = Database.GetNullableDecimal(reader, "lastUpdateListPrice");
                    IsAutoPilot = Database.GetNullableBoolean(reader, "isAutoPilot").GetValueOrDefault();
                    IsLongForm = Database.GetNullableBoolean(reader, "isLongForm").GetValueOrDefault();
                    TemplateId = Database.GetNullableInt(reader, "templateId");
                    ThemeId = Database.GetNullableInt(reader, "themeId");

                    int versionOrd = reader.GetOrdinal("version");
                    IsNew = reader.IsDBNull(versionOrd);
                    if (!IsNew)
                    {
                        Version = (byte[])reader.GetValue(versionOrd);
                    }
                }
                else
                {
                    IsNew = true;
                }

                if (reader.NextResult())
                {
                    //LoadContents(reader);
                    while (reader.Read())
                    {
                        _contents.Add(
                            (MerchandisingDescriptionItemType)
                            reader.GetInt32(reader.GetOrdinal("descriptionItemTypeId")));
                    }

                    ExtendedProperties = new DescriptionExtendedProperties(_contents);
                }

                if (reader.NextResult())
                {
                    //LoadSampleTextIds(reader);
                    SampleTextIds = new List<int>();
                    while (reader.Read())
                    {
                        SampleTextIds.Add(reader.GetInt32(reader.GetOrdinal("blurbTextId")));
                    }
                }
            }
            BusinessUnitId = businessUnitId;
        }

        #endregion

        #region "Business" Methods

        public AdTemplateSettings GetLastAdTemplateSettings()
        {
            var themeIds = new List<int>();
            if (ThemeId.HasValue)
            {
                themeIds.Add(ThemeId.Value);
            }
            return new AdTemplateSettings(TemplateId, themeIds, PricingAggressiveness.Low);
        }

        public void AddExpiresDisclaimer()
        {
            //first check to see if it has a disclaimer in it
            RemoveExpiresDisclaimer();

            if (!_includesExpiresDisclaimer && ExpiresOn.HasValue)
            {
                _footer += string.Format(EXPIRES_DISCLAIMER, ExpiresOn.Value.ToShortDateString());
                _includesExpiresDisclaimer = true;
            }
        }

        public void RemoveExpiresDisclaimer()
        {
            string discStr = string.Format(EXPIRES_DISCLAIMER, EXPIRES_DATE_FORMAT);
            var regex = new Regex(discStr);
            _footer = regex.Replace(_footer, string.Empty);
            _includesExpiresDisclaimer = false;
        }

        public void AddItemType(MerchandisingDescriptionItemType itemType)
        {
            Log.DebugFormat("AddItemType() called with MerchandisingDescriptionItemType {0}", itemType);

            if (!_contents.Contains(itemType))
            {
                Log.Debug("We don't yet contain this item type. Setting dirty to true, and adding itemType to _contents.");
                _isDirty = true;
                _contents.Add(itemType);

                AddMappedDescriptionItem(itemType);
            }
            else
            {
                Log.Debug("We already contain this item type.");
            }
        }

        public bool ContainsItemType(MerchandisingDescriptionItemType itemType)
        {
            if(_contents == null )
            {
                return false;
            }
            return _contents.Contains(itemType);
        }

        public void ResetDisclaimer()
        {
            var sb = new StringBuilder();
            CustomDisclaimer custom = CustomDisclaimer.FetchCustomDisclaimer(BusinessUnitId);

            if (!custom.IsReplace)
            {
                if (custom.IsAppend)
                    sb.Append(custom.CustomText + " ");

                if (_contents.Contains(MerchandisingDescriptionItemType.PriceBelowBook) ||
                    _contents.Contains(MerchandisingDescriptionItemType.PriceBelowBookRough) ||
                    _contents.Contains(MerchandisingDescriptionItemType.PriceBelowMsrp) ||
                    _contents.Contains(MerchandisingDescriptionItemType.PriceBelowMsrpRough) ||
                    _contents.Contains(MerchandisingDescriptionItemType.CountBelowBook) ||
                    _contents.Contains(MerchandisingDescriptionItemType.CountBelowPrice) ||
                    _contents.Contains(MerchandisingDescriptionItemType.KelleyBlueBookValue) ||
                    _contents.Contains(MerchandisingDescriptionItemType.NadaValue) ||
                    _contents.Contains(MerchandisingDescriptionItemType.BlackBookValue) ||
                    _contents.Contains(MerchandisingDescriptionItemType.OriginalMsrp)
                    )
                {
                    sb.Append(_priceDisclaimer);
                }

                if (_contents.Contains(MerchandisingDescriptionItemType.Horsepower))
                {
                    sb.Append(HORSEPOWER_DISCLAIMER);
                }

                if (_contents.Contains(MerchandisingDescriptionItemType.HighwayGasMileage)
                    || _contents.Contains(MerchandisingDescriptionItemType.CityGasMileage))
                {
                    sb.Append(GAS_DISCLAIMER);
                }

                if (_contents.Contains(MerchandisingDescriptionItemType.Tier0Equipment)
                    || _contents.Contains(MerchandisingDescriptionItemType.Tier1Equipment)
                    || _contents.Contains(MerchandisingDescriptionItemType.Tier2Equipment)
                    || _contents.Contains(MerchandisingDescriptionItemType.Tier3Equipment))
                {
                    sb.Append(EQUIPMENT_DISCLAIMER);
                }
            }
            else
            {
                sb.Append(custom.CustomText);
            }

            _footer = sb.ToString();
            if (ExpiresOn.HasValue)
            {
                AddExpiresDisclaimer();
            }
        }

        private void AddMappedDescriptionItem(MerchandisingDescriptionItemType itemType)
        {
            Log.DebugFormat("AddMappedDescriptionItem() called for MerchandisingDescriptionItemType {0}", itemType);
            ExtendedProperties.AddMappedDescriptionItem(itemType);
        }

        public void RemoveItemType(MerchandisingDescriptionItemType itemType)
        {
            if (_contents.Contains(itemType))
            {
                Log.DebugFormat("Removing MerchandisingDescriptionItemType {0} from _contents.", itemType);
                _contents.Remove(itemType);
            }
        }

        #endregion

        #region DataAccess

        public static MerchandisingDescription Fetch(int businessUnitId, int inventoryId)
        {
            MerchandisingDescription merchandisingDescription;

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Builder.Description#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        merchandisingDescription = new MerchandisingDescription(reader, false, businessUnitId);
                    }
                }

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.Advertisement#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            //  Get the xml.
                            string xml = Database.GetNullableString(reader, "advertisementXml");

                            var converter = new AdvertisementConverter();
                            var advertisement = converter.FromXml(xml);

                            merchandisingDescription.AdvertisementModel = advertisement;
                            merchandisingDescription.OldAdvertisementModel = advertisement;
                        }
                    }
                }
            }

            return merchandisingDescription;
        }

        /// <summary>
        /// The advertisement is being "released" - e.g. sent to Autotrader.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="inventoryId"></param>
        /// <param name="memberLogin"></param>
        public void Publish(int businessUnitId, int inventoryId, string memberLogin)
        {
            // Merchandising.Advertisement#Insert
            // Merchandising.Advertisement_Properties#Insert
            // Merchandising.Advertisement_ExtendedProperties#Insert
            // Merchandising.VehicleAdvertisement#Assign

            Log.InfoFormat("Publish() invoked for businessUnitId:{0}, inventoryId:{1}, memberLogin:{2}", businessUnitId, inventoryId, memberLogin);

            string oh = PricingData.GetOwnerHandle(businessUnitId);
            Pair<VehicleHandle, SearchHandle> vhsh = PricingData.GetVehicleHandle(oh, inventoryId);
            string vh = vhsh.First.Value;

            //using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            using (IDataConnection con = Database.GetConnection(Database.MarketDatabase))
            {
                con.Open();

                int status = 0;
                int advertId = 0;
                
                try
                {
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "Merchandising.Advertisement#Insert";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.AddRequiredParameter("Body", _description, DbType.String);
                        cmd.AddRequiredParameter("Footer", _footer, DbType.String);

                        IDbDataParameter param = cmd.CreateParameter();
                        param.Direction = ParameterDirection.Output;
                        param.ParameterName = "AdvertisementId";
                        param.DbType = DbType.Int32;

                        cmd.Parameters.Add(param);

                        cmd.ExecuteNonQuery();

                        advertId = (int)param.Value;
                    }
                }
                catch
                {
                    throw new ApplicationException("Could not publish ad to Accelerator");
                }

                //use advert id to insert properties and extendedProperties
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.Advertisement_Properties#Insert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter( "AdvertisementId", advertId, DbType.Int32);
                    cmd.AddRequiredParameter( "Author", BasicProperties.Author, DbType.String);
                    cmd.AddRequiredParameter( "Created", BasicProperties.Created, DbType.DateTime);
                    cmd.AddRequiredParameter( "Accessed", BasicProperties.Accessed, DbType.DateTime);
                    cmd.AddRequiredParameter( "Modified", BasicProperties.Modified, DbType.DateTime);
                    cmd.AddRequiredParameter( "EditDuration", BasicProperties.EditDuration, DbType.Int32);
                    cmd.AddRequiredParameter( "RevisionNumber", BasicProperties.RevisionNumber + 1,
                                                  DbType.Int32);
                    cmd.AddRequiredParameter( "HasEditedBody", BasicProperties.HasEditedBody,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "HasEditedFooter", BasicProperties.HasEditedFooter,
                                                  DbType.Boolean);

                    // Add output parameter for the ruturn value
                    // Database.AddRequiredParameter(,,BasicProperties.);

                    cmd.ExecuteNonQuery();
                }

                if (status != 0)
                {
                    throw new ApplicationException("Could not publish properties to Accelerator");
                }

                //use advert id to insert properties and extendedProperties
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.Advertisement_ExtendedProperties#Insert";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter( "AdvertisementId", advertId, DbType.Int32);
                    cmd.AddRequiredParameter( "AvailableCarfax", ExtendedProperties.AvailableCarfax,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "AvailableCertified", ExtendedProperties.AvailableCertified,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "AvailableEdmundsTrueMarketValue",
                                                  ExtendedProperties.AvailableEdmundsTrueMarketValue, DbType.Boolean);
                    cmd.AddRequiredParameter( "AvailableKelleyBlueBook",
                                                  ExtendedProperties.AvailableKelleyBlueBook,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "AvailableNada", ExtendedProperties.AvailableNada,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "AvailableTagline", ExtendedProperties.AvailableTagline,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "AvailableHighValueEquipment",
                                                  ExtendedProperties.AvailableHighValueOptions, DbType.Boolean);
                    cmd.AddRequiredParameter( "AvailableStandardEquipment",
                                                  ExtendedProperties.AvailableStandardOptions, DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedCarfax", ExtendedProperties.IncludedCarfax,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedCertified", ExtendedProperties.IncludedCertified,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedEdmundsTrueMarketValue",
                                                  ExtendedProperties.IncludedEdmundsTrueMarketValue, DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedKelleyBlueBook",
                                                  ExtendedProperties.IncludedKelleyBlueBook,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedNada", ExtendedProperties.IncludedNada,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedTagline", ExtendedProperties.IncludedTagline,
                                                  DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedHighValueEquipment",
                                                  ExtendedProperties.IncludedHighValueOptions, DbType.Boolean);
                    cmd.AddRequiredParameter( "IncludedStandardEquipment",
                                                  ExtendedProperties.IncludedStandardOptions, DbType.Boolean);


                    cmd.ExecuteNonQuery();
                }

                if (status != 0)
                {
                    throw new ApplicationException("Could not publish extended properties to Accelerator");
                }

                //use advert id to insert properties and extendedProperties
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Merchandising.VehicleAdvertisement#Assign";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter( "AdvertisementId", advertId, DbType.Int32);
                    cmd.AddRequiredParameter( "OwnerHandle", oh, DbType.String);
                    cmd.AddRequiredParameter( "VehicleHandle", vh, DbType.String);

                    cmd.ExecuteNonQuery();
                }
                if (status != 0)
                {
                    throw new ApplicationException("Could not assign ad to vehicle in Accelerator");
                }

                //if successful, now set our advert id to the new one...
                AdvertisementId = advertId;
            }
        }

        public void Save(int businessUnitId, int inventoryId, string memberLogin)
        {
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings[Database.MerchandisingDatabase].ConnectionString))
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                try
                {
                    SaveDescription(businessUnitId, inventoryId, memberLogin, con );

                    // Save our model of the ad.
                    SaveAdvertisementModel(businessUnitId, inventoryId, memberLogin, con);

                    //DeleteDescriptionItems(businessUnitId, inventoryId, con);
                    SaveDescriptionItems(businessUnitId, inventoryId, con);

                    //DeleteSampleTextItems(businessUnitId, inventoryId, con);
                    SaveSampleTextItems(businessUnitId, inventoryId, con);

                }
                catch (Exception e)
                {
                    throw new ApplicationException(
                        string.Format("Failed to save the Advertisement for invID {0} buID {1}, please try again.  Error: {2}", inventoryId, businessUnitId, e.Message));
                }
                
            }
        }

        /// <summary>
        /// Store our meta-model of the advertisement to the db.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="inventoryId"></param>
        /// <param name="memberLogin"></param>
        /// <param name="con"></param>
        private void SaveAdvertisementModel(int businessUnitId, int inventoryId, string memberLogin, SqlConnection con)
        {
            // If no model, nothing to do.
            if( AdvertisementModel == null )
                return;

            // Get the xml.
            var converter = new AdvertisementConverter();
            string xml = converter.ToXml(AdvertisementModel);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.Advertisement#Save";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.AddRequiredParameter( "BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);
                cmd.AddRequiredParameter("AdvertisementXml", xml, DbType.String);
                cmd.AddRequiredParameter("MemberLogin", memberLogin, DbType.String);

                cmd.ExecuteNonQuery();
            }
        }

        private void SaveDescription(int businessUnitId, int inventoryId, string memberLogin, SqlConnection con )
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.Description#Save";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);
                cmd.AddRequiredParameter("MerchandisingDescription", _description, DbType.String);
                cmd.AddRequiredParameter("Footer", _footer, DbType.String);
                if (LastUpdateListPrice.HasValue)
                {
                    cmd.AddRequiredParameter("CurrentListPrice", LastUpdateListPrice.Value, DbType.Decimal);
                }
                else
                {
                    //NOTE CHANGE ME TO NULLABLE ON NEXT RELEASE
                    //Database.AddNullValue(cmd, "CurrentListPrice", DbType.Decimal);
                    cmd.AddRequiredParameter("CurrentListPrice", 0.0m, DbType.Decimal);
                }
                cmd.AddRequiredParameter("IsAutoPilot", IsAutoPilot, DbType.Boolean);
                cmd.AddRequiredParameter("IsLongForm", IsLongForm, DbType.Boolean);
                cmd.AddRequiredParameter("AcceleratorAdvertisementId", AdvertisementId, DbType.Int32);
                cmd.AddRequiredParameter("ExpiresOn", ExpiresOn, DbType.DateTime);
                cmd.AddRequiredParameter("TemplateId", TemplateId, DbType.Int32);
                cmd.AddRequiredParameter("ThemeId", ThemeId, DbType.Int32);

                IDataParameter param = cmd.CreateParameter();
                param.Direction = ParameterDirection.InputOutput;
                param.DbType = DbType.Binary;
                param.Value = Version;
                param.ParameterName = "Version";
                cmd.Parameters.Add(param);

                cmd.AddRequiredParameter("MemberLogin", memberLogin, DbType.String);

                cmd.ExecuteNonQuery();

                Version = (byte[])param.Value;
            }
        }

        private void SaveDescriptionItems(int businessUnitId, int inventoryId, SqlConnection con)
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            // create table to pass to stored proc
            var descriptionContent = new DataTable();
            descriptionContent.Columns.Add("descriptionItemTypeId", typeof(int));


            // populate table with data from list
            foreach (MerchandisingDescriptionItemType itemTypeId in _contents)
            {
                DataRow descriptionContentRow = descriptionContent.NewRow();
                descriptionContentRow["descriptionItemTypeId"] = (int)itemTypeId;
                descriptionContent.Rows.Add(descriptionContentRow);
                
            }

            // save it as one big merge
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.descriptionItem#Merge";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("BusinessUnitID", businessUnitId);
                cmd.Parameters.AddWithValue("InventoryId", inventoryId);

                var tableParm = cmd.Parameters.AddWithValue("descriptionContent", descriptionContent);
                tableParm.SqlDbType = SqlDbType.Structured;
                tableParm.TypeName = "[builder].[descriptionContent]";

                cmd.ExecuteNonQuery();
            }


        }

        private static void DeleteDescriptionItems(int businessUnitId, int inventoryId, IDbConnection con)
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.descriptionItems#Delete";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.AddRequiredParameter( "BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter( "InventoryId", inventoryId, DbType.Int32);
                cmd.ExecuteNonQuery();
            }
        }

        private void SaveSampleTextItems(int businessUnitId, int inventoryId, SqlConnection con)
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            // create table to pass to stored proc
            var sampleTextItem = new DataTable();
            sampleTextItem.Columns.Add("blurbTextId", typeof(int));
            sampleTextItem.Columns.Add("textSequence", typeof(int));


            // populate table with data from list
            for (int i = 0; i < SampleTextIds.Count; ++i)
            {
                int sequenceNum = i + 1;

                DataRow sampleTextItemRow = sampleTextItem.NewRow();
                sampleTextItemRow["blurbTextId"] = SampleTextIds[i];
                sampleTextItemRow["textSequence"] = sequenceNum;
                sampleTextItem.Rows.Add(sampleTextItemRow);

            }

            // save it as one big merge
            using (var cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.descriptionSampleTextItem#Merge";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("BusinessUnitID", businessUnitId);
                cmd.Parameters.AddWithValue("InventoryId", inventoryId);

                var tableParm = cmd.Parameters.AddWithValue("sampleTextItem", sampleTextItem);
                tableParm.SqlDbType = SqlDbType.Structured;
                tableParm.TypeName = "[builder].[sampleTextItem]";

                cmd.ExecuteNonQuery();
            }

        }

        private static void DeleteSampleTextItems(int businessUnitId, int inventoryId, IDbConnection con)
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }

            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.descriptionSampleTextItems#Delete";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.AddRequiredParameter("BusinessUnitID", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);
                cmd.ExecuteNonQuery();
            }
        }

        public static int? FetchLatestAcceleratorAdvertisementId(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "merchandising.AcceleratorAdId#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter( "BusinessUnitId", businessUnitId, DbType.Int32);
                    cmd.AddRequiredParameter( "InventoryId", inventoryId, DbType.Int32);

                    IDbDataParameter param = cmd.CreateParameter();
                    param.Direction = ParameterDirection.Output;
                    param.ParameterName = "AdvertisementId";
                    param.DbType = DbType.Int32;


                    cmd.Parameters.Add(param);

                    cmd.ExecuteNonQuery();

                    if (param.Value == DBNull.Value)
                    {
                        return null;
                    }

                    return (int)param.Value;
                }
            }
        }

        /// <summary>
        /// Throws exception if description does not exist in accelerator...
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="inventoryId"></param>
        /// <returns></returns>
        public static MerchandisingDescription FetchAcceleratorDescription(int businessUnitId, int inventoryId)
        {
            string oh = PricingData.GetOwnerHandle(businessUnitId);
            Pair<VehicleHandle, SearchHandle> vhsh = PricingData.GetVehicleHandle(oh, inventoryId);
            string vh = vhsh.First.Value;

            //using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            using (IDataConnection con = Database.GetConnection(Database.MarketDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    //now using synonym

                    //cmd.CommandText = "INTERFACELINK.MerchandisingInterface.Interface.Advertisement#Fetch";
                    cmd.CommandText = "Merchandising.Advertisement#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.AddRequiredParameter( "OwnerHandle", oh, DbType.String);
                    cmd.AddRequiredParameter( "VehicleHandle", vh, DbType.String);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new MerchandisingDescription(reader, true, businessUnitId);
                    }
                }
            }
        }

        #endregion
    }
}