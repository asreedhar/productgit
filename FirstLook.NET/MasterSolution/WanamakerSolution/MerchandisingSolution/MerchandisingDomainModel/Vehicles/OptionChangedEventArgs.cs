using System;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class OptionChangedEventArgs : EventArgs
    {
        public OptionChangedEventArgs(string optionCode, bool selected, string chromeCategoryList)
        {
            Cancel = false;
            Equipment = new DetailedEquipment(optionCode, "", chromeCategoryList, string.Empty, false);
            Selected = selected;
        }

        public OptionChangedEventArgs(DetailedEquipment equipment, bool selected)
        {
            Cancel = false;
            Equipment = equipment;
            Selected = selected;
        }

        public DetailedEquipment Equipment { get; set; }
        public bool Selected { get; set; }
        public bool Cancel { get; set; }
    }

}