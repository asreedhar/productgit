﻿

using System.Collections.Generic;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class GenericEquipmentCollectionDuplicateRemover
    {
        private readonly GenericEquipmentReplacementCollection _replacementRules;
        private readonly GenericEquipmentCollection _generics;
        private List<ITieredEquipment> _filteredGenerics = new List<ITieredEquipment>();
        private List<int> _excludedCategories;

        public GenericEquipmentCollectionDuplicateRemover (GenericEquipmentReplacementCollection replacementRules, GenericEquipmentCollection generics)
        {
            _replacementRules = replacementRules;
            _generics = generics;
        }

        public List<ITieredEquipment> RemoveDuplicates ()
        {
            ApplyGenericEquipmentReplacementRules();
            return _filteredGenerics;
        }


        private void ApplyGenericEquipmentReplacementRules ()
        {
            InitialSetup();
            var replacementGenerics = CreateReplacementGenericsForApplicableRules();
            _filteredGenerics.AddRange( replacementGenerics );
            AddGenricsToFilteredList();
        }

        private void InitialSetup ()
        {
            _replacementRules.SortByCategoryIdCountDesc();
            _filteredGenerics = new List<ITieredEquipment>();
            _excludedCategories = new List<int>();
        }

        private void AddGenricsToFilteredList ()
        {
            var count = _generics.Count;
            for ( var i = 0; i < count; ++i )
            {
                var generic = _generics.Item(i);
                if ( _excludedCategories.Contains( generic.Category.CategoryID ) ) continue;
                _filteredGenerics.Add( generic );
                _excludedCategories.Add( generic.Category.CategoryID );

            }
        }

        private IEnumerable<GenericEquipmentReplacement> CreateReplacementGenericsForApplicableRules ()
        {
            var retList = new List<GenericEquipmentReplacement>();
            var ruleCount = _replacementRules.Count;
            for ( var i = 0; i < ruleCount; ++i )
            {
                var gme = _replacementRules.Item(i);
                if (!gme.IsIncluded(_generics) || gme.IsWhollyContained(_excludedCategories)) continue;
                retList.Add(gme);
                _excludedCategories.AddRange(from CategoryLink category in gme.IncludesCategories
                                             select category.CategoryID);
            }
            return retList;
        }
    }
}
