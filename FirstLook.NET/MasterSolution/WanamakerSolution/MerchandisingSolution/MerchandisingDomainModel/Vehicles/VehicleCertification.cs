﻿using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Exceptions;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using Google.Apis.Util;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    //NOTE: This class actually fits in an undefined super-domain (both merchandising and pricing domains are currently included).
    public class VehicleCertification
    {
		public class FailedToGetManufacturer : Exception { }

	    public static Manufacturer GetManufacturer(int inventoryId)
	    {
			var cmd = new GetInventoryManufacturerMakeInfo(inventoryId);
			AbstractCommand.DoRun(cmd);
			if (cmd.Result.ManufacturerId.HasValue && cmd.Result.ManufacturerName.IsNotNullOrEmpty())
				return new Manufacturer(cmd.Result.ManufacturerId.Value, cmd.Result.ManufacturerName);

			throw new FailedToGetManufacturer();
	    }

        public static VehicleUpdateCertificationResult CreateOrUpdate( int businessUnitId, int inventoryId, bool certified, int certifiedProgramId, string certifiedId, string username, bool autoSave = false)
        {
			var manufacturerCertified = false; 
			var actualCertifiedProgramId = certifiedProgramId > 0 ? certifiedProgramId : default(int?);
	        var actualCertifiedId = string.IsNullOrEmpty(certifiedId) ? null : certifiedId;

			var mfrCommand = new GetInventoryManufacturerMakeInfo(inventoryId);
			AbstractCommand.DoRun(mfrCommand);
			var manufacturerId = mfrCommand.Result.ManufacturerId ?? -1;
	        var make = mfrCommand.Result.MakeName ?? string.Empty;

			if (actualCertifiedProgramId.HasValue)
			{	// any program is selected (either dealer or manufacturer programs)

				// get the programs available at this dealer to make sure this is [still] a valid program
				var getCmd = new CertifiedProgamsForDealerMakeCommand(businessUnitId, make);
				AbstractCommand.DoRun(getCmd);

				if (getCmd.Programs.All(x => x.Id != certifiedProgramId))
					throw new ProgramNoLongerAvailableException();

				// program is valid, is it a manufacturer program?
				manufacturerCertified = getCmd.Programs.First(x => x.Id == certifiedProgramId).ProgramType == "MFR";
			}
	
			using( var transaction = new TransactionScope() )
            {

				using( var connection = Common.Core.Data.Database.Connection( Database.IMTDatabase) )
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }
                    using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = "dbo.InventoryCertified#Update";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.AddParameter( "@BusinessUnitID", DbType.Int32, businessUnitId );
                        cmd.AddParameter( "@InventoryID", DbType.Int32, inventoryId );
						cmd.AddParameter("@IsCertified", DbType.Boolean, certified);

                        cmd.ExecuteNonQuery();
                    }

                    using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = "dbo.Inventory_CertificationNumber#Update";
                        cmd.CommandType = CommandType.StoredProcedure;

						CommonMethods.AddWithValue(cmd, "InventoryID", inventoryId, DbType.Int32);
	                    CommonMethods.AddWithValue(cmd, "CertifiedID", actualCertifiedId, DbType.String, true);
						// TODO: rename this column/parameter to something like CertifiedIdFormatId (1 can be GM)
						CommonMethods.AddWithValue(cmd, "ManufacturerID", manufacturerCertified?manufacturerId:0, DbType.Int32);
						CommonMethods.AddWithValue(cmd, "UserName", username, DbType.String);
						CommonMethods.AddWithValue(cmd, "CertifiedProgramId", actualCertifiedProgramId, DbType.Int32, true);

	                    if (autoSave)
		                    CommonMethods.AddWithValue(cmd, "CertifiedProgramAutoSavedDate", DateTime.Now, DbType.DateTime);

                        cmd.ExecuteNonQuery();
                    }
                }
                transaction.Complete();
            }

			return new VehicleUpdateCertificationResult { ManufacturerCertified = manufacturerCertified };
        }

	    public static bool ValidateCertifiedId(string certifiedId, string manufacturer)
        {
	        if (manufacturer.ToLower() != "general motors") return true; // we only care about GM right now.

			var regexAndMessage = GetVerficationRegexAndMessage(manufacturer);

			return Regex.IsMatch(certifiedId, regexAndMessage.Item1);
        }

	    private static Tuple<string, string> GetVerficationRegexAndMessage(string manufacturer)
        {
            // empty OR digits{6,8}
	        return manufacturer.ToLower() == "general motors" ? new Tuple<string, string>(@"^([0-9]{6,8})?$", @"GM Certified ID must be between 6 and 8 digits.") : new Tuple<string, string>("", "");
        }

        [Serializable]
        public struct Manufacturer
        {
            readonly int _id;
            public int Id { get { return _id; } }

            readonly string _name;
            public string Name { get { return _name; } }

            public Manufacturer( int id, string name )
            {
                _id = id;
                _name = name;
            }
        }


    }
}
