using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    class StepStatusCollectionRepository : IStepStatusCollectionRepository
    {
        public IEnumerable<IStepStatus> Fetch(int businessUnitId, int inventoryId)
        {
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandText = "Merchandising.builder.VehicleStatus#Fetch";
                cmd.CommandType = CommandType.StoredProcedure;

                Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);

                using (var reader = cmd.ExecuteReader())
                {
                    var inventoryIdColumn = reader.GetOrdinal("inventoryId");
                    var statusTypeIdColumn = reader.GetOrdinal("statusTypeId");
                    var statusLevelColumn = reader.GetOrdinal("statusLevel");
                    var versionColumn = reader.GetOrdinal("version");
                    var defaultStatusLevelColumn = reader.GetOrdinal("defaultStatusLevel");

                    while (reader.Read())
                    {
                        var version = new byte[8];
                        reader.GetBytes(versionColumn, 0, version, 0, 8);
                        yield return new StepStatus(
                            businessUnitId,
                            reader.GetInt32(inventoryIdColumn),
                            (StepStatusTypes) reader.GetInt32(statusTypeIdColumn),
                            reader.IsDBNull(statusLevelColumn)
                                ? (int?) null
                                : reader.GetInt32(statusLevelColumn),
                            reader.IsDBNull(defaultStatusLevelColumn)
                                ? (int?) null
                                : reader.GetInt32(defaultStatusLevelColumn),
                            version);
                    }
                }
            }
        }

        public void Save(IEnumerable<IStepStatus> stepsToSave, string memberLogin)
        {
            using (var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            {
                foreach (var step in stepsToSave.Where(st => st.IsDirty))
                {
                    SaveStep(memberLogin, step, cn);
                }
            }
        }

        private static void SaveStep(string memberLogin, IStepStatus step, IDbConnection cn)
        {
            using (var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = step.IsNew
                                      ? "Merchandising.builder.VehicleStatus#Create"
                                      : "Merchandising.builder.VehicleStatus#Update";

                Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);
                Database.AddRequiredParameter(cmd, "InventoryId", step.InventoryId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "StatusTypeId", (int) step.StatusTypeId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "StatusLevel", step.StatusLevel, DbType.Int32);
                Database.AddRequiredParameter(cmd, "BusinessUnitId", step.BusinessUnitId, DbType.Int32);
                if (!step.IsNew)
                    Database.AddRequiredParameter(cmd, "Version", step.Version, DbType.Binary);

                cmd.ExecuteNonQuery();
            }
        }
    }
}