using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.MarketingData;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews.DAL;
using FirstLook.Merchandising.DomainModel.Templating.Previews;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using log4net;
using MvcMiniProfiler;
using VehicleDataAccess;
using FirstLook.Merchandising.DomainModel.Commands;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /* This class is used through reflection to read property values when the ad is constructed. Any property with
     * a comment above it like $base.EngineDetails$ means that property is called through reflection. The MerchandisingPoints table
     * contains these strings. Do not change the property names. Even if it doesn't have a comment above the property think 
     * twice before changing the name.
     */
    [Serializable]
    public class VehicleTemplatingData : RandomUtility, IVehicleTemplatingData
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(VehicleTemplatingData).FullName);
        #endregion


        #region  Injected Depdencies

        internal IPreviewPreferencesDao PreviewPreferencesDAO { get; set; }
 
        #endregion


        private ITemplateMediator _mediator;
        private string _requestingMemberLogin;
        private IVehicleConfiguration _vehicleConfig;
        private IInventoryData inventoryItem;

        private int _inventoryId;
        private int _businessUnitId;


        public VehicleTemplatingData()
        {
            InitializeDependencies();
            _requestingMemberLogin = string.Empty;
        }

        public VehicleTemplatingData(int businessUnitId, int inventoryId, string requestingMemberLogin, ITemplateMediator mediator)
            : this(businessUnitId, inventoryId, requestingMemberLogin)
        {
            InitializeDependencies();
            _mediator = mediator;
        }


        public VehicleTemplatingData(int businessUnitId, int inventoryId, string requestingMemberLogin)
        {
            Log.DebugFormat("VehicleTemplatingData() ctor called with businessUnitId {0}, inventoryId {1}, login '{2}'",
                            businessUnitId, inventoryId, requestingMemberLogin);

            InitializeDependencies();

            _businessUnitId = businessUnitId;
            _inventoryId = inventoryId;
            _requestingMemberLogin = requestingMemberLogin;

            using (MiniProfiler.Current.Step("VehicleTemplatingData.OrderAndFilterEquipment"))
            {
                //preload the major equipment (requires vehicleAttributes)           
                VehicleConfig.OrderAndFilterEquipment(businessUnitId, Preferences.MaxOptionsByTier);
                    //order the equipment now...
            }
        }


        private void InitializeDependencies()
        {
            if (PreviewPreferencesDAO == null)
            {
                PreviewPreferencesDAO = new PreviewPreferencesDao();
            }
        }

        #region priceData

        /*$base.HighInternetPrice$*/
        public string HighInternetPrice
        {
            get
            {
                
                return string.Format("{0:c0}", InventoryItem.HighInternetPrice);
            }
        }

        public bool IsPriceReduced
        {
            get
            {
                return InventoryItem.IsPriceReduced;
            }
        }
        public bool IsPriceReducedEnough
        {
            get
            {
                return InventoryItem.IsPriceReducedEnough(Preferences.MinGoodPriceReduction);
            }
        }
        public bool HasDisplayablePrice
        {
            get
            {
                return InventoryItem.ListPrice > 0;
            }
        }
        
        public string ListPrice
        {
            get
            {
                return string.Format("{0:c0}",InventoryItem.ListPrice);
            }
        }

        private decimal GetMsrp()
        {
            decimal msrp = inventoryItem.MSRP;

            var optionsCommand = GetMiscSettings.GetSettings(_businessUnitId);
            var priceNewCars = optionsCommand.HasSettings && optionsCommand.PriceNewCars;

            if (!priceNewCars) //get msrp from chrome if not pricing new cars. We know MSRP is correct if this option is turned on
                msrp = VehicleAttributes.OriginalMsrp;

            return msrp;
        }

        public bool HasGoodPriceBelowMSRP
        {
            get
            {
                var msrp = GetMsrp();

                return (InventoryItem.ListPrice > Preferences.MinValidListPrice &&
                              (msrp - InventoryItem.ListPrice) >=
                              Preferences.GoodMsrpDifferential);
            }
        }

        /*$base.OriginalMSRP$*/
        public string OriginalMsrp
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.OriginalMsrp);
                var msrp = GetMsrp();
                return string.Format("{0:c0}", Math.Round(msrp / 100) * 100) + "*";
            }
        }

        private const decimal minValidBookoutValue = 100.0m;
        public bool HasAvailableBookout(BookoutCategory category)
        {
            bool retVal = false;

            if (Bookouts.Contains(category))
            {
                Bookout bo = Bookouts.GetBookout(category);
                if (bo.IsValid &&
                    bo.IsAccurate &&
                    (InventoryItem.ListPrice >= Preferences.MinValidListPrice) &&
                    (bo.BookValue - InventoryItem.ListPrice > minValidBookoutValue))
                {
                    retVal = true;
                }
            }
            return retVal;
        }

        /*$base.PriceBelowBook$*/
        public string PriceBelowBook
        {
            get
            {
                decimal val = PreferredBookValue - InventoryItem.ListPrice;

                int valRnd = 100*(((int) val)/100);

                AddedDescriptionContent(MerchandisingDescriptionItemType.PriceBelowBook);

                return valRnd.ToString("c0");
            }
        }

        /*$base.RoughPriceBelowBook$*/
        public string RoughPriceBelowBook
        {
            get
            {
                decimal val = PreferredBookValue - InventoryItem.ListPrice;
                string retStr = string.Empty;

                int valRnd = 100*(((int) val)/100);
                if (valRnd >= 2000)
                {
                    retStr = "Thousands*";
                }
                else if (valRnd >= 200)
                {
                    retStr = "Hundreds*";
                }

                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.PriceBelowBookRough);
                }

                return retStr;
            }
        }

        /*$base.RoughPriceBelowMSRP$*/
        public string RoughPriceBelowMSRP
        {
            get
            {

                decimal val = VehicleAttributes.OriginalMsrp - InventoryItem.ListPrice;
                string retStr = string.Empty;

                int valRnd = 100*(((int) val)/100);
                if (valRnd >= 2000)
                {
                    retStr = "Thousands*";
                }
                else if (valRnd >= 200)
                {
                    retStr = "Hundreds*";
                }

                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.PriceBelowMsrpRough);
                }

                return retStr;
            }
        }

        /*$base.PriceBelowMSRP$*/
        public string PriceBelowMSRP
        {
            get
            {
                decimal val = VehicleAttributes.OriginalMsrp - InventoryItem.ListPrice;

                int valRnd = 100*(((int) val)/100);

                AddedDescriptionContent(MerchandisingDescriptionItemType.PriceBelowMsrp);

                return "" + valRnd.ToString("c0") + "*";
            }
        }

        public bool HasFinancingAvailable
        {
            get
            {
                return !string.IsNullOrEmpty(SpecialFinancing);
                //return SpecialsCollection.HasSpecialFinancing(businessUnitId, VehicleConfig.SpecialID,InventoryItem.CertificationTypeId);
            }
        }
        private string financingText;

        /*$base.SpecialFinancing$*/
        public string SpecialFinancing
        {
            get
            {

                if (string.IsNullOrEmpty(financingText))
                {
                    //if a specific special is selected...
                    try
                    {
                        SetFinancingSpecialText(VehicleConfig.SpecialID);
                    }
                    catch
                    {
                        //silent catch statement, shday -bf.
                    }
                }

                if (VehicleConfig.SpecialID != FinancingSpecial.DoNotUseFinancingSpecialId && string.IsNullOrEmpty(financingText))
                {
                    //get any outstanding specials for the vehicle
                    SpecialsCollection specials = SpecialsCollection.FetchCurrentSpecials(1, _businessUnitId,
                                                                                          InventoryItem.
                                                                                              CertificationTypeId);
                    foreach (FinancingSpecial special in specials)
                    {
                        financingText = special.getSpecialDescription(InventoryItem);
                        if (!string.IsNullOrEmpty(financingText))
                        {

                            AdjustExpirationDateForSpecial(special);
                            break;
                        }
                    }
                }

                //log the inclusion of the special text if it wasn't blank
                if (!string.IsNullOrEmpty(financingText))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.SpecialFinancing);
                }
                return financingText;
            }
        }

        /// <summary>
        /// Set the financing values based on a given special Id
        /// </summary>
        /// <param name="specialId"></param>
        private void SetFinancingSpecialText(int specialId)
        {



            if (specialId > 0)
            {

                FinancingSpecial special = FinancingSpecial.Fetch(1, _businessUnitId, specialId);
                financingText = special.getSpecialDescription(InventoryItem);

                if (!string.IsNullOrEmpty(financingText))
                {
                    AdjustExpirationDateForSpecial(special);
                }
            }
            else
            {
                //special was a non-value (negative for now), so clean out the possible vals
                financingText = string.Empty;
                ExpirationDate = null;
            }
        }

        private void AdjustExpirationDateForSpecial(FinancingSpecial special)
        {
            if (!ExpirationDate.HasValue ||
                (special.ExpirationDate.HasValue && special.ExpirationDate.Value < ExpirationDate.Value))
            {
                ExpirationDate = special.ExpirationDate;
            }
        }

        /// <summary>
        /// Set the financing special for a vehicle
        /// </summary>
        /// <param name="specialId"></param>
        /// <param name="memberLogin"></param>
        public void SetFinancingSpecial(int specialId, string memberLogin)
        {
            if (VehicleConfig.SpecialID != specialId)
            {
                VehicleConfig.SpecialID = specialId;
                VehicleConfig.Save(_businessUnitId, memberLogin);
            }
            SetFinancingSpecialText(specialId);
        }

        public decimal PreferredBookValue
        {
            get
            {

                decimal val = Bookouts.GetValue(Preferences.PrefferedBookoutCategory, true, true);
                if (val > 0)
                {
                    switch (Preferences.PrefferedBookoutCategory)
                    {
                        case BookoutCategory.NadaCleanRetail:
                            AddedDescriptionContent(MerchandisingDescriptionItemType.NadaValue);
                            break;
                        case BookoutCategory.KbbRetail:
                            AddedDescriptionContent(MerchandisingDescriptionItemType.KelleyBlueBookValue);
                            break;
                        default:
                            break;

                    }
                }
                return val;
            }
        }
        public bool HasGoodPriceBelowBook
        {
            get
            {
                return InventoryItem.ListPrice > Preferences.MinValidListPrice &&
                       ((PreferredBookValue - InventoryItem.ListPrice) >=
                        Preferences.GoodBookDifferential);
            }
        }

        private BookoutCollection bookouts;
        public BookoutCollection Bookouts
        {
            get
            {
                if (bookouts == null)
                {
                    bookouts = BookoutCollection.Fetch(_businessUnitId, _inventoryId);
                }
                return bookouts;
            }
        }

        /*$base.PreferredBook$*/
        public string PreferredBook
        {
            get
            {
                return Bookout.GetBookoutCategoryConsumerDescription(Preferences.PrefferedBookoutCategory);
            }
        }


        /*$base.TargetPriceDisplay$*/
        public string TargetPriceDisplay
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.PriceBelowMsrp);
                return String.Format("{0:c0}", TargetPrice);
            }
        }

        public decimal TargetPrice
        {
            get { return Math.Ceiling(InventoryItem.ListPrice / 1000) * 1000 - 5.0M; }
        }

        /*$base.CountBelowBook$*/
        public int CountBelowBook
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.CountBelowBook);
                return new InventoryDataSource().GetCountBetterThanBookDifference(_businessUnitId,
                                                                                  Preferences.GoodBookDifferential);
            }
        }

        /*$base.CountInPriceRange$*/
        public int CountInPriceRange
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.CountBelowPrice);
                return new InventoryDataSource().GetCountBelowTargetPrice(_businessUnitId, TargetPrice);
            }
        }

        public int CountBelowTwentyThousandDollars
        {
            get
            {
                return GetCountBelowTarget(15000.0m);
            }
        }
        public int CountBelowFifteenThousandDollars
        {
            get
            {
                return GetCountBelowTarget(15000.0m);
            }
        }
        public int CountBelowTenThousandDollars
        {
            get
            {
                return GetCountBelowTarget(10000.0m);
            }
        }
        public int CountBelowNineThousandDollars
        {
            get
            {
                return GetCountBelowTarget(9000.0m);
            }
        }
        public int CountBelowEightThousandDollars
        {
            get
            {
                return GetCountBelowTarget(8000.0m);
            }
        }
        public int CountBelowSevenThousandDollars
        {
            get
            {
                return GetCountBelowTarget(7000.0m);
            }
        }
        private int GetCountBelowTarget(decimal targetPrice)
        {
            AddedDescriptionContent(MerchandisingDescriptionItemType.CountBelowPrice);
            return new InventoryDataSource().GetCountBelowTargetPrice(_businessUnitId, targetPrice);
        }

        /*$base.DisclaimerDate$*/
        public string DisclaimerDate
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.PriceDisclaimer);

                return DateTime.Today.ToShortDateString();
            }
        }
                
        #endregion

        #region propertiesAndLoaders
        public DateTime? ExpirationDate { get; set; }
       
        private DealerAdvertisementPreferences preferences;

        private VehicleAttributes vehicleAttributes;

        public VehicleAttributes VehicleAttributes
        {
            get
            {
                if (vehicleAttributes == null)
                {
                    vehicleAttributes = VehicleAttributes.Fetch(VehicleConfig.ChromeStyleID, InventoryItem.VIN);
                }
                return vehicleAttributes;
            }
            set { vehicleAttributes = value; }
        }

       
        public IVehicleConfiguration VehicleConfig
        {
            get
            {
                if (_vehicleConfig == null)
                {
                    _vehicleConfig = VehicleConfiguration.FetchOrCreateDetailed(_businessUnitId, _inventoryId, _requestingMemberLogin ?? String.Empty);
                }
                return _vehicleConfig;
            }
            set { _vehicleConfig = value; }
        }
        
        public IInventoryData InventoryItem
        {
            get
            {
                if (inventoryItem == null)
                {
                    inventoryItem = InventoryData.Fetch(_businessUnitId, _inventoryId);
                }
                return inventoryItem;
            }
            set { inventoryItem = value; }
        }

       
        public DealerAdvertisementPreferences Preferences
        {
            get
            {
                if (preferences == null)
                {
                    preferences = DealerAdvertisementPreferences.Fetch(_businessUnitId);
                }
                return preferences;
            }
            set { preferences = value; }
        }

        public ITemplateMediator Mediator
        {
            get { return _mediator; }
            set { _mediator = value; }
        }


        #endregion


        #region vehicleBasics

        public bool IsLowMilesPerYear
        {
            get
            {
                return (InventoryItem.MileageReceived > 0 && //mileage is legit
                              InventoryItem.MileageReceived < Preferences.MaxMileageThatIsLow &&
                    //mileage isn't too high in absolute terms
                              (int)InventoryItem.GetMilesPerYearCategory() <
                              (int)MilesPerYearCategories.Average); //mileage is low or verylow
            }
        }


        /*$base.YearMakeModel$*/
        public string YearMakeModel
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.Year);
                AddedDescriptionContent(MerchandisingDescriptionItemType.Make);
                AddedDescriptionContent(MerchandisingDescriptionItemType.Model);
                return (InventoryItem.Year + " " + InventoryItem.Make + " " + InventoryItem.Model);
            }
        }

        public bool IsNewVehicle
        {
            get
            {
                return InventoryItem.IsNew();
            }
        }

        public bool HasInteriorType
        {
            get
            {
                return VehicleConfig.HasInteriorType;
            }
        }



        public bool HasHighPassengerCountForClass()
        {
            string mktLower = InventoryItem.MarketClass.ToLower();
            if (mktLower.Contains("suv") || mktLower.Contains("sport utility vehicle"))
            {
                return VehicleAttributes.PassengerCapacity >= Preferences.HighPassengerCountForSuvs;
            }

            if (mktLower.Contains("truck"))
            {
                return VehicleAttributes.PassengerCapacity >= Preferences.HighPassengerCountForTrucks;
            }

            return false;
        }

        /*$base.MilesPerYearCategory$*/
        public string MilesPerYearCategory
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.MileagePerYearCategory);
                return InventoryItem.GetMilesPerYearDescription;
            }
        }

        public string Make
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.Make);
                return InventoryItem.Make;
            }
        }

        public string Model
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.Model);
                return InventoryItem.Model;
            }
        }

        public string Year
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.Year);
                return InventoryItem.Year;
            }
        }

       
        public bool HasTrim
        {
            get
            {   
                // Adding model length to calculation.
                return (InventoryItem.Trim.Trim().Length + 
                        VehicleConfig.AfterMarketTrim.Trim().Length +
                        Model.Length) > 0;
            }
        }

        
        /*$base.Trim$*/
        public string Trim
        {
            get
            {
                // Get trim   
                var customTrim = VehicleConfig.AfterMarketTrim;
                var invTrim = InventoryItem.Trim;
                var retTrim = string.IsNullOrWhiteSpace(customTrim) ?  invTrim.Trim() : customTrim;
                if (!string.IsNullOrEmpty(retTrim))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Trim);
                    return retTrim;
                }

                // Get model instead.
                return Model;
            }
        }

        /*$base.Mileage$*/
        public string Mileage
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.Mileage);
                return InventoryItem.DisplayMileage;
            }
        }
           
        public bool HasColors
        {
            get
            {
                return EvaluateSingleCondition(TemplateCondition.HasExteriorColor) || EvaluateSingleCondition(TemplateCondition.HasInteriorColor);
            }
        }
        
        public string InteriorType
        {
            get
            {                
                string desc = VehicleConfig.InteriorType;
                if (!string.IsNullOrEmpty(desc))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.InteriorType);
                    return desc;
                }
                return string.Empty;
            }
        }

        /*$base.Colors$*/
        public string Colors
        {
            get
            {
                string retVal = string.Empty;
                if (_mediator != null)
                {
                    //if both color sets avail, create long string, if only one, use it alone
                    bool hasExt = EvaluateSingleCondition(TemplateCondition.HasExteriorColor);
                    bool hasInt = EvaluateSingleCondition(TemplateCondition.HasInteriorColor);
                    if (hasExt && hasInt)
                    {
                        retVal = string.Format("{0} exterior and {1} interior", ExteriorColor, InteriorColor);
                    }else if (hasExt)
                    {
                        retVal = string.Format("{0} exterior", ExteriorColor);
                    }else if (hasInt)
                    {
                        retVal = string.Format("{0} interior", InteriorColor);
                    }

                    if (retVal.Length > 0)
                    {
                        //use simple indefinite article selector
                        //Regex rg = new Regex(@"[aeiou]+");  //is there a library out there to do this?
                    }
                }


                return retVal;
            }   
        }

        /*$base.InteriorColor$*/
        public string InteriorColor
        {
            get
            {
                string val = VehicleConfig.InteriorColor;
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.InteriorColor);
                }
                return val;
            }
        }

        /*$base.ExteriorColor$*/
        public string ExteriorColor
        {
            get
            {

                string retStr = VehicleConfig.ExteriorColorDescription;                

                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ExteriorColor);
                }

                return retStr;
            }
        }


        public string StockNumber
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.StockNumber);
                
                return InventoryItem.StockNumber;
            }
        }
        #endregion

        #region certifiedDetails
        private ManufacturerCertification certification;
        protected ManufacturerCertification Certification
        {
	        get
	        {
		        if (certification == null)
		        {
			        certification = ManufacturerCertification.Fetch(InventoryItem);
		        }
		        
				return certification;

	        }
        }

        public bool HasCertificationPreview
        {
            get
            {
                var certifiedType = ManufacturerCertification.GetCertificationType(InventoryItem.Make);
                if (certifiedType == CertificationType.Undefined)
                    return false;
                
                return Certification.DisplayContains(CertificationFeatureType.DealerCustomPreviewText);
            }
        }
        public string CertificationPreview
        {
            get
            {
                
                string retStr = Certification.DealerPreviewText.TrimStart(" ".ToCharArray());

                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.DetailedCertified);
                }
                return retStr;
            }
        }
        public bool HasCertificationDetails
        {
            get
            {
                return Certification.ProgramFeatures.Count > 0;
            }
        }

        /*$base.CertificationDetails$*/
        public string CertificationDetails
        {
            get
            {
                
                string retStr = (Certification.GetFullDetails(", ")).TrimStart(" ".ToCharArray());

                //take out new lines which is causing the ad to be dirty when rendered to html.
                retStr = Regex.Replace(retStr, "\\r\\n", " ");
                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.DetailedCertified);
                }
                return retStr;
            }
        }
        public bool IsCertified
        {
            get
            {
                return InventoryItem.Certified // <-- Manufacturer Certified
	                   || (inventoryItem.CertifiedProgramId.HasValue && inventoryItem.CertifiedProgramId.Value > 0);  // <-- Dealer Certified            
            }
        }

        /*$base.Certified$*/
        public string Certified
        {
            get
            {
                string retStr = CertificationTitle;
                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.SimpleCertified);
                }
                return retStr;
            }
        }


        public string CertificationTitle
        {
            get
            {
                string retStr = InventoryItem.CertificationTitle;               
                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.CertifiedType);
                }
                return retStr;
            }
        }

        public string CertifiedID
        {
            get
            {
                string retStr = InventoryItem.CertifiedID;
                if( !string.IsNullOrEmpty( retStr ) )
                {
                    AddedDescriptionContent( MerchandisingDescriptionItemType.CertifiedID );
                }
                return retStr;
            }
        }
        #endregion             

        #region consumerInfo

        /*$base.FuelCity$*/
        public string FuelCity
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.CityGasMileage);
                return ConsInfo.FuelCity;
            }
        }

        /*$base.FuelHwy$*/
        public string FuelHwy
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.HighwayGasMileage);
                return ConsInfo.FuelHwy;
            }
        }

        /*$base.FuelHwyCity$*/
        public string FuelHwyCity
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.HighwayCityGasMileage);
                return FuelHwy + "/" + FuelCity;
            }            
        }

        public bool GetsGoodGasMileage
        {
            get
            {
                return (ConsInfo.FuelCityInt >=
                        Preferences.GetGoodCityMileage(InventoryItem.SegmentId) ||
                        ConsInfo.FuelHwyInt >=
                        Preferences.GetGoodHighwayMileage(InventoryItem.SegmentId));
            }
        }

        /*$base.BestGasMileage$*/
        public string BestGasMileage
        {
            get
            {
                if (ConsInfo.FuelHwyInt > ConsInfo.FuelCityInt)
                {
                    return FuelHwy;
                }
                else
                {
                    return FuelCity;
                }
            }
        }

        /*$base.GoodWarranties$*/
        public string GoodWarranties
        {
            get
            {
            string retStr = ConsInfo.GetGoodWarrantyDescriptions(Preferences.GoodWarrantyMileageRemaining,
                                                                     InventoryItem.MileageReceived,
                                                                     InventoryItem.InServiceDate,
                                                                     Preferences.MaxWarrantiesToDisplay);

                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.GoodWarranties);
                }
                return retStr;
            }
        }

       
        public bool HasRoadsideAssistance
        {
            get
            {
                return ConsInfo.HasRoadsideAssistanceRemaining(Preferences.GoodWarrantyMileageRemaining,
                                                               InventoryItem.MileageReceived,
                                                               InventoryItem.InServiceDate);
            }
        }
        public string RoadsideAssistance
        {
            get
            {
                string retStr = ConsInfo.RoadsideAssistance;
                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.RoadsideAssistance);
                }
                return retStr;
            }
        }

       
        public ConsumerInfo ConsInfo
        {
            get
            {
                if (consumerInfo == null)
                {
                    consumerInfo = ConsumerInfo.Fetch(VehicleConfig.ChromeStyleID, VehicleConfig.VehicleOptions.GetCategoryIdList(), VehicleConfig.VehicleOptions.options);
                }
                return consumerInfo;
            }
            set { consumerInfo = value; }
        }

        private ConsumerInfo consumerInfo;
        #endregion

        #region crashTests
        public bool HasGoodCrashTestRatings
        {
            get { return ConsInfo.HasGoodCrashRatings(Preferences.GoodCrashTestRating); }
        }

        /*$base.GoodCrashTests$*/
        public string GoodCrashTests
        {
            get
            {
                string retStr = ConsInfo.GetGoodCrashRatingText(Preferences.GoodCrashTestRating,
                                                                Preferences.MaxCrashTestRatingsToDisplay);
                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.GoodCrashTests);
                }
                return retStr;
            }
        }

        /*$base.DriverFrontCrash$*/
        public string DriverFrontCrash
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.DriverCrashTest);
                return ConsInfo.DriverFrontCrash;
            }
        }

        /*$base.DriverSideCrash$*/
        public string DriverSideCrash
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.DriverCrashTest);
                return ConsInfo.DriverFrontCrash;
            }
        }

        /*$base.RolloverRating$*/
        public string RolloverRating
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.RolloverRating);
                return ConsInfo.RolloverRatingText;
            }
        }

        /*$base.BasicWarranty$*/
        public string BasicWarranty
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.BasicWarranty);
                return ConsInfo.BasicWarranty;
            }
        }

        /*$base.DrivetrainWarranty$*/
        public string DrivetrainWarranty
        {
            get
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.DrivetrainWarranty);
                return ConsInfo.DrivetrainWarranty;
            }
        }
        #endregion

        #region carfaxAccessors
        
        private CarfaxInterface carfaxInfo;
        public CarfaxInterface CarfaxInfo
        {
            get
            {
                if (carfaxInfo == null)
                {
                    carfaxInfo = new CarfaxInterface(_businessUnitId, InventoryItem.VIN, _requestingMemberLogin);                    

                }
                return carfaxInfo;
            }
            set { carfaxInfo = value; }
        }

        public bool IsOneOwner
        {
            get
            {
                return CarfaxInfo.IsOneOwner;
            }
        }

        public bool IsCleanCarfaxReport
        {
            get
            {
                return CarfaxInfo.IsCleanReport;
            }
        }

        public string CleanCarfaxReport
        {
            get
            {
                if (CarfaxInfo.IsCleanReport)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.CarfaxCleanReport);
                    return "Clean Carfax Report";
                }

                return string.Empty;
            }
        }

        /*$base.Carfax1Owner$*/
        public string Carfax1Owner
        {
            get
            {
                string retStr = string.Empty;
                if (CarfaxInfo.IsOneOwner)
                {
                    retStr = "CARFAX 1-Owner";
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Carfax1Owner);
                }

                return retStr;
            }
        }
        public bool HasBuybackGuarantee
        {
            get
            {
                return CarfaxInfo.HasBuyBackGuarantee;
            }
        }

        /*$base.BuybackGuarantee$*/
        public string BuybackGuarantee
        {
            get
            {
                string tmp = string.Empty;
                if (CarfaxInfo.HasBuyBackGuarantee)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.CarfaxBuyBackGuarantee);
                    tmp = "Qualifies for CARFAX Buyback Guarantee";
                }
                return tmp;                
            }
        }

        #endregion

        #region autoCheckAccessors

        private AutoCheckInterface autoCheckInfo;
        public AutoCheckInterface AutoCheckInfo
        {
            get
            {
                if (autoCheckInfo == null)
                {
                    autoCheckInfo = new AutoCheckInterface(_businessUnitId, InventoryItem.VIN, _requestingMemberLogin);
                }
                return autoCheckInfo;
            }
        }

        public bool IsAutoCheckOneOwner
        {
            get
            {
                return AutoCheckInfo.IsOneOwner;
            }
        }

        /*$base.AutoCheckOneOwner$*/
        public string AutoCheckOneOwner
        {
            get
            {
                if (IsAutoCheckOneOwner)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.AutoCheckOneOwner);
                    return "AutoCheck One Owner";
                }
                return string.Empty;
            }
        }
        public bool IsAutoCheckAssured
        {
            get
            {
                return AutoCheckInfo.IsAssured;
            }
        }
        public string AutoCheckAssured
        {
            get
            {
                if (IsAutoCheckAssured)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.AutoCheckAssured);
                    return "AutoCheck Assured";
                }
                return string.Empty;
            }
        }
        public bool IsCleanAutoCheckReport
        {
             get
             {
                 return AutoCheckInfo.IsCleanReport;
             }   
        }
        public string CleanAutoCheckReport
        {
            get
            {
                if (IsCleanAutoCheckReport)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.AutoCheckCleanReport);
                    return "Clean AutoCheck Report";
                }
                return string.Empty;
            }
        }
        public bool HasGoodAutoCheckScore
        {
            get { return AutoCheckInfo.HasGoodScore; }
        }
        public string AutoCheckScore
        {
            get
            {
                if (HasGoodAutoCheckScore)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.AutoCheckScore);
                    return string.Format("AutoCheck Score of {0:n0}", AutoCheckInfo.Score);
                }
                return string.Empty;
            }
        }


        #endregion

        #region equipmentAccessors
      
        private List<string> majorEquipmentOfInterest;

        public bool HasEquipment(string equipmentDescription)
        {
            return VehicleConfig.VehicleOptions.Contains(equipmentDescription);
        }
        public bool HasHighlightedPackages
        {
            get
            {
                return VehicleConfig.VehicleOptions.HasHighlightedPackages();
            }
        }


        private List<PreviewEquipmentItem> _mostSearchedTermList;
        protected List<PreviewEquipmentItem> MostSearchedTermList
        {
            get
            {
                if (_mostSearchedTermList == null)
                {
                    _mostSearchedTermCategoryIds = new List<int>();
                    _mostSearchedTermCategoryIds.AddRange(PrepMostSearchedListAndIds());
                }
                return _mostSearchedTermList;
            }
        }
        private List<int> _mostSearchedTermCategoryIds;
        
        private List<int> PrepMostSearchedListAndIds()
        {

            var mstPackagesPair1 = MostSearchedTermMapper.GetHighValuePackageTitles(VehicleConfig.VehicleOptions.options);

            Pair<List<PreviewGenericEquipmentItem>, List<int>> mstPair =
                MostSearchedTermMapper.GetMostSearchedPreviewTerms(VehicleConfig.VehicleOptions.generics);

            _mostSearchedTermList = new List<PreviewEquipmentItem>();
            _mostSearchedTermList.AddRange(mstPackagesPair1);
            _mostSearchedTermList.AddRange(mstPair.First);
            
            var retList = new List<int>();
            retList.AddRange(mstPair.Second);
            if (PreviewPrefs.UseDrivetrain)
            {
                var driveTrain = VehicleConfig.VehicleOptions.GetDrivetrain();
                //when adding manual values that are not in the most searched list, be sure to add the proper "chrome category id" to the used mostSearchedTermCategoryIds
                if (VehicleAttributes.DrivetrainType == DrivetrainType.FourWheelDrive)
                {
                    _mostSearchedTermList.Add( new PreviewGenericEquipmentItem( string.IsNullOrWhiteSpace( driveTrain ) ? "4WD" : driveTrain, (int) ChromeCategory.FourWheelDrive ) );
                    retList.Add((int) ChromeCategory.FourWheelDrive);
                }
                else if (VehicleAttributes.DrivetrainType == DrivetrainType.AllWheelDrive)
                {

                    _mostSearchedTermList.Add( new PreviewGenericEquipmentItem( string.IsNullOrWhiteSpace( driveTrain ) ? "AWD" : driveTrain, (int) ChromeCategory.AllWheelDrive ) );
                    retList.Add((int) ChromeCategory.AllWheelDrive);
                }
            }
            return retList;
        }

        public List<PreviewEquipmentItem> PreviewEquipment
        {
            get
            {
                List<PreviewEquipmentItem> equipStr = new List<PreviewEquipmentItem>();
                if (HasMostSearchedEquipmentAvailable)
                {
                    equipStr.AddRange(MostSearchedTermList);
                }

                //get max count we can add
                int maxCountRemaining = Preferences.MaxOptionsCountInPreview - equipStr.Count;

                List<PreviewEquipmentItem> equip = VehicleConfig.VehicleOptions.GetPreviewList(_mostSearchedTermCategoryIds, Preferences.PreviewEquipmentIsReusable, maxCountRemaining);
                if (equip.Count > 0)
                {
                    equipStr.AddRange(equip);
                }

                equipStr = new PreviewEquipmentItemDuplicateRemover( VehicleConfig.VehicleOptions.replacements, equipStr ).RemoveDuplicates();
                return equipStr;
            }
        }
        public bool HasMostSearchedEquipmentAvailable
        {
            get
            {
                return MostSearchedTermList.Count > 0;
            }
        }
        public bool HasTier1EquipmentAvailable
        {
            get
            {
                Log.Debug("HasTier1EquipmentAvailable called.");
                bool val = VehicleConfig.VehicleOptions.HasEquipmentInTierRemaining(1);
                Log.DebugFormat("Returning {0}", val);
                return val;
            }
        }
        public bool HasEnoughTier1EquipmentAvailable
        {
            get
            {
                Log.Debug("HasEnoughTier1EquipmentAvailable called.");
                bool val = VehicleConfig.VehicleOptions.HasEquipmentInTierRemaining(1, Preferences.MinOptionsPerGroup);
                Log.DebugFormat("Returning {0}", val);
                return val;
            }
        }
        public bool HasTier2EquipmentAvailable
        {
            get
            {
                Log.Debug("HasTier2EquipmentAvailable called.");
                bool val = VehicleConfig.VehicleOptions.HasEquipmentInTierRemaining(2);
                Log.DebugFormat("Returning {0}", val);
                return val;
            }
        }
        public bool HasEnoughTier2EquipmentAvailable
        {
            get
            {
                Log.Debug("HasEnoughTier2EquipmentAvailable called.");
                bool val = VehicleConfig.VehicleOptions.HasEquipmentInTierRemaining(2, Preferences.MinOptionsPerGroup);
                Log.DebugFormat("Returning {0}", val);
                return val;
            }
        }

        /*$base.AfterMarketEquipment$*/
        public string AfterMarketEquipment
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (string ame in VehicleConfig.AfterMarketEquipment)
                {
                    sb.Append(ame);
                    sb.Append(",");
                }

                string retStr = sb.ToString().TrimEnd(",".ToCharArray());
                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.AfterMarketEquipment);
                }
                return retStr;
            }
        }

        /*$base.Loaded$*/
        public string Loaded
        {
            get
            {
                if (VehicleConfig.VehicleOptions.GetCountInTier(1) >= Preferences.CountTier1ForLoaded)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Loaded);
                    return "Loaded";
                }
                return string.Empty;
            }
        }

        /*$base.MajorEquipmentOfInterest$*/
        public List<string> MajorEquipmentOfInterest
        {
            get
            {
                //already set through previous call...return it
                if (majorEquipmentOfInterest != null)
                {
                    return majorEquipmentOfInterest;
                }

                //otherwise, create it
                List<string> retList = new List<string>();
                string str = "";
                /*str = TransmissionOfInterest;
                if (!String.IsNullOrEmpty(str))
                {
                    retList.Add(str);
                }*/

                str = DrivetrainOfInterest;
                if (!String.IsNullOrEmpty(str))
                {
                    //flag drive train so we do not duplicate below
                    VehicleConfig.VehicleOptions.clearDrivetrain();
                    retList.Add(str);
                }
                str = EngineOfInterest;
                if (!String.IsNullOrEmpty(str))
                {
                    VehicleConfig.VehicleOptions.clearEngine();
                    retList.Add(str);
                }
                str = FuelTypeOfInterest;
                if (!String.IsNullOrEmpty(str))
                {
                    VehicleConfig.VehicleOptions.clearFuelType();
                    retList.Add(str);
                }

                //currently, passenger capacity not considered tierzero
                if (retList.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Tier0Equipment);
                }

                if (HasHighPassengerCountForClass())
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.HighPassengerCapacity);

                    retList.Add("Seats " + VehicleAttributes.PassengerCapacity + " Passengers");
                }
                majorEquipmentOfInterest = retList;


                return retList;
            }
        }
        public bool HasValuedOptions
        {
            get
            {
                return VehicleConfig.VehicleOptions.HasValuedOptions;
            }
        }

        /*$base.ValuedOptions$*/
        public List<string> ValuedOptions
        {
            get
            {
                List<string> retList = VehicleConfig.VehicleOptions.ValuedOptions.GetRange(0,Math.Min(Preferences.MaxOptions, VehicleConfig.VehicleOptions.ValuedOptions.Count));
                if (retList.Count > 0)
                {                    
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ConsumerValuedEquipment);
                }
                return retList;
            }
        }
        public bool HasTruckOptions
        {
            get
            {
                return VehicleConfig.VehicleOptions.HasTruckOptions;
            }
        }

        /*$base.TruckOptions$*/
        public List<string> TruckOptions
        {
            get
            {
                List<string> retList = VehicleConfig.VehicleOptions.TruckOptions.GetRange(0, Math.Min(Preferences.MaxOptions, VehicleConfig.VehicleOptions.TruckOptions.Count));
                if (retList.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.TruckEquipment);
                }
                return retList;
            }
        }
        public bool HasSafetyOptions
        {
            get
            {
                return VehicleConfig.VehicleOptions.HasSafetyOptions;
            }
        }

        /*$base.SafetyOptions$*/
        public List<string> SafetyOptions
        {
            get
            {
                List<string> retList = VehicleConfig.VehicleOptions.SafetyOptions.GetRange(0, Math.Min(Preferences.MaxOptions, VehicleConfig.VehicleOptions.SafetyOptions.Count));
                if (retList.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.SafetyEquipment);
                }
                return retList;
            }
        }

        public bool HasConvenienceOptions
        {
            get
            {
                return VehicleConfig.VehicleOptions.HasConvenienceOptions;
            }
        }

        /*$base.ConvenienceOptions$*/
        public List<string> ConvenienceOptions
        {
            get
            {
                List<string> retList = VehicleConfig.VehicleOptions.ConvenienceOptions.GetRange(0, Math.Min(Preferences.MaxOptions, VehicleConfig.VehicleOptions.ConvenienceOptions.Count));
                if (retList.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ConvenienceEquipment);
                }
                return retList;
            }
        }
        public bool HasLuxuryOptions
        {
            get
            {
                return VehicleConfig.VehicleOptions.HasLuxuryOptions;
            }
        }

        /*$base.LuxuryOptions$*/
        public List<string> LuxuryOptions
        {
            get
            {
                List<string> retList = VehicleConfig.VehicleOptions.LuxuryOptions.GetRange(0, Math.Min(Preferences.MaxOptions, VehicleConfig.VehicleOptions.LuxuryOptions.Count));
                if (retList.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.LuxuryEquipment);
                }
                return retList;
            }
        }
        public bool HasSportyOptions
        {
            get
            {
                return VehicleConfig.VehicleOptions.HasSportyOptions;
            }
        }

        /*$base.SportyOptions$*/
        public List<string> SportyOptions
        {
            get
            {
                List<string> retList = VehicleConfig.VehicleOptions.SportyOptions.GetRange(0, Math.Min(Preferences.MaxOptions, VehicleConfig.VehicleOptions.SportyOptions.Count));
                if (retList.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.SportyEquipment);
                }
                return retList;
            }   
        }
        public bool HasThemedEquipment
        {
            get
            {
                return ThemedEquipment.Count > 0;
            }
        }

        /*$base.ThemedEquipment$*/
        public List<string> ThemedEquipment
        {
            get
            {
                if (_mediator.ThemeId.HasValue)
                {
                    ThemeType currTheme = (ThemeType) _mediator.ThemeId.Value;
                    switch (currTheme)
                    {
                        case ThemeType.Family:
                            return SafetyOptions;
                        
                        case ThemeType.WorkUtility:
                            return TruckOptions;
                        
                        case ThemeType.Value:
                            return ConvenienceOptions;
                        
                        case ThemeType.Sport:
                            return SportyOptions;
                        
                        case ThemeType.StudentTeen:
                            return ConvenienceOptions;
                        
                        case ThemeType.Luxury:
                            return LuxuryOptions;
                        default:
                            return Tier1Equipment;

                    }                    
                }
                return Tier1Equipment;
            }
        }

        /*$base.HighlightedPackages$*/
        public List<string> HighlightedPackages
        {
            get
            {
                List<string> equipList = VehicleConfig.VehicleOptions.GetHighlightedPackages();
                if (equipList.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.HighlightedPackages);

                }
                return equipList;
            }
        }

        public List<string> HighlightedPackageSummaries
        {
            get
            {
                List<string> packageNames =
                    VehicleConfig.VehicleOptions.GetHighlightedPackgeSummaries()
                                                .Distinct()
                                                .ToList();

                if (packageNames.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.HighlightedPackages);

                }
                return packageNames;
            }
        }

        public decimal KBBBookValue
        {
            get 
            { 
                return GetBookValue(BookoutCategory.KbbRetail);
            }
        }

        public decimal NADABookValue
        {
            get
            {
                return GetBookValue(BookoutCategory.NadaCleanRetail);
            }
        }

        private decimal GetBookValue(BookoutCategory category)
        {
            if (Bookouts != null && Bookouts.Contains(category))
            {
                return Bookouts.GetBookout(category).BookValue;
            }

            return 0M;
        }

        /*$base.Tier1Equipment$*/
        public List<string> Tier1Equipment
        {
            get
            {
                Log.Debug("Tier1Equipment called.");

                /*List<string> vals = getOptionsFromPrioritizedCategoryList(CategoryCollection.GetPrioritizedCategoryIDList(1)).GetDescriptionList();

                return vals.GetRange(0, Math.Min(vals.Count-1,maxTier1));*/
                List<string> vals = VehicleConfig.VehicleOptions.GetEquipmentDescriptions(1);
                if (vals.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Tier1Equipment);
                }
                Log.DebugFormat("Returning {0}", vals.ToDelimitedString(","));
                return vals;
            }
        }

        /*$base.Tier2Equipment$*/
        public List<string> Tier2Equipment
        {
            get
            {
                Log.Debug("Tier2Equipment called.");                
                
                List<string> vals = VehicleConfig.VehicleOptions.GetEquipmentDescriptions(2);
                if (vals.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Tier2Equipment);
                }
                Log.DebugFormat("Returning {0}", vals.ToDelimitedString(","));
                return vals;
            }
        }

        public List<string> Tier3Equipment
        {
            get
            {
                List<string> vals = VehicleConfig.VehicleOptions.GetEquipmentDescriptions(3);
                if (vals.Count > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Tier3Equipment);
                }
                return vals;
            }
        }

        /*$base.Engine$*/
        public string Engine
        {
            get
            {
                string retVal = VehicleConfig.VehicleOptions.GetEngine();
                if (string.IsNullOrEmpty(retVal))
                {
                    retVal = VehicleAttributes.Engine;
                }
                if (!string.IsNullOrEmpty(retVal))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Engine);
                }
                return retVal;
            }
        }

        /*$base.EngineOfInterest$*/
        public string EngineOfInterest
        {
            get
            {
                
                string retStr = VehicleConfig.VehicleOptions.GetEngine();
                if (string.IsNullOrEmpty(retStr) && VehicleAttributes.UniqueEngine)
                {       
                    retStr = VehicleAttributes.Engine;
                }

                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.EngineOfInterest);
                }

                return retStr;
            }
        }

        /*$base.EngineDetails$*/
        public string EngineDetails
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(Engine);
                string hp = Horsepower;
                if (!string.IsNullOrEmpty(hp))
                {
                    sb.Append(" with ");
                    sb.Append(hp);
                }

                return sb.ToString();
            }
        }

        public string Horsepower
        {
            get
            {
                TechSpecCollection specs = TechSpecCollection.Fetch(VehicleConfig.ChromeStyleID);
                string hp = specs.GetUnofficialTechSpec(TechSpecType.Horsepower, VehicleConfig.VehicleOptions.options);

                if (!string.IsNullOrEmpty(hp))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Horsepower);
                }
                return hp;
            }
        }

        /*$base.Drivetrain$*/
        public string Drivetrain
        {
            get
            {
                string retStr = VehicleConfig.VehicleOptions.GetDrivetrain();
                if (string.IsNullOrEmpty(retStr))
                {
                    retStr = VehicleAttributes.Drivetrain;
                }

                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Drivetrain);
                }
                return retStr;
            }
        }

        /*$base.DrivetrainOfInterest$*/
        public string DrivetrainOfInterest
        {
            get
            {
                string retStr = VehicleConfig.VehicleOptions.GetDrivetrain();
                if (string.IsNullOrEmpty(retStr) && VehicleAttributes.UniqueDrivetrain)
                {
                    retStr = VehicleAttributes.Drivetrain;
                }

                //eliminate what we consider non-interesting (2WD vehicles)
                if (retStr.Contains("FWD") || retStr.Contains("RWD")
                    || retStr.Contains("Front Wheel") || retStr.Contains("Rear Wheel"))
                {
                    retStr = "";
                }

                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.DrivetrainOfInterest);
                }

                return retStr;
            }
        }

        /*$base.Transmission$*/
        public string Transmission
        {
            get
            {
                string retStr = VehicleConfig.VehicleOptions.GetTransmission();
                if (string.IsNullOrEmpty(retStr))
                {
                    retStr = VehicleAttributes.Transmission;
                }

                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.Transmission);
                }

                return retStr;
            }
        }

        /*$base.TransmissionOfInterest$*/
        public string TransmissionOfInterest
        {
            get
            {
                string retStr = VehicleConfig.VehicleOptions.GetTransmission();
                if (string.IsNullOrEmpty(retStr) && VehicleAttributes.UniqueTransmission)
                {
                    retStr = VehicleAttributes.Transmission;
                }

                //present business logic says that automatic trans is not of interest!
                if (retStr.ToLower().Contains("automatic") || retStr.ToLower().Contains("a/t"))
                {
                    retStr = string.Empty;
                }
                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.TransmissionOfInterest);
                }

                return retStr;
            }
        }

        /*$base.FuelType$*/
        public string FuelType
        {
            get
            {
                string retStr = VehicleConfig.VehicleOptions.GetFuelType();
                if (string.IsNullOrEmpty(retStr))
                {
                    retStr = VehicleAttributes.FuelType;
                }

                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.FuelType);
                }

                return retStr;
            }
        }

        /*$base.FuelTypeOfInterest$*/
        public string FuelTypeOfInterest
        {
            get
            {
                string retStr = VehicleConfig.VehicleOptions.GetFuelType();
                if (string.IsNullOrEmpty(retStr) && VehicleAttributes.UniqueFuelType)
                {
                    //SPECIAL CASE - remove gasoline...
                    if (!VehicleAttributes.FuelType.ToLower().Contains("gasoline fuel"))
                    {
                        retStr = VehicleAttributes.FuelType;
                    }
                }

                if (!String.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.FuelTypeOfInterest);
                }

                return retStr;
            }
        }
        #endregion

        #region awardsAndAccolades
        private ModelMarketingCollection marketingCollection;
        public ModelMarketingCollection MarketingCollection
        {
            get
            {
                if (marketingCollection == null)
                {
                    marketingCollection = ModelMarketingCollection.Fetch(VehicleConfig.ChromeStyleID);
                }
                return marketingCollection;
            }
        }

        /*$base.BestModelAward$*/
        public string BestModelAward
        {
            get
            {
                string val = MarketingCollection.GetTopMarketingText(ModelMarketingType.Awards);
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.BestModelAward);
                }
                return val;
            }
        }
        public bool HasAwards
        {
            get
            {
                return MarketingCollection.Contains(ModelMarketingType.Awards);
            }
        }

        /*$base.ModelAwards$*/
        public string ModelAwards
        {
            get
            {
                string val = MarketingCollection.GetFullMarketingText(ModelMarketingType.Awards);
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ModelAwardList);
                }
                return val;
            }
        }

        public string MarketingPreview
        {
            get
            {
                MarketingDataCollection mdc = MarketingList.FindByTag(MarketingTag.Preview);
                string val = mdc.ToString(1, false, this);
                if (!string.IsNullOrEmpty(val))
                {
                    //NOTE CHANGE TO MARKETING PREVIEW IF SEPARATING Preview from body
                    AddedDescriptionContent(MerchandisingDescriptionItemType.MarketingSuperlative);
                }
                return val;
            }
        }
        public string MarketingSuperlative
        {
            get
            {
                MarketingDataCollection mdc = MarketingList.FindByTag(MarketingTag.Superlative);
                string val = mdc.ToString(1, false, this);
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.MarketingSuperlative);
                }
                return val;
            }
        }

        /*$base.MarketingSentence$*/
        public string MarketingSentence
        {
            get
            {
                string val = MarketingList.ToString(1, false, this);
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.MarketingSentence);
                }
                return val;
            }
        }

        /*$base.MarketingSummary$*/
        public string MarketingSummary
        {
            get
            {
                string val = MarketingList.ToString(Preferences.MaxCountMarketingReviews, false, this);
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.MarketingSummary);
                }
                return val;
            }
        }
        private MarketingDataCollection marketingList;
        public MarketingDataCollection MarketingList
        {
            get
            {
                if (marketingList == null)
                {
                    marketingList = MarketingDataCollection.Fetch(VehicleConfig.ChromeStyleID);
                }
                return marketingList;
            }
        }
        public bool HasMarketingPreviewSuperlative
        {
            get
            {

                return MarketingList.FindByTag(MarketingTag.Preview).Count > 0;

            }
        }

        public bool HasMarketingSuperlative
        {
            get
            {

                return MarketingList.FindByTag(MarketingTag.Superlative).Count > 0;

            }
        }
       
        public bool HasMarketingData
        {
            get
            {
                return MarketingList.Count > 0;
            }
        }

        private JdPowerRatingCollection jdPowerRatings;
        public JdPowerRatingCollection JdPowerRatings
        {
            get
            {
                if (jdPowerRatings == null)
                {
                    BusinessUnit dealer = BusinessUnitFinder.Instance().Find(_businessUnitId);

                    if (dealer.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData))
                    {
                        jdPowerRatings = JdPowerRatingCollection.Fetch(Int32.Parse(InventoryItem.Year),
                                                                       InventoryItem.Make,
                                                                       InventoryItem.Model,
                                                                       InventoryItem.SegmentId);
                    }else
                    {
                        jdPowerRatings = new JdPowerRatingCollection();
                    }
                }
                return jdPowerRatings;
            }
        }
        public bool HasGoodJdPowerRatings
        {
            get
            {
                return JdPowerRatings.HasDisplayableRatings(
                    Preferences.MaxCountJdPowerRatings,
                    Preferences.MinValidJdPowerRating);
            }
        }

        /*$base.GoodJdPowerRatings$*/
        public string GoodJdPowerRatings
        {
            get
            {
                string ratingText = JdPowerRatings.GetRatingText(Preferences.MaxCountJdPowerRatings,
                                                                 Preferences.MinValidJdPowerRating);
                if (!string.IsNullOrEmpty(ratingText))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.JdPowerRatings);
                }
                return ratingText;
            }
        }
        public string JdPowerRatingPreview
        {
            get
            {

                string ratingText = string.Empty;

                //only use ratings in preview whenever configured to take at least one
                if (Preferences.MaxCountJdPowerRatings > 0)
                {
                    ratingText = JdPowerRatings.GetRatingPreviewText(Preferences.MinValidJdPowerRating);
                }
                if (!string.IsNullOrEmpty(ratingText))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.JdPowerRatings);
                }
                return ratingText;
            }
        }
    #endregion

        #region conditionData
        private ConditionDescriber conditionDescriber;
        private ConditionDescriber ActiveConditionDescriber
        {
            get
            {
                if (conditionDescriber == null)
                {
                    int tireTread = 0;
                    if (VehicleConfig.TireTread.HasValue)
                    {
                        tireTread = VehicleConfig.TireTread.Value;
                    }
                    conditionDescriber = new ConditionDescriber(VehicleConfig.Checklist, VehicleConfig.Conditions, tireTread);
                }
                return conditionDescriber; 
            }
        }
        public bool HasConditionHighlights
        {
            get
            {
                return VehicleConfig.Checklist.HasAtLeastOneHighlight;
            }
        }
        public bool HasCondition
        {
            get
            {
                return !string.IsNullOrEmpty(VehicleConfig.VehicleCondition.Trim(" ".ToCharArray()));
            }
        }

        /*$base.ConditionSentence$*/
        public string ConditionSentence 
        {
            get
            {
                string val = ActiveConditionDescriber.PopDescription(1);
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ConditionSentence);
                }
                return val;
            }
        }

        /*$base.ConditionSummary$*/
        public string ConditionSummary
        {
            get
            {               
                string val = ActiveConditionDescriber.GetDescription(Preferences.MaxConditionSummaryLength);
                if (!string.IsNullOrEmpty(val))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ConditionSummary);
                }
                return val;
            }
        }
        public bool HasConditionDetails
        {
            get
            {
                return ActiveConditionDescriber.HasDescriptionsRemaining;
            }
        }
        public string GetCondition()
        {
            string retStr = VehicleConfig.VehicleCondition;
            if (!string.IsNullOrEmpty(retStr))
            {
                AddedDescriptionContent(MerchandisingDescriptionItemType.VehicleCondition);
            }

            
            return retStr;
        }
        #endregion

        /*$base.PreviewHighlights$*/
        public string PreviewHighlights
        {
            get
            {
                PreviewGenerator pg = new PreviewGenerator(Preferences.DesiredPreviewLength, _mediator);
                ThemeType theme = ThemeType.None;
                if (_mediator.ThemeId.HasValue)
                {
                    theme = (ThemeType)_mediator.ThemeId.Value;
                }

                string preview = pg.GetPreview(Preferences, theme);

                // return it.
                return preview;
            }
        }

        /*$base.ReconditioningValue$*/
        public decimal ReconditioningValue
        {
            get
            {
                decimal recon = VehicleConfig.ReconditioningValue;
                if (recon > 0)
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ReconditioningValue);
                }
                return recon;
            }
        }

        /*$base.ReconditioningText$*/
        public string ReconditioningText
        {
            get
            {
                string recon = VehicleConfig.ReconditioningText;
                if (!string.IsNullOrEmpty(recon))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.ReconditioningText);
                }
                return recon;
            }
        }
        public bool HasKeyInformation
        {
            get
            {
                return AdditionalInformation.Length > 0;
            }
        }

        /*$base.AdditionalInformation$*/
        public string AdditionalInformation
        {
            get
            {
                string retStr = VehicleConfig.AdditionalInfoText;
                if (!string.IsNullOrEmpty(retStr))
                {
                    AddedDescriptionContent(MerchandisingDescriptionItemType.CustomDealerDataPoints);
                }
                return retStr;
            }
        }
        private PreviewMissionPhrase missionPhraseCreator;
        public bool HasMissionPreviewStatment
        {
            get
            {
                if (missionPhraseCreator == null)
                {
                    missionPhraseCreator = PreviewMissionPhrase.Fetch(_businessUnitId);
                }
                return missionPhraseCreator.HasPhrases();
            }
        }
        public string MissionPreviewStatement
        {
            get
            {
                if (missionPhraseCreator == null)
                {
                    missionPhraseCreator = PreviewMissionPhrase.Fetch(_businessUnitId);
                }

                return missionPhraseCreator.GetPreviewPhrase(_mediator.ThemeId);
            }
        }

        private IPreviewPreferences usedPreviewPreferences;
        public IPreviewPreferences UsedPreviewPreferences
        {
            get
            {
                if (usedPreviewPreferences == null)
                {
                    usedPreviewPreferences = PreviewPreferencesDAO.Fetch(_businessUnitId, 2);
                }
                return usedPreviewPreferences;
            }
        }
        private IPreviewPreferences _newPreviewPreferences;

        public IPreviewPreferences NewPreviewPreferences
        {
            get
            {
                if (_newPreviewPreferences == null)
                {
                    _newPreviewPreferences = PreviewPreferencesDAO.Fetch(_businessUnitId, 1);
                }
                return _newPreviewPreferences;
            }
        }
        public IPreviewPreferences PreviewPrefs
        {
            get
            {
                if (InventoryItem.IsNew())
                {
                    return NewPreviewPreferences;
                }
                return UsedPreviewPreferences;
            }
        }

        #region methods
        public List<string> LimitCount(List<string> values)
        {
            int count = Math.Min(values.Count, Preferences.MaxOptions) - 1;
            if (count < 1)
            {
                return new List<string>();
            }

            return values.GetRange(0, count);
        }

        public void AddedDescriptionContent(MerchandisingDescriptionItemType itemType)
        {
            if (_mediator != null)
            {
                _mediator.DescriptionContentAdded(itemType);
            }
        }

        public bool EvaluateCondition(string templateCondition)
        {
            Log.DebugFormat("EvaluateCondition() called for condition '{0}'", templateCondition);

            bool retVal = ChromeLogicEngine.EvalLogic(templateCondition,EvaluateSingleCondition);

            Log.DebugFormat("Returning '{0}'", retVal);

            return retVal;
        }


        //UGLY SPECIAL CASE...Add to templating data?
        //Should move tagline out of standard templates...doesn't fit there any more

        public bool EvaluateSingleCondition(string condition)
        {
            return EvaluateSingleCondition(TemplateConditionHelper.GetCondition(condition));
        }

        public bool EvaluateSingleCondition(TemplateCondition condition)
        {
            Log.DebugFormat("EvaluateSingleCondition() called with TemplateCondition '{0}'", condition);

            bool retVal;

            switch (condition)
            {
                case TemplateCondition.IsNewVehicle:
                    retVal = IsNewVehicle;
                    break;
                case TemplateCondition.HasMostSearchedEquipmentAvailable:
                    retVal = HasMostSearchedEquipmentAvailable;
                    break;

                case TemplateCondition.HasValuedEquipment:
                    retVal = HasValuedOptions;
                    break;
                case TemplateCondition.HasEnoughValuedEquipment:
                    retVal = (VehicleConfig.VehicleOptions.ValuedOptions.Count >=
                              Preferences.MinOptionsPerGroup);
                    break;
                case TemplateCondition.HasTruckEquipment:
                    retVal = HasTruckOptions;
                    break;
                case TemplateCondition.HasEnoughTruckEquipment:
                    retVal = (VehicleConfig.VehicleOptions.TruckOptions.Count >=
                              Preferences.MinOptionsPerGroup);
                    break;
                case TemplateCondition.HasSportyEquipment:
                    retVal = HasSportyOptions;
                    break;
                case TemplateCondition.HasEnoughSportyEquipment:
                    retVal = (VehicleConfig.VehicleOptions.SportyOptions.Count >=
                              Preferences.MinOptionsPerGroup);
                    break;
                case TemplateCondition.HasSafetyEquipment:
                    retVal = HasSafetyOptions;
                    break;
                case TemplateCondition.HasEnoughSafetyEquipment:
                    retVal = (VehicleConfig.VehicleOptions.SafetyOptions.Count >=
                              Preferences.MinOptionsPerGroup);
                    break;
                case TemplateCondition.HasConvenienceEquipment:
                    retVal = HasConvenienceOptions;
                    break;
                case TemplateCondition.HasEnoughConvenienceEquipment:
                    retVal = (VehicleConfig.VehicleOptions.ConvenienceOptions.Count >=
                              Preferences.MinOptionsPerGroup);
                    break;
                case TemplateCondition.HasLuxuryEquipment:
                    retVal = HasLuxuryOptions;
                    break;
                case TemplateCondition.HasEnoughLuxuryEquipment:
                    retVal = (VehicleConfig.VehicleOptions.LuxuryOptions.Count >=
                              Preferences.MinOptionsPerGroup);
                    break;
                case TemplateCondition.HasMajorEquipmentOfInterest:
                    retVal = MajorEquipmentOfInterest.Count > 0;
                    break;
                case TemplateCondition.HasTier1Equipment:
                    retVal = HasTier1EquipmentAvailable;
                    break;
                case TemplateCondition.HasEnoughTier1Equipment:
                    retVal = HasEnoughTier1EquipmentAvailable;
                    break;
                case TemplateCondition.HasTier2Equipment:
                    retVal = HasTier2EquipmentAvailable;
                    break;
                case TemplateCondition.HasEnoughTier2Equipment:
                    retVal = HasEnoughTier2EquipmentAvailable;

                    break;
                case TemplateCondition.GoodPriceBelowOriginalMSRP:
                    retVal = HasGoodPriceBelowMSRP;
                    break;
                case TemplateCondition.GoodPriceBelowBook:
                    retVal = HasGoodPriceBelowBook;
                    break;
                case TemplateCondition.GetsGoodGasMileage:
                    retVal = GetsGoodGasMileage;
                    break;
                case TemplateCondition.IsOneOwner:
                    retVal = IsOneOwner;

                    break;
                case TemplateCondition.IsCertified:
                    retVal = IsCertified;
                    // || InventoryItem.CertificationTypeId > 0);
                    break;
                case TemplateCondition.IsLoaded:
                    retVal = VehicleConfig.VehicleOptions.GetCountInTier(1) >=
                             Preferences.CountTier1ForLoaded;
                    break;
                case TemplateCondition.IsSubprimeCandidate:
                    retVal = (false); //need data item for subprime  -- vehconfig
                    break;
                case TemplateCondition.IsLowMilesPerYear:
                    retVal = IsLowMilesPerYear;
                    break;
                case TemplateCondition.HasWarrantyRemaining:
                    retVal =
                        (ConsInfo.HasGoodWarrantyRemaining(
                            Preferences.GoodWarrantyMileageRemaining,
                            InventoryItem.MileageReceived,
                            InventoryItem.InServiceDate));
                    break;
                case TemplateCondition.HasRoadsideAssistance:
                    retVal = HasRoadsideAssistance;
                    break;

                case TemplateCondition.HasGoodCrashTestRatings:
                    retVal = HasGoodCrashTestRatings;
                    break;
                case TemplateCondition.HasEnoughBelowTargetPrice:
                    retVal = (CountInPriceRange >=
                              Preferences.MinNumberForCountBelowPriceDisplay);
                    break;
                case TemplateCondition.HasEnoughBelowBook:
                    retVal = (CountBelowBook >= Preferences.MinNumberForCountBelowBookDisplay);
                    break;
                case TemplateCondition.HasCondition:
                    retVal = HasCondition;
                    break;
                case TemplateCondition.IsDependableCondition:
                    retVal = (VehicleConfig.VehicleCondition == "IsDependableCondition");
                    break;
                case TemplateCondition.HasHighPassengerCountForClass:
                    retVal = HasHighPassengerCountForClass();
                    break;
                case TemplateCondition.HasAdditionalInformationSelected:
                    retVal =
                        !String.IsNullOrEmpty(VehicleConfig.AdditionalInfoText.TrimEnd(", ".ToCharArray()));
                    break;
                case TemplateCondition.HasFinancingAvailable:
                    retVal = HasFinancingAvailable;
                    break;
                case TemplateCondition.IsStripped:
                    retVal = (VehicleConfig.VehicleOptions.GetCountInTier(1) +
                              VehicleConfig.VehicleOptions.GetCountInTier(2)) <=
                             Preferences.MaxCountForStripped;
                    break;
                case TemplateCondition.IsFullDescription:
                    retVal = _mediator.IsInLongMode;
                    break;
                case TemplateCondition.HasAwards:
                    retVal = HasAwards;
                    break;
                case TemplateCondition.HasExteriorColor:
                    retVal = !string.IsNullOrEmpty(ExteriorColor);
                    break;
                case TemplateCondition.HasInteriorColor:
                    retVal = !string.IsNullOrEmpty(InteriorColor);
                    break;
                case TemplateCondition.HasInteriorType:
                    retVal = HasInteriorType;
                    break;
                case TemplateCondition.HasReconditioningText:
                    retVal = !string.IsNullOrEmpty(ReconditioningText);
                    break;
                case TemplateCondition.HasReconditioningValue:
                    retVal = ReconditioningValue > 0;
                    break;
                case TemplateCondition.HasHighReconditioningValue:
                    retVal = ReconditioningValue >= Preferences.HighReconditioningValue;
                    break;
                case TemplateCondition.HasAfterMarketEquipment:
                    retVal = !string.IsNullOrEmpty(AfterMarketEquipment.TrimEnd(", ".ToCharArray()));
                    break;
                case TemplateCondition.HasTagline:
                    retVal = _mediator.HasTagline;
                    break;
                case TemplateCondition.HasGoodJdPowerRatings:
                    retVal =
                        HasGoodJdPowerRatings;
                    break;
                case TemplateCondition.HasHighlightedPackages:
                    retVal = HasHighlightedPackages;
                    break;
                case TemplateCondition.HasConditionDetails:
                    retVal = HasConditionDetails;
                    break;
                case TemplateCondition.HasMarketingData:
                    retVal = HasMarketingData;
                    break;
                case TemplateCondition.HasMarketingSuperlative:
                    retVal = HasMarketingSuperlative;
                    break;
                case TemplateCondition.HasDisplayablePrice:
                    retVal = HasDisplayablePrice;
                    break;
                case TemplateCondition.HasBuybackGuarantee:
                    retVal = HasBuybackGuarantee;
                    break;
                case TemplateCondition.HasCertificationDetails:
                    retVal = HasCertificationDetails;
                    break;
                case TemplateCondition.IsCleanCarfaxReport:
                    retVal = IsCleanCarfaxReport;
                    break;                
                case TemplateCondition.IsPriceReduced:
                    retVal = IsPriceReduced;
                    break;
                case TemplateCondition.IsPriceReducedEnough:
                    retVal = IsPriceReducedEnough;
                    break;
                case TemplateCondition.IsAutoCheckOneOwner:
                    retVal = IsAutoCheckOneOwner;
                    break;
                case TemplateCondition.IsAutoCheckAssured:
                    retVal = IsAutoCheckAssured;
                    break;
                case TemplateCondition.HasGoodAutoCheckScore:
                    retVal = HasGoodAutoCheckScore;
                    break;
                case TemplateCondition.IsCleanAutoCheckReport:
                    retVal = IsCleanAutoCheckReport;
                    break;

                case TemplateCondition.Always:
                    retVal = true;
                    break;

                default:
                    //should we use reflection here to catch unenumerated conditions?
                    retVal = false;
                    break;
            }

            Log.DebugFormat("Returning '{0}'", retVal);

            return retVal;
        }

        #endregion       
    }


}