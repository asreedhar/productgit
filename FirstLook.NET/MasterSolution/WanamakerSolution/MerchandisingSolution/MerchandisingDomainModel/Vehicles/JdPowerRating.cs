using System;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class JdPowerRating : IComparable
    {
        private const string FORMAT_STRING = "{0} - {1} Power Circles";
        private const string SHORT_FORMAT_STRING = "{0} - {1} Power Circles";

        /// <summary>
        /// The name {0} is left empty in this format - it only exists in the string to make the system work as expected
        /// </summary>
        private const string PREVIEW_FORMAT_STRING = "{0}{1} Power Circle Rated";

        public string ShortName { get; set; }

        public JdPowerRating(string name, string shortName, double? rating, JdPowerRatingType ratingType)
        {
            Name = name;
            ShortName = shortName;
            Rating = rating;
            RatingType = ratingType;
        }

        public JdPowerRatingType RatingType { get; set; }

        public string Name { get; set; }

        public double? Rating { get; set; }

        public virtual string ToString(double minValidRating)
        {
            return IsDisplayable(minValidRating) ? ToString() : string.Empty;
        }

        /// <summary>
        /// the preview string is a short version used in advert previews
        /// they do not contain the name of the rating
        /// </summary>
        /// <returns></returns>
        public string ToPreviewString()
        {
            return ToString(PREVIEW_FORMAT_STRING, string.Empty);
        }
        public string ToShortString()
        {
            return ToString(SHORT_FORMAT_STRING, ShortName);
        }
        public override string ToString()
        {
            return ToString(FORMAT_STRING,Name);
        }

        /// <summary>
        /// string format - contains up to two references {0},{1}
        /// </summary>
        /// <param name="currentFormat"></param>
        /// <param name="nameToUse"></param>
        /// <returns></returns>
        public string ToString(string currentFormat, string nameToUse)
        {
            return Rating.HasValue ? string.Format(currentFormat, nameToUse, FormattedRating()) : string.Empty;
        }

        protected string FormattedRating()
        {
            if (Rating.HasValue)
            {
                if (Rating%1 < 0.01) //if it's a whole number, make it an int
                {
                    return ((int) Rating).ToString();
                }

                return String.Format("{0:n1}", Rating);
            }
            return string.Empty;
        }

        public virtual bool IsDisplayable(double minValidRating)
        {
            return Rating >= minValidRating;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is JdPowerRating || obj is JdPowerRatingGroup))
            {
                throw new ApplicationException("Can only compare JdPowerRating objects to self of JdPowerRatingGroups");
            }

            bool yIsGrp = obj is JdPowerRatingGroup;
            bool xIsGrp = this is JdPowerRatingGroup;

            int cmp = yIsGrp.CompareTo(xIsGrp);  //sort groups first (y.compareTo(x))
            if (cmp == 0)
            {
                double xVal = 0.0;
                double yVal = 0.0;
                if (Rating.HasValue)
                {
                    xVal = Rating.Value;
                }

                JdPowerRating yObj = obj as JdPowerRating;
                if (yObj.Rating.HasValue)
                {
                    yVal = yObj.Rating.Value;
                }
                cmp = yVal.CompareTo(xVal);   //sort largest first (y.compareTo(x)

                if (cmp == 0)
                {
                    ((int) RatingType).CompareTo((int) yObj.RatingType);  //sort smallest first (x.compareTo(y)
                }
            }

            
            return cmp;
        }
    }
}
