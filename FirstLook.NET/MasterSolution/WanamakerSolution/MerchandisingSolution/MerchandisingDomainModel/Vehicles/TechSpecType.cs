namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum TechSpecType
    {
        Undefined = 0,
        Horsepower = 48,
        HorsepowerSecondary = 900

    }
}
