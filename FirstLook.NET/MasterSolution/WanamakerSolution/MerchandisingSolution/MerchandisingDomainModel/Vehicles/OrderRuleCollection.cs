using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for EquipmentCollection
    /// </summary>
    public class OrderRuleCollection : CollectionBase
    {
        public OrderRuleCollection()
        {
        }


        public OrderRuleCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                OrderRule currRule = new OrderRule(reader);
                Add(currRule);
            }
        }


        public ListDictionary TOListDictionary()
        {
            ListDictionary ld = new ListDictionary();
            foreach (OrderRule ordRule in this)
            {
                //check for existing option code in LD?  shouldn't be there
                ld.Add(ordRule.OptionCode, ordRule);
            }
            return ld;
        }

        public int Add(OrderRule equip)
        {
            return (List.Add(equip));
        }

        /// <summary>
        /// determine if the list contains a specified code
        /// </summary>
        /// <param name="optionCode"></param>
        /// <returns></returns>
        public bool Contains(string optionCode)
        {
            foreach (OrderRule ordRule in this)
            {
                if (ordRule.OptionCode == optionCode)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// return an array of order rules
        /// </summary>
        /// <param name="optionCode"></param>
        /// <returns></returns>
        public List<OrderRule> GetByCode(string optionCode)
        {
            List<OrderRule> retList = new List<OrderRule>();
            foreach (OrderRule ordRule in this)
            {
                if (ordRule.OptionCode == optionCode)
                {
                    retList.Add(ordRule);
                }
            }
            return retList;
        }

    }
}