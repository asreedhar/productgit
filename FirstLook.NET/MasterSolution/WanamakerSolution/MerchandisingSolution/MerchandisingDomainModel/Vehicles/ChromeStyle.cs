using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;

using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class ChromeStyle : IChromeStyle
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(ChromeStyle).FullName);
        #endregion

        private const string AVAILABLE_AS_STANDARD_TEXT = "S";
        private const string AVAILABLE_AS_OPTIONAL_TEXT = "O";

        public int StyleID { get; set; }

        public string StyleNameWoTrim { get; set; }

        public string Trim { get; set; }

        public string ModelCode { get; set; }

        private readonly string _autoTransFlag;
        private readonly string _manualTransFlag;
        private readonly string _frontWheelDriveFlag;
        private readonly string _rearWheelDriveFlag;
        private readonly string _allWheelDriveFlag;
        private readonly string _fourWheelDriveFlag;

        public bool IsMatchingTrim(string testTrim)
        {
            if (testTrim == null) return false;

            if (testTrim == string.Empty)
            {
                testTrim = "base";                
            }
            
            return 0 == string.Compare(Trim.Trim(), testTrim.Trim(), StringComparison.InvariantCultureIgnoreCase);
            
        }
        public bool IsValidTransmission(TransmissionType transmissionType)
        {
            switch (transmissionType)
            {
                case TransmissionType.Automatic:
                    return IsFlagAvailable(_autoTransFlag);
                case TransmissionType.Manual:
                    return IsFlagAvailable(_manualTransFlag);
                default:
                    return true;
            }
        }
        public bool IsValidDrivetrain(DrivetrainType drivetrainType)
        {
            switch (drivetrainType)
            {
                case DrivetrainType.FrontWheelDrive:
                    return IsFlagAvailable(_frontWheelDriveFlag);
                case DrivetrainType.RearWheelDrive:
                    return IsFlagAvailable(_rearWheelDriveFlag);
                case DrivetrainType.AllWheelDrive:
                    return IsFlagAvailable(_allWheelDriveFlag);
                case DrivetrainType.FourWheelDrive:
                    return IsFlagAvailable(_fourWheelDriveFlag);
                case DrivetrainType.TwoWheelDrive:
                    return IsFlagAvailable(_frontWheelDriveFlag) || IsFlagAvailable(_rearWheelDriveFlag);
                default:
                    return true;
                
            }
        }
        private static bool IsFlagAvailable(string flagText)
        {
            return flagText == AVAILABLE_AS_STANDARD_TEXT || flagText == AVAILABLE_AS_OPTIONAL_TEXT;
        }

        public string ConsumerFriendlyStyleName { get; set; }

        internal ChromeStyle(IDataRecord record)
        {
            StyleID = record.GetInt32(record.GetOrdinal("styleId"));
            Trim = record.GetString(record.GetOrdinal("trim"));
            StyleNameWoTrim = record.GetString(record.GetOrdinal("styleNameWOTrim"));
            ConsumerFriendlyStyleName = "";
            _manualTransFlag = record.GetString(record.GetOrdinal("manualTrans"));
            _autoTransFlag = record.GetString(record.GetOrdinal("autoTrans"));
            _rearWheelDriveFlag = record.GetString(record.GetOrdinal("RearWD"));
            _frontWheelDriveFlag = record.GetString(record.GetOrdinal("FrontWD"));
            _allWheelDriveFlag = record.GetString(record.GetOrdinal("AllWD"));
            _fourWheelDriveFlag = record.GetString(record.GetOrdinal("FourWD"));
        }
        public ChromeStyle(int styleID, string styleNameWoTrim, string trim, string consumerFriendlyStyleName)
        {
            StyleID = styleID;
            StyleNameWoTrim = styleNameWoTrim;
            Trim = trim;
            ConsumerFriendlyStyleName = consumerFriendlyStyleName;
        }

        public ChromeStyle()
        {
            StyleID = -1;
            StyleNameWoTrim = "";
            Trim = "";
            ConsumerFriendlyStyleName = "";
        }

        /// <summary>
        /// Try to infer the style id using other known data points.
        /// </summary>
        /// <param name="vin">The car's vin</param>
        /// <param name="optionCodes">Option codes known to be on the car</param>
        /// <param name="modelCode">The OEM full style code</param>
        /// <param name="trimName">The trim name</param>
        /// <param name="transmissionType">Automatic, Manual, Unknown</param>
        /// <param name="drivetrainType">Two wheel drive, four wheel drive, etc...</param>
        /// <returns></returns>
        public static ChromeStyle InferStyleId(string vin, IEnumerable<string> optionCodes, string modelCode, string trimName, 
            TransmissionType transmissionType, DrivetrainType drivetrainType)
        {
            // Boundary conditions
            if (string.IsNullOrWhiteSpace(vin)) return null;            
            var knownOptionCodes = optionCodes ?? new List<string>();
            var model = string.IsNullOrWhiteSpace(modelCode) ? string.Empty : modelCode;
            var trim = string.IsNullOrWhiteSpace(trimName) ? string.Empty : trimName;

            Log.DebugFormat("Attempting to match style: vin: {0} model: {1} trim: {2} transmission: {3} drive train: {4}", vin, model, trim,  Enum.GetName(typeof(TransmissionType), transmissionType), Enum.GetName(typeof(DrivetrainType), drivetrainType));

            // Get the possible styles by doing vin pattern matching
            List<ChromeStyle> possibleStyles = FetchList(vin);

            // Only one...we can use it
            if (possibleStyles.Count == 1)
            {
                Log.DebugFormat("Matched style for VIN: {0} Style: {1} - {2} based on VIN only", vin, possibleStyles.First().ChromeStyleID, possibleStyles.First().Trim);
                return possibleStyles.First();
            }

            // Try to determine the style by inspecting the model code and the known options. 
            var inferredStyle = InferStyleByModelAndOptions(vin, model, knownOptionCodes);
            if (inferredStyle != null) return inferredStyle;

            // Rule out / eliminate possible styles by looking at options.
            // Look at the options on the car and the possible styles (vin pattern matched). If the car has a code that is invalid for a style,
            // then it cannot be that style.
            var matchingStyles = new List<ChromeStyle>();
            foreach (ChromeStyle style in possibleStyles)
            {
                // Check out the possible option packages for each...see if we have a match                
                EquipmentCollection ec = EquipmentCollection.FetchUnfilteredOptions(style.StyleID);

                // Assume the style is valid by default.
                var canMatch = true;
                foreach (string code in knownOptionCodes)
                {
                    if (string.IsNullOrWhiteSpace(code) || ec.Contains(code)) continue;
                    
                    // We have a code that is not possible for this style.
                    canMatch = false;
                        
                    // No need to evaluate other codes within this style.
                    break;
                }

                // if it could match add it
                if (canMatch)
                {
                    matchingStyles.Add(style);
                }                
            }

            // If by the process of elimination we have arrived at a single possible style, return it.
            if (matchingStyles.Count == 1)
            {
                Log.DebugFormat("Matched style for VIN: {0} Style: {1} - {2} based on VIN and options", vin, matchingStyles.First().ChromeStyleID, matchingStyles.First().Trim);
                return matchingStyles.First();
            }

            // If by restricting to a type of transmission we can arrive at a single style, return it.
            matchingStyles = matchingStyles.FindAll(style => style.IsValidTransmission( transmissionType));
            if (matchingStyles.Count == 1)
            {
                Log.DebugFormat("Matched style for VIN: {0} Style: {1} - {2} based on VIN, options and transmission", vin, matchingStyles.First().ChromeStyleID, matchingStyles.First().Trim);
                return matchingStyles.First();
            }

            // If by restricting to a type of drivetrain we can arrive at a single style, return it.
            matchingStyles = matchingStyles.FindAll(style => style.IsValidDrivetrain(drivetrainType));
            if (matchingStyles.Count == 1)
            {
                Log.DebugFormat("Matched style for VIN: {0} Style: {1} - {2} based on VIN, options, transmission and drivetrain", vin, matchingStyles.First().ChromeStyleID, matchingStyles.First().Trim);
                return matchingStyles.First();
            }

            // If by restricting to a trim name we can arrive at a single style, return it.
            if (trim != null)
            {
                matchingStyles = matchingStyles.FindAll(style => style.IsMatchingTrim(trim));
                if (matchingStyles.Count == 1)
                {
                    Log.DebugFormat("Matched style for VIN: {0} Style: {1} - {2} based on VIN, options, transmission, drivetrain and trim", vin, matchingStyles.First().ChromeStyleID, matchingStyles.First().Trim);
                    return matchingStyles.First();
                }                
            }

            // We cannot determine the chrome style.
            Log.InfoFormat("AutoLoad: Unable to infer chrome style id. Vin: {0}, ModelCode: {1}", vin, model);

            return null;
        }

        /// <summary>
        /// FB 13591: Use the model code and, if needed, the optionCodes to zero-in on a single style after having narrowed it down to a
        /// small group by doing vin pattern matching.
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="modelCode"></param>
        /// <param name="optionCodes"></param>
        /// <returns></returns>
        internal static ChromeStyle InferStyleByModelAndOptions(string vin, string modelCode, IEnumerable<string> optionCodes)
        {
            // Boundary cases in which we cannot proceed.
            if (string.IsNullOrWhiteSpace(vin) || string.IsNullOrWhiteSpace(modelCode) ) return null;

            string fullOemStyleCode = GetFullOemStyleCode(modelCode);

            IEnumerable<ChromeStyle> possibleStyles = FetchByVinAndOemStyleCode(vin, fullOemStyleCode);
            if (possibleStyles != null && possibleStyles.Count() == 1)
            {
                Log.DebugFormat("Matched style for VIN: {0} Style: {1} - {2} based on VIN and model code", vin, possibleStyles.First().ChromeStyleID, possibleStyles.First().Trim);
                return possibleStyles.First();
            }

            // We got back more than one style.  Try to detemine which one is correct by looking at the option codes.  
            // We are looking for an option code which can only map to _one_ of the possible styles.  
            // If the vehicle has such an option code, then we can infer the style.
            if (possibleStyles != null && possibleStyles.Count() > 1 && optionCodes.Count() > 0)
            {
                var optionStylePairs = GetUniqueOptionStylePairs(possibleStyles.Select(s => s.StyleID));

                var optionMatchedStyles = new List<int>();

                // does the car have any of these unique styles?
                if (optionStylePairs.Count() > 0)
                {
                    foreach (var tuple in optionStylePairs)
                    {
                        var styleId = tuple.Item1;
                        var optionCode = tuple.Item2;

                        // does the car have this option code?
                        var containsCode = optionCodes.Contains(optionCode); //Select(c => c.Equals(optionCode));

                        // If the car has this option code and we haven't already logged this style
                        if (containsCode && !optionMatchedStyles.Contains(styleId))
                        {
                            optionMatchedStyles.Add(styleId);
                        }
                    }
                }

                if (optionMatchedStyles.Count() > 0)
                {
                    if (optionMatchedStyles.Count == 1)
                    {
                        int styleId = optionMatchedStyles.First();
                        Log.DebugFormat("Matched style for VIN: {0} Style: {1} - {2} based on VIN, model code and option codes", vin, styleId, GetByStyleId(styleId).First().Trim);
                        return GetByStyleId(styleId).First();
                    }

                    // Make sure we didn't find more than one.                        
                    if (optionMatchedStyles.Count > 1)
                    {
                        // This is an inconsistency.  Log it and return null.  The style will have to be determined by some other logic.

                        // Log it and terminate
                        Log.WarnFormat(
                            "AutoLoad: Style-specific mutually-exlusive unique options found. The options data seems inconsistent. Matched options: {0}.",
                            optionMatchedStyles.ToDelimitedString(","));
                        return null;
                    }
                }
            }

            // We cannot determine the chrome style.
            // unable to 
            //Log.InfoFormat("AutoLoad: Unable to infer chrome style id from vin, model & options. Vin: {0}, ModelCode: {1}", vin, modelCode);
            return null;
        }

        /// <summary>
        /// Extract the full oem style code from the provided string, which may contain spaces and dashes.
        /// </summary>
        /// <param name="modelCode"></param>
        /// <returns></returns>
        internal static string GetFullOemStyleCode(string modelCode)
        {
            if (string.IsNullOrWhiteSpace(modelCode)) 
                return string.Empty;

            string fullOemStyleCode = modelCode;

            // If the supplied "model code" contains a "-", then we'll examine the portion before the "-".  Otherwise, we'll try to split on spaces.
            char separator = modelCode.Contains("-") ? '-' : ' ';

            // TODO: Revisit this with other manufacturers.  We may need to format the oem code differently per manufacturer.
            var codePortions = modelCode.Split(separator);
            if (codePortions.Length > 0)
            {
                fullOemStyleCode = codePortions[0].Trim();
            }
            return fullOemStyleCode;
        }

        public static bool HasValidVinPattern( string vin )
        {
            return GetVinPatternIdForVin(vin) > 0;
        }

        public static int GetVinPatternIdForVin(string vin)
        {
            return Registry
                .Resolve<IChromeMapper>()
                .LookupVinPatternIdByVin(vin) ?? -1;
        }

        public List<string> ChromeTrimNamesSelect(string vin)
        {
            int vinPatternID = GetVinPatternIdForVin(vin);
            if (vinPatternID < 0)
            {
                return new List<string>();
            }



            using (IDataConnection con = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT Distinct sty.Trim
                            FROM VehicleCatalog.Chrome.VINPatternStyleMapping AS sm 
                            INNER JOIN VehicleCatalog.Chrome.Styles as sty 
                                ON sty.StyleID = sm.ChromeStyleID 
                            WHERE (sm.CountryCode = 1) AND (sty.CountryCode = 1) AND                 
                            sm.vinpatternID = @VinPatternID";
                    cmd.CommandType = CommandType.Text;

                    Database.AddRequiredParameter(cmd, "VinPatternID", vinPatternID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<string> possTrims = new List<string>();

                        while (reader.Read())
                        {
                            string trimName = reader.GetString(reader.GetOrdinal("Trim"));
                            if (!possTrims.Contains(trimName))
                            {
                                possTrims.Add(trimName);
                            }
                        }

                        return possTrims;
                    }


                }
            }
        }


        public List<string> ChromeStyleNamesSelect(string vin)
        {
            int vinPatternID = GetVinPatternIdForVin(vin);
            if (vinPatternID < 0)
            {
                return new List<string>();
            }




            using (IDataConnection con = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT Distinct sty.StyleNameWOTrim as StyleName
                            FROM Chrome.VINPatternStyleMapping AS sm 
                            INNER JOIN Chrome.Styles as sty ON sty.StyleID = sm.ChromeStyleID 
                            WHERE (sm.CountryCode = 1) AND (sty.CountryCode = 1) AND                 
                            sm.vinpatternID = @VinPatternID";
                    cmd.CommandType = CommandType.Text;

                    Database.AddRequiredParameter(cmd, "VinPatternID", vinPatternID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {

                        List<string> possStyles = new List<string>();

                        while (reader.Read())
                        {
                            string styleName = Convert.ToString(reader["StyleName"]);
                            if (!possStyles.Contains(styleName))
                            {
                                possStyles.Add(styleName);
                            }
                        }

                        return possStyles;
                    }
                }
            }
        }


        /// <summary>
        /// Given a list of style ids, determine which options exist on only _one_ of the styles. 
        /// Return such options, along with the correct style.
        /// </summary>
        /// <param name="chromeStyleIds"></param>
        /// <returns>Tuples of styleId / optionCode</returns>
        public static IEnumerable<System.Tuple<int,string>> GetUniqueOptionStylePairs(IEnumerable<int> chromeStyleIds)
        {
            var tuples = new List<System.Tuple<int, string>>();

            // TODO: Logging, exception handling.
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getUniqueOptionsAmongChromeStyles";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "styles", chromeStyleIds.ToDelimitedString(","), DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader != null && reader.Read())
                        {
                            int styleId = reader.GetInt32(reader.GetOrdinal("StyleId"));
                            string optionCode = reader.GetString(reader.GetOrdinal("OptionCode"));

                            tuples.Add(new System.Tuple<int, string>(styleId, optionCode));
                        }
                    }
                }
            }
            return tuples;
        }

        /// <summary>
        /// Get a list of chrome styles which pattern match the specified vin and which match the oem style code.
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="oemFullStyleCode"></param>
        /// <returns></returns>
        public static List<ChromeStyle> FetchByVinAndOemStyleCode(string vin, string oemFullStyleCode)
        {
            // TODO: Logging, exception handling.

            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getChromeStylesByOEMCode";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "Vin", vin, DbType.String);
                    Database.AddRequiredParameter(cmd, "OemFullStyle", oemFullStyleCode, DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var styles = new List<ChromeStyle>();

                        while (reader != null && reader.Read())
                        {
                            // Get the id
                            var styleId = reader.GetInt32(reader.GetOrdinal("StyleId"));

                            // Get the styles
                            var foundStyles = GetByStyleId(styleId);

                            if( foundStyles != null )
                            {
                                styles.AddRange( foundStyles );
                            }
                        }

                        return styles;
                    }
                }
            }
        }

        /// <summary>
        /// Get ChromeStyles by passing in the style id.  
        /// </summary>
        /// <param name="styleId"></param>
        /// <returns>A list of ChromeStyle objects. I would normally expect only one.</returns>
        internal static List<ChromeStyle> GetByStyleId(int styleId)
        {
            // Don't waste time searching for invalid styles
            if( styleId <= 0 ) 
                return new List<ChromeStyle>();

            using (var con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getChromeStyleById";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "StyleId", styleId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        var styles = new List<ChromeStyle>();

                        while (reader != null && reader.Read())
                        {
                            styles.Add(new ChromeStyle(reader));
                        }

                        return styles;
                    }
                }
            }   
        }

        public static List<ChromeStyle> FetchList(string vin)
        {
            int vinPatternID = GetVinPatternIdForVin(vin);
            if (vinPatternID < 0)
            {
                return new List<ChromeStyle>();
            }



            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.GetChromeStyles";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "VinPatternID", vinPatternID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {

                        List<ChromeStyle> styles = new List<ChromeStyle>();
                        
                        while (reader != null && reader.Read())
                        {
                           styles.Add(new ChromeStyle(reader));
                        }

                        return styles;
                    }
                }
            }
        }

        public static Dictionary<int, string> ChromeTrimStyleNamesSelect(string vin)
        {
            int vinPatternID = GetVinPatternIdForVin(vin);
            if (vinPatternID < 0)
            {
                return new Dictionary<int, string>();
            }

            var possTrims = new Dictionary<int, string>();

            var chromeDictionary = ChromeTrimStylesSelect(vin);
            foreach (var chromeStyleId in chromeDictionary.Keys)
            {
                var chromeStyle = chromeDictionary[chromeStyleId];
                possTrims.Add(chromeStyleId, chromeStyle.Trim + " - " + chromeStyle.StyleNameWoTrim);
            }
            return possTrims;

        }

        public static Dictionary<int, ChromeStyle> ChromeTrimStylesSelect(string vin)
        {
            int vinPatternID = GetVinPatternIdForVin(vin);
            if (vinPatternID < 0)
            {
                return new Dictionary<int, ChromeStyle>();
            }

            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.GetChromeStyles";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "VinPatternID", vinPatternID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                       var possTrims = new Dictionary<int, ChromeStyle>();

                        while (reader != null && reader.Read())
                        {
                            var chromeStyleId = reader.GetInt32(reader.GetOrdinal("styleID"));
                            var trim = reader.GetString(reader.GetOrdinal("Trim"));
                            var styleNameWoTrim = reader.GetString(reader.GetOrdinal("styleNameWOTrim"));
                            if (!possTrims.ContainsKey(chromeStyleId))
                            {
                                var style = new ChromeStyle(chromeStyleId, styleNameWoTrim, trim, string.Empty);
                                possTrims.Add(chromeStyleId, style);
                            }
                        }

                        return possTrims;
                    }
                }
            }
        }

        public static ChromeStyle ChromeStyleSelect(int chromeStyleID)
        {


            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getChromeStyleDescription";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "chromeStyleID", chromeStyleID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        ChromeStyle csi = new ChromeStyle();
                        if (reader.Read())
                        {
                            csi.StyleID = chromeStyleID;
                            csi.ConsumerFriendlyStyleName = reader.GetString(reader.GetOrdinal("CFStyleName"));
                            csi.StyleNameWoTrim = reader.GetString(reader.GetOrdinal("StyleNameWOTrim"));
                            csi.Trim = reader.GetString(reader.GetOrdinal("trim"));
                        }
                        return csi;
                    }
                }
            }

        }

        public static Dictionary<string, Dictionary<string, int>> ChromeStylesStructureSelect(string vin)
        {
            int vinPatternID = GetVinPatternIdForVin(vin);
            if (vinPatternID < 0)
            {
                return new Dictionary<string, Dictionary<string, int>>();
            }

            using (IDataConnection con = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                            SELECT case when sty.trim = '' then 'base' else sty.trim end as trim, sty.styleNameWOTrim, max(styleID) as styleID, min(sequence) as seq
                                FROM Chrome.VINPatternStyleMapping AS sm 
                                INNER JOIN Chrome.Styles as sty ON sty.StyleID = sm.ChromeStyleID 
                                WHERE (sm.CountryCode = 1) AND (sty.CountryCode = 1) AND                 
                                sm.vinpatternID = @VinPatternID
                            group by trim, stylenamewotrim
                            order by seq";
                    cmd.CommandType = CommandType.Text;

                    Database.AddRequiredParameter(cmd, "VinPatternID", vinPatternID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {

                        Dictionary<string, Dictionary<string, int>> trimSets =
                            new Dictionary<string, Dictionary<string, int>>();

                        while (reader != null && reader.Read())
                        {

                            int styleID = reader.GetInt32(reader.GetOrdinal("styleID"));
                            string trimName = reader.GetString(reader.GetOrdinal("Trim"));
                            string styleName = reader.GetString(reader.GetOrdinal("styleNameWOTrim"));
                            if (!trimSets.ContainsKey(trimName))
                            {
                                trimSets.Add(trimName, new Dictionary<string, int>());
                            }

                            if (!trimSets[trimName].ContainsKey(styleName))
                            {
                                trimSets[trimName].Add(styleName, styleID);
                            }
                        }
                        return trimSets;


                    }
                }
            }
        }

        public int ChromeStyleID
        {
            get { return StyleID; }
        }

        public static int GetManufacturerId(int chromeStyleId)
        {
            int manufactureId = 0;

            using (var con = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                            SELECT Divisions.ManufacturerID
	                            FROM VehicleCatalog.Chrome.Styles
	                            INNER JOIN VehicleCatalog.Chrome.Models ON Styles.ModelID = Models.ModelID
												                            AND Styles.CountryCode = Models.CountryCode
	                            INNER JOIN VehicleCatalog.Chrome.Divisions ON Models.DivisionID = Divisions.DivisionID
												                            AND Models.CountryCode = Divisions.CountryCode
	                            WHERE StyleID = @styleID
	                            AND Styles.CountryCode = 1
                                    ";
                    cmd.CommandType = CommandType.Text;
                    cmd.AddParameterWithValue("@styleID", chromeStyleId);
                    
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            manufactureId = Convert.ToInt32(reader["ManufacturerID"]);
                        }
                    }
                }
                con.Close();
            }

            return manufactureId;
        }

        public static int GetDivisionId(int chromeStyleId)
        {
            int divisionId = 0;

            using (var con = Database.GetConnection(Database.VehicleCatalogDatabase))
            {
                con.Open();

                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = @"
                                SELECT Models.DivisionID
	                                FROM VehicleCatalog.Chrome.Styles
	                                INNER JOIN VehicleCatalog.Chrome.Models ON Styles.ModelID = Models.ModelID
												                                AND Styles.CountryCode = Models.CountryCode
	                                WHERE StyleID = @styleID
	                                AND Styles.CountryCode = 1
                                    ";
                    cmd.CommandType = CommandType.Text;
                    cmd.AddParameterWithValue("@styleID", chromeStyleId);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            divisionId = Convert.ToInt32(reader["DivisionID"]);
                        }
                    }
                }
                con.Close();
            }

            return divisionId;
        }


        /// <summary>
        /// just fetching 'default' chrome style from view in merchanding, chrome.InventoryChromeStyle
        /// lots of work has been done in this view, including regional for Toyota
        /// </summary>
        /// <param name="vin"></param>
        /// <returns></returns>
        public static ChromeStyle InventoryChromeStyle(string vin)
        {
            ChromeStyle chromeStyle = null;
            
            // retrieve and see if we get anything for this VIN
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                conn.Open();

                using (var cmd = conn.CreateCommand())
                {

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"SELECT TOP 1 ChromeStyleID
	                                        FROM chrome.InventoryChromeStyle
	                                        WHERE vin = @vin
	                                        AND ChromeStyleID != -1
                                        ";
                    cmd.Parameters.AddWithValue("@vin", vin);

                    var reader = cmd.ExecuteReader();

                    if(reader.Read())
                    {
                        var styleId = Convert.ToInt32(reader["ChromeStyleID"]);

                        chromeStyle = ChromeStyle.GetByStyleId(styleId).FirstOrDefault();
                    }

                }
                
            }
            return chromeStyle;
        }


        #region Style-related operations.
        /// <summary>
        /// Is this style valid?  Valid styles are not null and have positive style ids.
        /// </summary>
        /// <param name="chromeStyle"></param>
        /// <returns></returns>
        public static bool IsValid(IChromeStyle chromeStyle)
        {
            // valid styles are not null and have positive style ids.
            return chromeStyle != null && IsValid(chromeStyle.ChromeStyleID);
        }

        /// <summary>
        /// Two styles "match" if they have the same chrome style id and if the styles are valid.
        /// </summary>
        /// <param name="style1"></param>
        /// <param name="style2"></param>
        /// <returns></returns>
        public static bool StylesMatch(IChromeStyle style1, IChromeStyle style2)
        {
            // both must be valid
            if ( !(IsValid(style1) && IsValid(style2)) ) return false;

            return StylesMatch(style1.ChromeStyleID, style2.ChromeStyleID);
        }

        /// <summary>
        /// A vehicle has a style if the value > 0.
        /// </summary>
        /// <param name="styleId"></param>
        /// <returns></returns>
        internal static bool IsValid(int styleId)
        {
            return styleId > 0;
        }

        /// <summary>
        /// Styles match if they are valid and equal.
        /// </summary>
        /// <param name="currentStyleId"></param>
        /// <param name="newStyleId"></param>
        /// <returns></returns>
        internal static bool StylesMatch(int currentStyleId, int newStyleId)
        {
            return IsValid(currentStyleId) && IsValid(newStyleId) && currentStyleId == newStyleId;
        }

        internal static bool StylesMatch(IChromeStyle style, int newStyleId)
        {
            return IsValid(style) && StylesMatch(style.ChromeStyleID, newStyleId);
        }

        #endregion
    }
}
