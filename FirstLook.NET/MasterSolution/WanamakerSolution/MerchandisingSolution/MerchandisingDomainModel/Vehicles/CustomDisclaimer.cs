using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class CustomDisclaimer
    {
        #region Properties

        /// <summary>
        /// Identifies the business unit this custom disclaimer belongs to.
        /// </summary>
        public int BusinessUnitId { get; set; }

        /// <summary>
        /// Specifies whether or not this custom disclaimer is to be appended to the default disclaimer.
        /// </summary>
        public bool IsAppend { get; set; }

        /// <summary>
        /// Specifies whether or not this custom disclaimer is to replace the default disclaimer.
        /// </summary>
        public bool IsReplace
        {
            get
            {
                return !IsAppend && !String.IsNullOrEmpty(CustomText);
            }
        }
        /// <summary>
        /// Specifies whether or not this dealer simply wants to use the default disclaimer.
        /// </summary>
        public bool IsDefault
        {
            get {
                return !IsAppend && String.IsNullOrEmpty(CustomText);
            }
        }

        /// <summary>
        /// The custom text for this custom disclaimer.
        /// </summary>
        public string CustomText { get; set; }

        #endregion

        #region Constructor
        public CustomDisclaimer()
        {
            BusinessUnitId = 0;
            IsAppend = false;
            CustomText = String.Empty;
        }
        public CustomDisclaimer(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
            IsAppend = false;
            CustomText = String.Empty;
        }
        public CustomDisclaimer(int businessUnitId, bool isAppend, string customText)
        {
            BusinessUnitId = businessUnitId;
            IsAppend = isAppend;
            CustomText = customText;
        }
        internal CustomDisclaimer(IDataRecord reader) : this(Convert.ToInt32(reader["businessUnitId"]), Convert.ToBoolean(reader["isAppend"]), Convert.ToString(reader["customText"])) 
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Saves this custom disclaimer to the database.
        /// </summary>
        public void Save()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.CustomDisclaimer#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "isAppend", IsAppend, DbType.Byte);
                    Database.AddRequiredParameter(cmd, "customText", CustomText, DbType.String);

                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Retrieves the business unit's custom disclaimer from the database.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <returns></returns>
        public static CustomDisclaimer FetchCustomDisclaimer(int businessUnitId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.CustomDisclaimer#Fetch";

                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return reader.Read() ? new CustomDisclaimer(reader) : new CustomDisclaimer(businessUnitId);
                    }
                }
            }
        }
        #endregion
    }
}
