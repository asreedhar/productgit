using System;
using System.Data;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class KeyInformation
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(KeyInformation).FullName);
        #endregion

        
        public KeyInformation(int keyInformationId, string description)
        {
            KeyInformationId = keyInformationId;
            Description = description;
        }

        public int KeyInformationId { get; set; }

        public string Description { get; set; }

        internal KeyInformation(IDataRecord record)
        {
            KeyInformationId = record.GetInt32(record.GetOrdinal("infoId"));
            Description = record.GetString(record.GetOrdinal("description"));
        }

        public static void DeleteAll(int businessUnitId, int inventoryId)
        {
            Log.DebugFormat("KeyInformation.DeleteAll() called for businessUnitId {0} and inventoryId {1}", businessUnitId, inventoryId);
            using (IDbConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.KeyInformation#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "inventoryId", inventoryId, DbType.Int32);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Save(int businessUnitId, int inventoryId, IDbConnection con)
        {
            Log.DebugFormat("KeyINformation.Save() called for businessUnitId {0} and inventoryId {1}", businessUnitId, inventoryId);

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            using (IDbCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "builder.KeyInformation#Save";
                cmd.CommandType = CommandType.StoredProcedure;

                Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "inventoryId", inventoryId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "infoId", KeyInformationId, DbType.Int32);
                cmd.ExecuteNonQuery();
            }            
        }
    }
}
