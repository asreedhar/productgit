using System;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for ColorPair
    /// </summary>
    [Serializable]
    public class ColorPair
    {
        public ChromeColor First;
        public ChromeColor Second;
        public ColorPair(ChromeColor first, ChromeColor second)
        {
            First = first;
            Second = second;
        }

        public static string GetItemDescription(string firstDescription, string secondDescription, string genericColor)
        {
            string retstr = firstDescription;
            string desc2 = secondDescription;
            if (!String.IsNullOrEmpty(desc2))
            {
                retstr += " with " + desc2;
            }
            if (!String.IsNullOrEmpty(genericColor))
            {
                retstr += " (" + genericColor + ")";
            }
            return retstr;
        }

        public string GetItemDescription()
        {
            return GetItemDescription(First.Description, Second.Description, First.GenericColor);
        }

        public string GetPairDescription()
        {
            return GetPairDescription(First.Description,
                                      Second.Description);
        }

        public string CodesAndDescriptions
        {
            get
            {
                if (Second == null || string.IsNullOrEmpty(Second.ColorCode) || string.IsNullOrEmpty(Second.Description))
                {
                    return string.Format("{0}:{1}", First.ColorCode, First.Description);
                }
                return string.Format("{0}:{1}|{2}:{3}", First.ColorCode, First.Description, Second.ColorCode, Second.Description);
            }
        }

        public string PairDescription
        {
            get
            {
                return GetPairDescription();
            }
        }

        public string PairCode
        {
            get { return GetPairCode(); }
        }

        public string ItemDescription
        {
            get
            {
                return GetItemDescription();
            }
        }

        public static string GetPairDescription(string color1, string color2)
        {
            string retStr = color1;
            if (!String.IsNullOrEmpty(color2))
            {
                retStr += "|" + color2;
            }
            return retStr;
        }

        public string GetPairCode()
        {
            return GetPairCode(First.ColorCode,Second.ColorCode);
        }

        public static string GetPairCode(string colorCode1, string colorCode2)
        {
            string retStr = colorCode1;
            if (!String.IsNullOrEmpty(colorCode2))
            {
                retStr += "|" + colorCode2;
            }
            return retStr;
        }

        public bool Matches(string colorDescription1, string colorDescription2)
        {
            return !string.IsNullOrEmpty(colorDescription1) && 
                    string.Compare(colorDescription1, First.Description, true) == 0 && 
                    colorDescription2 != null && 
                    string.Compare(colorDescription2, Second.Description, true) == 0;
        }

        public static ColorPair Parse(string codeAndDescription)
        {
            var first = new ChromeColor(string.Empty, string.Empty, string.Empty, string.Empty);
            var second = new ChromeColor(string.Empty, string.Empty, string.Empty, string.Empty);

            if (string.IsNullOrEmpty(codeAndDescription))
            {
                return new ColorPair(first, second);
            }

            var codeAndDescriptions = codeAndDescription.Split('|');

            first = GetColorFrom(codeAndDescriptions[0]);

            if (codeAndDescriptions.Length > 1)
            {
                second = GetColorFrom(codeAndDescriptions[1]);
            }

            return new ColorPair(first, second);
        }

        private static ChromeColor GetColorFrom(string codeAndDescription)
        {
            var strings = codeAndDescription.Split(':');

            var code = strings[0];
            var description = strings[1];

            return new ChromeColor(description, string.Empty, code, string.Empty);
        }
    }
}