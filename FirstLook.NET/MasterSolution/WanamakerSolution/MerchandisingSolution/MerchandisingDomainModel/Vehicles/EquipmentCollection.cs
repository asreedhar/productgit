using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using MvcMiniProfiler;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for EquipmentCollection
    /// </summary>
    /// 
    [Serializable]
    public class EquipmentCollection : CollectionBase
    {
        public EquipmentCollection()
        {
        }


        internal EquipmentCollection(IDataReader reader)
        {
            var ld = new ListDictionary();
            while (reader.Read())
            {
                var eq = new Equipment(reader);
                if (eq.EquipmentKind == Equipment.EquipmentKinds.EquipStd)
                {
                    Add(eq);
                }
                else
                {
                    if (ld.Contains(eq.OptionCode))
                    {
                        //note the actual text stored for the equipent is the first one in the result set matching the option code
                        ((Equipment) ld[eq.OptionCode]).AddPricingState((PricingCondition) eq.PricingStates[0]);
                    }
                    else
                    {
                        ld.Add(eq.OptionCode, eq);
                    }
                }
            }
            foreach (DictionaryEntry de in ld)
            {
                Add((Equipment) de.Value);
            }
        }

        public EquipmentCollection FindBySearchText(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return this;
            }

            var eqList = new EquipmentCollection();
            foreach (Equipment equip in List)
            {
                if ((!string.IsNullOrEmpty(equip.OptionCode) &&
                     equip.OptionCode.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0)
                    ||
                    (!string.IsNullOrEmpty(equip.Description) &&
                     equip.Description.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0)
                    ||
                    (!string.IsNullOrEmpty(equip.ExtDescription) &&
                     equip.ExtDescription.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0))
                {
                    eqList.List.Add(equip);
                }
            }

            return eqList;
        }

        public static EquipmentCollection FetchOverrides(int businessUnitId, int chromeStyleId)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.EquipmentDescriptionOverrides#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ChromeStyleId", chromeStyleId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new EquipmentCollection(reader);
                    }
                }
            }
        }

        public static EquipmentCollection FetchUnfilteredOptionsByModel(int modelId)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getUnfilteredOptions";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ModelId", modelId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ChromeStyleId", null, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new EquipmentCollection(reader);
                    }
                }
            }
        }

        public static EquipmentCollection FetchUnfilteredOptions(int chromeStyleId)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getUnfilteredOptions";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new EquipmentCollection(reader);
                    }
                }
            }
        }

        public static EquipmentCollection FetchOptional(int chromeStyleId, string searchText,
                                                        int? customizedDescriptionDealerId)
        {
            using (MiniProfiler.Current.Step("EquipmentCollection.FetchOptional"))
            {
                EquipmentCollection eq;
                if (!string.IsNullOrEmpty(searchText))
                {
                    eq = FetchOptional(chromeStyleId, false);
                }
                else
                {
                    eq = FetchOptional(chromeStyleId);
                }

                if (customizedDescriptionDealerId.HasValue)
                {
                    eq.Add(FetchOverrides(customizedDescriptionDealerId.Value, chromeStyleId));
                    eq.SortByPrice();
                }
                if (!string.IsNullOrEmpty(searchText))
                {
                    eq = eq.FindBySearchText(searchText);
                }
                return eq;
            }
        }

        public static EquipmentCollection FetchOptional(int businessunitId, int inventoryId, int chromeStyleId, string searchText,
                                                       int? customizedDescriptionDealerId)
        {
            var eq = FetchOptional(chromeStyleId, searchText, customizedDescriptionDealerId);

            ProcessOptions(businessunitId, inventoryId, eq);

            return eq;
        }

        public static EquipmentCollection FetchOptionalByOptionCode(int chromeStyleId, string searchText,
                                                                    int? customizedDescriptionDealerId)
        {
            return FetchOptionalByOptionCode( chromeStyleId, searchText, customizedDescriptionDealerId, false );
        }

        public static EquipmentCollection FetchOptionalByOptionCode(int businessUnitId, int inventoryId, int chromeStyleId, string searchText)
        {
            var eq =  FetchOptionalByOptionCode(chromeStyleId, searchText, businessUnitId, false);
            
            ProcessOptions(businessUnitId, inventoryId, eq);

            return eq;
        }

         
        /// <summary>
        /// This method applies the option package rules like text replacement rules to the option packages
        /// e.g.,  remove the 1 year subscription from description for used cars only.
        /// </summary>
        private static void ProcessOptions(int businessUnitId, int inventoryId, EquipmentCollection options)
        {
            if (options == null || options.Count == 0) return;

            var optionRulesEngine = new OptionPackageRulesEngine(Registry.Resolve<IOptionPackageRulesRepository>());

            optionRulesEngine.Process(businessUnitId, inventoryId, options);
        }

        public static EquipmentCollection FetchOptionalByOptionCode( int chromeStyleId, string searchText,
                                                                    int? customizedDescriptionDealerId, bool whiteListOnly )
        {
            EquipmentCollection eq = FetchOptional( chromeStyleId, whiteListOnly );
            if( customizedDescriptionDealerId.HasValue )
            {
                eq.Add( FetchOverrides( customizedDescriptionDealerId.Value, chromeStyleId ) );
                eq.SortByOptionCode();
            }
            if( !string.IsNullOrEmpty( searchText ) )
            {
                eq = eq.FindBySearchText( searchText );
            }
            return eq;
        }

        public static EquipmentCollection FetchOptional(int chromeStyleId, string searchText)
        {
            return FetchOptional(chromeStyleId).FindBySearchText(searchText);
        }

        public static EquipmentCollection FetchOptional(int chromeStyleId)
        {
            return FetchOptional(chromeStyleId, false, -1.0M, false, true);
        }

        public static EquipmentCollection FetchOptional( int chromeStyleId, bool whiteListOnly )
        {
            return FetchOptional( chromeStyleId, false, -1.0M, false, whiteListOnly );
        }

        public static EquipmentCollection FetchOptional(int chromeStyleId, bool useCategories, decimal filterPrice)
        {
            //fetch w/ non-filtered categories, and including customized
            return FetchOptional(chromeStyleId, useCategories, filterPrice, false, true);
        }

        public static EquipmentCollection FetchOptional( int chromeStyleId, bool useCategories, decimal filterPrice, bool filterHeadings ) 
        {
            return FetchOptional( chromeStyleId, useCategories, filterPrice, filterHeadings, true );
        } 

        public static EquipmentCollection FetchOptional(int chromeStyleId, bool useCategories, decimal filterPrice, bool filterHeadings, bool whiteListOnly )
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    //cmd.CommandText = "Chrome.getStyleOptions";
                    cmd.CommandText = "Chrome.getUnfilteredOptions";
                        //use me for all the options (including those without generic categories attached to the set)
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "chromeStyleID", chromeStyleId, DbType.Int32);
                    Database.AddRequiredParameter( cmd, "whiteListOnly", whiteListOnly, DbType.Boolean );
                    //Database.AddWithValue(cmd, "filterPrice", filterPrice, DbType.Decimal);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var styleEquipment = new EquipmentCollection(reader);

                        if (filterHeadings)
                        {
                            var filters = new Hashtable();
                            filters.Add("SERIES ORDER CODE", "");
                            filters.Add("EMISSIONS", "");
                            filters.Add("REGION", "");
                            filters.Add("PAINT", "");
                            styleEquipment = styleEquipment.FilterHeadings(filters);
                        }
                        return styleEquipment;
                    }
                }
            }
        }

        public static EquipmentCollection FetchPossibleOptions(int categoryId, int chromeStyleId)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand()) //new SqlCommand(sql, con))
                {
                    //first try the database for carfax data
                    cmd.CommandText = "Chrome.getPossibleOptions";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "CategoryID", categoryId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var styleEquipment = new EquipmentCollection(reader);

                        var filters = new Hashtable();

                        filters.Add("SERIES ORDER CODE", "");
                        filters.Add("EMISSIONS", "");
                        filters.Add("REGION", "");
                        filters.Add("PAINT", "");
                        styleEquipment = styleEquipment.FilterHeadings(filters);

                        return styleEquipment;
                    }
                }
            }
        }

        public void ReorderList(bool byHeader, bool byPrice, bool packagePriority)
        {
            if (byHeader)
            {
                var ld = new ListDictionary();

                //add packages first if we're prioritizing them
                if (packagePriority)
                {
                    ld.Add("Packages", new EquipmentCollection());
                }

                foreach (Equipment eq in this)
                {
                    string currHeader = eq.Header;

                    //if we are prioritizing packages...and this is one
                    if (packagePriority && eq.CategoryList.ToLower().Contains("p"))
                    {
                        currHeader = "Packages";
                    }

                    if (!ld.Contains(currHeader))
                    {
                        ld.Add(currHeader, new EquipmentCollection());
                    }
                    ((EquipmentCollection) ld[currHeader]).Add(eq);
                }

                //replace
                Clear();
                foreach (DictionaryEntry de in ld)
                {
                    var ec = (EquipmentCollection) de.Value;
                    if (byPrice)
                    {
                        ec.SortByPrice();
                    }

                    Add(ec);
                }
            }


            return;
        }

        public decimal GetMsrpForOption(Equipment op)
        {
            return ChromeLogicEngine.GetSingleOptionPrice(op, this);
        }

        public ListDictionary TOListDictionary()
        {
            var ld = new ListDictionary();
            foreach (Equipment eq in this)
            {
                //check for existing option code in LD?  shouldn't be there
                ld.Add(eq.OptionCode, eq);
            }
            return ld;
        }

        public List<string> TOSelectedOptionCodeList()
        {
            var retList = new List<string>();
            foreach (Equipment eq in this)
            {
                if (eq.Selected)
                {
                    retList.Add(eq.OptionCode);
                }
            }
            return retList;
        }

        public ListDictionary TOHeaderBasedListDictionary()
        {
            var ld = new ListDictionary();
            foreach (Equipment eq in this)
            {
                //check for existing option code in LD?  shouldn't be there
                if (!ld.Contains(eq.Header))
                {
                    ld.Add(eq.Header, new EquipmentCollection());
                }
                ((EquipmentCollection) ld[eq.Header]).Add(eq);
            }
            return ld;
        }

        public string GetSelectedOptions()
        {
            string retStr = "";
            foreach (Equipment eq in this)
            {
                if (eq.Selected)
                {
                    retStr += eq.OptionCode + ",";
                }
            }
            retStr = retStr.TrimEnd(",".ToCharArray());
            return retStr;
        }

        /// <summary>
        /// remove any option code that has a flag in extdescription that indicates a standard piece of equipment in options for that style
        /// </summary>
        /// <returns></returns>
        public EquipmentCollection RemoveStdOptionsForStyle()
        {
            var retEqC = new EquipmentCollection();
            foreach (Equipment eq in this)
            {
                if (eq.EquipmentKind == Equipment.EquipmentKinds.EquipOpt)
                {
                    if (eq.ExtDescription.IndexOf("(STD)") == -1) //if not std...add me
                    {
                        retEqC.Add(eq);
                    }
                }
            }
            return retEqC;
        }

        public EquipmentCollection FilterByCategoryID(ArrayList categoryIDsToShow)
        {
            var retColl = new EquipmentCollection();
            foreach (Equipment eq in List)
            {
                foreach (CategoryLink cl in eq.parseCategoryList())
                {
                    if (categoryIDsToShow.Contains(cl.CategoryID))
                    {
                        retColl.Add(eq);
                        break;
                    }
                }
            }
            return retColl;
        }

        /// <summary>
        /// very messy at this time...need easy way to sort by category or just return the listdictionary
        /// </summary>
        /// <param name="headerFilters"></param>
        /// <returns></returns>
        public EquipmentCollection FilterHeadings(Hashtable headerFilters)
        {
            var ld = new ListDictionary();
            var retEq = new EquipmentCollection();
            foreach (Equipment eq in this)
            {
                if (headerFilters.Contains(eq.Header))
                {
                    if (String.IsNullOrEmpty((string) headerFilters[eq.Header]))
                    {
                        //we're removing these "" filters (and catching nulls)
                        continue;
                    }
                    eq.Header = (string) headerFilters[eq.Header];
                }
                if (!ld.Contains(eq.Header))
                {
                    ld[eq.Header] = new EquipmentCollection();
                }
                ((EquipmentCollection) ld[eq.Header]).Add(eq);
            }

            foreach (DictionaryEntry de in ld)
            {
                foreach (Equipment eq2 in (EquipmentCollection) de.Value)
                {
                    retEq.Add(eq2);
                }
            }
            return retEq;
        }

        public int Add(EquipmentCollection equip)
        {
            int ret = 0;
            foreach (Equipment eq in equip)
            {
                ret += Add(eq);
            }
            return ret;
        }

        public int Add(Equipment equip)
        {
            return (List.Add(equip));
        }

        //determine if the list contains a specified code
        public bool Contains(string optionCode)
        {
            foreach (Equipment eq in this)
            {
                if (eq.OptionCode == optionCode)
                {
                    return true;
                }
            }
            return false;
        }


        public Equipment GetOptionByCategoryID(int categoryID, bool onlySelected, bool getHighestPrice)
        {
            Equipment bestFit = null;
            decimal highPrice = -1.0M;


            foreach (Equipment eq in this)
            {
                string catList = eq.CategoryList;
                //remove items ending with D (e.g. 1019D) b/c this means they remove that category
                catList = catList.ToLower().Replace(categoryID + "d", "");

                if (
                    (eq.EquipmentKind == Equipment.EquipmentKinds.EquipOpt) &&
                    (!onlySelected || eq.Selected) && //if onlySelected false, eq.selected does not matter
                    (catList.Contains(categoryID.ToString()))
                    )
                {
                    if (getHighestPrice)
                    {
                        decimal temp = ChromeLogicEngine.GetSingleOptionPrice(eq, this);
                        if (temp > highPrice)
                        {
                            bestFit = eq;
                            highPrice = temp;
                        }
                    }
                    else
                    {
                        //fall out when not getting highest price
                        bestFit = eq;
                        break;
                    }
                }
            }
            return bestFit;
        }

        public bool Includes(string description)
        {
            foreach (Equipment eq in this)
            {
                if (eq.Description == description)
                {
                    return true;
                }
            }
            return false;
        }

        public Equipment GetByCategoryID(int categoryID, bool stdEquipOrSelOptionsOnly)
        {
            foreach (Equipment eq in this)
            {
                string catList = eq.CategoryList;
                //remove items ending with D (e.g. 1019D) b/c this means they remove that category
                catList = catList.ToLower().Replace(categoryID + "d", "");

                if ((!stdEquipOrSelOptionsOnly || eq.Selected || eq.EquipmentKind == Equipment.EquipmentKinds.EquipStd) &&
                    catList.Contains(categoryID.ToString()))
                {
                    //return the option, return it here
                    return eq;
                }
            }
            return null;
        }

        public Equipment GetByCode(string optionCode)
        {
            foreach (Equipment eq in this)
            {
                if (eq.OptionCode == optionCode)
                {
                    return eq;
                }
            }
            return null;
        }

        public void SetSelected(List<int> selectedCategories)
        {
            foreach (Equipment eq in this)
            {
                eq.Selected = true;
                List<int> cats = ProcessCategories(eq.CategoryList);
                foreach (int categ in cats)
                {
                    //it isn't selected
                    if (!selectedCategories.Contains(categ))
                    {
                        eq.Selected = false;
                        break;
                    }
                }
            }
        }

        public decimal PriceOptions()
        {
            return ChromeLogicEngine.ComputeOptionsPrice(this);
        }


        public static List<int> ProcessCategories(string catStr)
        {
            ArrayList cats = ParseCategoryList(catStr);

            //reset categories, then load them...

            var outputCategories = new List<int>();


            //now process the std equip. categories using the listDictionary
            foreach (CategoryLink catObj in cats)
            {
                int id = catObj.CategoryID;
                switch (catObj.CategoryAction.ToLower())
                {
                    case "c":
                        if (!outputCategories.Contains(id))
                        {
                            outputCategories.Add(id);
                        }
                        break;
                    case "d":
                        outputCategories.Remove(id);
                        break;
                    case "p":
                        if (!outputCategories.Contains(id))
                        {
                            outputCategories.Add(id);
                        }
                        break;
                    default:
                        break;
                }
            }

            return outputCategories;
        }

        public List<int> ProcessCategories()
        {
            string catStr = "";
            foreach (Equipment eq in this)
            {
                if (eq.EquipmentKind == Equipment.EquipmentKinds.EquipStd || eq.Selected)
                {
                    catStr += eq.CategoryList;
                }
            }
            return ProcessCategories(catStr);
        }

        private static ArrayList ParseCategoryList(string categoryList)
        {
            int len = categoryList.Length;

            var arrayList = new ArrayList();
            for (int i = 0; i < len; i += 5)
            {
                string id = categoryList.Substring(i, 4);
                string catAction = categoryList.Substring(i + 4, 1);
                arrayList.Add(new CategoryLink(Int32.Parse(id), catAction));
            }
            return arrayList;
        }

        public List<Equipment> GetPossibleEquipmentForCategoryID(int categoryID)
        {
            var matched = new List<Equipment>();
            foreach (Equipment eq in List)
            {
                foreach (CategoryLink cl in eq.parseCategoryList())
                {
                    if (categoryID == cl.CategoryID)
                    {
                        if (cl.CategoryAction.ToLower() != "d")
                        {
                            //this equipment removes that category, but we selected it in the list...mismatch
                            matched.Add(eq);
                            break;
                        }
                    }
                }
            }
            return matched;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (Equipment eq in List)
            {
                if (eq.EquipmentKind == Equipment.EquipmentKinds.EquipOpt)
                {
                    sb.Append(eq.OptionCode).Append(",");
                }
            }
            return sb.ToString().TrimEnd(",".ToCharArray());
        }

        public List<Equipment> GetBestEquipmentMatchForCategoryIDList(List<int> categoryIDs)
        {
            var matchedAll = new List<Equipment>();
            var matchedSome = new List<Equipment>();

            foreach (Equipment eq in List)
            {
                int matched = 0;
                int unmatched = 0;
                int mismatched = 0;
                foreach (CategoryLink cl in eq.parseCategoryList())
                {
                    if (categoryIDs.Contains(cl.CategoryID))
                    {
                        if (cl.CategoryAction.ToLower() == "d")
                        {
                            //this equipment removes that category, but we selected it in the list...mismatch
                            mismatched++;
                        }
                        else
                        {
                            //we selected hte category, this equipment contains it...match
                            matched++;
                        }
                    }
                    else
                    {
                        //no match found...unmatched
                        unmatched++;
                    }
                }
                if (matched > 0 && (unmatched + mismatched <= 0))
                {
                    matchedAll.Add(eq);
                }
                else if (matched > 0)
                {
                    matchedSome.Add(eq);
                }
            }
            return matchedAll;
        }

        public void SortByPrice()
        {
            InnerList.Sort(new EquipmentCompareByPrice());
        }

        public void SortByOptionCode()
        {
            InnerList.Sort(new EquipmentCompareByCode());
        }

        public List<Equipment> ToList()
        {
            return List.Cast<Equipment>().ToList();
        }

        #region detailedStandard

        /*FILTERED STANDARD EQUIPMENT FUNCTIONS for categorized data sets*/

        public static EquipmentCollection ExteriorEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false, StandardHeaders.Exterior);
        }

        public static EquipmentCollection InteriorEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false, StandardHeaders.Interior);
        }

        public static EquipmentCollection MechanicalEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false, StandardHeaders.Mechanical);
        }

        public static EquipmentCollection SafetyEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false, StandardHeaders.Safety);
        }

        public static EquipmentCollection WarrantyEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false, StandardHeaders.Warranty);
        }

        public static EquipmentCollection FuelEconomyEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false, StandardHeaders.FuelEconomy);
        }

        public static EquipmentCollection OtherEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false, StandardHeaders.Other);
        }

        public static DataSet StdEquipmentSelect(int chromeStyleID)
        {
            return new DataSet();
        }

        public static EquipmentCollection StandardEquipmentSelectNotHighlighted(int chromeStyleID,
                                                                                DetailedEquipmentCollection dec,
                                                                                int businessUnitId, int usedNew)
        {
            using (MiniProfiler.Current.Step("EquipmentCollection.StandardEquipmentSelectNotHighlighted"))
            {
                EquipmentCollection ec = StandardEquipmentSelect(chromeStyleID);

                //Check to see if items should be included
                var returnList = new EquipmentCollection();
                foreach (Equipment item in ec)
                {
                    if (!dec.ContainsStandardHighlight(item.Sequence))
                    {
                        //Replace description with custom description if exists

                        //PERFORMANCE: This is getting called 60X times from the packages control that is in the approval page for most business units that have no custom edit. this
                        //is adding 5 seconds to the page load. Need to delay load at a minimum the Standard Equipment tab on the packages control which is calling this 
                        string custDesc = CustomStandardEquipment.GetCustomStandardEquipmentDescription(businessUnitId,
                                                                                                        usedNew,
                                                                                                        DetailedEquipment
                                                                                                            .
                                                                                                            GetStandardCode
                                                                                                            (
                                                                                                                item.
                                                                                                                    Sequence));
                        if (!String.IsNullOrEmpty(custDesc))
                        {
                            item.Description = custDesc;
                            item.HasCustomDescription = true;
                        }

                        returnList.Add(item);
                    }
                }
                return returnList;
            }
        }

        public static EquipmentCollection StandardEquipmentSelectHighlighted(int chromeStyleID,
                                                                             DetailedEquipmentCollection dec,
                                                                             int businessUnitId, int usedNew)
        {
            using (MiniProfiler.Current.Step("EquipmentCollection.StandardEquipmentSelectHighlighted"))
            {
                EquipmentCollection ec = StandardEquipmentSelect(chromeStyleID);

                //Check to see if items should be included
                var returnList = new EquipmentCollection();
                foreach (Equipment item in ec)
                {
                    if (dec.ContainsStandardHighlight(item.Sequence))
                    {
                        //Replace description with custom description if exists
                        string custDesc = CustomStandardEquipment.GetCustomStandardEquipmentDescription(businessUnitId,
                                                                                                        usedNew,
                                                                                                        DetailedEquipment
                                                                                                            .
                                                                                                            GetStandardCode
                                                                                                            (
                                                                                                                item.
                                                                                                                    Sequence));
                        if (!String.IsNullOrEmpty(custDesc))
                        {
                            item.Description = custDesc;
                            item.HasCustomDescription = true;
                        }

                        returnList.Add(item);
                    }
                }
                return returnList;
            }
        }

        public static EquipmentCollection StandardEquipmentSelect(int chromeStyleID)
        {
            return StandardEquipmentSelect(chromeStyleID, false);
        }

        public static EquipmentCollection StandardEquipmentSelect(int chromeStyleID, bool useCategories)
        {
            return StandardEquipmentSelect(chromeStyleID, useCategories, StandardHeaders.All);
        }

        private static EquipmentCollection StandardEquipmentSelect(int chromeStyleID, bool useCategories,
                                                                   StandardHeaders header)
        {
            /*headers:  
                mechanical
             * interior
             * exterior
             * safety:  safety, safety features
             * warranty
             * fuel economy: epa fuel economy, epa fuel economy rating, epa fuel economy ratings, epa listings
             * others:  no header, no category, emissions certification, note
             */


            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getStandardEquipment";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "StandardHeaderType", (int) header, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var styleEquipment = new EquipmentCollection(reader);
                        var stdfilters = new Hashtable();
                        stdfilters.Add("Notes", "");
                        styleEquipment = styleEquipment.FilterHeadings(stdfilters);


                        if (useCategories)
                        {
                            //we need to process the categories, then run a separate query to get category names
                            List<int> categories = styleEquipment.ProcessCategories();
                            string sqlCat = "";
                            foreach (int categoryID in categories)
                            {
                                sqlCat += categoryID + ",";
                            }
                            sqlCat = sqlCat.TrimEnd(",".ToCharArray());
                            if (String.IsNullOrEmpty(sqlCat))
                            {
                                //no items returned...probably not a valie chromeStyleID
                                return new EquipmentCollection();
                            }


                            using (IDataConnection con2 = Database.GetConnection(Database.VehicleCatalogDatabase))
                            {
                                con2.Open();

                                using (IDbCommand cmd2 = con2.CreateCommand())
                                {
                                    cmd2.CommandText =
                                        "SELECT ct.UserFriendlyName as description, ch.sequence, '' as categoryList, ch.categoryheader as header " +
                                        "FROM chrome.categories as ct INNER JOIN chrome.categoryHeaders as ch ON ch.categoryHeaderID = ct.categoryHeaderID " +
                                        "WHERE ct.CountryCode = 1 AND ch.CountryCode = 1 AND ct.CategoryID IN (" +
                                        sqlCat + ") " +
                                        "ORDER BY ch.sequence, description";
                                    cmd2.CommandType = CommandType.Text;

                                    using (IDataReader reader2 = cmd2.ExecuteReader())
                                    {
                                        var filters = new Hashtable();

                                        //unsubstituted - used for self:  Air Conditioning, Audio, Convenience, Window, Seats

                                        filters.Add("Steering", "Mechanical");
                                        filters.Add("Suspension", "Mechanical");
                                        filters.Add("Differential", "Mechanical");
                                        filters.Add("Brakes", "Mechanical");

                                        //remove these
                                        filters.Add("Vehicle", "");
                                        filters.Add("Engine", "");
                                        filters.Add("Transmission", "");
                                        filters.Add("Telephone", "");
                                        filters.Add("Doors", "");
                                        filters.Add("Pickup Box", "");
                                        filters.Add("Roof", "");
                                        filters.Add("Drivetrain", "");
                                        filters.Add("Fuel", "");

                                        filters.Add("Running Boards", "Convenience");
                                        filters.Add("Floor Mats", "Convenience");
                                        filters.Add("Locks", "Convenience");
                                        filters.Add("Mirrors", "Convenience");

                                        filters.Add("Wheels", "Wheels/Tires");
                                        filters.Add("Tires", "Wheels/Tires");
                                        filters.Add("Tire - Front", "Wheels/Tires");
                                        filters.Add("Tire - Rear", "Wheels/Tires");
                                        filters.Add("Tire - Spare", "Wheels/Tires");

                                        filters.Add("Air Bag - Frontal", "Safety/Security");
                                        filters.Add("Air Bag - Side", "Safety/Security");
                                        filters.Add("Safety Features", "Safety/Security");

                                        filters.Add("Seat Trim", "Seats");

                                        styleEquipment = new EquipmentCollection(reader2);
                                        styleEquipment = styleEquipment.FilterHeadings(filters);
                                    }
                                }
                            }
                        }
                        return styleEquipment;
                    }
                }
            }
        }

        #endregion
    }


    public class EquipmentCompareByPrice : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            var eq1 = x as Equipment;
            var eq2 = y as Equipment;
            if (eq1 == null || eq2 == null)
            {
                return 0;
            }
            return eq2.getHighestMSRP().CompareTo(eq1.getHighestMSRP());
        }

        #endregion
    }

    public class EquipmentCompareByCode : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            var eq1 = x as Equipment;
            var eq2 = y as Equipment;
            if (eq1 == null || eq2 == null)
            {
                return 0;
            }
            return eq1.OptionCode.CompareTo(eq2.OptionCode);
        }

        #endregion
    }
}