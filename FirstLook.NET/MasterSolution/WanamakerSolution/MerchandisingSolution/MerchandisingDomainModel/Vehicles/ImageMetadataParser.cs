using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text;


namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class ImageMetadataParser
    {
        private const int ORIGINAL_DATE_TAG_NUMBER = 36867;
        private const int COMMENT_TAG_NUMBER = 40094;

        public static ImageMetadata Extract(Stream fileStream)
        {
            Bitmap bmp = new Bitmap(fileStream);
            
            return Extract(bmp);
        }

        public static ImageMetadata Extract(FileInfo jpegFile)
        {
            Bitmap bmp = new Bitmap(jpegFile.FullName);
            return Extract(bmp);
        }
        private static ImageMetadata Extract(Bitmap jpegImage)
        {
            
            PropertyItem[] propItems = jpegImage.PropertyItems;
            
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo {FullDateTimePattern = "yyyy:MM:dd HH:mm:ss"};

            DateTime dateTaken = DateTime.Now;
            string textTag = string.Empty;
            foreach (PropertyItem propItem in propItems)
            {
                if (propItem.Id == ORIGINAL_DATE_TAG_NUMBER)
                {
                    byte[] bytes = propItem.Value;

                    using (StreamReader tr = new StreamReader(new MemoryStream(bytes), Encoding.ASCII))
                    {
                        string dateStr = tr.ReadToEnd();
                        dateStr = dateStr.TrimEnd("\0".ToCharArray());
                        dateTaken = DateTime.ParseExact(dateStr, dtfi.FullDateTimePattern, dtfi);        
                    }
                    
                }
                else if (propItem.Id == COMMENT_TAG_NUMBER)
                {                    
                    byte[] characters = propItem.Value;
                    using (StreamReader tr = new StreamReader(new MemoryStream(characters), Encoding.Unicode))
                    {
                        textTag = tr.ReadToEnd();
                        textTag = textTag.TrimEnd("\0".ToCharArray());
                    }
                    
                    
                }

            }

            return new ImageMetadata(dateTaken, textTag);
        }
    }
}
