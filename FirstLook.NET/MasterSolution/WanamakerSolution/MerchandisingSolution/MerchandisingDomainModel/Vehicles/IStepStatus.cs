namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface IStepStatus
    {
        int BusinessUnitId { get; }
        int InventoryId { get; }
        StepStatusTypes StatusTypeId { get; }
        int StatusLevel { get; set; }
        bool IsDirty { get; }
        bool IsNew { get; }
        byte[] Version { get; }
    }
}