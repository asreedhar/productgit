using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using FirstLook.Common.Data;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum ForcedInductionType
    {
        None,
        Supercharged = 1053,
        Turbocharged = 1054
    }

    public enum EngineType
    {
        Undefined,
        Engine3Cyl = 1047,
        Engine4Cyl = 1048,
        Engine5Cyl = 1049,
        Engine6CylV6 = 1051,
        Engine6CylStraight = 1050,
        Engine6CylFlat = 1135,
        Engine8Cyl = 1052,
        Engine10Cyl = 1045,
        Engine12Cyl = 1046,
        EngineRotary = 1165
    }
    
    public class Engine
    {
        public bool IsStandard
        {
            get
            {
                return string.IsNullOrEmpty(OptionCode);
            }
        }
        public Engine(string description, string optionCode, EngineType engine, CategoryCollection packageCategoryIdList)
        {
            Description = description;
            PackageCategoryIdList = packageCategoryIdList;
            EngineCategory = engine;
            OptionCode = optionCode;
        }

        public int? Cylinders
        {
            get
            {
                switch (EngineCategory)
                {
                    case EngineType.Engine3Cyl:
                        return 3;
                    case EngineType.Engine4Cyl:
                        return 4;
                    case EngineType.Engine5Cyl:
                        return 5;
                    
                    case EngineType.Engine6CylFlat:
                    case EngineType.Engine6CylV6:
                    case EngineType.Engine6CylStraight:
                        return 6;
                    case EngineType.Engine8Cyl:
                        return 8;
                    case EngineType.Engine10Cyl:
                        return 10;
                    case EngineType.Engine12Cyl:
                        return 12;
                    default:
                        return null;
                    
                }
            }
        }
        public Engine()
        {
            Description = string.Empty;
            EngineCategory = EngineType.Undefined;
            PackageCategoryIdList = new CategoryCollection("");
            OptionCode = string.Empty;
        }
        public string EngineArgs
        {
            get
            {
                return OptionCode + "|" + ((int) EngineCategory);
            }
        }
        internal Engine(IDataRecord reader)
        {
            Description = reader.GetString(reader.GetOrdinal("engineDescription"));
            EngineCategory = (EngineType) reader.GetInt32(reader.GetOrdinal("engineCategoryId"));
            PackageCategoryIdList = Equipment.parseCategoryList(reader.GetString(reader.GetOrdinal("categoryList")));
            OptionCode = reader.GetString(reader.GetOrdinal("optionCode"));

            CleanEngineDescription();
        }

        public string Description { get; set; }

        public EngineType EngineCategory { get; set; }

        public CategoryCollection PackageCategoryIdList { get; set; }

        public string OptionCode { get; set; }

        private void CleanEngineDescription()
        {
            int ndx = Description.IndexOf("-inc", StringComparison.CurrentCultureIgnoreCase);

            if (ndx > -1)
            {
                Description = Description.Substring(0, ndx);
            }

            if (Description.StartsWith("engine,", StringComparison.CurrentCultureIgnoreCase))
            {
                Description = Description.Remove(0, 7).TrimStart(' ');
            }

            //remove items in parens
            Regex paren = new Regex(@"[\(][^\(\)]*[\)]");
            while (paren.IsMatch(Description))
            {
                Description = paren.Replace(Description, "");
            }

        }

        public static List<Engine> GetEngines(int chromeStyleId)
        {
            List<Engine> engines = new List<Engine>();
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "chrome.getEngines";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "StyleId", chromeStyleId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            engines.Add(new Engine(reader));
                        }
                        return engines;
                    }

                }
            }
            
            
        }
    }
}
