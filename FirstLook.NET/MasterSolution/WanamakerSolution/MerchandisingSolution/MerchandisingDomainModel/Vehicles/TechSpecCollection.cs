using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class TechSpecCollection : CollectionBase
    {
        public static TechSpecCollection Fetch(int chromeStyleId)
        {
            
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "chrome.TechSpecs#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "ChromeStyleId", chromeStyleId, DbType.Int32);

                    TechSpecCollection tsc = new TechSpecCollection();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tsc.Add(new TechSpec(reader));
                        }
                        return tsc;
                    }
                }
            }
        }

        /// <summary>
        /// Simple returns the first available tech spec of the type where there is no condition, has an asterisk after it
        /// </summary>
        /// <param name="specType"></param>
        /// <returns></returns>
        public string GetUnofficialTechSpec(TechSpecType specType, DetailedEquipmentCollection packages)
        {
            foreach (TechSpec ts in List)
            {
                if (ts.TitleId == (int)specType && packages.ContainsOption(ts.Condition)) //take TechSpecs with package.
                {
                    string desc = ts.GetDescription(specType);
                    //don't return if it's blank for some reason
                    if (!string.IsNullOrEmpty(desc))
                    {
                        return desc + "*";
                    }
                }
                //if this is the title we're looking for and it isn't predicated on some condition, use it
                else if (ts.TitleId == (int)specType && string.IsNullOrEmpty(ts.Condition))
                {
                    string desc = ts.GetDescription(specType);
                    //don't return if it's blank for some reason
                    if (!string.IsNullOrEmpty(desc))
                    {
                        return desc +"*";
                    }
                }
            }
            return string.Empty;
        }

        public string GetTechSpec(TechSpecType specType, List<string> optionCodes)
        {
            foreach (TechSpec ts in List)
            {
                if (ts.TitleId == (int)specType)
                {
                    if (ChromeLogicEngine.EvalLogic(ts.Condition, optionCodes))
                    {
                        return ts.GetDescription();
                    }
                }
            }
            return string.Empty;
        }

        public int Add(TechSpec spec)
        {
            return List.Add(spec);
        }
    }
}
