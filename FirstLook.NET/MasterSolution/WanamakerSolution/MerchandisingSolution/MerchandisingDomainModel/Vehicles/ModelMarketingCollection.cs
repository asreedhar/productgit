using System;
using System.Collections;
using System.Data;
using System.Text;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class ModelMarketingCollection : CollectionBase
    {
        internal ModelMarketingCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(new ModelMarketing(reader));
            }
        }

        public ModelMarketingCollection()
        {
            
        }

        public static ModelMarketingCollection Fetch(int chromeStyleId)
        {
            //TAKEN OUT FOR MOVE TO PROD
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "merchandising.modelMarketing#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ChromeStyleId", chromeStyleId, DbType.Int32);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new ModelMarketingCollection(reader);
                    }
                }

            }
        }

        public ModelMarketing GetByType(ModelMarketingType type)
        {
            foreach (ModelMarketing mkt in List)
            {
                if (mkt.MarketingType == type)
                {
                    return mkt;
                }
            }
            throw new ApplicationException("MarketingType not found in collection");
        }

        public int Add(ModelMarketing marketing)
        {
            return List.Add(marketing);
        }  

        public string GetTopMarketingText(ModelMarketingType type)
        {
            //order marketing items
            InnerList.Sort();

            string retStr = string.Empty;
            foreach (ModelMarketing mkt in List)
            {
                if (mkt.MarketingType == type && !string.IsNullOrEmpty(mkt.MarketingText))
                {
                    retStr = mkt.MarketingText;
                    break;
                }
            }
            return retStr;
        }

        public string GetFullMarketingText()
        {
            StringBuilder sb = new StringBuilder();
            foreach (ModelMarketing mkt in List)
            {
                sb.Append(mkt.MarketingText);
                sb.Append("  ");
            }
            return sb.ToString();
        }
        public string GetFullMarketingText(ModelMarketingType type)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ModelMarketing mkt in List)
            {
                if (mkt.MarketingType == type)
                {
                    sb.Append(mkt.MarketingText);
                    sb.Append(", ");
                }
            }
            return sb.ToString();
        }

        public bool Contains(ModelMarketingType type)
        {
            foreach (ModelMarketing mkt in List)
            {
                if (mkt.MarketingType == type)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
