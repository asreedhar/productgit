﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.MaxAnalytics;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    class InventoryOnlineStateData
    {
        private IMaxAnalytics _maxAnalytics;

        //private static ICache cache = Registry.Resolve<ICache>();
        //private const string cache_key_template = "Wanamaker_InventoryOnlineStateData_{0}_{1}_{2}";
        //private static int? cache_expiration_time = 30*60; // 30 minutes expressed in seconds

        public InventoryOnlineStateData(IMaxAnalytics analytics)
        {
            _maxAnalytics = analytics;
        }

        public int CrossFill( 
            int businessUnitId, 
            VehicleType usedOrNew, 
            int minAgeInDays, 
            List<IInventoryData> inventory)
        {
            int totalSet = 0;
            if( inventory.Count == 0 )
                return totalSet;
            
            var onlineState = GetOnlineState( businessUnitId, usedOrNew, minAgeInDays );
            totalSet = onlineState.Count;

            foreach( var inv in inventory )
                if( onlineState.ContainsKey( inv.InventoryID ))
                    inv.OnlineState = onlineState[inv.InventoryID];

            return totalSet;
        }

        private Dictionary<int,int> GetOnlineState( 
            int businessUnitId, 
            VehicleType usedOrNew, 
            int minAgeInDays )
        {
            
            var inventoryOnlineState = new Dictionary<int, int>();

            var vehiclesOnLine = _maxAnalytics.GetOnlineVehicles(businessUnitId, usedOrNew.IntValue, minAgeInDays);
            foreach(var online in vehiclesOnLine)
            {
                int onlineState;
                if(!online.HasData)
                    onlineState = 0;
                else if(online.Online)
                    onlineState = 1; //Online
                else
                    onlineState = 2; //not online*/

                inventoryOnlineState.Add(online.InventoryId, onlineState);
            }

            return inventoryOnlineState;
        }
    }
}
