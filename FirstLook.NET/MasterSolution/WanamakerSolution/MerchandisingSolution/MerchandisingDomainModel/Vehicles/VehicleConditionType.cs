namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum VehicleConditionType
    {
        Undefined = 0,
        Exterior = 1,
        Paint = 2,
        PanelScratches = 3,
        
        BodyworkAccidents = 6,
        Interior = 7,
        Carpet = 8,
        Seats = 9,
        Headliner = 10,
        Dashboard = 11,
        Tires = 12
    }
}
