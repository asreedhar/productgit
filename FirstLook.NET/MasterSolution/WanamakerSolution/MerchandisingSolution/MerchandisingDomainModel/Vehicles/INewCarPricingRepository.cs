﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface INewCarPricingRepository
    {
        IEnumerable<IInventoryData> GetNewCarPricingInventory(int businessUnitId);
    }
}