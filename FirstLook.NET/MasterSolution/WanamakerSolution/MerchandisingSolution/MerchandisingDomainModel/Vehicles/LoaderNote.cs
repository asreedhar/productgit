using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class LoaderNote
    {
        private readonly int _id;
        private readonly bool _isNew;

        public bool IsNew
        {
            get { return _isNew; }
        }

        public LoaderNote(string noteText, DateTime takenOn, string takenByLogin)
        {
            NoteText = noteText;
            TakenOn = takenOn;
            TakenByLogin = takenByLogin;
            _isNew = true;
        }

        internal LoaderNote(IDataRecord reader)
        {
            _id = reader.GetInt32(reader.GetOrdinal("noteId"));
            NoteText = reader.GetString(reader.GetOrdinal("noteText"));
            TakenByLogin = reader.GetString(reader.GetOrdinal("writtenBy"));
            TakenOn = reader.GetDateTime(reader.GetOrdinal("writtenOn"));
            _isNew = false;
        }

        public void Save(int businessUnitId, int inventoryId, string memberLogin)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    if (_isNew)
                    {
                        cmd.CommandText = @"builder.LoaderNote#Create";
                        Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);
                        Database.AddRequiredParameter(cmd, "NoteText", NoteText, DbType.String);
                    }
                    else
                    {
                        cmd.CommandText = @"builder.LoaderNote#Update";
                        Database.AddRequiredParameter(cmd, "NoteId", _id, DbType.Int32);
                        Database.AddRequiredParameter(cmd, "MemberLogin", memberLogin, DbType.String);
                        Database.AddRequiredParameter(cmd, "NoteText", NoteText, DbType.String);

                    }
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }

            }
        }

        public string NoteText { get; set; }

        public DateTime TakenOn { get; set; }

        public string TakenByLogin { get; set; }
    }
}
