using System;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class VehicleCatalogException : Exception
    {
        public string Vin { get; set; }

        public VehicleCatalogException(string message, string vin) : base(message)
        {
            Vin = vin;
        }

        public VehicleCatalogException(string message, string vin, Exception inner)
            : base(message, inner)
        {
            Vin = vin;
        }
    }
}
