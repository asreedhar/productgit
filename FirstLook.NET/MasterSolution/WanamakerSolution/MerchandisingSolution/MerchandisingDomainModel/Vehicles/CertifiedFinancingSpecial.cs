using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class CertifiedFinancingSpecial : FinancingSpecial
    {
        public CertifiedFinancingSpecial(int id, string title, string specialDescription, DateTime expirationDate,
                                         List<int> certificationTypeIds)
            : base(id, title, specialDescription, expirationDate)
        {
            CertificationTypeIds = certificationTypeIds;
        }
        
        internal CertifiedFinancingSpecial(IDataRecord reader) : base(reader)
        {
            CertificationTypeIds = new List<int>();
            string ids = reader.GetString(reader.GetOrdinal("certificationTypeIds"));
            foreach (string id in ids.Split(",".ToCharArray()))
            {
                CertificationTypeIds.Add(Int32.Parse(id));
            }
    
        }

        

        public static CertifiedFinancingSpecial FetchCertifiedSpecial(int ownerEntityTypeId, int ownerEntityId, int certifiedTypeId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.financingSpecials#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "OwnerEntityTypeId", ownerEntityTypeId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OwnerEntityId", ownerEntityId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "CertificationTypeId", certifiedTypeId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        
                        if (reader.Read())
                        {
                           return new CertifiedFinancingSpecial(reader);
                        }
                        throw new ApplicationException("Certified Special not found id=" + certifiedTypeId + ", owner entity=" + ownerEntityId + " entity type=" + ownerEntityTypeId);

                    }

                }
            }

        }

        public List<int> CertificationTypeIds { get; set; }
    }
}
