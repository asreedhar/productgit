using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using System.Text;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews;
using log4net;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class DisplayEquipmentCollection
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(DisplayEquipmentCollection).FullName);
        #endregion


        private readonly int _businessUnitId;
        private readonly DealerAdvertisementPreferences _dealerAdvertisementPreferences;

        private readonly VehicleDataTransfer _inventoryData;

        private OptionPackageRulesEngine _optionRulesEngine;

        public DisplayEquipmentCollection()
        {
            Log.Debug("DisplayEquipmentCollection() default ctor called.");

            options = new DetailedEquipmentCollection();
            generics = new GenericEquipmentCollection();
            replacements = new GenericEquipmentReplacementCollection();
            options.OptionChanged += OptionChanged;
        }

        internal DisplayEquipmentCollection(int businessUnitId, VehicleDataTransfer transfer, DealerAdvertisementPreferences dealerAdvertisementPreferences)
        {
            Log.DebugFormat("DisplayEquipmentCollection() called for businessUnitId {0}", businessUnitId);

            _businessUnitId = businessUnitId;

            _inventoryData = transfer;

            _dealerAdvertisementPreferences = dealerAdvertisementPreferences;

            generics = transfer.Generics;
            Log.DebugFormat("Generics are {0}", StandardObjectDumper.Write(generics,2));

            options = transfer.Options;
            Log.DebugFormat("Options are {0}", StandardObjectDumper.Write(options,2));

            options.OptionChanged += OptionChanged;
            replacements = transfer.Replacements;
            Log.DebugFormat("ReplacementCollection is {0}", StandardObjectDumper.Write(replacements,1));
            _chromeStyleId = transfer.ChromeStyleID;
        }

        private readonly int _chromeStyleId;

        private List<int> _usedGenerics = new List<int>();
       
        private bool IsGenericUsed(int categoryId)
        {
            Log.DebugFormat("IsGenericsUsed() called with {0}", categoryId);
            var result = _usedGenerics.Contains(categoryId);
            Log.DebugFormat("Result is {0}", result);
            return result;
        }
        private void SetGenericAsUsed(int categoryId)
        {
            Log.DebugFormat("SetGenericAsUsed() called with categoryId {0}", categoryId);
//            _usedGenerics.Add(categoryId);
        }
        private void SetGenericsAsUsed(IEnumerable<int> categoryIds)
        {
            Log.DebugFormat("SetGenericAsUsed() called with categoryId {0}", categoryIds.ToDelimitedString(","));
//            _usedGenerics.AddRange(categoryIds);
        }

        public DetailedEquipmentCollection options;
        public GenericEquipmentCollection generics;
        public GenericEquipmentReplacementCollection replacements;

        public string GetInteriorType()
        {
            return getTopDescriptionForHeader((int)GenericEquipmentHeaders.SeatTrim, true, true, 1);
        }
        public bool HasInteriorType()
        {
            return !string.IsNullOrEmpty(getTopDescriptionForHeader((int)GenericEquipmentHeaders.SeatTrim, true, false, 1));
        }

        public void AddGeneric(GenericEquipment equipment)
        {
            Log.DebugFormat("AddGeneric() called with equipment {0}", StandardObjectDumper.Write(equipment, 1));

            if( !generics.Contains(equipment))
            {
                Log.DebugFormat("Adding generic equipment {0} to collection", StandardObjectDumper.Write(equipment, 1));
                generics.Add(equipment);                
            }
            else
            {
                Log.DebugFormat("Generic collection already contains equipment {0}", StandardObjectDumper.Write(equipment, 1));                
            }

        }
        public void AddOption(DetailedEquipment equipment)
        {
            Log.DebugFormat("AddOption() called with equipment {0}", StandardObjectDumper.Write(equipment,1));

            if( !options.Contains(equipment))
            {
                Log.DebugFormat("Adding options equipment {0} to collection", StandardObjectDumper.Write(equipment, 1));
                options.Add(equipment); //fires optionchanged event                
            }
            else
            {
                Log.DebugFormat("Options collection already contains equipment {0}", StandardObjectDumper.Write(equipment, 1));                
            }

        }

        public void AddOption(Equipment equipment, int businessUnitId, int inventoryId)//
        {
            Log.DebugFormat("AddOption() called with equipment {0}, businessUnitId {1}, inventoryId {2}", 
                StandardObjectDumper.Write(equipment, 1), businessUnitId, inventoryId);

            DetailedEquipment equip = new DetailedEquipment(equipment);

            // Only proceed if we don't already contain the equipment.
            if(options.Contains(equip))
            {
                Log.DebugFormat("Options collection already contains equipment {0}", StandardObjectDumper.Write(equipment, 1));
                return;
            }


            //now convert the detailed text by default to derive from the combination of 
            //title and ext description
            equip.DetailText = (equip.OptionText.TrimEnd() + " " + equip.DetailText.Trim()).Trim();
            options.Add(equip);

            // remove generics which should be removed (by way of category links)
            GenericEquipmentCollection temp = generics;
            foreach (CategoryLink cl in equip.categoryRemoves)
            {
                List<int> removes = CategoryCollection.GetCategoryRemovesMap(cl.CategoryID);
                foreach (int item in removes)
                {
                    Log.DebugFormat("Removing item {0}", item);
                    temp.Remove(item);
                }
            }

            // TODO??? : add generics which should be added (by way of category links)
            generics = temp;
            generics.Save(businessUnitId, inventoryId);
        }

        public void Remove(int categoryId)
        {
            Log.DebugFormat("Remove() called with categoryId {0}", categoryId);
            generics.Remove(categoryId);
        }

        public bool Contains(int categoryId)
        {
            Log.DebugFormat("Contains() called with categoryId {0}", categoryId);
            var result = generics.Contains(categoryId);
            Log.DebugFormat("Result is {0}", result);
            return result;
        }
        
        public bool Contains(string description)
        {
            Log.DebugFormat("Contains() called with description '{0}'", description);
            var result = generics.Contains(description);
            Log.DebugFormat("Result is {0}", result);
            return result;
        }

        public void RemoveOption(DetailedEquipment equipment)
        {
            Log.DebugFormat("RemoveOption() called with DetailedEquipment {0}",
                StandardObjectDumper.Write(equipment,1));

            options.Remove(equipment.OptionCode); //fires optionchanged event
        }


        public bool LoadMappedEquipment(int businessUnitId, int inventoryId, bool forceReload, int chromeStyleID)
        {
            Log.InfoFormat("LoadMappedEquipment() called with businessUnitId {0}, inventoryId {1}, forceReload {2}",
                businessUnitId, inventoryId, forceReload);

            bool lotLoaded = false;
            if (forceReload || !Lot.IsLoaded(inventoryId))
            {
                //attempt the third party mapping
                GenericEquipmentCollection mappedOpts = Lot.FetchMappedThirdPartyOptions(businessUnitId, inventoryId);

                // before adding to the equipment for this vehicle, filter by equipment in chrome style
                mappedOpts = filterEquipment(mappedOpts, chromeStyleID);

                generics.AddRange(mappedOpts);

                if (mappedOpts.Count > 0)
                {
                    Lot.MarkAsLoaded(inventoryId);
                    lotLoaded = true;
                }

                Log.InfoFormat("Found {0} mapped options", mappedOpts.Count);
                Log.DebugFormat("Added thkird party mapping {0}", StandardObjectDumper.Write(mappedOpts,1));
            }

            return lotLoaded;
        }

        public virtual GenericEquipmentCollection filterEquipment(GenericEquipmentCollection gec, int chromeStyleID)
        {
            // insersects a generic equipment collection against a style
            // TODO: if an invalid chromeStyle is sent ... all equipment would be invalid, warn user? don't filter?

            GenericEquipmentCollection goodEquipment = new GenericEquipmentCollection();

            // find all equipment associated with this chrome style
            GenericChoiceCollection gcc = getChromeEquipment(chromeStyleID);

            // change to array so I can use a little linq
            GenericChoice[] gcArray = new GenericChoice[gcc.Count];

            gcc.CopyTo(gcArray, 0);

            foreach(GenericEquipment ge in gec)
            {
                if (gcArray.Any(g => g.ID == ge.Category.CategoryID))
                {
                    goodEquipment.Add(ge);
                }
            }

            return goodEquipment;
        }
        public virtual GenericChoiceCollection getChromeEquipment(int chromeStyleID)
        {
            return GenericChoicesDataSource.FetchStyleEquipment(chromeStyleID, 0);
        }

        private void OptionChanged(object sender, OptionChangedEventArgs e)
        {
            Log.DebugFormat("OptionChanged() called with OptionChangedEventArgs {0}.",
                StandardObjectDumper.Write(e, 1));

            foreach (CategoryLink cl in e.Equipment.categoryAdds)
            {
                if (e.Selected)
                {
                    //add it to the generics
                    var generic = new GenericEquipment(cl, false); 
                    generics.Add(generic);
                    Log.DebugFormat("Added to generics {0}", StandardObjectDumper.Write(generic,1));
                }
                else
                {
                    //remove from generics
                    generics.RemoveOptionsOnly(cl.CategoryID);
                    Log.DebugFormat("Removed options with categoryID {0}", cl.CategoryID);
                }
            }

            //we do opposite w/ the "removes" - may be a tiny bit dicey when removing...
            Log.Debug("Processing category removes.");
            foreach (CategoryLink cl in e.Equipment.categoryRemoves)
            {
                if (e.Selected)
                {
                    generics.RemoveOptionsOnly(cl.CategoryID);
                    Log.DebugFormat("RemoveOptionsOnly() called with categoryID {0}", cl.CategoryID);
                }
                else
                {
                    var generic = new GenericEquipment(cl, false); 
                    generics.Add(generic);
                    Log.DebugFormat("Added to generics {0}", generic);
                }
            }

            //check any genericMetaEquipment...if they are now included, add them/remove as necessary
            replacements.SetTierAndDisplayPriority(generics);
        }



        public void Save(int businessUnitId, int inventoryId)
        {
            Log.InfoFormat("Save() called for businessUnitId {0}, inventoryId {1}",
                businessUnitId,inventoryId);

            generics.Save(businessUnitId, inventoryId);
            options.Save(businessUnitId, inventoryId);
        }

        private int drivetrainHeaderID = 37;
        private int engineHeaderID = 13;
        private int fuelTypeHeaderID = 46;
        private int transmissionHeaderID = 15;

        public string GetEngine()
        {
            return GetTopDescriptionForTypeFilter(ChromeCatFilter.ENGINE, true);
        }


        private string GetTopDescriptionForTypeFilter(string typeFilter, bool favorOptional = false)
        {
            return GetTopDescriptionForTypeFilter(typeFilter, 4, favorOptional);
        }

        private string GetTopDescriptionForTypeFilter(string typeFilter, int maxTierToAccept, bool favorOptional)
        {
            foreach (GenericEquipment equip in generics)
            {
                if (equip.MatchesTypeFilter(typeFilter) && (equip.GetTier() <= maxTierToAccept))
                {
                    return equip.GetDescription();
                }
            }
            return string.Empty;
        }

        public string GetDrivetrain()
        {
            return GetTopDescriptionForTypeFilter(ChromeCatFilter.DRIVETRAIN);
        }

        public string GetFuelType()
        {
            return GetTopDescriptionForTypeFilter(ChromeCatFilter.FUEL);
        }
        public string GetTransmission()
        {
            return GetTopDescriptionForTypeFilter(ChromeCatFilter.TRANSMISSION);
        }

        public void clearEngine()
        {
            generics.RemoveAllWithHeader(engineHeaderID);

        }

        public void clearTransmission()
        {
            generics.RemoveAllWithHeader(transmissionHeaderID);
        }

        public void clearFuelType()
        {
            generics.RemoveAllWithHeader(fuelTypeHeaderID);
        }

        public void clearDrivetrain()
        {
            generics.RemoveAllWithHeader(drivetrainHeaderID);
        }


        private string getTopDescriptionForHeader(int headerId, bool includeStandard)
        {
            return getTopDescriptionForHeader(headerId, includeStandard, false, 4);
        }


        private string getTopDescriptionForHeader(int headerId, bool includeStandard, bool useEquipment, int maxTierToAccept)
        {
            Log.DebugFormat("getTopDescriptionForHeader() called with headerId {0}, includeStandard {1}, useEquipment {2}, maxTierToAccept {3}",
                headerId, includeStandard, useEquipment, maxTierToAccept);

            GenericEquipmentCollection gec = generics.GetByHeaderID(headerId, includeStandard);
            if (gec.Count > 0)
            {
                GenericEquipment equip = gec.Item(0);
                if (equip.GetTier() <= maxTierToAccept)
                {
                    if (useEquipment)
                    {
                        SetGenericAsUsed(equip.Category.CategoryID);
                    }
                    return equip.GetDescription();
                }
            }
            return string.Empty;
        }



        public List<int> GetCategoryIdList()
        {
            return generics.GetCategoryIdList();

        }

        public string GetOrderedDescriptionText(int businessUnitId)
        {
            FilterAndReorder(businessUnitId);

            StringBuilder sb = new StringBuilder();
            foreach (GenericEquipment de in generics)
            {
                sb.Append(de.GetDescription());
                sb.Append(", ");
            }
            return sb.ToString().TrimEnd(", ".ToCharArray());
        }


        #region sortingAndFiltering


        /*

        private DetailedEquipmentCollection GetScoredAndSortedOptions(int tierNumber, int[] maxOptionsByTier)
        {
            //need the data to exist
            if (tierNumber >= maxOptionsByTier.Length)
            {
                return new DetailedEquipmentCollection();
            }

            DetailedEquipmentCollection retCollection = new DetailedEquipmentCollection();
            List<Pair> codeScorePairs = new List<Pair>();
            foreach (DetailedEquipment equip in options)
            {
                codeScorePairs.Add(new Pair(equip.OptionCode, equip.GetDetailScore(generics, tierNumber)));
            }
            codeScorePairs.Sort(delegate(Pair firstPair, Pair nextPair)
                                    {
                                        return ((int) firstPair.Second).CompareTo(((int) nextPair.Second));
                                    });

            foreach (Pair pair in codeScorePairs)
            {
                retCollection.Add(options.GetByCode((string) pair.First));
            }

            int ctToRemove = 0;
            for(int i=0; i<tierNumber; i++)
            {
                ctToRemove += maxOptionsByTier[i];
            }
            if (retCollection.Count > ctToRemove)
            {
                retCollection.RemoveRange(0, ctToRemove);
            }
            return retCollection;
        }
        */

        /// <summary>
        /// Filter overridden generics - works through the list of sorted equipment and removes items
        /// further down in the list when they would be redundant with items earlier in the list
        /// </summary>
        /// <param name="equipment"></param>
        /// <returns></returns>
        private List<ITieredEquipment> FilterOverridenGenerics(List<ITieredEquipment> equipment)
        {
            replacements.SortByCategoryIdCountDesc();
            Log.DebugFormat("FilterOverriddenGenerics() called with list '{0}'", PrintEquipmentList(equipment));

            // as a test, do nothing
//            return equipment;

            List<ITieredEquipment> retList = new List<ITieredEquipment>();

            if( equipment == null )
            {
                return retList;
            }

            //excluded categories is used to track previously used generic equipment
            //to prevent the redundant use of items when there is overlap between
            //the three equipment types, to pull out generics falling lower in the list
            List<int> excludedCategories = new List<int>();
            foreach (ITieredEquipment equip in equipment)
            {

                if (equip is DetailedEquipment)
                {
                    DetailedEquipment de = equip as DetailedEquipment;
                    retList.Add(de);
                    foreach (CategoryLink cl in de.categoryAdds)
                    {
                        excludedCategories.Add(cl.CategoryID);
                    }
                }
                else if (equip is GenericEquipmentReplacement)
                {
                    GenericEquipmentReplacement gme = equip as GenericEquipmentReplacement;
                    if (gme.IsIncluded(generics) && !gme.IsWhollyContained(excludedCategories))
                    {
                        retList.Add(gme);
                        foreach (CategoryLink category in gme.IncludesCategories)
                        {
                            excludedCategories.Add(category.CategoryID);
                        }
                    }

                }
                if (equip is GenericEquipment)
                {
                    GenericEquipment ge = equip as GenericEquipment;

                    if (!excludedCategories.Contains(ge.Category.CategoryID))
                    {
                        retList.Add(ge);
                    }
                }
            }
            
            Log.DebugFormat("Returning equipment list {0}", PrintEquipmentList(retList));

            if (equipment.Count != retList.Count )
            {
                Log.InfoFormat("The input list had count {0} but the output list had count {1}. Some equipment has been filtered out.", 
                    equipment.Count, retList.Count);
            }

            return retList;
        }

        //excludes power sunroof, power steering, aux power outlet - because these don't really fit the description of "power everything"
        //Should we include things like auto/on auto/off headlights that are convenience items which MAY be considered "power" by a user?
        //others are remote trunk release, keyless entry, wireless phone, etc
        private List<int> powerGenerics = new List<int>(new int[] { 1063, 1065, 1074, 1075, 1126, 1153 });

        private List<ITieredEquipment> sortedEquipmentList;
        public List<ITieredEquipment> SortedEquipmentList
        {
            get
            {
                if (sortedEquipmentList == null)
                {
                    ProcessOptions();

                    sortedEquipmentList = new List<ITieredEquipment>();

                    // We're adding to the list in order of importance here.
                    sortedEquipmentList.AddRange(options);
                    
                    EnsureMsrp(sortedEquipmentList, _chromeStyleId);
                    OrderByMsrp(sortedEquipmentList);

                    sortedEquipmentList.AddRange(generics);
                    sortedEquipmentList.AddRange(replacements);

                    //// Perform tier / type sorting.
                    //sortedEquipmentList.Sort(new DisplayEquipmentOrderer());

                    // Remove redundancies between options and generics.
                    sortedEquipmentList = FilterOverridenGenerics(sortedEquipmentList);
                }

                Log.InfoFormat("SortedEquipmentList contains {0}", PrintEquipmentList(sortedEquipmentList));

                return sortedEquipmentList;
            }
        }

        /// <summary>
        /// Just wrap the sort function so keep it clean
        /// </summary>
        /// <param name="detailedEquipmentCollection"></param>
        private void OrderByMsrp(List<ITieredEquipment> detailedEquipmentCollection)
        {
            Log.InfoFormat("Un-sorted options: {0}", detailedEquipmentCollection.ToJson());
            detailedEquipmentCollection.Sort(new DetailedEquipmentPriceComparer());
            Log.InfoFormat("Sorted options: {0}", detailedEquipmentCollection.ToJson());
        }

        /// <summary>
        /// Add the msrp to each item in the detailedEquipmentCollection. This is an in-place modification of the collection.
        /// Do not try if everything already has a price.
        /// We can't do this without a chrome style id, so just bail if that is the case. 
        /// </summary>
        /// <param name="detailedEquipmentCollection"></param>
        /// <param name="chromeStyleId"></param>
        private void EnsureMsrp(List<ITieredEquipment> detailedEquipmentCollection, int chromeStyleId)
        {
            if (!detailedEquipmentCollection.Any(equipment => ((DetailedEquipment)equipment).Msrp < 0) || chromeStyleId <= 0)
            {
                return;
            }

            var optCodes = detailedEquipmentCollection.Select(equipment => ((DetailedEquipment)equipment).OptionCode);
            Log.InfoFormat("EnsureMsrp: styleid: {0}, option codes: {1}", chromeStyleId, optCodes.ToJson());
            var msrpConditions = ChromeLogicEngine.GetBestOptionsByMsrp(ChromeMsrpConditionHelpers.GetMsrpConditions(chromeStyleId, optCodes));
            Log.InfoFormat("EnsureMsrp: best options: {0}", msrpConditions.ToJson());

            // Set the MSRP for the detailed equipment
            var chromeMsrpConditions = msrpConditions.ToArray();
            
            detailedEquipmentCollection.ForEach(deq =>
                {
                    if (chromeMsrpConditions.Any(mc => mc.OptionCode == ((DetailedEquipment)deq).OptionCode))
                        ((DetailedEquipment)deq).Msrp = chromeMsrpConditions.First(mc => mc.OptionCode == ((DetailedEquipment)deq).OptionCode).Msrp;
                });
            Log.InfoFormat("DetailedEquipmentList with msrp: {0}", detailedEquipmentCollection.ToJson());
        }

        /// <summary>
        /// This method applies the option package rules like text replacement rules to the option packages
        /// e.g.,  remove the 1 year subscription from description for used cars only.
        /// </summary>
        private void ProcessOptions()
        {
            if (options == null || this.options.Count == 0) return;

            if (_inventoryData == null) return;
            if (_optionRulesEngine == null) _optionRulesEngine = new OptionPackageRulesEngine(Registry.Resolve<IOptionPackageRulesRepository>());

            _optionRulesEngine.Process(_inventoryData.BusinessUnitid, _inventoryData.InventoryId, options);
        }



        private static string PrintEquipmentList(List<ITieredEquipment> list)
        {
            if( list == null )
            {
                return "The list is null.";
            }
            if (list.Count == 0)
            {
                return "The list is empty";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("List contents: ");

            foreach( var equipment in list)
            {
                sb.Append(PrintEquipment(equipment));
            }

            return sb.ToString();
        }

        private static string PrintEquipment(ITieredEquipment equipment)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nITieredEquipment: ");
            sb.AppendFormat("n\tDescription: '{0}'", equipment.GetDescription());
            sb.AppendFormat("\n\tDisplayPriority: '{0}'", equipment.GetDisplayPriority());
            sb.AppendFormat("\n\tSummary: '{0}'", equipment.GetSummary());
            sb.AppendFormat("\n\tTier: '{0}'", equipment.GetTier());
            return sb.ToString();
        }

        public bool HasHighlightedPackages()
        {
            foreach (ITieredEquipment equip in SortedEquipmentList)
            {
                // This IsAvailable is a bad idea.  If the generic got registered first, it will "block" the package from display.
                // This happens frequently, as it's based on the order of the random permutation of TemplateItems.
                if (equip is DetailedEquipment /* && IsAvailableForDisplay(equip) */ ) 
                {
                    return true;
                }
            }
            return false;
        }

        private List<string> highlightedPackages;
        public List<string> GetHighlightedPackages()
        {
            Log.Debug("GetHighlightedPackages() called.");

            if (highlightedPackages == null || highlightedPackages.Count == 0)
            {
                Log.Debug("Getting equipment from SortedEquipmentList.");

                List<string> retEquip = new List<string>();
                foreach (ITieredEquipment equip in SortedEquipmentList)
                {
                    if (equip is DetailedEquipment && TryToUseEquipment(equip))
                    {
                        var deq = equip as DetailedEquipment;
                        var description = string.Format("{0}", deq.GetDescription()); //, deq.Msrp.ToString("C"));
                        retEquip.Add(description);
                        Log.DebugFormat("Added equipment description '{0}'", description);
                    }
                }
                highlightedPackages = retEquip;
            }

            Log.DebugFormat("Returning the following equipment packages: {0}", highlightedPackages.ToDelimitedString(","));
            return highlightedPackages;        
        }

        // Option Package names
        public List<string> GetHighlightedPackgeSummaries()
        {
            if (highlightedPackages == null || highlightedPackages.Count == 0)
            {
                List<string> retEquip = (from equip in SortedEquipmentList
                                         where (equip is DetailedEquipment) && TryToUseEquipment(equip)
                                         let summary = equip.GetSummary()
                                         where !string.IsNullOrEmpty(summary)
                                         select equip.GetSummary()).ToList();

                highlightedPackages = retEquip;
            }
            return highlightedPackages;                    
        }


        private Dictionary<int, List<ITieredEquipment>> tieredEquipmentLookup;

        public int GetCountInTier(int tierNumber)
        {

            return SortedEquipmentList.FindAll(delegate(ITieredEquipment equip)
                                            {
                                                return equip.GetTier() == tierNumber;
                                            }).Count;

        }
        public bool HasEquipmentInTierRemaining(int tierNumber)
        {
            return HasEquipmentInTierRemaining(tierNumber, 1);
        }

        /// <summary>
        /// Misleading name.  Actually tests whether the number of equipment items in the specified
        /// tier exceeds the minCountRequired.
        /// </summary>
        /// <param name="tierNumber"></param>
        /// <param name="minCountRequired"></param>
        /// <returns></returns>
        public bool HasEquipmentInTierRemaining(int tierNumber, int minCountRequired)
        {
            Log.DebugFormat("HasEquipmentInTierRemaining() called with tier {0} and minCount {1}",
                tierNumber, minCountRequired);

            bool retVal = false;

            /*int ct = 0;
            foreach(ITieredEquipment equip in SortedEquipmentList)
            {
                //equipment qualifies for tier if it has a lower or quivalent tier number
                if (equip.GetTier() <= tierNumber && IsAvailableForDisplay(equip))
                {
                    ct++;                    
                }
                if (ct >= minCountRequired) return true;
            }*/
            if( tieredEquipmentLookup == null )
            {
                Log.Debug("tieredEquipmentLookupis null");                
            }
            else if (!tieredEquipmentLookup.ContainsKey(tierNumber) )
            {
                Log.DebugFormat("tieredEquipmentLookup does not contain key {0}", tierNumber);
            }

            if (tieredEquipmentLookup != null && tieredEquipmentLookup.ContainsKey(tierNumber))
            {
                int count = tieredEquipmentLookup[tierNumber].Count;
                retVal = count >= minCountRequired;

                Log.DebugFormat("tieredEquipmentLookup[{0}].Count is {1}", tierNumber, count);
            }
            Log.DebugFormat("Returning {0}", retVal);
            return retVal;

        }
        public bool IsAvailableForDisplay(ITieredEquipment equip)
        {
            Log.DebugFormat("IsAvailableForDisplay() called with ITieredEquipment {0}",
                StandardObjectDumper.Write(equip,1));

            // Testing
            return true;

            bool retVal = false;

            if (equip is GenericEquipment)
            {
                retVal = !IsGenericUsed(((GenericEquipment)equip).Category.CategoryID);
            }
            else if (equip is DetailedEquipment)
            {
                foreach (CategoryLink category in ((DetailedEquipment)equip).categoryAdds)
                {
                    if (IsGenericUsed(category.CategoryID))
                    {
                        Log.DebugFormat("Generic category {0} is already used.", category.CategoryID);
                        retVal = false;
                        break;
                    }
                }
            }
            else if (equip is GenericEquipmentReplacement)
            {
                //we don't use replacement equipment we've already taken a part of
                CategoryCollection includedCategories = ((GenericEquipmentReplacement)equip).IncludesCategories;
                foreach (CategoryLink category in includedCategories)
                {
                    if (IsGenericUsed(category.CategoryID))
                    {
                        Log.DebugFormat("Generic category {0} is already used.", category.CategoryID);
                        retVal = false;
                        break;
                    }
                }
            }
            else
            {
                retVal = true;
            }
            Log.DebugFormat("Returning {0}", retVal);
            return retVal;
        }

        private void ShiftTiersForPreviewEquipment(List<int> categoryIdsUsedInPreview)
        {
            foreach (int categoryId in categoryIdsUsedInPreview)
            {
                foreach (ITieredEquipment equip in GetListByTier(1))
                {
                    TryToUseEquipment(equip);
                }
            }
        }

        //pass in the list of ids already used in the preview for most searched terms, so that we don't duplicate them
        public List<PreviewEquipmentItem> GetPreviewList(List<int> categoryIdsUsedInPreview, bool previewedEquipmentReusable, int maxCount)
        {
            List<PreviewEquipmentItem> previewableTier1Descriptions = new List<PreviewEquipmentItem>();

            //log the generics used in the most searched terms
            //process cached tier lists to remove any used in preview
            ResetTieredEquipmentLookup(categoryIdsUsedInPreview);

            if (maxCount <= 0)
            {
                return previewableTier1Descriptions;
            }
            //first get the legit tier1 list
            List<ITieredEquipment> tier1List = GetListByTier(1);
            int ct = Math.Min(tier1List.Count, maxCount);
            for (int i = 0; i < ct; i++)
            {
                if ( tier1List[i] is GenericEquipment )
                {
                    var equip = tier1List[i] as GenericEquipment;
                    if ( equip == null ) continue;
                    if ( !categoryIdsUsedInPreview.Contains( equip.Category.CategoryID ) )
                        previewableTier1Descriptions.Add( new PreviewGenericEquipmentItem( tier1List[i].GetDescription(), equip.Category.CategoryID  ) );
                }
                else if ( tier1List[i] is GenericEquipmentReplacement )
                {
                    var replacement = tier1List[i] as GenericEquipmentReplacement;
                    if ( replacement == null ) continue;
                    previewableTier1Descriptions.Add( new PreviewGenericEquipmentItem( replacement.Description, replacement.IncludesGenericEquipmentIds ) );
                }
                else
                {
                    previewableTier1Descriptions.Add( new PreviewEquipmentItem( tier1List[i].GetDescription() ) );
                }
            }

/*
            RemoveFromTier(1, ct);
            BubbleEquipmentIntoTier(1, ct);
*/

            return previewableTier1Descriptions;

            /*
            int ct = 0;
            //now loop over and see if we can use them...add any that work
            foreach (ITieredEquipment equip in tier1List)
            {            
                if (ct >= maxCount)
                {
                    break;
                }
                if(IsAvailableForDisplay(equip))
                {
                    //try to use equipment prior to adding description - this
                    //makes sure we log the "used generics" properly
                    if (TryToUseEquipment(equip,previewedEquipmentReusable))
                    {
                        previewableTier1Descriptions.Add(equip.GetDescription());
                        ct++;
                    }
                }
            }

            return previewableTier1Descriptions;*/
        }

        /// <summary>
        /// Called to initialize the display equipment collection with the dealer's preferences
        /// </summary>
        /// <param name="maximumOptionsByTier"></param>
        public void SetDisplayPreferences(int[] maximumOptionsByTier)
        {
            if (maximumOptionsByTier != null)
            {
                PrepTieredEquipmentLookup(maximumOptionsByTier);
            }
        }

        /// <summary>
        /// Default setting used if unprepped
        /// </summary>
        private void ResetTieredEquipmentLookup(IEnumerable<int> previouslyUsedGenerics)
        {
            _usedGenerics = new List<int>(previouslyUsedGenerics);
            tieredEquipmentLookup = new Dictionary<int, List<ITieredEquipment>>();

            // Set tier counts to 0
            var thisTierCount = new Dictionary<int, int>();
            for (int i = 0; i < 10; i++ )
            {
                thisTierCount.Add(i, 0);                
            }

            // NOTE: We ignore tier 4, 5, etc ... because maximums are not specified for those tiers.
            for (int tierNumber = 0; tierNumber < MaxOptionsByTier.Length; tierNumber++)
            {
                tieredEquipmentLookup[tierNumber] = new List<ITieredEquipment>();
                int currCt = 0;

                int maxForThisTier = MaxOptionsByTier[tierNumber];

                Log.Debug("Procesing SortedEquipmentList.");

                foreach (ITieredEquipment equip in SortedEquipmentList)
                {
                    // Ignore option packages.
                    if( equip is DetailedEquipment)
                    {
                        continue;
                    }

                    //we don't use blank ones...
                    string desc = equip.GetDescription();
                    if (string.IsNullOrEmpty(desc))
                    {
                        continue;
                    }

                    // Sort equipment by tier.

                    // this used to be <=
                    if (equip.GetTier() == tierNumber)
                    {
                        // formerly currCt
                        if (thisTierCount[tierNumber] >= maxForThisTier)
                        {
                            Log.DebugFormat("countByTier[{0}] >= MaxOptionsByTier[{0}] == {1}. Skipping equipment item {2}.",
                                tierNumber, MaxOptionsByTier[tierNumber], PrintEquipment(equip));

                            // break;
                        }
                        else if(TryToUseEquipment(equip))
                        {
                            tieredEquipmentLookup[tierNumber].Add(equip);
                            //once the current equipment count reaches the maxTierCount, break the loop
                            //currCt++;
                            thisTierCount[tierNumber] += 1;

                            Log.DebugFormat("Using tier {0} equipment {1}", tierNumber, PrintEquipment(equip));
                            Log.DebugFormat("Tier {0} count is now {1}", tierNumber, thisTierCount[tierNumber]);
                        }
                    }
                }
            }

        }

        private int[] maxOptionsByTier;
        protected int[] MaxOptionsByTier
        {
            get
            {
                if (maxOptionsByTier == null)
                {
                    if (_dealerAdvertisementPreferences != null)
                    {
                        maxOptionsByTier = _dealerAdvertisementPreferences.MaxOptionsByTier;
                        Log.DebugFormat("Using max options per tier from preference: {0}",
                            maxOptionsByTier.ToDelimitedString(","));
                    }
                    else
                    {
                        maxOptionsByTier = DealerAdvertisementPreferences.DefaultMaxOptionsByTier();
                        Log.DebugFormat("Using system defaults max options per tier: {0}",
                            maxOptionsByTier.ToDelimitedString(","));
                    }
                }
                Log.DebugFormat("Using max options per tier: {0}", maxOptionsByTier.ToDelimitedString(","));
                return maxOptionsByTier;
            }
            set
            {
                maxOptionsByTier = value;
            }
        }
        private void PrepTieredEquipmentLookup(int[] maximumOptionsByTier)
        {
            MaxOptionsByTier = maximumOptionsByTier;
            ResetTieredEquipmentLookup(new List<int>());
        }

        public List<ITieredEquipment> GetListByTier(int tierNumber)
        {
            Log.DebugFormat("GetListByTier() called with tier {0}", tierNumber);
            if (tieredEquipmentLookup.Count <= 0)
            {
                Log.Debug("Resetting tiered equipment lookup.");
                ResetTieredEquipmentLookup(new List<int>());
            }

            var list = new List<ITieredEquipment>();

            //tiered equipment caching mechanism b/c the formula for pulling them together breaks once you render them...this prevents that state issue
            if (tieredEquipmentLookup.ContainsKey(tierNumber))
            {
                list = tieredEquipmentLookup[tierNumber];
            }

            Log.DebugFormat("Returing tier {0} list: {1}", tierNumber, PrintEquipmentList(list));
            return list;
        }
        private void UseEquipment(ITieredEquipment equip)
        {
            Log.DebugFormat("UseEquipment() called with equipment {0}", PrintEquipment(equip));
            if (equip is GenericEquipment)
            {
                SetGenericAsUsed(((GenericEquipment)equip).Category.CategoryID);

                // NOTE: So we don't actually record the equipment?  Just the categoryID?
            }
            else if (equip is DetailedEquipment)
            {
                //we made it...this one will get used
                DetailedEquipment deq = equip as DetailedEquipment;
                foreach (CategoryLink cl in deq.categoryAdds)
                {
                    SetGenericAsUsed(cl.CategoryID);
                }
            }
            else if (equip is GenericEquipmentReplacement)
            {
                //if it is about to be added...designate the generics as used
                SetGenericsAsUsed(((GenericEquipmentReplacement)equip).IncludesGenericEquipmentIds);
            }
        }
/*
        private bool TryToUseEquipment(ITieredEquipment equip)
        {
            return TryToUseEquipment(equip, false);
        }
*/
        private bool TryToUseEquipment(ITieredEquipment equip/*, bool isReusable */)
        {
            Log.DebugFormat("TryToUseEquipment() called with equipment {0}", PrintEquipment(equip));

            // Using all equipment.
            UseEquipment(equip);
            Log.DebugFormat("Using equipment {0}.", PrintEquipment(equip));
            return true;
        }

        public List<string> GetEquipmentDescriptions(int tierNumber)
        {

            List<string> descriptions = new List<string>();

            List<ITieredEquipment> equipment = GetListByTier(tierNumber);

            foreach (ITieredEquipment equip in equipment)
            {
                descriptions.Add(equip.GetDescription());
            }
            return descriptions;
        }



        private List<int> countByTier;
        public List<int> CountByTier
        {
            get
            {
                if (countByTier == null)
                {
                    countByTier = new List<int>();

                    int maxTier = 0;
                    foreach (ITieredEquipment equipment in SortedEquipmentList)
                    {
                        maxTier = Math.Max(equipment.GetTier(), maxTier);
                    }

                    //if maxTier less than 4, make it 4
                    maxTier = Math.Max(4, maxTier);

                    //make sure to add one for zero (which we'll ignore mostly)
                    for (int i = 0; i <= maxTier; i++)
                    {
                        countByTier.Add(0);
                    }

                    foreach (ITieredEquipment equipment in SortedEquipmentList)
                    {
                        int tier = equipment.GetTier();
                        if (countByTier.Count > tier && !string.IsNullOrEmpty(equipment.GetDescription()))
                        {
                            countByTier[tier]++;
                        }
                    }
                }
                return countByTier;
            }
        }

        public void FilterAndReorder(int businessUnitId)
        {

            //now it is sorted for the filtering algorithm, let's filter it
            generics.FilterHeadersAndExclusiveCategories();
            generics.Sort();
        }

        public List<CategoryCollection> GetCategoryCollections()
        {
            return generics.GetCategoryCollections();
        }


        #endregion

        #region headerIdLists

        private List<int> luxuryOptionsHeaderIdList = new List<int>(new int[] { 10, 22, 23, 28, 32, 43, 44 });
        private List<int> valuedOptionsHeaderIdList = new List<int>(new int[] { 10, 13, 15, 21, 22, 23, 26, 28, 4, 32, 33, 38, 39, 42, 43, 44, 46 });
        private List<int> safetyOptionsHeaderIdList = new List<int>(new int[] { 4, 6, 7, 38, 39, 42 });
        private List<int> convenienceOptionsHeaderIdList = new List<int>(new int[] { 23, 26, 27, 28, 29, 21 });
        private List<int> sportyOptionsHeaderIdList = new List<int>(new int[] { 4, 6, 7, 10, 13, 0, 15, 17, 20, 32, 33, 34, 35, 37, 46 });
        private List<int> truckOptionsHeaderIdList = new List<int>(new int[] { 8, 13, 15, 17, 20, 29, 30, 31, 32, 33, 37, 46 });
        private List<int> greenOptionsHeaderIdList = new List<int>(new int[] { -1 }); //add me
        #endregion

        private List<string> valuedOptions;

        public bool HasValuedOptions
        {
            get
            {
                return HasOptionDescriptionListAvailable(valuedOptionsHeaderIdList);
            }
        }
        public List<string> ValuedOptions
        {
            get
            {
                if (valuedOptions == null)
                {
                    valuedOptions = GetOptionDescriptionList(valuedOptionsHeaderIdList);
                }
                return valuedOptions;
            }
        }

        private List<string> luxuryOptions;

        public bool HasLuxuryOptions
        {
            get
            {
                return HasOptionDescriptionListAvailable(luxuryOptionsHeaderIdList);
            }
        }
        public List<string> LuxuryOptions
        {
            get
            {
                if (luxuryOptions == null)
                {
                    luxuryOptions = GetOptionDescriptionList(luxuryOptionsHeaderIdList);
                }
                return luxuryOptions;

            }
        }

        private List<string> safetyOptions;
        public bool HasSafetyOptions
        {
            get
            {
                return HasOptionDescriptionListAvailable(safetyOptionsHeaderIdList);
            }
        }
        public List<string> SafetyOptions
        {
            get
            {
                if (safetyOptions == null)
                {
                    safetyOptions = GetOptionDescriptionList(safetyOptionsHeaderIdList);
                }
                return safetyOptions;
            }
        }

        private List<string> convenienceOptions;
        public bool HasConvenienceOptions
        {
            get
            {
                return HasOptionDescriptionListAvailable(convenienceOptionsHeaderIdList);
            }
        }
        public List<string> ConvenienceOptions
        {
            get
            {
                if (convenienceOptions == null)
                {
                    convenienceOptions = GetOptionDescriptionList(convenienceOptionsHeaderIdList);
                }
                return convenienceOptions;

            }
        }

        private List<string> sportyOptions;
        public bool HasSportyOptions
        {
            get
            {
                return HasOptionDescriptionListAvailable(sportyOptionsHeaderIdList);
            }
        }
        public List<string> SportyOptions
        {
            get
            {
                if (sportyOptions == null)
                {
                    sportyOptions = GetOptionDescriptionList(sportyOptionsHeaderIdList);
                }
                return sportyOptions;
            }
        }

        private List<string> truckOptions;
        public bool HasTruckOptions
        {
            get
            {
                return HasOptionDescriptionListAvailable(truckOptionsHeaderIdList);
            }
        }
        public List<string> TruckOptions
        {
            get
            {
                if (truckOptions == null)
                {
                    truckOptions = GetOptionDescriptionList(truckOptionsHeaderIdList);
                }
                return truckOptions;
            }
        }

        private List<string> GetOptionDescriptionList(List<int> headerIdList)
        {
            var equip = generics.GetOptionsFromHeaderList(
                    headerIdList, _usedGenerics);
            SetUsedGenericIds(equip);
            var filteredEquip = equip.RemoveDuplicates(replacements);
            return GetDescriptions(filteredEquip);
        }

        private static List<string> GetDescriptions(IEnumerable<ITieredEquipment> tieredEquipments)
        {
            return tieredEquipments.Select(equip => equip.GetDescription()).ToList();
      
        }

        private bool HasOptionDescriptionListAvailable(List<int> headerIdList)
        {
            return generics.GetOptionsFromHeaderList(
                    headerIdList, _usedGenerics).Count > 0;
        }
        private void SetUsedGenericIds(GenericEquipmentCollection equip)
        {
            foreach (GenericEquipment ge in equip)
            {
                SetGenericAsUsed(ge.Category.CategoryID);
            }
        }


        public void LoadViewState(object state)
        {
            if (state is System.Web.UI.Pair)
            {
                System.Web.UI.Pair pair = state as System.Web.UI.Pair;
                generics.LoadViewState(pair.First);
                options.LoadViewState(pair.Second);
            }
            else
            {
                generics = new GenericEquipmentCollection();
                options = new DetailedEquipmentCollection();
            }

        }

        public object SaveViewState()
        {
            System.Web.UI.Pair pair = new System.Web.UI.Pair();

            pair.First = generics.SaveViewState();
            pair.Second = options.SaveViewState();
            return pair;
        }
    }


    internal class DetailedEquipmentPriceComparer : IComparer<ITieredEquipment>
    {
        #region IComparer Members

        public int Compare(ITieredEquipment x, ITieredEquipment y)
        {
            if (x is DetailedEquipment && y is DetailedEquipment)
            {
                // we want descending order by msrp
                return ((DetailedEquipment) y).Msrp.CompareTo(((DetailedEquipment) x).Msrp);
            }

            return 0;
        }

        #endregion IComparer Members
    }

    //order by tier, then displaypriority
    public class DisplayEquipmentOrderer : IComparer<ITieredEquipment>
    {
        #region IComparer Members
        private int GetTypeValue(ITieredEquipment x)
        {
            if (x is DetailedEquipment)
            {
                return 0;
            }
            if (x is GenericEquipmentReplacement)
            {
                return 1;
            }

            //Generic Equip and future types
            return 2;
        }

        public int Compare(ITieredEquipment xo, ITieredEquipment yo)
        {
            //Take Detailed Equip first
            //--in Detailed, take top tier, then display priority
            int xType = GetTypeValue(xo);
            int yType = GetTypeValue(yo);

            int val = xType.CompareTo(yType);
            var detailedComparer = new DetailedEquipmentPriceComparer();
            if (val == 0)
            {
                //take higher tier
                ITieredEquipment t0 = xo;
                ITieredEquipment t1 = yo;
                if (t0 == null || t1 == null) throw new ApplicationException("Can only compare tiered equipment");

                val = t0.GetTier().CompareTo(t1.GetTier());

                if (val == 0)
                {
                    val = t0.GetDisplayPriority().CompareTo(t1.GetDisplayPriority());

                    if (val == 0)
                    {
                        if (t0 is GenericEquipmentReplacement)
                        {
                            GenericEquipmentReplacement ger0 = t0 as GenericEquipmentReplacement;
                            GenericEquipmentReplacement ger1 = t1 as GenericEquipmentReplacement;

                            //if both are not null, compare the count of items
                            //swap order b/c higher is first
                            if (ger1 != null)
                            {
                                val = ger1.IncludesCategories.Count.CompareTo(ger0.IncludesCategories.Count);
                            }
                        }
                        else if (t0 is DetailedEquipment)
                        {
                            val = detailedComparer.Compare(t0, t1);
                        }
                        else if (t0 is GenericEquipment)
                        {

                        }
                    }
                }
            }
            return val;
        }

        #endregion
    }
}