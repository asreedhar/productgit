﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using MAX.Entities.Filters;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public static class Extensions
    {
         public static IEnumerable<IInventoryData> FilterBy(this IEnumerable<IInventoryData> inventory,
             UsedOrNewFilter newOrUsed)
         {
             return newOrUsed == UsedOrNewFilter.New || newOrUsed == UsedOrNewFilter.Used
                        ? inventory.Where(inv => inv.InventoryType == (int) newOrUsed)
                        : inventory;
         }
    }
}