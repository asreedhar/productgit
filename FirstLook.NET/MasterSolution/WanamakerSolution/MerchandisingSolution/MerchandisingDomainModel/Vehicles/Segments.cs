namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum Segments
    {
        
        Unknown = 1,
        Truck,
        Sedan,
        Coupe,
        Van,
        SUV,
        Convertible,
        Wagon

    }
}
