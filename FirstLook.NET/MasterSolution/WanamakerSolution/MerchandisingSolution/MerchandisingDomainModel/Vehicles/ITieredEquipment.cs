using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface ITieredEquipment : IComparable
    {        
        string GetDescription();
        int GetTier();
        int GetDisplayPriority();
        void SetTierAndPriority(GenericEquipmentCollection generics);

        /// <summary>
        /// Allow clients to signal that they want to prioritize this equipment.  This should cause it to be used more often.
        /// </summary>
        void Prioritize();

        // Useful for preview.
        string GetSummary();

    }
}
