namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class GasMileage
    {
        public int City { get; set; }
        public int Highway { get; set; }
    }
}