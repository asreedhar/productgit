using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class GenericEquipmentReplacementCollection : CollectionBase, IEnumerable<ITieredEquipment>
    {
      
        public GenericEquipmentReplacementCollection()
        {

        }
        public static GenericEquipmentReplacementCollection Fetch(int businessUnitId, GenericEquipmentCollection generics)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbTransaction trans = con.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "settings.CategoryReplacements#Fetch";
                        cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                        
                        DataSet ds = new DataSet();
                        var dataReader = cmd.ExecuteReader();

                        // Using DataTable.Load rather than DataAdapter.Fill as it can be profiled
                        int i = 0;
                        while (!dataReader.IsClosed)
                        {
                            // Create a table
                            var table = ds.Tables.Add("table" + i++);

                            // Load the results into the table. DataTable.Load also auto-moves onto the next result set
                            table.Load(dataReader, LoadOption.OverwriteChanges);
                        }

                        if (ds.Tables.Count < 2)
                        {
                            throw new ApplicationException("Result set must contain two sets");
                        }

                        DataTable replacementTbl = ds.Tables[0];
                        DataTable replacementItemTbl = ds.Tables[1];

                        ds.Relations.Add("replacement",replacementTbl.Columns["replacementId"], replacementItemTbl.Columns["replacementId"]);

                        GenericEquipmentReplacementCollection gerc = new GenericEquipmentReplacementCollection();

                        foreach (DataRow dr in replacementTbl.Rows)
                        {
                            //List<int> included = new List<int>();
                            CategoryCollection included = new CategoryCollection("included");
                            foreach (DataRow row in dr.GetChildRows("replacement"))
                            {
                                CategoryLink cl = new CategoryLink
                                                      {
                                                          CategoryID = (int) row["categoryId"],
                                                          Description = (string) row["userFriendlyName"]
                                                      };
                                included.Add(cl);
                            }
                            List<string> descriptions = new List<string>(new[] { (string)dr["name"] });
                            GenericEquipmentReplacement ger = new GenericEquipmentReplacement((int)dr["replacementId"], 
                                    included, 
                                    new CategoryCollection("excluded"),
                                    descriptions, (GenericEquipmentReplacementType)dr["replacementType"]);
                            gerc.Add(ger);
                        }
                        gerc.SetTierAndDisplayPriority(generics);
                        return gerc;
                    }
                }
            }
            
        }
        public GenericEquipmentReplacementCollection(IDataReader reader, GenericEquipmentCollection generics)
        {
            while (reader.Read())
            {
                Add(new GenericEquipmentReplacement());
            }

            SetTierAndDisplayPriority(generics);
        }

        public void DeleteReplacement(int id, int businessUnitId, string memberLogin)
        {
            GenericEquipmentReplacement.Delete(id, businessUnitId, memberLogin);
        }

        /// <summary>
        /// check any genericMetaEquipment...if they are now included, add them/remove as necessary
        /// </summary>
        /// <param name="generics">The currently included generic equipment</param>
        public void SetTierAndDisplayPriority(GenericEquipmentCollection generics)
        {
            foreach (GenericEquipmentReplacement de in List)
            {
                de.SetTierAndPriority(generics);
            }
        }
        public int Add(GenericEquipmentReplacement equip)
        {
            return List.Add(equip);                    
        }

        
        public GenericEquipmentReplacement Item(int index)
        {
            return (GenericEquipmentReplacement) List[index];
        }


        public void Remove(GenericEquipmentReplacement item)
        {
            List.Remove(item);
        }
        
        public new IEnumerator<ITieredEquipment> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return Item(i);                
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        public List<GenericEquipmentReplacement> FindByGenericId(int categoryId)
        {
            return ((List<GenericEquipmentReplacement>)List).FindAll( equip => equip.Contains(categoryId) );
        }


        public void SortByCategoryIdCountDesc()
        {
            InnerList.Sort(new GenericEquipmentReplacementComparer());
        }
    }
}
