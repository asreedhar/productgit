﻿namespace FirstLook.Merchandising.DomainModel.Vehicles
{
	public class VehicleUpdateCertificationResult
	{
		public bool ManufacturerCertified { get; set; }
	}
}