﻿using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Vehicles.ManufacturerSpecificVehicleData
{
    public abstract class ManufacturerSpecificVehicleData : IVehicleTemplatingData
    {
        private readonly IVehicleTemplatingData _vehicleTemplatingData = null;

        protected ManufacturerSpecificVehicleData(IVehicleTemplatingData vehicleTemplatingData)
        {
            _vehicleTemplatingData = vehicleTemplatingData;
        }

        public new string Trim
        {
            get { return _vehicleTemplatingData.Trim; }
        }

        public string HighInternetPrice
        {
            get { return _vehicleTemplatingData.HighInternetPrice; }
        }

        public bool IsPriceReduced
        {
            get { return _vehicleTemplatingData.IsPriceReduced; }
        }

        public bool IsPriceReducedEnough
        {
            get { return _vehicleTemplatingData.IsPriceReducedEnough; }
        }

        public bool HasDisplayablePrice
        {
            get { return _vehicleTemplatingData.HasDisplayablePrice; }
        }

        public string ListPrice
        {
            get { return _vehicleTemplatingData.ListPrice; }
        }

        public bool HasGoodPriceBelowMSRP
        {
            get { return _vehicleTemplatingData.HasGoodPriceBelowMSRP; }
        }

        public string OriginalMsrp
        {
            get { return _vehicleTemplatingData.OriginalMsrp; }
        }

        public bool HasFinancingAvailable
        {
            get { return _vehicleTemplatingData.HasFinancingAvailable; }
        }

        public string SpecialFinancing
        {
            get { return _vehicleTemplatingData.SpecialFinancing; }
        }

        public decimal PreferredBookValue
        {
            get { return _vehicleTemplatingData.PreferredBookValue; }
        }

        public bool HasGoodPriceBelowBook
        {
            get { return _vehicleTemplatingData.HasGoodPriceBelowBook; }
        }

        public BookoutCollection Bookouts
        {
            get { return _vehicleTemplatingData.Bookouts; }
        }

        public string PreferredBook
        {
            get { return _vehicleTemplatingData.PreferredBook; }
        }

        public string TargetPriceDisplay
        {
            get { return _vehicleTemplatingData.TargetPriceDisplay; }
        }

        public decimal TargetPrice
        {
            get { return _vehicleTemplatingData.TargetPrice; }
        }

        public int CountBelowBook
        {
            get { return _vehicleTemplatingData.CountBelowBook; }
        }

        public int CountInPriceRange
        {
            get { return _vehicleTemplatingData.CountInPriceRange; }
        }

        public int CountBelowTwentyThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowTwentyThousandDollars; }
        }

        public int CountBelowFifteenThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowFifteenThousandDollars; }
        }

        public int CountBelowTenThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowTenThousandDollars; }
        }

        public int CountBelowNineThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowNineThousandDollars; }
        }

        public int CountBelowEightThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowEightThousandDollars; }
        }

        public int CountBelowSevenThousandDollars
        {
            get { return _vehicleTemplatingData.CountBelowSevenThousandDollars; }
        }

        public string DisclaimerDate
        {
            get { return _vehicleTemplatingData.DisclaimerDate; }
        }

        public VehicleAttributes VehicleAttributes
        {
            get
            {
                return _vehicleTemplatingData.VehicleAttributes;
            }
            set
            {
                _vehicleTemplatingData.VehicleAttributes = value;
            }
        }

        public VehicleConfiguration VehicleConfig
        {
            get
            {
                return _vehicleTemplatingData.VehicleConfig;
            }
            set
            {
                _vehicleTemplatingData.VehicleConfig = value;
            }
        }

        public IInventoryData InventoryItem
        {
            get
            {
                return _vehicleTemplatingData.InventoryItem;
            }
            set
            {
                _vehicleTemplatingData.InventoryItem = value;
            }
        }

        public Templating.DealerAdvertisementPreferences Preferences
        {
            get
            {
                return _vehicleTemplatingData.Preferences;
            }
            set
            {
                _vehicleTemplatingData.Preferences = value;
            }
        }

        public bool IsLowMilesPerYear
        {
            get { return _vehicleTemplatingData.IsLowMilesPerYear; }
        }

        public bool IsNewVehicle
        {
            get { return _vehicleTemplatingData.IsNewVehicle; }
        }

        public bool HasInteriorType
        {
            get { return _vehicleTemplatingData.HasInteriorType; }
        }

        public string MilesPerYearCategory
        {
            get { return _vehicleTemplatingData.MilesPerYearCategory; }
        }

        public string Make
        {
            get { return _vehicleTemplatingData.Make; }
        }

        public string Model
        {
            get { return _vehicleTemplatingData.Model; }
        }

        public string Year
        {
            get { return _vehicleTemplatingData.Year; }
        }

        public bool HasTrim
        {
            get { return _vehicleTemplatingData.HasTrim; }
        }

        public string Mileage
        {
            get { return _vehicleTemplatingData.Mileage; }
        }

        public bool HasColors
        {
            get { return _vehicleTemplatingData.HasColors; }
        }

        public string InteriorType
        {
            get { return _vehicleTemplatingData.InteriorType; }
        }

        public string Colors
        {
            get { return _vehicleTemplatingData.Colors; }
        }

        public string InteriorColor
        {
            get { throw new System.NotImplementedException(); }
        }

        public string ExteriorColor
        {
            get { throw new System.NotImplementedException(); }
        }

        public string StockNumber
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasCertificationPreview
        {
            get { throw new System.NotImplementedException(); }
        }

        public string CertificationPreview
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasCertificationDetails
        {
            get { throw new System.NotImplementedException(); }
        }

        public string CertificationDetails
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsCertified
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Certified
        {
            get { throw new System.NotImplementedException(); }
        }

        public string CertificationTitle
        {
            get { throw new System.NotImplementedException(); }
        }

        public string CertifiedID
        {
            get { throw new System.NotImplementedException(); }
        }

        public string FuelCity
        {
            get { throw new System.NotImplementedException(); }
        }

        public string FuelHwy
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool GetsGoodGasMileage
        {
            get { throw new System.NotImplementedException(); }
        }

        public string BestGasMileage
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasRoadsideAssistance
        {
            get { throw new System.NotImplementedException(); }
        }

        public string RoadsideAssistance
        {
            get { throw new System.NotImplementedException(); }
        }

        public ConsumerInfo ConsInfo
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public bool HasGoodCrashTestRatings
        {
            get { throw new System.NotImplementedException(); }
        }

        public string DriverFrontCrash
        {
            get { throw new System.NotImplementedException(); }
        }

        public string DriverSideCrash
        {
            get { throw new System.NotImplementedException(); }
        }

        public string RolloverRating
        {
            get { throw new System.NotImplementedException(); }
        }

        public string BasicWarranty
        {
            get { throw new System.NotImplementedException(); }
        }

        public string DrivetrainWarranty
        {
            get { throw new System.NotImplementedException(); }
        }

        public CarfaxInterface CarfaxInfo
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public bool IsOneOwner
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsCleanCarfaxReport
        {
            get { throw new System.NotImplementedException(); }
        }

        public string CleanCarfaxReport
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Carfax1Owner
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasBuybackGuarantee
        {
            get { throw new System.NotImplementedException(); }
        }

        public string BuybackGuarantee
        {
            get { throw new System.NotImplementedException(); }
        }

        public AutoCheckInterface AutoCheckInfo
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsAutoCheckOneOwner
        {
            get { throw new System.NotImplementedException(); }
        }

        public string AutoCheckOneOwner
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsAutoCheckAssured
        {
            get { throw new System.NotImplementedException(); }
        }

        public string AutoCheckAssured
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsCleanAutoCheckReport
        {
            get { throw new System.NotImplementedException(); }
        }

        public string CleanAutoCheckReport
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasGoodAutoCheckScore
        {
            get { throw new System.NotImplementedException(); }
        }

        public string AutoCheckScore
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasHighlightedPackages
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<Templating.Previews.PreviewEquipmentItem> PreviewEquipment
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasMostSearchedEquipmentAvailable
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasTier1EquipmentAvailable
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasEnoughTier1EquipmentAvailable
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasTier2EquipmentAvailable
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasEnoughTier2EquipmentAvailable
        {
            get { throw new System.NotImplementedException(); }
        }

        public string AfterMarketEquipment
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Loaded
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> MajorEquipmentOfInterest
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasValuedOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> ValuedOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasTruckOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> TruckOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasSafetyOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> SafetyOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasConvenienceOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> ConvenienceOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasLuxuryOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> LuxuryOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasSportyOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> SportyOptions
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasThemedEquipment
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> ThemedEquipment
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> HighlightedPackages
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> Tier1Equipment
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> Tier2Equipment
        {
            get { throw new System.NotImplementedException(); }
        }

        public System.Collections.Generic.List<string> Tier3Equipment
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Engine
        {
            get { throw new System.NotImplementedException(); }
        }

        public string EngineOfInterest
        {
            get { throw new System.NotImplementedException(); }
        }

        public string EngineDetails
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Horsepower
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Drivetrain
        {
            get { throw new System.NotImplementedException(); }
        }

        public string DrivetrainOfInterest
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Transmission
        {
            get { throw new System.NotImplementedException(); }
        }

        public string TransmissionOfInterest
        {
            get { throw new System.NotImplementedException(); }
        }

        public string FuelType
        {
            get { throw new System.NotImplementedException(); }
        }

        public string FuelTypeOfInterest
        {
            get { throw new System.NotImplementedException(); }
        }

        public ModelMarketingCollection MarketingCollection
        {
            get { throw new System.NotImplementedException(); }
        }

        public string BestModelAward
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasAwards
        {
            get { throw new System.NotImplementedException(); }
        }

        public string ModelAwards
        {
            get { throw new System.NotImplementedException(); }
        }

        public string MarketingPreview
        {
            get { throw new System.NotImplementedException(); }
        }

        public string MarketingSuperlative
        {
            get { throw new System.NotImplementedException(); }
        }

        public string MarketingSentence
        {
            get { throw new System.NotImplementedException(); }
        }

        public string MarketingSummary
        {
            get { throw new System.NotImplementedException(); }
        }

        public MarketingData.MarketingDataCollection MarketingList
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasMarketingPreviewSuperlative
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasMarketingSuperlative
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasMarketingData
        {
            get { throw new System.NotImplementedException(); }
        }

        public JdPowerRatingCollection JdPowerRatings
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasGoodJdPowerRatings
        {
            get { throw new System.NotImplementedException(); }
        }

        public string GoodJdPowerRatings
        {
            get { throw new System.NotImplementedException(); }
        }

        public string JdPowerRatingPreview
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasConditionHighlights
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasCondition
        {
            get { throw new System.NotImplementedException(); }
        }

        public string ConditionSentence
        {
            get { throw new System.NotImplementedException(); }
        }

        public string ConditionSummary
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasConditionDetails
        {
            get { throw new System.NotImplementedException(); }
        }

        public string PreviewHighlights
        {
            get { throw new System.NotImplementedException(); }
        }

        public decimal ReconditioningValue
        {
            get { throw new System.NotImplementedException(); }
        }

        public string ReconditioningText
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasKeyInformation
        {
            get { throw new System.NotImplementedException(); }
        }

        public string AdditionalInformation
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasMissionPreviewStatment
        {
            get { throw new System.NotImplementedException(); }
        }

        public string MissionPreviewStatement
        {
            get { throw new System.NotImplementedException(); }
        }

        public Templating.Previews.IPreviewPreferences UsedPreviewPreferences
        {
            get { throw new System.NotImplementedException(); }
        }

        public Templating.Previews.IPreviewPreferences NewPreviewPreferences
        {
            get { throw new System.NotImplementedException(); }
        }

        public Templating.Previews.IPreviewPreferences PreviewPrefs
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasAvailableBookout(BookoutCategory category)
        {
            throw new System.NotImplementedException();
        }

        public string PriceBelowBook
        {
            get { throw new System.NotImplementedException(); }
        }

        public string RoughPriceBelowBook
        {
            get { throw new System.NotImplementedException(); }
        }

        public string RoughPriceBelowMSRP
        {
            get { throw new System.NotImplementedException(); }
        }

        public string PriceBelowMSRP
        {
            get { throw new System.NotImplementedException(); }
        }

        public void SetFinancingSpecial(int specialId, string memberLogin)
        {
            throw new System.NotImplementedException();
        }

        public string YearMakeModel
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasHighPassengerCountForClass()
        {
            throw new System.NotImplementedException();
        }

        public string GoodWarranties
        {
            get { throw new System.NotImplementedException(); }
        }

        public string GoodCrashTests
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool HasEquipment(string equipmentDescription)
        {
            throw new System.NotImplementedException();
        }

        public string GetCondition()
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<string> LimitCount(System.Collections.Generic.List<string> values)
        {
            throw new System.NotImplementedException();
        }

        public bool EvaluateCondition(string templateCondition)
        {
            throw new System.NotImplementedException();
        }

        public bool EvaluateSingleCondition(string condition)
        {
            throw new System.NotImplementedException();
        }

        public bool EvaluateSingleCondition(Templating.TemplateCondition condition)
        {
            throw new System.NotImplementedException();
        }

        public T GetRandomValue<T>(T[] array)
        {
            throw new System.NotImplementedException();
        }

        public T[] RandomPermutation<T>(System.Collections.Generic.List<T> array)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<Templating.IWeightedItem> RandomWeightedPermutation(System.Collections.Generic.List<Templating.IWeightedItem> array)
        {
            throw new System.NotImplementedException();
        }

        public int Next()
        {
            throw new System.NotImplementedException();
        }

        public int Next(int minValue, int maxValue)
        {
            throw new System.NotImplementedException();
        }

        public int Next(int maxValue)
        {
            throw new System.NotImplementedException();
        }

        public double NextDouble()
        {
            throw new System.NotImplementedException();
        }

        public void NextBytes(byte[] buffer)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<string> HighlightedPackageSummaries
        {
            get { throw new System.NotImplementedException(); }
        }

        public decimal KBBBookValue
        {
            get { throw new System.NotImplementedException(); }
        }

        public decimal NADABookValue
        {
            get { throw new System.NotImplementedException(); }
        }

        public string QRCodeURL
        {
            get { throw new System.NotImplementedException(); }
        }
    }
}
