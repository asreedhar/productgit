using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using FirstLook.Common.Data;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class GenericEquipment : ITieredEquipment
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(GenericEquipment).FullName);
        #endregion


        private string _optionText = string.Empty;

        public GenericEquipment(int categoryID)
        {
            DetailText = new List<string>();
            UseDetailText = false;
            _optionText = string.Empty;
            OptionCode = string.Empty;
            Category = new CategoryLink(categoryID, "C");
        }

        

        public GenericEquipment(CategoryLink category, bool isStandard)
        {
            DetailText = new List<string>();
            UseDetailText = false;
            _optionText = string.Empty;
            OptionCode = string.Empty;
            Category = category;
            IsStandard = isStandard;
        }

        public GenericEquipment(CategoryLink category, int tierNumber, int displayPriority)
        {
            DetailText = new List<string>();
            UseDetailText = false;
            _optionText = string.Empty;
            OptionCode = string.Empty;
            Category = category;
            Tier = tierNumber;
            DisplayPriority = displayPriority;
        }
        public GenericEquipment()
        {
            DetailText = new List<string>();
            OptionCode = string.Empty;
            Category = new CategoryLink();
        }

        internal GenericEquipment(IDataRecord reader)
        {
            Category = new CategoryLink(reader);
           
            string details = reader.GetString(reader.GetOrdinal("detailText"));
            if (String.IsNullOrEmpty(details))
            {
                DetailText = new List<string>();
            }
            else
            {
                DetailText = new List<string>(details.Split(",".ToCharArray()));
            }
            OptionCode = reader.GetString(reader.GetOrdinal("optionCode"));
            _optionText = reader.GetString(reader.GetOrdinal("optionText"));
            Tier = reader.GetInt32(reader.GetOrdinal("tierNumber"));
            DisplayPriority = reader.GetInt32(reader.GetOrdinal("displayPriority"));
            IsStandard = reader.GetBoolean(reader.GetOrdinal("isStandardEquipment"));
            UseDetailText = true;
           
        }

        public bool MatchesTypeFilter(string typeFilter)
        {
            return Category.TypeFilter.Equals(typeFilter);
        }

        #region Properties

        public int DisplayPriority { get; set; }

        public int Tier { get; set; }

        public CategoryLink Category { get; set; }

        public string OptionCode { get; set; }

        public List<string> DetailText { get; set; }

        public string OptionText
        {
            get { return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(_optionText); }
            set { _optionText = value; }
        }

        public bool UseDetailText { get; set; }

        public bool IsStandard { get; set; }


        public string Description
        {
            get { return GetDescription(); }
        }
        #endregion

        #region IComparable Members

        public int CompareTo(object obj)
        {
            ITieredEquipment x = obj as ITieredEquipment;
            if (x == null) throw new ApplicationException("Can only compare Tiered to TieredEquipment");

            int val = Tier.CompareTo(x.GetTier());
            if (val == 0)
            {
                val = DisplayPriority.CompareTo(x.GetDisplayPriority());
            }

            if (val == 0 && (x is DetailedEquipment || x is GenericEquipmentReplacement)) 
            {
                val = 1;    
            }
            return val;
        }
        
        #endregion

        #region ITieredEquipment
        public string GetDetailText()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string dt in DetailText)
            {
                sb.Append(dt + ", ");
            }
            return sb.ToString().TrimEnd(", ".ToCharArray());
        }
        public void SetTierAndPriority(GenericEquipmentCollection generics)
        {

        }

        public void Prioritize()
        {
            // set tier and priority
            Tier = 1;
            DisplayPriority = 1;
        }

        public string GetSummary()
        {
            return string.Empty;
        }

        public string GetDescription()
        {
            string retStr = "";
            if (!String.IsNullOrEmpty(_optionText))
            {
                retStr += _optionText;
            }
            else
            {
                retStr += Category.Description;
            }

            if (UseDetailText && DetailText.Count > 0)
            {
                retStr += "(" + GetDetailText() + ")";
            }
            return retStr;
        }

        public int GetTier()
        {
            return Tier;
        }

        public int GetDisplayPriority()
        {
            return DisplayPriority;
        }
        #endregion

        public void Save(int businessUnitId, int inventoryId)
        {
            //condition may be unnecessary, now that we delete all options prior to insertion...

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                Save(businessUnitId, inventoryId, con);
            }
        }

        public void Save(int businessUnitId, int inventoryId, IDataConnection openConnection)
        {
            Log.InfoFormat("Saving VehicleOption for inventoryID = {0}, OptionID = {1}, IsStandard = {2}", inventoryId, Category.CategoryID, IsStandard);
            using (IDbCommand cmd = openConnection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                //
                cmd.CommandText = "builder.vehicleOption#Save";

                Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "InventoryID", inventoryId, DbType.Int32);
                Database.AddRequiredParameter(cmd, "OptionID", Category.CategoryID, DbType.Int32);
                Database.AddRequiredParameter(cmd, "OptionCode", OptionCode, DbType.String);
                Database.AddRequiredParameter(cmd, "OptionText", OptionText, DbType.String);
                Database.AddRequiredParameter(cmd, "DetailText", GetDetailText(), DbType.String);
                Database.AddRequiredParameter(cmd, "IsStandardEquipment", IsStandard, DbType.Boolean);
                cmd.ExecuteNonQuery();
            }
        
        }

    
        public static Dictionary<int, List<CategoryLink>> GetThirdPartyMappings()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "merchandising.categoryMappings#Fetch";
                    using(IDataReader reader = cmd.ExecuteReader())
                    {
                        Dictionary<int, List<CategoryLink>> retMap = new Dictionary<int, List<CategoryLink>>();
                        while (reader.Read())
                        {
                            int opId = reader.GetInt32(reader.GetOrdinal("optionId"));
                            if (!retMap.ContainsKey(opId))
                            {
                                retMap.Add(opId, new List<CategoryLink>());
                            }

                            object chromeCatId = reader.GetValue(reader.GetOrdinal("categoryId"));
                            if (chromeCatId != DBNull.Value)
                            {

                                retMap[opId].Add(new CategoryLink((int)chromeCatId, string.Empty, -1, string.Empty, reader.GetString(reader.GetOrdinal("category"))));    
                            }
                            
                        }
                        return retMap;
                    }
                }
            }
        }


        public static void RefreshMappingsFromLotData()
        {
            RefreshMappings(true);   
        }
        public static void RefreshMappingsFromMarket()
        {
            RefreshMappings(false);
        }
        public static void RefreshMappings(bool useLotData)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "merchandising.RefreshAvailableMappings";

                    Database.AddRequiredParameter(cmd, "UseLotDataFlag", useLotData, DbType.Boolean);
                    cmd.ExecuteNonQuery();
                }

            }
        }
        
        
        public static void AddThirdPartyMapping(int optionId, string optionText, int chromeCategoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                
                using(IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "merchandising.categoryMapping#Upsert";
                    Database.AddRequiredParameter(cmd, "OptionId", optionId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OptionText", optionText, DbType.String);
                    Database.AddRequiredParameter(cmd, "CategoryId", chromeCategoryId, DbType.Int32);
                    cmd.ExecuteNonQuery();
                }
              
            }
        
        }
        public static void ClearThirdPartyMapping(int optionId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "merchandising.categoryMapping#Delete";
                    Database.AddRequiredParameter(cmd, "OptionId", optionId, DbType.Int32);
                    cmd.ExecuteNonQuery();
                }
            
            }
        }
        public static void CreateThirdPartyMapping(int optionId, string optionText, List<int> chromeCategoryIds)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbTransaction trans = con.BeginTransaction())
                {
                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        //
                        cmd.CommandText = "merchandising.categoryMapping#Delete";
                        Database.AddRequiredParameter(cmd, "OptionId", optionId, DbType.Int32);
                        cmd.ExecuteNonQuery();
                    }

                    foreach (int chromeCategoryId in chromeCategoryIds)
                    {
                        using(IDbCommand cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trans;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "merchandising.categoryMapping#Insert";
                            Database.AddRequiredParameter(cmd, "OptionId", optionId, DbType.Int32);
                            Database.AddRequiredParameter(cmd, "OptionText", optionText, DbType.String);
                            Database.AddRequiredParameter(cmd, "CategoryId", chromeCategoryId, DbType.Int32);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    trans.Commit();
                }
            }
        }

        public void LoadViewState(object state)
        {
            Category = new CategoryLink();
            if (state is Pair<int, bool>)
            {
                Pair<int, bool> pair = state as Pair<int, bool>;
                Category.CategoryID = pair.First;
                IsStandard = pair.Second;
            }
        }

        public object SaveViewState()
        {
            return new Pair<int, bool>(Category.CategoryID, IsStandard);            
        }
    
    }



    //order by tier, then displaypriority
    public class GenericEquipmentPreFilterOrderer : IComparer
    {
        // Calls CaseInsensitiveComparer.Compare with the parameters reversed.

        #region IComparer Members

        public int Compare(object xo, object yo)
        {
            GenericEquipment x = xo as GenericEquipment;
            GenericEquipment y = yo as GenericEquipment;
            if (x == null || y == null)
                throw new ApplicationException("GenericEquipmentPreFilterOrderer can only order display equipment");
            if (x.IsStandard != y.IsStandard)
            {
                //if x is standard but y is not, put it lower in list
                if (x.IsStandard)
                {
                    return 1;
                }

                //otherwise, x is not standard, but y is, so put x higher in list
                return -1;
            }

            //if they're both std or non-std, use their native comparer
            return x.CompareTo(y);
        }

        #endregion
    }
}