using System.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for OrderRule
    /// </summary>
    public class OrderRule
    {
        public OrderRule(int sequence, string optionCode, string condition, string ruleOperator, string ruleOperand,
                         bool isConditional)
        {
            Sequence = sequence;
            OptionCode = optionCode;
            Condition = condition;
            RuleOperator = ruleOperator;
            RuleOperand = ruleOperand;
            IsConditional = isConditional;
        }

        public OrderRule(int sequence, string optionCode, string condition, string ruleOperator, string ruleOperand,
                         char conditionalYn)
        {
            Sequence = sequence;
            OptionCode = optionCode;
            Condition = condition;
            RuleOperator = ruleOperator;
            RuleOperand = ruleOperand;

            IsConditional = (conditionalYn.ToString().ToLower() == "y");
        }

        public OrderRule(IDataRecord reader)
            :
                this(reader.GetInt16(reader.GetOrdinal("sequence")),
                     reader.GetString(reader.GetOrdinal("optionCode")),
                     reader.GetString(reader.GetOrdinal("condition")),
                     reader.GetString(reader.GetOrdinal("operator")),
                     reader.GetString(reader.GetOrdinal("operand")),
                     reader.GetString(reader.GetOrdinal("isconditional"))[0])
        {
        }

        public int Sequence { get; set; }

        public string OptionCode { get; set; }

        public string Condition { get; set; }

        public string RuleOperator { get; set; }

        public string RuleOperand { get; set; }

        public bool IsConditional { get; set; }
    }
}