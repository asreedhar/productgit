namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    
    public enum ActivityStatusCodes
    {
        Untouched = 0,
        Incomplete,
        Complete

    }
}
