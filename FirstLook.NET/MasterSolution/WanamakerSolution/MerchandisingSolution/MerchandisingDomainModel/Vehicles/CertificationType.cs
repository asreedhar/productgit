namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum CertificationType
    {
        /* NOTE: These numeric values are stored in the database. Do not change. */
        Undefined = 0,
        Audi = 1,
        Ford = 2,
        GMC = 3,
        Nissan = 4,
        Toyota = 5,
        Honda = 6,
        Acura = 7,
        Cadillac = 8,
        Buick = 9,
        Chevrolet = 10,
        Chrysler = 11,
        Dodge = 12,
        Hyundai = 13,
        Infiniti = 14,
        Jeep = 15,
        Lexus = 16,
        BMW = 17,
        MercedesBenz = 18,        
        Lincoln = 19,
        Mazda = 20,
        Mercury = 21,
        Mitsubishi = 22,
        Oldsmobile = 23,
        Pontiac = 24,
        Saab = 25,
        Saturn = 26,
        Scion = 27,
        Suzuki = 28,
        Volkswagen = 29,
        Volvo = 30,
        Hummer = 31,
        MINI = 32,
        Subaru = 33,
        Kia = 39,
        LandRover = 40,
        Fiat = 42
    }
}
