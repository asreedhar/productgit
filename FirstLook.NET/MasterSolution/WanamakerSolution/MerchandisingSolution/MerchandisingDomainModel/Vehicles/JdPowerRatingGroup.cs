using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class JdPowerRatingGroup : JdPowerRating
    {
        private const string BASE_FORMAT_STRING = "{0} Ratings - ";
        

        public JdPowerRatingGroup(string name, string shortName, double? rating, JdPowerRatingType type, List<JdPowerRating> childRatings)
            : base(name, shortName, rating, type)
        {
            ChildRatings = childRatings;
        }

        public List<JdPowerRating> ChildRatings { get; set; }

        public void AddSubRating(JdPowerRating rating)
        {
            if (!ChildRatings.Contains(rating))
            {
                ChildRatings.Add(rating);
            }
        }

        public JdPowerRating GetSubRating(JdPowerRatingType type)
        {
            foreach (JdPowerRating rating in ChildRatings)
            {
                if (rating.RatingType == type)
                {
                    return rating;
                }
            }
            throw new ApplicationException("Rating not found in list");
        }

        public bool Contains(JdPowerRatingType type)
        {
            foreach (JdPowerRating rating in ChildRatings)
            {
                if (rating.RatingType == type)
                {
                    return true;
                }
            }
            return false;
        }

        public override bool IsDisplayable(double minValidRating)
        {
            if (Rating >= minValidRating)
            {
                return true;
            }
        
            foreach (JdPowerRating subRating in ChildRatings)
            {
                if (subRating.Rating >= minValidRating)
                {
                    return true;
                }
            }
            return false;
        }

        public override string ToString(double minValidRating)
        {
            if (base.IsDisplayable(minValidRating))
            {
                return base.ToString();
            }

            List<JdPowerRating> displayables = new List<JdPowerRating>();
            foreach (JdPowerRating rating in ChildRatings)
            {
                if (rating.IsDisplayable(minValidRating))
                {
                    displayables.Add(rating);
                }
            }
            if (displayables.Count > 1)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format(BASE_FORMAT_STRING, Name));
                foreach (JdPowerRating rating in displayables)
                {
                    sb.Append(rating.ToShortString());
                    sb.Append(", ");
                }
                return sb.ToString(0, sb.Length - 2); //trim off last comma space
            }
            
            if (displayables.Count == 1)
            {
                return displayables[0].ToString();
            }

            return string.Empty;
        }
    }
}
