namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum CpoGroup
    {
        Undefined = 0,
        Audi,
        BMW,
        Chrysler,
		Ford,
		GM,
		Honda,
		Hyundai,
		Kia,
		Mazda,
		Mercedes,
		Nissan,
		Toyota,
		Volkswagen,
		Volvo,
		Suzuki,
		Saab,
        LandRover = 25,
        Fiat = 26
    }
}
