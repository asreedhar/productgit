using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.UI;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Data;
using log4net;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class DetailedEquipment : ITieredEquipment
    {
        #region Logging

        private static readonly ILog Log = LogManager.GetLogger(typeof (DetailedEquipment).FullName);

        #endregion

        private const int PREVIEW_LENGTH = 37;
        private const string standardCodePrefix = "std";
        private string detailText;

        public string DetailText
        {
            get { return detailText; }
            set { detailText = value; }
        }

        private int tier;
        private string categoryList;
        public CategoryCollection categoryAdds;
        public CategoryCollection categoryRemoves;
        private string optionText = "";
        
        public decimal Msrp = -1;

        public DetailedEquipment(string optionCode, string optionText, string chromeCategoryList, string detailText,
                                 bool isStandard)
        {
            Log.DebugFormat(
                "DetailedEquipment() ctor called with optionCode: '{0}', optionText: '{1}', chromeCategoryList: '{2}', detailText: '{3}', isStandard: '{4}'",
                optionCode, optionText, chromeCategoryList, detailText, isStandard);

            this.optionText = optionText;
            this.OptionCode = optionCode;
            this.IsStandard = isStandard;
            this.detailText = detailText;

            SetCategoryList(chromeCategoryList);
        }

        public void SetCategoryList(string chromeCategoryList)
        {
            Log.DebugFormat("SetCategoryList() called with chromeCategoryList '{0}'", chromeCategoryList);

            categoryList = chromeCategoryList;
            CategoryCollection cc = Equipment.parseCategoryList(chromeCategoryList);
            categoryAdds = cc.GetCategoryAdds();
            categoryRemoves = cc.GetCategoryRemoves();
        }

        public void SwapAddsAndRemoves()
        {
            Log.Debug("SwapAddsAndRemoves() called. Swapping categoryRemoves and categoryAdds variables.");

            CategoryCollection tmp = categoryRemoves;
            categoryRemoves = categoryAdds;
            categoryAdds = tmp;
        }

        public DetailedEquipment()
        {
            OptionCode = "";
            Log.Debug("Default ctor called.");
        }

        public DetailedEquipment(Equipment equipment)
        {
            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("Ctor called with Equipment {0}", StandardObjectDumper.Write(equipment, 1));
            }

            detailText = equipment.ExtDescription;
            OptionCode = equipment.OptionCode;
            optionText = equipment.Description;
            IsStandard = false;

            SetCategoryList(equipment.CategoryList);
        }

        internal DetailedEquipment(IDataRecord reader)
        {
            Log.Debug("Ctor called with IDataRecord.");

            detailText = reader.GetString(reader.GetOrdinal("detailText"));
            OptionCode = reader.GetString(reader.GetOrdinal("optionCode"));
            optionText = NormalizeOptionText(reader.GetString(reader.GetOrdinal("optionText")));
            IsStandard = reader.GetBoolean(reader.GetOrdinal("isStandardEquipment"));

            SetCategoryList(reader.GetString(reader.GetOrdinal("categoryList")));
        }

        #region overrides

        public override bool Equals(object obj)
        {
            DetailedEquipment other = obj as DetailedEquipment;
            if (other == null) return false;

            // Just compare option codes.
            return OptionCode == other.OptionCode;
        }

        public override int GetHashCode()
        {
            return OptionCode.GetHashCode();
        }

        #endregion

        private string NormalizeOptionText(string inText)
        {
            Log.DebugFormat("NormalizeOptionText called with input '{0}'", inText);

            string titleCase = new CultureInfo("en").TextInfo.ToUpper(inText);
            string result = processExceptions(titleCase);

            Log.DebugFormat("Result is: '{0}'", result);
            return result;
        }

        public static string processExceptions(string inText)
        {
            Log.DebugFormat("ProcessExceptions() called with input: '{0}'", inText);

            List<Pair> regexReplaces = new List<Pair>();
            regexReplaces.Add(new Pair("mp3", "MP3"));
            regexReplaces.Add(new Pair("dvd", "DVD"));
            regexReplaces.Add(new Pair("(\\W)cd(\\W)", "$1CD$2"));
            regexReplaces.Add(new Pair("xm", "XM"));
            regexReplaces.Add(new Pair("(\\W)am(\\W)", "$1AM$2"));
            regexReplaces.Add(new Pair("fm(\\W)", "FM$1"));
            regexReplaces.Add(new Pair("(\\W)etr(\\W)", "$1ETR$2"));
            regexReplaces.Add(new Pair("(\\W)gm(\\W)", "$1GM$2"));
            regexReplaces.Add(new Pair("(\\W)bbs(\\W)", "$1BBS$2"));
            regexReplaces.Add(new Pair("(\\W)jbl(\\W)", "$1JBL$2"));
            regexReplaces.Add(new Pair("(\\W)xle(\\W)", "$1XLE$2"));
            regexReplaces.Add(new Pair("(\\W)xlt(\\W)", "$1XLT$2"));
            regexReplaces.Add(new Pair("(\\W)ipod(\\W)", "$1iPod$2"));
            regexReplaces.Add(new Pair("(\\W)usb(\\W)", "$1USB$2"));
            regexReplaces.Add(new Pair("(\\W)bmw(\\W)", "$1BMW$2"));
            // FB 29059 - tureen 02/12/2014            
            regexReplaces.Add(new Pair("HARMAN/KARDON", "harman/kardon")); 
            regexReplaces.Add(new Pair("HARMAN KARDON", "harman kardon"));
            regexReplaces.Add(new Pair("HARMAN-KARDON", "harman-kardon"));
            regexReplaces.Add(new Pair("gxp", "GXP"));

            foreach (Pair pair in regexReplaces)
            {
                Regex rg = new Regex((string) pair.First, RegexOptions.IgnoreCase);
                inText = rg.Replace(inText, (string) pair.Second);
            }

            Log.DebugFormat("Result is: '{0}'", inText);
            return inText;
        }


        public string OptionCode { get; set; }

        public static string GetStandardCode(int sequenceNumber)
        {
            string value = standardCodePrefix + sequenceNumber;
            Log.DebugFormat("Returning standard code '{0}'", value);
            return value;
        }

        public string OptionText
        {
            get { return NormalizeOptionText(optionText); }
            set { optionText = value; }
        }

        public string OptionTextPreview
        {
            get
            {
                string temp = OptionText;
                if (temp.Length > PREVIEW_LENGTH)
                {
                    Log.DebugFormat(
                        "OptionText '{0}' is longer than preview length {1}. Truncating and adding ... to end.", temp,
                        PREVIEW_LENGTH);
                    return temp.Substring(0, PREVIEW_LENGTH) + "...";
                }

                Log.DebugFormat("Returning '{0}'", temp);
                return temp;
            }
        }

        public string QuickPackagesToolTip
        {
            get
            {
                string result = OptionCode.Trim() + ": " + Description.Trim();
                Log.DebugFormat("Returning '{0}'", result);
                return result;
                //if (!String.IsNullOrEmpty(Description.Trim()))
                //{
                //    string temp = Description;
                //    if (temp.Contains(OptionText))
                //    {
                //        temp = temp.Remove(0, OptionText.Length);
                //        temp = temp.Trim();
                //    }

                //    builder.Append(": " + temp);
                //}
                //return builder.ToString();
            }
        }

        public bool IsStandard { get; set; }


        public string Description
        {
            get { return GetDescription(); }
        }

        public string DescriptionPreview
        {
            get
            {
                string temp = Description;
                string result = string.Empty;

                if (temp.Contains(OptionText))
                {
                    Log.DebugFormat("Description '{0}' contains OptionText '{1}'.  Removing it and trimming.", temp,
                                    OptionText);
                    temp = temp.Remove(0, OptionText.Length);
                    temp = temp.Trim();
                    result = temp;
                }
                if (temp.Length > PREVIEW_LENGTH)
                {
                    Log.DebugFormat(
                        "Description '{0}' is longer than preview length {1}.  Truncating to preview length and adding ... to end.",
                        temp, PREVIEW_LENGTH);

                    result = temp.Substring(0, PREVIEW_LENGTH) + "...";
                }

                Log.DebugFormat("Returning preview description '{0}'", result);
                return result;
            }
        }


        public string GetDescription()
        {
            // Get rid of any leading codes embedded in the detailText. 
            string retStr = RemoveLeadingOptionCode(detailText);
            retStr = retStr.TrimEnd(" ".ToCharArray());
            Log.DebugFormat("Returning '{0}'", retStr);
            return retStr;
        }

        /// <summary>
        /// Get rid of any leading codes embedded in the detailText.  The format is [xxx] 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string RemoveLeadingOptionCode(string input)
        {
            // Get rid of any leading codes embedded in the detailText.  The format is [xxx] 
            if (input.Length >= 5)
            {
                if (input[0] == '[' && input[4] == ']')
                {
                    return input.Substring(5, input.Length - 5);
                }
            }

            return input;
        }

        public static void DeleteDetailedEquipmentOverride(int businessUnitId, int chromeStyleId, string optionCode)
        {
            Log.DebugFormat(
                "DeleteDetailedEquipmentOverride() called with businessUnitId {0}, chromeStyleId {1}, optionCode '{2}'",
                businessUnitId, chromeStyleId, optionCode);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "builder.equipmentDescriptionOverride#Delete";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OptionCode", optionCode, DbType.String);
                    Database.AddRequiredParameter(cmd, "ChromeStyleId", chromeStyleId, DbType.Int32);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void CreateDetailedEquipmentOverrideForStyle(int businessUnitId, int chromeStyleId, string customTitle,
                                                            string customEquipmentBody)
        {
            CreateDetailedEquipmentOverride(businessUnitId, chromeStyleId, null, false, customTitle, customEquipmentBody);
        }

        public void CreateDetailedEquipmentOverrideForModelByStyle(int businessUnitId, int chromeStyleId,
                                                                   string customTitle, string customEquipmentBody)
        {
            CreateDetailedEquipmentOverride(businessUnitId, chromeStyleId, null, true, customTitle, customEquipmentBody);
        }

        public void CreateDetailedEquipmentOverrideForModel(int businessUnitId, int modelId, string customTitle,
                                                            string customEquipmentBody)
        {
            CreateDetailedEquipmentOverride(businessUnitId, null, modelId, true, customTitle, customEquipmentBody);
        }


        private void CreateDetailedEquipmentOverride(int businessUnitId, int? chromeStyleId, int? modelId,
                                                     bool applyToModel,
                                                     string customTitle, string customEquipmentBody)
        {
            Log.DebugFormat(
                "CreateDetailedEquipmentOverride() called with businessUnitId {0}, chromeStyleId {1}, modelId {2}, applyToModel {3}, customTitle '{4}', customEquipmentBody {5}",
                businessUnitId, chromeStyleId, modelId, applyToModel, customTitle, customEquipmentBody);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "builder.vehicleDetailedOption#Override";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OptionCode", OptionCode, DbType.String);
                    Database.AddRequiredParameter(cmd, "CustomTitle", customTitle, DbType.String);
                    Database.AddRequiredParameter(cmd, "CustomBody", customEquipmentBody, DbType.String);
                    Database.AddRequiredParameter(cmd, "IsStandard", IsStandard, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "ApplyToEntireModel", applyToModel, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "ModelId", modelId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ChromeStyleId", chromeStyleId, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void Save(int businessUnitId, int inventoryId)
        {
            Log.WarnFormat(
                "Saving VehicleDetailedOption for businessUnitId = {0}, inventoryId = {1}, OptionCode = {2}",
                businessUnitId, inventoryId, OptionCode);

            //condition may be unnecessary, now that we delete all options prior to insertion...
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "builder.vehicleDetailedOption#Save";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryID", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OptionCode", OptionCode, DbType.String);
                    Database.AddRequiredParameter(cmd, "OptionText", OptionText, DbType.String);
                    Database.AddRequiredParameter(cmd, "DetailText", detailText.SmartTruncate(800), DbType.String);
                    Database.AddRequiredParameter(cmd, "CategoryList", categoryList, DbType.String);
                    Database.AddRequiredParameter(cmd, "IsStandardEquipment", IsStandard, DbType.Boolean);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        //order by tier, then displaypriority
        public int GetDetailScore(GenericEquipmentCollection equipmentList, int tierNumber)
        {
            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("GetDetailScore() called with equipmentList {0}, tierNumber {1}",
                                StandardObjectDumper.Write(equipmentList, 1), tierNumber);
            }

            int score = 0;

            foreach (CategoryLink cl in categoryAdds)
            {
                //see if our list contains this item still...
                if (equipmentList.Contains(cl.CategoryID))
                {
                    Log.DebugFormat("EquipmentList contains categoryID {0}", cl.CategoryID);

                    int theTier = equipmentList.GetByCategoryId(cl.CategoryID).Tier;
                    if (theTier <= tierNumber)
                    {
                        //increment the score - use number large enough so higher tiers always trump lower tiers to make ordering work correctly
                        score += 100*(tierNumber - theTier + 1);

                        Log.DebugFormat("Category tier {0} < tierNumber {1}.  Score is now: {2}", theTier, tierNumber,
                                        score);
                    }
                }
            }

            Log.DebugFormat("Returning score {0}", score);
            return score;
        }

        public void SetTier(int tierNumber)
        {
            Log.DebugFormat("SetTier() called with tierNumber {0}", tierNumber);
            tier = tierNumber;
        }

        public void SetTierAndPriority(GenericEquipmentCollection generics)
        {
            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("SetTierAndPriority() called with generic collection {0}",
                                StandardObjectDumper.Write(generics, 1));
            }

            int tmpDisplayPriority = 999999;
            int tmpTier = 100;

            foreach (GenericEquipment equipment in generics)
            {
                if (categoryAdds.Contains(equipment.Category.CategoryID))
                {
                    tmpTier = Math.Min(equipment.GetTier(), tmpTier);
                    tmpDisplayPriority = Math.Min(equipment.GetDisplayPriority(), tmpDisplayPriority);
                }
            }

            Log.DebugFormat("Setting tier to {0} and displayPriority to {1}", tmpTier, tmpDisplayPriority);

            tier = tmpTier;
            displayPriority = tmpDisplayPriority;
        }

        public void Prioritize()
        {
            Log.DebugFormat("Prioritize called.  Setting tier to 1 and displayPriority to 0.");

            // set tier and priority
            tier = 1;
            displayPriority = 1;
        }

        public string GetSummary()
        {
            return OptionText;
        }

        public int GetTier()
        {
            //int tier = 10;
            return tier; //base on categories
        }

        private int displayPriority;

        public int GetDisplayPriority()
        {

            return displayPriority; //base on categories...
        }

        public int CompareTo(object obj)
        {
            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("CompareTo() called with object {0}", StandardObjectDumper.Write(obj, 1));
            }

            /*ITieredEquipment y = obj as ITieredEquipment;
            if (y==null) throw new ApplicationException("Can only compare tiered to tiered equipment");
            return GetTier().CompareTo(y.GetTier());*/


            ITieredEquipment x = obj as ITieredEquipment;
            if (x == null)
            {
                const string ERROR = "Can only compare Tiered to TieredEquipment";
                Log.Error(ERROR);
                throw new ApplicationException(ERROR);
            }

            int val = GetTier().CompareTo(x.GetTier());
            if (val == 0)
            {
                val = GetDisplayPriority().CompareTo(x.GetDisplayPriority());
            }

            //if x is anything but detailed equip, order this first
            if (val == 0 && !(x is DetailedEquipment))
            {
                val = -1;
            }

            Log.DebugFormat("Returning {0}", val);

            return val;
        }

        public void LoadViewState(object state)
        {
            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("LoadViewState called with state object {0}", StandardObjectDumper.Write(state, 1));
            }

            object[] castedState = state as object[];
            if (castedState != null && castedState.Length >= 5)
            {
                OptionCode = (string) castedState[0];
                OptionText = (string) castedState[1];
                DetailText = (string) castedState[2];
                IsStandard = (bool) castedState[3];
                SetCategoryList((string) castedState[4]);
            }

        }

        public object SaveViewState()
        {
            //save equipment items
            object[] state = new object[5];
            state[0] = OptionCode;
            state[1] = OptionText;
            state[2] = DetailText;
            state[3] = IsStandard;
            state[4] = categoryList;

            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("Returning state object {0}", StandardObjectDumper.Write(state, 1));
            }

            return state;
        }

        
    }
}