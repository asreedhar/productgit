using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Category Link represents a chrome category - some lookup values can be found in ChromeCategory enum
    /// </summary>
    [Serializable]
    public class CategoryLink
    {
        public string CategoryAction = "T"; // can be C, D, P, T -- defines action to take
        /* [Tyler W.R. Cole - April 8, 2013 4:01 PM], from CategoryCollection.cs
         * (this should probably be a proper C# Enumeration)
         *   T   default; unknown (no action?)
         *   D   interpret as "remove"
         *   C   interpret as "add"
         *   P   interpret as "add" (not known why there are two "adds")
         */

        private string _typeFilter;

        public CategoryLink()
        {
            Header = "";
            Description = "";
            CategoryID = -1;
        }

        internal CategoryLink(IDataRecord reader)
        {
            CategoryID = reader.GetInt32(reader.GetOrdinal("categoryID"));
            CategoryAction = reader.GetString(reader.GetOrdinal("state"));
            HeaderID = reader.GetInt32(reader.GetOrdinal("CategoryHeaderID"));
            Header = reader.GetString(reader.GetOrdinal("CategoryHeader"));
            Description = reader.GetString(reader.GetOrdinal("UserFriendlyName"));
            _typeFilter = reader.GetString(reader.GetOrdinal("CategoryTypeFilter"));
        }

        internal CategoryLink(DataRow reader)
        {
            CategoryID = Convert.ToInt32(reader["categoryID"]);
            CategoryAction = Convert.ToString(reader["state"]);
            HeaderID = Convert.ToInt32(reader["CategoryHeaderID"]);
            Header = Convert.ToString(reader["CategoryHeader"]);
            Description = Convert.ToString(reader["UserFriendlyName"]);
            _typeFilter = Convert.ToString(reader["CategoryTypeFilter"]);
        }

        public CategoryLink(int categoryID, string categoryClass)
        {
            Header = "";
            Description = "";
            CategoryID = categoryID;
            CategoryAction = categoryClass;
        }

        public CategoryLink(int categoryID, string categoryClass, int headerID, string header)
            :
                this(categoryID, categoryClass)
        {
            Header = header;
            HeaderID = headerID;
        }

        public CategoryLink(int categoryID, string categoryClass, int headerID, string header, string description)
            :
                this(categoryID, categoryClass, headerID, header)
        {
            Description = description;
        }

        public CategoryLink(int categoryID, string categoryClass, int headerID, string header, string description,
                            string typeFilter)
            :
                this(categoryID, categoryClass, headerID, header, description)
        {
            _typeFilter = typeFilter;
        }

        public CategoryLink(string catIDandClass)
        {
            Header = "";
            Description = "";
            if (catIDandClass.Length >= 5)
            {
                CategoryID = Int32.Parse(catIDandClass.Substring(0, 4));
                CategoryAction = catIDandClass.Substring(4, 1);
            }
            else
            {
                CategoryID = -1;
                CategoryAction = "T";
            }
        }

        public int CategoryID { get; set; }

        public string Description { get; set; }

        public string Header { get; set; }

        public int HeaderID { get; set; }

        public string AssociatedInfo { get; set; }

        public EquipmentCollection AssociatedEquipment { get; set; }

        public bool IsSelected { get; set; }

        public string TypeFilter
        {
            get
            {
                //we have a single exception for transmissions because a/t and m/t are actually considered "Transmission - Type"
                if (String.IsNullOrEmpty(_typeFilter))
                {
                    return string.Empty;
                }
                
                return _typeFilter.ToLower().Contains("transmission") ? "Transmission" : _typeFilter;
            }
            set { _typeFilter = value; }
        }

        public static List<int> GetCatIDsFromHeaderList(List<int> headers)
        {
            string headerList = string.Empty;
            foreach (int headID in headers)
            {
                headerList += headID + ",";
            }
            headerList = headerList.TrimEnd(",".ToCharArray());

            string sql =
                @"
                select categoryID from vehiclecatalog.chrome.categories c 
                inner join vehiclecatalog.chrome.categoryheaders h on h.categoryheaderid = c.categoryheaderid
                where c.countrycode = 1 and h.countrycode = 1 and h.categoryheaderid in (" +
                headerList + @")
                
            ";

            using (IDataConnection con = Database.GetConnection("Chrome"))
            {
                con.Open();
                    
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.Text;
                    using(IDataReader reader = cmd.ExecuteReader())
                    {
                        List<int> retList = new List<int>();
                        while (reader.Read())
                        {
                            retList.Add(reader.GetInt32(reader.GetOrdinal("categoryID")));
                        }
                        return retList;
                    }
                    
                }
            }
        }



    }
}