using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for VehicleAttributes
    /// </summary>
    [Serializable]
    public class VehicleAttributes
    {
        private string cfbodytype;
        public VehicleAttributes(string engine, string drivetrain, string transmission,
                                 string fuelType, int doors, decimal originalMsrp, int passengerCapacity,
                                 int multiEngineFlag, int multiTransFlag, int multiFuelFlag, int multiForcedInducsFlag,
                                 int multiDrivetrainFlag, int multiDoorsFlag, int multiCapacityFlag)
        {
            Doors = doors;
            Drivetrain = drivetrain;
            Engine = engine;
            FuelType = fuelType;

            Transmission = transmission;
            OriginalMsrp = originalMsrp;
            PassengerCapacity = passengerCapacity;

            UniqueDoors = multiDoorsFlag > 0;
            UniqueDrivetrain = multiDrivetrainFlag > 0;
            UniqueEngine = multiEngineFlag > 0;
            UniqueFuelType = multiFuelFlag > 0;
            UniquePassengerCount = multiCapacityFlag > 0;
            UniqueTransmission = multiTransFlag > 0;
        }

        public VehicleAttributes()
        {
            Doors = 2;
            Drivetrain = "";
            Engine = "";
            FuelType = "";
            Transmission = "";
            OriginalMsrp = 0.0M;
            PassengerCapacity = 1;

            UniqueDoors = false;
            UniqueDrivetrain = false;
            UniqueEngine = false;
            UniqueFuelType = false;
            UniquePassengerCount = false;
            UniqueTransmission = false;
        }

        public string Engine { get; set; }

        public bool UniqueEngine { get; set; }


        public string Transmission { get; set; }

        public bool UniqueTransmission { get; set; }

        public DrivetrainType DrivetrainType
        {
            get
            {
                switch (Drivetrain)
                {
                    case "Four Wheel Drive":
                        return DrivetrainType.FourWheelDrive;
                    case "All Wheel Drive":
                        return DrivetrainType.AllWheelDrive;
                    case "Front Wheel Drive":
                        return DrivetrainType.FrontWheelDrive;
                    case "Rear Wheel Drive":
                        return DrivetrainType.RearWheelDrive;
                    default:
                        return DrivetrainType.Unknown;
                }
            }
        }

        public string Drivetrain { get; set; }

        public bool UniqueDrivetrain { get; set; }


        public string FuelType { get; set; }

        public bool UniqueFuelType { get; set; }

        public int Doors { get; set; }

        public bool UniqueDoors { get; set; }

        public string Brakes { get; set; }

        public decimal OriginalMsrp { get; set; }

        public int PassengerCapacity { get; set; }

        public bool UniquePassengerCount { get; set; }


        public static VehicleAttributes Fetch(string vin)
        {
            int vinPatternID = ChromeStyle.GetVinPatternIdForVin(vin);

            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getVinDecodedVehicleAttributes";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "VinPatternID", vinPatternID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.Read())
                        {
                            var vehAttr = new VehicleAttributes();
                            //add when new best score or same...b/c we just set to same if less than
                            vehAttr.Engine = reader.GetString(reader.GetOrdinal("VehicleEngine"));
                            vehAttr.FuelType = reader.GetString(reader.GetOrdinal("FuelType"));

                            return vehAttr;
                        }

                        throw new ApplicationException("VinPatternID issue");
                    }
                }
            }
        }

        public static VehicleAttributes Fetch(int chromeStyleId, string vin)
        {
            if ( chromeStyleId < 0 || chromeStyleId >= FakeChromeStyle.FakeChromeStyleId )
            {
                return new VehicleAttributes();
            }
            int vinPatternID = ChromeStyle.GetVinPatternIdForVin(vin);


            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getVehicleAttributes";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //multiForcedInducsFlag NOT USED
                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "VinPatternID", vinPatternID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var vehAttr = new VehicleAttributes();
                        if (reader.Read())
                        {
                            vehAttr.CFBodyType = reader.GetString(reader.GetOrdinal("cfbodytype"));
                            vehAttr.UniqueDoors = 1 <= reader.GetInt32(reader.GetOrdinal("multiDoorsFlag"));
                            vehAttr.UniqueDrivetrain = 1 <= reader.GetInt32(reader.GetOrdinal("multiDrivetrainFlag"));
                            vehAttr.UniqueEngine = 1 <= reader.GetInt32(reader.GetOrdinal("multiEngineFlag"));
                            vehAttr.UniqueFuelType = 1 <= reader.GetInt32((reader.GetOrdinal("multiFuelFlag")));
                            vehAttr.UniquePassengerCount = 1 <= reader.GetInt32(reader.GetOrdinal("multiCapacityFlag"));
                            vehAttr.UniqueTransmission = 1 <= reader.GetInt32(reader.GetOrdinal("multiTransFlag"));
                        }
                        if (reader.NextResult())
                        {
                            if (reader.Read())
                            {
                                vehAttr.Engine = reader.GetString(reader.GetOrdinal("VehicleEngine"));
                                if( ! reader.IsDBNull(reader.GetOrdinal("CFDriveTrain")))
                                    vehAttr.Drivetrain = reader.GetString(reader.GetOrdinal("CFDriveTrain"));
                                vehAttr.Transmission = reader.GetString(reader.GetOrdinal("VehicleTransmission"));
                                vehAttr.FuelType = reader.GetString(reader.GetOrdinal("FuelType"));
                                vehAttr.Doors = reader.GetInt32(reader.GetOrdinal("PassengerDoors"));
                                vehAttr.OriginalMsrp = (decimal) reader.GetFloat(reader.GetOrdinal("MSRP"));
                                if( ! reader.IsDBNull(reader.GetOrdinal("PassengerCapacity")))
                                    vehAttr.PassengerCapacity = reader.GetInt32(reader.GetOrdinal("PassengerCapacity"));
                            }
                        }

                        return vehAttr;
                    }
                }
            }
        }

        public string CFBodyType
        {
            get { return cfbodytype; }
            set { cfbodytype = value; }
        }
    }
}