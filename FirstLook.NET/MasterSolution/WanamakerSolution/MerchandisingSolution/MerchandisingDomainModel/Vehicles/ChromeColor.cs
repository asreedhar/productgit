using System;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for ChromeColor
    /// </summary>
    [Serializable]
    public class ChromeColor
    {
        public string Description { get; set; }

        //used for css display of color
        public string Color { get; set; }
        public string GenericColor { get; set; }
        public string ColorCode { get; set; }


        public ChromeColor(string description, string color, string colorCode, string genericColor)
        {            
            Description = description;
            Color = color;
            ColorCode = colorCode;
            GenericColor = genericColor;
        }
        public ChromeColor(string description, string rgbcolor, string genericColorName, string mfrColorCode, string altColorCode)
        {
            Description = description;
            GenericColor = genericColorName;

            Color = String.IsNullOrEmpty(rgbcolor) ? genericColorName : "#" + rgbcolor;
            ColorCode = String.IsNullOrEmpty(mfrColorCode) ? altColorCode : mfrColorCode;
        }
    }
}