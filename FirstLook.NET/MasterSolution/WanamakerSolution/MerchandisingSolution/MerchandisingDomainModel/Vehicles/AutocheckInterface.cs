using FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class AutoCheckInterface
    {
        private const int ONE_OWNER = 1;
        private const int ASSURED = 2;
        private const int NO_TOTAL_LOSS_REPORTED = 3;
        private const int NO_FRAME_DAMAGE_REPORTED = 4;
        private const int NO_AIRBAG_DEPLOYMENT_REPORTED = 5;
        private const int NO_ODOMETER_ROLLBACK_REPORTED = 6;
        private const int NO_ACCIDENTS_OR_DAMAGE_REPORTED = 7;
        private const int NO_MANUFACTURER_RECALLS_REPORTED = 8;
        private readonly int _maxScore;
        private readonly int _minScore;

        public AutoCheckInterface(int businessUnitId, string vin, string requestingUserLogin)
        {
            var service = new AutoCheckWebService();

            var identity = new UserIdentity();

            identity.UserName = requestingUserLogin;

            service.UserIdentityValue = identity;
            try
            {
                AutoCheckReportTO report = service.GetReport(businessUnitId, vin);

                if (report != null)
                {
                    Score = report.Score;
                    _minScore = report.CompareScoreRangeLow;
                    _maxScore = report.CompareScoreRangeHigh;

                    foreach (AutoCheckReportInspectionTO inspectionTO in report.Inspections)
                    {
                        switch (inspectionTO.Id)
                        {
                            case ONE_OWNER:
                                IsOneOwner = inspectionTO.Selected;
                                break;
                            case ASSURED:
                                IsAssured = inspectionTO.Selected;
                                break;
                            case NO_TOTAL_LOSS_REPORTED:
                                HasNoTotalLossReported = inspectionTO.Selected;
                                break;
                            case NO_FRAME_DAMAGE_REPORTED:
                                HasNoFrameDamageReported = inspectionTO.Selected;
                                break;
                            case NO_AIRBAG_DEPLOYMENT_REPORTED:
                                HasNoAirbagDeploymentReported = inspectionTO.Selected;
                                break;
                            case NO_ODOMETER_ROLLBACK_REPORTED:
                                HasNoOdometerRollbackReported = inspectionTO.Selected;
                                break;
                            case NO_ACCIDENTS_OR_DAMAGE_REPORTED:
                                HasNoAccidentsOrDamageReported = inspectionTO.Selected;
                                break;
                            case NO_MANUFACTURER_RECALLS_REPORTED:
                                HasNoManufacturerRecallsReported = inspectionTO.Selected;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch
            {
                // doh.  zb
            }
        }

        public bool HasGoodScore
        {
            get { return Score > (_minScore + _maxScore)/2; }
        }

        public int Score { get; set; }

        public bool IsOneOwner { get; private set; }


        public bool IsCleanReport
        {
            get
            {
                return HasNoAccidentsOrDamageReported
                       && HasNoAirbagDeploymentReported
                       && HasNoFrameDamageReported
                       && HasNoManufacturerRecallsReported
                       && HasNoOdometerRollbackReported
                       && HasNoTotalLossReported;
            }
        }

        public bool IsAssured { get; private set; }


        public bool HasNoTotalLossReported { get; private set; }

        public bool HasNoFrameDamageReported { get; private set; }

        public bool HasNoAirbagDeploymentReported { get; private set; }

        public bool HasNoOdometerRollbackReported { get; private set; }

        public bool HasNoAccidentsOrDamageReported { get; private set; }

        public bool HasNoManufacturerRecallsReported { get; private set; }
    }
}