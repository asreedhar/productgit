namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface ICategorizedEquipment
    {
        int GetCategoryId();
    }
}
