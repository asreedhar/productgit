﻿

using System.Data;
using FirstLook.Common.Data;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Templating;


namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    internal class OptionPackageRulesRepository : IOptionPackageRulesRepository
    {
        public IEnumerable<OptionPackageRule> GetRulesForVehicle(int businessunitId, int inventoryId)
        {

            var optionPackageRules = new List<OptionPackageRule>();

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "[templates].[getOptionCodeRulesForVehicle]";

                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessunitId, DbType.Int32);

                    Database.AddRequiredParameter(cmd, "InventoryID", inventoryId, DbType.Int32);


                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var rule = PopulateStatusFromReader(reader);
                            optionPackageRules.Add(rule);
                        }

                    }
                }
            }
            
            return optionPackageRules;
        }

        private static OptionPackageRule PopulateStatusFromReader(IDataReader rdr)
        {
            var rule = new OptionPackageTextReplacementRule();

            //change to create rule type here
            if (rdr.ColumnExists("ruleId")) rule.RuleId = rdr.GetInt32(rdr.GetOrdinal("ruleId"));

            if (rdr.ColumnExists("inventoryType")) rule.AppliesToInventoryType = rdr.GetInt32(rdr.GetOrdinal("inventoryType"));

            if (rdr.ColumnExists("textToReplace")) rule.TextToReplace = rdr.GetString(rdr.GetOrdinal("textToReplace"));

            if (rdr.ColumnExists("textToReplaceWith")) rule.TextToReplaceWith = rdr.GetString(rdr.GetOrdinal("textToReplaceWith"));

            if (rdr.ColumnExists("optionCode")) rule.OptionCode = rdr.GetString(rdr.GetOrdinal("optionCode"));


            return rule;

        }
    }
}
