using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class CustomStandardEquipment
    {
        #region Properties

        /// <summary>
        /// Identifies the business unit this custom description belongs to.
        /// </summary>
        public int BusinessUnitId { get; set; }

        /// <summary>
        /// Identifies if this custom description is for new vehicles (1) or used vehicles (2).
        /// </summary>
        public int UsedNew { get; set; }

        /// <summary>
        /// Identifies the option code that this custom description applies to.
        /// </summary>
        public string OptionCode { get; set; }

        /// <summary>
        /// The custom description text.
        /// </summary>
        public string CustomText { get; set; }

        #endregion

        #region Constructor
        public CustomStandardEquipment()
        {
            BusinessUnitId = 0;
            UsedNew = 0;
            OptionCode = String.Empty;
            CustomText = String.Empty;
        }
        public CustomStandardEquipment(int businessUnitId)
        {
            BusinessUnitId = businessUnitId;
            UsedNew = 0;
            OptionCode = String.Empty;
            CustomText = String.Empty;
        }
        public CustomStandardEquipment(int businessUnitId, int usedNew, string optionCode, string customText)
        {
            BusinessUnitId = businessUnitId;
            UsedNew = usedNew;
            OptionCode = optionCode;
            CustomText = customText;
        }
        internal CustomStandardEquipment(IDataRecord reader)
            : this(Convert.ToInt32(reader["businessUnitId"]), Convert.ToInt32(reader["UsedNew"]), Convert.ToString(reader["OptionCode"]), Convert.ToString(reader["customText"]))
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Saves this custom description to the database.
        /// </summary>
        public void Save()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.CustomStandardEquipment#Upsert";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "UsedNew", UsedNew, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OptionCode", OptionCode, DbType.String);
                    Database.AddRequiredParameter(cmd, "customText", CustomText, DbType.String);

                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// Deletes this custom description from the database.
        /// </summary>
        public void Delete()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.CustomStandardEquipment#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "businessUnitId", BusinessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "UsedNew", UsedNew, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OptionCode", OptionCode, DbType.String);

                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Fetches a list of custom descriptions from the database for the specified business unit.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <returns></returns>
        public static List<CustomStandardEquipment> FetchCustomStandardEquipment(int businessUnitId)
        {
            List<CustomStandardEquipment> retlist = new List<CustomStandardEquipment>();
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.CustomStandardEquipment#Fetch";

                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read()) //if it has a record...return it
                        {
                            retlist.Add(new CustomStandardEquipment(reader));
                        }
                    }
                }
            }
            return retlist;
        }
        /// <summary>
        /// Gets the specified custom description for this business unit if it exists.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="usedNew"></param>
        /// <param name="optionCode"></param>
        /// <returns></returns>
        public static string GetCustomStandardEquipmentDescription(int businessUnitId, int usedNew, string optionCode)
        {
            string customText = String.Empty;

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Settings.GetCustomStandardEquipmentDesc";

                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "businessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "UsedNew", usedNew, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "OptionCode", optionCode, DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        customText = reader.Read() ? Convert.ToString(reader["customText"]) : String.Empty;
                    }
                }
            }

            return customText;
        }
        #endregion
    }
}
