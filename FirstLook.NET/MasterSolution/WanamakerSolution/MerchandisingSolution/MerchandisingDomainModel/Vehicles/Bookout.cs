using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{

    public enum BookoutValueType
    {
        Undefined = 0,
        BaseValue,
        FinalValue,
        MileageIndependent,
        OptionsValue

    }

    public enum BookoutCategory
    {
        Undefined = 0,
        NadaCleanRetail,    //NADA
        NadaCleanTradeIn,   //NADA
        NadaCleanLoan,      //NADA
        BlackBookExtraClean,     //BlkB
        BlackBookClean,          //BlkB
        BlackBookAverage,        //BlkB
        BlackBookRough,          //BlkB
        KbbWholesale,      //KBB
        KbbRetail,         //KBB
        GalvesMarketReady,    //Galves
        KbbTradeIn,     //KBB
        KbbPrivateParty,   //KBB
        GalvesTradeIn,  //Galves
        BlackBookFinanceAdvance, //BlkB
        NadaAverageTradeIn, //NADA
        NadaRoughTradeIn,    //NADA
        KbbTradeInRangeLow, // kbb - FB:28818
        KbbTradeInRangeHigh // kbb - FB:28818


    }
    public enum BookoutProvider
    {
        Undefined = 0,
        BlackBook,
        NADA,
        KBB,
        Galves,
        Edmunds

    }

    #region PriceProofPoints

    public enum ThresholdColors
    {
        [Description("Red")]
        Red = 0,
        [Description("Red/Yellow")]
        RedYellow = 1
    }

    public enum BookType
    {
        [Description("N/A")]
        Na = 0,
        [Description("Primary")]
        Primary = 1,
        [Description("Secondary")]
        Secondary = 2,

    }

    #endregion
    public class Bookout
    {
        private BookoutValueType valueType;
        private decimal bookValue;
        private bool isAccurate;
        private bool isValid;
        private BookoutProvider provider;
        private BookoutCategory providerCategory;
        private decimal edmundsValue;

        public BookoutProvider Provider
        {
            get { return provider; }
            set { provider = value; }
        }

        public BookoutCategory ProviderCategory
        {
            get { return providerCategory; }
            set { providerCategory = value; }
        }

        internal Bookout()
        {

        }
        public Bookout(BookoutValueType valueType, decimal bookValue, bool isAccurate, bool isValid, BookoutProvider provider, BookoutCategory providerCategory)
        {
            this.valueType = valueType;
            this.bookValue = bookValue;
            this.isAccurate = isAccurate;
            this.isValid = isValid;
            this.provider = provider;
            this.providerCategory = providerCategory;
        }


        internal Bookout(IDataRecord reader)
        {
            valueType = (BookoutValueType)reader.GetByte(reader.GetOrdinal("bookoutValueTypeId"));
            bookValue = Database.GetNullableDecimal(reader, "bookValue").GetValueOrDefault(0);
            isAccurate = reader.GetByte(reader.GetOrdinal("isAccurate")) > 0;
            isValid = reader.GetByte(reader.GetOrdinal("isValid")) > 0;
            providerCategory = (BookoutCategory)reader.GetInt32(reader.GetOrdinal("ThirdPartyCategoryId"));
            provider = GetProviderFromMap(providerCategory);

        }
        public static Dictionary<string, BookoutCategory> FetchValidDisplayCategories()
        {
            Dictionary<string, BookoutCategory> retVal = new Dictionary<string, BookoutCategory>();
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.BlackBookRough), BookoutCategory.BlackBookRough);
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.BlackBookAverage), BookoutCategory.BlackBookAverage);
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.BlackBookClean), BookoutCategory.BlackBookClean);
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.BlackBookExtraClean), BookoutCategory.BlackBookExtraClean);
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.NadaCleanRetail), BookoutCategory.NadaCleanRetail);
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.KbbWholesale), BookoutCategory.KbbWholesale);
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.KbbRetail), BookoutCategory.KbbRetail);
            retVal.Add(GetBookoutCategoryInternalDescription(BookoutCategory.GalvesMarketReady), BookoutCategory.GalvesMarketReady);
            return retVal;
        }
        public static BookoutProvider GetProviderFromMap(BookoutCategory category)
        {
            switch (category)
            {
                case BookoutCategory.NadaCleanLoan:
                case BookoutCategory.NadaCleanRetail:
                case BookoutCategory.NadaCleanTradeIn:
                case BookoutCategory.NadaAverageTradeIn:
                case BookoutCategory.NadaRoughTradeIn:
                    return BookoutProvider.NADA;

                case BookoutCategory.BlackBookAverage:
                case BookoutCategory.BlackBookClean:
                case BookoutCategory.BlackBookExtraClean:
                case BookoutCategory.BlackBookFinanceAdvance:
                case BookoutCategory.BlackBookRough:
                    return BookoutProvider.BlackBook;


                case BookoutCategory.KbbTradeIn:
                case BookoutCategory.KbbWholesale:
                case BookoutCategory.KbbPrivateParty:
                case BookoutCategory.KbbRetail:
                case BookoutCategory.KbbTradeInRangeLow: // FB:28818
                case BookoutCategory.KbbTradeInRangeHigh: // FB:28818
                    return BookoutProvider.KBB;

                case BookoutCategory.GalvesMarketReady:
                case BookoutCategory.GalvesTradeIn:
                    return BookoutProvider.Galves;

                case BookoutCategory.Undefined:
                    throw new ApplicationException("Bookout category undefined");
                default:
                    throw new ApplicationException("Bookout category not mapped to any provider");

            }
        }
        public static string GetBookoutCategoryInternalDescription(BookoutCategory category)
        {
            switch (category)
            {
                case BookoutCategory.NadaCleanLoan:
                    return "NADA Clean Loan";
                case BookoutCategory.NadaCleanRetail:
                    return "NADA Clean Retail";
                case BookoutCategory.NadaCleanTradeIn:
                    return "NADA Clean Trade-In";
                case BookoutCategory.NadaAverageTradeIn:
                    return "NADA Average Trade-In";
                case BookoutCategory.NadaRoughTradeIn:
                    return "NADA Rough Trade-In";

                case BookoutCategory.BlackBookAverage:
                    return "Black Book Average";
                case BookoutCategory.BlackBookClean:
                    return "Black Book Clean";
                case BookoutCategory.BlackBookExtraClean:
                    return "Black Book Extra Clean";
                case BookoutCategory.BlackBookFinanceAdvance:
                    return "Black Book Finance Advance";
                case BookoutCategory.BlackBookRough:
                    return "Black Book Rough";

                case BookoutCategory.KbbTradeIn:
                    return "KBB Trade-In";
                case BookoutCategory.KbbWholesale:
                    return "KBB Wholesale";
                case BookoutCategory.KbbPrivateParty:
                    return "KBB Private Party";
                case BookoutCategory.KbbRetail:
                    return "KBB Retail";
                case BookoutCategory.KbbTradeInRangeLow:
                    return "KBB Trade-In + RangeLow";
                case BookoutCategory.KbbTradeInRangeHigh:
                    return "KBB Trade-In + RangeHigh";

                case BookoutCategory.GalvesMarketReady:
                    return "Galves Market Ready";
                case BookoutCategory.GalvesTradeIn:
                    return "Galves Trade-In";


                case BookoutCategory.Undefined:
                    throw new ApplicationException("Bookout category undefined");
                default:
                    throw new ApplicationException("Bookout category not mapped to any provider");

            }
        }
        public static string GetBookoutCategoryConsumerDescription(BookoutCategory category)
        {
            switch (category)
            {
                case BookoutCategory.NadaCleanLoan:
                    return "NADA Loan";
                case BookoutCategory.NadaCleanRetail:
                    return "NADA Retail";
                case BookoutCategory.NadaCleanTradeIn:
                    return "NADA Trade-in";
                case BookoutCategory.NadaAverageTradeIn:
                    return "NADA Trade-in";
                case BookoutCategory.NadaRoughTradeIn:
                    return "NADA Trade-in";

                case BookoutCategory.BlackBookAverage:
                    return "Black Book";
                case BookoutCategory.BlackBookClean:
                    return "Black Book";
                case BookoutCategory.BlackBookExtraClean:
                    return "Black Book";
                case BookoutCategory.BlackBookFinanceAdvance:
                    return "Black Book";
                case BookoutCategory.BlackBookRough:
                    return "Black Book";

                case BookoutCategory.KbbTradeIn:
                    return "Kelley Blue Book";
                case BookoutCategory.KbbWholesale:
                    return "Kelley Blue Book";
                case BookoutCategory.KbbPrivateParty:
                    return "Kelley Blue Book";
                case BookoutCategory.KbbRetail:
                    return "Kelley Blue Book";
                case BookoutCategory.KbbTradeInRangeLow:
                    return "Kelley Blue Book";
                case BookoutCategory.KbbTradeInRangeHigh:
                    return "Kelley Blue Book";

                case BookoutCategory.GalvesMarketReady:
                    return "Galves";
                case BookoutCategory.GalvesTradeIn:
                    return "Galves";


                case BookoutCategory.Undefined:
                    throw new ApplicationException("Bookout category undefined");
                default:
                    throw new ApplicationException("Bookout category not mapped to any provider");

            }
        }

        public static bool HasAccurateBookoutValue(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.hasAccurateBookout";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);

                    int valid = Convert.ToInt32(cmd.ExecuteScalar());
                    return valid > 0;
                }
            }
        }

        public static Bookout Fetch(int businessUnitID, int inventoryId, BookoutCategory providerCategory)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getLatestBookoutValue";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ValueType", BookoutValueType.FinalValue, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ThirdPartyCategoryId", (int)providerCategory, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new Bookout(reader);
                        }

                        if (providerCategory == BookoutCategory.KbbRetail)
                        {
                            var bookout = GetKbbConsumerValueBookout(businessUnitID, inventoryId);
                            if (bookout != null) return bookout;
                        }

                        throw new ApplicationException("No bookout found for given parameters");
                    }

                }
            }

        }

        public static Bookout FetchKbbConsumer(int businessUnitID, int inventoryId)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getLatestKbbConsumerValue";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);


                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            decimal? bookValue = Database.GetNullableInt(reader, "BookValue");
                            return new Bookout() { isAccurate = true, isValid = true, BookValue = bookValue == null ? 0 : (decimal)bookValue };
                        }

                        throw new ApplicationException("No bookout found for given parameters");
                    }

                }
            }

        }

        public static Bookout Fetch(int businessUnitID, int inventoryId)
        {

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getLatestEdmundsValue";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);


                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int? tmvValue = Database.GetNullableInt(reader, "edmundsTMV");
                            return new Bookout() { EdmundsValue = tmvValue == null ? 0 : (int)tmvValue };
                        }

                        throw new ApplicationException("No bookout found for given parameters");
                    }

                }
            }

        }

        internal static Bookout GetKbbConsumerValueBookout(int businessUnitID, int inventoryId)
        {
            var pricingContext = PricingContext.FromMerchandising(businessUnitID, inventoryId);
            KelleyBlueBookReport kbbReport = KelleyBlueBookReportCommand.TryFetch(pricingContext.OwnerHandle, pricingContext.VehicleHandle);
            if (kbbReport != null && kbbReport.Valuations.Count > 0)
            {
                int kbbPrice = kbbReport.Valuations.GetValue(KelleyBlueBookCategory.Retail, KelleyBlueBookCondition.NotApplicable).Value;
                return new Bookout(BookoutValueType.FinalValue, kbbPrice, true, true, GetProviderFromMap(BookoutCategory.KbbRetail), BookoutCategory.KbbRetail);
            }
            return null;
        }

        public BookoutValueType ValueType
        {
            get { return valueType; }
            set { valueType = value; }
        }

        public decimal BookValue
        {
            get { return bookValue; }
            set { this.bookValue = value; }
        }

        public bool IsAccurate
        {
            get { return isAccurate; }
            set { isAccurate = value; }
        }

        public bool IsValid
        {
            get { return isValid; }
            set { isValid = value; }
        }

        public decimal EdmundsValue
        {
            get { return edmundsValue; }
            set { edmundsValue = value; }
        }
    }
}
