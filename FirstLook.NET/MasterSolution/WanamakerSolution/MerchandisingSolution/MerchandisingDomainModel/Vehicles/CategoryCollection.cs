using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Data;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class CategoryCollection : CollectionBase
    {
        private const decimal MIN_OPTION_PRICE_TO_SHOW_EXTRA_INFO = 300.0M;

        public CategoryCollection(string headerText)
        {
            HeaderText = headerText;
        }

        public string HeaderText { get; set; }

        public bool Includes(string description)
        {
            return this.Cast<CategoryLink>().Any(cl => cl.Description == description);
        }

        public static CategoryCollection GetCategoriesFromEquipment(EquipmentCollection equipment,
                                                                    Hashtable globalCategories)
        {
            var retColl = new CategoryCollection("Included Equipment");
            List<int> categories = equipment.ProcessCategories();
            foreach (int catID in categories)
            {
                if (!retColl.Contains(catID))
                {
                    retColl.Add((CategoryLink) globalCategories[catID]);
                }
            }
            return retColl;
        }

        public void Add(CategoryCollection collection)
        {
            foreach (CategoryLink link in collection)
            {
                Add(link);
            }
        }

        public int Add(CategoryLink categ)
        {
            return List.Add(categ);
        }

        public void Insert(int index, CategoryLink categ)
        {
            List.Insert(index, categ);
        }

        public CategoryLink Item(int index)
        {
            return (CategoryLink) List[index];
        }

        public CategoryCollection GetCategoryRemoves()
        {
            var categoryCollection = new CategoryCollection("");
            foreach (CategoryLink cl in List)
            {
                if (cl.CategoryAction.ToLower() == "d")
                {
                    categoryCollection.Add(cl);
                }
            }
            return categoryCollection;
        }

        public CategoryCollection GetCategoryAdds()
        {
            var categoryCollection = new CategoryCollection("");
            foreach (CategoryLink cl in List)
            {
                if (cl.CategoryAction.ToLower() == "c" || cl.CategoryAction.ToLower() == "p")
                {
                    categoryCollection.Add(cl);
                }
            }
            return categoryCollection;
        }

        //determine if the list contains a specified code
        public bool Contains(int categoryID)
        {
            return this.Cast<CategoryLink>().Any(cl => cl.CategoryID == categoryID);
        }

        public int GetIdByDescription(string description)
        {
            foreach (CategoryLink cl in List)
            {
                if (cl.Description == description)
                {
                    return cl.CategoryID;
                }
            }
            return 0;
        }


        public List<CategoryCollection> FilterIntoDisplaySections()
        {
            //get lookup table of categoryid and categoryheadername

            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    //first try the database for carfax data
                    cmd.CommandText = "chrome.getKeyDisplayCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        //create a hashtable
                        var lookup = new Dictionary<int, string>();
                        while (reader != null && reader.Read())
                        {
                            lookup.Add(reader.GetInt32(reader.GetOrdinal("categoryID")),
                                       reader.GetString(reader.GetOrdinal("categoryHeader")));
                        }

                        var dict = new Dictionary<string, CategoryCollection>();
                        foreach (CategoryLink cl in this)
                        {
                            if (lookup.ContainsKey(cl.CategoryID))
                            {
                                string header = lookup[cl.CategoryID];
                                if (!dict.ContainsKey(header))
                                {
                                    dict.Add(header, new CategoryCollection(header));
                                }
                                dict[header].Add(cl);
                            }
                        }

                        return new List<CategoryCollection>(dict.Values);
                    }
                }
            }
        }

        public CategoryCollection GetByHeaderID(int headerID)
        {
            var cc = new CategoryCollection("");
            foreach (CategoryLink cl in List)
            {
                if (cl.HeaderID == headerID)
                {
                    cc.HeaderText = cl.Header;
                    cc.Add(cl);
                }
            }
            return cc;
        }

        public CategoryLink GetByCategoryID(int categoryID)
        {
            foreach (CategoryLink cl in List)
            {
                if (cl.CategoryID == categoryID)
                {
                    return cl;
                }
            }
            throw new ApplicationException("Category not in list, check for existence first.");
        }

        public void RemoveByCategoryID(int categoryID)
        {
            if (Contains(categoryID))
            {
                List.Remove(GetByCategoryID(categoryID));
            }
        }

        public CategoryCollection GetByCategoryIdList(List<int> categoryIDs)
        {
            var categoryCollection = new CategoryCollection("");
            foreach (CategoryLink cl in List)
            {
                if (categoryIDs.Contains(cl.CategoryID))
                {
                    categoryCollection.Add(cl);
                }
            }
            return categoryCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="availableEquipment"></param>
        /// <param name="selectedCategoryIDs">List of integer values for category IDs</param>
        public void SetAssociatedInformation(EquipmentCollection availableEquipment, List<int> selectedCategoryIDs)
        {
            if (selectedCategoryIDs == null) return;

            foreach (CategoryLink cl in List)
            {
                var stB = new StringBuilder();

                foreach (Equipment eq in availableEquipment)
                {
                    if (!eq.IncludesCategoryID(cl.CategoryID)) continue;
                    
                    if (eq.MaxMSRP> MIN_OPTION_PRICE_TO_SHOW_EXTRA_INFO)
                    {
                        stB.AppendLine(eq.Description + " " + String.Format("{0:c}", eq.MaxMSRP));
                    }
                    else
                    {
                        stB.AppendLine(eq.Description);
                    }
                }
                cl.AssociatedInfo = stB.ToString();

                if (selectedCategoryIDs.Contains(cl.CategoryID))
                {
                    cl.IsSelected = true;
                }
            }
        }

        public List<string> GetDescriptionList()
        {
            return (from CategoryLink cl in List select cl.Description).ToList();
        }

        private static Dictionary<int, List<int>> GetExclusionMap()
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.getCategoryExclusionMap";
                    cmd.CommandType = CommandType.StoredProcedure;


                    var exclusionMap = new Dictionary<int, List<int>>();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int dispID = reader.GetInt32(reader.GetOrdinal("displayCategoryID"));
                            int elimID = reader.GetInt32(reader.GetOrdinal("eliminateCategoryID"));

                            if (!exclusionMap.ContainsKey(dispID))
                            {
                                exclusionMap.Add(dispID, new List<int>());
                            }
                            exclusionMap[dispID].Add(elimID);
                        }

                        return exclusionMap;
                    }
                }
            }
        }

        public CategoryCollection GetAllByTypeFilter(string typeFilter)
        {
            var categoryCollection = new CategoryCollection(typeFilter);
            foreach (CategoryLink cl in this)
            {
                if (cl.TypeFilter == typeFilter)
                {
                    categoryCollection.Add(cl);
                }
            }
            return categoryCollection;
        }

        public static List<int> GetCategoryRemovesMap(int categoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                const string QUERY = @"SELECT displayCategoryID 
                                FROM templates.EquipmentExclusionMap
                                WHERE eliminateCategoryID = @CategoryID";
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = QUERY;
                    cmd.CommandType = CommandType.Text;
                    var exclusionMap = new List<int>();
                    Database.AddRequiredParameter(cmd, "@CategoryID", categoryId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int dispID = reader.GetInt32(reader.GetOrdinal("displayCategoryID"));
                            exclusionMap.Add(dispID);
                        }
                        return exclusionMap;
                    }
                }
            }
        }

        private CategoryCollection ExecuteTypeFilters()
        {
            var retCol = new CategoryCollection("");

            //first copy all over...
            foreach (CategoryLink cl in this)
            {
                retCol.Add(cl);
            }

            foreach (CategoryLink cl in this)
            {
                if (String.IsNullOrEmpty(cl.TypeFilter))
                {
                    continue;
                }

                //if it has a typeFilter, though, we want to remove all other following categories with same type filter
                if (retCol.Contains(cl.CategoryID))
                {
                    CategoryCollection cc = GetAllByTypeFilter(cl.TypeFilter);
                    if (cc.Count >= 2)
                    {
                        for (int i = 1; i < cc.Count; i++)
                        {
                            retCol.List.Remove(cc.Item(i));
                        }
                    }
                    //retCol.RemoveByFilter(cl.TypeFilter, cl.CategoryID);
                    //it's still in the return list
                    //so remove all others like it
                }
            }
            return retCol;
        }


        public CategoryCollection FilterExclusiveCategories()
        {
            var retCol = new CategoryCollection("");
            Dictionary<int, List<int>> exclusionMap = GetExclusionMap();


            //first copy all over...
            foreach (CategoryLink cl in this)
            {
                retCol.Add(cl);
            }

            retCol = retCol.ExecuteTypeFilters();

            //now use the hashtable to remove categories that should not appear
            foreach (CategoryLink cl in this)
            {
                if (retCol.Contains(cl.CategoryID) && (exclusionMap.ContainsKey(cl.CategoryID)))
                {
                    List<int> elimCatIDs = exclusionMap[cl.CategoryID];
                    foreach (int catID in elimCatIDs)
                    {
                        retCol.RemoveByCategoryID(catID);
                    }
                }
            }
            return retCol;
        }

        public static List<int> FilterExclusiveCategoryIds(List<int> categoryIDs)
        {
            var retList = new List<int>();

            Dictionary<int, List<int>> exclusionMap = GetExclusionMap();

            //now use the hashtable to remove categories that should not appear
            foreach (int id in categoryIDs)
            {
                retList.Add(id);
            }

            foreach (int id in categoryIDs)
            {
                if (retList.Contains(id) && (exclusionMap.ContainsKey(id)))
                {
                    List<int> elimCatIDs = exclusionMap[id];
                    foreach (int catID in elimCatIDs)
                    {
                        retList.Remove(catID);
                    }
                }
            }
            return retList;
        }
    }

    public class CategoryCollectionListLengthOrderer : IComparer<CategoryCollection>
    {
        // Calls CaseInsensitiveComparer.Compare with the parameters reversed.

        #region IComparer<CategoryCollection> Members

        public int Compare(CategoryCollection x, CategoryCollection y)
        {
            return y.Count.CompareTo(x.Count);
        }

        #endregion
    }
}