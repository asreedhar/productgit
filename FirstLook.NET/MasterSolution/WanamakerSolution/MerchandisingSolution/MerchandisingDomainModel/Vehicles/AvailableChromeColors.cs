using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for AvailableChromeColors
    /// </summary>
    /// 
    [Serializable]
    public class AvailableChromeColors
    {
        public AvailableChromeColors()
        {
            ExteriorColors = new List<ColorPair>();
            InteriorColors = new List<ChromeColor>();
        }

        public List<ChromeColor> InteriorColors { get; set; }

        public List<ColorPair> ExteriorColors { get; set; }

        public static List<ColorPair> FetchExterior(int chromeStyleID)
        {
            return Fetch(chromeStyleID).ExteriorColors;
        }

        public static List<ColorPair> FetchExterior(int chromeStyleID, string currentExtColorDesc1,
                                                    string currentExtColorDesc2)
        {
            List<ColorPair> lcp = FetchExterior(chromeStyleID);

            if (!String.IsNullOrEmpty(currentExtColorDesc1))
            {
                if (
                    !lcp.Exists(
                        cp => cp.Matches(currentExtColorDesc1, currentExtColorDesc2)))
                {
                    lcp.Add(
                        new ColorPair(new ChromeColor(currentExtColorDesc1, String.Empty, String.Empty, String.Empty),
                                      new ChromeColor(currentExtColorDesc2, String.Empty, String.Empty, String.Empty)));
                }
            }
            return lcp;
        }

        public static List<ChromeColor> FetchInterior(int chromeStyleID)
        {
            return Fetch(chromeStyleID).InteriorColors;
        }

        public static AvailableChromeColors Fetch(int chromeStyleID)
        {
            var retColors = new AvailableChromeColors();
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand()) //new SqlCommand(sql, con))
                {
                    cmd.CommandText = "Chrome.getInteriorAndExteriorColorSets";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "styleID", chromeStyleID, DbType.Int32);

                    //Interior colors

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var color = new ChromeColor(
                                reader.GetString(reader.GetOrdinal("IntDesc")),
                                string.Empty, string.Empty,
                                reader.GetString(reader.GetOrdinal("IntManCode")),
                                reader.GetString(reader.GetOrdinal("IntCode"))
                                );

                            // DO NOT Filter this list to only contain unique descriptions!
                            // This code is used widely in the domain and the different codes are important.
                            // Filter it down in the UI if you need to.
                            retColors.InteriorColors.Add(color);
                        }

                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                var colorPair = new ColorPair(
                                    new ChromeColor(reader.GetString(reader.GetOrdinal("Ext1Desc")),
                                                    reader.GetString(reader.GetOrdinal("Ext1RGBHex")),
                                                    reader.GetString(reader.GetOrdinal("GenericExtColor")),
                                                    reader.GetString(reader.GetOrdinal("Ext1ManCode")),
                                                    reader.GetString(reader.GetOrdinal("Ext1Code")))
                                    ,
                                    new ChromeColor(reader.GetString(reader.GetOrdinal("Ext2Desc")),
                                                    reader.GetString(reader.GetOrdinal("Ext2RGBHex")),
                                                    reader.GetString(reader.GetOrdinal("GenericExt2Color")),
                                                    reader.GetString(reader.GetOrdinal("Ext2ManCode")),
                                                    reader.GetString(reader.GetOrdinal("Ext2Code")))
                                    );

                                // Do NOT filter this list to contain only unique descriptions.
                                // This code is used widely in the domain and the different codes are important.
                                // Filter it in the UI if you need to.
                                retColors.ExteriorColors.Add(colorPair);
                            }
                        }
                        return retColors;
                    }
                }
            }
        }



    }
}