namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class KeyInformationSettings
    {
        public bool HasKeyInformation(int businessUnitId)
        {
            var dataSource = new VehicleConfigurationDataSource();
            var keyInformation = dataSource.GetAdditionalInfoItems(businessUnitId);
            return (keyInformation != null) && (keyInformation.Count > 0);
        }
    }
}