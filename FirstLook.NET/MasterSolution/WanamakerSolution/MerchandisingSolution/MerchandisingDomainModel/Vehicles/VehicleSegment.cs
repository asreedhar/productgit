namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class VehicleSegment
    {
        public VehicleSegment(int segmentId, string segmentName)
        {
            SegmentId = segmentId;
            SegmentName = segmentName;
        }

        public int SegmentId { get; set; }

        public string SegmentName { get; set; }
    }
}