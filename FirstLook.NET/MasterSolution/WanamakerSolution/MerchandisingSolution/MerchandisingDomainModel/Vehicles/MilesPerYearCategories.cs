﻿namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum MilesPerYearCategories
    {
        VeryLow = 1,
        Low = 2,
        Average = 3,
        High = 4,
        Excesssive = 5
    }
}