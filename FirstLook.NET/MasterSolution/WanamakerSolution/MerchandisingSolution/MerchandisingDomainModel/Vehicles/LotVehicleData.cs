using System.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class LotVehicleData
    {
        internal LotVehicleData(IDataRecord record)
        {
            
        }
        
        public LotVehicleData()
        {
            OptionCodes = string.Empty;
            ExteriorColor = string.Empty;
            InteriorColor = string.Empty;
            Vin = string.Empty;
        }
        
        public LotVehicleData(string optionCodes, string exteriorColor, string interiorColor, int? chromeStyleId, string vin)
        {
            OptionCodes = optionCodes;
            ExteriorColor = exteriorColor;
            InteriorColor = interiorColor;
            ChromeStyleId = chromeStyleId;
            Vin = vin;
        }

        public string OptionCodes { get; set; }

        public string ExteriorColor { get; set; }

        public string InteriorColor { get; set; }

        public int? ChromeStyleId { get; set; }

        public string Vin { get; set; }
    }
}