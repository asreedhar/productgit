﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Logging;
using MvcMiniProfiler;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class OptionPackageRulesEngine
    {
        private readonly IOptionPackageRulesRepository _rulesRepo;
        private static readonly string AllOptionCode = new string('~', 20);  // all ~ is the "wildcard" option code (had to choose somehting)

        private static readonly ILog Log = LoggerFactory.GetLogger<OptionPackageRulesEngine>();


        public OptionPackageRulesEngine(IOptionPackageRulesRepository rulesRepository)
        {
            _rulesRepo = rulesRepository;
        }

        //We are passing the DetailedEquipmentCollection instead of each DetailedEquipment to save the DB calls.
        public void Process(int businessunitId, int inventoryId,  List<DetailedEquipment> optionPackages)
        {

            using (MiniProfiler.Current.Step("OptionPackageRulesEngine.Process(DetailEquipment)"))
            {

                if (optionPackages == null) return;

                var rules = _rulesRepo.GetRulesForVehicle(businessunitId, inventoryId);

                foreach (var optionPackageRule in rules)
                {
                    var ruleOptionCode = optionPackageRule.OptionCode;
                    List<DetailedEquipment> vehicleOptionPackages;

                    if (ruleOptionCode != AllOptionCode)
                        vehicleOptionPackages =
                            optionPackages.Where(x => x != null && x.OptionCode.ToUpper() == ruleOptionCode.ToUpper()).
                                ToList();
                    else
                        vehicleOptionPackages = optionPackages.Where(x => x != null).ToList();
                            // rule applies to all options!!!

                    foreach (var vehicleOptionPackage in vehicleOptionPackages)
                    {
                        optionPackageRule.ApplyRuleTo(vehicleOptionPackage);
                    }

                }
            }
        }

        //We are passing the DetailedEquipmentCollection instead of each DetailedEquipment to save the DB calls.
        public void Process(int businessunitId, int inventoryId, DetailedEquipmentCollection options)
        {
            try
            {
                if (options == null) return;
                Process(businessunitId, inventoryId, options.Cast<DetailedEquipment>().ToList());
            }
            catch (Exception ex)
            {
                //if the string replacement rules fail the approval page should not crash.
                Log.ErrorFormat("Error applying the rules to the options for {0} {1} {2}", businessunitId, inventoryId, ex.ToString());
            }
        }

        //We are passing the DetailedEquipmentCollection instead of each DetailedEquipment to save the DB calls.
        public void Process(int businessunitId, int inventoryId, EquipmentCollection options)
        {
            try
            {
                if (options == null) return;
                Process(businessunitId, inventoryId, options.Cast<Equipment>().ToList());

            }
            catch (Exception ex)
            {
                //if the string replacement rules fail the approval page should not crash.
                Log.ErrorFormat("Error applying the rules to the options for {0} {1} {2}", businessunitId, inventoryId, ex.ToString());
            }
        }


        //We are passing the DetailedEquipmentCollection instead of each DetailedEquipment to save the DB calls.
        private void Process(int businessunitId, int inventoryId, List<Equipment> optionPackages)
        {
            using (MiniProfiler.Current.Step("OptionPackageRulesEngine.Process(Equipment)"))
            {
                if (optionPackages == null) return;

                var rules = _rulesRepo.GetRulesForVehicle(businessunitId, inventoryId);

                foreach (var optionPackageRule in rules)
                {
                    var ruleOptionCode = optionPackageRule.OptionCode;
                    List<Equipment> vehicleOptionPackages;

                    if (ruleOptionCode != AllOptionCode)
                        vehicleOptionPackages =
                            optionPackages.Where(
                                x =>
                                x != null && x.EquipmentKind == Equipment.EquipmentKinds.EquipOpt &&
                                x.OptionCode.ToUpper() == ruleOptionCode.ToUpper()).ToList();
                    else
                        vehicleOptionPackages =
                            optionPackages.Where(x => x != null && x.EquipmentKind == Equipment.EquipmentKinds.EquipOpt)
                                .ToList(); // rule applies to all options!!!


                    foreach (var vehicleOptionPackage in vehicleOptionPackages)
                    {
                        optionPackageRule.ApplyRuleTo(vehicleOptionPackage);
                    }

                }
            }
        }
        
    }

   
}
