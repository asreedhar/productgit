using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Common.Core.Extensions;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public static class ChromeMsrpConditionHelpers
    {
        /// <summary>
        /// Get the processable option codes and conditions for a 
        /// </summary>
        /// <param name="chromeStyleId"></param>
        /// <param name="optionCodesList"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static IEnumerable<ChromeMsrpCondition> GetMsrpConditions(int chromeStyleId, IEnumerable<string> optionCodesList, int countryCode = 1)
        {
            var list = new List<ChromeMsrpCondition>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    var query = @"SELECT OptionCode, Condition, CONVERT(DECIMAL(10,2), Msrp) AS Msrp
                        FROM VehicleCatalog.Chrome.Prices
                        WHERE CountryCode = @countryCode AND StyleID = @chromeStyleId";

                    var codesList = optionCodesList as string[] ?? optionCodesList.ToArray();
                    if (codesList.Any())
                    {
                        query += string.Format(" AND OptionCode IN ('{0}')", codesList.ToDelimitedString("','"));
                    }

                    cmd.CommandText = query;
                    cmd.AddParameterWithValue("@countryCode", countryCode);
                    cmd.AddParameterWithValue("@chromeStyleId", chromeStyleId);


                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var cmc = new ChromeMsrpCondition
                            {
                                Condition = reader.GetString("Condition"),
                                OptionCode = reader.GetString("OptionCode"),
                                Msrp = reader.GetDecimal("Msrp")
                            };
                            list.Add(cmc);
                        }
                    }
                }
            }
            return list;
        }
    }
}