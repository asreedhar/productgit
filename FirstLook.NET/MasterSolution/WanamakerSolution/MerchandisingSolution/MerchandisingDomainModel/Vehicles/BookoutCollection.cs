using System;
using System.Collections;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class BookoutCollection : CollectionBase
    {

        public BookoutCollection()
        {
        }

        internal BookoutCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(new Bookout(reader));
            }
        }

        public Bookout GetBookout(BookoutCategory category)
        {
            foreach (Bookout bk in List)
            {
                if (bk.ProviderCategory == category && bk.ValueType == BookoutValueType.FinalValue)
                {
                    return bk;
                }
            }
            throw new ApplicationException("Bookout Category not found in collection");
        }

        public bool Contains(BookoutCategory category)
        {
            foreach (Bookout bk in List)
            {
                if (bk.ProviderCategory == category)
                {
                    return true;
                }
            }
            return false;
        }

        public decimal GetValue(BookoutCategory category, bool onlyAccurate, bool onlyValid)
        {
            //if found, return it!
            foreach (Bookout bk in List)
            {
                if (bk.ProviderCategory == category && 
                    (!onlyAccurate || bk.IsAccurate) && 
                    (!onlyValid || bk.IsValid))
                {
                    return bk.BookValue;
                }
            }

            return 0.0m;
        }

        public static BookoutCollection Fetch(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "templates.bookouts#fetch";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var bookouts = new BookoutCollection(reader);

                        var dealer = BusinessUnitFinder.Instance().Find(businessUnitId);
                        if (dealer.HasDealerUpgrade(Upgrade.KelleyBlueBookTradeInValues)
                            && ! bookouts.Contains(BookoutCategory.KbbRetail))
                        {
                            var bookout = Bookout.GetKbbConsumerValueBookout(businessUnitId, inventoryId);
                            if (bookout != null) bookouts.Add(bookout);
                        }

                        return bookouts;
                    }
                }
            }
        }
        public int Add(Bookout bookout)
        {
            return List.Add(bookout);
        }

        public void Remove(Bookout bookout)
        {
            List.Remove(bookout);
        }
    }
}
