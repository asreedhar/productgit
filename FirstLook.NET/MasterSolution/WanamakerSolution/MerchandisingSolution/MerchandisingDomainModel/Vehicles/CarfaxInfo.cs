using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Xml;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Templating;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for CarfaxInfo
    /// </summary>
    [Serializable]
    public class CarfaxInfo
    {
        private readonly Hashtable _carfaxText = new Hashtable();

        private readonly string[] _textNodeStrings = {
                                                        "Problem", "OwnershipText", "BBGText", "TotalLossText",
                                                        "FrameDamageText",
                                                        "AirbagDeploymentText", "OdometerRollbackText",
                                                        "AccidentIndicatorsText",
                                                        "ManufacturerRecallText"
                                                    };

        private DateTime _expiresOn;

        public CarfaxInfo()
        {
        }

        public CarfaxInfo(string responseCode, XmlDocument doc, string vin)
        {
            ResponseCode = responseCode;

            foreach (string desc in _textNodeStrings)
            {
                XmlNodeList xnl = doc.GetElementsByTagName(desc);
                if (xnl.Count > 0)
                {
                    _carfaxText.Add(desc, xnl[0].FirstChild.InnerText);
                }
            }

            XmlNodeList xnl2 = doc.GetElementsByTagName("Expiration");
            string expiry = string.Empty;
            if (xnl2.Count > 0)
            {
                expiry = xnl2[0].FirstChild.InnerText;
            }

            var dtfi = new DateTimeFormatInfo();
            dtfi.FullDateTimePattern = "MM/dd/yyyy";
            if (DateTime.TryParseExact(expiry, dtfi.FullDateTimePattern, dtfi, DateTimeStyles.None, out _expiresOn))
            {
                //fall back on three months from now
                _expiresOn = DateTime.Today.AddMonths(3);
            }

            VIN = vin;
        }

        //reader is already set to the record
        public CarfaxInfo(IDataRecord reader)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string colName = reader.GetName(i);
                switch (colName.ToLower())
                {
                    case "expireson":
                        _expiresOn = reader.GetDateTime(i);
                        break;
                    default:
                        _carfaxText.Add(colName, reader.GetString(i));
                        break;
                }
            }
        }

        public string ResponseCode { get; set; }

        public DateTime ExpiresOn
        {
            get { return _expiresOn; }
            set { _expiresOn = value; }
        }

        public bool IsValidReport
        {
            get { return _carfaxText.Count > 0; }
        }

        public string NumOwners
        {
            get
            {
                if (_carfaxText.Contains("OwnershipText"))
                {
                    return (string) _carfaxText["OwnershipText"];
                }
                return string.Empty;
            }
        }

        public string BuybackGuarantee
        {
            get
            {
                if (_carfaxText.Contains("BBGText"))
                {
                    return (string) _carfaxText["BBGText"];
                }
                return string.Empty;
            }
        }

        public string TotalLoss
        {
            get
            {
                if (_carfaxText.Contains("TotalLossText"))
                {
                    return (string) _carfaxText["TotalLossText"];
                }
                return string.Empty;
            }
        }

        public string FrameDamage
        {
            get
            {
                if (_carfaxText.Contains("FrameDamageText"))
                {
                    return (string) _carfaxText["FrameDamageText"];
                }
                return string.Empty;
            }
        }

        public string AirbagDeployment
        {
            get
            {
                if (_carfaxText.Contains("AirbagDeploymentText"))
                {
                    return (string) _carfaxText["AirbagDeploymentText"];
                }
                return string.Empty;
            }
        }

        public string OdometerRollback
        {
            get
            {
                if (_carfaxText.Contains("OdometerRollbackText"))
                {
                    return (string) _carfaxText["OdometerRollbackText"];
                }
                return string.Empty;
            }
        }

        public string AccidentIndicators
        {
            get
            {
                if (_carfaxText.Contains("AccidentIndicatorsText"))
                {
                    return (string) _carfaxText["AccidentIndicatorsText"];
                }
                return string.Empty;
            }
        }

        public string ManufacturerRecall
        {
            get
            {
                if (_carfaxText.Contains("ManufacturerRecallText"))
                {
                    return (string) _carfaxText["ManufacturerRecallText"];
                }
                return string.Empty;
            }
        }

        public bool IsCleanReport
        {
            get
            {
                return ProblemVehicle == "N";
                //false when "Y" or "U" -- U generated by FirstLook
            }
        }

        public string ProblemVehicle
        {
            get
            {
                if (_carfaxText.Contains("Problem"))
                {
                    return (string) _carfaxText["Problem"];
                }
                return "U";
            }
        }


        public string VIN { get; set; }

        public bool IsOneOwner()
        {
            return NumOwners.Contains("CARFAX 1-Owner");
        }

        public bool HasBuybackGuarantee()
        {
            return BuybackGuarantee.Length > 0;
        }

        public bool NoTotalLoss()
        {
            return TotalLoss.Length > 0;
        }

        public bool NoFrameDamage()
        {
            return FrameDamage.Length > 0;
        }

        public bool NoAirbagDeployment()
        {
            return AirbagDeployment.Length > 0;
        }

        public bool NoOdometerRollback()
        {
            return OdometerRollback.Length > 0;
        }

        public bool NoAccidentIndicators()
        {
            return AccidentIndicators.Length > 0;
        }

        public bool NoManufacturerRecall()
        {
            return ManufacturerRecall.Length > 0;
        }


        public static CarfaxInfo FetchOrCreate(int businessUnitId, string vin)
        {
            try
            {
                using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
                {
                    con.Open();

                    using (IDbCommand cmd = con.CreateCommand())
                    {
                        //first try the database for carfax data
                        cmd.CommandText = "builder.getCarfaxReport";
                        cmd.CommandType = CommandType.StoredProcedure;
                        Database.AddRequiredParameter(cmd, "VIN", vin, DbType.String);

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return new CarfaxInfo(reader);
                            }

                            //if nothing, go to carfax webservice
                            CarfaxInfo liveReport = RequestReportFromCarfax(businessUnitId, vin);

                            if (liveReport == null)
                            {
                                throw new ApplicationException("Could not create or retrieve carfax report for VIN:" +
                                                               vin);
                            }
                            //insert into db for future use
                            liveReport.Save();

                            return liveReport;
                        }
                    }
                }
            }
            catch
            {
                return new CarfaxInfo();
            }
        }

        public bool Save()
        {
            if (String.IsNullOrEmpty(VIN))
            {
                return false;
            }
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    //first try the database for carfax data
                    cmd.CommandText = "merchandising.createCarfaxReport";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "VIN", VIN, DbType.String);

                    Database.AddRequiredParameter(cmd, "Problem", ProblemVehicle, DbType.String);
                    Database.AddRequiredParameter(cmd, "OwnershipText", NumOwners, DbType.String);
                    Database.AddRequiredParameter(cmd, "BBGText", BuybackGuarantee, DbType.String);
                    Database.AddRequiredParameter(cmd, "TotalLossText", TotalLoss, DbType.String);
                    Database.AddRequiredParameter(cmd, "FrameDamageText", FrameDamage, DbType.String);
                    Database.AddRequiredParameter(cmd, "AirbagDeploymentText", AirbagDeployment, DbType.String);
                    Database.AddRequiredParameter(cmd, "OdometerRollbackText", OdometerRollback, DbType.String);
                    Database.AddRequiredParameter(cmd, "AccidentIndicatorsText", AccidentIndicators, DbType.String);
                    Database.AddRequiredParameter(cmd, "ManufacturerRecallText", ManufacturerRecall, DbType.String);
                    Database.AddRequiredParameter(cmd, "ExpiresOn", _expiresOn, DbType.DateTime);

                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }

        public static string GetReportUrl(int businessUnitId, string vin)
        {
            MerchandisingPreferences prefs = MerchandisingPreferences.Fetch(businessUnitId);
            string url = string.Empty;
            if (prefs.UseCarfax && !string.IsNullOrEmpty(prefs.CarfaxUsername) &&
                !string.IsNullOrEmpty(prefs.CarfaxUsername))
            {
                //make sure report exists...

                if (FetchOrCreate(businessUnitId, vin).IsValidReport)
                {
                    url =
                        "http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID=" +
                        prefs.CarfaxUsername +
                        "&vin=" +
                        vin;
                }
            }
            return url;
        }

        public static List<CarfaxInfo> GetReportsFromCarfax(string carfaxUsername, string carfaxPassword,
                                                            List<string> vins, string request, string autopurchase,
                                                            string report, string ccc, string ws)
        {
            var retList = new List<CarfaxInfo>();
            foreach (string vin in vins)
            {
                retList.Add(RequestReportFromCarfax(carfaxUsername, carfaxPassword, vin, request, autopurchase, report,
                                                    ccc, ws));
            }
            return retList;
        }

        private static CarfaxInfo RequestReportFromCarfax(string carfaxUsername, string carfaxPassword, string vin,
                                                          string request, string autopurchase, string report, string ccc,
                                                          string ws)
        {
            string reqStr =
                string.Format(
                    "http://socket.carfax.com:8080/?partner=FLK_0&UID={0}&vin={1}&REQUEST={2}&CODE=FLN&AUTOPURCHASE={3}&PW={4}&REPORT={5}&CCC={6}&WS={7}",
                    carfaxUsername, vin, request, autopurchase, carfaxPassword, report, ccc, ws);

            var req = (HttpWebRequest) WebRequest.Create(reqStr);

            var resp = (HttpWebResponse) req.GetResponse();
            Stream stm = resp.GetResponseStream();

            //advance stream past the response code
            var stmRd = new StreamReader(stm);
            string tmpCode = stmRd.ReadLine();

            //read the code line of report to advance reader to proper location for XML start
            string txt = stmRd.ReadToEnd();

            var doc = new XmlDocument();
            try
            {
                doc.Load(new StringReader(txt));
            }
            catch (XmlException ex)
            {
                //throw exception here?
                throw new ApplicationException("Carfax report not available at current time - xml error", ex);
            }

            var retInfo = new CarfaxInfo(tmpCode, doc, vin);
            return retInfo;
        }

        private static CarfaxInfo RequestReportFromCarfax(int businessUnitId, string vin)
        {
            MerchandisingPreferences prefs = MerchandisingPreferences.Fetch(businessUnitId);
            if (prefs.UseCarfax && !string.IsNullOrEmpty(prefs.CarfaxUsername) &&
                !string.IsNullOrEmpty(prefs.CarfaxPassword))
            {
                return RequestReportFromCarfax(prefs.CarfaxUsername, prefs.CarfaxPassword, vin, "IM2", "Y", "VHR", "N",
                                               "WS1");
            }
            return new CarfaxInfo();
        }
    }
}