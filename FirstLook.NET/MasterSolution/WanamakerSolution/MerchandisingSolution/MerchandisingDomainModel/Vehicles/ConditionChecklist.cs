using System.Collections.Generic;
using System.Data;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.DataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class ConditionChecklist : RandomUtility
    {
        public ConditionChecklist()
        {            
        }

        internal ConditionChecklist(VehicleDataTransfer transfer)
            : this()
        {
            IsDealerMaintained = transfer.IsDealerMaintained;
            IsFullyDetailed = transfer.IsFullyDetailed;
            HasNoPanelScratches = transfer.HasNoPanelScratches;
            HasNoVisibleDents = transfer.HasNoVisibleDents;
            HasNoVisibleRust = transfer.HasNoVisibleRust;
            HasNoKnownAccidents = transfer.HasNoKnownAccidents;
            HasNoKnownBodyWork = transfer.HasNoKnownBodyWork;
            HasPassedDealerInspection = transfer.HasPassedDealerInspection;
            HaveAllKeys = transfer.HaveAllKeys;
            NoKnownMechanicalProblems = transfer.NoKnownMechanicalProblems;
            HadAllRequiredScheduledMaintenancePerformed = transfer.HadAllRequiredScheduledMaintenancePerformed;
            HaveServiceRecords = transfer.HaveServiceRecords;
            HaveOriginalManuals = transfer.HaveOriginalManuals;
        }

        public ConditionChecklist(bool isDealerMaintained, bool isFullyDetailed, bool hasNoPanelScratches, bool hasNoVisibleDents, bool hasNoVisibleRust, bool hasNoKnownAccidents, bool hasNoKnownBodyWork, bool hasPassedDealerInspection, bool haveAllKeys, bool noKnownMechanicalProblems, bool hadAllRequiredScheduledMaintenancePerformed, bool haveServiceRecords, bool haveOriginalManuals) : this()
        {
            IsDealerMaintained = isDealerMaintained;
            IsFullyDetailed = isFullyDetailed;
            HasNoPanelScratches = hasNoPanelScratches;
            HasNoVisibleDents = hasNoVisibleDents;
            HasNoVisibleRust = hasNoVisibleRust;
            HasNoKnownAccidents = hasNoKnownAccidents;
            HasNoKnownBodyWork = hasNoKnownBodyWork;
            HasPassedDealerInspection = hasPassedDealerInspection;
            HaveAllKeys = haveAllKeys;
            NoKnownMechanicalProblems = noKnownMechanicalProblems;
            HadAllRequiredScheduledMaintenancePerformed = hadAllRequiredScheduledMaintenancePerformed;
            HaveServiceRecords = haveServiceRecords;
            HaveOriginalManuals = haveOriginalManuals;
        }

        public void Save(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "builder.ConditionChecklist#Save";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryID", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "isDealerMaintained",IsDealerMaintained, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "isFullyDetailed", IsFullyDetailed, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoPanelScratches", HasNoPanelScratches, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoVisibleDents", HasNoVisibleDents, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoVisibleRust", HasNoVisibleRust, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoKnownAccidents", HasNoKnownAccidents, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasNoKnownBodyWork", HasNoKnownBodyWork, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hasPassedDealerInspection", HasPassedDealerInspection, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "haveAllKeys", HaveAllKeys, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "noKnownMechanicalProblems", NoKnownMechanicalProblems, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "hadAllRequiredScheduledMaintenancePerformed", HadAllRequiredScheduledMaintenancePerformed, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "haveServiceRecords", HaveServiceRecords, DbType.Boolean);
                    Database.AddRequiredParameter(cmd, "haveOriginalManuals", HaveOriginalManuals, DbType.Boolean);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public bool HasAtLeastOneHighlight
        {
            get
            {
                return IsDealerMaintained || IsFullyDetailed || 
                    HasNoKnownAccidents || HasNoKnownBodyWork ||
                       HasNoPanelScratches || HasNoVisibleDents || 
                       HasNoVisibleRust || HasPassedDealerInspection ||
                       HaveAllKeys || HaveOriginalManuals || HaveServiceRecords ||
                       NoKnownMechanicalProblems || HadAllRequiredScheduledMaintenancePerformed;
            }
        }

        public bool IsDealerMaintained { get; set; }

        public bool IsFullyDetailed { get; set; }

        public bool HasNoPanelScratches { get; set; }

        public bool HasNoVisibleDents { get; set; }

        public bool HasNoVisibleRust { get; set; }

        public bool HasNoKnownAccidents { get; set; }

        public bool HasNoKnownBodyWork { get; set; }

        public bool HasPassedDealerInspection { get; set; }

        public bool HaveAllKeys { get; set; }

        public bool NoKnownMechanicalProblems { get; set; }

        public bool HadAllRequiredScheduledMaintenancePerformed { get; set; }

        public bool HaveServiceRecords { get; set; }

        public bool HaveOriginalManuals { get; set; }


        public string GetConditionSummary(int maxCount)
        {
            List<string> conditionSentences = new List<string>();
            if (HaveOriginalManuals && HaveServiceRecords && HaveAllKeys)
            {
                conditionSentences.Add("We have the original manuals, copies of service records, and all the original keys. ");

            }else
            {
                if (HaveOriginalManuals)
                {
                    conditionSentences.Add("We have the original manuals.");
                }
                if (HaveServiceRecords)
                {
                    conditionSentences.Add("We have copies of service records.");
                }
                if (HaveAllKeys)
                {
                    conditionSentences.Add("We have the full set of keys.");
                }
            }

            if (IsFullyDetailed && HasPassedDealerInspection)
            {
                conditionSentences.Add("It just passed our dealership inspection and is fully detailed.");
                
            }else
            {
                if (IsFullyDetailed)
                {
                    conditionSentences.Add("We fully detailed it to look like new.");
                } 
                if (HasPassedDealerInspection)
                {
                    conditionSentences.Add("It fully passed our dealership inspection.");
                }
            }

            if (HadAllRequiredScheduledMaintenancePerformed)
            {
                conditionSentences.Add("All the regularly scheduled maintenance was performed.");
            }
            if (NoKnownMechanicalProblems && HasNoKnownAccidents && HasNoKnownBodyWork)
            {
                conditionSentences.Add("It has no known mechanical problems, accidents, or body work");
            }else
            {
                if (NoKnownMechanicalProblems)
                {
                    conditionSentences.Add("It has no known mechanical problems");
                }
                if (HasNoKnownAccidents)
                {
                    conditionSentences.Add("It looks like there have never been any accidents.");
                }
                if (HasNoKnownBodyWork)
                {
                    conditionSentences.Add("No known body work was performed on this vehicle.");
                }
            }

            if (HasNoVisibleRust && HasNoVisibleDents && HasNoPanelScratches)
            {
                conditionSentences.Add("No visible rust, no dents, no scratches.");
            }else
            {
                if (HasNoVisibleRust)
                {
                    conditionSentences.Add("No visible rust.");
                }
                if (HasNoVisibleDents)
                {
                    conditionSentences.Add("No visible dents.");
                } 
                if (HasNoPanelScratches)
                {
                    conditionSentences.Add("No panel scratches.");
                }
            }

            string[] reordering = RandomPermutation(conditionSentences);
            StringBuilder retConditionDescription = new StringBuilder();
            for (int i=0; i<reordering.Length;i++)
            {
                if (i==maxCount)
                {
                    return retConditionDescription.ToString();
                }

                retConditionDescription.Append(conditionSentences[i]);
                retConditionDescription.Append("  ");

            }
            return retConditionDescription.ToString();
        }
    }
}

