using System;
using FirstLook.VehicleHistoryReport.WebService.Client.Carfax;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class CarfaxInterface
    {
        public CarfaxInterface(int businessUnitId, string vin, string requestingUser)
        {
            //if no user...blank it...
            if (string.IsNullOrEmpty(requestingUser))
            {
                return;
            }


            CarfaxWebService service = new CarfaxWebService();
            
            UserIdentity identity = new UserIdentity {UserName = requestingUser};

            service.UserIdentityValue = identity;

            CarfaxReportTO report;
            try
            {
                report = service.GetReport(businessUnitId, vin);
            
                if (report != null)
                {
                    IsProblemFree = !report.HasProblem;
                    foreach (CarfaxReportInspectionTO inspectionTO in report.Inspections)
                    {
                        switch (inspectionTO.Id)
                        {
                            case ONE_OWNER:
                                IsOneOwner = inspectionTO.Selected;
                                break;
                            case BUY_BACK_GUARANTEE:
                                HasBuyBackGuarantee = inspectionTO.Selected;
                                break;
                            case NO_TOTAL_LOSS_REPORTED:
                                HasNoTotalLossReported = inspectionTO.Selected;
                                break;
                            case NO_FRAME_DAMAGE_REPORTED:
                                HasNoFrameDamageReported = inspectionTO.Selected;
                                break;
                            case NO_AIRBAG_DEPLOYMENT_REPORTED:
                                HasNoAirbagDeploymentReported = inspectionTO.Selected;
                                break;
                            case NO_ODOMETER_ROLLBACK_REPORTED:
                                HasNoOdometerRollbackReported = inspectionTO.Selected;
                                break;
                            case NO_ACCIDENTS_OR_DAMAGE_REPORTED:
                                HasNoAccidentsOrDamageReported = inspectionTO.Selected;
                                break;
                            case NO_MANUFACTURER_RECALLS_REPORTED:
                                HasNoManufacturerRecallsReported = inspectionTO.Selected;
                                break;
                            default:
                                break;
                        }

                        IsValidReport = report.ReportType != CarfaxReportType.Undefined && report.ExpirationDate > DateTime.Now;

                        ReportTypeName = GetNameForReportType(report.ReportType);

                        UserName = report.UserName;

                        Vin = report.Vin;
                    }
                }else
                {
                    IsValidReport = false;
                }
            }
            catch
            {
                IsValidReport = false;
                //silent catch block, shady -bf.
            }
        }

        public bool IsOneOwner { get; private set; }

        public bool IsProblemFree { get; private set; }

        public bool IsCleanReport
        {
            get
            {
                return IsProblemFree && HasNoAccidentsOrDamageReported
                       && HasNoAirbagDeploymentReported
                       && HasNoFrameDamageReported
                       && HasNoManufacturerRecallsReported
                       && HasNoOdometerRollbackReported
                       && HasNoTotalLossReported;
            }
        }

        public bool HasBuyBackGuarantee { get; private set; }

        public bool HasNoTotalLossReported { get; private set; }

        public bool HasNoFrameDamageReported { get; private set; }

        public bool HasNoAirbagDeploymentReported { get; private set; }

        public bool HasNoOdometerRollbackReported { get; private set; }

        public bool HasNoAccidentsOrDamageReported { get; private set; }

        public bool HasNoManufacturerRecallsReported { get; private set; }

        public bool IsValidReport { get; private set; }

        public string ReportTypeName { get; private set; }

        public string UserName { get; private set; }

        public string Vin { get; private set; }

        private const int ONE_OWNER = 1;
        private const int BUY_BACK_GUARANTEE = 2;
        private const int NO_TOTAL_LOSS_REPORTED = 3;
        private const int NO_FRAME_DAMAGE_REPORTED = 4;
        private const int NO_AIRBAG_DEPLOYMENT_REPORTED = 5;
        private const int NO_ODOMETER_ROLLBACK_REPORTED = 6;
        private const int NO_ACCIDENTS_OR_DAMAGE_REPORTED = 7;
        private const int NO_MANUFACTURER_RECALLS_REPORTED = 8;
        
        public string ReportUrl
        {
            get
            {
                return string.Format(
                 "http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID={0}&vin={1}",
                 UserName,
                 Vin);
            }
        }

        private static string GetNameForReportType(CarfaxReportType reportType)
        {
            string returnValue;
            switch (reportType)
            {
                case CarfaxReportType.BTC:
                    returnValue = "Branded Title Check";
                    break;
                case CarfaxReportType.VHR:
                    returnValue = "Vehicle History Report";
                    break;
                case CarfaxReportType.CIP:
                    returnValue = "Consumer Information Pack";
                    break;
                default:
                    returnValue = string.Empty;
                    break;
            }

            return returnValue;
        }
    }
}
