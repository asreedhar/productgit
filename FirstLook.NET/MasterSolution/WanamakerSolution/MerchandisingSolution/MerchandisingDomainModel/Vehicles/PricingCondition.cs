using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using MvcMiniProfiler.Helpers.Dapper;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for PricingCondition
    /// </summary>
    [Serializable]
    public class PricingCondition
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(PricingCondition).FullName);
        #endregion


        public string Condition = string.Empty;
        public decimal Msrp;
        public decimal Invoice;
        public string PriceState;
        public int Sequence = -1;
        public string OptionCode;

        public PricingCondition(string condition, decimal msrp, decimal invoice, string priceState, int sequence)
        {
            Condition = condition;
            Msrp = msrp;
            Invoice = invoice;
            PriceState = priceState;
            Sequence = sequence;

            Log.DebugFormat("PricingCondition() ctor called with condition '{0}', msrp {1}, invoice {2}, priceState '{3}', sequence {4}",
                condition, msrp, invoice, priceState, sequence);

        }
    }
}