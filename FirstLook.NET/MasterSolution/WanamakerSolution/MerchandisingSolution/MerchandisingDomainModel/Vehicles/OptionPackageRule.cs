﻿using MAX.Entities;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public abstract class OptionPackageRule
    {
        private static readonly string AllOptionCode = new string('~', 20);  // all ~ is the "wildcard" option code (had to choose somehting)
        
        public int RuleId { get; set; }

        public string OptionCode { get; set; }

        public VehicleType AppliesToInventoryType { get; set; }

        public abstract void ApplyRuleTo(DetailedEquipment optionPackage);

        public abstract void ApplyRuleTo(Equipment optionPackage);

        //@TODO Test for ValidateOptionPackage
        protected bool ValidateOptionPackage(DetailedEquipment optionPackage)
        {
            return optionPackage != null && (optionPackage.OptionCode.ToLower() == OptionCode.ToLower() || OptionCode == AllOptionCode);
        }

        protected bool ValidateOptionPackage(Equipment optionPackage)
        {
            return optionPackage != null && (optionPackage.OptionCode.ToLower() == OptionCode.ToLower() || OptionCode == AllOptionCode);
        }
    }
}
