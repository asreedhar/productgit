using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Postings;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class StepStatusCollection
    {
        private readonly List<IStepStatus> StatusList;

        private StepStatusCollection(IEnumerable<IStepStatus> values)
        {
            StatusList = values.ToList();
        }

        private static IStepStatusCollectionRepository GetRepository()
        {
            return new StepStatusCollectionRepository();
        }

        public static StepStatusCollection Fetch(int businessUnitId, int inventoryId)
        {
            return Fetch(GetRepository(), businessUnitId, inventoryId);
        }

        public static StepStatusCollection Fetch(IStepStatusCollectionRepository repository, 
            int businessUnitId, int inventoryId)
        {
            return new StepStatusCollection(repository.Fetch(businessUnitId, inventoryId));
        }

        public void Save(string memberLogin)
        {
            Save(GetRepository(), memberLogin);
        }

        public void Save(IStepStatusCollectionRepository repository, string memberLogin)
        {
            repository.Save(StatusList.Where(st => st.IsDirty), memberLogin);
        }

        public StepStatusCollection SetStatus(StepStatusTypes type, ActivityStatusCodes status)
        {
            var step = StatusList.FirstOrDefault(st => st.StatusTypeId == type);
            if(step != null)
                step.StatusLevel = (int) status;
            return this;
        }

        public StepStatusCollection SetPostingStatus(AdvertisementPostingStatus status)
        {
            var postStatus = FindByType(StepStatusTypes.Posting);
            postStatus.StatusLevel = (int) status;
            return this;
        }

        public ActivityStatusCodes GetStatus(StepStatusTypes stepType)
        {
            var step = StatusList.FirstOrDefault(st => st.StatusTypeId == stepType);
            return step == null 
                ? ActivityStatusCodes.Untouched 
                : (ActivityStatusCodes) step.StatusLevel;
        }

        private IStepStatus FindByType(StepStatusTypes stepType)
        {
            return StatusList.Single(st => st.StatusTypeId == stepType);
        }
    }
}