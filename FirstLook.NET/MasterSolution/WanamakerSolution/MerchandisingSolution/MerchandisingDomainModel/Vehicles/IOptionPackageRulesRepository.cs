﻿
using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface IOptionPackageRulesRepository
    {
        //We are calling using inventoryId instead of specific parameters so the database can do the filtering
        IEnumerable<OptionPackageRule> GetRulesForVehicle(int businessunitId, int inventoryId);

    }
}
