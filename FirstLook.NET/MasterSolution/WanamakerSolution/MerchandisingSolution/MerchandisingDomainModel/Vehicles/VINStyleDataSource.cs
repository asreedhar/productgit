using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Core;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class VINStyleDataSource
    {
        public string MktClassSelect(int chromeStyleID)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "chrome.getMarketClass";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                    
                    using (IDataReader reader = cmd.ExecuteReader())
                    {            
                        string retStr = "";
            
                        if (reader.Read())
                        {
                            //add when new best score or same...b/c we just set to same if less than
                            retStr = reader.GetString(reader.GetOrdinal("MarketClass"));
                        }
                        return retStr;
                    }
                }
            }

        }

        public OrderRuleCollection OrderRulesSelect(int chromeStyleID)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "SELECT * " +
                                      "FROM VehicleCatalog.Chrome.OrderRules " +
                                      "WHERE StyleID = @ChromeStyleID";
                    cmd.CommandType = CommandType.Text;

                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {


                        OrderRuleCollection orderRules = new OrderRuleCollection(reader);


                        return orderRules;
                    }
                }
            }

        }
          
        public VehicleAttributes BasicStyleEquipmentSelect(int chromeStyleID, string vin)
        {
            return VehicleAttributes.Fetch(chromeStyleID, vin);
        }
        
        public IVehicleTemplatingData TemplateDataSelect(int businessUnitId, int inventoryId, string memberLogin)
        {
            var vtdFactory = Registry.Resolve<IVehicleTemplatingFactory>();
            return vtdFactory.GetVehicleTemplatingData(businessUnitId, inventoryId, memberLogin);
        }

        public CategoryCollection GetGlobalCategories2()
        {
            
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getAllCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
            
                        CategoryCollection globalCategories = new CategoryCollection("All Categories");

                        while (reader.Read())
                        {
                            globalCategories.Add(new CategoryLink(reader));
                        }

                        return globalCategories;   
                        
                    }
                    
                }
            }
           
        }
        

        public Hashtable GetGlobalCategories()
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getAllCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        Hashtable globalCategories = new Hashtable();

                        while (reader != null && reader.Read())
                        {
                            globalCategories.Add(Convert.ToInt32(reader["CategoryID"]),
                                                 new CategoryLink(reader));
                        }
                        return globalCategories;
                    }

                }
            }

        }



        /// <summary>
        /// categoryCategory can be 0= both, 1= interior 2=exterior
        /// </summary>
        /// <param name="chromeStyleID"></param>
        /// <param name="categoryCategory"></param>
        /// <returns></returns>
        public List<CategoryCollection> StandardCategoriesSelect(int chromeStyleID, int categoryCategory)
        {
            List<CategoryCollection> retColl = StandardCategoriesSelect(chromeStyleID, categoryCategory, new EquipmentCollection());
            retColl.Sort(new CategoryCollectionListLengthOrderer());
            return retColl;

        }
        public List<CategoryCollection> StandardCategoriesSelect(int chromeStyleID, int categoryCategory, EquipmentCollection availableVehicleEquipment)
        {

            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getStandardEquipmentCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", categoryCategory, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        List<CategoryCollection> retList = new List<CategoryCollection>();
                        string currHead = "";
                        CategoryCollection cc = new CategoryCollection("");
                        while (reader != null && reader.Read())
                        {
                            string tempHead = reader.GetString(reader.GetOrdinal("categoryheader"));
                            if (currHead != tempHead)
                            {
                                if (currHead != "")
                                {
                                    //not first, so add to retList
                                    retList.Add(cc);
                                }
                                currHead = tempHead;
                                cc = new CategoryCollection(currHead);
                            }
                            cc.Add(new CategoryLink(reader));
                        }
                        if (currHead != "")
                        {
                            retList.Add(cc);
                        }
                        foreach (CategoryCollection catCol in retList)
                        {
                            catCol.SetAssociatedInformation(availableVehicleEquipment, new List<int>());
                        }


                        return retList;

                    }
                }
            }
        }

        public List<CategoryCollection> LengthSortedKeyFeaturesCategoriesSelect(int chromeStyleID, int categoryCategory)
        {

            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getOptionalEquipmentCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", categoryCategory, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {

                        List<CategoryCollection> retList = new List<CategoryCollection>();
                        string currHead = "";
                        CategoryCollection cc = new CategoryCollection("");
                        while (reader != null && reader.Read())
                        {
                            string tempHead = reader.GetString(reader.GetOrdinal("categoryheader"));
                            if (currHead != tempHead)
                            {
                                if (currHead != "")
                                {
                                    //not first, so add to retList
                                    retList.Add(cc);
                                }
                                currHead = tempHead;
                                cc = new CategoryCollection(currHead);
                            }
                            cc.Add(new CategoryLink(reader));
                        }
                        if (currHead != "")
                        {
                            retList.Add(cc);
                        }


                        retList.Sort(new CategoryCollectionListLengthOrderer());
                        return retList;
                    }
                }
            }

            
        }

        public List<CategoryCollection> KeyFeaturesCategoriesSelect(int chromeStyleID, int categoryCategory, List<int> selectedCategoryIDs, EquipmentCollection availableVehicleEquipment)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.getOptionalEquipmentCategories";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", categoryCategory, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "ChromeStyleID", chromeStyleID, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        
                        List<CategoryCollection> retList = new List<CategoryCollection>();
                        string currHead = "";
                        CategoryCollection cc = new CategoryCollection("");
                        while (reader != null && reader.Read())
                        {
                            string tempHead = reader.GetString(reader.GetOrdinal("categoryheader"));
                            if (currHead != tempHead)
                            {
                                if (currHead != "")
                                {
                                    //not first, so add to retList
                                    retList.Add(cc);
                                }
                                currHead = tempHead;
                                cc = new CategoryCollection(currHead);
                            }
                            cc.Add(new CategoryLink(reader));
                        }
                        if (currHead != "")
                        {
                            retList.Add(cc);
                        }

                        foreach (CategoryCollection catCol in retList)
                        {
                            catCol.SetAssociatedInformation(availableVehicleEquipment, selectedCategoryIDs);
                        }


                        return retList;
                    }
                }
            }
        }

        public List<CategoryCollection> AllFeaturesCategoriesSelect(string vin, int categoryCategory)
        {


            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "chrome.getEquipmentCategoriesForVinBuilder";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BothInteriorOrExteriorFlag", categoryCategory, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "Vin", vin, DbType.String);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {

                        List<CategoryCollection> retList = new List<CategoryCollection>();
                        string currHead = "";
                        CategoryCollection cc = new CategoryCollection("");
                        while (reader.Read())
                        {
                            if (reader.FieldCount <= 1)
                            {
                                reader.NextResult();
                                continue;
                            }
                            string tempHead = Convert.ToString(reader["categoryheader"]);
                            if (currHead != tempHead)
                            {
                                if (currHead != "")
                                {
                                    //not first, so add to retList
                                    retList.Add(cc);
                                }
                                currHead = tempHead;
                                cc = new CategoryCollection(currHead);
                            }
                            cc.Add(new CategoryLink(reader));
                        }
                        if (currHead != "")
                        {
                            retList.Add(cc);
                        }
                        return retList;

                    }
                }
            }
        }

       
        
    }


}