﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.DataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    //@TODO use the caching
    internal class OptionPackageRulesCache : IOptionPackageRulesRepository
    {
        private const string PreKey = "OptionPackageRulesCache";

        private IOptionPackageRulesRepository _impl;

        public OptionPackageRulesCache(IOptionPackageRulesRepository impl)
        {
            _impl = impl;
        }

        private string GetKey(int businessUnitId, int inventoryId)
        {
            return PreKey + businessUnitId + inventoryId;
        }


        public IEnumerable<OptionPackageRule> GetRulesForVehicle(int businessUnitId, int inventoryId)
        {
            string key = GetKey(businessUnitId, inventoryId);
            if (RequestCache.Contains<IEnumerable<OptionPackageRule>>(key))
                return RequestCache.Fetch<IEnumerable<OptionPackageRule>>(key);

            var transfer = _impl.GetRulesForVehicle(businessUnitId, inventoryId);
            RequestCache.Save(key, transfer);
            return transfer;
        }
    }
}