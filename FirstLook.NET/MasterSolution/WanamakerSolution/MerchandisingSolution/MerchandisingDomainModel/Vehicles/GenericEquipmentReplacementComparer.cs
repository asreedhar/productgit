﻿using System;
using System.Collections;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    internal class GenericEquipmentReplacementComparer : IComparer
    {
        public int Compare(Object obj1, Object obj2)
        {
            if ( !( obj1 is GenericEquipmentReplacement ) || !( obj2 is GenericEquipmentReplacement ) ) return 0;
            var x = obj1 as GenericEquipmentReplacement;
            var y = obj2 as GenericEquipmentReplacement;
            
            if ( !HasIncludedCategories( x ) && !HasIncludedCategories (y)) return 0;
            if ( !HasIncludedCategories( x ) ) return -1;
            if ( !HasIncludedCategories( y ) ) return 1;

            return y.IncludesCategories.Count - x.IncludesCategories.Count;
        }

        private static bool HasIncludedCategories(GenericEquipmentReplacement x)
        {
            return x != null && x.IncludesCategories != null;
        }
    }
}
