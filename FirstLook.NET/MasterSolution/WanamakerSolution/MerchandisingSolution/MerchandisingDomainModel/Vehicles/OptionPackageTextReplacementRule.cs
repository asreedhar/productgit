﻿
using System;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Common.Core.Logging;
using VehicleDataAccess;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    //This is one of the rules that can be applied to the option packages.
    //It is a replaccement rule.  In the future we can add more rules and extract this out to a base class 
    public class OptionPackageTextReplacementRule : OptionPackageRule
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<OptionPackageTextReplacementRule>();

        public string TextToReplace { get; set; }

        public string TextToReplaceWith { get; set; }

        public override void ApplyRuleTo(DetailedEquipment optionPackage)
        {
            if (!ValidateOptionPackage(optionPackage)) return;
            optionPackage.DetailText = Replace(optionPackage.DetailText);
            optionPackage.OptionText = Replace(optionPackage.OptionText);
        }

        public override void ApplyRuleTo(Equipment optionPackage)
        {
            if (!ValidateOptionPackage(optionPackage)) return;
            optionPackage.Description = Replace(optionPackage.Description);
            optionPackage.ExtDescription = Replace(optionPackage.ExtDescription);
        }

        //@TODO refine this further to handle all special cases.
        //@TODO use regex for the string replacement so the TextToReplace can be made a regex in the DB
        private  string Replace(string propertyValue)
        {
            if (string.IsNullOrWhiteSpace(propertyValue)) return string.Empty;

            return ReplaceString(propertyValue, TextToReplace, TextToReplaceWith);

        }

        
        public static  string ReplaceString(string input, string textToReplace, string replaceWith)
        {
            try
            {
                string returnVal = "";

                // if the "text to replace" and the "replacement text" are the same except for the case, do a strait replace because it is a case change situation
                if (textToReplace.Equals(replaceWith, StringComparison.InvariantCultureIgnoreCase))
                    returnVal = Regex.Replace(input, Regex.Escape(textToReplace), replaceWith, RegexOptions.IgnoreCase);
                else
                {
                    //@TODO another rule type for regex
                    //using regex only to have case insensitive replace 
                    returnVal = Regex.Replace(input, Regex.Escape(textToReplace), (match) =>
                                                                                            {
                                                                                                bool isupper = (match.Value.ToUpper() == match.Value);
                                                                                                return isupper ? replaceWith.ToUpper() : replaceWith.ToLower();
                                                                                            }, RegexOptions.IgnoreCase);
                }

                return returnVal;

            }catch(Exception ex)
            {
                Log.ErrorFormat("Error replacing string '{0}' to string '{1}' in string '{2}' error {3}", textToReplace, replaceWith, input, ex.ToString());
                return input;
            }
        } 

    }

 
}
