namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum EquipmentCategoryType
    {
        Undefined = 0,
        PowerLocks = 1063,
        PowerWindows = 1126,
        PowerDriverSeat = 1074,
        PowerPassengerSeat = 1075,
        PowerDriverMirror = 1065,
        PowerPassengerMirror = 1153,

    }
}
