using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.DataAccess;
using VehicleDataAccess;
using log4net;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface IVehicleConfiguration
    {
        List<KeyInformation> KeyInformationList { get; set; }
        ConditionChecklist Checklist { get; set; }
        bool LotDataImportLock { get; set; }
        Engine SelectedEngine { get; set; }
        string ReconditioningText { get; set; }
        decimal ReconditioningValue { get; set; }
        string InteriorType { get; }
        bool HasInteriorType { get; }
        string TireTreadText { get; }
        int? TireTread { get; set; }
        int SpecialID { get; set; }
        LoaderNoteCollection Notes { get; set; }
        VehicleConditionCollection Conditions { get; set; }
        int InventoryId { get; }
        string AdditionalInfoText { get; }
        DisplayEquipmentCollection VehicleOptions { get; set; }
        List<int> SelectedOptions { get; set; }
        List<string> AfterMarketEquipment { get; set; }
        string ExteriorColorDescription { get; }
        string ExteriorColor1 { get; set; }
        string ExteriorColor2 { get; set; }
        string InteriorColor { get; set; }
        string VehicleCondition { get; set; }
        string AfterMarketTrim { get; set; }
        int ChromeStyleID { get; }
        string InteriorColorCode { get; set; }
        string ExteriorColorCode { get; set; }
        string ExteriorColorCode2 { get; set; }
        string GetExteriorColorDescription();
        FinancingSpecial GetSpecialFinancing(int ownerEntityTypeId, int ownerEntityId);
        void UpdateStyleId(int businessUnitID, int newChromeStyleID);
        void SetChromeStyleID(int businessUnitId, int inventoryId, int newChromeStyleID);
        void AddEquipmentToLists(string afterMarketList);
        void ProcessAfterMarketEquipment();
        void Save(int businessUnitID, string memberLogin);
        void OrderAndFilterEquipment(int businessUnitId, int[] maxOptionsByTier);
    }

    [Serializable]
    public class VehicleConfiguration : IChromeStyle, IVehicleConfiguration
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(VehicleConfiguration).FullName);
        #endregion

        private byte[] _version = new byte[8];
        private string _additionalInfoText = string.Empty;


        private VehicleConfiguration()
        {
            AfterMarketTrim = string.Empty;
            VehicleCondition = string.Empty;
            InteriorColor = string.Empty;
            ExteriorColor2 = string.Empty;
            ExteriorColor1 = string.Empty;
            AfterMarketEquipment = new List<string>();
            SelectedOptions = new List<int>();
            VehicleOptions = new DisplayEquipmentCollection();
            Conditions = new VehicleConditionCollection();
            Notes = new LoaderNoteCollection();
            ReconditioningText = string.Empty;
            LotDataImportLock = true;
            Checklist = new ConditionChecklist();

            InteriorColorCode = string.Empty;
            ExteriorColorCode = string.Empty;
            ExteriorColorCode2 = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="reader">reader should contain two result sets - one with the base configuration
        /// the other with the rows of selected options</param>
        internal VehicleConfiguration(int businessUnitId, VehicleDataTransfer transfer)
        {
            Log.DebugFormat("VehicleContiguration() ctor called with businessUnitId {0} and an IDataReader", businessUnitId);

			AfterMarketTrim = string.Empty;
            VehicleCondition = string.Empty;
            InteriorColor = string.Empty;
            ExteriorColor2 = string.Empty;
            ExteriorColor1 = string.Empty;
            AfterMarketEquipment = new List<string>();
            SelectedOptions = new List<int>();
            ReconditioningText = string.Empty;
            LotDataImportLock = true;
            Checklist = new ConditionChecklist();
           
            TireTread = transfer.TireTread;
            ChromeStyleID = transfer.ChromeStyleID;
            SpecialID = transfer.SpecialID;

            InteriorColor = transfer.InteriorColor;
            ExteriorColor1 = transfer.ExteriorColor1;
            ExteriorColor2 = transfer.ExteriorColor2;
            VehicleCondition = transfer.VehicleCondition;

            InteriorColorCode = transfer.InteriorColorCode;
            ExteriorColorCode = transfer.ExteriorColorCode;
            ExteriorColorCode2 = transfer.ExteriorColorCode2;

            AfterMarketEquipment = transfer.AfterMarketEquipment;

            AfterMarketTrim = transfer.AfterMarketTrim;

            InventoryId = transfer.InventoryId;
            ReconditioningText = transfer.ReconditioningText;
            ReconditioningValue = transfer.ReconditioningValue;
            LotDataImportLock = transfer.LotDataImportLock;

            _version = transfer.Version;

            SelectedEngine = new Engine();
            Checklist = new ConditionChecklist(transfer);
            
            VehicleOptions = new DisplayEquipmentCollection(businessUnitId, transfer, transfer.DealerAdvertisementPreferences);

            Conditions = transfer.Conditions;
            Notes = transfer.Notes;
            KeyInformationList = transfer.KeyInformationList;
        }

        public List<KeyInformation> KeyInformationList { get; set; }

        public ConditionChecklist Checklist { get; set; }

        public bool LotDataImportLock { get; set; }

        public Engine SelectedEngine { get; set; }

        public string ReconditioningText { get; set; }

        public decimal ReconditioningValue { get; set; }

        public string InteriorType
        {
            get { return VehicleOptions.GetInteriorType(); }
        }

        public bool HasInteriorType
        {
            get { return VehicleOptions.HasInteriorType(); }
        }

        public string TireTreadText
        {
            get
            {
                return TireTread.HasValue ? TireTread.ToString() : string.Empty;
            }
        }

        public int? TireTread { get; set; }

        public int SpecialID { get; set; }

        public LoaderNoteCollection Notes { get; set; }

        public VehicleConditionCollection Conditions { get; set; }

        public int InventoryId { get; private set; }

        public string AdditionalInfoText
        {
            get
            {
                if (string.IsNullOrEmpty(_additionalInfoText))
                {
                    var sb = new StringBuilder();

                    foreach (KeyInformation info in KeyInformationList)
                    {
                        sb.Append(info.Description).Append(", ");
                    }
                    _additionalInfoText = sb.ToString().Trim(", ".ToCharArray());
                }
                return _additionalInfoText;
            }
        }

        public DisplayEquipmentCollection VehicleOptions { get; set; }


        public List<int> SelectedOptions { get; set; }

        public List<string> AfterMarketEquipment { get; set; }

        public string ExteriorColorDescription
        {
            get
            {
                string retStr;
                
                if (!string.IsNullOrEmpty(ExteriorColor2))
                {
                    retStr = ExteriorColor1 + " with " + ExteriorColor2;
                }
                else
                {
                    retStr = ExteriorColor1;
                }

                //reject any description containing the word unknown
                if (retStr.ToLower().Contains("unknown"))
                {
                    retStr = string.Empty;
                }
                return retStr;
            }
        }

        public string ExteriorColor1 { get; set; }

        public string ExteriorColor2 { get; set; }

        public string InteriorColor { get; set; }

        public string VehicleCondition { get; set; }

        public string AfterMarketTrim { get; set; }
        
        public int ChromeStyleID { get; private set; }

        public string InteriorColorCode { get; set; }
        public string ExteriorColorCode { get; set; }
        public string ExteriorColorCode2 { get; set; }

        public string GetExteriorColorDescription()
        {
            string retStr = string.Empty;
            if (!string.IsNullOrEmpty(ExteriorColor2))
            {
                retStr = " with " + ExteriorColor2;
            }
            return ExteriorColor1 + retStr;
        }

        public FinancingSpecial GetSpecialFinancing(int ownerEntityTypeId, int ownerEntityId)
        {
            return FinancingSpecial.Fetch(ownerEntityTypeId, ownerEntityId, SpecialID);
        }

        public void UpdateStyleId(int businessUnitID, int newChromeStyleID)
        {
            // TODO:  Some sort of sanity check here to make sure we aren't assigning an invalid styleid to this vehicle.

            // Avoid this if we already have the style id.
            if (ChromeStyleID == newChromeStyleID)
            {
                return;
            }

            SetChromeStyleID(businessUnitID, InventoryId, newChromeStyleID);

            var dataAccess = Registry.Resolve<IVehicleConfigurationDataAccess>();
            dataAccess.SaveChromeStyle(businessUnitID, InventoryId, newChromeStyleID);
        }

        public void SetChromeStyleID(int businessUnitId, int inventoryId, int newChromeStyleID)
        {
            //if we have a new value...
            if (ChromeStyleID != newChromeStyleID)
            {
                Log.WarnFormat("ChromeStyleId being changed from {0} to {1}", ChromeStyleID, newChromeStyleID);

                //Clear out all options. We don't want to merge them with new chrome data
                VehicleOptions.generics.DeleteAll(businessUnitId, inventoryId);
                VehicleOptions.generics.Clear();

                //load all generics
                VehicleOptions.generics.LoadChromeStyle(businessUnitId, newChromeStyleID, true);
                VehicleOptions.LoadMappedEquipment(businessUnitId, inventoryId, true, newChromeStyleID);

                var dataAccess = Registry.Resolve<IVehicleConfigurationDataAccess>();
                dataAccess.SaveGenerics(VehicleOptions.generics, businessUnitId, inventoryId);

                ChromeStyleID = newChromeStyleID;

                AfterMarketTrim = string.Empty;
            }
        }

        public static VehicleConfiguration FetchOrCreateDetailed(int businessUnitID, int inventoryId, string memberLogin)
        {
            bool isStaging = inventoryId < 0;

            return FetchOrCreateDetailed(businessUnitID, inventoryId, memberLogin, isStaging);
        }

        private static VehicleConfiguration FetchOrCreateDetailed(int businessUnitID, int inventoryId, string memberLogin,
                                                                 bool isStaging)
        {
            Log.DebugFormat("FetchOrCreateDetailed() called with businessUnitID {0}, inventoryId {1}, memberId '{2}', isStaging {3}",
                businessUnitID, inventoryId, memberLogin, isStaging);

            var dataAccess = Registry.Resolve<IVehicleConfigurationDataAccess>();
            
            // There is some magic going on in here. You'd think this is a straight "load from the db"
            // method, but there is some extra magic going on to set the chromeStyleId
            var transfer = dataAccess.Fetch(businessUnitID, inventoryId, memberLogin);

            var vc = new VehicleConfiguration(businessUnitID, transfer);

            var loaded = false;

            //We need to make sure if there is a valid ChromeStyle we have the VehicleOptions table populated for selection
            //Because trim could be already set before entering MaxAd
            if (vc.ChromeStyleID > 0 && vc.VehicleOptions.generics.Count == 0)
            {
                Log.Warn("No VehicleOptions, loading them..");

                if (!Lot.IsLoaded(inventoryId))
                {
                    loaded = LoadLotData(businessUnitID, vc);
                    if (loaded) vc.Save(businessUnitID, memberLogin);
                }

                vc.VehicleOptions.generics.LoadChromeStyle(businessUnitID, vc.ChromeStyleID, false);
                vc.VehicleOptions.LoadMappedEquipment(businessUnitID, inventoryId, false, vc.ChromeStyleID);

                if (loaded)
                {
                    SetPackageEquipmentComplete(businessUnitID, inventoryId, memberLogin);
                    Lot.MarkAsLoaded(inventoryId);
                }

                //save all (changed to call more efficient save at gec level) FB: 30042, tureen 6/19/2014
                vc.VehicleOptions.generics.Save(businessUnitID, inventoryId);

            }
            else
            {
                
                if (!Lot.IsLoaded(inventoryId))
                {
                    loaded = LoadLotData(businessUnitID, vc);
                    if (loaded) vc.Save(businessUnitID, memberLogin);
                }

                vc.VehicleOptions.generics.LoadChromeStyle(businessUnitID, vc.ChromeStyleID, false);
                bool lotLoaded = vc.VehicleOptions.LoadMappedEquipment(businessUnitID, inventoryId, false, vc.ChromeStyleID);

                if (loaded)
                {
                    SetPackageEquipmentComplete(businessUnitID, inventoryId, memberLogin);
                    Lot.MarkAsLoaded(inventoryId);
                }

                if (lotLoaded)
                {
                    SetPackageEquipmentComplete(businessUnitID, inventoryId, memberLogin);
                    vc.VehicleOptions.generics.Save(businessUnitID, inventoryId);
                }
            }

            return vc; 
        }

        private static void SetPackageEquipmentComplete(int businessUnitId, int inventoryId, string processName)
        {
            StepStatusCollection
                .Fetch(businessUnitId, inventoryId)
                .SetStatus(StepStatusTypes.ExteriorEquipment, ActivityStatusCodes.Complete)
                .SetStatus(StepStatusTypes.InteriorEquipment, ActivityStatusCodes.Complete)
                .Save(processName);
        }
        
        private static bool LoadLotData(int businessUnitId, VehicleConfiguration vehicleConfiguration)
        {
            var loader = new LotLoader();
            return loader.Load(businessUnitId, vehicleConfiguration);
        }

        public void AddEquipmentToLists(string afterMarketList)
        {
            string[] afterMktItems = afterMarketList.Split(",".ToCharArray());
            foreach (string afterMktItem in afterMktItems)
            {
                if (!AfterMarketEquipment.Contains(afterMktItem))
                {
                    AfterMarketEquipment.Add(afterMktItem);
                }
            }
        }

        public void ProcessAfterMarketEquipment()
        {
            //we need global categories to check 'after market' features against for normalization
            var vsds = new VINStyleDataSource();
            Hashtable validCategories = vsds.GetGlobalCategories();

            //AFTER MARKET SELECTIONS
            var itemsToRemove = new List<string>();
            foreach (string equipment in AfterMarketEquipment)
            {
                foreach (CategoryLink cl in validCategories.Values)
                {
                    if (equipment == cl.Description)
                    {
                        VehicleOptions.AddGeneric(new GenericEquipment(cl, false));
                        itemsToRemove.Add(equipment);
                        break;
                    }
                }
            }

            foreach (string equipment in itemsToRemove)
            {
                AfterMarketEquipment.Remove(equipment);
            }
        }
       
        
        public static void Update(int businessUnitID, string memberLogin, VehicleConfiguration vehicle)
        {
            vehicle.Save(businessUnitID, memberLogin);
        }

        private VehicleDataTransfer PopulateTransfer(int businessUnitId)
        {
            VehicleDataTransfer transfer = new VehicleDataTransfer();

            transfer.TireTread = TireTread;
            transfer.ChromeStyleID = ChromeStyleID;
            transfer.SpecialID = SpecialID;

            transfer.InteriorColor = InteriorColor;
            transfer.ExteriorColor1 = ExteriorColor1;
            transfer.ExteriorColor2 = ExteriorColor2;
            transfer.VehicleCondition = VehicleCondition;

            transfer.ExteriorColorCode = ExteriorColorCode;
            transfer.ExteriorColorCode2 = ExteriorColorCode2;
            transfer.InteriorColorCode = InteriorColorCode;

            transfer.AfterMarketEquipment = AfterMarketEquipment;
            transfer.AfterMarketTrim = AfterMarketTrim;

            transfer.InventoryId = InventoryId;
            transfer.BusinessUnitid = businessUnitId;
            transfer.ReconditioningText = ReconditioningText;
            transfer.ReconditioningValue = ReconditioningValue;
            transfer.LotDataImportLock = LotDataImportLock;

            transfer.Version = _version;

            //Checklist
            transfer.IsDealerMaintained = Checklist.IsDealerMaintained;
            transfer.IsFullyDetailed = Checklist.IsFullyDetailed;
            transfer.HasNoPanelScratches = Checklist.HasNoPanelScratches;
            transfer.HasNoVisibleDents = Checklist.HasNoVisibleDents;
            transfer.HasNoVisibleRust = Checklist.HasNoVisibleRust;
            transfer.HasNoKnownAccidents = Checklist.HasNoKnownAccidents;
            transfer.HasNoKnownBodyWork = Checklist.HasNoKnownBodyWork;
            transfer.HasPassedDealerInspection = Checklist.HasPassedDealerInspection;
            transfer.HaveAllKeys = Checklist.HaveAllKeys;
            transfer.NoKnownMechanicalProblems = Checklist.NoKnownMechanicalProblems;
            transfer.HadAllRequiredScheduledMaintenancePerformed = Checklist.HadAllRequiredScheduledMaintenancePerformed;
            transfer.HaveServiceRecords = Checklist.HaveServiceRecords;
            transfer.HaveOriginalManuals = Checklist.HaveOriginalManuals;

            //VehicleOptions
            transfer.Generics = VehicleOptions.generics;
            transfer.Options = VehicleOptions.options;
            transfer.Replacements = VehicleOptions.replacements;

            transfer.Conditions = Conditions;
            transfer.Notes = Notes;
            transfer.KeyInformationList = KeyInformationList;

            return transfer;
        }

        public void Save(int businessUnitID, string memberLogin)
        {
            var dataAccess = Registry.Resolve<IVehicleConfigurationDataAccess>();
            var transfer = PopulateTransfer(businessUnitID);

            dataAccess.Save(transfer, memberLogin);
        }

        public void OrderAndFilterEquipment(int businessUnitId, int[] maxOptionsByTier)
        {
            Log.DebugFormat("OrderAndFilterEquipment() called with businessUnitId {0}, maxOptionsByTier {1}",
                businessUnitId, new List<int>(maxOptionsByTier).ToDelimitedString(","));

            VehicleOptions.FilterAndReorder(businessUnitId);

            //set display preferences sorts the items into proper buckets by tier
            VehicleOptions.SetDisplayPreferences(maxOptionsByTier);
        }


        public static void SetLotDataLockOutStatus(int businessUnitId, int inventoryId, bool isLotDataLockedOut)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.LotDataImportLock#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "LotDataImportLock", isLotDataLockedOut, DbType.Boolean);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void UpdateLotLocation(int businessUnitID, int inventoryId, int lotLocationId)
        {
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.LotLocation#Update";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitID, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "LotLocationId", lotLocationId, DbType.Int32);

                    cmd.ExecuteNonQuery();
                }
            }
        }


    }



}