using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Data;
using FirstLook.Merchandising.DomainModel.Core;
using log4net;
using IDataConnection = FirstLook.Common.Data.IDataConnection;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    //TODO: Rename this class (it is not Manufacturer specific)
    [Serializable]
    public class ManufacturerCertification
    {
        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(ManufacturerCertification).FullName);
        #endregion

        private readonly CertificationFeatureCollection _features;

        public ManufacturerCertification()
        {
	        ProgramId = default(int?);
            _features = new CertificationFeatureCollection();
	        DealerPreviewText = string.Empty;
	        ReportedByManufacturer = false;
	        ProgramIsPersisted = true;
        }

	    /// <summary>
	    /// 
	    /// </summary>g
	    /// <param name="reader">expects a reader with two results sets, the first with the title and id
	    /// the second contains the certification features</param>
	    public ManufacturerCertification(IDataReader reader)
        {
			ProgramId = reader.GetNullableInt32("ProgramId");

			DealerPreviewText = reader.GetString(reader.GetOrdinal("CertificationPreviewText"));

		    ReportedByManufacturer = reader.GetBoolean(reader.GetOrdinal("ReportedByManufacturer"));

			ProgramIsPersisted = reader.GetBoolean(reader.GetOrdinal("ProgramPersisted"));

	        var marketingNameOrdinal = reader.GetOrdinal("MarketingName");
	        MarketingName = reader.IsDBNull(marketingNameOrdinal) ? null : reader.GetString(marketingNameOrdinal);


			reader.NextResult();

            //initialize the features and add a feature for each record of the result set
            _features = new CertificationFeatureCollection(reader);
        }

        public string MarketingName { get; set; }


        public CertificationFeatureCollection ProgramFeatures
        {
            get
            {
                return _features.ProgramFeatures;
            }
        }

        public bool DisplayContains(CertificationFeatureType featureType)
        {
            return _features.IsItemDisplayed(featureType);
        }

		public int? ProgramId { get; set; }

		public string DealerPreviewText { get; private set; }

		public bool ReportedByManufacturer { get; private set; }

		public bool ProgramIsPersisted { get; private set; }

	    public string GetFullDetails(string separator)
        {
			var sb = new StringBuilder();
			foreach (var feature in _features.Cast<CertificationFeature>().Where(feature => feature.FeatureDisplayFlag))
			{
				sb.Append(feature);
				sb.Append(separator);
			}
			return sb.ToString().TrimEnd(separator.ToCharArray());
		}

		private static IDataConnection GetConnection()
		{
			return Database.GetConnection(Database.MerchandisingDatabase);
		}

		public static ManufacturerCertification Fetch(int businessUnitId, int inventoryId, string make)
		{
			using (var con = GetConnection())
			{
				con.Open();
				using (var cmd = con.CreateCommand())
				{
					cmd.CommandText = "builder.Certification#Fetch";
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.AddRequiredParameter("BusinessUnitId", businessUnitId, DbType.Int32);
					cmd.AddRequiredParameter("InventoryId", inventoryId, DbType.Int32);
					cmd.AddRequiredParameter("MakeName", make, DbType.String);

					using (var reader = cmd.ExecuteReader())
					{

						if (reader.Read()) //if it has a record...return it
						{
							return new ManufacturerCertification(reader);
						}

						return new ManufacturerCertification();
					}
				}
			}
		}

		public static ManufacturerCertification Fetch(IInventoryData inventoryItem)
		{
			return Fetch(inventoryItem.BusinessUnitID, inventoryItem.InventoryID, inventoryItem.Make);
		}


		// Keeping this here because it is called elsewhere as a general "GetMake" function (not CPO specific) - will/should probably move this soon.
		public static CertificationType GetCertificationType(string make)
		{
			switch (make.ToLower())
			{
				case "audi":
					return CertificationType.Audi;
				case "ford":
					return CertificationType.Ford;
				case "gmc":
					return CertificationType.GMC;
				case "nissan":
					return CertificationType.Nissan;
				case "toyota":
					return CertificationType.Toyota;
				case "honda":
					return CertificationType.Honda;
				case "acura":
					return CertificationType.Acura;
				case "cadillac":
					return CertificationType.Cadillac;
				case "buick":
					return CertificationType.Buick;
				case "chevrolet":
					return CertificationType.Chevrolet;
				case "chrysler":
					return CertificationType.Chrysler;
				case "dodge":
					return CertificationType.Dodge;
				case "hyundai":
					return CertificationType.Hyundai;
				case "infiniti":
					return CertificationType.Infiniti;
				case "jeep":
					return CertificationType.Jeep;
				case "lexus":
					return CertificationType.Lexus;
				case "bmw":
					return CertificationType.BMW;
				case "mercedes":
				case "mercedes-benz":
					return CertificationType.MercedesBenz;
				case "mini":
					return CertificationType.MINI;
				case "lincoln":
					return CertificationType.Lincoln;
				case "mazda":
					return CertificationType.Mazda;
				case "mercury":
					return CertificationType.Mercury;
				case "mitsubishi":
					return CertificationType.Mitsubishi;
				case "oldsmobile":
					return CertificationType.Oldsmobile;
				case "pontiac":
					return CertificationType.Pontiac;
				case "saab":
					return CertificationType.Saab;
				case "saturn":
					return CertificationType.Saturn;
				case "scion":
					return CertificationType.Scion;
				case "subaru":
					return CertificationType.Subaru;
				case "suzuki":
					return CertificationType.Suzuki;
				case "volkswagen":
					return CertificationType.Volkswagen;
				case "volvo":
					return CertificationType.Volvo;
				case "hummer":
					return CertificationType.Hummer;
				case "kia":
					return CertificationType.Kia;
				case "land rover":
					return CertificationType.LandRover;
				case "fiat":
					return CertificationType.Fiat;
				default:
					Log.WarnFormat("Make {0} is not supported in ManufacturerCertification.GetCertificationType().  The CPO features for this make have not been completely set up.", make.ToLower());
					return CertificationType.Undefined;
			}
		}


    }
}
