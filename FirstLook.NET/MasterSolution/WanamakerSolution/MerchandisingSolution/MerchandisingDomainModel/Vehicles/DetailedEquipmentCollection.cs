using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using log4net;
using FirstLook.Merchandising.DomainModel.VehicleVinData;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class DetailedEquipmentCollection : CollectionBase, IEnumerable<ITieredEquipment>
    {

        #region Logging
        private static readonly ILog Log = LogManager.GetLogger(typeof(DetailedEquipmentCollection).FullName);
        #endregion


        public DetailedEquipmentCollection()
        {
        }

        public DetailedEquipmentCollection(IDataReader reader, GenericEquipmentCollection generics)
        {
            while (reader.Read())
            {
                Add(new DetailedEquipment(reader));
            }
            foreach (DetailedEquipment de in List)
            {
                de.SetTierAndPriority(generics);
            }
        }

        public static DetailedEquipmentCollection FetchSelections(int businessUnitId, int inventoryId)
        {
            DetailedEquipmentCollection dec = new DetailedEquipmentCollection();
            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.detailedOptions#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dec.Add(new DetailedEquipment(reader));
                        }
                        //Apply the option rules
                        ProcessOptions(businessUnitId, inventoryId, dec);

                        return dec;
                    }
                }
            }
            
        }

        /// <summary>
        /// This method applies the option package rules like text replacement rules to the option packages
        /// e.g.,  remove the 1 year subscription from description for used cars only.
        /// </summary>
        private static void ProcessOptions(int businessUnitId, int inventoryId, DetailedEquipmentCollection options)
        {
            if (options == null || options.Count == 0) return;

            var optionRulesEngine = new OptionPackageRulesEngine(Registry.Resolve<IOptionPackageRulesRepository>());

            optionRulesEngine.Process(businessUnitId, inventoryId, options);
        }


        public bool ContainsOptionText(string optionText)
        {
            foreach (DetailedEquipment equip in this)
            {
                if (equip.OptionText == optionText)
                {
                    return true;
                }
            }
            return false;
        }


        public int Add(DetailedEquipment equip)
        {
            //if option code is empty we check optiontext for dups
            if (String.IsNullOrEmpty(equip.OptionCode))
            {
                if (!ContainsOptionText(equip.OptionText))
                {
                    int val = List.Add(equip);
                    if (OptionChanged != null)
                    {
                        OptionChanged(this, new OptionChangedEventArgs(equip, true)); //equipment added!
                    }
                    return val;
                }
                return 0;
            }

            //valid option code
            if (!ContainsOption(equip.OptionCode))
            {
                int val = List.Add(equip);
                if (OptionChanged != null)
                {
                    OptionChanged(this, new OptionChangedEventArgs(equip, true)); //equipment added!
                }
                return val;
            }
            return 0;
        }

        /// <summary>
        /// AddRange does not duplicate values
        /// </summary>
        /// <param name="collection"></param>
        public void AddRange(DetailedEquipmentCollection collection)
        {
            foreach (DetailedEquipment equip in collection)
                Add(equip);
        }
        
        public DetailedEquipment Item(int index)
        {
            return (DetailedEquipment) List[index];
        }


        public void Remove(DetailedEquipment item)
        {
            List.Remove(item);
        }


/*
        public bool IsReadOnly
        {
            get { throw new System.NotImplementedException(); }
        }
*/

        public bool ContainsOption(string optionCode)
        {
            foreach (DetailedEquipment equip in this)
            {
                if (equip.OptionCode == optionCode)
                {
                    return true;
                }
            }
            return false;
        }

        public DetailedEquipment GetStandardHighlight(int sequenceNumber)
        {
            foreach (DetailedEquipment eq in this)
            {
                if (eq.OptionCode == DetailedEquipment.GetStandardCode(sequenceNumber))
                {
                    return eq;
                }
            }
            return null;
        }

        /// <summary>
        /// determine if the list contains a highlight of the specified standard equipment
        /// </summary>
        /// <param name="sequenceNumber"></param>
        /// <returns></returns>
        public bool ContainsStandardHighlight(int sequenceNumber)
        {
            foreach (DetailedEquipment eq in this)
            {
                if (eq.OptionCode == DetailedEquipment.GetStandardCode(sequenceNumber))
                {
                    return true;
                }
            }
            return false;
        }
        public void ClearStandardHighlights()
        {
            //copy to a new collection
            DetailedEquipmentCollection newDec = new DetailedEquipmentCollection();
            newDec.AddRange(this);
            
            //clear all equip from this list
            Clear();
            foreach (DetailedEquipment de in newDec)
            {
                //if it isn't standard add it back to our list
                if (!de.IsStandard)
                {
                    Add(de);
                }
            }
            
        }

        public string GetOptionCodes(string separator)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DetailedEquipment de in this)
            {
                if (de.IsStandard) continue;
                sb.Append(de.OptionCode);
                sb.Append(separator);
            }
            int validCharCt = sb.Length - separator.Length;
            return validCharCt > 0 ? sb.ToString(0,validCharCt) : string.Empty;
        }


        public void RemoveRange(int startNdx, int count)
        {
            int i = Math.Min(startNdx+count, Count-1);
            while (i >= startNdx)
            {
                RemoveRange(i,1);
            }
            
        }
        public DetailedEquipment GetByCode(string optionCode)
        {
            foreach (DetailedEquipment equip in this)
            {
                if (equip.OptionCode == optionCode)
                {
                    return equip;
                }
            }
            throw new ApplicationException("Could not find detailed equipment with option code " + optionCode);
        }
        public void Remove(string optionCode)
        {
            foreach (DetailedEquipment de in this)
            {
                if (de.OptionCode == optionCode)
                {
                    Remove(de);
                    if (OptionChanged != null)
                    {
                        OptionChanged(this, new OptionChangedEventArgs(de, false)); //equipment removed
                    }
                    return;
                }
            }
        }

        public void Save(int businessUnitId, int inventoryId)
        {
            Log.DebugFormat("Save() called with businessUnitID {0} and inventoryId {1}", businessUnitId, inventoryId);

            DeleteAll(businessUnitId, inventoryId);

            EquipmentCategoryIds equipmentOptions = new EquipmentCategoryIds(0);
            List<EquipmentObject> collectEquipment = new List<EquipmentObject>();
            foreach (DetailedEquipment de in this)
            {
                de.Save(businessUnitId, inventoryId);
 
                foreach (EquipmentObject equipment in equipmentOptions.AllEquipmentObjects)
                {
                    if (de.categoryAdds.Contains(equipment.TypeID))
                    {
                        collectEquipment.Add(equipment);
                    }
                }

                if (collectEquipment.Count > 0)
                {
                    foreach (EquipmentObject equipment in collectEquipment)
                    {
                        if (equipment.Category.Equals("Engine"))
                        {
                            UpsertVehicleConfiguration.UpdateEngine(inventoryId, businessUnitId, equipment.TypeID);
                        }
                        if (equipment.Category.Equals("Transmission"))
                        {
                            UpsertVehicleConfiguration.UpdateTrans(inventoryId, businessUnitId, equipment.TypeID);
                        }
                        if (equipment.Category.Equals("Drivetrain"))
                        {
                            UpsertVehicleConfiguration.UpdateDrivetrain(inventoryId, businessUnitId, equipment.TypeID);
                        }
                        if (equipment.Category.Equals("Fuel system"))
                        {
                           UpsertVehicleConfiguration.UpdateFuelsystem(inventoryId, businessUnitId, equipment.TypeID);
                        }
                    }
                }
            }

            UpdateNoPackagesField(businessUnitId, inventoryId, GetCurrentUserName());
        }

        /// <summary>
        /// This should be called whenever the VehicleDetailedOptions or ChromeStyleId changes for a vehicle.
        /// </summary>
        /// <param name="businessUnitId"></param>
        /// <param name="inventoryId"></param>
        /// <param name="memberLogin"></param>
        public static void UpdateNoPackagesField(int businessUnitId, int inventoryId, string memberLogin)
        {
            if(Log.IsDebugEnabled)
                Log.DebugFormat("UpdateNoPackagesField called for business unit {0} and inventory {1}.", businessUnitId,
                                inventoryId);
            
            using(var cn = Database.GetOpenConnection(Database.MerchandisingDatabase))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "workflow.UpdateOptionsConfigurationNoPackagesField";

                cmd.AddRequiredParameter("@MemberLogin", memberLogin, DbType.AnsiString);
                cmd.AddRequiredParameter("@BusinessUnitId", businessUnitId, DbType.Int32);
                cmd.AddRequiredParameter("@InventoryId", inventoryId, DbType.Int32);

                cmd.ExecuteNonQuery();
            }
        }

        private static string GetCurrentUserName()
        {
            return HttpContext.Current == null ? "" : HttpContext.Current.User.Identity.Name;
        }

        public void DeleteAll(int businessUnitId, int inventoryId)
        {
            Log.DebugFormat("DeleteAll() called for businessUnitId {0} and inventoryId {1}", businessUnitId, inventoryId);

            using (IDataConnection con = Database.GetConnection(Database.MerchandisingDatabase))
            {
                con.Open();

                //delete detailed options
                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //
                    cmd.CommandText = "builder.vehicleDetailedOptions#Delete";

                    Database.AddRequiredParameter(cmd, "BusinessUnitID", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryID", inventoryId, DbType.Int32);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public delegate void OptionChangedEventHandler(object sender, OptionChangedEventArgs e);
        public event OptionChangedEventHandler OptionChanged;

        
        public new IEnumerator<ITieredEquipment> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                // Note use of "yield return".  This is an iterator.
                yield return Item(i);                
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public List<DetailedEquipment> FindByGenericId(int categoryId)
        {
            Log.DebugFormat("FindByGenericId() called with categoryId {0}", categoryId);

            List<DetailedEquipment> retList = List.Cast<DetailedEquipment>().Where(eq => eq.categoryAdds.Contains(categoryId)).ToList();

            if( Log.IsDebugEnabled)
            {
                Log.DebugFormat("Returning List<DetailedEquipment>: {0}", StandardObjectDumper.Write(retList,1));                
            }

            return retList;
        }

        //This method goes through the packages and selects the subset that contain equipment that is passed in
        public List<DetailedEquipment> FindAllMatching(IEnumerable<ICategorizedEquipment> searchEquipment)
        {
            if( Log.IsDebugEnabled)
            {
                Log.DebugFormat("FindAllMatching() called with the following categorized equipment: {0}",
                    StandardObjectDumper.Write(searchEquipment, 1));                
            }

            List<DetailedEquipment> retList = List.Cast<DetailedEquipment>().ToList();

            var result = retList.FindAll(
                equip =>
                searchEquipment.Any(equipmentPiece => equip.categoryAdds.Contains(equipmentPiece.GetCategoryId())));

            if( Log.IsDebugEnabled )
            {
                Log.DebugFormat("Result is: {0}", StandardObjectDumper.Write(result, 1));                
            }
            return result;
        }


        public List<DetailedEquipment> FindBySearchText(string searchText)
        {
            Log.DebugFormat("FindBySearchText() called with searchText '{0}'", searchText);

            var result = ((List<DetailedEquipment>) List).FindAll(
                equip => string.Compare(equip.OptionCode, searchText, StringComparison.InvariantCultureIgnoreCase) == 0
                         || equip.OptionText.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0
                );

            if( Log.IsDebugEnabled)
            {
                Log.DebugFormat("Result is {0}", StandardObjectDumper.Write(result, 1));                
            }
            return result;
        }

        public void LoadViewState(object state)
        {
            List.Clear();

            
            if (state is ArrayList)
            {
                ArrayList al = state as ArrayList;
                foreach (object obj in al)
                {
                    DetailedEquipment de = new DetailedEquipment();
                    de.LoadViewState(obj);
                    Add(de);
                }
            }            

        }

        public object SaveViewState()
        {
            ArrayList al = new ArrayList();
            foreach (DetailedEquipment de in List)
            {
                al.Add(de.SaveViewState());
            }
            return al;            
        }      
    }
}
