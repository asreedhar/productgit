using System;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class InventoryException : Exception
    {
        public InventoryException(string message, int inventoryId)
            : base(message)
        {
            InventoryId = inventoryId;
        }

        public int InventoryId { get; set; }
    }
}
