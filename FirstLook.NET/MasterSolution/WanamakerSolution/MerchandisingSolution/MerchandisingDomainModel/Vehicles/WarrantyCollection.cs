using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    [Serializable]
    public class WarrantyCollection : CollectionBase
    {
        private const int MIN_DAYS_REMAINING = 90;
        private readonly Dictionary<WarrantyTypes, string> _warrantyKeyMap = new Dictionary<WarrantyTypes, string>();

        public WarrantyCollection()
        {
        }

        public WarrantyCollection(string chromeWarrantyText)
        {
            _warrantyKeyMap.Add(WarrantyTypes.BasicWarranty, "Basic");
            _warrantyKeyMap.Add(WarrantyTypes.DrivetrainWarranty, "Drivetrain");
            _warrantyKeyMap.Add(WarrantyTypes.ExtendedWarranty, "Extended");
            _warrantyKeyMap.Add(WarrantyTypes.CorrosionWarranty, "Corrosion");
            _warrantyKeyMap.Add(WarrantyTypes.RoadsideAssistance, "Roadside Assistance");

            string[] warrantyItems =
                chromeWarrantyText.Split(new[] {"\r\n\r\n"}, StringSplitOptions.RemoveEmptyEntries);

            foreach (string warranty in warrantyItems)
            {
                string warrantyTrimmed = warranty.Replace("\r\n", "");
                string warrantyPrepped = warrantyTrimmed.ToLower();

                //remove the extra line characters
                if (warrantyPrepped.Contains("basic"))
                {
                    Add(WarrantyTypes.BasicWarranty, warrantyPrepped);
                }
                else if (warrantyPrepped.Contains("drivetrain"))
                    //probably need to break out for diesel/gasoline drivetrain
                {
                    Add(WarrantyTypes.DrivetrainWarranty, warrantyPrepped);
                }
                else if (warrantyPrepped.Contains("extend"))
                {
                    Add(WarrantyTypes.ExtendedWarranty, warrantyPrepped);
                }
                else if (warrantyPrepped.Contains("corrosion"))
                {
                    Add(WarrantyTypes.CorrosionWarranty, warrantyPrepped);
                }
                else if (warrantyPrepped.Contains("roadside assistance"))
                {
                    Add(WarrantyTypes.RoadsideAssistance, warrantyPrepped);
                }
            }
        }

        public void Add(Warranty warranty)
        {
            List.Add(warranty);
        }

        public void Add(WarrantyTypes warrantyType, string warrantyText)
        {
            List.Add(new Warranty(_warrantyKeyMap[warrantyType], warrantyType, warrantyText));
        }

        public bool Contains(WarrantyTypes warrantyType)
        {
            foreach (Warranty warranty in List)
            {
                if (warranty.DescriptionName == _warrantyKeyMap[warrantyType])
                {
                    return true;
                }
            }
            return false;
        }

        public Warranty GetByType(WarrantyTypes warrantyType)
        {
            foreach (Warranty warranty in List)
            {
                if (warranty.DescriptionName == _warrantyKeyMap[warrantyType])
                {
                    return warranty;
                }
            }

            throw new ApplicationException("Warranty type not found exception");
        }

        public bool HasGoodWarrantyRemaining(int minMileageLimit, int currentMileage, DateTime inServiceDate,
                                             List<WarrantyTypes> warrantiesOfInterest)
        {
            foreach (Warranty warranty in List)
            {
                if (warrantiesOfInterest.Contains(warranty.WarrantyType) &&
                    warranty.IsStillGood(currentMileage, minMileageLimit, inServiceDate, MIN_DAYS_REMAINING))
                {
                    return true;
                }
            }

            return false;
        }

        public WarrantyCollection GetWarrantiesOfInterest(List<WarrantyTypes> warrantiesOfInterest)
        {
            var retCol = new WarrantyCollection();
            foreach (Warranty warranty in List)
            {
                if (warrantiesOfInterest.Contains(warranty.WarrantyType))
                {
                    retCol.Add(warranty);
                }
            }
            return retCol;
        }


        public string GetGoodWarrantyDescriptions(int minMileageLimit, int currentMileage, DateTime inServiceDate,
                                                  int maxCt, List<WarrantyTypes> warrantiesOfInterest)
        {
            WarrantyCollection tempCol = GetWarrantiesOfInterest(warrantiesOfInterest);
            tempCol.InnerList.Sort(new WarrantyOrderer());
            string retStr = "";
            int ct = 0;

            foreach (Warranty warranty in tempCol)
            {
                if (warranty.IsStillGood(currentMileage, minMileageLimit, inServiceDate, MIN_DAYS_REMAINING))
                {
                    retStr += warranty.ToString(0) + "; ";
                    ct++;
                }
                if (ct == maxCt)
                {
                    break;
                }
            }


            //clean up weird whitespace issues
            var rg = new Regex("[\\s]{3,}");
            retStr = rg.Replace(retStr, "  "); //replace three or more with a double space

            return retStr;
        }
    }
}