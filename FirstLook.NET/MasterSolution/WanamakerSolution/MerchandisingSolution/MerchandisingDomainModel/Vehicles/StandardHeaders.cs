namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum StandardHeaders
    {
        All = 0, 
        Exterior = 1, 
        Interior = 2, 
        Safety = 3, 
        Mechanical = 4, 
        Warranty = 5, 
        FuelEconomy = 6, 
        Other = 7
    
    }
}
