using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class VehicleConditionCollection : CollectionBase
    {
       
        public VehicleConditionCollection()
        {
            
        }
        internal VehicleConditionCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(new VehicleCondition(reader));
            }
        }

        public int Add(VehicleCondition condition)
        {
            return List.Add(condition);
        }

        public string GetConditionText()
        {
            return string.Empty;
        }

        public VehicleConditionCollection Fetch(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.VehicleConditions#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        return new VehicleConditionCollection(reader);
                    }
                    
                }
            }
        }

        public int GetLevel(int conditionTypeId)
        {
            foreach (VehicleCondition condition in List)
            {
                if (condition.ConditionTypeId == conditionTypeId)
                {
                    return condition.ConditionLevel;
                }
            }
            return VehicleCondition.CONDITION_LEVEL_GOOD; //default to condition of 'good' = 2
        }

        public void SetCondition(int conditionTypeId, int conditionLevel)
        {
            foreach (VehicleCondition condition in List)
            {

                if (condition.ConditionTypeId == conditionTypeId)
                {
                    condition.ConditionLevel = conditionLevel;
                    return;
                }
               
            }
            //not found, so add it!
            List.Add(new VehicleCondition(conditionTypeId, conditionLevel, ""));
        }
        public void Save(int businessUnitId, int inventoryId)
        {
            //delete all existing entries for vehicle, then save the new ones
            DeleteAll(businessUnitId, inventoryId);
            foreach (VehicleCondition condition in List)
            {
                condition.Save(businessUnitId, inventoryId);
            }
        }

        public void DeleteAll(int businessUnitId, int inventoryId)
        {
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.VehicleConditions#Delete";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);
                    Database.AddRequiredParameter(cmd, "InventoryId", inventoryId, DbType.Int32);
                    
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public Dictionary<VehicleConditionType, string> FetchConditionTypeNames(int conditionCategory)
        {
            Dictionary<VehicleConditionType, string> retDict = new Dictionary<VehicleConditionType, string>();
            using (IDataConnection con = Database.GetConnection(Database.ChromeDatabase))
            {
                con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "builder.VehicleConditionTypes#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Database.AddRequiredParameter(cmd, "Category", conditionCategory, DbType.Int32);
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("vehicleConditionTypeId"));

                            if (!string.IsNullOrEmpty(Enum.GetName(typeof (VehicleConditionType), id)))
                            {
                                retDict.Add((VehicleConditionType) id, reader.GetString(reader.GetOrdinal("title")));
                            }
                        }
                        return retDict;
                    }
                }
            }  
        }
        
            
        
        
    }
}
