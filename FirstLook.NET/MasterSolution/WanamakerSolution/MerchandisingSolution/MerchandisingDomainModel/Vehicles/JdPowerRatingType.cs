namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public enum JdPowerRatingType
    {
        Undefined = 0,
        OverallPerformanceAndDesign,
        OverallInitialQualityStudy,
        OverallQualityMechanical,
        OverallQualityDesign,
        OverallDependability,
        
        PredictedReliability,

        QualitySubRatingPowertrain,
        QualitySubRatingBodyAndInterior,
        QualitySubRatingFeaturesAndAccessories,

        ApealSubRatingPerformance,
        ApealSubRatingComfort,
        ApealSubRatingFeaturesAndInstrumentPanel,
        ApealSubRatingStyle
    }
}
