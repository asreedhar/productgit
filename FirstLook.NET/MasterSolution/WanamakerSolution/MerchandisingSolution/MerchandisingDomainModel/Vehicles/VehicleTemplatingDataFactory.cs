﻿using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles.VehicleTemplatingDataDecorators;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class VehicleTemplatingDataFactory : IVehicleTemplatingFactory
    {
        private const string Lincoln = @"LINCOLN";
        private const string Mazda = @"MAZDA";

        public IVehicleTemplatingData GetVehicleTemplatingData(int businessUnitId, int inventoryId, string userName, ITemplateMediator mediator = null)
        {
            var vehicleTemplatingData = GetUnderLyingTemplatingData(businessUnitId, inventoryId, userName, mediator);

            // FB: 28622 - create a starting point for manufacturer specific vehicle templating rules.
            // Find out if this is a lincoln, otherwise return the original
            if (vehicleTemplatingData.InventoryItem.Make.ToUpperInvariant().Equals(Lincoln))
                return new LincolnVehicleTemplatingData(vehicleTemplatingData);

            if (vehicleTemplatingData.InventoryItem.Make.ToUpperInvariant().Equals(Mazda))
                return new MazdaVehicleTemplatingData(vehicleTemplatingData);

            return new VehicleTemplatingDataDecorator(vehicleTemplatingData);
        }

        protected internal virtual IVehicleTemplatingData GetUnderLyingTemplatingData(int businessUnitId, int inventoryId, string userName, ITemplateMediator mediator = null)
        {
            return new VehicleTemplatingData(businessUnitId, inventoryId, userName, mediator);
        }
    }
}
