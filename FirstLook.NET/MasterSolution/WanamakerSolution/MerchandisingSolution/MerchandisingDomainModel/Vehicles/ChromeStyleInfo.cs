using System.Collections;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    /// <summary>
    /// Summary description for ChromeVINInfo
    /// </summary>
    public class ChromeStyleInfo
    {
        public Hashtable PossibleStyles { get; set; }


        public int Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public decimal Destination { get; set; }

        public decimal Msrp { get; set; }

        public ChromeStyleInfo()
        {
            PossibleStyles = new Hashtable();
            Make = "Unavaialble in data source";
            Model = "";
            Year = -1;
            Destination = 0.0M; //is it safe to set defaults at zero when we're talking about cost?
            Msrp = 0.0M;
        }

        public ChromeStyleInfo(Hashtable possibleStyles, string make, string model, int year, decimal destination,
                               decimal msrp)
        {
            PossibleStyles = possibleStyles;
            Make = make;
            Model = model;
            Year = year;
            Destination = destination;
            Msrp = msrp;
        }
    }
}