namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public interface IChromeStyle
    {
        int ChromeStyleID { get; }
    }
}