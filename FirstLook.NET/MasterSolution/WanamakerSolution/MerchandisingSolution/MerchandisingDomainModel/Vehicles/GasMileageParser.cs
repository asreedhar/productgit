using System.Text.RegularExpressions;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public static class GasMileageParser
    {
        public static GasMileage Parse(string description)
        {
            var gasMileage = new GasMileage();

            var citySpaceNumber = GetCitySpaceAndNumber(description);

            gasMileage.City = string.IsNullOrEmpty(citySpaceNumber) ? GetNumber(GetNumberSpaceCity(description)) : GetNumber(citySpaceNumber);

            var hwySpaceNumber = GetHwySpaceAndNumber(description);

            gasMileage.Highway = string.IsNullOrEmpty(citySpaceNumber) ? GetNumber(GetNumberSpaceHwy(description)) : GetNumber(hwySpaceNumber);

            return gasMileage;
        }

        private static string GetCitySpaceAndNumber(string description)
        {
            var regex = new Regex(@"\bcity\b \d+", RegexOptions.IgnoreCase);
            return regex.Match(description).ToString();
        }

        private static string GetNumberSpaceCity(string description)
        {
            var regex = new Regex(@"\d+ \bcity\b", RegexOptions.IgnoreCase);
            return regex.Match(description).ToString();
        }

        private static string GetNumberSpaceHwy(string description)
        {
            var regex = new Regex(@"\d+ \bhwy\b", RegexOptions.IgnoreCase);
            return regex.Match(description).ToString();
        }

        private static string GetHwySpaceAndNumber(string description)
        {
            var regex = new Regex(@"\bhwy\b \d+", RegexOptions.IgnoreCase);
            return regex.Match(description).ToString();
        }

        private static string GetNumberFromString(string stringWithNumber)
        {
            var regex = new Regex(@"\d+", RegexOptions.IgnoreCase);
            return regex.Match(stringWithNumber).ToString();
        }

        private static int GetNumber(string stringWithNumber)
        {
            int result;
            int.TryParse(GetNumberFromString(stringWithNumber), out result);
            return result;
        }

    }
}