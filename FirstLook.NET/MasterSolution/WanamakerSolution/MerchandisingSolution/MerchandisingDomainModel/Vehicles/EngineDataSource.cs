using System.Collections.Generic;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class EngineDataSource
    {
        public static List<Engine> FetchEngines(int chromeStyleId)
        {
            return Engine.GetEngines(chromeStyleId);
        }
    }
}
