using System;
using System.Collections;
using System.Data;
using System.Linq;

namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public class CertificationFeatureCollection : CollectionBase
    {
        public CertificationFeatureCollection()
        {
            
        }

		internal CertificationFeatureCollection(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(new CertificationFeature(reader));
            }        
        }

        public int Add(CertificationFeature feature)
        {
            return List.Add(feature);
        }

        public void SetFeatureDisplayByType(CertificationFeatureType featureType, bool display)
        {
            foreach (CertificationFeature feature in List)
            {
                if (feature.FeatureTypeId == (int)featureType)
                {
                    feature.FeatureDisplayFlag = display;
                }
            }
        }

        public bool IsItemDisplayed(CertificationFeatureType featureType)
        {
            foreach (CertificationFeature feature in List)
            {
                if (feature.FeatureTypeId == (int)featureType && feature.FeatureDisplayFlag)
                {
                    return true;
                }
            }
            return false;
        }

        //currently, the features are not limited such that only one of any feature type is included
        //hence, this function is inadequate - only the first item of a type will be found
        //to change, search by featureId
        public CertificationFeature Item(CertificationFeatureType featureType)
        {
            foreach (CertificationFeature feature in List)
            {
                if (feature.FeatureTypeId == (int)featureType)
                {
                    return feature;
                }
            }
            throw new ApplicationException("Feature type not found in list");
        }

        public string GetFeatureValue(CertificationFeatureType featureType)
        {
            foreach (CertificationFeature feature in List)
            {
                if (feature.FeatureTypeId == (int)featureType)
                {
                    return feature.Description;
                }
            }
            return string.Empty;
        }

        public bool Contains(CertificationFeatureType featureType)
        {
	        return List.Cast<CertificationFeature>().Any(feature => feature.FeatureTypeId == (int) featureType);
        }

	    public CertificationFeatureCollection ProgramFeatures
        {
            get
            {
                var retList = new CertificationFeatureCollection();

                foreach (CertificationFeature feature in List)
                {
                    
                    if (feature.FeatureTypeId != (int)CertificationFeatureType.CpoSuggestedText && 
                        feature.FeatureTypeId != (int)CertificationFeatureType.DealerCustomText && 
                        feature.FeatureTypeId != (int)CertificationFeatureType.DealerCustomPreviewText)
                    {
                        retList.Add(feature);
                    }
                }
                return retList;
            }
        }
    }
}
