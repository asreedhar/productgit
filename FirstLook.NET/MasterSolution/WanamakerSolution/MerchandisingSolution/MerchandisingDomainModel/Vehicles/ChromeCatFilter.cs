namespace FirstLook.Merchandising.DomainModel.Vehicles
{
    public static class ChromeCatFilter
    {
        public const string ENGINE = "Engine";
        public const string TRANSMISSION = "Transmission";
        public const string DRIVETRAIN = "Drivetrain";
        public const string FUEL = "Fuel system";
    }
}
