﻿using System;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using MAX.Entities;

namespace FirstLook.Merchandising.DomainModel
{
    internal static class GoogleQueryHelper
    {
        internal static GoogleQueryType VDPTypeFromVehicleTypeEnum(VehicleTypeEnum vehicleType)
        {
            string gqtName = String.Format("VDP{0}", Enum.GetName(typeof(VehicleTypeEnum), vehicleType));
            
            GoogleQueryType gqt = GoogleQueryType.VDPBoth;
            Enum.TryParse(gqtName, out gqt);

            return gqt;
        }

        internal static GoogleQueryType VDPTypeFromVehicleType(VehicleType vehicleType)
        {
            string gqtName = String.Format("VDP{0}", vehicleType.StringValue);

            GoogleQueryType gqt = GoogleQueryType.VDPBoth;
            Enum.TryParse(gqtName, out gqt);

            return gqt;
        }
    }
}
