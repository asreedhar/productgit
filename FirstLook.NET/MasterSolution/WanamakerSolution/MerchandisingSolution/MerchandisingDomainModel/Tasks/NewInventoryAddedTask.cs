﻿using System;
using Core.Messages;
using Core.Messaging;
using FirstLook.Common.Core.Logging;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    /// <summary>
    /// Detects new inventory.  Issues messages of type InventoryMessage & ApplyInventoryDiscountCampaignMessage,
    /// which cause other down-stream things to happen.
    /// </summary>
    public class NewInventoryAddedTask : TaskRunner<NewInventoryMessage>
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<NewInventoryAddedTask>();

        private readonly IWorkflowRepository _workflowRepository;
        private readonly IPricingMessager _pricingMessager;
        
        private const string DiscountActivatedBy = @"New Inventory Added";

        public NewInventoryAddedTask(IQueueFactory queueFactory, IWorkflowRepository workflowRepositor, IPricingMessager pricingMessager)
            : base(queueFactory.CreateMerchandisingNewInventory())
        {
            //subscribe to the buses
            var inventoryBus = BusFactory.CreateNewInventory<NewInventoryMessage, NewInventoryMessage>();
            inventoryBus.Subscribe(ReadQueue);

            _workflowRepository = workflowRepositor;
            _pricingMessager = pricingMessager;
        }

        public override void Process(NewInventoryMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
            
			if(Log.IsDebugEnabled)
                Log.DebugFormat("Processing NewInventoryMessage: BusinessUnitId: {0}, InventoryId: {1}",
                                message.BusinessUnitId, message.InventoryId);

            if (!HasMaxAdUpgrade(message.BusinessUnitId))
                return;

            try
            {
                OptionsConfiguration.InsertMissingOptionsConfigurationRecords(message.BusinessUnitId, message.InventoryId);
                OptionsConfiguration.UpdateOptionsConfigurationChromeStyleIdField(message.BusinessUnitId, message.InventoryId);

                //This is to load all standard equipment and stuff from lot
                VehicleConfiguration.FetchOrCreateDetailed(message.BusinessUnitId, message.InventoryId, "NewInventoryTask");

                SendInventoryChanged(message.BusinessUnitId, message.InventoryId);

                Log.DebugFormat("Sending New Inventory ApplyInventoryDiscountCampaignMessage: BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
                SendApplyDiscount(message.BusinessUnitId, message.InventoryId);

                if (Log.IsDebugEnabled)
                    Log.DebugFormat("Done Processing NewInventoryMessage: BusinessUnitId: {0}, InventoryId: {1}",
                                    message.BusinessUnitId, message.InventoryId);
            }
            catch (Exception ex)
            {
                Log.Error("Failed while creating OptionsConfiguration record.", ex);
                throw;
            }
        }

        private void SendApplyDiscount(int businessUnitId, int inventoryId)
        {
            _pricingMessager.SendApplyDiscountMessage(businessUnitId, inventoryId, DiscountActivatedBy);
        }

        private void SendInventoryChanged(int businessUnitId, int inventoryId)
        {
            //we send if there is no ad. This is to stop the timing issues if the trim is set after auto approve runs for new inventory
            var inventoryData = InventoryData.Fetch(businessUnitId, inventoryId);
            var workflow = _workflowRepository.GetWorkflow(WorkflowType.CreateInitialAd);

            if (workflow.Filter(inventoryData)) //if doesn't have an ad
                _pricingMessager.SendInventoryChangedMessage(businessUnitId, inventoryId);
        }

        private static bool HasMaxAdUpgrade(int businessUnitId)
        {
            var businessUnit = BusinessUnitFinder.Instance().Find(businessUnitId);
            return businessUnit != null && businessUnit.HasDealerUpgrade(Upgrade.Merchandising);
        }
    }
}