﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Merchandising.Messages;
using Merchandising.Messages.WebLoader;
using Convert = System.Convert;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    internal class WebLoaderDirectPhotoUploadTask : TaskRunner<DirectPhotoUploadMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IWebLoaderPhotoRepository _webLoaderPhotoRepository;
        private readonly IFileStorage _imageStore;
        private readonly IWebloaderMessager _webloaderMessager;

        //private const String IosString = "IOS";
        //private const String AndroidString = "Android";
        private const String UnknownOs = "Mobile";

        private const String AppVersion = "2.0";


        public WebLoaderDirectPhotoUploadTask(IQueueFactory queueFactory, IWebLoaderPhotoRepository webLoaderPhotoRepository, IFileStoreFactory fileStoreFactory, IWebloaderMessager webloaderMessager)
            : base(queueFactory.CreateWebLoaderDirectPhotoUpload())
        {
            _webLoaderPhotoRepository = webLoaderPhotoRepository;
            _imageStore = fileStoreFactory.WebLoaderApiPhotos();
            _webloaderMessager = webloaderMessager;
        }

        public override void Process(DirectPhotoUploadMessage message)
        {
            /*---------------------------------------------------------------------------------------------------
             Get the position offset for this vehicle. If we have not seen any photos for this vehicle before
             or there the position offsets are all null (older version) the return value is 0.  If we have seen 
             photos before, the return value is the max position offset for this vehicle in WebLoaderPhotos)
            
             If the position offset is 0, query aultec to see if any photos have already 
             been uploaded using a different mechanism (lot loader or photo manager) and insert the offset
             into the database.  
             
             This offset will then be used for every subsequent photo uploaded for this vehicle via the webloader
             Note that going back and forth between the photo manager and the webloader for the same vehicle 
             might result in photos being overwritten.
             -----------------------------------------------------------------------------------------------------*/
            
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("DealerCode: {0}, PhotoGuid: {1}", message.Dealercode, message.Guid);
            var tTime = DateTime.ParseExact(message.Timestamp, "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
            const string mobileAppVersion = UnknownOs + " " + AppVersion;

            string firstName;
            string lastName;
            int offset;
            // Hit merchandising db to get vehicle and user details
            var photo = _webLoaderPhotoRepository.GetPhoto(message.VehicleId, message.UserName, message.Guid,
                message.Timestamp, message.Position, out firstName, out lastName);

            if (photo == null)
            {
                Log.DebugFormat("Unable to get vehicle details for this inventory id.  Message will be retried. DealerCode: {0}, InventoryId: {1}, Guid: = {2}, Position = {3}", message.Dealercode, message.VehicleId, message.Guid, message.Position);
                throw new InvalidOperationException(
                        String.Format(
                            "Vehicle was not found.  It may no longer be active. Inv Id {0}, Guid {1}",
                            message.VehicleId, message.Guid));
            }

            if (_webLoaderPhotoRepository.ExistsForInventory(message.VehicleId))
                offset = _webLoaderPhotoRepository.GetOffsetForInventory(message.VehicleId);
            else
                offset = GetPhotoCount(message.Dealercode, photo.Vin);

            
            // check to see if we have already seen this particular image Guid
            if (_webLoaderPhotoRepository.Exists(message.VehicleId, message.Guid))
            {
                // But then there is not point sending the photoUpload message if the photo isn't available in S3 so check that.
                // An exception is being thrown here intentionally so that message is retried and will be visible since this likely
                // indicates a problem.  Possibly with the webloader mobile app.
                if (!_imageStore.ExistsNoStream(message.Guid + ".jpg"))
                {
                    Log.DebugFormat("Processing DirectPhotoUploadMessage, found retried message: Image not found in S3. DealerCode: {0}, InventoryId: {1}, Guid: = {2}, Position = {3}", message.Dealercode, message.VehicleId, message.Guid, message.Position);
                    throw new InvalidOperationException(
                        String.Format(
                            "DirectPhotoUpload message was resent and no photo was found in S3 for Inv Id {0}, Guid {1}",
                            message.VehicleId, message.Guid));
                }
            }
            else
            {
                // If we haven't seen this image, insert it into the database
                _webLoaderPhotoRepository.InsertWithOffset(message.VehicleId, message.Position, tTime.ToLocalTime(), message.Batch, message.Guid, message.UserName, offset, mobileAppVersion);    
            }
            
            //Catch 1 error before the message goes on the dead letter
            if (message.Errors.Length >= ReadQueue.DeadLetterErrorLimit - 1) //the batch has errored. Need to error out the photos
            {
                _webLoaderPhotoRepository.SetError(message.VehicleId, message.Guid);
                RequeueWithError(message, message.Errors.Last());
                return;
            }

            int newPosition = message.Position + offset;
            var photoMessage = new PhotoUploadMessage(message.Dealercode, message.Lock, photo.Vin, photo.StockNumber,
                message.VehicleId, newPosition, message.Timestamp, message.Guid, message.Guid.ToString() + ".jpg",
                message.UserName, firstName, lastName);

            Log.DebugFormat("Sending upload message for photo {0}, inventory id {1}, VIN {2}, Position {3}, dealer {4}", message.Guid, message.VehicleId, 
                photo.Vin, message.Position, message.Dealercode);

            _webloaderMessager.SendPhotoUploadMessage(photoMessage);
        }

        private int GetPhotoCount(string dealerCode, string vin)
        {
            int count = 0;
            string url = string.Format("https://incisent.aultec.net/photoquery/{0}/vinPhotos/{1}", dealerCode, vin);
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request != null)
            {
                request.Timeout = (int)TimeSpan.FromSeconds(60).TotalMilliseconds;

                var authInfo = "flrestapi:w@t3rl0g$";
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                request.Headers["Authorization"] = "Basic " + authInfo;

                try
                {
                    using (var response = request.GetResponse() as HttpWebResponse)
                        if (response != null)
                            using (var responseStream = response.GetResponseStream())
                            {
                                var doc = new XmlDocument();
                                if (responseStream != null) doc.Load(responseStream);

                                var list = doc.SelectNodes("//url");
                                if (list != null)
                                    count = list.Count;
                            }
                }
                catch (WebException e)
                {
                    var response = e.Response as HttpWebResponse;
                    if (response != null && response.StatusCode == HttpStatusCode.NotFound) //aultec doesn't know about this vehicle.
                        count = 0;
                    else
                        throw;
                }
            }

            return count;
        }
    }
}
