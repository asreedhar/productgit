using System;
using System.Configuration;
using System.Text;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Tasks;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Release.Tasks;
using FirstLook.Merchandising.DomainModel.Workflow;
using FirstLook.Merchandising.DomainModel.Workflow.AutoOffline;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.Impl.V_2_0;
using MAX.Threading;
using Merchandising.Messages;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    /// <summary>
    /// The "kitchen sink".  Needs to be broken up.  The constructor makes me very sad.  ZB 05/2014
    /// </summary>
    internal class MerchandisingTaskThreadStarter : IMerchandisingTaskThreadStarter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IQueueFactory _queueFactory;
        private readonly IFileStoreFactory _fileStoreFactory;
        private readonly IMarketListingSearchAdapter _searchAdapter;
        private readonly ILogger _logger;
        private readonly IEmail _emailProvider;
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IDashboardRepository _dashboardRepository;
        private readonly IWebLoaderPhotoRepository _webLoaderRepository;
        private readonly IInventorySearch _inventorySearch;
        private readonly IDiscountPricingCampaignManager _discountPricingCampaignManager;
        
        private readonly Func<AutoOfflineProcessorTask> _autoOfflineProcessorTask;
        private readonly IPhotoServices _photoServices;
        private readonly IDealerServices _dealerServices;
        private readonly IPhotoTransferDataAccess _vehicleTransferDataAccess;
        private readonly IGoogleAnalyticsResultCache _googleResultCache;
        private readonly IEquipmentRepository _equipmentRepository;
        private readonly IOptionsRepository _optionsRepository;
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IVehicleReferenceDataRepository _vehicleReferenceDataRepository;
        private readonly ICarFaxUrlBuilder _carFaxUrlBuilder;
        private readonly IEmailQueueFactory _emailQueueFactory;
        private readonly INoSqlDbFactory _noSqlDbFactory;
        private readonly IDocumentPublishingQueueFactory _documentPublishingQueueFactory;
        private readonly IDiscountCampaignQueueFactory _discountCampaignQueueFactory;
	    private readonly IMarketingDocumentRepository _marketingDocumentRepository;
        private readonly IAdMessageSender _messageSender;
        private readonly IDocumentPublishMessager _documentPublishMessager;
        private readonly IPricingMessager _pricingMessager;
        private readonly IWebloaderMessager _webloaderMessager;

	    private readonly object _sync = new object();

        public MerchandisingTaskThreadStarter(IQueueFactory queueFactory, IFileStoreFactory fileStoreFactory,IMarketListingSearchAdapter searchAdapter, ILogger logger,
            IEmail emailProvider, IWorkflowRepository workflowRepository, IDashboardRepository dashboardRepository, IWebLoaderPhotoRepository webLoaderRepository,
            Func<AutoOfflineProcessorTask> autoOfflineProcessorTask, IPhotoServices photoServices, IDealerServices dealerServices, IPhotoTransferDataAccess vehicleTransferDataAccess,
            IGoogleAnalyticsResultCache googleResultCache, IDiscountPricingCampaignManager discountPricingCampaignManager, 
            IInventorySearch inventorySearch, IEquipmentRepository equipmentRepository, IOptionsRepository optionsRepository, 
            IReferenceDataRepository referenceDataRepository, IVehicleReferenceDataRepository vehicleReferenceDataRepository, ICarFaxUrlBuilder carFaxUrlBuilder,
            IEmailQueueFactory emailQueueFactory, INoSqlDbFactory noSqlDbFactory, IDocumentPublishingQueueFactory documentQueueFactory, IDiscountCampaignQueueFactory discountQueueFactory,
			IMarketingDocumentRepository marketingDocumentRepository, IDocumentPublishMessager documentPublishMessager, IPricingMessager pricingMessager, IWebloaderMessager webloaderMessager, IAdMessageSender messageSender)
        {
            _queueFactory = queueFactory;
            _fileStoreFactory = fileStoreFactory;
            _searchAdapter = searchAdapter;
            _logger = logger;
            _emailProvider = emailProvider;
            _workflowRepository = workflowRepository;
            _dashboardRepository = dashboardRepository;
            _webLoaderRepository = webLoaderRepository;
            _photoServices = photoServices;
            _autoOfflineProcessorTask = autoOfflineProcessorTask;
            _photoServices = photoServices;
            _dealerServices = dealerServices;
            _vehicleTransferDataAccess = vehicleTransferDataAccess;
            _googleResultCache = googleResultCache;
            _discountPricingCampaignManager = discountPricingCampaignManager;
            _inventorySearch = inventorySearch;
            _equipmentRepository = equipmentRepository;
            _optionsRepository = optionsRepository;
            _referenceDataRepository = referenceDataRepository;
            _vehicleReferenceDataRepository = vehicleReferenceDataRepository;
            _carFaxUrlBuilder = carFaxUrlBuilder;
            _emailQueueFactory = emailQueueFactory;
            _noSqlDbFactory = noSqlDbFactory;
            _documentPublishingQueueFactory = documentQueueFactory;
            _discountCampaignQueueFactory = discountQueueFactory;
	        _marketingDocumentRepository = marketingDocumentRepository;
            _documentPublishMessager = documentPublishMessager;
            _pricingMessager = pricingMessager;
            _webloaderMessager = webloaderMessager;
            _messageSender = messageSender;
        }

        private void StartPdfGeneratorWorkers(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages )
        {
            NamedBackgroundThread.Start("Worker - CreatePdfTask", 
                () => new CreatePdfTask(_queueFactory, _searchAdapter, _logger).Run(shouldCancel, blockWhileNoMessages));
        }

        private void StartApproveWorkers(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            NamedBackgroundThread.Start("Worker - ApproveTask", 
                () => new ApproveTask(_queueFactory, _messageSender).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - AutoApproveTask",
                () => new AutoApproveTask(_queueFactory, _discountPricingCampaignManager, _messageSender).Run(shouldCancel, blockWhileNoMessages));
        }

        /// <summary>
        ///  These tasks monitor for different changes and issue auto-approve messages (and others) in response.
        /// </summary>
        private void StartAutoApproveTriggers(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            NamedBackgroundThread.Start("Worker - AutoApproveInventoryTask",
                () => new AutoApproveInventoryTask(_queueFactory, _messageSender).Run(shouldCancel, blockWhileNoMessages));
            NamedBackgroundThread.Start("Worker - AutoApproveFilterTask",
                () => new AutoApproveFilterTask(_queueFactory, _messageSender).Run(shouldCancel, blockWhileNoMessages));
            NamedBackgroundThread.Start("Worker - AutoApproveBatchMessageTask", 
                () => new BatchPriceMessageTask(_discountCampaignQueueFactory, _pricingMessager).Run(shouldCancel, blockWhileNoMessages));
            NamedBackgroundThread.Start("Worker - NewInventoryAddedTask", 
                () => new NewInventoryAddedTask(_queueFactory, _workflowRepository, _pricingMessager).Run(shouldCancel, blockWhileNoMessages));
        }

        private void StartDeadLetterHandlers(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            NamedBackgroundThread.Start("Worker - DeadLetterArchiver", 
                () => new DeadLetterArchive(_queueFactory, _fileStoreFactory).Run(shouldCancel, blockWhileNoMessages));
        }

        private void StartDiscountCampaignHandlers(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            NamedBackgroundThread.Start("Worker - DiscountCampaign Task", 
                () => new DiscountCampaignExpirationTask(_discountCampaignQueueFactory, _discountPricingCampaignManager).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - ApplyInventoryDiscountCampaignTask",
                () => new ApplyInventoryDiscountCampaignTask(_discountCampaignQueueFactory, _discountPricingCampaignManager, _inventorySearch).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - CampaignStyleSetTask",
                () => new CampaignStyleSetTask(_pricingMessager).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - ReprocessInventoryForCampaigns", 
                () => new ReprocessInventoryForDiscountCampaigns(_discountCampaignQueueFactory, _pricingMessager).Run(shouldCancel, blockWhileNoMessages));
        }

        private void StartMaxPublishingTasks(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
			NamedBackgroundThread.Start("Worker - InventoryActivityTask",
				() => new InactiveInventoryTask(_queueFactory, _fileStoreFactory, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

			NamedBackgroundThread.Start("Worker - MarketingDocumentPublisherTask",
                () => new MaxMarketingDocumentPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, _marketingDocumentRepository, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - MarketingHTMLDocumentPublisherTask",
                () => new MarketingHtmlDocumentPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, _marketingDocumentRepository, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - EquipmentDocumentPublisherTask",
                () => new MaxEquipmentDocumentPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, _equipmentRepository, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

			NamedBackgroundThread.Start("Worker - OptionDocumentPublisherTask",
                () => new OptionDocumentPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, _optionsRepository, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

			NamedBackgroundThread.Start("Worker - referenceDocumentPublisherTask",
                () => new ReferenceDocumentPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, _referenceDataRepository, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

			NamedBackgroundThread.Start("Worker - adDescriptionDocumentPublisherTask",
                () => new AdDescriptionDocumentPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, Encoding.UTF8, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

			NamedBackgroundThread.Start("Worker - carFaxReportUrlPublisherTask",
                () => new CarFaxReportUrlPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, _carFaxUrlBuilder, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

			NamedBackgroundThread.Start("Worker - photoDocumentGenerationTask",
				() => new PhotoDocumentGeneration(_documentPublishingQueueFactory, _photoServices, _documentPublishMessager).Run(shouldCancel, blockWhileNoMessages));

			int threadCount = int.Parse(ConfigurationManager.AppSettings["PhotoDocumentPublishThreads"]);

			for (int x = 0; x < threadCount; x++)
			{
				NamedBackgroundThread.Start("Worker - photoPublishTask",
					() =>
                        new PhotoDocumentPublishTask(_documentPublishingQueueFactory, _fileStoreFactory, _documentPublishMessager).Run(
							shouldCancel, blockWhileNoMessages));
			}
        }

        private void StartWebLoaderTasks(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            NamedBackgroundThread.Start("Worker - WebLoaderPhotoUploadTask",
                () => new WebLoaderPhotoUploadTask(_webLoaderRepository, _queueFactory, _fileStoreFactory).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - WebLoaderAbsolutePositionTask",
                () => new WebLoaderAbsolutePositionTask(_queueFactory, _webLoaderRepository, _webloaderMessager).Run(shouldCancel, blockWhileNoMessages));

            NamedBackgroundThread.Start("Worker - WebLoaderDirectPhotoUploadTask",
                () => new WebLoaderDirectPhotoUploadTask(_queueFactory, _webLoaderRepository, _fileStoreFactory, _webloaderMessager).Run(shouldCancel, blockWhileNoMessages));
        }

        public void Run(Func<bool> shouldCancel, Func<bool> blockWhileNoMessages)
        {
            Log.Info("Merchandising threads starting.");

            lock (_sync)
            {
                StartApproveWorkers(shouldCancel, blockWhileNoMessages);
                StartPdfGeneratorWorkers(shouldCancel, blockWhileNoMessages);

                StartAutoApproveTriggers(shouldCancel, blockWhileNoMessages);
                StartDeadLetterHandlers(shouldCancel, blockWhileNoMessages);
                StartDiscountCampaignHandlers(shouldCancel, blockWhileNoMessages);

                StartWebLoaderTasks(shouldCancel, blockWhileNoMessages);
                StartMaxPublishingTasks(shouldCancel, blockWhileNoMessages);


                NamedBackgroundThread.Start("Worker - DailyEmailTask",
                    () => new DailyEmailTask(_emailQueueFactory, _emailProvider).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - BucketAlertsTask",
                    () => new BucketAlertTask(_emailQueueFactory, _emailProvider, _dashboardRepository, _noSqlDbFactory, _messageSender).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - BucketValueTask",
                    () => new BucketValueTask(_emailQueueFactory, _noSqlDbFactory).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - AutoOfflineProcessorTask",
                    () => _autoOfflineProcessorTask().Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - PhotoTransferTask",
                    () => new PhotoTransferTask(_queueFactory, _photoServices, _dealerServices, _vehicleTransferDataAccess).Run(shouldCancel, blockWhileNoMessages));

                NamedBackgroundThread.Start("Worker - GoogleAnalyticsGenerateTask",
                    () => new GoogleAnalyticsGenerateTask(_queueFactory, _googleResultCache).Run(shouldCancel, blockWhileNoMessages));

			}
        }
    }
}
