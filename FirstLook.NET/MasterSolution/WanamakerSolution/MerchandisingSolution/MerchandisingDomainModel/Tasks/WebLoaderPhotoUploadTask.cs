﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Core.Messages;
using Core.Messaging;
using FirstLook.Common.Core.Utilities;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Merchandising.Messages;
using Merchandising.Messages.WebLoader;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    internal class WebLoaderPhotoUploadTask : TaskRunner<PhotoUploadMessage>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string AulTechBasePath = @"https://incisent.aultec.net/photoupload";
        private const string WhiteListKey = @"BusinessUnitCode_PhotoWhiteList";

        private IFileStorage _imageStore;
        private IWebLoaderPhotoRepository _repository;

        public WebLoaderPhotoUploadTask(IWebLoaderPhotoRepository repository, IQueueFactory queueFactory, IFileStoreFactory fileStoreFactory)
            : base(queueFactory.CreateWebLoaderPhotoUpload())
        {
            _imageStore = fileStoreFactory.WebLoaderApiPhotos();
            _repository = repository;
        }

        private static void SetBasicAuthHeader(WebRequest req, String userName, String userPassword)
        {
            string authInfo = userName + ":" + userPassword;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            req.Headers["Authorization"] = "Basic " + authInfo;
        }

        public override void Process(PhotoUploadMessage message)
        {
            //Skip dealers not in white list because Aultec only has one environment
			var threadId = string.Format("DealerCode: {0}, InventoryId: {1}, PhotoGuid: {2}", message.Dealercode, message.InventoryId, message.Guid);
			log4net.ThreadContext.Properties["threadId"] = threadId;

            if (!WhiteListHelper.IsInWhiteList(message.Dealercode, ConfigurationManager.AppSettings[WhiteListKey] ?? ""))
            {
                Log.DebugFormat("Photo Upload Message not processed: Dealer is not in whitelist. DealerCode: {0}, InventoryId: {1}, Guid: = {2}, Position = {3}", message.Dealercode, message.InventoryId, message.Guid, message.Position);
                return;
            }

            //If not in s3 return. Messages are can get replayed is amazon has error deleting them
            if (!_imageStore.ExistsNoStream(message.S3FileName))
            {
                Log.DebugFormat("Photo Upload Message not processed: Image not found in S3DealerCode: {0}, InventoryId: {1}, Guid: = {2}, Position = {3}", message.Dealercode, message.InventoryId, message.Guid, message.Position);
                return;
            }

            //Catch 1 error before the message goes on the dead letter
            if(message.Errors.Length >= ReadQueue.DeadLetterErrorLimit - 1)
            {
                _repository.SetError(message.InventoryId, message.Guid);
                RequeueWithError(message, message.Errors.Last());
                return;
            }

            Log.DebugFormat("Photo Upload: DealerCode: {0}, InventoryId: {1}, Guid: = {2}, Position = {3}", message.Dealercode, message.InventoryId, message.Guid, message.Position);

            string path = string.Format("{0}/{1}/{2}?lock={3}&t={4}&rnd={5}&uid={6}&fname={7}&lname={8}&position={9}&snum={10}",
                AulTechBasePath, message.Dealercode, message.Vin, message.Lock, message.T, message.Guid, message.User, message.FirstName, message.LastName, message.Position, message.StockNumber);
            
            Log.DebugFormat("Calling AulTec with path: {0} - InventoryId: {1}", path, message.InventoryId);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);
            request.ServicePoint.Expect100Continue = false;  
            request.Method = "POST";
            request.ContentType = "image/jpeg";

            SetBasicAuthHeader(request, "flrestapi", "w@t3rl0g$");

            using (Stream stream = _imageStore.Read(message.S3FileName))
            using (BinaryReader reader = new BinaryReader(stream))
            using (var requestStream = request.GetRequestStream())
            using (BinaryWriter binaryWriter = new BinaryWriter(requestStream))
            {
                byte[] image = reader.ReadBytes((int)stream.Length);
                binaryWriter.Write(image);
            }
            
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                int statusCode = (int)response.StatusCode;
                if (statusCode >= 200 && statusCode < 300) //code is in the 200's must be good
                {
                    //record as successful
                    _repository.MarkUploaded(message.InventoryId, message.Guid);
                    _imageStore.Delete(message.S3FileName);
                }
                else
                {
                    throw new WebException(response.StatusDescription);
                }
            }

        }
    }
}
