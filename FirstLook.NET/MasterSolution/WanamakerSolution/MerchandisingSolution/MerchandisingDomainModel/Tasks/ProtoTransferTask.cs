﻿using System;
using System.Text.RegularExpressions;
using System.Linq;
using Core.Messaging;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Merchandising.Messages;
using Merchandising.Messages.VehicleTransfer;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    public class PhotoTransferTask : TaskRunner<PhotoTransferMessage>
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<PhotoTransferTask>();

        private readonly IDealerServices _dealerServices;
        private readonly IPhotoServices _photoServices;
        private readonly IPhotoTransferDataAccess _dao;

        public PhotoTransferTask(IQueueFactory queueFactory, IPhotoServices photoServices, IDealerServices dealerServices, IPhotoTransferDataAccess dao) : base(queueFactory.CreatePhotoTransferQueue())
        {
            _photoServices = photoServices;
            _dealerServices = dealerServices;
            _dao = dao;
        }
        
        public override void Process(PhotoTransferMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.DstBusinessUnitId, message.DstInventoryId);
            if (message.Errors.Length == 0)
            {
                // This is the first time we've seen the message
                Log.Debug("Processing " + message);
                _dao.UpdateTransferStatus(message.DstInventoryId, TransferStatusCode.Pending);    
            }
            else if (message.Errors.Length == ReadQueue.DeadLetterErrorLimit - 1)
            {
                // This is the last time we will see this message
                _dao.UpdateTransferStatus(message.DstInventoryId, TransferStatusCode.GeneralFailure);
                RequeueWithError(message, message.Errors.Last());
                return;
            }
            
            // Fetch photos for source vehicle

            var srcPhotos = GetPhotos(message.SrcBusinessUnitCode, message.Vin);
            if (srcPhotos.PhotoUrls.Length == 0)
            {
                Log.Debug("No source photos");
                _dao.UpdateTransferStatus(message.DstInventoryId, TransferStatusCode.NoSourcePhotos);
                return;
            }

            // Fetch photos for destination vehicle. Really just need a count.
            
            var dstPhotos = GetPhotos(message.DstBusinessUnitCode, message.Vin);
            if (dstPhotos.PhotoUrls.Length > 0)
            {
                Log.Debug("Destination already has photos.");
                _dao.UpdateTransferStatus(message.DstInventoryId, TransferStatusCode.DestinationHasPhotos);
                return;
            }
            
            // Copy photos from source inventory to desintation inventory

            int seqNum = 0;
            foreach (var url in srcPhotos.PhotoUrls)
            {

                Log.Debug("Transferring photo " + url);

                // bugzID: 27402 URL substritution so AULTect can harvest image from themselves, tureen 9/27/2013
                string sCorrectedURL = Regex.Replace(url, @"https://incisent.aultec.net/v/", @"http://i.aultec.com/v/71/", RegexOptions.IgnoreCase);

                _dao.TransferPhoto(message.DstInventoryId, sCorrectedURL, seqNum, /* IsPrimary? */ seqNum == 0);  // First photo returned is primary
                
                
                //_dao.TransferPhoto(message.DstInventoryId, url, seqNum, /* IsPrimary? */ seqNum == 0);  // First photo returned is primary
                seqNum += 1;
            }
            
            // done

            _dao.UpdateTransferStatus(message.DstInventoryId, TransferStatusCode.Success);
        }
        
        /// <summary>
        /// Fetch photos for the given inventory and log any errors.
        /// </summary>
        /// <param name="dealerCode"></param>
        /// <param name="vin"></param>
        /// <returns></returns>
        private IVehiclePhotosResult GetPhotos(string dealerCode, string vin)
        {
            IVehiclePhotosResult photos = _photoServices.GetPhotoUrlsByVin(dealerCode, vin, TimeSpan.FromMinutes(1));

            foreach (var error in photos.Errors)
                Log.Debug(String.Format("Error {0} {1}", error.Code, error.Message));

            return photos;
        }
    }
}
