﻿using System.Reflection;
using Core.Messages;
using Core.Messaging;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    public class CampaignStyleSetTask : TaskRunner<StyleSetMessage>
    {
        private readonly IPricingMessager _pricingMessager;
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public CampaignStyleSetTask(IPricingMessager pricingMessager)
            : base(pricingMessager.CreateStyleSetQueue())
        {
            var styleSetBus = pricingMessager.CreateStyleSetBus();
            styleSetBus.Subscribe(ReadQueue);
            _pricingMessager = pricingMessager;
        }

        public override void Process(StyleSetMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, InventoryId: {1}", message.BusinessUnitId, message.InventoryId);
			Log.Info("Sending ApplyInventoryDiscountCampaignMessage");

            _pricingMessager.SendApplyInventoryDiscountCampaignMessage(message);
        }
    }
}
