using System.Linq;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using log4net;
using Merchandising.Messages;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    internal class DiscountCampaignExpirationTask : TaskRunner<DiscountCampaignExpirationMessage>
    {
        private readonly IDiscountPricingCampaignManager _campaignManager;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string AutoUserName = "Automatic Campaign Expiration";

        public DiscountCampaignExpirationTask(IDiscountCampaignQueueFactory queueFactory, IDiscountPricingCampaignManager campaignManager) : base(queueFactory.CreateDiscountCampaignExpirationQueue())
        {
            _campaignManager = campaignManager;
        }

        public override void Process(DiscountCampaignExpirationMessage message)
        {
	        LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}, CampaignId: {1}", message.BusinessUnitId, message.CampaignId);

            Log.Info("Expiring Old Discount Campaigns");
            // This message has the campaign and a list of inventoryids.
            // Foreach inventory id, if there is an "OldSpecialPrice", reset that, 
            // otherwise delete the new car price?
            // Always set the inventory as Deactivated
            var campaign =
                _campaignManager.FetchCampaign(message.BusinessUnitId, message.CampaignId);
            
            if (campaign != null)
            {
                Log.InfoFormat("Calling ExpireDiscountCampaign for {0} inventory items.", campaign.InventoryList.Count);
                _campaignManager.ExpireDiscountCampaign(campaign, AutoUserName);
            }
        }
    }
}