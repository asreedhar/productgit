﻿using System.Reflection;
using Core.Messaging;
using Merchandising.Messages.AutoApprove;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    /// <summary>
    /// Issues PriceChangeMessages for each inventoryId in the received BatchPriceChangeMessage.
    /// </summary>
    public class BatchPriceMessageTask : TaskRunner<BatchPriceChangeMessage>
    {
	    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IPricingMessager _pricingMessager;

        public BatchPriceMessageTask(IDiscountCampaignQueueFactory queueFactory, IPricingMessager pricingMessager)
            : base(queueFactory.CreateBatchPriceChangeQueue())
        {
            _pricingMessager = pricingMessager;
        }

		
        public override void Process(BatchPriceChangeMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}", message.BusinessUnitId);
			Log.Info("Processing BatchPriceChangeMessage for BusinessUnitId:");

            _pricingMessager.SendBatchPriceMessages(message.BusinessUnitId, message.InventoryIds);
        }
    }
}