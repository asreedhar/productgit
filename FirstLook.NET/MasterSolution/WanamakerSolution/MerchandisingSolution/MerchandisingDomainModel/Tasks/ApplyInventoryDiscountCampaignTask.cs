﻿using System.Reflection;
using Core.Messaging;
using FirstLook.Common.Core.Command;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.QueueFactories;
using ILog = log4net.ILog;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    public class ApplyInventoryDiscountCampaignTask : TaskRunner<ApplyInventoryDiscountCampaignMessage>
    {
        private readonly IDiscountPricingCampaignManager _campaignManager;
        private readonly IInventorySearch _inventorySearch;

	    #region Logging


	    protected static readonly ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


	    #endregion Logging

        public ApplyInventoryDiscountCampaignTask(IDiscountCampaignQueueFactory queueFactory, IDiscountPricingCampaignManager campaignManager, IInventorySearch inventorySearch)
            : base(queueFactory.CreateApplyInventoryDiscountCampaignQueue())
        {
            _campaignManager = campaignManager;
            _inventorySearch = inventorySearch;
        }

        public override void Process(ApplyInventoryDiscountCampaignMessage message)
        {
            Log.InfoFormat("Begin InventoryDiscountCampaign Message for BU: {0} , InventoryId: {1}",
                message.BusinessUnitId, message.InventoryId);

            var cmd = new ApplyDiscountCampaignToInventoryCommand(_campaignManager, _inventorySearch,
                message.BusinessUnitId, message.InventoryId, message.ActivatedBy,
                message.Force);

            AbstractCommand.DoRun(cmd);

            Log.InfoFormat("Finished InventoryDiscountCampaign Message for BU: {0} , InventoryId: {1}",
                message.BusinessUnitId, message.InventoryId);
        }
    }
}