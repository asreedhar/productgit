﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Core.Messaging;
using log4net;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.InventoryDocuments;
using Merchandising.Messages.QueueFactories;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    class ReprocessInventoryForDiscountCampaigns : TaskRunner<DealerCommand>
    {
        private readonly IPricingMessager _pricingMessager;

	    #region Logging


	    protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


	    #endregion Logging

        public ReprocessInventoryForDiscountCampaigns(IDiscountCampaignQueueFactory queueFactory, IPricingMessager pricingMessager)
            : base(queueFactory.CreateReprocessDealerInventoryForDiscountsQueue())
        {
            _pricingMessager = pricingMessager;
        }

        public override void Process(DealerCommand message)
        {
            Log.DebugFormat("Dealer command Reprocess inventory for discount campaigns message for BU: {0}",
                message.BusinessUnitId);

            // For this BU obtain a list of active and recently active inventory
            var inventoryIds = GetActiveAndRecentlyActiveInventoryForBU(message.BusinessUnitId).ToArray();
			Log.InfoFormat("Found {0} inventory ids.", inventoryIds.Length);

			// For this BU obtain a list of inventory that is part of a campaign but does not have a special price
	        var forceInventoryIds = GetForceInventoryForBU(message.BusinessUnitId).ToArray();
			Log.InfoFormat("Found {0} inventory ids with a special price that does not match the calculated discount price.", forceInventoryIds.Length);
			
			// Send a message with the Force flag set so that these inventory are fully reprocessed
            _pricingMessager.SendApplyInventoryDiscountCampaignMessage(message.BusinessUnitId, forceInventoryIds, "ReprocessInventoryForDiscountCampaigns", true);
			
            // Send an ApplyDiscountCampaignToInventory message for each inventory id that was not in the Force list
            _pricingMessager.SendApplyInventoryDiscountCampaignMessage(message.BusinessUnitId, inventoryIds.Except(forceInventoryIds), "ReprocessInventoryForDiscountCampaigns", false);

            Log.DebugFormat("Finished Dealer command Reprocess inventory for discount campaigns message for BU: {0}", message.BusinessUnitId);
        }

	    private IEnumerable<int> GetForceInventoryForBU(int businessUnitId)
	    {
		    var inventoryList = new List<int>();
		    using (var con = Database.GetOpenConnection(Database.MerchandisingDatabase))
		    {
				using (var cmd = con.CreateCommand())
				{
					cmd.CommandText = "merchandising.DiscountPricingCampaignInventory#InAnInvalidState";
					cmd.CommandType = CommandType.StoredProcedure;
					Release.Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

					using (var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							var id = reader.GetInt32(reader.GetOrdinal("InventoryID"));
							inventoryList.Add(id);
						}
					}
				}   
		    }
		    return inventoryList;
	    }

	    private IEnumerable<int> GetActiveAndRecentlyActiveInventoryForBU(int businessUnitId)
        {
            var vehicles = new List<int>();

            using (var con = Release.Database.GetConnection(Release.Database.MerchandisingDatabase))
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
					cmd.CommandText = "merchandising.ActiveInventoryByBU#Fetch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    Release.Database.AddWithValue(cmd, "BusinessUnitId", businessUnitId, DbType.Int32);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var id = reader.GetInt32(reader.GetOrdinal("InventoryID"));
                            vehicles.Add(id);
                        }
                    }
                }
            }

            return vehicles;   
        }
    }
}
