﻿using System;
using System.Linq;
using Core.Messaging;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using Merchandising.Messages;
using Merchandising.Messages.GoogleAnalytics;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    internal class GoogleAnalyticsGenerateTask : TaskRunner<GoogleAnalyticsGenerateMessage>
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<GoogleAnalyticsGenerateTask>();

        IGoogleAnalyticsResultCache _resultCache;

        public GoogleAnalyticsGenerateTask(IQueueFactory queueFactory, IGoogleAnalyticsResultCache resultCache)
            : base(queueFactory.CreateGoogleAnalyticsQueue())
        {
            _resultCache = resultCache;
        }

        public override void Process(GoogleAnalyticsGenerateMessage message)
        {
			log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BusinessUnitId: {0}", message.BusinessUnitId);

            GoogleAnalyticsType gType = (GoogleAnalyticsType)Enum.Parse(typeof(GoogleAnalyticsType), message.ProfileType, false);
            if (Log.IsDebugEnabled)
                Log.DebugFormat("Processing GoogleAnalytics for BusinessUnitId: {0}, Type: {1}, DateRange: {2}",
                    message.BusinessUnitId, gType, message.DateRange.ToString() );

            
            // Make sure we are always querying complete months.
            message.DateRange = new DateRange(
                DateRange.MonthOfDate(message.DateRange.StartDate).StartDate,
                DateRange.MonthOfDate(message.DateRange.EndDate).EndDate
            );

            try
            {
                if (message.Force)
                {
                    ForceRegenerateAll(message, gType);
                }
                else
                {
                    GenerateMissingAndThisMonth(message, gType);
                }
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("Failed while creating GoogleAnalyticsCache record for BusinessUnitId: {0}, Type: {1}, DateRange: {2}",
                    message.BusinessUnitId, gType, message.DateRange.ToString()), ex);
                throw;
            }
        }

        private void GenerateMissingAndThisMonth(GoogleAnalyticsGenerateMessage message, GoogleAnalyticsType gType)
        {
            DateRange thisMonth = DateRange.MonthOfDate(DateTime.Today);
            if (message.DateRange.AsMonths().Contains(thisMonth))
            {
                if (Log.IsDebugEnabled)
                    Log.Debug("Generating this month");

                _resultCache.GenerateRangedGoogleData(message.BusinessUnitId, gType, thisMonth);
            }

            var fetchMonths = message.DateRange.AsMonths().Except(thisMonth.AsMonths());
            if (fetchMonths.Count() > 0)
            {
                if (Log.IsDebugEnabled)
                    Log.Debug("Fetching the remaining months");

                DateRange fetchRange = new DateRange(fetchMonths.First().StartDate, fetchMonths.Last().EndDate);

                // Fetch will generate any missing data on a month by month basis
                _resultCache.FetchRangedGoogleData(message.BusinessUnitId, gType, fetchRange);
            }
        }

        private void ForceRegenerateAll(GoogleAnalyticsGenerateMessage message, GoogleAnalyticsType gType)
        {
 	        var fetchMonths = message.DateRange.AsMonths();

            foreach (DateRange month in fetchMonths)
            {
                _resultCache.GenerateRangedGoogleData(message.BusinessUnitId, gType, month);
            }
        }
    }
}
