﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Text;
using Core.Messages;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using Merchandising.Messages;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    internal class DeadLetterArchive : TaskRunner<DeadLetterMessage>
    {
        private IQueueFactory _queueFactory;
        private IFileStorage _fileStore;
        private IFileStoreFactory _fileStoreFactory;
	    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public DeadLetterArchive(IQueueFactory queueFactory, IFileStoreFactory fileStoreFactory)
            : base(queueFactory.CreateDeadLetterArchive())
        {
            _queueFactory = queueFactory;
            _fileStoreFactory = fileStoreFactory;

            IBus<DeadLetterMessage, DeadLetterMessage> deadLetterBus = BusFactory.CreateDeadLetterBus<DeadLetterMessage, DeadLetterMessage>();
            deadLetterBus.Subscribe(_queueFactory.CreateDeadLetterArchive());
        }

        public override void Process(DeadLetterMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("Message Type: {0}", message.MessageType);
			Log.InfoFormat("Archiving dead letter for error count: {0}, message: {1}", message.Errors.Length, message.ErrorMessage);

            string today = DateTime.Now.Date.ToString("yyyy-MM-dd");
            string bucketName = string.Format("{0}/{1}/{1}-{2}", today, message.MessageType, DateTime.UtcNow.Ticks);

            NameValueCollection headers = new NameValueCollection();
            MessageBase errorMessage = null;

            try
            {
                errorMessage = message.ErrorMessage.FromJson<MessageBase>();
            }
            catch (Exception) { }

            if (errorMessage != null)
            {
                headers.Add("x-amz-meta-MessageType", message.MessageType);
                headers.Add("x-amz-meta-Sent", errorMessage.Sent.ToString("yyyy-MM-dd HH:mm:ss"));
                headers.Add("x-amz-meta-ErrorCount", errorMessage.Errors.Length.ToString());
            }

            _fileStore = _fileStoreFactory.DeadLetterArchive(headers);
            _fileStore.Write(bucketName, new MemoryStream(Encoding.ASCII.GetBytes(message.ToJson())));
        }
    }
}
