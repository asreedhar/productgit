﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;
using Core.Messaging;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Merchandising.Messages;
using Merchandising.Messages.WebLoader;

namespace FirstLook.Merchandising.DomainModel.Tasks
{
    internal class WebLoaderAbsolutePositionTask : TaskRunner<PhotoBatchUploadMessage>
    {
        private static readonly TimeSpan QueryApiSettleTime = TimeSpan.FromMinutes(10);

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IWebLoaderPhotoRepository _webLoaderPhotoRepository;
        private readonly IWebloaderMessager _webloaderMessager;

        public WebLoaderAbsolutePositionTask(IQueueFactory queueFactory, IWebLoaderPhotoRepository webLoaderPhotoRepository, IWebloaderMessager webloaderMessager)
            : base(queueFactory.CreateWebLoaderAbsolutePosition())
        {
            _webLoaderPhotoRepository = webLoaderPhotoRepository;
            _webloaderMessager = webloaderMessager;
        }

        // we need to get the real time photo count
        // photo services goes through our proxy cache https://max.firstlook.biz/aultec-cache
        private static int GetPhotoCount(string dealerCode, string vin)
        {
            int count = 0;
            string url = string.Format("https://incisent.aultec.net/photoquery/{0}/vinPhotos/{1}", dealerCode, vin);
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.Timeout = (int)TimeSpan.FromSeconds(60).TotalMilliseconds;

            string authInfo = "flrestapi:w@t3rl0g$";
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers["Authorization"] = "Basic " + authInfo;

            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                using (var responseStream = response.GetResponseStream())
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(responseStream);

                    var list = doc.SelectNodes("//url");
                    if (list != null)
                        count = list.Count;
                }
            }
            catch (WebException e)
            {
                var response = e.Response as HttpWebResponse;
                if (response != null && response.StatusCode == HttpStatusCode.NotFound) //aultec doesn't know about this vehicle.
                    count = 0;
                else
                    throw;
            }

            return count;
        }

        private bool IsBatchesCompleteBeforeBatch(int inventoryId, Guid batch)
        {
            //all batches before this one must be complete
            var batchStatus = _webLoaderPhotoRepository.FetchBatchStatus(inventoryId);
            foreach(var status in batchStatus)
            {
                if (status.Batch == batch)
                    return true;

                //Every batch before you has to be complete and past the Aul tec photo Query API
                //settle time. If batches run to close together the query API doesn't return the true photo count.

                if (!status.IsComplete) //not complete
                    return false;

                //Not past the settle time
                if (status.BatchComplete.HasValue && DateTime.Now < status.BatchComplete + QueryApiSettleTime)
                    return false;
            }

            return true;
        }

        private void SetBatchPhotoError(int inventoryId, PhotoBatchUploadMessage message)
        {
            foreach (var photo in message.Photos)
                _webLoaderPhotoRepository.SetError(inventoryId, photo.Guid);
        }

        public override void Process(PhotoBatchUploadMessage message)
        {
	        log4net.LogicalThreadContext.Properties["threadId"] = string.Format("BatchId: {0}", message.Batch);
            if(message.Photos.Length > 0)
            {
                int inventoryId = message.Photos.First().InventoryId;

                //Catch 1 error before the message goes on the dead letter
                if (message.Errors.Length >= ReadQueue.DeadLetterErrorLimit - 1) //the batch has errored. Need to error out the photos
                {
                    SetBatchPhotoError(inventoryId, message);
                    RequeueWithError(message, message.Errors.Last());
                    return;
                }

                //see if all batches before it have completed. If not recycle message for later.
                if (!IsBatchesCompleteBeforeBatch(inventoryId, message.Batch))
                {
                    RequeueNoError(message);
                    return;
                }

                Log.DebugFormat("Processing Batch {0}, for ordering", message.Batch);
                int photoCount = GetPhotoCount(message.Photos.First().Dealercode, message.Photos.First().Vin);
                

                Log.DebugFormat("Photo Count for {0} is {1}", message.Photos.First().Vin, photoCount);
                _webloaderMessager.SendPhotoUploadMessage(message.Photos, photoCount);
            }
        }
    }
}
