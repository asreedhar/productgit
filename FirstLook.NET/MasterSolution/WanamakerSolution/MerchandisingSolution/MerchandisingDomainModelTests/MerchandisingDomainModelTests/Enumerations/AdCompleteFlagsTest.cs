﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.Enumerations;

namespace MerchandisingDomainModelTests.Enumerations
{
    /// <summary>
    /// If the values are changed they will break stored procs that rely on them.
    /// </summary>
    [TestFixture]
    public class AdCompleteFlagsTest
    {
        [TestCase(AdCompleteFlags.MinimumPhotos)]
        public void MinimumPhotosIsOne(AdCompleteFlags testVal)
        {
            Assert.That((int)testVal, Is.EqualTo(1));
        }

        [TestCase(AdCompleteFlags.Description)]
        public void DescriptionIsTwo(AdCompleteFlags testVal)
        {
            Assert.That((int)testVal, Is.EqualTo(2));
        }

        [TestCase(AdCompleteFlags.Price)]
        public void PriceIsFour(AdCompleteFlags testVal)
        {
            Assert.That((int)testVal, Is.EqualTo(4));
        }

        [TestCase(AdCompleteFlags.MinimumPhotos | AdCompleteFlags.Description | AdCompleteFlags.Price)]
        public void AllFlagsAreSeven(AdCompleteFlags testVal)
        {
            Assert.That((int)testVal, Is.EqualTo(7));
        }
    }
}
