﻿using MAX.Entities.Enumerations;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Enumerations
{
    /// <summary>
    /// If the values are changed they will break stored procs that rely on them.
    /// </summary>
    [TestFixture]
    class GidProviderTests
    {
        [TestCase(GidProvider.Unknown, 0)]
        [TestCase(GidProvider.None, 1)]
        [TestCase(GidProvider.Aultec, 2)]
        [TestCase(GidProvider.eBiz, 3)]
        public void GidProviderValues(GidProvider provider, int value)
        {
            Assert.That((int)provider == value);
        }
    }
}
