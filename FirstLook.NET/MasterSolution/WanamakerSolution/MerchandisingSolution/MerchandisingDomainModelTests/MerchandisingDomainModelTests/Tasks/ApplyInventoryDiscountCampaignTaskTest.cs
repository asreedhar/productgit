﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Core.Messages;
using Core.Messaging;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using Merchandising.Messages.DiscountCampaigns;
using Merchandising.Messages.QueueFactories;
using Moq;
using NUnit.Framework;
using ICache = FirstLook.Common.Core.ICache;

namespace MerchandisingDomainModelTests.Tasks
{
    [TestFixture]
    public class ApplyInventoryDiscountCampaignTaskTest
    {

        [SetUp]
        public void SetupTests()
        {
            SetupIOC();
        }

        /// <summary>
        /// Test data contains inventory ids 1 and 2, so send a message with id 3 and make sure everything gets called correctly.
        /// </summary>
        [Test]
        public void Test_Task_AddsInventoryOnce()
        {
            var queueFactoryMock = new Mock<IDiscountCampaignQueueFactory>();
            var queueMock = new Mock<IQueue<ApplyInventoryDiscountCampaignMessage>>();
            var campaignManagerMock = new Mock<IDiscountPricingCampaignManager>();
            var searchMock = new Mock<IInventorySearch>();
            var message = new ApplyInventoryDiscountCampaignMessage(1, 3, "user");

            queueMock.Setup(x => x.GetMessage()).Returns(new AmazonMessage<ApplyInventoryDiscountCampaignMessage>(message));

            queueFactoryMock.Setup(x => x.CreateApplyInventoryDiscountCampaignQueue()).Returns(queueMock.Object);

            campaignManagerMock.Setup(x => x.FetchAllCampaigns(It.IsAny<int>(), It.IsAny<bool>())).Returns(TestDiscountCampaignModels());
	        campaignManagerMock.Setup(x => x.FetchActiveCampaignsNoInventory(It.IsAny<int>())).Returns(TestDiscountCampaignModels());

            searchMock.Setup(x => x.GetInventoryData(It.IsAny<int>())).Returns(FakeInventoryDatas());
            searchMock.Setup(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>())).Returns(FakeInventoryDatas().Where(data => data.InventoryID == 3));

            var task = new ApplyInventoryDiscountCampaignTask(queueFactoryMock.Object, campaignManagerMock.Object, searchMock.Object);
            
            task.Process(message);
            
            campaignManagerMock.Verify(x => x.FetchActiveCampaignsNoInventory(It.IsAny<int>()), Times.Once());
            campaignManagerMock.Verify(x => x.AddInventoryToCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<InventoryDiscountPricingModel>()), Times.Once());
        }

        /// <summary>
        /// Test data contains inventory ids 1 and 2 so send a message for id 1 and make sure we don't try to add it to the campaign
        /// </summary>
        [Test]
        public void Test_Task_DoesNotAddExistingInventory()
        {
            var queueFactoryMock = new Mock<IDiscountCampaignQueueFactory>();
            var queueMock = new Mock<IQueue<ApplyInventoryDiscountCampaignMessage>>();
            var campaignManagerMock = new Mock<IDiscountPricingCampaignManager>();
            var searchMock = new Mock<IInventorySearch>();
            var message = new ApplyInventoryDiscountCampaignMessage(1, 1, "user");

            queueMock.Setup(x => x.GetMessage()).Returns(new AmazonMessage<ApplyInventoryDiscountCampaignMessage>(message));

            queueFactoryMock.Setup(x => x.CreateApplyInventoryDiscountCampaignQueue()).Returns(queueMock.Object);

            campaignManagerMock.Setup(x => x.FetchActiveCampaignsNoInventory(It.IsAny<int>())).Returns(TestDiscountCampaignModels());

            searchMock.Setup(x => x.GetInventoryData(It.IsAny<int>())).Returns(FakeInventoryDatas());
            searchMock.Setup(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>())).Returns(FakeInventoryDatas().Where(data => data.InventoryID == 1));

            var task = new ApplyInventoryDiscountCampaignTask(queueFactoryMock.Object, campaignManagerMock.Object, searchMock.Object);

            task.Process(message);

            searchMock.Verify(x => x.ByCampaignFilter(It.IsAny<CampaignFilter>()), Times.Once());
			campaignManagerMock.Verify(x => x.FetchActiveCampaignsNoInventory(It.IsAny<int>()), Times.Once());
            campaignManagerMock.Verify(x => x.AddInventoryToCampaign(It.IsAny<DiscountPricingCampaignModel>(), It.IsAny<InventoryDiscountPricingModel>()), Times.Never() );
        }


        #region Test Classes

        private class AmazonMessage<TValue> : IMessage<TValue>
        {
            public AmazonMessage(TValue data)
            {
                Object = data;
            }

            public TValue Object { get; internal set; }
            public string ClaimCheck { get; internal set; }
            public string ReceiptHandle { get; internal set; }
        }


        #endregion Test Classes

        #region Test Data

        private static void SetupIOC()
        {
            var b = new ContainerBuilder();
            b.RegisterType<DotnetMemoryCacheWrapper>().As<FirstLook.Common.Core.ICache>();
            var container = b.Build();
            Registry.RegisterContainer(container);

            SetCache();
        }

        private static void SetCache()
        {
            var cache = Registry.Resolve<ICache>();
            var cacheKey = string.Format("BusinessUnitInventoryKey=1");
            cache.Set(cacheKey, FakeInventoryDatas());
        }

        private static List<DiscountPricingCampaignModel> TestDiscountCampaignModels()
        {
            return new List<DiscountPricingCampaignModel>
            {
                new DiscountPricingCampaignModel()
                {
                    BusinessUnitId = 1,
                    CampaignFilter = GetTestCampaignFilter(1),
                    CampaignId = 1,
                    CreatedBy = "testFixture",
                    CreationDate = DateTime.Today.AddDays(1),
                    DealerDiscount = 1000,
                    ManufacturerRebate = 2000,
                    DiscountType = PriceBaseLine.Invoice,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                    HasBeenExpired = false,
                    InventoryList = GetCampaignInventoryList(1),
                    InventoryExclusionList = new List<InventoryExclusionModel>()
                },
                new DiscountPricingCampaignModel()
                {
                    BusinessUnitId = 1,
                    CampaignFilter = GetTestCampaignFilter(2),
                    CampaignId = 2,
                    CreatedBy = "testFixture",
                    CreationDate = DateTime.Today.AddDays(1),
                    DealerDiscount = 1000,
                    ManufacturerRebate = 2000,
                    DiscountType = PriceBaseLine.Msrp,
                    ExpirationDate = DateTime.Today.AddMonths(1),
                    HasBeenExpired = false,
                    InventoryList = GetCampaignInventoryList(2),
                    InventoryExclusionList = new List<InventoryExclusionModel>()
                }
            };

        }

        private static CampaignFilter GetTestCampaignFilter(int incrementer)
        {
            return new CampaignFilter
            {
                Make = new List<string> { "Make-" + incrementer },
                Model = new List<string> { "Model-" + incrementer },
                Trim = new List<string> { "Trim-" + incrementer },
                Year = new List<string> { "2013, 2014" }
            };
        }

        private static List<InventoryDiscountPricingModel> GetCampaignInventoryList(int i)
        {
            return new List<InventoryDiscountPricingModel>
            {
                new InventoryDiscountPricingModel
                {
                    Active = true,
                    ActivatedBy = "testFixture",
                    CampaignId = i,
                    InventoryId = 1
                },
                new InventoryDiscountPricingModel
                {
                    Active = true,
                    ActivatedBy = "testFixture",
                    CampaignId = i,
                    InventoryId = 2,
                    OriginalNewCarPrice = 25000
                }
            };
        }




        private static List<IInventoryData> FakeInventoryDatas()
        {
            return new List<IInventoryData>
            {
                new InventoryData("stock-11", 1, "vin-1", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Make-1", "Model-1", "MarketClass-1", 1, "Trim-1", "afterMarketTrim-1", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false, null, null),
                new InventoryData("stock-21", 1, "vin-2", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-2", "afterMarketTrim-2", 2, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false, null, null),
                new InventoryData("stock-31", 1, "vin-3", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2011", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-3", "afterMarketTrim-3", 3, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false, null, null),
                new InventoryData("stock-12", 1, "vin-4", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Make-1", "Model-1", "MarketClass-1", 1, "Trim-1", "afterMarketTrim-1", 4, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false, null, null),
                new InventoryData("stock-22", 1, "vin-5", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-2", "afterMarketTrim-2", 5, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false, null, null),
                new InventoryData("stock-32", 1, "vin-6", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2014", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-3", "afterMarketTrim-3", 6, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false, null, null)
            };
        }

        #endregion Test Data
    }


}