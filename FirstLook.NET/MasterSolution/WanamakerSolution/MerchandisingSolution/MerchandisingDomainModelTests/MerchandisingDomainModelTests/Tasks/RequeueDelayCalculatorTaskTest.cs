﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Moq;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Core.Messaging;
using Core.Messages;

namespace MerchandisingDomainModelTests.Tasks
{
    [TestFixture]
    public class RequeueDelayCalculatorTaskTest
    {
        [Test]
        public void ExponentialRequeueTaskTest()
        {
            var MinTimeSpan = TimeSpan.FromMinutes(10);
            var MaxTimeSpan = TimeSpan.FromDays(7);

            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<NewInventoryMessage>>();

            var message = new NewInventoryMessage(101, 4);
            var messageList = new List<IMessage<NewInventoryMessage>>
            {
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage)
            };
            queueMock.Setup(x => x.GetMessages(It.IsAny<int>())).Returns(messageList);

            var calculators = new Dictionary<NewInventoryMessage, ExponentialDelayCalculator>();
            var counters = new Dictionary<NewInventoryMessage, int>();
            foreach (var m in messageList)
            {
                var calculator = new ExponentialDelayCalculator(MinTimeSpan, MaxTimeSpan);
                calculators.Add(m.Object, calculator);
                counters.Add(m.Object, 0);
            }
            queueMock.Setup(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()))
                .Callback((NewInventoryMessage x, TimeSpan y, string sentFrom) =>
                {
                    counters[x]++;
                    Assert.IsTrue(x.Delay.Equals(calculators[x].CalculateDelay(counters[x])));
                    Assert.IsTrue(x.BusinessUnitId == message.BusinessUnitId);
                    Assert.IsTrue(x.InventoryId == message.InventoryId);
                });

            queueMock.Setup(x => x.DeadLetterErrorLimit).Returns(14);
			queueFactoryMock.Setup(x => x.CreateAutoLoadNewInventory()).Returns(queueMock.Object);

            var task = new ExponentialRequeueTask(queueFactoryMock.Object);
            
            Assert.IsTrue(message.Delay == TimeSpan.Zero);

            // Run the task until we are hit theDeadLetterErrorLimit
            task.Run(() => messageList.FirstOrDefault(x => x.Object.Errors.Length < queueMock.Object.DeadLetterErrorLimit) == null, () => false);

            queueMock.Verify(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Exactly(queueMock.Object.DeadLetterErrorLimit * messageList.Count));

            foreach (var m in messageList)
            {
                Assert.IsTrue(m.Object.Delay <= MaxTimeSpan);
                Assert.IsTrue(m.Object.Delay >= MinTimeSpan);
                Assert.IsTrue(m.Object.Delay.Equals(calculators[m.Object].CalculateDelay(m.Object.Errors.Length)));
            }
        }

        [Test]
        public void FibonacciRequeueTaskTest()
        {
            var MinTimeSpan = TimeSpan.FromMinutes(10);
            var MaxTimeSpan = TimeSpan.FromDays(7);

            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<NewInventoryMessage>>();

            var message = new NewInventoryMessage(101, 4);
            var messageList = new List<IMessage<NewInventoryMessage>>
            {
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage)
            };
            queueMock.Setup(x => x.GetMessages(It.IsAny<int>())).Returns(messageList);

            var calculators = new Dictionary<NewInventoryMessage, FibonacciDelayCalculator>();
            var counters = new Dictionary<NewInventoryMessage, int>();
            foreach (var m in messageList)
            {
                var calculator = new FibonacciDelayCalculator(MinTimeSpan, MaxTimeSpan);
                calculators.Add(m.Object, calculator);
                counters.Add(m.Object, 0);
            }

            queueMock.Setup(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()))
                .Callback((NewInventoryMessage x, TimeSpan y, string sentFrom) =>
                {
                    counters[x]++;
                    Assert.IsTrue(x.Delay.Equals(calculators[x].CalculateDelay(counters[x])));
                    Assert.IsTrue(x.BusinessUnitId == message.BusinessUnitId);
                    Assert.IsTrue(x.InventoryId == message.InventoryId);
                });

            queueMock.Setup(x => x.DeadLetterErrorLimit).Returns(14);
			queueFactoryMock.Setup(x => x.CreateAutoLoadNewInventory()).Returns(queueMock.Object);

            var task = new  FibonacciRequeueTask(queueFactoryMock.Object);

            Assert.IsTrue(message.Delay == TimeSpan.Zero);

            // Run the task until we are hit theDeadLetterErrorLimit
            task.Run(() => messageList.FirstOrDefault(x => x.Object.Errors.Length < queueMock.Object.DeadLetterErrorLimit) == null, () => false);

            queueMock.Verify(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Exactly(queueMock.Object.DeadLetterErrorLimit * messageList.Count));

            foreach (var m in messageList)
            {
                Assert.IsTrue(m.Object.Delay <= MaxTimeSpan);
                Assert.IsTrue(m.Object.Delay >= MinTimeSpan);
                Assert.IsTrue(m.Object.Delay.Equals(calculators[m.Object].CalculateDelay(m.Object.Errors.Length)));
            }
        }

        [Test]
        public void PowerOfPiRequeueTaskTest()
        {
            var MinTimeSpan = TimeSpan.FromMinutes(10);
            var MaxTimeSpan = TimeSpan.FromDays(7);

            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<NewInventoryMessage>>();

            var message = new NewInventoryMessage(101, 4);
            var messageList = new List<IMessage<NewInventoryMessage>>
            {
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage)
            };
            queueMock.Setup(x => x.GetMessages(It.IsAny<int>())).Returns(messageList);

            var calculators = new Dictionary<NewInventoryMessage, PowerOfPiDelayCalculator>();
            var counters = new Dictionary<NewInventoryMessage, int>();
            foreach (var m in messageList)
            {
                var calculator = new PowerOfPiDelayCalculator(MinTimeSpan, MaxTimeSpan);
                calculators.Add(m.Object, calculator);
                counters.Add(m.Object, 0);
            }
            
            queueMock.Setup(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()))
                .Callback((NewInventoryMessage x, TimeSpan y, string sentFrom) =>
                {
                    counters[x]++;
                    Assert.IsTrue(x.Delay.Equals(calculators[x].CalculateDelay(counters[x])));
                    Assert.IsTrue(x.BusinessUnitId == message.BusinessUnitId);
                    Assert.IsTrue(x.InventoryId == message.InventoryId);
                });

            queueMock.Setup(x => x.DeadLetterErrorLimit).Returns(14);
			queueFactoryMock.Setup(x => x.CreateAutoLoadNewInventory()).Returns(queueMock.Object);

            var task = new PowerOfPiRequeueTask(queueFactoryMock.Object);

            Assert.IsTrue(message.Delay == TimeSpan.Zero);

            // Run the task until we are hit theDeadLetterErrorLimit
            task.Run(() => messageList.FirstOrDefault(x => x.Object.Errors.Length < queueMock.Object.DeadLetterErrorLimit) == null, () => false);

            queueMock.Verify(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Exactly(queueMock.Object.DeadLetterErrorLimit * messageList.Count));

            foreach (var m in messageList)
            {
                Assert.IsTrue(m.Object.Delay <= MaxTimeSpan);
                Assert.IsTrue(m.Object.Delay >= MinTimeSpan);
                Assert.IsTrue(m.Object.Delay.Equals(calculators[m.Object].CalculateDelay(m.Object.Errors.Length)));
            }
        }

        [Test]
        public void ConstantDelayRequeueTaskTest()
        {
            var MinTimeSpan = TimeSpan.FromMinutes(10);

            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<NewInventoryMessage>>();

            var message = new NewInventoryMessage(101, 4);
            var messageList = new List<IMessage<NewInventoryMessage>>
            {
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage),
                new AmazonMessage<NewInventoryMessage>(message.Clone() as NewInventoryMessage)
            };
            queueMock.Setup(x => x.GetMessages(It.IsAny<int>())).Returns(messageList);

            var calculators = new Dictionary<NewInventoryMessage, ConstantDelayCalculator>();
            var counters = new Dictionary<NewInventoryMessage, int>();
            foreach (var m in messageList)
            {
                var calculator = new ConstantDelayCalculator(MinTimeSpan);
                calculators.Add(m.Object, calculator);
                counters.Add(m.Object, 0);
            }

            queueMock.Setup(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()))
                .Callback((NewInventoryMessage x, TimeSpan y, string sentFrom) =>
                {
                    counters[x]++;
                    Assert.IsTrue(x.Delay.Equals(calculators[x].CalculateDelay(counters[x])));
                    Assert.IsTrue(x.Delay.Equals(MinTimeSpan));
                    Assert.IsTrue(x.BusinessUnitId == message.BusinessUnitId);
                    Assert.IsTrue(x.InventoryId == message.InventoryId);
                });

            queueMock.Setup(x => x.DeadLetterErrorLimit).Returns(14);
            queueFactoryMock.Setup(x => x.CreateAutoLoadNewInventory()).Returns(queueMock.Object);

            var task = new ConstantDelayRequeueTask(queueFactoryMock.Object);

            Assert.IsTrue(message.Delay == TimeSpan.Zero);

            // Run the task until we are hit the DeadLetterErrorLimit
            task.Run(() => messageList.FirstOrDefault(x => x.Object.Errors.Length < queueMock.Object.DeadLetterErrorLimit) == null, () => false);

            queueMock.Verify(x => x.SendMessage(It.IsAny<NewInventoryMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Exactly(queueMock.Object.DeadLetterErrorLimit * messageList.Count));

            foreach (var m in messageList)
            {
                Assert.IsTrue(m.Object.Delay == MinTimeSpan);
                Assert.IsTrue(m.Object.Delay.Equals(calculators[m.Object].CalculateDelay(m.Object.Errors.Length)));
            }
        }

        #region Test Classes
        private class AmazonMessage<TValue> : IMessage<TValue>
        {
            public AmazonMessage(TValue data)
            {
                Object = data;
            }

            public TValue Object { get; internal set; }
            public string ClaimCheck { get; internal set; }
            public string ReceiptHandle { get; internal set; }
        }


        private class RequeueTask : TaskRunner<NewInventoryMessage>
        {
            protected static readonly TimeSpan DefaultRequeueDelay = TimeSpan.FromMinutes(10);
            protected static readonly TimeSpan DefaultMaxRequeueDelay = TimeSpan.FromDays(7);

            public RequeueTask(IQueueFactory factory)
				: base(factory.CreateAutoLoadNewInventory())
            {
            }

            public override void Process(NewInventoryMessage message)
            {
                throw new Exception(Status.NoVehicleFound.ToString());
            }
        }

        private class ExponentialRequeueTask : RequeueTask
        {
            public ExponentialRequeueTask(IQueueFactory factory) : base(factory) { }
            protected override AbstractDelayCalculator CreateDelayCalculator()
            {
                return new ExponentialDelayCalculator(DefaultRequeueDelay, DefaultMaxRequeueDelay);
            }
        }

        private class FibonacciRequeueTask : RequeueTask
        {
            public FibonacciRequeueTask(IQueueFactory factory) : base(factory) { }
            protected override AbstractDelayCalculator CreateDelayCalculator()
            {
                return new FibonacciDelayCalculator(DefaultRequeueDelay, DefaultMaxRequeueDelay);
            }
        }

        private class PowerOfPiRequeueTask : RequeueTask
        {
            public PowerOfPiRequeueTask(IQueueFactory factory) : base(factory) { }
            protected override AbstractDelayCalculator CreateDelayCalculator()
            {
                return new PowerOfPiDelayCalculator(DefaultRequeueDelay, DefaultMaxRequeueDelay);
            }
        }

        private class ConstantDelayRequeueTask : RequeueTask
        {
            public ConstantDelayRequeueTask(IQueueFactory factory) : base(factory) { }
            protected override AbstractDelayCalculator CreateDelayCalculator()
            {
                return new ConstantDelayCalculator(DefaultRequeueDelay);
            }
        }
        
        #endregion Test Classes

    }
}
