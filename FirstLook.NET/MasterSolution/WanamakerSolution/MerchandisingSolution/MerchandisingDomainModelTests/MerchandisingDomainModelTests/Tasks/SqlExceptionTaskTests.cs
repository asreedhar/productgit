﻿using System;
using System.Data.SqlClient;
using System.Reflection;
using Core.Messages;
using Core.Messaging;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Tasks
{
    [TestFixture]
    public class SqlExceptionTaskTests
    {
        /*
         * The following tests enter a non-returning block of code and will hang indefinitely. [task.Run(() => kill, () => false);] is the culprit
         * 
        [Test]
        public void NonSqlExceptionShouldRequeueTaskTest()
        {
            bool kill = false;

            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<AutoApproveMessage>>();

            var message = new AutoApproveMessage(101, 4, "user");
            queueMock.Setup(x => x.GetMessage()).Returns(new AmazonMessage<AutoApproveMessage>(message));

            queueMock.Setup(x => x.SendMessage(It.IsAny<AutoApproveMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()))
                .Callback((AutoApproveMessage x, TimeSpan y, string sentFrom) =>
                    {
                        Assert.IsTrue(x.BusinessUnitId == message.BusinessUnitId);
                        Assert.IsTrue(x.InventoryId == message.InventoryId);
                    });

            queueFactoryMock.Setup(x => x.CreateAutoApprove()).Returns(queueMock.Object);
            var task = new CommonExceptionTask(queueFactoryMock.Object, (value) => kill = value);
            task.Run(() => kill, () => false);

            queueMock.Verify(x => x.SendMessage(It.IsAny<AutoApproveMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Exactly(1));
            queueMock.Verify(x => x.DeleteMessage(It.IsAny<IMessage<AutoApproveMessage>>()), Times.Exactly(1));
        }*/

        /*
         * The following tests enter a non-returning block of code and will hang indefinitely. [task.Run(() => kill, () => false);] is the culprit
         * 
        [TestCase(-1)]
        [TestCase(2)]
        [TestCase(53)]
        [TestCase(18470)] //invalid login
        public void SqlExceptionWithConnectionErrorShouldNotRequeueTaskTest(int errorNumber)
        {
            bool kill = false;

            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<AutoApproveMessage>>();
            queueFactoryMock.Setup(x => x.CreateAutoApprove()).Returns(queueMock.Object);

            var message = new AutoApproveMessage(101, 4, "user");
            queueMock.Setup(x => x.GetMessage()).Returns(new AmazonMessage<AutoApproveMessage>(message));

            var task = new SqlExceptionTask(queueFactoryMock.Object, errorNumber, (value) => kill = value);
            task.Run(() => kill, () => false);

            queueMock.Verify(m => m.SendMessage(It.IsAny<AutoApproveMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Never());
            queueMock.Verify(x => x.DeleteMessage(It.IsAny<IMessage<AutoApproveMessage>>()), Times.Never());
        }*/

        /*
         * The following tests enter a non-returning block of code and will hang indefinitely. [task.Run(() => kill, () => false);] is the culprit
         * 
        [TestCase(101)]
        [TestCase(109)]
        [TestCase(21)]
        public void SqlExceptionNonConnectionErrorShouldRequeueTaskTest(int errorNumber)
        {
            bool kill = false;

            var queueFactoryMock = new Mock<IQueueFactory>();
            var queueMock = new Mock<IQueue<AutoApproveMessage>>();
            queueFactoryMock.Setup(x => x.CreateAutoApprove()).Returns(queueMock.Object);

            var message = new AutoApproveMessage(101, 4, "user");
            queueMock.Setup(x => x.GetMessage()).Returns(new AmazonMessage<AutoApproveMessage>(message));

            var task = new SqlExceptionTask(queueFactoryMock.Object, errorNumber, (value) => kill = value);
            task.Run(() => kill,() => false);

            queueMock.Verify(x => x.SendMessage(It.IsAny<AutoApproveMessage>(), It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Exactly(1));
            queueMock.Verify(x => x.DeleteMessage(It.IsAny<IMessage<AutoApproveMessage>>()), Times.Exactly(1));
        }*/

        private class AmazonMessage<TValue> : IMessage<TValue>
        {
            public AmazonMessage(TValue data)
            {
                Object = data;
            }

            public TValue Object { get; internal set; }
            public string ClaimCheck { get; internal set; }
            public string ReceiptHandle { get; internal set; }
        }

        private class CommonExceptionTask : TaskRunner<AutoApproveMessage>
        {
            public Action<bool> Setter { get; private set; } 

            public CommonExceptionTask(IQueueFactory factory, Action<bool> setter)
            : base(factory.CreateAutoApprove())
            {
                Setter = setter;
            }

            public override void Process(AutoApproveMessage message)
            {
                Setter(true);
                throw new InvalidOperationException();
            }
        }

        private class SqlExceptionTask : CommonExceptionTask
        {
            private int _errorNumber;

            public SqlExceptionTask(IQueueFactory factory, int errorNumber, Action<bool> setter)
                : base(factory, setter)
            {
                _errorNumber = errorNumber;
            }

            public override void Process(AutoApproveMessage message)
            {

                Setter(true);

                var errorCollection = typeof (SqlErrorCollection)
                    .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance,
                        null,
                        new Type[0],
                        null)
                    .Invoke(new object[0]) as SqlErrorCollection;

                var sqlError = typeof(SqlError)
                    .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance,
                        null,
                        new[] { typeof(int), typeof(byte), typeof(byte), typeof(string), typeof(string), typeof(string), typeof(int) },
                        null)
                    .Invoke(new object[] { _errorNumber, (byte)0, (byte)0, "", "", "error", 0 }) as SqlError;

                typeof (SqlErrorCollection)
                    .GetMethod("Add", BindingFlags.NonPublic | BindingFlags.Instance, null, new [] { typeof(SqlError) }, null)
                    .Invoke(errorCollection, new object[] {sqlError});


                SqlException sqlException = null;
                    
                var sqlExceptionConstructor = typeof (SqlException)
                    .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance,
                        null,
                        new[] {typeof (string), typeof (SqlErrorCollection)},
                        null);

                if (sqlExceptionConstructor != null)
                {
                    sqlException = sqlExceptionConstructor.Invoke(new object[] {"error", errorCollection}) as SqlException;
                }
                else
                {
                    // TH: 2013-11-19
                    // newer versions of the SqlException have a different private constructor.
                    // This is a sealed private class, so we have no real way to `new` one up.
                    // Therefore we have to work around it in order to fake an exception with a specific error number
                    // which is exactly what this entire series of tests is designed to verify.
                    // This is brittle as MS may change their private constructors any ole time they like.
                    sqlExceptionConstructor = typeof (SqlException)
                        .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance,
                            null,
                            new[] {typeof (string), typeof (SqlErrorCollection), typeof (Exception), typeof (Guid)},
                            null);

                    if(sqlExceptionConstructor != null)
                        sqlException = sqlExceptionConstructor.Invoke(new object[] { "error", errorCollection, null, null }) as SqlException;
                }
                        
                throw sqlException ?? new Exception("Failed to throw a sql exception");
                
            }
        }

    }
}
