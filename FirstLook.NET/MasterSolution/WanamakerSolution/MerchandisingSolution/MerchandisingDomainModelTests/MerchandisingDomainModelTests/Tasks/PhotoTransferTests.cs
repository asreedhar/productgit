﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Tasks;
using Merchandising.Messages;
using Merchandising.Messages.VehicleTransfer;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Tasks
{
    [TestFixture]
    public class PhotoTransferTests
    {
        
        [Test]
        public void TestTransformationCalls()
        {
            var queueFactoryMock = new Mock<IQueueFactory>();
            var photoMock = new Mock<IPhotoServices>();
            var dealerMock = new Mock<IDealerServices>();
            var photoAccessMock = new Mock<IPhotoTransferDataAccess>();

            // setup returns from URL for the following situations
            photoMock.Setup(x => x.GetPhotoUrlsByVin("0", "A", It.IsAny<TimeSpan>())).Returns((string businessUnitCode, string vin, TimeSpan timeout) => new PhotoResult(vin, new [] { "junk.com", "test.com" }));
            photoMock.Setup(x => x.GetPhotoUrlsByVin("1", "A", It.IsAny<TimeSpan>())).Returns((string businessUnitCode, string vin, TimeSpan timeout) => new PhotoResult(vin, new string[]{}));

            photoAccessMock.Setup(x => x.TransferPhoto(1, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>())).
                Callback((int dstInventoryId, string photoUrl, int photoSequenceNumber, bool photoIsPrimary) =>
                             {
                                 if(photoSequenceNumber == 0)
                                     Assert.IsTrue(photoUrl == "junk.com");
                                 if(photoSequenceNumber == 1)
                                     Assert.IsTrue(photoUrl == "test.com");
                             });
            PhotoTransferTask task = new PhotoTransferTask(queueFactoryMock.Object, photoMock.Object, dealerMock.Object, photoAccessMock.Object);
            task.Process(new PhotoTransferMessage("A", 0, "0", 0, 1, "1", 1));

            photoMock.Verify(x => x.GetPhotoUrlsByVin("0", "A", It.IsAny<TimeSpan>()), Times.Exactly(1));
            photoMock.Verify(x => x.GetPhotoUrlsByVin("1", "A", It.IsAny<TimeSpan>()), Times.Exactly(1));

            photoAccessMock.Verify(x => x.TransferPhoto(1, It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()), Times.Exactly(2));

            // should never contain the following url!!
            //photoAccessMock.Verify(pam => pam.TransferPhoto(It.IsAny<int>(), It.Is<string>(sURL => sURL.ToLower().StartsWith(@"https://incisent.aultec.net/v/")), It.IsAny<int>(), It.IsAny<bool>()), Times.Never(), "URL was not properly replaced");

        }
        [Test]
        public void TestUlrSubstituion()
        {

            // simple test to make sure specific URL is not inserted on transfer

            var queueFactoryMock = new Mock<IQueueFactory>();
            var photoMock = new Mock<IPhotoServices>();
            var dealerMock = new Mock<IDealerServices>();
            var photoAccessMock = new Mock<IPhotoTransferDataAccess>();

            // setup returns from URL for the following situations
            photoMock.Setup(x => x.GetPhotoUrlsByVin("0", "A", It.IsAny<TimeSpan>())).Returns((string businessUnitCode, string vin, TimeSpan timeout) => new PhotoResult(vin, new[] { @"https://incisent.aultec.net/v//junk.png", "test.com" }));
            photoMock.Setup(x => x.GetPhotoUrlsByVin("1", "A", It.IsAny<TimeSpan>())).Returns((string businessUnitCode, string vin, TimeSpan timeout) => new PhotoResult(vin, new string[] { }));

            PhotoTransferTask task = new PhotoTransferTask(queueFactoryMock.Object, photoMock.Object, dealerMock.Object, photoAccessMock.Object);
            task.Process(new PhotoTransferMessage("A", 0, "0", 0, 1, "1", 1));

            // make sure photo transfer is twice
            photoAccessMock.Verify(x => x.TransferPhoto(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()), Times.Exactly(2));

            // should never contain the following url!!
            photoAccessMock.Verify(pam => pam.TransferPhoto(It.IsAny<int>(), It.Is<string>(sURL => sURL.ToLower().StartsWith(@"https://incisent.aultec.net/v/")), It.IsAny<int>(), It.IsAny<bool>()), Times.Never(), "URL was not properly replaced!!");

            // should never contain the following url!!
            photoAccessMock.Verify(pam => pam.TransferPhoto(It.IsAny<int>(), It.Is<string>(sURL => sURL.ToLower().StartsWith(@"http://i.aultec.com/v/71/")), It.IsAny<int>(), It.IsAny<bool>()), Times.AtLeastOnce(), "URL was never replaced!!");

        }

        private class PhotoResult : IVehiclePhotosResult
        {
            public PhotoResult(string vin, string[] photos)
            {
                Vin = vin;
                PhotoUrls = photos;
            }

            public ICollection<IPhotoServicesError> Errors
            {
                get { return new List<IPhotoServicesError>(); }
            }

            public string Vin { get; private set; }

            public string[] PhotoUrls { get; private set; }
        }
    }
}
