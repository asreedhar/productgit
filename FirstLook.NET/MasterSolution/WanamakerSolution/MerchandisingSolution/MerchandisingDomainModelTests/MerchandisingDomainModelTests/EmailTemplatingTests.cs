﻿using System;
using FirstLook.Merchandising.DomainModel.Release;
using MAX.Entities;
using NUnit.Framework;

namespace MerchandisingDomainModelTests
{
    [TestFixture]
    public class EmailTemplatingTests
    {
        [Test]
        public void TestBucketAlertTemplate()
        {
            BucketAlertEmailViewModel model = new BucketAlertEmailViewModel("Windy City");
            model.Add(new BucketAlertEmailItem("No Price", 3, WorkflowType.NoPrice));
            model.Add(new BucketAlertEmailItem("No Trim", 0, WorkflowType.TrimNeeded));

            model.Add(new BucketAlertEmailItem("Low Activity", 5, WorkflowType.LowActivity));
            model.Add(new BucketAlertEmailItem("No Packages", 10, WorkflowType.NoPackages));

            model.LowQualityLink = "http://google.com";
            model.OnlineGapLink = "http://google.com";
            model.SettingsLink = "http://google.com";


            var emailTemplate = new BucketAlertEmailTemplate(model);
            var emailBody = emailTemplate.Run();
        }

        [Test]
        public void TestTemplate()
        {
            AdvertisementEmailViewModel[] ads = new AdvertisementEmailViewModel[2];
            
            var adModel = new AdvertisementEmailViewModel()
            {
                InternetPrice = "$43,000",
                MakeModel = "Chevy Tahoe",
                Mileage = "3,567",
                Vin = "837VBDREW",
                StockNumber = "T7C0K",
                EditLink = "http://breeve01:2476/MaxAd/Workflow/RedirectToInventoryItem.aspx?bu=102402&inv=21310548"
            };

            adModel.Advertisement = "Preview</br>This car has a preview.</br></br>This car rocks.</br>I am the body. You should buy it.";
            adModel.Disclaimer = "Horsepower calculations based on trim engine configuration. Fuel economy calculations based on original manufacturer data for trim engine configuration.";

            ads[0] = adModel;

            adModel = new AdvertisementEmailViewModel()
            {
                InternetPrice = "$24,000",
                MakeModel = "Doge Neon",
                Mileage = "1,567",
                Vin = "8759304JKOJNG",
                StockNumber = "SLKOK",
                EditLink = "http://breeve01:2476/MaxAd/Workflow/RedirectToInventoryItem.aspx?bu=102402&inv=21310548"
            };

            adModel.Advertisement = "Preview</br>This car has a preview.</br></br>This is better car</br>I am the body. You should buy it.";
            adModel.Disclaimer = "Horsepower calculations based on trim engine configuration. Fuel economy calculations based on original manufacturer data for trim engine configuration.";

            ads[1] = adModel;

            var emailModel = new EmailViewModel() { Advertisements = ads };
            emailModel.Date = DateTime.MinValue.ToString();

            var razorTemplate = new EmailStringTemplate(emailModel);
            var emailBody = razorTemplate.Run();

        }
    }

}
