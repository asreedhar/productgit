﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Antlr.StringTemplate;
using Antlr.StringTemplate.Language;
using NUnit.Framework;

using FirstLook.Common.Core.Extensions;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    public class StringTemplateTests
    {

        public static string stringTemplateBaseObjectName = "Foo";
        private static string listTemplate = "group simple; main(base) ::= << <base; separator=\", \"> >>";

        private static string GetGroupTemplateText(string baseTemplateText)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append(listTemplate);
            sb.Append("main(").Append(stringTemplateBaseObjectName).Append(") ::= \"").Append(baseTemplateText).Append("\"");
            sb.Append("\n");

            var groupText = sb.ToString();
            return groupText;
        }


        /// <summary>
        /// This test test is very similar to the previous templating code, and it doesn't work.
        /// </summary>
        [Test][Ignore]
        public void ToList()
        {
            Foo f = new Foo { Bar = new List<string> { "a", "b" } };
            var groupTemplateText = GetGroupTemplateText("Foo.Bar");

            Console.WriteLine(groupTemplateText);

            StringTemplateGroup stg = 
                new StringTemplateGroup(new StringReader(groupTemplateText),
                                        typeof(GroupLexer));

            //StringTemplate currTemplate = new StringTemplate(validTemplateText);
            StringTemplate currTemplate = stg.GetInstanceOf("main");
            currTemplate.SetAttribute(stringTemplateBaseObjectName, f);

            var s = currTemplate.ToString();

            Console.WriteLine(s);
        }

        [Test][Ignore]
        public void MixedWithFormatRenderer()
        {
            Foo f = new Foo { Bar = new List<string> { "a", "b" }, Baz="c" };

            StringTemplate st = new StringTemplate("The list is: <Bar>. The scalar is: <Baz>.", 
                typeof(AngleBracketTemplateLexer));

            st.SetAttribute("Bar", f.Bar);
            Assert.AreEqual(typeof(List<String>), st.GetAttribute("Bar").GetType());

            st.SetAttribute("Baz", f.Baz);
            Assert.AreEqual(typeof(string), st.GetAttribute("Baz").GetType());

            var rend = new FormatRenderer();

            // System.Collections.Generic.IEnumerable`1[System.String]
            // System.Collections.Generic.List`1[System.String]
            // "System.Collections.Generic.List`1[[System.String, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]"
            string typeStr = "System.Collections.Generic.List`1[[System.String, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]";
            Type t = Type.GetType(typeStr); // typeof(List<string>);

            st.RegisterAttributeRenderer(t, rend); 
            Assert.AreEqual( rend, st.GetAttributeRenderer(t));

            var ret = st.ToString();

            Console.WriteLine(st.ToDebugString());
            Console.WriteLine(ret);

            Assert.IsTrue(ret.Equals("The list is: a, b. The scalar is: c."));
        }

        [Test][Ignore]
        public void ArrayRenderer()
        {
            Foo f = new Foo { Bar = new List<string> { "a", "b" }, Baz="c" };

            StringTemplate st = new StringTemplate("The list is: <Bar>. The scalar is: <Baz>.", 
                typeof(AngleBracketTemplateLexer));

            st.SetAttribute("Bar", f.Bar.ToArray() );
            Assert.AreEqual(typeof(String[]), st.GetAttribute("Bar").GetType());

            st.SetAttribute("Baz", f.Baz);
            Assert.AreEqual(typeof(string), st.GetAttribute("Baz").GetType());

            var rend = new ArrayRenderer();

            Type t = typeof(Array);

            st.RegisterAttributeRenderer(t, rend); 
            Assert.AreEqual( rend, st.GetAttributeRenderer(t));

            var ret = st.ToString();

            Console.WriteLine(st.ToDebugString());
            Console.WriteLine(ret);

            Assert.IsTrue(ret.Equals("The list is: a, b. The scalar is: c."));
        }



        [Test]
        public void MixedNoRenderer()
        {

            // Use the constructor that accepts a System.IO.TextReader
            StringTemplateGroup group = new StringTemplateGroup(new StringReader(listTemplate));
            StringTemplate t = group.GetInstanceOf("main");
            
            t.SetAttribute("base", new List<string>{"a", "b"});
            Console.Out.WriteLine(t.ToString());

            t.RemoveAttribute("base");
            t.SetAttribute("base", "abcdefg");
            Console.Out.WriteLine(t.ToString());

        }


        [Test]
        public void TemplatedEmployedWithIntroAndClosing()
        {
            string intro = "LOADED WITH:";
            string closing = "THE END";
            string propName = "HighlightedPackages";

            string temp = String.Format("group simple; main({0}) ::= <<{1} <{0}; separator=\", \"> {2} >>", propName, intro,
                closing);

            MockVehicleData d = new MockVehicleData();
            d.HighlightedPackages = new List<string> { "a", "b" };

            // Use the constructor that accepts a System.IO.TextReader
            StringTemplateGroup group = new StringTemplateGroup(new StringReader(temp));
            StringTemplate t = group.GetInstanceOf("main");

            var propertyInfo = d.GetType().GetProperty(propName);
            var value = propertyInfo.GetValue(d, null);

            t.SetAttribute(propName, value);

            var str = t.ToString();

            Console.WriteLine(t.ToDebugString());
            Console.Out.WriteLine(str);

            Assert.IsTrue(str.Equals("LOADED WITH: a, b THE END "));
        }

        /*
                var template = "group simple;";
                template += " vardef(type,name) ::= \"<type> <name>;\"";
                template += " method(type,name,args) ::= <<";
                template += " <type> <name>(<args; separator=\",\">) {";
                template += "   <statements; separator=\"\n\">}>>";

                Console.WriteLine(template);
        */

        /// <summary>
        /// This is my only working renderer test.  I don't know why the other renderers don't work, but
        /// if they did, that would greatly simplify the code.
        /// </summary>
        [Test]
        public void Date()
        {
            StringTemplate st = new StringTemplate("date: <created>", typeof(AngleBracketTemplateLexer));
            st.SetAttribute("created", new DateTime(2005, 07, 05));
            st.RegisterAttributeRenderer(typeof(DateTime), new DateRenderer());
            string expecting = "date: 2005.07.05";
            string result = st.ToString();

            Console.WriteLine(result);

            Assert.IsTrue(result.Equals(expecting));
        }


        
    }

    public class MockVehicleData
    {
        public List<string> HighlightedPackages { get; set; }
        public string Make { get; set; }
    }

    public class DateRenderer : IAttributeRenderer
    {
        public string ToString(object o)
        {
            Console.WriteLine("DateRenderer.ToString() called.");
            DateTime dt = (DateTime)o;
            return dt.ToString("yyyy.MM.dd");
        }

        public string ToString(object o, string formatName)
        {
            return ToString(o);
        }
    }

    public class ArrayRenderer : IAttributeRenderer
    {
        #region Implementation of IAttributeRenderer

        public string ToString(object o)
        {
            Console.WriteLine("ArrayRenderer() called.");

            return "hello";

            // try to cast to a Array
            Array a = o as Array;
            if (a != null)
            {
            }

            return o.ToString();
        }

        public string ToString(object o, string formatName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    // System.Collections.Generic.List`1[System.String]
    /// <summary>
    /// If I could get this renderer to work, we could simply register it with the StringTemplate or
    /// StringTemplateGroup, and the renderer would be responsible for creating CSV lists whenever
    /// the bound item is of type List<string>.  Sadly, the renderer is never called by st.ToString(),
    /// and I can't figure out why.
    /// </summary>
    public class FormatRenderer : IAttributeRenderer
    {
        #region Implementation of IAttributeRenderer

        public string ToString(object o)
        {
            Console.WriteLine("FormatRenderer() called.");
            
            // try to cast to a list<string>
            List<string> list = o as List<string>;
            if (list != null)
            {
                var csv = list.ToDelimitedString(",");
                Console.WriteLine(csv);
                return csv;
            }

            return o.ToString();
        }

        public string ToString(object o, string formatName)
        {
            return ToString(o);
        }

        #endregion
    }

    public class Foo
    {
        public List<string> Bar { get; set; }
        public string Baz { get; set; }
    }
}
