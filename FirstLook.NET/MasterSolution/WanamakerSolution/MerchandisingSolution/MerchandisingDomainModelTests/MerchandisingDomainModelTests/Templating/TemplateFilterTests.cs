﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.Entities;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    public class TemplateFilterTests
    {
        private const int BusinessUnit = 101111;

        private VehicleProfile allCarsProfile;
        private VehicleProfile hondaAccordProfile;
        private TemplateData familyEnhancedTemplate;
        private TemplateData familySafetyTemplate;
        private TemplateData hondaAccordTemplate;
        private TemplateData familyEnhancedPlus;
        
        private Mock<ITemplateDataRepository> mockRepository;

        [SetUp]
        public void SetUp()
        {

            allCarsProfile = new VehicleProfile
            {
                Id = 1,
                Makes = new List<string>(),
                Models = new List<string>(),
                Trims = new List<string>(),
                Segments = new List<Segments>(),
            };

            hondaAccordProfile = new VehicleProfile
            {
                Id = 278,
                Title = "Honda Accord",
                Makes = new List<string> { "Honda" },
                Models = new List<string> { "Accord", "Accord Cpe", "Accord Sdn", "Accord Sedan" },
                Trims = new List<string>(),
                Segments = new List<Segments>(),
                StartYear = 2007,
                EndYear = 2012,
                VehicleType = VehicleType.Used
            };

            familyEnhancedTemplate = new TemplateData
            {
                Id = 1,
                Name = "Family Enhanced",
                VehicleProfile = allCarsProfile
            };

            familySafetyTemplate = new TemplateData
            {
                Id = 2,
                Name = "Family Safety",
                VehicleProfile = allCarsProfile
            };

            hondaAccordTemplate = new TemplateData
            {
                Id = 3,
                Name = "Honda Accord",
                VehicleProfile = hondaAccordProfile
            };

            familyEnhancedPlus = new TemplateData
            {
                Id = 4,
                Name = "Family Enhanced +",
                VehicleProfile = null
            };

            mockRepository = new Mock<ITemplateDataRepository>();
            mockRepository.Setup(r => r.GetTemplateDataForBusinessUnit(BusinessUnit))
                          .Returns(new List<TemplateData> { familyEnhancedTemplate, familySafetyTemplate, hondaAccordTemplate, familyEnhancedPlus });

        }

        private TemplateFilter CreateObjectUnderTest()
        {
            return new TemplateFilter(mockRepository.Object);
        }

        [Test]
        public void Should_filter_out_templates_that_do_not_match()
        {
            var newHondaAccord = new Mock<IInventoryData>();
            newHondaAccord.Setup(i => i.Make).Returns("Honda");
            newHondaAccord.Setup(i => i.Model).Returns("Accord");
            newHondaAccord.Setup(i => i.Year).Returns("2009");
            newHondaAccord.Setup(i => i.InventoryType).Returns(VehicleType.New);

            var filter = CreateObjectUnderTest();

            var templates = filter.SelectTemplates(BusinessUnit, newHondaAccord.Object);

            Assert.That(templates.Count, Is.EqualTo(3));
            Assert.That(templates[familyEnhancedTemplate.Id], Is.EqualTo(familyEnhancedTemplate.Name));
            Assert.That(templates[familySafetyTemplate.Id], Is.EqualTo(familySafetyTemplate.Name)); 
            Assert.That(templates[familyEnhancedPlus.Id], Is.EqualTo(familyEnhancedPlus.Name));
        }
    }
}