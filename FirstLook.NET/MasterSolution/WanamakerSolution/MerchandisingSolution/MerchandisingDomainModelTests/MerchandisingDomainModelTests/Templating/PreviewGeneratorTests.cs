﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Templating.Previews;
using FirstLook.Merchandising.DomainModel.Templating.Previews.DAL;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Moq;
using NUnit.Framework;

using Autofac;
using PreviewItem = FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.PreviewItem;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    public class PreviewGeneratorTests
    {
        private IContainer _container;
        private Mock<IPreviewPreferencesDao> _mockPreviewPreferencesDao;
        private IPreviewPreferencesDao _preferencesDao;

        [SetUp]
        public void Setup()
        {
            // Setup a fake IPreviewPreference
            var mockPreference = new Mock<IPreviewPreferences>();

            // Setup a fake DAO
            _mockPreviewPreferencesDao = new Mock<IPreviewPreferencesDao>();
            _mockPreviewPreferencesDao.Setup(m => m.Fetch(It.IsAny<int>(), It.IsAny<int>())).Returns(
                mockPreference.Object);

            _preferencesDao = _mockPreviewPreferencesDao.Object;

            // Register our mocks with the Registry and IoC container.
            var builder = new ContainerBuilder();

            //builder.RegisterType<AdvertisementConverter>().As<IAdvertisementConverter>();
            builder.RegisterInstance(_preferencesDao).As<IPreviewPreferencesDao>();

            _container = builder.Build();
            Registry.RegisterContainer(_container);
        }

        [Test]
        public void CanConstruct()
        {
            const int LENGTH = 1;
            ITemplateMediator mediator = new Mock<ITemplateMediator>().Object;
            PreviewGenerator generator = new PreviewGenerator( LENGTH, mediator );

            Assert.IsNotNull(generator);
        }

        [Test]
        //[Ignore("This test is not mocking IPreviewPreferences. Fails null reference")]
        public void GetPreviewNewVehicle()
        {

            // mock the mediator class need for the preview generator
            var mockMediator = new Mock<ITemplateMediator>();

            // mock the dealers adversiting preferences
            var mockDealerAdPreferences = new Mock<IDealerAdvertisementPreferences>();
            mockDealerAdPreferences.SetupGet(x => x.DesiredPreviewLength).Returns(250);

            // setup dealer new preview preferrences
            var mockNewPreviewPreferences = new Mock<IPreviewPreferences>();
            mockNewPreviewPreferences.SetupAllProperties();
            mockNewPreviewPreferences.Object.NewOrUsed = 1; // new

            // mock the data souce (can't really inject this dependancy into preview generator)
            var mockPreviewPreferencesDAO = new Mock<IPreviewPreferencesDao>();
            mockPreviewPreferencesDAO.SetupAllProperties();
            mockPreviewPreferencesDAO.Setup(x => x.Fetch(It.IsAny<Int32>(), It.IsAny<Int32>())).Returns(mockNewPreviewPreferences.Object);

            var cbContainer = new ContainerBuilder();
            cbContainer.RegisterInstance(mockPreviewPreferencesDAO.Object);
            var container = cbContainer.Build();
            Registry.RegisterContainer(container);
            
            // setup vehicle template data
            var mockVehicleTemplateData = new Mock<IVehicleTemplatingData>();
            mockVehicleTemplateData.SetupAllProperties();
            mockVehicleTemplateData.Setup(d => d.InventoryItem.IsNew()).Returns(true); // new cars only
            mockVehicleTemplateData.SetupGet(x => x.NewPreviewPreferences).Returns(mockNewPreviewPreferences.Object);

            // setup new car preferences for this vehicle
            mockNewPreviewPreferences.Object.UseModelAwards = true;  // model awards
            mockVehicleTemplateData.SetupGet(x => x.HasAwards).Returns(true);
            mockVehicleTemplateData.SetupGet(x => x.BestModelAward).Returns("2013 Tureen cool car award");

            // set fuel economy
            mockNewPreviewPreferences.Object.UseFuelEconomy = true;  // fuel ecomony
            mockVehicleTemplateData.SetupGet(x => x.GetsGoodGasMileage).Returns(true);
            mockVehicleTemplateData.SetupGet(x => x.FuelHwy).Returns("awesome!");
            mockVehicleTemplateData.SetupGet(x => x.FuelCity).Returns("more awesome!");

            mockMediator.SetupGet(x => x.VehicleData).Returns(mockVehicleTemplateData.Object);

            // run the previous and test the results
            var generator = new PreviewGenerator(mockDealerAdPreferences.Object.DesiredPreviewLength, mockMediator.Object);
            
            var preview = generator.GetPreview(mockDealerAdPreferences.Object, ThemeType.None);

//            Console.WriteLine(preview);
            Assert.IsNotEmpty(preview);


        }

        [Test( Description = "Fix for BUGZID : 18531" )]
        public void EditedBlurbWithLength250ShouldNotBeDeleted ()
        {
            const string longBlurb250 =
            @"Leather Seats, Sunroof, NAV, Premium Sound System, Park Distance Control, Overhead Airbag, Alloy Wheels, Turbo, Heated Mirrors, Heated Leather Seats, All Wheel Drive. Leather Seats, Sunroof, NAV, Premium Sound System, Park Distance Con test test test";

            var blurb1 = new PreviewItem { Edited = true, Text = " " };
            blurb1.Children.Add( new Rule { ID = new Guid( "{1e13c8de-7795-46b7-a114-f606218430ab}" ) } );
            var blurb2 = new PreviewItem { Edited = true, Text = longBlurb250 };
            blurb2.Children.Add( new Rule { ID = new Guid( "{4c4d10ae-c02a-44dc-91fe-ac4fd765266d}" ) } );
            var blurb3 = new PreviewItem { Edited = true, Text = " " };
            blurb3.Children.Add( new Rule { ID = new Guid( "{788a31d8-9424-4585-a325-4b773991b891}" ) } );
            var blurb4 = new PreviewItem { Edited = true, Text = " " };
            blurb4.Children.Add( new Rule { ID = new Guid( "{c165e302-57e4-4b65-8517-4e83c77f733d}" ) } );

            var mockTemplateData = new Mock<IVehicleTemplatingData>();
            mockTemplateData.SetupAllProperties();
            mockTemplateData.Setup(d => d.InventoryItem.IsNew()).Returns(true);
            var templateData = mockTemplateData.Object;

            var mockMediator = new Mock<ITemplateMediator>();
            mockMediator.Setup( x => x.GetPreviewItems() ).Returns( new PreviewItem[] { blurb1, blurb2, blurb3, blurb4 } );
            mockMediator.SetupGet(x => x.VehicleData).Returns(templateData);
            var mediator = mockMediator.Object;

            var mockPreferences = new Mock<IDealerAdvertisementPreferences>();
            mockPreferences.SetupGet( x => x.DesiredPreviewLength ).Returns( 250 );
            var preferences = mockPreferences.Object;

            var generator = new PreviewGenerator( preferences.DesiredPreviewLength, mediator );

            var preview = generator.GetPreview( preferences, ThemeType.None );

            Assert.IsTrue( preview.Trim().Length > 0 );
            Assert.IsTrue( preview.Contains( longBlurb250 ) );
        }



        [Test]
        public void CommaCleanupTest()
        {
            var item = new PreviewItem();

            item.Text = "Leather Interior,, Navigation, Back-Up Camera,,,, Heated Seats, Alloy Wheels,, Heated Mirrors,,, Overhead Airbag";
            PreviewGenerator.CleanupPreviewItems(new []{item});
            Assert.IsTrue(item.Text == "Leather Interior, Navigation, Back-Up Camera, Heated Seats, Alloy Wheels, Heated Mirrors, Overhead Airbag");

            item.Text = "Leather Interior, Navigation, Back-Up Camera, Heated Seats, Alloy Wheels, Heated Mirrors, Overhead Airbag";
            PreviewGenerator.CleanupPreviewItems(new[] { item });
            Assert.IsTrue(item.Text == "Leather Interior, Navigation, Back-Up Camera, Heated Seats, Alloy Wheels, Heated Mirrors, Overhead Airbag");

            item.Text = "Leather Interior, Navigation,,, Back-Up Camera, Heated Seats, Alloy Wheels, Heated Mirrors,, Overhead Airbag  ";
            PreviewGenerator.CleanupPreviewItems(new[] { item });
            Assert.IsTrue(item.Text == "Leather Interior, Navigation, Back-Up Camera, Heated Seats, Alloy Wheels, Heated Mirrors, Overhead Airbag  ");

            item.Text = "Nav System, Moonroof, Leather, Dual Zone A/C, IPOD & USB ADAPTER , Aluminum Wheels, Heated Mirrors, Rear Air, Turbo Charged Engine, Head Airbag, PADDLE SHIFTERS";
            PreviewGenerator.CleanupPreviewItems(new[] { item });
            Assert.IsTrue(item.Text == "Nav System, Moonroof, Leather, Dual Zone A/C, IPOD & USB ADAPTER, Aluminum Wheels, Heated Mirrors, Rear Air, Turbo Charged Engine, Head Airbag, PADDLE SHIFTERS");

            item.Text = "Nav System, Moonroof, Leather, Dual Zone A/C,, IPOD & USB ADAPTER ,, Aluminum Wheels, Heated Mirrors, Rear Air, Turbo Charged Engine, Head Airbag, PADDLE SHIFTERS";
            PreviewGenerator.CleanupPreviewItems(new[] { item });
            Assert.IsTrue(item.Text == "Nav System, Moonroof, Leather, Dual Zone A/C, IPOD & USB ADAPTER, Aluminum Wheels, Heated Mirrors, Rear Air, Turbo Charged Engine, Head Airbag, PADDLE SHIFTERS");

            item.Text = "100, 000 miles!";
            PreviewGenerator.CleanupPreviewItems(new [] { item });
            Assert.IsTrue(item.Text == "100,000 miles!");
        }


        // run this test multiple times because the preview generator has randomized phrases
        // FB: 26640 - remove non-compliant phrases from adds, tureen 10/31/2013
        [TestCase("1000", "NADA")]
        [TestCase("2500", "NADA")]
        [TestCase("500", "MY LITTLE BLACK BOOK")]
        [TestCase("750", "SOME CRAZY BOOK")]
        public void wordComplianceTest(string sPriceBelowBook, string sPreferredBook)
        {

            // mock the mediator class need for the preview generator
            var mockMediator = new Mock<ITemplateMediator>();

            // mock the dealers adversiting preferences
            var mockDealerAdPreferences = new Mock<IDealerAdvertisementPreferences>();
            mockDealerAdPreferences.SetupGet(x => x.DesiredPreviewLength).Returns(250);
            //var preferences = mockPreferences.Object;

            // setup dealer used preview preferrences
            var mockUsedPreviewPreferences = new Mock<IPreviewPreferences>();
            mockUsedPreviewPreferences.SetupAllProperties();
            mockUsedPreviewPreferences.Object.NewOrUsed = 2; // used
            mockUsedPreviewPreferences.Object.UsePriceBelowBook = true;
            mockUsedPreviewPreferences.Object.UsePriceReduced = true;
            mockUsedPreviewPreferences.Object.UseCertified = true;

            mockUsedPreviewPreferences.Object.UseOneOwner = true;

            // setup vehicle template
            var mockTemplateData = new Mock<IVehicleTemplatingData>();
            mockTemplateData.SetupAllProperties();
            mockTemplateData.Setup(d => d.InventoryItem.IsNew()).Returns(false); // used cars only

            // setup used care preferences
            mockTemplateData.SetupGet(x => x.UsedPreviewPreferences).Returns(mockUsedPreviewPreferences.Object);

            // set price below book value
            mockTemplateData.SetupGet(x => x.HasGoodPriceBelowBook).Returns(true);
            mockTemplateData.SetupGet(x => x.PriceBelowBook).Returns(sPriceBelowBook);
            mockTemplateData.SetupGet(x => x.PreferredBook).Returns(sPreferredBook);

            // set one owner
            mockTemplateData.SetupGet(x => x.IsOneOwner).Returns(true);
            mockTemplateData.SetupGet(x => x.Carfax1Owner).Returns("One Owner");

            // set use price reduced
            mockTemplateData.SetupGet(x => x.IsPriceReduced).Returns(true);
            mockTemplateData.SetupGet(x => x.HighInternetPrice).Returns("$45,000");

            // set certified stuff
            mockTemplateData.SetupGet(x => x.IsCertified).Returns(true);
            mockTemplateData.SetupGet(x => x.HasCertificationPreview).Returns(true);
            mockTemplateData.SetupGet(x => x.Certified).Returns("Certified vehicle");
            mockTemplateData.SetupGet(x => x.IsCertified).Returns(true);
            mockTemplateData.SetupGet(x => x.CertificationPreview).Returns("This car is certified");

            mockMediator.SetupGet(x => x.VehicleData).Returns(mockTemplateData.Object);


            // get a real preview generator and test the output
            var generator = new PreviewGenerator(mockDealerAdPreferences.Object.DesiredPreviewLength , mockMediator.Object);

            var preview = generator.GetPreview(mockDealerAdPreferences.Object, ThemeType.None);

            Assert.IsTrue(!preview.ToUpper().Contains("DEAL"), "The word or phrase 'DEAL' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("SPECIAL DEAL"), "The word or phrase 'Special Deal' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("SPECIAL PURCHASE"), "The word or phrase 'Special Purchase' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("SPECIAL PRICE"), "The word or phrase 'Special Price' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("SAVE"), "The word or phrase 'Save' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("SAVINGS"), "The word or phrase 'Savings' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("INCENTIVES"), "The word or phrase 'Incentives' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("INFINITI INCENTIVES"), "The word or phrase 'Infiniti Incentives' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("HUGE SAVINGS"), "The word or phrase 'Huge Savings' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("BEST SAVINGS"), "The word or phrase 'Best Savings' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("AGGRESSIVE DEALS"), "The word or phrase 'Aggressive Deals' is not compliant in the add preview");
            Assert.IsTrue(!preview.ToUpper().Contains("AGGRESSIVE SAVINGS"), "The word or phrase 'Aggressive Savings' is not compliant in the add preview");
        }


    }
     
}
