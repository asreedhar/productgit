﻿using MAX.Entities;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    public class VehicleTypeTests
    {
        [Test]
        public void Should_convert_0_to_Both()
        {
            VehicleType type = 0;

            Assert.That(type, Is.EqualTo(VehicleType.Both));
        }

        [Test]
        public void Should_convert_New_to_New()
        {
            VehicleType type = "New";

            Assert.That(type, Is.EqualTo(VehicleType.New));
        }

        [Test]
        public void Should_convert_Used_to_Used()
        {
            VehicleType type = "Used";

            Assert.That(type, Is.EqualTo(VehicleType.Used));
        }

        [Test]
        public void Should_convert_Both_to_Both()
        {
            VehicleType type = "Both";

            Assert.That(type, Is.EqualTo(VehicleType.Both));
        }

        [Test]
        public void Should_convert_1_to_New()
        {
            VehicleType type = 1;

            Assert.That(type, Is.EqualTo(VehicleType.New));
        }

        [Test]
        public void Should_convert_2_to_Used()
        {
            VehicleType type = 2;

            Assert.That(type, Is.EqualTo(VehicleType.Used));
        }

        [Test]
        [ExpectedException]
        public void Should_throw_exception_when_converting_to_unknown_type()
        {
            VehicleType type = 928475;
        }

    }
}