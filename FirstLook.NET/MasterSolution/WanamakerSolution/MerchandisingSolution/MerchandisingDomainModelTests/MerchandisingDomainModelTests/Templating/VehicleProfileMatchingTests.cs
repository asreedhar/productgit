﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using MAX.Entities;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    public class VehicleProfileMatchingTests
    {
        private IInventoryData usedHonda;
        private IInventoryData newBmw;
        private VehicleProfile hondaProfile;
        private IInventoryData unknownInventoryHonda;

        [SetUp]
        public void SetUp()
        {
            var usedHondaMock = new Mock<IInventoryData>();
            usedHondaMock.Setup(i => i.Make).Returns("Honda");
            usedHondaMock.Setup(i => i.Model).Returns("Accord");
            usedHondaMock.Setup(i => i.Year).Returns("2009");
            usedHondaMock.Setup(i => i.ChromeStyleId).Returns(12345);
            usedHondaMock.Setup(i => i.InventoryType).Returns(VehicleType.Used);
            usedHonda = usedHondaMock.Object;

            var newHondaMock = new Mock<IInventoryData>();
            newHondaMock.Setup(i => i.Make).Returns("Honda");
            newHondaMock.Setup(i => i.Model).Returns("Accord");
            newHondaMock.Setup(i => i.Year).Returns("2009");
            newHondaMock.Setup(i => i.ChromeStyleId).Returns(12345);
            newHondaMock.Setup(i => i.InventoryType).Returns(VehicleType.New);
            newBmw = newHondaMock.Object;

            var unknownHondaInventoryMock = new Mock<IInventoryData>();
            unknownHondaInventoryMock.Setup(i => i.Make).Returns("Honda");
            unknownHondaInventoryMock.Setup(i => i.Model).Returns("Accord");
            unknownHondaInventoryMock.Setup(i => i.Year).Returns("2009");
            unknownHondaInventoryMock.Setup(i => i.ChromeStyleId).Returns(12345);
            unknownHondaInventoryMock.Setup(i => i.InventoryType).Returns(42);
            unknownInventoryHonda = unknownHondaInventoryMock.Object;

            var mockStyleRepository = new Mock<IStyleRepository>();

            var accord = new Model(modelName: "Accord");
            var style = new Style(model: accord);

            mockStyleRepository.Object.GetStyleById(12345);
            mockStyleRepository.Setup(r => r.GetStyleById(It.IsAny<IEnumerable<int>>())).Returns(new List<Style> { style });

            hondaProfile = new VehicleProfile("Honda Accords",
                                                new List<string> { "Honda" },
                                                new List<string> { "Accord" },
                                                new List<string>(),
                                                new List<Segments>(),
                                                new List<string>(),
                                                null, null, null, null, null, null, null);

            hondaProfile.StyleRepository = mockStyleRepository.Object;
			
        }

        [Test]
        public void Profile_for_new_vehicles_should_not_match_used_inventory()
        {

            hondaProfile.VehicleType = VehicleType.New;

            var match = hondaProfile.IsMatch(usedHonda);

            Assert.That(match, Is.False);
        }


        [Test]
        public void Profile_for_new_vehicles_should_match_new_inventory()
        {
            hondaProfile.VehicleType = VehicleType.New;
           
            var match = hondaProfile.IsMatch(newBmw);

            Assert.That(match, Is.True);
        }

        [Test]
        public void Profile_for_any_vehicle_type_should_match_new_inventory()
        {

            hondaProfile.VehicleType = VehicleType.Both;

            var match = hondaProfile.IsMatch(newBmw);

            Assert.That(match, Is.True);
        }

        [Test]
        public void Profile_for_any_vehicle_type_should_match_used_inventory()
        {
           
            hondaProfile.VehicleType = VehicleType.Both;
            var match = hondaProfile.IsMatch(usedHonda);

            Assert.That(match, Is.True);
        }

        [Test]
        public void Profile_with_new_vehicle_type_should_match_unknown_inventory_type()
        {
            hondaProfile.VehicleType = VehicleType.New;

            var match = hondaProfile.IsMatch(unknownInventoryHonda);

            Assert.That(match, Is.True);
        }

        [Test]
        public void Profile_with_used_vehicle_type_should_match_unknown_inventory_type()
        {
            hondaProfile.VehicleType = VehicleType.Used;
         
            var match = hondaProfile.IsMatch(unknownInventoryHonda);

            Assert.That(match, Is.True);
        }


        [Test]
        public void Profile_with_both_vehicle_type_should_match_unknown_inventory_type()
        {
            hondaProfile.VehicleType = VehicleType.Both;

            var match = hondaProfile.IsMatch(unknownInventoryHonda);

            Assert.That(match, Is.True);
        }

        [Test]
        public void Profile_match_should_not_be_case_sensitive_for_models()
        {

            var newBmwMock = new Mock<IInventoryData>();
            newBmwMock.Setup(i => i.Make).Returns("BMW");
            newBmwMock.Setup(i => i.Model).Returns("7 SERIES");
            newBmwMock.Setup(i => i.Year).Returns("2012");
            newBmwMock.Setup(i => i.InventoryType).Returns(VehicleType.New);
            newBmw = newBmwMock.Object;

            var bmwProfile = new VehicleProfile("BMWs",
                                          new List<string> { "BMW" },
                                          new List<string> { "7 Series", "7-Series" },
                                          new List<string>(),
                                          new List<Segments>(),
                                          new List<string>(),
                                          null, null, null, null, null, null, null);

            Assert.That(bmwProfile.IsMatch(newBmw));
        }

        [Test]
        public void Profile_should_match_if_it_inventory_model_starts_with_profile_model()
        {

            var newBmwMock = new Mock<IInventoryData>();
            newBmwMock.Setup(i => i.Make).Returns("BMW");
            newBmwMock.Setup(i => i.Model).Returns("7 SERIES SEDAN");
            newBmwMock.Setup(i => i.Year).Returns("2012");
            newBmwMock.Setup(i => i.InventoryType).Returns(VehicleType.New);
            newBmw = newBmwMock.Object;

            var bmwProfile = new VehicleProfile("BMWs",
                                          new List<string> { "BMW" },
                                          new List<string> { "7 Series", "7-Series" },
                                          new List<string>(),
                                          new List<Segments>(),
                                          new List<string>(),
                                          null, null, null, null, null, null, null);

            Assert.That(bmwProfile.IsMatch(newBmw));
        }



        [Test]
        public void Should_not_match_profiles_if_one_model_contains_the_text_for_another()
        {

            var newBmwMock = new Mock<IInventoryData>();
            newBmwMock.Setup(i => i.Make).Returns("BMW");
            newBmwMock.Setup(i => i.Model).Returns("X3 Series");
            newBmwMock.Setup(i => i.Year).Returns("2012");
            newBmwMock.Setup(i => i.InventoryType).Returns(VehicleType.New);
            newBmw = newBmwMock.Object;

            var bmwProfile = new VehicleProfile("BMWs",
                                          new List<string> { "BMW" },
                                          new List<string> { "3 Series" },
                                          new List<string>(),
                                          new List<Segments>(),
                                          new List<string>(),
                                          null, null, null, null, null, null, null) {VehicleType = VehicleType.New}; 

            Assert.That(!bmwProfile.IsMatch(newBmw));
        }


        [Test]
        public void Profile_match_should_not_be_case_sensitive_for_makes()
        {

            var newBmwMock = new Mock<IInventoryData>();
            newBmwMock.Setup(i => i.Make).Returns("BMW");
            newBmwMock.Setup(i => i.Model).Returns("7 SERIES");
            newBmwMock.Setup(i => i.Year).Returns("2012");
            newBmwMock.Setup(i => i.InventoryType).Returns(VehicleType.New);
            newBmw = newBmwMock.Object;

            var bmwProfile = new VehicleProfile("BMWs",
                                          new List<string> { "Bmw" },
                                          new List<string> { "7 Series", "7-Series" },
                                          new List<string>(),
                                          new List<Segments>(),
                                          new List<string>(),
                                          null, null, null, null, null, null, null);

            Assert.That(bmwProfile.IsMatch(newBmw));
        }


        [Test]
        public void Profile_match_should_not_be_case_sensitive_for_trims()
        {

            var newBmwMock = new Mock<IInventoryData>();
            newBmwMock.Setup(i => i.Make).Returns("BMW");
            newBmwMock.Setup(i => i.Model).Returns("7 Series");
            newBmwMock.Setup(i => i.Year).Returns("2012");
            newBmwMock.Setup(i => i.Trim).Returns("330i");
            newBmwMock.Setup(i => i.InventoryType).Returns(VehicleType.New);
            newBmw = newBmwMock.Object;

            var bmwProfile = new VehicleProfile("BMWs",
                                          new List<string> { "Bmw" },
                                          new List<string> { "7 Series", "7-Series" },
                                          new List<string> { "330I" },
                                          new List<Segments>(),
                                          new List<string>(),
                                          null, null, null, null, null, null, null);

            Assert.That(bmwProfile.IsMatch(newBmw));
        }

        [Test]
        public void Profile_match_should_match_if_no_trim_on_inventory_data()
        {

            var newBmwMock = new Mock<IInventoryData>();
            newBmwMock.Setup(i => i.Make).Returns("BMW");
            newBmwMock.Setup(i => i.Model).Returns("7 Series");
            newBmwMock.Setup(i => i.Year).Returns("2012");
            newBmwMock.Setup(i => i.Trim).Returns("");
            newBmwMock.Setup(i => i.InventoryType).Returns(VehicleType.New);
            newBmw = newBmwMock.Object;

            var bmwProfile = new VehicleProfile("BMWs",
                                          new List<string> { "Bmw" },
                                          new List<string> { "7 Series", "7-Series" },
                                          new List<string> { "330I" },
                                          new List<Segments>(),
                                          new List<string>(),
                                          null, null, null, null, null, null, null) {VehicleType = VehicleType.New};

            Assert.That(bmwProfile.IsMatch(newBmw));
        }

    }
}