﻿using System.Xml;
using FirstLook.Merchandising.DomainModel.Templating;
using MAX.Entities;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    public class VehicleProfileXmlConstructorTests
    {
        [Test]
        public void ConstructingFromXmlShouldSetTheVehicleType()
        {
            var xmlString = "<VehicleProfile vehicleProfileId=\"5\" title=\"All Cars\" makeIdList=\"\" modelList=\"\" trimList=\"\" segmentIdList=\"\" marketClassIdList=\"\" vehicleType=\"1\" />";

            var vehicleProfileXml = new XmlDocument();
            vehicleProfileXml.LoadXml(xmlString);

            var profile = new VehicleProfile(vehicleProfileXml.SelectSingleNode("VehicleProfile"));

            Assert.That(profile.VehicleType == VehicleType.New);
        }

        [Test]
        public void ConstructingFromXmlWithoutVehicleTypeShouldDefaultToAny()
        {
            var xmlString = "<VehicleProfile vehicleProfileId=\"5\" title=\"All Cars\" makeIdList=\"\" modelList=\"\" trimList=\"\" segmentIdList=\"\" marketClassIdList=\"\" />";

            var vehicleProfileXml = new XmlDocument();
            vehicleProfileXml.LoadXml(xmlString);

            var profile = new VehicleProfile(vehicleProfileXml.SelectSingleNode("VehicleProfile"));

            Assert.That(profile.VehicleType == VehicleType.Both);
        }

    }
}