﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antlr.StringTemplate;
using NUnit.Framework;

using FirstLook.Merchandising.DomainModel.Core;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    public class TemplatingTests
    {
        [Test]
        public void ExtractPropertyValue()
        {
            // templatedPropertyName: $Foo$ 
            // obj: an object that has property Foo
            // returns: value of obj.Foo

            var data = new MockVehicleData { Make = "a" };
            data.HighlightedPackages = new List<string> { "b", "c" };

            var make = Templater.ExtractPropertyValue(data, "$Make$");
            Assert.IsNotNull(make);
            Console.WriteLine(make);
            Assert.AreEqual("a", make);

            var makeList = Templater.ExtractPropertyValue(data, "$Make; separator=\",\"$");
            Assert.IsNotNull(makeList);
            Console.WriteLine(makeList);
            Assert.AreEqual("a", makeList);

            var pack = Templater.ExtractPropertyValue(data, "$HighlightedPackages$");
            Assert.IsNotNull(pack);
            Assert.AreEqual(data.HighlightedPackages, pack); 
            
            Assert.IsNotNull(pack as List<string>);
            Assert.IsTrue(((List<string>)pack).Count == 2);
            Assert.IsTrue(((List<string>)pack)[0] == "b");
            Assert.IsTrue(((List<string>)pack)[1] == "c");


            var packList = Templater.ExtractPropertyValue(data, "$HighlightedPackages; separator=\",\"$");
            Assert.IsNotNull(packList);
            Assert.AreEqual(data.HighlightedPackages, packList);

            Assert.IsNotNull(pack as List<string>);
            Assert.IsTrue(((List<string>)packList).Count == 2);
            Assert.IsTrue(((List<string>)packList)[0] == "b");
            Assert.IsTrue(((List<string>)packList)[1] == "c");

            // a property we don't have
            var bar = Templater.ExtractPropertyValue(data, "$Bar$");
            Assert.IsNull(bar);

        }

        [Test]
        public void ApplyTemplateTest()
        {
            var variable = "$a$";
            var template = "separator=\",\"";

            var result = Templater.ApplyTemplate(variable, template);
            Assert.AreEqual("$a; separator=\",\"$", result);

            // edge cases
            Assert.AreEqual(string.Empty, Templater.ApplyTemplate(string.Empty, string.Empty));
            Assert.AreEqual(string.Empty, Templater.ApplyTemplate(null, null));
            Assert.AreEqual("$var$", Templater.ApplyTemplate("$var$", string.Empty));
            Assert.AreEqual(string.Empty, Templater.ApplyTemplate(string.Empty, "separator=\",\""));
        }

        [Test]
        public void MultiVariableReplacement()
        {
            var template = "separator=\",\"";

            string var1 = "$base.Foo$ and $base.Bar$";
            var result = Templater.ApplyTemplateToVariables(var1, template);
            Assert.AreEqual("$base.Foo; separator=\",\"$ and $base.Bar; separator=\",\"$", result);

            var1 = "Hello $foo$, how is $bar$?";
            result = Templater.ApplyTemplateToVariables(var1, template);
            Assert.AreEqual("Hello $foo; separator=\",\"$, how is $bar; separator=\",\"$?", result);
        }

        [Test]
        public void SingleVariableReplacement()
        {
            var template = "separator=\",\"";

            string var1 = "$base.Foo$";
            var result = Templater.ApplyTemplateToVariables(var1, template);
            Assert.AreEqual("$base.Foo; separator=\",\"$", result);

            var1 = "Hello $world$, how are you?";
            result = Templater.ApplyTemplateToVariables(var1, template);
            Assert.AreEqual("Hello $world; separator=\",\"$, how are you?", result);

            var1 = "Hello world.";
            result = Templater.ApplyTemplateToVariables(var1, template);
            Assert.AreEqual("Hello world.", result);
        }


        [Test]
        public void RegexMultipleMatches()
        {
            string var2 = "hello $base.Foo$ world $base.Bar$. How is it going $base.Baz$? Some variables have numbers like $base.Tier1$.";
            Assert.IsTrue(Templater.VariableRegex().IsMatch(var2));
            var matches2 = Templater.VariableRegex().Matches(var2);
            Assert.AreEqual(4, matches2.Count);

            foreach (var match in matches2)
            {
                Console.WriteLine(match);
            }

            Assert.AreEqual("$base.Foo$", matches2[0].ToString());
            Assert.AreEqual("$base.Bar$", matches2[1].ToString());
            Assert.AreEqual("$base.Baz$", matches2[2].ToString());
            Assert.AreEqual("$base.Tier1$", matches2[3].ToString());
        }


        [Test]
        public void RegexSingleMatch()
        {
            string var1 = "$base.Foo$";
            Assert.IsTrue(Templater.VariableRegex().IsMatch(var1));
            var match1 = Templater.VariableRegex().Match(var1).ToString();
            Console.WriteLine(match1);
            Assert.AreEqual(match1, var1);
        }

        [Test]
        public void GetMultipleSimpleTemplateAttributes()
        {
            string input = "$hello$ $world$";
            var attr = Templater.GetTemplateAttributes(input);
            Assert.AreEqual(2, attr.Count);
            Assert.AreEqual("$hello$", attr.First());
            Assert.AreEqual("$world$", attr.Last());
        }

        [Test]
        public void GetSingleDecoratedAttribute()
        {
            string input = "$hello; separator=\",\"$";
            var attr = Templater.GetTemplateAttributes(input);
            Assert.AreEqual(1, attr.Count);
            Assert.AreEqual("$hello; separator=\",\"$", attr.First());                        
        }

        [Test]
        public void GetSingleDottedTemplateAttribute()
        {
            string input = "$hello.world$";
            var attr = Templater.GetTemplateAttributes(input);
            Assert.AreEqual(1, attr.Count);
            Assert.AreEqual("$hello.world$", attr.First());                        
        }

        [Test]
        public void GetSingleSimpleTemplateAttribute()
        {
            string input = "Hello $world$";
            var attr = Templater.GetTemplateAttributes(input);
            Assert.AreEqual(1, attr.Count);
            Assert.AreEqual("$world$", attr.First());            
        }

        [Test]
        public void SingleVariableSubstitution()
        {
            // raw input template
            string toTemplate = "This $Make$ is nice.";

            // Apply list to template. -- make a function to do this.
            toTemplate = Templater.ApplyTemplateToVariables(toTemplate, Templater.LIST_NAME);

            // Extract attributes without function names
            var attributes = Templater.GetTemplateAttributes(toTemplate, true);

            var t = new StringTemplate(toTemplate);

            // A variable to pass into the template.
            var data = new MockVehicleData { Make = "a" };

            // Set values in string template.
            foreach(var attr in attributes)
            {
                var value = Templater.ExtractPropertyValue(data, attr);
                Assert.IsNotNull(value );

                t.SetAttribute( Templater.StripDollarDelimiters(attr), value );
            }

            string result = t.ToString();
            Console.WriteLine(result);

            Assert.IsNotEmpty(result);
            Assert.AreEqual("This a is nice.", result);
        }

        [Test]
        public void DoTemplatingSingleDataPoint()
        {
            // A variable to pass into the template.
            var data = new MockVehicleData { Make = "a" };

            // raw input template
            string toTemplate = "This $Make$ is nice.";

            string result = Templater.DoTemplating(toTemplate, data);
            Console.WriteLine(result);

            Assert.IsNotEmpty(result);
            Assert.AreEqual("This a is nice.", result);
        }

        [Test]
        public void DoTemplatingMultipleDataPoint()
        {
            // A variable to pass into the template.
            var data = new MockVehicleData { Make = "a" };
            data.HighlightedPackages = new List<string> {"b", "c"};

            // raw input template
            string toTemplate = "This $Make$ is nice. It has packages $HighlightedPackages$.";

            string result = Templater.DoTemplating(toTemplate, data);
            Console.WriteLine(result);

            Assert.IsNotEmpty(result);
            Assert.AreEqual("This a is nice. It has packages b, c.", result);
        }

        [Test]
        public void StripBase()
        {
            // A variable to pass into the template.
            var data = new MockVehicleData { Make = "a" };
            data.HighlightedPackages = new List<string> { "b", "c" };

            // raw input template
            string toTemplate = "This $base.Make$ is nice. It has packages $base.HighlightedPackages$.";

            string result = Templater.DoTemplating(toTemplate, data);
            Console.WriteLine(result);

            Assert.IsNotEmpty(result);
            Assert.AreEqual("This a is nice. It has packages b, c.", result);
            
        }

        [Test]
        public void GroupTest()
        {
            StringTemplateGroup supergroup = new StringTemplateGroup("super");
            supergroup.DefineTemplate("bold", "<b>$it$</b>");
            StringTemplate st = new StringTemplate(supergroup, "$name:bold()$");
            st.SetAttribute("name", "Terence");
            string expecting = "<b>Terence</b>";

            Assert.AreEqual(expecting, st.ToString());
        }


    }
}
