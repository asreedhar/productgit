﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Templating.Previews;
using FirstLook.Merchandising.DomainModel.Vehicles;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Templating
{
    [TestFixture]
    class PreviewDuplicateRemoverTests
    {
        [Test( Description = "Fogbugzid : 13743" )]
        public void Should_apply_generic_replacement_groups_to_preview_equipment_items ()
        {

            var replacementRule1 = CreateRule( "All Wheel Drive", new List<int> { 1, 2, 3 } );
            var replacementRule2 = CreateRule( "Heated Leather Seats", new List<int> { 4, 5, 6 } );

            var rules = new GenericEquipmentReplacementCollection { replacementRule1, replacementRule2 };

            var previewItems = new List<PreviewEquipmentItem>();
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio" ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Heated driver seats", 4 ) );
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio2" ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Heated passenger seats", 5 ) );
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio3" ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Leather", 6 ) );
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio5" ) );


            var duplicateRemover = new PreviewEquipmentItemDuplicateRemover( rules, previewItems );

            var result = duplicateRemover.RemoveDuplicates();

            Assert.AreEqual( result.Count, 5 );
            Assert.AreEqual( result[4].Description, "Heated Leather Seats" );
        }

        [Test( Description = "Fogbugzid : 13743" )]
        public void Should_apply_longest_replacement_group_first ()
        {

            var replacementRule1 = CreateRule( "Heated seats", new List<int> { 4, 5 } );

            var replacementRule2 = CreateRule( "Heated Leather Seats", new List<int> { 4, 5, 6 } );

            var rules = new GenericEquipmentReplacementCollection { replacementRule1, replacementRule2 };

            var previewItems = new List<PreviewEquipmentItem>();
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio" ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Heated driver seats", 4 ) );
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio2" ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Heated passenger seats", 5 ) );
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio3" ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Leather", 6 ) );
            previewItems.Add( new PreviewEquipmentItem( "Satellite Radio5" ) );


            var duplicateRemover = new PreviewEquipmentItemDuplicateRemover( rules, previewItems );

            var result = duplicateRemover.RemoveDuplicates();

            Assert.AreEqual( result.Count, 5 );
            Assert.AreEqual( result[4].Description, "Heated Leather Seats" );
        }

        private static GenericEquipmentReplacement CreateRule (string description, List<int> categoryIds)
        {
            var includedCategories = new CategoryCollection( description + "Categories" );
            foreach ( var includedCategory in categoryIds )
            {
                includedCategories.Add( new CategoryLink( includedCategory, "", 0, "", "desc" + includedCategory ) );
            }
            return new GenericEquipmentReplacement() { Description = description, IncludesCategories = includedCategories };
        }
        [Test( Description = "Generic replacements in preview items were not keeping their categorids" )]
        public void Generic_replacement_groups_in_preview_items_should_be_added_tocategoryids ()
        {
            var rules = new GenericEquipmentReplacementCollection
                            {
                                CreateRule("Heated Cooled Leather Seats", new List<int>() {1156, 1267, 1078}),
                                CreateRule("Heated Cooled  Seats", new List<int>() {1156, 1267, 1078}),
                                CreateRule("Heated Colled Leather Seats", new List<int>() {1156, 1267}),
                                CreateRule("Panoramic Sunroof", new List<int>() {1143, 1069}),
                                CreateRule("Heated Leather Seats", new List<int>() {1156, 1078}),
                                CreateRule("Power Seats", new List<int>() {1074, 1075}),
                                CreateRule("Heated Seats", new List<int>() {1156})
                            };

            var previewItems = new List<PreviewEquipmentItem>();
            previewItems.Add( new PreviewGenericEquipmentItem( "Leather Interior", 1078 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Moonroof", 1069 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Heated Seats", 1156 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Aluminium Wheels", 1123 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Turbo Charged", 1054 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Head Airbag", 1007 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "All Wheel Drive", 1041 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( rules.Item( 4 ).Description, rules.Item( 4 ).IncludesGenericEquipmentIds ) );
            previewItems.Add( new PreviewGenericEquipmentItem( rules.Item( 3 ).Description, rules.Item( 3 ).IncludesGenericEquipmentIds ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Power liftgate", 1225 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "Premium Soundsystem", 1136 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "iPod/mp3 input", 1230 ) );
            previewItems.Add( new PreviewGenericEquipmentItem( "MP3 Player", 1150 ) );

            var duplicateRemover = new PreviewEquipmentItemDuplicateRemover( rules, previewItems );

            var result = duplicateRemover.RemoveDuplicates();

            Assert.IsFalse( ItemsContainDescription( result, "Moonroof" ) );
            Assert.IsTrue( ItemsContainDescription( result, "Panoramic Sunroof" ) );
        }

        private static bool ItemsContainDescription (IEnumerable<PreviewEquipmentItem> items, string description)
        {
            return items.Any( item => item.Description.Equals( description ) );
        }
    }
}
