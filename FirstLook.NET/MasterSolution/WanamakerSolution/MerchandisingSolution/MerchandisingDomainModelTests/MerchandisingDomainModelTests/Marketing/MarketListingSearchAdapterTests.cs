﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Amazon.DynamoDBv2.DocumentModel;
using Autofac;
using Dapper;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.ModelBuilder;
using FirstLook.Pricing.DomainModel;
using Merchandising.Messages;
using NUnit.Framework;
using Moq;

namespace MerchandisingDomainModelTests.Marketing
{
    [TestFixture]
    public class MarketListingSearchAdapterTests
    {
        [SetUp]
        public void SetupMethod()
        {
            SetupAutofac();
        }

        [Test]
        public void ExcludesListingsWithUnder1KMiles()
        {
            var listings = new List<MarketListing>
            {
                new MarketListing()
                {
                    Color = "red",
                    DealerName = "WC BMW",
                    IsCertified = false,
                    ListPrice = 100,
                    Mileage = 999,
                    Options = "o1",
                    VehicleDescription = "d1",
                    VehicleId = 1,
                    VehicleName = "n1"
                },
                new MarketListing()
                {
                    Color = "green",
                    DealerName = "WC BMW",
                    IsCertified = false,
                    ListPrice = 1000,
                    Mileage = 1000,
                    Options = "o2",
                    VehicleDescription = "d2",
                    VehicleId = 2,
                    VehicleName = "n2"
                },
                new MarketListing()
                {
                    Color = "blue",
                    DealerName = "WC BMW",
                    IsCertified = false,
                    ListPrice = 10000,
                    Mileage = 1001,
                    Options = "o3",
                    VehicleDescription = "d3",
                    VehicleId = 3,
                    VehicleName = "n3"
                }
            };

            var dalMarketListingsMock = new Mock<MarketListingsV1DataAccess>();
            dalMarketListingsMock.Setup(dal => dal.Search(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(listings);
            dalMarketListingsMock.CallBase = true;

            var searchAdapterMock = new Mock<MarketListingSearchAdapter>();
            searchAdapterMock.Setup(t => t.GetDealerMarketListingsDataAccess(It.IsAny<string>()))
                .Returns(dalMarketListingsMock.Object);
            searchAdapterMock.CallBase = true;

            var sa = searchAdapterMock.Object;
            var results = sa.Search("a", "b", "c").ToList();

            Assert.That(results.Count() == 2);
            Assert.That(!results.Any(r => r.Mileage < 1000));
        }

        private static void SetupAutofac()
        {
            var builder = new ContainerBuilder();

            #region marketing XML Autofac registration
            /** The below are all required for MarketingXML generation **/
            //builder.RegisterModule(new CoreModule());
            //builder.RegisterType<DealerGeneralPreferenceDataAccess>().As<IDealerGeneralPreferenceDataAccess>();

            //// Photos
            //builder.RegisterType<PhotoLocatorDataAccess>().As<IPhotoLocatorDataAccess>();
            //builder.RegisterType<PhotoVehiclePreferenceDataAccess>().As<IPhotoVehiclePreferenceDataAccess>();
            //builder.RegisterModule(new PhotoServicesModule());
            ////AdPreview
            //builder.RegisterType<AdPreviewVehiclePreferenceDataAccess>().As<IAdPreviewVehiclePreferenceDataAccess>();
            //builder.RegisterType<EquipmentDataAccess>().As<IEquipmentDataAccess>();
            //// Equipment
            //builder.RegisterType<EquipmentProviderAssignmentDataAccess>().As<IEquipmentProviderAssignmentDataAccess>();
            //builder.RegisterType<EquipmentDataAccess>().As<IEquipmentDataAccess>();
            //builder.RegisterType<EquipmentProviderDataAccess>().As<IEquipmentProviderDataAccess>();
            //// Vehicle summary
            //builder.RegisterType<VehicleSummaryDataAccess>().As<IVehicleSummaryDataAccess>();
            //// Consumer Highlights
            //builder.RegisterType<ConsumerHighlightsDealerPreferenceDataAccess>()
            //    .As<IConsumerHighlightsDealerPreferenceDataAccess>();
            //builder.RegisterType<ConsumerHighlightsVehiclePreferenceDataAccess>()
            //    .As<IConsumerHighlightsVehiclePreferenceDataAccess>();

            //// Certified
            //builder.RegisterType<CertifiedVehiclePreferenceDataAccess>().As<ICertifiedVehiclePreferenceDataAccess>();

            //// Market Analysis
            //builder.RegisterType<MarketAnalysisDealerPreferenceDataAccess>()
            //    .As<IMarketAnalysisDealerPreferenceDataAccess>();
            //builder.RegisterType<MarketAnalysisPricingDataAccess>().As<IMarketAnalysisPricingDataAccess>();
            //builder.RegisterType<MarketAnalysisVehiclePreferenceDataAccess>()
            //    .As<IMarketAnalysisVehiclePreferenceDataAccess>();

            //// MarketListing
            //builder.RegisterType<MarketListingVehiclePreferenceDataAccess>()
            //    .As<IMarketListingVehiclePreferenceDataAccess>();
            //builder.Register(c => new SearchAdapter(c.Resolve<IMarketListingVehiclePreferenceDataAccess>()))
            //    .As<ISearchAdapter>();

            //// Publishing
            //builder.RegisterType<ValueAnalyzerVehiclePreferenceDataAccess>()
            //    .As<IValueAnalyzerVehiclePreferenceDataAccess>();
            #endregion marketing XML

            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());
            builder.RegisterModule(new MerchandisingMessagesModule());

            new PricingDomainRegister().Register(builder);
            new MerchandisingDomainRegistry().Register(builder);
            new MerchandisingModelBuilderRegistry().Register(builder);

            var mockLogger = new Mock<ILogger>().Object;
            builder.RegisterInstance(mockLogger).AsImplementedInterfaces();

            var container = builder.Build();
            Registry.RegisterContainer(container);
        }
    }
}
