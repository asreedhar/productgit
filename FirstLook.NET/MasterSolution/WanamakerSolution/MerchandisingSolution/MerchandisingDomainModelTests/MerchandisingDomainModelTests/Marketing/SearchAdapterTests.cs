﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using NUnit.Framework;
using Moq;

namespace MerchandisingDomainModelTests.Marketing
{
    [TestFixture]
    public class SearchAdapterTests
    {        
        [Test]
        public void ExcludesListingsWithUnder1KMiles()
        {
            var listings = new List<MarketListing>();
            listings.Add( new MarketListing(){Color="red", DealerName = "WC BMW", IsCertified = false, ListPrice = 100, Mileage = 999, Options = "o1", VehicleDescription = "d1", VehicleId = 1, VehicleName = "n1"} );
            listings.Add( new MarketListing(){Color="green", DealerName = "WC BMW", IsCertified = false, ListPrice = 1000, Mileage = 1000, Options = "o2", VehicleDescription = "d2", VehicleId = 2, VehicleName = "n2"} );
            listings.Add( new MarketListing(){Color="blue", DealerName = "WC BMW", IsCertified = false, ListPrice = 10000, Mileage = 1001, Options = "o3", VehicleDescription = "d3", VehicleId = 3, VehicleName = "n3"} );

            var dalMock = new Mock<MarketListingVehiclePreferenceDataAccess>();
            dalMock.Setup(da => da.Search(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(listings);
            dalMock.CallBase = true;

            // A search specification
            IMarketListingFilterSpecification filter = new MarketListingFilterSpecification();

            var sa = new SearchAdapter(dalMock.Object);
            var results = sa.Search("a", "b", "c").ToList();

            Assert.That(results.Count() == 2);
            Assert.That( !results.Any(r => r.Mileage < 1000));
        }

    }

}
