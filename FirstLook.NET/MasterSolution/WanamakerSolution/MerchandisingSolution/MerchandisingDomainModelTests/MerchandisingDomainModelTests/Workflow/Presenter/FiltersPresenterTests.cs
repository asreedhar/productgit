﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Workflow;
using FirstLook.Merchandising.DomainModel.Workflow.Presenter;
using MAX.Entities;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Workflow.Presenter
{
    [TestFixture]
    public class FiltersPresenterTests
    {
        private static IFilterTemplate CreateBasicTemplate()
        {
            // NOTE: This template doesn't exactly behave like the real template. It is a modified copy of the original.
            return new FilterTemplate(
                defaultWorkflow: model => model.GetAutoApproveStatus() == AutoApproveStatus.Off ? WorkflowType.CreateInitialAd : WorkflowType.AdReviewNeeded, 
                auxFilters: new[] { WorkflowType.AllInventory, WorkflowType.Offline },
                highLevelFilters: new[] { WorkflowType.NeedsApprovalAll, WorkflowType.NeedsAdReviewAll, WorkflowType.NeedsPricingAll },
                itemTemplates: new[]
                    {
                        /* Auxiliary Filters */
                        new FilterItemTemplate(WorkflowType.AllInventory, "All Inventory"), 
                        new FilterItemTemplate(WorkflowType.Offline, "Offline"),

                        /* Needs Approval */
                        new FilterItemTemplate(WorkflowType.NeedsApprovalAll, "All", 
                                               children: new[] { WorkflowType.NeedsApprovalAll, WorkflowType.CreateInitialAd, WorkflowType.TrimNeeded, WorkflowType.OutdatedPrice },
                                               highLevelParent: WorkflowType.NeedsApprovalAll,
                                               highLevelLabel: "Needs Approval",
                                               isValid: model => model.GetAutoApproveStatus() == AutoApproveStatus.Off),
                        new FilterItemTemplate(WorkflowType.CreateInitialAd, "Ad Approval Needed", highLevelParent: WorkflowType.NeedsApprovalAll),
                        new FilterItemTemplate(WorkflowType.TrimNeeded, "No Trim", // NoTrim can appear under both "Needs Ad Approval" or "Needs Ad Review"
                                               highLevelParents: new[] { WorkflowType.NeedsApprovalAll, WorkflowType.NeedsAdReviewAll }),
                        new FilterItemTemplate(WorkflowType.OutdatedPrice, "Outdated Price in Ad", highLevelParent: WorkflowType.NeedsApprovalAll),

                        /* Needs Ad Review */
                        new FilterItemTemplate(WorkflowType.NeedsAdReviewAll, "All",
                                               children: new[] { WorkflowType.NeedsAdReviewAll, WorkflowType.TrimNeeded, WorkflowType.AdReviewNeeded },
                                               highLevelParent: WorkflowType.NeedsAdReviewAll,
                                               highLevelLabel: "Needs Ad Review",
                                               isValid: model => model.GetAutoApproveStatus() != AutoApproveStatus.Off), // NoTrim specified above
                        new FilterItemTemplate(WorkflowType.AdReviewNeeded, "Ad", highLevelParent: WorkflowType.NeedsAdReviewAll), 

                        /* Needs Pricing */
                        new FilterItemTemplate(WorkflowType.NeedsPricingAll, "All",
                                               children: new[] { WorkflowType.NeedsPricingAll, WorkflowType.NoPrice, WorkflowType.DueForRepricing },
                                               highLevelParent: WorkflowType.NeedsPricingAll,
                                               highLevelLabel: "Needs Pricing"), 
                        new FilterItemTemplate(WorkflowType.NoPrice, "No Price", 
                                               highLevelParent: WorkflowType.NeedsPricingAll,
                                               isVisible: m => false), // Test item visibility
                        new FilterItemTemplate(WorkflowType.DueForRepricing, "Due for Updating Price/Ad", highLevelParent: WorkflowType.NeedsPricingAll)
                    }
                );
        }

        private static IWorkflowCounts GetBasicCounts()
        {
            var counts = new Dictionary<WorkflowType, int>
                             {
                                 {WorkflowType.AllInventory, 10},
                                 {WorkflowType.Offline, 15},
                                 {WorkflowType.NeedsApprovalAll, 40},
                                 {WorkflowType.CreateInitialAd, 20},
                                 {WorkflowType.TrimNeeded, 25},
                                 {WorkflowType.OutdatedPrice, 30},
                                 {WorkflowType.NeedsAdReviewAll, 50},
                                 {WorkflowType.AdReviewNeeded, 35},
                                 {WorkflowType.NeedsPricingAll, 60},
                                 {WorkflowType.NoPrice, 45},
                                 {WorkflowType.DueForRepricing, 55}
                             };

            var countsMock = new Mock<IWorkflowCounts>();
            countsMock
                .Setup(m => m.GetCount(It.IsAny<WorkflowType>()))
                .Returns<WorkflowType>(wf => counts.ContainsKey(wf) ? counts[wf] : 0);
            return countsMock.Object;
        }

        private IWorkflowRepository GetRepository()
        {
            var repositoryMock = new Mock<IWorkflowRepository>();
            repositoryMock
                .Setup(r => r.CalculateCounts(It.IsAny<IEnumerable<IInventoryData>>(), It.IsAny<ISet<WorkflowType>>()))
                .Returns(WorkflowCounts);
            repositoryMock
                .Setup(r => r.Workflows).Returns(WorkflowList);
            repositoryMock
                .Setup(r => r.GetWorkflow(It.IsAny<WorkflowType>()))
                .Returns<WorkflowType>(wf => WorkflowList.Single(w => w.Type == wf));

            WorkflowList.Add(CreateWorkflow(WorkflowType.AllInventory));
            WorkflowList.Add(CreateWorkflow(WorkflowType.Offline));
            WorkflowList.Add(CreateWorkflow(WorkflowType.NeedsApprovalAll));
            WorkflowList.Add(CreateWorkflow(WorkflowType.CreateInitialAd));
            WorkflowList.Add(CreateWorkflow(WorkflowType.TrimNeeded));
            WorkflowList.Add(CreateWorkflow(WorkflowType.OutdatedPrice));
            WorkflowList.Add(CreateWorkflow(WorkflowType.NeedsAdReviewAll));
            WorkflowList.Add(CreateWorkflow(WorkflowType.AdReviewNeeded));
            WorkflowList.Add(CreateWorkflow(WorkflowType.NeedsPricingAll));
            WorkflowList.Add(CreateWorkflow(WorkflowType.NoPrice));
            WorkflowList.Add(CreateWorkflow(WorkflowType.DueForRepricing));
            WorkflowList.Add(CreateWorkflow(WorkflowType.LowActivity));
            WorkflowList.Add(CreateWorkflow(WorkflowType.NoCarfax));

            return repositoryMock.Object;
        }

        private static IWorkflow CreateWorkflow(WorkflowType wf)
        {
            var workflow = new Mock<IWorkflow>();
            workflow.Setup(w => w.Type).Returns(wf);
            return workflow.Object;
        }

        [SetUp]
        public void Setup()
        {
            FiltersViewMock = new Mock<IFiltersView>();
            FiltersModelMock = new Mock<IFiltersModel>();
            FiltersView = FiltersViewMock.Object;
            FiltersModel = FiltersModelMock.Object;

            FiltersViewMock.SetupAllProperties();

            WorkflowList = new List<IWorkflow>();
            WorkflowCounts = GetBasicCounts();
            WorkflowRepository = GetRepository();

            FiltersModelMock.Setup(m => m.GetWorkflowCounts(It.IsAny<ISet<WorkflowType>>())).Returns(WorkflowCounts);
            FiltersModelMock.Setup(m => m.GetWorkflowRepository()).Returns(WorkflowRepository);
        }

        private List<IWorkflow> WorkflowList { get; set; }
        private IWorkflowCounts WorkflowCounts { get; set; }
        private IWorkflowRepository WorkflowRepository { get; set; }
        private IFiltersModel FiltersModel { get; set; }
        private IFiltersView FiltersView { get; set; }
        private Mock<IFiltersModel> FiltersModelMock { get; set; }
        private Mock<IFiltersView> FiltersViewMock { get; set; }

        [TestCase(WorkflowType.TrimNeeded, false, WorkflowType.TrimNeeded)]
        [TestCase(WorkflowType.NoCarfax, false, WorkflowType.CreateInitialAd)]
        [TestCase(WorkflowType.Offline, false, WorkflowType.Offline)]
        [TestCase(WorkflowType.NoCarfax, true, WorkflowType.AdReviewNeeded)]
        [TestCase(null, true, WorkflowType.AdReviewNeeded)]
        public void Setting_CurrentWorkflow_should_only_allow_valid_values(WorkflowType? valueToSet, bool isPreApproveOn, WorkflowType expectedValue)
        {
            var workflowToSet = WorkflowList.SingleOrDefault(w => w.Type == valueToSet);
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(isPreApproveOn
                                                                              ? AutoApproveStatus.OnForAll
                                                                              : AutoApproveStatus.Off);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate())
                                {CurrentWorkflow = workflowToSet};

            Assert.That(presenter.CurrentWorkflow.Type, Is.EqualTo(expectedValue));
        }

        [TestCase(WorkflowType.TrimNeeded, false, WorkflowType.TrimNeeded)]
        [TestCase(null, false, WorkflowType.CreateInitialAd)]
        [TestCase(WorkflowType.NoCarfax, false, WorkflowType.CreateInitialAd)]
        [TestCase(WorkflowType.Offline, false, WorkflowType.Offline)]
        [TestCase(WorkflowType.NoCarfax, true, WorkflowType.AdReviewNeeded)]
        public void Initial_value_for_CurrentWorkflow_should_come_from_Model(WorkflowType? workflowFromModel, bool isPreApproveOn, WorkflowType expectedValue)
        {
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(workflowFromModel);
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(isPreApproveOn
                                                                              ? AutoApproveStatus.OnForAll
                                                                              : AutoApproveStatus.Off);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            Assert.That(presenter.CurrentWorkflow.Type, Is.EqualTo(expectedValue));
        }

        [TestCase(WorkflowType.TrimNeeded, false, WorkflowType.TrimNeeded)]
        [TestCase(WorkflowType.NoCarfax, false, WorkflowType.CreateInitialAd)]
        [TestCase(WorkflowType.Offline, false, WorkflowType.Offline)]
        [TestCase(WorkflowType.NoCarfax, true, WorkflowType.AdReviewNeeded)]
        public void When_View_WorkflowClicked_then_should_change_to_valid_workflow(WorkflowType workflowToChangeTo, bool isPreApproveOn, WorkflowType expectedValue)
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(isPreApproveOn
                                                                              ? AutoApproveStatus.OnForAll
                                                                              : AutoApproveStatus.Off);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();

            FiltersView.WorkflowClicked(workflowToChangeTo);

            Assert.That(presenter.CurrentWorkflow.Type, Is.EqualTo(expectedValue));
        }

        [TestCase(WorkflowType.TrimNeeded, false, WorkflowType.TrimNeeded)]
        [TestCase(WorkflowType.NoCarfax, false, WorkflowType.CreateInitialAd)]
        [TestCase(WorkflowType.Offline, false, WorkflowType.Offline)]
        [TestCase(WorkflowType.NoCarfax, true, WorkflowType.AdReviewNeeded)]
        public void Changing_workflow_using_View_WorkflowClicked_should_call_Model_Save(WorkflowType workflowToChangeTo, bool isPreApproveOn, WorkflowType expectedValueSaved)
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(isPreApproveOn
                                                                  ? AutoApproveStatus.OnForAll
                                                                  : AutoApproveStatus.Off);

            new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            FiltersView.WorkflowClicked(workflowToChangeTo);

            FiltersModelMock.Verify(v => v.SaveWorkflow(expectedValueSaved, true));
        }

        [TestCase(WorkflowType.TrimNeeded, false)]
        [TestCase(WorkflowType.Offline, false)]
        [TestCase(WorkflowType.TrimNeeded, true)]
        [TestCase(WorkflowType.Offline, true)]
        public void AllInventory_and_Offline_should_render_as_AuxFilters_for_various_workflows(WorkflowType initialWorkflow, bool isPreApproveOn)
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(isPreApproveOn
                                                      ? AutoApproveStatus.OnForAll
                                                      : AutoApproveStatus.Off);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(initialWorkflow);
            
            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.AuxilaryFilters.Select(f => f.WorkflowType).ToArray(), Is.EqualTo(new[] { WorkflowType.AllInventory, WorkflowType.Offline }));
            Assert.That(savedVm.AuxilaryFilters.Select(f => f.Label).ToArray(), Is.EqualTo(new[] { "All Inventory", "Offline" }));
            Assert.That(savedVm.AuxilaryFilters.Select(f => f.Count).ToArray(), Is.EqualTo(new[] { 10, 15 }));
        }

        [Test]
        public void HighLevelFilters_should_be_correct_when_PreApprove_is_off()
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(AutoApproveStatus.Off);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.CreateInitialAd);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.HighLevelFilters.Select(f => f.WorkflowType).ToArray(), 
                Is.EqualTo(new[] { WorkflowType.NeedsApprovalAll, WorkflowType.NeedsPricingAll }));
            Assert.That(savedVm.HighLevelFilters.Select(f => f.Count).ToArray(), Is.EqualTo(new[] { 40, 60 }));
            Assert.That(savedVm.HighLevelFilters.Select(f => f.Label).ToArray(), Is.EqualTo(new[] { "Needs Approval", "Needs Pricing" }));
        }

        [Test]
        public void HighLevelFilters_should_be_correct_when_PreApprove_is_on()
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(AutoApproveStatus.OnForAll);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.CreateInitialAd);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.HighLevelFilters.Select(f => f.WorkflowType).ToArray(),
                Is.EqualTo(new[] { WorkflowType.NeedsAdReviewAll, WorkflowType.NeedsPricingAll }));
            Assert.That(savedVm.HighLevelFilters.Select(f => f.Count).ToArray(), Is.EqualTo(new[] { 50, 60 }));
            Assert.That(savedVm.HighLevelFilters.Select(f => f.Label).ToArray(), Is.EqualTo(new[] { "Needs Ad Review", "Needs Pricing" }));
        }

        [Test]
        public void Filters_under_NeedsApproval_should_be_correct_whe_PreApprove_is_off()
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(AutoApproveStatus.Off);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.CreateInitialAd);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            var detailedFilters = savedVm.HighLevelFilters[0].DetailedFilters;
            Assert.That(detailedFilters.Select(f => f.WorkflowType).ToArray(),
                Is.EqualTo(new[] { WorkflowType.NeedsApprovalAll, WorkflowType.CreateInitialAd, WorkflowType.TrimNeeded, WorkflowType.OutdatedPrice }));
            Assert.That(detailedFilters.Select(f => f.Count).ToArray(), Is.EqualTo(new[] { 40, 20, 25, 30 }));
            Assert.That(detailedFilters.Select(f => f.Label).ToArray(), Is.EqualTo(new[] { "All", "Ad Approval Needed", "No Trim", "Outdated Price in Ad" }));
        }

        [Test]
        public void Filters_under_NeedsAdReview_should_be_correct_when_PreApprove_is_on()
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(AutoApproveStatus.OnForAll);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.CreateInitialAd);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            var detailedFilters = savedVm.HighLevelFilters[0].DetailedFilters;
            Assert.That(detailedFilters.Select(f => f.WorkflowType).ToArray(),
                Is.EqualTo(new[] { WorkflowType.NeedsAdReviewAll, WorkflowType.TrimNeeded, WorkflowType.AdReviewNeeded }));
            Assert.That(detailedFilters.Select(f => f.Count).ToArray(), Is.EqualTo(new[] { 50, 25, 35 }));
            Assert.That(detailedFilters.Select(f => f.Label).ToArray(), Is.EqualTo(new[] { "All", "No Trim", "Ad" }));
        }

        [Test]
        public void Filters_under_NeedsPricing_should_be_correct()
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(AutoApproveStatus.OnForAll);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.CreateInitialAd);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            var detailedFilters = savedVm.HighLevelFilters[1].DetailedFilters;
            Assert.That(detailedFilters.Select(f => f.WorkflowType).ToArray(),
                Is.EqualTo(new[] { WorkflowType.NeedsPricingAll, /* WorkflowType.NoPrice, (Removed because isVisible: false) */ WorkflowType.DueForRepricing }));
            Assert.That(detailedFilters.Select(f => f.Count).ToArray(), Is.EqualTo(new[] { 60, 55 }));
            Assert.That(detailedFilters.Select(f => f.Label).ToArray(), Is.EqualTo(new[] { "All", "Due for Updating Price/Ad" }));
        }

        [TestCase(WorkflowType.TrimNeeded, false, WorkflowType.TrimNeeded)]
        [TestCase(WorkflowType.NoCarfax, false, WorkflowType.CreateInitialAd)]
        [TestCase(WorkflowType.Offline, false, WorkflowType.Offline)]
        [TestCase(WorkflowType.NoCarfax, true, WorkflowType.AdReviewNeeded)]
        public void ViewModel_CurrentWorkflow_should_be_the_same_as_the_presenters_CurrentWorkflow(
            WorkflowType initialWorkflow, bool isPreApproveOn, WorkflowType expectedResult)
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(
                isPreApproveOn ? AutoApproveStatus.OnForAll : AutoApproveStatus.Off);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(initialWorkflow);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(presenter.CurrentWorkflow.Type, Is.EqualTo(expectedResult));
            Assert.That(savedVm.CurrentWorkflow, Is.EqualTo(expectedResult));
        }

        [TestCase(WorkflowType.TrimNeeded, false, WorkflowType.NeedsApprovalAll)]
        [TestCase(WorkflowType.NoCarfax, false, WorkflowType.NeedsApprovalAll)]
        [TestCase(WorkflowType.Offline, false, null)]
        [TestCase(WorkflowType.NoCarfax, true, WorkflowType.NeedsAdReviewAll)]
        [TestCase(WorkflowType.TrimNeeded, true, WorkflowType.NeedsAdReviewAll)]
        [TestCase(WorkflowType.NeedsAdReviewAll, true, WorkflowType.NeedsAdReviewAll)]
        [TestCase(WorkflowType.AllInventory, true, null)]
        [TestCase(WorkflowType.DueForRepricing, true, WorkflowType.NeedsPricingAll)]
        public void ViewModel_CurrentHighLevelWorkflow_should_match_expected_value_given_specific_CurrentWorkflow(
            WorkflowType initialCurrentWorkflow, bool isPreApproveOn, WorkflowType? expectedHighLevelWorkflow)
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(
                isPreApproveOn ? AutoApproveStatus.OnForAll : AutoApproveStatus.Off);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(initialCurrentWorkflow);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();
            
            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.CurrentHighLevelWorkflow, Is.EqualTo(expectedHighLevelWorkflow));
        }

        [TestCase(WorkflowType.CreateInitialAd, false)]
        [TestCase(WorkflowType.AllInventory, true)]
        [TestCase(WorkflowType.Offline, true)]
        [TestCase(WorkflowType.LowAdQualityAll, false)]
        public void ViewModel_IsAuxiliaryFilterSelected_should_be_true_if_auxiliary_filter_selected(WorkflowType workflowToSet, bool expectedIsAuxFilterSelected)
        {
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(workflowToSet);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.IsAuxiliaryFilterSelected, Is.EqualTo(expectedIsAuxFilterSelected));
        }

        [Test]
        public void Presenter_should_execute_Model_Save_on_Initialization_with_click_set_to_false()
        {
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.NoPrice);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();
            presenter.GetViewModel();

            FiltersModelMock
                .Verify(m => m.SaveWorkflow(It.IsAny<WorkflowType?>(), false), Times.Once());
        }

        [Test]
        public void Presenter_should_call_Model_Save_on_WorkflowClicked()
        {
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.NoPrice);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();

            FiltersView.WorkflowClicked(WorkflowType.TrimNeeded);

            presenter.PreRender();

            FiltersModelMock
                .Verify(m => m.SaveWorkflow(WorkflowType.TrimNeeded, true), Times.Once());
        }

        [Test]
        public void Presenter_should_call_Model_Save_on_WorkflowClicked_even_if_the_workflow_didnt_change()
        {
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(WorkflowType.NoPrice);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();

            FiltersView.WorkflowClicked(WorkflowType.TrimNeeded);
            FiltersView.WorkflowClicked(WorkflowType.TrimNeeded);

            presenter.PreRender();

            FiltersModelMock
                .Verify(m => m.SaveWorkflow(WorkflowType.TrimNeeded, true), Times.Exactly(2));
        }

        [TestCase(WorkflowType.AllInventory, false, "All Inventory")]
        [TestCase(WorkflowType.Offline, true, "Offline")]
        [TestCase(WorkflowType.TrimNeeded, false, "Needs Approval,No Trim")]
        [TestCase(WorkflowType.TrimNeeded, true, "Needs Ad Review,No Trim")]
        [TestCase(WorkflowType.NeedsPricingAll, false, "Needs Pricing,All")]
        [TestCase(WorkflowType.NoPrice, false, "Needs Pricing,No Price")]
        [TestCase(WorkflowType.DueForRepricing, false, "Needs Pricing,Due for Updating Price/Ad")]
        public void CurrentWorkflowLabels_should_be_correct_on_ViewModel_according_to_current_workflow(WorkflowType currentWorkflow, bool isPreApproveOn, string expectedLabelsCsv)
        {
            FiltersModelMock.Setup(m => m.GetAutoApproveStatus()).Returns(
                isPreApproveOn ? AutoApproveStatus.OnForAll : AutoApproveStatus.Off);
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(currentWorkflow);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.CurrentWorkflowLabels, Is.EqualTo(expectedLabelsCsv.Split(',')));
        }

        [TestCase(WorkflowType.AllInventory, 10)]
        [TestCase(WorkflowType.NoPrice, 45)]
        [TestCase(WorkflowType.NeedsPricingAll, 60)]
        public void CurrentWorkflowCount_should_be_correct_on_ViewModel_according_to_current_workflow(WorkflowType currentWorkflow, int expectedCount)
        {
            FiltersModelMock.Setup(m => m.LoadWorkflow()).Returns(currentWorkflow);

            var presenter = new FiltersPresenter(FiltersView, FiltersModel, CreateBasicTemplate());

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.CurrentWorkflowCount, Is.EqualTo(expectedCount));
        }
    }
}