﻿using System;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Workflow.Aging
{
    [TestFixture]
    public class AgeBucketsTests
    {
        [Test]
        public void Constructor_should_throw_if_bucket_ranges_are_not_contiguous()
        {
            var buckets = new[]
                              {
                                  new AgeBucket(1, 0, 10),
                                  new AgeBucket(2, 11, 20),
                                  new AgeBucket(3, 22, null)
                              }.Cast<IAgeBucket>().ToArray();

            Assert.Throws<ArgumentException>(
                () => new AgeBuckets(buckets));
        }

        [Test]
        public void Constructor_should_throw_if_first_range_does_not_start_at_zero()
        {
            var buckets = new[]
                              {
                                  new AgeBucket(1, 5, 10),
                                  new AgeBucket(2, 11, 20),
                                  new AgeBucket(3, 21, null)
                              }.Cast<IAgeBucket>().ToArray();

            Assert.Throws<ArgumentException>(
                () => new AgeBuckets(buckets));
        }

        [Test]
        public void Constructor_should_throw_if_bucket_that_is_not_last_has_null_value_for_High()
        {
            var buckets = new[]
                              {
                                  new AgeBucket(1, 0, 10),
                                  new AgeBucket(2, 11, null),
                                  new AgeBucket(3, 21, null)
                              }.Cast<IAgeBucket>().ToArray();

            Assert.Throws<ArgumentException>(
                () => new AgeBuckets(buckets));
        }

        [Test]
        public void Constructor_should_append_additional_item_if_last_range_does_not_have_null_value_for_High()
        {
            var inputBuckets = new[]
                                   {
                                       new AgeBucket(1, 0, 10),
                                       new AgeBucket(2, 11, 20),
                                       new AgeBucket(3, 21, 30)
                                   }.Cast<IAgeBucket>().ToArray();

            var ageBuckets = new AgeBuckets(inputBuckets);

            var buckets = ageBuckets.GetBuckets();

            Assert.That(buckets.Select(b => b.Low).ToArray(), Is.EqualTo(new[]  {  0,  0, 11, 21, 31 }));
            Assert.That(buckets.Select(b => b.High).ToArray(), Is.EqualTo(new[] { 31, 10, 20, 30, (int?)null }));
            Assert.That(buckets.Select(b => b.Id).ToArray(), Is.EqualTo(new[] { (int?)null, 1, 2, 3, 4 }));
        }

        [Test]
        public void GetBuckets_should_return_list_of_buckets_in_ascending_order_including_initial_All_bucket()
        {
            var inputBuckets = new[]
                              {
                                  new AgeBucket(1, 0, 15),
                                  new AgeBucket(2, 16, 30),
                                  new AgeBucket(3, 31, 45),
                                  new AgeBucket(4, 46, null)
                              }.Cast<IAgeBucket>().ToArray();

            var ageBuckets = new AgeBuckets(inputBuckets);

            var buckets = ageBuckets.GetBuckets();

            Assert.That(buckets.Select(b => b.Low).ToArray(), Is.EqualTo(new[] { 0, 0, 16, 31, 46}));
            Assert.That(buckets.Select(b => b.High).ToArray(), Is.EqualTo(new[] { 46, 15, 30, 45, (int?)null }));
            Assert.That(buckets.Select(b => b.Id).ToArray(), Is.EqualTo(new[] {(int?)null, 1, 2, 3, 4}));
        }

        [Test]
        public void GetBuckets_should_return_initial_All_bucket_if_there_are_no_input_buckets()
        {
            var inputBuckets = new IAgeBucket[0];

            var ageBuckets = new AgeBuckets(inputBuckets);

            var buckets = ageBuckets.GetBuckets();

            Assert.That(buckets.Select(b => b.Low).ToArray(), Is.EqualTo(new[] { 0 }));
            Assert.That(buckets.Select(b => b.High).ToArray(), Is.EqualTo(new[] { 0 }));
            Assert.That(buckets.Select(b => b.Id).ToArray(), Is.EqualTo(new[] { (int?)null }));
        }

        [TestCase(3, true, 31, 45)]
        [TestCase(1, true, 0, 15)]
        [TestCase(4, true, 46, null)]
        [TestCase(0, false, 0, null)]
        [TestCase(5, false, 0, null)]
        [TestCase(null, true, 0, 46)]
        public void GetBucket_should_return_age_bucket_given_id(int? id, bool expectedFound, int expectedLow, int? expectedHigh)
        {
            var inputBuckets = new[]
                              {
                                  new AgeBucket(1, 0, 15),
                                  new AgeBucket(2, 16, 30),
                                  new AgeBucket(3, 31, 45),
                                  new AgeBucket(4, 46, null)
                              }.Cast<IAgeBucket>().ToArray();

            var ageBuckets = new AgeBuckets(inputBuckets);

            var bucket = ageBuckets.GetBucket(id);

            if(!expectedFound)
            {
                Assert.That(bucket, Is.Null);
            }
            else
            {
                Assert.That(bucket.Low, Is.EqualTo(expectedLow));
                Assert.That(bucket.High, Is.EqualTo(expectedHigh));
            }
        }

        [TestCase(-100, 1)]
        [TestCase(0, 1)]
        [TestCase(1, 1)]
        [TestCase(15, 1)]
        [TestCase(16, 2)]
        [TestCase(35, 3)]
        [TestCase(45, 3)]
        [TestCase(46, 4)]
        [TestCase(1000, 4)]
        public void GetBucketIdForAge_should_return_expected_bucket_id_given_age(int age, int? expectedBucketId)
        {
            var inputBuckets = new[]
                              {
                                  new AgeBucket(1, 0, 15),
                                  new AgeBucket(2, 16, 30),
                                  new AgeBucket(3, 31, 45),
                                  new AgeBucket(4, 46, null)
                              }.Cast<IAgeBucket>().ToArray();

            var ageBuckets = new AgeBuckets(inputBuckets);

            Assert.That(ageBuckets.GetBucketIdForAge(age), Is.EqualTo(expectedBucketId));
        }

        [Test]
        public void GetBucketIdForAge_should_return_null_if_no_buckets_are_present()
        {
            var inputBuckets = new IAgeBucket[0];

            var ageBuckets = new AgeBuckets(inputBuckets);

            Assert.That(ageBuckets.GetBucketIdForAge(5), Is.EqualTo(null));
        }
    }
}