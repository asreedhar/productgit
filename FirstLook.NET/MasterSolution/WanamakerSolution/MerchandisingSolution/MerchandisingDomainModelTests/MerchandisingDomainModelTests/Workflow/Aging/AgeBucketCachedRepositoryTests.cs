﻿using System.Collections;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Workflow.Aging
{
    [TestFixture]
    public class AgeBucketCachedRepositoryTests
    {
        protected IAgeBucketRepository Inner { get; set; }

        protected ICache Cache { get; set; }

        protected Mock<IAgeBucketRepository> InnerMock { get; set; }

        protected Mock<ICache> CacheMock { get; set; }

        protected IAgeBuckets Buckets { get; set; }

        protected Mock<IAgeBuckets> BucketsMock { get; set; }

        [SetUp]
        public void Setup()
        {
            CacheMock = new Mock<ICache>();
            InnerMock = new Mock<IAgeBucketRepository>();
            BucketsMock = new Mock<IAgeBuckets>();

            Cache = CacheMock.Object;
            Inner = InnerMock.Object;
            Buckets = BucketsMock.Object;
        }

        [Test]
        public void GetAgeBucketsForBusinessUnit_should_call_inner_when_cache_miss()
        {
            var repo = new AgeBucketCachedRepository(Cache, Inner);

            repo.GetBucketsForBusinessUnit(50);

            InnerMock.Verify(j => j.GetBucketsForBusinessUnit(50), Times.Once());
        }

        [Test]
        public void GetAgeBucketsForBusinessUnit_should_cache_inner_result_and_return_on_second_call()
        {
            var cacheVals = new Hashtable();
            CacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns<object>(k => cacheVals[k]);
            CacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((key, val, time) => cacheVals[key] = val);

            InnerMock.Setup(i => i.GetBucketsForBusinessUnit(It.IsAny<int>()))
                .Returns(Buckets);

            var repo = new AgeBucketCachedRepository(Cache, Inner);

            Assert.That(repo.GetBucketsForBusinessUnit(50), Is.SameAs(Buckets));
            Assert.That(repo.GetBucketsForBusinessUnit(50), Is.SameAs(Buckets));

            InnerMock.Verify(j => j.GetBucketsForBusinessUnit(50), Times.Once());
        }
         
        [Test]
        public void GetAgeBucketsForBusinessUnit_should_not_call_inner_on_cache_hit()
        {
            CacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns(Buckets);

            var repo = new AgeBucketCachedRepository(Cache, Inner);

            var buckets = repo.GetBucketsForBusinessUnit(50);

            Assert.That(buckets, Is.SameAs(Buckets));

            InnerMock.Verify(j => j.GetBucketsForBusinessUnit(50), Times.Never());
        }
    }
}