﻿using System.Linq;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace MerchandisingDomainModelTests.Workflow.Aging
{
    [TestFixture]
    public class AgeFilterPresenterTests
    {
        private static IAgeBucket CreateAgeBucket(int? id, int low, int? high)
        {
            var ageBucketMock = new Mock<IAgeBucket>();
            ageBucketMock.Setup(ab => ab.Id).Returns(id);
            ageBucketMock.Setup(ab => ab.Low).Returns(low);
            ageBucketMock.Setup(ab => ab.High).Returns(high);
            return ageBucketMock.Object;
        }

        [SetUp]
        public void Setup()
        {
            AgeFilterViewMock = new Mock<IAgeFilterView>();
            AgeFilterModelMock = new Mock<IAgeFilterModel>();
            AgeFilterView = AgeFilterViewMock.Object;
            AgeFilterModel = AgeFilterModelMock.Object;

            AgeFilterViewMock.SetupAllProperties();

            AgeBucketsMock = new Mock<IAgeBuckets>();
            AgeBuckets = AgeBucketsMock.Object;
            AgeBucketList = new List<IAgeBucket>();
            AgeBucketsMock.Setup(ab => ab.GetBuckets()).Returns(AgeBucketList);
            AgeBucketsMock.Setup(ab => ab.GetBucket(It.IsAny<int?>())).Returns<int?>(
                id => AgeBucketList.FirstOrDefault(b => b.Id == id));
            AgeFilterModelMock.Setup(m => m.GetAgeBuckets()).Returns(AgeBuckets);
        }

        protected List<IAgeBucket> AgeBucketList { get; set; }

        protected Mock<IAgeBuckets> AgeBucketsMock { get; set; }

        protected IAgeBuckets AgeBuckets { get; set; }

        protected IAgeFilterModel AgeFilterModel { get; set; }

        protected IAgeFilterView AgeFilterView { get; set; }

        protected Mock<IAgeFilterModel> AgeFilterModelMock { get; set; }

        protected Mock<IAgeFilterView> AgeFilterViewMock { get; set; }

        [Test]
        public void AgeBuckets_in_ViewModel_should_be_passed_back_from_Model()
        {
            AgeBucketList.AddRange(new[]
                                       {
                                           CreateAgeBucket(null, 0, 35),
                                           CreateAgeBucket(1, 0, 10),
                                           CreateAgeBucket(2, 11, 34),
                                           CreateAgeBucket(3, 35, null)
                                       });
            AgeFilterModelMock.Setup(m => m.GetAgeBuckets()).Returns(AgeBuckets);

            var presenter = new AgeFilterPresenter(AgeFilterView, AgeFilterModel);

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.AgeBuckets.Select(ab => ab.Id).ToArray(), Is.EqualTo(new[] { (int?)null, 1, 2, 3 }));
            Assert.That(savedVm.AgeBuckets.Select(ab => ab.Low).ToArray(), Is.EqualTo(new[] { 0, 0, 11, 35 }));
            Assert.That(savedVm.AgeBuckets.Select(ab => ab.High).ToArray(), Is.EqualTo(new[] { 35, 10, 34, (int?)null }));
        }

        [Test]
        public void CurrentAgeBucketId_should_default_to_null()
        {
            var presenter = new AgeFilterPresenter(AgeFilterView, AgeFilterModel);

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            Assert.That(presenter.CurrentAgeBucketId, Is.Null);
        }

        [TestCase(null, null)]
        [TestCase(1, 1)]
        [TestCase(2, 2)]
        [TestCase(10, null, Description = "CurrentAgeBucketId should be null if underlying model contains an invalid AgeBucketId")]
        public void CurrentAgeBucketId_should_be_pulled_and_validated_from_underlying_Model(int? initialModelAgeBucket, int? expectedCurrentAgeBucket)
        {
            AgeBucketList.AddRange(new[]
                                       {
                                           CreateAgeBucket(null, 0, 35),
                                           CreateAgeBucket(1, 0, 10),
                                           CreateAgeBucket(2, 11, 34),
                                           CreateAgeBucket(3, 35, null)
                                       });
            AgeFilterModelMock.Setup(m => m.LoadAgeBucket()).Returns(initialModelAgeBucket);

            var presenter = new AgeFilterPresenter(AgeFilterView, AgeFilterModel);

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            Assert.That(presenter.CurrentAgeBucketId, Is.EqualTo(expectedCurrentAgeBucket));
        }

        [TestCase(null, null)]
        [TestCase(2, 2)]
        [TestCase(10, null)]
        public void Changing_CurrentAgeBucketId_to_valid_value_should_set_value_but_not_trigger_Model_Save(int? valueToSet, int? expectedValue)
        {
            AgeBucketList.AddRange(new[]
                                       {
                                           CreateAgeBucket(null, 0, 35),
                                           CreateAgeBucket(1, 0, 10),
                                           CreateAgeBucket(2, 11, 34),
                                           CreateAgeBucket(3, 35, null)
                                       });
            AgeFilterModelMock.Setup(m => m.LoadAgeBucket()).Returns<int?>(null);

            var presenter = new AgeFilterPresenter(AgeFilterView, AgeFilterModel);

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            presenter.CurrentAgeBucketId = valueToSet;

            Assert.That(presenter.CurrentAgeBucketId, Is.EqualTo(expectedValue));

            AgeFilterModelMock.Verify(m => m.SaveAgeBucket(It.IsAny<int?>()), Times.Never());
        }

        [TestCase(null, null)]
        [TestCase(2, 2)]
        [TestCase(10, null)]
        public void Changing_CurrentAgeBucketId_using_View_AgeBucketClicked_should_set_value_and_trigger_Model_Save(int? valueToSet, int? expectedValue)
        {
            AgeBucketList.AddRange(new[]
                                       {
                                           CreateAgeBucket(null, 0, 35),
                                           CreateAgeBucket(1, 0, 10),
                                           CreateAgeBucket(2, 11, 34),
                                           CreateAgeBucket(3, 35, null)
                                       });
            AgeFilterModelMock.Setup(m => m.LoadAgeBucket()).Returns<int?>(null);

            var presenter = new AgeFilterPresenter(AgeFilterView, AgeFilterModel);

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            AgeFilterView.AgeBucketClicked(valueToSet);

            Assert.That(presenter.CurrentAgeBucketId, Is.EqualTo(expectedValue));

            AgeFilterModelMock.Verify(m => m.SaveAgeBucket(It.IsAny<int?>()), Times.Once());
        }

        [TestCase(null, null)]
        [TestCase(2, 2)]
        [TestCase(10, null)]
        public void CurrentAgeBucketId_should_be_correct_on_ViewModel_according_to_current_age_bucket_id(int? initialAgeBucketId, int? expectedAgeBucketId)
        {
            AgeBucketList.AddRange(new[]
                                       {
                                           CreateAgeBucket(null, 0, 35),
                                           CreateAgeBucket(1, 0, 10),
                                           CreateAgeBucket(2, 11, 34),
                                           CreateAgeBucket(3, 35, null)
                                       });
            AgeFilterModelMock.Setup(m => m.LoadAgeBucket()).Returns(initialAgeBucketId);

            var presenter = new AgeFilterPresenter(AgeFilterView, AgeFilterModel);

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            var savedVm = presenter.GetViewModel();

            Assert.That(savedVm.CurrentAgeBucketId, Is.EqualTo(expectedAgeBucketId));
        }

        [Test]
        public void CurrentAgeBucketId_should_be_correct_on_ViewModel_when_changed_twice()
        {
            AgeBucketList.AddRange(new[]
                                       {
                                           CreateAgeBucket(null, 0, 35),
                                           CreateAgeBucket(1, 0, 10),
                                           CreateAgeBucket(2, 11, 34),
                                           CreateAgeBucket(3, 35, null)
                                       });

            var presenter = new AgeFilterPresenter(AgeFilterView, AgeFilterModel);

            presenter.Initialize();
            presenter.Load();
            presenter.PreRender();

            presenter.CurrentAgeBucketId = 3;
            Assert.That(presenter.GetViewModel().CurrentAgeBucketId, Is.EqualTo(3));

            presenter.CurrentAgeBucketId = 1;
            Assert.That(presenter.GetViewModel().CurrentAgeBucketId, Is.EqualTo(1));
        }
    }
}