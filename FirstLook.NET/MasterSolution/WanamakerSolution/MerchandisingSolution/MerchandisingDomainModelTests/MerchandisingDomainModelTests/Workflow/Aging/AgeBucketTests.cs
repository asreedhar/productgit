﻿using System;
using FirstLook.Merchandising.DomainModel.Workflow.Aging;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Workflow.Aging
{
    [TestFixture]
    public class AgeBucketTests
    {
        [TestCase(null, 0, 65, "0-65+ Days")]
        [TestCase(null, 0, 0, "0+ Days")]
        [TestCase(1, 5, 5, "5 Days")]
        [TestCase(1, 0, 20, "0-20 Days")]
        [TestCase(1, 5, 25, "5-25 Days")]
        [TestCase(1, 30, 40, "30-40 Days")]
        [TestCase(1, 60, null, "60+ Days")]
        [TestCase(1, -5, 20, "", ExpectedException = typeof(ArgumentOutOfRangeException))]
        [TestCase(1, 20, 10, "", ExpectedException = typeof(ArgumentOutOfRangeException))]
        public void Description_should_return_expected_value_given_bucket_inputs(
            int? id, int low, int? high, string expectedDescription)
        {
            var ageBucket = new AgeBucket(id, low, high);

            Assert.That(ageBucket.Description, Is.EqualTo(expectedDescription));
        }

        [TestCase(1, 0, 20, "0-20 Days")]
        [TestCase(1, 5, 25, "5-25 Days")]
        [TestCase(1, 30, 40, "30-40 Days")]
        public void ToString_should_return_expected_value_given_bucket_inputs(
            int? id, int low, int? high, string expectedToString)
        {
            var ageBucket = new AgeBucket(id, low, high);

            Assert.That(ageBucket.ToString(), Is.EqualTo(expectedToString));
        }
    }
}