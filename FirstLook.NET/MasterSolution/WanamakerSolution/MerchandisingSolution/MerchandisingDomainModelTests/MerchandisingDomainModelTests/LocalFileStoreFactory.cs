﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Reflection;
using AmazonServices.S3;
using Core.Messaging;
using FirstLook.Common.Core;
using Merchandising.Messages;

namespace MerchandisingDomainModelTests
{
	public class LocalFileStoreFactory : IFileStoreFactory
	{
        public IFileStorage CreateFileStore()
        {
            return new LocalFileStore(prefix: "CreateFileStore");
        }
		
		public IFileStorage CreateFileStore(NameValueCollection headers)
		{
			return new LocalFileStore(prefix: "CreateFileStoreWithHeaders");
		}

		public IFileStorage CreatePdfFileStore(NameValueCollection headers)
		{
			return new LocalFileStore(prefix: "CreatePdfFileStore");
		}

		public IFileStorage CreateAutoLoadAudit()
		{
			return new LocalFileStore(prefix: "CreateAutoLoadAudit");
		}

		public IFileStorage WebLoaderApiPhotos()
		{
			return new LocalFileStore(prefix: "WebLoaderApiPhotos");
		}

		public IFileStorage DeadLetterArchive(NameValueCollection headers = null)
		{
			return new LocalFileStore(prefix: "CreateFileStore");
		}

		public IFileStorage MaxDocStorage(NameValueCollection headers = null, string contentType = "text/plain")
		{
			return new LocalFileStore(prefix: "MaxDocStorage");
		}

		public IFileStorage CreateWindowSticker()
		{
			return new LocalFileStore(prefix: "CreateWindowSticker");
		}

		public IFileStorage CreateBionicReportStorage()
		{
			return new LocalFileStore(prefix: "CreateBionicReportStorage");
		}

		public IFileStorage CreateBionicImageStorage()
		{
			return new LocalFileStore(prefix: "CreateBionicImageStorage");
		}

	    public IFileStorage CreateBionicTrackingStorage()
	    {
	        return new LocalFileStore(prefix: "CreateBionicTrackingStorage");
	    }
        
        public IFileStorage CreateFileStore(bool isAsync)
        {
            return new LocalFileStore(prefix: "CreateFileStore");
        }
    }

	public class LocalFileStore : IFileStorage
	{
		private readonly string _path;
		private readonly string _prefix;

		public LocalFileStore(string path = "", string prefix = "test-")
		{
			if(string.IsNullOrWhiteSpace(path))
				path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

			_path = path;

			_prefix = prefix;
		}

		public void Write(string fileName, Stream stream)
		{
			var filePath = Path.Combine(_path, Path.Combine(_prefix, fileName));
			using (var fileStream = File.Create(filePath))
			{
				stream.CopyTo(fileStream);
			}
		}

        public void Write(string fileName, Stream stream, string content)
        {
            Write(fileName, stream);
        }


	    public Stream Read(string fileName)
		{
			var filePath = Path.Combine(_path, Path.Combine(_prefix, fileName));
			Stream fs = File.OpenRead(filePath);
			fs.Seek(0, SeekOrigin.Begin);
			return fs;
		}

		public Stream Read(string fileName, DateTime? modifiedSince)
		{
			return Read(fileName);
		}

		public Stream Exists(string fileName, out bool exists)
		{
			var filePath = Path.Combine(_path, Path.Combine(_prefix, fileName));
			exists = File.Exists(filePath);
			return exists ? Read(fileName) : null;
		}

		public Stream Exists(string fileName, out bool exists, DateTime? modifiedSince)
		{
			return Exists(fileName, out exists);
		}

		public Stream ReadIfModified(string fileName, DateTime modifiedSince, out bool exists)
		{
			return Exists(fileName, out exists);
		}

		public bool ExistsNoStream(string fileName)
		{
			var filePath = Path.Combine(_path, Path.Combine(_prefix, fileName));
			return File.Exists(filePath);
		}

		public void Delete(string fileName)
		{
			var filePath = Path.Combine(_path, Path.Combine(_prefix, fileName));
			File.Delete(filePath);
		}

		public string ContainerName()
		{
			return _path;
		}

	    public IAsyncResult BeginWrite(string fileName, Stream stream, AsyncCallback callback)
	    {
	        Write(fileName, stream);
            
            var result = new AsyncResultNoResult(callback, null);
            result.SetAsCompleted(null, true);

	        return result;
	    }

        public void LogEndWrite(IAsyncResult asyncResult)
        {}

	    public HttpStatusCode EndWrite(IAsyncResult asyncResult)
	    {
	        return HttpStatusCode.OK;
	    }
    }
}