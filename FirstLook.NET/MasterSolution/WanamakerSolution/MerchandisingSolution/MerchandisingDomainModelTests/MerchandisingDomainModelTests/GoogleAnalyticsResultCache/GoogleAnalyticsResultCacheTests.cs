﻿using System;
using FirstLook.Merchandising.DomainModel.Dashboard.Abstract;
using Moq;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics.Internal;
using Merchandising.Messages;
using Core.Messaging;
using FirstLook.Common.Core;
using System.Linq;
using FirstLook.Common.Core.Extensions;

namespace MerchandisingDomainModelTests.GoogleAnalyticsResultCacheTest
{
    [TestFixture]
    public class GoogleAnalyticsResultCacheTest
    {
        [Test]
        public void TestTest()
        {
            Assert.That(true, Is.True);
        }

        [Test]
        public void SL_Fetch_Is_Null_When_No_Data()
        {
            var badBusinessUnitId = 1;
            var gQuery = new Mock<IGoogleAnalyticsQuery>();
            var gRepo = new Mock<IGoogleAnalyticsRepository>();
            var gAuth = new Mock<IGoogleAnalyticsWebAuthorization>();
            var brep = new Mock<IDashboardBusinessUnitRepository>();

            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();
            fileStoreFactory.Setup(x => x.CreateFileStore()).Returns(fileStore.Object);

            var resultCache = new GoogleAnalyticsResultCache(gQuery.Object, gRepo.Object, gAuth.Object, fileStoreFactory.Object, brep.Object);
            var result = resultCache.FetchRangedGoogleData(badBusinessUnitId, GoogleAnalyticsType.PC, DateRange.MonthOfDate(DateTime.Now));

            Assert.That(result, Is.Null);
        }

        [Test]
        public void SL_Fetch_Gets_Null()
        {
            // This is a bad profile, so we should not get any results.
            var goodBusinessUnitId = 101590;
            var gQuery = new Mock<IGoogleAnalyticsQuery>();
            var gRepo = new Mock<IGoogleAnalyticsRepository>();
            var gAuth = new Mock<IGoogleAnalyticsWebAuthorization>();
            var brep = new Mock<IDashboardBusinessUnitRepository>();

            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();

            
            gAuth.Setup( ga => ga.IsAuthorized(It.IsAny<int>())).Returns(true);
            gQuery.Setup( gq => gq.GetProfiles()).Returns(new [] { new ProfileResult(123, "FakeProfile") });
            gQuery.Setup( gq => gq.Init(goodBusinessUnitId));
            

            fileStoreFactory.Setup(x => x.CreateFileStore()).Returns(fileStore.Object);

            var resultCache = new GoogleAnalyticsResultCache(gQuery.Object, gRepo.Object, gAuth.Object, fileStoreFactory.Object, brep.Object);
            var result = resultCache.FetchRangedGoogleData(goodBusinessUnitId, GoogleAnalyticsType.PC, DateRange.MonthOfDate(DateTime.Now));

            Assert.That(result, Is.Null);
        }

        [Test]
        public void SL_FetchUngrouped_Is_Empty_When_No_Data()
        {
            var badBusinessUnitId = 1;
            var gQuery = new Mock<IGoogleAnalyticsQuery>();
            var gRepo = new Mock<IGoogleAnalyticsRepository>();
            var gAuth = new Mock<IGoogleAnalyticsWebAuthorization>();
            var brep = new Mock<IDashboardBusinessUnitRepository>();

            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();
            fileStoreFactory.Setup(x => x.CreateFileStore()).Returns(fileStore.Object);

            var resultCache = new GoogleAnalyticsResultCache(gQuery.Object, gRepo.Object, gAuth.Object, fileStoreFactory.Object, brep.Object);
            var result = resultCache.FetchRangedGoogleDataUngrouped(badBusinessUnitId, GoogleAnalyticsType.PC, DateRange.MonthOfDate(DateTime.Now));

            Assert.That(result, Is.Empty);
        }
    }
}