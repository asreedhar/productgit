﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Pricing.DiscountCampaigns;
using FirstLook.Merchandising.DomainModel.Templating;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using MAX.Entities;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.InventorySearchTests
{
    [TestFixture]
    public class InventorySearchTests
    {
        private IList<IInventoryData> _inventoryList;
        private InventorySearch _search;

        [SetUp]
        public void Setup()
        {
            _inventoryList = FakeInventoryDatas();

            var mockISearchRepo = new Mock<INewCarPricingRepository>();
            mockISearchRepo.Setup(x => x.GetNewCarPricingInventory(It.IsAny<int>())).Returns(_inventoryList);
            
            _search = new InventorySearch(mockISearchRepo.Object);

        }

        [TestCase("All")]
        public void Search_Make(string yearList)
        {
            _search.Initialize(1, VehicleType.New);

            // Given a list of years
            // model returns list of makes with the dealership default selected
            // sort alphabetically
            var years = new List<string>();
            years.AddRange(yearList.Split(','));

            var searchMakes = _search.ByCampaignFilter(new CampaignFilter{Year = years}).ToList();

            var makeList = searchMakes.Select(x => x.Make).Distinct().ToList();

            Assert.That(makeList.Contains("Make-1"));
            Assert.That(makeList.Contains("Make-2"));
            Assert.That(makeList.Contains("Make-3"));

            Assert.That(makeList.Count == 3);
        }

        [TestCase("2013,2012")]
        public void Search_Make_Multiple_Years(string yearList)
        {
            _search.Initialize(1, VehicleType.New);

            // Given a list of years
            // model returns list of makes with the dealership default selected
            // sort alphabetically
            var years = new List<string>();
            years.AddRange(yearList.Split(','));

            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).ToList();
            var makeList = searchMakes.Select(x => x.Make).Distinct().ToList();

            Assert.That(searchMakes.Count == 2);
            Assert.That(makeList.Contains("Make-1"));
            Assert.That(makeList.Contains("Make-2"));
        }

        [TestCase("2013")]
        public void Search_Make_One_Year(string yearList)
        {
            _search.Initialize(1, VehicleType.New);

            // Given a list of years
            // model returns list of makes with the dealership default selected
            // sort alphabetically
            var years = new List<string>();
            years.AddRange(yearList.Split(','));

            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).ToList();
            var makeList = searchMakes.Select(x => x.Make).Distinct().ToList();

            Assert.That(searchMakes.Count == 1);
            Assert.That(makeList.Contains("Make-1"));
        }

        [Test]
        public void Search_Models()
        {
            // Given a list of Years and Makes
            // Return a list of available models.
            // List is empty if multiple makes are provided
            _search.Initialize(1, VehicleType.New);

            var years = new List<string> {CampaignFilter.Allyears};
            var makes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();

            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = makes }).ToList();

            var models = searchModels.Select(x => x.Model).Distinct().ToList();

            Assert.That(models.Count == 3);
            Assert.That(models.Contains("Model-1"));
            Assert.That(models.Contains("Model-2"));
            Assert.That(models.Contains("Model-3"));
        }

        [TestCase("Trim-1")]
        [TestCase("Trim-2")]
        [TestCase("Trim-1,afterMarketTrim-3 ")]
        [TestCase("Trim-1,BASE")]
        public void Search_Trims(string splitTrim)
        {
            // given one model
            // return all trims

            var trimList = splitTrim.ToEnumerable().ToList();

            _search.Initialize(1, VehicleType.New);

            var years = new List<string> { CampaignFilter.Allyears };
            var searchMakesList = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModelsList = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakesList }).Select(x => x.Model).Distinct().ToList();
            
            var foundTrims = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakesList, Model = searchModelsList, Trim = trimList}).Distinct().ToList();

            Assert.That(foundTrims.Count == trimList.Count);
        }

        [TestCase("body style 1")]
        [TestCase("body style 1,body style 2")]
        public void Search_Matches_BodyStyles(string bodyStyleStrings)
        {
            _search.Initialize(1, VehicleType.New);

            var years = new List<string> { CampaignFilter.Allyears };
            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes }).Select(x => x.Model).Distinct().ToList();
            var searchTrims = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels }).Select(x => x.Trim).Distinct().ToList();

            
            var bodyStyle = bodyStyleStrings.ToEnumerable();

            var enumerable = bodyStyle as List<string> ?? bodyStyle.ToList();
            
            var searchBodys = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, BodyStyle = enumerable }).ToList();

            Assert.That(searchBodys.Count == enumerable.Count());
        }

        [Test]
        public void Search_Ignores_Null_ListObjects()
        {
            _search.Initialize(1, VehicleType.New);

            var years = _search.ByCampaignFilter(new CampaignFilter {Year = new List<string> {CampaignFilter.Allyears}}).Select(x => x.Year).Distinct().ToList();
            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes }).Select(x => x.Model).Distinct().ToList();
            var searchTrims = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels }).Select(
                    x => (!String.IsNullOrEmpty(x.AfterMarketTrim)) ? x.AfterMarketTrim.FirstCharacterToUpper().Trim() : x.Trim.FirstCharacterToUpper().Trim())
                        .Distinct(StringComparer.InvariantCultureIgnoreCase)
                        .Where(x => !String.IsNullOrWhiteSpace(x.Trim()))
                        .ToList();

            var searchBodyStyles =
                _search.ByCampaignFilter(new CampaignFilter {Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims})
                    .Select(x => x.BodyStyleName)
                    .Distinct()
                    .ToList();

            var engineVehicles =
                _search.ByCampaignFilter(new CampaignFilter {Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, BodyStyle = searchBodyStyles});
            
            var searchEngineDescriptions = GetEngineDescriptions(engineVehicles);

            /*****
             * Simple tests, just add null to the list
             *****/
            var testYears = years.ToList();
            testYears.Add(null);
            var assertYears = _search.ByCampaignFilter(new CampaignFilter { Year = testYears }).Count();
            Assert.That(assertYears == years.Count);

            var testMakes = searchMakes.ToList();
            testMakes.Add(null);
            var assertMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = testMakes }).Select(x => x.Make).Distinct().Count();
            Assert.That(assertMakes == searchMakes.Count);

            var testModels = searchModels.ToList();
            testModels.Add(null);
            var assertModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = testModels}).Select(x => x.Model).Distinct().Count();
            Assert.That(assertModels == searchModels.Count);

            var testTrims = searchTrims.ToList();
            testTrims.Add(null);
            var assertTrims = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = testTrims }).Select(
                x => (!String.IsNullOrEmpty(x.AfterMarketTrim)) ? x.AfterMarketTrim.FirstCharacterToUpper().Trim() : x.Trim.FirstCharacterToUpper().Trim())
                .Distinct(StringComparer.InvariantCultureIgnoreCase).Count(x => !String.IsNullOrWhiteSpace(x.Trim()));
            Assert.That(assertTrims == searchTrims.Count);

            var testBodys = searchBodyStyles.ToList();
            testBodys.Add(null);
            var assertBodys = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, BodyStyle = testBodys}).Select(x => x.BodyStyleName).Distinct().Count();
            Assert.That(assertBodys == searchBodyStyles.Count);

            var testOldEngines = searchEngineDescriptions.Values.ToList();
            testOldEngines.Add(null);
            var assertEngines = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, BodyStyle = searchBodyStyles, EngineDescription = testOldEngines}).Select(x => x.EngineDescription).Distinct().Count();
            Assert.That(assertEngines == searchEngineDescriptions.Count);


            /*****
             * New engine descriptions require a little more fiddling to mimic the way we get data to/from the front-end
             *****/
            var testNewEngines = searchEngineDescriptions.Select(engDesc => new CampaignEngineDescriptionInfo
            {
                IsStandard = (engDesc.Key.Equals("maxchromestandard")),
                OptionCode = (engDesc.Key.Equals("maxchromestandard")) ? "" : engDesc.Key,

                Description = engDesc.Value
            }).ToList();
            testNewEngines.Add( null );
            var assertNewEngines = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, BodyStyle = searchBodyStyles, CampaignEngineDescription = testNewEngines }).Select(x => x.EngineDescription).Distinct().Count();
            Assert.That(assertNewEngines == searchEngineDescriptions.Count);
        }

        

        [Test]
        public void Search_Matches_Old_EngineDescriptions()
        {
            _search.Initialize(1, VehicleType.New);

            var years = new List<string> { CampaignFilter.Allyears };
            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes }).Select(x => x.Model).Distinct().ToList();
            var searchTrims = _search
                .ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels })
                .Select(x =>
                    (String.IsNullOrEmpty(x.AfterMarketTrim)) 
                        ? x.Trim 
                        : x.AfterMarketTrim)
                        .Distinct()
                        .ToList();

            const string engineDescriptions = "Engine Option1";

            var engineDesc = engineDescriptions.ToEnumerable();

            var engineDescArray = engineDesc as string[] ?? engineDesc.ToArray();

            var searchBodys = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, EngineDescription = engineDescArray }).ToList();

            Assert.That(searchBodys.Count == 3);
        }

        [Test]
        public void Search_Matches_New_EngineDescriptions()
        {
            _search.Initialize(1, VehicleType.New);

            var years = new List<string> { CampaignFilter.Allyears };
            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes }).Select(x => x.Model).Distinct().ToList();
            var searchTrims = _search
                .ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels })
                .Select(x =>
                    (String.IsNullOrEmpty(x.AfterMarketTrim))
                        ? x.Trim
                        : x.AfterMarketTrim)
                        .Distinct()
                        .ToList();

            var edl = new List<CampaignEngineDescriptionInfo>
            {
                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option1",
                    IsStandard = true
                },
                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option2",
                    IsStandard = false,
                    OptionCode = "eo2"
                }
            };

            var searchEngines = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, CampaignEngineDescription = edl}).ToList();

            // Only match 5 because one of these vehicles has no trim at all
            Assert.That(searchEngines.Count == 5);
        }

        [Test]
        public void Search_Doesnt_Choke_On_Null_New_EngineDescriptions()
        {
            _search.Initialize(1, VehicleType.New);

            var years = new List<string> { CampaignFilter.Allyears };
            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes }).Select(x => x.Model).Distinct().ToList();
            var searchTrims = _search
                .ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels })
                .Select(x =>
                    (String.IsNullOrEmpty(x.AfterMarketTrim))
                        ? x.Trim
                        : x.AfterMarketTrim)
                        .Distinct()
                        .ToList();

            var edl = new List<CampaignEngineDescriptionInfo>
            {
                null,
                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option1",
                    IsStandard = true
                },
                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option2",
                    IsStandard = false,
                    OptionCode = "eo2"
                }
            };

            var searchEngines = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, CampaignEngineDescription = edl }).ToList();

            // Only match 5 because one of these vehicles has no trim at all
            Assert.That(searchEngines.Count == 5);
        }


        [Test]
        public void Search_Does_Not_Match_Bad_EngineDescriptions()
        {
            _search.Initialize(1, VehicleType.New);

            var years = new List<string> { CampaignFilter.Allyears };
            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes }).Select(x => x.Model).Distinct().ToList();
            var searchTrims = _search
                .ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels })
                .Select(x =>
                    (String.IsNullOrEmpty(x.AfterMarketTrim))
                        ? x.Trim
                        : x.AfterMarketTrim)
                        .Distinct()
                        .ToList();

            var edl = new List<CampaignEngineDescriptionInfo>
            {
                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option1",
                    IsStandard = false
                },
                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option3",
                    IsStandard = false,
                    OptionCode = "eo3"
                }
            };

            var searchEngines = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, CampaignEngineDescription = edl }).ToList();

            Assert.That(searchEngines.Count == 0);
        }

        [Test]
        public void Search_Matches_Only_Good_Descriptions()
        {
            _search.Initialize(1, VehicleType.New);

            var years = new List<string> { CampaignFilter.Allyears };
            var searchMakes = _search.ByCampaignFilter(new CampaignFilter { Year = years }).Select(x => x.Make).Distinct().ToList();
            var searchModels = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes }).Select(x => x.Model).Distinct().ToList();
            var searchTrims = _search
                .ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels })
                .Select(x =>
                    (String.IsNullOrEmpty(x.AfterMarketTrim))
                        ? x.Trim
                        : x.AfterMarketTrim)
                        .Distinct()
                        .ToList();

            var edl = new List<CampaignEngineDescriptionInfo>
            {
                // This is bad, there is no inventory with "Engine Option1" that is not standard
                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option1",
                    IsStandard = false
                },

                new CampaignEngineDescriptionInfo
                {
                    Description = "Engine Option2",
                    IsStandard = false,
                    OptionCode = "eo2"
                }
            };

            var searchEngines = _search.ByCampaignFilter(new CampaignFilter { Year = years, Make = searchMakes, Model = searchModels, Trim = searchTrims, CampaignEngineDescription = edl }).ToList();

            Assert.That(searchEngines.Count == 2);
        }

		/// <summary>
		/// FB Case 30646 turned up vehicles that were in a completely wrong campaign. Dodge vehicles in a Jeep campaign. The only
		/// thing the campaign and the vehicles had in common were the year and the fact that the campaign was not trim specific.
		/// This test will prove that was not the cause of the mismatch.
		/// </summary>
	    [Test]
	    public void Search_Does_Not_Match_On_Year_And_Trim()
		{

			var inventoryList = FakeInventoryDataOneYearAllTrims();

			var mockISearchRepo = new Mock<INewCarPricingRepository>();
			mockISearchRepo.Setup(x => x.GetNewCarPricingInventory(It.IsAny<int>())).Returns(inventoryList);

			var search = new InventorySearch(mockISearchRepo.Object);

			search.Initialize(1, VehicleType.New);

			// One year - 2014
			// One make - Make 1
			// One model - Model 1
			// No trim.
			// Should match only one vehicle.
			var campaignFilter = new CampaignFilter
			{
				Year = new List<string> {"2014"},
				Make = new List<string> {"Make-1"},
				Model = new List<string> {"Model-1"}
			};
			var results = search.ByCampaignFilter(campaignFilter);
			Assert.That(results.Count() == 1);
		}

        private static List<IInventoryData> FakeInventoryDatas()
        {
            return new List<IInventoryData>
            {
                new InventoryData("stock-11", 1, "vin-1", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2013", "Make-1", "Model-1", "MarketClass-1", 1, "Trim-1", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 1","Engine Option1", null, true),
                new InventoryData("stock-21", 1, "vin-2", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2012", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-2", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 2","Engine Option1", null, true),
                new InventoryData("stock-31", 1, "vin-3", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2011", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-3", "afterMarketTrim-3", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 3","Engine Option1", null, true),
                new InventoryData("stock-12", 1, "vin-4", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2010", "Make-1", "Model-2", "MarketClass-1", 1, "Trim-4", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 4","Engine Option2", "eo2"),
                new InventoryData("stock-22", 1, "vin-5", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2009", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-5", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 5","Engine Option2", "eo2"),
                new InventoryData("stock-32", 1, "vin-6", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2008", "Make-3", "Model-3", "MarketClass-3", 1, ""      , ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 6","Engine Option2", "eo2")
            };
        }


		private static List<IInventoryData> FakeInventoryDataOneYearAllTrims()
		{
			return new List<IInventoryData>
            {
                new InventoryData("stock-11", 1, "vin-1", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2014", "Make-1", "Model-1", "MarketClass-1", 1, "Trim-1", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 1","Engine Option1", null, true),
                new InventoryData("stock-21", 1, "vin-2", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2014", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-2", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 2","Engine Option1", null, true),
                new InventoryData("stock-31", 1, "vin-3", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2014", "Make-3", "Model-3", "MarketClass-3", 1, "Trim-3", "afterMarketTrim-3", 1, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 3","Engine Option1", null, true),
                new InventoryData("stock-12", 1, "vin-4", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2014", "Make-1", "Model-2", "MarketClass-1", 1, "Trim-4", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-1", "extCode-1", "extDesc-1", "ext2Code-1", "ext2Desc-1", "intCode-1", "intDesc-1", 1, null, "vehicleLocation-1", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 4","Engine Option2", "eo2"),
                new InventoryData("stock-22", 1, "vin-5", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2014", "Make-2", "Model-2", "MarketClass-2", 1, "Trim-5", ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-2", "extCode-2", "extDesc-2", "ext2Code-2", "ext2Desc-2", "intCode-2", "intDesc-2", 1, null, "vehicleLocation-2", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 5","Engine Option2", "eo2"),
                new InventoryData("stock-32", 1, "vin-6", 1, false, "", null, 1, 1, (decimal) 1.0, DateTime.Now.AddDays(-9), "2014", "Make-3", "Model-3", "MarketClass-3", 1, ""      , ""                 , 1, false, 1, 1, 1, 1, 1, 1, "baseColor-3", "extCode-3", "extDesc-3", "ext2Code-3", "ext2Desc-3", "intCode-3", "intDesc-3", 1, null, "vehicleLocation-3", 1, DateTime.Now.AddDays(-7), null, null, 1, 1, 1, false, null, false, false, null, null, false,"body style 6","Engine Option2", "eo2")
            };
		}


        private static Dictionary<string, string> GetEngineDescriptions(IEnumerable<IInventoryData> engines)
        {
            var inventoryDatas = engines as IInventoryData[] ?? engines.ToArray();
            var enginedescriptions = inventoryDatas
                    .Where(x => !string.IsNullOrEmpty(x.EngineDescription))
                    .Select(x => x.EngineDescription.Trim())
                    .Distinct(StringComparer.CurrentCultureIgnoreCase)
                    .ToList();

            enginedescriptions.Sort();

            var optionsList = inventoryDatas
                .Where(x => !string.IsNullOrEmpty(x.EngineDescription))
                .Select(x => new { Key = (x.IsStandardEngine) ? "maxchromestandard" : x.EngineOptionCode.Trim(), Value = x.EngineDescription.Trim() })
                .ToList();
            var options = new Dictionary<string, string>();
            foreach (var item in optionsList.Where(item => !options.ContainsKey(item.Key)))
                options.Add(item.Key, item.Value);

            return options;
        }
    }
}