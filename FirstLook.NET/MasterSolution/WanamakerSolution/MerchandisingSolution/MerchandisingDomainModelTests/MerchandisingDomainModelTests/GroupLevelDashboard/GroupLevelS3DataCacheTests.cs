﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.GroupLevelDashboard;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.GroupLevelDashboard
{
    [TestFixture]
    public class GroupLevelS3DataCacheTests
    {
        [Test]
        public void TestSetCachedState()
        {
            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();
            fileStoreFactory.Setup(x => x.CreateFileStore(false)).Returns(fileStore.Object);

            fileStore.Setup(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>())).Callback(
                (string file, Stream stream) =>
                {
                    Assert.IsTrue(file == "GroupLevelCache/12/Current/State");
                    using (var reader = new StreamReader(stream))
                    {
                        string text = reader.ReadToEnd();
                        var status = CachedStatus.FromString(text);
                        Assert.IsTrue(status.State == State.Cached);
                        Assert.IsTrue(status.PercentComplete == 1);
                    }
                });

            var cache = new GroupLevelS3DataCache(fileStoreFactory.Object);
            cache.SetCachedState(12, State.Cached);

            fileStore.Verify(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>()), Times.Once());
        }

        [Test]
        public void TestPendingStateKeepsTimeStamp()
        {
            DateTime time = DateTime.UtcNow;
            State state = State.Cached;

            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();
            fileStoreFactory.Setup(x => x.CreateFileStore(false)).Returns(fileStore.Object);

            bool exists = true;
            fileStore.Setup(x => x.Exists(It.IsAny<string>(), out exists)).Returns((string file, bool test) =>
            {
                Assert.IsTrue(file == "GroupLevelCache/100/Current/State");

                MemoryStream stream = new MemoryStream();
                var writer = new StreamWriter(stream);

                string value = string.Format(@"{0};{1}", state, time.ToString("yyyy-MM-ddTHH:mm:ssZ"));
                writer.WriteLine(value);

                writer.Flush();
                stream.Position = 0;
                return stream;
            });

            fileStore.Setup(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>())).Callback(
              (string file, Stream stream) =>
              {
                  Assert.IsTrue(file == "GroupLevelCache/100/Current/State");
                  using (var reader = new StreamReader(stream))
                  {
                      string text = reader.ReadToEnd();
                      var status = CachedStatus.FromString(text);
                      Assert.IsTrue(status.State == State.Pending);
                      
                      Assert.IsTrue(status.TimeStamp.Kind == time.Kind);
                      Assert.IsTrue(status.TimeStamp.Hour == time.Hour);
                      Assert.IsTrue(status.TimeStamp.Minute == time.Minute);
                      Assert.IsTrue(status.TimeStamp.Second == time.Second);
                      Assert.IsTrue(status.TimeStamp.Day == time.Day);
                      Assert.IsTrue(status.TimeStamp.Month == time.Month);
                      Assert.IsTrue(status.TimeStamp.Year == time.Year);
                  }
              });

            var cache = new GroupLevelS3DataCache(fileStoreFactory.Object);
            cache.SetCachedState(100, State.Pending);

            fileStore.Verify(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>()), Times.Once());
        }

        [Test]
        public void TestGetCachedStatus()
        {
            DateTime time = DateTime.UtcNow;
            State state = State.Pending;

            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();
            fileStoreFactory.Setup(x => x.CreateFileStore(false)).Returns(fileStore.Object);
            
            bool exists = true;
            fileStore.Setup(x => x.Exists(It.IsAny<string>(), out exists)).Returns((string file, bool test) =>
            {
                Assert.IsTrue(file == "GroupLevelCache/100/Current/State");

                MemoryStream stream = new MemoryStream();
                var writer = new StreamWriter(stream);
                
                string value = string.Format(@"{0};{1}", state, time.ToString("yyyy-MM-ddTHH:mm:ssZ"));
                writer.WriteLine(value);
                
                writer.Flush();
                stream.Position = 0;
                return stream;
            });

            var cache = new GroupLevelS3DataCache(fileStoreFactory.Object);
            var cachedStatus = cache.GetCachedStatus(100);

            Assert.IsTrue(cachedStatus.TimeStamp.Kind == time.Kind);
            Assert.IsTrue(cachedStatus.TimeStamp.Hour == time.Hour);
            Assert.IsTrue(cachedStatus.TimeStamp.Minute == time.Minute);
            Assert.IsTrue(cachedStatus.TimeStamp.Second == time.Second);
            Assert.IsTrue(cachedStatus.TimeStamp.Day == time.Day);
            Assert.IsTrue(cachedStatus.TimeStamp.Month == time.Month);
            Assert.IsTrue(cachedStatus.TimeStamp.Year == time.Year);

            Assert.IsTrue(cachedStatus.State == state);
            fileStore.Verify(x => x.Exists(It.IsAny<string>(), out exists), Times.Once());
        }

        [Test]
        public void TestWriteDashBoardData()
        {
            string data = "{data:23}";
            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();

            string[] paths = new []
                                 {
                                     "GroupLevelCache/34/Current/DashBoardData-Used",
                                     string.Format("GroupLevelCache/34/{0}/DashBoardData-Used", DateTime.Now.ToString("yyyy-MM-dd"))
                                 };

            fileStore.Setup(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>())).Callback(
                (string file, Stream stream) =>
                    {
                        Assert.IsTrue(paths.Contains(file));
                        using (var reader = new StreamReader(stream))
                        {
                            string text = reader.ReadToEnd();
                            Assert.IsTrue(text == data);
                        }
                    });

            fileStoreFactory.Setup(x => x.CreateFileStore(false)).Returns(fileStore.Object);
            var cache = new GroupLevelS3DataCache(fileStoreFactory.Object);

            cache.WriteDashboardData(34, "Used", data);
            fileStore.Verify(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>()), Times.Exactly(2));
        }

        [Test]
        public void TestWriteSitePerformanceData()
        {
            string data = "{data:23}";
            var fileStoreFactory = new Mock<IFileStoreFactory>();
            var fileStore = new Mock<IFileStorage>();

            string[] paths = new string[]
                                 {
                                     "GroupLevelCache/34/Current/SitePerformance-2012-12-Used",
                                     string.Format("GroupLevelCache/34/{0}/SitePerformance-2012-12-Used", DateTime.Now.ToString("yyyy-MM-dd"))
                                 };

            fileStore.Setup(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>())).Callback(
                (string file, Stream stream) =>
                {
                    Assert.IsTrue(paths.Contains(file));
                    using (var reader = new StreamReader(stream))
                    {
                        string text = reader.ReadToEnd();
                        Assert.IsTrue(text == data);
                    }
                });

            fileStoreFactory.Setup(x => x.CreateFileStore(false)).Returns(fileStore.Object);
            var cache = new GroupLevelS3DataCache(fileStoreFactory.Object);

            cache.WriteSitePerformanceData(34, "Used", 12, 2012, data);
            fileStore.Verify(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>()), Times.Exactly(2));
        }
    }
}
