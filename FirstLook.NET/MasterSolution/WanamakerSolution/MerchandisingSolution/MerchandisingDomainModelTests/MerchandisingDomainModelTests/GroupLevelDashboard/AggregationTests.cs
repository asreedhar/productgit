﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FirstLook.Merchandising.DomainModel.GoogleAnalytics;
using FirstLook.Merchandising.DomainModel.Reports;
using FirstLook.Merchandising.DomainModel.Reports.DashboardHelpers;
using FirstLook.Merchandising.DomainModel.Reports.DashboardViewModels;
using NUnit.Framework;
using System.Web.Script.Serialization;
using FirstLook.Merchandising.DomainModel.Reports;

namespace MerchandisingDomainModelTests.GroupLevelDashboard
{
    [TestFixture]
    public class AggregationTests
    {

        [Test]
        public void TestGoogleAggregation()
        {
            var files = new[]
                                 {
                                     "MerchandisingDomainModelTests.Files.googleAnalytics.txt",
                                     "MerchandisingDomainModelTests.Files.googleAnalytics.txt"
                                 };

            var selectedMonths = new List<GoogleAnalyticsResult[]>();
            foreach (var file in files)
            {
                string value;
                using (TextReader textReader = new StreamReader(typeof(AggregationTests).Assembly.GetManifestResourceStream(file)))
                    value = textReader.ReadToEnd();

                var data = new JavaScriptSerializer().Deserialize(value, typeof(GoogleAnalyticsResult[]));
                selectedMonths.Add(data as GoogleAnalyticsResult[]);
            }

            var sum = selectedMonths.Summation<GoogleAnalyticsResult>();

            Assert.IsTrue(sum.Length == 3);
            Assert.IsTrue(sum[0].VDP.New == selectedMonths[0][0].VDP.New * 2);
            Assert.IsTrue(sum[0].VDP.Used == selectedMonths[0][0].VDP.Used * 2);
            Assert.IsTrue(sum[0].VDP.Both == selectedMonths[0][0].VDP.Both * 2);

            var googleOrganic = selectedMonths[0][0].Referrals["Sources"].Rows["google|organic"];
            Assert.IsTrue(sum[0].Referrals["Sources"].Rows["google|organic"].Bounces == googleOrganic.Bounces * 2);

            Assert.IsTrue(sum[0].BusinessUnit == selectedMonths[0][0].BusinessUnit);
            Assert.IsTrue(sum[0].BusinessUnitId == selectedMonths[0][0].BusinessUnitId);
        }
    }
}
