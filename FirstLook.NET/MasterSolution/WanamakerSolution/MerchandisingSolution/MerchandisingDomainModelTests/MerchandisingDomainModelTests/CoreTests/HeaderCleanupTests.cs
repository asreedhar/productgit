﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Merchandising.DomainModel;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests
{
    [TestFixture]
    public class HeaderCleanupTests
    {

        [Test]
        public void TestHeaders1()
        {
            string header = "=== BMW 3-SERIES: THE BENCHMARK FOR PERFORMANCE AND THRILLS::";

            string result = header.CleanUpHeader();

            Assert.IsTrue(result == "BMW 3-SERIES: THE BENCHMARK FOR PERFORMANCE AND THRILLS");
        }

        [Test]
        public void TestHeaders2()
        {
            string header = "===  BMW 3-SERIES: ===THE BENCHMARK FOR PERFORMANCE AND THRILLS::  ";

            string result = header.CleanUpHeader();

            Assert.IsTrue(result == "BMW 3-SERIES: ===THE BENCHMARK FOR PERFORMANCE AND THRILLS");
        }
    }
}
