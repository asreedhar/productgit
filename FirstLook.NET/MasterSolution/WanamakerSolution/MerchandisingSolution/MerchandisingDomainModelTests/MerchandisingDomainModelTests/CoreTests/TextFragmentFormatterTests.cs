﻿using System;
using FirstLook.Merchandising.DomainModel.Core;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests
{
    [TestFixture]
    public class TextFragmentFormatterTests
    {
        private TextFragmentFormatter _formatter;

        [SetUp]
        public void Setup()
        {
            _formatter = new TextFragmentFormatter();
        }

        [Test]
        public void Empty()
        {
            Assert.AreEqual(string.Empty, _formatter.Format(string.Empty));
        }

        [Test]
        public void Null()
        {
            Assert.AreEqual(string.Empty, _formatter.Format(null));
        }

        [Test]
        public void Transmissions()
        {
            const string AT = "Automatic Transmission ";
            const string MT = "Manual Transmission ";

            Assert.AreEqual(AT, _formatter.Format("a/t"));
            Assert.AreEqual(AT, _formatter.Format("A/t"));
            Assert.AreEqual(AT, _formatter.Format("a/T"));
            Assert.AreEqual(AT, _formatter.Format("A/T"));

            Assert.AreEqual(MT, _formatter.Format("m/t"));
            Assert.AreEqual(MT, _formatter.Format("M/t"));
            Assert.AreEqual(MT, _formatter.Format("m/T"));
            Assert.AreEqual(MT, _formatter.Format("M/T"));
        }

        [Test]
        public void LeadingTrailingWhitespace()
        {
            string s = "a";
            s = s.PadLeft(5, ' ');
            s = s.PadRight(5, ' ');

            Console.WriteLine(s);

            Assert.AreEqual("a ", _formatter.Format(s));
        }

        [Test]
        public void RemoveLeadingAndTrailingPunctuation()
        {
            //remove all ", ." ", ~" ", !" or ", *"  with 
            // Regex rg1 = new Regex("[,][\\s]*[\\.\\*\\~!\\-]");

            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities(",."));
            //            Assert.AreEqual("*", TextFragmentFragmentFormatter.CleanUpIrregularities(", *"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities(",~"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities(",!"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities(",~!"));
            //            Assert.AreEqual("-", TextFragmentFragmentFormatter.CleanUpIrregularities(", -"));


            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities(", ."));
//            Assert.AreEqual("*", TextFragmentFragmentFormatter.CleanUpIrregularities(", *"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities(", ~"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities(", !"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities(", ~!"));
//            Assert.AreEqual("-", TextFragmentFragmentFormatter.CleanUpIrregularities(", -"));

            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities(" ."));
            //            Assert.AreEqual("*", TextFragmentFragmentFormatter.CleanUpIrregularities(", *"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities(" ~"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities(" !"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities(" ~!"));
            //            Assert.AreEqual("-", TextFragmentFragmentFormatter.CleanUpIrregularities(", -"));

        }

        [Test]
        public void RemoveTrailingPunctuation()
        {
            //remove all "! [punctuation]" ". [punctuation]"  "? [punctuation]"  or ": [punctuation]"   with 
            // Regex rg5 = new Regex("[,!?][\\s]*[\\.?!\\-,]");

            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("! ."));            
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("! ?"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("! !"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("! -"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("! ,"));

            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("? ."));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("? ?"));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("? !"));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("? -"));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("? ,"));

            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("!."));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("!?"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("!!"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("!-"));
            Assert.AreEqual("!", TextFragmentFormatter.CleanUpIrregularities("!,"));

            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("?."));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("??"));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("?!"));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("?-"));
            Assert.AreEqual("?", TextFragmentFormatter.CleanUpIrregularities("?,"));

            //remove ., and . , and ~, and ~ ,
            /*
                    Regex rg3 = new Regex("[\\.|\\~][\\s]*[,]");
                    inString = rg3.Replace(inString, ". ");
                    Regex rg6 = new Regex("[\\~][\\s]*[,\\.\\:!?]");
                    inString = rg6.Replace(inString, "~ ");

                    Regex rg4 = new Regex("[\\:][\\s]*[\\.,\\-\\~]");
                    inString = rg4.Replace(inString, ": ");
            */

            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities(".,"));
            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities(". ,"));
            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities("~,"));
            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities("~, "));

            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities("~,"));
            Assert.AreEqual(".", TextFragmentFormatter.CleanUpIrregularities("~ ,"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~."));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~ ."));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~:"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~ :"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~!"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~ !"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~?"));
            Assert.AreEqual("~", TextFragmentFormatter.CleanUpIrregularities("~ ?"));

            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(":."));
            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(": ."));
            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(":,"));
            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(": ,"));
            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(":-"));
            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(": -"));
            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(":~"));
            Assert.AreEqual(":", TextFragmentFormatter.CleanUpIrregularities(": ~"));            

        }

        [Test]
        public void ReplaceWhitespaceAroundAndWithinAlphaNumericCodes()
        {
/*
            Regex rg = new Regex("[\\dA-Za-z][\\s]{2,}[\\dA-Za-z]");
            while (rg.IsMatch(inString))
            {
                Match mt = rg.Match(inString);
                //remove all but the first space...
                inString = inString.Remove(mt.Index + 2, mt.Length - 3);
            }
*/
            Assert.AreEqual("1A 1A", TextFragmentFormatter.CleanUpIrregularities(" 1A  1A "));
            Assert.AreEqual("1A 1A", TextFragmentFormatter.CleanUpIrregularities(" 1A                  1A "));  
        }

        [Test]
        public void RewriteLongWhiteSpaceAsEmpty()
        {
/*
            Regex rg2 = new Regex("[\\s]{3,}");
            inString = rg2.Replace(inString, "  ");
*/
            Assert.AreEqual(string.Empty, TextFragmentFormatter.CleanUpIrregularities("   "));
        }

        [Test]
        public void Elipses()
        {
/*
            rg2 = new Regex("[\\.,\\-\\~]+[\\s]+\\.\\.\\.");
            inString = rg2.Replace(inString, "...");
            return inString.Trim(" ".ToCharArray());
*/
            Assert.AreEqual("...", TextFragmentFormatter.CleanUpIrregularities(". ..."));
            Assert.AreEqual("...", TextFragmentFormatter.CleanUpIrregularities(", ..."));
            Assert.AreEqual("...", TextFragmentFormatter.CleanUpIrregularities("- ..."));
//            Assert.AreEqual("...", TextFragmentFormatter.CleanUpIrregularities("~ ..."));

        }

        [Test]
        public void RemoveDelimiters()
        {
            Assert.AreEqual("",TextFragmentFormatter.RemoveDelimiters("*"));
            Assert.AreEqual("", TextFragmentFormatter.RemoveDelimiters("+"));
        }

        [Test]
        public void FixGrammer()
        {
            // phrases should not start with and, or, or comma.
            Assert.AreEqual(" a", TextFragmentFormatter.FixGrammer("and a"));
            Assert.AreEqual(" a", TextFragmentFormatter.FixGrammer("or a"));
            Assert.AreEqual(" we go", TextFragmentFormatter.FixGrammer("Or we go"));
            Assert.AreEqual("a", TextFragmentFormatter.FixGrammer(",a"));
            Assert.AreEqual("This is a sentence with or in the middle.", TextFragmentFormatter.FixGrammer("This is a sentence with or in the middle."));
            Assert.AreEqual("Originally bought here.", TextFragmentFormatter.FixGrammer("Originally bought here."));

            // Sentences should not end with ","
            Assert.AreEqual("a", TextFragmentFormatter.FixGrammer("a,"));

            // sentences should not end with "-"
            Assert.AreEqual("a", TextFragmentFormatter.FixGrammer("a-"));
        }

        [Test]
        public void RemoveTwoOrMoreEquals()
        {
            Assert.AreEqual(" ", TextFragmentFormatter.RemoveRepeatedCharacters("=="));

            string oneHundredEquals = "=".PadRight(100, '=');

            Assert.AreEqual(" ", TextFragmentFormatter.RemoveRepeatedCharacters(oneHundredEquals));
        }

        [Test]
        public void ReplaceEqualsSpaceEquals()
        {
            Assert.AreEqual("=", TextFragmentFormatter.ReplaceEqualSpace("= ="));            
        }

        [Test]
        public void RemoveRepeatedCharacters()
        {
            Assert.IsTrue(string.Equals(" ", TextFragmentFormatter.RemoveRepeatedCharacters("==")));
            Assert.IsTrue(string.Equals(". ", TextFragmentFormatter.RemoveRepeatedCharacters("..")));
            Assert.IsTrue(string.Equals(" ", TextFragmentFormatter.RemoveRepeatedCharacters(",,")));
            Assert.IsTrue(string.Equals(" ", TextFragmentFormatter.RemoveRepeatedCharacters("~~")));
            Assert.IsTrue(string.Equals("hello world", TextFragmentFormatter.RemoveRepeatedCharacters("hello world")));
        }
    }
}
