﻿using System;
using FirstLook.Merchandising.DomainModel.Core;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests
{
    [TestFixture]
    public class HeaderTests
    {
        private MatchTest _test;

        internal class MatchTest
        {
            private readonly AdStructure _structure;

            internal MatchTest(AdStructure structure)
            {
                _structure = structure;
            }

            internal void Expect(string expected)
            {
                var tuple = _structure.IsHeader(expected);
                Console.WriteLine(tuple.Second);

                Assert.IsTrue(tuple.First);
                Assert.AreEqual(expected, tuple.Second);
            }

            internal void Expect(string input, string expected)
            {
                var tuple = _structure.IsHeader(input);
                Console.WriteLine(tuple.Second);
                Assert.IsTrue(tuple.First);
                Assert.AreEqual(expected, tuple.Second);
            }

            internal string HeaderText(string input)
            {
                var tuple = _structure.IsHeader(input);
                if (tuple.First)
                {
                    return tuple.Second;
                }
                return string.Empty;
            }
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            _test = new MatchTest(new AdStructure());
        }

        [Test]
        public void Common()
        {
            string input = "====== A :";
            _test.Expect(input, " A ");
            Assert.AreEqual("A", _test.HeaderText(input).Trim());
        }

        [Test]
        public void NoSpaces()
        {
            _test.Expect("======A:", "A");
        }

        [Test]
        public void Short()
        {
            _test.Expect("==A:", "A");
        }

        [Test]
        public void ManyEquals()
        {
            _test.Expect("============================================================ A :", " A ");
        }

        [Test]
        public void EndsWithPeriod()
        {
            _test.Expect("== A.", " A");
        }

        [Test]
        public void EndsWithExclaimation()
        {
            _test.Expect("== A!", " A");
        }

        [Test]
        public void EndsWithQuestion()
        {
            _test.Expect("== A?", " A");
        }

        [Test]
        public void EndsWithThreeTilde()
        {
            _test.Expect("==A~~~", "A");
        }

        [Test]
        public void Complex()
        {
            _test.Expect("== the quick brown fox's store. more stuff", " the quick brown fox's store");
        }

        [Test]
        public void Eplipses()
        {
            _test.Expect(
                "== Great safety equipment to protect you on the road...Electronic Stability Control, Brake Assist, 4-Wheel ABS",
                " Great safety equipment to protect you on the road");
        }

        [Test]
        public void Dash()
        {
            _test.Expect("== A - B:", " A - B");
        }

        [Test]
        public void WeCanHelp()
        {
            _test.Expect("====== You need reliable transportation to get around town - we can help.",
                " You need reliable transportation to get around town - we can help");
        }

        [Test]
        public void Plus()
        {
            _test.Expect("==Get your A+ student a dependable car.", "Get your A+ student a dependable car");
        }

        [Test]
        public void LotsOfPunctuation()
        {
            _test.Expect("== Why is GMC's Certified-program best?", " Why is GMC's Certified-program best");
        }

        [Test]
        public void Slash()
        {
            _test.Expect("== Three year / 36 month warrany.", " Three year / 36 month warrany");
        }

        [Test]
        public void Percent()
        {
            _test.Expect("== Only 1% down.", " Only 1% down");
        }

        [Test]
        public void RepeatExclaimation()
        {
            _test.Expect("== Great savings!!!", " Great savings");
        }

    }
}
