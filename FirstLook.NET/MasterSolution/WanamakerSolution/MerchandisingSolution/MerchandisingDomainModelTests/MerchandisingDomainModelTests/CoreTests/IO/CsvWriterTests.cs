﻿using System;
using System.IO;
using System.Text;
using FirstLook.Merchandising.DomainModel.Core.IO;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.IO
{
    [TestFixture]
    public class CsvWriterTests
    {
        [Test]
        public void WriteValue_should_emit_single_row_and_column()
        {
            var stream = new MemoryStream();

            using (var writer = new CsvWriter(stream))
            {
                writer.StartRecord();
                writer.WriteValue("25");
            }

            var csv = Encoding.UTF8.GetString(stream.ToArray());
            Assert.That(csv, Is.EqualTo("25"));
        }

        [Test]
        public void WriteValue_twice_should_emit_two_columns_separated_by_comma()
        {
            var stream = new MemoryStream();

            using (var writer = new CsvWriter(stream))
            {
                writer.StartRecord();
                writer.WriteValue("A1");
                writer.WriteValue("A2");
            }

            var csv = Encoding.UTF8.GetString(stream.ToArray());
            Assert.That(csv, Is.EqualTo("A1,A2"));
        }

        [Test]
        public void WriteDate_should_write_M_D_YYYY_format()
        {
            var stream = new MemoryStream();

            using (var writer = new CsvWriter(stream))
            {
                writer.StartRecord();
                writer.WriteDate(new DateTime(2005, 10, 17, 5, 6, 7));
            }

            var csv = Encoding.UTF8.GetString(stream.ToArray());
            Assert.That(csv, Is.EqualTo("10/17/2005"));
        }

        [TestCase("foo\r\nbar", "\"foo\r\nbar\"", Description = "Embedded newline.")]
        [TestCase("foo\nbar", "\"foo\nbar\"", Description = "Embedded \\n.")]
        [TestCase("foo\rbar", "\"foo\rbar\"", Description = "Embedded \\r.")]
        [TestCase("\"foo bar\"", "\"\"\"foo bar\"\"\"", Description = "Embedded quotes.")]
        [TestCase("foo, bar", "\"foo, bar\"", Description = "Embedded commas.")]
        public void WriteValue_should_escape_if_contains_control_characters(string inputStr, string expectedCsv)
        {
            var stream = new MemoryStream();

            using (var writer = new CsvWriter(stream))
            {
                writer.StartRecord();
                writer.WriteValue(inputStr);
            }

            var csv = Encoding.UTF8.GetString(stream.ToArray());
            Assert.That(csv, Is.EqualTo(expectedCsv));
        }

        [Test]
        public void Multiple_rows_should_be_separeted_with_newlines()
        {
            var stream = new MemoryStream();

            using (var writer = new CsvWriter(stream))
            {
                writer.StartRecord();
                writer.WriteValue("A1");
                writer.WriteValue("A2");
                writer.StartRecord();
                writer.WriteValue("B1");
                writer.WriteValue("B2");
                writer.StartRecord();
                writer.WriteValue("C1");
                writer.WriteValue("C2");
            }

            var csv = Encoding.UTF8.GetString(stream.ToArray());
            Assert.That(csv, Is.EqualTo("A1,A2\r\nB1,B2\r\nC1,C2"));
        }

        [TestCase(true, "text/csv;charset=utf-8;header=present")]
        [TestCase(false, "text/csv;charset=utf-8;header=absent")]
        public void Verify_content_type_header_property_is_correct_given_constructor_parameter_hasHeader(bool hasHeader, string expectedContentType)
        {
            var stream = new MemoryStream();
            using(var writer = new CsvWriter(stream, hasHeader))
            {
                Assert.That(writer.ContentTypeHeader, Is.EqualTo(expectedContentType));
                writer.StartRecord();
                writer.WriteValue("A1");
            }
        }

        [Test]
        public void Throw_when_too_few_columns_after_first_row()
        {
            var stream = new MemoryStream();

            Assert.Throws<InvalidOperationException>(
                () =>
                    {
                        using (var writer = new CsvWriter(stream))
                        {
                            writer.StartRecord();
                            writer.WriteValue("A1");
                            writer.WriteValue("A2");
                            writer.StartRecord();
                            writer.WriteValue("B1");
                            writer.StartRecord();
                        }
                    });
        }

        [Test]
        public void Throw_during_dispose_when_too_few_columns_after_first_row()
        {
            var stream = new MemoryStream();

            Assert.Throws<InvalidOperationException>(
                () =>
                {
                    using (var writer = new CsvWriter(stream))
                    {
                        writer.StartRecord();
                        writer.WriteValue("A1");
                        writer.WriteValue("A2");
                        writer.StartRecord();
                        writer.WriteValue("B1");
                    }
                });
        }

        [Test]
        public void Throw_when_too_many_columns_after_first_row()
        {
            var stream = new MemoryStream();

            Assert.Throws<InvalidOperationException>(
                () =>
                {
                    using (var writer = new CsvWriter(stream))
                    {
                        writer.StartRecord();
                        writer.WriteValue("A1");
                        writer.WriteValue("A2");
                        writer.StartRecord();
                        writer.WriteValue("B1");
                        writer.WriteValue("B2");
                        writer.WriteValue("B3");
                    }
                });
        }

        [Test]
        public void Throw_on_dispose_when_zero_columns()
        {
            var stream = new MemoryStream();

            Assert.Throws<InvalidOperationException>(
                () =>
                {
                    using (var writer = new CsvWriter(stream))
                    {
                        writer.StartRecord();
                    }
                });
        }

        [Test]
        public void Throw_on_StartRecord_when_zero_columns()
        {
            var stream = new MemoryStream();

            Assert.Throws<InvalidOperationException>(
                () =>
                {
                    using (var writer = new CsvWriter(stream))
                    {
                        writer.StartRecord();
                        writer.StartRecord();
                    }
                });
        }

        [Test]
        public void Throw_if_hasHeader_but_no_rows()
        {
            var stream = new MemoryStream();

            Assert.Throws<InvalidOperationException>(
                () =>
                {
                    using (new CsvWriter(stream, true))
                    {
                    }
                });
        }

        [Test]
        public void Throw_if_WriteValue_called_before_StartRecord()
        {
            var stream = new MemoryStream();

            Assert.Throws<InvalidOperationException>(
                () =>
                {
                    using (var writer = new CsvWriter(stream, false))
                    {
                        writer.WriteValue("A1");
                    }
                });
        }
    }
}