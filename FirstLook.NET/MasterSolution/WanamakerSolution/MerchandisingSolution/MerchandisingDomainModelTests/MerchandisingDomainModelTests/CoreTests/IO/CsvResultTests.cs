﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FirstLook.Merchandising.DomainModel.Core.IO;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.IO
{
    [TestFixture]
    public class CsvResultTests
    {
        private ControllerContext _controllerContext;
        private MemoryStream _buffer;
        private Mock<HttpResponseBase> _httpResponseMock;
        private Mock<HttpContextBase> _httpContextMock;

        [SetUp]
        public void Setup()
        {
            _httpResponseMock = new Mock<HttpResponseBase>();
            _buffer = new MemoryStream();
            _httpResponseMock.SetupAllProperties();
            _httpResponseMock.Setup(r => r.OutputStream).Returns(_buffer);

            _httpContextMock = new Mock<HttpContextBase>();
            _httpContextMock.Setup(c => c.Response).Returns(_httpResponseMock.Object);

            _controllerContext = new ControllerContext {HttpContext = _httpContextMock.Object};
        }

        private string GetBufferAsString()
        {
            return Encoding.UTF8.GetString(_buffer.ToArray());
        }

        [Test]
        public void ExecuteResult_with_basic_model_and_data_should_output_correct_stream_data()
        {
            var data = GetSampleData();
            var writer = new CsvResult<TestModel1>(data);

            writer.ExecuteResult(_controllerContext);

            Assert.That(GetBufferAsString(), Is.EqualTo(
                @"Id,Val
5,Five
17,Seventeen"));
        }

        [Test]
        public void ExecuteResult_with_no_rows_should_output_only_headers_to_stream_data()
        {
            var data =  Enumerable.Empty<TestModel1>();
            var writer = new CsvResult<TestModel1>(data);

            writer.ExecuteResult(_controllerContext);

            Assert.That(GetBufferAsString(), Is.EqualTo(
                @"Id,Val"));
        }

        [Test]
        public void ExecuteResult_should_use_data_annotations_to_render_csv()
        {
            var data = new[]
                           {
                               new TestModel2 {StockNum = "P100", Vin = "WGXP0000000000000"},
                               new TestModel2 {StockNum = "P101", Vin = "WGXP0000000000001", ListPrice = 5000}
                           };
            var writer = new CsvResult<TestModel2>(data);

            writer.ExecuteResult(_controllerContext);

            Assert.That(GetBufferAsString(), Is.EqualTo(
@"VIN,Stock Num,Internet Price
WGXP0000000000000,P100,
WGXP0000000000001,P101,""$5,000.00"""));
        }

        [Test]
        public void ExecuteTest_with_custom_header_and_row_logic_should_render_correctly()
        {
            var data = GetSampleData();
            var writer = new CsvResult<TestModel1>(data,
                    wr =>
                        {
                            wr.StartRecord();
                            wr.WriteValue("Col1");
                            wr.WriteValue("Col2");
                            wr.WriteValue("Col3");
                        },
                    (wr, d) =>
                        {
                            wr.StartRecord();
                            wr.WriteValue("55");
                            wr.WriteValue((d.Id*2).ToString(CultureInfo.InvariantCulture));
                            wr.WriteValue(d.Val + "$");
                        });

            writer.ExecuteResult(_controllerContext);

            Assert.That(GetBufferAsString(), Is.EqualTo(
@"Col1,Col2,Col3
55,10,Five$
55,34,Seventeen$"));
        }

        [Test]
        public void ExecuteResult_should_set_ContentType()
        {
            var data = Enumerable.Empty<TestModel1>();
            var writer = new CsvResult<TestModel1>(data);

            writer.ExecuteResult(_controllerContext);

            Assert.That(_controllerContext.HttpContext.Response.ContentType, Is.EqualTo("text/csv;charset=utf-8;header=present"));
        }

        [Test]
        public void ExecuteResult_with_model_with_DateTime_should_serialize_to_Date_value()
        {
            var data = new[] {new TestModel3 {SomeDate = new DateTime(2005, 05, 10)}};
            var result = new CsvResult<TestModel3>(data);

            result.ExecuteResult(_controllerContext);

            Assert.That(GetBufferAsString(), Is.EqualTo(
                @"SomeDate
5/10/2005"));
        }

        private static IEnumerable<TestModel1> GetSampleData()
        {
            return new[]
                       {
                           new TestModel1 {Id = 5, Val = "Five"},
                           new TestModel1 {Id = 17, Val = "Seventeen"}
                       };
        }

        public class TestModel1
        {
            public int Id { get; set; }
            public string Val { get; set; }
        }

        public class TestModel2
        {
            [Display(Name = "Stock Num", Order = 2)]
            public string StockNum { get; set; }

            [Display(Name = "VIN", Order = 1)]
            public string Vin { get; set; }

            [Display(Name = "Internet Price", Order = 3)]
            [DisplayFormat(DataFormatString = "{0:C}")]
            public decimal? ListPrice { get; set; }
        }

        public class TestModel3
        {
            public DateTime SomeDate { get; set; }
        }
    }
}