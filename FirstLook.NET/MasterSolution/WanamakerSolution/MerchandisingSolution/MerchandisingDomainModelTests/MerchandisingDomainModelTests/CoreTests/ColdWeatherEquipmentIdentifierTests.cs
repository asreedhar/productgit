﻿using FirstLook.Merchandising.DomainModel.Core;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests
{
    [TestFixture]
    public class ColdWeatherEquipmentIdentifierTests
    {
        private ColdWeatherEquipmentIdentifier _identifier;

        [SetUp]
        public void Setup()
        {
            _identifier = new ColdWeatherEquipmentIdentifier();
        }

        [Test]
        public void Positives()
        {
            Assert.IsTrue(_identifier.IsColdWeatherEquipment("a heated b"));
            Assert.IsTrue(_identifier.IsColdWeatherEquipment("a cold b"));
            Assert.IsTrue(_identifier.IsColdWeatherEquipment("a winter b"));
            Assert.IsTrue(_identifier.IsColdWeatherEquipment("a snow b"));
            Assert.IsTrue(_identifier.IsColdWeatherEquipment("a all weather b"));
        }

        [Test]
        public void Negatives()
        {
            Assert.IsFalse(_identifier.IsColdWeatherEquipment(""));
            Assert.IsFalse(_identifier.IsColdWeatherEquipment("a"));
            Assert.IsFalse(_identifier.IsColdWeatherEquipment(null));
        }
    }
}
