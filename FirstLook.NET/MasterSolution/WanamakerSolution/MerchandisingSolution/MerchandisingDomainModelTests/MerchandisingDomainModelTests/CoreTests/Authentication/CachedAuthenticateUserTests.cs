﻿//using FirstLook.Common.Core.Cache;

using System;
using FirstLook.Common.Core.Cache;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using Moq;
using NUnit.Framework;
using MemoryCache = System.Runtime.Caching.MemoryCache;

namespace MerchandisingDomainModelTests.CoreTests.Authentication
{
    [TestFixture]
    public class CachedAuthenticateUserTests
    {
        private Mock<IAuthenticateUser> _authenticateUserMock;
        private IAuthenticateUser _authenticateUser;
        private DotnetMemoryCacheWrapper _cache;
        private Mock<ITimeProvider> _timeProviderMock;
        private ITimeProvider _timeProvider;

        [SetUp]
        public void Setup()
        {
            _authenticateUserMock = new Mock<IAuthenticateUser>();
            _authenticateUser = _authenticateUserMock.Object;

            _timeProviderMock = new Mock<ITimeProvider>();
            _timeProvider = _timeProviderMock.Object;

            _cache = new DotnetMemoryCacheWrapper(new MemoryCache("Test_Cache"));
        }

        [TestCase("bob", "p@ssw0rd", true)]
        [TestCase("bob", "h@ck3r", false)]
        [TestCase("steve", "I'm not a real user", false)]
        public void Authenticate_once_should_call_inner_authenticate_once(string username, string password, bool shouldBeAuthenticated)
        {
            _authenticateUserMock.Setup(au => au.Authenticate(new BasicCredential("bob", "p@ssw0rd"))).Returns("CAS");

            var decorator = new CachedAuthenticateUser(_cache, _authenticateUser, _timeProvider);

            var authenticationType = decorator.Authenticate(new BasicCredential(username, password));

            Assert.That(authenticationType, shouldBeAuthenticated ? Is.Not.Null : Is.Null);
            _authenticateUserMock.Verify(au => au.Authenticate(It.IsAny<BasicCredential>()), Times.Once());
        }

        [TestCase("bob", "p@ssw0rd", true)]
        [TestCase("bob", "h@ck3r", false)]
        [TestCase("steve", "I'm not a real user", false)]
        public void Authenticate_twice_should_call_inner_authenticate_once(string username, string password, bool shouldBeAuthenticated)
        {
            _authenticateUserMock.Setup(au => au.Authenticate(new BasicCredential("bob", "p@ssw0rd"))).Returns("NTLM");

            var decorator = new CachedAuthenticateUser(_cache, _authenticateUser, _timeProvider);

            var authenticationType = decorator.Authenticate(new BasicCredential(username, password));
            var authenticationType2 = decorator.Authenticate(new BasicCredential(username, password));

            Assert.That(authenticationType, shouldBeAuthenticated ? Is.Not.Null : Is.Null);
            Assert.That(authenticationType2, shouldBeAuthenticated ? Is.Not.Null : Is.Null);
            _authenticateUserMock.Verify(au => au.Authenticate(It.IsAny<BasicCredential>()), Times.Once());
        }

        [Test]
        public void Authenticate_twice_within_timespan_greater_than_limit_should_call_inner_Authenticate_twice()
        {
            var startTime = new DateTimeOffset(new DateTime(2005, 05, 05, 05, 05, 05));
            _authenticateUserMock.Setup(au => au.Authenticate(new BasicCredential("bob", "p@ssw0rd"))).Returns("NTLM");

            var decorator = new CachedAuthenticateUser(_cache, _authenticateUser, _timeProvider);

            _timeProviderMock.Setup(t => t.UtcNow).Returns(startTime);
            var authenticationType = decorator.Authenticate(new BasicCredential("bob", "p@ssw0rd"));

            _timeProviderMock.Setup(t => t.UtcNow).Returns(startTime +
                                                           TimeSpan.FromSeconds(
                                                               CachedAuthenticateUser.CacheIntervalInSeconds*2));
            var authenticationType2 = decorator.Authenticate(new BasicCredential("bob", "p@ssw0rd"));

            Assert.That(authenticationType, Is.Not.Null);
            Assert.That(authenticationType2, Is.Not.Null);
            _authenticateUserMock.Verify(au => au.Authenticate(It.IsAny<BasicCredential>()), Times.Exactly(2));
        }

        [Test]
        public void Authenticate_twice_within_timespan_greater_than_limit_with_second_inner_Authenticate_returning_false_should_return_false()
        {
            var startTime = new DateTimeOffset(new DateTime(2005, 05, 05, 05, 05, 05));
            var decorator = new CachedAuthenticateUser(_cache, _authenticateUser, _timeProvider);

            // First try
            _authenticateUserMock.Setup(au => au.Authenticate(new BasicCredential("bob", "p@ssw0rd"))).Returns("NTLM");
            _timeProviderMock.Setup(t => t.UtcNow).Returns(startTime);
            var authenticationType = decorator.Authenticate(new BasicCredential("bob", "p@ssw0rd"));

            // Second try
            _authenticateUserMock.Setup(au => au.Authenticate(new BasicCredential("bob", "p@ssw0rd"))).Returns((string)null);
            _timeProviderMock.Setup(t => t.UtcNow).Returns(startTime +
                                                           TimeSpan.FromSeconds(
                                                               CachedAuthenticateUser.CacheIntervalInSeconds * 2));
            var authenticationType2 = decorator.Authenticate(new BasicCredential("bob", "p@ssw0rd"));

            Assert.That(authenticationType, Is.Not.Null);
            Assert.That(authenticationType2, Is.Null);
            _authenticateUserMock.Verify(au => au.Authenticate(It.IsAny<BasicCredential>()), Times.Exactly(2));
        }
    }
}