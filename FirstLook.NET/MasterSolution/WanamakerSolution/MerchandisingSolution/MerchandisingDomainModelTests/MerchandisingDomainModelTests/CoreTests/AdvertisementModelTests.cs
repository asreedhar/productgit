﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel;
using FirstLook.Merchandising.DomainModel.Core.AdvertisementModel.Converters;
using NUnit.Framework;
using System.IO;

namespace MerchandisingDomainModelTests.CoreTests
{
    [TestFixture]
    public class AdvertisementModelTests
    {
        [Test]
        public void TestFromXmlClassic()
        {
            Advertisement model;
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.LegecyAdModel.xml")))
            {
                AdvertisementConverter converter = new AdvertisementConverter();
                model = converter.FromXml(textReader.ReadToEnd());
            }

            Assert.IsTrue(model.AdTemplateID == -1);
            Assert.IsTrue(model.Version == "1.5");
            Assert.IsTrue(model.Modules.Length == 5);
            Assert.IsTrue(model.Modules[0].Header.Item.Text == "PURCHASE WITH CONFIDENCE ");
            Assert.IsTrue(model.Modules[0].Paragraph.Items[0].BlurbTemplateID == -1);
            Assert.IsTrue(model.Preview.Header.Item.Text == "PREVIEW");
            Assert.IsTrue(model.Preview.Paragraph.Items.Length == 1);
            Assert.IsTrue(model.Preview.Paragraph.Items[0].Text == "CARFAX 1-Owner, Honda Certified, LOW MILES - 23,881! WAS $18,990, FUEL EFFICIENT 30 MPG Hwy/23 MPG City! Edmunds.com's review says \"Remarkably nimble.\", IIHS Top Safety Pick, Steel Wheels CD Player Overhead Airbag Auxiliary Audio Input AND MORE!     ");

        }

        [Test]
        public void ReadFromVersionOnePointFiveClassic()
        {
            Advertisement model;
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.AdModel1.5.xml")))
            {
                AdvertisementConverter converter = new AdvertisementConverter();
                model = converter.FromXml(textReader.ReadToEnd());
            }

            Assert.IsTrue(model.AdTemplateID == -1);
            Assert.IsTrue(model.Version == "1.5");
            Assert.IsTrue(model.Modules[0].Header.Item.Text == "KEY FEATURES: Hybrid,iPod/MP3 Input,Bluetooth,Keyless Start,Dual Zone A/C ");
        }

        [Test]
        public void TestFromXml()
        {
            Advertisement model;
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.AdvertisementModel1.xml")))
            {
                AdvertisementConverter converter = new AdvertisementConverter();
                model = converter.FromXml(textReader.ReadToEnd());
            }
            
            Assert.IsTrue(model.Preview.Paragraph.Items.Length == 9);
            Assert.IsTrue(model.Modules.Length == 4);
            Assert.IsTrue(model.Modules[2].Header.Item.Text == "A GREAT VALUE:");
            Assert.IsTrue(model.Modules[2].Paragraph.Items[0].Edited);
        }

        [Test]
        public void TestToXml()
        {
            AdvertisementConverter converter = new AdvertisementConverter();
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.AdvertisementModel1.xml")))
            {
                string orignalXml = textReader.ReadToEnd();
                XmlDocument doc = new XmlDocument(); //remove formatting like newlines etc.
                doc.LoadXml(orignalXml);
                orignalXml = doc.InnerXml;

                Advertisement model = converter.FromXml(orignalXml);
                string xml = converter.ToXml(model);

                Assert.IsTrue(orignalXml == xml);
            }

        }

        [Test]
        public void TestToJson()
        {
            AdvertisementConverter converter = new AdvertisementConverter();
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.AdvertisementModel1.xml")))
            {
                string orignalXml = textReader.ReadToEnd();

                Advertisement model = converter.FromXml(orignalXml);
                string json = converter.ToJson(model);

                //Assert.IsTrue(orignalXml == xml);
            }
        }

        [Test]
        public void TestFromJson()
        {
            AdvertisementConverter converter = new AdvertisementConverter();
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.AdvertisementModel1.xml")))
            {
                string orignalXml = textReader.ReadToEnd();
                XmlDocument doc = new XmlDocument(); //remove formatting like newlines etc.
                doc.LoadXml(orignalXml);
                orignalXml = doc.InnerXml;
                Advertisement model = converter.FromXml(orignalXml);

                string json = converter.ToJson(model);
                model = converter.FromJson(json);

                string xml = converter.ToXml(model);
                Assert.IsTrue(orignalXml == xml);
            }
        }


        [Test]
        public void TestToHtml()
        {
            AdvertisementConverter converter = new AdvertisementConverter();
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.AdvertisementModel1.xml")))
            {
                string orignalXml = textReader.ReadToEnd();

                Advertisement model = converter.FromXml(orignalXml);
                string html = converter.ToHtml(model);

                //Assert.IsTrue(orignalXml == xml);
            }
        }

        [Test]
        public void TestFromText()
        {
            AdvertisementConverter converter = new AdvertisementConverter();
            Advertisement model = converter.FromText("I am a long ad with no model");

            Assert.IsTrue(model.Modules.Length == 1);
            Assert.IsTrue(model.Modules[0].Paragraph.Items.Length == 1);
            Assert.IsTrue(model.Modules[0].Paragraph.Items[0].Text == "I am a long ad with no model");
        }

        [Test]
        public void TestToText()
        {
            AdvertisementConverter converter = new AdvertisementConverter();
            using (TextReader textReader = new StreamReader(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.AdvertisementModel1.xml")))
            {
                string orignalXml = textReader.ReadToEnd();

                Advertisement model = converter.FromXml(orignalXml);
                string text = converter.ToText(model);

                //Assert.IsTrue(orignalXml == xml);
            }
            
        }

    }
}
