﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.Core.TemplatingModel;

namespace MerchandisingDomainModelTests.CoreTests
{
    [TestFixture]
    public class TemplatingModelTests
    {

        [Test]
        public void Create()
        {
            TemplatingModelFactory fact = new TemplatingModelFactory();
            AdTemplate model;
            using (XmlReader xmlReader = XmlReader.Create(typeof(TemplatingModelTests).Assembly.GetManifestResourceStream("MerchandisingDomainModelTests.Files.TemplatingModel1.xml")))
            {
                model = fact.CreateAdTemplateModel(xmlReader);
            }

            Assert.IsTrue(model.TemplateID == 1992);
            Assert.IsTrue(model.Children.Count == 23);

            //Assert.IsTrue();
        }
    }
}
