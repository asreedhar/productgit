﻿using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using NUnit.Framework;
using Moq;

namespace MerchandisingDomainModelTests.CoreTests
{
    [TestFixture]
    public class FilteringTests
    {
        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<WorkflowRepository>().As<IWorkflowRepository>();
            var container = builder.Build();
            Registry.RegisterContainer(container);
        }

        [TearDown]
        public void Teardown()
        {
            Registry.Reset();
        }

        // Test truth table for NoPackages bucket
        [TestCase(false, false, false, MerchandisingBucket.DoNotPost, false)]
        [TestCase(false, false, false, MerchandisingBucket.PendingApproval, false)]
        [TestCase(false, false, true, MerchandisingBucket.DoNotPost, false)]
        [TestCase(false, false, true, MerchandisingBucket.PendingApproval, false)]
        [TestCase(false, true, false, MerchandisingBucket.DoNotPost, false)]
        [TestCase(false, true, false, MerchandisingBucket.PendingApproval, false)]
        [TestCase(false, true, true, MerchandisingBucket.DoNotPost, false)]
        [TestCase(false, true, true, MerchandisingBucket.PendingApproval, false)]
        [TestCase(true, false, false, MerchandisingBucket.DoNotPost, false)]
        [TestCase(true, false, false, MerchandisingBucket.PendingApproval, false)]
        [TestCase(true, false, true, MerchandisingBucket.DoNotPost, false)]
        [TestCase(true, false, true, MerchandisingBucket.PendingApproval, true)]
        [TestCase(true, true, false, MerchandisingBucket.DoNotPost, false)]
        [TestCase(true, true, false, MerchandisingBucket.PendingApproval, true)]
        [TestCase(true, true, true, MerchandisingBucket.DoNotPost, false)]
        [TestCase(true, true, true, MerchandisingBucket.PendingApproval, true)]
        public void TestNoPackagesBin(bool noPackages, bool exteriorEquipmentNeeded, bool interiorEquipmentNeeded, MerchandisingBucket? statusBucket,
            bool expectedInBin)
        {
            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.NoPackages).Returns(noPackages);
            mock.Setup(x => x.NeedsExterior).Returns(exteriorEquipmentNeeded);
            mock.Setup(x => x.NeedsInterior).Returns(interiorEquipmentNeeded);
            mock.Setup(x => x.StatusBucket).Returns(statusBucket);

            var bin = Bin.Create(WorkflowType.NoPackages);

            Assert.That(bin.Filter(mock.Object), Is.EqualTo(expectedInBin));
        }

        [TestCase(true, null, true)]
        [TestCase(false, MerchandisingBucket.Online, false)]
        [TestCase(true, MerchandisingBucket.PendingApproval, true)]
        [TestCase(true, MerchandisingBucket.DoNotPost, false)]
        public void TestAdReviewNeededBin(bool adReviewNeeded, MerchandisingBucket? statusBucket, 
            bool expectedInBin)
        {
            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.NeedsAdReview).Returns(adReviewNeeded);
            mock.Setup(x => x.StatusBucket).Returns(statusBucket);

            var bin = Bin.Create(WorkflowType.AdReviewNeeded);

            Assert.That(bin.Filter(mock.Object), Is.EqualTo(expectedInBin));
        }

        [TestCase(null, null, true)]
        [TestCase(50, MerchandisingBucket.Online, false)]
        [TestCase(-1, MerchandisingBucket.PendingApproval, true)]
        [TestCase(-1, MerchandisingBucket.DoNotPost, false)]
        public void TestTrimNeededBin(int? chromeStyleId, MerchandisingBucket? statusBucket, 
            bool expectedInBin)
        {
            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.ChromeStyleId).Returns(chromeStyleId);
            mock.Setup(x => x.StatusBucket).Returns(statusBucket);

            var bin = Bin.Create(WorkflowType.TrimNeeded);

            Assert.That(bin.Filter(mock.Object), Is.EqualTo(expectedInBin));
        }
        
        [TestCase(true, 2, MerchandisingBucket.PendingApproval, false)]
        [TestCase(true, 2, MerchandisingBucket.DoNotPost, false)]
        [TestCase(true, 1, MerchandisingBucket.PendingApproval, false)]
        [TestCase(true, 1, MerchandisingBucket.DoNotPost, false)]
        [TestCase(false, 2, MerchandisingBucket.PendingApproval, true)]
        [TestCase(false, 2, MerchandisingBucket.DoNotPost, false)]
        [TestCase(false, 1, MerchandisingBucket.PendingApproval, false)]
        [TestCase(false, 1, MerchandisingBucket.DoNotPost, false)]
        public void TestNoBookValueBin(bool hasCurrentBookValue, int inventoryType, MerchandisingBucket? statusBucket, bool expectedInBin)
        {
            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.HasCurrentBookValue).Returns(hasCurrentBookValue);
            mock.Setup(x => x.InventoryType).Returns(inventoryType);
            mock.Setup(x => x.StatusBucket).Returns(statusBucket);
            mock.Setup(x => x.IsNew()).Returns(inventoryType == 1);
            mock.Setup(x => x.IsPreOwned()).Returns(inventoryType == 2);

            var bin = Bin.Create(WorkflowType.BookValueNeeded);

            Assert.That(bin.Filter(mock.Object), Is.EqualTo(expectedInBin));
        }

        [Test]
        public void TestCreateInitialAdBin()
        {
            var bin = Bin.Create(WorkflowType.CreateInitialAd);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.PendingApproval);
            Assert.IsTrue(bin.Filter(mock.Object));

            mock = new Mock<IInventoryData>(); 
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.Online);
            Assert.IsFalse(bin.Filter(mock.Object)); //moves to online shouldn't be in bin.
        }

        [Test]
        public void TestDueForRepricingBin()
        {
            var bin = Bin.Create(WorkflowType.DueForRepricing);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.IsDueForRepricing).Returns(DueForRepricingMode.DueForRepricing);
            Assert.IsTrue(bin.Filter(mock.Object));

            mock = new Mock<IInventoryData>();
            mock.Setup(x => x.IsDueForRepricing).Returns(DueForRepricingMode.NotDueForRepricing);
            Assert.IsFalse(bin.Filter(mock.Object));
        }

        [Test]
        public void TestOutdatedPriceBin()
        {
            var bin = Bin.Create(WorkflowType.OutdatedPrice);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.Online);
            mock.Setup(x => x.OutdatedPrice).Returns(true);
            Assert.IsTrue(bin.Filter(mock.Object));

            mock = new Mock<IInventoryData>();
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.Online);
            mock.Setup(x => x.OutdatedPrice).Returns(false);
            Assert.IsFalse(bin.Filter(mock.Object));

            new Mock<IInventoryData>();
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.PendingApproval);
            mock.Setup(x => x.OutdatedPrice).Returns(true);
            Assert.IsFalse(bin.Filter(mock.Object));
        }

        [TestCase(true, false, MerchandisingBucket.PendingApproval, true)]
        [TestCase(false, true, MerchandisingBucket.PendingApproval, true)]
        [TestCase(false, true, MerchandisingBucket.Online, true)]
        [TestCase(false, false, MerchandisingBucket.PendingApproval, false)]
        [TestCase(false, false, MerchandisingBucket.Online, false)]
        [TestCase(true, false, MerchandisingBucket.DoNotPost, false)]
        public void TestEquipmentNeededBin(bool interior, bool exterior, MerchandisingBucket statusBucket, bool isInBucket)
        {
            var bin = Bin.Create(WorkflowType.EquipmentNeeded);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.NeedsInterior).Returns(interior);
            mock.Setup(x => x.NeedsExterior).Returns(exterior);
            mock.Setup(x => x.StatusBucket).Returns(statusBucket);
            Assert.That(bin.Filter(mock.Object), Is.EqualTo(isInBucket));
        }

        [Test]
        public void TestNoPriceBin()
        {
            var bin = Bin.Create(WorkflowType.NoPrice);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.ListPrice).Returns(0);
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.Online);
            Assert.IsTrue(bin.Filter(mock.Object));

            mock = new Mock<IInventoryData>();
            mock.Setup(x => x.ListPrice).Returns(0);
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.DoNotPost);
            Assert.IsFalse(bin.Filter(mock.Object));
        }

        [TestCase(10, 5, MerchandisingBucket.PendingApproval, true, true)]
        [TestCase(10, 12, MerchandisingBucket.PendingApproval, true, false)]
        [TestCase(10, 5, MerchandisingBucket.Online, true, true)]
        [TestCase(0, 5, MerchandisingBucket.Online, true, false)]
        [TestCase(0, 0, MerchandisingBucket.Online, true, false)] // 0 photos is now a "no photos" car, not a "low photos" car (FB 21400)
        [TestCase(10, 5, MerchandisingBucket.Online, false, false)]
        public void TestLowPhotosBin(int desiredPhotoCount, int actualPhotoCount, MerchandisingBucket merchandisingBucket, bool needsPhotos, 
            bool expectedInBin)
        {
            var bin = Bin.Create(WorkflowType.LowPhotos);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.DesiredPhotoCount).Returns(desiredPhotoCount);
            mock.Setup(x => x.PhotoCount).Returns(actualPhotoCount);
            mock.Setup(x => x.StatusBucket).Returns(merchandisingBucket);
            mock.Setup(x => x.NeedsPhotos).Returns(needsPhotos);

            Assert.That(bin.Filter(mock.Object), Is.EqualTo(expectedInBin));
        }

        [TestCase(1, 1, MerchandisingBucket.PendingApproval, true, false)]
        [TestCase(1, 0, MerchandisingBucket.PendingApproval, true, true)]
        [TestCase(10, 1, MerchandisingBucket.Online, true, false)]
        [TestCase(10, 0, MerchandisingBucket.Online, true, true)]
        [TestCase(0, 0, MerchandisingBucket.Online, true, true)]
        [TestCase(0, 1, MerchandisingBucket.Online, true, false)]
        [TestCase(10, 5, MerchandisingBucket.Online, false, false)]
        [TestCase(10, 5, MerchandisingBucket.Online, true, false)]
        public void TestNoPhotosBin(int desiredPhotoCount, int actualPhotoCount, MerchandisingBucket merchandisingBucket, bool needsPhotos,
            bool expectedInBin)
        {
            var bin = Bin.Create(WorkflowType.NoPhotos);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.DesiredPhotoCount).Returns(desiredPhotoCount);
            mock.Setup(x => x.PhotoCount).Returns(actualPhotoCount);
            mock.Setup(x => x.StatusBucket).Returns(merchandisingBucket);
            mock.Setup(x => x.NeedsPhotos).Returns(needsPhotos);

            Assert.That(bin.Filter(mock.Object), Is.EqualTo(expectedInBin));
        }

        [Test]
        public void TestOptimizedEquipmentNeededBin()
        {
            var bin = Bin.Create(WorkflowType.OptimizedEquipmentNeeded);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.NeedsInterior).Returns(true);
            mock.Setup(x => x.NeedsExterior).Returns(false);
            mock.Setup(x => x.OutdatedPrice).Returns(false);
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.Online);
            Assert.IsTrue(bin.Filter(mock.Object));
        }

        [Test]
        public void TestOptimizedNoLowPhotosBin()
        {
            var bin = Bin.Create(WorkflowType.OptimizedNoLowPhotos);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.NeedsPhotos).Returns(true);
            mock.Setup(x => x.DesiredPhotoCount).Returns(10);
            mock.Setup(x => x.PhotoCount).Returns(5);
            mock.Setup(x => x.OutdatedPrice).Returns(false);
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.Online);
            Assert.IsTrue(bin.Filter(mock.Object));
        }

        [Test]
        public void TestLowActivityBin()
        {
            var bin = Bin.Create(WorkflowType.LowActivity);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.IsLowActivity).Returns(true);
            mock.Setup(x => x.OutdatedPrice).Returns(false);
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.Online);
            Assert.IsTrue(bin.Filter(mock.Object));
        }

        [Test]
        public void TestOfflineBin()
        {
            var bin = Bin.Create(WorkflowType.Offline);

            var mock = new Mock<IInventoryData>();
            mock.Setup(x => x.StatusBucket).Returns(MerchandisingBucket.DoNotPost);
            Assert.IsTrue(bin.Filter(mock.Object));
        }
    }
}
