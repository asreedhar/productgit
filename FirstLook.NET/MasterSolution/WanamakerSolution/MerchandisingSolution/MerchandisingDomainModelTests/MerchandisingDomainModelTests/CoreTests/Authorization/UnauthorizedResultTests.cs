﻿using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class UnauthorizedResultTests
    {
        private ControllerContext _context;
        private Mock<HttpContextBase> _httpContextMock;
        private Mock<HttpResponseBase> _httpResponseMock;
        private NameValueCollection _headers;
        private MemoryStream _outputStream;

        [SetUp]
        public void Setup()
        {
            _context = new ControllerContext();
            _httpContextMock = new Mock<HttpContextBase>();
            _httpResponseMock = new Mock<HttpResponseBase>();
            _headers = new NameValueCollection();
            _outputStream = new MemoryStream();

            _httpContextMock.Setup(m => m.Response).Returns(_httpResponseMock.Object);

            _httpResponseMock.SetupAllProperties();
            _httpResponseMock.Setup(m => m.Headers).Returns(_headers);
            _httpResponseMock.Setup(m => m.OutputStream).Returns(_outputStream);

            _context.HttpContext = _httpContextMock.Object;
        }

        [Test]
        public void ExecuteResult_should_set_StatusCode_to_401()
        {
            var result = new UnauthorizedResult {Message = "Unauthorized."};

            result.ExecuteResult(_context);

            Assert.That(_context.HttpContext.Response.StatusCode, Is.EqualTo(401));
        }

        [Test]
        public void ExecuteResult_should_set_StatusDescription_to_Message()
        {
            var result = new UnauthorizedResult { Message = "Unauthorized." };

            result.ExecuteResult(_context);

            Assert.That(_context.HttpContext.Response.StatusDescription, Is.EqualTo("Unauthorized."));
        }

        [Test]
        public void ExecuteResult_should_set_StatusDescription_to_Default_if_Message_is_null()
        {
            var result = new UnauthorizedResult();

            result.ExecuteResult(_context);

            Assert.That(_context.HttpContext.Response.StatusDescription, Is.EqualTo(UnauthorizedResult.DefaultMessage));
        }

        [Test]
        public void ExecuteResult_should_set_ContentType_to_TextHtml()
        {
            var result = new UnauthorizedResult { Message = "Unauthorized." };

            result.ExecuteResult(_context);

            Assert.That(_context.HttpContext.Response.ContentType, Is.EqualTo("text/html"));
        }

        [Test]
        public void ExecuteResult_should_set_Charset_to_Utf8()
        {
            var result = new UnauthorizedResult { Message = "Unauthorized." };

            result.ExecuteResult(_context);

            Assert.That(_context.HttpContext.Response.Charset, Is.EqualTo("utf-8"));
        }

        [Test]
        public void ExecuteResult_should_set_ContentLength_should_be_greater_than_zero()
        {
            var result = new UnauthorizedResult { Message = "Unauthorized." };

            result.ExecuteResult(_context);

            Assert.That(int.Parse(_context.HttpContext.Response.Headers.Get("Content-Length")), Is.GreaterThan(0));
        }

        [Test]
        public void ExecuteResult_should_output_message_to_output_content()
        {
            var result = new UnauthorizedResult { Message = "Unauthorized!" };

            result.ExecuteResult(_context);

            var content = Encoding.UTF8.GetString(_outputStream.ToArray());
            Assert.That(content, Is.StringContaining("Unauthorized!"));
        }
    }
}