﻿using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class UserPrincipalTests
    {
        [TestCase("the_user_name")]
        [TestCase("frank@gmail.com")]
        public void Identity_Name_should_return_UserName_from_inner_object(string providedUserName)
        {
            var user = new Mock<IUser>();
            user.Setup(u => u.UserName).Returns(providedUserName);

            var princ = new UserPrincipal(user.Object);

            Assert.That(princ.Identity.Name, Is.EqualTo(providedUserName));
        }

        [TestCase("CAS")]
        [TestCase("CoolAuth")]
        public void Identity_AuthenticationType_should_return_injected_parameter(string authenticationType)
        {
            var user = new Mock<IUser>();

            var princ = new UserPrincipal(user.Object, authenticationType);

            Assert.That(princ.Identity.AuthenticationType, Is.EqualTo(authenticationType));
        }

        [TestCase(null, false)]
        [TestCase("", false)]
        [TestCase("CAS", true)]
        public void Identity_IsAuthenticated_should_return_true_if_AuthenticationType_is_set(string authenticationType, bool shouldBeAuthenticated)
        {
            var user = new Mock<IUser>();

            var princ = new UserPrincipal(user.Object, authenticationType);

            Assert.That(princ.Identity.IsAuthenticated, Is.EqualTo(shouldBeAuthenticated));
        }

        [TestCase("Admin", false)]
        [TestCase("Reports", true)]
        [TestCase("Reader", true)]
        public void IsInRole_should_call_inner_IsInRole(string roleToTest, bool isInRole)
        {
            var user = new Mock<IUser>();
            user.Setup(u => u.IsInRole("Reports")).Returns(true);
            user.Setup(u => u.IsInRole("Reader")).Returns(true);

            var princ = new UserPrincipal(user.Object);

            Assert.That(princ.IsInRole(roleToTest), Is.EqualTo(isInRole));
        }

        [Test]
        public void User_should_return_inner_User()
        {
            var user = new Mock<IUser>();

            var princ = new UserPrincipal(user.Object);

            Assert.That(princ.User, Is.SameAs(user.Object));
        }
    }
}