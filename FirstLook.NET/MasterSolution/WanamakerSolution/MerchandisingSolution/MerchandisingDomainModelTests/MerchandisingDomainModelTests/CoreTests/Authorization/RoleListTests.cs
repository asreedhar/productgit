﻿using System.Linq;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class RoleListTests
    {
        [TestCase("RoleB", "RoleB")]
        [TestCase("RoleA", "RoleA")]
        [TestCase("RoleA,RoleB", "RoleA,RoleB")]
        [TestCase("RoleB,RoleA", "RoleB,RoleA")]
        [TestCase("  RoleB , RoleA ", "RoleB,RoleA", Description = "Whitespace should be supported.")]
        [TestCase("  RoleC ", "RoleC", Description = "Whitespace should be supported.")]
        [TestCase("  RoleA and C ", "RoleA and C", Description = "Inner whitespace should be supported.")]
        [TestCase("", "", Description = "Empty list should be supported.")]
        [TestCase(null, "", Description = "Empty list should be supported.")]
        [TestCase("  ", "", Description = "Empty list should be supported.")]
        public void List_should_return_parsed_input(string input, string expectedRoleListCsv)
        {
            var expectedRoleList = expectedRoleListCsv
                .Split(',')
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Select(s => s.Trim())
                .ToList();

            var list = new RoleList(input);

            Assert.That(list.List, Is.EqualTo(expectedRoleList));
        }

        [Test]
        public void Original_should_return_input_string()
        {
            var list = new RoleList("Some  ,  Values, With,White Space");

            Assert.That(list.List, Is.EqualTo(new[] { "Some", "Values", "With", "White Space"}));
            Assert.That(list.Original, Is.EqualTo("Some  ,  Values, With,White Space"));
        }
    }
}