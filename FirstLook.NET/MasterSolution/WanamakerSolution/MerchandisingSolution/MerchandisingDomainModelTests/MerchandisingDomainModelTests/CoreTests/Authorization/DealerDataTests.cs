﻿using System;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class DealerDataTests
    {
        private IDealer[] _dealers;

        [SetUp]
        public void Setup()
        {
            _dealers = new[]
                           {
                               MakeDealer(1, "ALLCAPS"),
                               MakeDealer(20, "MixedCase"),
                               MakeDealer(30, "alllower")
                           };
        }

        private static IDealer MakeDealer(int dealerId, string code)
        {
            var mock = new Mock<IDealer>();
            mock.Setup(m => m.Code).Returns(code);
            mock.Setup(m => m.Id).Returns(dealerId);
            return mock.Object;
        }

        [TestCase("MixedCase", 20)]
        [TestCase("mixedcase", 20)]
        [TestCase("MIXEDCASE", 20)]
        [TestCase("NotFound", null)]
        public void DealersByCode_should_ignore_case(string codeToLookup, int? expectedId)
        {
            var data = new DealerData(_dealers);

            IDealer dealer;
            data.DealersByCode.TryGetValue(codeToLookup, out dealer);

            Assert.That(dealer == null ? (int?)null : dealer.Id, Is.EqualTo(expectedId));
        }

        [TestCase("MixedCase", 1)]
        [TestCase("mixedcase", 1)]
        [TestCase("MIXEDCASE", 1)]
        [TestCase("NotFound", null)]
        public void Constructor_should_accept_duplicate_codes_and_DealersByCode_should_return_first_dealer_with_code(string codeToLookup, int? expectedId)
        {
            var dealers = new[]
                           {
                               MakeDealer(1, "MIXEDCASE"),
                               MakeDealer(20, "MixedCase"),
                               MakeDealer(30, "mixedcase")
                           };

            var data = new DealerData(dealers);

            IDealer dealer;
            data.DealersByCode.TryGetValue(codeToLookup, out dealer);

            Assert.That(dealer == null ? (int?)null : dealer.Id, Is.EqualTo(expectedId));
        }

        [Test]
        public void Constructor_with_Dealers_with_duplicate_Ids_should_throw()
        {
            var dealers = new[]
                           {
                               MakeDealer(1, "DuplicateId1"),
                               MakeDealer(1, "DuplicateId2"),
                               MakeDealer(1, "DuplicateId3")
                           };

            Assert.Throws<ArgumentException>(() => new DealerData(dealers));
        }
    }
}