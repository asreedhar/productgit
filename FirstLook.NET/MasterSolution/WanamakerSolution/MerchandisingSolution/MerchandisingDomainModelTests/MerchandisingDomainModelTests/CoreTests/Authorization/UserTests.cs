﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class UserTests
    {
        private Mock<IDealerServices> _dealerServicesMock;
        private IDealerServices _dealerServices;

        [SetUp]
        public void Setup()
        {
            _dealerServicesMock = new Mock<IDealerServices>();
            _dealerServices = _dealerServicesMock.Object;
        }

        [TestCase("reader", true, false)]
        [TestCase("administrator", false, false)]
        [TestCase("writer,reader", false, false)]
        [TestCase("administrator,approver", true, true)]
        public void IsAdmin_should_be_true_if_in_Administrator_role_and_provided_isAdmin_is_true(string roleList, bool isAdmin, bool expectedIsAdmin)
        {
            var roles = roleList.Split(',');
            var dealers = new int[0];

            var user = new User(7, null, "jsmith", "Jimmy", "Smith", isAdmin, roles, _dealerServices, dealers);

            var actualIsAdmin = user.IsAdmin;

            Assert.That(actualIsAdmin, Is.EqualTo(expectedIsAdmin));
        }

        [TestCase("admin", true)]
        [TestCase("Admin", true, Description =  "Should be case insensitive")]
        [TestCase("mobile", true)]
        [TestCase("writer", false)]
        [TestCase(null, false, Description = "Should handle nulls ok")]
        public void IsInRole_should_test_against_role_list_and_be_case_insensitive(string roleToTest, bool shouldBeInRole)
        {
            var roles = new[] {"admin", "reader", "mobile"};
            var dealers = new[] {0};

            var user = new User(5, null, "testuser", "Test", "User", true, roles, _dealerServices, dealers);

            var isInRole = user.IsInRole(roleToTest);

            Assert.That(isInRole, Is.EqualTo(shouldBeInRole));
        }

        [TestCase(105239, true)]
        [TestCase(104000, true)]
        [TestCase(105111, false)]
        [TestCase(null, false, Description = "Should handle null IDealer")]
        public void HasAccessTo_should_test_business_unit_ids_against_allowed_list(int? businessUnitIdToTest, bool shouldHaveAccess)
        {
            var dealerToTest = MakeDealer(businessUnitIdToTest);

            var dealers = new[] {105239, 104000, 192003};
            var roles = new string[0];

            var user = new User(6, null, "bob", "Robert", "Smith", false, roles, _dealerServices, dealers);

            var hasAccess = user.HasAccessTo(dealerToTest);

            Assert.That(hasAccess, Is.EqualTo(shouldHaveAccess));
        }

        [TestCase(105239, true)]
        [TestCase(104000, true)]
        [TestCase(105111, true)]
        [TestCase(null, false, Description = "Even admins can't access null dealers")]
        public void HasAccessTo_should_allow_access_to_all_business_units_when_IsAdmin(int? businessUnitIdToTest, bool shouldHaveAccess)
        {
            var dealerToTest = MakeDealer(businessUnitIdToTest);

            var dealers = new[] { 105239, 104000, 192003 };
            var roles = new[] {"administrator", "approver"};

            var user = new User(6, null, "bob", "Robert", "Smith", true, roles, _dealerServices, dealers);

            var hasAccess = user.HasAccessTo(dealerToTest);

            Assert.That(hasAccess, Is.EqualTo(shouldHaveAccess));
        }

        [TestCase("administrator,approver", true, "105239,104000,192003", "25,35,45,105239,104000", Description = "IsAdmin should see all dealers in repository.")]
        [TestCase("approver", false, "105239,104000,192003", "105239,104000", Description = "Non-admin should see intersection of dealers he has access to and dealers in repository.")]
        public void GetDealers_should_return_list_of_dealers_from_repository_that_user_has_access_to(
            string rolesCsv, bool isAdmin, string dealersUserHasAccessToCsv, string expectedDealerIdsCsv)
        {
            var dealerRepository = new[] {MakeDealer(25), MakeDealer(35), MakeDealer(45), MakeDealer(105239), MakeDealer(104000)};
            _dealerServicesMock.Setup(ds => ds.GetAllDealers()).Returns(dealerRepository);
            _dealerServicesMock.Setup(ds => ds.GetDealersById(It.IsAny<IEnumerable<int>>()))
                .Returns<IEnumerable<int>>(
                    ids => ids.Select(id => dealerRepository.FirstOrDefault(d => d.Id == id)).Where(d => d != null));

            var dealers =
                dealersUserHasAccessToCsv.Split(',').Where(d => !string.IsNullOrWhiteSpace(d)).Select(int.Parse)
                .ToArray();
            var roles = rolesCsv.Split(',').Where(r => !string.IsNullOrEmpty(r)).ToArray();
            var expectedDealerIds =
                expectedDealerIdsCsv.Split(',').Where(d => !string.IsNullOrEmpty(d)).Select(int.Parse).ToArray();

            var user = new User(6, null, "bob", "Robert", "Smith", true, roles, _dealerServices, dealers);

            var result = user.GetDealers();

            Assert.That(result.Select(d => d.Id).ToArray(), Is.EquivalentTo(expectedDealerIds));
        }

        private static IDealer MakeDealer(int? businessUnitIdToTest)
        {
            if (businessUnitIdToTest == null)
                return null;

            var dealer = new Mock<IDealer>();
            dealer.Setup(d => d.Id).Returns(businessUnitIdToTest.Value);
            return dealer.Object;
        }
    }
}