﻿using System;
using System.Collections.Generic;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class DealerTests
    {
        private static ISet<DealerUpgradeKey> CreateSampleUpgrades()
        {
            return new HashSet<DealerUpgradeKey>(
                new[]
                    {
                        new DealerUpgradeKey(105239, Upgrade.Merchandising),
                        new DealerUpgradeKey(105239, Upgrade.WebSitePDF),
                        new DealerUpgradeKey(105239, Upgrade.PingTwo),
                        new DealerUpgradeKey(105250, Upgrade.TradeAnalyzer),
                        new DealerUpgradeKey(105250, Upgrade.Merchandising)
                    });
        }

        [TestCase(105239, Upgrade.Merchandising, true)]
        [TestCase(105239, Upgrade.PingTwo, true)]
        [TestCase(105239, Upgrade.Redistribution, false)]
        [TestCase(105250, Upgrade.TradeAnalyzer, true)]
        [TestCase(100517, Upgrade.Merchandising, false)]
        public void HasUpgrade_should_return_correct_status_for_the_given_upgrade(int buId, Upgrade upgradeToCheck,
                                                                                  bool expectedStatus)
        {
            var dealer = new Dealer(buId, default(Guid), "CODE", "Name", CreateSampleUpgrades(), false);

            var status = dealer.HasUpgrade(upgradeToCheck);

            Assert.That(status, Is.EqualTo(expectedStatus));
        }
    }
}