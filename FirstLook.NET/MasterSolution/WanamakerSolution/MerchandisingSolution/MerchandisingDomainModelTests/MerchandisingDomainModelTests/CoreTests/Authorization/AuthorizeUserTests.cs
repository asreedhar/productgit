﻿using System.Linq;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class AuthorizeUserTests
    {
        private Mock<IUserServices> _userServicesMock;
        private IUserServices _userServices;

        private Mock<IUser> _userMock;
        private IUser _user;

        private UserPrincipal _principal;

        [SetUp]
        public void Setup()
        {
            _userServicesMock = new Mock<IUserServices>();
            _userServices = _userServicesMock.Object;
            
            _userMock = new Mock<IUser>();
            _user = _userMock.Object;

            _principal = new UserPrincipal(_user);
            _userServicesMock.Setup(us => us.GetUser(_principal)).Returns(_user);
        }

        [Test]
        public void IsAuthorized_should_be_false_when_GetUser_returns_null()
        {
            _userServicesMock.Setup(us => us.GetUser(_principal)).Returns((IUser)null);

            var authorizeUser = new AuthorizeUser(_userServices);

            var result = authorizeUser.IsAuthorized(_principal);

            Assert.That(result.IsAuthorized, Is.False);
        }

        [TestCase("RoleA", "RoleB", false)]
        [TestCase("RoleA", "RoleA", true)]
        [TestCase("RoleA,RoleB", "RoleA", true)]
        [TestCase("RoleA,RoleB", "RoleB", true)]
        [TestCase("RoleA,RoleB", "RoleA,RoleB", true)]
        [TestCase("RoleA,RoleB", "RoleB,RoleA", true)]
        [TestCase("RoleA,RoleB", "RoleC,RoleA", false)]
        [TestCase("RoleA and C", "RoleA and C", true, Description = "Inner whitespace should be supported.")]
        [TestCase("RoleA and C", "RoleC and B", false, Description = "Inner whitespace should be supported.")]
        [TestCase("RoleA and C", "", true, Description = "No asserts should be supported.")]
        public void Authenticated_user_with_access_to_specified_roles_and_specific_assertAllRoles_should_have_expected_authorization(
            string userHasRolesCsv, string assertAllRolesCsv, bool expectedAuthorization)
        {
            var userHasRoles =
                userHasRolesCsv.Split(',').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            _userMock.Setup(u => u.IsInRole(It.IsAny<string>())).Returns<string>(userHasRoles.Contains);

            var assertAllRoles =
                assertAllRolesCsv.Split(',').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            var authorizeUser = new AuthorizeUser(_userServices);

            var result = authorizeUser.IsAuthorized(_principal, assertAllRoles);

            Assert.That(result.IsAuthorized, Is.EqualTo(expectedAuthorization));
        }

        [TestCase("RoleA", "RoleB", false)]
        [TestCase("RoleA", "RoleA", true)]
        [TestCase("RoleA,RoleB", "RoleA", true)]
        [TestCase("RoleA,RoleB", "RoleB", true)]
        [TestCase("RoleA,RoleB", "RoleA,RoleB", true)]
        [TestCase("RoleA,RoleB", "RoleB,RoleA", true)]
        [TestCase("RoleA,RoleB", "RoleC,RoleA", true)]
        [TestCase("RoleA and C", "RoleA and C,Admin", true, Description = "Inner whitespace should be supported.")]
        [TestCase("RoleA and C", "RoleC and B", false, Description = "Inner whitespace should be supported.")]
        [TestCase("RoleA and C", "", true, Description = "No asserts should be supported.")]
        public void Authenticated_user_with_access_to_specified_roles_and_specific_assertAnyRoles_should_have_expected_authorization(
            string userHasRolesCsv, string assertAnyRolesCsv, bool expectedAuthorization)
        {
            var userHasRoles =
                userHasRolesCsv.Split(',').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            _userMock.Setup(u => u.IsInRole(It.IsAny<string>())).Returns<string>(userHasRoles.Contains);

            var assertAnyRoles =
                assertAnyRolesCsv.Split(',').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            var authorizeUser = new AuthorizeUser(_userServices);

            var result = authorizeUser.IsAuthorized(_principal, assertAnyRoles: assertAnyRoles);

            Assert.That(result.IsAuthorized, Is.EqualTo(expectedAuthorization));
        }
    }
}