﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;


namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class DealerServicesTests
    {
        private static IDealerRepository GetSampleRepository(IDealerData data = null)
        {
            var repositoryMock = new Mock<IDealerRepository>();
            repositoryMock
                .Setup(r => r.GetData())
                .Returns(() => data ?? new DealerData(GetSampleDealers()));
            return repositoryMock.Object;
        }

        private static IList<IDealer> GetSampleDealers()
        {
            return new[]
                       {
                           CreateDealer(105239, "WINDYCIT05", "Windy City BMW"),
                           CreateDealer(100540, "CJD17", "Chrysler Jeep and Dodge"),
                           CreateDealer(215017, "EDSEL5", "Edsel of Austin")
                       };
        }

        private static IDealer CreateDealer(int id, string code, string name, Guid handle = default(Guid), Upgrade[] upgrades = null)
        {
            upgrades = upgrades ?? new Upgrade[0];
            var mock = new Mock<IDealer>();
            mock.Setup(d => d.Id).Returns(id);
            mock.Setup(d => d.Code).Returns(code);
            mock.Setup(d => d.Name).Returns(name);
            mock.Setup(d => d.OwnerHandle).Returns(handle);
            mock.Setup(d => d.HasUpgrade(It.IsAny<Upgrade>())).Returns<Upgrade>(
                upgrade => upgrades.Contains(upgrade));
            return mock.Object;
        }

        [TestCase(105239, "WINDYCIT05")]
        [TestCase(215017, "EDSEL5")]
        public void GetDealerById_should_return_dealer_when_given_valid_id(int idToLookup, string expectedCode)
        {
            var services = new DealerServices(GetSampleRepository());

            var dealer = services.GetDealerById(idToLookup);

            Assert.That(dealer.Code, Is.EqualTo(expectedCode));
        }

        [TestCase(23234)]
        [TestCase(654632)]
        public void GetDealerById_should_return_null_when_given_invalid_id(int idToLookup)
        {
            var services = new DealerServices(GetSampleRepository());

            var dealer = services.GetDealerById(idToLookup);

            Assert.That(dealer, Is.Null);
        }
        
        [TestCase("WINDYCIT05", 105239)]
        [TestCase("CJD17", 100540)]
        public void GetDealerByCode_should_return_dealer_when_given_valid_code(string codeToLookup, int expectedId)
        {
            var services = new DealerServices(GetSampleRepository());

            var dealer = services.GetDealerByCode(codeToLookup);

            Assert.That(dealer.Id, Is.EqualTo(expectedId));
        }

        [TestCase("BENTLEY007")]
        public void GetDealerByCode_should_return_null_when_given_invalid_code(string codeToLookup)
        {
            var services = new DealerServices(GetSampleRepository());

            var dealer = services.GetDealerByCode(codeToLookup);

            Assert.That(dealer, Is.Null);
        }

        [Test]
        public void GetDealerByCode_with_duplicate_codes_should_return_first_match()
        {
            var services = new DealerServices(GetSampleRepository(
                new DealerData(
                    new[]
                        {
                            CreateDealer(1020, "BUC", "Name1"),
                            CreateDealer(1015, "CD1", "Name2"),
                            CreateDealer(1011, "CD1", "Name3")
                        })));

            var dealer = services.GetDealerByCode("CD1");

            Assert.That(dealer.Id, Is.EqualTo(1015));
        }

        [Test]
        public void GetAllDealers_should_return_full_list_of_dealers()
        {
            var services = new DealerServices(GetSampleRepository());

            var dealers = services.GetAllDealers().ToArray();

            Assert.That(dealers.Count(), Is.EqualTo(3));
            Assert.That(dealers.Select(d => d.Id).ToArray(), Is.EquivalentTo(new[] {105239, 100540, 215017}));
        }
        
        [TestCase("105239, 100540, 215017", "105239, 100540, 215017")]
        [TestCase("300, 301", "")]
        [TestCase("300, 301, 105239", "105239")]
        [TestCase("", "")]
        public void GetDealersById_should_return_filtered_list_of_dealers(string dealerIdsCsv, string expectedDealerIdCsv)
        {
            var idsToGet = dealerIdsCsv.Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(int.Parse).ToArray();
            var expectedIds = expectedDealerIdCsv.Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(int.Parse).ToArray();

            var services = new DealerServices(GetSampleRepository());

            var dealers = services.GetDealersById(idsToGet).ToArray();

            Assert.That(dealers.Select(d => d.Id).ToArray(), Is.EquivalentTo(expectedIds));
        }
    }
}