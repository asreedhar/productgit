﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class UserCachedRepositoryTests
    {
        private Dictionary<string, object> _cacheData;
        private ICache _cache;
        private IUser _data;
        private Mock<IUserRepository> _innerMock;
        private IUserRepository _inner;
        private Mock<ICache> _cacheMock;

        [SetUp]
        public void Setup()
        {
            SetupCache();
            _data = new Mock<IUser>().Object;
            _innerMock = new Mock<IUserRepository>();
            _inner = _innerMock.Object;
        }

        private void SetupCache()
        {
            _cacheData = new Dictionary<string, object>();
            _cacheMock = new Mock<ICache>();
            _cacheMock
                .Setup(c => c.Get(It.IsAny<string>()))
                .Returns<string>(k =>
                {
                    object val;
                    _cacheData.TryGetValue(k, out val);
                    return val;
                });
            _cacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>()))
                .Callback<string, object>((k, v) => _cacheData[k] = v);
            _cacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((k, v, t) => _cacheData[k] = v);
            _cacheMock.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<TimeSpan>()))
                .Callback<string, object, TimeSpan>((k, v, t) => _cacheData[k] = v);
            _cacheMock.Setup(c => c.Delete(It.IsAny<string>()))
                .Callback<string>(k => _cacheData.Remove(k));
            _cache = _cacheMock.Object;
        }

        [Test]
        public void GetUser_once_should_call_inner_GetUser()
        {
            var cachedRepository = new UserCachedRepository(_cache, _inner);

            _innerMock.Setup(r => r.GetUser("Foo")).Returns(_data);

            var user = cachedRepository.GetUser("Foo");

            Assert.That(user, Is.SameAs(_data));
            _innerMock.Verify(r => r.GetUser(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetUser_twice_should_call_inner_once()
        {
            var cachedRepository = new UserCachedRepository(_cache, _inner);

            _innerMock.Setup(r => r.GetUser("Foo")).Returns(_data);

            var user1 = cachedRepository.GetUser("Foo");
            var user2 = cachedRepository.GetUser("Foo");

            Assert.That(user1, Is.SameAs(_data));
            Assert.That(user2, Is.SameAs(_data));
            _innerMock.Verify(r => r.GetUser("Foo"), Times.Once());
            _cacheMock.Verify(r => r.Get(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void GetUser_with_null_username_should_throw()
        {
            var cachedRepository = new UserCachedRepository(_cache, _inner);

            Assert.Throws<ArgumentNullException>(() => cachedRepository.GetUser(null));
        }

        [Test]
        public void GetUser_should_return_null_when_inner_returns_null()
        {
            var cachedRepository = new UserCachedRepository(_cache, _inner);

            _innerMock.Setup(r => r.GetUser("Foo")).Returns(_data);

            var user1 = cachedRepository.GetUser("A_Different_User");

            Assert.That(user1, Is.Null);
            _innerMock.Verify(r => r.GetUser(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void GetUser_should_also_cache_inner_null_return_values()
        {
            var cachedRepository = new UserCachedRepository(_cache, _inner);

            _innerMock.Setup(r => r.GetUser("Foo")).Returns(_data);

            var user1 = cachedRepository.GetUser("A_Different_User");
            var user2 = cachedRepository.GetUser("A_Different_User");

            Assert.That(user1, Is.Null);
            Assert.That(user2, Is.Null);
            _innerMock.Verify(r => r.GetUser(It.IsAny<string>()), Times.Once());
            _cacheMock.Verify(r => r.Get(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void InvalidUserCache_should_clear_the_cache_for_a_single_username()
        {
            var cachedRepository = new UserCachedRepository(_cache, _inner);

            _innerMock.Setup(r => r.GetUser("Foo")).Returns(_data);

            var user1 = cachedRepository.GetUser("Foo");
            cachedRepository.InvalidateUserCache("Foo");
            var user2 = cachedRepository.GetUser("Foo");

            Assert.That(user1, Is.SameAs(_data));
            Assert.That(user2, Is.SameAs(_data));
            _innerMock.Verify(r => r.GetUser(It.IsAny<string>()), Times.Exactly(2));
            _cacheMock.Verify(r => r.Get(It.IsAny<string>()), Times.Exactly(2));
        }

        [Test]
        public void InvalidUserCache_with_null_username_should_throw()
        {
            var cachedRepository = new UserCachedRepository(_cache, _inner);

            Assert.Throws<ArgumentNullException>(() => cachedRepository.InvalidateUserCache(null));
        }
    }
}