﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class AuthorizeDealerTests
    {
        private Mock<IUserServices> _userServicesMock;
        private IUserServices _userServices;
        private Mock<IDealerServices> _dealerServicesMock;
        private IDealerServices _dealerServices;
        private Mock<IUser> _userMock;
        private IUser _user;
        private GenericPrincipal _principal;
        private Mock<IDealer> _dealerMock;
        private IDealer _dealer;

        [SetUp]
        public void Setup()
        {
            _userServicesMock = new Mock<IUserServices>();
            _userServices = _userServicesMock.Object;

            _dealerServicesMock = new Mock<IDealerServices>();
            _dealerServices = _dealerServicesMock.Object;

            _userMock = new Mock<IUser>();
            _user = _userMock.Object;

            _principal = new GenericPrincipal(new GenericIdentity("the_user"), new string[0]);
            _userServicesMock.Setup(us => us.GetUser(_principal)).Returns(_user);

            _dealerMock = new Mock<IDealer>();
            _dealer = _dealerMock.Object;
            _dealerServicesMock.Setup(ds => ds.GetDealerByCode("WINDYCIT05")).Returns(_dealer);
        }

        [Test]
        public void IsAuthenticated_with_user_not_found_in_repository_should_not_return_IsAuthorized()
        {
            _userServicesMock.Setup(us => us.GetUser(_principal)).Returns((IUser) null);

            var authorizeDealer = new AuthorizeDealer(_userServices, _dealerServices);

            var result = authorizeDealer.IsAuthorized(_principal, "WINDYCIT05");

            Assert.That(result.IsAuthorized, Is.False);
        }

        [TestCase("WINDYCIT05", true)]
        [TestCase("WINDYCIT02", false)]
        [TestCase("", false)]
        [TestCase(null, false)]
        public void IsAuthenticated_should_return_IsAuthorized_only_if_dealer_code_found(string dealerCode, bool shouldBeAuthorized)
        {
            _userMock.Setup(u => u.HasAccessTo(It.IsAny<IDealer>())).Returns(true);

            var authorizeDealer = new AuthorizeDealer(_userServices, _dealerServices);

            var result = authorizeDealer.IsAuthorized(_principal, dealerCode);

            Assert.That(result.IsAuthorized, Is.EqualTo(shouldBeAuthorized));
        }

        [TestCase(true, true)]
        [TestCase(false, false)]
        public void IsAuthenticated_should_return_IsAuthorized_only_if_use_has_access_to_specified_dealer(bool hasDealerAccess, bool shouldBeAuthorized)
        {
            _userMock.Setup(u => u.HasAccessTo(It.IsAny<IDealer>())).Returns(hasDealerAccess);

            var authorizeDealer = new AuthorizeDealer(_userServices, _dealerServices);

            var result = authorizeDealer.IsAuthorized(_principal, "WINDYCIT05");

            Assert.That(result.IsAuthorized, Is.EqualTo(shouldBeAuthorized));
        }

        [TestCase("", "Merchandising", false)]
        [TestCase("MarketData", "MakeADeal", false)]
        [TestCase("MarketData", "MarketData", true)]
        [TestCase("MarketData,Ping", "MakeADeal", false)]
        [TestCase("MarketData,Ping", "MarketData", true)]
        [TestCase("MarketData,Ping", "MarketData,Ping", true)]
        [TestCase("MarketData,Ping", "Ping,MarketData", true)]
        [TestCase("MarketData,Ping", "Ping,MarketData,Merchandising", false)]
        [TestCase("MarketData,Ping", "", true, Description = "AssertDealerUpgrades should accept an empty string.")]
        public void IsAuthenticated_with_access_to_specific_dealer_upgrades_and_specified_AssertDealerUpgrades_should_have_expected_authorization(
            string actualDealerUpgradesCsv, string assertDealerUpgradesCsv, bool expectedAuthorization)
        {
            _userMock.Setup(u => u.HasAccessTo(It.IsAny<IDealer>())).Returns(true);

            var dealersUpgrades =
                new HashSet<Upgrade>(
                    actualDealerUpgradesCsv.Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(s => (Upgrade)Enum.Parse(typeof(Upgrade), s, true)));
            _dealerMock.Setup(d => d.HasUpgrade(It.IsAny<Upgrade>())).Returns<Upgrade>(dealersUpgrades.Contains);

            var assertDealerUpgrades =
                assertDealerUpgradesCsv
                    .Split(',')
                    .Where(s => !string.IsNullOrEmpty(s))
                    .Select(s => (Upgrade) Enum.Parse(typeof (Upgrade), s))
                    .ToList();

            var authorizeDealer = new AuthorizeDealer(_userServices, _dealerServices);

            var result = authorizeDealer.IsAuthorized(_principal, "WINDYCIT05", assertDealerUpgrades);

            Assert.That(result.IsAuthorized, Is.EqualTo(expectedAuthorization));
        }

        [TestCase(false, false, true)]
        [TestCase(false, true, true)]
        [TestCase(true, false, false)]
        [TestCase(true, true, true)]
        public void IsAuthenticated_with_specific_assertWebLoaderEnabled_and_specific_dealer_WebLoaderEnabled_value_should_produce_expected_authorization(
            bool assertWebLoaderUpgrade, bool dealerHasWebLoaderUpgrade, bool expectedAuthorization)
        {
            _dealerMock.Setup(d => d.HasWebLoader).Returns(dealerHasWebLoaderUpgrade);
            _userMock.Setup(u => u.HasAccessTo(It.IsAny<IDealer>())).Returns(true);
            
            var authorizeDealer = new AuthorizeDealer(_userServices, _dealerServices);

            var result = authorizeDealer.IsAuthorized(_principal, "WINDYCIT05", assertDealerHasWebLoaderUpgrade: assertWebLoaderUpgrade);

            Assert.That(result.IsAuthorized, Is.EqualTo(expectedAuthorization));
        }
    }
}