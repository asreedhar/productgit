﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Core.Authorization.Mvc;
using Moq;
using NUnit.Framework;

// ReSharper disable UseObjectOrCollectionInitializer

namespace MerchandisingDomainModelTests.CoreTests.Authorization.Mvc
{
    [TestFixture]
    public class AuthorizeUserAttributeTests
    {
        private Mock<IUnauthorizedResultFactory> _unauthorizedResultFactoryMock;
        private IUnauthorizedResultFactory _unauthorizedResultFactory;
        
        private Mock<IAuthorizeUser> _authorizeUserMock;
        private IAuthorizeUser _authorizeUser;

        private AuthorizationContext _authorizationContext;

        [SetUp]
        public void Setup()
        {
            _unauthorizedResultFactoryMock = new Mock<IUnauthorizedResultFactory>();
            _unauthorizedResultFactoryMock.Setup(u => u.CreateResult(It.IsAny<string>())).Returns(
                () => new HttpUnauthorizedResult());
            _unauthorizedResultFactory = _unauthorizedResultFactoryMock.Object;

            _authorizeUserMock = new Mock<IAuthorizeUser>();
            _authorizeUser = _authorizeUserMock.Object;

            _authorizationContext = new AuthorizationContext();
            var httpContextMock = new Mock<HttpContextBase>();
            _authorizationContext.HttpContext = httpContextMock.Object;
            httpContextMock.Setup(c => c.User).Returns(new GenericPrincipal(new GenericIdentity("pstephens"),
                                                                            new string[0]));
        }

        private void AssertIsAuthorized()
        {
            AssertAuthorization(true);
        }

        private void AssertIsNotAuthorized()
        {
            AssertAuthorization(false);
        }

        private void AssertAuthorization(bool shouldBeAuthorized)
        {
            Assert.That(_authorizationContext.Result, shouldBeAuthorized ? Is.Null : Is.Not.Null);
        }

        [Test]
        public void Unauthorized_user_should_not_be_authorized()
        {
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(new AuthorizationResult("Not authenticated!"));

            var authorizeUser = new AuthorizeUserAttribute(_authorizeUser, _unauthorizedResultFactory);

            authorizeUser.OnAuthorization(_authorizationContext);

            AssertIsNotAuthorized();
        }

        [Test]
        public void Authorized_user_should_be_authorized()
        {
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(new AuthorizationResult(null));

            var authorizeUser = new AuthorizeUserAttribute(_authorizeUser, _unauthorizedResultFactory);

            authorizeUser.OnAuthorization(_authorizationContext);

            AssertIsAuthorized();
        }

        [Test]
        public void AssertAllRoles_should_be_passed_to_inner_IsAuthenticated()
        {
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(new AuthorizationResult(null));

            var authorizeUser = new AuthorizeUserAttribute(_authorizeUser, _unauthorizedResultFactory)
                                    {
                                        AssertAllRoles = "PowerUser, QualityAssurance, Peon"
                                    };

            authorizeUser.OnAuthorization(_authorizationContext);

            _authorizeUserMock.Verify(au => au.IsAuthorized(It.IsAny<IPrincipal>(), 
                new [] { "PowerUser", "QualityAssurance", "Peon" }, It.IsAny<IList<string>>()));
        }

        [Test]
        public void AssertAnyRoles_should_be_passed_to_inner_IsAuthenticated()
        {
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(new AuthorizationResult(null));

            var authorizeUser = new AuthorizeUserAttribute(_authorizeUser, _unauthorizedResultFactory)
            {
                AssertAnyRoles = "Foo,Root"
            };

            authorizeUser.OnAuthorization(_authorizationContext);

            _authorizeUserMock.Verify(au => au.IsAuthorized(It.IsAny<IPrincipal>(),
                It.IsAny<IList<string>>(), new [] { "Foo", "Root"}));
        }
    }
}