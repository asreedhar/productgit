﻿using System.Security.Principal;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class UserServicesTests
    {
        private IPrincipal _savedPrincipal;

        private Mock<IUserRepository> _userRepositoryMock;
        private IUserRepository _userRepository;

        private Mock<IUser> _userMock;
        private IUser _user;

        private Mock<ICurrentPrincipal> _currentPrincipalMock;
        private ICurrentPrincipal _currentPrincipal;

        private Mock<IAuthenticateUser> _authenticateUserMock;
        private IAuthenticateUser _authenticateUser;

        [SetUp]
        public void Setup()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _userRepository = _userRepositoryMock.Object;

            _userMock = new Mock<IUser>();
            _user = _userMock.Object;

            _currentPrincipalMock = new Mock<ICurrentPrincipal>();
            _currentPrincipalMock.SetupAllProperties();
            _currentPrincipal = _currentPrincipalMock.Object;

            _authenticateUserMock = new Mock<IAuthenticateUser>();
            _authenticateUser = _authenticateUserMock.Object;
        }

        [Test]
        public void Current_should_return_user_object_reflected_by_current_IPrincipal()
        {
            _currentPrincipal.Current = new GenericPrincipal(new GenericIdentity("pstephens"), new string[0]);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns(_user);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.Current;

            Assert.That(user, Is.EqualTo(_user));
        }

        [Test]
        public void GetUser_should_return_user_object_reflected_by_supplied_IPrincipal()
        {
            var principal = new GenericPrincipal(new GenericIdentity("pstephens2"), new string[0]);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens2")).Returns(_user);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.GetUser(principal);

            Assert.That(user, Is.SameAs(_user));
        }

        [Test]
        public void Current_should_return_null_when_inner_repository_returns_null()
        {
            _currentPrincipal.Current = new GenericPrincipal(new GenericIdentity("pstephens"), new string[0]);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.Current;

            Assert.That(user, Is.Null);
        }

        [Test]
        public void GetUser_should_return_null_when_inner_repository_returns_null()
        {
            var principal = new GenericPrincipal(new GenericIdentity("pstephens2"), new string[0]);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephen2s")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.GetUser(principal);

            Assert.That(user, Is.Null);
        }

        [Test]
        public void Current_should_return_null_when_thread_Principal_is_null()
        {
            _currentPrincipal.Current = null;
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.Current;

            Assert.That(user, Is.Null);
        }

        [Test]
        public void GetUser_should_return_null_when_passed_null_Principal()
        {
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.GetUser(null);

            Assert.That(user, Is.Null);
        }

        [Test]
        public void Current_should_return_null_when_thread_Principal_Identity_is_null()
        {
            _currentPrincipal.Current = new CustomPrincipal(null);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.Current;

            Assert.That(user, Is.Null);
        }

        [Test]
        public void GetUser_should_return_null_when_supplied_Principal_Identity_is_null()
        {
            var principal = new CustomPrincipal(null);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.GetUser(principal);

            Assert.That(user, Is.Null);
        }

        [Test]
        [Ignore("I'm not sure what was supposed to be tested here, but CustomIdentity is written in such a way that it will always throw an exception.")]
        public void Current_should_return_null_when_thread_Principal_Identity_username_is_null()
        {
            _currentPrincipal.Current = new GenericPrincipal(new CustomIdentity(), new string[0]);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.Current;

            Assert.That(user, Is.Null);
        }

        [Test]
        [Ignore("I'm not sure what was supposed to be tested here, but CustomIdentity is written in such a way that it will always throw an exception.")]
        public void GetUser_should_return_null_when_supplied_Principal_Identity_username_is_null()
        {
            var principal = new GenericPrincipal(new CustomIdentity(), new string[0]);
            _userRepositoryMock.Setup(ur => ur.GetUser("pstephens")).Returns((IUser)null);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.GetUser(principal);

            Assert.That(user, Is.Null);
        }

        [Test]
        public void Current_should_be_able_to_unwrap_user_from_IPrincipal_if_it_implements_IUserProvider()
        {
            _userMock.Setup(u => u.UserName).Returns("joe");
            _userRepositoryMock.Setup(ur => ur.GetUser("joe")).Returns(_user);
            var princ = new UserPrincipal(_user);
            _currentPrincipal.Current = princ;

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.Current;

            Assert.That(user, Is.SameAs(_user));
            _userRepositoryMock.Verify(r => r.GetUser(It.IsAny<string>()), Times.Never());
        }

        [Test]
        public void GetUser_should_be_able_to_unwrap_user_from_IPrincipal_if_it_implements_IUserProvider()
        {
            _userMock.Setup(u => u.UserName).Returns("joe");
            _userRepositoryMock.Setup(ur => ur.GetUser("joe")).Returns(_user);
            var principal = new UserPrincipal(_user);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var user = userServices.GetUser(principal);

            Assert.That(user, Is.SameAs(_user));
            _userRepositoryMock.Verify(r => r.GetUser(It.IsAny<string>()), Times.Never());
        }

        [TestCase("joe", "pwd", true)]
        [TestCase("tom", "h@ck0r", false)]
        public void AuthenticateAndSetCurrent_should_return_true_if_inner_Authenticate_returns_true(string username, string password, bool shouldBeAuthenticated)
        {
            _userRepositoryMock.Setup(ur => ur.GetUser(It.IsAny<string>())).Returns(_user);
            _authenticateUserMock.Setup(au => au.Authenticate(new BasicCredential("joe", "pwd"))).Returns("CAS");

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var isAuthenticated = userServices.AuthenticateAndSetCurrent(new BasicCredential(username, password));

            Assert.That(isAuthenticated, Is.EqualTo(shouldBeAuthenticated));
        }

        [TestCase("joe", "pwd", true)]
        [TestCase("tom", "123", false)]
        public void AuthenticateAndSetCurrent_should_return_false_if_user_does_not_exist_in_repository(string username, string password, bool shouldBeAuthenticated)
        {
            _userRepositoryMock.Setup(ur => ur.GetUser("joe")).Returns(_user);
            _authenticateUserMock.Setup(au => au.Authenticate(It.IsAny<BasicCredential>())).Returns("CAS");

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            var isAuthenticated = userServices.AuthenticateAndSetCurrent(new BasicCredential(username, password));

            Assert.That(isAuthenticated, Is.EqualTo(shouldBeAuthenticated));
        }
        
        [TestCase("joe", "joe")]
        [TestCase(null, "")]
        [TestCase("pstephens", "")]
        public void AuthenticateAndSetCurrent_should_set_Principal_if_username_found_in_repository(string userNameToSet, string expectedUserName)
        {
            _userRepositoryMock.Setup(ur => ur.GetUser("joe")).Returns(_user);
            _authenticateUserMock.Setup(au => au.Authenticate(It.IsAny<BasicCredential>())).Returns("CAS");
            _userMock.Setup(u => u.UserName).Returns("joe");

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            userServices.AuthenticateAndSetCurrent(new BasicCredential(userNameToSet, "anyPwd"));

            var currentUserName = userServices.Current != null ? userServices.Current.UserName : "";
            var principalUserName = _currentPrincipal.Current != null ? _currentPrincipal.Current.Identity.Name : "";

            Assert.That(currentUserName, Is.EqualTo(expectedUserName));
            Assert.That(principalUserName, Is.EqualTo(expectedUserName));
        }

        [TestCase("joe", "NTLM")]
        [TestCase(null, null)]
        [TestCase("pstephens", null)]
        public void AuthenticateAndSetCurrent_should_set_Principal_AuthenticationType_if_authenticated(string userNameToSet, string expectedAuthenticationType)
        {
            _userRepositoryMock.Setup(ur => ur.GetUser("joe")).Returns(_user);
            _authenticateUserMock.Setup(au => au.Authenticate(new BasicCredential("joe", "pwd"))).Returns("NTLM");
            _userMock.Setup(u => u.UserName).Returns("joe");

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            userServices.AuthenticateAndSetCurrent(new BasicCredential(userNameToSet, "pwd"));

            var principalAuthenticationType = _currentPrincipal.Current != null
                                                  ? _currentPrincipal.Current.Identity.AuthenticationType
                                                  : null;

            Assert.That(principalAuthenticationType, Is.EqualTo(expectedAuthenticationType));
        }

        [Test]
        public void AuthenticateAndSetCurrent_should_clear_Principal_if_null_BasicCredential_is_passed()
        {
            _currentPrincipal.Current = new GenericPrincipal(new GenericIdentity("joe"), new string[0]);

            var userServices = new UserServices(_userRepository, _authenticateUser, _currentPrincipal);

            userServices.AuthenticateAndSetCurrent(null);

            Assert.That(_currentPrincipal.Current, Is.Null);
        }

        private class CustomPrincipal : IPrincipal
        {
            public CustomPrincipal(IIdentity identity)
            {
                Identity = identity;
            }

            public bool IsInRole(string role)
            {
                throw new System.NotImplementedException();
            }

            public IIdentity Identity { get; private set; }
        }

        private class CustomIdentity : IIdentity
        {
            public string Name
            {
                get { return null; }
            }

            public string AuthenticationType
            {
                get { throw new System.NotImplementedException(); }
            }

            public bool IsAuthenticated
            {
                get { throw new System.NotImplementedException(); }
            }
        }
    }
}