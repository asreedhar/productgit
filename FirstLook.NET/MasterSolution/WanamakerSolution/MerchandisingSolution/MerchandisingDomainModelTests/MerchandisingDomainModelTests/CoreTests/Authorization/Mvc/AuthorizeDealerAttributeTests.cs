﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Core.Authorization.Mvc;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization.Mvc
{
    [TestFixture]
    public class AuthorizeDealerAttributeTests
    {
        private Mock<IUnauthorizedResultFactory> _unauthorizedResultFactoryMock;
        private IUnauthorizedResultFactory _unauthorizedResultFactory;
        
        private Mock<IAuthorizeDealer> _authorizeDealerMock;
        private IAuthorizeDealer _authorizeDealer;
        
        private AuthorizationContext _authorizationContext;
        private Mock<HttpContextBase> _httpContextMock;

        [SetUp]
        public void Setup()
        {
            _unauthorizedResultFactoryMock = new Mock<IUnauthorizedResultFactory>();
            _unauthorizedResultFactoryMock.Setup(u => u.CreateResult(It.IsAny<string>())).Returns(
                () => new HttpUnauthorizedResult());
            _unauthorizedResultFactory = _unauthorizedResultFactoryMock.Object;

            _authorizeDealerMock = new Mock<IAuthorizeDealer>();
            _authorizeDealer = _authorizeDealerMock.Object;

            _authorizationContext = new AuthorizationContext();
            _httpContextMock = new Mock<HttpContextBase>();
            _authorizationContext.HttpContext = _httpContextMock.Object;
            _authorizationContext.RouteData.Values.Add("businessunit", "windycit05");
            _authorizationContext.RouteData.Values.Add("dealer", "windycit01");
        }

        private void AssertIsAuthorized()
        {
            AssertAuthorization(true);
        }

        private void AssertIsNotAuthorized()
        {
            AssertAuthorization(false);
        }

        private void AssertAuthorization(bool shouldBeAuthorized)
        {
            Assert.That(_authorizationContext.Result, shouldBeAuthorized ? Is.Null : Is.Not.Null);
        }

        [Test]
        public void Unauthorized_dealer_should_be_unauthorized()
        {
            _authorizeDealerMock.Setup(
                ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult("Not Authorized!"));

            var authDealer = new AuthorizeDealerAttribute(_authorizeDealer, _unauthorizedResultFactory);

            authDealer.OnAuthorization(_authorizationContext);

            AssertIsNotAuthorized();
        }

        [Test]
        public void Authorized_dealer_should_be_authorized()
        {
            _authorizeDealerMock.Setup(
                ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult(null));

            var authDealer = new AuthorizeDealerAttribute(_authorizeDealer, _unauthorizedResultFactory);

            authDealer.OnAuthorization(_authorizationContext);

            AssertIsAuthorized();
        }

        [TestCase("dealer", "windycit01")]
        [TestCase("businessunit", "windycit05")]
        public void Dealer_parameter_should_be_passed_to_inner_IsAuthorized(string dealerParameterName, string expectedDealerCode)
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult(null));

            var authDealer = new AuthorizeDealerAttribute(_authorizeDealer, _unauthorizedResultFactory)
                                 {
                                     DealerParameter = dealerParameterName
                                 };

            authDealer.OnAuthorization(_authorizationContext);

            _authorizeDealerMock
                .Verify(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), expectedDealerCode, It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()));
        }

        [Test]
        public void Default_DealerParameter_should_be_passed_to_inner_IsAuthorized()
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult(null));

            var authDealer = new AuthorizeDealerAttribute(_authorizeDealer, _unauthorizedResultFactory);
            
            authDealer.OnAuthorization(_authorizationContext);

            _authorizeDealerMock
                .Verify(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), "windycit01", It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()));
        }

        [TestCase("qashiner")]
        [TestCase("pstephens")]
        public void Principal_should_be_passed_to_inner_IsAuthorized(string username)
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult(null));
            _httpContextMock.Setup(c => c.User).Returns(new GenericPrincipal(new GenericIdentity(username),
                                                                             new string[0]));

            var authDealer = new AuthorizeDealerAttribute(_authorizeDealer, _unauthorizedResultFactory);
            
            authDealer.OnAuthorization(_authorizationContext);

            _authorizeDealerMock
                .Verify(ad => ad.IsAuthorized(It.Is<IPrincipal>(p => p.Identity.Name == username),
                                              It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()));
        }

        [Test]
        public void AssertDealerUpgrades_should_be_passed_to_inner_IsAuthorized()
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult(null));

            var authDealer = new AuthorizeDealerAttribute(_authorizeDealer, _unauthorizedResultFactory)
                                 {
                                     AssertDealerUpgrades = "Merchandising,Marketing"
                                 };

            authDealer.OnAuthorization(_authorizationContext);

            _authorizeDealerMock
                .Verify(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(),
                                              It.IsAny<string>(), new[] { Upgrade.Merchandising, Upgrade.Marketing }, It.IsAny<bool>()));
        }
    }
}