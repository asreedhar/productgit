﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class DealerCachedRepositoryTests
    {
        private Dictionary<string, object> CacheData;
        private ICache Cache;
        private IDealerData Data;
        private Mock<IDealerRepository> InnerMock;
        private IDealerRepository Inner;

        [SetUp]
        public void Setup()
        {
            SetupCache();
            Data = new Mock<IDealerData>().Object;
            InnerMock = new Mock<IDealerRepository>();
            InnerMock.Setup(i => i.GetData()).Returns(Data);
            Inner = InnerMock.Object;
        }

        private void SetupCache()
        {
            CacheData = new Dictionary<string, object>();
            var cache = new Mock<ICache>();
            cache
                .Setup(c => c.Get(It.IsAny<string>()))
                .Returns<string>(k =>
                                     {
                                         object val;
                                         CacheData.TryGetValue(k, out val);
                                         return val;
                                     });
            cache.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>()))
                .Callback<string, object>((k, v) => CacheData[k] = v);
            cache.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int?>()))
                .Callback<string, object, int?>((k, v, t) => CacheData[k] = v);
            cache.Setup(c => c.Set(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<TimeSpan>()))
                .Callback<string, object, TimeSpan>((k, v, t) => CacheData[k] = v);
            cache.Setup(c => c.Delete(It.IsAny<string>()))
                .Callback<string>(k => CacheData.Remove(k));
            Cache = cache.Object;
        }
        
        [Test]
        public void GetData_once_should_call_inner_GetData()
        {
            var repo = new DealerCachedRepository(Inner, Cache);

            var data = repo.GetData();

            Assert.That(data, Is.SameAs(Data));
            InnerMock.Verify(i => i.GetData(), Times.Once());
        }

        [Test]
        public void GetData_twice_should_call_inner_GetaData_once()
        {
            var repo = new DealerCachedRepository(Inner, Cache);

            var data1 = repo.GetData();
            var data2 = repo.GetData();

            Assert.That(data1, Is.SameAs(Data));
            Assert.That(data2, Is.SameAs(Data));
            InnerMock.Verify(i => i.GetData(), Times.Once());
        }

        [Test]
        public void GetData_once_then_cache_clear_then_GetData_once_should_call_GetData_twice()
        {
            var repo = new DealerCachedRepository(Inner, Cache);

            var data1 = repo.GetData();
            CacheData.Clear();
            var data2 = repo.GetData();

            Assert.That(data1, Is.SameAs(Data));
            Assert.That(data2, Is.SameAs(Data));
            InnerMock.Verify(i => i.GetData(), Times.Exactly(2));
        }
    }
}