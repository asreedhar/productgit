﻿using System.Web.Configuration;
using System.Web.Mvc;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class UnauthorizedResultFactoryTests
    {
        [Test]
        public void CreateResult_should_return_simple_UnauthorizedResult_if_no_CustomErrors_section_in_Web_config()
        {
            var section = new CustomErrorsSection();
            var factory = new UnauthorizedResultFactory(section);

            var result = factory.CreateResult("Failed auth.");

            Assert.That(result, Is.InstanceOf<UnauthorizedResult>());
        }

        [Test]
        public void CreateResult_with_details_should_populate_Message_property_of_UnauthorizedResult()
        {
            var section = new CustomErrorsSection();
            var factory = new UnauthorizedResultFactory(section);

            var result = factory.CreateResult("This is the details.");

            Assert.That(((UnauthorizedResult) result).Message, Is.EqualTo("This is the details."));
        }

        [Test]
        public void CreateResult_with_403_section_in_CustomErrors_should_return_RedirectResult()
        {
            var section = new CustomErrorsSection();
            section.Errors.Add(new CustomError(403, "~/unauthorized.html"));
            var factory = new UnauthorizedResultFactory(section);

            var result = factory.CreateResult("This is the details.");

            Assert.That(result, Is.InstanceOf<RedirectResult>());
        }

        [Test]
        public void CreateResult_with_403_section_in_CustomErrors_should_return_RedirectResult_with_specified_Url()
        {
            var section = new CustomErrorsSection();
            section.Errors.Add(new CustomError(403, "~/unauthorized.html"));
            var factory = new UnauthorizedResultFactory(section);

            var result = factory.CreateResult("This is the details.");

            Assert.That(((RedirectResult) result).Url, Is.EqualTo("~/unauthorized.html"));
        }

        [Test]
        public void CreateResult_with_403_section_in_CustomErrors_should_return_RedirectResult_that_is_not_Permanent()
        {
            var section = new CustomErrorsSection();
            section.Errors.Add(new CustomError(403, "~/unauthorized.html"));
            var factory = new UnauthorizedResultFactory(section);

            var result = factory.CreateResult("This is the details.");

            Assert.That(((RedirectResult) result).Permanent, Is.False);
        }
    }
}