﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Authorization
{
    [TestFixture]
    public class DealerUpgradeListTests
    {
        [TestCase("Merchandising", "Merchandising")]
        [TestCase("MakeADeal", "MakeADeal")]
        [TestCase("MarketData,Ping", "MarketData,Ping")]
        [TestCase("Ping,MarketData,Merchandising", "Ping,MarketData,Merchandising")]
        [TestCase("", "", Description = "AssertDealerUpgrades should accept an empty string.")]
        [TestCase(" ", "", Description = "AssertDealerUpgrades should accept an empty string.")]
        [TestCase(null, "", Description = "AssertDealerUpgrades should accept a null value.")]
        [TestCase(" Ping , MarketData ", "Ping,MarketData", Description = "Handles whitespace too.")]
        [TestCase("ping,marketdata", "Ping,MarketData", Description = "Upgrade codes should be case insensitive.")]
        public void Constructor_should_parse_input_string(string input, string expectedUpgradesCsv)
        {
            var expectedUpgrade = expectedUpgradesCsv
                .Split(',')
                .Where(s => !string.IsNullOrEmpty(s))
                .Select(s => (Upgrade) Enum.Parse(typeof (Upgrade), s, true))
                .ToList();

            var list = new DealerUpgradeList(input);

            Assert.That(list.List, Is.EqualTo(expectedUpgrade));
        }

        [Test]
        public void Original_input_should_be_returned_from_Original()
        {
            var list = new DealerUpgradeList("  Ping,  MarketData");

            Assert.That(list.List, Is.EqualTo(new[] {Upgrade.Ping, Upgrade.MarketData}));
            Assert.That(list.Original, Is.EqualTo("  Ping,  MarketData"));
        }

        [Test]
        public void Invalid_Upgrade_in_input_should_throw_during_List_dereference()
        {
            var list = new DealerUpgradeList("  invalid_upgrade  ");

            IList<Upgrade> r = null;
            Assert.Throws<InvalidOperationException>(() => r = list.List);
            Assert.That(r, Is.Null);
        }
    }
}