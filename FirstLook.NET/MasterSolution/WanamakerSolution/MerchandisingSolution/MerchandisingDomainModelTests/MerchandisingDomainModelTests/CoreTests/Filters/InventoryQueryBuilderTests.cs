﻿using System;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using FirstLook.Merchandising.DomainModel.Workflow;
using MAX.Entities;
using MAX.Entities.Filters;
using Moq;
using NUnit.Framework;

// ReSharper disable RedundantArgumentDefaultValue

namespace MerchandisingDomainModelTests.CoreTests.Filters
{
    [TestFixture]
    public class InventoryQueryBuilderTests
    {
        private Mock<IWorkflowRepository> _workflowsMock;
        private IWorkflowRepository _workflows;
        
        private Mock<IWorkflow> _workflowMock;
        private IWorkflow _workflow;

        [SetUp]
        public void Setup()
        {
            _workflowsMock = new Mock<IWorkflowRepository>();
            _workflows = _workflowsMock.Object;

            _workflowMock = new Mock<IWorkflow>();
            _workflow = _workflowMock.Object;
        }

        private static IInventoryData Create(int id, int newOrUsed = InventoryData.NewVehicle, bool isLowActivity = false, 
            int chromeStyleId = -1, int? ageBucketId = null, string model = null, string make = null)
        {
            var m = new Mock<IInventoryData>();
            m.Setup(i => i.InventoryID).Returns(id);
            m.Setup(i => i.IsNew()).Returns(newOrUsed == InventoryData.NewVehicle);
            m.Setup(i => i.IsPreOwned()).Returns(newOrUsed == InventoryData.UsedVehicle);
            m.Setup(i => i.InventoryType).Returns(newOrUsed);
            m.Setup(i => i.IsLowActivity).Returns(isLowActivity);
            m.Setup(i => i.ChromeStyleId).Returns(chromeStyleId);
            m.Setup(i => i.AgeBucketId).Returns(ageBucketId);
            m.Setup(i => i.Make).Returns(make);
            m.Setup(i => i.Model).Returns(model);
            return m.Object;
        }

        private static IWorkflow CreateWorkflow(WorkflowType t, Predicate<IInventoryData> filter)
        {
            var mock = new Mock<IWorkflow>();
            mock.Setup(wf => wf.Type).Returns(t);
            mock.Setup(wf => wf.Filter).Returns(filter);
            return mock.Object;
        }

        [TestCase(UsedOrNewFilter.New, "5, 15")]
        [TestCase(UsedOrNewFilter.Used, "10, 20")]
        [TestCase(UsedOrNewFilter.Used_and_New, "5, 10, 15, 20")]
        public void FilterByNewOrUsed_should_filter_data_correctly(UsedOrNewFilter filter, string expectedIdsCsv)
        {
            var expectedIds = expectedIdsCsv.Split(',').Select(int.Parse).ToArray();
            var samples = new[]
                              {

                                  Create(5, newOrUsed: 1),
                                  Create(10, newOrUsed: 2),
                                  Create(15, newOrUsed: 1),
                                  Create(20, newOrUsed: 2)
                              };

            var query = new InventoryQueryBuilder(_workflows);

            query.FilterByNewOrUsed(filter);

            var filtered = samples.WorkflowFilter(query).ToArray();

            Assert.That(filtered.Select(d => d.InventoryID).ToArray(),
                        Is.EquivalentTo(expectedIds));
        }

        [Test]
        public void FilterByWorkflow_with_IWorkflow_should_filter_data_correctly()
        {
            _workflowMock
                .Setup(wf => wf.Filter)
                .Returns(d => d.InventoryID%3 == 0); // hypothetical workflow that only returns items with an id divisible by 3.
            var samples = new[]
                    {
                        Create(3),
                        Create(10),
                        Create(15),
                        Create(20)
                    };

            var query = new InventoryQueryBuilder(_workflows);

            query.FilterByWorkflow(_workflow);

            var filtered = samples.WorkflowFilter(query).ToArray();

            Assert.That(filtered.Select(d => d.InventoryID).ToArray(),
                        Is.EquivalentTo(new[] {3, 15}));
        }

        [TestCase(WorkflowType.AllInventory, "5, 10, 15, 20")]
        [TestCase(WorkflowType.LowActivity, "15, 20")]
        [TestCase(WorkflowType.TrimNeeded, "10, 15")]
        public void FilterByWorkflow_with_WorkflowType_should_filter_data_correctly(WorkflowType type, string expectedIdsCsv)
        {
            var expectedIds = expectedIdsCsv.Split(',').Select(int.Parse).ToArray();
            
            _workflowsMock
                .Setup(wfs => wfs.GetWorkflow(WorkflowType.AllInventory))
                .Returns(CreateWorkflow(WorkflowType.AllInventory, d => true));
            _workflowsMock
                .Setup(wfs => wfs.GetWorkflow(WorkflowType.LowActivity))
                .Returns(CreateWorkflow(WorkflowType.LowActivity,
                                        d => d.StatusBucket != MerchandisingBucket.DoNotPost && d.IsLowActivity));
            _workflowsMock
                .Setup(wfs => wfs.GetWorkflow(WorkflowType.TrimNeeded))
                .Returns(CreateWorkflow(WorkflowType.TrimNeeded,
                                        d =>
                                        d.StatusBucket != MerchandisingBucket.DoNotPost &&
                                        d.ChromeStyleId.GetValueOrDefault(-1) == -1));

            var samples = new[]
                    {
                        Create(5, chromeStyleId: 3000),
                        Create(10),
                        Create(15, isLowActivity: true),
                        Create(20, chromeStyleId: 4000, isLowActivity: true)
                    };

            var query = new InventoryQueryBuilder(_workflows);

            query.FilterByWorkflow(type);

            var filtered = samples.WorkflowFilter(query).ToArray();

            Assert.That(filtered.Select(d => d.InventoryID).ToArray(), Is.EquivalentTo(expectedIds));
        }

        [TestCase(3, "10, 15")]
        [TestCase(null, "5, 10, 15, 20")]
        [TestCase(5, "5, 20")]
        public void FilterByAgeBucket_should_filter_data_correctly(int? ageBucketFilter, string expectedIdsCsv)
        {
            var expectedIds = expectedIdsCsv.Split(',').Select(int.Parse).ToArray();
            var samples = new[]
                    {
                        Create(5, ageBucketId: 5),
                        Create(10, ageBucketId: 3),
                        Create(15, ageBucketId: 3),
                        Create(20, ageBucketId: 5)
                    };

            var query = new InventoryQueryBuilder(_workflows);

            query.FilterByAgeBucket(ageBucketFilter);

            var filtered = samples.WorkflowFilter(query).ToArray();

            Assert.That(filtered.Select(d => d.InventoryID).ToArray(), Is.EquivalentTo(expectedIds));
        }

        [TestCase("Toyota", "10")]
        [TestCase("Accord", "5, 20")]
        [TestCase(null, "5, 10, 15, 20")]
        public void FilterByKeyword_should_filter_data_correctly(string keywords, string expectedIdsCsv)
        {
            var expectedIds = expectedIdsCsv.Split(',').Select(int.Parse).ToArray();
            var samples = new[]
                    {
                        Create(5, model: "Accord"),
                        Create(10, make: "Toyota"),
                        Create(15, make: "Ford"),
                        Create(20, model: "Accord")
                    };

            var query = new InventoryQueryBuilder(_workflows);

            query.FilterByKeywords(keywords);

            var filtered = samples.WorkflowFilter(query).ToArray();

            Assert.That(filtered.Select(d => d.InventoryID).ToArray(), Is.EquivalentTo(expectedIds));
        }

        [Test]
        public void FilterByKeyword_should_exclude_all_other_filters()
        {
            _workflowMock
                .Setup(wf => wf.Filter)
                .Returns(d => d.InventoryID % 4 == 0); // hypothetical workflow that only returns items with an id divisible by 4.

            var samples = new[]
                    {
                        Create(5, model: "Accord", newOrUsed: 2, ageBucketId: 1),
                        Create(10, make: "Toyota", newOrUsed: 1, ageBucketId: 1),
                        Create(15, make: "Ford", newOrUsed: 2, ageBucketId: 3),
                        Create(20, model: "Accord", newOrUsed: 1, ageBucketId: 3)
                    };
            
            var query = new InventoryQueryBuilder(_workflows);

            query.FilterByWorkflow(_workflow);
            query.FilterByAgeBucket(3);
            query.FilterByNewOrUsed(UsedOrNewFilter.New);
            query.FilterByKeywords("Accord"); // <-- this should be the only filter that is applied

            var filtered = samples.WorkflowFilter(query).ToArray();

            Assert.That(filtered.Select(d => d.InventoryID).ToArray(), Is.EquivalentTo(new[] { 5, 20 }));
        }
    }
}