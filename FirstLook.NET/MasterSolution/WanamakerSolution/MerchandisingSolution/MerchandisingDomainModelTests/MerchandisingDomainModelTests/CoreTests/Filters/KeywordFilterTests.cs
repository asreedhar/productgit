﻿using System.Collections.Generic;
using System.Globalization;
using FirstLook.Merchandising.DomainModel.Core.Filters;
using FirstLook.Merchandising.DomainModel.Reports.VehicleActivity.Client;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.CoreTests.Filters
{
    [TestFixture]
    public class KeywordFilterTests
    {
        private static VehicleActivity CreateVehicleActivity(
            int? destinationID = null, 
            string make = "", string model = "", string stockNumber = "", 
            string trim = "", string vin = "", int? year = null)
        {
            return new VehicleActivity
                       {
                           DestinationID = destinationID.HasValue ? destinationID.Value : 0,
                           Make = make,
                           Model = model,
                           StockNumber = stockNumber,
                           Trim = trim,
                           VIN = vin,
                           Year = year
                       };
        }

        private static IEnumerable<string> GetPhrases(VehicleActivity va)
        {
            yield return va.DestinationName;
            yield return va.Make;
            yield return va.Model;
            yield return va.StockNumber;
            yield return va.Trim;
            yield return va.VIN;
            yield return va.Year.HasValue ? va.Year.Value.ToString(CultureInfo.InvariantCulture) : null;
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        [TestCase(".")]
        public void No_keywords_should_return_null(string searchText)
        {
            var factory = new KeywordFilterFactory<VehicleActivity>();

            var predicate = factory.GetFilter(searchText, GetPhrases);

            Assert.That(predicate, Is.Null);
        }

        [TestCase(1, "HomeNet", false)]
        [TestCase(2, "Auto", true)]
        [TestCase(2, "Auto Trader", true)]
        [TestCase(1, "Cars.com", true)]
        [TestCase(1, "Cars", true)]
        public void Destination_should_filter_correctly(int destinationId, string searchText, bool expectedResult)
        {
            var va = CreateVehicleActivity(destinationId);
            var predicate = new KeywordFilterFactory<VehicleActivity>().GetFilter(searchText, GetPhrases);

            var result = predicate(va);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [TestCase("Chysler", "Big Chevy", false)]
        [TestCase("Rolls Royce", "Rolls", true)]
        [TestCase("Rolls Royce", "Roy", true)]
        public void Make_should_filter_correctly(string make, string searchText, bool expectedResult)
        {
            var va = CreateVehicleActivity(make: make);
            var predicate = new KeywordFilterFactory<VehicleActivity>().GetFilter(searchText, GetPhrases);

            var result = predicate(va);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [TestCase("3rd Gen Prius", "Accord", false)]
        [TestCase("Land Cruiser", "Cruiser", true)]
        [TestCase("Land Cruiser", "Land", true)]
        [TestCase("Land Cruiser", "Land Cruiser", true)]
        public void Model_should_filter_correctly(string model, string searchText, bool expectedResult)
        {
            var va = CreateVehicleActivity(model: model);
            var predicate = new KeywordFilterFactory<VehicleActivity>().GetFilter(searchText, GetPhrases);

            var result = predicate(va);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [TestCase("A5002", "B2332", false)]
        [TestCase("A5002", "A5002", true)]
        [TestCase("A5002", "A50", true)]
        [TestCase("A5002", "002", true)]
        public void StockNumber_should_filter_correctly(string stockNumber, string searchText, bool expectedResult)
        {
            var va = CreateVehicleActivity(stockNumber: stockNumber);
            var predicate = new KeywordFilterFactory<VehicleActivity>().GetFilter(searchText, GetPhrases);

            var result = predicate(va);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [TestCase("I4 SE", "V6 SE", false)]
        [TestCase("I4 SE", "I4", true)]
        [TestCase("I4 SE", "SE", true)]
        [TestCase("I4 SE", "I4 SE", true)]
        public void Trim_should_filter_correctly(string trim, string searchText, bool expectedResult)
        {
            var va = CreateVehicleActivity(trim: trim);
            var predicate = new KeywordFilterFactory<VehicleActivity>().GetFilter(searchText, GetPhrases);

            var result = predicate(va);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [TestCase("V1234", "V2345", false)]
        [TestCase("V1234", "V1234", true)]
        [TestCase("V1234", "234", true)]
        [TestCase("V1234", "V12", true)]
        public void VIN_should_filter_correctly(string vin, string searchText, bool expectedResult)
        {
            var va = CreateVehicleActivity(vin: vin);
            var predicate = new KeywordFilterFactory<VehicleActivity>().GetFilter(searchText, GetPhrases);

            var result = predicate(va);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [TestCase(2005, "2011", false)]
        [TestCase(2005, "20", true)]
        [TestCase(2005, "2005", true)]
        [TestCase(2005, "05", true)]
        public void VIN_should_filter_correctly(int year, string searchText, bool expectedResult)
        {
            var va = CreateVehicleActivity(year: year);
            var predicate = new KeywordFilterFactory<VehicleActivity>().GetFilter(searchText, GetPhrases);

            var result = predicate(va);

            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}