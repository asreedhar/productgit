﻿using System.CodeDom;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Synonyms;
using MAX.Caching;
using Moq;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.Synonyms
{
    [TestFixture]
    public class SynonymTests
    {
        private ISynonymRepository Repository { get; set; }
        private List<Synonym> Synonyms { get; set; }
        private ISynonymFetcher Fetcher { get; set; }
        private ISynonymEvaluator Evaluator { get; set; }

        [SetUp]
        public void TestSetup()
        {
            Synonyms = new List<Synonym>
            {
                new Synonym {Value = "A/C", SynonymousValue = "AC"},
                new Synonym {Value = "FWD", SynonymousValue = "Front Wheel Drive"},
                new Synonym {Value = "A/C", SynonymousValue = "Air Cond."},
                new Synonym {Value = "RWD", SynonymousValue = "Rear Wheel Drive"},
                new Synonym {Value = "A/C", SynonymousValue = "Air Conditioning"}
            };

            Synonyms.Sort(new SynonymComparer());

            var fetcher = new Mock<ISynonymFetcher>();
            fetcher.Setup(x => x.Fetch()).Returns(Synonyms);
            Fetcher = fetcher.Object;
            Repository = new CachedSynonymRepository(new CacheKeyBuilder(), new MemoryCacheWrapper(), Fetcher );
            Evaluator = new SynonymEvaluator(Repository);
        }

        [TestCase("A/C", "A/C", true)]
        [TestCase("A/C", "Air Cond.", true)]
        [TestCase("A/C", "Air Conditioning", true)]
        [TestCase("A/C", "AC", true)]
        [TestCase("A/C", "ac", true)]
        [TestCase("RWD", "Rear Wheel Drive", true)]
        [TestCase("RWD", "rear wheel drive", true)]
        [TestCase("A/C", "ACx", false)]
        [TestCase("A/C", "No A/C", false)]
        [TestCase("RWD", "Front Wheel Drive", false)]
        [TestCase(null, "Front Wheel Drive", false)]
        [TestCase("Front Wheel Drive", "FWD", false)]   // only one way
        public void Test(string val1, string val2, bool synonymous)
        {
            Assert.AreEqual(synonymous, Evaluator.AreSynonymous(val1, val2));
        }


        [Test]
        public void TestSynonymSort()
        {
            var list = new List<Synonym>
            {
                new Synonym {Value = "A/C", SynonymousValue = "AC"},
                new Synonym {Value = "DubDueces", SynonymousValue = "22 inch wheels"},
                new Synonym {Value = "Dubs", SynonymousValue = "20 inch rims"},
                new Synonym {Value = "A/C", SynonymousValue = "Air Cond."},
                new Synonym {Value = "DubDueces", SynonymousValue = "22 inch rims"},
                new Synonym(),
                new Synonym {Value = "A/C", SynonymousValue = "AC"}
            };

            list.Sort(new SynonymComparer());

            Assert.AreEqual(7, list.Count); // if you add something to the list, add the assert below

            for (var i = 0; i < list.Count; i++)
            {
                var synonym = list[i];

                if (i == 0) Assert.IsTrue(synonym.Value == null && synonym.SynonymousValue == null);
                if (i == 1) Assert.IsTrue(synonym.Value == "A/C" && synonym.SynonymousValue == "AC");
                if (i == 2) Assert.IsTrue(synonym.Value == "A/C" && synonym.SynonymousValue == "AC");
                if (i == 3) Assert.IsTrue(synonym.Value == "A/C" && synonym.SynonymousValue == "Air Cond.");
                if (i == 4) Assert.IsTrue(synonym.Value == "DubDueces" && synonym.SynonymousValue == "22 inch rims");
                if (i == 5) Assert.IsTrue(synonym.Value == "DubDueces" && synonym.SynonymousValue == "22 inch wheels");
                if (i == 6) Assert.IsTrue(synonym.Value == "Dubs" && synonym.SynonymousValue == "20 inch rims");

            }
        }

        [Test]
        public void TestNullSynonym()
        {
            var cmp = new SynonymComparer();

            Assert.AreEqual(0, cmp.Compare(null, null));
        }

    }
}
