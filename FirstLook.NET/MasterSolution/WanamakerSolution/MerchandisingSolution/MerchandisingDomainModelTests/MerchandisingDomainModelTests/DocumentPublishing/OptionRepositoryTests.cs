﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.DataObjects;
using FirstLook.Merchandising.DomainModel.DocumentPublishing.Repositories;
using MvcMiniProfiler.Helpers.Dapper;
using NUnit.Framework;

namespace MerchandisingDomainModelTests.DocumentPublishing
{
    [TestFixture]
    public class OptionRepositoryTests
    {
        public IEnumerable<VDO> ActiveInventoryOptions { get; set; } 

        [SetUp]
        public void GetActiveInventory()
        {
            using (var conn = new SqlConnection("Data Source=betadb01sql;Initial Catalog=Merchandising;Type System Version=SQL Server 2008;Application Name=Merchandising;Integrated Security = True"))
            {
                conn.Open();

                const string query = @"
                    SELECT TOP 10 VDO.*, V.Vin
                    FROM Merchandising.builder.VehicleDetailedOptions VDO
                    JOIN FLDW.dbo.InventoryActive IA
                    ON VDO.inventoryID = IA.InventoryID AND VDO.businessUnitID = IA.BusinessUnitID
                    JOIN FLDW.dbo.Vehicle V
                    ON IA.VehicleID = V.VehicleID AND IA.BusinessUnitID = V.BusinessUnitID
                ";

                var options = conn.Query<VDO>(query).ToList();
                ActiveInventoryOptions = options;
            }
        }

        private IEnumerable<LimitedVdo> GetMultipleMsrpOptions()
        {
            List<LimitedVdo> vdoList;
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString))
            {
                conn.Open();
                const string query = @"
                    SELECT VDO.businessUnitID, VDO.inventoryID, wi.Vin
                    FROM FLDW.dbo.InventoryActive IA
                    JOIN Merchandising.builder.OptionsConfiguration OC
	                    ON IA.BusinessUnitID = OC.businessUnitID AND IA.InventoryID = OC.inventoryId
                    JOIN IMT.dbo.tbl_Vehicle wi
	                    ON wi.VehicleId = IA.VehicleId
                    JOIN Merchandising.builder.VehicleDetailedOptions VDO
	                    ON OC.businessUnitID = VDO.businessUnitID AND OC.inventoryId = VDO.inventoryID
                    JOIN VehicleCatalog.Chrome.Prices P
	                    ON OC.chromeStyleID = P.StyleID AND VDO.optionCode = P.OptionCode
                    WHERE P.CountryCode = 1 AND IA.InventoryId = 32215820
                    GROUP BY VDO.businessUnitID, VDO.inventoryID, VDO.OptionCode, wi.Vin
                    HAVING COUNT(P.MSRP) > 1
                ";

                vdoList = conn.Query<LimitedVdo>(query).ToList();
            }

            return vdoList;
        }

        [Test][Ignore]
        public void ReturnsRows()
        {
            var first = ActiveInventoryOptions.First();
            var r = new OptionsRepository();
            var options = r.GetPublishableVinOptions(first.BusinessUnitId, first.InventoryId, first.VIN);

            if (options != null)
            {
                foreach (var option in options)
                {
                    Console.WriteLine("Option Code: {0}, Desc: {1}", option.OptionCode, option.OptionText);
                }
            }
            Assert.IsNotNull(options);
            Assert.That(options.Count > 0 );
        }

        // 
        [Test]
        [Ignore]
        public void GetPublishableVinOptions_1GNSCCE03DR192971()
        {
            var r = new OptionsRepository();
            var options = r.GetPublishableVinOptions(105363, 37408290, "1GNSCCE03DR192971");

            if (options != null)
            {
                foreach (var option in options)
                {
                    Console.WriteLine("Option Code: {0}, Desc: {1}", option.OptionCode, option.OptionText);                                    
                }
            }
            Assert.IsNotNull(options);
            Assert.That(options.Count > 0);
        }

        [Test][Ignore]
        public void GetPublishableVinOptions_5UXFE43588L033184()
        {
            var r = new OptionsRepository();
            var options = r.GetPublishableVinOptions(105363, 37441511, "5UXFE43588L033184");

            if (options != null)
            {
                foreach (var option in options)
                {
                    Console.WriteLine("Option Code: {0}, Desc: {1}", option.OptionCode, option.OptionText);
                }
            }
            Assert.IsNotNull(options);
            Assert.That(options.Count > 0);                        
        }

        [Test][Ignore]
        public void VinOptionsToXml()
        {
            var first = ActiveInventoryOptions.First();
            var r = new OptionsRepository();
            var options = r.GetPublishableVinOptions(first.BusinessUnitId, first.InventoryId, first.VIN);
            var xmlToPublish = OptionsToXml.ToXmlString(options);
            Console.WriteLine(xmlToPublish);
        }

        [Test][Ignore]
        public void VinOptionsMsrp()
        {
            var first = GetMultipleMsrpOptions().First();
            var r = new OptionsRepository();
            var options = r.GetPublishableVinOptions(first.BusinessUnitId, first.InventoryId, first.VIN);
            var xml = OptionsToXml.ToXmlString(options);
            Console.WriteLine(xml);
        }
    }


    public class LimitedVdo
    {
        public int BusinessUnitId;
        public int InventoryId;
        public string VIN;
    }

    public class VDO
    {
        public int VehicleDetailedOptionID { get; set; }
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string VIN { get; set; }
        public string OptionCode { get; set; }
        public string DetailText { get; set; }
        public string OptionText { get; set; }
        public string CategoryList { get; set; }
        public bool IsStandardEquipment { get; set; }
    }
}
